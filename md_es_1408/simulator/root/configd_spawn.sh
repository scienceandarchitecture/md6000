
##********************************************************************************
 # MeshDynamics 
 # -------------- 
 # File     : configd_spawn.sh
 # Comments : Script executed by configd after mesh starts
 # Created  : 9/25/2006
 # Author   : Sriram Dayanandan
 # Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
 #
 # File Revision History 
 # -----------------------------------------------------------------------------
 # | No  |Date      |  Comment                                        | Author |
 # -----------------------------------------------------------------------------
 # |  1  |2/16/2007 | Added SMASH SNMP daemon                         | Sriram |
 # -----------------------------------------------------------------------------
 # |  0  |9/25/2006 | Created.                                        | Sriram |
 # -----------------------------------------------------------------------------
 #*******************************************************************************/

# This script is executed by configd right after it assigns the mip0 the correct IP address
# This happens right after the DS and WM (DCA/etc) are initialized.
# It is advised to provide details of each application being spawned using comments

#Auto-Start-Begin

#Block-Begin

# Start the IPerf daemon

chmod +x /usr/bin/iperf
/usr/bin/iperf -s -D

#Block-End

#Block-Begin

# Start the SMASH SNMP daemon

chmod +x /sbin/meshsnmpd
/sbin/meshsnmpd&

#Block-End

#Block-Begin

dmesg -n 1

#Block-End


#Auto-Start-End

