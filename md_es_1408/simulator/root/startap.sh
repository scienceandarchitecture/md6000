
##********************************************************************************
 # MeshDynamics 
 # -------------- 
 # File     : startap.sh
 # Comments : Script for starting meshap
 # Created  : 4/22/2005
 # Author   : Sriram Dayanandan
 # Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
 #
 # File Revision History 
 # -----------------------------------------------------------------------------
 # | No  |Date      |  Comment                                        | Author |
 # -----------------------------------------------------------------------------
 # |  8  |08/22/2008| Added meshd configure_sip						  | Abhijit|
 # -----------------------------------------------------------------------------
 # |  7  |01/07/2008| Added meshd vconfigure						  | Abhijit|
 # -----------------------------------------------------------------------------
 # |  6  |9/25/2006 | Specified broadcast address for interfaces      | Sriram |
 # -----------------------------------------------------------------------------
 # |  5  |03/14/2006| Changes for ACL		                          | Bindu  |
 # -----------------------------------------------------------------------------
 # |  4  |9/19/2005 | Automatic private addressing corrected.         | Sriram |
 # -----------------------------------------------------------------------------
 # |  3  |9/12/2005 | Automatic private addressing used.              | Sriram |
 # -----------------------------------------------------------------------------
 # |  2  |6/21/2005 | Misc changes.                                   | Sriram |
 # -----------------------------------------------------------------------------
 # |  1  |5/17/2005 | Added miscd and watchd.                         | Sriram |
 # -----------------------------------------------------------------------------
 # |  0  |4/22/2005 | Created.                                        | Sriram |
 # -----------------------------------------------------------------------------
 #*******************************************************************************/

# Execute load_torna script to load torna_natsemi and meshap

echo "updating mip0 configuration in /etc/config/network"

#echo $'\n' >> /etc/config/network
#echo $'config interface mip' >> /etc/config/network
#echo $'\toption ifname mip0' >> /etc/config/network
#echo $'\toption proto static' >> /etc/config/network
#echo $'\toption ipaddr 192.168.0.100' >> /etc/config/network
#echo $'\toption netmask 255.255.255.0' >> /etc/config/network
#echo $'\toption gateway 192.168.0.1' >> /etc/config/network
#echo $'\toption broadcast 192.168.0.255' >> /etc/config/network

/root/load_torna

FLAG=0

echo "Bringing up wlan0,wlan1, wlan2 and wlan3"

ifconfig wlan0 169.254.129.1 netmask 255.255.255.0 broadcast 169.254.129.255
#ifconfig wlan1 169.254.130.1 netmask 255.255.255.0 broadcast 169.254.130.255
ifconfig wlan2 169.254.131.1 netmask 255.255.255.0 broadcast 169.254.131.255
ifconfig wlan3 169.254.132.1 netmask 255.255.255.0 broadcast 169.254.132.255

sleep 3

echo "validating ht_vht_configurations..."
configd validate 1>/var/log/validation_log
if [ $? != 0 ]; then
echo -e "\n\nError in ht_vht validations\n"
#exit
else
echo -e "\n\nvalidations success"
fi
echo "Create vlan interface..."
configd vlan
echo "Virtual configuring mesh..."
/sbin/meshd vconfigure
echo "Configuring mesh..."
/sbin/meshd configure
echo "Configuring mesh dot11e..."
/sbin/meshd configure_dot11e
echo "Configuring mesh ACL..."
/sbin/meshd configure_acl
echo "Configuring mobility..."
/sbin/meshd configure_mobility
echo "Configuring SIP..."
/sbin/meshd configure_sip

dmesg -c > /dev/null

echo "Starting mesh..."

sleep 5
/sbin/meshd start

sleep 5
configd 1>/dev/null &
#configd &
miscd&
watchd&
echo "Setup LED's"
if [ -d /sys/class/leds/user ]; then
	echo "meshap" > /sys/class/leds/user/trigger
elif [ -d /sys/class/leds/user1 ]; then
	echo "meshap" > /sys/class/leds/user1/trigger
elif [ -d /sys/class/leds/wlan ]; then
	echo "meshap" > /sys/class/leds/wlan/trigger
elif [ -d /sys/class/leds/gpio1 ]; then
	echo "meshap" > /sys/class/leds/gpio1/trigger
elif [ -d /sys/class/leds/gpio2 ]; then
	echo "meshap" > /sys/class/leds/gpio2/trigger
elif [ -d /sys/class/leds/gpio3 ]; then
	echo "meshap" > /sys/class/leds/gpio3/trigger
elif [ -d /sys/class/leds/gpio4 ]; then
	echo "meshap" > /sys/class/leds/gpio4/trigger
fi

#chmod +x /root/nc_log.sh
#/root/nc_log.sh
