intf=${1}

ifconfig eth2 up
ifconfig ${1} up
wmediumd -c /etc/config/wmediumd_2.conf > /dev/null &
wpa_supplicant -i ${1} -c /etc/config/wpa_supplicant.conf &
