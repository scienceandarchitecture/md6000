import filelock
from copy import copy
from subprocess import Popen,PIPE,STDOUT
import sys, threading, os, socket, json, time
import ConfigParser
import subprocess
import fcntl
import struct
import shutil
import logging

VM_COORDINATES_PORT = 4970
WMEDIUMD_DEST_PORT = ('localhost',4975)

Config = ConfigParser.RawConfigParser()
Config.read("vm_config.ini")

def ConfigSectionMap(section):    #function to read data from local_database_status_file.ini
    dict1 = {}
    options = Config.options(section)
    for option in options:
        try:
            dict1[option] = Config.get(section, option)
            if dict1[option] == -1:
                DebugPrint("skip: %s" % option)
        except:
            print("exception on %s!" % option)
            dict1[option] = None
    return dict1


def get_ip_address(ifname):
        s = socket.socket(socket.AF_INET,socket.SOCK_DGRAM)
        ETH3_IP = socket.inet_ntoa( fcntl.ioctl(s.fileno(), 0x8915, struct.pack('256s', ifname[:15]))[20:24] )
        s.close()
        return ETH3_IP

MGMT_IP = get_ip_address("eth3")

def file_edit(str_main,param,new_value):
        Config.set(str_main,param,new_value)
        cfgfile=open("vm_config.ini",'w')
        Config.write(cfgfile)
        cfgfile.close


def vm_coordinates_sendto_own_wmediumd():
        vmtowmedium_coordinates_sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
	mgmt_mac_data = ConfigSectionMap(MGMT_IP)['mgmt_mac']
        vm_coordinates_data = ConfigSectionMap(MGMT_IP)['vm_coordinates']
        vm_coordinates_action_data = "2"
        FINAL_WMEDIUMD_DATA = mgmt_mac_data+" " +vm_coordinates_data+" " +vm_coordinates_action_data
        vmtowmedium_coordinates_sock.sendto(FINAL_WMEDIUMD_DATA,WMEDIUMD_DEST_PORT)

        section_available_list = Config.sections()
	section_available_list.remove(MGMT_IP)
        for wmediumd_section_identifier in section_available_list:
   	       mgmt_mac_data = ConfigSectionMap(wmediumd_section_identifier)['mgmt_mac']
               vm_coordinates_data = ConfigSectionMap(wmediumd_section_identifier)['vm_coordinates']
               vm_coordinates_action_data = "2"
               FINAL_WMEDIUMD_DATA = mgmt_mac_data+" " +vm_coordinates_data+" " +vm_coordinates_action_data
               vmtowmedium_coordinates_sock.sendto(FINAL_WMEDIUMD_DATA,WMEDIUMD_DEST_PORT)
        vmtowmedium_coordinates_sock.close()
        return True


def vm_coordinates_sendto_wmediumd(str_vm_coordinates_data):
	section_available_list = Config.sections()
	section_available_list.remove(MGMT_IP)
	file_edit(MGMT_IP,"vm_coordinates",str_vm_coordinates_data['data_1']['vm_coordinates'])
	str_vm_coordinates_data = json.dumps(str_vm_coordinates_data)
        vm_coordinates_sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

	vm_coordinates_sendto_own_wmediumd()

	for vm_coordinates_section_identifier in section_available_list:
        	#print str_vm_coordinates_data
          	vm_coordinates_sock.sendto(str_vm_coordinates_data,(vm_coordinates_section_identifier,VM_COORDINATES_PORT))

      	vm_coordinates_sock.close()



if __name__ == "__main__":
	section_available_list = Config.sections()
	str_vm_coordinates_data = {}
	str_vm_coordinates_data['action_tag'] = "UPDATE"
	vm_coordinates_create_data = {}
 	vm_coordinates_create_data['mgmt_ip'] = MGMT_IP
        vm_coordinates_create_data['vm_coordinates'] = ConfigSectionMap(MGMT_IP)['vm_coordinates']
        vm_coordinates_create_data['mgmt_mac'] = ConfigSectionMap(MGMT_IP)['mgmt_mac']
        str_vm_coordinates_data['data_1'] = vm_coordinates_create_data

        vm_mobile_speed = ConfigSectionMap(MGMT_IP)['vm_mobile_speed']
        vm_mobile_distance = ConfigSectionMap(MGMT_IP)['vm_mobile_distance']
	DEFAULT_VALUE = 1 # assuming that 1 = 1 meter
	TOTAL_DEFAULT_DISTANCE = int(vm_mobile_distance)
	NODE_SPEED_KM = int(vm_mobile_speed)  #speed km/hr
	NODE_SPEED_M = NODE_SPEED_KM * 1000
	NODE_SPEED_PER_SECOND = NODE_SPEED_M/3600
	NODE_COORDINATES_STRING = vm_coordinates_create_data['vm_coordinates']
	NODE_COORDINATES_LIST = NODE_COORDINATES_STRING.split(":")
	TOTAL_DISTANCE = 0
	SLEEP_TIME = 5

	while True:
		while TOTAL_DISTANCE < TOTAL_DEFAULT_DISTANCE:
			DISTANCE_COVERED = SLEEP_TIME*NODE_SPEED_PER_SECOND
			FINAL_DISTANCE_COVERED = DISTANCE_COVERED/DEFAULT_VALUE
			NEW_DATA = int(NODE_COORDINATES_LIST[0]) + FINAL_DISTANCE_COVERED
			TOTAL_DISTANCE = TOTAL_DISTANCE + (FINAL_DISTANCE_COVERED*DEFAULT_VALUE)
			NODE_COORDINATES_LIST[0] = str(NEW_DATA)
        		str_vm_coordinates_data['data_1']['vm_coordinates'] =  NODE_COORDINATES_LIST[0]+":00:00"
			time.sleep(SLEEP_TIME)
			#print str_vm_coordinates_data
	        	vm_coordinates_ret = vm_coordinates_sendto_wmediumd(str_vm_coordinates_data)
		while TOTAL_DISTANCE > 0:
			DISTANCE_COVERED = SLEEP_TIME*NODE_SPEED_PER_SECOND
			FINAL_DISTANCE_COVERED = DISTANCE_COVERED/DEFAULT_VALUE
			NEW_DATA = int(NODE_COORDINATES_LIST[0]) - FINAL_DISTANCE_COVERED
			TOTAL_DISTANCE = TOTAL_DISTANCE - (FINAL_DISTANCE_COVERED*DEFAULT_VALUE)
			NODE_COORDINATES_LIST[0] = str(NEW_DATA)
        		str_vm_coordinates_data['data_1']['vm_coordinates'] =  NODE_COORDINATES_LIST[0]+":00:00"
			time.sleep(SLEEP_TIME)
			#print str_vm_coordinates_data
	        	vm_coordinates_ret = vm_coordinates_sendto_wmediumd(str_vm_coordinates_data)

		#break





