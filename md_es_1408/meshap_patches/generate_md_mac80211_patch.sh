#!/bin/bash

rm -rf compat-wireless-2014-05-22*
mkdir -p compat-wireless-2014-05-22_base
mkdir -p compat-wireless-2014-05-22/net/mac80211/
cp -r ../md-mac80211 compat-wireless-2014-05-22/net/mac80211/

diff  -Ebur --unidirectional-new-file -x ath* -x ipkg-* -x Kconfig.* -x 1 -x autoconf.h -x .config -x .config.old -x *mod.c -x compat_version -x postinst -x compat_autoconf.h  -x *.o -x *.cmd -x *.ko -x *.order -x *.txt -x control -x *.symvers -x *.mod -x *.dep_files -x cscope.out -x tags -x ipkg-imx6 /opt/1408/compat-wireless-2014-05-22_base compat-wireless-2014-05-22 | grep  -v "Ebur" > ../meshap_patches/meshap_md_mac80211.patch
