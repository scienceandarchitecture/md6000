#! /bin/sh

COMPAT_DIR=${PWD}/../compat-wireless/
MESHAP_PATCHES_DIR=${PWD}/../meshap_patches/

if [ $# -eq 0 ]
then
    echo " Please specifiy the patch to be generated"
    echo " Options : core md_mac hw_sim "
    exit 0
elif [ -n =$1 ]
then
    patch=$1
fi

case $patch in
    "core") 
        echo "Create pathc for core "
        echo ${COMPAT_DIR};echo ${MESHAP_PATCHES_DIR};
        cd ${COMPAT_DIR}; 
        diff  -Ebur --unidirectional-new-file -x ipkg-* -x Kconfig.* -x 1 -x autoconf.h -x .config -x .config.old -x *mod.c -x compat_version -x postinst -x compat_autoconf.h  -x *.o -x *.cmd -x *.ko -x *.order -x *.txt -x control -x *.symvers -x *.mod -x *.dep_files -x cscope.out -x tags -x ipkg-imx6 -x mac80211_hwsim.c --exclude=md-mac80211 compat-wireless-2014-05-22_base compat-wireless-2014-05-22 | grep  -v "Ebur" > ${MESHAP_PATCHES_DIR}/compat_core_meshap.patch
        cd -;
        exit 0
        ;;
    "md_mac")
        echo "Create patch for md_mac"
        echo ${COMPAT_DIR};
        cd ${COMPAT_DIR}; 
        diff  -Ebur --unidirectional-new-file -x ath* -x ipkg-* -x Kconfig.* -x 1 -x autoconf.h -x .config -x .config.old -x *mod.c -x compat_version -x postinst -x compat_autoconf.h  -x *.o -x *.cmd -x *.ko -x *.order -x *.txt -x control -x *.symvers -x *.mod -x *.dep_files -x cscope.out -x tags -x ipkg-imx6 -x mac80211_hwsim.c  compat-wireless-2014-05-22_base/net/mac80211/md-mac80211/  compat-wireless-2014-05-22/net/mac80211/md-mac80211/ | grep  -v "Ebur" > ${MESHAP_PATCHES_DIR}/compat_md-mac80211.patch
        cd -;
        exit 0
        ;;
    "hw_sim")
        echo ${COMPAT_DIR};
        cd ${COMPAT_DIR}; 
        diff  -Ebur --unidirectional-new-file -x ath* -x ipkg-* -x Kconfig.* -x 1 -x autoconf.h -x .config -x .config.old -x *mod.c -x compat_version -x postinst -x compat_autoconf.h  -x *.o -x *.cmd -x *.ko -x *.order -x *.txt -x control -x *.symvers -x *.mod -x *.dep_files -x cscope.out -x tags -x ipkg-imx6 compat-wireless-2014-05-22_base/drivers/net/wireless/mac80211_hwsim.c compat-wireless-2014-05-22/drivers/net/wireless/mac80211_hwsim.c | grep  -v "Ebur" > ${MESHAP_PATCHES_DIR}/compat_hwsim.patch
        cd -;
        exit 0
        ;;
    *)
        echo "Sorry unkown patch option"
        echo "options are core md_mac hw_sim" 
        exit 0
        ;;
esac
