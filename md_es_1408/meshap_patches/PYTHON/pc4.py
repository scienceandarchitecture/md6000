#!/usr/bin/env python
#array = []
#y = [] 
s = []    # array contains error posititons address
t = []    # array contains module names
v = []    # array contains base addresses for all modules
da = []
dac = []
un_da = []
un_dm = []
un_dac = []
un_arra = []
replace_un = []
macis = 0
meshapis = 0
keris = 0
unknown_mod = 0
dm = []
merge_try = []
funct_num = []
funct_dic = {}
kerm1 = []
kera = []
un = []
output_funct = []
kernel1 = []
ker_index = []
kernala = []  # address to be replaced
kernala_deci = []
preplace = []
freplace = []
mac = []
cho = 1
mac_index = []
meshap_index = []
base_array = []   # base address for adresses to be replaced
base_sub = []
base_subhex = []
replace_mac = []
replace_meshap = []
rep_ker2 = []
mesha = []
meshm = []
meshap = []
mesh = []
modu = []
unknown_index = []
un_funct = []
word1 = '['
word2 = ']'
word3 = ' '
word4 = '[<[a-z0-9]+>]'
word5 = 'Function entered at <'
word6 = ' from '
word7 = '_NOT_FOUND'
ind = 0
index3 = 0
index7 = 0
ind1 = 0
#flna = raw_input("ENTER FILENAME CONTAINING BACKTRACE ")
flna = "bt"
flna = str(flna)
#flna1 = raw_input("ENTER FILENAME CONTAING MODULES AND BASE ADDRESSES ")
flna1 = "proc"
flna1 = str(flna1)
def arra():                  #extracting address from backtrace
 import re
 #flna = raw_input("Enter filename containg backtrace ")
 #flna = str(flna)
 with open(flna, 'r') as infile:
  with open('pr0f11.txt', 'w') as outfile:
   for line in infile:
     if re.findall('[<[a-z0-9]+>]', line):
      a = re.findall('[<[a-z0-9]+>]',line)
      #array.append(x)
      outfile.write('%s\n' % (a))
 return
arra()

def re_array():              #obtaining address in correct form
 import re
 with open('pr0f11.txt', 'r') as infile2:
  with open('pr0f12.txt', 'w') as outfile2:
   for line in infile2:
    line = line.strip()
    if re.findall('[a-z0-9]+',line):
      b = re.findall('[a-z0-9]+',line)
    # y.append(t)
      outfile2.write('%s\n' % (b))
 return
re_array()

def split_it():          # splitting of two addresseses in one line to seperate lines
 with open('pr0f12.txt', 'r') as infile3:
  with open('pr0f13.txt', 'w') as outfile3:
   for line in infile3:
    outfile3.write(line.replace(',', '\n'))
 return
split_it()
def split2():
 with open('pr0f13.txt', 'r') as infile4:
  with open('pr0f14.txt', 'w') as outfile4:
   for line in infile4:
    if word1 in line:
     outfile4.write(line.replace(word1, ''))
 return
split_it()
def split2():
 with open('pr0f13.txt', 'r') as infile4:
  with open('pr0f14.txt', 'w') as outfile4:
   for line in infile4:
    if word1 in line:
     outfile4.write(line.replace(word1, ''))
    if word2 in line:
     outfile4.write(line.replace(word2, ''))
 return
split2()
def final_split():
 with open('pr0f14.txt', 'r') as infile5:
  with open('pr0f15.txt', 'w') as outfile5:
   for line in infile5:
    if word3 in line:
     outfile5.write(line.replace(word3,''))
    else:
     outfile5.write(line)
 return
final_split()
def add0x():
 import re
 with open('pr0f15.txt', 'r') as infile10:
  with open('pr0f16.txt', 'w') as outfile10:
   for line in infile10:
     outfile10.write(line.replace('\'', '\'0x', 1))
 return
add0x()

def remove_q():   #remove quotes
 import re
 with open('pr0f16.txt', 'r') as infile11:
  with open('pr0f17.txt', 'w') as outfile11:
   for line in infile11:
    outfile11.write(line.replace('\'', '', 2))
 return
remove_q()

def final_array():   # reading in array
 import re
 infile6 = open('pr0f17.txt', 'r') 
 for line in infile6.readlines():
  for i in line.split():
   s.append(i)
 return
final_array()
print '____________Addresses to be replaced_________________'
for index2, elem1 in enumerate(s):
  print(index2, elem1)

print '\n'
def base_start():              #extracting module names and addresses
 import re
 with open(flna1, 'r') as infile7:
  with open('pr0basemod.txt', 'w') as outfile7:
   with open('pr0baseadr.txt', 'w') as outfile8:
    for line in infile7:
     if re.findall('(^[a-zA-Z0-9_]+[_]*[a-zA-Z0-9]+ )+',line):
      d = re.findall('(^[a-zA-Z0-9_]+[_]*[a-zA-Z0-9]+ )+',line)
      outfile7.write('%s\n' % (d))
     if re.findall('Live 0x[a-z0-9]+',line):
      e = re.findall('0x[a-z0-9]+',line)
      outfile8.write('%s\n' % (e)) 
 return
base_start()

def ara():
 import re
 with open('pr0basemod.txt', 'r') as infile8:
  for line in infile8:
    if re.findall('([a-zA-Z0-9_]+[_]*[a-zA-Z0-9]+)+',line):
      f = re.findall('([a-zA-Z0-9_]+[_]*[a-zA-Z0-9]+)+',line) 
      t.append(f)
 return
ara()
#print('-----------------------------------------------------------')
#for index2, elem1 in enumerate(t):
  # print(index2, elem1)

def baseadr_change():     #extrcting base addresses
 import re
 with open('pr0baseadr.txt', 'r') as infile9:
  with open('pr0baseadr_change.txt', 'w') as outfile9:
   for line in infile9:
     outfile9.write(line.replace('[\'', ''))
 return
baseadr_change()
def baseadr_change2():
 import re
 with open('pr0baseadr_change.txt', 'r') as infile9:
  with open('pr0baseadr_change2.txt', 'w') as outfile9:
   for line in infile9:
     outfile9.write(line.replace('\']', ''))
 return
baseadr_change2()

def ara2():
 import re
 infile8 = open('pr0baseadr_change2.txt', 'r')
 for line in infile8.readlines():
   for i in line.split():
    v.append(i)
 return
ara2()
#print('-----------------------------------------------------------')
#for index2, elem1 in enumerate(v):
  # print(index2, elem1)

class Node:
  def __init__(self, data1, data2):
    self.left = None
    self.right = None
    self.data1 = data1
    self.data2 = data2

  def insert(self, data1, data2):
    if self.data1:
       if data1 < self.data1:
           if self.left is None:
                self.left = Node(data1, data2)
	   else:
                self.left.insert(data1, data2)
       elif data1 > self.data1:
           if self.right is None:
	        self.right = Node(data1, data2)
           else:
                self.right.insert(data1, data2)
    else:
       self.data1 = data1
       self.data2 = data2

  def print_tree(self):
     if self.left:
       self.left.print_tree()
       print self.data1,
       print self.data2
     else:
       print self.data1,
       print self.data2
     if self.right:
       self.right.print_tree()


  def lookup(self, data1, parent=None):
     if data1 < self.data1:
       if self.left is None:
           return None, None
       if self.left.data1 < data1:
           key = self.left.data1
       else:
           return self.left.lookup(data1, self)
       return key

  def looks(self, data1, parent=None):
      if data1 == self.data1:
        key4 = self.data2
        return key4
      else:
        return self.left.looks(data1, self)
     
                                  
#print '\n___________________BST____________________\n'
print'*******Base addresses with corresponding module names*******'
print'______________________________________________________________'
root = Node(v[0],t[0])
my_len = len(v)
for index in range(1, my_len):
 root.insert(v[index], t[index])

root.print_tree()
my_len2 = len(s)
for index2 in range(0, my_len2):
 addr = root.lookup(s[index2])
 base_array.append(addr)

bi = len(base_array)
for i in range(0, bi):
 if base_array[i] == None:
    base_array[i] = '0'

print '\n********************** base address array for addresses to be replaced in backtrace*************************\n'
for index, elem in enumerate(base_array):
  print(index, elem)	
my_len3 = len(s) 
for index3 in range(0, my_len3):
  val3 = s[index3]
  val4 = int(val3, 16)
  val1 = base_array[index3]
  val2 = int(val1, 16)
  val5 = val4 - val2
  base_sub.append(val5)

base_subhex = [hex(x) [2:] for x in base_sub]
print '\n********************** Subtracted array*************************\n'
for index, elem in enumerate(base_subhex):
  print(index, elem)

with open('pr0kerp.txt', 'w') as outfile:
 for i in base_subhex:
  outfile.write('%s\n' % (i))
def ker_extract():
 import re
 with open('pr0kerp.txt','r') as infile:
  with open('pr0kerp1.txt', 'w') as outfile:  
   for line in infile:
    if re.findall('^c[a-z0-9]+', line):
     a = re.findall('^c[a-z0-9]+', line)
     outfile.write('%s\n' % (a))
 return
ker_extract()
def ker_extract1():
 import re
 with open('pr0kerp1.txt','r') as infile:
   with open('pr0kerp2.txt', 'w') as outfile:
    for line in infile:
     outfile.write(line.replace('[\'', ''))
 return
ker_extract1()
def ker_extract2():
 import re
 with open('pr0kerp2.txt','r') as infile:
   with open('pr0kerp3.txt', 'w') as outfile:
    for line in infile:
     outfile.write(line.replace('\']', ''))
 return
ker_extract2()
def kerp_array():   # reading in array
 import re
 infile6 = open('pr0kerp3.txt', 'r')
 for line in infile6.readlines():
  for i in line.split():
   kernala.append(i)
 return
kerp_array()
kernala_deci = [int(x, 16) for x in kernala]
mylen = len(base_array) 
indo = -1
for index7 in range(0, mylen):
 indo += 1
 if base_array[index7] == '0xbf107000':
  mac.append(base_subhex[index7])
  mac_index.append(indo)
  macis = 1
 if base_array[index7] == '0xbf05c000':
  meshap.append(base_subhex[index7])
  meshap_index.append(indo)
  meshapis = 2
 if base_array[index7] == '0':
  ker_index.append(indo)
  keris = 3
 if base_array[index7] != '0xbf107000' and base_array[index7] != '0xbf05c000' and base_array[index7] != '0':
  un_arra.append(base_subhex[index7])
  un.append(base_array[index7])
  unknown_index.append(index7)
  unknown_mod = 4
unl = len(un)
for i in range(0, unl):
  keyf = root.looks(un[i])
  un_funct.append(keyf)
for x in un_funct:
    if x not in output_funct:
      output_funct.append(x)
preplace = mac_index + meshap_index + ker_index + unknown_index 
#print preplace
prel = len(preplace)
'''
print '\n********************** mac array*************************\n'
for index, elem in enumerate(mac):
  print(index, elem)
#print mac_index
print '\n********************** meshap array*************************\n'
for index, elem in enumerate(meshap):
  print(index, elem)
#print meshap_index
'''
class Nod:

      def __init__(self,info1,info2): #constructor of class
	
          self.info1 = info1  #information for node
          self.info2 = info2
          self.left = None  
          self.right = None 
          self.level = None #level none defined
class searchtree:

      def __init__(self): #constructor of class

          self.root = None


      def create(self,val1,val2):  #create BST

          if self.root == None:

             self.root = Nod(val1,val2)

          else:

             current = self.root

             while 1:

                 if val1 < current.info1:

                   if current.left:
                      current = current.left
                   else:
                      current.left = Nod(val1,val2)
                      break;

                 elif val1 > current.info1:

                    if current.right:
                       current = current.right
                    else:
                       current.right = Nod(val1,val2)
                       break;

                 else:
                    break
      def inorder(self,node):

           if node is not None:

              self.inorder(node.left)
              print node.info1,
              print node.info2
              self.inorder(node.right)

      def find(self, val1):
           return self.findNode(self.root, val1)

      def findNode(self, node, val1):
           if node is None:
             return
           elif val1 == node.info1:
             key1 = node.info2
             return key1
           elif val1 > node.info1:
             if node.right is None:
               key1 = node.info2
               return key1
             if node.right.info1 > val1:
               key1 = node.info2
               return key1
             else:
               return self.findNode(node.right, val1)


print '\n-------------------------------------------------------------------------\n'
if macis == 1:
# flna2 = raw_input("ENTER FILENAME CONTAINING MAC DUMP ")
 flna2 = "mac"
 flna2 = str(flna2)
 def dump0():
  with open(flna2, 'r') as infile:
   with open('pr0dump0.txt', 'w') as outfile:
    for line in infile:
     if line.strip() == 'Disassembly of section .text:':
       break
    for line in infile:
     if line.strip() == '00000000 <cleanup_module>:':
       break
     outfile.write(line)
  return
 dump0()

 def dump1():
  import re
  with open('pr0dump0.txt', 'r') as infile12:
   with open('pr0dump1.txt', 'w') as outfile12:
    for line in infile12:
     if re.findall('[0-9a-z]+ <[A-Za-z0-9_.]+>:', line):
      a = re.findall('[0-9a-z]+ <[A-Za-z0-9_.]+>:', line)
      outfile12.write('%s\n' % (a))
  return
 dump1()

 def dump2():
  import re
  with open('pr0dump1.txt', 'r') as infile13:
   with open('pr0dump_adr.txt', 'w') as outfile13:
     for line in infile13:
      if re.findall('[0-9a-z]+ ', line):
       a = re.findall('[0-9a-z]+ ', line)
       outfile13.write('%s\n' % (a))
  return
 dump2()
 def dumpm():
  import re
  with open('pr0dump1.txt', 'r') as infile13:
   with open('pr0dump_mod.txt', 'w') as outfile14:
    for line in infile13:
     if re.findall('<[A-Za-z0-9_.]+>', line):
       b = re.findall('<[A-Za-z0-9_.]+>', line)
       outfile14.write('%s\n' % (b))
  return
 dumpm()
 def dump3():
  import re
  with open('pr0dump_adr.txt', 'r') as infile15:
   with open('pr0dump_adrchange.txt', 'w') as outfile15:
    for line in infile15:
     outfile15.write(line.replace('[\'', ''))
  return 
 dump3()
 def dump4():
  import re
  with open('pr0dump_adrchange.txt', 'r') as infile15:
   with open('pr0dump_adrfinal.txt', 'w') as outfile15:
    for line in infile15:
     outfile15.write(line.replace('\']', ''))
  return
 dump4()
 def dump5():
  import re
  with open('pr0dump_mod.txt', 'r') as infile15:
   with open('pr0dump_modchange.txt', 'w') as outfile15:
    for line in infile15:
     outfile15.write(line.replace('[\'', ''))
  return
 dump5()
 def dump6():
  import re
  with open('pr0dump_modchange.txt', 'r') as infile15:
   with open('pr0dump_modfinal.txt', 'w') as outfile15:
    for line in infile15:
     outfile15.write(line.replace('\']', ''))
  return
 dump6()
 def ara3():
  infile = open('pr0dump_adrfinal.txt', 'r')
  for line in infile.readlines():
   for i in line.split():
    da.append(i)
  return
 ara3()
 def ara4():
  infile = open('pr0dump_modfinal.txt', 'r')
  for line in infile.readlines():
   for i in line.split():
    dm.append(i)
  return
 ara4()
 '''
 print '\n_______________________________________________________\n'
 print 'BST2\n___________________________________________________________\n'
 for index2, elem1 in enumerate(da):
  print(index2, elem1)
 print '\n******###############***************#####################\n'
 for index2, elem1 in enumerate(dm):
  print(index2,elem1)
 '''
 my_len8 = len(da)
 for index1a in range(0, my_len8):
  key0 = int(da[index1a], 16)
  dac.append(key0)

 ro = searchtree()
 my_len5 = len(dac)
 while(ind < my_len5):
  ro.create(dac[ind], dm[ind])
  ind += 1
 #ro.inorder(ro.root)
 my_len20 = len(mac)
 for index20 in range(0, my_len20):
  num = int(mac[index20], 16)
  repl = ro.find(num)
  replace_mac.append(repl)
 print'_________________________mac function name to be replaced __________________________'
 for index2, elem1 in enumerate(replace_mac):
  print(index2,elem1)
 freplace = replace_mac  
if meshapis == 2:
# flna3 = raw_input("ENTER FILENAME CONTAINING MESHAP DUMP ")
 flna3 = "meshap"
 flna3 = str(flna3)
 def meshap0():
  with open(flna3, 'r') as infile:
   with open('meshap0.txt', 'w') as outfile:
    for line in infile:
     if line.strip() == 'Disassembly of section .text:':
      break
    for line in infile:
     if line.strip() == '00000000 <init_module>:':
      break
     outfile.write(line)
  return
 meshap0()
 def meshap1():
  import re
  with open('meshap0.txt', 'r') as infile:
   with open('meshap1.txt', 'w') as outfile:
    for line in infile:
     if re.findall('[0-9a-z]+ <[A-Za-z0-9_.]+>:', line):
       a = re.findall('[0-9a-z]+ <[A-Za-z0-9_.]+>:', line)
       outfile.write('%s\n' %(a))
  return
 meshap1()
 def meshap2():
  import re
  with open('meshap1.txt', 'r') as infilea:
   with open('meshap_adr.txt', 'w') as outfilea:
    for line in infilea:
     if re.findall('[0-9a-z]+ ', line):
       a = re.findall('[0-9a-z]+ ', line)
       outfilea.write('%s\n' %(a)) 
  return
 meshap2()
 def meshapm():
  import re
  with open('meshap1.txt', 'r') as infile:
   with open('meshap_mod.txt', 'w') as outfile:
    for line in infile:
     if re.findall('<[A-Za-z0-9_.]+>', line):
       b = re.findall('<[A-Za-z0-9_.]+>', line)
       outfile.write('%s\n' %(b))
  return
 meshapm()
 def meshap3():
  import re
  with open('meshap_adr.txt', 'r') as infile:
   with open('meshap_adrchange.txt', 'w') as outfile:
    for line in infile:
     outfile.write(line.replace('[\'', ''))
  return
 meshap3()
 def meshap4():
  import re
  with open('meshap_adrchange.txt', 'r') as infile:
   with open('meshap_adrfinal.txt', 'w') as outfile:
    for line in infile:
     outfile.write(line.replace('\']', ''))
  return
 meshap4()
 def meshap5():
  import re
  with open('meshap_mod.txt', 'r') as infile:
   with open('meshap_modchange.txt', 'w') as outfile:
    for line in infile:
     outfile.write(line.replace('[\'', ''))
  return
 meshap5()
 def meshap6():
  import re
  with open('meshap_modchange.txt', 'r') as infile:
   with open('meshap_modfinal.txt', 'w') as outfile:
    for line in infile:
     outfile.write(line.replace('\']', ''))
  return
 meshap6()
 def aramesha():
  infile = open('meshap_adrfinal.txt', 'r')
  for line in infile.readlines():
   for i in line.split():
    mesha.append(i)
  return
 aramesha()
 def arameshm():
  infile = open('meshap_modfinal.txt', 'r')
  for line in infile.readlines():
   for i in line.split():
    meshm.append(i)
  return
 arameshm()
 mylen8 = len(mesha)
 for i in range(0, mylen8):
  key01 = int(mesha[i], 16)
  mesh.append(key01)
 ro1 = searchtree()
 mylen5 = len(mesh)
 while(ind1 < mylen5):
  ro1.create(mesh[ind1], meshm[ind1])
  ind1 += 1
 #print 'MESHAP**********'
 #ro1.inorder(ro1.root)
 #print meshap
 mylen20 = len(meshap)
 for index in range(0, mylen20):
  num1 = int(meshap[index], 16)
  repl = ro1.find(num1)
  replace_meshap.append(repl)

 print '_________________________________MESHAP FUNCTIONS ARE______________________________'
 for index, elem1 in enumerate(replace_meshap):
  print(index,elem1)
 freplace += replace_meshap
if keris == 3:
# flna4 = raw_input("ENTER FILENAME CONTAING KERNEL DUMP ")
 flna4 = "kernel"
 flna4 = str(flna4)
 def ker0():
  with open(flna4, 'r') as infile:
   with open('ker_dump0.txt', 'w') as outfile:
    for line in infile:
     if line.strip() == 'Disassembly of section .head.text:':
      break
    for line in infile:
     if line.strip() == '00000000 <.comment>:':
      break
     outfile.write(line)
  return
 ker0()
 
 def ker1():
  import re
  with open('ker_dump0.txt', 'r') as infile:
   with open('ker_dump1.txt', 'w') as outfile:
    for line in infile:
     if re.findall('[0-9a-z]+ <[A-Za-z0-9_.]+>:', line):
       a = re.findall('[0-9a-z]+ <[A-Za-z0-9_.]+>:', line)
       outfile.write('%s\n' %(a))
  return
 ker1()
 def ker2():
  import re
  with open('ker_dump1.txt', 'r') as infilea:
   with open('ker_adr.txt', 'w') as outfilea:
    for line in infilea:
     if re.findall('[0-9a-z]+ ', line):
       a = re.findall('[0-9a-z]+ ', line)
       outfilea.write('%s\n' %(a))
  return
 ker2()
 def kerm():
  import re
  with open('ker_dump1.txt', 'r') as infile:
   with open('ker_mod.txt', 'w') as outfile:
    for line in infile:
     if re.findall('<[A-Za-z0-9_.]+>', line):
       b = re.findall('<[A-Za-z0-9_.]+>', line)
       outfile.write('%s\n' %(b))
  return
 kerm()
 def ker3():
  import re
  with open('ker_adr.txt', 'r') as infile:
   with open('ker_adrchange.txt', 'w') as outfile:
    for line in infile:
     outfile.write(line.replace('[\'', ''))
  return
 ker3()
 def ker4():
  import re
  with open('ker_adrchange.txt', 'r') as infile:
   with open('ker_adrfinal.txt', 'w') as outfile:
    for line in infile:
     outfile.write(line.replace('\']', ''))
  return
 ker4()
 def ker5():
  import re
  with open('ker_mod.txt', 'r') as infile:
   with open('ker_modchange.txt', 'w') as outfile:
    for line in infile:
     outfile.write(line.replace('[\'', ''))
  return
 ker5()
 def ker6():
  import re
  with open('ker_modchange.txt', 'r') as infile:
   with open('ker_modfinal.txt', 'w') as outfile:
    for line in infile:
     outfile.write(line.replace('\']', ''))
  return
 ker6()
 def arakera():
  infile = open('ker_adrfinal.txt', 'r')
  for line in infile.readlines():
   for i in line.split():
    kera.append(i)
  return
 arakera()
 def arakerm():
  infile = open('ker_modfinal.txt', 'r')
  for line in infile.readlines():
   for i in line.split():
    kerm1.append(i)
  return
 arakerm()

 mylen8 = len(kera)
 for i in range(0, mylen8):
  key01 = int(kera[i], 16)
  kernel1.append(key01)
 kcount = 0
 indker = 0
 lenkp = len(kernala_deci)
 for i in range(0, lenkp):
   numk = kernala_deci[i]
   kcount = 0
   indker = 0
   while numk > kernel1[kcount]:
    kcount += 1
    if numk == kernel1[kcount]:
      indker = kcount
   if kcount == 0:
    ktemp = 0
   if indker != 0:
    ktemp = indker
   else:
    ktemp = kcount - 1
   ktemp1 = kerm1[ktemp]
   rep_ker2.append(ktemp1)
 print '___________________________KERNEL FUNCTIONS ARE_________________________________'
 for index, elem1 in enumerate(rep_ker2):
  print(index,elem1)
 freplace += rep_ker2

if unknown_mod == 4:
 print 'YOUR BACKTRACE ALSO HAS OTHER MODULES AS SHOWN BELOW'
 for index, elem1 in enumerate(output_funct):
   print(index,elem1)
 unkle  = len(un_funct)
 print(unkle)
 print("unfunct.......",un_funct)
#gprasadh changed to 3
 for i in range(0, 3):
  cho = 1
  print 'DO YOU WANT TO ENTER DUMP FOR FOLLOWING MODULE: '
  print un_funct[i]
  choice = raw_input("ENTER yes or no ")
  choice = str(choice)
  if choice == 'YES' or choice == 'yes':
   cho = 0
   flna5 = raw_input("ENTER FILENAME CONTAINING DUMP ") 
   print flna5
   flna5 = str(flna5)
   def dump_un1():
    import re
    with open(flna5, 'r') as infile12:
     with open('pr0un_dump1.txt', 'w') as outfile12:
      for line in infile12:
       if re.findall('[0-9a-z]+ <[A-Za-z0-9_.]+>:', line):
        a = re.findall('[0-9a-z]+ <[A-Za-z0-9_.]+>:', line)
        outfile12.write('%s\n' % (a))
    return
   dump_un1()
   def dump_un2():
    import re
    with open('pr0un_dump1.txt', 'r') as infile13:
     with open('pr0un_dump_adr.txt', 'w') as outfile13:
       for line in infile13:
        if re.findall('[0-9a-z]+ ', line):
         a = re.findall('[0-9a-z]+ ', line)
         outfile13.write('%s\n' % (a))
    return
   dump_un2()
   def dumpm():
    import re
    with open('pr0un_dump1.txt', 'r') as infile13:
     with open('pr0un_dump_mod.txt', 'w') as outfile14:
      for line in infile13:
       if re.findall('<[A-Za-z0-9_.]+>', line):
         b = re.findall('<[A-Za-z0-9_.]+>', line)
         outfile14.write('%s\n' % (b))
    return
   dumpm()
   def dump_un3():
    import re
    with open('pr0un_dump_adr.txt', 'r') as infile15:
     with open('pr0un_dump_adrchange.txt', 'w') as outfile15:
      for line in infile15:
       outfile15.write(line.replace('[\'', ''))
    return
   dump_un3()
   def dump_un4():
    import re
    with open('pr0un_dump_adrchange.txt', 'r') as infile15:
     with open('pr0un_dump_adrfinal.txt', 'w') as outfile15:
      for line in infile15:
       outfile15.write(line.replace('\']', ''))
    return
   dump_un4()
   def dump_un5():
    import re
    with open('pr0un_dump_mod.txt', 'r') as infile15:
     with open('pr0un_dump_modchange.txt', 'w') as outfile15:
      for line in infile15:
       outfile15.write(line.replace('[\'', ''))
    return
   dump_un5()
   def dump_un6():
    import re
    with open('pr0un_dump_modchange.txt', 'r') as infile15:
     with open('pr0dump_modfinal.txt', 'w') as outfile15:
      for line in infile15:
       outfile15.write(line.replace('\']', ''))
    return
   dump_un6()
   def ara_un3():
    infile = open('pr0un_dump_adrfinal.txt', 'r')
    for line in infile.readlines():
     for i in line.split():
      un_da.append(i)
    return
   ara_un3()
   def ara_un4():
    infile = open('pr0dump_modfinal.txt', 'r')
    for line in infile.readlines():
     for i in line.split():
      un_dm.append(i)
    return
   ara_un4()
   my_unlen8 = len(un_da)
   for index1a in range(0, my_unlen8):
    keyu0 = int(un_da[index1a], 16)
    un_dac.append(keyu0)
   unro = searchtree()
   my_unlen5 = len(un_dac)
   indun = 0
   while(indun < my_unlen5):
    unro.create(un_dac[ind], un_dm[ind])
    indun += 1
   #unro.inorder(ro.root)
   my_unlen20 = len(un_funct)
   #for index20 in range(0, my_len20):
   numu = int(un_arra[i], 16)
   replu = unro.find(numu)
   replace_un.append(replu)
   print'_________________________**function name to be replaced __________________________'
   for index2, elem1 in enumerate(replace_un):
    print(index2,elem1)
  elif choice == 'no' or choice == 'No':
   freplace.append(un_funct[i])
 if cho == 0:
    freplace += replace_un
#freplace = replace_mac + replace_meshap + rep_ker2 + un_funct
print('freplace::::', freplace)
mergel = len(preplace)
print(mergel)
#gprasadh lenim = len(freplace)
lenim = len(freplace)
print ('lenim %d' %lenim)
for i in range(0, lenim): #gprasadh earlier it was lenim times
  funct_dic[preplace[i]] = freplace[i]   #creating dictionary to display output in proper format
for key in funct_dic:
   merge_try.append(funct_dic[key])
lenb = len(base_subhex)
#mergel = len(preplace)
kfhh = len(merge_try)
print ('merge try lenght %d' %kfhh)
def merge():
 k = 0
 import re
 with open('pf0f7.txt', 'w') as outfile:
  #gprasadh for i in range(0, mergel):
  for i in range(0, lenim):
   outfile.write('%s' % (merge_try[i]))
   outfile.write('/')
   outfile.write(base_subhex[i])
   outfile.write('>')
   outfile.write('(')
   outfile.write(s[i])
   outfile.write(')')
   k += 1
   if k % 2 != 0:
    outfile.write(word6)
   if k % 2 == 0:
    outfile.write('\n')
 return
merge()
def merge2(): #gprasadh 
 import re
 with open('pf0f7.txt', 'r') as infi:
  with open('pf0f8.txt', 'w') as oufi:
   for line in infi:
    if re.findall('>/', line):
     oufi.write(line.replace('>/', '/'))
 return
merge2()
def merge3():
 import re
 with open('pf0f8.txt', 'r') as infi:
  with open('pf0f9.txt', 'w') as oufi:
   for line in infi:
    if re.findall('\[\'', line):
     oufi.write(line.replace('[\'', '<'))
    else:
     oufi.write(line)
 return
merge3()
def merge4():
 import re
 with open('pf0f9.txt', 'r') as infi:
  with open('pf0f10.txt', 'w') as oufi:
   for line in infi:
    if re.findall('\'\]', line):
     oufi.write(line.replace('\']', word7))
    else:
     oufi.write(line)
 return
merge4()

def rep2():
 import re
 with open('pf0f10.txt', 'r') as infile:
  with open('pf0f2.txt', 'w') as outfile:
   for line in infile:
    if re.findall('<[A-Za-z0-9_./]+>', line):
     outfile.write(line.replace('<', word5, 1))
 return
rep2()
print '_____________________________________________________________________________________________________________________________'
print '**************************************OUTPUT (along with offset values)******************************************************'
print '_____________________________________________________________________________________________________________________________'
with open('pf0f2.txt', 'r') as infi:
 for line in infi:
  print line,
