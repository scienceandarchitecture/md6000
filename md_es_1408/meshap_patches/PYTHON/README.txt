README on how to run python script for IXP boards

1) cd "build folder" and 

2) then cd PYTHON

2) before running the script, pls save the backtrace in file name "bt" under PYTHON folder 

3) on IXP board, do "cat /proc/modules" pls save the modules base address output in file name "proc" under PYTHON folder

4) simply run "sh -x python_script.sh"
  - this will generate all objdumps from already built *.ko

4) simply run "python pc4.py"
  - this will ask for file name of "backtrace" and "cat proc/modules"
  - also it will ask for file names of modules to debug, pls provide suitably as "mac, ath5k, cfg, kernel, meshap"


Thanks!



