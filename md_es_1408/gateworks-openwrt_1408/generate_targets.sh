echo "Build server Deployment"

PATH_DIR=trunk
GATEWORKS=openwrt
CURR_DIR=gateworks-openwrt_1408
USER_DIR=.

# Command Line Arguments
while getopts "h:v:t:p:" arg; do
	case $arg in
		h)
			echo "./generate_targets.sh -v <version> -t <cns3xxx or imx6 or imx6_1k>"
			exit 1
			;;
		v)
			version=$OPTARG
			echo $version
			;;
		t)
			target=$OPTARG
			echo $target
			;;
		p)
			USER_DIR=${OPTARG}
			echo $USER_DIR
			;;
	esac
done


# Target should be either cns3xxx or imx6 or imx6_1k
if [ ${target} != "cns3xxx" ] && [ ${target} != "imx6" && [ ${target} != "imx6_1k"] ]; then
	echo "Target should be either cns3xxx or imx6 or imx6_1k"
	exit 1
fi

# Target Check --> Target is Mandatory
if [ -z "${target}" ]; then
	echo "Target is Mandatory"
	echo "usage: sudo ./generate_targets.sh -v <version> -t <target> -p <pathdir>"
	exit 1
fi

# Version Check --> Version is Mandatory
if [ -z "${version}" ]; then
	echo "Version is Mandatory"
	echo "usage: sudo ./generate_targets.sh -v <version> -t <target> -p <pathdir>"
	exit 1
fi

if [ -z "${USER_DIR}" ]; then
	echo "Path not specified. Generation tar at current dir."
fi

# Making Directories
mkdir -p ${USER_DIR}/build_server/${version}/${target}/${PATH_DIR}/


CURR_DIR_SERVER=${USER_DIR}/build_server/${version}/${target}
echo "$CURR_DIR_SERVER"

#overiding path
PATH_DIR=${target}/trunk
#Copying Makefile
cp --parents deploy_targets.sh ${CURR_DIR_SERVER}/
cp --parents Makefile ${CURR_DIR_SERVER}/

#Copying all target essentials depending on the target
if [ ${target} = "cns3xxx" ]
then
	echo "Copying Target essentials for $target"
	cp -r --parents ${PATH_DIR}/bin/cns3xxx/* ${CURR_DIR_SERVER}/
	cp -fpr --parents ${PATH_DIR}/build_dir/target-arm_mpcore+vfp_uClibc-0.9.33.2_eabi/root-cns3xxx ${CURR_DIR_SERVER}/
	cp -r --parents tmp_md_base_files/ ${CURR_DIR_SERVER}
	cp --parents tmp_files/* ${CURR_DIR_SERVER}/
	cp -r --parents md_configs/ ${CURR_DIR_SERVER}/
	cp --parents ${PATH_DIR}/staging_dir/host/bin/patcher ${CURR_DIR_SERVER}/

#Jtag essentials for cns3xxx
	cp --parents images_1408/cns3xxx/${GATEWORKS}-cns3xxx-squashfs.img ${CURR_DIR_SERVER}/
	cp --parents bootloader/laguna/* ${CURR_DIR_SERVER}/

#sysupgrade essentials for cns3xxx
	cp --parents ${PATH_DIR}/staging_dir/host/bin/mkimage ${CURR_DIR_SERVER}/
	cp --parents ${PATH_DIR}/build_dir/target-arm_mpcore+vfp_uClibc-0.9.33.2_eabi/linux-cns3xxx/zImage ${CURR_DIR_SERVER}/
	cp --parents ${PATH_DIR}/build_dir/target-arm_mpcore+vfp_uClibc-0.9.33.2_eabi/linux-cns3xxx/uImage ${CURR_DIR_SERVER}/
	cp --parents ${PATH_DIR}/build_dir/target-arm_mpcore+vfp_uClibc-0.9.33.2_eabi/linux-cns3xxx/root.squashfs ${CURR_DIR_SERVER}/
#	cp --parents ${PATH_DIR}/bin/cns3xxx/${GATEWORKS}-cns3xxx-squashfs.img ${CURR_DIR_SERVER}/
	cp --parents ${PATH_DIR}/scripts/combined-image.sh ${CURR_DIR_SERVER}/
	cp --parents ${PATH_DIR}/bin/cns3xxx/${GATEWORKS}-cns3xxx-squashfs-uboot-sysupgrade.bin ${CURR_DIR_SERVER}/
	cp --parents ${PATH_DIR}/build_dir/target-arm_mpcore+vfp_uClibc-0.9.33.2_eabi/linux-cns3xxx/root.squashfs.pad ${CURR_DIR_SERVER}/

	cp --parents ${PATH_DIR}/staging_dir/host/bin/mksquashfs4 ${CURR_DIR_SERVER}/
	cp --parents ${PATH_DIR}/staging_dir/host/bin/padjffs2 ${CURR_DIR_SERVER}/

elif [ ${target} = "imx6" ]
then
	echo "Copying Target essential for $target"

	cp -r --parents tmp_md_base_files/ ${CURR_DIR_SERVER}
	cp --parents tmp_files/* ${CURR_DIR_SERVER}/
	cp --parents md_configs/* ${CURR_DIR_SERVER}/
	cp --parents ${PATH_DIR}/bin/imx6/openwrt-imx6-ventana-rootfs.tar.gz ${CURR_DIR_SERVER}/
	cp --parents images_1408/imx6/root-fs-imx6/etc/init.d/meshdynamics ${CURR_DIR_SERVER}/

	cp --parents ${PATH_DIR}/staging_dir/host/bin/patcher ${CURR_DIR_SERVER}/
	cp --parents ${PATH_DIR}/bin/imx6/openwrt-imx6-uImage ${CURR_DIR_SERVER}/
	cp --parents ${PATH_DIR}/bin/imx6/openwrt-imx6-*-gw*.dtb ${CURR_DIR_SERVER}/
	cp --parents ${PATH_DIR}/staging_dir/host/bin/mkfs.ubifs ${CURR_DIR_SERVER}/
	cp --parents tmp_files/ubinize.cfg ${CURR_DIR_SERVER}/
	cp --parents ${PATH_DIR}/staging_dir/host/bin/ubinize ${CURR_DIR_SERVER}/
	cp --parents scripts/mkimage_jtag ${CURR_DIR_SERVER}/
	cp --parents bootloader/ventana/SPL ${CURR_DIR_SERVER}/
	cp --parents bootloader/ventana/u-boot.img ${CURR_DIR_SERVER}/

elif [ ${target} = "imx6_1k" ]
then
   echo "Copying Target essential for $target"

	cp -r --parents tmp_md_base_files/ ${CURR_DIR_SERVER}
	cp --parents md_configs/* ${CURR_DIR_SERVER}/
	cp --parents  ${PATH_DIR}/bin/imx6/openwrt-imx6-ventana-rootfs.tar.gz ${CURR_DIR_SERVER}/
	cp --parents ${PATH_DIR}/staging_dir/host/bin/patcher ${CURR_DIR_SERVER}/

	cp --parents ${PATH_DIR}/bin/imx6/openwrt-imx6-uImage ${CURR_DIR_SERVER}/
	cp --parents ${PATH_DIR}/bin/imx6/openwrt-imx6-*-gw*.dtb ${CURR_DIR_SERVER}/
	cp --parents ${PATH_DIR}/staging_dir/host/bin/mkfs.ubifs ${CURR_DIR_SERVER}/
	cp --parents tmp_files/ubinize.cfg ${CURR_DIR_SERVER}/
	cp --parents ${PATH_DIR}/staging_dir/host/bin/ubinize ${CURR_DIR_SERVER}/
	cp --parents ./scripts/mkimage_jtag ${CURR_DIR_SERVER}/
	cp --parents bootloader/ventana/SPL ${CURR_DIR_SERVER}/
	cp --parents bootloader/ventana/u-boot.img ${CURR_DIR_SERVER}/

fi

#Compressing build
tar -zcvf build_server_${target}.tar.gz ${USER_DIR}/build_server
if [ ${USER_DIR} != "." ]; then
mv build_server_${target}.tar.gz  ${USER_DIR}
fi
echo "build_server.tar.gz is generated succesfully at path ${USER_DIR}/build_server !!"
echo "scp build_server.tar.gz to the build server and run deploy_targets.sh on the build server"
rm -rf ${USER_DIR}/build_server
