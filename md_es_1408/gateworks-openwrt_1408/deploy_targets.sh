echo "extracting targets"
tar -zxvf build_server_${1}.tar.gz --transform 's/.*\/build_server/build_server/'
test -d /opt/md-man-src || mkdir -p /opt/md-man-src
test -d /opt/md-man-src/${2} || mkdir -p /opt/md-man-src/${2}
mv build_server/${2}/* /opt/md-man-src/${2}
rm -rf build_server
echo "done !!"
