
/********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : patch.h
 * Comments : Torna Common Patching Utility Header
 * Created  : 11/23/2004
 * Author   : Sriram Dayanandan
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * -----------------------------------------------------------------------------
 * | No  |Date      |  Comment                                        | Author |
 * -----------------------------------------------------------------------------
 * |  0  |11/23/2004| Created.                                        | Sriram |
 * -----------------------------------------------------------------------------
 ********************************************************************************/

#ifndef __PATCH_H__
#define __PATCH_H__

#ifdef __cplusplus
extern "C" {
#endif

#define PATCH_ERROR_SUCCESS					0
#define PATCH_ERROR_FILE_NOT_FOUND			-1
#define PATCH_ERROR_SIGNATURE_NOT_FOUND		-2
#define PATCH_ERROR_READ_ACCESS_DENIED		-3
#define PATCH_ERROR_WRITE_ACCESS_DENIED		-4
#define PATCH_ERROR_GENERIC					-5

int		patch_file		(const char* file_name,const unsigned char* mac_address);
int		patch_buffer	(unsigned char* buffer,long buffer_length,const unsigned char* mac_address);

#ifdef __cplusplus
}
#endif

#endif /*__PATCH_H__*/
