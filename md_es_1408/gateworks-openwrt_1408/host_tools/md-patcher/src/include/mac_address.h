
/********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : mac_address.h
 * Comments : Common MAC address parsing routines
 * Created  : 11/23/2004
 * Author   : Sriram Dayanandan
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * -----------------------------------------------------------------------------
 * | No  |Date      |  Comment                                        | Author |
 * -----------------------------------------------------------------------------
 * |  0  |11/23/2004| Created.                                        | Sriram |
 * -----------------------------------------------------------------------------
 ********************************************************************************/

#ifndef __MAC_ADDRESS_H__
#define __MAC_ADDRESS_H__

#ifdef __cplusplus
extern "C" {
#endif

int	mac_address_ascii_to_binary	(const char* mac_address_ascii,unsigned char* mac_address_binary);

#ifdef __cplusplus
}
#endif

#endif /*__MAC_ADDRESS_H__*/
