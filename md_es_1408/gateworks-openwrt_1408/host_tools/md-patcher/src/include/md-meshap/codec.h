/********************************************************************************
* MeshDynamics
* --------------
* File     : codec.h
* Comments : AL Encoder/Decoder for various algorithms
* Created  : 9/26/2004
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  2  |8/22/2008 | Changes for FIPS                                |Abhijit |
* -----------------------------------------------------------------------------
* |  1  |4/8/2005  | DATA_LENGTH_ERROR added                         | Sriram |
* -----------------------------------------------------------------------------
* |  0  |9/26/2004 | Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

#ifndef __CODEC_H__
#define __CODEC_H__

/* Error Codes */


#define AES_KEY_ERROR        -1
#define AES_ENCR_ERROR       -2
#define INVALID_KEY_ERROR    -3
#define UNKNOWN_ERROR        -4
#define KEY_NOT_FOUND        -5
#define NULL_ARGUMENT        -6
#define UUENCODE_ERROR       -7
#define UUDECODE_ERROR       -8
#define DATA_LENGTH_ERROR    -9
#define AES_WRAP_ERROR       -10

struct error_code
{
   char desc[50];
   int  type;
};


#define ENCR_HEADER                                      "ENCR"
#define ENCR_HEADER_LEN                                  4


#define AES_MAX_STRING_LEN                               256
#define AES_BLOCK_SIZE                                   16
#define AES_KEY_SIZE                                     16
#define MAC_ADDR_LEN                                     6


#define AES_ENCRYPTION_DECRYPTION_METHOD_AES_ECB         0
#define AES_ENCRYPTION_DECRYPTION_METHOD_AES_KEY_WRAP    1

#ifdef __cplusplus
extern "C" {
#endif


int _uuencode(const char *name, const char *permissions, const unsigned char *input, int input_length, unsigned char **output);
int _uudecode(const char *name, const char *input, unsigned char **output);

int _aesencrypt(const unsigned char *input,
                int                 input_length,
                const unsigned char *key,
                int                 key_length,
                unsigned char       *output);

int _aesdecrypt(const unsigned char *input,
                int                 input_length,
                const unsigned char *key,
                int                 key_length,
                unsigned char       *output);

int _encode_key(unsigned char *text_key, int text_key_len, unsigned char *mac_address, unsigned char *output);
int _decode_key(unsigned char *buffer, unsigned char *mac_address, unsigned char *output);

int encrypt_helper(unsigned char *input, int input_length, const unsigned char *key, int key_len, unsigned char *output, int enc_dec_method);
int decrypt_helper(unsigned char *input, int input_length, const unsigned char *key, int key_len, unsigned char *output, int enc_dec_method);

#ifdef __cplusplus
}
#endif
#endif /*__CODEC_H__*/
