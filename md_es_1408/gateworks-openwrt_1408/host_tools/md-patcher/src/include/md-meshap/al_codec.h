/********************************************************************************
* MeshDynamics
* --------------
* File     : al_codec.h
* Comments : AL Encoder/Decoder for various algorithms
* Created  : 9/26/2004
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  2  |8/22/2008 | Changes for FIPS                                | Abhijit|
* -----------------------------------------------------------------------------
* |  1  |11/23/2004| Merged changes from India                       | Sriram |
* -----------------------------------------------------------------------------
* |  0  |9/26/2004 | Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

#ifndef __AL_CODEC_H__
#define __AL_CODEC_H__

#define AES_MAX_STRING_LEN    256
#define AES_BLOCK_SIZE        16
#define AES_KEY_SIZE          16

int al_uuencode(AL_CONTEXT_PARAM_DECL const unsigned char *input, int input_length, char **output);
int al_uudecode(AL_CONTEXT_PARAM_DECL const char *input, unsigned char **output);

#define AL_ENCRYPTION_DECRYPTION_METHOD_AES_ECB         0
#define AL_ENCRYPTION_DECRYPTION_METHOD_AES_KEY_WRAP    1

int al_encrypt(AL_CONTEXT_PARAM_DECL const unsigned char *input,
               int                                       input_length,
               const unsigned char                       *key,
               int                                       key_length,
               unsigned char                             *output,
               int                                       enc_dec_method);

int al_decrypt(AL_CONTEXT_PARAM_DECL const unsigned char *input,
               int                                       input_length,
               const unsigned char                       *key,
               int                                       key_length,
               unsigned char                             *output,
               int                                       enc_dec_method);

int al_encode_key(AL_CONTEXT_PARAM_DECL unsigned char *text_key, int text_key_len, unsigned char *mac_address, unsigned char *output);
int al_decode_key(AL_CONTEXT_PARAM_DECL unsigned char *buffer, unsigned char *mac_address, unsigned char *output);

#endif /*__AL_CODEC_H__*/
