/********************************************************************************
* MeshDynamics
* --------------
* File     : al_types.h
* Comments : Abstraction Layer  misc type definitions
* Created  : 4/5/2004
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  0  |4/5/2004  | Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

#include "al_impl.h"
#include "al_atomic.h"

#ifndef __AL_TYPES_H__
#define __AL_TYPES_H__

typedef al_impl_u64_t   al_u64_t;
typedef al_impl_s64_t   al_s64_t;
#endif /*__AL_TYPES_H__*/
