/********************************************************************************
* MeshDynamics
* --------------
* File     : conf_common.h
* Comments : Common configuration parsing routines
* Created  : 8/26/2008
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  0  |8/26/2008 | Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

#ifndef __CONF_COMMON_H__
#define __CONF_COMMON_H__


int al_conf_atoi(const char *string, int *value);
int al_conf_hex_atoi(const char *string, int *value);


#endif /*__CONF_COMMON_H__*/
