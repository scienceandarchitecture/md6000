/********************************************************************************
* MeshDynamics
* --------------
* File     : al_impl.h
* Comments : Shasta AL implementation
* Created  : 9/28/2004
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* | 12  |7/9/2007  | Reset generator architecture changed            | Sriram |
* -----------------------------------------------------------------------------
* | 11  |1/26/2007 | Added ATOMIC DEC                                | Sriram |
* -----------------------------------------------------------------------------
* | 10  |12/14/2006| Reset generator changes                         | Sriram |
* -----------------------------------------------------------------------------
* |  9  |10/24/2006| Spelling mistake corrected                      | Sriram |
* -----------------------------------------------------------------------------
* |  8  |3/13/2006 | Added atomic type definitions                   | Sriram |
* -----------------------------------------------------------------------------
* |  7  |9/22/2005 | Interrupt disable/enable semantics changed      | Sriram |
* -----------------------------------------------------------------------------
* |  6  |6/17/2005 | Misc changes                                    | Sriram |
* -----------------------------------------------------------------------------
* |  5  |6/12/2005 | Status message added                            | Sriram |
* -----------------------------------------------------------------------------
* |  4  |03/09/2005| al_impl_notify_message, al_impl_board_info added| Anand  |
* -----------------------------------------------------------------------------
* |  3  |12/26/2004| Added interrupt functions                       | Sriram |
* -----------------------------------------------------------------------------
* |  1  |12/9/2004 | Swap bug fixed                                  | Sriram |
* -----------------------------------------------------------------------------
* |  0  |9/28/2004 | Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

#ifdef __KERNEL__

//RAMESH16MIG commented
//#include <asm/system.h>

#include <asm/atomic.h>
#endif

#ifndef __AL_IMPL_H__
#define __AL_IMPL_H__

typedef unsigned long long   al_impl_u64_t;
typedef long long            al_impl_s64_t;

#ifdef CODEC_SYS_BIG_ENDIAN // Big Endian

#define al_impl_le32_to_cpu(n)    al_swap32(n)
#define al_impl_le16_to_cpu(n)    al_swap16(n)
#define al_impl_be32_to_cpu(n)    (n)
#define al_impl_be16_to_cpu(n)    (n)

#define al_impl_cpu_to_le32(n)    al_swap32(n)
#define al_impl_cpu_to_le16(n)    al_swap16(n)
#define al_impl_cpu_to_be16(n)    (n)
#define al_impl_cpu_to_be32(n)    (n)

#else // Little Endian

#define al_impl_le32_to_cpu(n)    (n)
#define al_impl_le16_to_cpu(n)    (n)
#define al_impl_be32_to_cpu(n)    al_swap32(n)
#define al_impl_be16_to_cpu(n)    al_swap16(n)

#define al_impl_cpu_to_le32(n)    (n)
#define al_impl_cpu_to_le16(n)    (n)
#define al_impl_cpu_to_be16(n)    al_swap16(n)
#define al_impl_cpu_to_be32(n)    al_swap32(n)
#endif // CODEC_SYS_BIG_ENDIAN

#define AL_IMPL_SNPRINTF    snprintf

int snprintf(char *buf, size_t size, const char *fmt, ...);

#define AL_IMPL_NOTIFY_MESSAGE_TYPE_SET_INTERFACE_INFO    1
#define AL_IMPL_NOTIFY_MESSAGE_TYPE_SET_STATE             2
#define AL_IMPL_NOTIFY_MESSAGE_TYPE_STATUS                3

struct al_impl_notify_message
{
   int  message_type;
   void *message_data;
};

typedef struct al_impl_notify_message   al_impl_notify_message_t;

#define AL_IMPL_BOARD_INFO_MASK_TEMP       1
#define AL_IMPL_BOARD_INFO_MASK_VOLTAGE    2


struct al_impl_board_info
{
   short valid_values_mask;
   short temperature;
   short voltage;
};

typedef struct al_impl_board_info   al_impl_board_info_t;

int al_impl_get_board_info(al_impl_board_info_t *board_info);
int al_impl_notify_message(al_impl_notify_message_t *message);
void al_impl_enable_reset_generator(unsigned char enable_or_disable, int interval_in_millis);
void al_impl_strobe_reset_generator(void);

#ifdef __KERNEL__
#define al_impl_disable_interrupts(flags)    do { preempt_disable(); } while (0)
#define al_impl_enable_interrupts(flags)     do { preempt_enable(); } while (0)
#endif

#if __KERNEL__
#define al_impl_atomic_t    atomic_t
#define AL_IMPL_ATOMIC_SET(av, v)          atomic_set(&(av), (v))
#define AL_IMPL_ATOMIC_GET(av)             atomic_read(&(av))
#define AL_IMPL_ATOMIC_INC(av)             atomic_inc(&(av))
#define AL_IMPL_ATOMIC_DEC_AND_TEST(av)    atomic_dec_and_test(&(av))
#define AL_IMPL_ATOMIC_DEC(av)             atomic_dec(&(av))
#else
#define al_impl_atomic_t    int
#define AL_IMPL_ATOMIC_SET(av, v)          do { (av) = (v); } while (0)
#define AL_IMPL_ATOMIC_GET(av)             (av)
#define AL_IMPL_ATOMIC_INC(av)             do { ++(av); } while (0)
#define AL_IMPL_ATOMIC_DEC_AND_TEST(av)    ((--(av)) == 0)
#define AL_IMPL_ATOMIC_DEC(av)             do { --(av); } while (0)
#endif
#endif /*__AL_IMPL_H__*/
