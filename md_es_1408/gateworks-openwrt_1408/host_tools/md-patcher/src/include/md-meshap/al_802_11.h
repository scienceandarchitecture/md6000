/********************************************************************************
* MeshDynamics
* --------------
* File     : al_802_11.h
* Comments : Abstraction Layer 802.11 definitions
* Created  : 4/5/2004
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* | 42  |02/12/2009| Changes for Action Frame Support                |Abhijit |
* -----------------------------------------------------------------------------
* | 40  |8/22/2008 | Changes for FIPS                                | Sriram |
* -----------------------------------------------------------------------------
* | 39  |05/09/2008| Changes for Beaconing Uplink		              |Prachiti|
* -----------------------------------------------------------------------------
* | 38  |11/28/2007| Probe Request Hook added                        | Sriram |
* -----------------------------------------------------------------------------
* | 37  |4/11/2007 | Changes for Radio diagnostics                   | Sriram |
* -----------------------------------------------------------------------------
* | 36  |2/23/2007 | Changes for Effistream                          |Prachiti|
* -----------------------------------------------------------------------------
* | 35  |2/15/2007 | Added set_tx_antenna                            | Sriram |
* -----------------------------------------------------------------------------
* | 34  |2/14/2007 | Changes for PS PHY sub-types                    | Sriram |
* -----------------------------------------------------------------------------
* | 33  |01/01/2007| Changes for channel specific DFS                 |Prachiti|
* -----------------------------------------------------------------------------
* | 32  |11/8/2006 | Changes for compression support                 | Sriram |
* -----------------------------------------------------------------------------
* | 31  |10/31/2006| Custom frequency changes                        | Sriram |
* -----------------------------------------------------------------------------
* | 30  |02/15/2006| dot11e added                                    | Bindu  |
* -----------------------------------------------------------------------------
* | 29  |02/08/2006| Hide SSID added                                 | Sriram |
* -----------------------------------------------------------------------------
* | 28  |01/17/2006|Max value for AID defined					      | Sriram |
* -----------------------------------------------------------------------------
* | 27  |12/16/2005|supported channels changes					      |prachiti|
* -----------------------------------------------------------------------------
* | 26  |12/14/2005| acktimeout and regdomain changes                |prachiti|
* -----------------------------------------------------------------------------
* | 25  |8/20/2005 | get_bit_rate_info Added                         | Sriram |
* -----------------------------------------------------------------------------
* | 24  |4/11/2005 | Changes after merging                           | Sriram |
* -----------------------------------------------------------------------------
* | 23  |4/8/2005  | Parameter added to set_essid_info               | Sriram |
* -----------------------------------------------------------------------------
* | 22  |2/24/2005 | 802_11_essid_info and set_essid_info added      | Sriram |
* -----------------------------------------------------------------------------
* | 21  |2/21/2005 | Vendor Info added to Beacons & ScanAP           | Anand  |
* -----------------------------------------------------------------------------
* | 20  |1/24/2005 | Changes for 802.11i/WPA security                | Sriram |
* -----------------------------------------------------------------------------
* | 19  |1/6/2005  | Added get_last_beacon_time                      | Sriram |
* -----------------------------------------------------------------------------
* | 18  |11/15/2004| Added capability parameter to assoc hook        | Sriram |
* -----------------------------------------------------------------------------
* | 17  |11/15/2004| Added functions to 802.11 operations for G mode | Sriram |
* -----------------------------------------------------------------------------
* | 16  |10/14/2004| AL_802_11_MAX_BIT_RATES changed to 15           | Sriram |
* -----------------------------------------------------------------------------
* | 15  |10/7/2004 | scan_access_points_active and passive added     | Sriram |
* -----------------------------------------------------------------------------
* | 14  |9/28/2004 | 3 Radio changes                                 | Sriram |
* -----------------------------------------------------------------------------
* | 13  |6/10/2004 | Added more packet size constants                | Sriram |
* -----------------------------------------------------------------------------
* | 12  |6/2/2004  | Added assoc and auth hook operations            | Sriram |
* -----------------------------------------------------------------------------
* | 11  |5/28/2004 | Added FC_SET_TYPE and FC_SET_STYPE macros       | Sriram |
* -----------------------------------------------------------------------------
* | 10  |5/24/2004 | Misc changes for Linux compilation              | Sriram |
* -----------------------------------------------------------------------------
* |  9  |5/5/2004  | al_context added to hook functions              | Anand  |
* -----------------------------------------------------------------------------
* |  8  |4/22/2004 | Added min frame size constants                  | Bindu P|
* -----------------------------------------------------------------------------
* |  7  |4/22/2004 | Added al_ prefix to hook function types         | Bindu P|
* -----------------------------------------------------------------------------
* |  6  |4/19/2004 | Added AL_802_11_MODE_RADIO_OFF                  | Bindu P|
* -----------------------------------------------------------------------------
* |  5  |4/14/2004 | Added  Frame Field Positions for Probe Req/Res  | Bindu  |
* -----------------------------------------------------------------------------
* |  4  |4/13/2004 | Added Information Element IDs                   | Bindu  |
* -----------------------------------------------------------------------------
* |  3  |4/13/2004 | Fixed misc syntax errors                        | Bindu  |
* -----------------------------------------------------------------------------
* |  2  |4/13/2004 | Added frame processing macros                   | Bindu  |
* -----------------------------------------------------------------------------
* |  1  |4/8/2004  | Fixed misc syntax errors                        | Bindu  |
* -----------------------------------------------------------------------------
* |  0  |4/5/2004  | Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

#include "al_endian.h"
#include "al_net_if.h"

#ifndef __AL_802_11_H__
#define __AL_802_11_H__
#include "al.h"
#define AL_802_11_PACKET_GET_BYTE(buffer, position)          ((unsigned char *)buffer)[position]
#define AL_802_11_PACKET_SET_BYTE(buffer, position, byte)    do { ((unsigned char *)buffer)[position] = byte; } while (0)

#ifdef __GNUC__

#define AL_802_11_PACKET_GET_WORD16(buffer, position)           \
   ({                                                           \
      unsigned short word;                                      \
      memcpy(&word, buffer + position, sizeof(unsigned short)); \
      al_le16_to_cpu(word);                                     \
   }                                                            \
   )

#define AL_802_11_PACKET_GET_WORD32(buffer, position)         \
   ({                                                         \
      unsigned int word;                                      \
      memcpy(&word, buffer + position, sizeof(unsigned int)); \
      al_le32_to_cpu(word);                                   \
   }                                                          \
   )


#define AL_802_11_PACKET_SET_WORD16(buffer, position, set_word) \
   do {                                                         \
      unsigned short word;                                      \
      word = al_cpu_to_le16(set_word);                          \
      memcpy(buffer + position, &word, sizeof(unsigned short)); \
   } while (0)


#define AL_802_11_PACKET_SET_WORD32(buffer, position, set_word) \
   do {                                                         \
      unsigned int word;                                        \
      word = al_cpu_to_le32(set_word);                          \
      memcpy(buffer + position, &word, sizeof(unsigned int));   \
   } while (0)


#else
static unsigned short AL_802_11_PACKET_GET_WORD16(unsigned char *buffer, int position)
{
   unsigned short word;

   memcpy(&word, buffer + position, sizeof(unsigned short));
   return al_le16_to_cpu(word);
}


static unsigned int AL_802_11_PACKET_GET_WORD32(unsigned char *buffer, int position)
{
   unsigned int word;

   memcpy(&word, buffer + position, sizeof(unsigned int));
   return al_le32_to_cpu(word);
}


static void AL_802_11_PACKET_SET_WORD16(unsigned char *buffer, int position, unsigned short set_word)
{
   unsigned short word;

   word = al_cpu_to_le16(set_word);
   memcpy(buffer + position, &word, sizeof(unsigned short));
}


static void AL_802_11_PACKET_SET_WORD32(unsigned char *buffer, int position, unsigned int set_word)
{
   unsigned int word;

   word = al_cpu_to_le32(set_word);
   memcpy(buffer + position, &word, sizeof(unsigned int));
}
#endif

#define AL_802_11_TXPOW_DBM                      0x0001 /* Value is in dBm */
#define AL_802_11_TXPOW_MWATT                    0x0002 /* Value is in mW */
#define AL_802_11_TXPOW_PERC                     0x0004 /* Value is in Percentage */
#define AL_802_11_TXPOW_FIXED                    0x0008 /* Power fixed by software */

#define AL_802_11_MODE_AUTO                      0      /* Let the driver decide */
#define AL_802_11_MODE_ADHOC                     1      /* Single cell network */
#define AL_802_11_MODE_INFRA                     2      /* Multi cell network, roaming, ... */
#define AL_802_11_MODE_MASTER                    3      /* Synchronisation master or Access Point */
#define AL_802_11_MODE_REPEAT                    4      /* Wireless Repeater (forwarder) */
#define AL_802_11_MODE_SECOND                    5      /* Secondary master/repeater (backup) */
#define AL_802_11_MODE_RADIO_OFF                 6      /* Shut-off radio */
#define AL_802_11_MODE_ACTIVE_MONITOR            7      /* Special case of AL_802_11_MODE_INFRA (probe request) */
#define AL_802_11_MODE_PASSIVE_MONITOR           8      /* Special case of AL_802_11_MODE_INFRA (beacon listening) */
#define AL_802_11_MODE_PURE_MONITOR              9      /* Pure RF monitoring mode (all RF activity) */

#define AL_802_11_ESSID_MAX_SIZE                 32
#define AL_802_11_VENDOR_INFO_MAX_SIZE           256

#define AL_802_11_MAX_SECURITY_KEYS              4
#define AL_802_11_MAX_KEY_LENGTH                 13

#define AL_802_11_SECURITY_ENCODE_MODE_EASY      0
#define AL_802_11_SECURITY_ENCODE_MODE_STRICT    1

#define AL_802_11_MAX_BIT_RATES                  15

#define AL_802_11_SUPPORTED_RATES_LENGTH         10
#define AL_802_11_MAX_ESSID_LENGTH               32
#define AL_802_11_MAX_SECURITY_KEYS              4
#define AL_802_11_MAX_KEY_LENGTH                 13

#define AL_802_11_AUTHENTICATION_OPEN            0
#define AL_802_11_AUTHENTICATION_SHARED          1


/**
 * Status codes for authencition request/response
 */

#define AL_802_11_STATUS_SUCCESS                        0
#define AL_802_11_STATUS_UNSPECIFIED_FAILURE            1
#define AL_802_11_STATUS_CAPS_UNSUPPORTED               10
#define AL_802_11_STATUS_REASSOC_NO_ASSOC               11
#define AL_802_11_STATUS_ASSOC_DENIED_UNSPEC            12
#define AL_802_11_STATUS_NOT_SUPPORTED_AUTH_ALG         13
#define AL_802_11_STATUS_UNKNOWN_AUTH_TRANSACTION       14
#define AL_802_11_STATUS_CHALLENGE_FAIL                 15
#define AL_802_11_STATUS_AUTH_TIMEOUT                   16
#define AL_802_11_STATUS_AP_UNABLE_TO_HANDLE_NEW_STA    17
#define AL_802_11_STATUS_ASSOC_DENIED_RATES             18


/* Reason codes */

#define AL_802_11_REASON_UNSPECIFIED                       1
#define AL_802_11_REASON_PREV_AUTH_NOT_VALID               2
#define AL_802_11_REASON_DEAUTH_LEAVING                    3
#define AL_802_11_REASON_DISASSOC_DUE_TO_INACTIVITY        4
#define AL_802_11_REASON_DISASSOC_AP_BUSY                  5
#define AL_802_11_REASON_CLASS2_FRAME_FROM_NONAUTH_STA     6
#define AL_802_11_REASON_CLASS3_FRAME_FROM_NONASSOC_STA    7
#define AL_802_11_REASON_DISASSOC_STA_HAS_LEFT             8
#define AL_802_11_REASON_STA_REQ_ASSOC_WITHOUT_AUTH        9
#define AL_802_11_REASON_STA_ASSOC_ANAMOLY                 10

#define AL_BIT(x)    (1 << (x))

/**
 * 802.11 Frame control fields
 */

#define AL_802_11_FC_PVER        (AL_BIT(1) | AL_BIT(0))
#define AL_802_11_FC_TODS        AL_BIT(8)
#define AL_802_11_FC_FROMDS      AL_BIT(9)
#define AL_802_11_FC_MOREFRAG    AL_BIT(10)
#define AL_802_11_FC_RETRY       AL_BIT(11)
#define AL_802_11_FC_PWRMGT      AL_BIT(12)
#define AL_802_11_FC_MOREDATA    AL_BIT(13)
#define AL_802_11_FC_ISWEP       AL_BIT(14)
#define AL_802_11_FC_ORDER       AL_BIT(15)

#define AL_802_11_FC_GET_TYPE(fc)            (((fc) & (AL_BIT(3) | AL_BIT(2))) >> 2)
#define AL_802_11_FC_GET_STYPE(fc)           (((fc) & (AL_BIT(7) | AL_BIT(6) | AL_BIT(5) | AL_BIT(4))) >> 4)
#define AL_802_11_FC_SET_TYPE(fc, type)      do { (fc) &= 0xFFF3; (fc) |= ((type) << 2); } while (0)
#define AL_802_11_FC_SET_STYPE(fc, stype)    do { (fc) &= 0xFF0F; (fc) |= ((stype) << 4); } while (0)

#define AL_802_11_GET_SEQ_FRAG(seq)          ((seq) & (AL_BIT(3) | AL_BIT(2) | AL_BIT(1) | AL_BIT(0)))
#define AL_802_11_GET_SEQ_SEQ(seq)           (((seq) & (~(AL_BIT(3) | AL_BIT(2) | AL_BIT(1) | AL_BIT(0)))) >> 4)

#define AL_802_11_FC_TYPE_MGMT    0
#define AL_802_11_FC_TYPE_CTRL    1
#define AL_802_11_FC_TYPE_DATA    2

/* management */

#define AL_802_11_FC_STYPE_ASSOC_REQ       0
#define AL_802_11_FC_STYPE_ASSOC_RESP      1
#define AL_802_11_FC_STYPE_REASSOC_REQ     2
#define AL_802_11_FC_STYPE_REASSOC_RESP    3
#define AL_802_11_FC_STYPE_PROBE_REQ       4
#define AL_802_11_FC_STYPE_PROBE_RESP      5
#define AL_802_11_FC_STYPE_BEACON          8
#define AL_802_11_FC_STYPE_ATIM            9
#define AL_802_11_FC_STYPE_DISASSOC        10
#define AL_802_11_FC_STYPE_AUTH            11
#define AL_802_11_FC_STYPE_DEAUTH          12
#define AL_802_11_FC_STYPE_ACTION          13

/* control */

#define AL_802_11_FC_STYPE_PSPOLL      10
#define AL_802_11_FC_STYPE_RTS         11
#define AL_802_11_FC_STYPE_CTS         12
#define AL_802_11_FC_STYPE_ACK         13
#define AL_802_11_FC_STYPE_CFEND       14
#define AL_802_11_FC_STYPE_CFENDACK    15

/* data */

#define AL_802_11_FC_STYPE_DATA              0
#define AL_802_11_FC_STYPE_DATA_CFACK        1
#define AL_802_11_FC_STYPE_DATA_CFPOLL       2
#define AL_802_11_FC_STYPE_DATA_CFACKPOLL    3
#define AL_802_11_FC_STYPE_NULLFUNC          4
#define AL_802_11_FC_STYPE_CFACK             5
#define AL_802_11_FC_STYPE_CFPOLL            6
#define AL_802_11_FC_STYPE_CFACKPOLL         7
#define AL_802_11_FC_STYPE_QOS               8

/* capability */

#define AL_802_11_CAPABILITY_ESS                AL_BIT(0)
#define AL_802_11_CAPABILITY_IBSS               AL_BIT(1)
#define AL_802_11_CAPABILITY_CF_POLLABLE        AL_BIT(2)
#define AL_802_11_CAPABILITY_CF_POLL_REQUEST    AL_BIT(3)
#define AL_802_11_CAPABILITY_PRIVACY            AL_BIT(4)
#define AL_802_11_CAPABILITY_SHORT_PREAMBLE     AL_BIT(5)
#define AL_802_11_CAPABILITY_PBCC               AL_BIT(6)
#define AL_802_11_CAPABILITY_CHANNEL_AGILITY    AL_BIT(7)
#define AL_802_11_CAPABILITY_SHORT_SLOT_TIME    AL_BIT(10)
#define AL_802_11_CAPABILITY_DSSS_OFDM          AL_BIT(13)

/**
 * Frame field positions for Authentication frame Request/Response
 */

#define AL_802_11_AUTHENTICATION_ALGORITHM_POSITION        0
#define AL_802_11_AUTHENTICATION_ATSN_POSITION             (AL_802_11_AUTHENTICATION_ALGORITHM_POSITION + 2)
#define AL_802_11_AUTHENTICATION_STATUS_POSITION           (AL_802_11_AUTHENTICATION_ATSN_POSITION + 2)
#define AL_802_11_AUTHENTICATION_CHALLENGETEXT_POSITION    (AL_802_11_AUTHENTICATION_STATUS_POSITION + 2)

/**
 * Frame field positions of various fields in an association request
 */

#define AL_802_11_ASSOCIATION_REQUEST_CAPABILITY_POSITION        0
#define AL_802_11_ASSOCIATION_REQUEST_LISTENINTERVAL_POSITION    (AL_802_11_ASSOCIATION_REQUEST_CAPABILITY_POSITION + 2)
#define AL_802_11_ASSOCIATION_REQUEST_SSID_POSITION              (AL_802_11_ASSOCIATION_REQUEST_LISTENINTERVAL_POSITION + 2)
#define AL_802_11_ASSOCIATION_REQUEST_SSID_START_POSITION        (AL_802_11_ASSOCIATION_REQUEST_SSID_POSITION + 2)
#define AL_802_11_ASSOCIATION_REQUEST_SUPPORTEDRATES_POSITION(ssid_length)          (AL_802_11_ASSOCIATION_REQUEST_SSID_START_POSITION + ssid_length)
#define AL_802_11_ASSOCIATION_REQUEST_SUPPORTEDRATES_START_POSITION(ssid_length)    (AL_802_11_ASSOCIATION_REQUEST_SUPPORTEDRATES_POSITION(ssid_length) + 2)

/**
 * Only for 802.11g i.e (pure 802.11g and 802.11bg mode)
 */

#define AL_802_11_ASSOCIATION_REQUEST_EX_SUPPORTEDRATES_POSITION(ssid_length, supp_rates_length)          (AL_802_11_ASSOCIATION_REQUEST_SUPPORTEDRATES_POSITION(ssid_length) + 2 + supp_rates_length)
#define AL_802_11_ASSOCIATION_REQUEST_EX_SUPPORTEDRATES_START_POSITION(ssid_length, supp_rates_length)    (AL_802_11_ASSOCIATION_REQUEST_EX_SUPPORTEDRATES_POSITION(ssid_length, supp_rates_length) + 2)


/**
 * Frame field positions for various fields in a reassociation request
 */

#define AL_802_11_REASSOCIATION_REQUEST_CAPABILITY_POSITION        0
#define AL_802_11_REASSOCIATION_REQUEST_LISTENINTERVAL_POSITION    (AL_802_11_REASSOCIATION_REQUEST_CAPABILITY_POSITION + 2)
#define AL_802_11_REASSOCIATION_REQUEST_CURRENTAP_POSITION         (AL_802_11_REASSOCIATION_REQUEST_LISTENINTERVAL_POSITION + 2)
#define AL_802_11_REASSOCIATION_REQUEST_SSID_POSITION              (AL_802_11_REASSOCIATION_REQUEST_CURRENTAP_POSITION + 6)
#define AL_802_11_REASSOCIATION_REQUEST_SSID_START_POSITION        (AL_802_11_REASSOCIATION_REQUEST_SSID_POSITION + 2)
#define AL_802_11_REASSOCIATION_REQUEST_SUPPORTEDRATES_POSITION(ssid_length)          (AL_802_11_REASSOCIATION_REQUEST_SSID_START_POSITION + ssid_length)
#define AL_802_11_REASSOCIATION_REQUEST_SUPPORTEDRATES_START_POSITION(ssid_length)    (AL_802_11_REASSOCIATION_REQUEST_SUPPORTEDRATES_POSITION(ssid_length) + 2)

/**
 * Only for 802.11g i.e (pure 802.11g and 802.11bg mode)
 */

#define AL_802_11_REASSOCIATION_REQUEST_EX_SUPPORTEDRATES_POSITION(ssid_length, supp_rates_length)          (AL_802_11_REASSOCIATION_REQUEST_SUPPORTEDRATES_POSITION(ssid_length) + 2 + supp_rates_length)
#define AL_802_11_REASSOCIATION_REQUEST_EX_SUPPORTEDRATES_START_POSITION(ssid_length, supp_rates_length)    (AL_802_11_REASSOCIATION_REQUEST_EX_SUPPORTEDRATES_POSITION(ssid_length, supp_rates_length) + 2)

/**
 * Frame field positions of various fields in an association response
 */
#if 0
#define AL_802_11_ASSOCIATION_RESPONSE_CAPABILITY_POSITION              0
#define AL_802_11_ASSOCIATION_RESPONSE_STATUS_POSITION                  (AL_802_11_ASSOCIATION_RESPONSE_CAPABILITY_POSITION + 2)
#define AL_802_11_ASSOCIATION_RESPONSE_AID_POSITION                     (AL_802_11_ASSOCIATION_RESPONSE_STATUS_POSITION + 2)
#define AL_802_11_ASSOCIATION_RESPONSE_SUPPORTEDRATES_POSITION          (AL_802_11_ASSOCIATION_RESPONSE_AID_POSITION + 2)
#define AL_802_11_ASSOCIATION_RESPONSE_SUPPORTEDRATES_START_POSITION    (AL_802_11_ASSOCIATION_RESPONSE_SUPPORTEDRATES_POSITION + 2)
#endif

#define AL_802_11_ASSOCIATION_RESPONSE_SUPPORTEDRATES_POSITION          0
#define AL_802_11_ASSOCIATION_RESPONSE_SUPPORTEDRATES_START_POSITION    (AL_802_11_ASSOCIATION_RESPONSE_SUPPORTEDRATES_POSITION + 2)

/**
 * Only for 802.11g i.e (pure 802.11g and 802.11bg mode)
 */

#define AL_802_11_ASSOCIATION_RESPONSE_EX_SUPPORTEDRATES_POSITION(supported_rates_length)          (AL_802_11_ASSOCIATION_RESPONSE_SUPPORTEDRATES_START_POSITION + supported_rates_length)
#define AL_802_11_ASSOCIATION_RESPONSE_EX_SUPPORTEDRATES_START_POSITION(supported_rates_length)    (AL_802_11_ASSOCIATION_RESPONSE_EX_SUPPORTEDRATES_POSITION(supported_rates_length) + 2)

/**
 * For 802.11a and pure 802.11b mode
 */

#define AL_802_11_ASSOCIATION_RESPONSE_PAYLOAD_LENGTH(supported_rates_length)    (AL_802_11_ASSOCIATION_RESPONSE_SUPPORTEDRATES_START_POSITION + supported_rates_length)

/**
 * Only for 802.11g i.e (pure 802.11g and 802.11bg mode)
 */

#define AL_802_11_ASSOCIATION_RESPONSE_EX_PAYLOAD_LENGTH(supported_rates_length, ex_supported_rates_length)    (AL_802_11_ASSOCIATION_RESPONSE_EX_SUPPORTEDRATES_START_POSITION(supported_rates_length) + ex_supported_rates_length)

/**
 * Frame field positions of various fields in a re-association response
 */

#define AL_802_11_REASSOCIATION_RESPONSE_CAPABILITY_POSITION              AL_802_11_ASSOCIATION_RESPONSE_CAPABILITY_POSITION
#define AL_802_11_REASSOCIATION_RESPONSE_STATUS_POSITION                  AL_802_11_ASSOCIATION_RESPONSE_STATUS_POSITION
#define AL_802_11_REASSOCIATION_RESPONSE_AID_POSITION                     AL_802_11_ASSOCIATION_RESPONSE_AID_POSITION
#define AL_802_11_REASSOCIATION_RESPONSE_SUPPORTEDRATES_POSITION          AL_802_11_ASSOCIATION_RESPONSE_SUPPORTEDRATES_POSITION
#define AL_802_11_REASSOCIATION_RESPONSE_SUPPORTEDRATES_START_POSITION    AL_802_11_ASSOCIATION_RESPONSE_SUPPORTEDRATES_START_POSITION

/**
 * Only for 802.11g i.e (pure 802.11g and 802.11bg mode)
 */

#define AL_802_11_REASSOCIATION_RESPONSE_EX_SUPPORTEDRATES_POSITION(supported_rates_length)          (AL_802_11_REASSOCIATION_RESPONSE_SUPPORTEDRATES_START_POSITION + supported_rates_length)
#define AL_802_11_REASSOCIATION_RESPONSE_EX_SUPPORTEDRATES_START_POSITION(supported_rates_length)    (AL_802_11_REASSOCIATION_RESPONSE_EX_SUPPORTEDRATES_POSITION(supported_rates_length) + 2)

/**
 * For 802.11a and pure 802.11b mode
 */

#define AL_802_11_REASSOCIATION_RESPONSE_PAYLOAD_LENGTH(supported_rates_length)    AL_802_11_ASSOCIATION_RESPONSE_PAYLOAD_LENGTH(supported_rates_length)

/**
 * Only for 802.11g i.e (pure 802.11g and 802.11bg mode)
 */

#define AL_802_11_REASSOCIATION_RESPONSE_EX_PAYLOAD_LENGTH(supported_rates_length, ex_supported_rates_length)    (AL_802_11_REASSOCIATION_RESPONSE_EX_SUPPORTEDRATES_START_POSITION(supported_rates_length) + ex_supported_rates_length)


/**
 * Frame field positions of various fields in a de-authentication notification
 */

#define AL_802_11_DEAUTHENTICATION_NOTIFICATION_REASON_CODE_POSITION    0

/**
 * Frame field positions of various fields in a disassociation notification
 */

#define AL_802_11_DISASSOCIATION_NOTIFICATION_REASON_CODE_POSITION    0

/**
 * Frame field positions of various fields in a Probe request
 */

#define AL_802_11_PROBE_REQUEST_SSID_POSITION          0
#define AL_802_11_PROBE_REQUEST_SSID_START_POSITION    (AL_802_11_PROBE_REQUEST_SSID_POSITION)
#define AL_802_11_PROBE_REQUEST_SUPPORTEDRATES_POSITION(ssid_length)          (AL_802_11_PROBE_REQUEST_SSID_START_POSITION + ssid_length)
#define AL_802_11_PROBE_REQUEST_SUPPORTEDRATES_START_POSITION(ssid_length)    (AL_802_11_PROBE_REQUEST_SUPPORTEDRATES_POSITION(ssid_length) + 2)

/**
 * Only for 802.11g i.e (pure 802.11g and 802.11bg mode)
 */

#define AL_802_11_PROBE_REQUEST_EX_SUPPORTEDRATES_POSITION(ssid_length, supported_rates_length)          (AL_802_11_PROBE_REQUEST_SUPPORTEDRATES_START_POSITION(ssid_length) + supported_rates_length)
#define AL_802_11_PROBE_REQUEST_EX_SUPPORTEDRATES_START_POSITION(ssid_length, supported_rates_length)    (AL_802_11_PROBE_REQUEST_EX_SUPPORTEDRATES_POSITION(ssid_length, supported_rates_length) + 2)



/**
 * Frame field positions of various fields in a Probe Response
 */

#define AL_802_11_PROBE_RESPONSE_TIMESTAMP              0
#define AL_802_11_PROBE_RESPONSE_BEACON_INTERVAL        (AL_802_11_PROBE_RESPONSE_TIMESTAMP + 8)
#define AL_802_11_PROBE_RESPONSE_CAPABILITY_POSITION    (AL_802_11_PROBE_RESPONSE_BEACON_INTERVAL + 2)
#define AL_802_11_PROBE_RESPONSE_SSID_POSITION          (AL_802_11_PROBE_RESPONSE_CAPABILITY_POSITION + 2)
#define AL_802_11_PROBE_RESPONSE_SSID_START_POSITION    (AL_802_11_PROBE_RESPONSE_SSID_POSITION + 2)
#define AL_802_11_PROBE_RESPONSE_SUPPORTEDRATES_POSITION(ssid_length)                            (AL_802_11_PROBE_RESPONSE_SSID_START_POSITION + ssid_length)
#define AL_802_11_PROBE_RESPONSE_SUPPORTEDRATES_START_POSITION(ssid_length)                      (AL_802_11_PROBE_RESPONSE_SUPPORTEDRATES_POSITION(ssid_length) + 2)
#define AL_802_11_PROBE_RESPONSE_DS_PARAM_POSITION(ssid_length, supported_rates_length)          (AL_802_11_PROBE_RESPONSE_SUPPORTEDRATES_START_POSITION(ssid_length) + supported_rates_length)
#define AL_802_11_PROBE_RESPONSE_DS_PARAM_START_POSITION(ssid_length, supported_rates_length)    (AL_802_11_PROBE_RESPONSE_DS_PARAM_POSITION(ssid_length, supported_rates_length) + 2)

/**
 * Only for 802.11g i.e (pure 802.11g and 802.11bg mode)
 */

#define AL_802_11_PROBE_RESPONSE_ERP_INFO_POSITION(ssid_length, supported_rates_length)                   (AL_802_11_PROBE_RESPONSE_DS_PARAM_START_POSITION(ssid_length, supported_rates_length) + 1)
#define AL_802_11_PROBE_RESPONSE_ERP_INFO_START_POSITION(ssid_length, supported_rates_length)             (AL_802_11_PROBE_RESPONSE_ERP_INFO_POSITION(ssid_length, supported_rates_length) + 2)
#define AL_802_11_PROBE_RESPONSE_EX_SUPPORTEDRATES_POSITION(ssid_length, supported_rates_length)          (AL_802_11_PROBE_RESPONSE_ERP_INFO_START_POSITION(ssid_length, supported_rates_length) + 1)
#define AL_802_11_PROBE_RESPONSE_EX_SUPPORTEDRATES_START_POSITION(ssid_length, supported_rates_length)    (AL_802_11_PROBE_RESPONSE_EX_SUPPORTEDRATES_POSITION(ssid_length, supported_rates_length) + 2)

/* Maxiumum value for AID */

#define AL_802_11_AID_MAX_VALUE    2007

/* Information Element IDs */

#define AL_802_11_EID_SSID                      0
#define AL_802_11_EID_SUPP_RATES                1
#define AL_802_11_EID_FH_PARAMS                 2
#define AL_802_11_EID_DS_PARAMS                 3
#define AL_802_11_EID_CF_PARAMS                 4
#define AL_802_11_EID_TIM                       5
#define AL_802_11_EID_IBSS_PARAMS               6
#define AL_802_11_EID_CHALLENGE                 16
#define AL_802_11_EID_ERP_INFO                  42
#define AL_802_11_EID_RSN                       48
#define AL_802_11_EID_EX_SUPP_RATES             50
#define AL_802_11_EID_VENDOR_PRIVATE            221
#define AL_802_11_EID_HT_CAPAB                  45
#define AL_802_11_EID_VHT_CAPAB                 226
#define AL_802_11_EID_HT_OPERATION              61

#define AL_802_11_EID_MAX_LENGTH                255


#define AL_802_11_DOT11E_CATEGORY_TYPE_AC_BK    0
#define AL_802_11_DOT11E_CATEGORY_TYPE_AC_BE    1
#define AL_802_11_DOT11E_CATEGORY_TYPE_AC_VI    2
#define AL_802_11_DOT11E_CATEGORY_TYPE_AC_VO    3

/* vijay : HT Cap flags   */
  /* HT Capabilities Info field within HT Capabilities element */
#define AL_HT_CAP_INFO_LDPC_CODING_CAP     ((u16) AL_BIT(0))
#define AL_HT_CAP_INFO_SUPP_CHANNEL_WIDTH_SET  ((u16) AL_BIT(1))
#define AL_HT_CAP_INFO_SMPS_MASK           ((u16) (AL_BIT(2) | AL_BIT(3)))
#define AL_HT_CAP_INFO_SMPS_STATIC         ((u16) 0)
#define AL_HT_CAP_INFO_SMPS_DYNAMIC        ((u16) AL_BIT(2))
#define AL_HT_CAP_INFO_SMPS_DISABLED       ((u16) (AL_BIT(2) | AL_BIT(3)))
#define AL_HT_CAP_INFO_GREEN_FIELD         ((u16) AL_BIT(4))
#define AL_HT_CAP_INFO_SHORT_GI20MHZ       ((u16) AL_BIT(5))
#define AL_HT_CAP_INFO_SHORT_GI40MHZ       ((u16) AL_BIT(6))
#define AL_HT_CAP_INFO_TX_STBC         ((u16) AL_BIT(7))
#define AL_HT_CAP_INFO_RX_STBC_MASK        ((u16) (AL_BIT(8) | AL_BIT(9)))
#define AL_HT_CAP_INFO_RX_STBC_1           ((u16) AL_BIT(8))
#define AL_HT_CAP_INFO_RX_STBC_12          ((u16) AL_BIT(9))
#define AL_HT_CAP_INFO_RX_STBC_123         ((u16) (AL_BIT(8) | AL_BIT(9)))
#define AL_HT_CAP_INFO_DELAYED_BA          ((u16) AL_BIT(10))
#define AL_HT_CAP_INFO_MAX_AMSDU_SIZE      ((u16) AL_BIT(11))
#define AL_HT_CAP_INFO_DSSS_CCK40MHZ       ((u16) AL_BIT(12))
/* B13 - Reserved (was PSMP support during P802.11n development) */
#define AL_HT_CAP_INFO_40MHZ_INTOLERANT        ((u16) AL_BIT(14))
#define AL_HT_CAP_INFO_LSIG_TXOP_PROTECT_SUPPORT   ((u16) AL_BIT(15))


// HT opeartio ralated MACROS
/* First octet of HT Operation Information within HT Operation element */
#define AL_HT_INFO_HT_PARAM_SECONDARY_CHNL_OFF_MASK    ((u8) BIT(0) | BIT(1))
#define AL_HT_INFO_HT_PARAM_SECONDARY_CHNL_ABOVE       ((u8) BIT(0))
#define AL_HT_INFO_HT_PARAM_SECONDARY_CHNL_BELOW       ((u8) BIT(0) | BIT(1))
#define AL_HT_INFO_HT_PARAM_STA_CHNL_WIDTH         ((u8) BIT(2))
#define AL_HT_INFO_HT_PARAM_RIFS_MODE          ((u8) BIT(3))
/* B4..B7 - Reserved */

/* HT Protection (B8..B9 of HT Operation Information) */
#define AL_HT_PROT_NO_PROTECTION           0
#define AL_HT_PROT_NONMEMBER_PROTECTION    1
#define AL_HT_PROT_20MHZ_PROTECTION        2
#define AL_HT_PROT_NON_HT_MIXED            3
/* Bits within ieee80211_ht_operation::operation_mode (BIT(0) maps to B8 in
 * HT Operation Information) */
#define AL_HT_OPER_OP_MODE_HT_PROT_MASK ((u16) (BIT(0) | BIT(1))) /* B8..B9 */
#define AL_HT_OPER_OP_MODE_NON_GF_HT_STAS_PRESENT  ((u16) BIT(2)) /* B10 */
/* BIT(3), i.e., B11 in HT Operation Information field - Reserved */
#define AL_HT_OPER_OP_MODE_OBSS_NON_HT_STAS_PRESENT    ((u16) BIT(4)) /* B12 */
/* BIT(5)..BIT(15), i.e., B13..B23 - Reserved */

/* Last two octets of HT Operation Information (BIT(0) = B24) */
/* B24..B29 - Reserved */
#define AL_HT_OPER_PARAM_DUAL_BEACON           ((u16) BIT(6))
#define AL_HT_OPER_PARAM_DUAL_CTS_PROTECTION       ((u16) BIT(7))
#define AL_HT_OPER_PARAM_STBC_BEACON           ((u16) BIT(8))
#define AL_HT_OPER_PARAM_LSIG_TXOP_PROT_FULL_SUPP      ((u16) BIT(9))
#define AL_HT_OPER_PARAM_PCO_ACTIVE            ((u16) BIT(10))
#define AL_HT_OPER_PARAM_PCO_PHASE             ((u16) BIT(11))
/* B36..B39 - Reserved */







struct al_802_11_information_element
{
   unsigned char element_id;
   unsigned char length;
   unsigned char data[AL_802_11_EID_MAX_LENGTH + 1];
};

typedef struct al_802_11_information_element   al_802_11_information_element_t;

struct al_802_11_rsn_cipher
{
   unsigned char oui[3];
   unsigned char type;
};

typedef struct al_802_11_rsn_cipher   al_802_11_rsn_cipher_t;

struct al_802_11_akm
{
   unsigned char oui[3];
   unsigned char type;
};

typedef struct al_802_11_akm   al_802_11_akm_t;

#define AL_802_11_PMKID_LENGTH    16

struct al_802_11_pmkid
{
   unsigned char bytes[AL_802_11_PMKID_LENGTH];
};

typedef struct al_802_11_pmkid   al_802_11_pmkid_t;

struct al_802_11_rsn_ie
{
   /** Processed Information */
   unsigned char          mode;                                         /** AL_802_11_SECURITY_INFO_RSN_MODE_* */
   unsigned char          auth_modes;                                   /** AL_802_11_SECURITY_INFO_RSN_AUTH_* */
   unsigned char          cipher_modes;                                 /** AL_802_11_SECURITY_INFO_RSN_CIPHER_* except AL_802_11_SECURITY_INFO_RSN_CIPHER_WEP */
   unsigned char          group_cipher_mode;                            /** AL_802_11_SECURITY_INFO_RSN_CIPHER_* */
   /** Information as appearing in Packet */
   unsigned short         version;
   al_802_11_rsn_cipher_t group_suite;
   unsigned short         pairwise_count;
   al_802_11_rsn_cipher_t *pairwise_suites;
   unsigned short         akm_count;
   al_802_11_akm_t        *akm_suites;
   unsigned short         rsn_capabilities;
   unsigned short         pmkid_count;
   al_802_11_pmkid_t      *pmkid_list;
};

typedef struct al_802_11_rsn_ie   al_802_11_rsn_ie_t;

#define AL_802_11_RSN_CAPABILITY_PRE_AUTH          AL_BIT(0)
#define AL_802_11_RSN_CAPABILITY_NO_PAIRWISE       AL_BIT(1)
#define AL_802_11_RSN_CAPABILITY_PTSKA_REPLAY      (AL_BIT(2) | AL_BIT(3))
#define AL_802_11_RSN_CAPABILITY_GTSKA_REPLAY      (AL_BIT(4) | AL_BIT(5))

#define AL_802_11_RSN_CIPHER_GROUP                 0
#define AL_802_11_RSN_CIPHER_WEP_40                1
#define AL_802_11_RSN_CIPHER_TKIP                  2
#define AL_802_11_RSN_CIPHER_CCMP                  4
#define AL_802_11_RSN_CIPHER_WEP_104               5

#define AL_802_11_RSN_AUTH_1X                      1
#define AL_802_11_RSN_PSK                          2

#define AL_802_11_MD_IE_MAX_GROUP_KEY_LENGTH       52
#define AL_802_11_MD_IE_MAX_PAIRWISE_KEY_LENGTH    16
#define AL_802_11_MD_IE_CRYPT_HEADER_LENGTH        8

#define AL_802_11_MD_IE_TOTAL_GROUP_KEY_LENGTH \
   (AL_802_11_MD_IE_MAX_GROUP_KEY_LENGTH       \
    + AL_802_11_MD_IE_CRYPT_HEADER_LENGTH      \
    + 4 /* AES block size padding */)

#define AL_802_11_MD_IE_FIPS_TOTAL_GROUP_KEY_LENGTH                                              \
   (AL_802_11_MD_IE_MAX_GROUP_KEY_LENGTH                                                         \
    + AL_802_11_MD_IE_CRYPT_HEADER_LENGTH                                                        \
    + 4                                                             /* AES block size padding */ \
    + 8 /* aes wrap extra length for fips mode */)

#define AL_802_11_MD_IE_TOTAL_PAIRWISE_KEY_LENGTH \
   (AL_802_11_MD_IE_MAX_PAIRWISE_KEY_LENGTH       \
    + AL_802_11_MD_IE_CRYPT_HEADER_LENGTH         \
    + 8 /* AES block size padding */)

#define AL_802_11_MD_IE_FIPS_TOTAL_PAIRWISE_KEY_LENGTH \
   (AL_802_11_MD_IE_MAX_PAIRWISE_KEY_LENGTH            \
    + AL_802_11_MD_IE_CRYPT_HEADER_LENGTH              \
    + 8 /* aes wrap extra length for fips mode */)

#define AL_802_11_MD_IE_COMPRESSION_SUPPORT    0x01
#define AL_802_11_MD_IE_EFFISTREAM_SUPPORT     0x02

struct al_802_11_md_ie
{
   unsigned char version;
   unsigned char type;
   union
   {
      struct
      {
         unsigned char assoc_type;
         /*unsigned char	compression_support;*/	/* Version 1 onwards */

         /**
          *	Above Field is used for EffiStreaming and compression_support,
          *	renamed as md_ie_capability from Version 2
          */
         unsigned char md_ie_capability;                        /* Version 2 onwards */
      }
      assoc_request;
      struct
      {
         unsigned char group_cipher_type;
         unsigned char group_key_index;
         unsigned char group_key[AL_802_11_MD_IE_FIPS_TOTAL_GROUP_KEY_LENGTH];
         unsigned char pairwise_key[AL_802_11_MD_IE_FIPS_TOTAL_PAIRWISE_KEY_LENGTH];

         /*unsigned char	compression_support;*/	/* Version 1 onwards */

         /**
          *	Above Field is used for EffiStreaming and compression_support,
          *	renamed as md_ie_capability from Version 2
          */
         unsigned char md_ie_capability;                        /* Version 2 onwards */
      }
      assoc_response;
      struct
      {
         unsigned char length;
         unsigned char contents[AL_802_11_EID_MAX_LENGTH + 1];
      }
      imcp_packet;
   }
                 data;
};

typedef struct al_802_11_md_ie   al_802_11_md_ie_t;

#define AL_802_11_MD_IE_VERSION_0                     0
#define AL_802_11_MD_IE_VERSION_1                     1
#define AL_802_11_MD_IE_VERSION_2                     2
#define AL_802_11_MD_IE_VERSION                       AL_802_11_MD_IE_VERSION_2
#define AL_802_11_MD_IE_VERSION_COMPRESSION           AL_802_11_MD_IE_VERSION_1
#define AL_802_11_MD_IE_VERSION_EFFISTREAM            AL_802_11_MD_IE_VERSION_2

#define AL_802_11_MD_IE_TYPE_ASSOC_REQUEST            1
#define AL_802_11_MD_IE_TYPE_ASSOC_RESPONSE           2
#define AL_802_11_MD_IE_TYPE_IMCP                     3
#define AL_802_11_MD_IE_TYPE_FIPS_ASSOC_REQUEST       4
#define AL_802_11_MD_IE_TYPE_FIPS_ASSOC_RESPONSE      5

#define AL_802_11_MD_IE_ASSOCIATION_TYPE_NORMAL       0
#define AL_802_11_MD_IE_ASSOCIATION_TYPE_HANDSHAKE    1
#define AL_802_11_MD_IE_ASSOCIATION_TYPE_FINAL        2

/**
 * Minimum sizes of various 802.11 frames
 */

#define AL_802_11_DISASSOC_MIN_SIZE          2
#define AL_802_11_ASSOC_RESP_MIN_SIZE        16
#define AL_802_11_REASSOC_RESP_MIN_SIZE      16
#define AL_802_11_PROBE_RESP_MIN_SIZE        27
#define AL_802_11_AUTHENTICATION_MIN_SIZE    6
#define AL_802_11_DEAUTH_MIN_SIZE            2

struct al_802_11_tx_power_info
{
   int flags;
   int power;
};

typedef struct al_802_11_tx_power_info   al_802_11_tx_power_info_t;

#define AL_802_11_SECURITY_INFO_STRENGTH_CCMP         16
#define AL_802_11_SECURITY_INFO_STRENGTH_40           40
#define AL_802_11_SECURITY_INFO_STRENGTH_104          104

#define AL_802_11_SECURITY_INFO_RSN_MODE_WPA          0
#define AL_802_11_SECURITY_INFO_RSN_MODE_11I          1
#define AL_802_11_SECURITY_INFO_RSN_MODE_BOTH         2

#define AL_802_11_SECURITY_INFO_RSN_AUTH_NONE         1
#define AL_802_11_SECURITY_INFO_RSN_AUTH_PSK          2
#define AL_802_11_SECURITY_INFO_RSN_AUTH_1X           4
#define AL_802_11_SECURITY_INFO_RSN_AUTH_IMCP         8

#define AL_802_11_SECURITY_INFO_RSN_CIPHER_NONE       1
#define AL_802_11_SECURITY_INFO_RSN_CIPHER_WEP        2
#define AL_802_11_SECURITY_INFO_RSN_CIPHER_TKIP       4
#define AL_802_11_SECURITY_INFO_RSN_CIPHER_CCMP       8
#define AL_802_11_SECURITY_INFO_RSN_CIPHER_IMCP       16
#define AL_802_11_SECURITY_INFO_RSN_CIPHER_GROUP      32
#define AL_802_11_SECURITY_INFO_RSN_CIPHER_WEP_104    64

#define AL_802_11_SECURITY_INFO_RSN_MAX_PSK_LENGTH    32

struct  al_802_11_security_info
{
   struct __none_info
   {
      unsigned char enabled;
   }
   none;
   struct __wep_info
   {
      unsigned char enabled;
      unsigned char strength;
      unsigned char key_index;
      unsigned char keys[AL_802_11_MAX_SECURITY_KEYS][AL_802_11_MAX_KEY_LENGTH];
   }
   wep;
   struct __rsn
   {
      unsigned char enabled;
      unsigned char mode;                                               /** AL_802_11_SECURITY_INFO_RSN_MODE_* */
      unsigned char auth_modes;                                         /** AL_802_11_SECURITY_INFO_RSN_AUTH_* */
      unsigned char cipher_modes;                                       /** AL_802_11_SECURITY_INFO_RSN_CIPHER_* except AL_802_11_SECURITY_INFO_RSN_CIPHER_WEP */
      unsigned char psk[AL_802_11_SECURITY_INFO_RSN_MAX_PSK_LENGTH];    /** The pre-shared key */
   }
   rsn;
};

typedef struct  al_802_11_security_info   al_802_11_security_info_t;

struct al_802_11_security_key_info
{
   unsigned char  enabled;
   al_net_addr_t  destination;
   unsigned short vlan_tag;
   unsigned char  cipher_type;          /* AL_802_11_SECURITY_INFO_RSN_CIPHER_* */
   unsigned char  key_index;            /* Only valid for AL_802_11_SECURITY_INFO_RSN_CIPHER_WEP */
   unsigned char  strength;             /* Only valid for AL_802_11_SECURITY_INFO_RSN_CIPHER_WEP */

   /**
    * For WEP this contains all the 4 keys concatenated together
    * For CCMP the first 16 bytes are valid
    * and for TKIP the first 32 bytes are valid
    */
   unsigned char  key[AL_802_11_MAX_KEY_LENGTH * AL_802_11_MAX_SECURITY_KEYS];
};

typedef struct al_802_11_security_key_info   al_802_11_security_key_info_t;

struct al_802_11_security_key_data
{
   int          cipher_type;                    /** AL_802_11_RSN_CIPHER_* */
   unsigned int wep_iv;                         /** The WEP Initialization Vector */
   al_u64_t     key_rsc;                        /** The TKIP/CCMP Receive Sequence Counter */
   al_u64_t     key_tsc;                        /** The TKIP/CCMP Transmit Sequence Counter */
};

typedef struct al_802_11_security_key_data   al_802_11_security_key_data_t;

struct al_802_11_essid_info
{
   al_802_11_security_info_t sec_info;
   unsigned short            vlan_tag;
   char                      essid[AL_802_11_ESSID_MAX_SIZE + 1];
};

typedef struct al_802_11_essid_info   al_802_11_essid_info_t;

#if MDE_80211N_SUPPORT //sowndarya Added
struct al_802_11_fram_agre
{
        int             ampdu_enable;
        int             max_ampdu_len;
};
typedef struct al_802_11_fram_agre al_802_11_fram_agre_t;

struct al_802_11_ht_capab
{
        unsigned char                           ldpc;
        int                                     ht_bandwidth;
	unsigned char				gfmode;
        unsigned char                           smps;
        unsigned char                           gi_20;
        unsigned char                           gi_40;
        unsigned char                           tx_stbc;
        unsigned char                           rx_stbc;
        unsigned char                           delayed_ba;
        int                                     max_amsdu_len;
        unsigned char                           dsss_cck_40;
        unsigned char                           intolerant;
        unsigned char                           lsig_txop;

};
typedef struct al_802_11_ht_capab   al_802_11_ht_capab_t;

#endif

#if MDE_80211AC_SUPPORT     //sowndarya Added

struct al_802_11_vht_capab
{
        int                     max_mpdu_len;
        int                     supported_channel_width;
        unsigned char           rx_ldpc;
        unsigned char           gi_80;
        unsigned char           gi_160;
        unsigned char           tx_stbc;
        unsigned char           rx_stbc;
        unsigned char           su_beamformer_cap;
        unsigned char           su_beamformee_cap;
        int                     beamformee_sts_count;
        int                     sounding_dimensions;
        unsigned char           mu_beamformer_cap;
        unsigned char           mu_beamformee_cap;
        unsigned char           vht_txop_ps;
        unsigned char           htc_vht_cap;
        unsigned char           rx_ant_pattern_consistency;
        unsigned char           tx_ant_pattern_consistency;
        int                     vht_oper_bandwidth;
        int                     seg0_center_freq;
        int                     seg1_center_freq;
};
typedef struct al_802_11_vht_capab al_802_11_vht_capab_t;


#endif
//vijay : structure to store the assoc Request frame  HT & VHT related information 

/* HT Capabilities element */
struct al_802_11_ht_capabilities {
    unsigned short int  ht_capabilities_info;
    unsigned char  a_mpdu_params; /* Maximum A-MPDU Length Exponent B0..B1
               * Minimum MPDU Start Spacing B2..B4
               * Reserved B5..B7 */
    unsigned char  supported_mcs_set[16];
    unsigned short int  ht_extended_capabilities;
    unsigned int  tx_bf_capability_info;
    unsigned char asel_capabilities;
}
__attribute__((packed));

typedef struct al_802_11_ht_capabilities al_802_11_ht_capabilities_t;

struct al_802_11_vht_capabilities {
    unsigned int  vht_capabilities_info;
    struct {
        unsigned short int  rx_map;
        unsigned short int  rx_highest;
        unsigned short int  tx_map;
        unsigned short int  tx_highest;
    } vht_supported_mcs_set;
};

typedef struct al_802_11_vht_capabilities  al_802_11_vht_capabilities_t;

/* HT Operation element */
struct al_802_11_ht_operation {
    unsigned char primary_chan;
    /* Five octets of HT Operation Information */
    unsigned char  ht_param; /* B0..B7 */
    unsigned short int  operation_mode; /* B8..B23 */
    unsigned short int param; /* B24..B39 */
    unsigned char  basic_mcs_set[16];
}
__attribute__((packed));

typedef struct al_802_11_ht_operation  al_802_11_ht_operation_t;

struct al_802_11_vht_operation {
    unsigned char  vht_op_info_chwidth;
    unsigned char  vht_op_info_chan_center_freq_seg0_idx;
    unsigned char  vht_op_info_chan_center_freq_seg1_idx;
    unsigned short int  vht_basic_mcs_set;
};

typedef struct al_802_11_vht_operation al_802_11_vht_operation_t ;

#define AL_802_11_I_KDE_TYPE_RESERVED       0
#define AL_802_11_I_KDE_TYPE_GTK            1
#define AL_802_11_I_KDE_TYPE_STAKEY         2
#define AL_802_11_I_KDE_TYPE_MACADDR        3
#define AL_802_11_I_KDE_TYPE_PMKID          4

#define AL_802_11_PHY_MODE_A                0
#define AL_802_11_PHY_MODE_B                1
#define AL_802_11_PHY_MODE_G                2
#define AL_802_11_PHY_MODE_BG               3
#define AL_802_11_PHY_MODE_ATHEROS_TURBO    4                   /* Proprietary Atheros turbo mode (double bonding) */
#define AL_802_11_PHY_MODE_PSQ              5                   /* 5 MHz 4.9GHz US Public safety */
#define AL_802_11_PHY_MODE_PSH              6                   /* 10 MHz 4.9GHz US Public safety */
#define AL_802_11_PHY_MODE_PSF              7                   /* 20 MHz 4.9GHz US Public safety */
// VIJAY : added for set_phy_mode function for N & AC support
#define AL_802_11_PHY_MODE_N                10
#define AL_802_11_PHY_MODE_AC               11
#define AL_802_11_PHY_MODE_BGN              12
#define AL_802_11_PHY_MODE_AN               13
#define AL_802_11_PHY_MODE_ANAC             14
struct al_scan_access_point_info
{
   int            mode;
   al_net_addr_t  bssid;
   int            phy_mode;
   int            channel;
   char           essid[AL_802_11_ESSID_MAX_SIZE + 1];
   unsigned char  signal;
   unsigned char  noise;
   unsigned short beacon_interval;
   unsigned short capabilities;
   unsigned char  supported_rates[AL_802_11_MAX_BIT_RATES];
   unsigned char  probe_response_rate;
   unsigned char  vendor_info_length;
   unsigned char  vendor_info[AL_802_11_VENDOR_INFO_MAX_SIZE];
#if 1// VIJAY_STA_HT :
   al_802_11_ht_capabilities_t  ap_ht_capab ;
#endif
};

typedef struct al_scan_access_point_info   al_scan_access_point_info_t;

/**
 * F/W 2.5.26 onwards :
 * Regulatory domain code field is 16 bits and is formatted
 * as follows :
 *
 *    15   14   13   12   11   10   9    8  | 7    6    5    4    3    2     1   0
 *  +----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+
 *  |            RESERVED              |DFS |           REG DOMAIN CODE             |
 *  +----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+
 *                                          |
 */

#define AL_REG_DOMAIN_NONE                   0
#define AL_REG_DOMAIN_FCC                    1
#define AL_REG_DOMAIN_ETSI                   2
#define AL_REG_DOMAIN_CUSTOM                 3

#define AL_REG_DOMAIN_COUNTRY_CODE_CUSTOM    1

#define AL_REG_DOMAIN_CTL_NONE               0
#define AL_REG_DOMAIN_CTL_FCC                1
#define AL_REG_DOMAIN_CTL_ETSI               2
#define AL_REG_DOMAIN_CTL_MKK                3

#define AL_REG_DOMAIN_NEEDS_DFS(rdc)          (rdc & 0x0100)
#define AL_REG_DOMAIN_CODE_GET_DOMAIN(rdc)    (rdc & 0x00FF)

struct al_reg_domain_info
{
   unsigned short country_code;         /* AL_REG_DOMAIN_COUNTRY_CODE_CUSTOM or a valid code */
   unsigned short reg_domain;           /* AL_REG_DOMAIN_* */
   /** The following fields are only valid for Custom Country code (AL_REG_DOMAIN_COUNTRY_CODE_CUSTOM) */
   unsigned char  bandwidth;
   unsigned char  max_power;
   unsigned char  channel_count;
   unsigned short *frequencies;
   unsigned char  *channel_numbers;
   unsigned char  *channel_flags;       /*DFS*/
   unsigned char  antenna_max;
   unsigned char  conformance_test_limit;
};

typedef struct al_reg_domain_info   al_reg_domain_info_t;

#define AL_CHANNEL_STATE_RADAR           0x01
#define AL_CHANNEL_STATE_DFS_REQUIRED    0x02
#define AL_CHANNEL_STATE_4MS_LIMIT       0x04
#define AL_CHANNEL_STATE_DFS_CLEAR       0x08

struct al_channel_info
{
   unsigned char number;
   unsigned int  frequency;                     /** In MHz */
   unsigned int  flags;
   unsigned int  state_flags;
   char          max_reg_power;
   char          max_power;
   char          min_power;
};

typedef struct al_channel_info   al_channel_info_t;


/** 802.11e Categories
 *
 *	AC_BK    = 0		(background)
 *	AC_BE	= 1		(best_effort)
 *	AC_VI	= 2		(video)
 *	AC_VO	= 3		(voice)
 */

struct al_dot11e_category_details
{
   unsigned char  category;
   unsigned short acwmin;
   unsigned short acwmax;
   unsigned char  aifsn;
   unsigned char  disable_backoff;
   unsigned int   burst_time;
};

typedef struct al_dot11e_category_details   al_dot11e_category_details_t;


struct al_dot11e_category_info
{
   unsigned char                count;
   al_dot11e_category_details_t *category_info;                 /* 802.11e Category Info */
};

typedef struct al_dot11e_category_info   al_dot11e_category_info_t;

struct al_duty_cycle_info
{
   int          channel;
   int          number_of_aps;                          /* Count of All APs found */
   struct
   {
      char          essid[AL_802_11_ESSID_MAX_SIZE + 1];
      al_net_addr_t bssid;
      unsigned char signal;                             /* Expressed as units above -96 dBm */
      unsigned int  transmit_duration;                  /* Transmit air time in Micro-seconds */
      unsigned int  retry_duration;                     /* Total retry air time in Micro-seconds */
   }
                *aps;
   int          max_signal;                             /* Max power expressed as units above -96 dBm */
   int          avg_signal;                             /* Avg power expressed as units above -96 dBm */
   unsigned int total_duration;                         /* Total dwell time in Micro-seconds */
   unsigned int transmit_duration;                      /* Total transmit air time in Micro-seconds */
   unsigned int retry_duration;                         /* Total retry air time in Micro-seconds */
   void         *priv_data;                             /* Private data used by caller */
};

typedef struct al_duty_cycle_info   al_duty_cycle_info_t;

struct al_rate_ctrl_param
{
   int t_el_max;                                /* Number of seconds */
   int t_el_min;                                /* Number of seconds */
   int qualification;                           /* Number of packets */
   int max_retry_percent;
   int max_error_percent;
   int max_errors;                              /* Count */
   int min_retry_percent;
   int min_qualification;                       /* Number of packets */
   int initial_rate_index;                      /* Rate Index from rate table */
};

typedef struct al_rate_ctrl_param   al_rate_ctrl_param_t;

#define AL_802_11_RATE_TABLE_MAX_COUNT    24

struct  al_rate_table
{
   int count;
   struct
   {
      int index;
      int rate_in_mbps;
   }
       rates[AL_802_11_RATE_TABLE_MAX_COUNT];
};

typedef struct  al_rate_table   al_rate_table_t;

#define AL_802_11_RATE_CTRL_OUTLOOK_POSSITIVE    1
#define AL_802_11_RATE_CTRL_OUTLOOK_NEGATIVE     -1

struct al_rate_ctrl_info
{
   int           t_elapsed;
   int           t_ch;
   int           rate_index;
   int           rate_in_mbps;
   int           tx_success;
   int           tx_error;
   int           tx_retry;
   int           confidence;
   int           outlook;
   unsigned int  last_check;
   unsigned char avg_ack_rssi;
};

typedef struct al_rate_ctrl_info   al_rate_ctrl_info_t;

typedef int (*al_process_management_frame_t)    (AL_CONTEXT_PARAM_DECL al_net_if_t *al_net_if, int sub_type, al_packet_t *packet);
typedef int (*al_process_beacon_t)                              (AL_CONTEXT_PARAM_DECL al_net_if_t *al_net_if, al_net_addr_t *bssid, unsigned char signal, unsigned char noise);
typedef int (*al_process_packet_error_t)                (AL_CONTEXT_PARAM_DECL al_net_if_t *al_net_if, al_net_addr_t *src, al_net_addr_t *dst, int type);
typedef int (*al_process_auth_t)                                (AL_CONTEXT_PARAM_DECL al_net_if_t *al_net_if, al_net_addr_t *sta_addr);

typedef int (*al_process_assoc_t)                               (AL_CONTEXT_PARAM_DECL al_net_if_t *al_net_if,
                                                                 al_net_addr_t                     *sta_addr,
                                                                 unsigned short                    capability,
                                                                 int                               ie_count,
                                                                 al_802_11_information_element_t   *ie,
                                                                 unsigned char                     *imcp_key_ie_valid_out,
                                                                 al_802_11_information_element_t   *imcp_key_ie_out);

typedef void (*al_round_robin_notify_t)                 (AL_CONTEXT_PARAM_DECL al_net_if_t *al_net_if);

typedef void (*al_radar_notify_t)                               (AL_CONTEXT_PARAM_DECL al_net_if_t *al_net_if, int channel);
typedef void (*al_on_probe_request_t)   (AL_CONTEXT_PARAM_DECL al_net_if_t *al_net_if, al_net_addr_t *sta_addr, int rssi);
typedef void (*al_action_notify_t)                              (AL_CONTEXT_PARAM_DECL al_net_if_t *al_net_if, al_net_addr_t *sender, unsigned char *data, int length);
typedef int (*al_send_assoc_resp_t)                             (AL_CONTEXT_PARAM_DECL al_net_if_t *al_net_if,
                                                                 unsigned short stype,
                                                                 unsigned char *address0, unsigned char *address1,
                                                                 unsigned short aid,
                                                                 unsigned char ie_out_valid,
                                                                 al_802_11_information_element_t *ie_out, struct meshap_mac80211_sta_info sta_info);

typedef int (*al_send_deauth_t)                              (AL_CONTEXT_PARAM_DECL al_net_if_t *al_net_if, unsigned char *address0, unsigned char *address1);
typedef int (*al_del_station_t)                  (AL_CONTEXT_PARAM_DECL al_net_if_t *al_net_if, unsigned char *station_addr);
typedef int (*al_send_auth_response_t)                   (AL_CONTEXT_PARAM_DECL al_net_if_t *al_net_if, unsigned char *address0, unsigned char *address1);

#define AL_802_11_PREAMBLE_TYPE_LONG               0
#define AL_802_11_PREAMBLE_TYPE_SHORT              1

#define AL_802_11_SLOT_TIME_TYPE_LONG              0
#define AL_802_11_SLOT_TIME_TYPE_SHORT             1

#define AL_802_11_ERP_INFO_NON_ERP_PRESENT         1
#define AL_802_11_ERP_INFO_USE_PROTECTION          2
#define AL_802_11_ERP_INFO_BARKER_PREAMBLE_MODE    4

struct al_802_11_operations
{
   int      (*associate)    (al_net_if_t *al_net_if, al_net_addr_t *ap_addr, int channel, const char *essid, int length, int timeout, al_802_11_md_ie_t *md_ie_in, al_802_11_md_ie_t *md_ie_out);
   int      (*dis_associate) (al_net_if_t *al_net_if);
   int      (*get_bssid)                                                            (al_net_if_t *al_net_if, al_net_addr_t *bssid);
   int      (*send_management_frame)                                        (al_net_if_t *al_net_if, int sub_type, al_packet_t *packet);
   int      (*scan_access_points)                                           (al_net_if_t *al_net_if, al_scan_access_point_info_t **access_points, int *count, int scan_duration_in_millis, int channel_count, unsigned char *channels);
   int      (*scan_access_points_active)                            (al_net_if_t *al_net_if, al_scan_access_point_info_t **access_points, int *count, int scan_duration_in_millis, int channel_count, unsigned char *channels);
   int      (*scan_access_points_passive)                           (al_net_if_t *al_net_if, al_scan_access_point_info_t **access_points, int *count, int scan_duration_in_millis, int channel_count, unsigned char *channels);
   int      (*set_management_frame_hook)                            (al_net_if_t *al_net_if, al_process_management_frame_t hook);
   int      (*set_beacon_hook)                                                      (al_net_if_t *al_net_if, al_process_beacon_t hook);
   int      (*set_error_hook)                                                       (al_net_if_t *al_net_if, al_process_packet_error_t hook);
   al_u64_t (*get_last_beacon_time)                                         (al_net_if_t *al_net_if);
   void     (*set_mesh_downlink_round_robin_time)           (al_net_if_t *al_net_if, int beacon_intervals);
   void     (*add_downlink_round_robin_child)                       (al_net_if_t *al_net_if, al_net_addr_t *addr);
   void     (*remove_downlink_round_robin_child)            (al_net_if_t *al_net_if, al_net_addr_t *addr);
   int      (*virtual_associate)                                            (al_net_if_t *al_net_if, al_net_addr_t *ap_addr, int channel, int start);
   int      (*get_duty_cycle_info)                                          (al_net_if_t *al_net_if, int channel_count, al_duty_cycle_info_t *info, unsigned int dwell_time_minis);
   void     (*set_rate_ctrl_parameters)                                     (al_net_if_t *al_net_if, al_rate_ctrl_param_t *rate_ctrl_param);
   void     (*reset_rate_ctrl)                                                      (al_net_if_t *al_net_if, al_net_addr_t *addr);
   void     (*get_rate_ctrl_info)                                           (al_net_if_t *al_net_if, al_net_addr_t *addr, al_rate_ctrl_info_t *rate_ctrl_info);
   void     (*set_round_robin_notify_hook)                          (al_net_if_t *al_net_if, al_round_robin_notify_t hook);
   void     (*enable_ds_verification_opertions)                     (al_net_if_t *al_net_if, int enable);
   void     (*set_probe_request_hook)                                       (al_net_if_t *al_net_if, al_on_probe_request_t on_probe_request);

   /** DFS operations for ETSI/FCC3/etc */

   int      (*dfs_scan)                                                                     (al_net_if_t *al_net_if);
   void     (*set_radar_notify_hook)                                        (al_net_if_t *al_net_if, al_radar_notify_t hook);

   /** MAC OPERATIONS */
   int      (*get_mode)                                                                     (al_net_if_t *al_net_if);
   int      (*set_mode)                                                                     (al_net_if_t *al_net_if, int mode, unsigned char master_quiet);
   int      (*get_essid)                                                            (al_net_if_t *al_net_if, char *essid_buffer, int length);
   int      (*set_essid)                                                            (al_net_if_t *al_net_if, char *essid_buffer);
   //VIJAY : AL layer HT & VHT Functionality
   int      (*set_ht_vht_capab)                                            (al_net_if_t *al_net_if, al_net_if_ht_vht_capab_t *ht_vht_capab);
   int      (*set_ht_operation)                                            (al_net_if_t *al_net_if, unsigned short int *ht_oper_mode);
   int      (*set_vht_operation)                                            (al_net_if_t *al_net_if, al_802_11_vht_operation_t* vht_oper);
   int      (*get_rts_threshold)                                            (al_net_if_t *al_net_if);
   int      (*set_rts_threshold)                                            (al_net_if_t *al_net_if, int threshold);
   int      (*get_frag_threshold)                                           (al_net_if_t *al_net_if);
   int      (*set_frag_threshold)                                           (al_net_if_t *al_net_if, int threshold);
   int      (*get_beacon_interval)                                          (al_net_if_t *al_net_if);
   int      (*set_beacon_interval)                                          (al_net_if_t *al_net_if, int interval);
   int      (*set_security_info)                                            (al_net_if_t *al_net_if, al_802_11_security_info_t *security_info);
   int      (*set_security_key)                                                     (al_net_if_t *al_net_if, al_802_11_security_key_info_t *security_key_info, int enable_compression);
   int      (*release_security_key)                                         (al_net_if_t *al_net_if, int index, const char *mac_addr);
   int      (*set_ds_security_key)                                          (al_net_if_t *al_net_if, unsigned char *group_key, unsigned char group_key_type, unsigned char group_key_index, unsigned char *pairwise_key, int enable_compression);
   int      (*get_security_key_data)                                        (al_net_if_t *
                                                                             al_net_if, int index, al_802_11_security_key_data_t *security_key_data,
                                                                             const char *mac_addr);

   int      (*get_default_capabilities)                                     (al_net_if_t *al_net_if);
   int      (*get_capabilities)                                                     (al_net_if_t *al_net_if);
   int      (*get_slot_time_type)                                           (al_net_if_t *al_net_if);
   int      (*set_slot_time_type)                                           (al_net_if_t *al_net_if, int slot_time_type);
   int      (*get_erp_info)                                                         (al_net_if_t *al_net_if);
   int      (*set_erp_info)                                                         (al_net_if_t *al_net_if, int erp_info);
   int      (*set_beacon_vendor_info)                                       (al_net_if_t *al_net_if, unsigned char *vendor_info_buffer, int length);
   int      (*get_ack_timeout)                                                      (al_net_if_t *al_net_if);
   int      (*set_ack_timeout)                                                      (al_net_if_t *al_net_if, unsigned short timeout);
   int      (*get_hide_ssid)                                                        (al_net_if_t *al_net_if);
   int      (*set_hide_ssid)                                                        (al_net_if_t *al_net_if, int hide);
   int      (*enable_beaconing_uplink)                                      (al_net_if_t *al_net_if, unsigned char *vendor_info_buffer, int length);
   int      (*disable_beaconing_uplink)                                     (al_net_if_t *al_net_if);

   /** PHY OPERATIONS */
   int      (*get_supported_rates)                                          (al_net_if_t *al_net_if, unsigned char *supported_rates, int length);
   int      (*get_bit_rate)                                                         (al_net_if_t *al_net_if);
   int      (*set_bit_rate)                                                         (al_net_if_t *al_net_if, int bit_rate);
   void     (*get_rate_table)                                                       (al_net_if_t *al_net_if, al_rate_table_t *rate_table);
   int      (*get_tx_power)                                                         (al_net_if_t *al_net_if, al_802_11_tx_power_info_t *power_info);
   int      (*set_tx_power)                                                         (al_net_if_t *al_net_if, al_802_11_tx_power_info_t *power_info);
   int      (*get_channel_count)                                            (al_net_if_t *al_net_if);
   int      (*get_channel)                                                          (al_net_if_t *al_net_if);
   int      (*set_channel)                                                          (al_net_if_t *al_net_if, int channel);
   int      (*get_phy_mode)                                                         (al_net_if_t *al_net_if);
   int      (*set_phy_mode)                                                         (al_net_if_t *al_net_if, int phy_mode);
   int      (*get_preamble_type)                                            (al_net_if_t *al_net_if);
   int      (*set_preamble_type)                                            (al_net_if_t *al_net_if, int preamble_type);
   int      (*get_reg_domain_info)                                          (al_net_if_t *al_net_if, al_reg_domain_info_t *reg_domain_info);
   int      (*set_reg_domain_info)                                          (al_net_if_t *al_net_if, al_reg_domain_info_t *reg_domain_info);
   int      (*get_supported_channels)                                       (al_net_if_t *al_net_if, al_channel_info_t **supported_channels, int *count);

   int      (*set_dot11e_category_info)                                     (al_net_if_t *al_net_if, al_dot11e_category_info_t *info);
   int      (*set_tx_antenna)                                                       (al_net_if_t *al_net_if, int antenna_index);

   /** OTHER OPERATIONS */
   int      (*set_auth_hook)                                                        (al_net_if_t *al_net_if, al_process_auth_t auth_hook);
   int      (*set_assoc_hook)                                                       (al_net_if_t *al_net_if, al_process_assoc_t assoc_hook);
   int      (*set_essid_info)                                                       (al_net_if_t *al_net_if, al_802_11_security_info_t *net_if_security_info, int count, al_802_11_essid_info_t *info);

   /**RADIO SPECIFIC OPERATIONS */
   int      (*set_radio_data)                                                       (al_net_if_t *al_net_if, unsigned char *radio_data, int data_length);
   int      (*radio_diagnostic_command)                                     (al_net_if_t *al_net_if, unsigned char *diag_data_in, int data_in_length, unsigned char *diag_data_out, int data_out_buf_len);

   /** Action operations */
   int      (*set_action_hook)                                                      (al_net_if_t *al_net_if, al_action_notify_t action_hook);
   int      (*send_action)                                                          (al_net_if_t *al_net_if, al_net_addr_t *addr, unsigned char *buffer, int length);
   int      (*init_channels)                                                        (al_net_if_t *al_net_if);
   int      (*set_use_type)                                                         (al_net_if_t *al_net_if, int use_type);
   int      (*set_all_conf_complete)                                        (al_net_if_t *al_net_if, int value);
   int      (*send_assoc_response)                                          (al_net_if_t *al_net_if, unsigned short stype, unsigned char *address0, unsigned char *address1,
                                                                             unsigned short aid, unsigned char ie_out_valid, al_802_11_information_element_t *ie_out,
                                                                             struct meshap_mac80211_sta_info sta);
   int      (*send_deauth)                                                          (al_net_if_t *al_net_if, unsigned char *address0, unsigned char *address1);

   int      (*del_station)                                                          (al_net_if_t *al_net_if, unsigned char *station_addr);

   int      (*send_auth_response)                                           (al_net_if_t *al_net_if, unsigned char *address0, unsigned char *address1);
   int      (*send_disassoc_from_station)                            (al_net_if_t *al_net_if, int flag);
};

typedef struct al_802_11_operations   al_802_11_operations_t;

#define AL_802_11_IS_WPA_OUI(oui) \
   (                              \
      (oui)[0] == 0x00 &&         \
      (oui)[1] == 0x50 &&         \
      (oui)[2] == 0xF2            \
   )

#define AL_802_11_IS_IEEE_OUI(oui) \
   (                               \
      (oui)[0] == 0x00 &&          \
      (oui)[1] == 0x0F &&          \
      (oui)[2] == 0xAC             \
   )

#define AL_802_11_IS_MD_OUI(oui) \
   (                             \
      (oui)[0] == 0x00 &&        \
      (oui)[1] == 0x12 &&        \
      (oui)[2] == 0xCE           \
   )

int al_802_11_parse_rsn_ie(AL_CONTEXT_PARAM_DECL al_802_11_information_element_t *ie_in, al_802_11_rsn_ie_t *rsn_ie_out);
void al_802_11_cleanup_rsn_ie(AL_CONTEXT_PARAM_DECL al_802_11_rsn_ie_t *rsn_ie);
al_802_11_rsn_ie_t *al_802_11_clone_rsn_ie(AL_CONTEXT_PARAM_DECL al_802_11_rsn_ie_t *rsn_ie);
int al_802_11_format_rsn_ie(AL_CONTEXT_PARAM_DECL al_802_11_security_info_t *security_info, al_802_11_security_info_t *net_if_security_info, unsigned char *buffer, int length);

int al_802_11_parse_md_ie(AL_CONTEXT_PARAM_DECL al_802_11_information_element_t *ie_in, al_802_11_md_ie_t *md_ie_out);
int al_802_11_format_md_ie(AL_CONTEXT_PARAM_DECL al_802_11_md_ie_t *md_ie_in, al_802_11_information_element_t *ie_out);

int al_802_11_format_md_action(AL_CONTEXT_PARAM_DECL unsigned char type, unsigned char *data, unsigned char data_length, unsigned char *buffer);
int al_802_11_parse_md_action(AL_CONTEXT_PARAM_DECL unsigned char *buffer, unsigned char *type, unsigned char *data, unsigned char *data_length);

#define AL_802_11_ACTION_CATEGORY_VENDOR_SPECIFIC    127
#endif /*__AL_802_11_H__*/
