/********************************************************************************
* MeshDynamics
* --------------
* File     : al_print_log.h
* Comments : AL logging macros
* Created  : 6/4/2004
* Author   : Bindu Parakala
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  1  |6/4/2004  | AL_LOG_TYPE_RX | AL_LOG_TYPE_MGM added          | Bindu  |
* -----------------------------------------------------------------------------
* |  0  |6/4/2004  | Created.                                        | Bindu  |
* -----------------------------------------------------------------------------
********************************************************************************/

#ifndef __AL_PRINT_LOG_H__
#define __AL_PRINT_LOG_H__

#ifndef _AL_PRINT_LOG_ENABLED
#define _AL_PRINT_LOG_FILTER    0
#endif

#if _AL_PRINT_LOG_FILTER & AL_LOG_TYPE_ERROR
#define AL_PRINT_LOG_ERROR_0(format)                al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, format)
#define AL_PRINT_LOG_ERROR_1(format, a)             al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, format, a)
#define AL_PRINT_LOG_ERROR_2(format, a, b)          al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, format, a, b)
#define AL_PRINT_LOG_ERROR_3(format, a, b, c)       al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, format, a, b, c)
#define AL_PRINT_LOG_ERROR_4(format, a, b, c, d)    al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, format, a, b, c, d)
#else
#define AL_PRINT_LOG_ERROR_0(format)
#define AL_PRINT_LOG_ERROR_1(format, a)
#define AL_PRINT_LOG_ERROR_2(format, a, b)
#define AL_PRINT_LOG_ERROR_3(format, a, b, c)
#define AL_PRINT_LOG_ERROR_4(format, a, b, c, d)
#endif /* _AL_PRINT_LOG_FILTER & AL_LOG_TYPE_ERROR */

#if _AL_PRINT_LOG_FILTER & AL_LOG_TYPE_FLOW
#define AL_PRINT_LOG_FLOW_0(format)                al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, format)
#define AL_PRINT_LOG_FLOW_1(format, a)             al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, format, a)
#define AL_PRINT_LOG_FLOW_2(format, a, b)          al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, format, a, b)
#define AL_PRINT_LOG_FLOW_3(format, a, b, c)       al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, format, a, b, c)
#define AL_PRINT_LOG_FLOW_4(format, a, b, c, d)    al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, format, a, b, c, d)
#else
#define AL_PRINT_LOG_FLOW_0(format)
#define AL_PRINT_LOG_FLOW_1(format, a)
#define AL_PRINT_LOG_FLOW_2(format, a, b)
#define AL_PRINT_LOG_FLOW_3(format, a, b, c)
#define AL_PRINT_LOG_FLOW_4(format, a, b, c, d)
#endif /* _AL_PRINT_LOG_FILTER & AL_LOG_TYPE_FLOW */

#if _AL_PRINT_LOG_FILTER & AL_LOG_TYPE_DEBUG
#define AL_PRINT_LOG_DEBUG_0(format)                al_print_log(AL_CONTEXT AL_LOG_TYPE_DEBUG, format)
#define AL_PRINT_LOG_DEBUG_1(format, a)             al_print_log(AL_CONTEXT AL_LOG_TYPE_DEBUG, format, a)
#define AL_PRINT_LOG_DEBUG_2(format, a, b)          al_print_log(AL_CONTEXT AL_LOG_TYPE_DEBUG, format, a, b)
#define AL_PRINT_LOG_DEBUG_3(format, a, b, c)       al_print_log(AL_CONTEXT AL_LOG_TYPE_DEBUG, format, a, b, c)
#define AL_PRINT_LOG_DEBUG_4(format, a, b, c, d)    al_print_log(AL_CONTEXT AL_LOG_TYPE_DEBUG, format, a, b, c, d)
#else
#define AL_PRINT_LOG_DEBUG_0(format)
#define AL_PRINT_LOG_DEBUG_1(format, a)
#define AL_PRINT_LOG_DEBUG_2(format, a, b)
#define AL_PRINT_LOG_DEBUG_3(format, a, b, c)
#define AL_PRINT_LOG_DEBUG_4(format, a, b, c, d)
#endif /* _AL_PRINT_LOG_FILTER & AL_LOG_TYPE_DEBUG */

#if _AL_PRINT_LOG_FILTER & AL_LOG_TYPE_MSG
#define AL_PRINT_LOG_MSG_0(format)                al_print_log(AL_CONTEXT AL_LOG_TYPE_MSG, format)
#define AL_PRINT_LOG_MSG_1(format, a)             al_print_log(AL_CONTEXT AL_LOG_TYPE_MSG, format, a)
#define AL_PRINT_LOG_MSG_2(format, a, b)          al_print_log(AL_CONTEXT AL_LOG_TYPE_MSG, format, a, b)
#define AL_PRINT_LOG_MSG_3(format, a, b, c)       al_print_log(AL_CONTEXT AL_LOG_TYPE_MSG, format, a, b, c)
#define AL_PRINT_LOG_MSG_4(format, a, b, c, d)    al_print_log(AL_CONTEXT AL_LOG_TYPE_MSG, format, a, b, c, d)
#else
#define AL_PRINT_LOG_MSG_0(format)
#define AL_PRINT_LOG_MSG_1(format, a)
#define AL_PRINT_LOG_MSG_2(format, a, b)
#define AL_PRINT_LOG_MSG_3(format, a, b, c)
#define AL_PRINT_LOG_MSG_4(format, a, b, c, d)
#endif /* _AL_PRINT_LOG_FILTER & AL_LOG_TYPE_MSG */


#if _AL_PRINT_LOG_FILTER & AL_LOG_TYPE_MGM
#define AL_PRINT_LOG_MGM_0(format)                al_print_log(AL_CONTEXT AL_LOG_TYPE_MGM, format)
#define AL_PRINT_LOG_MGM_1(format, a)             al_print_log(AL_CONTEXT AL_LOG_TYPE_MGM, format, a)
#define AL_PRINT_LOG_MGM_2(format, a, b)          al_print_log(AL_CONTEXT AL_LOG_TYPE_MGM, format, a, b)
#define AL_PRINT_LOG_MGM_3(format, a, b, c)       al_print_log(AL_CONTEXT AL_LOG_TYPE_MGM, format, a, b, c)
#define AL_PRINT_LOG_MGM_4(format, a, b, c, d)    al_print_log(AL_CONTEXT AL_LOG_TYPE_MGM, format, a, b, c, d)
#else
#define AL_PRINT_LOG_MGM_0(format)
#define AL_PRINT_LOG_MGM_1(format, a)
#define AL_PRINT_LOG_MGM_2(format, a, b)
#define AL_PRINT_LOG_MGM_3(format, a, b, c)
#define AL_PRINT_LOG_MGM_4(format, a, b, c, d)
#endif /* _AL_PRINT_LOG_FILTER & AL_LOG_TYPE_MGM */


#if _AL_PRINT_LOG_FILTER & AL_LOG_TYPE_DATA
#define AL_PRINT_LOG_DATA_0(format)                al_print_log(AL_CONTEXT AL_LOG_TYPE_DATA, format)
#define AL_PRINT_LOG_DATA_1(format, a)             al_print_log(AL_CONTEXT AL_LOG_TYPE_DATA, format, a)
#define AL_PRINT_LOG_DATA_2(format, a, b)          al_print_log(AL_CONTEXT AL_LOG_TYPE_DATA, format, a, b)
#define AL_PRINT_LOG_DATA_3(format, a, b, c)       al_print_log(AL_CONTEXT AL_LOG_TYPE_DATA, format, a, b, c)
#define AL_PRINT_LOG_DATA_4(format, a, b, c, d)    al_print_log(AL_CONTEXT AL_LOG_TYPE_DATA, format, a, b, c, d)
#else
#define AL_PRINT_LOG_DATA_0(format)
#define AL_PRINT_LOG_DATA_1(format, a)
#define AL_PRINT_LOG_DATA_2(format, a, b)
#define AL_PRINT_LOG_DATA_3(format, a, b, c)
#define AL_PRINT_LOG_DATA_4(format, a, b, c, d)
#endif /* _AL_PRINT_LOG_FILTER & AL_LOG_TYPE_DATA */


#if _AL_PRINT_LOG_FILTER & AL_LOG_TYPE_RX
#define AL_PRINT_LOG_RX_0(format)                al_print_log(AL_CONTEXT AL_LOG_TYPE_RX, format)
#define AL_PRINT_LOG_RX_1(format, a)             al_print_log(AL_CONTEXT AL_LOG_TYPE_RX, format, a)
#define AL_PRINT_LOG_RX_2(format, a, b)          al_print_log(AL_CONTEXT AL_LOG_TYPE_RX, format, a, b)
#define AL_PRINT_LOG_RX_3(format, a, b, c)       al_print_log(AL_CONTEXT AL_LOG_TYPE_RX, format, a, b, c)
#define AL_PRINT_LOG_RX_4(format, a, b, c, d)    al_print_log(AL_CONTEXT AL_LOG_TYPE_RX, format, a, b, c, d)
#else
#define AL_PRINT_LOG_RX_0(format)
#define AL_PRINT_LOG_RX_1(format, a)
#define AL_PRINT_LOG_RX_2(format, a, b)
#define AL_PRINT_LOG_RX_3(format, a, b, c)
#define AL_PRINT_LOG_RX_4(format, a, b, c, d)
#endif /* _AL_PRINT_LOG_FILTER & AL_LOG_TYPE_RX */


#if _AL_PRINT_LOG_FILTER & AL_LOG_TYPE_TX
#define AL_PRINT_LOG_TX_0(format)                al_print_log(AL_CONTEXT AL_LOG_TYPE_TX, format)
#define AL_PRINT_LOG_TX_1(format, a)             al_print_log(AL_CONTEXT AL_LOG_TYPE_TX, format, a)
#define AL_PRINT_LOG_TX_2(format, a, b)          al_print_log(AL_CONTEXT AL_LOG_TYPE_TX, format, a, b)
#define AL_PRINT_LOG_TX_3(format, a, b, c)       al_print_log(AL_CONTEXT AL_LOG_TYPE_TX, format, a, b, c)
#define AL_PRINT_LOG_TX_4(format, a, b, c, d)    al_print_log(AL_CONTEXT AL_LOG_TYPE_TX, format, a, b, c, d)
#else
#define AL_PRINT_LOG_TX_0(format)
#define AL_PRINT_LOG_TX_1(format, a)
#define AL_PRINT_LOG_TX_2(format, a, b)
#define AL_PRINT_LOG_TX_3(format, a, b, c)
#define AL_PRINT_LOG_TX_4(format, a, b, c, d)
#endif /* _AL_PRINT_LOG_FILTER & AL_LOG_TYPE_TX */


#if _AL_PRINT_LOG_FILTER & (AL_LOG_TYPE_RX | AL_LOG_TYPE_MGM)
#define AL_PRINT_LOG_MGM_RX_0(format)                al_print_log(AL_CONTEXT AL_LOG_MGM_RX, format)
#define AL_PRINT_LOG_MGM_RX_1(format, a)             al_print_log(AL_CONTEXT AL_LOG_MGM_RX, format, a)
#define AL_PRINT_LOG_MGM_RX_2(format, a, b)          al_print_log(AL_CONTEXT AL_LOG_MGM_RX, format, a, b)
#define AL_PRINT_LOG_MGM_RX_3(format, a, b, c)       al_print_log(AL_CONTEXT AL_LOG_MGM_RX, format, a, b, c)
#define AL_PRINT_LOG_MGM_RX_4(format, a, b, c, d)    al_print_log(AL_CONTEXT AL_LOG_MGM_RX, format, a, b, c, d)
#else
#define AL_PRINT_LOG_MGM_RX_0(format)
#define AL_PRINT_LOG_MGM_RX_1(format, a)
#define AL_PRINT_LOG_MGM_RX_2(format, a, b)
#define AL_PRINT_LOG_MGM_RX_3(format, a, b, c)
#define AL_PRINT_LOG_MGM_RX_4(format, a, b, c, d)
#endif /* _AL_PRINT_LOG_FILTER & (AL_LOG_TYPE_RX|AL_LOG_TYPE_MGM) */


#if _AL_PRINT_LOG_FILTER & (AL_LOG_TYPE_MSG | AL_LOG_TYPE_DEBUG)
#define AL_PRINT_LOG_MSG_DEBUG_0(format)                al_print_log(AL_CONTEXT AL_LOG_MGM_RX, format)
#define AL_PRINT_LOG_MSG_DEBUG_1(format, a)             al_print_log(AL_CONTEXT AL_LOG_MGM_RX, format, a)
#define AL_PRINT_LOG_MSG_DEBUG_2(format, a, b)          al_print_log(AL_CONTEXT AL_LOG_MGM_RX, format, a, b)
#define AL_PRINT_LOG_MSG_DEBUG_3(format, a, b, c)       al_print_log(AL_CONTEXT AL_LOG_MGM_RX, format, a, b, c)
#define AL_PRINT_LOG_MSG_DEBUG_4(format, a, b, c, d)    al_print_log(AL_CONTEXT AL_LOG_MGM_RX, format, a, b, c, d)
#else
#define AL_PRINT_LOG_MSG_DEBUG_0(format)
#define AL_PRINT_LOG_MSG_DEBUG_1(format, a)
#define AL_PRINT_LOG_MSG_DEBUG_2(format, a, b)
#define AL_PRINT_LOG_MSG_DEBUG_3(format, a, b, c)
#define AL_PRINT_LOG_MSG_DEBUG_4(format, a, b, c, d)
#endif /* _AL_PRINT_LOG_FILTER & (AL_LOG_TYPE_RX|AL_LOG_TYPE_MGM) */

#endif /*__AL_PRINT_LOG_H__*/
