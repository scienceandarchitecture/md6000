/********************************************************************************
* MeshDynamics
* --------------
* File     : al_atomic.h
* Comments : AL atomic type declarations
* Created  : 3/13/2006
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  1  |1/26/2007 | Added AL_ATOMIC_DEC                             | Sriram |
* -----------------------------------------------------------------------------
* |  0  |3/13/2006 | Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

#ifndef __AL_ATOMIC_H__
#define __AL_ATOMIC_H__

#define al_atomic_t    al_impl_atomic_t

#define AL_ATOMIC_SET(av, v)          AL_IMPL_ATOMIC_SET(av, v)
#define AL_ATOMIC_GET(av)             AL_IMPL_ATOMIC_GET(av)
#define AL_ATOMIC_INC(av)             AL_IMPL_ATOMIC_INC(av)
#define AL_ATOMIC_DEC_AND_TEST(av)    AL_IMPL_ATOMIC_DEC_AND_TEST(av)
#define AL_ATOMIC_DEC(av)             AL_IMPL_ATOMIC_DEC(av)

#endif /*__AL_ATOMIC_H__*/
