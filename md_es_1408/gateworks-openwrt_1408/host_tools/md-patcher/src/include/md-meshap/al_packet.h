/********************************************************************************
* MeshDynamics
* --------------
* File     : al_packet.h
* Comments : Abstraction Layer  packet structure
* Created  : 4/5/2004
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  16 |01/07/2008| Changes for mixed mode	                      |Prachiti|
* -----------------------------------------------------------------------------
* |  15 |11/8/2007 | Changes for queued retry                        |Prachiti|
* -----------------------------------------------------------------------------
* |  14 |4/11/2007 | Flag added for packet specific rates            | Sriram |
* -----------------------------------------------------------------------------
* |  13 |2/23/2007 | Flag added for EFFISTREAM                       |Prachiti|
* -----------------------------------------------------------------------------
* |  12 |1/10/2007 | Flag added for ignoring BCAST/MCAST by mesh     | Sriram |
* -----------------------------------------------------------------------------
* |  11 |11/8/2006 | Support for compression                         | Sriram |
* -----------------------------------------------------------------------------
* |  10 |3/1/2006  | Added ADHOC mode flag                           | Sriram |
* -----------------------------------------------------------------------------
* |  9  |02/15/2006| dot11e changes                                  | Bindu  |
* -----------------------------------------------------------------------------
* |  8  |2/24/2005 | #defines for vlan added                         | Abhijit|
* -----------------------------------------------------------------------------
* |  7  |1/24/2005 | Added members for dot1p/dot1q                   | Sriram |
* -----------------------------------------------------------------------------
* |  6  |12/22/2004| Added flag for using 802.11b rates              | Sriram |
* -----------------------------------------------------------------------------
* |  5  |5/28/2004 | Added reference counting and removed busy flag  | Bindu  |
* -----------------------------------------------------------------------------
* |  4  |5/5/2004  | Packet/Memory Leak Debug params added           | Anand  |
* -----------------------------------------------------------------------------
* |  3  |4/15/2004 | Added type member                               | Sriram |
* -----------------------------------------------------------------------------
* |  2  |4/14/2004 | Added BUSY flag                                 | Bindu P|
* -----------------------------------------------------------------------------
* |  1  |4/8/2004  | Fixed misc syntax errors                        | Bindu P|
* -----------------------------------------------------------------------------
* |  0  |4/5/2004  | Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

#include "al_net_addr.h"

#ifndef __AL_PACKET_H__
#define __AL_PACKET_H__

#define AL_PACKET_FLAGS_BUFFER               0x00000001
#define AL_PACKET_FLAGS_ADDRESS_1            0x00000002
#define AL_PACKET_FLAGS_ADDRESS_2            0x00000004
#define AL_PACKET_FLAGS_ADDRESS_3            0x00000008
#define AL_PACKET_FLAGS_ADDRESS_4            0x00000010
#define AL_PACKET_FLAGS_FRAME_CONTROL        0x00000020
#define AL_PACKET_FLAGS_RSSI                 0x00000040
#define AL_PACKET_FLAGS_BIT_RATE             0x00000080
#define AL_PACKET_FLAGS_USE_802_11B_RATES    0x00000100
#define AL_PACKET_FLAGS_USER_APP_PACKET      0x00000200
#define AL_PACKET_FLAGS_ENCRYPTION           0x00000400
#define AL_PACKET_FLAGS_ADHOC                0x00001000
#define AL_PACKET_FLAGS_DOT11E_ENABLED       0x00002000
#define AL_PACKET_FLAGS_REQUEUED             0x00004000
#define AL_PACKET_FLAGS_VIRTUAL              0x00008000                         /** Packet received when virtually associated */
#define AL_PACKET_FLAGS_COMPRESSION          0x00010000
#define AL_PACKET_FLAGS_MESH_IGNORE          0x00020000                         /** BCAST/MCAST packet to be ignored by mesh children */
#define AL_PACKET_FLAGS_EFFISTREAM           0x00040000
#define AL_PACKET_FLAGS_FIXED_RATE           0x00080000
#define AL_PACKET_FLAGS_QUEUED_RETRY         0x00100000
#define AL_PACKET_FLAGS_VIRTUAL_INFRA        0x00200000                 /* used for virtual net_ifs in packet */
#define AL_PACKET_FLAGS_VIRTUAL_MASTER       0x00400000                 /* used for virtual net_ifs in packet */

#define AL_PACKET_TYPE_IP                    0x0800
#define AL_PACKET_TYPE_ARP                   0x0806
#define AL_PACKET_TYPE_RARP                  0x8035
#define AL_PACKET_TYPE_VLAN                  0x8100
#define AL_PACKET_TYPE_EAPOL                 0x888E

#define AL_PACKET_DOT_1Q_MODE_NONE           0
#define AL_PACKET_DOT_1Q_MODE_TAG_ADD        1
#define AL_PACKET_DOT_1Q_MODE_TAG_REMOVE     2

#define AL_PACKET_VLAN_INFO_DATA_SIZE        4
#define AL_PACKET_VLAN_INFO_TYPE_OFFSET      2
#define AL_PACKET_VLAN_INFO_TAG_SIZE         2

#define AL_PACKET_VLAN_TAG_TYPE_UNTAGGED     0xffff

/**
 * IMPORTANT:
 * ---------
 * Assumption is info has been converted to the host byte order
 */

#define AL_PACKET_VLAN_INFO_GET_TAG(info)         ((info) & 0x0FFF)
#define AL_PACKET_VLAN_INFO_GET_PRIORITY(info)    (((info) >> 13) & 0x7)

#define AL_PACKET_VLAN_INFO_SET_TAG(info, tag) \
   do {                                        \
      (info) &= ~0x0FFF;                       \
      (info) |= ((tag) & 0x0FFF);              \
   } while (0)

#define AL_PACKET_VLAN_INFO_SET_PRIORITY(info, priority) \
   do {                                                  \
      (info) &= ~0xE000;                                 \
      (info) |= ((((priority) & 0x7) << 13));            \
   } while (0)


struct al_packet
{
   unsigned char  *buffer;
   int            buffer_length;
   int            data_length;
   int            position;
   unsigned short type;
   unsigned char  dot1q_mode;                                   /* AL_PACKET_DOT_1Q_MODE_* */
   unsigned short dot1q_tag;                                    /* VLAN Tag */
   unsigned char  dot1p_priority;                               /* 802.1p priority */
   al_net_addr_t  addr_1;                                       /* Immediate destination */
   al_net_addr_t  addr_2;                                       /* Source */
   al_net_addr_t  addr_3;
   al_net_addr_t  addr_4;
   unsigned int   frame_control;
   unsigned int   rssi;
   unsigned int   flags;
   int            bit_rate;
   int            ref_count;
   int            key_index;                            /* Key index to use for encryption/compression */
   unsigned char  dot11e_category;
   unsigned char  dot1p_priority_index;                 /* For use in requeing packets */
#ifdef _AL_PACKET_DEBUG
   const char     *filename;
   int            line;
#endif
};

typedef struct al_packet   al_packet_t;
#endif /*__AL_PACKET_H__*/
