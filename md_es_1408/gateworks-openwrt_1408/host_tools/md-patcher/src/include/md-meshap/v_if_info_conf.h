/********************************************************************************
* MeshDynamics
* --------------
* File     : v_if_info_conf.h
* Comments : virtual if info parser for virtualisation
* Created  : 9/11/2006
* Author   : Prachiti Gaikwad
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  1  |8/27/2008 | Size optimization                               | Sriram |
* -----------------------------------------------------------------------------
* |  0  |9/11/2006 | Created.                                        |Prachiti|
* -----------------------------------------------------------------------------
********************************************************************************/

#ifndef __V_IF_INFO_CONF_H__
#define __V_IF_INFO_CONF_H__

struct _virtual_if_info
{
   char name[32];
   char map_rx[32];
   char map_tx[32];
};

typedef struct _virtual_if_info   _virtual_if_info_t;

int virt_if_info_conf_parse(AL_CONTEXT_PARAM_DECL const char *v_if_info_conf_string);
void virt_if_info_conf_close(AL_CONTEXT_PARAM_DECL int conf_handle);
int virt_if_info_conf_get_count(AL_CONTEXT_PARAM_DECL int conf_handle);
int virt_if_info_conf_get_if(AL_CONTEXT_PARAM_DECL int conf_handle, int v_if_index, _virtual_if_info_t *v_if_info);

#ifndef _MESHAP_V_IF_CONF_NO_SET_AND_PUT

int virt_if_info_conf_set_count(AL_CONTEXT_PARAM_DECL int conf_handle, int v_if_count);
int virt_if_info_conf_set_if(AL_CONTEXT_PARAM_DECL int conf_handle, int v_if_index, _virtual_if_info_t *v_if_info);
#endif /* _MESHAP_V_IF_CONF_NO_SET_AND_PUT */

#endif /*__V_IF_INFO_CONF_H__*/
