/********************************************************************************
* MeshDynamics
* --------------
* File     : al.h
* Comments : Abstraction Layer header
* Created  : 4/5/2004
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* | 48  |8/22/2008 | Changes for FIPS and SIP                        |Abhijit |
* -----------------------------------------------------------------------------
* | 47  |1/22/2008 | Changes for Mixed mode                          |Prachiti|
* -----------------------------------------------------------------------------
* | 46  |8/16/2007 | al_get_packet_pool_info Added                   | Sriram |
* -----------------------------------------------------------------------------
* | 45  |8/16/2007 | al_get_cpu_load_info Added                      | Sriram |
* -----------------------------------------------------------------------------
* | 44  |7/24/2007 | REBOOT logging changes                          | Sriram |
* -----------------------------------------------------------------------------
* | 43  |7/17/2007 | al_get_board_mem_info Added                     | Sriram |
* -----------------------------------------------------------------------------
* | 42  |7/9/2007  | Added al_strobe_reset_generator                 | Sriram |
* -----------------------------------------------------------------------------
* | 41  |7/9/2007  | Added al_enable_reset_generator                 | Sriram |
* -----------------------------------------------------------------------------
* | 40  |11/16/2006| Added Gen 2K Buffer Pool                        | Sriram |
* -----------------------------------------------------------------------------
* | 39  |3/14/2006 | Changes for ACL                                 | Bindu  |
* -----------------------------------------------------------------------------
* | 38  |3/13/2006 | Added atomic definitions                        | Sriram |
* -----------------------------------------------------------------------------
* | 37  |02/15/2006| Additions for dot11e                            | Bindu  |
* -----------------------------------------------------------------------------
* | 36  |9/22/2005 | Interrupt disable/enable semantics changed      | Sriram |
* -----------------------------------------------------------------------------
* | 35  |5/9/2005  | Watchdog timer additional changes               | Sriram |
* -----------------------------------------------------------------------------
* | 34  |5/8/2005  | Watchdog timer framework added                  | Sriram |
* -----------------------------------------------------------------------------
* | 33  |4/22/2005 | Added al_restart_mesh                           | Sriram |
* -----------------------------------------------------------------------------
* | 32  |4/17/2005 | Added al_reboot_board                           | Sriram |
* -----------------------------------------------------------------------------
* | 31  |4/6/2005  | Security changes                                | Sriram |
* -----------------------------------------------------------------------------
* | 30  |03/09/2005| Board info added                                | Anand  |
* -----------------------------------------------------------------------------
* | 29  |03/05/2005| functions added for board information values    | Anand  |
* -----------------------------------------------------------------------------
* | 28  |12/26/2004| Changed interrupt functions to macros           | Sriram |
* -----------------------------------------------------------------------------
* | 27  |11/19/2004| Added random number routines                    | Sriram |
* -----------------------------------------------------------------------------
* | 26  |6/14/2004 | Included al_assert.h                            | Sriram |
* -----------------------------------------------------------------------------
* | 25  |6/9/2004  | Implemented AL_NET_PACKET_DUMP                  | Sriram |
* -----------------------------------------------------------------------------
* | 24  |6/4/2004  | AL_LOG_TYPE_INFORMATION added                   | Bindu  |
* -----------------------------------------------------------------------------
* | 23  |6/4/2004  | Added CRASH_DEBUG log type                      | Sriram |
* -----------------------------------------------------------------------------
* | 22  |5/31/2004 | Added al_get_conf_handle                        | Anand  |
* -----------------------------------------------------------------------------
* | 21  |5/31/2004 | Added al_set_thread_name                        | Sriram |
* -----------------------------------------------------------------------------
* | 20  |5/31/2004 | Defined AL_PACKET_DEBUG_PARAM                   | Sriram |
* -----------------------------------------------------------------------------
* | 19  |5/28/2004 | Added al_add_packet_reference method            | Bindu  |
* -----------------------------------------------------------------------------
* | 18  |5/25/2004 | AL_NOTIFY_MESSAGE_TYPE_SET_STATE  added         | Anand  |
* -----------------------------------------------------------------------------
* | 17  |5/25/2004 | on_phy_link_notify added.                       | Anand  |
* -----------------------------------------------------------------------------
* | 16  |5/21/2004 | Print log types defined.                        | Anand  |
* -----------------------------------------------------------------------------
* | 15  |5/7/2004  | al_notify_message added                         | Anand  |
* -----------------------------------------------------------------------------
* | 14  |5/5/2004  | Packet/Memory Leak Debug params added           | Anand  |
* -----------------------------------------------------------------------------
* | 13  |4/22/2004 | Added FILE_ID & PACKET_MAX_SIZE                 | Bindu  |
* -----------------------------------------------------------------------------
* | 12  |4/22/2004 | Removed AL receive & before_transmit Handlers   | Bindu  |
* -----------------------------------------------------------------------------
* | 11  |4/22/2004 | Added file handling functions                   | Sriram |
* -----------------------------------------------------------------------------
* | 10  |4/21/2004 | **Made (major) changes for AL_CONTEXT**         | Sriram |
* -----------------------------------------------------------------------------
* |  9  |4/21/2004 | Changed al_open_file's parameters               | Sriram |
* -----------------------------------------------------------------------------
* |  8  |4/21/2004 | Wait State codes added                          | Anand  |
* -----------------------------------------------------------------------------
* |  7  |4/21/2004 | Declared handlers as extern with name change    | Bindu  |
* -----------------------------------------------------------------------------
* |  6  |4/14/2004 | Added AL_INLINE                                 | Bindu  |
* -----------------------------------------------------------------------------
* |  5  |4/13/2004 | Added direction parameter to al_allocate_packet | Bindu  |
* -----------------------------------------------------------------------------
* |  4  |4/13/2004 | Included al_endian.h                            | Bindu  |
* -----------------------------------------------------------------------------
* |  3  |4/12/2004 | Added functions for interrupt handling          | Sriram |
* -----------------------------------------------------------------------------
* |  2  |4/12/2004 | Added thread priority constants                 | Bindu  |
* -----------------------------------------------------------------------------
* |  1  |4/6/2004  | Added al_print_log function                     | Sriram |
* -----------------------------------------------------------------------------
* |  0  |4/5/2004  | Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

#include "al_types.h"
#include "al_context.h"
#include "al_net_if.h"
#include "al_endian.h"
#include "al_socket.h"
#include "al_assert.h"
#include "al_802_11.h"

#ifndef __AL_H__
#define __AL_H__

#define SKB_ETHERNET(skb)    eth_hdr(skb)

#define AL_INLINE                          AL_IMPL_INLINE
#define AL_SNPRINTF                        AL_IMPL_SNPRINTF

#define AL_FILE_ID_MESHAP_CONFIG           1
#define AL_FILE_ID_DOT11E_CONFIG           2
#define AL_FILE_ID_ACL_CONFIG              3
#define AL_FILE_ID_VIRT_IF_INFO_CONFIG     4
#define AL_FILE_ID_SIP_CONFIG              5

#define AL_THREAD_PRIORITY_LOW             0
#define AL_THREAD_PRIORITY_BELOW_NORMAL    1
#define AL_THREAD_PRIORITY_NORMAL          2
#define AL_THREAD_PRIORITY_ABOVE_NORMAL    3
#define AL_THREAD_PRIORITY_HIGH            4
#define AL_THREAD_PRIORITY_REAL_TIME       5

#define AL_PACKET_DIRECTION_IN             0
#define AL_PACKET_DIRECTION_OUT            1
#define AL_PACKET_MAX_SIZE                 2400

#define AL_OPEN_FILE_MODE_READ             1
#define AL_OPEN_FILE_MODE_WRITE            2
#define AL_OPEN_FILE_MODE_BINARY           4

struct al_thread_param
{
   int  thread_id;
   void *param;
};

typedef struct al_thread_param   al_thread_param_t;

typedef int (*al_thread_function_t)     (AL_CONTEXT_PARAM_DECL al_thread_param_t *param);

struct al_thread_message
{
   int  thread_id;
   int  message_id;
   void *message_data;
};

typedef struct al_thread_message   al_thread_message_t;

typedef struct meshap_mac80211_sta_info
{
   char           supported_rates[10];
   char           supported_rates_len;
   char           sta_addr[6];
   unsigned short aid;
   unsigned short listen_interval;
   unsigned short capability;
} meshap_mac80211_sta_info_t;

#define AL_NOTIFY_MESSAGE_TYPE_SET_INTERFACE_INFO    1
#define AL_NOTIFY_MESSAGE_TYPE_SET_STATE             2

struct al_notify_message
{
   int  message_type;
   void *message_data;
};

typedef struct al_notify_message   al_notify_message_t;

#define AL_BOARD_INFO_MASK_TEMP       1
#define AL_BOARD_INFO_MASK_VOLTAGE    2

struct al_board_info
{
   short valid_values_mask;
   short temperature;
   short voltage;
};

typedef struct al_board_info   al_board_info_t;

struct al_packet_pool_info
{
   unsigned int pool_max;
   unsigned int pool_current;
   unsigned int pool_min;
   unsigned int gen_2k_max;
   unsigned int gen_2k_current;
   unsigned int gen_2k_min;
   unsigned int gen_2k_heap_alloc;
   unsigned int gen_2k_heap_free;
   unsigned int mip_skb_alloc;
   unsigned int mip_skb_free;
};

typedef struct al_packet_pool_info   al_packet_pool_info_t;

#define AL_NET_PACKET_DUMP(bytes, length)                                                     \
   {                                                                                          \
      int  i, j;                                                                              \
      char str[8];                                                                            \
      char buf[64];                                                                           \
      for (i = 0, j = 0; i<length; i++) {                                                     \
                              if (i % 16 == 0 && i != 0) {                                    \
                                 buf[j] = 0;                                                  \
                                 al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "%s", buf); \
                                 j = 0;                                                       \
                              }                                                               \
                              AL_SNPRINTF(str, sizeof(str), "%02X", bytes[i]);                \
                              buf[j++] = str[0];                                              \
                              buf[j++] = str[1];                                              \
                              buf[j++] = ' ';                                                 \
                           }                                                                  \
                           buf[j] = 0;                                                        \
                           al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "%s", buf);       \
                           j = 0;                                                             \
                           }

#define AL_NET_PACKET_DUMP_EX(name, bytes, length)                                            \
   {                                                                                          \
      int  i, j;                                                                              \
      char str[16];                                                                           \
      char buf[128];                                                                          \
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "unsigned char %s[] = {", name);       \
      for (i = 0, j = 0; i<length; i++) {                                                     \
                              if (i % 16 == 0 && i != 0) {                                    \
                                 buf[j] = 0;                                                  \
                                 al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "%s", buf); \
                                 j = 0;                                                       \
                              }                                                               \
                              AL_SNPRINTF(str, sizeof(str), "0x%02X", bytes[i]);              \
                              buf[j++] = str[0];                                              \
                              buf[j++] = str[1];                                              \
                              buf[j++] = str[2];                                              \
                              buf[j++] = str[3];                                              \
                              buf[j++] = ',';                                                 \
                           }                                                                  \
                           buf[j] = 0;                                                        \
                           al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "%s\n};", buf);   \
                           j = 0;                                                             \
                           }



typedef int (*al_on_receive_t)                  (AL_CONTEXT_PARAM_DECL al_net_if_t *al_net_if, al_packet_t *packet);
typedef int (*al_on_before_transmit_t)  (AL_CONTEXT_PARAM_DECL al_net_if_t *al_net_if, al_packet_t *packet);
typedef int (*al_on_phy_link_notify_t)  (AL_CONTEXT_PARAM_DECL al_net_if_t *al_net_if, int link_state);

#define AL_WAIT_TIMEOUT_INFINITE    0xFFFFFFFF          /** For use with al_wait_on_event and al_wait_on_semaphore */
#define AL_WAIT_STATE_SIGNAL        0                   /** For use with al_wait_on_event and al_wait_on_semaphore */
#define AL_WAIT_STATE_TIMEOUT       1                   /** For use with al_wait_on_event and al_wait_on_semaphore */


#define AL_LOG_TYPE_ERROR           0x01
#define AL_LOG_TYPE_FLOW            0x02
#define AL_LOG_TYPE_DEBUG           0x04
#define AL_LOG_TYPE_MSG             0x08
#define AL_LOG_TYPE_MGM             0x10
#define AL_LOG_TYPE_DATA            0x20
#define AL_LOG_TYPE_RX              0x40
#define AL_LOG_TYPE_TX              0x80
#define AL_LOG_TYPE_CRASH_DEBUG     0x100
#define AL_LOG_TYPE_INFORMATION     0x200

#define AL_LOG_MGM_RX               AL_LOG_TYPE_RX | AL_LOG_TYPE_MGM
#define AL_LOG_MGM_RX_DEBUG         AL_LOG_TYPE_RX | AL_LOG_TYPE_MGM | AL_LOG_TYPE_DEBUG
#define AL_LOG_DATA_RX              AL_LOG_TYPE_RX | AL_LOG_TYPE_DATA
#define AL_LOG_DATA_RX_DEBUG        AL_LOG_TYPE_RX | AL_LOG_TYPE_DATA | AL_LOG_TYPE_DEBUG
#define AL_LOG_MGM_TX               AL_LOG_TYPE_TX | AL_LOG_TYPE_MGM
#define AL_LOG_MGM_TX_DEBUG         AL_LOG_TYPE_TX | AL_LOG_TYPE_MGM | AL_LOG_TYPE_DEBUG
#define AL_LOG_DATA_TX              AL_LOG_TYPE_TX | AL_LOG_TYPE_DATA
#define AL_LOG_DATA_TX_DEBUG        AL_LOG_TYPE_TX | AL_LOG_TYPE_DATA | AL_LOG_TYPE_DEBUG


#ifndef _AL_HEAP_DEBUG
void *al_heap_alloc(AL_CONTEXT_PARAM_DECL size_t size);

#define AL_HEAP_DEBUG_PARAM
#else
void *al_heap_alloc(AL_CONTEXT_PARAM_DECL size_t size, const char *file_name, int line);

#define AL_HEAP_DEBUG_PARAM    , __FILE__, __LINE__
#endif

void al_heap_free(AL_CONTEXT_PARAM_DECL void *memblock);
int al_create_thread(AL_CONTEXT_PARAM_DECL al_thread_function_t thread_function, void *param, int priority, char *name);
int al_post_thread_message(AL_CONTEXT_PARAM_DECL int thread_id, int message_id, void *message_data);
int al_get_thread_message(AL_CONTEXT_PARAM_DECL int thread_id, al_thread_message_t *message);
int al_create_semaphore(AL_CONTEXT_PARAM_DECL int max_count, int initial_count);
int al_wait_on_semaphore(AL_CONTEXT_PARAM_DECL int semaphore_id, unsigned int timeout);
int al_release_semaphore(AL_CONTEXT_PARAM_DECL int semaphore_id, int release_count);
int al_destroy_semaphore(AL_CONTEXT_PARAM_DECL int semaphore_id);
int al_create_event(AL_CONTEXT_PARAM_DECL int manual_reset);
int al_wait_on_event(AL_CONTEXT_PARAM_DECL int event_id, unsigned int timeout);
int al_set_event(AL_CONTEXT_PARAM_DECL int event_id);
int al_reset_event(AL_CONTEXT_PARAM_DECL int event_id);
int al_destroy_event(AL_CONTEXT_PARAM_DECL int event_id);

al_u64_t al_get_tick_count(AL_CONTEXT_PARAM_DECL_SINGLE);

int al_timer_after(AL_CONTEXT_PARAM_DECL al_u64_t timer1, al_u64_t timer2);

int al_timer_after_eq(AL_CONTEXT_PARAM_DECL al_u64_t timer1, al_u64_t timer2);

int al_timer_before(AL_CONTEXT_PARAM_DECL al_u64_t timer1, al_u64_t timer2);

int al_timer_before_eq(AL_CONTEXT_PARAM_DECL al_u64_t timer1, al_u64_t timer2);

void al_thread_sleep(AL_CONTEXT_PARAM_DECL int milliseconds);

int al_get_net_if_count(AL_CONTEXT_PARAM_DECL_SINGLE);
al_net_if_t *al_get_net_if(AL_CONTEXT_PARAM_DECL int index);
int al_set_on_receive_hook(AL_CONTEXT_PARAM_DECL al_on_receive_t on_recieve);
int al_set_on_before_transmit_hook(AL_CONTEXT_PARAM_DECL al_on_before_transmit_t on_before_transmit);
int al_set_on_phy_link_notify_hook(AL_CONTEXT_PARAM_DECL al_on_phy_link_notify_t on_phy_link_notify);
int al_send_packet_up_stack(AL_CONTEXT_PARAM_DECL al_net_if_t *al_net_if, al_packet_t *packet);

void al_set_thread_name(AL_CONTEXT_PARAM_DECL const char *name);

int al_get_conf_handle(AL_CONTEXT_PARAM_DECL_SINGLE);
int al_get_dot11e_conf_handle(AL_CONTEXT_PARAM_DECL_SINGLE);
int al_get_acl_conf_handle(AL_CONTEXT_PARAM_DECL_SINGLE);
int al_get_mobility_conf_handle(AL_CONTEXT_PARAM_DECL_SINGLE);

#ifndef _AL_PACKET_DEBUG
al_packet_t *al_allocate_packet(AL_CONTEXT_PARAM_DECL int direction);

#define AL_PACKET_DEBUG_PARAM
#else
al_packet_t *al_allocate_packet(AL_CONTEXT_PARAM_DECL int direction, const char *filename, int line);

#define AL_PACKET_DEBUG_PARAM    , __FILE__, __LINE__
#endif

void al_release_packet(AL_CONTEXT_PARAM_DECL al_packet_t *packet);
void al_add_packet_reference(AL_CONTEXT_PARAM_DECL al_packet_t *packet);

int al_open_file(AL_CONTEXT_PARAM_DECL int file_id, int mode);
int al_read_file_line(AL_CONTEXT_PARAM_DECL int file_handle, char *string, int n);
int al_read_file(AL_CONTEXT_PARAM_DECL int file_handle, unsigned char *buffer, int bytes);
int al_write_file(AL_CONTEXT_PARAM_DECL int file_handle, unsigned char *buffer, int bytes);
int al_puts_file(AL_CONTEXT_PARAM_DECL int file_handle, const char *string);
int al_printf_file(AL_CONTEXT_PARAM_DECL int file_handle, const char *format, ...);
int al_close_file(AL_CONTEXT_PARAM_DECL int file_handle);
int al_print_log(AL_CONTEXT_PARAM_DECL int log_type, const char *format, ...);

int al_get_board_info(AL_CONTEXT_PARAM_DECL al_board_info_t *board_info);
int al_notify_message(AL_CONTEXT_PARAM_DECL al_notify_message_t *message);

unsigned char *al_get_gen_2KB_buffer(AL_CONTEXT_PARAM_DECL_SINGLE);
void al_release_gen_2KB_buffer(AL_CONTEXT_PARAM_DECL unsigned char *buffer);



#define al_disable_interrupts(flags)    al_impl_disable_interrupts(flags)
#define al_enable_interrupts(flags)     al_impl_enable_interrupts(flags)

#define al_preempt_disable()    preempt_disable()
#define al_preempt_enable()     preempt_enable()

int al_notify_ds_net_if(AL_CONTEXT_PARAM_DECL al_net_if_t *net_if);
int al_random_initialize(AL_CONTEXT_PARAM_DECL unsigned char *seed);
long al_random(AL_CONTEXT_PARAM_DECL int min, int max);
void al_cryptographic_random_bytes(AL_CONTEXT_PARAM_DECL unsigned char *buffer, int length);

#define AL_REBOOT_BOARD_CODE_MESH_THREAD_WATCHDOG     128
#define AL_REBOOT_BOARD_CODE_ETHERNET_LINK_DOWN       129
#define AL_REBOOT_BOARD_CODE_ETHERNET_LINK_UP         130
#define AL_REBOOT_BOARD_CODE_FIPS_SELF_TEST_AES       131
#define AL_REBOOT_BOARD_CODE_FIPS_SELF_TEST_HMAC      132
#define AL_REBOOT_BOARD_CODE_MESH_TABLE_PROTECTION    133
#define AL_REBOOT_BOARD_CODE_FIPS_RNG                 134
#define AL_REBOOT_BOARD_CODE_AL_WATCHDOG_THREAD       135
#define AL_REBOOT_BOARD_CODE_FIPS_SELF_TEST_RNG       136

void al_reboot_board(AL_CONTEXT_PARAM_DECL unsigned char reboot_code);

void al_restart_mesh(AL_CONTEXT_PARAM_DECL_SINGLE);
void al_get_board_mem_info(AL_CONTEXT_PARAM_DECL unsigned int *max_mem, unsigned int *free_mem);
void al_get_cpu_load_info(AL_CONTEXT_PARAM_DECL unsigned char *avg_load);
void al_get_gps_info(AL_CONTEXT_PARAM_DECL char *longitude, char *latitude, char *altitude, char *speed);
int al_atoi(AL_CONTEXT_PARAM_DECL char *string);

void al_enable_reset_generator(AL_CONTEXT_PARAM_DECL int enable_or_disable, int interval_in_millis);

void al_strobe_reset_generator(AL_CONTEXT_PARAM_DECL_SINGLE);

typedef void (*al_watchdog_routine_t)   (void);

int al_create_watchdog(AL_CONTEXT_PARAM_DECL const char *thread_name, int timeout_in_millis, al_watchdog_routine_t routine);
void al_destroy_watchdog(AL_CONTEXT_PARAM_DECL int watchdog_id);
void al_update_watchdog(AL_CONTEXT_PARAM_DECL int watchdog_id);
void al_set_watchdog_timeout(AL_CONTEXT_PARAM_DECL int watchdog_id, int timeout_in_millis);
void al_suspend_watchdog(AL_CONTEXT_PARAM_DECL int watchdog_id);
void al_resume_watchdog(AL_CONTEXT_PARAM_DECL int watchdog_id);

void al_get_packet_pool_info(AL_CONTEXT_PARAM_DECL al_packet_pool_info_t *info);

void al_initialize(AL_CONTEXT_PARAM_DECL_SINGLE);
void al_uninitialize(AL_CONTEXT_PARAM_DECL_SINGLE);


int al_get_sip_conf_handle(AL_CONTEXT_PARAM_DECL_SINGLE);


/* VIJAY : prototype to check the phy mode type as N OR AC or both
 * @function : cheks the interface physical mode and returns the value
 * input arguments : reference to phy_mode
 * return value : 1-only N ; 2-only AC ; 3-both N & AC ; 0-none 
 * */
#define AL_PHY_SUB_TYPE_NO_N_NO_AC_FOR_HT 0 
#define AL_PHY_SUB_TYPE_ONLY_N_FOR_HT 1 
#define AL_PHY_SUB_TYPE_ONLY_AC_FOR_HT 2 
#define AL_PHY_SUB_TYPE_BOTH_N_AND_AC_FOR_HT 3 
extern int meshap_is_N_or_AC_supported_interface(int phy_sub_type);
#endif /*__AL_H__*/
