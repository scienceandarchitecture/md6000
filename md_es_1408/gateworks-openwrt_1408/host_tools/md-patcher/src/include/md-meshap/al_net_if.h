/********************************************************************************
* MeshDynamics
* --------------
* File     : al_net_if.h
* Comments : Abstraction Layer Network Interface Header
* Created  : 4/5/2004
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* | 11  |1/29/2007 | Changes for Allowed VLAN Tag                    | Sriram |
* -----------------------------------------------------------------------------
* | 10  |4/12/2005 | set_hw_addr method added                        | Sriram |
* -----------------------------------------------------------------------------
* |  9  |4/6/2005  | Added ext_config_info                           | Sriram |
* -----------------------------------------------------------------------------
* |  8  |6/15/2004 | Added macro for encapsulation check             | Sriram |
* -----------------------------------------------------------------------------
* |  7  |6/11/2004 | Added transmit_ex                               | Sriram |
* -----------------------------------------------------------------------------
* |  6  |6/10/2004 | Added last_rx and last_tx time                  | Sriram |
* -----------------------------------------------------------------------------
* |  5  |5/25/2004 | Link states added.                              | Anand  |
* -----------------------------------------------------------------------------
* |  4  |5/11/2004 | start & stop function p added to al_net_if      | Anand  |
* -----------------------------------------------------------------------------
* |  3  |4/19/2004 | Added PHY hardware types                        | Sriram |
* -----------------------------------------------------------------------------
* |  2  |4/19/2004 | Added data members to al_net_if                 | Sriram |
* -----------------------------------------------------------------------------
* |  1  |4/8/2004  | Fixed misc syntax errors                        | Bindu P|
* -----------------------------------------------------------------------------
* |  0  |4/5/2004  | Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

#include "al_ip.h"
#include "al_types.h"
#include "al_net_addr.h"
#include "al_packet.h"
//#include <atheros_instance.h>

#ifndef __AL_NET_IF_H__
#define __AL_NET_IF_H__

#define AL_NET_IF_CONFIG_ALLOWED_VLAN_NONE    0x0000
#define AL_NET_IF_CONFIG_ALLOWED_VLAN_ALL     0xFFFF

struct al_net_if_config
{
   al_net_addr_t  hw_addr;

   /**
    * IP Specific Information.
    */
   int            ip_addr_assignment;
   al_ip_addr_t   ip_addr;
   al_ip_addr_t   mask;
   al_ip_addr_t   gateway;
   al_ip_addr_t   broadcast;
   al_ip_addr_t   primary_dns;
   al_ip_addr_t   sec_dns;
   unsigned short allowed_vlan_tag;                     /* AL_NET_IF_CONFIG_ALLOWED_VLAN_* or a specific tag */
   unsigned char  default_flag;
   void           *ext_config_info;
};

typedef struct al_net_if_config   al_net_if_config_t;

struct al_net_if_stat
{
   unsigned long rx_packets;                  /* total packets received       */
   unsigned long tx_packets;                  /* total packets transmitted    */
   unsigned long rx_bytes;                    /* total bytes received         */
   unsigned long tx_bytes;                    /* total bytes transmitted      */
   unsigned long rx_errors;                   /* bad packets received         */
   unsigned long tx_errors;                   /* packet transmit problems     */
   unsigned long rx_dropped;                  /* no space in buffers    */
   unsigned long tx_dropped;                  /* no space available  */
};

typedef struct al_net_if_stat   al_net_if_stat_t;


struct al_net_if_ht_vht_capab
{
       int type;    /* type =1 HT capabilities type =2 VHT capabilities */
       void* ht_vht_capabilities;
       unsigned char sec_chan_offset ; 
};

typedef struct al_net_if_ht_vht_capab  al_net_if_ht_vht_capab_t;

#define AL_NET_IF_ENCAPSULATION_ETHERNET     0
#define AL_NET_IF_ENCAPSULATION_PPP          1
#define AL_NET_IF_ENCAPSULATION_802_3        2
#define AL_NET_IF_ENCAPSULATION_802_11       3

#define AL_NET_IF_FLAGS_UP                   0x00000001         /* Interface is up		     */
#define AL_NET_IF_FLAGS_LOOPBACK             0x00000002         /* is a loopback net		     */
#define AL_NET_IF_FLAGS_POINTOPOINT          0x00000004         /* interface is has p-p link	     */
#define AL_NET_IF_FLAGS_NOARP                0x00000008         /* no ARP protocol		     */
#define AL_NET_IF_FLAGS_PROMISC              0x00000010         /* receive all packets		     */
#define AL_NET_IF_FLAGS_ALLMULTI             0x00000020         /* receive all multicast packets     */
#define AL_NET_IF_FLAGS_MULTICAST            0x00000040         /* Supports multicast		     */
#define AL_NET_IF_FLAGS_ASSOCIATED           0x00000080         /* for wlan stations                     */
#define AL_NET_IF_FLAGS_ACCESS_POINT         0x00000100         /* for wlan access points                */
#define AL_NET_IF_FLAGS_TX_QUEUE_STOPPED     0x00000200         /* TX queue stopped until further notice */
#define AL_NET_IF_FLAGS_LINKED               0x00000400         /* Whether the interface is linked	 */
#define AL_NET_IF_FLAGS_BROADCAST            0x00000800         /* receive all broadcast packets	 */

#define AL_NET_IF_FILTER_MODE_UNICAST        1
#define AL_NET_IF_FILTER_MODE_MULTICAST      2
#define AL_NET_IF_FILTER_MODE_BROADCAST      4
#define AL_NET_IF_FILTER_MODE_PROMISCOUS     8

#define AL_NET_IF_PHY_HW_TYPE_802_11_B       1
#define AL_NET_IF_PHY_HW_TYPE_802_11_G_B     1
#define AL_NET_IF_PHY_HW_TYPE_802_11_A       2
#define AL_NET_IF_PHY_HW_TYPE_802_3          3

// VIJAY : adding MACROS to PHY_SUB_TYPE need to double check it before commiting.
// VIJAY : added for N & AC support
#define AL_NET_IF_PHY_HW_TYPE_802_11_N      10
#define AL_NET_IF_PHY_HW_TYPE_802_11_AC     11
#define AL_NET_IF_PHY_HW_TYPE_802_11_BGN    12
#define AL_NET_IF_PHY_HW_TYPE_802_11_AN     13
#define AL_NET_IF_PHY_HW_TYPE_802_11_ANAC   14


#define AL_NET_IF_LINK_STATE_NOT_LINKED      0
#define AL_NET_IF_LINK_STATE_LINKED          1

#define AL_NET_IF_TRANSMIT_TYPE_IMMEDIATE    1
#define AL_NET_IF_TRANSMIT_TYPE_QUEUED       2

#define AL_NET_IF_BUFFERING_STATE_NONE       0
#define AL_NET_IF_BUFFERING_STATE_REQUEUE    1
#define AL_NET_IF_BUFFERING_STATE_DROP       2

struct al_net_if
{
   al_net_if_config_t config;
   char               name[16];
   int                encapsulation;
   int                arp_hw_type;
   int                mtu;
   al_atomic_t        buffering_state;
   int                phy_hw_type;
   al_u64_t           last_rx_time;
   al_u64_t           last_tx_time;
   int                (*get_name)(struct al_net_if *al_net_if, char *buffer, int length);
   int                (*get_encapsulation)(struct al_net_if *al_net_if);
   int                (*get_arp_hw_type)(struct al_net_if *al_net_if);
   int                (*get_hw_addr_size)(struct al_net_if *al_net_if);
   int                (*get_mtu)(struct al_net_if *al_net_if);
   int                (*get_config)(struct al_net_if *al_net_if, al_net_if_config_t *config);
   int                (*get_stats)(struct al_net_if *al_net_if, al_net_if_stat_t *stats);
   int                (*get_last_rx_time)(struct al_net_if *al_net_if, al_u64_t *last_rx_time);
   int                (*get_last_tx_time)(struct al_net_if *al_net_if, al_u64_t *last_tx_time);
   int                (*get_flags)(struct al_net_if *al_net_if);
   int                (*transmit)(struct al_net_if *al_net_if, al_packet_t *packet);
   int                (*set_filter_mode)(struct al_net_if *al_net_if, int filter_mode);
   void               * (*get_extended_operations)(struct al_net_if *al_net_if);
   int                (*start)(struct al_net_if *al_net_if);
   int                (*stop)(struct al_net_if *al_net_if);
   int                (*transmit_ex)(struct al_net_if *al_net_if, al_packet_t *packet, int transmit_type);
   int                (*set_hw_addr)(struct al_net_if *al_net_if, al_net_addr_t *hw_addr);
};

typedef struct al_net_if   al_net_if_t;

#define AL_NET_IF_IS_WIRELESS(net_if) \
   (net_if->encapsulation == AL_NET_IF_ENCAPSULATION_802_11)
#define AL_NET_IF_IS_ETHERNET(net_if)                           \
   ((net_if->encapsulation == AL_NET_IF_ENCAPSULATION_ETHERNET) \
    || (net_if->encapsulation == AL_NET_IF_ENCAPSULATION_802_3))
#endif /*__AL_NET_IF_H__*/
