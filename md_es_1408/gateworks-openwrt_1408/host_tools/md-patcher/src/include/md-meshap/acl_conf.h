/********************************************************************************
* MeshDynamics
* --------------
* File     : acl_conf.h
* Comments : ACL Parser Header
* Created  : 3/13/2006
* Author   : Bindu Parakala
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  3  |8/27/2008 | Size optimization                               | Sriram |
* -----------------------------------------------------------------------------
* |  2  |3/15/2006 | Changes for UNTAGGED value                      | Sriram |
* -----------------------------------------------------------------------------
* |  1  |3/15/2006 | Added .11e enabled & category for ACL           | Bindu  |
* |     |          | Removed vlan_name                               |        |
* -----------------------------------------------------------------------------
* |  0  |3/13/2006 | Created                                         | Bindu  |
* -----------------------------------------------------------------------------
********************************************************************************/

#ifndef __ACL_CONF_H__
#define __ACL_CONF_H__

#define ACL_CONF_VLAN_UNTAGGED    0xFFFF

struct acl_conf_info
{
   unsigned char  sta_mac[6];
   unsigned short vlan_tag;
   unsigned char  allow;
   unsigned char  dot11e_enabled;
   unsigned char  dot11e_category;
};

typedef struct acl_conf_info   acl_conf_info_t;


struct acl_conf
{
   int             parse_mode;
   int             count;
   acl_conf_info_t *info;
};

typedef struct acl_conf   acl_conf_t;

int acl_conf_parse(AL_CONTEXT_PARAM_DECL const char *acl_conf_string);
void acl_conf_close(AL_CONTEXT_PARAM_DECL int acl_conf_handle);

int acl_conf_get_info_count(AL_CONTEXT_PARAM_DECL int acl_conf_handle);
int acl_conf_get_info(AL_CONTEXT_PARAM_DECL int acl_conf_handle, int index, acl_conf_info_t *info);


#ifndef _MESHAP_ACL_CONF_NO_SET_AND_PUT

int acl_conf_set_info_count(AL_CONTEXT_PARAM_DECL int acl_conf_handle, int count);
int acl_conf_set_info(AL_CONTEXT_PARAM_DECL int acl_conf_handle, int index, acl_conf_info_t *info);
int acl_conf_create_config_string_from_data(AL_CONTEXT_PARAM_DECL int acl_conf_handle, int *config_string_length, char **acl_conf_string);
int acl_conf_put(AL_CONTEXT_PARAM_DECL int acl_conf_handle);
#endif /* _MESHAP_ACL_CONF_NO_SET_AND_PUT */

#endif /* __ACL_CONF_H__ */
