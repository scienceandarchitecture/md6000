
/********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : al_impl.h
 * Comments : Shasta AL implementation
 * Created  : 9/28/2004
 * Author   : Sriram Dayanandan
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * -----------------------------------------------------------------------------
 * | No  |Date      |  Comment                                        | Author |
 * -----------------------------------------------------------------------------
 * |  1  |3/30/2006 | Added atomic type definitions                   | Sriram |
 * -----------------------------------------------------------------------------
 * |  0  |9/28/2004 | Created.                                        | Sriram |
 * -----------------------------------------------------------------------------
 ********************************************************************************/

#ifndef __AL_IMPL_H__
#define __AL_IMPL_H__

typedef unsigned long long al_impl_u64_t;

#define al_impl_le32_to_cpu(n)	al_swap32(n)
#define al_impl_le16_to_cpu(n)	al_swap16(n)
#define al_impl_be32_to_cpu(n)	(n)
#define al_impl_be16_to_cpu(n)	(n)

#define al_impl_cpu_to_le32(n)	al_swap32(n)
#define al_impl_cpu_to_le16(n)	al_swap16(n)
#define al_impl_cpu_to_be16(n)	(n)
#define al_impl_cpu_to_be32(n)	(n)
#define AL_IMPL_SNPRINTF		snprintf

int snprintf(char * buf, size_t size, const char *fmt, ...);

#if __KERNEL__
	#define al_impl_atomic_t					atomic_t
	#define AL_IMPL_ATOMIC_SET(av,v)			atomic_set(&(av),(v))
	#define AL_IMPL_ATOMIC_GET(av)				atomic_read(&(av))
	#define AL_IMPL_ATOMIC_INC(av)				atomic_inc(&(av))
	#define AL_IMPL_ATOMIC_DEC_AND_TEST(av)		atomic_dec_and_test(&(av))
#else
	#define al_impl_atomic_t					int
	#define AL_IMPL_ATOMIC_SET(av,v)			do { (av) = (v); }while(0)
	#define AL_IMPL_ATOMIC_GET(av)				(av)
	#define AL_IMPL_ATOMIC_INC(av)				do { ++(av); } while(0)
	#define AL_IMPL_ATOMIC_DEC_AND_TEST(av)		((--(av)) == 0)
#endif

#endif /*__AL_IMPL_H__*/
