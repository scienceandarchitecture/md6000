/********************************************************************************
* MeshDynamics
* --------------
* File     : torna_byte_order.h
* Comments : Torna byte order functions
* Created  : 10/22/2004
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  0  |10/22/2004| Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

#ifndef __TORNA_BYTE_ORDER_H__
#define __TORNA_BYTE_ORDER_H__

#define TORNA_GET_LE_SHORT(s, p)    \
   ({                               \
      memcpy(&s, p, sizeof(short)); \
      le16_to_cpu(s);               \
   }                                \
   )

#define TORNA_GET_LE_LONG(l, p)    \
   ({                              \
      memcpy(&l, p, sizeof(long)); \
      le32_to_cpu(l);              \
   }                               \
   )

#define TORNA_GET_BE_SHORT(s, p)    \
   ({                               \
      memcpy(&s, p, sizeof(short)); \
      be16_to_cpu(s);               \
   }                                \
   )

#define TORNA_GET_BE_LONG(l, p)    \
   ({                              \
      memcpy(&l, p, sizeof(long)); \
      be32_to_cpu(l);              \
   }                               \
   )

#define TORNA_GET_SHORT(s, p)       \
   ({                               \
      memcpy(&s, p, sizeof(short)); \
      s;                            \
   }                                \
   )

#define TORNA_GET_LONG(l, p)       \
   ({                              \
      memcpy(&l, p, sizeof(long)); \
      l;                           \
   }                               \
   )

#define TORNA_PUT_LE_SHORT(v, s, p) \
   do {                             \
      s = cpu_to_le16(v);           \
      memcpy(p, &s, sizeof(short)); \
   } while (0)

#define TORNA_PUT_BE_SHORT(v, s, p) \
   do {                             \
      s = cpu_to_be16(v);           \
      memcpy(p, &s, sizeof(short)); \
   } while (0)

#define TORNA_PUT_LE_LONG(v, l, p) \
   do {                            \
      l = cpu_to_le32(v);          \
      memcpy(p, &s, sizeof(long)); \
   } while (0)

#define TORNA_PUT_BE_LONG(v, l, p) \
   do {                            \
      l = cpu_to_be32(v);          \
      memcpy(p, &l, sizeof(long)); \
   } while (0)

#define TORNA_PUT_SHORT(v, s, p)    \
   do {                             \
      s = v;                        \
      memcpy(p, &s, sizeof(short)); \
   } while (0)

#define TORNA_PUT_LONG(v, l, p)    \
   do {                            \
      l = v;                       \
      memcpy(p, &l, sizeof(long)); \
   } while (0)

#endif /*__TORNA_BYTE_ORDER_H__*/
