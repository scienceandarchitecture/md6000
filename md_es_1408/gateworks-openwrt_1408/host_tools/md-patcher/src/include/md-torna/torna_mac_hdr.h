/********************************************************************************
* MeshDynamics
* --------------
* File     : torna_mac_hdr.h
* Comments : Torna Linux skbuff MAC header
* Created  : 5/11/2004
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* | 12  |11/20/2007| Changes for mixed mode						  |Prachiti|
* -----------------------------------------------------------------------------
* | 11  |11/8/2007 | Changes for queued retry                        |Prachiti|
* -----------------------------------------------------------------------------
* | 10  |4/11/2007 | Changes for Packet specific rates               | Sriram |
* -----------------------------------------------------------------------------
* |  9  |2/23/2007 | Flag added for EFFISTREAM                       |Prachiti|
* -----------------------------------------------------------------------------
* |  8  |1/10/2007 | Flag added for BCAST ignoring                   | Sriram |
* -----------------------------------------------------------------------------
* |  7  |11/8/2006 | Support for compression                         | Sriram |
* -----------------------------------------------------------------------------
* |  6  |03/01/2006| Flag added for AODV ad-hoc mode packket         | Sriram |
* -----------------------------------------------------------------------------
* |  5  |02/15/2006| Flag for dot11e added                           | Bindu  |
* -----------------------------------------------------------------------------
* |  4  |4/11/2005 | VLAN fields added                               | Sriram |
* -----------------------------------------------------------------------------
* |  3  |4/6/2005  | Security changes                                | Sriram |
* -----------------------------------------------------------------------------
* |  2  |12/22/2004| Added flag for using 802.11b rates              | Sriram |
* -----------------------------------------------------------------------------
* |  1  |6/10/2004 | Added type member                               | Sriram |
* -----------------------------------------------------------------------------
* |  0  |5/11/2004 | Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

#ifndef __TORNA_MAC_HDR_H__
#define __TORNA_MAC_HDR_H__


/**
 * The Torna MAC header basically is a specialized ethernet header, with
 * additional Torna specific fields. To verify whether the skb->mac->ethernet
 * is actually a torna_mac_hdr, the signature field contains two 32 bit MAGIC
 * numbers 0xAAAABBBB and 0xCCCCDDDD. Hence using the signature fields torna
 * headers can be identified.
 */

#define TORNA_MAC_HDR_SIGNATURE_1            0xAAAABBBB
#define TORNA_MAC_HDR_SIGNATURE_2            0xCCCCDDDD

#define TORNA_MAC_HDR_TYPE_HEAP              1
#define TORNA_MAC_HDR_TYPE_SK_BUFF           2

#define TORNA_MAC_HDR_FLAG_STANDARD_WDS      0x0001                     /** Transmits packets using standard WDS and not TORNA wds */
#define TORNA_MAC_HDR_FLAG_802_11_B_RATES    0x0002                     /** Transmits packets using 802.11b rates when in 802.11g mode */
#define TORNA_MAC_HDR_FLAG_ENCRYPTION        0x0004                     /** Encrypt packet before transmitting */
#define TORNA_MAC_HDR_FLAG_DOT11E_ENABLED    0x0008                     /** Enabled 802.11e flag for packet */
#define TORNA_MAC_HDR_FLAG_ADHOC             0x0010                     /** AODV ADHOC mode packet */
#define TORNA_MAC_HDR_FLAG_VIRTUAL           0x0020                     /** Packet received when virtually associated */
#define TORNA_MAC_HDR_FLAG_COMPRESSION       0x0040                     /** Compress packet using hardware if possible */
#define TORNA_MAC_HDR_FLAG_MESH_IGNORE       0x0080                     /** Set when BCAST/MCAST to be ingored by mesh nodes */
#define TORNA_MAC_HDR_FLAG_EFFISTREAM        0x0100                     /** When set, the packet is transmitted without retries */
#define TORNA_MAC_HDR_FLAG_FIXED_RATE        0x0200                     /** When set, tx_rate is the rate index to transmit packet at */
#define TORNA_MAC_HDR_FLAG_QUEUED_RETRY      0x0400
#define TORNA_MAC_HDR_FLAG_VIRTUAL_MASTER    0x0800                     /** */
#define TORNA_MAC_HDR_FLAG_VIRTUAL_INFRA     0x1000                     /** */

struct torna_mac_hdr_ext_info;

struct torna_mac_hdr
{
   struct ethhdr  ethernet;
   u16            flags;                                                        /* TORNA_MAC_HDR_FLAG_* */
   u32            signature[2];                                                 /* 0xAAAABBBB,0xCCCCDDDD */
   u16            frame_control;                                                /* The 802.11 frame control */
   u16            tx_rate;                                                      /** In Mbps for RX packets, and is a rate index for TX packets with TORNA_MAC_HDR_FLAG_FIXED_RATE set */
   u8             type;                                                         /** TORNA_MAC_HDR_TYPE_* */
   u8             rssi;                                                         /** Valid in Received frames (0-100)*/
   u8             wme_class;                                                    /** 802.11e priority class */
   u8             key_index;                                                    /** Key index used for encryption */
   u8             addresses[4][6];                                              /** The 4 802.11 addresses */
   u32            data_len;                                                     /** Not used currently (will substitute later) */
   unsigned short vlan_info;                                                    /** Always in network byte order */
   unsigned short vlan_original_type;                                           /** Always in network byte order */
   u16            qos_control;							/** Holds qos control filed*/
}
__attribute__((packed));

typedef struct torna_mac_hdr   torna_mac_hdr_t;


#endif /*__TORNA_MAC_HDR_H__*/
