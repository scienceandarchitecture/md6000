/********************************************************************************
* MeshDynamics
* --------------
* File     : torna_wds.h
* Comments : Torna WDS header
* Created  : 5/11/2004
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  3  |6/6/2007  | Fixed-rate field added                          | Sriram |
* -----------------------------------------------------------------------------
* |  2  |3/12/2007 | NO ACK field added                              |Prachiti|
* -----------------------------------------------------------------------------
* |  1  |2/28/2006 | 802.11e and Ad-hoc field added                  | Sriram |
* -----------------------------------------------------------------------------
* |  0  |5/11/2004 | Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

#ifndef __TORNA_WDS_H__
#define __TORNA_WDS_H__

/**
 * Some chipsets do not support WDS frames. e.g. Intersil Prism (fw < 1.5.0)
 * For such chipsets, Torna uses a proprietary WDS header, wherein the fourth
 * address is stored in the data portion, following (if present) the 802.2
 * LLC SNAP header.
 *
 * Version 1
 * ---------
 *
 * The Torna WDS header starts of with the 5 byte signature 'TORNA',followed
 * by one byte version and one byte type. The type field can be 0 for FROM_DS,
 * 1 for TO_DS. Following the type field is the 6 byte ADDRESS field.
 *
 *     0       1      2     3      4      5       6     7      8      9     10     11      12     13
 *	+------+------+------+------+------+------+------+------+------+------+------+------+------+------+
 *	| 'T'  |  'O' |  'R' |  'N' |  'A' |VER   | TYPE |                 ADDRESS                 |FLAGS |
 *	+------+------+------+------+------+------+------+------+------+------+------+------+------+------+
 *
 * The meaning of the ADDRESS field depends on the type field, and is described below.
 *
 *		TYPE			ADDRESS
 *     --------------------------
 *		0 (FROM_DS)	    DST ADDR
 *      1 (TO_DS)		SRC ADDR
 *
 *
 * The FLAGS field (previously the PAD field) is defined as follows:
 *
 *  +-----+-----+-----+-----+-----+-----+-----+-----+
 *  |ADHOC| EX  | FR  |NOACK| EV  |   802.11E CAT   |
 *  +-----+-----+-----+-----+-----+-----+-----+-----+
 *     7     6     5     4     3    2      1     0
 *
 * The ADHOC bit specifies AODV packet
 * The VALID bit specifies if the 802.11e CAT field has valid data
 * 802.11e category specifies one of 8 priority levels.
 * NOACK field specifies EFFISTREAM NO ACK policy
 * FR field specifies the packet was sent with a Fixed-bit-rate policy
 * EX field specifies that an extension length byte follows the header followed
 * by the extension length number of bytes of additional data
 *
 */

#define TORNA_WDS_HEADER_SIZE                14
#define TORNA_WDS_SIGNATURE                  { 'T', 'O', 'R', 'N', 'A' }
#define TORNA_WDS_SIGNATURE_SIZE             5
#define TORNA_WDS_VERSION                    1

#define TORNA_WDS_HEADER_VER_POSITION        5
#define TORNA_WDS_HEADER_TYPE_POSITION       6
#define TORNA_WDS_HEADER_ADDRESS_POSITION    7
#define TORNA_WDS_HEADER_FLAGS_POSITION      13

#define TORNA_WDS_TYPE_FROM_DS               0
#define TORNA_WDS_TYPE_TO_DS                 1

#define TORNA_WDS_FLAGS_GET_DOT11E_CAT(flags)         ((flags) & 0x07)
#define TORNA_WDS_FLAGS_SET_DOT11E_CAT(flags, cat)    do { (flags) &= 0xF8; (flags) |= (cat & 0x07); } while (0)

#define TORNA_WDS_FLAGS_GET_11E_VALID(flags)          (((flags) >> 3) & 0x01)
#define TORNA_WDS_FLAGS_SET_11E_VALID(flags, val)     do { (flags) &= 0xF7; (flags) |= ((val) << 3); } while (0)

#define TORNA_WDS_FLAGS_GET_NOACK(flags)              (((flags) >> 4) & 0x01)
#define TORNA_WDS_FLAGS_SET_NOACK(flags, val)         do { (flags) &= 0xEF; (flags) |= ((val) << 4); } while (0)

#define TORNA_WDS_FLAGS_GET_FIXED_RATE(flags)         (((flags) >> 5) & 0x01)
#define TORNA_WDS_FLAGS_SET_FIXED_RATE(flags, val)    do { (flags) &= 0xDF; (flags) |= ((val) << 5); } while (0)

#define TORNA_WDS_FLAGS_GET_ADHOC(flags)              (((flags) >> 7) & 0x01)
#define TORNA_WDS_FLAGS_SET_ADHOC(flags, adhoc)       do { (flags) &= 0x7F; (flags) |= ((adhoc) << 7); } while (0)


#endif /*__TORNA_WDS_H__*/
