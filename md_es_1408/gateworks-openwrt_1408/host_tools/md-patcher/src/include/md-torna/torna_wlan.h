/********************************************************************************
 * MeshDynamics
 * --------------
 * File     : torna_wlan.h
 * Comments : Torna 802.11 definitions
 * Created  : 5/11/2004
 * Author   : Sriram Dayanandan
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
 *
 * File Revision History
 * -----------------------------------------------------------------------------
 * | No  |Date      |  Comment                                        | Author |
 * -----------------------------------------------------------------------------
 * |  5  |2/12/2009 | Changes for Action frame                        | Abhijit|
 * -----------------------------------------------------------------------------
 * |  4  |2/14/2007 | Changes for PS PHY sub-types                    | Sriram |
 * -----------------------------------------------------------------------------
 * |  3  |10/18/2006| Changes for Radar detection                     | Sriram |
 * -----------------------------------------------------------------------------
 * |  2  |4/16/2005 | Added FROMDS and TODS monitor flags             | Sriram |
 * -----------------------------------------------------------------------------
 * |  1  |4/6/2005  | Security changes                                | Sriram |
 * -----------------------------------------------------------------------------
 * |  0  |5/11/2004 | Created.                                        | Sriram |
 * -----------------------------------------------------------------------------
 *********************************************************************************/

#ifndef __TORNA_WLAN_H__
#define __TORNA_WLAN_H__

/* IEEE 802.11 defines */

#define WLAN_FC_PVER        (TORNA_BIT(1) | TORNA_BIT(0))
#define WLAN_FC_TODS        TORNA_BIT(8)
#define WLAN_FC_FROMDS      TORNA_BIT(9)
#define WLAN_FC_MOREFRAG    TORNA_BIT(10)
#define WLAN_FC_RETRY       TORNA_BIT(11)
#define WLAN_FC_PWRMGT      TORNA_BIT(12)
#define WLAN_FC_MOREDATA    TORNA_BIT(13)
#define WLAN_FC_ISWEP       TORNA_BIT(14)
#define WLAN_FC_ORDER       TORNA_BIT(15)

#define WLAN_FC_GET_TYPE(fc)            (((fc) & (TORNA_BIT(3) | TORNA_BIT(2))) >> 2)
#define WLAN_FC_GET_STYPE(fc)           (((fc) & (TORNA_BIT(7) | TORNA_BIT(6) | TORNA_BIT(5) | TORNA_BIT(4))) >> 4)

#define WLAN_FC_SET_TYPE(fc, type)      do { (fc) &= 0xFFF3; (fc) |= ((type) << 2); } while (0)
#define WLAN_FC_SET_STYPE(fc, stype)    do { (fc) &= 0xFF0F; (fc) |= ((stype) << 4); } while (0)

#define WLAN_GET_SEQ_FRAG(seq)          ((seq) & (TORNA_BIT(3) | TORNA_BIT(2) | TORNA_BIT(1) | TORNA_BIT(0)))
#define WLAN_GET_SEQ_SEQ(seq)           (((seq) & (~(TORNA_BIT(3) | TORNA_BIT(2) | TORNA_BIT(1) | TORNA_BIT(0)))) >> 4)

#define WLAN_FC_TYPE_MGMT    0
#define WLAN_FC_TYPE_CTRL    1
#define WLAN_FC_TYPE_DATA    2

/* management */

#define WLAN_FC_STYPE_ASSOC_REQ       0
#define WLAN_FC_STYPE_ASSOC_RESP      1
#define WLAN_FC_STYPE_REASSOC_REQ     2
#define WLAN_FC_STYPE_REASSOC_RESP    3
#define WLAN_FC_STYPE_PROBE_REQ       4
#define WLAN_FC_STYPE_PROBE_RESP      5
#define WLAN_FC_STYPE_BEACON          8
#define WLAN_FC_STYPE_ATIM            9
#define WLAN_FC_STYPE_DISASSOC        10
#define WLAN_FC_STYPE_AUTH            11
#define WLAN_FC_STYPE_DEAUTH          12
#define WLAN_FC_STYPE_ACTION          13

/* control */

#define WLAN_FC_STYPE_PSPOLL      10
#define WLAN_FC_STYPE_RTS         11
#define WLAN_FC_STYPE_CTS         12
#define WLAN_FC_STYPE_ACK         13
#define WLAN_FC_STYPE_CFEND       14
#define WLAN_FC_STYPE_CFENDACK    15

/* data */

#define WLAN_FC_STYPE_DATA              0
#define WLAN_FC_STYPE_DATA_CFACK        1
#define WLAN_FC_STYPE_DATA_CFPOLL       2
#define WLAN_FC_STYPE_DATA_CFACKPOLL    3
#define WLAN_FC_STYPE_NULLFUNC          4
#define WLAN_FC_STYPE_CFACK             5
#define WLAN_FC_STYPE_CFPOLL            6
#define WLAN_FC_STYPE_CFACKPOLL         7
#define WLAN_FC_STYPE_QOS               8

#define WLAN_WEP_EXT_IV                 0x20


/* Authentication algorithms */

#define WLAN_AUTH_OPEN                              0
#define WLAN_AUTH_SHARED_KEY                        1

#define WLAN_AUTH_CHALLENGE_LEN                     128

#if 0
#define WLAN_CAPABILITY_ESS                         TORNA_BIT(0)
#define WLAN_CAPABILITY_IBSS                        TORNA_BIT(1)
#define WLAN_CAPABILITY_CF_POLLABLE                 TORNA_BIT(2)
#define WLAN_CAPABILITY_CF_POLL_REQUEST             TORNA_BIT(3)
#define WLAN_CAPABILITY_PRIVACY                     TORNA_BIT(4)
#define WLAN_CAPABILITY_SHORT_PREAMBLE              TORNA_BIT(5)
#define WLAN_CAPABILITY_PBCC                        TORNA_BIT(6)
#define WLAN_CAPABILITY_CHANNEL_AGILITY             TORNA_BIT(7)
#define WLAN_CAPABILITY_SHORT_SLOT_TIME             TORNA_BIT(10)
#define WLAN_CAPABILITY_DSSS_OFDM                   TORNA_BIT(13)
/* Status codes */

#define WLAN_STATUS_SUCCESS                         0
#define WLAN_STATUS_UNSPECIFIED_FAILURE             1
#define WLAN_STATUS_CAPS_UNSUPPORTED                10
#define WLAN_STATUS_REASSOC_NO_ASSOC                11
#define WLAN_STATUS_ASSOC_DENIED_UNSPEC             12
#define WLAN_STATUS_NOT_SUPPORTED_AUTH_ALG          13
#define WLAN_STATUS_UNKNOWN_AUTH_TRANSACTION        14
#define WLAN_STATUS_CHALLENGE_FAIL                  15
#define WLAN_STATUS_AUTH_TIMEOUT                    16
#define WLAN_STATUS_AP_UNABLE_TO_HANDLE_NEW_STA     17
#define WLAN_STATUS_ASSOC_DENIED_RATES              18
#define WLAN_STATUS_ASSOC_DENIED_SHORT_PREAMBLE     19
#define WLAN_STATUS_ASSOC_DENIED_PBCC               20
#define WLAN_STATUS_ASSOC_DENIED_CHANNEL_AGILITY    21
#define WLAN_STATUS_ASSOC_DENIED_SHORT_SLOT         25
#define WLAN_STATUS_ASSOC_DENIED_DSSS_OFDM          26
/* 802.11b */

#define WLAN_STATUS_ASSOC_DENIED_NOSHORT            19
#define WLAN_STATUS_ASSOC_DENIED_NOPBCC             20
#define WLAN_STATUS_ASSOC_DENIED_NOAGILITY          21

/* Reason codes */

#define WLAN_REASON_UNSPECIFIED                       1
#define WLAN_REASON_PREV_AUTH_NOT_VALID               2
#define WLAN_REASON_DEAUTH_LEAVING                    3
#define WLAN_REASON_DISASSOC_DUE_TO_INACTIVITY        4
#define WLAN_REASON_DISASSOC_AP_BUSY                  5
#define WLAN_REASON_CLASS2_FRAME_FROM_NONAUTH_STA     6
#define WLAN_REASON_CLASS3_FRAME_FROM_NONASSOC_STA    7
#define WLAN_REASON_DISASSOC_STA_HAS_LEFT             8
#define WLAN_REASON_STA_REQ_ASSOC_WITHOUT_AUTH        9

/* Information Element IDs */

#define WLAN_EID_SSID               0
#define WLAN_EID_SUPP_RATES         1
#define WLAN_EID_FH_PARAMS          2
#define WLAN_EID_DS_PARAMS          3
#define WLAN_EID_CF_PARAMS          4
#define WLAN_EID_TIM                5
#define WLAN_EID_IBSS_PARAMS        6
#define WLAN_EID_CHALLENGE          16
#define WLAN_EID_ERP_INFO           42
#endif
#define WLAN_EID_EX_SUPP_RATES      50
#define WLAN_EID_VENDOR_PRIVATE     221

#define WLAN_TIM_MAX_BITMAP_SIZE    251

#define WLAN_MAX_CHANNELS           255

//VIJAY ELEMENT ID FOR HT CAPABLITIES
#define WLAN_EID_HT_CAPAB           45
#define WLAN_EID_HT_OPER            61
//#define WLAN_EID_SECONDARY_CHANNEL_OFFSET 62 

//VIJAY ELEMENT ID FOR VHT CAPABLITIES
#define WLAN_EID_VHT_CAPAB           191
struct wlan_channel
{
   unsigned char number;                /** IEEE 802.11 channel number */
   unsigned int  frequency;             /** In MHz */
   unsigned int  flags;                 /** WLAN_CHANNEL_* flags */
   unsigned int  state_flags;           /** WLAN_CHANNEL_STATE_* */
   char          max_reg_power;         /** Max power allowed by the regulatory domain */
   char          max_power;             /** Max power allowed by the radio */
   char          min_power;             /** Min power allowed by the radio */
};

typedef struct wlan_channel   wlan_channel_t;

#define WLAN_CHANNEL_CW_DETECTED              0x0002            /* CW interference detected on channel */
#define WLAN_CHANNEL_ATHEROS_DYNAMIC_TURBO    0x0010            /* Atheros dynamic Turbo mode channel */
#define WLAN_CHANNEL_CCK                      0x0020            /* CCK channel */
#define WLAN_CHANNEL_OFDM                     0x0040            /* OFDM channel */
#define WLAN_CHANNEL_2GHZ                     0x0080            /* 2 GHz spectrum channel. */
#define WLAN_CHANNEL_5GHZ                     0x0100            /* 5 GHz spectrum channel. */
#define WLAN_CHANNEL_ONLY_PASSIVE             0x0200            /* Only passive scan allowd */
#define WLAN_CHANNEL_DYNAMIC_CCK_OFDM         0x0400            /* and dynamically selected CCK or OFDM channel */
#define WLAN_CHANNEL_ATHEROS_XTENDED_RANGE    0x0800            /* Extended range channel */
#define WLAN_CHANNEL_ATHEROS_TURBO_MODE       0x2000            /* Atheros Turbo mode channel */
#define WLAN_CHANNEL_ATHEROS_HALF_RATE        0x4000            /* Atheros half rate channel */
#define WLAN_CHANNEL_ATHEROS_QTR_RATE         0x8000            /* Atheros quarter rate channel */

#define WLAN_CHANNEL_STATE_RADAR              0x01
#define WLAN_CHANNEL_STATE_DFS_REQUIRED       0x02
#define WLAN_CHANNEL_STATE_4MS_LIMIT          0x04
#define WLAN_CHANNEL_STATE_DFS_CLEAR          0x08

#define WLAN_CHANNEL_802_11_A                 (WLAN_CHANNEL_5GHZ | WLAN_CHANNEL_OFDM)
#define WLAN_CHANNEL_802_11_B                 (WLAN_CHANNEL_2GHZ | WLAN_CHANNEL_CCK)
#define WLAN_CHANNEL_PURE_802_11_G            (WLAN_CHANNEL_2GHZ | WLAN_CHANNEL_OFDM)
#define WLAN_CHANNEL_802_11_G                 (WLAN_CHANNEL_2GHZ | WLAN_CHANNEL_OFDM)
#define WLAN_CHANNEL_ATHEROS_TURBO            (WLAN_CHANNEL_5GHZ | WLAN_CHANNEL_OFDM | WLAN_CHANNEL_ATHEROS_TURBO_MODE)
#define WLAN_CHANNEL_108_G                    (WLAN_CHANNEL_2GHZ | WLAN_CHANNEL_OFDM | WLAN_CHANNEL_ATHEROS_TURBO_MODE)
#define WLAN_CHANNEL_ATHEROS_X                (WLAN_CHANNEL_802_11_A | WLAN_CHANNEL_ATHEROS_XTENDED_RANGE)

/**
 * 802.11 PHY modes
 */

#define WLAN_PHY_MODE_AUTO             0                                        /** Auto select */
#define WLAN_PHY_MODE_802_11_A         1                                        /** 5 GHz OFDM */
#define WLAN_PHY_MODE_802_11_B         2                                        /** 2.4 GHz DSSS */
#define WLAN_PHY_MODE_802_11_G         3                                        /** 2.4 GHz CCK-OFDM */
#define WLAN_PHY_MODE_802_11_FH        4                                        /** 2.4 GHz FSSS */
#define WLAN_PHY_MODE_ATHEROS_TURBO    5                                        /** 5GHz Atheros Turbo mode */
#define WLAN_PHY_MODE_802_11_PURE_G    6                                        /** 2.4 GHz OFDM without backward compatibility */
#define WLAN_PHY_MODE_802_11_PSQ       7                                        /** 5 MHz 4.9 GHz US Public safety */
#define WLAN_PHY_MODE_802_11_PSH       8                                        /** 10 MHz 4.9 GHz US Public safety */
#define WLAN_PHY_MODE_802_11_PSF       9                                        /** 20 MHz 4.9 GHz US Public safety */

// VIJAY : added for N & AC support
#define WLAN_PHY_MODE_802_11_N      10
#define WLAN_PHY_MODE_802_11_AC     11
#define WLAN_PHY_MODE_802_11_BGN    12
#define WLAN_PHY_MODE_802_11_AN     13
#define WLAN_PHY_MODE_802_11_ANAC   14
/**
 * Although WLAN_PHY_MODE_802_11_PSQ/H/F has been added after WLAN_PHY_MODE_802_11_PURE_G,
 * the WLAN_PHY_MODE_COUNT has not been incremented because WLAN_PHY_MODE_802_11_PSQ/H/F use
 * WLAN_PHY_MODE_802_11_A internally.
 */

#define WLAN_PHY_MODE_COUNT    (WLAN_PHY_MODE_802_11_PURE_G + 1)

#define WLAN_RATE_MASK         0x7F
#define WLAN_RATE_BASE         0x80
#define WLAN_MAX_BIT_RATES     15

/**
 * 802.11 wireless multimedia extentions
 */

#define WLAN_WME_CLASS_BACKGROUND     0
#define WLAN_WME_CLASS_BEST_EFFORT    1
#define WLAN_WME_CLASS_VIDEO          2
#define WLAN_WME_CLASS_VOICE          3

#define WLAN_WME_MAX_CLASSES          4


/**
 * Frame field positions for Authentication frame Request/Response
 */

#define WLAN_AUTHENTICATION_ALGORITHM_POSITION        0
#define WLAN_AUTHENTICATION_ATSN_POSITION             (WLAN_AUTHENTICATION_ALGORITHM_POSITION + 2)
#define WLAN_AUTHENTICATION_STATUS_POSITION           (WLAN_AUTHENTICATION_ATSN_POSITION + 2)
#define WLAN_AUTHENTICATION_CHALLENGETEXT_POSITION    (WLAN_AUTHENTICATION_STATUS_POSITION + 2)

/**
 * Frame field positions of various fields in an association request
 */

#if 0
#define WLAN_ASSOCIATION_REQUEST_CAPABILITY_POSITION        0
#define WLAN_ASSOCIATION_REQUEST_LISTENINTERVAL_POSITION    (WLAN_ASSOCIATION_REQUEST_CAPABILITY_POSITION + 2)
#endif
#define WLAN_ASSOCIATION_REQUEST_SSID_POSITION              0                         //(WLAN_ASSOCIATION_REQUEST_LISTENINTERVAL_POSITION + 2)
#define WLAN_ASSOCIATION_REQUEST_SSID_START_POSITION        (WLAN_ASSOCIATION_REQUEST_SSID_POSITION + 2)
#define WLAN_ASSOCIATION_REQUEST_SUPPORTEDRATES_POSITION(ssid_length)          (WLAN_ASSOCIATION_REQUEST_SSID_START_POSITION + ssid_length)
#define WLAN_ASSOCIATION_REQUEST_SUPPORTEDRATES_START_POSITION(ssid_length)    (WLAN_ASSOCIATION_REQUEST_SUPPORTEDRATES_POSITION(ssid_length) + 2)

/**
 * Only for 802.11g i.e (pure 802.11g and 802.11bg mode)
 */

#define WLAN_ASSOCIATION_REQUEST_EX_SUPPORTEDRATES_POSITION(ssid_length, supp_rates_length) \
   (WLAN_ASSOCIATION_REQUEST_SUPPORTEDRATES_START_POSITION(ssid_length) + supp_rates_length)
#define WLAN_ASSOCIATION_REQUEST_EX_SUPPORTEDRATES_START_POSITION(ssid_length, supp_rates_length) \
   (WLAN_ASSOCIATION_REQUEST_EX_SUPPORTEDRATES_POSITION(ssid_length, supp_rates_length) + 2)


/**
 * Frame field positions of various fields in an association response
 */

#define WLAN_ASSOCIATION_RESPONSE_CAPABILITY_POSITION              0
#define WLAN_ASSOCIATION_RESPONSE_STATUS_POSITION                  (WLAN_ASSOCIATION_RESPONSE_CAPABILITY_POSITION + 2)
#define WLAN_ASSOCIATION_RESPONSE_AID_POSITION                     (WLAN_ASSOCIATION_RESPONSE_STATUS_POSITION + 2)
#define WLAN_ASSOCIATION_RESPONSE_SUPPORTEDRATES_POSITION          (WLAN_ASSOCIATION_RESPONSE_AID_POSITION + 2)
#define WLAN_ASSOCIATION_RESPONSE_SUPPORTEDRATES_START_POSITION    (WLAN_ASSOCIATION_RESPONSE_SUPPORTEDRATES_POSITION + 2)

/**
 * Only for 802.11g i.e (pure 802.11g and 802.11bg mode)
 */

#define WLAN_ASSOCIATION_RESPONSE_EX_SUPPORTEDRATES_POSITION(supported_rates_length)          (WLAN_ASSOCIATION_RESPONSE_SUPPORTEDRATES_START_POSITION + supported_rates_length)
#define WLAN_ASSOCIATION_RESPONSE_EX_SUPPORTEDRATES_START_POSITION(supported_rates_length)    (WLAN_ASSOCIATION_RESPONSE_EX_SUPPORTEDRATES_POSITION(supported_rates_length) + 2)

/**
 * For 802.11a and pure 802.11b mode
 */

#define WLAN_ASSOCIATION_RESPONSE_PAYLOAD_LENGTH(supported_rates_length)    (WLAN_ASSOCIATION_RESPONSE_SUPPORTEDRATES_START_POSITION + supported_rates_length)

/**
 * Only for 802.11g i.e (pure 802.11g and 802.11bg mode)
 */

#define WLAN_ASSOCIATION_RESPONSE_EX_PAYLOAD_LENGTH(supported_rates_length, ex_supported_rates_length)    (WLAN_ASSOCIATION_RESPONSE_EX_SUPPORTEDRATES_START_POSITION(supported_rates_length) + ex_supported_rates_length)

/**
 * Frame field positions of various fields in a de-authentication notification
 */

#define WLAN_DEAUTHENTICATION_NOTIFICATION_REASON_CODE_POSITION    0

/**
 * Frame field positions of various fields in a disassociation notification
 */

#define WLAN_DISASSOCIATION_NOTIFICATION_REASON_CODE_POSITION    0


/**
 * Frame field positions of various fields in a Probe request
 */

#define WLAN_PROBE_REQUEST_SSID_POSITION          0
#define WLAN_PROBE_REQUEST_SSID_START_POSITION    (WLAN_PROBE_REQUEST_SSID_POSITION + 2)
#define WLAN_PROBE_REQUEST_SUPPORTEDRATES_POSITION(ssid_length)          (WLAN_PROBE_REQUEST_SSID_START_POSITION + ssid_length)
#define WLAN_PROBE_REQUEST_SUPPORTEDRATES_START_POSITION(ssid_length)    (WLAN_PROBE_REQUEST_SUPPORTEDRATES_POSITION(ssid_length) + 2)

/**
 * Only for 802.11g i.e (pure 802.11g and 802.11bg mode)
 */

#define WLAN_PROBE_REQUEST_EX_SUPPORTEDRATES_POSITION(ssid_length, supported_rates_length)          (WLAN_PROBE_REQUEST_SUPPORTEDRATES_START_POSITION(ssid_length) + supported_rates_length)
#define WLAN_PROBE_REQUEST_EX_SUPPORTEDRATES_START_POSITION(ssid_length, supported_rates_length)    (WLAN_PROBE_REQUEST_EX_SUPPORTEDRATES_POSITION(ssid_length, supported_rates_length) + 2)

/**
 * Frame field positions of various fields in a Probe Response
 */

#define WLAN_PROBE_RESPONSE_TIMESTAMP_POSITION          0
#define WLAN_PROBE_RESPONSE_BEACON_INTERVAL_POSITION    (WLAN_PROBE_RESPONSE_TIMESTAMP + 8)
#define WLAN_PROBE_RESPONSE_CAPABILITY_POSITION         (WLAN_PROBE_RESPONSE_BEACON_INTERVAL + 2)
#define WLAN_PROBE_RESPONSE_SSID_POSITION               (WLAN_PROBE_RESPONSE_CAPABILITY_POSITION + 2)
#define WLAN_PROBE_RESPONSE_SSID_START_POSITION         (WLAN_PROBE_RESPONSE_SSID_POSITION + 2)
#define WLAN_PROBE_RESPONSE_SUPPORTEDRATES_POSITION(ssid_length)                       (WLAN_PROBE_RESPONSE_SSID_START_POSITION + ssid_length)
#define WLAN_PROBE_RESPONSE_SUPPORTEDRATES_START_POSITION(ssid_length)                 (WLAN_PROBE_RESPONSE_SUPPORTEDRATES_POSITION(ssid_length) + 2)

#define WLAN_PROBE_RESPONSE_DS_PARAM_POSITION(ssid_length, supp_rates_length)          (WLAN_PROBE_RESPONSE_SUPPORTEDRATES_START_POSITION(ssid_length) + supp_rates_length)
#define WLAN_PROBE_RESPONSE_DS_PARAM_START_POSITION(ssid_length, supp_rates_length)    (WLAN_PROBE_RESPONSE_DS_PARAM_POSITION(ssid_length, supp_rates_length) + 2)

/**
 * Only for 802.11g i.e (pure 802.11g and 802.11bg mode)
 */

#define WLAN_PROBE_RESPONSE_ERP_INFO_POSITION(ssid_length, supported_rates_length)                   (WLAN_PROBE_RESPONSE_DS_PARAM_START_POSITION(ssid_length, supported_rates_length) + 1)
#define WLAN_PROBE_RESPONSE_ERP_INFO_START_POSITION(ssid_length, supported_rates_length)             (WLAN_PROBE_RESPONSE_ERP_INFO_POSITION(ssid_length, supported_rates_length) + 2)
#define WLAN_PROBE_RESPONSE_EX_SUPPORTEDRATES_POSITION(ssid_length, supported_rates_length)          (WLAN_PROBE_RESPONSE_ERP_INFO_START_POSITION(ssid_length, supported_rates_length) + 1)
#define WLAN_PROBE_RESPONSE_EX_SUPPORTEDRATES_START_POSITION(ssid_length, supported_rates_length)    (WLAN_PROBE_RESPONSE_EX_SUPPORTEDRATES_POSITION(ssid_length, supported_rates_length) + 2)



/**
 * Frame field positions of various fields in a Beacon
 */

#define WLAN_BEACON_TIMESTAMP_POSITION          0
#define WLAN_BEACON_BEACON_INTERVAL_POSITION    (WLAN_BEACON_TIMESTAMP_POSITION + 8)
#define WLAN_BEACON_CAPABILITY_POSITION         (WLAN_BEACON_BEACON_INTERVAL_POSITION + 2)
#define WLAN_BEACON_SSID_POSITION               (WLAN_BEACON_CAPABILITY_POSITION + 2)
#define WLAN_BEACON_SSID_START_POSITION         (WLAN_BEACON_SSID_POSITION + 2)
#define WLAN_BEACON_SUPPORTEDRATES_POSITION(ssid_length)                       (WLAN_BEACON_SSID_START_POSITION + ssid_length)
#define WLAN_BEACON_SUPPORTEDRATES_START_POSITION(ssid_length)                 (WLAN_BEACON_SUPPORTEDRATES_POSITION(ssid_length) + 2)

#define WLAN_BEACON_DS_PARAM_POSITION(ssid_length, supp_rates_length)          (WLAN_BEACON_SUPPORTEDRATES_START_POSITION(ssid_length) + supp_rates_length)
#define WLAN_BEACON_DS_PARAM_START_POSITION(ssid_length, supp_rates_length)    (WLAN_BEACON_DS_PARAM_POSITION(ssid_length, supp_rates_length) + 2)

/**
 * Only for 802.11g i.e (pure 802.11g and 802.11bg mode)
 */

#define WLAN_BEACON_ERP_INFO_POSITION(ssid_length, supported_rates_length)                   (WLAN_BEACON_DS_PARAM_START_POSITION(ssid_length, supported_rates_length) + 1)
#define WLAN_BEACON_ERP_INFO_START_POSITION(ssid_length, supported_rates_length)             (WLAN_BEACON_ERP_INFO_POSITION(ssid_length, supported_rates_length) + 2)
#define WLAN_BEACON_EX_SUPPORTEDRATES_POSITION(ssid_length, supported_rates_length)          (WLAN_BEACON_ERP_INFO_START_POSITION(ssid_length, supported_rates_length) + 1)
#define WLAN_BEACON_EX_SUPPORTEDRATES_START_POSITION(ssid_length, supported_rates_length)    (WLAN_BEACON_EX_SUPPORTEDRATES_POSITION(ssid_length, supported_rates_length) + 2)


/**
 * ERP information element flags
 */

#define WLAN_ERP_INFO_NON_ERP_PRESENT         1
#define WLAN_ERP_INFO_USE_PROTECTION          2
#define WLAN_ERP_INFO_BARKER_PREAMBLE_MODE    4

/**
 * Minimum sizes of various 802.11 frames
 */

#define WLAN_DISASSOC_MIN_SIZE          2
#define WLAN_ASSOC_RESP_MIN_SIZE        16
#define WLAN_REASSOC_RESP_MIN_SIZE      16
#define WLAN_PROBE_RESP_MIN_SIZE        27
#define WLAN_AUTHENTICATION_MIN_SIZE    6
#define WLAN_DEAUTH_MIN_SIZE            2
#define WLAN_ESSID_MAX_SIZE             32

/**
 * Monitor mode flags
 */

#define WLAN_MONITOR_MODE_FLAG_DATA          1
#define WLAN_MONITOR_MODE_FLAG_ASSOC_REQ     2
#define WLAN_MONITOR_MODE_FLAG_ASSOC_RESP    4
#define WLAN_MONITOR_MODE_FLAG_PROBE_REQ     8
#define WLAN_MONITOR_MODE_FLAG_PROBE_RESP    16
#define WLAN_MONITOR_MODE_FLAG_BEACON        32
#define WLAN_MONITOR_MODE_FLAG_AUTH          64
#define WLAN_MONITOR_MODE_FLAG_DEAUTH        128
#define WLAN_MONITOR_MODE_FLAG_DISASSOC      256
#define WLAN_MONITOR_MODE_FLAG_ENCR_DATA     512
#define WLAN_MONITOR_MODE_FLAG_FROM_DS       1024
#define WLAN_MONITOR_MODE_FLAG_TO_DS         2048
#define WLAN_MONITOR_MODE_FLAG_CUSTOM        4096

/**
 * Slot time types
 */

#define WLAN_PREAMBLE_TYPE_LONG      0
#define WLAN_PREAMBLE_TYPE_SHORT     1

#define WLAN_SLOT_TIME_TYPE_LONG     0
#define WLAN_SLOT_TIME_TYPE_SHORT    1

#endif /*__TORNA_WLAN_H__*/
