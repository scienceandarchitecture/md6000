/********************************************************************************
* MeshDynamics
* --------------
* File     : torna_log.h
* Comments : Torna Generic Logging routines
* Created  : 5/12/2004
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  0  |5/12/2004 | Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

#ifndef __TORNA_LOG_H__
#define __TORNA_LOG_H__

/**
 * To enable logging _TORNA_ENABLE_LOGGER_ needs to be defined.
 * The module being compiled must declare storage for torna_debug_mask
 * and initialize it.
 */

#ifdef _TORNA_ENABLE_LOGGER_

#ifndef _TORNA_DEBUG_MASK_VALUE_
#define _TORNA_DEBUG_MASK_VALUE_    0
#endif

#define TORNA_DEBUG_MASK_DECLARE(val)    int torna_debug_mask = val
extern int torna_debug_mask;
#define TORNA_LOG(type, args ...)       \
   do {                                 \
      if (torna_debug_mask & type) {    \
         printk(type ## _LEVEL args); } \
   } while (0)
#else
#define TORNA_DEBUG_MASK_DECLARE(val)
#define TORNA_LOG(type, args ...)
#endif

#endif /*__TORNA_LOG_H__*/
