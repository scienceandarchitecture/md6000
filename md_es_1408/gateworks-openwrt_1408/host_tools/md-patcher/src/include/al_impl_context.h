
/********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : al_impl_context.h
 * Comments : Torna MeshAP al_impl_context.h
 * Created  : 5/21/2004
 * Author   : Sriram Dayanandan
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * -----------------------------------------------------------------------------
 * | No  |Date      |  Comment                                        | Author |
 * -----------------------------------------------------------------------------
 * |  0  |5/21/2004 | Created.                                        | Sriram |
 * -----------------------------------------------------------------------------
 ********************************************************************************/

#ifndef __AL_IMPL_CONTEXT_H__
#define __AL_IMPL_CONTEXT_H__

/**
 * THIS FILE IS JUST A PLACE HOLDER FILE.
 */

struct al_impl_context {
	int		dummy;	/** NEVER USED */
};

typedef struct al_impl_context al_impl_context_t;

#endif /*__AL_IMPL_CONTEXT_H__*/
