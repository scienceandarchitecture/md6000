/********************************************************************************
* MeshDynamics
* --------------
* File     : _codec.c
* Comments : Encoder/Decoder routines
* Created  : 9/26/2004
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  8  |8/22/2008 | Changes for FIPS                                |Abhijit |
* -----------------------------------------------------------------------------
* |  7  |11/16/2006| Buffer optimization (Gen 2K) changes            | Sriram |
* -----------------------------------------------------------------------------
* |  6  |4/11/2005 | Misc changes                                    | Sriram |
* -----------------------------------------------------------------------------
* |  5  |12/14/2004| Used LE32 conversion macros                     | Sriram |
* -----------------------------------------------------------------------------
* |  4  |10/29/2004| bug fixed - AES KEY SIZE                        | Anand  |
* -----------------------------------------------------------------------------
* |  3  |10/29/2004| Bug Fixes                                       | Anand  |
* -----------------------------------------------------------------------------
* |  2  |10/1/2004 | Added _encode_key /_decode_key		          | Prachiti|
* -----------------------------------------------------------------------------
* |  1  |9/30/2004 | AES implementation _encrypt/_decrypt            | Prachiti|
* -----------------------------------------------------------------------------
* |  0  |9/26/2004 | Created.                                        | Sriram  |
* -----------------------------------------------------------------------------
********************************************************************************/

#ifdef __KERNEL__
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/string.h>
#else
#include <stdio.h>
#include <strings.h>
#include <string.h>
#endif
#include "codec_sys.h"
#include "codec.h"
#include "aes.h"
#include "aes_wrap.h"

static const char _uu_encode_symbols[64] =
{
   '`', '!', '"', '#', '$',  '%', '&', '\'',
   '(', ')', '*', '+', ',',  '-', '.', '/',
   '0', '1', '2', '3', '4',  '5', '6', '7',
   '8', '9', ':', ';', '<',  '=', '>', '?',
   '@', 'A', 'B', 'C', 'D',  'E', 'F', 'G',
   'H', 'I', 'J', 'K', 'L',  'M', 'N', 'O',
   'P', 'Q', 'R', 'S', 'T',  'U', 'V', 'W',
   'X', 'Y', 'Z', '[', '\\', ']', '^', '_'
};

#define _UU_DENORMALIZE(c)          ((c) == '`' ? 0 : (c) - 32)
#define _ROUND_TO_MULTIPLE(n, m)    (((n + m - 1) / m) * m)

/*
 *   Later add the symbols in Make file
 */

static aes_ctx s_aes_context;
static long    holdrand = 0;

#define MAX_BUF_SIZE    1024


static void _uuencode_helper(const unsigned char *three_bytes, char *four_bytes)
{
   unsigned char left_over;

   four_bytes[0] = _uu_encode_symbols[(three_bytes[0] >> 2)];
   left_over     = three_bytes[0] & 3;
   four_bytes[1] = _uu_encode_symbols[((left_over << 4) + (three_bytes[1] >> 4))];
   left_over     = three_bytes[1] & 15;
   four_bytes[2] = _uu_encode_symbols[((left_over << 2) + (three_bytes[2] >> 6))];
   four_bytes[3] = _uu_encode_symbols[(three_bytes[2] & 63)];
}


static void _uudecode_helper(const char *four_bytes, unsigned char *three_bytes)
{
#define _GET_CHAR(index)    _UU_DENORMALIZE(four_bytes[index])

   unsigned char c1, c2;

   c1             = _GET_CHAR(0);
   c2             = _GET_CHAR(1);
   three_bytes[0] = (c1 << 2) + (c2 >> 4);
   c1             = c2 & 0xF;
   c2             = _GET_CHAR(2);
   three_bytes[1] = (c1 << 4) + (c2 >> 2);
   c1             = c2 & 0x3;
   c2             = _GET_CHAR(3);
   three_bytes[2] = (c1 << 6) + c2;

#undef _GET_CHAR
}


/**
 * uuencode takes in a stream of bytes (text and binary) and converts
 * it into a textual string that can be inserted into text files.
 * It does that by converting every 3 bytes (24 bits) into 4 bytes (32 bits)
 * padding 0s when necessary.
 */
int _uuencode(const char *name, const char *permissions, const unsigned char *input, int input_length, unsigned char **output)
{
   int                 rounded_length;
   int                 output_length;
   char                *output_buffer;
   char                *four_bytes;
   const unsigned char *three_bytes;
   const unsigned char *in;
   char                *out;
   int                 i;
   int                 j;
   int                 output_count;
   char                *output_line_begin;
   const unsigned char *input_line_begin;
   char                header[256];

   rounded_length = _ROUND_TO_MULTIPLE(input_length, 3);
   output_length  = ((rounded_length * 4) / 3) + 1;

   /**
    * For every 60 characters, we need to output a M and a \n into the
    * output.
    */

   output_length += ((output_length + 60 - 1) / 60) * 2;

   /**
    * We also have to include the length of the uuencode header and trailor
    */

   sprintf(header, "begin %s %s\n", permissions, name);

   output_length += strlen(header);
   output_length += 1 /* ` */ + 1 /* \n */ + 3 /* end */ + 1 /* \n */;

   output_buffer = (char *)codec_alloc(output_length);

   memset(output_buffer, 0, output_length);

   in          = input;
   three_bytes = input;
   out         = output_buffer;

   input_line_begin = input;
   i            = 0;
   j            = 0;
   output_count = 0;

   strcpy(out, header);
   out += strlen(header);
   output_line_begin = out;

   /** Skip for inserting length later */
   out++;

   while (i++ < input_length)
   {
      if (input_length - i < 10)
      {
         i = i;
      }
      ++in;
      if (++j == 3)
      {
         four_bytes    = out;
         out          += 4;
         output_count += 4;
         _uuencode_helper(three_bytes, four_bytes);
         three_bytes = in;
         j           = 0;
         if (output_count == 60)
         {
            *out++             = '\n';
            *output_line_begin = _uu_encode_symbols[in - input_line_begin];
            output_line_begin  = out;
            input_line_begin   = in;
            out++;
            output_count = 0;
         }
      }
   }

   if (--i < rounded_length)
   {
      unsigned char padded_three_bytes[3];
      memset(padded_three_bytes, 0, sizeof(padded_three_bytes));
      memcpy(padded_three_bytes, three_bytes, j);
      four_bytes    = out;
      out          += 4;
      output_count += 4;
      _uuencode_helper(three_bytes, four_bytes);
   }

   if (output_count != 0)
   {
      *output_line_begin = _uu_encode_symbols[in - input_line_begin];
      *out++             = '\n';
   }

   strcpy(out, "`\nend\n");

   *output = output_buffer;

   return output_length;
}


/**
 * uudecode takes in a text string. Each line in the text string is atmost 62 bytes
 * long including the new line character. The first character specifies the length
 * of the line.
 */
int _uudecode(const char *name, const char *input, unsigned char **output)
{
   int           output_length;
   unsigned char *output_buffer;
   const char    *in;
   const char    *four_bytes;
   const char    *p;
   char          *end;
   unsigned char *three_bytes;
   unsigned char *out;
   int           l;
   int           i;

   /**
    * Find the file name mentioned and start decoding the stream
    */

   in = input;

   end = NULL;

   end = strstr(in, "end");

   if (end == 0)
   {
      return UUDECODE_ERROR;
   }

   while (1)
   {
      p = strstr(in, "begin ");
      if (p != NULL)
      {
         p += 6;                /* length of begin and space */
         /* skip over permissions */
         while (*p == ' ' || *p == '\t')
         {
            p++;
         }
         while (*p != ' ' && *p != '\t')
         {
            p++;
         }
         while (*p == ' ' || *p == '\t')
         {
            p++;
         }
         if (!strncmp(p, name, strlen(name)))
         {
            p += strlen(name);
            if (*p == '\n')
            {
               p++;
            }
            break;
         }
      }
      else
      {
         return UUDECODE_ERROR;
      }
   }

   in            = p;
   output_length = 0;

   while (1)
   {
      l = _UU_DENORMALIZE(*in);

      if (l == 0)
      {
         break;
      }
      output_length += l;
      in            += 1 + (_ROUND_TO_MULTIPLE(l, 3) * 4 / 3) + 1;
   }

   output_buffer = (unsigned char *)codec_alloc(output_length + 2);
   memset(output_buffer, 0, output_length);

   out         = output_buffer;
   in          = p;
   three_bytes = out;

   while (*in)
   {
      l = _UU_DENORMALIZE(*in);
      if (l == 0)
      {
         break;
      }
      four_bytes = in + 1;
      for (i = 0; i < l; i += 3, three_bytes += 3, four_bytes += 4)
      {
         _uudecode_helper(four_bytes, three_bytes);
      }

      in += 1 + (_ROUND_TO_MULTIPLE(l, 3) * 4 / 3) + 1;
   }

   *output = output_buffer;

   return output_length;
}


int _aesencrypt(const unsigned char *input, int input_length, const unsigned char *key, int key_length, unsigned char *output)
{
   unsigned char cipher_text[AES_BLOCK_SIZE];
   unsigned char plain_text[AES_BLOCK_SIZE];

   const unsigned char *temp;
   int                 i, len;
   int                 output_length;


   unsigned char padded_key[MAX_BUF_SIZE];


   if (key == NULL)
   {
      return KEY_NOT_FOUND;
   }

   memset(padded_key, 0, sizeof(padded_key));
   memcpy(padded_key, key, AES_KEY_SIZE);


   if (aes_set_key(&s_aes_context, padded_key, AES_KEY_SIZE, aes_both) == aes_bad)
   {
      return AES_KEY_ERROR;
   }

   temp = input;
   len  = 0;


   if ((input_length % AES_BLOCK_SIZE) > 0)
   {
      output_length = (int)input_length / AES_BLOCK_SIZE;           //AL_AES_KEY_SIZE * 8;
      output_length++;
      output_length *= AES_BLOCK_SIZE;
   }
   else
   {
      output_length = input_length;
   }

   memset(output, 0, output_length);

   for (i = 0; i < (input_length / AES_BLOCK_SIZE); i++)
   {
      memset(cipher_text, 0, sizeof(cipher_text));
      memset(plain_text, 0, sizeof(plain_text));

      memcpy(plain_text, temp + len, sizeof(plain_text));

      if (aes_encrypt(&s_aes_context, plain_text, cipher_text) == aes_bad)
      {
         return AES_ENCR_ERROR;
      }

      memcpy(output + len, cipher_text, AES_BLOCK_SIZE);

      len = len + AES_BLOCK_SIZE;
   }
   if (input_length % AES_BLOCK_SIZE)
   {
      memset(cipher_text, 0, sizeof(cipher_text));
      memset(plain_text, 0, sizeof(plain_text));

      memcpy(plain_text, temp + len, input_length % AES_BLOCK_SIZE);

      if (aes_encrypt(&s_aes_context, plain_text, cipher_text) == aes_bad)
      {
         return AES_ENCR_ERROR;
      }


      memcpy(output + len, cipher_text, AES_BLOCK_SIZE);
   }

   return output_length;
}


int _aesdecrypt(const unsigned char *input, int input_length, const unsigned char *key, int key_length, unsigned char *output)
{
   unsigned char       ct[AES_BLOCK_SIZE];
   const unsigned char *temp;
   int                 i;
   int                 output_length;
   int                 len;
   unsigned char       pt[AES_BLOCK_SIZE];
   unsigned char       padded_key[MAX_BUF_SIZE];


   if (key == NULL)
   {
      return KEY_NOT_FOUND;
   }

   memset(padded_key, 0, sizeof(padded_key));
   memcpy(padded_key, key, AES_KEY_SIZE);


   if (aes_set_key(&s_aes_context, padded_key, AES_KEY_SIZE, aes_both) == aes_bad)
   {
      return AES_KEY_ERROR;
   }

   temp          = input;
   len           = 0;
   output_length = input_length;


   memset(output, 0, output_length);


   for (i = 0; i < (input_length / AES_BLOCK_SIZE); i++)
   {
      memset(pt, 0, sizeof(pt));
      memset(ct, 0, sizeof(ct));

      memcpy(ct, temp + len, sizeof(ct));

      if (aes_decrypt(&s_aes_context, ct, pt) == aes_bad)
      {
         return AES_ENCR_ERROR;
      }

      memcpy(output + len, pt, AES_BLOCK_SIZE);

      len = len + AES_BLOCK_SIZE;
   }
   if (input_length % AES_BLOCK_SIZE)
   {
      memset(pt, 0, sizeof(pt));
      memset(ct, 0, sizeof(ct));

      memcpy(ct, temp + len, AES_BLOCK_SIZE);

      if (aes_decrypt(&s_aes_context, ct, pt) == aes_bad)
      {
         return AES_ENCR_ERROR;
      }

      memcpy(output + len, pt, input_length % AES_BLOCK_SIZE);
   }

   return output_length;
}


#define _RAND_MAX    0xffff

static long codec_rand(void)
{
   return(((holdrand = holdrand * 214013L + 2531011L) >> 16) & _RAND_MAX);
}


static int randomInt(int min, int max)
{
   long scaled_down;
   long r;

   r           = codec_rand();
   scaled_down = r / (max - min);

   return(scaled_down + min);
}


int _encode_key(unsigned char *text_key, int text_key_len, unsigned char *mac_address, unsigned char *output)
{
   unsigned char uuencode_name[] = "mesh_imcp_key";
   unsigned char uuencode_perm[] = "777";

   unsigned char added_address[256];
   unsigned char random_address[MAC_ADDR_LEN];

   unsigned char *uuencoded_key;
   unsigned char *key_for_aes;

   unsigned char random_plus_aes_encrypted_key[1024];

   unsigned char out_aes_encrypted_key[256];
   int           encrypt_len = 0;
   int           i, out_len = 0;


   memset(added_address, 0, sizeof(added_address));

   /*
    *	get random address and add  it to mac_address
    *	Added_address = mac_address + Random
    *	aes_key = 0000000000 + Added_address + 0000000000
    */


   key_for_aes = added_address;

   key_for_aes = key_for_aes + 5;

   for (i = 0; i < MAC_ADDR_LEN; i++)
   {
      random_address[i] = randomInt(0, 127);
      key_for_aes[i]    = mac_address[i] + random_address[i];
   }

   /*
    *  text_key + aes_key ------------------> aes_encrypted_form_of_text_key
    *                        AES
    */

   encrypt_len = encrypt_helper(text_key, text_key_len, added_address, AES_KEY_SIZE, out_aes_encrypted_key, AES_ENCRYPTION_DECRYPTION_METHOD_AES_ECB);

   /*
    *  Random+aes_encrypted_form_of_text_key
    *
    */

   memset(random_plus_aes_encrypted_key, 0, 1024);
   memcpy(random_plus_aes_encrypted_key, random_address, MAC_ADDR_LEN);

   memcpy(random_plus_aes_encrypted_key + MAC_ADDR_LEN, out_aes_encrypted_key, encrypt_len);

   /*
    *  Random+aes_encrypted_form_of_text_key --------------> uuencoded_key
    *											UUENCODE
    */

   out_len = _uuencode(uuencode_name, uuencode_perm, random_plus_aes_encrypted_key, encrypt_len + MAC_ADDR_LEN, &uuencoded_key);

   memcpy(output, uuencoded_key, out_len);

   codec_free(uuencoded_key);

   return out_len;
}


int _decode_key(unsigned char *buffer, unsigned char *mac_address, unsigned char *output)
{
   unsigned char random[MAC_ADDR_LEN];
   unsigned char *key_for_aes;
   unsigned char added_address[256];
   unsigned char *out_uudecoded_key;
   unsigned char out_aes_decrypted_key[256];
   int           ret = 0;
   int           i, len;
   unsigned char uudecode_name[] = "mesh_imcp_key";

   /*
    * UUENCODED ascii string --------------> Random+aes_encypted_form_of_cybernetics!
    *                            UUDECODE
    */

   len = _uudecode(uudecode_name, buffer, &out_uudecoded_key);

   if (len <= 0)
   {
      return len;
   }

   memset(&random, 0, sizeof(random));
   memcpy(&random, out_uudecoded_key, MAC_ADDR_LEN);

   /*
    *	aes_encrypted_form_of_text_key + aes_key -------------> text_key
    *                                              AES DECRYPT
    */

   memset(added_address, 0, sizeof(added_address));

   /*
    *	get random address and add  it to mac_address
    *	Added_address = mac_address + Random
    *	aes_key = 0000000000 + Added_address + 0000000000
    */


   key_for_aes = added_address;

   key_for_aes = key_for_aes + 5;

   for (i = 0; i < MAC_ADDR_LEN; i++)
   {
      key_for_aes[i] = mac_address[i] + random[i];
   }

   ret = decrypt_helper(out_uudecoded_key + MAC_ADDR_LEN, len - MAC_ADDR_LEN, added_address, AES_KEY_SIZE, out_aes_decrypted_key, AES_ENCRYPTION_DECRYPTION_METHOD_AES_ECB);
   if (ret > 0)
   {
      memcpy(output, out_aes_decrypted_key, ret);
   }

   codec_free(out_uudecoded_key);


   return ret;
}


int encrypt_helper(unsigned char *input, int input_length, const unsigned char *key, int key_len, unsigned char *output, int enc_dec_method)
{
   int           actual_length;
   int           ret;
   unsigned char *buffer;
   unsigned char key_buf[AES_KEY_SIZE];
   int           temp;

   if ((input == NULL) || (input_length <= 0))
   {
      return NULL_ARGUMENT;
   }

   memset(key_buf, 0, AES_KEY_SIZE);
   memcpy(key_buf, key, key_len);

   actual_length = ENCR_HEADER_LEN + sizeof(int) + input_length;
   if (enc_dec_method == AES_ENCRYPTION_DECRYPTION_METHOD_AES_KEY_WRAP)
   {
      actual_length += ((actual_length % 8) != 0) ? (8 - (actual_length % 8)) : 0;
   }

   if (actual_length <= 2048)
   {
      buffer = codec_get_gen_buffer();
   }
   else
   {
      buffer = codec_alloc(actual_length);
   }

   memset(buffer, 0, actual_length);

   memset(output, 0, actual_length);

   memcpy(buffer, ENCR_HEADER, ENCR_HEADER_LEN);

   temp = CODEC_SYS_CONVERT_TO_LE32(input_length);
   memcpy(buffer + ENCR_HEADER_LEN, &temp, sizeof(int));

   memcpy(buffer + ENCR_HEADER_LEN + sizeof(int), input, input_length);

   if (enc_dec_method == AES_ENCRYPTION_DECRYPTION_METHOD_AES_KEY_WRAP)
   {
      aes_wrap(key_buf, actual_length / 8, buffer, output);
      ret = actual_length + 8;
   }
   else
   {
      ret = _aesencrypt((unsigned char *)buffer, actual_length, key_buf, AES_KEY_SIZE, output);
   }

   if (ret <= 0)
   {
      /* error occured */
      if (actual_length <= 2048)
      {
         codec_release_gen_buffer(buffer);
      }
      else
      {
         codec_free(buffer);
      }
      return ret;
   }

   /* TODO check error and return accordingly */

   if (actual_length <= 2048)
   {
      codec_release_gen_buffer(buffer);
   }
   else
   {
      codec_free(buffer);
   }

   return ret;
}


int decrypt_helper(unsigned char *input, int input_length, const unsigned char *key, int key_len, unsigned char *output, int enc_dec_method)
{
   int           data_length;
   unsigned char *out;
   int           ret;
   unsigned char key_buf[AES_KEY_SIZE];

   if ((input == NULL) || (input_length <= 0))
   {
      return NULL_ARGUMENT;
   }

   memset(key_buf, 0, AES_KEY_SIZE);
   memcpy(key_buf, key, key_len);


   if (input_length <= 2048)
   {
      out = codec_get_gen_buffer();
   }
   else
   {
      out = codec_alloc(input_length);
   }

   memset(output, 0, input_length);
   memset(out, 0, input_length);

   /* check error and return accordingly */
   if (enc_dec_method == AES_ENCRYPTION_DECRYPTION_METHOD_AES_KEY_WRAP)
   {
      if (aes_unwrap(key_buf, (input_length - 8) / 8, input, out) != 0)
      {
         output = NULL;
         if (input_length <= 2048)
         {
            codec_release_gen_buffer(out);
         }
         else
         {
            codec_free(out);
         }

         return AES_WRAP_ERROR;
      }

      ret = input_length - 8;
   }
   else
   {
      ret = _aesdecrypt((unsigned char *)input, input_length, key_buf, AES_KEY_SIZE, out);
   }

   if (ret <= 0)
   {
      output = NULL;
      if (input_length <= 2048)
      {
         codec_release_gen_buffer(out);
      }
      else
      {
         codec_free(out);
      }
      return NULL_ARGUMENT;
   }

   if (memcmp(out, ENCR_HEADER, ENCR_HEADER_LEN) == 0)
   {
      /* found correct header */


      memcpy(&data_length, out + ENCR_HEADER_LEN, sizeof(int));
      data_length = CODEC_SYS_CONVERT_FROM_LE32(data_length);


      if (data_length > 0)
      {
         memcpy(output, out + ENCR_HEADER_LEN + sizeof(int), data_length);
      }
      else
      {
         output = NULL;
         if (input_length <= 2048)
         {
            codec_release_gen_buffer(out);
         }
         else
         {
            codec_free(out);
         }
         return INVALID_KEY_ERROR;
      }
      if (input_length <= 2048)
      {
         codec_release_gen_buffer(out);
      }
      else
      {
         codec_free(out);
      }
      return data_length;
   }

   /* TODO  return INVALID_HEADER ERROR CODE */

   output = NULL;

   if (input_length <= 2048)
   {
      codec_release_gen_buffer(out);
   }
   else
   {
      codec_free(out);
   }

   return INVALID_KEY_ERROR;
}
