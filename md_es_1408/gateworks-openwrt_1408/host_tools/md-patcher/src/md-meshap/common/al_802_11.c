/********************************************************************************
* MeshDynamics
* --------------
* File     : al_802_11.c
* Comments : Common 802.11 helper routines
* Created  : 3/3/2005
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  7  |02/12/2009| Changes for action frame                        |Abhijit |
* -----------------------------------------------------------------------------
* |  6  |8/22/2008 | Changes for FIPS                                |Abhijit |
* -----------------------------------------------------------------------------
* |  5  |7/25/2007 | Check NULL in cleanup                           | Sriram |
* -----------------------------------------------------------------------------
* |  4  |2/23/2007 | Changes for Effistream support                  |Prachiti|
* -----------------------------------------------------------------------------
* |  3  |11/14/2006| Changes for heap debug support                  | Sriram |
* -----------------------------------------------------------------------------
* |  2  |11/8/2006 | Changes for compression support                 | Sriram |
* -----------------------------------------------------------------------------
* |  1  |4/14/2005 | Misc changes                                    | Sriram |
* -----------------------------------------------------------------------------
* |  0  |3/3/2005  | Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

#include <linux/string.h>
#include "al.h"
#include "al_802_11.h"

static AL_INLINE unsigned char *_parse_akm_suite(AL_CONTEXT_PARAM_DECL unsigned char *p, al_802_11_akm_t *akm)
{
   memcpy(akm->oui, p, 3);
   p        += 3;
   akm->type = *p;
   p++;
   return p;
}


static AL_INLINE unsigned char *_parse_cipher_suite(AL_CONTEXT_PARAM_DECL unsigned char *p, al_802_11_rsn_cipher_t *cipher)
{
   memcpy(cipher->oui, p, 3);
   p           += 3;
   cipher->type = *p;
   p++;
   return p;
}


static AL_INLINE unsigned char *_parse_pmkid(AL_CONTEXT_PARAM_DECL unsigned char *p, al_802_11_pmkid_t *pmkid)
{
   memcpy(p, pmkid->bytes, AL_802_11_PMKID_LENGTH);
   p += AL_802_11_PMKID_LENGTH;
   return p;
}


/**
 * Assumes rsn_ie_out->mode is set
 */
static AL_INLINE int _parse_rsn_ie(AL_CONTEXT_PARAM_DECL al_802_11_information_element_t *ie_data, al_802_11_rsn_ie_t *rsn_ie_out)
{
   unsigned char  *p, *end;
   unsigned short s;
   int            i;

#define check_if_end_of_data(p)    (p >= end)
   p   = ie_data->data;
   end = p + ie_data->length;

   /** Version */
   memcpy(&s, p, 2);
   p += 2;
   rsn_ie_out->version = al_le16_to_cpu(s);

   /** Group Cipher Suite */
   p = _parse_cipher_suite(p, &rsn_ie_out->group_suite);
   if (check_if_end_of_data(p))
   {
      goto end_of_data;
   }
   switch (rsn_ie_out->group_suite.type)
   {
   case AL_802_11_RSN_CIPHER_WEP_40:
      rsn_ie_out->group_cipher_mode = AL_802_11_SECURITY_INFO_RSN_CIPHER_WEP;
      break;

   case AL_802_11_RSN_CIPHER_TKIP:
      rsn_ie_out->group_cipher_mode = AL_802_11_SECURITY_INFO_RSN_CIPHER_TKIP;
      break;

   case AL_802_11_RSN_CIPHER_CCMP:
      rsn_ie_out->group_cipher_mode = AL_802_11_SECURITY_INFO_RSN_CIPHER_CCMP;
      break;

   case AL_802_11_RSN_CIPHER_WEP_104:
      rsn_ie_out->group_cipher_mode = AL_802_11_SECURITY_INFO_RSN_CIPHER_WEP;
      break;
   }

   /** Pairwise Cipher count */
   memcpy(&s, p, 2);
   p += 2;
   rsn_ie_out->pairwise_count = al_le16_to_cpu(s);

   /** Pairwise Ciphers */
   rsn_ie_out->pairwise_suites = (al_802_11_rsn_cipher_t *)al_heap_alloc(AL_CONTEXT sizeof(al_802_11_rsn_cipher_t) * rsn_ie_out->pairwise_count AL_HEAP_DEBUG_PARAM);
   for (i = 0; i < rsn_ie_out->pairwise_count; i++)
   {
      p = _parse_cipher_suite(AL_CONTEXT p, &rsn_ie_out->pairwise_suites[i]);
      if (check_if_end_of_data(p))
      {
         goto end_of_data;
      }
      switch (rsn_ie_out->pairwise_suites[i].type)
      {
      case AL_802_11_RSN_CIPHER_WEP_40:
         rsn_ie_out->cipher_modes |= AL_802_11_SECURITY_INFO_RSN_CIPHER_WEP;
         break;

      case AL_802_11_RSN_CIPHER_TKIP:
         rsn_ie_out->cipher_modes |= AL_802_11_SECURITY_INFO_RSN_CIPHER_TKIP;
         break;

      case AL_802_11_RSN_CIPHER_CCMP:
         rsn_ie_out->cipher_modes |= AL_802_11_SECURITY_INFO_RSN_CIPHER_CCMP;
         break;

      case AL_802_11_RSN_CIPHER_WEP_104:
         rsn_ie_out->cipher_modes |= AL_802_11_SECURITY_INFO_RSN_CIPHER_WEP;
         break;
      }
   }

   /** AKM count */
   memcpy(&s, p, 2);
   p += 2;
   rsn_ie_out->akm_count = al_le16_to_cpu(s);
   if (check_if_end_of_data(p))
   {
      goto end_of_data;
   }

   /** AKM Suites */
   rsn_ie_out->akm_suites = (al_802_11_akm_t *)al_heap_alloc(AL_CONTEXT sizeof(al_802_11_akm_t) * rsn_ie_out->akm_count AL_HEAP_DEBUG_PARAM);
   for (i = 0; i < rsn_ie_out->akm_count; i++)
   {
      p = _parse_akm_suite(AL_CONTEXT p, &rsn_ie_out->akm_suites[i]);
      if (check_if_end_of_data(p))
      {
         goto end_of_data;
      }

      switch (rsn_ie_out->akm_suites[i].type)
      {
      case AL_802_11_RSN_AUTH_1X:
         rsn_ie_out->auth_modes |= AL_802_11_SECURITY_INFO_RSN_AUTH_1X;
         break;

      case AL_802_11_RSN_PSK:
         rsn_ie_out->auth_modes |= AL_802_11_SECURITY_INFO_RSN_AUTH_PSK;
         break;
      }
   }

   if (rsn_ie_out->mode != AL_802_11_SECURITY_INFO_RSN_MODE_WPA)
   {
      /** RSN Capabilities */
      memcpy(&s, p, 2);
      p += 2;
      if (check_if_end_of_data(p))
      {
         goto end_of_data;
      }
      rsn_ie_out->rsn_capabilities = al_le16_to_cpu(s);

      /** PMKID count */
      memcpy(&s, p, 2);
      p += 2;
      if (check_if_end_of_data(p))
      {
         goto end_of_data;
      }
      rsn_ie_out->pmkid_count = al_le16_to_cpu(s);

      /** PMKID List */
      rsn_ie_out->pmkid_list = (al_802_11_pmkid_t *)al_heap_alloc(AL_CONTEXT sizeof(al_802_11_pmkid_t) * rsn_ie_out->pmkid_count AL_HEAP_DEBUG_PARAM);
      for (i = 0; i < rsn_ie_out->pmkid_count; i++)
      {
         p = _parse_pmkid(AL_CONTEXT p, &rsn_ie_out->pmkid_list[i]);
      }
   }

end_of_data:
   return 0;
}


static AL_INLINE int _parse_wpa_ie(AL_CONTEXT_PARAM_DECL al_802_11_information_element_t *ie_data, al_802_11_rsn_ie_t *rsn_ie_out)
{
   if (!AL_802_11_IS_WPA_OUI(ie_data->data) || (ie_data->data[3] != 0x01))
   {
      return -1;
   }

   rsn_ie_out->mode = AL_802_11_SECURITY_INFO_RSN_MODE_WPA;

   return _parse_rsn_ie(AL_CONTEXT ie_data, rsn_ie_out);
}


int al_802_11_parse_rsn_ie(AL_CONTEXT_PARAM_DECL al_802_11_information_element_t *ie_in, al_802_11_rsn_ie_t *rsn_ie_out)
{
   memset(rsn_ie_out, 0, sizeof(al_802_11_rsn_ie_t));
   switch (ie_in->element_id)
   {
   case AL_802_11_EID_RSN:      /** WPA2/802.11i use RSN element */
      rsn_ie_out->mode = AL_802_11_SECURITY_INFO_RSN_MODE_11I;
      return _parse_rsn_ie(AL_CONTEXT ie_in, rsn_ie_out);

   case AL_802_11_EID_VENDOR_PRIVATE:      /** WPA uses Vendor private element */
      ;
      return _parse_wpa_ie(AL_CONTEXT ie_in, rsn_ie_out);
   }

   return -1;
}


void al_802_11_cleanup_rsn_ie(AL_CONTEXT_PARAM_DECL al_802_11_rsn_ie_t *rsn_ie)
{
   if (rsn_ie->akm_suites != NULL)
   {
      al_heap_free(AL_CONTEXT rsn_ie->akm_suites);
   }
   if (rsn_ie->pairwise_suites)
   {
      al_heap_free(AL_CONTEXT rsn_ie->pairwise_suites);
   }
   if (rsn_ie->pmkid_list != NULL)
   {
      al_heap_free(AL_CONTEXT rsn_ie->pmkid_list);
   }

   memset(rsn_ie, 0, sizeof(al_802_11_rsn_ie_t));
}


al_802_11_rsn_ie_t *al_802_11_clone_rsn_ie(AL_CONTEXT_PARAM_DECL al_802_11_rsn_ie_t *rsn_ie)
{
   al_802_11_rsn_ie_t *rsn_ie_new;

   rsn_ie_new = (al_802_11_rsn_ie_t *)al_heap_alloc(AL_CONTEXT sizeof(al_802_11_rsn_ie_t)AL_HEAP_DEBUG_PARAM);

   memset(rsn_ie_new, 0, sizeof(al_802_11_rsn_ie_t));

   memcpy(rsn_ie_new, rsn_ie, sizeof(al_802_11_rsn_ie_t));

   /**
    * Copy pairwise ciphers
    */

   rsn_ie_new->pairwise_suites = (al_802_11_rsn_cipher_t *)al_heap_alloc(AL_CONTEXT sizeof(al_802_11_rsn_cipher_t) * rsn_ie_new->pairwise_count AL_HEAP_DEBUG_PARAM);
   memcpy(rsn_ie_new->pairwise_suites,
          rsn_ie->pairwise_suites,
          sizeof(sizeof(al_802_11_rsn_cipher_t) * rsn_ie_new->pairwise_count));

   /**
    * Copy AKM suites
    */

   rsn_ie_new->akm_suites = (al_802_11_akm_t *)al_heap_alloc(AL_CONTEXT sizeof(al_802_11_akm_t) * rsn_ie_new->akm_count AL_HEAP_DEBUG_PARAM);
   memcpy(rsn_ie_new->akm_suites,
          rsn_ie->akm_suites,
          sizeof(al_802_11_akm_t) * rsn_ie_new->akm_count);

   /**
    * Copy PMK id list
    */

   rsn_ie_new->pmkid_list = (al_802_11_pmkid_t *)al_heap_alloc(AL_CONTEXT sizeof(al_802_11_pmkid_t) * rsn_ie_new->pmkid_count AL_HEAP_DEBUG_PARAM);

   memcpy(rsn_ie_new->pmkid_list,
          rsn_ie->pmkid_list,
          sizeof(al_802_11_pmkid_t) * rsn_ie_new->pmkid_count);


   return rsn_ie_new;
}


static AL_INLINE int _format_rsn_ie(AL_CONTEXT_PARAM_DECL al_802_11_security_info_t *security_info,
                                    al_802_11_security_info_t                       *net_if_security_info,
                                    int                                             rsn_mode,
                                    unsigned char                                   *buffer,
                                    int                                             length)
{
   unsigned char             *p;
   unsigned char             *temp;
   unsigned short            count;
   unsigned short            s;
   const unsigned char       *oui;
   al_802_11_security_info_t *group_key_sec_info;
   unsigned char             *length_pos;
   unsigned char             *content_start;

   static const unsigned char wpa_oui[3] = { 0x00, 0x50, 0xF2 };
   static const unsigned char rsn_oui[3] = { 0x00, 0x0F, 0xAC };

   oui           = NULL;
   p             = buffer;
   length_pos    = NULL;
   content_start = NULL;

   rsn_mode = security_info->rsn.mode;

   switch (rsn_mode)
   {
   case AL_802_11_SECURITY_INFO_RSN_MODE_WPA:
      oui           = wpa_oui;
      p[0]          = AL_802_11_EID_VENDOR_PRIVATE;
      p[1]          = 0;
      length_pos    = &p[1];
      p            += 2;
      content_start = p;
      memcpy(p, wpa_oui, 3);
      p   += 3;
      p[0] = 0x01;
      p++;
      break;

   case AL_802_11_SECURITY_INFO_RSN_MODE_11I:
      oui           = rsn_oui;
      p[0]          = AL_802_11_EID_RSN;
      p[1]          = 0;
      length_pos    = &p[1];
      p            += 2;
      content_start = p;
      break;

   case AL_802_11_SECURITY_INFO_RSN_MODE_BOTH:
      /** TODO later */
      return 0;
   }

   /** Version */
   s = 1;
   s = al_cpu_to_le16(s);
   memcpy(p, &s, 2);
   p += 2;

   /**
    * For the group cipher suite we always choose the net_if_security_info
    * if provided. This is for the case where we can't have seperate MAC
    * addresses for every VLAN
    */

   if (net_if_security_info != NULL)
   {
      group_key_sec_info = net_if_security_info;
   }
   else
   {
      group_key_sec_info = security_info;
   }

   if (group_key_sec_info->wep.enabled)
   {
      memcpy(p, oui, 3);
      p += 3;
      switch (group_key_sec_info->wep.strength)
      {
      case AL_802_11_SECURITY_INFO_STRENGTH_40:
         p[0] = AL_802_11_RSN_CIPHER_WEP_40;
         break;

      case AL_802_11_SECURITY_INFO_STRENGTH_104:
         p[0] = AL_802_11_RSN_CIPHER_WEP_104;
         break;
      }
      p++;
   }
   else if (group_key_sec_info->rsn.enabled)
   {
      /** Select TKIP if TKIP is selected, else select CCMP */
      memcpy(p, oui, 3);
      p += 3;
      if (group_key_sec_info->rsn.cipher_modes & AL_802_11_SECURITY_INFO_RSN_CIPHER_TKIP)
      {
         p[0] = AL_802_11_RSN_CIPHER_TKIP;
      }
      else
      {
         p[0] = AL_802_11_RSN_CIPHER_CCMP;
      }
      p++;
   }
   else
   {
      memcpy(p, oui, 3);
      p   += 3;
      p[0] = AL_802_11_RSN_CIPHER_WEP_40;
      p++;
   }

   count = 0;
   /** Skip pairwise cipher count initially */
   temp = p;
   p   += 2;

   /** Pairwise Ciphers */

   if (security_info->rsn.cipher_modes & AL_802_11_SECURITY_INFO_RSN_CIPHER_TKIP)
   {
      memcpy(p, oui, 3);
      p   += 3;
      p[0] = AL_802_11_RSN_CIPHER_TKIP;
      p++;
      count++;
   }

   if (security_info->rsn.cipher_modes & AL_802_11_SECURITY_INFO_RSN_CIPHER_CCMP)
   {
      memcpy(p, oui, 3);
      p   += 3;
      p[0] = AL_802_11_RSN_CIPHER_CCMP;
      p++;
      count++;
   }

   /** Pairwise Cipher count */
   s = al_cpu_to_le16(count);
   memcpy(temp, &s, 2);

   count = 0;
   /** Skip AKM count initially */
   temp = p;
   p   += 2;

   /** AKM suites */

   if (security_info->rsn.auth_modes & AL_802_11_SECURITY_INFO_RSN_AUTH_1X)
   {
      memcpy(p, oui, 3);
      p   += 3;
      p[0] = AL_802_11_RSN_AUTH_1X;
      p++;
      count++;
   }

   if (security_info->rsn.auth_modes & AL_802_11_SECURITY_INFO_RSN_AUTH_PSK)
   {
      memcpy(p, oui, 3);
      p   += 3;
      p[0] = AL_802_11_RSN_PSK;
      p++;
      count++;
   }

   /** AKM suite count */
   s = al_cpu_to_le16(count);
   memcpy(temp, &s, 2);

   if (rsn_mode != AL_802_11_SECURITY_INFO_RSN_MODE_WPA)
   {
      /** RSN Capabilities */
      s = 0;
      s = al_cpu_to_le16(s);
      memcpy(p, &s, 2);
      p += 2;

      /** PMKID count */
      s = 0;
      s = al_cpu_to_le16(s);
      memcpy(p, &s, 2);
      p += 2;
   }

   /**
    * Set the length
    */

   *length_pos = (p - content_start);

   return(p - buffer);
}


/**
 * Formats a 802.11i RSN IE. If WEP has been enabled, then the
 * group key has to be WEP. Otherwise the group key is the lowest
 * enabled cipher.
 */
int al_802_11_format_rsn_ie(AL_CONTEXT_PARAM_DECL al_802_11_security_info_t *security_info,
                            al_802_11_security_info_t                       *net_if_security_info,
                            unsigned char                                   *buffer,
                            int                                             length)
{
   int rsn_mode;

   if (!security_info->rsn.enabled)
   {
      return 0;
   }

   rsn_mode = security_info->rsn.mode;

   switch (rsn_mode)
   {
   case AL_802_11_SECURITY_INFO_RSN_MODE_WPA:
      return _format_rsn_ie(AL_CONTEXT security_info, net_if_security_info, AL_802_11_SECURITY_INFO_RSN_MODE_WPA, buffer, length);

   case AL_802_11_SECURITY_INFO_RSN_MODE_11I:
      return _format_rsn_ie(AL_CONTEXT security_info, net_if_security_info, AL_802_11_SECURITY_INFO_RSN_MODE_11I, buffer, length);

   case AL_802_11_SECURITY_INFO_RSN_MODE_BOTH:
      /** TODO */
      return 0;
   }

   return 0;
}


int al_802_11_parse_md_ie(AL_CONTEXT_PARAM_DECL al_802_11_information_element_t *ie_in, al_802_11_md_ie_t *md_ie_out)
{
   unsigned char *p;
   unsigned char md_ie_cap;

   if (ie_in->element_id != AL_802_11_EID_VENDOR_PRIVATE)
   {
      return -1;
   }

   p = ie_in->data;

   if (!AL_802_11_IS_MD_OUI(p))
   {
      return -2;
   }

   p += 3;

   memset(md_ie_out, 0, sizeof(al_802_11_md_ie_t));

   md_ie_out->version = *p++;
   md_ie_out->type    = *p++;

   switch (md_ie_out->type)
   {
   case AL_802_11_MD_IE_TYPE_ASSOC_REQUEST:
   case AL_802_11_MD_IE_TYPE_FIPS_ASSOC_REQUEST:
      md_ie_out->data.assoc_request.assoc_type = *p++;
      md_ie_cap = *p++;
      if (md_ie_out->version >= AL_802_11_MD_IE_VERSION_COMPRESSION)
      {
         md_ie_out->data.assoc_request.md_ie_capability |= (md_ie_cap & AL_802_11_MD_IE_COMPRESSION_SUPPORT);
      }
      if (md_ie_out->version >= AL_802_11_MD_IE_VERSION_EFFISTREAM)
      {
         md_ie_out->data.assoc_request.md_ie_capability |= (md_ie_cap & AL_802_11_MD_IE_EFFISTREAM_SUPPORT);
      }
      break;

   case AL_802_11_MD_IE_TYPE_ASSOC_RESPONSE:
      md_ie_out->data.assoc_response.group_cipher_type = *p++;
      md_ie_out->data.assoc_response.group_key_index   = *p++;
      memcpy(md_ie_out->data.assoc_response.group_key,
             p,
             AL_802_11_MD_IE_TOTAL_GROUP_KEY_LENGTH);
      p += AL_802_11_MD_IE_TOTAL_GROUP_KEY_LENGTH;
      memcpy(md_ie_out->data.assoc_response.pairwise_key,
             p,
             AL_802_11_MD_IE_TOTAL_PAIRWISE_KEY_LENGTH);
      p        += AL_802_11_MD_IE_TOTAL_PAIRWISE_KEY_LENGTH;
      md_ie_cap = *p++;
      if (md_ie_out->version >= AL_802_11_MD_IE_VERSION_COMPRESSION)
      {
         md_ie_out->data.assoc_response.md_ie_capability |= (md_ie_cap & AL_802_11_MD_IE_COMPRESSION_SUPPORT);
      }
      if (md_ie_out->version >= AL_802_11_MD_IE_VERSION_EFFISTREAM)
      {
         md_ie_out->data.assoc_response.md_ie_capability |= (md_ie_cap & AL_802_11_MD_IE_EFFISTREAM_SUPPORT);
      }
      break;

   case AL_802_11_MD_IE_TYPE_FIPS_ASSOC_RESPONSE:
      md_ie_out->data.assoc_response.group_cipher_type = *p++;
      md_ie_out->data.assoc_response.group_key_index   = *p++;
      memcpy(md_ie_out->data.assoc_response.group_key,
             p,
             AL_802_11_MD_IE_FIPS_TOTAL_GROUP_KEY_LENGTH);
      p += AL_802_11_MD_IE_FIPS_TOTAL_GROUP_KEY_LENGTH;
      memcpy(md_ie_out->data.assoc_response.pairwise_key,
             p,
             AL_802_11_MD_IE_FIPS_TOTAL_PAIRWISE_KEY_LENGTH);
      p        += AL_802_11_MD_IE_FIPS_TOTAL_PAIRWISE_KEY_LENGTH;
      md_ie_cap = *p++;
      if (md_ie_out->version >= AL_802_11_MD_IE_VERSION_COMPRESSION)
      {
         md_ie_out->data.assoc_response.md_ie_capability |= (md_ie_cap & AL_802_11_MD_IE_COMPRESSION_SUPPORT);
      }
      if (md_ie_out->version >= AL_802_11_MD_IE_VERSION_EFFISTREAM)
      {
         md_ie_out->data.assoc_response.md_ie_capability |= (md_ie_cap & AL_802_11_MD_IE_EFFISTREAM_SUPPORT);
      }
      break;

   case AL_802_11_MD_IE_TYPE_IMCP:
      md_ie_out->data.imcp_packet.length = ie_in->length - (p - ie_in->data);
      memcpy(md_ie_out->data.imcp_packet.contents, p, md_ie_out->data.imcp_packet.length);
      break;
   }

   return 0;
}


int al_802_11_format_md_ie(AL_CONTEXT_PARAM_DECL al_802_11_md_ie_t *md_ie_in, al_802_11_information_element_t *ie_out)
{
   unsigned char *p;
   unsigned char md_ie_cap;

   static const unsigned char md_oui[3] = { 0x00, 0x12, 0xCE };

   memset(ie_out, 0, sizeof(al_802_11_information_element_t));

   md_ie_cap          = 0;
   ie_out->element_id = AL_802_11_EID_VENDOR_PRIVATE;
   p = ie_out->data;

   memcpy(p, md_oui, 3);
   p   += 3;
   *p++ = md_ie_in->version;
   *p++ = md_ie_in->type;

   switch (md_ie_in->type)
   {
   case AL_802_11_MD_IE_TYPE_ASSOC_REQUEST:
   case AL_802_11_MD_IE_TYPE_FIPS_ASSOC_REQUEST:
      *p++ = md_ie_in->data.assoc_request.assoc_type;
      if (md_ie_in->version >= AL_802_11_MD_IE_VERSION_COMPRESSION)
      {
         md_ie_cap |= (md_ie_in->data.assoc_request.md_ie_capability & AL_802_11_MD_IE_COMPRESSION_SUPPORT);
      }
      if (md_ie_in->version >= AL_802_11_MD_IE_VERSION_EFFISTREAM)
      {
         md_ie_cap |= (md_ie_in->data.assoc_request.md_ie_capability & AL_802_11_MD_IE_EFFISTREAM_SUPPORT);
      }
      *p++ = md_ie_cap;
      break;

   case AL_802_11_MD_IE_TYPE_ASSOC_RESPONSE:
      *p++ = md_ie_in->data.assoc_response.group_cipher_type;
      *p++ = md_ie_in->data.assoc_response.group_key_index;
      memcpy(p,
             md_ie_in->data.assoc_response.group_key,
             AL_802_11_MD_IE_TOTAL_GROUP_KEY_LENGTH);
      p += AL_802_11_MD_IE_TOTAL_GROUP_KEY_LENGTH;
      memcpy(p,
             md_ie_in->data.assoc_response.pairwise_key,
             AL_802_11_MD_IE_TOTAL_PAIRWISE_KEY_LENGTH);
      p += AL_802_11_MD_IE_TOTAL_PAIRWISE_KEY_LENGTH;
      if (md_ie_in->version >= AL_802_11_MD_IE_VERSION_COMPRESSION)
      {
         md_ie_cap |= (md_ie_in->data.assoc_response.md_ie_capability & AL_802_11_MD_IE_COMPRESSION_SUPPORT);
      }
      if (md_ie_in->version >= AL_802_11_MD_IE_VERSION_EFFISTREAM)
      {
         md_ie_cap |= (md_ie_in->data.assoc_response.md_ie_capability & AL_802_11_MD_IE_EFFISTREAM_SUPPORT);
      }

      *p++ = md_ie_cap;
      break;

   case AL_802_11_MD_IE_TYPE_FIPS_ASSOC_RESPONSE:
      *p++ = md_ie_in->data.assoc_response.group_cipher_type;
      *p++ = md_ie_in->data.assoc_response.group_key_index;
      memcpy(p,
             md_ie_in->data.assoc_response.group_key,
             AL_802_11_MD_IE_FIPS_TOTAL_GROUP_KEY_LENGTH);
      p += AL_802_11_MD_IE_FIPS_TOTAL_GROUP_KEY_LENGTH;
      memcpy(p,
             md_ie_in->data.assoc_response.pairwise_key,
             AL_802_11_MD_IE_FIPS_TOTAL_PAIRWISE_KEY_LENGTH);
      p += AL_802_11_MD_IE_FIPS_TOTAL_PAIRWISE_KEY_LENGTH;
      if (md_ie_in->version >= AL_802_11_MD_IE_VERSION_COMPRESSION)
      {
         md_ie_cap |= (md_ie_in->data.assoc_response.md_ie_capability & AL_802_11_MD_IE_COMPRESSION_SUPPORT);
      }
      if (md_ie_in->version >= AL_802_11_MD_IE_VERSION_EFFISTREAM)
      {
         md_ie_cap |= (md_ie_in->data.assoc_response.md_ie_capability & AL_802_11_MD_IE_EFFISTREAM_SUPPORT);
      }

      *p++ = md_ie_cap;
      break;

   case AL_802_11_MD_IE_TYPE_IMCP:
      memcpy(p, md_ie_in->data.imcp_packet.contents, md_ie_in->data.imcp_packet.length);
      p += md_ie_in->data.imcp_packet.length;
      break;
   }

   ie_out->length = p - ie_out->data;

   return 0;
}


int al_802_11_format_md_action(AL_CONTEXT_PARAM_DECL unsigned char type, unsigned char *data, unsigned char data_length, unsigned char *buffer)
{
   unsigned char              *p;
   static const unsigned char md_oui[3] = { 0x00, 0x12, 0xCE };

   p = buffer;

   *p++ = AL_802_11_ACTION_CATEGORY_VENDOR_SPECIFIC;
   memcpy(p, md_oui, 3);
   p += 3;

   *p++ = type;
   *p++ = data_length;

   memcpy(p, data, data_length);
   p += data_length;

   return(p - buffer);
}


int al_802_11_parse_md_action(AL_CONTEXT_PARAM_DECL unsigned char *buffer, unsigned char *type, unsigned char *data, unsigned char *data_length)
{
   unsigned char *p;

   p = buffer;

   if (*p != AL_802_11_ACTION_CATEGORY_VENDOR_SPECIFIC)
   {
      return -1;
   }

   p++;

   if (!AL_802_11_IS_MD_OUI(p))
   {
      return -1;
   }

   p += 3;

   *type        = *p++;
   *data_length = *p++;

   memcpy(data, p, *data_length);

   return 0;
}
