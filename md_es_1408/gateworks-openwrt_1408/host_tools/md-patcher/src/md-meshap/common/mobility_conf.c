/********************************************************************************
* MeshDynamics
* --------------
* File     : mobility_conf.c
* Comments : Mobility configuration information parser
* Created  : 5/1/2006
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  1  |11/14/2006| Changes for heap debug                          | Sriram |
* -----------------------------------------------------------------------------
* |  0  |5/1/2006  | Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

#include <linux/string.h>
#include <linux/kernel.h>
#include "al.h"
#include "al_context.h"
#include "al_impl_context.h"
#include "mobility_conf.h"
#include "conf_common.h"

#define _MAX_TOKEN_SIZE                           256

#define _SUCCESS                                  0
#define _ERR                                      -1

#define _GET_TOKEN_SUCCESS                        0
#define _GET_TOKEN_EOF                            -1
#define _GET_TOKEN_UNKNOWN                        -2
#define _GET_TOKEN_ERROR                          -3

#define _MOBILITY_CONF_TOKEN_TEL_MAX              1
#define _MOBILITY_CONF_TOKEN_TEL_MIN              2
#define _MOBILITY_CONF_TOKEN_QUAL                 3
#define _MOBILITY_CONF_TOKEN_MAX_RETRY_P          4
#define _MOBILITY_CONF_TOKEN_MAX_ERR_P            5
#define _MOBILITY_CONF_TOKEN_MAX_ERR              6
#define _MOBILITY_CONF_TOKEN_MIN_RETRY_P          7
#define _MOBILITY_CONF_TOKEN_MIN_QUAL             8
#define _MOBILITY_CONF_TOKEN_INIT_RATE            9
#define _MOBILITY_CONF_TOKEN_DISC_MAX             10
#define _MOBILITY_CONF_TOKEN_DISC_COST            11
#define _MOBILITY_CONF_TOKEN_RRBC                 12
#define _MOBILITY_CONF_TOKEN_SCAN_COUNT           13
#define _MOBILITY_CONF_TOKEN_SCAN_DWELL           14
#define _MOBILITY_CONF_TOKEN_SAMP_MIN             15
#define _MOBILITY_CONF_TOKEN_SAMP_MAX             16
#define _MOBILITY_CONF_TOKEN_SAMP_N_P             17
#define _MOBILITY_CONF_TOKEN_EVAL_DAMP            18

#define _MOBILITY_CONF_TOKEN_MOBIILITY_INDICES    19
#define _MOBILITY_CONF_TOKEN_INDEX                20
#define _MOBILITY_CONF_TOKEN_OPEN_BRACE           21
#define _MOBILITY_CONF_TOKEN_CLOSE_BRACE          22
#define _MOBILITY_CONF_TOKEN_OPEN_BRACKET         23
#define _MOBILITY_CONF_TOKEN_CLOSE_BRACKET        24
#define _MOBILITY_CONF_TOKEN_EQUALS               25
#define _MOBILITY_CONF_TOKEN_COMMA                26
#define _MOBILITY_CONF_TOKEN_IDENT                27

static struct
{
   const char *token;
   int        code;
}
_token_info[] =
{
   { "mobility_indices", _MOBILITY_CONF_TOKEN_MOBIILITY_INDICES },
   { "index",            _MOBILITY_CONF_TOKEN_INDEX             },
   { "tel_max",          _MOBILITY_CONF_TOKEN_TEL_MAX           },
   { "tel_min",          _MOBILITY_CONF_TOKEN_TEL_MIN           },
   { "qual",             _MOBILITY_CONF_TOKEN_QUAL              },
   { "max_rtry_p",       _MOBILITY_CONF_TOKEN_MAX_RETRY_P       },
   { "max_err_p",        _MOBILITY_CONF_TOKEN_MAX_ERR_P         },
   { "max_err",          _MOBILITY_CONF_TOKEN_MAX_ERR           },
   { "min_rtry_p",       _MOBILITY_CONF_TOKEN_MIN_RETRY_P       },
   { "min_qual",         _MOBILITY_CONF_TOKEN_MIN_QUAL          },
   { "init_rate",        _MOBILITY_CONF_TOKEN_INIT_RATE         },
   { "disc_max",         _MOBILITY_CONF_TOKEN_DISC_MAX          },
   { "disc_cost",        _MOBILITY_CONF_TOKEN_DISC_COST         },
   { "rrbc",             _MOBILITY_CONF_TOKEN_RRBC              },
   { "scan_count",       _MOBILITY_CONF_TOKEN_SCAN_COUNT        },
   { "scan_dwell",       _MOBILITY_CONF_TOKEN_SCAN_DWELL        },
   { "samp_min",         _MOBILITY_CONF_TOKEN_SAMP_MIN          },
   { "samp_max",         _MOBILITY_CONF_TOKEN_SAMP_MAX          },
   { "samp_n_p",         _MOBILITY_CONF_TOKEN_SAMP_N_P          },
   { "eval_damp",        _MOBILITY_CONF_TOKEN_EVAL_DAMP         },
   { "{",                _MOBILITY_CONF_TOKEN_OPEN_BRACE        },
   { "}",                _MOBILITY_CONF_TOKEN_CLOSE_BRACE       },
   { "(",                _MOBILITY_CONF_TOKEN_OPEN_BRACKET      },
   { ")",                _MOBILITY_CONF_TOKEN_CLOSE_BRACKET     },
   { "=",                _MOBILITY_CONF_TOKEN_EQUALS            },
   { ",",                _MOBILITY_CONF_TOKEN_COMMA             }
};

/** Data offsets indexed by token code */

#define __offsetof_(MEMBER)    ((size_t)&((mobility_index_data_t *)0)->MEMBER)

static const int _offsets[] =
{
   __offsetof_(t_el_max),
   __offsetof_(t_el_min),
   __offsetof_(qualification),
   __offsetof_(max_retry_percent),
   __offsetof_(max_error_percent),
   __offsetof_(max_errors),
   __offsetof_(min_retry_percent),
   __offsetof_(min_qualification),
   __offsetof_(initial_rate_index),
   __offsetof_(disconnect_count_max),
   __offsetof_(disconnect_multiplier),
   __offsetof_(round_robin_beacon_count),
   __offsetof_(scan_count),
   __offsetof_(scanner_dwell_interval),
   __offsetof_(sampling_t_next_min),
   __offsetof_(sampling_t_next_max),
   __offsetof_(sampling_n_p),
   __offsetof_(evaluation_damping_factor),
};

/**
 * The various states or modes of the parser.
 */

#define _PARSE_MODE_NONE                    -1
#define _PARSE_MODE_MOBILITY_INDICES        1
#define _PARSE_MODE_INDEX_BEGIN             2
#define _PARSE_MODE_INDEX                   3
#define _PARSE_MODE_INDEX_END               4
#define _PARSE_MODE_MOBILITY_INDICES_END    5

struct _mobility_conf_data
{
   char                  *parser_buffer;
   char                  *pos;
   char                  current_token[_MAX_TOKEN_SIZE + 1];
   char                  *curent_token_pos;
   int                   current_token_code;
   int                   line_number;
   int                   column_number;
   int                   parse_mode;
   int                   index_count;
   mobility_index_data_t *index_data;
};

typedef struct _mobility_conf_data   _mobility_conf_data_t;

#define _MAP_TOKEN_CODE_TO_PARSE_MODE(tc, pm) \
case (tc):                                    \
   conf_data->parse_mode = (pm);              \
   break

#define _BEGIN_MAP_BLOCK(tc) \
   switch (tc) {
#define _END_MAP_BLOCK() \
   }

#define _MAP_TOKEN_CODES(tc)                                                                            \
   _BEGIN_MAP_BLOCK(tc)                                                                                 \
   _MAP_TOKEN_CODE_TO_PARSE_MODE(_MOBILITY_CONF_TOKEN_MOBIILITY_INDICES, _PARSE_MODE_MOBILITY_INDICES); \
   _MAP_TOKEN_CODE_TO_PARSE_MODE(_MOBILITY_CONF_TOKEN_INDEX, _PARSE_MODE_INDEX);                        \
default:                                                                                                \
   al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "al_conf.c(%d,%d): ignoring token %s\n",                  \
                conf_data->line_number,                                                                 \
                conf_data->column_number,                                                               \
                conf_data->current_token);                                                              \
   _END_MAP_BLOCK()

static int _parse(AL_CONTEXT_PARAM_DECL _mobility_conf_data_t *conf_data);
static int _get_token(AL_CONTEXT_PARAM_DECL _mobility_conf_data_t *conf_data);
static int _set_index_data(AL_CONTEXT_PARAM_DECL _mobility_conf_data_t *conf_data, int index, int token, unsigned int value);

static int _parse(AL_CONTEXT_PARAM_DECL _mobility_conf_data_t *conf_data)
{
   int ret;
   int status;
   int i;
   int token_code;
   int val;


#define _PARSE_INTERMEDIATE_TOKEN(tok, tok_code)                                                                            \
   do {                                                                                                                     \
      ret = _get_token(AL_CONTEXT conf_data);                                                                               \
      if (ret == -1) {                                                                                                      \
         al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "mobility_conf.c(%d,%d): unexpected eof file while looking for '%c'\n", \
                      conf_data->line_number,                                                                               \
                      conf_data->column_number,                                                                             \
                      tok);                                                                                                 \
         status = -1;                                                                                                       \
         goto label_ret;                                                                                                    \
      }                                                                                                                     \
      if (conf_data->current_token_code != tok_code) {                                                                      \
         al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "mobility_conf.c(%d,%d): expecting token '%c' but found %s\n",          \
                      conf_data->line_number,                                                                               \
                      conf_data->column_number,                                                                             \
                      tok,                                                                                                  \
                      conf_data->current_token);                                                                            \
         status = -1;                                                                                                       \
         goto label_ret;                                                                                                    \
      }                                                                                                                     \
   } while (0)

   conf_data->parse_mode = _PARSE_MODE_NONE;
   status = 0;
   i      = -1;

   while (1)
   {
      switch (conf_data->parse_mode)
      {
      case _PARSE_MODE_NONE:
         ret = _get_token(AL_CONTEXT conf_data);
         if (ret == -1)
         {
            goto label_ret;
         }
         _MAP_TOKEN_CODES(conf_data->current_token_code);
         break;

      case _PARSE_MODE_MOBILITY_INDICES:
         _PARSE_INTERMEDIATE_TOKEN('(', _MOBILITY_CONF_TOKEN_OPEN_BRACKET);
         ret = _get_token(AL_CONTEXT conf_data);
         if (ret == -1)
         {
            al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "mobility_conf.c(%d,%d): unexpected eof file while looking for index count\n",
                         conf_data->line_number,
                         conf_data->column_number);
            status = -1;
            goto label_ret;
         }
         ret = al_conf_atoi(conf_data->current_token, &val);
         if (ret == -1)
         {
            al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "mobility_conf.c(%d,%d): token %s was found instead of an integer\n",
                         conf_data->line_number,
                         conf_data->column_number,
                         conf_data->current_token);
            status = -1;
            goto label_ret;
         }
         conf_data->index_count = val;
         conf_data->index_data  = (mobility_index_data_t *)al_heap_alloc(AL_CONTEXT sizeof(mobility_index_data_t) * val AL_HEAP_DEBUG_PARAM);
         memset(conf_data->index_data, 0, sizeof(mobility_index_data_t) * val);
         _PARSE_INTERMEDIATE_TOKEN(')', _MOBILITY_CONF_TOKEN_CLOSE_BRACKET);
         _PARSE_INTERMEDIATE_TOKEN('{', _MOBILITY_CONF_TOKEN_OPEN_BRACE);
         conf_data->parse_mode = _PARSE_MODE_INDEX_BEGIN;
         i = -1;
         break;

      case _PARSE_MODE_INDEX_BEGIN:
         if (++i >= conf_data->index_count)
         {
            conf_data->parse_mode = _PARSE_MODE_MOBILITY_INDICES_END;
            continue;
         }
         ret = _get_token(AL_CONTEXT conf_data);
         if (ret == -1)
         {
            al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "mobility_conf.c(%d,%d): unexpected eof file while looking for token\n",
                         conf_data->line_number,
                         conf_data->column_number);
            status = -1;
            goto label_ret;
         }
         _PARSE_INTERMEDIATE_TOKEN('(', _MOBILITY_CONF_TOKEN_OPEN_BRACKET);

         ret = _get_token(AL_CONTEXT conf_data);
         if (ret == -1)
         {
            al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "mobility_conf.c(%d,%d): unexpected eof file while looking for token\n",
                         conf_data->line_number,
                         conf_data->column_number);
            status = -1;
            goto label_ret;
         }

         _PARSE_INTERMEDIATE_TOKEN(')', _MOBILITY_CONF_TOKEN_CLOSE_BRACKET);
         _PARSE_INTERMEDIATE_TOKEN('{', _MOBILITY_CONF_TOKEN_OPEN_BRACE);

         conf_data->parse_mode = _PARSE_MODE_INDEX;
         break;

      case _PARSE_MODE_INDEX:
         ret = _get_token(AL_CONTEXT conf_data);
         if (ret == -1)
         {
            al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "mobility_conf.c(%d,%d): unexpected eof file while looking for keyword token\n",
                         conf_data->line_number,
                         conf_data->column_number);
            status = -1;
            goto label_ret;
         }
         token_code = conf_data->current_token_code;
         _PARSE_INTERMEDIATE_TOKEN('=', _MOBILITY_CONF_TOKEN_EQUALS);
         ret = _get_token(AL_CONTEXT conf_data);
         if (ret == -1)
         {
            al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "mobility_conf.c(%d,%d): unexpected eof file while looking for integer token data\n",
                         conf_data->line_number,
                         conf_data->column_number);
            status = -1;
            goto label_ret;
         }

         ret = al_conf_atoi(conf_data->current_token, &val);
         if (ret == -1)
         {
            al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "mobility_conf.c(%d,%d): token %s was found instead of an integer\n",
                         conf_data->line_number,
                         conf_data->column_number,
                         conf_data->current_token);
            status = -1;
            goto label_ret;
         }

         _set_index_data(AL_CONTEXT conf_data, i, token_code, val);

         conf_data->parse_mode = _PARSE_MODE_INDEX_END;

         break;

      case _PARSE_MODE_INDEX_END:

         ret = _get_token(AL_CONTEXT conf_data);
         if (ret == -1)
         {
            al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "mobility_conf.c(%d,%d): unexpected eof file while looking for } or ,\n",
                         conf_data->line_number,
                         conf_data->column_number);
            status = -1;
            goto label_ret;
         }

         if (conf_data->current_token_code == _MOBILITY_CONF_TOKEN_COMMA)
         {
            conf_data->parse_mode = _PARSE_MODE_INDEX;
         }
         else if (conf_data->current_token_code == _MOBILITY_CONF_TOKEN_CLOSE_BRACE)
         {
            conf_data->parse_mode = _PARSE_MODE_INDEX_BEGIN;
         }
         else
         {
            al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "mobility_conf.c(%d,%d): Expecting tokens } or , but found %s\n",
                         conf_data->line_number,
                         conf_data->column_number,
                         conf_data->current_token);
            status = -1;
            goto label_ret;
         }

         break;

      case _PARSE_MODE_MOBILITY_INDICES_END:
         _PARSE_INTERMEDIATE_TOKEN('}', _MOBILITY_CONF_TOKEN_CLOSE_BRACE);
         conf_data->parse_mode = _PARSE_MODE_NONE;
         break;
      }
   }


label_ret:
   return status;

#undef _PARSE_INTERMEDIATE_TOKEN
}


static int _get_token(AL_CONTEXT_PARAM_DECL _mobility_conf_data_t *conf_data)
{
   /**
    * Here we skip white spaces, comment lines, etc
    * and return then copy the token onto current_token
    * and update current_token_code. We also update the line_number
    * and column_number
    */

   int inside_comment;
   int token_found;
   int identifier_found;
   int i;

   inside_comment = 0;
   token_found    = 0;
   conf_data->curent_token_pos  = conf_data->current_token;
   identifier_found             = 0;
   *conf_data->curent_token_pos = 0;

   while (*conf_data->pos)
   {
      if (inside_comment)
      {
         if (*conf_data->pos == '\n')
         {
            ++conf_data->line_number;
            conf_data->column_number = 0;
            inside_comment           = 0;
         }
         goto label;
      }
      switch (*conf_data->pos)
      {
      case ' ':
      case '\t':
      case '\r':
         if (identifier_found)
         {
            token_found = 1;
         }
         break;

      case '\n':
         /** Skip but increment line_number */
         ++conf_data->line_number;
         conf_data->column_number = -1;
         if (identifier_found)
         {
            token_found = 1;
            conf_data->pos++;
         }
         break;

      case '#':
         /** From here to end of line skip everything */
         inside_comment = 1;
         break;

      case '=':
      case '(':
      case '{':
      case ',':
      case ')':
      case '}':
      case ':':
      case '.':
         if (!identifier_found)
         {
            *conf_data->curent_token_pos++ = *conf_data->pos++;
         }
         token_found = 1;
         break;

      default:
         *conf_data->curent_token_pos++ = *conf_data->pos;
         identifier_found = 1;
      }
      ++conf_data->column_number;
label:
      if (token_found)
      {
         break;
      }
      conf_data->pos++;
   }

   *conf_data->curent_token_pos = 0;

   if (*conf_data->current_token == 0)
   {
      return -1;
   }

   conf_data->current_token_code = _MOBILITY_CONF_TOKEN_IDENT;

   for (i = 0; i < sizeof(_token_info) / sizeof(*_token_info); i++)
   {
      if (!strcmp(conf_data->current_token, _token_info[i].token))
      {
         conf_data->current_token_code = _token_info[i].code;
         break;
      }
   }

   return 0;
}


static int _set_index_data(AL_CONTEXT_PARAM_DECL _mobility_conf_data_t *conf_data, int index, int token, unsigned int value)
{
   unsigned char *p;

   p = (unsigned char *)&conf_data->index_data[index];

   if (token - 1 < sizeof(_offsets) / sizeof(*_offsets))
   {
      memcpy(p + _offsets[token - 1], &value, 4);
   }

   return 0;
}


int mobility_conf_parse(AL_CONTEXT_PARAM_DECL const char *mobility_conf_string)
{
   _mobility_conf_data_t *data;
   int ret;

   data = (_mobility_conf_data_t *)al_heap_alloc(AL_CONTEXT sizeof(_mobility_conf_data_t)AL_HEAP_DEBUG_PARAM);

   memset(data, 0, sizeof(_mobility_conf_data_t));

   data->parser_buffer = (char *)al_heap_alloc(AL_CONTEXT strlen(mobility_conf_string) + 1 AL_HEAP_DEBUG_PARAM);
   strcpy(data->parser_buffer, mobility_conf_string);

   data->pos         = data->parser_buffer;
   data->line_number = 1;

   ret = _parse(AL_CONTEXT data);

   if (ret != 0)
   {
      if (data->index_data != NULL)
      {
         al_heap_free(AL_CONTEXT data->index_data);
      }
      al_heap_free(AL_CONTEXT data->parser_buffer);
      al_heap_free(AL_CONTEXT data);
      return 0;
   }

   return (int)data;
}


void mobility_conf_close(AL_CONTEXT_PARAM_DECL int conf_handle)
{
   _mobility_conf_data_t *data;

   data = (_mobility_conf_data_t *)conf_handle;

   if (data->index_data != NULL)
   {
      al_heap_free(AL_CONTEXT data->index_data);
   }
   al_heap_free(AL_CONTEXT data->parser_buffer);
   al_heap_free(AL_CONTEXT data);
}


int mobility_conf_get_index_count(AL_CONTEXT_PARAM_DECL int conf_handle)
{
   _mobility_conf_data_t *data;

   data = (_mobility_conf_data_t *)conf_handle;

   return data->index_count;
}


int mobility_conf_get_index_data(AL_CONTEXT_PARAM_DECL int conf_handle, int index, mobility_index_data_t *data_out)
{
   _mobility_conf_data_t *data;

   data = (_mobility_conf_data_t *)conf_handle;

   if ((index < 0) || (index >= data->index_count))
   {
      return -1;
   }

   memcpy(data_out, &data->index_data[index], sizeof(mobility_index_data_t));

   return 0;
}
