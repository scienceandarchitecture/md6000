/********************************************************************************
* MeshDynamics
* --------------
* File     : al_conf.c
* Comments : Mesh AP configuration file parser
* Created  : 4/23/2004
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  50 |8/27/2008 | Changes for Options                             | Sriram |
* -----------------------------------------------------------------------------
* |  49 |8/26/2008 | Used common configuration parser routines       | Sriram |
* -----------------------------------------------------------------------------
* |  48 |1/22/2008 | Changes for Mixed mode                          | Sriram |
* -----------------------------------------------------------------------------
* |  47 |11/8/2007 | Changes for effistream queued retry             |Prachiti|
* -----------------------------------------------------------------------------
* |  46 |6/6/2007  |  Changes for Effistream bitrate                 |Prachiti|
* -----------------------------------------------------------------------------
* |  45 |3/16/2007 | Fixed heap debug support                        | Sriram |
* -----------------------------------------------------------------------------
* |  44 |2/20/2007 | Changes for Effistream                          | Sriram |
* -----------------------------------------------------------------------------
* |  43 |2/15/2007 | Added txantenna                                 | Sriram |
* -----------------------------------------------------------------------------
* |  42 |2/14/2007 | Changes for PS PHY sub-types                    | Sriram |
* -----------------------------------------------------------------------------
* |  41 |1/9/2007  | Changes for channel specific DFS                |Prachiti|
* -----------------------------------------------------------------------------
* |  40 |11/14/2006| Changes for heap debug support                  | Sriram |
* -----------------------------------------------------------------------------
* |  39 |10/31/2006| Changes for private frequency list              | Sriram |
* -----------------------------------------------------------------------------
* |  38 |02/15/2006| dot11e implementation added				      | Bindu  |
* -----------------------------------------------------------------------------
* |  37 |02/08/2006| Config SQNR and Hide SSID added				  | Sriram |
* -----------------------------------------------------------------------------
* |  36 |11/11/2005| ACK_Timeout added								  |Prachiti|
* -----------------------------------------------------------------------------
* |  35 |11/11/2005| Regulatory Domain and Country code added        |Prachiti|
* -----------------------------------------------------------------------------
* |  34 |6/12/2005 | Misc changes                                    | Sriram |
* -----------------------------------------------------------------------------
* |  33 |6/9/2005  | TX Rate added to VLAN                           | Sriram |
* -----------------------------------------------------------------------------
* |  32 |5/20/2005 | Normalized changes                              | Sriram |
* -----------------------------------------------------------------------------
* |  31 |5/19/2005 | fcc_certified,etsi_certified added              | Abhijit|
* -----------------------------------------------------------------------------
* |  30 |5/8/2005  | Changes for GPS/description                     | Sriram |
* -----------------------------------------------------------------------------
* |  29 |4/26/2005 | Model Name added                                |Prachiti|
* -----------------------------------------------------------------------------
* |  28 |4/15/2005 | Misc changes                                    | Sriram |
* -----------------------------------------------------------------------------
* |  27 |4/12/2005 | radius_secret_key chaged to array               |Prachiti|
* -----------------------------------------------------------------------------
* |  26 |3/9/2005  | comma added dot11e_enabled in al_conf_put       |Prachiti|
* -----------------------------------------------------------------------------
* |  25 |3/07/2005 | dot11e_enabled added in al_conf_put             |Prachiti|
* -----------------------------------------------------------------------------
* |  24 |2/09/2005 | status var initialised			              |Prachiti|
* -----------------------------------------------------------------------------
* |  23 |2/09/2005 | removed warnings					              |Prachiti|
* -----------------------------------------------------------------------------
* |  22 |2/09/2005 | wds_encrypted parsing added		              |Prachiti|
* -----------------------------------------------------------------------------
* |  21 |2/09/2005 | al_conf_put for security and vlan               |Prachiti|
* -----------------------------------------------------------------------------
* |  20 |2/08/2005 | vlan parsing added			                  |Prachiti|
* -----------------------------------------------------------------------------
* |  19 |2/07/2005 | security parsing added		                  |Prachiti|
* -----------------------------------------------------------------------------
* |  18 |1/14/2005 | beacon_interval printing added                  | Sriram |
* -----------------------------------------------------------------------------
* |  17 |1/8/2005  | Changes for per-interface essid/etc             | Sriram |
* -----------------------------------------------------------------------------
* |  16 |11/17/2004| added al_conf_put                               | Abhijit|
* -----------------------------------------------------------------------------
* |  15 |11/16/2004| Some error fixed from string put (Rover Put)    | Anand  |
* -----------------------------------------------------------------------------
* |  14 |11/10/2004| preamble_type,slot_time_type added              | Sriram |
* -----------------------------------------------------------------------------
* |  13 |10/29/2004| Bug fixed for Encode/Decode(String put)         | Anand  |
* -----------------------------------------------------------------------------
* |  12 |10/28/2004| Bug fixes in set functions(al_conf_put)         | Anand  |
* -----------------------------------------------------------------------------
* |  11 |10/15/2004| al_conf_open added                              | Abhijit|
* -----------------------------------------------------------------------------
* |  10 |10/15/2004| added al_conf_create_config_string_from_data    | Abhijit|
* -----------------------------------------------------------------------------
* |  9  |10/5/2004 | Additions of al_conf_set functions              | Abhijit|
* -----------------------------------------------------------------------------
* |  8  |9/28/2004 | Changes for 3 radio                             | Sriram |
* -----------------------------------------------------------------------------
* |  7  |7/19/2004 | Implemented dca_list property                   | Sriram |
* -----------------------------------------------------------------------------
* |  6  |7/2/2004  | Implemented preferred_parent and name properties| Sriram |
* -----------------------------------------------------------------------------
* |  5  |6/10/2004 | Fixed integer parsing bug                       | Sriram |
* -----------------------------------------------------------------------------
* |  4  |5/21/2004 | Removed #include <stdlib.h>                     | Sriram |
* -----------------------------------------------------------------------------
* |  3  |5/21/2004 | Log type added to al_print_log                  | Anand  |
* -----------------------------------------------------------------------------
* |  2  |5/7/2004  | returning if fopen failed                       | Anand  |
* -----------------------------------------------------------------------------
* |  1  |5/5/2004  | Packet/Memory Leak Debug params added           | Anand  |
* -----------------------------------------------------------------------------
* |  0  |4/23/2004 | Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/
#ifdef __KERNEL__
#include <linux/string.h>
#include <linux/kernel.h>
#include <linux/printk.h>
#else
#include <stdio.h>
#include <strings.h>
#include <string.h>
#endif
#include "al.h"
#include "al_context.h"
#include "al_impl_context.h"
#include "al_802_11.h"
#include "al_conf.h"
#include "conf_common.h"

/**
 * Configuration file tokens for lexical analyzer
 */

#define _AL_CONF_TOKEN_HASH                        0
#define _AL_CONF_TOKEN_NAME                        1
#define _AL_CONF_TOKEN_EQUALS                      2
#define _AL_CONF_TOKEN_MESH_ID                     3
#define _AL_CONF_TOKEN_MESH_IMCP_KEY               4
#define _AL_CONF_TOKEN_OPEN_BRACE                  5
#define _AL_CONF_TOKEN_CLOSE_BRACE                 6
#define _AL_CONF_TOKEN_ESSID                       7
#define _AL_CONF_TOKEN_RTS_TH                      8
#define _AL_CONF_TOKEN_FRAG_TH                     9
#define _AL_CONF_TOKEN_BCN_INT                     10
#define _AL_CONF_TOKEN_DCA                         11
#define _AL_CONF_TOKEN_STAY_AWAKE_COUNT            12
#define _AL_CONF_TOKEN_BRIDGE_AGEING_TIME          13
#define _AL_CONF_TOKEN_IF_INFO                     14
#define _AL_CONF_TOKEN_OPEN_BRACK                  15
#define _AL_CONF_TOKEN_CLOSE_BRACK                 14
#define _AL_CONF_TOKEN_MEDIUM_TYPE                 15
#define _AL_CONF_TOKEN_E                           16
#define _AL_CONF_TOKEN_W                           17
#define _AL_CONF_TOKEN_V                           18
#define _AL_CONF_TOKEN_COMMA                       19
#define _AL_CONF_TOKEN_MEDIUM_SUB_TYPE             20
#define _AL_CONF_TOKEN_A                           21
#define _AL_CONF_TOKEN_B                           22
#define _AL_CONF_TOKEN_G                           23
#define _AL_CONF_TOKEN_BG                          24
#define _AL_CONF_TOKEN_X                           25
#define _AL_CONF_TOKEN_USAGE_TYPE                  26
#define _AL_CONF_TOKEN_DS                          27
#define _AL_CONF_TOKEN_WM                          28
#define _AL_CONF_TOKEN_PMON                        29
#define _AL_CONF_TOKEN_AMON                        30
#define _AL_CONF_TOKEN_CHANNEL                     31
#define _AL_CONF_TOKEN_BONDING                     32
#define _AL_CONF_TOKEN_S                           33
#define _AL_CONF_TOKEN_D                           34
#define _AL_CONF_TOKEN_TXPOWER                     35
#define _AL_CONF_TOKEN_DCA_LIST                    36
#define _AL_CONF_TOKEN_SERVICE                     37
#define _AL_CONF_TOKEN_ACK_TIMEOUT                 38

#define _AL_CONF_TOKEN_PREFERRED_PARENT            39
#define _AL_CONF_TOKEN_COLON                       40
#define _AL_CONF_TOKEN_SIGNAL_MAP                  41
#define _AL_CONF_TOKEN_HBI                         42
#define _AL_CONF_TOKEN_HBMC                        43
#define _AL_CONF_TOKEN_HOP_COST                    44
#define _AL_CONF_TOKEN_MAH                         45
#define _AL_CONF_TOKEN_LASI                        46
#define _AL_CONF_TOKEN_CRT                         47
#define _AL_CONF_TOKEN_IDENT                       48
#define _AL_CONF_TOKEN_EOF                         49
#define _AL_CONF_TOKEN_PREAMBLE_TYPE               50
#define _AL_CONF_TOKEN_LONG                        51
#define _AL_CONF_TOKEN_SHORT                       52
#define _AL_CONF_TOKEN_SLOT_TIME_TYPE              53
#define _AL_CONF_TOKEN_SECURITY                    54
#define _AL_CONF_TOKEN_SECURITY_NONE               55
#define _AL_CONF_TOKEN_WEP                         56
#define _AL_CONF_TOKEN_RSN_PSK                     57
#define _AL_CONF_TOKEN_RSN_RADIUS                  58
#define _AL_CONF_TOKEN_ENABLED                     59
#define _AL_CONF_TOKEN_STRENGTH                    60
#define _AL_CONF_TOKEN_KEY_INDEX                   61
#define _AL_CONF_TOKEN_KEYS                        62
#define _AL_CONF_TOKEN_KEY                         63
#define _AL_CONF_TOKEN_MODE                        64
#define _AL_CONF_TOKEN_CIPHER_TKIP                 65
#define _AL_CONF_TOKEN_CIPHER_CCMP                 66
#define _AL_CONF_TOKEN_GROUP_KEY_RENEWAL           67
#define _AL_CONF_TOKEN_RADIUS_SERVER               68
#define _AL_CONF_TOKEN_RADIUS_PORT                 69
#define _AL_CONF_TOKEN_RADIUS_SECRET               70
#define _AL_CONF_TOKEN_DOT                         71
#define _AL_CONF_TOKEN_VLAN_INFO                   72
#define _AL_CONF_TOKEN_VLAN_DEFAULT                73
#define _AL_CONF_TOKEN_VLAN_TAG                    74
#define _AL_CONF_TOKEN_VLAN_UNTAGGED               75
#define _AL_CONF_TOKEN_VLAN_DOT1P_PRIORITY         76
#define _AL_CONF_TOKEN_VLAN_IP                     77
#define _AL_CONF_TOKEN_WDS_ENCRYPTED               78
#define _AL_CONF_TOKEN_MODEL                       79
#define _AL_CONF_TOKEN_GPS_X_COORDINATE            80
#define _AL_CONF_TOKEN_GPS_Y_COORDINATE            81

#define _AL_CONF_TOKEN_REGULATORY_DOMAIN           82
#define _AL_CONF_TOKEN_COUNTRY_CODE                83

#define _AL_CONF_TOKEN_DESCRIPTION                 84
#define _AL_CONF_TOKEN_FCC_CERTIFIED_OPERATION     85
#define _AL_CONF_TOKEN_ETSI_CERTIFIED_OPERATION    86
#define _AL_CONF_TOKEN_DS_TX_RATE                  87
#define _AL_CONF_TOKEN_DS_TX_POWER                 88
#define _AL_CONF_TOKEN_TXRATE                      89

#define _AL_CONF_TOKEN_IF_DOT11E_ENABLED           90
#define _AL_CONF_TOKEN_IF_DOT11E_CATEGORY          91

#define _AL_CONF_TOKEN_CONFIG_SQNR                 92
#define _AL_CONF_TOKEN_HIDE_SSID                   93

#define _AL_CONF_TOKEN_VLAN_DOT11E_ENABLED         94
#define _AL_CONF_TOKEN_VLAN_DOT11E_PRIORITY        95
#define _AL_CONF_TOKEN_PRIV_CHANNEL_BANDWIDTH      96
#define _AL_CONF_TOKEN_PRIV_CHANNEL_MAXPOWER       97
#define _AL_CONF_TOKEN_PRIV_CHANNEL_LIST           98
#define _AL_CONF_TOKEN_PRIV_CHANNEL_ANT_MAX        99
#define _AL_CONF_TOKEN_PRIV_CHANNEL_CTL            100

#define _AL_CONF_TOKEN_PSQ                         101
#define _AL_CONF_TOKEN_PSH                         102
#define _AL_CONF_TOKEN_PSF                         103
#define _AL_CONF_TOKEN_TXANT                       104

#define _AL_CONF_TOKEN_EFFISTREAM                  105
#define _AL_CONF_TOKEN_LEFT_SQBRACK                106
#define _AL_CONF_TOKEN_RIGHT_SQBRACK               107
#define _AL_CONF_TOKEN_ETH_TYPE                    108
#define _AL_CONF_TOKEN_ETH_DST                     109
#define _AL_CONF_TOKEN_ETH_SRC                     110
#define _AL_CONF_TOKEN_IP_TOS                      111
#define _AL_CONF_TOKEN_IP_DIFFSRV                  112
#define _AL_CONF_TOKEN_IP_SRC                      113
#define _AL_CONF_TOKEN_IP_DST                      114
#define _AL_CONF_TOKEN_IP_PROTO                    115
#define _AL_CONF_TOKEN_UDP_SRC_PORT                116
#define _AL_CONF_TOKEN_UDP_DST_PORT                117
#define _AL_CONF_TOKEN_UDP_LENGTH                  118
#define _AL_CONF_TOKEN_TCP_SRC_PORT                119
#define _AL_CONF_TOKEN_TCP_DST_PORT                120
#define _AL_CONF_TOKEN_TCP_LENGTH                  121
#define _AL_CONF_TOKEN_RTP_VERSION                 122
#define _AL_CONF_TOKEN_RTP_PAYLOAD                 123
#define _AL_CONF_TOKEN_RTP_LENGTH                  124
#define _AL_CONF_TOKEN_NO_ACK                      125
#define _AL_CONF_TOKEN_DOT11E_CATEGORY             126
#define _AL_CONF_TOKEN_DROP                        127
#define _AL_CONF_TOKEN_BIT_RATE                    128

#define _AL_CONF_TOKEN_GPS                         129
#define _AL_CONF_TOKEN_DHCP                        130
#define _AL_CONF_TOKEN_LOGMON                      131

#define _AL_CONF_TOKEN_INPUT                       132
#define _AL_CONF_TOKEN_PUSH_DEST                   133
#define _AL_CONF_TOKEN_PUSH_INT                    134

#define _AL_CONF_TOKEN_NET_ID                      135
#define _AL_CONF_TOKEN_GATEWAY                     136
#define _AL_CONF_TOKEN_DNS                         137
#define _AL_CONF_TOKEN_MASK                        138
#define _AL_CONF_TOKEN_LEASE                       139

#define _AL_CONF_TOKEN_QUEUED_RETRY                140

#define _AL_CONF_TOKEN_USE_VIRT_IF                 141

#define _AL_CONF_TOKEN_OPTIONS                     142

#define _AL_CONF_MAX_TOKEN_SIZE                    128
#define _AL_CONF_TOKEN_AP                          143

#define _AL_CONF_TOKEN_FAILOVER_ETH                144
#define _AL_CONF_TOKEN_POWER_ON_DEFAULT            145
#define _AL_CONF_TOKEN_SERVER_ADDR                 146
#if (MDE_80211N_SUPPORT || MDE_80211AC_SUPPORT)

#define            _AL_CONF_TOKEN_HT_CAPAB		   147
#define		   _AL_CONF_TOKEN_HT_BANDWIDTH          	148
#define            _AL_CONF_TOKEN_LDPC_ENABLED          	149
#define            _AL_CONF_TOKEN_SMPS                  	150
#define            _AL_CONF_TOKEN_STATIC                	151
#define	           _AL_CONF_TOKEN_DYNAMIC		        152
#define		   _AL_CONF_TOKEN_DISABLED		        153
#define            _AL_CONF_TOKEN_GI_20                 	154
#define            _AL_CONF_TOKEN_GI_40                 	155
#define            _AL_CONF_TOKEN_GI_AUTO		        156

#define            _AL_CONF_TOKEN_TX_STBC_ENABLED       	157
#define            _AL_CONF_TOKEN_RX_STBC_ENABLED       	158

#define            _AL_CONF_TOKEN_DELAYED_BA_ENABLED		159
#define            _AL_CONF_TOKEN_MAX_AMSDU_LEN		    	160
#define            _AL_CONF_TOKEN_DSSS_CCK_40		    	161
#define            _AL_CONF_TOKEN_DSSS_CCK_40_ALLOW	    	162
#define            _AL_CONF_TOKEN_DSSS_CCK_40_DENY	    	163
#define            _AL_CONF_TOKEN_40_INTOLERANT_ENABLED		164
#define            _AL_CONF_TOKEN_LSIG_TXOP_ENABLED	    	165
#define            _AL_CONF_TOKEN_FRM_AGRE_ENABLED   		166
#define            _AL_CONF_TOKEN_AMPDU_ENABLED		    	167
#define            _AL_CONF_TOKEN_MAX_AMPDU_LEN  		168
#define            _AL_CONF_TOKEN_MAX_AMPDU_LEN_8		169
#define            _AL_CONF_TOKEN_MAX_AMPDU_LEN_16	    	170
#define            _AL_CONF_TOKEN_MAX_AMPDU_LEN_32	    	171
#define            _AL_CONF_TOKEN_MAX_AMPDU_LEN_64	    	172


#define	           _AL_CONF_TOKEN_GREENFIELD            	173
#define            _AL_CONF_TOKEN_HT_K				174
#define            _AL_CONF_TOKEN_HT_B				175
#define            _AL_CONF_TOKEN_40_ABOVE                      176
#define            _AL_CONF_TOKEN_40_BELOW                      177
#define            _AL_CONF_TOKEN_20                            178

#define		   _AL_CONF_TOKEN_MAX_MPDU_LEN_ENABLED          179
#define            _AL_CONF_TOKEN_SUPPORTED_CHANNEL_WIDTH	180
#define            _AL_CONF_TOKEN_RX_LDPC			181
#define            _AL_CONF_TOKEN_GI_80				182
#define            _AL_CONF_TOKEN_GI_160			183
#define            _AL_CONF_TOKEN_SU_BEAMFORMER_CAPAB		184
#define            _AL_CONF_TOKEN_YES				185
#define            _AL_CONF_TOKEN_NO				186
#define            _AL_CONF_TOKEN_SU_BEAMFORMEE_CAPAB		187
#define            _AL_CONF_TOKEN_BEAMFORMEE_STS_COUNT		188
#define            _AL_CONF_TOKEN_SOUNDING_DIMENSIONS		191
#define            _AL_CONF_TOKEN_MU_BEAMFORMER_CAPAB		192
#define            _AL_CONF_TOKEN_MU_BEAMFORMEE_CAPAB		193
#define            _AL_CONF_TOKEN_VHT_TXOP_PS			194
#define            _AL_CONF_TOKEN_HTC_VHT_CAPAB			195
#define            _AL_CONF_TOKEN_RX_ANT_PATTERN_CONSISTENCY	196
#define            _AL_CONF_TOKEN_TX_ANT_PATTERN_CONSISTENCY	197
#define            _AL_CONF_TOKEN_VHT_OPER_BANDWIDTH		198
#define            _AL_CONF_TOKEN_SEG0_CENTER_FREQ		199
#define            _AL_CONF_TOKEN_SEG1_CENTER_FREQ		200
#define            _AL_CONF_TOKEN_VHT_CAPAB		        201
#define            _AL_CONF_TOKEN_AC                            202
#define            _AL_CONF_TOKEN_BGN                           203
#define            _AL_CONF_TOKEN_AN                            204
#define            _AL_CONF_TOKEN_ANAC                          205
#endif

#define _AL_CONF_TOKEN_MGMT_GW_SERVER_ADDR							206
#define _AL_CONF_TOKEN_MGMT_GW_ENABLE									207
#define _AL_CONF_TOKEN_MGMT_GW_CERTIFICATES							208

#define _AL_CONF_TOKEN_DISABLE_BACKHAUL_SECURITY					209
#define            _AL_CONF_TOKEN_N_2_4_GHZ                            210
#define            _AL_CONF_TOKEN_N_5_GHZ                             211
#define            _AL_CONF_TOKEN_SIG_THESHOLD                  212
/**
 * The various states or modes of the parser.
 */

#define _AL_CONF_PARSE_MODE_NONE                        -1
#define _AL_CONF_PARSE_MODE_NAME                        0
#define _AL_CONF_PARSE_MODE_MESH_ID                     1
#define _AL_CONF_PARSE_MODE_MESH_IMCP_KEY               2
#define _AL_CONF_PARSE_MODE_ESSID                       3
#define _AL_CONF_PARSE_MODE_RTS_TH                      4
#define _AL_CONF_PARSE_MODE_FRAG_TH                     5
#define _AL_CONF_PARSE_MODE_BCN_INT                     6
#define _AL_CONF_PARSE_MODE_DCA                         7
#define _AL_CONF_PARSE_MODE_STAY_AWAKE_COUNT            8
#define _AL_CONF_PARSE_MODE_BRIDGE_AGEING_TIME          9
#define _AL_CONF_PARSE_MODE_WDS_ENCRYPTED               10
#define _AL_CONF_PARSE_MODE_MODEL                       11

#define _AL_CONF_PARSE_MODE_IF_INFO                     12
#define _AL_CONF_PARSE_MODE_IF_INFO_ITEM                13
#define _AL_CONF_PARSE_MODE_IF_INFO_MED_TYPE            14
#define _AL_CONF_PARSE_MODE_IF_INFO_MED_SUB_TYPE        15
#define _AL_CONF_PARSE_MODE_IF_INFO_USAGE_TYPE          16
#define _AL_CONF_PARSE_MODE_IF_INFO_CHANNEL             17
#define _AL_CONF_PARSE_MODE_IF_INFO_BONDING             18
#define _AL_CONF_PARSE_MODE_IF_INFO_DCA                 19
#define _AL_CONF_PARSE_MODE_IF_INFO_TXPOWER             20
#define _AL_CONF_PARSE_MODE_IF_INFO_DCA_LIST            21
#define _AL_CONF_PARSE_MODE_IF_INFO_SERVICE             22
#define _AL_CONF_PARSE_MODE_IF_INFO_ACK_TIME            23

#define _AL_CONF_PARSE_MODE_VLAN_INFO                   24
#define _AL_CONF_PARSE_MODE_VLAN_INFO_ITEM              25
#define _AL_CONF_PARSE_MODE_VLAN_INFO_TAIL              26

#define _AL_CONF_PARSE_MODE_PREFERRED_PARENT            27
#define _AL_CONF_PARSE_MODE_SIGNAL_MAP                  28
#define _AL_CONF_PARSE_MODE_HBI                         29
#define _AL_CONF_PARSE_MODE_HBMC                        30
#define _AL_CONF_PARSE_MODE_HOP_COST                    31
#define _AL_CONF_PARSE_MODE_MAH                         32
#define _AL_CONF_PARSE_MODE_LASI                        33
#define _AL_CONF_PARSE_MODE_CRT                         34
#define _AL_CONF_PARSE_MODE_IF_INFO_TAIL                35
#define _AL_CONF_PARSE_MODE_DESCRIPTION                 36

#define _AL_CONF_PARSE_MODE_GPS_X_COORDINATE            37
#define _AL_CONF_PARSE_MODE_GPS_Y_COORDINATE            38

#define _AL_CONF_PARSE_MODE_REGULATORY_DOMAIN           39
#define _AL_CONF_PARSE_MODE_COUNTRY_CODE                40

#define _AL_CONF_PARSE_MODE_FCC_CERTIFIED_OPERATION     41
#define _AL_CONF_PARSE_MODE_ETSI_CERTIFIED_OPERATION    42
#define _AL_CONF_PARSE_MODE_DS_TX_RATE                  43
#define _AL_CONF_PARSE_MODE_DS_TX_POWER                 44

#define _AL_CONF_PARSE_MODE_CONFIG_SQNR                 45

#define _AL_CONF_PARSE_MODE_EFFISTREAM                  46

#define _AL_CONF_PARSE_MODE_GPS                         47
#define _AL_CONF_PARSE_MODE_DHCP                        48

#define _AL_CONF_PARSE_MODE_LOGMON                      49

#define _AL_CONF_PARSE_MODE_USE_VIRT_IF                 50

#define _AL_CONF_PARSE_MODE_OPTIONS                     51

#define _AL_CONF_PARSE_MODE_FAILOVER_ETH_ENABLED        52

#define _AL_CONF_PARSE_MODE_POWER_ON_DEFAULT            53
#define _AL_CONF_PARSE_MODE_SERVER_ADDR                 54

#define _AL_CONF_PARSE_MODE_MGMT_GW_SERVER_ADDR			  55
#define _AL_CONF_PARSE_MODE_MGMT_GW_ENABLE				  56
#define _AL_CONF_PARSE_MODE_MGMT_GW_CERTIFICATES		  57

#define _AL_CONF_PARSE_MODE_DISABLE_BACKHAUL_SECURITY	  58
#define _AL_CONF_PARSE_MODE_SIG_THRESHOLD               59 

struct _al_conf_data
{
   char                          name[_AL_CONF_MAX_TOKEN_SIZE + 1];
   char                          description[_AL_CONF_MAX_TOKEN_SIZE + 1];
   char                          mesh_id[_AL_CONF_MAX_TOKEN_SIZE + 1];
   char                          mesh_imcp_key[16 + 1];
   char                          essid[AL_802_11_ESSID_MAX_SIZE + 1];
   int                           rts_th;
   int                           frag_th;
   int                           beacon_interval;
   int                           dynamic_channel_allocation;
   int                           stay_awake_count;
   int                           bridge_ageing_time;
   int                           wds_encrypted;
   char                          model[32];
   char                          gps_x_coordinate[32];
   char                          gps_y_coordinate[32];
   int                           failover_enabled;
   int                           disable_backhaul_security;
   int                           power_on_default;
   unsigned char                 server_ip_addr[128];
	unsigned char						mgmt_gw_addr[64];
	int 									mgmt_gw_enable;
	unsigned char						mgmt_gw_certificates[64];
   int                           fcc_certified_operation;
   int                           etsi_certified_operation;
   int                           regulatory_domain;
   int                           country_code;
   int                           ds_tx_rate;
   int                           ds_tx_power;
   unsigned int                  config_sqnr;

   int                           if_count;
   al_conf_if_info_t             *if_info;

   int                           vlan_count;
   al_conf_vlan_info_t           *vlan_info;

   al_conf_effistream_criteria_t *criteria_root;

   unsigned char                 preferred_parent[6];
   unsigned char                 signal_map[8];
   int                           heartbeat_interval;
   int                           heartbeat_miss_count;
   int                           hop_cost;
   int                           max_allowable_hops;
   int                           las_interval;
   int                           change_resistance;
   int                           use_virt_if;

   char                          *parser_buffer;
   char                          *pos;
   char                          current_token[_AL_CONF_MAX_TOKEN_SIZE + 1];
   char                          *curent_token_pos;
   int                           current_token_code;
   int                           line_number;
   int                           column_number;
   int                           parse_mode;
   char                          *mesh_imcp_key_buffer;                                         /** un-uudecoded, un-aesdecrypted */
   int                           security_heirarchy_level;

   al_conf_dhcp_info_t           dhcp_info;
   al_conf_gps_info_t            gps_info;
   al_conf_logmon_info_t         logmon_info;

   al_conf_option_t              *option_info;
   int                           sig_threshold;
};

typedef struct _al_conf_data   _al_conf_data_t;


#define _al_offsetof_(TYPE, MEMBER)     ((size_t)&((TYPE *)0)->MEMBER)
#define _INT_PARSE_DATA_ENTRY(field)    {# field, _al_offsetof_(_al_conf_data_t, field) }
#define _INT_PARSE_DATA_NULL_ENTRY()    { NULL, 0 }

#define _MAP_TOKEN_CODE_TO_PARSE_MODE(tc, pm) \
case tc:                                      \
   conf_data->parse_mode = pm;                \
   break

#define _BEGIN_MAP_BLOCK(tc) \
   switch (tc) {
#define _END_MAP_BLOCK() \
   }

#define _MAP_TOKEN_CODES(tc)                                                                                             \
   _BEGIN_MAP_BLOCK(tc)                                                                                                  \
   _MAP_TOKEN_CODE_TO_PARSE_MODE(_AL_CONF_TOKEN_NAME, _AL_CONF_PARSE_MODE_NAME);                                         \
   _MAP_TOKEN_CODE_TO_PARSE_MODE(_AL_CONF_TOKEN_MESH_ID, _AL_CONF_PARSE_MODE_MESH_ID);                                   \
   _MAP_TOKEN_CODE_TO_PARSE_MODE(_AL_CONF_TOKEN_MESH_IMCP_KEY, _AL_CONF_PARSE_MODE_MESH_IMCP_KEY);                       \
   _MAP_TOKEN_CODE_TO_PARSE_MODE(_AL_CONF_TOKEN_ESSID, _AL_CONF_PARSE_MODE_ESSID);                                       \
   _MAP_TOKEN_CODE_TO_PARSE_MODE(_AL_CONF_TOKEN_RTS_TH, _AL_CONF_PARSE_MODE_RTS_TH);                                     \
   _MAP_TOKEN_CODE_TO_PARSE_MODE(_AL_CONF_TOKEN_FRAG_TH, _AL_CONF_PARSE_MODE_FRAG_TH);                                   \
   _MAP_TOKEN_CODE_TO_PARSE_MODE(_AL_CONF_TOKEN_BCN_INT, _AL_CONF_PARSE_MODE_BCN_INT);                                   \
   _MAP_TOKEN_CODE_TO_PARSE_MODE(_AL_CONF_TOKEN_STAY_AWAKE_COUNT, _AL_CONF_PARSE_MODE_STAY_AWAKE_COUNT);                 \
   _MAP_TOKEN_CODE_TO_PARSE_MODE(_AL_CONF_TOKEN_BRIDGE_AGEING_TIME, _AL_CONF_PARSE_MODE_BRIDGE_AGEING_TIME);             \
   _MAP_TOKEN_CODE_TO_PARSE_MODE(_AL_CONF_TOKEN_WDS_ENCRYPTED, _AL_CONF_PARSE_MODE_WDS_ENCRYPTED);                       \
   _MAP_TOKEN_CODE_TO_PARSE_MODE(_AL_CONF_TOKEN_MODEL, _AL_CONF_PARSE_MODE_MODEL);                                       \
   _MAP_TOKEN_CODE_TO_PARSE_MODE(_AL_CONF_TOKEN_IF_INFO, _AL_CONF_PARSE_MODE_IF_INFO);                                   \
   _MAP_TOKEN_CODE_TO_PARSE_MODE(_AL_CONF_TOKEN_VLAN_INFO, _AL_CONF_PARSE_MODE_VLAN_INFO);                               \
   _MAP_TOKEN_CODE_TO_PARSE_MODE(_AL_CONF_TOKEN_PREFERRED_PARENT, _AL_CONF_PARSE_MODE_PREFERRED_PARENT);                 \
   _MAP_TOKEN_CODE_TO_PARSE_MODE(_AL_CONF_TOKEN_SIGNAL_MAP, _AL_CONF_PARSE_MODE_SIGNAL_MAP);                             \
   _MAP_TOKEN_CODE_TO_PARSE_MODE(_AL_CONF_TOKEN_HBI, _AL_CONF_PARSE_MODE_HBI);                                           \
   _MAP_TOKEN_CODE_TO_PARSE_MODE(_AL_CONF_TOKEN_HBMC, _AL_CONF_PARSE_MODE_HBMC);                                         \
   _MAP_TOKEN_CODE_TO_PARSE_MODE(_AL_CONF_TOKEN_HOP_COST, _AL_CONF_PARSE_MODE_HOP_COST);                                 \
   _MAP_TOKEN_CODE_TO_PARSE_MODE(_AL_CONF_TOKEN_MAH, _AL_CONF_PARSE_MODE_MAH);                                           \
   _MAP_TOKEN_CODE_TO_PARSE_MODE(_AL_CONF_TOKEN_LASI, _AL_CONF_PARSE_MODE_LASI);                                         \
   _MAP_TOKEN_CODE_TO_PARSE_MODE(_AL_CONF_TOKEN_DCA, _AL_CONF_PARSE_MODE_DCA);                                           \
   _MAP_TOKEN_CODE_TO_PARSE_MODE(_AL_CONF_TOKEN_CRT, _AL_CONF_PARSE_MODE_CRT);                                           \
   _MAP_TOKEN_CODE_TO_PARSE_MODE(_AL_CONF_TOKEN_DESCRIPTION, _AL_CONF_PARSE_MODE_DESCRIPTION);                           \
   _MAP_TOKEN_CODE_TO_PARSE_MODE(_AL_CONF_TOKEN_GPS_X_COORDINATE, _AL_CONF_PARSE_MODE_GPS_X_COORDINATE);                 \
   _MAP_TOKEN_CODE_TO_PARSE_MODE(_AL_CONF_TOKEN_GPS_Y_COORDINATE, _AL_CONF_PARSE_MODE_GPS_Y_COORDINATE);                 \
   _MAP_TOKEN_CODE_TO_PARSE_MODE(_AL_CONF_TOKEN_REGULATORY_DOMAIN, _AL_CONF_PARSE_MODE_REGULATORY_DOMAIN);               \
   _MAP_TOKEN_CODE_TO_PARSE_MODE(_AL_CONF_TOKEN_COUNTRY_CODE, _AL_CONF_PARSE_MODE_COUNTRY_CODE);                         \
   _MAP_TOKEN_CODE_TO_PARSE_MODE(_AL_CONF_TOKEN_FCC_CERTIFIED_OPERATION, _AL_CONF_PARSE_MODE_FCC_CERTIFIED_OPERATION);   \
   _MAP_TOKEN_CODE_TO_PARSE_MODE(_AL_CONF_TOKEN_ETSI_CERTIFIED_OPERATION, _AL_CONF_PARSE_MODE_ETSI_CERTIFIED_OPERATION); \
   _MAP_TOKEN_CODE_TO_PARSE_MODE(_AL_CONF_TOKEN_DS_TX_RATE, _AL_CONF_PARSE_MODE_DS_TX_RATE);                             \
   _MAP_TOKEN_CODE_TO_PARSE_MODE(_AL_CONF_TOKEN_DS_TX_POWER, _AL_CONF_PARSE_MODE_DS_TX_POWER);                           \
   _MAP_TOKEN_CODE_TO_PARSE_MODE(_AL_CONF_TOKEN_CONFIG_SQNR, _AL_CONF_PARSE_MODE_CONFIG_SQNR);                           \
   _MAP_TOKEN_CODE_TO_PARSE_MODE(_AL_CONF_TOKEN_EFFISTREAM, _AL_CONF_PARSE_MODE_EFFISTREAM);                             \
   _MAP_TOKEN_CODE_TO_PARSE_MODE(_AL_CONF_TOKEN_DHCP, _AL_CONF_PARSE_MODE_DHCP);                                         \
   _MAP_TOKEN_CODE_TO_PARSE_MODE(_AL_CONF_TOKEN_GPS, _AL_CONF_PARSE_MODE_GPS);                                           \
   _MAP_TOKEN_CODE_TO_PARSE_MODE(_AL_CONF_TOKEN_LOGMON, _AL_CONF_PARSE_MODE_LOGMON);                                     \
   _MAP_TOKEN_CODE_TO_PARSE_MODE(_AL_CONF_TOKEN_USE_VIRT_IF, _AL_CONF_PARSE_MODE_USE_VIRT_IF);                           \
   _MAP_TOKEN_CODE_TO_PARSE_MODE(_AL_CONF_TOKEN_OPTIONS, _AL_CONF_PARSE_MODE_OPTIONS);                                   \
   _MAP_TOKEN_CODE_TO_PARSE_MODE(_AL_CONF_TOKEN_FAILOVER_ETH, _AL_CONF_PARSE_MODE_FAILOVER_ETH_ENABLED);                 \
   _MAP_TOKEN_CODE_TO_PARSE_MODE(_AL_CONF_TOKEN_POWER_ON_DEFAULT, _AL_CONF_PARSE_MODE_POWER_ON_DEFAULT);                 \
   _MAP_TOKEN_CODE_TO_PARSE_MODE(_AL_CONF_TOKEN_SERVER_ADDR, _AL_CONF_PARSE_MODE_SERVER_ADDR);                           \
   _MAP_TOKEN_CODE_TO_PARSE_MODE(_AL_CONF_TOKEN_MGMT_GW_SERVER_ADDR, _AL_CONF_PARSE_MODE_MGMT_GW_SERVER_ADDR);				 \
   _MAP_TOKEN_CODE_TO_PARSE_MODE(_AL_CONF_TOKEN_MGMT_GW_ENABLE, _AL_CONF_PARSE_MODE_MGMT_GW_ENABLE);							 \
	_MAP_TOKEN_CODE_TO_PARSE_MODE(_AL_CONF_TOKEN_MGMT_GW_CERTIFICATES, _AL_CONF_PARSE_MODE_MGMT_GW_CERTIFICATES);			 \
	_MAP_TOKEN_CODE_TO_PARSE_MODE(_AL_CONF_TOKEN_DISABLE_BACKHAUL_SECURITY, _AL_CONF_PARSE_MODE_DISABLE_BACKHAUL_SECURITY);	\
   _MAP_TOKEN_CODE_TO_PARSE_MODE(_AL_CONF_TOKEN_SIG_THESHOLD, _AL_CONF_PARSE_MODE_SIG_THRESHOLD);                       \
default:                                                                                                                 \
   al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "al_conf.c(%d,%d): ignoring token %s\n",                                   \
                conf_data->line_number,                                                                                  \
                conf_data->column_number,                                                                                \
                conf_data->current_token);                                                                               \
   _END_MAP_BLOCK()

/**
 * The Indices in this array are the PARSE MODES and not TOKEN IDs
 * To add new parse modes, add _INT_PARSE_DATA_NULL_ENTRY's until
 * the new parse mode index is reached.
 */

static struct
{
   const char *name;
   int        offset;
}
_int_parse_data[] =
{
   _INT_PARSE_DATA_NULL_ENTRY(),                                        /* (0) NAME */
   _INT_PARSE_DATA_NULL_ENTRY(),                                        /* (1) MESH_ID */
   _INT_PARSE_DATA_NULL_ENTRY(),                                        /* (2) MESH_IMCP_KEY */
   _INT_PARSE_DATA_NULL_ENTRY(),                                        /* (3) ESSID */
   _INT_PARSE_DATA_ENTRY(rts_th),                                       /* (4) */
   _INT_PARSE_DATA_ENTRY(frag_th),                                      /* (5) */
   _INT_PARSE_DATA_ENTRY(beacon_interval),                              /* (6) */
   _INT_PARSE_DATA_ENTRY(dynamic_channel_allocation),                   /* (7) */
   _INT_PARSE_DATA_ENTRY(stay_awake_count),                             /* (8) */
   _INT_PARSE_DATA_ENTRY(bridge_ageing_time),                           /* (9) */
   _INT_PARSE_DATA_ENTRY(wds_encrypted),                                /* (10) */
   _INT_PARSE_DATA_NULL_ENTRY(),                                        /* (11) MODEL */
   _INT_PARSE_DATA_NULL_ENTRY(),                                        /* (12) IF_INFO */
   _INT_PARSE_DATA_NULL_ENTRY(),                                        /* (13) IF_INFO_ITEM */
   _INT_PARSE_DATA_NULL_ENTRY(),                                        /* (14) IF_INFO_MED_TYPE */
   _INT_PARSE_DATA_NULL_ENTRY(),                                        /* (15) IF_INFO_MED_SUB_TYPE */
   _INT_PARSE_DATA_NULL_ENTRY(),                                        /* (16) IF_INFO_USAGE_TYPE */
   _INT_PARSE_DATA_NULL_ENTRY(),                                        /* (17) IF_INFO_CHANNEL */
   _INT_PARSE_DATA_NULL_ENTRY(),                                        /* (18) IF_INFO_BONDING*/
   _INT_PARSE_DATA_NULL_ENTRY(),                                        /* (19) IF_INFO_DCA*/
   _INT_PARSE_DATA_NULL_ENTRY(),                                        /* (20) IF_INFO_TXPOWER*/
   _INT_PARSE_DATA_NULL_ENTRY(),                                        /* (21) IF_INFO_DCA_LIST*/
   _INT_PARSE_DATA_NULL_ENTRY(),                                        /* (22) IF_INFO_SERVICE*/
   _INT_PARSE_DATA_NULL_ENTRY(),                                        /* (23) IF_INFO_ACK_TIME*/
   _INT_PARSE_DATA_NULL_ENTRY(),                                        /* (24) VLAN_INFO */
   _INT_PARSE_DATA_NULL_ENTRY(),                                        /* (25) VLAN_INFO_ITEM */
   _INT_PARSE_DATA_NULL_ENTRY(),                                        /* (26) VLAN_INFO_TAIL */
   _INT_PARSE_DATA_NULL_ENTRY(),                                        /* (27) PREFERRED PARENT */
   _INT_PARSE_DATA_NULL_ENTRY(),                                        /* (28) SIGNAL MAP */
   _INT_PARSE_DATA_ENTRY(heartbeat_interval),                           /* (29) HBI  */
   _INT_PARSE_DATA_ENTRY(heartbeat_miss_count),                         /* (30) HBMC */
   _INT_PARSE_DATA_ENTRY(hop_cost),                                     /* (31) HC */
   _INT_PARSE_DATA_ENTRY(max_allowable_hops),                           /* (32) MAH */
   _INT_PARSE_DATA_ENTRY(las_interval),                                 /* (33) LAS */
   _INT_PARSE_DATA_ENTRY(change_resistance),                            /* (34) CRT */
   _INT_PARSE_DATA_NULL_ENTRY(),                                        /* (35) IF_INFO_TAIL */
   _INT_PARSE_DATA_NULL_ENTRY(),                                        /* (36) DESCRIPTION */
   _INT_PARSE_DATA_NULL_ENTRY(),                                        /* (37) GPS_X_COORDINATE */
   _INT_PARSE_DATA_NULL_ENTRY(),                                        /* (38) GPS_Y_COORDINATE */
   _INT_PARSE_DATA_ENTRY(regulatory_domain),                            /* (39) REGULATORY_DOMAIN */
   _INT_PARSE_DATA_ENTRY(country_code),                                 /* (40) COUNTRY_CODE */
   _INT_PARSE_DATA_ENTRY(fcc_certified_operation),                      /* (41) FCC CERTIFIED */
   _INT_PARSE_DATA_ENTRY(etsi_certified_operation),                     /* (42) ETSI CERTIFIED */
   _INT_PARSE_DATA_ENTRY(ds_tx_rate),                                   /* (43) DS TX RATE */
   _INT_PARSE_DATA_ENTRY(ds_tx_power),                                  /* (44) DS TX POWER */
   _INT_PARSE_DATA_ENTRY(config_sqnr),                                  /* (45) CONFIG SQNR */
   _INT_PARSE_DATA_NULL_ENTRY(),                                        /* (46) EFFISTREAM */
   _INT_PARSE_DATA_NULL_ENTRY(),                                        /* (47) GPS */
   _INT_PARSE_DATA_NULL_ENTRY(),                                        /* (48) DHCP */
   _INT_PARSE_DATA_NULL_ENTRY(),                                        /* (49) LOGMON */
   _INT_PARSE_DATA_ENTRY(use_virt_if),                                  /* (50) USE VIRT IF */
   _INT_PARSE_DATA_NULL_ENTRY(),                                        /* (51) OPTIONS */
   _INT_PARSE_DATA_ENTRY(failover_enabled),                             /* (52) FAILOVER ETHERNET*/
   _INT_PARSE_DATA_ENTRY(power_on_default),                             /* (53) POWER ON DEFAULT */
   _INT_PARSE_DATA_ENTRY(server_ip_addr),                               /* (54) SERVERADRR */
   _INT_PARSE_DATA_ENTRY(mgmt_gw_addr),                               	/* (55) MGMT GW ADDR */
   _INT_PARSE_DATA_ENTRY(mgmt_gw_enable),                              	/* (56) MGMT ENABLE FLAG */
	_INT_PARSE_DATA_ENTRY(mgmt_gw_certificates),									/* (57) MGMT GW CERTIFICATES*/
	_INT_PARSE_DATA_ENTRY(disable_backhaul_security),							/* (58) DISABLE BACKHAUL SECURITY*/
   _INT_PARSE_DATA_ENTRY(sig_threshold)                                /* (59) SIG_THRESHOLD */
};

static struct
{
   const char *token;
   int        code;
}
_token_info[] =
{
   { "name",                       _AL_CONF_TOKEN_NAME                     },
   { "=",                          _AL_CONF_TOKEN_EQUALS                   },
   { "mesh_id",                    _AL_CONF_TOKEN_MESH_ID                  },
   { "mesh_imcp_key",              _AL_CONF_TOKEN_MESH_IMCP_KEY            },
   { "{",                          _AL_CONF_TOKEN_OPEN_BRACE               },
   { "}",                          _AL_CONF_TOKEN_CLOSE_BRACE              },
   { "essid",                      _AL_CONF_TOKEN_ESSID                    },
   { "rts_th",                     _AL_CONF_TOKEN_RTS_TH                   },
   { "frag_th",                    _AL_CONF_TOKEN_FRAG_TH                  },
   { "beacon_interval",            _AL_CONF_TOKEN_BCN_INT                  },
   { "dynamic_channel_allocation", _AL_CONF_TOKEN_DCA                      },
   { "stay_awake_count",           _AL_CONF_TOKEN_STAY_AWAKE_COUNT         },
   { "wds_encrypted",              _AL_CONF_TOKEN_WDS_ENCRYPTED            },
   { "model",                      _AL_CONF_TOKEN_MODEL                    },
   { "bridge_ageing_time",         _AL_CONF_TOKEN_BRIDGE_AGEING_TIME       },
   { "interface_info",             _AL_CONF_TOKEN_IF_INFO                  },
   { "(",                          _AL_CONF_TOKEN_OPEN_BRACK               },
   { ")",                          _AL_CONF_TOKEN_CLOSE_BRACK              },
   { "medium_type",                _AL_CONF_TOKEN_MEDIUM_TYPE              },
   { "e",                          _AL_CONF_TOKEN_E                        },
   { "w",                          _AL_CONF_TOKEN_W                        },
   { "v",                          _AL_CONF_TOKEN_V                        },
   { ",",                          _AL_CONF_TOKEN_COMMA                    },
   { "medium_sub_type",            _AL_CONF_TOKEN_MEDIUM_SUB_TYPE          },
   { "a",                          _AL_CONF_TOKEN_A                        },
   { "b",                          _AL_CONF_TOKEN_B                        },
   { "g",                          _AL_CONF_TOKEN_G                        },
   { "bg",                         _AL_CONF_TOKEN_BG                       },
   { "x",                          _AL_CONF_TOKEN_X                        },
   { "usage_type",                 _AL_CONF_TOKEN_USAGE_TYPE               },
   { "ds",                         _AL_CONF_TOKEN_DS                       },
   { "wm",                         _AL_CONF_TOKEN_WM                       },
   { "pmon",                       _AL_CONF_TOKEN_PMON                     },
   { "amon",                       _AL_CONF_TOKEN_AMON                     },
   { "channel",                    _AL_CONF_TOKEN_CHANNEL                  },
   { "bonding",                    _AL_CONF_TOKEN_BONDING                  },
   { "s",                          _AL_CONF_TOKEN_S                        },
   { "d",                          _AL_CONF_TOKEN_D                        },
   { "dca",                        _AL_CONF_TOKEN_DCA                      },
   { "txpower",                    _AL_CONF_TOKEN_TXPOWER                  },
   { "dca_list",                   _AL_CONF_TOKEN_DCA_LIST                 },
   { "service",                    _AL_CONF_TOKEN_SERVICE                  },
   { "ack_timeout",                _AL_CONF_TOKEN_ACK_TIMEOUT              },
   { "preferred_parent",           _AL_CONF_TOKEN_PREFERRED_PARENT         },
   { ":",                          _AL_CONF_TOKEN_COLON                    },
   { "signal_map",                 _AL_CONF_TOKEN_SIGNAL_MAP               },
   { "heartbeat_interval",         _AL_CONF_TOKEN_HBI                      },
   { "heartbeat_miss_count",       _AL_CONF_TOKEN_HBMC                     },
   { "hop_cost",                   _AL_CONF_TOKEN_HOP_COST                 },
   { "max_allowable_hops",         _AL_CONF_TOKEN_MAH                      },
   { "las_interval",               _AL_CONF_TOKEN_LASI                     },
   { "change_resistance",          _AL_CONF_TOKEN_CRT                      },
   { "long",                       _AL_CONF_TOKEN_LONG                     },
   { "short",                      _AL_CONF_TOKEN_SHORT                    },
   { "preamble_type",              _AL_CONF_TOKEN_PREAMBLE_TYPE            },
   { "slot_time_type",             _AL_CONF_TOKEN_SLOT_TIME_TYPE           },
   { "security",                   _AL_CONF_TOKEN_SECURITY                 },
   { "none",                       _AL_CONF_TOKEN_SECURITY_NONE            },
   { "wep",                        _AL_CONF_TOKEN_WEP                      },
   { "rsn_psk",                    _AL_CONF_TOKEN_RSN_PSK                  },
   { "rsn_radius",                 _AL_CONF_TOKEN_RSN_RADIUS               },
   { "enabled",                    _AL_CONF_TOKEN_ENABLED                  },
   { "strength",                   _AL_CONF_TOKEN_STRENGTH                 },
   { "key_index",                  _AL_CONF_TOKEN_KEY_INDEX                },
   { "keys",                       _AL_CONF_TOKEN_KEYS                     },
   { "key",                        _AL_CONF_TOKEN_KEY                      },
   { "mode",                       _AL_CONF_TOKEN_MODE                     },
   { "cipher_tkip",                _AL_CONF_TOKEN_CIPHER_TKIP              },
   { "cipher_ccmp",                _AL_CONF_TOKEN_CIPHER_CCMP              },
   { "group_key_renewal",          _AL_CONF_TOKEN_GROUP_KEY_RENEWAL        },
   { "radius_server",              _AL_CONF_TOKEN_RADIUS_SERVER            },
   { "radius_port",                _AL_CONF_TOKEN_RADIUS_PORT              },
   { "radius_secret",              _AL_CONF_TOKEN_RADIUS_SECRET            },
   { ".",                          _AL_CONF_TOKEN_DOT                      },
#if (MDE_80211N_SUPPORT || MDE_80211AC_SUPPORT) 
	{ "ht_capab",                   _AL_CONF_TOKEN_HT_CAPAB                 },
	{ "ht_bandwidth",               _AL_CONF_TOKEN_HT_BANDWIDTH             },
	{ "ldpc",                       _AL_CONF_TOKEN_LDPC_ENABLED             },
	{ "smps",                       _AL_CONF_TOKEN_SMPS                     },
	{ "static",                     _AL_CONF_TOKEN_STATIC                   },
	{ "dynamic",                    _AL_CONF_TOKEN_DYNAMIC                  },
	{ "disabled",                   _AL_CONF_TOKEN_DISABLED                 },
	{ "gi_20",                      _AL_CONF_TOKEN_GI_20                    },
	{ "gi_40",                      _AL_CONF_TOKEN_GI_40                    },
	{ "auto",                       _AL_CONF_TOKEN_GI_AUTO                  },
	{ "tx_stbc",                    _AL_CONF_TOKEN_TX_STBC_ENABLED          },
	{ "rx_stbc",                    _AL_CONF_TOKEN_RX_STBC_ENABLED          },
	{ "delayed_ba",                 _AL_CONF_TOKEN_DELAYED_BA_ENABLED       },
	{ "max_amsdu_len",              _AL_CONF_TOKEN_MAX_AMSDU_LEN            },

	{ "dsss_cck_40",                _AL_CONF_TOKEN_DSSS_CCK_40		},
	{ "allow",                      _AL_CONF_TOKEN_DSSS_CCK_40_ALLOW        },
	{ "deny",                       _AL_CONF_TOKEN_DSSS_CCK_40_DENY	        },
	{ "intolerant",                 _AL_CONF_TOKEN_40_INTOLERANT_ENABLED    },
	{ "lsig_txop",                  _AL_CONF_TOKEN_LSIG_TXOP_ENABLED        },
	{ "frame_aggregation",          _AL_CONF_TOKEN_FRM_AGRE_ENABLED		},
	{ "ampdu_enable",               _AL_CONF_TOKEN_AMPDU_ENABLED		},
	{ "max_ampdu_len",              _AL_CONF_TOKEN_MAX_AMPDU_LEN		},
	{ "8",                          _AL_CONF_TOKEN_MAX_AMPDU_LEN_8		},
	{ "16",                         _AL_CONF_TOKEN_MAX_AMPDU_LEN_16		},
	{ "32",                         _AL_CONF_TOKEN_MAX_AMPDU_LEN_32		},
	{ "64",                         _AL_CONF_TOKEN_MAX_AMPDU_LEN_64		},
	{ "gfmode",                     _AL_CONF_TOKEN_GREENFIELD               },
	{ "K",			        _AL_CONF_TOKEN_HT_K			},
	{ "B",			        _AL_CONF_TOKEN_HT_B			},
	{ "40+",			_AL_CONF_TOKEN_40_ABOVE	        	},
	{ "40-",			_AL_CONF_TOKEN_40_BELOW	        	},
	{ "20",			        _AL_CONF_TOKEN_20			},
	{ "vht_capab",                  _AL_CONF_TOKEN_VHT_CAPAB                },
	{ "max_mpdu_len",               _AL_CONF_TOKEN_MAX_MPDU_LEN_ENABLED     },
	{ "supported_channel_width",    _AL_CONF_TOKEN_SUPPORTED_CHANNEL_WIDTH  },
	{ "rx_ldpc",                    _AL_CONF_TOKEN_RX_LDPC                  },
	{ "gi_80",                      _AL_CONF_TOKEN_GI_80                    },
	{ "gi_160",                     _AL_CONF_TOKEN_GI_160                   },
	{ "su_beamformer_cap",          _AL_CONF_TOKEN_SU_BEAMFORMER_CAPAB      },
	{ "su_beamformee_cap",          _AL_CONF_TOKEN_SU_BEAMFORMEE_CAPAB      },
	{ "beamformee_sts_count",       _AL_CONF_TOKEN_BEAMFORMEE_STS_COUNT     },
	{ "sounding_dimensions",        _AL_CONF_TOKEN_SOUNDING_DIMENSIONS      },
	{ "mu_beamformer_cap",          _AL_CONF_TOKEN_MU_BEAMFORMER_CAPAB      },
	{ "mu_beamformee_cap",          _AL_CONF_TOKEN_MU_BEAMFORMEE_CAPAB      },
	{ "vht_txop_ps",                _AL_CONF_TOKEN_VHT_TXOP_PS              },
	{ "htc_vht_cap",                _AL_CONF_TOKEN_HTC_VHT_CAPAB            },
	{ "rx_ant_pattern_consistency", _AL_CONF_TOKEN_RX_ANT_PATTERN_CONSISTENCY },
	{ "tx_ant_pattern_consistency", _AL_CONF_TOKEN_TX_ANT_PATTERN_CONSISTENCY },
	{ "vht_oper_bandwidth",         _AL_CONF_TOKEN_VHT_OPER_BANDWIDTH       },
	{ "seg0_center_freq",           _AL_CONF_TOKEN_SEG0_CENTER_FREQ         },
	{ "seg1_center_freq",           _AL_CONF_TOKEN_SEG1_CENTER_FREQ         },
	{ "yes",                        _AL_CONF_TOKEN_YES                      },
	{ "no",                         _AL_CONF_TOKEN_NO                       },

	{ "ac",                         _AL_CONF_TOKEN_AC                       },
	{ "bgn",                        _AL_CONF_TOKEN_BGN                      },
	{ "an",                         _AL_CONF_TOKEN_AN                       },
	{ "anac",                       _AL_CONF_TOKEN_ANAC                     },

#endif

   { "vlan_info",                  _AL_CONF_TOKEN_VLAN_INFO                },
   { "default",                    _AL_CONF_TOKEN_VLAN_DEFAULT             },
   { "tag",                        _AL_CONF_TOKEN_VLAN_TAG                 },
   { "untagged",                   _AL_CONF_TOKEN_VLAN_UNTAGGED            },
   { "dot1p_priority",             _AL_CONF_TOKEN_VLAN_DOT1P_PRIORITY      },
   { "dot11e_priority",            _AL_CONF_TOKEN_VLAN_DOT11E_PRIORITY     },
   { "dot11e_enabled",             _AL_CONF_TOKEN_VLAN_DOT11E_ENABLED      },
   { "ip",                         _AL_CONF_TOKEN_VLAN_IP                  },
   { "description",                _AL_CONF_TOKEN_DESCRIPTION              },
   { "gps_x_coordinate",           _AL_CONF_TOKEN_GPS_X_COORDINATE         },
   { "gps_y_coordinate",           _AL_CONF_TOKEN_GPS_Y_COORDINATE         },
   { "regulatory_domain",          _AL_CONF_TOKEN_REGULATORY_DOMAIN        },
   { "country_code",               _AL_CONF_TOKEN_COUNTRY_CODE             },
   { "fcc_certified_operation",    _AL_CONF_TOKEN_FCC_CERTIFIED_OPERATION  },
   { "etsi_certified_operation",   _AL_CONF_TOKEN_ETSI_CERTIFIED_OPERATION },
   { "ds_tx_rate",                 _AL_CONF_TOKEN_DS_TX_RATE               },
   { "ds_tx_power",                _AL_CONF_TOKEN_DS_TX_POWER              },
   { "txrate",                     _AL_CONF_TOKEN_TXRATE                   },
   { "config_sqnr",                _AL_CONF_TOKEN_CONFIG_SQNR              },
   { "hide_ssid",                  _AL_CONF_TOKEN_HIDE_SSID                },
   { "dot11e_if_category",         _AL_CONF_TOKEN_IF_DOT11E_CATEGORY       },
   { "dot11e_if_enabled",          _AL_CONF_TOKEN_IF_DOT11E_ENABLED        },
   { "priv_chan_bw",               _AL_CONF_TOKEN_PRIV_CHANNEL_BANDWIDTH   },
   { "priv_chan_power",            _AL_CONF_TOKEN_PRIV_CHANNEL_MAXPOWER    },
   { "priv_chan_list",             _AL_CONF_TOKEN_PRIV_CHANNEL_LIST        },
   { "priv_chan_ant_max",          _AL_CONF_TOKEN_PRIV_CHANNEL_ANT_MAX     },
   { "priv_chan_ctl",              _AL_CONF_TOKEN_PRIV_CHANNEL_CTL         },
   { "psq",                        _AL_CONF_TOKEN_PSQ                      },
   { "psh",                        _AL_CONF_TOKEN_PSH                      },
   { "psf",                        _AL_CONF_TOKEN_PSF                      },
   { "effistream",                 _AL_CONF_TOKEN_EFFISTREAM               },
   { "[",                          _AL_CONF_TOKEN_LEFT_SQBRACK             },
   { "]",                          _AL_CONF_TOKEN_RIGHT_SQBRACK            },
   { "eth_type",                   _AL_CONF_TOKEN_ETH_TYPE                 },
   { "eth_dst",                    _AL_CONF_TOKEN_ETH_DST                  },
   { "eth_src",                    _AL_CONF_TOKEN_ETH_SRC                  },
   { "ip_tos",                     _AL_CONF_TOKEN_IP_TOS                   },
   { "ip_diffsrv",                 _AL_CONF_TOKEN_IP_DIFFSRV               },
   { "ip_src",                     _AL_CONF_TOKEN_IP_SRC                   },
   { "ip_dst",                     _AL_CONF_TOKEN_IP_DST                   },
   { "ip_proto",                   _AL_CONF_TOKEN_IP_PROTO                 },
   { "udp_src_port",               _AL_CONF_TOKEN_UDP_SRC_PORT             },
   { "udp_dst_port",               _AL_CONF_TOKEN_UDP_DST_PORT             },
   { "udp_length",                 _AL_CONF_TOKEN_UDP_LENGTH               },
   { "tcp_src_port",               _AL_CONF_TOKEN_TCP_SRC_PORT             },
   { "tcp_dst_port",               _AL_CONF_TOKEN_TCP_DST_PORT             },
   { "tcp_length",                 _AL_CONF_TOKEN_TCP_LENGTH               },
   { "rtp_version",                _AL_CONF_TOKEN_RTP_VERSION              },
   { "rtp_payload_type",           _AL_CONF_TOKEN_RTP_PAYLOAD              },
   { "rtp_length",                 _AL_CONF_TOKEN_RTP_LENGTH               },
   { "no_ack",                     _AL_CONF_TOKEN_NO_ACK                   },
   { "dot11e_category",            _AL_CONF_TOKEN_DOT11E_CATEGORY          },
   { "drop",                       _AL_CONF_TOKEN_DROP                     },
   { "txant",                      _AL_CONF_TOKEN_TXANT                    },
   { "bit_rate",                   _AL_CONF_TOKEN_BIT_RATE                 },
   { "gps",                        _AL_CONF_TOKEN_GPS                      },
   { "dhcp",                       _AL_CONF_TOKEN_DHCP                     },
   { "logmon",                     _AL_CONF_TOKEN_LOGMON                   },
   { "input",                      _AL_CONF_TOKEN_INPUT                    },
   { "push_dest",                  _AL_CONF_TOKEN_PUSH_DEST                },
   { "push_int",                   _AL_CONF_TOKEN_PUSH_INT                 },
   { "net_id",                     _AL_CONF_TOKEN_NET_ID                   },
   { "gateway",                    _AL_CONF_TOKEN_GATEWAY                  },
   { "dns",                        _AL_CONF_TOKEN_DNS                      },
   { "mask",                       _AL_CONF_TOKEN_MASK                     },
   { "lease",                      _AL_CONF_TOKEN_LEASE                    },
   { "queued_retry",               _AL_CONF_TOKEN_QUEUED_RETRY             },
   { "use_virt_if",                _AL_CONF_TOKEN_USE_VIRT_IF              },
   { "options",                    _AL_CONF_TOKEN_OPTIONS                  },
   { "ap",                         _AL_CONF_TOKEN_AP                       },
   { "failover_enabled",           _AL_CONF_TOKEN_FAILOVER_ETH             },
   { "power_on_default",           _AL_CONF_TOKEN_POWER_ON_DEFAULT         },
   { "server_ip_addr",             _AL_CONF_TOKEN_SERVER_ADDR              },
	{ "mgmt_gw_addr",		  		 	  _AL_CONF_TOKEN_MGMT_GW_SERVER_ADDR		},
	{ "mgmt_gw_enable",				  _AL_CONF_TOKEN_MGMT_GW_ENABLE				},
	{ "mgmt_gw_certificates",		  _AL_CONF_TOKEN_MGMT_GW_CERTIFICATES		},
	{ "disable_backhaul_security",  _AL_CONF_TOKEN_DISABLE_BACKHAUL_SECURITY	},
	{ "n_2_4GHZ",                          _AL_CONF_TOKEN_N_2_4_GHZ                       },
	{ "n_5GHZ",                          _AL_CONF_TOKEN_N_5_GHZ                        },
    { "signal_threshold",           _AL_CONF_TOKEN_SIG_THESHOLD             }

};

int meshap_is_N_or_AC_supported_interface(int phy_sub_type);
static int _parse(AL_CONTEXT_PARAM_DECL _al_conf_data_t *conf_data);
static int _get_rest_of_line_ex(AL_CONTEXT_PARAM_DECL _al_conf_data_t *conf_data, int end_char);
static int _get_rest_of_line(AL_CONTEXT_PARAM_DECL _al_conf_data_t *conf_data);
static int _get_token(AL_CONTEXT_PARAM_DECL _al_conf_data_t *conf_data);

static int _parse_string_attribute(AL_CONTEXT_PARAM_DECL _al_conf_data_t *conf_data,
                                   const char                            *attribute_name,
                                   char                                  *buffer,
                                   int                                   max_length);

static int _parse_int_attribute(AL_CONTEXT_PARAM_DECL _al_conf_data_t *conf_data);

static void _update_cursor(AL_CONTEXT_PARAM_DECL _al_conf_data_t *conf_data);

static int _parse_security_info(AL_CONTEXT_PARAM_DECL _al_conf_data_t *conf_data, al_security_info_t *security_info);

#if MDE_80211N_SUPPORT 
static int _parse_ht_capab_info(AL_CONTEXT_PARAM_DECL _al_conf_data_t *conf_data, al_ht_capab_t *ht_capab);
static int _parse_fram_agre_info(AL_CONTEXT_PARAM_DECL _al_conf_data_t *conf_data, al_fram_agre_t *frame_aggregation);
#endif
#if MDE_80211AC_SUPPORT 
static int _parse_vht_capab_info(AL_CONTEXT_PARAM_DECL _al_conf_data_t *conf_data, al_vht_capab_t *vht_capab);
#endif

static int _parse_effistream_info(AL_CONTEXT_PARAM_DECL _al_conf_data_t *conf_data);
static int _parse_gps_info(AL_CONTEXT_PARAM_DECL _al_conf_data_t *conf_data);
static int _parse_dhcp_info(AL_CONTEXT_PARAM_DECL _al_conf_data_t *conf_data);
static int _parse_logmon_info(AL_CONTEXT_PARAM_DECL _al_conf_data_t *conf_data);

static int _parse_options(AL_CONTEXT_PARAM_DECL _al_conf_data_t *conf_data);

static int _parse_string_attribute_ex(AL_CONTEXT_PARAM_DECL _al_conf_data_t *conf_data,
                                      const char                            *attribute_name,
                                      char                                  *buffer,
                                      int                                   max_length,
                                      int                                   end_char)
{
   int  ret;
   char *p;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   ret = _get_token(AL_CONTEXT conf_data);
   if (ret == -1)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : unexpected eof file while processing %s : %s<%d>\n", attribute_name, __func__, __LINE__);
      return -1;
   }


   if (conf_data->current_token_code != _AL_CONF_TOKEN_EQUALS)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): token %s was found instead of '=' : %s<%d>\n",
                   conf_data->line_number,
                   conf_data->column_number,
                   conf_data->current_token, __func__, __LINE__);
      return -1;
   }

   if (!end_char)
   {
      ret = _get_rest_of_line(AL_CONTEXT conf_data);
   }
   else
   {
      ret = _get_rest_of_line_ex(AL_CONTEXT conf_data, end_char);
   }

   if (ret == -1)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c: unexpected eof file while processing %s : %s<%d>\n", attribute_name, __func__, __LINE__);
      return -1;
   }

   /**
    * Copy buffer over until comment # is found
    */

   p = conf_data->current_token;

   if (!end_char)
   {
      while (*p != '#' && *p != 0 && *p != '\t' && *p != '\r' && *p != '\n')
      {
         p++;
      }
   }
   else
   {
      while (*p != end_char && *p != 0)
      {
         p++;
      }
   }

   *p = 0;

   strncpy(buffer, conf_data->current_token, max_length);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);

   return 0;
}


static int _parse_string_attribute(AL_CONTEXT_PARAM_DECL _al_conf_data_t *conf_data,
                                   const char                            *attribute_name,
                                   char                                  *buffer,
                                   int                                   max_length)
{
   //int ret ;
   //al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);
   //ret = _parse_string_attribute_ex(AL_CONTEXT conf_data, attribute_name, buffer, max_length, 0);
   //al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"MESH_AP : INFO : ret : %d %s<%d>\n",ret, __func__,__LINE__);
   //al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   //return ret;
   return _parse_string_attribute_ex(AL_CONTEXT conf_data, attribute_name, buffer, max_length, 0);
}


static int _parse_int_attribute(AL_CONTEXT_PARAM_DECL _al_conf_data_t *conf_data)
{
   int  ret;
   int  value;
   char *p;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);
   ret = _get_token(AL_CONTEXT conf_data);
   if (ret == -1)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c: unexpected eof file while processing %s : %s<%d>\n",
                   _int_parse_data[conf_data->parse_mode].name, __func__, __LINE__);
      return -1;
   }
   if (conf_data->current_token_code != _AL_CONF_TOKEN_EQUALS)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): token %s was found instead of '=' : %s<%d>\n",
                   conf_data->line_number,
                   conf_data->column_number,
                   conf_data->current_token, __func__, __LINE__);
      return -1;
   }
   ret = _get_rest_of_line(AL_CONTEXT conf_data);
   if (ret == -1)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c: unexpected eof file while processing %s : %s<%d>\n",
                   _int_parse_data[conf_data->parse_mode].name, __func__, __LINE__);
      return -1;
   }

   /**
    * Copy buffer over until comment # is found
    */

   p = conf_data->current_token;

   while (*p != '#' && *p != 0 && *p != '\t' && *p != ' ' && *p != '\r' && *p != '\n')
   {
      p++;
   }

   *p = 0;

   ret = al_conf_atoi(conf_data->current_token, &value);

   if (ret == -1)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): token %s was found instead of an integer : %s<%d>\n",
                   conf_data->line_number,
                   conf_data->column_number,
                   conf_data->current_token, __func__, __LINE__);
      return -1;
   }

   memcpy(((unsigned char *)conf_data) + _int_parse_data[conf_data->parse_mode].offset,
          &value,
          sizeof(int));

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return 0;
}


static int _parse(AL_CONTEXT_PARAM_DECL _al_conf_data_t *conf_data)
{
   struct _line_list
   {
      char              line[_AL_CONF_MAX_TOKEN_SIZE + 1];
      int               line_length;
      struct _line_list *next;
   };

   typedef struct _line_list   _line_list_t;

   int          ret;
   int          status;
   int          i, j;
   int          val;
   _line_list_t *head;
   _line_list_t *tail;
   int          imcp_key_length;
   char         *p;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

#define _PARSE_INTERMEDIATE_TOKEN(tok, tok_code)                                                                      \
   do {                                                                                                               \
      ret = _get_token(AL_CONTEXT conf_data);                                                                         \
      if (ret == -1) {                                                                                                \
         al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): unexpected eof file while looking for '%c' : %s<%d>\n", \
                      conf_data->line_number,                                                                         \
                      conf_data->column_number,                                                                       \
                      tok, __func__, __LINE__);                                                                                           \
         status = -1;                                                                                                 \
         goto label_ret;                                                                                              \
      }                                                                                                               \
      if (conf_data->current_token_code != tok_code) {                                                                \
         al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): expecting token %c but found %s : %s<%d>\n",            \
                      conf_data->line_number,                                                                         \
                      conf_data->column_number,                                                                       \
                      tok,                                                                                            \
                      conf_data->current_token, __func__, __LINE__);                                                                      \
         status = -1;                                                                                                 \
         goto label_ret;                                                                                              \
      }                                                                                                               \
   } while (0)

#define _FREE_LINE_LIST()               \
   do {                                 \
      tail = head;                      \
      while (tail != NULL) {            \
         head = tail->next;             \
         al_heap_free(AL_CONTEXT tail); \
         tail = head;                   \
      }                                 \
      head = NULL;                      \
      tail = NULL;                      \
   } while (0)


   conf_data->parse_mode = _AL_CONF_PARSE_MODE_NONE;
   status = 0;
   head   = NULL;
   tail   = NULL;

   while (1)
   {
      switch (conf_data->parse_mode)
      {
      case _AL_CONF_PARSE_MODE_NONE:
         ret = _get_token(AL_CONTEXT conf_data);
         if (ret == -1)
         {
			al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : _get_token() failed : %s<%d>\n", __func__, __LINE__);
            goto label_ret;
         }
         _MAP_TOKEN_CODES(conf_data->current_token_code);
         break;

      case _AL_CONF_PARSE_MODE_RTS_TH:
      case _AL_CONF_PARSE_MODE_FRAG_TH:
      case _AL_CONF_PARSE_MODE_BCN_INT:
      case _AL_CONF_PARSE_MODE_DCA:
      case _AL_CONF_PARSE_MODE_STAY_AWAKE_COUNT:
      case _AL_CONF_PARSE_MODE_BRIDGE_AGEING_TIME:
      case _AL_CONF_PARSE_MODE_WDS_ENCRYPTED:
      case _AL_CONF_PARSE_MODE_HBI:
      case _AL_CONF_PARSE_MODE_HBMC:
      case _AL_CONF_PARSE_MODE_HOP_COST:
      case _AL_CONF_PARSE_MODE_MAH:
      case _AL_CONF_PARSE_MODE_LASI:
      case _AL_CONF_PARSE_MODE_CRT:
      case _AL_CONF_PARSE_MODE_REGULATORY_DOMAIN:
      case _AL_CONF_PARSE_MODE_COUNTRY_CODE:
      case _AL_CONF_PARSE_MODE_FCC_CERTIFIED_OPERATION:
      case _AL_CONF_PARSE_MODE_ETSI_CERTIFIED_OPERATION:
      case _AL_CONF_PARSE_MODE_DS_TX_RATE:
      case _AL_CONF_PARSE_MODE_DS_TX_POWER:
      case _AL_CONF_PARSE_MODE_CONFIG_SQNR:
      case _AL_CONF_PARSE_MODE_USE_VIRT_IF:
      case _AL_CONF_PARSE_MODE_FAILOVER_ETH_ENABLED:
      case _AL_CONF_PARSE_MODE_POWER_ON_DEFAULT:
		case _AL_CONF_PARSE_MODE_MGMT_GW_ENABLE:
		case _AL_CONF_PARSE_MODE_DISABLE_BACKHAUL_SECURITY:
      case _AL_CONF_PARSE_MODE_SIG_THRESHOLD:
         ret = _parse_int_attribute(AL_CONTEXT conf_data);
         if (ret != 0)
         {
			al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : _parse_int_attribute(ret : %d) failed : %s<%d>\n", ret, __func__, __LINE__);
            goto label_ret;
         }
         conf_data->parse_mode = _AL_CONF_PARSE_MODE_NONE;
         break;

      case _AL_CONF_PARSE_MODE_SERVER_ADDR:
         ret = _parse_string_attribute(AL_CONTEXT conf_data, "server_ip_addr", conf_data->server_ip_addr, _AL_CONF_MAX_TOKEN_SIZE);
         al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MESH_AP : INFO : READ: server_ip_addr = %s : %s<%d>\n", conf_data->server_ip_addr, __func__, __LINE__);
         if (ret != 0)
         {
			al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : _parse_string_attribute(ret : %d) failed : %s<%d>\n", ret, __func__, __LINE__);
            goto label_ret;
         }
         conf_data->parse_mode = _AL_CONF_PARSE_MODE_NONE;
         break;

      case _AL_CONF_PARSE_MODE_MGMT_GW_SERVER_ADDR:
         ret = _parse_string_attribute(AL_CONTEXT conf_data, "mgmt_gw_addr", conf_data->mgmt_gw_addr, 64);
         al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MESH_AP : INFO : READ: mgmt_gw_addr = %s : %s<%d>\n", conf_data->mgmt_gw_addr, __func__, __LINE__);
         if (ret != 0)
         {
			al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : _parse_string_attribute(ret : %d) failed : %s<%d>\n", ret, __func__, __LINE__);
            goto label_ret;
         }
         conf_data->parse_mode = _AL_CONF_PARSE_MODE_NONE;
			break;

      case _AL_CONF_PARSE_MODE_MGMT_GW_CERTIFICATES:
         ret = _parse_string_attribute(AL_CONTEXT conf_data, "mgmt_gw_certificates", conf_data->mgmt_gw_certificates, 64);
         al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MESH_AP : INFO : READ: mgmt_gw_certificates = %s : %s<%d>\n", conf_data->mgmt_gw_certificates, __func__, __LINE__);
         if (ret != 0)
         {
			al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : _parse_string_attribute(ret : %d) failed : %s<%d>\n", ret, __func__, __LINE__);
            goto label_ret;
         }
         conf_data->parse_mode = _AL_CONF_PARSE_MODE_NONE;
			break;

      case _AL_CONF_PARSE_MODE_NAME:
         ret = _parse_string_attribute(AL_CONTEXT conf_data, "name", conf_data->name, _AL_CONF_MAX_TOKEN_SIZE);
         if (ret != 0)
         {
         al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : _parse_string_attribute() failed Read name : %s : %s<%d>\n", conf_data->name, __func__, __LINE__);
            goto label_ret;
         }
         conf_data->parse_mode = _AL_CONF_PARSE_MODE_NONE;
         break;

      case _AL_CONF_PARSE_MODE_DESCRIPTION:
         ret = _parse_string_attribute(AL_CONTEXT conf_data, "description", conf_data->description, _AL_CONF_MAX_TOKEN_SIZE);
         if (ret != 0)
         {
            al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : _parse_string_attribute() failed Description : %s : %s<%d>\n", conf_data->description, __func__, __LINE__);
            goto label_ret;
         }
         conf_data->parse_mode = _AL_CONF_PARSE_MODE_NONE;
         break;

      case _AL_CONF_PARSE_MODE_MESH_ID:
         ret = _parse_string_attribute(AL_CONTEXT conf_data, "mesh_id", conf_data->mesh_id, _AL_CONF_MAX_TOKEN_SIZE);
         if (ret != 0)
         {
            al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : _parse_string_attribute() failed mesh_id : %s : %s<%d>\n", conf_data->mesh_id, __func__, __LINE__);
            goto label_ret;
         }
         conf_data->parse_mode = _AL_CONF_PARSE_MODE_NONE;
         break;

      case _AL_CONF_PARSE_MODE_MESH_IMCP_KEY:
         _FREE_LINE_LIST();
         _PARSE_INTERMEDIATE_TOKEN('{', _AL_CONF_TOKEN_OPEN_BRACE);
         _update_cursor(AL_CONTEXT conf_data);
         imcp_key_length = 0;
         while (1)
         {
            ret = _get_rest_of_line(AL_CONTEXT conf_data);
            if (ret == -1)
            {
               al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c: unexpected eof file while processing mesh imcp key : %s<%d>\n", __func__, __LINE__);
               return -1;
            }
            if (!strncmp(conf_data->current_token, "begin ", 6))
            {
               head = (_line_list_t *)al_heap_alloc(AL_CONTEXT sizeof(_line_list_t)AL_HEAP_DEBUG_PARAM);
               tail = head;
            }
            else if (tail != NULL)
            {
               tail->next = (_line_list_t *)al_heap_alloc(AL_CONTEXT sizeof(_line_list_t)AL_HEAP_DEBUG_PARAM);
               tail       = tail->next;
            }
            if (tail != NULL)
            {
               memset(tail, 0, sizeof(_line_list_t));
               strcpy(tail->line, conf_data->current_token);
               tail->line_length = strlen(tail->line);
               imcp_key_length  += tail->line_length + 1;
            }
            if (!strcmp(conf_data->current_token, "end"))
            {
               break;
            }
         }
         _PARSE_INTERMEDIATE_TOKEN('}', _AL_CONF_TOKEN_CLOSE_BRACE);

         /**
          * Now convert the list into a big string
          */
         conf_data->mesh_imcp_key_buffer = (char *)al_heap_alloc(AL_CONTEXT imcp_key_length + 1 AL_HEAP_DEBUG_PARAM);
         memset(conf_data->mesh_imcp_key_buffer, 0, imcp_key_length + 1);
         tail = head;
         p    = conf_data->mesh_imcp_key_buffer;
         while (tail != NULL)
         {
            memcpy(p, tail->line, tail->line_length);
            p   += tail->line_length;
            *p++ = '\n';
            tail = tail->next;
         }
         _FREE_LINE_LIST();
         conf_data->parse_mode = _AL_CONF_PARSE_MODE_NONE;
         break;

      case _AL_CONF_PARSE_MODE_ESSID:
         ret = _parse_string_attribute(AL_CONTEXT conf_data, "essid", conf_data->essid, AL_802_11_ESSID_MAX_SIZE);
         if (ret != 0)
         {
            al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : _parse_string_attribute() failed\n essid : %s : %s<%d>\n", conf_data->essid, __func__, __LINE__);
            goto label_ret;
         }
         conf_data->parse_mode = _AL_CONF_PARSE_MODE_NONE;
         break;

      case _AL_CONF_PARSE_MODE_MODEL:
         ret = _parse_string_attribute(AL_CONTEXT conf_data, "model", conf_data->model, 32);
         if (ret != 0)
         {
            al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : _parse_string_attribute() failed\n model : %s : %s<%d>\n", conf_data->model, __func__, __LINE__);
            goto label_ret;
         }
         conf_data->parse_mode = _AL_CONF_PARSE_MODE_NONE;
         break;

      case _AL_CONF_PARSE_MODE_GPS_X_COORDINATE:
         ret = _parse_string_attribute(AL_CONTEXT conf_data, "gps_x_coordinate", conf_data->gps_x_coordinate, 32);
         if (ret != 0)
         {
            al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : _parse_string_attribute() failed\n gps_x_coordinate : %s : %s<%d>\n", conf_data->gps_x_coordinate, __func__, __LINE__);
            goto label_ret;
         }
         conf_data->parse_mode = _AL_CONF_PARSE_MODE_NONE;
         break;

      case _AL_CONF_PARSE_MODE_GPS_Y_COORDINATE:
         ret = _parse_string_attribute(AL_CONTEXT conf_data, "gps_y_coordinate", conf_data->gps_y_coordinate, 32);
         if (ret != 0)
         {
            al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : _parse_string_attribute() failed\n gps_y_coordinate : %s : %s<%d>\n", conf_data->gps_y_coordinate, __func__, __LINE__);
            goto label_ret;
         }
         conf_data->parse_mode = _AL_CONF_PARSE_MODE_NONE;
         break;

      case _AL_CONF_PARSE_MODE_IF_INFO:
         _PARSE_INTERMEDIATE_TOKEN('(', _AL_CONF_TOKEN_OPEN_BRACK);
         ret = _get_token(AL_CONTEXT conf_data);
         if (ret == -1)
         {
            al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): unexpected eof file while looking for interface count : %s<%d>\n",
                         conf_data->line_number,
                         conf_data->column_number, __func__, __LINE__);
            status = -1;
            goto label_ret;
         }
         ret = al_conf_atoi(conf_data->current_token, &conf_data->if_count);
         if (ret == -1)
         {
            al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): token %s was found instead of an integer : %s<%d>\n",
                         conf_data->line_number,
                         conf_data->column_number,
                         conf_data->current_token, __func__, __LINE__);
            status = -1;
            goto label_ret;
         }
         conf_data->if_info = (al_conf_if_info_t *)al_heap_alloc(AL_CONTEXT sizeof(al_conf_if_info_t) * conf_data->if_count AL_HEAP_DEBUG_PARAM);
         memset(conf_data->if_info, 0, sizeof(al_conf_if_info_t) * conf_data->if_count);
         _PARSE_INTERMEDIATE_TOKEN(')', _AL_CONF_TOKEN_CLOSE_BRACK);
         _PARSE_INTERMEDIATE_TOKEN('{', _AL_CONF_TOKEN_OPEN_BRACE);
         conf_data->parse_mode = _AL_CONF_PARSE_MODE_IF_INFO_ITEM;
         break;

      case _AL_CONF_PARSE_MODE_IF_INFO_ITEM:
         /** First get the interface name */
         for (i = 0; i < conf_data->if_count; i++)
         {
            /** Read interface name */
            ret = _get_token(AL_CONTEXT conf_data);
            if (ret == -1)
            {
               al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): unexpected eof file while looking for interface name : %s<%d>\n",
                            conf_data->line_number,
                            conf_data->column_number, __func__, __LINE__);
               status = -1;
               goto label_ret;
            }
            /** Initialize essid,rts_th,frag_th,beacon_interval to global values */
            strcpy(conf_data->if_info[i].essid, conf_data->essid);
            conf_data->if_info[i].rts_th          = conf_data->rts_th;
            conf_data->if_info[i].frag_th         = conf_data->frag_th;
            conf_data->if_info[i].beacon_interval = conf_data->beacon_interval;
            strcpy(conf_data->if_info[i].name, conf_data->current_token);
            _PARSE_INTERMEDIATE_TOKEN('{', _AL_CONF_TOKEN_OPEN_BRACE);
            while (1)
            {
               ret = _get_token(AL_CONTEXT conf_data);
               if (ret == -1)
               {
                  al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): unexpected eof file while looking for interface info item : %s<%d>\n", conf_data->line_number, conf_data->column_number, __func__, __LINE__);
                  status = -1;
                  goto label_ret;
               }
               switch (conf_data->current_token_code)
               {
               case _AL_CONF_TOKEN_MEDIUM_TYPE:
                  _PARSE_INTERMEDIATE_TOKEN('=', _AL_CONF_TOKEN_EQUALS);
                  ret = _get_token(AL_CONTEXT conf_data);
                  if (ret == -1)
                  {
                     al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): unexpected eof file while looking for medium type : %s<%d>\n", conf_data->line_number, conf_data->column_number, __func__, __LINE__);
                     status = -1;
                     goto label_ret;
                  }
                  if (conf_data->current_token_code == _AL_CONF_TOKEN_E)
                  {
                     conf_data->if_info[i].phy_type = AL_CONF_IF_PHY_TYPE_ETHERNET;
                  }
                  else if (conf_data->current_token_code == _AL_CONF_TOKEN_W)
                  {
                     conf_data->if_info[i].phy_type = AL_CONF_IF_PHY_TYPE_802_11;
                  }
                  else if (conf_data->current_token_code == _AL_CONF_TOKEN_V)
                  {
                     conf_data->if_info[i].phy_type = AL_CONF_IF_PHY_TYPE_802_11_VIRTUAL;
                  }
                  else
                  {
                     al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): invalid medium type %s specified : %s<%d>\n",
                                  conf_data->line_number,
                                  conf_data->column_number,
                                  conf_data->current_token, __func__, __LINE__);
                     status = -1;
                     goto label_ret;
                  }
                  break;

               case _AL_CONF_TOKEN_MEDIUM_SUB_TYPE:
                  _PARSE_INTERMEDIATE_TOKEN('=', _AL_CONF_TOKEN_EQUALS);
                  ret = _get_token(AL_CONTEXT conf_data);
                  if (ret == -1)
                  {
                     al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): unexpected eof file while looking for medium sub type : %s<%d>\n", conf_data->line_number, conf_data->column_number, __func__, __LINE__);
                     status = -1;
                     goto label_ret;
                  }
                  if (conf_data->current_token_code == _AL_CONF_TOKEN_A)
                  {
                     conf_data->if_info[i].phy_sub_type = AL_CONF_IF_PHY_SUB_TYPE_802_11_A;
                  }
                  else if (conf_data->current_token_code == _AL_CONF_TOKEN_B)
                  {
                     conf_data->if_info[i].phy_sub_type = AL_CONF_IF_PHY_SUB_TYPE_802_11_B;
                  }
                  else if (conf_data->current_token_code == _AL_CONF_TOKEN_G)
                  {
                     conf_data->if_info[i].phy_sub_type = AL_CONF_IF_PHY_SUB_TYPE_802_11_G;
                  }
                  else if (conf_data->current_token_code == _AL_CONF_TOKEN_BG)
                  {
                     conf_data->if_info[i].phy_sub_type = AL_CONF_IF_PHY_SUB_TYPE_802_11_BG;
                  }
                  else if (conf_data->current_token_code == _AL_CONF_TOKEN_PSQ)
                  {
                     conf_data->if_info[i].phy_sub_type = AL_CONF_IF_PHY_SUB_TYPE_802_11_PSQ;
                  }
                  else if (conf_data->current_token_code == _AL_CONF_TOKEN_PSH)
                  {
                     conf_data->if_info[i].phy_sub_type = AL_CONF_IF_PHY_SUB_TYPE_802_11_PSH;
                  }
                  else if (conf_data->current_token_code == _AL_CONF_TOKEN_PSF)
                  {
                     conf_data->if_info[i].phy_sub_type = AL_CONF_IF_PHY_SUB_TYPE_802_11_PSF;
                  }
                  else if (conf_data->current_token_code == _AL_CONF_TOKEN_X)
                  {
                     conf_data->if_info[i].phy_sub_type = AL_CONF_IF_PHY_SUB_TYPE_IGNORE;
                  }
#if (MDE_80211N_SUPPORT || MDE_80211AC_SUPPORT)
								else if (conf_data->current_token_code == _AL_CONF_TOKEN_N_2_4_GHZ)
								{
									conf_data->if_info[i].phy_sub_type = AL_CONF_IF_PHY_SUB_TYPE_802_11_N_2_4GHZ;
								}
								else if (conf_data->current_token_code == _AL_CONF_TOKEN_N_5_GHZ)
								{
									conf_data->if_info[i].phy_sub_type = AL_CONF_IF_PHY_SUB_TYPE_802_11_N_5GHZ;
								}

								else if (conf_data->current_token_code == _AL_CONF_TOKEN_AC)
								{
									conf_data->if_info[i].phy_sub_type = AL_CONF_IF_PHY_SUB_TYPE_802_11_AC;
								}
								else if (conf_data->current_token_code == _AL_CONF_TOKEN_BGN)
								{
									conf_data->if_info[i].phy_sub_type = AL_CONF_IF_PHY_SUB_TYPE_802_11_BGN;
								}
								else if (conf_data->current_token_code == _AL_CONF_TOKEN_AN)
								{
									conf_data->if_info[i].phy_sub_type = AL_CONF_IF_PHY_SUB_TYPE_802_11_AN;
								}
								else if (conf_data->current_token_code == _AL_CONF_TOKEN_ANAC)
								{
									conf_data->if_info[i].phy_sub_type = AL_CONF_IF_PHY_SUB_TYPE_802_11_ANAC;
								}
#endif
                  else
                  {
                     al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): invalid medium sub type %s specified : %s<%d>\n",
                                  conf_data->line_number,
                                  conf_data->column_number,
                                  conf_data->current_token, __func__, __LINE__);
                     status = -1;
                     goto label_ret;
                  }
                  break;


               case _AL_CONF_TOKEN_USAGE_TYPE:
                  _PARSE_INTERMEDIATE_TOKEN('=', _AL_CONF_TOKEN_EQUALS);
                  ret = _get_token(AL_CONTEXT conf_data);
                  if (ret == -1)
                  {
                     al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): unexpected eof file while looking for usage type : %s<%d>\n",
                                  conf_data->line_number,
                                  conf_data->column_number, __func__, __LINE__);
                     status = -1;
                     goto label_ret;
                  }
                  if (conf_data->current_token_code == _AL_CONF_TOKEN_DS)
                  {
                     conf_data->if_info[i].use_type = AL_CONF_IF_USE_TYPE_DS;
                  }
                  else if (conf_data->current_token_code == _AL_CONF_TOKEN_WM)
                  {
                     conf_data->if_info[i].use_type = AL_CONF_IF_USE_TYPE_WM;
                  }
                  else if (conf_data->current_token_code == _AL_CONF_TOKEN_PMON)
                  {
                     conf_data->if_info[i].use_type = AL_CONF_IF_USE_TYPE_PMON;
                  }
                  else if (conf_data->current_token_code == _AL_CONF_TOKEN_AMON)
                  {
                     conf_data->if_info[i].use_type = AL_CONF_IF_USE_TYPE_AMON;
                  }
                  else if (conf_data->current_token_code == _AL_CONF_TOKEN_AP)
                  {
                     conf_data->if_info[i].use_type = AL_CONF_IF_USE_TYPE_AP;
                  }
                  else
                  {
                     al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): invalid usage type %s specified : %s<%d>\n",
                                  conf_data->line_number,
                                  conf_data->column_number,
                                  conf_data->current_token, __func__, __LINE__);
                     status = -1;
                     goto label_ret;
                  }
                  break;

               case _AL_CONF_TOKEN_CHANNEL:
                  _PARSE_INTERMEDIATE_TOKEN('=', _AL_CONF_TOKEN_EQUALS);
                  ret = _get_token(AL_CONTEXT conf_data);
                  if (ret == -1)
                  {
                     al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): unexpected eof file while looking for channel : %s<%d>\n",
                                  conf_data->line_number,
                                  conf_data->column_number, __func__, __LINE__);
                     status = -1;
                     goto label_ret;
                  }
                  ret = al_conf_atoi(conf_data->current_token, &val);
                  conf_data->if_info[i].wm_channel = val;
                  if (ret == -1)
                  {
                     al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): token %s was found instead of an integer : %s<%d>\n",
                                  conf_data->line_number,
                                  conf_data->column_number,
                                  conf_data->current_token, __func__, __LINE__);
                     status = -1;
                     goto label_ret;
                  }
                  break;

               case _AL_CONF_TOKEN_BONDING:
                  _PARSE_INTERMEDIATE_TOKEN('=', _AL_CONF_TOKEN_EQUALS);
                  ret = _get_token(AL_CONTEXT conf_data);
                  if (ret == -1)
                  {
                     al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): unexpected eof file while looking for usage type : %s<%d>\n",
                                  conf_data->line_number,
                                  conf_data->column_number, __func__, __LINE__);
                     status = -1;
                     goto label_ret;
                  }
                  if (conf_data->current_token_code == _AL_CONF_TOKEN_S)
                  {
                     conf_data->if_info[i].bonding = AL_CONF_IF_BONDING_SINGLE;
                  }
                  else if (conf_data->current_token_code == _AL_CONF_TOKEN_D)
                  {
                     conf_data->if_info[i].bonding = AL_CONF_IF_BONDING_DOUBLE;
                  }
                  else if (conf_data->current_token_code == _AL_CONF_TOKEN_X)
                  {
                     conf_data->if_info[i].bonding = AL_CONF_IF_BONDING_IGNORE;
                  }
                  else
                  {
                     al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): invalid bonding %s specified : %s<%d>\n",
                                  conf_data->line_number,
                                  conf_data->column_number,
                                  conf_data->current_token, __func__, __LINE__);
                     status = -1;
                     goto label_ret;
                  }
                  break;

               case _AL_CONF_TOKEN_TXPOWER:
                  _PARSE_INTERMEDIATE_TOKEN('=', _AL_CONF_TOKEN_EQUALS);
                  ret = _get_token(AL_CONTEXT conf_data);
                  if (ret == -1)
                  {
                     al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): unexpected eof file while looking for txpower : %s<%d>\n",
                                  conf_data->line_number,
                                  conf_data->column_number, __func__, __LINE__);
                     status = -1;
                     goto label_ret;
                  }
                  ret = al_conf_atoi(conf_data->current_token, &val);
                  if (ret == -1)
                  {
                     al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): token %s was found instead of an integer : %s<%d>\n",
                                  conf_data->line_number,
                                  conf_data->column_number,
                                  conf_data->current_token, __func__, __LINE__);
                     status = -1;
                     goto label_ret;
                  }
                  conf_data->if_info[i].txpower = val;
                  break;

               case _AL_CONF_TOKEN_TXRATE:
                  _PARSE_INTERMEDIATE_TOKEN('=', _AL_CONF_TOKEN_EQUALS);
                  ret = _get_token(AL_CONTEXT conf_data);
                  if (ret == -1)
                  {
                     al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): unexpected eof file while looking for txrate : %s<%d>\n",
                                  conf_data->line_number,
                                  conf_data->column_number, __func__, __LINE__);
                     status = -1;
                     goto label_ret;
                  }
                  ret = al_conf_atoi(conf_data->current_token, &val);
                  if (ret == -1)
                  {
                     al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): token %s was found instead of an integer : %s<%d>\n",
                                  conf_data->line_number,
                                  conf_data->column_number,
                                  conf_data->current_token, __func__, __LINE__);
                     status = -1;
                     goto label_ret;
                  }
                  conf_data->if_info[i].txrate = val;
                  break;

               case _AL_CONF_TOKEN_TXANT:
                  _PARSE_INTERMEDIATE_TOKEN('=', _AL_CONF_TOKEN_EQUALS);
                  ret = _get_token(AL_CONTEXT conf_data);
                  if (ret == -1)
                  {
                     al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): unexpected eof file while looking for txant : %s<%d>\n",
                                  conf_data->line_number,
                                  conf_data->column_number, __func__, __LINE__);
                     status = -1;
                     goto label_ret;
                  }
                  ret = al_conf_atoi(conf_data->current_token, &val);
                  if (ret == -1)
                  {
                     al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): token %s was found instead of an integer : %s<%d>\n",
                                  conf_data->line_number,
                                  conf_data->column_number,
                                  conf_data->current_token, __func__, __LINE__);
                     status = -1;
                     goto label_ret;
                  }
                  conf_data->if_info[i].txant = val;
                  break;

               case _AL_CONF_TOKEN_DCA:
                  _PARSE_INTERMEDIATE_TOKEN('=', _AL_CONF_TOKEN_EQUALS);
                  ret = _get_token(AL_CONTEXT conf_data);
                  if (ret == -1)
                  {
                     al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): unexpected eof file while looking for dca flag : %s<%d>\n",
                                  conf_data->line_number,
                                  conf_data->column_number, __func__, __LINE__);
                     status = -1;
                     goto label_ret;
                  }
                  ret = al_conf_atoi(conf_data->current_token, &val);
                  if (ret == -1)
                  {
                     al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): token %s was found instead of an integer : %s<%d>\n",
                                  conf_data->line_number,
                                  conf_data->column_number,
                                  conf_data->current_token, __func__, __LINE__);
                     status = -1;
                     goto label_ret;
                  }
                  conf_data->if_info[i].dca = val;
                  break;


               case _AL_CONF_TOKEN_DCA_LIST:
                  _PARSE_INTERMEDIATE_TOKEN('(', _AL_CONF_TOKEN_OPEN_BRACK);
                  ret = _get_token(AL_CONTEXT conf_data);
                  if (ret == -1)
                  {
                     al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): unexpected eof file while looking for dca_list count : %s<%d>\n", conf_data->line_number, conf_data->column_number, __func__, __LINE__);
                     status = -1;
                     goto label_ret;
                  }
                  ret = al_conf_atoi(conf_data->current_token, &conf_data->if_info[i].dca_list_count);
                  if (ret == -1)
                  {
                     al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): token %s was found instead of an integer : %s<%d>\n",
                                  conf_data->line_number,
                                  conf_data->column_number,
                                  conf_data->current_token, __func__, __LINE__);
                     status = -1;
                     goto label_ret;
                  }
                  if (conf_data->if_info[i].dca_list_count > 32)
                  {
                     al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): dca_list count must be <= 32 : %s<%d>\n",
                                  conf_data->line_number,
                                  conf_data->column_number, __func__, __LINE__);
                     status = -1;
                     goto label_ret;
                  }
                  _PARSE_INTERMEDIATE_TOKEN(')', _AL_CONF_TOKEN_CLOSE_BRACK);
                  _PARSE_INTERMEDIATE_TOKEN('{', _AL_CONF_TOKEN_OPEN_BRACE);
                  for (j = 0; j < conf_data->if_info[i].dca_list_count; j++)
                  {
                     ret = _get_token(AL_CONTEXT conf_data);
                     if (ret == -1)
                     {
                        al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): unexpected eof file while looking for dca_list value : %s<%d>\n", conf_data->line_number, conf_data->column_number, __func__, __LINE__);
                        status = -1;
                        goto label_ret;
                     }
                     ret = al_conf_atoi(conf_data->current_token, &val);
                     if (ret == -1)
                     {
                        al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): token %s was found instead of an integer : %s<%d>\n",
                                     conf_data->line_number,
                                     conf_data->column_number,
                                     conf_data->current_token, __func__, __LINE__);
                        status = -1;
                        goto label_ret;
                     }
                     conf_data->if_info[i].dca_list[j] = val;
                     if (j != conf_data->if_info[i].dca_list_count - 1)
                     {
                        _PARSE_INTERMEDIATE_TOKEN(',', _AL_CONF_TOKEN_COMMA);
                     }
                  }
                  _PARSE_INTERMEDIATE_TOKEN('}', _AL_CONF_TOKEN_CLOSE_BRACE);
                  break;

               case _AL_CONF_TOKEN_SERVICE:
                  _PARSE_INTERMEDIATE_TOKEN('=', _AL_CONF_TOKEN_EQUALS);
                  ret = _get_token(AL_CONTEXT conf_data);
                  if (ret == -1)
                  {
                     al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): unexpected eof file while looking for dca flag : %s<%d>\n",
                                  conf_data->line_number,
                                  conf_data->column_number, __func__, __LINE__);
                     status = -1;
                     goto label_ret;
                  }
                  ret = al_conf_atoi(conf_data->current_token, &val);
                  if (ret == -1)
                  {
                     al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): token %s was found instead of an integer : %s<%d>\n",
                                  conf_data->line_number,
                                  conf_data->column_number,
                                  conf_data->current_token, __func__, __LINE__);
                     status = -1;
                     goto label_ret;
                  }
                  conf_data->if_info[i].service = val;
                  break;

               case _AL_CONF_TOKEN_ACK_TIMEOUT:
                  _PARSE_INTERMEDIATE_TOKEN('=', _AL_CONF_TOKEN_EQUALS);
                  ret = _get_token(AL_CONTEXT conf_data);
                  if (ret == -1)
                  {
                     al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): unexpected eof file while looking for ack_timeout : %s<%d>\n", conf_data->line_number, conf_data->column_number, __func__, __LINE__);
                     status = -1;
                     goto label_ret;
                  }
                  ret = al_conf_atoi(conf_data->current_token, &val);
                  if (ret == -1)
                  {
                     al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): token %s was found instead of an integer : %s<%d>\n",
                                  conf_data->line_number,
                                  conf_data->column_number,
                                  conf_data->current_token, __func__, __LINE__);
                     status = -1;
                     goto label_ret;
                  }
                  conf_data->if_info[i].ack_timeout = val;
                  break;

               case _AL_CONF_TOKEN_PREAMBLE_TYPE:
                  _PARSE_INTERMEDIATE_TOKEN('=', _AL_CONF_TOKEN_EQUALS);
                  ret = _get_token(AL_CONTEXT conf_data);
                  if (ret == -1)
                  {
                     al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): unexpected eof file while looking for preamble type : %s<%d>\n", conf_data->line_number, conf_data->column_number, __func__, __LINE__);
                     status = -1;
                     goto label_ret;
                  }
                  if (conf_data->current_token_code == _AL_CONF_TOKEN_LONG)
                  {
                     conf_data->if_info[i].preamble_type = AL_CONF_IF_PREAMBLE_TYPE_LONG;
                  }
                  else if (conf_data->current_token_code == _AL_CONF_TOKEN_SHORT)
                  {
                     conf_data->if_info[i].preamble_type = AL_CONF_IF_PREAMBLE_TYPE_SHORT;
                  }
                  else
                  {
                     al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): invalid preamble type %s specified : %s<%d>\n",
                                  conf_data->line_number,
                                  conf_data->column_number,
                                  conf_data->current_token, __func__, __LINE__);
                     status = -1;
                     goto label_ret;
                  }
                  break;

               case _AL_CONF_TOKEN_SLOT_TIME_TYPE:
                  _PARSE_INTERMEDIATE_TOKEN('=', _AL_CONF_TOKEN_EQUALS);
                  ret = _get_token(AL_CONTEXT conf_data);
                  if (ret == -1)
                  {
                     al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): unexpected eof file while looking for preamble type : %s<%d>\n", conf_data->line_number, conf_data->column_number, __func__, __LINE__);
                     status = -1;
                     goto label_ret;
                  }
                  if (conf_data->current_token_code == _AL_CONF_TOKEN_LONG)
                  {
                     conf_data->if_info[i].slot_time_type = AL_CONF_IF_SLOT_TIME_TYPE_LONG;
                  }
                  else if (conf_data->current_token_code == _AL_CONF_TOKEN_SHORT)
                  {
                     conf_data->if_info[i].slot_time_type = AL_CONF_IF_SLOT_TIME_TYPE_SHORT;
                  }
                  else
                  {
                     al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): invalid slot time type %s specified : %s<%d>\n",
                                  conf_data->line_number,
                                  conf_data->column_number,
                                  conf_data->current_token, __func__, __LINE__);
                     status = -1;
                     goto label_ret;
                  }
                  break;

               case _AL_CONF_TOKEN_ESSID:
                  ret = _parse_string_attribute_ex(AL_CONTEXT conf_data, "essid", conf_data->if_info[i].essid, AL_802_11_ESSID_MAX_SIZE, ',');
                  if (ret != 0)
                  {
					 al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : _parse_string_attribute_ex() failed essid : %s : %s<%d>\n",conf_data->if_info[i].essid,__func__, __LINE__); 
                     goto label_ret;
                  }
                  break;

               case _AL_CONF_TOKEN_RTS_TH:
                  _PARSE_INTERMEDIATE_TOKEN('=', _AL_CONF_TOKEN_EQUALS);
                  ret = _get_token(AL_CONTEXT conf_data);
                  if (ret == -1)
                  {
                     al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): unexpected eof file while looking for rts_th : %s<%d>\n",
                                  conf_data->line_number,
                                  conf_data->column_number, __func__, __LINE__);
                     status = -1;
                     goto label_ret;
                  }
                  ret = al_conf_atoi(conf_data->current_token, &val);
                  if (ret == -1)
                  {
                     al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): token %s was found instead of an integer : %s<%d>\n",
                                  conf_data->line_number,
                                  conf_data->column_number,
                                  conf_data->current_token, __func__, __LINE__);
                     status = -1;
                     goto label_ret;
                  }
                  conf_data->if_info[i].rts_th = val;
                  break;

               case _AL_CONF_TOKEN_FRAG_TH:
                  _PARSE_INTERMEDIATE_TOKEN('=', _AL_CONF_TOKEN_EQUALS);
                  ret = _get_token(AL_CONTEXT conf_data);
                  if (ret == -1)
                  {
                     al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): unexpected eof file while looking for frag_th : %s<%d>\n",
                                  conf_data->line_number,
                                  conf_data->column_number, __func__, __LINE__);
                     status = -1;
                     goto label_ret;
                  }
                  ret = al_conf_atoi(conf_data->current_token, &val);
                  if (ret == -1)
                  {
                     al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): token %s was found instead of an integer : %s<%d>\n",
                                  conf_data->line_number,
                                  conf_data->column_number,
                                  conf_data->current_token, __func__, __LINE__);
                     status = -1;
                     goto label_ret;
                  }
                  conf_data->if_info[i].frag_th = val;
                  break;

               case _AL_CONF_TOKEN_BCN_INT:
                  _PARSE_INTERMEDIATE_TOKEN('=', _AL_CONF_TOKEN_EQUALS);
                  ret = _get_token(AL_CONTEXT conf_data);
                  if (ret == -1)
                  {
                     al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): unexpected eof file while looking for beacon_interval : %s<%d>\n", conf_data->line_number, conf_data->column_number, __func__, __LINE__);
                     status = -1;
                     goto label_ret;
                  }
                  ret = al_conf_atoi(conf_data->current_token, &val);
                  if (ret == -1)
                  {
                     al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): token %s was found instead of an integer : %s<%d>\n",
                                  conf_data->line_number,
                                  conf_data->column_number,
                                  conf_data->current_token, __func__, __LINE__);
                     status = -1;
                     goto label_ret;
                  }
                  conf_data->if_info[i].beacon_interval = val;
                  break;

               case _AL_CONF_TOKEN_IF_DOT11E_ENABLED:
                  _PARSE_INTERMEDIATE_TOKEN('=', _AL_CONF_TOKEN_EQUALS);
                  ret = _get_token(AL_CONTEXT conf_data);
                  if (ret == -1)
                  {
                     al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): unexpected eof file while looking for dot11e_enabled : %s<%d>\n", conf_data->line_number, conf_data->column_number, __func__, __LINE__);
                     status = -1;
                     goto label_ret;
                  }
                  ret = al_conf_atoi(conf_data->current_token, &val);
                  if (ret == -1)
                  {
                     al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): token %s was found instead of an integer : %s<%d>\n",
                                  conf_data->line_number,
                                  conf_data->column_number,
                                  conf_data->current_token, __func__, __LINE__);
                     status = -1;
                     goto label_ret;
                  }
                  conf_data->if_info[i].dot11e_enabled = val;
                  break;

               case _AL_CONF_TOKEN_IF_DOT11E_CATEGORY:
                  _PARSE_INTERMEDIATE_TOKEN('=', _AL_CONF_TOKEN_EQUALS);
                  ret = _get_token(AL_CONTEXT conf_data);
                  if (ret == -1)
                  {
                     al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): unexpected eof file while looking for if dot11e_category : %s<%d>\n", conf_data->line_number, conf_data->column_number, __func__, __LINE__);
                     status = -1;
                     goto label_ret;
                  }

                  ret = al_conf_atoi(conf_data->current_token, &val);

                  if (ret == -1)
                  {
                     al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): token %s was found instead of an integer : %s<%d>\n",
                                  conf_data->line_number,
                                  conf_data->column_number,
                                  conf_data->current_token, __func__, __LINE__);
                     status = -1;
                     goto label_ret;
                  }

                  if (val == AL_CONF_DOT11E_CATEGORY_TYPE_AC_BK)
                  {
                     conf_data->if_info[i].dot11e_category = val;
                  }
                  else if (val == AL_CONF_DOT11E_CATEGORY_TYPE_AC_BE)
                  {
                     conf_data->if_info[i].dot11e_category = val;
                  }
                  else if (val == AL_CONF_DOT11E_CATEGORY_TYPE_AC_VI)
                  {
                     conf_data->if_info[i].dot11e_category = val;
                  }
                  else if (val == AL_CONF_DOT11E_CATEGORY_TYPE_AC_VO)
                  {
                     conf_data->if_info[i].dot11e_category = val;
                  }
                  else
                  {
                     al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): invalid dot11e_category type %s specified : %s<%d>\n",
                                  conf_data->line_number,
                                  conf_data->column_number,
                                  conf_data->current_token, __func__, __LINE__);
                     status = -1;
                     goto label_ret;
                  }

                  break;

               case _AL_CONF_TOKEN_SECURITY:
                  ret = _parse_security_info(AL_CONTEXT conf_data, &conf_data->if_info[i].security_info);
                  if (ret == -1)
                  {
									al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): unexpected eof file while looking for , or } : %s<%d>\n", conf_data->line_number, conf_data->column_number, __func__, __LINE__);
                     status = -1;
                     goto label_ret;
                  }
                  break;
#if 1 //Sowndarya added				  

							case _AL_CONF_TOKEN_HT_CAPAB:
								ret = _parse_ht_capab_info(AL_CONTEXT conf_data, &conf_data->if_info[i].ht_capab);
								if (ret == -1)
								{
									al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): unexpected eof file while looking for , or } : %s<%d>\n", conf_data->line_number, conf_data->column_number, __func__, __LINE__);
									status = -1;
									goto label_ret;
								}
								break;

							case _AL_CONF_TOKEN_FRM_AGRE_ENABLED:
								ret = _parse_fram_agre_info(AL_CONTEXT conf_data, &conf_data->if_info[i].frame_aggregation);
								if (ret == -1)
								{
									al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): unexpected eof file while looking for , or } : %s<%d>\n", conf_data->line_number, conf_data->column_number, __func__, __LINE__);
									status = -1;
									goto label_ret;
								}
								break;
							case _AL_CONF_TOKEN_VHT_CAPAB:
								ret = _parse_vht_capab_info(AL_CONTEXT conf_data, &conf_data->if_info[i].vht_capab);
								if (ret == -1)
								{
									al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): unexpected eof file while looking for , or } : %s<%d>\n", conf_data->line_number, conf_data->column_number, __func__, __LINE__);
									status = -1;
									goto label_ret;
								}
								break;
#endif				 

               case _AL_CONF_TOKEN_HIDE_SSID:
                  _PARSE_INTERMEDIATE_TOKEN('=', _AL_CONF_TOKEN_EQUALS);
                  ret = _get_token(AL_CONTEXT conf_data);
                  if (ret == -1)
                  {
                     al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): unexpected eof file while looking for hide_ssid : %s<%d>\n",
                                  conf_data->line_number,
                                  conf_data->column_number, __func__, __LINE__);
                     status = -1;
                     goto label_ret;
                  }
                  ret = al_conf_atoi(conf_data->current_token, &val);
                  if (ret == -1)
                  {
                     al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): token %s was found instead of an integer : %s<%d>\n",
                                  conf_data->line_number,
                                  conf_data->column_number,
                                  conf_data->current_token, __func__, __LINE__);
                     status = -1;
                     goto label_ret;
                  }
                  conf_data->if_info[i].hide_ssid = val;
                  break;

               case _AL_CONF_TOKEN_PRIV_CHANNEL_BANDWIDTH:
                  _PARSE_INTERMEDIATE_TOKEN('=', _AL_CONF_TOKEN_EQUALS);
                  ret = _get_token(AL_CONTEXT conf_data);
                  if (ret == -1)
                  {
                     al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): unexpected eof file while looking for bw flag : %s<%d>\n",
                                  conf_data->line_number,
                                  conf_data->column_number, __func__, __LINE__);
                     status = -1;
                     goto label_ret;
                  }
                  ret = al_conf_atoi(conf_data->current_token, &val);
                  if (ret == -1)
                  {
                     al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): token %s was found instead of an integer : %s<%d>\n",
                                  conf_data->line_number,
                                  conf_data->column_number,
                                  conf_data->current_token, __func__, __LINE__);
                     status = -1;
                     goto label_ret;
                  }
                  conf_data->if_info[i].priv_channel_bw = val;
                  break;

               case _AL_CONF_TOKEN_PRIV_CHANNEL_ANT_MAX:
                  _PARSE_INTERMEDIATE_TOKEN('=', _AL_CONF_TOKEN_EQUALS);
                  ret = _get_token(AL_CONTEXT conf_data);
                  if (ret == -1)
                  {
                     al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): unexpected eof file while looking for ant max flag : %s<%d>\n", conf_data->line_number, conf_data->column_number, __func__, __LINE__);
                     status = -1;
                     goto label_ret;
                  }
                  ret = al_conf_atoi(conf_data->current_token, &val);
                  if (ret == -1)
                  {
                     al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): token %s was found instead of an integer : %s<%d>\n",
                                  conf_data->line_number,
                                  conf_data->column_number,
                                  conf_data->current_token, __func__, __LINE__);
                     status = -1;
                     goto label_ret;
                  }
                  conf_data->if_info[i].priv_channel_ant_max = val;
                  break;

               case _AL_CONF_TOKEN_PRIV_CHANNEL_CTL:
                  _PARSE_INTERMEDIATE_TOKEN('=', _AL_CONF_TOKEN_EQUALS);
                  ret = _get_token(AL_CONTEXT conf_data);
                  if (ret == -1)
                  {
                     al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): unexpected eof file while looking for ctl flag : %s<%d>\n",
                                  conf_data->line_number,
                                  conf_data->column_number, __func__, __LINE__);
                     status = -1;
                     goto label_ret;
                  }
                  ret = al_conf_atoi(conf_data->current_token, &val);
                  if (ret == -1)
                  {
                     al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): token %s was found instead of an integer : %s<%d>\n",
                                  conf_data->line_number,
                                  conf_data->column_number,
                                  conf_data->current_token, __func__, __LINE__);
                     status = -1;
                     goto label_ret;
                  }
                  conf_data->if_info[i].priv_channel_ctl = val;
                  break;

               case _AL_CONF_TOKEN_PRIV_CHANNEL_MAXPOWER:
                  _PARSE_INTERMEDIATE_TOKEN('=', _AL_CONF_TOKEN_EQUALS);
                  ret = _get_token(AL_CONTEXT conf_data);
                  if (ret == -1)
                  {
                     al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): unexpected eof file while looking for maxpower flag : %s<%d>\n",conf_data->line_number, conf_data->column_number, __func__, __LINE__);
                     status = -1;
                     goto label_ret;
                  }
                  ret = al_conf_atoi(conf_data->current_token, &val);
                  if (ret == -1)
                  {
                     al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): token %s was found instead of an integer : %s<%d>\n",
                                  conf_data->line_number,
                                  conf_data->column_number,
                                  conf_data->current_token, __func__, __LINE__);
                     status = -1;
                     goto label_ret;
                  }
                  conf_data->if_info[i].priv_channel_max_power = val;
                  break;

               case _AL_CONF_TOKEN_PRIV_CHANNEL_LIST:
                  _PARSE_INTERMEDIATE_TOKEN('(', _AL_CONF_TOKEN_OPEN_BRACK);
                  ret = _get_token(AL_CONTEXT conf_data);
                  if (ret == -1)
                  {
                     al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): unexpected eof file while looking for priv_chan_list count : %s<%d>\n", conf_data->line_number, conf_data->column_number, __func__, __LINE__);
                     status = -1;
                     goto label_ret;
                  }
                  ret = al_conf_atoi(conf_data->current_token, &conf_data->if_info[i].priv_channel_count);
                  if (ret == -1)
                  {
                     al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): token %s was found instead of an integer : %s<%d>\n",
                                  conf_data->line_number,
                                  conf_data->column_number,
                                  conf_data->current_token, __func__, __LINE__);
                     status = -1;
                     goto label_ret;
                  }
                  if (conf_data->if_info[i].priv_channel_count > 32)
                  {
                     al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): priv_channel_count count must be <= 32 : %s<%d>\n",
                                  conf_data->line_number,
                                  conf_data->column_number, __func__, __LINE__);
                     status = -1;
                     goto label_ret;
                  }
                  _PARSE_INTERMEDIATE_TOKEN(')', _AL_CONF_TOKEN_CLOSE_BRACK);
                  _PARSE_INTERMEDIATE_TOKEN('{', _AL_CONF_TOKEN_OPEN_BRACE);
                  for (j = 0; j < conf_data->if_info[i].priv_channel_count; j++)
                  {
                     ret = _get_token(AL_CONTEXT conf_data);
                     if (ret == -1)
                     {
                        al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): unexpected eof file while looking for priv_channel value : %s<%d>\n", conf_data->line_number, conf_data->column_number, __func__, __LINE__);
                        status = -1;
                        goto label_ret;
                     }
                     ret = al_conf_atoi(conf_data->current_token, &val);
                     if (ret == -1)
                     {
                        al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): token %s was found instead of an integer : %s<%d>\n",
                                     conf_data->line_number,
                                     conf_data->column_number,
                                     conf_data->current_token, __func__, __LINE__);
                        status = -1;
                        goto label_ret;
                     }
                     conf_data->if_info[i].priv_frequencies[j] = val;
                     _PARSE_INTERMEDIATE_TOKEN(':', _AL_CONF_TOKEN_COLON);
                     ret = _get_token(AL_CONTEXT conf_data);
                     if (ret == -1)
                     {
                        al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): unexpected eof file while looking for priv_channel value : %s<%d>\n", conf_data->line_number, conf_data->column_number, __func__, __LINE__);
                        status = -1;
                        goto label_ret;
                     }
                     ret = al_conf_atoi(conf_data->current_token, &val);
                     if (ret == -1)
                     {
                        al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): token %s was found instead of an integer : %s<%d>\n",
                                     conf_data->line_number,
                                     conf_data->column_number,
                                     conf_data->current_token, __func__, __LINE__);
                        status = -1;
                        goto label_ret;
                     }
                     conf_data->if_info[i].priv_channel_numbers[j] = val;
                     _PARSE_INTERMEDIATE_TOKEN(':', _AL_CONF_TOKEN_COLON);
                     ret = _get_token(AL_CONTEXT conf_data);
                     if (ret == -1)
                     {
                        al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): unexpected eof file while looking for priv_channel value : %s<%d>\n", conf_data->line_number, conf_data->column_number, __func__, __LINE__);
                        status = -1;
                        goto label_ret;
                     }
                     ret = al_conf_atoi(conf_data->current_token, &val);
                     if (ret == -1)
                     {
                        al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): token %s was found instead of an integer : %s<%d>\n",
                                     conf_data->line_number,
                                     conf_data->column_number,
                                     conf_data->current_token, __func__, __LINE__);
                        status = -1;
                        goto label_ret;
                     }
                     conf_data->if_info[i].priv_channel_flags[j] = val;
                     if (j != conf_data->if_info[i].priv_channel_count - 1)
                     {
                        _PARSE_INTERMEDIATE_TOKEN(',', _AL_CONF_TOKEN_COMMA);
                     }
                  }
                  _PARSE_INTERMEDIATE_TOKEN('}', _AL_CONF_TOKEN_CLOSE_BRACE);
                  break;
               }
               /** Now we expect either a , or a } */
               ret = _get_token(AL_CONTEXT conf_data);
               if (ret == -1)
               {
							al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): unexpected eof file while looking for , or } : %s<%d>\n", conf_data->line_number, conf_data->column_number, __func__, __LINE__);
                  status = -1;
                  goto label_ret;
               }
               if (conf_data->current_token_code == _AL_CONF_TOKEN_COMMA)
               {
                  continue;
               }
               else if (conf_data->current_token_code == _AL_CONF_TOKEN_CLOSE_BRACE)
               {
                  break;
               }
               else
               {
							al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): token %s was found instead of , or } : %s<%d>\n",
                               conf_data->line_number,
                               conf_data->column_number,
                               conf_data->current_token, __func__, __LINE__);
                  status = -1;
                  goto label_ret;
               }
            }
         }
         conf_data->parse_mode = _AL_CONF_PARSE_MODE_IF_INFO_TAIL;
         break;

      case _AL_CONF_PARSE_MODE_IF_INFO_TAIL:
         _PARSE_INTERMEDIATE_TOKEN('}', _AL_CONF_TOKEN_CLOSE_BRACE);
         conf_data->parse_mode = _AL_CONF_PARSE_MODE_NONE;
         break;

      /* vlan info */
      case _AL_CONF_PARSE_MODE_VLAN_INFO:
         _PARSE_INTERMEDIATE_TOKEN('(', _AL_CONF_TOKEN_OPEN_BRACK);
         ret = _get_token(AL_CONTEXT conf_data);
         if (ret == -1)
         {
            al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): unexpected eof file while looking for vlan count : %s<%d>\n",
                         conf_data->line_number,
                         conf_data->column_number, __func__, __LINE__);
            status = -1;
            goto label_ret;
         }
         ret = al_conf_atoi(conf_data->current_token, &conf_data->vlan_count);
         if (ret == -1)
         {
            al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): token %s was found instead of an integer : %s<%d>\n",
                         conf_data->line_number,
                         conf_data->column_number,
                         conf_data->current_token, __func__, __LINE__);
            status = -1;
            goto label_ret;
         }
         conf_data->vlan_info = (al_conf_vlan_info_t *)al_heap_alloc(AL_CONTEXT sizeof(al_conf_vlan_info_t) * conf_data->vlan_count AL_HEAP_DEBUG_PARAM);
         memset(conf_data->vlan_info, 0, sizeof(al_conf_vlan_info_t) * conf_data->vlan_count);
         _PARSE_INTERMEDIATE_TOKEN(')', _AL_CONF_TOKEN_CLOSE_BRACK);
         _PARSE_INTERMEDIATE_TOKEN('{', _AL_CONF_TOKEN_OPEN_BRACE);

         conf_data->parse_mode = _AL_CONF_PARSE_MODE_VLAN_INFO_ITEM;
         break;

      case _AL_CONF_PARSE_MODE_VLAN_INFO_ITEM:

         for (i = 0; i < conf_data->vlan_count; i++)
         {
            /* read vlan name */
            ret = _get_token(AL_CONTEXT conf_data);
            if (ret == -1)
            {
               al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): unexpected eof file while looking for vlan name : %s<%d>\n",
                            conf_data->line_number,
                            conf_data->column_number, __func__, __LINE__);
               status = -1;
               goto label_ret;
            }
            /** Initialize essid,rts_th,frag_th,beacon_interval to global values */
            strcpy(conf_data->vlan_info[i].essid, conf_data->essid);
            conf_data->vlan_info[i].rts_th          = conf_data->rts_th;
            conf_data->vlan_info[i].frag_th         = conf_data->frag_th;
            conf_data->vlan_info[i].beacon_interval = conf_data->beacon_interval;
            conf_data->vlan_info[i].tag             = AL_CONF_VLAN_INFO_UNTAGGED;

            strcpy((char *)conf_data->vlan_info[i].name, conf_data->current_token);


            _PARSE_INTERMEDIATE_TOKEN('{', _AL_CONF_TOKEN_OPEN_BRACE);
            while (1)
            {
               ret = _get_token(AL_CONTEXT conf_data);
               if (ret == -1)
               {
                  al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): unexpected eof file while looking for vlan info item : %s<%d>\n", conf_data->line_number, conf_data->column_number, __func__, __LINE__);
                  status = -1;
                  goto label_ret;
               }
               switch (conf_data->current_token_code)
               {
               case _AL_CONF_TOKEN_VLAN_TAG:
                  _PARSE_INTERMEDIATE_TOKEN('=', _AL_CONF_TOKEN_EQUALS);
                  ret = _get_token(AL_CONTEXT conf_data);
                  if (ret == -1)
                  {
                     al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): unexpected eof file while looking for vlan tag : %s<%d>\n",
                                  conf_data->line_number,
                                  conf_data->column_number, __func__, __LINE__);
                     status = -1;
                     goto label_ret;
                  }

                  if (conf_data->current_token_code == _AL_CONF_TOKEN_VLAN_UNTAGGED)
                  {
                     conf_data->vlan_info[i].tag = AL_CONF_VLAN_INFO_UNTAGGED;
                  }
                  else
                  {
                     ret = al_conf_atoi(conf_data->current_token, &val);
                     conf_data->vlan_info[i].tag = val;
                     if (ret == -1)
                     {
                        al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): token %s was found instead of an integer : %s<%d>\n",
                                     conf_data->line_number,
                                     conf_data->column_number,
                                     conf_data->current_token, __func__, __LINE__);
                        status = -1;
                        goto label_ret;
                     }
                  }
                  break;

               case _AL_CONF_TOKEN_VLAN_DOT1P_PRIORITY:

                  _PARSE_INTERMEDIATE_TOKEN('=', _AL_CONF_TOKEN_EQUALS);
                  ret = _get_token(AL_CONTEXT conf_data);
                  if (ret == -1)
                  {
                     al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): unexpected eof file while looking for vlan dot1p_priority : %s<%d>\n", conf_data->line_number, conf_data->column_number, __func__, __LINE__);
                     status = -1;
                     goto label_ret;
                  }
                  ret = al_conf_atoi(conf_data->current_token, &val);
                  if (ret == -1)
                  {
                     al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): token %s was found instead of an integer : %s<%d>\n",
                                  conf_data->line_number,
                                  conf_data->column_number,
                                  conf_data->current_token, __func__, __LINE__);
                     status = -1;
                     goto label_ret;
                  }
                  conf_data->vlan_info[i].dot1p_priority = val;

                  break;

               case _AL_CONF_TOKEN_VLAN_DOT11E_ENABLED:
                  _PARSE_INTERMEDIATE_TOKEN('=', _AL_CONF_TOKEN_EQUALS);
                  ret = _get_token(AL_CONTEXT conf_data);
                  if (ret == -1)
                  {
                     al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): unexpected eof file while looking for vlan dot11e_enabled : %s<%d>\n", conf_data->line_number, conf_data->column_number, __func__, __LINE__);
                     status = -1;
                     goto label_ret;
                  }
                  ret = al_conf_atoi(conf_data->current_token, &val);
                  if (ret == -1)
                  {
                     al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): token %s was found instead of an integer : %s<%d>\n",
                                  conf_data->line_number,
                                  conf_data->column_number,
                                  conf_data->current_token, __func__, __LINE__);
                     status = -1;
                     goto label_ret;
                  }
                  conf_data->vlan_info[i].dot11e_enabled = val;
                  break;

               case _AL_CONF_TOKEN_VLAN_DOT11E_PRIORITY:

                  _PARSE_INTERMEDIATE_TOKEN('=', _AL_CONF_TOKEN_EQUALS);

                  ret = _get_token(AL_CONTEXT conf_data);
                  if (ret == -1)
                  {
                     al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): unexpected eof file while looking for interface dot11e_category : %s<%d>\n", conf_data->line_number, conf_data->column_number, __func__, __LINE__);

                     status = -1;
                     goto label_ret;
                  }

                  ret = al_conf_atoi(conf_data->current_token, &val);

                  if (ret == -1)
                  {
                     al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): token %s was found instead of an integer : %s<%d>\n",
                                  conf_data->line_number,
                                  conf_data->column_number,
                                  conf_data->current_token, __func__, __LINE__);
                     status = -1;
                     goto label_ret;
                  }

                  if (val == AL_CONF_DOT11E_CATEGORY_TYPE_AC_BK)
                  {
                     conf_data->vlan_info[i].dot11e_category = val;
                  }
                  else if (val == AL_CONF_DOT11E_CATEGORY_TYPE_AC_BE)
                  {
                     conf_data->vlan_info[i].dot11e_category = val;
                  }
                  else if (val == AL_CONF_DOT11E_CATEGORY_TYPE_AC_VI)
                  {
                     conf_data->vlan_info[i].dot11e_category = val;
                  }
                  else if (val == AL_CONF_DOT11E_CATEGORY_TYPE_AC_VO)
                  {
                     conf_data->vlan_info[i].dot11e_category = val;
                  }
                  else
                  {
                     al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): invalid interface dot11e_category type %s specified : %s<%d>\n", conf_data->line_number, conf_data->column_number, conf_data->current_token, __func__, __LINE__);
                     status = -1;
                     goto label_ret;
                  }

                  break;

               case _AL_CONF_TOKEN_VLAN_IP:
                  _PARSE_INTERMEDIATE_TOKEN('=', _AL_CONF_TOKEN_EQUALS);
                  for (j = 0; j < 4; j++)
                  {
                     ret = _get_token(AL_CONTEXT conf_data);
                     if (ret == -1)
                     {
                        al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): unexpected eof file while looking for vlan ip value : %s<%d>\n", conf_data->line_number, conf_data->column_number, __func__, __LINE__);
                        status = -1;
                        goto label_ret;
                     }
                     ret = al_conf_atoi(conf_data->current_token, &val);
                     if (ret == -1)
                     {
                        al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): token %s was found instead of an integer : %s<%d>\n",
                                     conf_data->line_number,
                                     conf_data->column_number,
                                     conf_data->current_token, __func__, __LINE__);
                        status = -1;
                        goto label_ret;
                     }
                     conf_data->vlan_info[i].ip_address[j] = val;
                     if (j != 3)
                     {
                        _PARSE_INTERMEDIATE_TOKEN('.', _AL_CONF_TOKEN_DOT);
                     }
                  }
                  break;

               case _AL_CONF_TOKEN_ESSID:
                  ret = _parse_string_attribute_ex(AL_CONTEXT conf_data, "essid", conf_data->vlan_info[i].essid, AL_802_11_ESSID_MAX_SIZE, ',');
                  if (ret != 0)
                  {
					 al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : _parse_string_attribute_ex() failed : %s<%d>\n", __func__, __LINE__);
                     goto label_ret;
                  }
                  break;

               case _AL_CONF_TOKEN_RTS_TH:
                  _PARSE_INTERMEDIATE_TOKEN('=', _AL_CONF_TOKEN_EQUALS);
                  ret = _get_token(AL_CONTEXT conf_data);
                  if (ret == -1)
                  {
                     al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): unexpected eof file while looking for rts_th : %s<%d>\n",
                                  conf_data->line_number,
                                  conf_data->column_number, __func__, __LINE__);
                     status = -1;
                     goto label_ret;
                  }
                  ret = al_conf_atoi(conf_data->current_token, &val);
                  if (ret == -1)
                  {
                     al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): token %s was found instead of an intege : %s<%d>r\n",
                                  conf_data->line_number,
                                  conf_data->column_number,
                                  conf_data->current_token, __func__, __LINE__);
                     status = -1;
                     goto label_ret;
                  }
                  conf_data->vlan_info[i].rts_th = val;
                  break;

               case _AL_CONF_TOKEN_FRAG_TH:

                  _PARSE_INTERMEDIATE_TOKEN('=', _AL_CONF_TOKEN_EQUALS);
                  ret = _get_token(AL_CONTEXT conf_data);
                  if (ret == -1)
                  {
                     al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): unexpected eof file while looking for frag_th : %s<%d>\n",
                                  conf_data->line_number,
                                  conf_data->column_number, __func__, __LINE__);
                     status = -1;
                     goto label_ret;
                  }
                  ret = al_conf_atoi(conf_data->current_token, &val);
                  if (ret == -1)
                  {
                     al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): token %s was found instead of an integer\n",
                                  conf_data->line_number,
                                  conf_data->column_number,
                                  conf_data->current_token, __func__, __LINE__);
                     status = -1;
                     goto label_ret;
                  }
                  conf_data->vlan_info[i].frag_th = val;
                  break;

               case _AL_CONF_TOKEN_BCN_INT:

                  _PARSE_INTERMEDIATE_TOKEN('=', _AL_CONF_TOKEN_EQUALS);
                  ret = _get_token(AL_CONTEXT conf_data);
                  if (ret == -1)
                  {
                     al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): unexpected eof file while looking for beacon_interval : %s<%d>\n", conf_data->line_number, conf_data->column_number, __func__, __LINE__);
                     status = -1;
                     goto label_ret;
                  }
                  ret = al_conf_atoi(conf_data->current_token, &val);
                  if (ret == -1)
                  {
                     al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): token %s was found instead of an integer : %s<%d>\n",
                                  conf_data->line_number,
                                  conf_data->column_number,
                                  conf_data->current_token, __func__, __LINE__);
                     status = -1;
                     goto label_ret;
                  }
                  conf_data->vlan_info[i].beacon_interval = val;
                  break;

               case _AL_CONF_TOKEN_SERVICE:

                  _PARSE_INTERMEDIATE_TOKEN('=', _AL_CONF_TOKEN_EQUALS);
                  ret = _get_token(AL_CONTEXT conf_data);
                  if (ret == -1)
                  {
                     al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): unexpected eof file while looking for service : %s<%d>\n",
                                  conf_data->line_number,
                                  conf_data->column_number, __func__, __LINE__);
                     status = -1;
                     goto label_ret;
                  }
                  ret = al_conf_atoi(conf_data->current_token, &val);
                  if (ret == -1)
                  {
                     al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): token %s was found instead of an integer : %s<%d>\n",
                                  conf_data->line_number,
                                  conf_data->column_number,
                                  conf_data->current_token,__func__, __LINE__);
                     status = -1;
                     goto label_ret;
                  }
                  conf_data->vlan_info[i].service = val;
                  break;

               case _AL_CONF_TOKEN_TXPOWER:
                  _PARSE_INTERMEDIATE_TOKEN('=', _AL_CONF_TOKEN_EQUALS);
                  ret = _get_token(AL_CONTEXT conf_data);
                  if (ret == -1)
                  {
                     al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): unexpected eof file while looking for txpower : %s<%d>\n",
                                  conf_data->line_number,
                                  conf_data->column_number, __func__, __LINE__);
                     status = -1;
                     goto label_ret;
                  }
                  ret = al_conf_atoi(conf_data->current_token, &val);
                  if (ret == -1)
                  {
                     al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): token %s was found instead of an integer : %s<%d>\n",
                                  conf_data->line_number,
                                  conf_data->column_number,
                                  conf_data->current_token, __func__, __LINE__);
                     status = -1;
                     goto label_ret;
                  }
                  conf_data->vlan_info[i].txpower = val;
                  break;

               case _AL_CONF_TOKEN_TXRATE:
                  _PARSE_INTERMEDIATE_TOKEN('=', _AL_CONF_TOKEN_EQUALS);
                  ret = _get_token(AL_CONTEXT conf_data);
                  if (ret == -1)
                  {
                     al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): unexpected eof file while looking for txrate : %s<%d>\n",
                                  conf_data->line_number,
                                  conf_data->column_number, __func__, __LINE__);
                     status = -1;
                     goto label_ret;
                  }
                  ret = al_conf_atoi(conf_data->current_token, &val);
                  if (ret == -1)
                  {
                     al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): token %s was found instead of an integer : %s<%d>\n",
                                  conf_data->line_number,
                                  conf_data->column_number,
                                  conf_data->current_token, __func__, __LINE__);
                     status = -1;
                     goto label_ret;
                  }
                  conf_data->vlan_info[i].txrate = val;
                  break;

               case _AL_CONF_TOKEN_SECURITY:
                  ret = _parse_security_info(AL_CONTEXT conf_data, &conf_data->vlan_info[i].security_info);
                  if (ret == -1)
                  {
							al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): unexpected eof file while looking for , or } : %s<%d>\n", conf_data->line_number, conf_data->column_number, __func__, __LINE__);
                     status = -1;
                     goto label_ret;
                  }
                  break;
               }                          // switch
                                          /** Now we expect either a , or a } */
               ret = _get_token(AL_CONTEXT conf_data);

               if (ret == -1)
               {
					al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): unexpected eof file while looking for , or } : %s<%d>\n",
                               conf_data->line_number,
                               conf_data->column_number, __func__, __LINE__);
                  status = -1;
                  goto label_ret;
               }
               if (conf_data->current_token_code == _AL_CONF_TOKEN_COMMA)
               {
                  continue;
               }
               else if (conf_data->current_token_code == _AL_CONF_TOKEN_CLOSE_BRACE)
               {
                  break;
               }
               else
               {
					al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): token %s was found instead of , or } : %s<%d>\n",
                               conf_data->line_number,
                               conf_data->column_number,
                               conf_data->current_token, __func__, __LINE__);
                  status = -1;
                  goto label_ret;
               }
            }             // while
         }                // upper for

         conf_data->parse_mode = _AL_CONF_PARSE_MODE_VLAN_INFO_TAIL;
         break;

      case _AL_CONF_PARSE_MODE_VLAN_INFO_TAIL:
         _PARSE_INTERMEDIATE_TOKEN('}', _AL_CONF_TOKEN_CLOSE_BRACE);
         conf_data->parse_mode = _AL_CONF_PARSE_MODE_NONE;
         break;

      /* vlan info end */
      case _AL_CONF_PARSE_MODE_PREFERRED_PARENT:
         _PARSE_INTERMEDIATE_TOKEN('=', _AL_CONF_TOKEN_EQUALS);
         for (i = 0; i < 6; i++)
         {
            ret = _get_token(AL_CONTEXT conf_data);
            if (ret == -1)
            {
               al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): unexpected eof file while looking for preferred parent value : %s<%d>\n", conf_data->line_number, conf_data->column_number, __func__, __LINE__);
               status = -1;
               goto label_ret;
            }
            ret = al_conf_hex_atoi(conf_data->current_token, &val);
            if (ret == -1)
            {
               al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): token %s was found instead of an hex integer : %s<%d>\n",
                            conf_data->line_number,
                            conf_data->column_number,
                            conf_data->current_token, __func__, __LINE__);
               status = -1;
               goto label_ret;
            }
            conf_data->preferred_parent[i] = val;
            if (i != 5)
            {
               _PARSE_INTERMEDIATE_TOKEN(':', _AL_CONF_TOKEN_COLON);
            }
         }
         conf_data->parse_mode = _AL_CONF_PARSE_MODE_NONE;
         break;

      case _AL_CONF_PARSE_MODE_SIGNAL_MAP:
         _PARSE_INTERMEDIATE_TOKEN('=', _AL_CONF_TOKEN_EQUALS);
         for (i = 0; i < 8; i++)
         {
            ret = _get_token(AL_CONTEXT conf_data);
            if (ret == -1)
            {
               al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): unexpected eof file while looking for signal map value : %s<%d>\n",
                            conf_data->line_number,
                            conf_data->column_number, __func__, __LINE__);
               status = -1;
               goto label_ret;
            }
            ret = al_conf_atoi(conf_data->current_token, &val);
            if (ret == -1)
            {
               al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): token %s was found instead of an integer : %s<%d>\n",
                            conf_data->line_number,
                            conf_data->column_number,
                            conf_data->current_token, __func__, __LINE__);
               status = -1;
               goto label_ret;
            }
            conf_data->signal_map[i] = val;
            if (i != 7)
            {
               _PARSE_INTERMEDIATE_TOKEN(',', _AL_CONF_TOKEN_COMMA);
            }
         }
         conf_data->parse_mode = _AL_CONF_PARSE_MODE_NONE;
         break;

      case _AL_CONF_PARSE_MODE_EFFISTREAM:
         ret = _parse_effistream_info(AL_CONTEXT conf_data);
         if (ret == -1)
         {
            al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): error parsing effistream block : %s<%d>\n",
                         conf_data->line_number,
                         conf_data->column_number,
                         conf_data->current_token, __func__, __LINE__);
            status = -1;
            goto label_ret;
         }
         conf_data->parse_mode = _AL_CONF_PARSE_MODE_NONE;
         break;

      case _AL_CONF_PARSE_MODE_GPS:
         ret = _parse_gps_info(AL_CONTEXT conf_data);
         if (ret == -1)
         {
            al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): error parsing gps block : %s<%d>\n",
                         conf_data->line_number,
                         conf_data->column_number,
                         conf_data->current_token, __func__, __LINE__);
            status = -1;
            goto label_ret;
         }
         conf_data->parse_mode = _AL_CONF_PARSE_MODE_NONE;
         break;

      case _AL_CONF_PARSE_MODE_DHCP:
         ret = _parse_dhcp_info(AL_CONTEXT conf_data);
         if (ret == -1)
         {
            al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): error parsing dhcp block : %s<%d>\n",
                         conf_data->line_number,
                         conf_data->column_number,
                         conf_data->current_token, __func__, __LINE__);
            status = -1;
            goto label_ret;
         }
         conf_data->parse_mode = _AL_CONF_PARSE_MODE_NONE;
         break;

      case _AL_CONF_PARSE_MODE_LOGMON:
         ret = _parse_logmon_info(AL_CONTEXT conf_data);
         if (ret == -1)
         {
            al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): error parsing logmon block : %s<%d>\n",
                         conf_data->line_number,
                         conf_data->column_number,
                         conf_data->current_token, __func__, __LINE__);
            status = -1;
            goto label_ret;
         }
         conf_data->parse_mode = _AL_CONF_PARSE_MODE_NONE;
         break;

      case _AL_CONF_PARSE_MODE_OPTIONS:
         ret = _parse_options(AL_CONTEXT conf_data);
         if (ret == -1)
         {
            al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): error parsing options block : %s<%d>\n",
                         conf_data->line_number,
                         conf_data->column_number,
                         conf_data->current_token, __func__, __LINE__);
            status = -1;
            goto label_ret;
         }
         conf_data->parse_mode = _AL_CONF_PARSE_MODE_NONE;
         break;
      }
   }

label_ret:
   _FREE_LINE_LIST();
   return status;

#undef _PARSE_INTERMEDIATE_TOKEN
}


static int _get_rest_of_line_ex(AL_CONTEXT_PARAM_DECL _al_conf_data_t *conf_data, int end_char)
{
   int i;
   int found_char;
   //al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   /**
    * Here we read from current position till end of line,
    * and also identify the token.
    */

   conf_data->curent_token_pos  = conf_data->current_token;
   *conf_data->curent_token_pos = 0;
   found_char = 0;

   while (*conf_data->pos)
   {
      if (!found_char &&
          ((*conf_data->pos == ' ') ||
           (*conf_data->pos == '\t') ||
           (*conf_data->pos == '\r')
          )
          )
      {
         conf_data->pos++;
         continue;
      }
      found_char = 1;
      if (*conf_data->pos == end_char)
      {
         if (end_char == '\n')
         {
            conf_data->line_number++;
            conf_data->column_number = 0;
            conf_data->pos++;
         }
         break;
      }
      *conf_data->curent_token_pos++ = *conf_data->pos++;
   }

   *conf_data->curent_token_pos = 0;

   if (*conf_data->current_token == 0)
   {
	  al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : Invalid current_tok found : %s<%d>\n", __func__, __LINE__);
      return -1;
   }

   conf_data->current_token_code = _AL_CONF_TOKEN_IDENT;

   for (i = 0; i < sizeof(_token_info) / sizeof(*_token_info); i++)
   {
      if (!strcmp(conf_data->current_token, _token_info[i].token))
      {
         conf_data->current_token_code = _token_info[i].code;
         break;
      }
   }
   //al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);

   return 0;
}


static int _get_rest_of_line(AL_CONTEXT_PARAM_DECL _al_conf_data_t *conf_data)
{
   //int ret;
   //al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);
  //ret = _get_rest_of_line_ex(AL_CONTEXT conf_data, '\n');
   //al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"MESH_AP : INFO: ret : %d : %s<%d>\n", ret, __func__,__LINE__);
   //al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   //return ret;
   return  _get_rest_of_line_ex(AL_CONTEXT conf_data, '\n');
}


static int _get_token(AL_CONTEXT_PARAM_DECL _al_conf_data_t *conf_data)
{
   /**
    * Here we skip white spaces, comment lines, etc
    * and return then copy the token onto current_token
    * and update current_token_code. We also update the line_number
    * and column_number
    */

   int inside_comment;
   int token_found;
   int identifier_found;
   int i;

   inside_comment = 0;
   token_found    = 0;

   conf_data->curent_token_pos  = conf_data->current_token;
   identifier_found             = 0;
   *conf_data->curent_token_pos = 0;

   while (*conf_data->pos)
   {
      if (inside_comment)
      {
         if (*conf_data->pos == '\n')
         {
            ++conf_data->line_number;
            conf_data->column_number = 0;
            inside_comment           = 0;
         }
         goto label;
      }
      switch (*conf_data->pos)
      {
      case ' ':
      case '\t':
      case '\r':
         if (identifier_found)
         {
            token_found = 1;
         }
         break;

      case '\n':
         /** Skip but increment line_number */
         ++conf_data->line_number;
         conf_data->column_number = -1;
         if (identifier_found)
         {
            token_found = 1;
            conf_data->pos++;
         }
         break;

      case '#':
         /** From here to end of line skip everything */
         inside_comment = 1;
         break;

      case '=':
      case '(':
      case '{':
      case ',':
      case ')':
      case '}':
      case ':':
      case '.':
      case '[':
      case ']':
         if (!identifier_found)
         {
            *conf_data->curent_token_pos++ = *conf_data->pos++;
         }
         token_found = 1;
         break;

      default:
         *conf_data->curent_token_pos++ = *conf_data->pos;
         identifier_found = 1;
      }
      ++conf_data->column_number;
label:
      if (token_found)
      {
         break;
      }
      conf_data->pos++;
   }

   *conf_data->curent_token_pos = 0;

   if (*conf_data->current_token == 0)
   {
/*#ifdef __KERNEL__
		  printk("DEBUG: LINE : %d token : %s \n",__LINE__,*conf_data->current_token); 
#else
printf("DEBUG:LINE : %d token : %c \n",__LINE__,*conf_data->current_token); 
#endif*/
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : Invalid current token : %s<%d>\n", __func__, __LINE__);
      return -1;
   }

   conf_data->current_token_code = _AL_CONF_TOKEN_IDENT;

   for (i = 0; i < sizeof(_token_info) / sizeof(*_token_info); i++)
   {
      if (!strcmp(conf_data->current_token, _token_info[i].token))
      {
         conf_data->current_token_code = _token_info[i].code;
         break;
      }
   }

   return 0;
}


static void _update_cursor(AL_CONTEXT_PARAM_DECL _al_conf_data_t *conf_data)
{
   int inside_comment;

   /**
    * Here we skip white spaces, comment lines, etc
    * and set the cursor to the beginning of the next token
    * The current token code is not updated. We update the line_number
    * and column_number
    */

   inside_comment = 0;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   while (*conf_data->pos)
   {
      if (inside_comment)
      {
         if (*conf_data->pos == '\n')
         {
            ++conf_data->line_number;
            conf_data->column_number = -1;
            inside_comment           = 0;
         }
      }
      else
      {
         if ((*conf_data->pos != '\t') &&
             (*conf_data->pos != '\r') &&
             (*conf_data->pos != ' ') &&
             (*conf_data->pos != '#') &&
             (*conf_data->pos != '\n'))
         {
            break;
         }
         if (*conf_data->pos == '#')
         {
            inside_comment = 1;
         }
         else if (*conf_data->pos == '\n')
         {
            ++conf_data->line_number;
            conf_data->column_number = -1;
         }
      }
      ++conf_data->column_number;
      ++conf_data->pos;
   }
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
}


#define _PARSE_INTERMEDIATE_TOKEN(tok, tok_code)                                                                      \
   do {                                                                                                               \
      ret = _get_token(AL_CONTEXT conf_data);                                                                         \
      if (ret == -1) {                                                                                                \
         al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "al_conf.c(%d,%d): unexpected eof file while looking for '%c'\n", \
                      conf_data->line_number,                                                                         \
                      conf_data->column_number,                                                                       \
                      tok);                                                                                           \
         status = -1;                                                                                                 \
         goto label_ret;                                                                                              \
      }                                                                                                               \
      if (conf_data->current_token_code != tok_code) {                                                                \
         al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "al_conf.c(%d,%d): expecting token %c but found %s\n",            \
                      conf_data->line_number,                                                                         \
                      conf_data->column_number,                                                                       \
                      tok,                                                                                            \
                      conf_data->current_token);                                                                      \
         status = -1;                                                                                                 \
         goto label_ret;                                                                                              \
      }                                                                                                               \
   } while (0)

#define _GET_TOKEN_AND_CHECK_EOF()                                                        \
   do {                                                                                   \
      ret = _get_token(AL_CONTEXT conf_data);                                             \
      if (ret == -1) {                                                                    \
         al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "al_conf.c(%d,%d): unexpected eof\n", \
                      conf_data->line_number,                                             \
                      conf_data->column_number);                                          \
         status = -1;                                                                     \
         goto label_ret;                                                                  \
      }                                                                                   \
   } while (0)


static void _free_effistream_info(AL_CONTEXT_PARAM_DECL al_conf_effistream_criteria_t *criteria)
{
   al_conf_effistream_criteria_t *next_sibling;

   next_sibling = criteria->next_sibling;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   if (criteria->first_child != NULL)
   {
      _free_effistream_info(AL_CONTEXT criteria->first_child);
   }

   al_heap_free(AL_CONTEXT criteria);

   if (next_sibling != NULL)
   {
      _free_effistream_info(AL_CONTEXT next_sibling);
   }
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
}


static int _parse_action_block(AL_CONTEXT_PARAM_DECL _al_conf_data_t *conf_data,
                               al_conf_effistream_criteria_t         *val_out)
{
   int ret;
   int status;
   int value;

#define _PARSE_INT_ACTION(fld)                                                                                   \
   _PARSE_INTERMEDIATE_TOKEN('=', _AL_CONF_TOKEN_EQUALS);                                                        \
   _GET_TOKEN_AND_CHECK_EOF();                                                                                   \
   ret = al_conf_atoi(conf_data->current_token, &value);                                                         \
   if (ret == -1) {                                                                                              \
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "al_conf.c(%d,%d): token %s was found instead of an integer\n", \
                   conf_data->line_number,                                                                       \
                   conf_data->column_number,                                                                     \
                   conf_data->current_token);                                                                    \
      status = -1;                                                                                               \
      goto label_ret;                                                                                            \
   }                                                                                                             \
   val_out->action.fld = (unsigned char)value;

   status = 0;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   /** Initialize actions to defaults */

   val_out->action.dot11e_category = 0xFF;
   val_out->action.no_ack          = 0;
   val_out->action.drop            = 0;
   val_out->action.bit_rate        = 0;
   val_out->action.queued_retry    = 0;

   _GET_TOKEN_AND_CHECK_EOF();

   while (conf_data->current_token_code != _AL_CONF_TOKEN_RIGHT_SQBRACK)
   {
      switch (conf_data->current_token_code)
      {
      case _AL_CONF_TOKEN_NO_ACK:
         _PARSE_INT_ACTION(no_ack);
         break;

      case _AL_CONF_TOKEN_DOT11E_CATEGORY:
         _PARSE_INT_ACTION(dot11e_category);
         break;

      case _AL_CONF_TOKEN_DROP:
         _PARSE_INT_ACTION(drop);
         break;

      case _AL_CONF_TOKEN_BIT_RATE:
         _PARSE_INT_ACTION(bit_rate);
         break;

      case _AL_CONF_TOKEN_QUEUED_RETRY:
         _PARSE_INT_ACTION(queued_retry);
         break;

      default:
         al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): unexpected token %s found : %s<%d>\n",
                      conf_data->line_number,
                      conf_data->column_number,
                      conf_data->current_token, __func__, __LINE__);
         status = -1;
         goto label_ret;
      }

      _GET_TOKEN_AND_CHECK_EOF();

      if (conf_data->current_token_code == _AL_CONF_TOKEN_COMMA)
      {
         _GET_TOKEN_AND_CHECK_EOF();
         continue;
      }

      if (conf_data->current_token_code != _AL_CONF_TOKEN_RIGHT_SQBRACK)
      {
         al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): unexpected token %s found : %s<%d>\n",
                      conf_data->line_number,
                      conf_data->column_number,
                      conf_data->current_token, __func__, __LINE__);
         status = -1;
         goto label_ret;
      }
   }

#undef _PARSE_INT_ACTION

label_ret:
   
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return status;
}


static int _parse_match_int(AL_CONTEXT_PARAM_DECL _al_conf_data_t *conf_data,
                            al_conf_effistream_criteria_t         *val_out)
{
   int ret;
   int status;
   int value;

   status = 0;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   _GET_TOKEN_AND_CHECK_EOF();

   ret = al_conf_atoi(conf_data->current_token, &value);

   if (ret == -1)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): token %s was found instead of an integer : %s<%d>\n",
                   conf_data->line_number,
                   conf_data->column_number,
                   conf_data->current_token, __func__, __LINE__);
      status = -1;
      goto label_ret;
   }

   val_out->match.value = value;

label_ret:
   
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return status;
}


static int _parse_match_mac_addr(AL_CONTEXT_PARAM_DECL _al_conf_data_t *conf_data,
                                 al_conf_effistream_criteria_t         *val_out)
{
   int ret;
   int status;
   int value;
   int i;

   status = 0;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   memset(&val_out->match.mac_address, 0, sizeof(al_net_addr_t));
   val_out->match.mac_address.length = 6;

   for (i = 0; i < 6; i++)
   {
      ret = _get_token(AL_CONTEXT conf_data);
      if (ret == -1)
      {
         al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): unexpected eof file while looking for MAC ADDR value : %s<%d>\n",
                      conf_data->line_number,
                      conf_data->column_number, __func__, __LINE__);
         status = -1;
         goto label_ret;
      }
      ret = al_conf_hex_atoi(conf_data->current_token, &value);
      if (ret == -1)
      {
         al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): token %s was found instead of an hex integer : %s<%d>\n",
                      conf_data->line_number,
                      conf_data->column_number,
                      conf_data->current_token, __func__, __LINE__);
         status = -1;
         goto label_ret;
      }

      val_out->match.mac_address.bytes[i] = value;

      if (i != 5)
      {
         _PARSE_INTERMEDIATE_TOKEN(':', _AL_CONF_TOKEN_COLON);
      }
   }


label_ret:
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);

   return status;
}


static int _parse_match_ip_addr(AL_CONTEXT_PARAM_DECL _al_conf_data_t *conf_data,
                                al_conf_effistream_criteria_t         *val_out)
{
   int ret;
   int status;
   int value;
   int i;

   status = 0;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   memset(&val_out->match.ip_address, 0, 4);

   for (i = 0; i < 4; i++)
   {
      ret = _get_token(AL_CONTEXT conf_data);

      if (ret == -1)
      {
         al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): unexpected eof file while looking for IP ADDR value : %s<%d>\n",
                      conf_data->line_number,
                      conf_data->column_number, __func__, __LINE__);
         status = -1;
         goto label_ret;
      }

      ret = al_conf_atoi(conf_data->current_token, &value);

      if (ret == -1)
      {
         al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): token %s was found instead of an integer\n",
                      conf_data->line_number,
                      conf_data->column_number,
                      conf_data->current_token, __func__, __LINE__);
         status = -1;
         goto label_ret;
      }

      val_out->match.ip_address[i] = value;

      if (i != 3)
      {
         _PARSE_INTERMEDIATE_TOKEN('.', _AL_CONF_TOKEN_DOT);
      }
   }


label_ret:
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return status;
}


static int _parse_match_range(AL_CONTEXT_PARAM_DECL _al_conf_data_t *conf_data,
                              al_conf_effistream_criteria_t         *val_out)
{
   int ret;
   int status;
   int value;
   int i;

   status = 0;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   for (i = 0; i < 2; i++)
   {
      ret = _get_token(AL_CONTEXT conf_data);

      if (ret == -1)
      {
         al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): unexpected eof file while looking for range value : %s<%d>\n",
                      conf_data->line_number,
                      conf_data->column_number, __func__, __LINE__);
         status = -1;
         goto label_ret;
      }

      ret = al_conf_atoi(conf_data->current_token, &value);

      if (ret == -1)
      {
         al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): token %s was found instead of an integer : %s<%d>\n",
                      conf_data->line_number,
                      conf_data->column_number,
                      conf_data->current_token, __func__, __LINE__);
         status = -1;
         goto label_ret;
      }

      if (i != 1)
      {
         val_out->match.range.min_val = value;
         _PARSE_INTERMEDIATE_TOKEN(':', _AL_CONF_TOKEN_COLON);
      }
      else
      {
         val_out->match.range.max_val = value;
      }
   }

label_ret:
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return status;
}


static int _parse_match_value(AL_CONTEXT_PARAM_DECL _al_conf_data_t *conf_data,
                              al_conf_effistream_criteria_t         *val_out)
{
   int ret;
   int status;
   int prev_token;

#define _PARSE_MATCH(type, id)                                \
   ret = _parse_match ## type(AL_CONTEXT conf_data, val_out); \
   if (ret == -1) {                                           \
      status = -1;                                            \
      goto label_ret;                                         \
   }                                                          \
   val_out->match_id = id;                                    \

   status     = 0;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);
   prev_token = conf_data->current_token_code;

   _PARSE_INTERMEDIATE_TOKEN('=', _AL_CONF_TOKEN_EQUALS);

   switch (prev_token)
   {
   case _AL_CONF_TOKEN_ETH_TYPE:
      _PARSE_MATCH(_int, AL_CONF_EFFISTREAM_MATCH_ID_ETH_TYPE);
      break;

   case _AL_CONF_TOKEN_ETH_DST:
      _PARSE_MATCH(_mac_addr, AL_CONF_EFFISTREAM_MATCH_ID_ETH_DST);
      break;

   case _AL_CONF_TOKEN_ETH_SRC:
      _PARSE_MATCH(_mac_addr, AL_CONF_EFFISTREAM_MATCH_ID_ETH_SRC);
      break;

   case _AL_CONF_TOKEN_IP_TOS:
      _PARSE_MATCH(_int, AL_CONF_EFFISTREAM_MATCH_ID_IP_TOS);
      break;

   case _AL_CONF_TOKEN_IP_DIFFSRV:
      _PARSE_MATCH(_int, AL_CONF_EFFISTREAM_MATCH_ID_IP_DIFFSRV);
      break;

   case _AL_CONF_TOKEN_IP_SRC:
      _PARSE_MATCH(_ip_addr, AL_CONF_EFFISTREAM_MATCH_ID_IP_SRC);
      break;

   case _AL_CONF_TOKEN_IP_DST:
      _PARSE_MATCH(_ip_addr, AL_CONF_EFFISTREAM_MATCH_ID_IP_DST);
      break;

   case _AL_CONF_TOKEN_IP_PROTO:
      _PARSE_MATCH(_int, AL_CONF_EFFISTREAM_MATCH_ID_IP_PROTO);
      break;

   case _AL_CONF_TOKEN_UDP_SRC_PORT:
      _PARSE_MATCH(_range, AL_CONF_EFFISTREAM_MATCH_ID_UDP_SRC_PORT);
      break;

   case _AL_CONF_TOKEN_UDP_DST_PORT:
      _PARSE_MATCH(_range, AL_CONF_EFFISTREAM_MATCH_ID_UDP_DST_PORT);
      break;

   case _AL_CONF_TOKEN_UDP_LENGTH:
      _PARSE_MATCH(_range, AL_CONF_EFFISTREAM_MATCH_ID_UDP_LENGTH);
      break;

   case _AL_CONF_TOKEN_TCP_SRC_PORT:
      _PARSE_MATCH(_range, AL_CONF_EFFISTREAM_MATCH_ID_TCP_SRC_PORT);
      break;

   case _AL_CONF_TOKEN_TCP_DST_PORT:
      _PARSE_MATCH(_range, AL_CONF_EFFISTREAM_MATCH_ID_TCP_DST_PORT);
      break;

   case _AL_CONF_TOKEN_TCP_LENGTH:
      _PARSE_MATCH(_range, AL_CONF_EFFISTREAM_MATCH_ID_TCP_LENGTH);
      break;

   case _AL_CONF_TOKEN_RTP_VERSION:
      _PARSE_MATCH(_int, AL_CONF_EFFISTREAM_MATCH_ID_RTP_VERSION);
      break;

   case _AL_CONF_TOKEN_RTP_PAYLOAD:
      _PARSE_MATCH(_int, AL_CONF_EFFISTREAM_MATCH_ID_RTP_PAYLOAD);
      break;

   case _AL_CONF_TOKEN_RTP_LENGTH:
      _PARSE_MATCH(_range, AL_CONF_EFFISTREAM_MATCH_ID_RTP_LENGTH);
      break;
   }

#undef _PARSE_MATCH

label_ret:
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return status;
}


static int _parse_effistream_block(AL_CONTEXT_PARAM_DECL _al_conf_data_t *conf_data,
                                   al_conf_effistream_criteria_t         *parent,
                                   int                                   expect_open_brace)
{
   int status;
   int ret;
   al_conf_effistream_criteria_t *last_child;
   al_conf_effistream_criteria_t temp_child;

   status = 0;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   if (expect_open_brace)
   {
      _PARSE_INTERMEDIATE_TOKEN('{', _AL_CONF_TOKEN_OPEN_BRACE);
   }

   parent->first_child = NULL;
   last_child          = NULL;

   while (1)
   {
      _GET_TOKEN_AND_CHECK_EOF();

      switch (conf_data->current_token_code)
      {
      case _AL_CONF_TOKEN_CLOSE_BRACE:
         status = 0;
         goto label_ret;

      case _AL_CONF_TOKEN_ETH_TYPE:
      case _AL_CONF_TOKEN_ETH_DST:
      case _AL_CONF_TOKEN_ETH_SRC:
      case _AL_CONF_TOKEN_IP_TOS:
      case _AL_CONF_TOKEN_IP_DIFFSRV:
      case _AL_CONF_TOKEN_IP_SRC:
      case _AL_CONF_TOKEN_IP_DST:
      case _AL_CONF_TOKEN_IP_PROTO:
      case _AL_CONF_TOKEN_UDP_SRC_PORT:
      case _AL_CONF_TOKEN_UDP_DST_PORT:
      case _AL_CONF_TOKEN_UDP_LENGTH:
      case _AL_CONF_TOKEN_TCP_SRC_PORT:
      case _AL_CONF_TOKEN_TCP_DST_PORT:
      case _AL_CONF_TOKEN_TCP_LENGTH:
      case _AL_CONF_TOKEN_RTP_VERSION:
      case _AL_CONF_TOKEN_RTP_PAYLOAD:
      case _AL_CONF_TOKEN_RTP_LENGTH:

         memset(&temp_child, 0, sizeof(al_conf_effistream_criteria_t));

         ret = _parse_match_value(AL_CONTEXT conf_data, &temp_child);

         if (ret == -1)
         {
            al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): error parsing match value : %s<%d>\n",
                         conf_data->line_number,
                         conf_data->column_number, __func__, __LINE__);
            status = -1;
            goto label_ret;
         }

         /**
          * Now We expect either a '[' for an action block or a '{' to begin
          * a child block
          */

         _GET_TOKEN_AND_CHECK_EOF();

         if (conf_data->current_token_code == _AL_CONF_TOKEN_LEFT_SQBRACK)
         {
            ret = _parse_action_block(AL_CONTEXT conf_data, &temp_child);
            if (ret == -1)
            {
               al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): error parsing action block : %s<%d>\n",
                            conf_data->line_number,
                            conf_data->column_number, __func__, __LINE__);
               status = -1;
               goto label_ret;
            }
         }
         else if (conf_data->current_token_code == _AL_CONF_TOKEN_OPEN_BRACE)
         {
            ret = _parse_effistream_block(AL_CONTEXT conf_data, &temp_child, 0);
            if (ret == -1)
            {
               if (temp_child.first_child != NULL)
               {
                  _free_effistream_info(AL_CONTEXT temp_child.first_child);
               }
               al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): error parsing child block : %s<%d>\n",
                            conf_data->line_number,
                            conf_data->column_number, __func__, __LINE__);
               status = -1;
               goto label_ret;
            }
         }
         else
         {
            al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): unexpected token %s while looking for action or child match block : %s<%d>\n", conf_data->line_number, conf_data->column_number, conf_data->current_token, __func__, __LINE__);
            status = -1;
            goto label_ret;
         }

         if (parent->first_child == NULL)
         {
            parent->first_child = (al_conf_effistream_criteria_t *)al_heap_alloc(AL_CONTEXT sizeof(al_conf_effistream_criteria_t)AL_HEAP_DEBUG_PARAM);
            memset(parent->first_child, 0, sizeof(al_conf_effistream_criteria_t));
            last_child = parent->first_child;
         }
         else
         {
            last_child->next_sibling = (al_conf_effistream_criteria_t *)al_heap_alloc(AL_CONTEXT sizeof(al_conf_effistream_criteria_t)AL_HEAP_DEBUG_PARAM);
            memset(last_child->next_sibling, 0, sizeof(al_conf_effistream_criteria_t));
            last_child = last_child->next_sibling;
         }

         last_child->match_id = temp_child.match_id;
         memcpy(&last_child->match, &temp_child.match, sizeof(temp_child.match));
         memcpy(&last_child->action, &temp_child.action, sizeof(temp_child.action));
         last_child->first_child = temp_child.first_child;

         break;

      default:
         al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): unexpected token %s while parsing criteria : %s<%d>\n",
                      conf_data->line_number,
                      conf_data->column_number,
                      conf_data->current_token, __func__, __LINE__);
         status = -1;
         goto label_ret;
      }
   }

label_ret:
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return status;
}


static int _parse_effistream_info(AL_CONTEXT_PARAM_DECL _al_conf_data_t *conf_data)
{
   int ret;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   conf_data->criteria_root = (al_conf_effistream_criteria_t *)al_heap_alloc(AL_CONTEXT sizeof(al_conf_effistream_criteria_t)AL_HEAP_DEBUG_PARAM);
   memset(conf_data->criteria_root, 0, sizeof(al_conf_effistream_criteria_t));

   ret = _parse_effistream_block(AL_CONTEXT conf_data, conf_data->criteria_root, 1);

   if (ret == -1)
   {
	   al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : _parse_effistream_block() is failed : %s<%d>\n", __func__, __LINE__);
      _free_effistream_info(AL_CONTEXT conf_data->criteria_root);
      conf_data->criteria_root = NULL;
   }
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);

   return ret;
}


static int _parse_gps_info(AL_CONTEXT_PARAM_DECL _al_conf_data_t *conf_data)
{
   int ret;
   int status;
   int value;
   int i;

   ret    = 0;
   status = 0;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   _PARSE_INTERMEDIATE_TOKEN('{', _AL_CONF_TOKEN_OPEN_BRACE);

   do
   {
      _GET_TOKEN_AND_CHECK_EOF();

      switch (conf_data->current_token_code)
      {
      case _AL_CONF_TOKEN_ENABLED:
         _PARSE_INTERMEDIATE_TOKEN('=', _AL_CONF_TOKEN_EQUALS);
         _GET_TOKEN_AND_CHECK_EOF();
         ret = al_conf_atoi(conf_data->current_token, &value);
         if (ret == -1)
         {
            al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): token %s was found instead of an integer : %s<%d>\n",
                         conf_data->line_number,
                         conf_data->column_number,
                         conf_data->current_token, __func__, __LINE__);
            status = -1;
            goto label_ret;
         }
         conf_data->gps_info.gps_enabled = value;
         break;

      case _AL_CONF_TOKEN_INPUT:
         _PARSE_INTERMEDIATE_TOKEN('=', _AL_CONF_TOKEN_EQUALS);
         ret = _get_rest_of_line_ex(AL_CONTEXT conf_data, ',');
         strcpy(conf_data->gps_info.gps_input_dev, conf_data->current_token);
         break;

      case _AL_CONF_TOKEN_PUSH_DEST:

         _PARSE_INTERMEDIATE_TOKEN('=', _AL_CONF_TOKEN_EQUALS);

         for (i = 0; i < 4; i++)
         {
            _GET_TOKEN_AND_CHECK_EOF();

            ret = al_conf_atoi(conf_data->current_token, &value);

            if (ret == -1)
            {
               al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): token %s was found instead of an integer : %s<%d>\n",
                            conf_data->line_number,
                            conf_data->column_number,
                            conf_data->current_token, __func__, __LINE__);
               status = -1;
               goto label_ret;
            }

            conf_data->gps_info.gps_push_dest_ip[i] = value;

            if (i != 3)
            {
               _PARSE_INTERMEDIATE_TOKEN('.', _AL_CONF_TOKEN_DOT);
            }
         }

         _PARSE_INTERMEDIATE_TOKEN(':', _AL_CONF_TOKEN_COLON);

         _GET_TOKEN_AND_CHECK_EOF();
         ret = al_conf_atoi(conf_data->current_token, &value);
         if (ret == -1)
         {
            al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): token %s was found instead of an integer : %s<%d>\n",
                         conf_data->line_number,
                         conf_data->column_number,
                         conf_data->current_token, __func__, __LINE__);
            status = -1;
            goto label_ret;
         }

         conf_data->gps_info.gps_push_port = value;

         break;

      case _AL_CONF_TOKEN_PUSH_INT:

         _PARSE_INTERMEDIATE_TOKEN('=', _AL_CONF_TOKEN_EQUALS);

         _GET_TOKEN_AND_CHECK_EOF();
         ret = al_conf_atoi(conf_data->current_token, &value);
         if (ret == -1)
         {
            al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): token %s was found instead of an integer : %s<%d>\n",
                         conf_data->line_number,
                         conf_data->column_number,
                         conf_data->current_token, __func__, __LINE__);
            status = -1;
            goto label_ret;
         }

         conf_data->gps_info.gps_push_interval = value;

         break;

      default:
         al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): un-expected token %s found in gps blocki : %s<%d>\n",
                      conf_data->line_number,
                      conf_data->column_number,
                      conf_data->current_token);
         status = -1;
         goto label_ret;
      }

      /**
       * At this statge we expect either a comma or a }
       */

      _GET_TOKEN_AND_CHECK_EOF();

      if (conf_data->current_token_code == _AL_CONF_TOKEN_COMMA)
      {
         continue;
      }
      else if (conf_data->current_token_code == _AL_CONF_TOKEN_CLOSE_BRACE)
      {
         break;
      }
      else
      {
			al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): token %s was found instead of , or } : %s<%d>\n",
                      conf_data->line_number,
                      conf_data->column_number,
                      conf_data->current_token, __func__, __LINE__);
         status = -1;
         goto label_ret;
      }
   } while (1);

label_ret:
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return status;
}


static int _parse_dhcp_info(AL_CONTEXT_PARAM_DECL _al_conf_data_t *conf_data)
{
   int ret;
   int status;
   int value;
   int i;
   int token;

   ret    = 0;
   status = 0;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   _PARSE_INTERMEDIATE_TOKEN('{', _AL_CONF_TOKEN_OPEN_BRACE);

   do
   {
      _GET_TOKEN_AND_CHECK_EOF();

      switch (conf_data->current_token_code)
      {
      case _AL_CONF_TOKEN_NET_ID:
      case _AL_CONF_TOKEN_GATEWAY:
      case _AL_CONF_TOKEN_DNS:
      case _AL_CONF_TOKEN_MASK:

         token = conf_data->current_token_code;

         _PARSE_INTERMEDIATE_TOKEN('=', _AL_CONF_TOKEN_EQUALS);

         for (i = 0; i < 4; i++)
         {
            _GET_TOKEN_AND_CHECK_EOF();

            ret = al_conf_atoi(conf_data->current_token, &value);

            if (ret == -1)
            {
               al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): token %s was found instead of an integer : %s<%d>\n",
                            conf_data->line_number,
                            conf_data->column_number,
                            conf_data->current_token, __func__, __LINE__);
               status = -1;
               goto label_ret;
            }

            switch (token)
            {
            case _AL_CONF_TOKEN_NET_ID:
               conf_data->dhcp_info.dhcp_net_id[i] = value;
               break;

            case _AL_CONF_TOKEN_GATEWAY:
               conf_data->dhcp_info.dhcp_gateway[i] = value;
               break;

            case _AL_CONF_TOKEN_DNS:
               conf_data->dhcp_info.dhcp_dns[i] = value;
               break;

            case _AL_CONF_TOKEN_MASK:
               conf_data->dhcp_info.dhcp_mask[i] = value;
               break;
            }

            if (i != 3)
            {
               _PARSE_INTERMEDIATE_TOKEN('.', _AL_CONF_TOKEN_DOT);
            }
         }
         break;

      case _AL_CONF_TOKEN_MODE:
         _PARSE_INTERMEDIATE_TOKEN('=', _AL_CONF_TOKEN_EQUALS);
         _GET_TOKEN_AND_CHECK_EOF();
         ret = al_conf_atoi(conf_data->current_token, &value);
         if (ret == -1)
         {
            al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): token %s was found instead of an integer : %s<%d>\n",
                         conf_data->line_number,
                         conf_data->column_number,
                         conf_data->current_token, __func__, __LINE__);
            status = -1;
            goto label_ret;
         }
         conf_data->dhcp_info.dhcp_mode = value;

         break;

      case _AL_CONF_TOKEN_LEASE:
         _PARSE_INTERMEDIATE_TOKEN('=', _AL_CONF_TOKEN_EQUALS);
         _GET_TOKEN_AND_CHECK_EOF();
         ret = al_conf_atoi(conf_data->current_token, &value);
         if (ret == -1)
         {
            al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERRRO : al_conf.c(%d,%d): token %s was found instead of an integer : %s<%d>\n",
                         conf_data->line_number,
                         conf_data->column_number,
                         conf_data->current_token, __func__, __LINE__);
            status = -1;
            goto label_ret;
         }
         conf_data->dhcp_info.dhcp_lease_time = value;
         break;

      default:
         al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): un-expected token %s found in dhcp block : %s<%d>\n",
                      conf_data->line_number,
                      conf_data->column_number,
                      conf_data->current_token, __func__, __LINE__);
         status = -1;
         goto label_ret;
      }

      /**
       * At this statge we expect either a comma or a }
       */

      _GET_TOKEN_AND_CHECK_EOF();

      if (conf_data->current_token_code == _AL_CONF_TOKEN_COMMA)
      {
         continue;
      }
      else if (conf_data->current_token_code == _AL_CONF_TOKEN_CLOSE_BRACE)
      {
         break;
      }
      else
      {
			al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): token %s was found instead of , or } : %s<%d>\n",
                      conf_data->line_number,
                      conf_data->column_number,
                      conf_data->current_token, __func__, __LINE__);
         status = -1;
         goto label_ret;
      }
   } while (1);

label_ret:
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return status;
}


static int _parse_options(AL_CONTEXT_PARAM_DECL _al_conf_data_t *conf_data)
{
   int              ret;
   int              status;
   int              value;
   int              i;
   al_conf_option_t parsed_option;
   al_conf_option_t *option;

   ret    = 0;
   status = 0;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   _PARSE_INTERMEDIATE_TOKEN('{', _AL_CONF_TOKEN_OPEN_BRACE);

   _GET_TOKEN_AND_CHECK_EOF();

   if (conf_data->current_token_code != _AL_CONF_TOKEN_CLOSE_BRACE)
   {
      i = 0;
      goto _loop;
   }
   else
   {
      goto label_ret;
   }

   do
   {
      /**
       * We expect the 128-bit key followed either by a comma or a '}'
       * Each 128-bit key is made up of 16 2-hexdigit numbers followed
       * by a colon expect for the last which is followed either by
       * a comma or '}'
       */

      for (i = 0; i < 16; i++)
      {
         _GET_TOKEN_AND_CHECK_EOF();

_loop:

         ret = al_conf_hex_atoi(conf_data->current_token, &value);

         if (ret == -1)
         {
            al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): token %s was found instead of a hexadecimal digit\n",
                         conf_data->line_number,
                         conf_data->column_number,
                         conf_data->current_token, __func__, __LINE__);
            status = -1;
            goto label_ret;
         }

         parsed_option.key[i] = value;

         if (i != 15)
         {
            _PARSE_INTERMEDIATE_TOKEN(':', _AL_CONF_TOKEN_COLON);
         }
      }

      option = (al_conf_option_t *)al_heap_alloc(AL_CONTEXT sizeof(al_conf_option_t)AL_HEAP_DEBUG_PARAM);
      memcpy(option->key, parsed_option.key, sizeof(option->key));

      option->next           = conf_data->option_info;
      conf_data->option_info = option;

      /**
       * We expect either a comma or a closing brace
       */

      _GET_TOKEN_AND_CHECK_EOF();

      if (conf_data->current_token_code == _AL_CONF_TOKEN_COMMA)
      {
         continue;
      }
      else if (conf_data->current_token_code == _AL_CONF_TOKEN_CLOSE_BRACE)
      {
         break;
      }
      else
      {
			al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): token %s was found instead of , or } : %s<%d>\n",
                      conf_data->line_number,
                      conf_data->column_number,
                      conf_data->current_token, __func__, __LINE__);
         status = -1;
         goto label_ret;
      }
   } while (1);

label_ret:
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return status;
}


static int _parse_logmon_info(AL_CONTEXT_PARAM_DECL _al_conf_data_t *conf_data)
{
   int ret;
   int status;
   int value;
   int i;

   ret    = 0;
   status = 0;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   _PARSE_INTERMEDIATE_TOKEN('{', _AL_CONF_TOKEN_OPEN_BRACE);

   do
   {
      _GET_TOKEN_AND_CHECK_EOF();

      switch (conf_data->current_token_code)
      {
      case _AL_CONF_TOKEN_PUSH_DEST:

         _PARSE_INTERMEDIATE_TOKEN('=', _AL_CONF_TOKEN_EQUALS);

         for (i = 0; i < 4; i++)
         {
            _GET_TOKEN_AND_CHECK_EOF();

            ret = al_conf_atoi(conf_data->current_token, &value);

            if (ret == -1)
            {
               al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): token %s was found instead of an integer : %s<%d>\n",
                            conf_data->line_number,
                            conf_data->column_number,
                            conf_data->current_token, __func__, __LINE__);
               status = -1;
               goto label_ret;
            }

            conf_data->logmon_info.logmon_dest_ip[i] = value;

            if (i != 3)
            {
               _PARSE_INTERMEDIATE_TOKEN('.', _AL_CONF_TOKEN_DOT);
            }
         }

         _PARSE_INTERMEDIATE_TOKEN(':', _AL_CONF_TOKEN_COLON);

         _GET_TOKEN_AND_CHECK_EOF();
         ret = al_conf_atoi(conf_data->current_token, &value);
         if (ret == -1)
         {
            al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): token %s was found instead of an integer : %s<%d>\n",
                         conf_data->line_number,
                         conf_data->column_number,
                         conf_data->current_token, __func__, __LINE__);
            status = -1;
            goto label_ret;
         }

         conf_data->logmon_info.logmon_dest_port = value;

         break;

      default:
         al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): un-expected token %s found in logmon block : %s<%d>\n",
                      conf_data->line_number,
                      conf_data->column_number,
                      conf_data->current_token, __func__, __LINE__);
         status = -1;
         goto label_ret;
      }

      /**
       * At this statge we expect either a comma or a }
       */

      _GET_TOKEN_AND_CHECK_EOF();

      if (conf_data->current_token_code == _AL_CONF_TOKEN_COMMA)
      {
         continue;
      }
      else if (conf_data->current_token_code == _AL_CONF_TOKEN_CLOSE_BRACE)
      {
         break;
      }
      else
      {
			al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): token %s was found instead of , or } : %s<%d>\n",
                      conf_data->line_number,
                      conf_data->column_number,
                      conf_data->current_token, __func__, __LINE__);
         status = -1;
         goto label_ret;
      }
   } while (1);

label_ret:
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return status;
}


#undef _PARSE_INTERMEDIATE_TOKEN
#undef _GET_TOKEN_AND_CHECK_EOF

static int _parse_security_info(AL_CONTEXT_PARAM_DECL _al_conf_data_t *conf_data, al_security_info_t *security_info)
{
   struct _line_list
   {
      char              line[_AL_CONF_MAX_TOKEN_SIZE + 1];
      int               line_length;
      struct _line_list *next;
   };

   typedef struct _line_list   _line_list_t;

   int          ret;
   int          status;
   int          j, key_count;
   int          val;
   _line_list_t *head;
   _line_list_t *tail;
   char         *p;
   int          rsn_psk_key_length;

   head = NULL;
   tail = NULL;

   status = 0;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

#define _PARSE_INTERMEDIATE_TOKEN(tok, tok_code)                                                                      \
   do {                                                                                                               \
      ret = _get_token(AL_CONTEXT conf_data);                                                                         \
      if (ret == -1) {                                                                                                \
         al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): unexpected eof file while looking for '%c' : %s<%d>\n", \
                      conf_data->line_number,                                                                         \
                      conf_data->column_number,                                                                       \
                      tok, __func__, __LINE__);                                                                                           \
         status = -1;                                                                                                 \
         goto label_ret;                                                                                              \
      }                                                                                                               \
      if (conf_data->current_token_code != tok_code) {                                                                \
         al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): expecting token %c but found %s : %s<%d>\n",            \
                      conf_data->line_number,                                                                         \
                      conf_data->column_number,                                                                       \
                      tok,                                                                                            \
                      conf_data->current_token, __func__, __LINE__);                                                                      \
         status = -1;                                                                                                 \
         goto label_ret;                                                                                              \
      }                                                                                                               \
   } while (0)

#define _FREE_LINE_LIST()               \
   do {                                 \
      tail = head;                      \
      while (tail != NULL) {            \
         head = tail->next;             \
         al_heap_free(AL_CONTEXT tail); \
         tail = head;                   \
      }                                 \
      head = NULL;                      \
      tail = NULL;                      \
   } while (0)


   _PARSE_INTERMEDIATE_TOKEN('{', _AL_CONF_TOKEN_OPEN_BRACE);
   conf_data->security_heirarchy_level = 0;

   while (1)
   {
      ret = _get_token(AL_CONTEXT conf_data);
      if (ret == -1)
      {
         al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): unexpected eof file while looking for security info item : %s<%d>\n",
                      conf_data->line_number,
                      conf_data->column_number, __func__, __LINE__);
         status = -1;
         goto label_ret;
      }
      if ((conf_data->security_heirarchy_level == 0) && (conf_data->current_token_code == _AL_CONF_TOKEN_CLOSE_BRACE))
      {
         break;
      }
      switch (conf_data->current_token_code)
      {
      case _AL_CONF_TOKEN_SECURITY_NONE:
         _PARSE_INTERMEDIATE_TOKEN('{', _AL_CONF_TOKEN_OPEN_BRACE);
         conf_data->security_heirarchy_level++;
         ret = _get_token(AL_CONTEXT conf_data);
         if ((ret == -1) || (conf_data->current_token_code != _AL_CONF_TOKEN_ENABLED))
         {
            al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): unexpected eof file while looking for security info item : %s<%d>\n",
                         conf_data->line_number,
                         conf_data->column_number, __func__, __LINE__);
            status = -1;
            goto label_ret;
         }
         _PARSE_INTERMEDIATE_TOKEN('=', _AL_CONF_TOKEN_EQUALS);
         ret = _get_token(AL_CONTEXT conf_data);
         if (ret == -1)
         {
            al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): unexpected eof file while looking for none enabled : %s<%d>\n",
                         conf_data->line_number,
                         conf_data->column_number, __func__, __LINE__);
            status = -1;
            goto label_ret;
         }
         ret = al_conf_atoi(conf_data->current_token, &val);
         security_info->_security_none.enabled = val;
         if (ret == -1)
         {
            al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): token %s was found instead of an integer : %s<%d>\n",
                         conf_data->line_number,
                         conf_data->column_number,
                         conf_data->current_token, __func__, __LINE__);
            status = -1;
            goto label_ret;
         }
         _PARSE_INTERMEDIATE_TOKEN('}', _AL_CONF_TOKEN_CLOSE_BRACE);
         conf_data->security_heirarchy_level--;
         break;

      case _AL_CONF_TOKEN_WEP:
         _PARSE_INTERMEDIATE_TOKEN('{', _AL_CONF_TOKEN_OPEN_BRACE);
         conf_data->security_heirarchy_level++;
         while (1)
         {
            ret = _get_token(AL_CONTEXT conf_data);
            if (ret == -1)
            {
               al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): unexpected eof file while looking for security info item : %s<%d>\n", conf_data->line_number, conf_data->column_number, __func__, __LINE__);
               status = -1;
               goto label_ret;
            }
            if ((conf_data->security_heirarchy_level == 0) &&
                (conf_data->current_token_code == _AL_CONF_TOKEN_CLOSE_BRACE))
            {
               break;
            }
            switch (conf_data->current_token_code)
            {
            case _AL_CONF_TOKEN_ENABLED:
               _PARSE_INTERMEDIATE_TOKEN('=', _AL_CONF_TOKEN_EQUALS);
               ret = _get_token(AL_CONTEXT conf_data);
               if (ret == -1)
               {
                  al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): unexpected eof file while looking for wep enabled : %s<%d>\n",
                               conf_data->line_number,
                               conf_data->column_number, __func__, __LINE__);
                  status = -1;
                  goto label_ret;
               }
               ret = al_conf_atoi(conf_data->current_token, &val);
               security_info->_security_wep.enabled = val;
               if (ret == -1)
               {
                  al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): token %s was found instead of an integer : %s<%d>\n",
                               conf_data->line_number,
                               conf_data->column_number,
                               conf_data->current_token, __func__, __LINE__);
                  status = -1;
                  goto label_ret;
               }
               break;

            case _AL_CONF_TOKEN_STRENGTH:

               _PARSE_INTERMEDIATE_TOKEN('=', _AL_CONF_TOKEN_EQUALS);
               ret = _get_token(AL_CONTEXT conf_data);
               if (ret == -1)
               {
                  al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): unexpected eof file while looking for strength : %s<%d>\n",
                               conf_data->line_number,
                               conf_data->column_number, __func__, __LINE__);
                  status = -1;
                  goto label_ret;
               }
               ret = al_conf_atoi(conf_data->current_token, &val);
               security_info->_security_wep.strength = val;
               if (ret == -1)
               {
                  al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): token %s was found instead of an integer : %s<%d>\n",
                               conf_data->line_number,
                               conf_data->column_number,
                               conf_data->current_token, __func__, __LINE__);
                  status = -1;
                  goto label_ret;
               }

               break;

            case _AL_CONF_TOKEN_KEY_INDEX:

               _PARSE_INTERMEDIATE_TOKEN('=', _AL_CONF_TOKEN_EQUALS);
               ret = _get_token(AL_CONTEXT conf_data);
               if (ret == -1)
               {
                  al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): unexpected eof file while looking for key index : %s<%d>\n",
                               conf_data->line_number,
                               conf_data->column_number, __func__, __LINE__);
                  status = -1;
                  goto label_ret;
               }
               ret = al_conf_atoi(conf_data->current_token, &val);
               if ((val < 0) || (val > 4))
               {
                  al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): key index incorrect : %s<%d>\n",
                               conf_data->line_number,
                               conf_data->column_number,
                               conf_data->current_token, __func__, __LINE__);
                  status = -1;
                  goto label_ret;
               }
               security_info->_security_wep.key_index = val;
               if (ret == -1)
               {
                  al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): token %s was found instead of an integer : %s<%d>\n",
                               conf_data->line_number,
                               conf_data->column_number,
                               conf_data->current_token, __func__, __LINE__);
                  status = -1;
                  goto label_ret;
               }

               break;

            case _AL_CONF_TOKEN_KEYS:

               _PARSE_INTERMEDIATE_TOKEN('=', _AL_CONF_TOKEN_EQUALS);

               _PARSE_INTERMEDIATE_TOKEN('{', _AL_CONF_TOKEN_OPEN_BRACE);
               conf_data->security_heirarchy_level++;
               /* Parse keys here */                                                  // TODO
               key_count = 0;
               j         = 0;
               while (1)
               {
                  ret = _get_token(AL_CONTEXT conf_data);
                  if (ret == -1)
                  {
                     al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): unexpected eof file while looking for wep keys value : %s<%d>\n", conf_data->line_number, conf_data->column_number, __func__, __LINE__);
                     status = -1;
                     goto label_ret;
                  }
                  if (conf_data->current_token_code == _AL_CONF_TOKEN_COLON)
                  {
                     continue;
                  }
                  if (conf_data->current_token_code == _AL_CONF_TOKEN_COMMA)
                  {
                     security_info->_security_wep.keys[key_count].len = j;
                     key_count++;
                     j = 0;
                     continue;
                  }
                  if (conf_data->current_token_code == _AL_CONF_TOKEN_CLOSE_BRACE)
                  {
                     security_info->_security_wep.keys[key_count].len = j;
                     key_count++;
                     j = 0;
                     security_info->_security_wep.key_count = key_count;
                     conf_data->security_heirarchy_level--;
                     break;
                  }

                  ret = al_conf_hex_atoi(conf_data->current_token, &val);

                  security_info->_security_wep.keys[key_count].bytes[j] = val;

                  if (ret == -1)
                  {
                     al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): token %s was found instead of an hex integer : %s<%d>\n",
                                  conf_data->line_number,
                                  conf_data->column_number,
                                  conf_data->current_token, __func__, __LINE__);
                     status = -1;
                     goto label_ret;
                  }
                  j++;
               }
            }
            if (conf_data->current_token_code == _AL_CONF_TOKEN_COMMA)
            {
               continue;
            }
            else if (conf_data->current_token_code == _AL_CONF_TOKEN_CLOSE_BRACE)
            {
               conf_data->security_heirarchy_level--;
            }
         }
         break;

      case _AL_CONF_TOKEN_RSN_PSK:

         _PARSE_INTERMEDIATE_TOKEN('{', _AL_CONF_TOKEN_OPEN_BRACE);
         conf_data->security_heirarchy_level++;

         while (1)
         {
            ret = _get_token(AL_CONTEXT conf_data);
            if (ret == -1)
            {
               al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): unexpected eof file while looking for rsn psk : %s<%d>\n",
                            conf_data->line_number,
                            conf_data->column_number, __func__, __LINE__);
               status = -1;
               goto label_ret;
            }

            if ((conf_data->security_heirarchy_level == 0) &&
                (conf_data->current_token_code == _AL_CONF_TOKEN_CLOSE_BRACE))
            {
               break;
            }
            /* parse enabled */
            switch (conf_data->current_token_code)
            {
            case _AL_CONF_TOKEN_ENABLED:
               _PARSE_INTERMEDIATE_TOKEN('=', _AL_CONF_TOKEN_EQUALS);
               ret = _get_token(AL_CONTEXT conf_data);
               if (ret == -1)
               {
                  al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): unexpected eof file while looking for rsn psk enabled : %s<%d>\n", conf_data->line_number, conf_data->column_number, __func__, __LINE__);
                  status = -1;
                  goto label_ret;
               }
               ret = al_conf_atoi(conf_data->current_token, &val);
               security_info->_security_rsn_psk.enabled = val;
               if (ret == -1)
               {
                  al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): token %s was found instead of an integer : %s<%d>\n",
                               conf_data->line_number,
                               conf_data->column_number,
                               conf_data->current_token, __func__, __LINE__);
                  status = -1;
                  goto label_ret;
               }
               break;

            case _AL_CONF_TOKEN_MODE:
               _PARSE_INTERMEDIATE_TOKEN('=', _AL_CONF_TOKEN_EQUALS);
               ret = _get_token(AL_CONTEXT conf_data);
               if (ret == -1)
               {
                  al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): unexpected eof file while looking for rsn psk enabled : %s<%d>\n", conf_data->line_number, conf_data->column_number, __func__, __LINE__);
                  status = -1;
                  goto label_ret;
               }
               ret = al_conf_atoi(conf_data->current_token, &val);
               security_info->_security_rsn_psk.mode = val;
               if (ret == -1)
               {
                  al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): token %s was found instead of an integer : %s<%d>\n",
                               conf_data->line_number,
                               conf_data->column_number,
                               conf_data->current_token,__func__, __LINE__);
                  status = -1;
                  goto label_ret;
               }
               break;

            case _AL_CONF_TOKEN_KEY:
               _PARSE_INTERMEDIATE_TOKEN('=', _AL_CONF_TOKEN_EQUALS);

               _FREE_LINE_LIST();

               _PARSE_INTERMEDIATE_TOKEN('{', _AL_CONF_TOKEN_OPEN_BRACE);
               conf_data->security_heirarchy_level++;
               _update_cursor(AL_CONTEXT conf_data);
               rsn_psk_key_length = 0;

               while (1)
               {
                  ret = _get_rest_of_line(AL_CONTEXT conf_data);
                  if (ret == -1)
                  {
                     al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c: unexpected eof file while processing rsn psk key (read line) : %s<%d>\n", __func__, __LINE__);
                     return -1;
                  }
                  if (!strncmp(conf_data->current_token, "begin ", 6))
                  {
                     head = (_line_list_t *)al_heap_alloc(AL_CONTEXT sizeof(_line_list_t)AL_HEAP_DEBUG_PARAM);
                     tail = head;
                  }
                  else if (tail != NULL)
                  {
                     tail->next = (_line_list_t *)al_heap_alloc(AL_CONTEXT sizeof(_line_list_t)AL_HEAP_DEBUG_PARAM);
                     tail       = tail->next;
                  }
                  if (tail != NULL)
                  {
                     memset(tail, 0, sizeof(_line_list_t));
                     strcpy(tail->line, conf_data->current_token);
                     tail->line_length   = strlen(tail->line);
                     rsn_psk_key_length += tail->line_length + 1;
                  }
                  if (!strcmp(conf_data->current_token, "end"))
                  {
                     break;
                  }
               }
               _PARSE_INTERMEDIATE_TOKEN('}', _AL_CONF_TOKEN_CLOSE_BRACE);
               conf_data->security_heirarchy_level--;

               /**
                * Now convert the list into a big string
                */
               memset(security_info->_security_rsn_psk.key_buffer,
                      0,
                      sizeof(security_info->_security_rsn_psk.key_buffer));
               tail = head;
               p    = security_info->_security_rsn_psk.key_buffer;
               while (tail != NULL)
               {
                  memcpy(p, tail->line, tail->line_length);
                  p   += tail->line_length;
                  *p++ = '\n';
                  tail = tail->next;
               }

               security_info->_security_rsn_psk.key_buffer_len = rsn_psk_key_length;

               _FREE_LINE_LIST();
               break;

            case _AL_CONF_TOKEN_CIPHER_TKIP:
               /* parse cipher_tkip*/
               _PARSE_INTERMEDIATE_TOKEN('=', _AL_CONF_TOKEN_EQUALS);
               ret = _get_token(AL_CONTEXT conf_data);
               if (ret == -1)
               {
                  al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): unexpected eof file while looking for rsn psk cipher tkip : %s<%d>\n", conf_data->line_number, conf_data->column_number, __func__, __LINE__);
                  status = -1;
                  goto label_ret;
               }
               ret = al_conf_atoi(conf_data->current_token, &val);
               security_info->_security_rsn_psk.cipher_tkip = val;
               if (ret == -1)
               {
                  al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): token %s was found instead of an integer : %s<%d>\n",
                               conf_data->line_number,
                               conf_data->column_number,
                               conf_data->current_token, __func__, __LINE__);
                  status = -1;
                  goto label_ret;
               }
               break;

            case _AL_CONF_TOKEN_CIPHER_CCMP:
               /* parse cipher_ccmp*/

               _PARSE_INTERMEDIATE_TOKEN('=', _AL_CONF_TOKEN_EQUALS);
               ret = _get_token(AL_CONTEXT conf_data);
               if (ret == -1)
               {
                  al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): unexpected eof file while looking for rsn psk cipher ccmp : %s<%d>\n", conf_data->line_number, conf_data->column_number, __func__, __LINE__);
                  status = -1;
                  goto label_ret;
               }
               ret = al_conf_atoi(conf_data->current_token, &val);
               security_info->_security_rsn_psk.cipher_ccmp = val;
               if (ret == -1)
               {
                  al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): token %s was found instead of an integer : %s<%d>\n",
                               conf_data->line_number,
                               conf_data->column_number,
                               conf_data->current_token, __func__, __LINE__);
                  status = -1;
                  goto label_ret;
               }

               break;

            /* parse group key renewal*/
            case _AL_CONF_TOKEN_GROUP_KEY_RENEWAL:

               _PARSE_INTERMEDIATE_TOKEN('=', _AL_CONF_TOKEN_EQUALS);
               ret = _get_token(AL_CONTEXT conf_data);
               if (ret == -1)
               {
                  al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): unexpected eof file while looking for rsn psk group key renewal : %s<%d>\n", conf_data->line_number, conf_data->column_number, __func__, __LINE__);
                  status = -1;
                  goto label_ret;
               }
               ret = al_conf_atoi(conf_data->current_token, &val);
               security_info->_security_rsn_psk.group_key_renewal = val;
               if (ret == -1)
               {
                  al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): token %s was found instead of an integer : %s<%d>\n",
                               conf_data->line_number,
                               conf_data->column_number,
                               conf_data->current_token, __func__, __LINE__);
                  status = -1;
                  goto label_ret;
               }
               break;
            }
            if (conf_data->current_token_code == _AL_CONF_TOKEN_COMMA)
            {
               continue;
            }
            else if (conf_data->current_token_code == _AL_CONF_TOKEN_CLOSE_BRACE)
            {
               conf_data->security_heirarchy_level--;
            }
         }
         break;

      case _AL_CONF_TOKEN_RSN_RADIUS:
         _PARSE_INTERMEDIATE_TOKEN('{', _AL_CONF_TOKEN_OPEN_BRACE);
         conf_data->security_heirarchy_level++;

         while (1)
         {
            ret = _get_token(AL_CONTEXT conf_data);
            if (ret == -1)
            {
               al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): unexpected eof file while looking for rsn radius enabled : %s<%d>\n", conf_data->line_number, conf_data->column_number, __func__, __LINE__);
               status = -1;
               goto label_ret;
            }
            if ((conf_data->security_heirarchy_level == 0) &&
                (conf_data->current_token_code == _AL_CONF_TOKEN_CLOSE_BRACE))
            {
               break;
            }
            /* parse enabled */
            switch (conf_data->current_token_code)
            {
            case _AL_CONF_TOKEN_ENABLED:
               _PARSE_INTERMEDIATE_TOKEN('=', _AL_CONF_TOKEN_EQUALS);
               ret = _get_token(AL_CONTEXT conf_data);
               if (ret == -1)
               {
                  al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): unexpected eof file while looking for rsn radius enabled : %s<%d>\n", conf_data->line_number, conf_data->column_number, __func__, __LINE__);
                  status = -1;
                  goto label_ret;
               }
               ret = al_conf_atoi(conf_data->current_token, &val);
               security_info->_security_rsn_radius.enabled = val;
               if (ret == -1)
               {
                  al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): token %s was found instead of an integer : %s<%d>\n",
                               conf_data->line_number,
                               conf_data->column_number,
                               conf_data->current_token, __func__, __LINE__);
                  status = -1;
                  goto label_ret;
               }
               break;

            case _AL_CONF_TOKEN_MODE:
               _PARSE_INTERMEDIATE_TOKEN('=', _AL_CONF_TOKEN_EQUALS);
               ret = _get_token(AL_CONTEXT conf_data);
               if (ret == -1)
               {
                  al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): unexpected eof file while looking for rsn radius enabled : %s<%d>\n", conf_data->line_number, conf_data->column_number, __func__, __LINE__);
                  status = -1;
                  goto label_ret;
               }
               ret = al_conf_atoi(conf_data->current_token, &val);
               security_info->_security_rsn_radius.mode = val;
               if (ret == -1)
               {
                  al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): token %s was found instead of an integer : %s<%d>\n",
                               conf_data->line_number,
                               conf_data->column_number,
                               conf_data->current_token, __func__, __LINE__);
                  status = -1;
                  goto label_ret;
               }
               break;

            case _AL_CONF_TOKEN_RADIUS_SERVER:
               _PARSE_INTERMEDIATE_TOKEN('=', _AL_CONF_TOKEN_EQUALS);
               for (j = 0; j < 4; j++)
               {
                  ret = _get_token(AL_CONTEXT conf_data);
                  if (ret == -1)
                  {
                     al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): unexpected eof file while looking for radius_server value : %s<%d>\n", conf_data->line_number, conf_data->column_number, __func__, __LINE__);
                     status = -1;
                     goto label_ret;
                  }
                  ret = al_conf_atoi(conf_data->current_token, &val);
                  security_info->_security_rsn_radius.radius_server[j] = val;
                  if (ret == -1)
                  {
                     al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): token %s was found instead of an integer : %s<%d>\n",
                                  conf_data->line_number,
                                  conf_data->column_number,
                                  conf_data->current_token, __func__, __LINE__);
                     status = -1;
                     goto label_ret;
                  }
                  if (j != 3)
                  {
                     _PARSE_INTERMEDIATE_TOKEN('.', _AL_CONF_TOKEN_DOT);
                  }
               }
               break;

            case _AL_CONF_TOKEN_RADIUS_PORT:

               _PARSE_INTERMEDIATE_TOKEN('=', _AL_CONF_TOKEN_EQUALS);
               ret = _get_token(AL_CONTEXT conf_data);
               if (ret == -1)
               {
                  al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): unexpected eof file while looking for rsn radius port : %s<%d>\n", conf_data->line_number, conf_data->column_number, __func__, __LINE__);
                  status = -1;
                  goto label_ret;
               }
               ret = al_conf_atoi(conf_data->current_token, &val);
               security_info->_security_rsn_radius.radius_port = val;
               if (ret == -1)
               {
                  al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): token %s was found instead of an integer : %s<%d>\n",
                               conf_data->line_number,
                               conf_data->column_number,
                               conf_data->current_token, __func__, __LINE__);
                  status = -1;
                  goto label_ret;
               }
               break;

            case _AL_CONF_TOKEN_RADIUS_SECRET:

               _FREE_LINE_LIST();
               _PARSE_INTERMEDIATE_TOKEN('{', _AL_CONF_TOKEN_OPEN_BRACE);
               conf_data->security_heirarchy_level++;
               _update_cursor(AL_CONTEXT conf_data);
               rsn_psk_key_length = 0;
               while (1)
               {
                  ret = _get_rest_of_line(AL_CONTEXT conf_data);
                  if (ret == -1)
                  {
                     al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c: unexpected eof file while processing rsn radius key (read line) : %s<%d>\n", __func__, __LINE__);
                     return -1;
                  }
                  if (!strncmp(conf_data->current_token, "begin ", 6))
                  {
                     head = (_line_list_t *)al_heap_alloc(AL_CONTEXT sizeof(_line_list_t)AL_HEAP_DEBUG_PARAM);
                     tail = head;
                  }
                  else if (tail != NULL)
                  {
                     tail->next = (_line_list_t *)al_heap_alloc(AL_CONTEXT sizeof(_line_list_t)AL_HEAP_DEBUG_PARAM);
                     tail       = tail->next;
                  }
                  if (tail != NULL)
                  {
                     memset(tail, 0, sizeof(_line_list_t));
                     strcpy(tail->line, conf_data->current_token);
                     tail->line_length   = strlen(tail->line);
                     rsn_psk_key_length += tail->line_length + 1;
                  }
                  if (!strcmp(conf_data->current_token, "end"))
                  {
                     break;
                  }
               }
               _PARSE_INTERMEDIATE_TOKEN('}', _AL_CONF_TOKEN_CLOSE_BRACE);
               conf_data->security_heirarchy_level--;

               /**
                * Now convert the list into a big string
                */
               memset(security_info->_security_rsn_radius.radius_secret_key,
                      0,
                      sizeof(security_info->_security_rsn_radius.radius_secret_key));
               tail = head;
               p    = security_info->_security_rsn_radius.radius_secret_key;
               while (tail != NULL)
               {
                  memcpy(p, tail->line, tail->line_length);
                  p   += tail->line_length;
                  *p++ = '\n';
                  tail = tail->next;
               }
               security_info->_security_rsn_radius.radius_secret_key_len = rsn_psk_key_length;
               _FREE_LINE_LIST();
               break;

            case _AL_CONF_TOKEN_CIPHER_TKIP:
               /* parse cipher_tkip*/
               _PARSE_INTERMEDIATE_TOKEN('=', _AL_CONF_TOKEN_EQUALS);
               ret = _get_token(AL_CONTEXT conf_data);
               if (ret == -1)
               {
                  al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): unexpected eof file while looking for rsn radius cipher tkip : %s<%d>\n", conf_data->line_number, conf_data->column_number, __func__, __LINE__);
                  status = -1;
                  goto label_ret;
               }
               ret = al_conf_atoi(conf_data->current_token, &val);
               security_info->_security_rsn_radius.cipher_tkip = val;
               if (ret == -1)
               {
                  al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): token %s was found instead of an integer : %s<%d>\n",
                               conf_data->line_number,
                               conf_data->column_number,
                               conf_data->current_token, __func__, __LINE__);
                  status = -1;
                  goto label_ret;
               }
               break;

            case _AL_CONF_TOKEN_CIPHER_CCMP:
               /* parse cipher_ccmp*/

               _PARSE_INTERMEDIATE_TOKEN('=', _AL_CONF_TOKEN_EQUALS);
               ret = _get_token(AL_CONTEXT conf_data);
               if (ret == -1)
               {
                  al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): unexpected eof file while looking for rsn radius cipher ccmp : %s<%d>\n", conf_data->line_number, conf_data->column_number, __func__, __LINE__);
                  status = -1;
                  goto label_ret;
               }
               ret = al_conf_atoi(conf_data->current_token, &val);
               security_info->_security_rsn_radius.cipher_ccmp = val;
               if (ret == -1)
               {
                  al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): token %s was found instead of an integer : %s<%d>\n",
                               conf_data->line_number,
                               conf_data->column_number,
                               conf_data->current_token, __func__, __LINE__);
                  status = -1;
                  goto label_ret;
               }

               break;

            /* parse group key renewal*/
            case _AL_CONF_TOKEN_GROUP_KEY_RENEWAL:

               _PARSE_INTERMEDIATE_TOKEN('=', _AL_CONF_TOKEN_EQUALS);
               ret = _get_token(AL_CONTEXT conf_data);
               if (ret == -1)
               {
                  al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): unexpected eof file while looking for rsn radius group key renewal : %s<%d>\n", conf_data->line_number, conf_data->column_number, __func__, __LINE__);
                  status = -1;
                  goto label_ret;
               }
               ret = al_conf_atoi(conf_data->current_token, &val);
               security_info->_security_rsn_radius.group_key_renewal = val;
               if (ret == -1)
               {
                  al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): token %s was found instead of an integer : %s<%d>\n",
                               conf_data->line_number,
                               conf_data->column_number,
                               conf_data->current_token, __func__, __LINE__);
                  status = -1;
                  goto label_ret;
               }
               break;
            }
            if (conf_data->current_token_code == _AL_CONF_TOKEN_COMMA)
            {
               continue;
            }
            else if (conf_data->current_token_code == _AL_CONF_TOKEN_CLOSE_BRACE)
            {
               conf_data->security_heirarchy_level--;
            }
         }
      }
   }

label_ret:
   _FREE_LINE_LIST();
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return status;

#undef _PARSE_INTERMEDIATE_TOKEN
}

#if MDE_80211N_SUPPORT 

static int _parse_ht_capab_info(AL_CONTEXT_PARAM_DECL _al_conf_data_t *conf_data, al_ht_capab_t *ht_capab)
{

	struct _line_list
	{
		char				line[_AL_CONF_MAX_TOKEN_SIZE + 1];
		int				line_length;
		struct _line_list		*next;
	};

	typedef struct _line_list   _line_list_t;

	int			ret;
	int			status,val;
	_line_list_t		*head;
	_line_list_t		*tail;

	head = NULL;
	tail = NULL;

	status = 0;
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);


#define _PARSE_INTERMEDIATE_TOKEN(tok, tok_code)                                                                                 \
	do {                                                                                                                         \
		ret = _get_token(AL_CONTEXT conf_data);                                                                                  \
		if (ret == -1) {                                                                                                         \
			al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): unexpected eof file while looking for '%c' : %s<%d>\n",         \
					conf_data->line_number,                                                                                      \
					conf_data->column_number,                                                                                    \
					tok, __func__, __LINE__);                                                                                                        \
			status = -1;                                                                                                         \
			goto label_ret;                                                                                                      \
		}                                                                                                                        \
		if (conf_data->current_token_code != tok_code) {                                                                         \
			al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): expecting token %c but found %s : %s<%d>\n",                    \
					conf_data->line_number,                                                                                      \
					conf_data->column_number,                                                                                    \
					tok,                                                                                                         \
					conf_data->current_token, __func__, __LINE__);                                                                                   \
			status = -1;                                                                                                         \
			goto label_ret;                                                                                                      \
		}                                                                                                                        \
	} while (0)

#define _FREE_LINE_LIST()                   \
	do {                                    \
		tail = head;                        \
		while (tail != NULL) {              \
			head = tail->next;              \
			al_heap_free(AL_CONTEXT tail);  \
			tail = head;                    \
		}                                   \
		head = NULL;                        \
		tail = NULL;                        \
	} while (0)

	_PARSE_INTERMEDIATE_TOKEN('{', _AL_CONF_TOKEN_OPEN_BRACE);

	while (1)
	{
		ret = _get_token(AL_CONTEXT conf_data);
		if (ret == -1)
		{
			al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): unexpected eof file while looking for ht capab info item : %s<%d>\n",
					conf_data->current_token_code,
					__LINE__,
					conf_data->line_number,
					conf_data->column_number, __func__, __LINE__);
			status = -1;
			goto label_ret;
		}

		switch (conf_data->current_token_code)
		{
			case _AL_CONF_TOKEN_LDPC_ENABLED :

				_PARSE_INTERMEDIATE_TOKEN('=', _AL_CONF_TOKEN_EQUALS);
				ret = _get_token(AL_CONTEXT conf_data);
				if (ret == -1)
				{
					al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): unexpected eof file while looking for ldpc enabled : %s<%d>\n", conf_data->line_number, conf_data->column_number, __func__, __LINE__);

					status = -1;
					goto label_ret;
				}
				if (conf_data->current_token_code == _AL_CONF_TOKEN_ENABLED)
				{
					ht_capab->ldpc = AL_CONF_IF_HT_PARAM_ENABLED;
				}
				else if (conf_data->current_token_code == _AL_CONF_TOKEN_DISABLED)
				{
					ht_capab->ldpc= AL_CONF_IF_HT_PARAM_DISABLED;
				}
				else
				{
					al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): invalid ldpc %s specified : %s<%d>\n",
							conf_data->line_number,
							conf_data->column_number,
							conf_data->current_token, __func__, __LINE__);
					status = -1;
					goto label_ret;
				}
                                
				break;


			case _AL_CONF_TOKEN_HT_BANDWIDTH :
				_PARSE_INTERMEDIATE_TOKEN('=', _AL_CONF_TOKEN_EQUALS);
				ret = _get_token(AL_CONTEXT conf_data);
				if (ret == -1)
				{
					al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): unexpected eof file while looking for ht_bandwidth : %s<%d>\n", conf_data->line_number, conf_data->column_number, __func__, __LINE__);
					status = -1;
					goto label_ret;
				}
				if (conf_data->current_token_code == _AL_CONF_TOKEN_40_ABOVE)
				{
					ht_capab->ht_bandwidth = AL_CONF_IF_HT_PARAM_40_ABOVE;
					ht_capab->sec_offset=1;
				}
				else if (conf_data->current_token_code == _AL_CONF_TOKEN_40_BELOW)
				{
					ht_capab->ht_bandwidth = AL_CONF_IF_HT_PARAM_40_BELOW;
				}
				else if (conf_data->current_token_code == _AL_CONF_TOKEN_20)
				{
					ht_capab->ht_bandwidth = AL_CONF_IF_HT_PARAM_20;
				}
			
				else
				{
					al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): invalid ht_bandwidth %s specified : %s<%d>\n",
							conf_data->line_number,
							conf_data->column_number,
							conf_data->current_token, __func__, __LINE__);
					status = -1;
					goto label_ret;
				}
				break;


			case _AL_CONF_TOKEN_GREENFIELD :

				_PARSE_INTERMEDIATE_TOKEN('=', _AL_CONF_TOKEN_EQUALS);
				ret = _get_token(AL_CONTEXT conf_data);
				if (ret == -1)
				{
					al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): unexpected eof file while looking for gfmode : %s<%d>\n",
							conf_data->line_number,
							conf_data->column_number, __func__, __LINE__);

					status = -1;
					goto label_ret;
				}
				if (conf_data->current_token_code == _AL_CONF_TOKEN_ENABLED)
				{
					ht_capab->gfmode = AL_CONF_IF_HT_PARAM_ENABLED;
				}
				else if (conf_data->current_token_code == _AL_CONF_TOKEN_DISABLED)
				{
					ht_capab->gfmode = AL_CONF_IF_HT_PARAM_DISABLED;
				}
				else
				{
					al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): invalid gfmode %s specified : %s<%d>\n",
							conf_data->line_number,
							conf_data->column_number,
							conf_data->current_token, __func__, __LINE__);
					status = -1;
					goto label_ret;
				}
				break;


			case _AL_CONF_TOKEN_SMPS  :

				_PARSE_INTERMEDIATE_TOKEN('=', _AL_CONF_TOKEN_EQUALS);
				ret = _get_token(AL_CONTEXT conf_data);
				if (ret == -1)
				{
					al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): unexpected eof file while looking for smps : %s<%d>\n",
							conf_data->line_number,
							conf_data->column_number, __func__, __LINE__);

					status = -1;
					goto label_ret;
				}
				if (conf_data->current_token_code == _AL_CONF_TOKEN_DISABLED)
				{
					ht_capab->smps = AL_CONF_IF_HT_PARAM_SMPS_DISABLED;
				}
				else if (conf_data->current_token_code == _AL_CONF_TOKEN_STATIC)
				{
					ht_capab->smps= AL_CONF_IF_HT_PARAM_STATIC;
				}
				else if (conf_data->current_token_code == _AL_CONF_TOKEN_DYNAMIC)
				{
					ht_capab->smps= AL_CONF_IF_HT_PARAM_DYNAMIC;
				}
				else
				{
					al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): invalid smps %s specified : %s<%d>\n",
							conf_data->line_number,
							conf_data->column_number,
							conf_data->current_token, __func__, __LINE__);
					status = -1;
					goto label_ret;
				}
				break;


			case _AL_CONF_TOKEN_GI_20  :

				_PARSE_INTERMEDIATE_TOKEN('=', _AL_CONF_TOKEN_EQUALS);
				ret = _get_token(AL_CONTEXT conf_data);
				if (ret == -1)
				{
					al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): unexpected eof file while looking for gi_20 : %s<%d>\n",
							conf_data->line_number,
							conf_data->column_number, __func__, __LINE__);
					status = -1;
					goto label_ret;
				}
				if (conf_data->current_token_code == _AL_CONF_TOKEN_LONG)
				{
					ht_capab->gi_20 = AL_CONF_IF_HT_PARAM_LONG;
				}
				else if (conf_data->current_token_code == _AL_CONF_TOKEN_GI_AUTO)
				{
					ht_capab->gi_20= AL_CONF_IF_HT_PARAM_AUTO;
				}
				else if (conf_data->current_token_code == _AL_CONF_TOKEN_SHORT)
				{
					ht_capab->gi_20= AL_CONF_IF_HT_PARAM_SHORT;
				}
				else
				{
					al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): invalid gi_20 %s specified : %s<%d>\n",
							conf_data->line_number,
							conf_data->column_number,
							conf_data->current_token, __func__, __LINE__);
					status = -1;
					goto label_ret;
				}
				break;


			case _AL_CONF_TOKEN_GI_40 :

				_PARSE_INTERMEDIATE_TOKEN('=', _AL_CONF_TOKEN_EQUALS);
				ret = _get_token(AL_CONTEXT conf_data);
				if (ret == -1)
				{
					al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): unexpected eof file while looking for gi_40 : %s<%d>\n",
							conf_data->line_number,
							conf_data->column_number, __func__, __LINE__);
					status = -1;
					goto label_ret;
				}
				if (conf_data->current_token_code == _AL_CONF_TOKEN_LONG)
				{
					ht_capab->gi_40 = AL_CONF_IF_HT_PARAM_LONG;
				}
				else if (conf_data->current_token_code == _AL_CONF_TOKEN_GI_AUTO)
				{
					ht_capab->gi_40= AL_CONF_IF_HT_PARAM_AUTO;
				}
				else if (conf_data->current_token_code == _AL_CONF_TOKEN_SHORT)
				{
					ht_capab->gi_40= AL_CONF_IF_HT_PARAM_SHORT;
				}
				else
				{
					al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): invalid gi_40 %s specified : %s<%d>\n",
							conf_data->line_number,
							conf_data->column_number,
							conf_data->current_token, __func__, __LINE__);
					status = -1;
					goto label_ret;
				}
				break;


			case _AL_CONF_TOKEN_TX_STBC_ENABLED  :
				_PARSE_INTERMEDIATE_TOKEN('=', _AL_CONF_TOKEN_EQUALS);
				ret = _get_token(AL_CONTEXT conf_data);
				if (ret == -1)
				{
					al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): unexpected eof file while looking for tx_stbc : %s<%d>\n",
							conf_data->line_number,
							conf_data->column_number, __func__, __LINE__);
					status = -1;
					goto label_ret;
				}
				if (conf_data->current_token_code == _AL_CONF_TOKEN_ENABLED)
				{
					ht_capab->tx_stbc = AL_CONF_IF_HT_PARAM_ENABLED;
				}
				else if (conf_data->current_token_code == _AL_CONF_TOKEN_DISABLED)
				{
					ht_capab->tx_stbc= AL_CONF_IF_HT_PARAM_DISABLED;
				}
				else
				{
					al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): invalid tx_stbc %s specified : %s<%d>\n",
							conf_data->line_number,
							conf_data->column_number,
							conf_data->current_token, __func__, __LINE__);
					status = -1;
					goto label_ret;
				}
				break;


			case _AL_CONF_TOKEN_RX_STBC_ENABLED  :
				_PARSE_INTERMEDIATE_TOKEN('=', _AL_CONF_TOKEN_EQUALS);
				ret = _get_token(AL_CONTEXT conf_data);
				if (ret == -1)
				{
					al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): unexpected eof file while looking for rx_stbc :%s<%d>\n",
							conf_data->line_number,
							conf_data->column_number, __func__, __LINE__);
					status = -1;
					goto label_ret;
				}
				if (conf_data->current_token_code == _AL_CONF_TOKEN_ENABLED)
				{
					ht_capab->rx_stbc = AL_CONF_IF_HT_PARAM_ENABLED;
				}
				else if (conf_data->current_token_code == _AL_CONF_TOKEN_DISABLED)
				{
					ht_capab->rx_stbc= AL_CONF_IF_HT_PARAM_DISABLED;
				}
				else
				{
					al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : al_conf.c(%d,%d): invalid rx_stbc %s specified : %s<%d>\n",
							conf_data->line_number,
							conf_data->column_number,
							conf_data->current_token, __func__, __LINE__);
					status = -1;
					goto label_ret;
				}
				break;


			case _AL_CONF_TOKEN_DELAYED_BA_ENABLED:
				_PARSE_INTERMEDIATE_TOKEN('=', _AL_CONF_TOKEN_EQUALS);
				ret = _get_token(AL_CONTEXT conf_data);
				if (ret == -1)
				{
					al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): unexpected eof file while looking delayed_ba : %s<%d>\n",
							conf_data->line_number,
							conf_data->column_number, __func__, __LINE__);

					status = -1;
					goto label_ret;
				}
				if (conf_data->current_token_code == _AL_CONF_TOKEN_ENABLED)
				{
					ht_capab->delayed_ba = AL_CONF_IF_HT_PARAM_ENABLED;
				}
				else if (conf_data->current_token_code == _AL_CONF_TOKEN_DISABLED)
				{
					ht_capab->delayed_ba= AL_CONF_IF_HT_PARAM_DISABLED;
				}
				else
				{
					al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): invalid delayed_ba %s specified : %s<%d>\n",
							conf_data->line_number,
							conf_data->column_number,
							conf_data->current_token, __func__, __LINE__);
					status = -1;
					goto label_ret;
				}
				break;


			case _AL_CONF_TOKEN_MAX_AMSDU_LEN:

				_PARSE_INTERMEDIATE_TOKEN('=', _AL_CONF_TOKEN_EQUALS);
				ret = _get_token(AL_CONTEXT conf_data);
				if (ret == -1)
				{
					al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : al_conf.c(%d,%d): unexpected eof file while looking mac_amsdu_len : %s<%d>\n",
							conf_data->line_number,
							conf_data->column_number, __func__, __LINE__);
					status = -1;
					goto label_ret;
				}
				ret = al_conf_atoi(conf_data->current_token, &val);
				if( val == 0 || val == 1)
				{
					ht_capab->max_amsdu_len = val;
				}
				else
				{
					ht_capab->max_amsdu_len = 1;
				}
				if (ret == -1)
				{
					al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): token %s was found instead of an integer : %s<%d>\n",
							conf_data->line_number,
							conf_data->column_number,
							conf_data->current_token, __func__, __LINE__);
					status = -1;
					goto label_ret;
				}
				break;


			case _AL_CONF_TOKEN_DSSS_CCK_40  :

				_PARSE_INTERMEDIATE_TOKEN('=', _AL_CONF_TOKEN_EQUALS);
				ret = _get_token(AL_CONTEXT conf_data);
				if (ret == -1)
				{
					al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): unexpected eof file while looking dsss_cck_40 : %s<%d>\n",
							conf_data->line_number,
							conf_data->column_number, __func__, __LINE__);
					status = -1;
					goto label_ret;
				}
				if (conf_data->current_token_code == _AL_CONF_TOKEN_DSSS_CCK_40_ALLOW)
				{
					ht_capab->dsss_cck_40 = AL_CONF_IF_HT_PARAM_ALLOW;
				}
				else if (conf_data->current_token_code == _AL_CONF_TOKEN_DSSS_CCK_40_DENY)
				{
					ht_capab->dsss_cck_40= AL_CONF_IF_HT_PARAM_DENY;
				}
				else
				{
					al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): invalid dsss_cck_40 %s specified : %s<%d>\n",
							conf_data->line_number,
							conf_data->column_number,
							conf_data->current_token, __func__, __LINE__);
					status = -1;
					goto label_ret;
				}
				break;


			case _AL_CONF_TOKEN_40_INTOLERANT_ENABLED:

				_PARSE_INTERMEDIATE_TOKEN('=', _AL_CONF_TOKEN_EQUALS);
				ret = _get_token(AL_CONTEXT conf_data);
				if (ret == -1)
				{
					al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): unexpected eof file while looking for 40_intolerant : %s<%d>\n", conf_data->line_number, conf_data->column_number, __func__, __LINE__);
					status = -1;
					goto label_ret;
				}
				if (conf_data->current_token_code == _AL_CONF_TOKEN_ENABLED)
				{
					ht_capab->intolerant = AL_CONF_IF_HT_PARAM_ENABLED;
				}
				else if (conf_data->current_token_code == _AL_CONF_TOKEN_DISABLED)
				{
					ht_capab->intolerant = AL_CONF_IF_HT_PARAM_DISABLED;
				}
				else
				{
					al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): invalid 40_intolerant %s specified : %s<%d>\n",
							conf_data->line_number,
							conf_data->column_number,
							conf_data->current_token, __func__, __LINE__);
					status = -1;
					goto label_ret;

				}
				break;


			case _AL_CONF_TOKEN_LSIG_TXOP_ENABLED:

				_PARSE_INTERMEDIATE_TOKEN('=', _AL_CONF_TOKEN_EQUALS);
				ret = _get_token(AL_CONTEXT conf_data);
				if (ret == -1)
				{
					al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): unexpected eof file while looking lsig_txop : %s<%d>\n",
							conf_data->line_number,
							conf_data->column_number, __func__, __LINE__);
					status = -1;
					goto label_ret;
				}
				if (conf_data->current_token_code == _AL_CONF_TOKEN_ENABLED)
				{
					ht_capab->lsig_txop = AL_CONF_IF_HT_PARAM_ENABLED;
				}
				else if (conf_data->current_token_code == _AL_CONF_TOKEN_DISABLED)
				{
					ht_capab->lsig_txop = AL_CONF_IF_HT_PARAM_DISABLED;
				}
				else
				{
					al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): invalid lsig_txop %s specified : %s<%d>\n",
							conf_data->line_number,
							conf_data->column_number,
							conf_data->current_token, __func__, __LINE__);
					status = -1;
					goto label_ret;
				}

				break;

		}
		if (conf_data->current_token_code == _AL_CONF_TOKEN_COMMA)
		{
			continue;
		}
		else if (conf_data->current_token_code == _AL_CONF_TOKEN_CLOSE_BRACE)
		{
			break;
		}


	}
label_ret:
	_FREE_LINE_LIST();
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
	return status;
#undef _PARSE_INTERMEDIATE_TOKEN
}

static int _parse_fram_agre_info(AL_CONTEXT_PARAM_DECL _al_conf_data_t *conf_data, al_fram_agre_t *frame_aggregation)
{
	struct _line_list
	{
		char                    line[_AL_CONF_MAX_TOKEN_SIZE + 1];
		int                     line_length;
		struct _line_list       *next;
	};

	typedef struct _line_list   _line_list_t;

	int             ret;
	int             status,val;
	_line_list_t    *head;
	_line_list_t    *tail;

	head = NULL;
	tail = NULL;

	status = 0;
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

#define _PARSE_INTERMEDIATE_TOKEN(tok, tok_code)                                                                                 \
	do {                                                                                                                         \
		ret = _get_token(AL_CONTEXT conf_data);                                                                                  \
		if (ret == -1) {                                                                                                         \
			al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): unexpected eof file while looking for '%c' : %s<%d>\n",         \
					conf_data->line_number,                                                                                      \
					conf_data->column_number,                                                                                    \
					tok, __func__, __LINE__);                                                                                                        \
			status = -1;                                                                                                         \
			goto label_ret;                                                                                                      \
		}                                                                                                                        \
		if (conf_data->current_token_code != tok_code) {                                                                         \
			al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): expecting token %c but found %s : %s<%d>\n",                    \
					conf_data->line_number,                                                                                      \
					conf_data->column_number,                                                                                    \
					tok,                                                                                                         \
					conf_data->current_token, __func__, __LINE__);                                                                                   \
			status = -1;                                                                                                         \
			goto label_ret;                                                                                                      \
		}                                                                                                                        \
	} while (0)



#define _FREE_LINE_LIST()                   \
	do {                                    \
		tail = head;                        \
		while (tail != NULL) {              \
			head = tail->next;              \
			al_heap_free(AL_CONTEXT tail);  \
			tail = head;                    \
		}                                   \
		head = NULL;                        \
		tail = NULL;                        \
	} while (0)

	_PARSE_INTERMEDIATE_TOKEN('{', _AL_CONF_TOKEN_OPEN_BRACE);

	while(1)
	{
		ret = _get_token(AL_CONTEXT conf_data);
		if (ret == -1)
		{
			al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): unexpected eof file while looking frame_aggregation info item : %s<%d>\n", conf_data->line_number, conf_data->column_number, __func__, __LINE__);
			status = -1;
			goto label_ret;
		}
		switch (conf_data->current_token_code)
		{
			case _AL_CONF_TOKEN_AMPDU_ENABLED:

				_PARSE_INTERMEDIATE_TOKEN('=', _AL_CONF_TOKEN_EQUALS);
				ret = _get_token(AL_CONTEXT conf_data);
				if (ret == -1)
				{
					al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): unexpected eof file while looking ampdu_enabled : %s<%d>\n",
							conf_data->line_number,
							conf_data->column_number, __func__, __LINE__);
					status = -1;
					goto label_ret;
				}
				ret = al_conf_atoi(conf_data->current_token, &val);
				if( val == 0 || val == 1)
				{
					frame_aggregation->ampdu_enable = val;
				}
				else
				{
					frame_aggregation->ampdu_enable = 1;
				}
				if (ret == -1)
				{
					al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): token %s was found instead of an integer : %s<%d>\n",
							conf_data->line_number,
							conf_data->column_number,
							conf_data->current_token, __func__, __LINE__);
					status = -1;
					goto label_ret;
				}
				break;

			case _AL_CONF_TOKEN_MAX_AMPDU_LEN:            
				_PARSE_INTERMEDIATE_TOKEN('=', _AL_CONF_TOKEN_EQUALS);
				ret = _get_token(AL_CONTEXT conf_data);
				if (ret == -1)
				{
					al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): unexpected eof file while looking for max_ampdu_len : %s<%d>\n", conf_data->line_number, conf_data->column_number, __func__, __LINE__);
					status = -1;
					goto label_ret;
				}
				ret = al_conf_atoi(conf_data->current_token, &val);
				if(val == 8 || val == 16 || val == 32 || val==64)
				{
					frame_aggregation->max_ampdu_len = val;
				}
				else
				{
					frame_aggregation->max_ampdu_len = 64;
				}
				if (ret == -1)
				{
					al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): token %s was found instead of an integer : %s<%d>\n",
							conf_data->line_number,
							conf_data->column_number,
							conf_data->current_token, __func__, __LINE__);
					status = -1;
					goto label_ret;
				}
				break;
		}
		if (conf_data->current_token_code == _AL_CONF_TOKEN_COMMA)
		{
			continue;
		}
		else if (conf_data->current_token_code == _AL_CONF_TOKEN_CLOSE_BRACE)
		{
			break;
		}

	}
label_ret:
	_FREE_LINE_LIST();
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
	return status;
#undef _PARSE_INTERMEDIATE_TOKEN
}
#endif

#if MDE_80211AC_SUPPORT  

static int _parse_vht_capab_info(AL_CONTEXT_PARAM_DECL _al_conf_data_t *conf_data, al_vht_capab_t *vht_capab)
{

	struct _line_list
	{
		char				line[_AL_CONF_MAX_TOKEN_SIZE + 1];
		int				line_length;
		struct _line_list		*next;
	};

	typedef struct _line_list   _line_list_t;

	int				ret;
	int				status,val;
	_line_list_t	*head;
	_line_list_t	*tail;

	head = NULL;
	tail = NULL;

	status = 0;
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);


#define _PARSE_INTERMEDIATE_TOKEN(tok, tok_code)                                                                                 \
	do {                                                                                                                         \
		ret = _get_token(AL_CONTEXT conf_data);                                                                                  \
		if (ret == -1) {                                                                                                         \
			al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): unexpected eof file while looking for '%c' : %s<%d>\n",         \
					conf_data->line_number,                                                                                      \
					conf_data->column_number,                                                                                    \
					tok, __func__, __LINE__);                                                                                                        \
			status = -1;                                                                                                         \
			goto label_ret;                                                                                                      \
		}                                                                                                                        \
		if (conf_data->current_token_code != tok_code) {                                                                         \
			al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): expecting token %c but found %s : %s<%d>\n",                    \
					conf_data->line_number,                                                                                      \
					conf_data->column_number,                                                                                    \
					tok,                                                                                                         \
					conf_data->current_token, __func__, __LINE__);                                                                                   \
			status = -1;                                                                                                         \
			goto label_ret;                                                                                                      \
		}                                                                                                                        \
	} while (0)

#define _FREE_LINE_LIST()                   \
	do {                                    \
		tail = head;                        \
		while (tail != NULL) {              \
			head = tail->next;              \
			al_heap_free(AL_CONTEXT tail);  \
			tail = head;                    \
		}                                   \
		head = NULL;                        \
		tail = NULL;                        \
	} while (0)

	_PARSE_INTERMEDIATE_TOKEN('{', _AL_CONF_TOKEN_OPEN_BRACE);

	while (1)
	{
		ret = _get_token(AL_CONTEXT conf_data);
		if (ret == -1)
		{
			al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): unexpected eof file while looking for vht capab info item : %s<%d>\n",
					conf_data->current_token_code,
					conf_data->line_number,
					conf_data->column_number, __func__, __LINE__);
			status = -1;
			goto label_ret;
		}

		switch (conf_data->current_token_code)
		{
			case _AL_CONF_TOKEN_MAX_MPDU_LEN_ENABLED :

				_PARSE_INTERMEDIATE_TOKEN('=', _AL_CONF_TOKEN_EQUALS);
				ret = _get_token(AL_CONTEXT conf_data);
				if (ret == -1)
				{
					al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): unexpected eof file while looking max_mpdu_len : %s<%d>\n",
							conf_data->line_number,
							conf_data->column_number, __func__, __LINE__);
					status = -1;
					goto label_ret;
				}
				ret = al_conf_atoi(conf_data->current_token, &val);
				if( val == 3895 || val == 7991 || val == 11454)
				{
					vht_capab->max_mpdu_len = val;
				}
				else
				{
					vht_capab->max_mpdu_len = 3895;
				}
				if (ret == -1)
				{
					al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): token %s was found instead of an integer : %s<%d>\n",
							conf_data->line_number,
							conf_data->column_number,
							conf_data->current_token, __func__, __LINE__);
					status = -1;
					goto label_ret;
				}
				break;


			case _AL_CONF_TOKEN_SUPPORTED_CHANNEL_WIDTH :

				_PARSE_INTERMEDIATE_TOKEN('=', _AL_CONF_TOKEN_EQUALS);
				ret = _get_token(AL_CONTEXT conf_data);
				if (ret == -1)
				{
					al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): unexpected eof file while looking supported_channel_width : %s<%d>\n", conf_data->line_number, conf_data->column_number, __func__, __LINE__);
					status = -1;
					goto label_ret;
				}
				ret = al_conf_atoi(conf_data->current_token, &val);
				if( val == 0 || val == 1 || val == 2 )
				{
					vht_capab->supported_channel_width = val;
				}
				else
				{
					vht_capab->supported_channel_width = 0;
				}
				
				if (ret == -1)
				{
					al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): token %s was found instead of an integer : %s<%d>\n",
							conf_data->line_number,
							conf_data->column_number,
							conf_data->current_token, __func__, __LINE__);
					status = -1;
					goto label_ret;
				}
				break;


			case _AL_CONF_TOKEN_RX_LDPC  :

				_PARSE_INTERMEDIATE_TOKEN('=', _AL_CONF_TOKEN_EQUALS);
				ret = _get_token(AL_CONTEXT conf_data);
				if (ret == -1)
				{
					al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): unexpected eof file while looking for rx_ldpc : %s<%d>\n",
							conf_data->line_number,
							conf_data->column_number, __func__, __LINE__);

					status = -1;
					goto label_ret;
				}
				if (conf_data->current_token_code == _AL_CONF_TOKEN_DISABLED)
				{
					vht_capab->rx_ldpc = AL_CONF_IF_HT_PARAM_DISABLED;
				}
				else if (conf_data->current_token_code == _AL_CONF_TOKEN_ENABLED)
				{
					vht_capab->rx_ldpc = AL_CONF_IF_HT_PARAM_ENABLED;
				}
				else
				{
					al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): invalid rx_ldpc %s specified : %s<%d>\n",
							conf_data->line_number,
							conf_data->column_number,
							conf_data->current_token, __func__, __LINE__);
					status = -1;
					goto label_ret;
				}
				break;


			case _AL_CONF_TOKEN_GI_80  :

				_PARSE_INTERMEDIATE_TOKEN('=', _AL_CONF_TOKEN_EQUALS);
				ret = _get_token(AL_CONTEXT conf_data);
				if (ret == -1)
				{
					al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): unexpected eof file while looking for gi_80 : %s<%d>\n",
							conf_data->line_number,
							conf_data->column_number, __func__, __LINE__);
					status = -1;
					goto label_ret;
				}
				if (conf_data->current_token_code == _AL_CONF_TOKEN_LONG)
				{
					vht_capab->gi_80 = AL_CONF_IF_HT_PARAM_LONG;
				}
				else if (conf_data->current_token_code == _AL_CONF_TOKEN_GI_AUTO)
				{
					vht_capab->gi_80= AL_CONF_IF_HT_PARAM_AUTO;
				}
				else if (conf_data->current_token_code == _AL_CONF_TOKEN_SHORT)
				{
					vht_capab->gi_80= AL_CONF_IF_HT_PARAM_SHORT;
				}
				else
				{
					al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): invalid gi_80 %s specified : %s<%d>\n",
							conf_data->line_number,
							conf_data->column_number,
							conf_data->current_token, __func__, __LINE__);
					status = -1;
					goto label_ret;
				}
				break;


			case _AL_CONF_TOKEN_GI_160 :

				_PARSE_INTERMEDIATE_TOKEN('=', _AL_CONF_TOKEN_EQUALS);
				ret = _get_token(AL_CONTEXT conf_data);
				if (ret == -1)
				{
					al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): unexpected eof file while looking for gi_160i : %s<%d>\n",
							conf_data->line_number,
							conf_data->column_number, __func__, __LINE__);
					status = -1;
					goto label_ret;
				}
				if (conf_data->current_token_code == _AL_CONF_TOKEN_LONG)
				{
					vht_capab->gi_160 = AL_CONF_IF_HT_PARAM_LONG;
				}
				else if (conf_data->current_token_code == _AL_CONF_TOKEN_GI_AUTO)
				{
					vht_capab->gi_160= AL_CONF_IF_HT_PARAM_AUTO;
				}
				else if (conf_data->current_token_code == _AL_CONF_TOKEN_SHORT)
				{
					vht_capab->gi_160= AL_CONF_IF_HT_PARAM_SHORT;
				}
				else
				{
					al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): invalid gi_160 %s specified : %s<%d>\n",
							conf_data->line_number,
							conf_data->column_number,
							conf_data->current_token, __func__, __LINE__);
					status = -1;
					goto label_ret;
				}
				break;


			case _AL_CONF_TOKEN_TX_STBC_ENABLED  :
				_PARSE_INTERMEDIATE_TOKEN('=', _AL_CONF_TOKEN_EQUALS);
				ret = _get_token(AL_CONTEXT conf_data);
				if (ret == -1)
				{
					al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): unexpected eof file while looking for vtx_stbc : %s<%d>\n",
							conf_data->line_number,
							conf_data->column_number, __func__, __LINE__);
					status = -1;
					goto label_ret;
				}
				if (conf_data->current_token_code == _AL_CONF_TOKEN_ENABLED)
				{
					vht_capab->tx_stbc = AL_CONF_IF_HT_PARAM_ENABLED;
				}
				else if (conf_data->current_token_code == _AL_CONF_TOKEN_DISABLED)
				{
					vht_capab->tx_stbc= AL_CONF_IF_HT_PARAM_DISABLED;
				}
				else
				{
					al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): invalid vtx_stbc %s specified : %s<%d>\n",
							conf_data->line_number,
							conf_data->column_number,
							conf_data->current_token, __func__, __LINE__);
					status = -1;
					goto label_ret;
				}
				break;


			case _AL_CONF_TOKEN_RX_STBC_ENABLED  :
				_PARSE_INTERMEDIATE_TOKEN('=', _AL_CONF_TOKEN_EQUALS);
				ret = _get_token(AL_CONTEXT conf_data);
				if (ret == -1)
				{
					al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): unexpected eof file while looking for vrx_stbc enabled : %s<%d>\n", conf_data->line_number, conf_data->column_number, __func__, __LINE__);
					status = -1;
					goto label_ret;
				}
				if (conf_data->current_token_code == _AL_CONF_TOKEN_ENABLED)
				{
					vht_capab->rx_stbc = AL_CONF_IF_HT_PARAM_ENABLED;
				}
				else if (conf_data->current_token_code == _AL_CONF_TOKEN_DISABLED)
				{
					vht_capab->rx_stbc= AL_CONF_IF_HT_PARAM_DISABLED;
				}
				else
				{
					al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): invalid vrx_stbc %s specified : %s<%d>\n",
							conf_data->line_number,
							conf_data->column_number,
							conf_data->current_token, __func__, __LINE__);
					status = -1;
					goto label_ret;
				}
				break;


			case _AL_CONF_TOKEN_SU_BEAMFORMER_CAPAB:
				_PARSE_INTERMEDIATE_TOKEN('=', _AL_CONF_TOKEN_EQUALS);
				ret = _get_token(AL_CONTEXT conf_data);
				if (ret == -1)
				{
					al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): unexpected eof file while looking su_beamformer_capab : %s<%d>\n", conf_data->line_number, conf_data->column_number, __func__, __LINE__);

					status = -1;
					goto label_ret;
				}
				if (conf_data->current_token_code == _AL_CONF_TOKEN_YES)
				{
					vht_capab->su_beamformer_cap = AL_CONF_IF_VHT_PARAM_YES;
				}
				else if (conf_data->current_token_code == _AL_CONF_TOKEN_NO)
				{
					vht_capab->su_beamformer_cap= AL_CONF_IF_VHT_PARAM_NO;
				}
				else
				{
					al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): invalid su_beamformer_capab %s specified : %s<%d>\n",
							conf_data->line_number,
							conf_data->column_number,
							conf_data->current_token, __func__, __LINE__);
					status = -1;
					goto label_ret;
				}
				break;


			case _AL_CONF_TOKEN_SU_BEAMFORMEE_CAPAB:
				_PARSE_INTERMEDIATE_TOKEN('=', _AL_CONF_TOKEN_EQUALS);
				ret = _get_token(AL_CONTEXT conf_data);
				if (ret == -1)
				{
					al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): unexpected eof file while looking su_beamformee_capab : %s<%d>\n", conf_data->line_number, conf_data->column_number, __func__, __LINE__);

					status = -1;
					goto label_ret;
				}
				if (conf_data->current_token_code == _AL_CONF_TOKEN_YES)
				{
					vht_capab->su_beamformee_cap = AL_CONF_IF_VHT_PARAM_YES;
				}
				else if (conf_data->current_token_code == _AL_CONF_TOKEN_NO)
				{
					vht_capab->su_beamformee_cap = AL_CONF_IF_VHT_PARAM_NO;
				}
				else
				{
					al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR: al_conf.c(%d,%d): invalid su_beamformee_capab %s specified : %s<%d>\n",
							conf_data->line_number,
							conf_data->column_number,
							conf_data->current_token, __func__, __LINE__);
					status = -1;
					goto label_ret;
				}
				break;


			case _AL_CONF_TOKEN_BEAMFORMEE_STS_COUNT:

				_PARSE_INTERMEDIATE_TOKEN('=', _AL_CONF_TOKEN_EQUALS);
				ret = _get_token(AL_CONTEXT conf_data);
				if (ret == -1)
				{
					al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): unexpected eof file while looking beamformee_sts_count : %s<%>\n", conf_data->line_number, conf_data->column_number, __func__, __LINE__);
					status = -1;
					goto label_ret;
				}
				ret = al_conf_atoi(conf_data->current_token, &val);
				if( val == 1 || val == 2 || val == 3 )
				{
					vht_capab->beamformee_sts_count = val;
				}
				else
				{
					vht_capab->beamformee_sts_count = 1;
				}
				if (ret == -1)
				{
					al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): token %s was found instead of an integer : %s<%d>\n",
							conf_data->line_number,
							conf_data->column_number,
							conf_data->current_token, __func__, __LINE__);
					status = -1;
					goto label_ret;
				}
				break;


			case _AL_CONF_TOKEN_SOUNDING_DIMENSIONS:

				_PARSE_INTERMEDIATE_TOKEN('=', _AL_CONF_TOKEN_EQUALS);
				ret = _get_token(AL_CONTEXT conf_data);
				if (ret == -1)
				{
					al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): unexpected eof file while looking sounding dimensions : %s<%d>\n", conf_data->line_number, conf_data->column_number, __func__, __LINE__);
					status = -1;
					goto label_ret;
				}
				ret = al_conf_atoi(conf_data->current_token, &val);
				if( val == 1 || val == 2 || val == 3 )
                                {
                                        vht_capab->sounding_dimensions = val;
                                }
                                else
                                {
                                        vht_capab->sounding_dimensions = 1;
                                }
				if (ret == -1)
				{
					al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): token %s was found instead of an integer : %s<%d>\n",
							conf_data->line_number,
							conf_data->column_number,
							conf_data->current_token, __func__, __LINE__);
					status = -1;
					goto label_ret;
				}
				break;


			case _AL_CONF_TOKEN_MU_BEAMFORMER_CAPAB:
				_PARSE_INTERMEDIATE_TOKEN('=', _AL_CONF_TOKEN_EQUALS);
				ret = _get_token(AL_CONTEXT conf_data);
				if (ret == -1)
				{
					al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): unexpected eof file while looking mu_beamformer_capab : %s<%d>\n", conf_data->line_number, conf_data->column_number, __func__, __LINE__);

					status = -1;
					goto label_ret;
				}
				if (conf_data->current_token_code == _AL_CONF_TOKEN_YES)
				{
					vht_capab->mu_beamformer_cap = AL_CONF_IF_VHT_PARAM_YES;
				}
				else if (conf_data->current_token_code == _AL_CONF_TOKEN_NO)
				{
					vht_capab->mu_beamformer_cap= AL_CONF_IF_VHT_PARAM_NO;
				}
				else
				{
					al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): invalid mu_beamformer_capab %s specified : %s<%d>\n",
							conf_data->line_number,
							conf_data->column_number,
							conf_data->current_token, __func__, __LINE__);
					status = -1;
					goto label_ret;
				}
				break;
	

			case _AL_CONF_TOKEN_MU_BEAMFORMEE_CAPAB:
				_PARSE_INTERMEDIATE_TOKEN('=', _AL_CONF_TOKEN_EQUALS);
				ret = _get_token(AL_CONTEXT conf_data);
				if (ret == -1)
				{
					al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): unexpected eof file while looking mu_beamformee_capab : %s<%d>\n", conf_data->line_number, conf_data->column_number, __func__, __LINE__);

					status = -1;
					goto label_ret;
				}
				if (conf_data->current_token_code == _AL_CONF_TOKEN_YES)
				{
					vht_capab->mu_beamformee_cap = AL_CONF_IF_VHT_PARAM_YES;
				}
				else if (conf_data->current_token_code == _AL_CONF_TOKEN_NO)
				{
					vht_capab->mu_beamformee_cap= AL_CONF_IF_VHT_PARAM_NO;
				}
				else
				{
					al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): invalid mu_beamformee_capab %s specified : %s<%d>\n",
							conf_data->line_number,
							conf_data->column_number,
							conf_data->current_token, __func__, __LINE__);
					status = -1;
					goto label_ret;
				}
				break;


			case _AL_CONF_TOKEN_VHT_TXOP_PS:
				_PARSE_INTERMEDIATE_TOKEN('=', _AL_CONF_TOKEN_EQUALS);
				ret = _get_token(AL_CONTEXT conf_data);
				if (ret == -1)
				{
					al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): unexpected eof file while looking vht_txop_ps :%s<%d>\n",
							conf_data->line_number,
							conf_data->column_number, __func__, __LINE__);

					status = -1;
					goto label_ret;
				}
				if (conf_data->current_token_code == _AL_CONF_TOKEN_YES)
				{
					vht_capab->vht_txop_ps = AL_CONF_IF_VHT_PARAM_YES;
				}
				else if (conf_data->current_token_code == _AL_CONF_TOKEN_NO)
				{
					vht_capab->vht_txop_ps = AL_CONF_IF_VHT_PARAM_NO;
				}
				else
				{
					al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): invalid vht_txop_ps %s specified : %s<%d>\n",
							conf_data->line_number,
							conf_data->column_number,
							conf_data->current_token, __func__, __LINE__);
					status = -1;
					goto label_ret;
				}
				break;


			case _AL_CONF_TOKEN_HTC_VHT_CAPAB:
				_PARSE_INTERMEDIATE_TOKEN('=', _AL_CONF_TOKEN_EQUALS);
				ret = _get_token(AL_CONTEXT conf_data);
				if (ret == -1)
				{
					al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): unexpected eof file while looking htc_vht_capab : %s<%d>\n",
							conf_data->line_number,
							conf_data->column_number, __func__, __LINE__);

					status = -1;
					goto label_ret;
				}
				if (conf_data->current_token_code == _AL_CONF_TOKEN_YES)
				{
					vht_capab->htc_vht_cap = AL_CONF_IF_VHT_PARAM_YES;
				}
				else if (conf_data->current_token_code == _AL_CONF_TOKEN_NO)
				{
					vht_capab->htc_vht_cap = AL_CONF_IF_VHT_PARAM_NO;
				}
				else
				{
					al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): invalid htc_vht_capab %s specified : %s<%d>\n",
							conf_data->line_number,
							conf_data->column_number,
							conf_data->current_token, __func__, __LINE__);
					status = -1;
					goto label_ret;
				}
				break;


			case _AL_CONF_TOKEN_RX_ANT_PATTERN_CONSISTENCY:
				_PARSE_INTERMEDIATE_TOKEN('=', _AL_CONF_TOKEN_EQUALS);
				ret = _get_token(AL_CONTEXT conf_data);
				if (ret == -1)
				{
					al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): unexpected eof file while looking rx_ant_pattern_consistency : %s<%d>\n", conf_data->line_number, conf_data->column_number, __func__, __LINE__);

					status = -1;
					goto label_ret;
				}
				if (conf_data->current_token_code == _AL_CONF_TOKEN_YES)
				{
					vht_capab->rx_ant_pattern_consistency = AL_CONF_IF_VHT_PARAM_YES;
				}
				else if (conf_data->current_token_code == _AL_CONF_TOKEN_NO)
				{
					vht_capab->rx_ant_pattern_consistency = AL_CONF_IF_VHT_PARAM_NO;
				}
				else
				{
					al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): invalid rx_ant_pattern_consistency %s specified : %s<%d>\n",
							conf_data->line_number,
							conf_data->column_number,
							conf_data->current_token, __func__, __LINE__);
					status = -1;
					goto label_ret;
				}
				break;


			case _AL_CONF_TOKEN_TX_ANT_PATTERN_CONSISTENCY:
				_PARSE_INTERMEDIATE_TOKEN('=', _AL_CONF_TOKEN_EQUALS);
				ret = _get_token(AL_CONTEXT conf_data);
				if (ret == -1)
				{
					al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): unexpected eof file while looking tx_ant_pattern_consistency : %s<%d>\n", conf_data->line_number, conf_data->column_number, __func__, __LINE__);

					status = -1;
					goto label_ret;
				}
				if (conf_data->current_token_code == _AL_CONF_TOKEN_YES)
				{
					vht_capab->tx_ant_pattern_consistency = AL_CONF_IF_VHT_PARAM_YES;
				}
				else if (conf_data->current_token_code == _AL_CONF_TOKEN_NO)
				{
					vht_capab->tx_ant_pattern_consistency = AL_CONF_IF_VHT_PARAM_NO;
				}
				else
				{
					al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): invalid tx_ant_pattern_consistency %s specified : %s<%d>\n",
							conf_data->line_number,
							conf_data->column_number,
							conf_data->current_token,__func__, __LINE__);
					status = -1;
					goto label_ret;
				}
				break;


			case _AL_CONF_TOKEN_VHT_OPER_BANDWIDTH:

				_PARSE_INTERMEDIATE_TOKEN('=', _AL_CONF_TOKEN_EQUALS);
				ret = _get_token(AL_CONTEXT conf_data);
				if (ret == -1)
				{
					al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): unexpected eof file while looking vht_oper_bandwidth : %s<%d>\n", conf_data->line_number, conf_data->column_number, __func__, __LINE__);
					status = -1;
					goto label_ret;
				}
				ret = al_conf_atoi(conf_data->current_token, &val);
				if( val == 0 || val ==1 || val == 2 || val ==3 )
				{
					vht_capab->vht_oper_bandwidth = val;
				}
				else
				{
					vht_capab->vht_oper_bandwidth = 1;
				}
					
				if (ret == -1)
				{
					al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): token %s was found instead of an integer : %s<%d>\n",
							conf_data->line_number,
							conf_data->column_number,
							conf_data->current_token, __func__, __LINE__);
					status = -1;
					goto label_ret;
				}
				break;


			case _AL_CONF_TOKEN_SEG0_CENTER_FREQ:

				_PARSE_INTERMEDIATE_TOKEN('=', _AL_CONF_TOKEN_EQUALS);
				ret = _get_token(AL_CONTEXT conf_data);
				if (ret == -1)
				{
					al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): unexpected eof file while looking seg0_center_freq : %s<%d>\n", conf_data->line_number, conf_data->column_number, __func__, __LINE__);
					status = -1;
					goto label_ret;
				}
				ret = al_conf_atoi(conf_data->current_token, &val);
				if( val == 0 || val == 42 || val == 58 || val == 155)
				{
					vht_capab->seg0_center_freq = val;
				}
				else
				{
					vht_capab->seg0_center_freq = 42;

				}

				if (ret == -1)
				{
					al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): token %s was found instead of an integer : %s<%d>\n",
							conf_data->line_number,
							conf_data->column_number,
							conf_data->current_token, __func__, __LINE__);
					status = -1;
					goto label_ret;
				}
				break;


			case _AL_CONF_TOKEN_SEG1_CENTER_FREQ:

				_PARSE_INTERMEDIATE_TOKEN('=', _AL_CONF_TOKEN_EQUALS);
				ret = _get_token(AL_CONTEXT conf_data);
				if (ret == -1)
				{
					al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): unexpected eof file while looking seg1_center_freq : %s<%d>\n", conf_data->line_number, conf_data->column_number, __func__, __LINE__);
					status = -1;
					goto label_ret;
				}
				ret = al_conf_atoi(conf_data->current_token, &val);
				if( val == 0 || val == 42 || val == 58 || val == 155)
				{
					vht_capab->seg1_center_freq = val;
				}
				else
				{
					vht_capab->seg1_center_freq = 0;

				}
				if (ret == -1)
				{
					al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf.c(%d,%d): token %s was found instead of an integer : %s<%d>\n",
							conf_data->line_number,
							conf_data->column_number,
							conf_data->current_token, __func__, __LINE__);
					status = -1;
					goto label_ret;
				}
				break;

		}
		if (conf_data->current_token_code == _AL_CONF_TOKEN_COMMA)
		{
			continue;
		}
		else if (conf_data->current_token_code == _AL_CONF_TOKEN_CLOSE_BRACE)
		{
			break;
		}


	}
label_ret:
	_FREE_LINE_LIST();
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
	return status;
#undef _PARSE_INTERMEDIATE_TOKEN
}

#endif


int al_conf_open(AL_CONTEXT_PARAM_DECL_SINGLE)
{
   _al_conf_data_t *data;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   data = (_al_conf_data_t *)al_heap_alloc(AL_CONTEXT sizeof(_al_conf_data_t)AL_HEAP_DEBUG_PARAM);

   memset(data, 0, sizeof(_al_conf_data_t));

   data->parser_buffer        = NULL;
   data->pos                  = NULL;
   data->curent_token_pos     = NULL;
   data->mesh_imcp_key_buffer = NULL;
   
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return (int)data;
}


int al_conf_open_file(AL_CONTEXT_PARAM_DECL_SINGLE)
{
   struct _list
   {
      char         line[256];
      struct _list *next;
   }
   *head, *node, *tail, *temp;

   int  handle;
   int  ret;
   char line_buffer[256];
   int  total_size;
   char *conf_string;
   char *pdst;
   char *psrc;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   head       = NULL;
   tail       = NULL;
   total_size = 0;

   handle = al_open_file(AL_CONTEXT AL_FILE_ID_MESHAP_CONFIG, AL_OPEN_FILE_MODE_READ);

   if (handle == (int)NULL)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : al_conf_open_file - handle == (int)NULL : %s<%d>\n", __func__, __LINE__);
      return -1;
   }

   while (1)
   {
      ret = al_read_file_line(AL_CONTEXT handle, line_buffer, 255);
      if (ret != 0)
      {
         break;
      }
      if (head == NULL)
      {
         head = (struct _list *)al_heap_alloc(AL_CONTEXT sizeof(struct _list)AL_HEAP_DEBUG_PARAM);
         tail = head;
         node = head;
      }
      else
      {
         node       = (struct _list *)al_heap_alloc(AL_CONTEXT sizeof(struct _list)AL_HEAP_DEBUG_PARAM);
         tail->next = node;
      }
      node->next = NULL;
      strcpy(node->line, line_buffer);
      total_size += strlen(line_buffer);
      tail        = node;
   }

   al_close_file(AL_CONTEXT handle);

   conf_string = (char *)al_heap_alloc(AL_CONTEXT(total_size + 1) AL_HEAP_DEBUG_PARAM);
   pdst        = conf_string;
   node        = head;

   while (node != NULL)
   {
      psrc = node->line;
      while (*psrc)
      {
         *pdst++ = *psrc++;
      }
      temp = node->next;
      al_heap_free(AL_CONTEXT node);
      node = temp;
   }

   *pdst = 0;

   ret = al_conf_parse(AL_CONTEXT conf_string);

   al_heap_free(AL_CONTEXT conf_string);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);

   return ret;
}


int al_conf_parse(AL_CONTEXT_PARAM_DECL const char *al_conf_string)
{
   _al_conf_data_t  *data;
   int              ret;
   al_conf_option_t *option;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   data = (_al_conf_data_t *)al_heap_alloc(AL_CONTEXT sizeof(_al_conf_data_t)AL_HEAP_DEBUG_PARAM);

   memset(data, 0, sizeof(_al_conf_data_t));

   data->parser_buffer = (char *)al_heap_alloc(AL_CONTEXT strlen(al_conf_string) + 1  AL_HEAP_DEBUG_PARAM);
   strcpy(data->parser_buffer, al_conf_string);

   data->pos         = data->parser_buffer;
   data->line_number = 1;

   ret = _parse(AL_CONTEXT data);

   if (ret != 0)
   {
      al_heap_free(AL_CONTEXT data->parser_buffer);

      if (data->if_info != NULL)
      {
         al_heap_free(AL_CONTEXT data->if_info);
      }

      if (data->criteria_root != NULL)
      {
         _free_effistream_info(AL_CONTEXT data->criteria_root);
      }

      while (data->option_info != NULL)
      {
         option = data->option_info->next;
         al_heap_free(AL_CONTEXT data->option_info);
         data->option_info = option;
      }

      al_heap_free(AL_CONTEXT data);
	  al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : _parse() failed : %s<%d>\n", __func__, __LINE__, __func__, __LINE__);

      return 0;
   }

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return (int)data;
}


void al_conf_close(AL_CONTEXT_PARAM_DECL unsigned int al_conf_handle)
{
   _al_conf_data_t  *data;
   al_conf_option_t *option;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);
   data = (_al_conf_data_t *)al_conf_handle;

   if (data == NULL)
   {
	  al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR: Data is NULL : %s<%d>\n", __func__, __LINE__);
      return;
   }

   if (data->parser_buffer != NULL)
   {
      al_heap_free(AL_CONTEXT data->parser_buffer);
   }

   if (data->if_info != NULL)
   {
      al_heap_free(AL_CONTEXT data->if_info);
   }

   /*TODO free keys,security etc */

   if (data->vlan_info != NULL)
   {
      al_heap_free(AL_CONTEXT data->vlan_info);
   }

   if (data->mesh_imcp_key_buffer != NULL)
   {
      al_heap_free(AL_CONTEXT data->mesh_imcp_key_buffer);
   }

   if (data->criteria_root != NULL)
   {
      _free_effistream_info(AL_CONTEXT data->criteria_root);
   }

   while (data->option_info != NULL)
   {
      option = data->option_info->next;
      al_heap_free(AL_CONTEXT data->option_info);
      data->option_info = option;
   }
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   al_heap_free(AL_CONTEXT data);
}


int al_conf_get_name(AL_CONTEXT_PARAM_DECL int al_conf_handle, char *name_buffer, int buffer_length)
{
   _al_conf_data_t *data;
   int             length;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   data = (_al_conf_data_t *)al_conf_handle;

   length = strlen(data->name) + 1;

   if (buffer_length < length)
   {
	  al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : buffer_length error %s<%d>\n", __func__, __LINE__);
      return length;
   }

   strcpy(name_buffer, data->name);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);

   return 0;
}


int al_conf_get_description(AL_CONTEXT_PARAM_DECL int al_conf_handle, char *desc_buffer, int buffer_length)
{
   _al_conf_data_t *data;
   int             length;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   data = (_al_conf_data_t *)al_conf_handle;

   length = strlen(data->description) + 1;

   if (buffer_length < length)
   {
	  al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : buffer_length error %s<%d>\n", __func__, __LINE__);
      return length;
   }

   strcpy(desc_buffer, data->description);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);

   return 0;
}


int al_conf_get_mesh_id(AL_CONTEXT_PARAM_DECL int al_conf_handle, char *mesh_id_buffer, int buffer_length)
{
   _al_conf_data_t *data;
   int             length;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   data = (_al_conf_data_t *)al_conf_handle;

   length = strlen(data->mesh_id) + 1;

   if (buffer_length < length)
   {
	  al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : buffer_length error %s<%d>\n", __func__, __LINE__);
      return length;
   }

   strcpy(mesh_id_buffer, data->mesh_id);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);

   return 0;
}


int al_conf_get_mesh_imcp_key_buffer(AL_CONTEXT_PARAM_DECL unsigned int al_conf_handle, char *mesh_imcp_key_buffer, int buffer_length)
{
   _al_conf_data_t *data;
   int             length;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   data = (_al_conf_data_t *)al_conf_handle;

   length = strlen(data->mesh_imcp_key_buffer) + 1;

   if (buffer_length < length)
   {
	  al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : buffer_length error %s<%d>\n", __func__, __LINE__);
      return length;
   }

   strcpy(mesh_imcp_key_buffer, data->mesh_imcp_key_buffer);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);

   return 0;
}


int al_conf_get_essid(AL_CONTEXT_PARAM_DECL int al_conf_handle, char *essid_buffer, int buffer_length)
{
   _al_conf_data_t *data;
   int             length;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   data = (_al_conf_data_t *)al_conf_handle;

   length = strlen(data->essid) + 1;

   if (buffer_length < length)
   {
	  al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : buffer_length error %s<%d>\n", __func__, __LINE__);
      return length;
   }

   strcpy(essid_buffer, data->essid);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);

   return 0;
}


int al_conf_get_rts_th(AL_CONTEXT_PARAM_DECL int al_conf_handle)
{
   _al_conf_data_t *data;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   data = (_al_conf_data_t *)al_conf_handle;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return data->rts_th;
}


int al_conf_get_frag_th(AL_CONTEXT_PARAM_DECL int al_conf_handle)
{
   _al_conf_data_t *data;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   data = (_al_conf_data_t *)al_conf_handle;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return data->frag_th;
}


int al_conf_get_beacon_interval(AL_CONTEXT_PARAM_DECL int al_conf_handle)
{
   _al_conf_data_t *data;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   data = (_al_conf_data_t *)al_conf_handle;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return data->beacon_interval;
}

int al_conf_get_dynamic_channel_allocation(AL_CONTEXT_PARAM_DECL int al_conf_handle)
{
   _al_conf_data_t *data;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   data = (_al_conf_data_t *)al_conf_handle;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return data->dynamic_channel_allocation;
}


int al_conf_get_stay_awake_count(AL_CONTEXT_PARAM_DECL int al_conf_handle)
{
   _al_conf_data_t *data;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   data = (_al_conf_data_t *)al_conf_handle;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return data->stay_awake_count;
}
int al_conf_get_sig_threshold(AL_CONTEXT_PARAM_DECL int al_conf_handle)
{
   _al_conf_data_t *data;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);
   data = (_al_conf_data_t *)al_conf_handle;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return data->sig_threshold;
}

int al_conf_get_bridge_ageing_time(AL_CONTEXT_PARAM_DECL int al_conf_handle)
{
   _al_conf_data_t *data;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   data = (_al_conf_data_t *)al_conf_handle;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return data->bridge_ageing_time;
}


int al_conf_get_wds_encrypted(AL_CONTEXT_PARAM_DECL int al_conf_handle)
{
   _al_conf_data_t *data;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   data = (_al_conf_data_t *)al_conf_handle;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return data->wds_encrypted;
}


int al_conf_get_model(AL_CONTEXT_PARAM_DECL int al_conf_handle, char *model_name, int model_name_length)
{
   _al_conf_data_t *data;
   int             length;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   data = (_al_conf_data_t *)al_conf_handle;

   length = strlen(data->model) + 1;

   if (model_name_length < length)
   {
	  al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : model_name_length(%d) ERROR : %s<%d>\n",model_name_length, __func__, __LINE__);
      return length;
   }

   strcpy(model_name, data->model);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);

   return 0;
}


int al_conf_get_gps_x_coordinate(AL_CONTEXT_PARAM_DECL int al_conf_handle, char *gps_x_coordinate, int coordinate_length)
{
   _al_conf_data_t *data;
   int             length;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   data = (_al_conf_data_t *)al_conf_handle;

   length = strlen(data->gps_x_coordinate) + 1;

   if (coordinate_length < length)
   {
	  al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : coordinate_length(%d) ERROR : %s<%d>\n", coordinate_length, __func__, __LINE__);
      return length;
   }

   strcpy(gps_x_coordinate, data->gps_x_coordinate);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);

   return 0;
}


int al_conf_get_gps_y_coordinate(AL_CONTEXT_PARAM_DECL int al_conf_handle, char *gps_y_coordinate, int coordinate_length)
{
   _al_conf_data_t *data;
   int             length;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   data = (_al_conf_data_t *)al_conf_handle;

   length = strlen(data->gps_y_coordinate) + 1;

   if (coordinate_length < length)
   {
	  al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : coordinate_length(%d) ERROR : %s<%d>\n", coordinate_length, __func__, __LINE__);
      return length;
   }

   strcpy(gps_y_coordinate, data->gps_y_coordinate);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);

   return 0;
}


int al_conf_get_regulatory_domain(AL_CONTEXT_PARAM_DECL int al_conf_handle)
{
   _al_conf_data_t *data;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   data = (_al_conf_data_t *)al_conf_handle;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return data->regulatory_domain;
}


int al_conf_get_country_code(AL_CONTEXT_PARAM_DECL int al_conf_handle)
{
   _al_conf_data_t *data;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   data = (_al_conf_data_t *)al_conf_handle;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return data->country_code;
}


int al_conf_get_fcc_certified(AL_CONTEXT_PARAM_DECL int al_conf_handle)
{
   _al_conf_data_t *data;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   data = (_al_conf_data_t *)al_conf_handle;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return data->fcc_certified_operation;
}


int al_conf_get_etsi_certified(AL_CONTEXT_PARAM_DECL int al_conf_handle)
{
   _al_conf_data_t *data;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   data = (_al_conf_data_t *)al_conf_handle;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return data->etsi_certified_operation;
}


int al_conf_get_ds_tx_rate(AL_CONTEXT_PARAM_DECL int al_conf_handle)
{
   _al_conf_data_t *data;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   data = (_al_conf_data_t *)al_conf_handle;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return data->ds_tx_rate;
}


int al_conf_get_ds_tx_power(AL_CONTEXT_PARAM_DECL int al_conf_handle)
{
   _al_conf_data_t *data;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   data = (_al_conf_data_t *)al_conf_handle;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return data->ds_tx_power;
}


int al_conf_get_if_count(AL_CONTEXT_PARAM_DECL unsigned int al_conf_handle)
{
   _al_conf_data_t *data;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   data = (_al_conf_data_t *)al_conf_handle;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return data->if_count;
}


int al_conf_get_if(AL_CONTEXT_PARAM_DECL unsigned int al_conf_handle, int if_index, al_conf_if_info_t *if_info)
{
   _al_conf_data_t *data;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   data = (_al_conf_data_t *)al_conf_handle;

   if (if_index >= data->if_count)
   {
	  al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : if_index(%d) error : %s<%d>\n",if_index, __func__, __LINE__);
      return -1;
   }

	memcpy(if_info, &data->if_info[if_index], sizeof(al_conf_if_info_t));
    

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return 0;
}


int al_conf_get_vlan_count(AL_CONTEXT_PARAM_DECL int al_conf_handle)
{
   _al_conf_data_t *data;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   data = (_al_conf_data_t *)al_conf_handle;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return data->vlan_count;
}


int al_conf_get_vlan(AL_CONTEXT_PARAM_DECL int al_conf_handle, int vlan_index, al_conf_vlan_info_t *vlan_info)
{
   _al_conf_data_t *data;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   data = (_al_conf_data_t *)al_conf_handle;

   if (vlan_index >= data->vlan_count)
   {
	  al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : vlan_index(%d) error : %s<%d>\n",vlan_index, __func__, __LINE__);
      return -1;
   }

   memcpy(vlan_info, &data->vlan_info[vlan_index], sizeof(al_conf_vlan_info_t));

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return 0;
}


int al_conf_get_preferred_parent(AL_CONTEXT_PARAM_DECL int al_conf_handle, al_net_addr_t *preferred_parent)
{
   _al_conf_data_t *data;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   data = (_al_conf_data_t *)al_conf_handle;

   memcpy(preferred_parent->bytes, data->preferred_parent, 6);
   preferred_parent->length = 6;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return 0;
}


int al_conf_get_signal_map(AL_CONTEXT_PARAM_DECL int al_conf_handle, unsigned char *signal_map)
{
   _al_conf_data_t *data;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   data = (_al_conf_data_t *)al_conf_handle;

   memcpy(signal_map, data->signal_map, 8);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return 0;
}


int al_conf_get_heartbeat_interval(AL_CONTEXT_PARAM_DECL int al_conf_handle)
{
   _al_conf_data_t *data;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   data = (_al_conf_data_t *)al_conf_handle;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return data->heartbeat_interval;
}


int al_conf_get_heartbeat_miss_count(AL_CONTEXT_PARAM_DECL int al_conf_handle)
{
   _al_conf_data_t *data;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   data = (_al_conf_data_t *)al_conf_handle;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return data->heartbeat_miss_count;
}


int al_conf_get_hop_cost(AL_CONTEXT_PARAM_DECL int al_conf_handle)
{
   _al_conf_data_t *data;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   data = (_al_conf_data_t *)al_conf_handle;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return data->hop_cost;
}


int al_conf_get_max_allowable_hops(AL_CONTEXT_PARAM_DECL int al_conf_handle)
{
   _al_conf_data_t *data;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   data = (_al_conf_data_t *)al_conf_handle;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return data->max_allowable_hops;
}


int al_conf_get_las_interval(AL_CONTEXT_PARAM_DECL int al_conf_handle)
{
   _al_conf_data_t *data;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   data = (_al_conf_data_t *)al_conf_handle;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return data->las_interval;
}


int al_conf_get_failover_enabled(AL_CONTEXT_PARAM_DECL int al_conf_handle)
{
   _al_conf_data_t *data;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   data = (_al_conf_data_t *)al_conf_handle;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return data->failover_enabled;
}


int al_conf_set_failover_enabled(AL_CONTEXT_PARAM_DECL int al_conf_handle, int on_off, int power_on_default, int scan_freq_secs, char *server_ip_addr)
{
   _al_conf_data_t *data;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   data = (_al_conf_data_t *)al_conf_handle;

   data->failover_enabled = on_off;

   data->power_on_default = power_on_default;

   al_conf_set_server_addr(al_conf_handle, server_ip_addr);

   //	data->scan_freq_secs = scan_freq_secs;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return 0;
}


int al_conf_get_power_on_default(AL_CONTEXT_PARAM_DECL int al_conf_handle)
{
   _al_conf_data_t *data;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   data = (_al_conf_data_t *)al_conf_handle;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return data->power_on_default;
}


unsigned char *al_conf_get_server_addr(AL_CONTEXT_PARAM_DECL int al_conf_handle)
{
   _al_conf_data_t *data;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   data = (_al_conf_data_t *)al_conf_handle;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return data->server_ip_addr;
}


int al_conf_set_server_addr(AL_CONTEXT_PARAM_DECL int al_conf_handle, unsigned char *server_addr)
{
   _al_conf_data_t *data;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   data = (_al_conf_data_t *)al_conf_handle;

   memset(data->server_ip_addr, 0, 128);
   memcpy(data->server_ip_addr, server_addr, strlen(server_addr));

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return 0;
}

unsigned char *al_conf_get_mgmt_gw_addr(AL_CONTEXT_PARAM_DECL int al_conf_handle)
{
   _al_conf_data_t *data;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   data = (_al_conf_data_t *)al_conf_handle;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return data->mgmt_gw_addr;
}


int al_conf_set_mgmt_gw_addr(AL_CONTEXT_PARAM_DECL int al_conf_handle, unsigned char *mgmt_gw_addr)
{
   _al_conf_data_t *data;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   data = (_al_conf_data_t *)al_conf_handle;

   memset(data->mgmt_gw_addr, 0, 64);
   memcpy(data->mgmt_gw_addr, mgmt_gw_addr, strlen(mgmt_gw_addr));

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return 0;
}

int al_conf_get_mgmt_gw_enable(AL_CONTEXT_PARAM_DECL int al_conf_handle)
{
   _al_conf_data_t *data;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   data = (_al_conf_data_t *)al_conf_handle;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return data->mgmt_gw_enable;
}

int al_conf_set_mgmt_gw_enable(AL_CONTEXT_PARAM_DECL int al_conf_handle, int mgmt_gw_enable)
{
   _al_conf_data_t *data;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   data = (_al_conf_data_t *)al_conf_handle;

   data->mgmt_gw_enable = mgmt_gw_enable;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return 0;
}

unsigned char *al_conf_get_mgmt_gw_certificates(AL_CONTEXT_PARAM_DECL int al_conf_handle)
{
	_al_conf_data_t *data;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

	data = (_al_conf_data_t *)al_conf_handle;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
	return data->mgmt_gw_certificates;
}

int al_conf_set_mgmt_gw_certificates(AL_CONTEXT_PARAM_DECL int al_conf_handle, unsigned char *mgmt_gw_certificates)
{
	_al_conf_data_t *data;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

	data = (_al_conf_data_t *)al_conf_handle;

	memset(data->mgmt_gw_certificates, 0, 64);
	memcpy(data->mgmt_gw_certificates, mgmt_gw_certificates, strlen(mgmt_gw_certificates));

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
	return 0;
}

int al_conf_get_disable_backhaul_security(AL_CONTEXT_PARAM_DECL int al_conf_handle)
{
	_al_conf_data_t *data;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

	data = (_al_conf_data_t *)al_conf_handle;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
	return data->disable_backhaul_security;
}

int al_conf_set_disable_backhaul_security(AL_CONTEXT_PARAM_DECL int al_conf_handle, int on_off)
{
	_al_conf_data_t *data;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

	data = (_al_conf_data_t *)al_conf_handle;

	data->disable_backhaul_security = on_off;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
	return 0;
}

int al_conf_get_change_resistance(AL_CONTEXT_PARAM_DECL int al_conf_handle)
{
   _al_conf_data_t *data;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   data = (_al_conf_data_t *)al_conf_handle;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return data->change_resistance;
}


int al_conf_get_config_sqnr(AL_CONTEXT_PARAM_DECL int al_conf_handle)
{
   _al_conf_data_t *data;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   data = (_al_conf_data_t *)al_conf_handle;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return data->config_sqnr;
}


int al_conf_get_use_virt_if(AL_CONTEXT_PARAM_DECL int al_conf_handle)
{
   _al_conf_data_t *data;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   data = (_al_conf_data_t *)al_conf_handle;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return data->use_virt_if;
}


static void _copy_effistream_tree(AL_CONTEXT_PARAM_DECL al_conf_effistream_criteria_t *src_first_child,
                                  al_conf_effistream_criteria_t                       *dst_parent,
                                  al_conf_effistream_criteria_t                       *prev_sibling)
{
   al_conf_effistream_criteria_t *node;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   if (src_first_child == NULL)
   {
	  al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : src_first_child is NULL : %s<%d>\n", __func__, __LINE__);
      return;
   }

   node = (al_conf_effistream_criteria_t *)al_heap_alloc(AL_CONTEXT sizeof(al_conf_effistream_criteria_t)AL_HEAP_DEBUG_PARAM);
   memset(node, 0, sizeof(al_conf_effistream_criteria_t));

   memcpy(&node->action, &src_first_child->action, sizeof(src_first_child->action));
   memcpy(&node->match, &src_first_child->match, sizeof(src_first_child->match));
   node->match_id = src_first_child->match_id;

   if (prev_sibling == NULL)
   {
      dst_parent->first_child = node;
   }
   else
   {
      prev_sibling->next_sibling = node;
   }

   prev_sibling = node;

   _copy_effistream_tree(AL_CONTEXT src_first_child->first_child, node, NULL);

   _copy_effistream_tree(AL_CONTEXT src_first_child->next_sibling, dst_parent, prev_sibling);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
}


al_conf_effistream_criteria_t *al_conf_get_effistream_root(AL_CONTEXT_PARAM_DECL int al_conf_handle)
{
   _al_conf_data_t               *data;
   al_conf_effistream_criteria_t *criteria_root;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   data = (_al_conf_data_t *)al_conf_handle;

   criteria_root = (al_conf_effistream_criteria_t *)al_heap_alloc(AL_CONTEXT sizeof(al_conf_effistream_criteria_t)AL_HEAP_DEBUG_PARAM);
   memset(criteria_root, 0, sizeof(al_conf_effistream_criteria_t));

   if ((data->criteria_root == NULL) ||
       (data->criteria_root->first_child == NULL))
   {
	  al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : criteria_root : %p first_child : %p : %s<%d>\n",data->criteria_root, data->criteria_root->first_child, __func__, __LINE__);
      return criteria_root;
   }

   _copy_effistream_tree(AL_CONTEXT data->criteria_root->first_child, criteria_root, NULL);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);

   return criteria_root;
}


int al_conf_get_gps_info(AL_CONTEXT_PARAM_DECL int al_conf_handle, al_conf_gps_info_t *gps_info)
{
   _al_conf_data_t *data;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   data = (_al_conf_data_t *)al_conf_handle;

   memcpy(gps_info, &data->gps_info, sizeof(al_conf_gps_info_t));
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);

   return 0;
}


int al_conf_get_dhcp_info(AL_CONTEXT_PARAM_DECL int al_conf_handle, al_conf_dhcp_info_t *dhcp_info)
{
   _al_conf_data_t *data;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   data = (_al_conf_data_t *)al_conf_handle;

   memcpy(dhcp_info, &data->dhcp_info, sizeof(al_conf_dhcp_info_t));
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);

   return 0;
}


int al_conf_get_logmon_info(AL_CONTEXT_PARAM_DECL int al_conf_handle, al_conf_logmon_info_t *logmon_info)
{
   _al_conf_data_t *data;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   data = (_al_conf_data_t *)al_conf_handle;

   memcpy(logmon_info, &data->logmon_info, sizeof(al_conf_logmon_info_t));
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);

   return 0;
}


void al_conf_free_effistream_tree(AL_CONTEXT_PARAM_DECL al_conf_effistream_criteria_t *tree_root)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);
   if (tree_root != NULL)
   {
      _free_effistream_info(AL_CONTEXT tree_root);
   }
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
}


al_conf_option_t *al_conf_get_options(AL_CONTEXT_PARAM_DECL int al_conf_handle)
{
   _al_conf_data_t  *data;
   al_conf_option_t *ret_options;
   al_conf_option_t *options;
   al_conf_option_t *temp_option;

   data        = (_al_conf_data_t *)al_conf_handle;
   ret_options = NULL;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   for (options = data->option_info; options != NULL; options = options->next)
   {
      temp_option = (al_conf_option_t *)al_heap_alloc(AL_CONTEXT sizeof(al_conf_option_t)AL_HEAP_DEBUG_PARAM);

      memcpy(temp_option->key, options->key, sizeof(options->key));

      temp_option->next = ret_options;
      ret_options       = temp_option;
   }

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return ret_options;
}


#ifndef _MESHAP_AL_CONF_NO_SET_AND_PUT

/** ---- BEGIN SET AND PUT FUNCTIONS ---- */

int al_conf_set_use_virt_if(AL_CONTEXT_PARAM_DECL int al_conf_handle, int use_virt_if)
{
   _al_conf_data_t *data;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   data = (_al_conf_data_t *)al_conf_handle;

   data->use_virt_if = use_virt_if;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);

   return 0;
}


int al_conf_set_name(AL_CONTEXT_PARAM_DECL int al_conf_handle, char *name_buffer, int name_length)
{
   _al_conf_data_t *data;
   int             length;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   data = (_al_conf_data_t *)al_conf_handle;

   length = _AL_CONF_MAX_TOKEN_SIZE + 1;

   if (name_length > length)
   {
	  al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : name_length(%d) : length(%d) : %s<%d>\n",name_length, length,  __func__, __LINE__);
      return length;
   }

   strcpy(data->name, name_buffer);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);

   return 0;
}


int al_conf_set_description(AL_CONTEXT_PARAM_DECL int al_conf_handle, char *desc_buffer, int desc_length)
{
   _al_conf_data_t *data;
   int             length;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   data = (_al_conf_data_t *)al_conf_handle;

   length = _AL_CONF_MAX_TOKEN_SIZE + 1;

   if (desc_length > length)
   {
	  al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : desc_length(%d) : length(%d) : %s<%d>\n", desc_length, length,  __func__, __LINE__);
      return length;
   }

   strcpy(data->description, desc_buffer);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);

   return 0;
}


int al_conf_set_mesh_id(AL_CONTEXT_PARAM_DECL int al_conf_handle, char *mesh_id_buffer, int id_length)
{
   _al_conf_data_t *data;
   int             length;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   data = (_al_conf_data_t *)al_conf_handle;

   length = _AL_CONF_MAX_TOKEN_SIZE + 1;

   if (id_length > length)
   {
	  al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : id_length(%d) : length(%d) : %s<%d>\n", id_length, length,  __func__, __LINE__);
      return length;
   }

   strcpy(data->mesh_id, mesh_id_buffer);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);

   return 0;
}


int al_conf_set_mesh_imcp_key_buffer(AL_CONTEXT_PARAM_DECL unsigned int al_conf_handle, char *mesh_imcp_key_buffer, int key_length)
{
   _al_conf_data_t *data;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   data = (_al_conf_data_t *)al_conf_handle;

   if (data->mesh_imcp_key_buffer != NULL)
   {
      al_heap_free(AL_CONTEXT data->mesh_imcp_key_buffer);
   }

   data->mesh_imcp_key_buffer = (char *)al_heap_alloc(AL_CONTEXT key_length + 1 AL_HEAP_DEBUG_PARAM);
   memset(data->mesh_imcp_key_buffer, 0, key_length + 1);
   strcpy(data->mesh_imcp_key_buffer, mesh_imcp_key_buffer);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);

   return 0;
}


int al_conf_set_essid(AL_CONTEXT_PARAM_DECL int al_conf_handle, char *essid_buffer, int essid_length)
{
   _al_conf_data_t *data;
   int             length;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   data = (_al_conf_data_t *)al_conf_handle;

   length = AL_802_11_ESSID_MAX_SIZE + 1;

   if (essid_length > length)
   {
	  al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : essid_length(%d) : length(%d) : %s<%d>\n", essid_length, length,  __func__, __LINE__);
      return length;
   }

   strcpy(data->essid, essid_buffer);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);

   return 0;
}


int al_conf_set_rts_th(AL_CONTEXT_PARAM_DECL int al_conf_handle, int rts_th)
{
   _al_conf_data_t *data;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   data = (_al_conf_data_t *)al_conf_handle;

   data->rts_th = rts_th;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return 0;
}


int al_conf_set_frag_th(AL_CONTEXT_PARAM_DECL int al_conf_handle, int frag_th)
{
   _al_conf_data_t *data;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   data = (_al_conf_data_t *)al_conf_handle;

   data->frag_th = frag_th;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return 0;
}


int al_conf_set_beacon_interval(AL_CONTEXT_PARAM_DECL int al_conf_handle, int beacon_interval)
{
   _al_conf_data_t *data;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   data = (_al_conf_data_t *)al_conf_handle;

   data->beacon_interval = beacon_interval;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return 0;
}

int al_conf_set_dynamic_channel_allocation(AL_CONTEXT_PARAM_DECL int al_conf_handle, int dynamic_channel_allocation)
{
   _al_conf_data_t *data;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   data = (_al_conf_data_t *)al_conf_handle;

   data->dynamic_channel_allocation = dynamic_channel_allocation;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return 0;
}


int al_conf_set_stay_awake_count(AL_CONTEXT_PARAM_DECL int al_conf_handle, int stay_awake_count)
{
   _al_conf_data_t *data;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   data = (_al_conf_data_t *)al_conf_handle;

   data->stay_awake_count = stay_awake_count;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return 0;
}
int al_conf_set_sig_threshold(AL_CONTEXT_PARAM_DECL int al_conf_handle, int sig_ts)
{
   _al_conf_data_t *data;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   data = (_al_conf_data_t *)al_conf_handle;

   data->sig_threshold = sig_ts;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return 0;
}

int al_conf_set_bridge_ageing_time(AL_CONTEXT_PARAM_DECL int al_conf_handle, int bridge_ageing_time)
{
   _al_conf_data_t *data;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   data = (_al_conf_data_t *)al_conf_handle;

   data->bridge_ageing_time = bridge_ageing_time;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return 0;
}


int al_conf_set_wds_encrypted(AL_CONTEXT_PARAM_DECL int al_conf_handle, int wds_encrypted)
{
   _al_conf_data_t *data;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   data = (_al_conf_data_t *)al_conf_handle;

   data->wds_encrypted = wds_encrypted;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return 0;
}


int al_conf_set_model(AL_CONTEXT_PARAM_DECL int al_conf_handle, char *model_name, int model_name_length)
{
   _al_conf_data_t *data;
   int             length;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   data = (_al_conf_data_t *)al_conf_handle;

   length = 32 + 1;

   if (model_name_length > length)
   {
	  al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : model_name_length : %d length : %d : %s<%d>\n", model_name_length, length, __func__, __LINE__);
      return length;
   }

   strcpy(data->model, model_name);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return 0;
}


int al_conf_set_gps_x_coordinate(AL_CONTEXT_PARAM_DECL int al_conf_handle, char *gps_x_coordinate, int coordinate_length)
{
   _al_conf_data_t *data;
   int             length;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   data = (_al_conf_data_t *)al_conf_handle;

   length = 32 + 1;

   if (coordinate_length > length)
   {
	  al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : coordinate_length : %d length : %d : %s<%d>\n", coordinate_length, length, __func__, __LINE__);
      return length;
   }

   strcpy(data->gps_x_coordinate, gps_x_coordinate);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return 0;
}


int al_conf_set_gps_y_coordinate(AL_CONTEXT_PARAM_DECL int al_conf_handle, char *gps_y_coordinate, int coordinate_length)
{
   _al_conf_data_t *data;
   int             length;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   data = (_al_conf_data_t *)al_conf_handle;

   length = 32 + 1;

   if (coordinate_length > length)
   {
	  al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : coordinate_length : %d length : %d : %s<%d>\n", coordinate_length, length, __func__, __LINE__);
      return length;
   }

   strcpy(data->gps_y_coordinate, gps_y_coordinate);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return 0;
}


int al_conf_set_regulatory_domain(AL_CONTEXT_PARAM_DECL int al_conf_handle, int regulatory_domain)
{
   _al_conf_data_t *data;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   data = (_al_conf_data_t *)al_conf_handle;

   data->regulatory_domain = regulatory_domain;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return 0;
}


int al_conf_set_country_code(AL_CONTEXT_PARAM_DECL int al_conf_handle, int country_code)
{
   _al_conf_data_t *data;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   data = (_al_conf_data_t *)al_conf_handle;

   data->country_code = country_code;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return 0;
}


int al_conf_set_fcc_certified(AL_CONTEXT_PARAM_DECL int al_conf_handle, int fcc_certified_operation)
{
   _al_conf_data_t *data;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   data = (_al_conf_data_t *)al_conf_handle;

   data->fcc_certified_operation = fcc_certified_operation;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return 0;
}


int al_conf_set_etsi_certified(AL_CONTEXT_PARAM_DECL int al_conf_handle, int etsi_certified_operation)
{
   _al_conf_data_t *data;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   data = (_al_conf_data_t *)al_conf_handle;

   data->etsi_certified_operation = etsi_certified_operation;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return 0;
}


int al_conf_set_ds_tx_rate(AL_CONTEXT_PARAM_DECL int al_conf_handle, int ds_tx_rate)
{
   _al_conf_data_t *data;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   data = (_al_conf_data_t *)al_conf_handle;

   data->ds_tx_rate = ds_tx_rate;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return 0;
}


int al_conf_set_ds_tx_power(AL_CONTEXT_PARAM_DECL int al_conf_handle, int ds_tx_power)
{
   _al_conf_data_t *data;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   data = (_al_conf_data_t *)al_conf_handle;

   data->ds_tx_power = ds_tx_power;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return 0;
}


int al_conf_set_if_count(AL_CONTEXT_PARAM_DECL int al_conf_handle, int if_count)
{
   _al_conf_data_t *data;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   data = (_al_conf_data_t *)al_conf_handle;

   if (data->if_count > 0)
   {
      al_heap_free(AL_CONTEXT data->if_info);
      data->if_info = NULL;
   }

   data->if_count = if_count;

   if (data->if_count > 0)
   {
      data->if_info = (al_conf_if_info_t *)al_heap_alloc(AL_CONTEXT sizeof(al_conf_if_info_t) * data->if_count AL_HEAP_DEBUG_PARAM);
      memset(data->if_info, 0, sizeof(al_conf_if_info_t) * data->if_count);
   }

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return 0;
}


int al_conf_set_if(AL_CONTEXT_PARAM_DECL unsigned int al_conf_handle, int if_index, al_conf_if_info_t *if_info)
{
   _al_conf_data_t *data;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   data = (_al_conf_data_t *)al_conf_handle;

   if (if_index >= data->if_count)
   {
	  al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : if_index : %d if_count : %d : %s<%d>\n",if_index, data->if_count, __func__, __LINE__);
      return -1;
   }

   memcpy(&data->if_info[if_index], if_info, sizeof(al_conf_if_info_t));

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return 0;
}


int al_conf_set_vlan_count(AL_CONTEXT_PARAM_DECL int al_conf_handle, int vlan_count)
{
   _al_conf_data_t *data;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   data = (_al_conf_data_t *)al_conf_handle;

   if (data->vlan_count > 0)
   {
      al_heap_free(AL_CONTEXT data->vlan_info);
      data->vlan_info = NULL;
   }

   data->vlan_count = vlan_count;

   if (data->vlan_count > 0)
   {
      data->vlan_info = (al_conf_vlan_info_t *)al_heap_alloc(AL_CONTEXT sizeof(al_conf_vlan_info_t) * data->vlan_count AL_HEAP_DEBUG_PARAM);
      memset(data->vlan_info, 0, sizeof(al_conf_vlan_info_t) * data->vlan_count);
   }

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return 0;
}


int al_conf_set_vlan(AL_CONTEXT_PARAM_DECL int al_conf_handle, int vlan_index, al_conf_vlan_info_t *vlan_info)
{
   _al_conf_data_t *data;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   data = (_al_conf_data_t *)al_conf_handle;

   if (vlan_index >= data->vlan_count)
   {
	  al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : vlan_index : %d vlan_count : %d : %s<%d>\n",vlan_index, data->vlan_count, __func__, __LINE__);
      return -1;
   }

   memcpy(&data->vlan_info[vlan_index], vlan_info, sizeof(al_conf_vlan_info_t));
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return 0;
}


int al_conf_set_preferred_parent(AL_CONTEXT_PARAM_DECL int al_conf_handle, al_net_addr_t *preferred_parent)
{
   _al_conf_data_t *data;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   data = (_al_conf_data_t *)al_conf_handle;

   memcpy(data->preferred_parent, preferred_parent->bytes, 6);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return 0;
}


int al_conf_set_signal_map(AL_CONTEXT_PARAM_DECL int al_conf_handle, unsigned char *signal_map)
{
   _al_conf_data_t *data;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   data = (_al_conf_data_t *)al_conf_handle;

   memcpy(data->signal_map, signal_map, 8);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return 0;
}


int al_conf_set_heartbeat_interval(AL_CONTEXT_PARAM_DECL int al_conf_handle, int heartbeat_interval)
{
   _al_conf_data_t *data;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   data = (_al_conf_data_t *)al_conf_handle;

   data->heartbeat_interval = heartbeat_interval;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return 0;
}


int al_conf_set_heartbeat_miss_count(AL_CONTEXT_PARAM_DECL int al_conf_handle, int heartbeat_miss_count)
{
   _al_conf_data_t *data;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   data = (_al_conf_data_t *)al_conf_handle;

   data->heartbeat_miss_count = heartbeat_miss_count;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return 0;
}


int al_conf_set_hop_cost(AL_CONTEXT_PARAM_DECL int al_conf_handle, int hop_cost)
{
   _al_conf_data_t *data;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   data = (_al_conf_data_t *)al_conf_handle;

   data->hop_cost = hop_cost;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return 0;
}


int al_conf_set_max_allowable_hops(AL_CONTEXT_PARAM_DECL int al_conf_handle, int max_allowable_hops)
{
   _al_conf_data_t *data;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   data = (_al_conf_data_t *)al_conf_handle;

   data->max_allowable_hops = max_allowable_hops;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return 0;
}


int al_conf_set_las_interval(AL_CONTEXT_PARAM_DECL int al_conf_handle, int las_interval)
{
   _al_conf_data_t *data;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   data = (_al_conf_data_t *)al_conf_handle;

   data->las_interval = las_interval;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return 0;
}


int al_conf_set_change_resistance(AL_CONTEXT_PARAM_DECL int al_conf_handle, int change_resistance)
{
   _al_conf_data_t *data;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   data = (_al_conf_data_t *)al_conf_handle;

   data->change_resistance = change_resistance;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return 0;
}


int al_conf_set_config_sqnr(AL_CONTEXT_PARAM_DECL int al_conf_handle, unsigned int config_sqnr)
{
   _al_conf_data_t *data;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   data = (_al_conf_data_t *)al_conf_handle;

   data->config_sqnr = config_sqnr;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return 0;
}


void al_conf_set_effistream_root(AL_CONTEXT_PARAM_DECL int al_conf_handle, al_conf_effistream_criteria_t *criteria_root)
{
   _al_conf_data_t *data;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   data = (_al_conf_data_t *)al_conf_handle;

   if (data->criteria_root != NULL)
   {
      _free_effistream_info(AL_CONTEXT data->criteria_root);
   }

   data->criteria_root = criteria_root;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
}


int al_conf_set_gps_info(AL_CONTEXT_PARAM_DECL int al_conf_handle, al_conf_gps_info_t *gps_info)
{
   _al_conf_data_t *data;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   data = (_al_conf_data_t *)al_conf_handle;

   memcpy(&data->gps_info, gps_info, sizeof(al_conf_gps_info_t));

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return 0;
}


int al_conf_set_dhcp_info(AL_CONTEXT_PARAM_DECL int al_conf_handle, al_conf_dhcp_info_t *dhcp_info)
{
   _al_conf_data_t *data;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   data = (_al_conf_data_t *)al_conf_handle;

   memcpy(&data->dhcp_info, dhcp_info, sizeof(al_conf_dhcp_info_t));

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return 0;
}


int al_conf_set_logmon_info(AL_CONTEXT_PARAM_DECL int al_conf_handle, al_conf_logmon_info_t *logmon_info)
{
   _al_conf_data_t *data;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   data = (_al_conf_data_t *)al_conf_handle;

   memcpy(&data->logmon_info, logmon_info, sizeof(al_conf_logmon_info_t));

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return 0;
}


int al_conf_set_options(AL_CONTEXT_PARAM_DECL int al_conf_handle, al_conf_option_t *options)
{
   _al_conf_data_t  *data;
   al_conf_option_t *option;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   data = (_al_conf_data_t *)al_conf_handle;

   /**
    * Free current options
    */

   while (data->option_info != NULL)
   {
      option = data->option_info->next;
      al_heap_free(AL_CONTEXT data->option_info);
      data->option_info = option;
   }

   data->option_info = options;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return 0;
}


static int _write_criteria_block(char *conf_string, int level, al_conf_effistream_criteria_t *criteria)
{
   char *p;
   int  i;
   char tab_string[32];
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   for (i = 0; i < level; i++)
   {
      tab_string[i] = '\t';
   }
   tab_string[level] = 0;

   p = conf_string;

   switch (criteria->match_id)
   {
   case AL_CONF_EFFISTREAM_MATCH_ID_ETH_TYPE:
      p += sprintf(p, "%seth_type=%d ", tab_string, criteria->match.value);
      break;

   case AL_CONF_EFFISTREAM_MATCH_ID_ETH_DST:
      p += sprintf(p, "%seth_dst="AL_NET_ADDR_STR " ", tab_string, AL_NET_ADDR_TO_STR(&criteria->match.mac_address));
      break;

   case AL_CONF_EFFISTREAM_MATCH_ID_ETH_SRC:
      p += sprintf(p, "%seth_src="AL_NET_ADDR_STR " ", tab_string, AL_NET_ADDR_TO_STR(&criteria->match.mac_address));
      break;

   case AL_CONF_EFFISTREAM_MATCH_ID_IP_TOS:
      p += sprintf(p, "%sip_tos=%d ", tab_string, criteria->match.value);
      break;

   case AL_CONF_EFFISTREAM_MATCH_ID_IP_DIFFSRV:
      p += sprintf(p, "%sip_diffsrv=%d ", tab_string, criteria->match.value);
      break;

   case AL_CONF_EFFISTREAM_MATCH_ID_IP_SRC:
      p += sprintf(p,
                   "%sip_src=%d.%d.%d.%d ",
                   tab_string,
                   criteria->match.ip_address[0],
                   criteria->match.ip_address[1],
                   criteria->match.ip_address[2],
                   criteria->match.ip_address[3]);
      break;

   case AL_CONF_EFFISTREAM_MATCH_ID_IP_DST:
      p += sprintf(p,
                   "%sip_dst=%d.%d.%d.%d ",
                   tab_string,
                   criteria->match.ip_address[0],
                   criteria->match.ip_address[1],
                   criteria->match.ip_address[2],
                   criteria->match.ip_address[3]);
      break;

   case AL_CONF_EFFISTREAM_MATCH_ID_IP_PROTO:
      p += sprintf(p, "%sip_proto=%d ", tab_string, criteria->match.value);
      break;

   case AL_CONF_EFFISTREAM_MATCH_ID_UDP_SRC_PORT:
      p += sprintf(p, "%sudp_src_port=%d:%d ", tab_string, criteria->match.range.min_val, criteria->match.range.max_val);
      break;

   case AL_CONF_EFFISTREAM_MATCH_ID_UDP_DST_PORT:
      p += sprintf(p, "%sudp_dst_port=%d:%d ", tab_string, criteria->match.range.min_val, criteria->match.range.max_val);
      break;

   case AL_CONF_EFFISTREAM_MATCH_ID_UDP_LENGTH:
      p += sprintf(p, "%sudp_length=%d:%d ", tab_string, criteria->match.range.min_val, criteria->match.range.max_val);
      break;

   case AL_CONF_EFFISTREAM_MATCH_ID_TCP_SRC_PORT:
      p += sprintf(p, "%stcp_src_port=%d:%d ", tab_string, criteria->match.range.min_val, criteria->match.range.max_val);
      break;

   case AL_CONF_EFFISTREAM_MATCH_ID_TCP_DST_PORT:
      p += sprintf(p, "%stcp_dst_port=%d:%d ", tab_string, criteria->match.range.min_val, criteria->match.range.max_val);
      break;

   case AL_CONF_EFFISTREAM_MATCH_ID_TCP_LENGTH:
      p += sprintf(p, "%stcp_length=%d:%d ", tab_string, criteria->match.range.min_val, criteria->match.range.max_val);
      break;

   case AL_CONF_EFFISTREAM_MATCH_ID_RTP_VERSION:
      p += sprintf(p, "%srtp_version=%d ", tab_string, criteria->match.value);
      break;

   case AL_CONF_EFFISTREAM_MATCH_ID_RTP_PAYLOAD:
      p += sprintf(p, "%srtp_payload_type=%d ", tab_string, criteria->match.value);
      break;

   case AL_CONF_EFFISTREAM_MATCH_ID_RTP_LENGTH:
      p += sprintf(p, "%srtp_length=%d:%d ", tab_string, criteria->match.range.min_val, criteria->match.range.max_val);
      break;
   }

   if (criteria->first_child != NULL)
   {
      p += sprintf(p, "{\n");
      p += _write_criteria_block(p, level + 1, criteria->first_child);
      p += sprintf(p, "%s}\n", tab_string);
   }
   else
   {
      p += sprintf(p, "[\n");
      p += sprintf(p, "%s\tno_ack=%d,\n", tab_string, criteria->action.no_ack);
      p += sprintf(p, "%s\tdot11e_category=%d,\n", tab_string, criteria->action.dot11e_category);
      p += sprintf(p, "%s\tbit_rate=%d,\n", tab_string, criteria->action.bit_rate);
      p += sprintf(p, "%s\tdrop=%d,\n", tab_string, criteria->action.drop);
      p += sprintf(p, "%s\tqueued_retry=%d\n", tab_string, criteria->action.queued_retry);
      p += sprintf(p, "%s]\n", tab_string);
   }

   if (criteria->next_sibling != NULL)
   {
      p += _write_criteria_block(p, level, criteria->next_sibling);
   }

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return(p - conf_string);
}


static int _write_effistream_info(char *conf_string, _al_conf_data_t *conf_data)
{
   char *p;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   p = conf_string;

   p += sprintf(p, "effistream {\n");

   if ((conf_data->criteria_root != NULL) && (conf_data->criteria_root->first_child != NULL))
   {
      p += _write_criteria_block(p, 1, conf_data->criteria_root->first_child);
   }

   p += sprintf(p, "}\n");
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);

   return(p - conf_string);
}


static int _write_security_info(char **conf_string, al_security_info_t *security_info, int tab_count)
{
   char buffer[1024];
   char *ret_buffer;
   int  ret, i, j;
   char *p;
   char tab_char[10];
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   memset(tab_char, '\0', sizeof(tab_char));

   for (i = 0; i < tab_count; i++)
   {
      tab_char[i] = '\t';
   }

   ret_buffer = *conf_string;

   p = ret_buffer;

   memset(buffer, 0, 1024);
   ret = sprintf(buffer, "%ssecurity {", tab_char);
   memcpy(p, buffer, strlen(buffer));
   p += ret;
   *p = '\n';
   p++;


   memset(buffer, 0, 1024);
   ret = sprintf(buffer, "%s\t\t none {", tab_char);
   memcpy(p, buffer, strlen(buffer));
   p += ret;
   *p = '\n';
   p++;

   memset(buffer, 0, 1024);
   ret = sprintf(buffer, "%s\t\t\t enabled=%d", tab_char, security_info->_security_none.enabled);
   memcpy(p, buffer, strlen(buffer));
   p += ret;
   *p = '\n';
   p++;

   memset(buffer, 0, 1024);
   ret = sprintf(buffer, "%s\t\t }\n", tab_char);
   memcpy(p, buffer, strlen(buffer));
   p += ret;

   memset(buffer, 0, 1024);
   ret = sprintf(buffer, "%s\t\t wep {\n", tab_char);
   memcpy(p, buffer, strlen(buffer));
   p += ret;
   *p = '\n';
   p++;

   memset(buffer, 0, 1024);
   ret = sprintf(buffer, "%s\t\t\t enabled=%d,", tab_char, security_info->_security_wep.enabled);
   memcpy(p, buffer, strlen(buffer));
   p += ret;
   *p = '\n';
   p++;

   if (security_info->_security_wep.enabled == 0)
   {
      /* set default values in structure */
      memset(&security_info->_security_wep, 0, sizeof(security_info->_security_wep));
      security_info->_security_wep.key_count   = 1;
      security_info->_security_wep.keys[0].len = 5;
   }

   memset(buffer, 0, 1024);
   ret = sprintf(buffer, "%s\t\t\t strength=%d,", tab_char, security_info->_security_wep.strength);
   memcpy(p, buffer, strlen(buffer));
   p += ret;
   *p = '\n';
   p++;

   memset(buffer, 0, 1024);
   ret = sprintf(buffer, "%s\t\t\t key_index=%d,", tab_char, security_info->_security_wep.key_index);
   memcpy(p, buffer, strlen(buffer));
   p += ret;
   *p = '\n';
   p++;


   memset(buffer, 0, 1024);
   ret = sprintf(buffer, "%s\t\t\t keys= {", tab_char);
   memcpy(p, buffer, strlen(buffer));
   p += ret;
   *p = '\n';
   p++;

   if (security_info->_security_wep.strength == 40)
   {
      security_info->_security_wep.key_count = 4;
      for (i = 0; i < security_info->_security_wep.key_count; i++)
      {
         security_info->_security_wep.keys[i].len = 5;
      }
   }
   else if (security_info->_security_wep.strength == 104)
   {
      security_info->_security_wep.key_count   = 1;
      security_info->_security_wep.keys[0].len = 13;
   }

   for (i = 0; i < security_info->_security_wep.key_count; i++)
   {
      memset(buffer, 0, 1024);
      ret = sprintf(buffer, "%s\t\t\t\t", tab_char);
      memcpy(p, buffer, strlen(buffer));
      p += ret;


      for (j = 0; j <= security_info->_security_wep.keys[i].len - 1; j++)
      {
         sprintf(buffer, "%02X", security_info->_security_wep.keys[i].bytes[j]);
         ret = strlen(buffer);
         memcpy(p, buffer, ret);
         p += ret;
         if (j < security_info->_security_wep.keys[i].len - 1)
         {
            *p = ':';
            p++;
         }
      }
      if (i < security_info->_security_wep.key_count - 1)
      {
         *p = ',';
         p++;
      }
      *p = '\n';
      p++;
   }

   memset(buffer, 0, 1024);
   ret = sprintf(buffer, "%s\t\t\t }\n", tab_char);    /* keys end brace */
   memcpy(p, buffer, strlen(buffer));
   p += ret;

   memset(buffer, 0, 1024);
   ret = sprintf(buffer, "%s\t\t }\n", tab_char);    /* wep end brace */
   memcpy(p, buffer, strlen(buffer));
   p += ret;


   memset(buffer, 0, 1024);
   ret = sprintf(buffer, "%s\t\t rsn_psk {", tab_char);
   memcpy(p, buffer, strlen(buffer));
   p += ret;
   *p = '\n';
   p++;

   memset(buffer, 0, 1024);
   ret = sprintf(buffer, "%s\t\t\t enabled=%d,", tab_char, security_info->_security_rsn_psk.enabled);
   memcpy(p, buffer, strlen(buffer));
   p += ret;
   *p = '\n';
   p++;

   if (security_info->_security_rsn_psk.enabled == 0)
   {
      /* set default values in structure */

      memset(&security_info->_security_rsn_psk, 0, sizeof(security_info->_security_rsn_psk));
      sprintf(security_info->_security_rsn_psk.key_buffer, "begin 777 key\nend\n");
   }

   memset(buffer, 0, 1024);
   ret = sprintf(buffer, "%s\t\t\t mode=%d,", tab_char, security_info->_security_rsn_psk.mode);
   memcpy(p, buffer, strlen(buffer));
   p += ret;
   *p = '\n';
   p++;

   memset(buffer, 0, 1024);
   ret = sprintf(buffer, "%s\t\t\t key = {", tab_char);
   memcpy(p, buffer, strlen(buffer));
   p += ret;
   *p = '\n';
   p++;

   memset(buffer, 0, 1024);
   ret = sprintf(buffer, "%s\t\t\t\t%s\n", tab_char, security_info->_security_rsn_psk.key_buffer);
   memcpy(p, buffer, strlen(buffer));
   p += ret;
   *p = '\n';
   p++;

   memset(buffer, 0, 1024);
   ret = sprintf(buffer, "%s\t\t\t },", tab_char);
   memcpy(p, buffer, strlen(buffer));
   p += ret;
   *p = '\n';
   p++;

   memset(buffer, 0, 1024);
   ret = sprintf(buffer, "%s\t\t\t cipher_tkip=%d,", tab_char, security_info->_security_rsn_psk.cipher_tkip);
   memcpy(p, buffer, strlen(buffer));
   p += ret;
   *p = '\n';
   p++;

   memset(buffer, 0, 1024);
   ret = sprintf(buffer, "%s\t\t\t cipher_ccmp=%d,", tab_char, security_info->_security_rsn_psk.cipher_ccmp);
   memcpy(p, buffer, strlen(buffer));
   p += ret;
   *p = '\n';
   p++;

   memset(buffer, 0, 1024);
   ret = sprintf(buffer, "%s\t\t\t group_key_renewal=%d", tab_char, security_info->_security_rsn_psk.group_key_renewal);
   memcpy(p, buffer, strlen(buffer));
   p += ret;
   *p = '\n';
   p++;

   memset(buffer, 0, 1024);
   ret = sprintf(buffer, "%s\t\t }", tab_char);
   memcpy(p, buffer, strlen(buffer));
   p += ret;
   *p = '\n';
   p++;

   memset(buffer, 0, 1024);
   ret = sprintf(buffer, "%s\t\t rsn_radius {", tab_char);
   memcpy(p, buffer, strlen(buffer));
   p += ret;
   *p = '\n';
   p++;

   memset(buffer, 0, 1024);
   ret = sprintf(buffer, "%s\t\t\t enabled=%d,", tab_char, security_info->_security_rsn_radius.enabled);
   memcpy(p, buffer, strlen(buffer));
   p += ret;
   *p = '\n';
   p++;

   if (security_info->_security_rsn_radius.enabled == 0)
   {
      /* set default values in structure */
      memset(&security_info->_security_rsn_radius, 0, sizeof(security_info->_security_rsn_radius));
      sprintf(security_info->_security_rsn_radius.radius_secret_key, "begin 777 radius_secret\nend\n");
   }

   memset(buffer, 0, 1024);
   ret = sprintf(buffer, "%s\t\t\t mode=%d,", tab_char, security_info->_security_rsn_radius.mode);
   memcpy(p, buffer, strlen(buffer));
   p += ret;
   *p = '\n';
   p++;

   memset(buffer, 0, 1024);
   ret = sprintf(buffer, "%s\t\t\t radius_server=%d.%d.%d.%d,", tab_char, security_info->_security_rsn_radius.radius_server[0],
                 security_info->_security_rsn_radius.radius_server[1],
                 security_info->_security_rsn_radius.radius_server[2],
                 security_info->_security_rsn_radius.radius_server[3]);
   memcpy(p, buffer, strlen(buffer));
   p += ret;
   *p = '\n';
   p++;

   memset(buffer, 0, 1024);
   ret = sprintf(buffer, "%s\t\t\t radius_port=%d,", tab_char, security_info->_security_rsn_radius.radius_port);
   memcpy(p, buffer, strlen(buffer));
   p += ret;
   *p = '\n';
   p++;

   memset(buffer, 0, 1024);
   ret = sprintf(buffer, "%s\t\t\t radius_secret {", tab_char);
   memcpy(p, buffer, strlen(buffer));
   p += ret;
   *p = '\n';
   p++;

   memset(buffer, 0, 1024);
   ret = sprintf(buffer, "%s\t\t\t\t %s\n", tab_char, security_info->_security_rsn_radius.radius_secret_key);
   memcpy(p, buffer, strlen(buffer));
   p += ret;
   *p = '\n';
   p++;

   memset(buffer, 0, 1024);
   ret = sprintf(buffer, "%s\t\t\t },", tab_char);
   memcpy(p, buffer, strlen(buffer));
   p += ret;
   *p = '\n';
   p++;

   memset(buffer, 0, 1024);
   ret = sprintf(buffer, "%s\t\t\t cipher_tkip=%d,", tab_char, security_info->_security_rsn_radius.cipher_tkip);
   memcpy(p, buffer, strlen(buffer));
   p += ret;
   *p = '\n';
   p++;

   memset(buffer, 0, 1024);
   ret = sprintf(buffer, "%s\t\t\t cipher_ccmp=%d,", tab_char, security_info->_security_rsn_radius.cipher_ccmp);
   memcpy(p, buffer, strlen(buffer));
   p += ret;
   *p = '\n';
   p++;

   memset(buffer, 0, 1024);
   ret = sprintf(buffer, "%s\t\t\t group_key_renewal=%d", tab_char, security_info->_security_rsn_radius.group_key_renewal);
   memcpy(p, buffer, strlen(buffer));
   p += ret;
   *p = '\n';
   p++;

   memset(buffer, 0, 1024);
   ret = sprintf(buffer, "%s\t\t }", tab_char);
   memcpy(p, buffer, strlen(buffer));
   p += ret;
   *p = '\n';
   p++;

   memset(buffer, 0, 1024);
   ret = sprintf(buffer, "%s\t}", tab_char);
   memcpy(p, buffer, strlen(buffer));
   p += ret;

   *conf_string = p;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return 0;
}


static int _write_vlan_info(char **conf_string, int al_conf_handle)
{
   char buffer[1024];
   char *ret_buffer;
   int  ret, i;
   char *p;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   _al_conf_data_t *al_conf_data;

   ret_buffer = *conf_string;

   p = ret_buffer;

   al_conf_data = (_al_conf_data_t *)al_conf_handle;

   if (al_conf_data == NULL)
   {
	  al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : al_conf_data ERROR : %s<%d>\n", __func__, __LINE__);
      return -1;
   }
   memset(buffer, 0, 1024);
   ret = sprintf(buffer, "vlan_info(%d) {\n", al_conf_data->vlan_count);
   memcpy(p, buffer, strlen(buffer));
   p += ret;
   *p = '\n';
   p++;

   for (i = 0; i < al_conf_data->vlan_count; i++)
   {
      if (!strcmp(al_conf_data->vlan_info[i].name, "default"))
      {
         memset(buffer, 0, 1024);
         ret = sprintf(buffer, "\t\t default {\n");
         memcpy(p, buffer, strlen(buffer));
         p += ret;
         *p = '\n';
         p++;


         if (al_conf_data->vlan_info[i].tag == AL_CONF_VLAN_INFO_UNTAGGED)
         {
            memset(buffer, 0, 1024);
            ret = sprintf(buffer, "\t\t\t\t tag=untagged,\n");
            memcpy(p, buffer, strlen(buffer));
            p += ret;
            *p = '\n';
            p++;
         }
         else
         {
            memset(buffer, 0, 1024);
            ret = sprintf(buffer, "\t\t\t\t tag=%d,\n", al_conf_data->vlan_info[i].tag);
            memcpy(p, buffer, strlen(buffer));
            p += ret;
            *p = '\n';
            p++;
         }
         memset(buffer, 0, 1024);
         ret = sprintf(buffer, "\t\t\t\t dot1p_priority=%d,\n", al_conf_data->vlan_info[i].dot1p_priority);
         memcpy(p, buffer, strlen(buffer));
         p += ret;
         *p = '\n';
         p++;

         memset(buffer, 0, 1024);
         ret = sprintf(buffer, "\t\t\t\t dot11e_enabled=%d,\n", al_conf_data->vlan_info[i].dot11e_enabled);
         memcpy(p, buffer, strlen(buffer));
         p += ret;
         *p = '\n';
         p++;


         memset(buffer, 0, 1024);
         ret = sprintf(buffer, "\t\t\t\t dot11e_priority=%d\n", al_conf_data->vlan_info[i].dot11e_category);
         memcpy(p, buffer, strlen(buffer));
         p += ret;
         *p = '\n';
         p++;

         memset(buffer, 0, 1024);
         ret = sprintf(buffer, "\t\t }\n");
         memcpy(p, buffer, strlen(buffer));
         p += ret;
         *p = '\n';
         p++;

         continue;
      }
      memset(buffer, 0, 1024);
      ret = sprintf(buffer, "\t\t%s {\n", al_conf_data->vlan_info[i].name);
      memcpy(p, buffer, strlen(buffer));
      p += ret;
      *p = '\n';
      p++;

      memset(buffer, 0, 1024);
      ret = sprintf(buffer, "\t\t\t\tip=%d.%d.%d.%d,",
                    al_conf_data->vlan_info[i].ip_address[0],
                    al_conf_data->vlan_info[i].ip_address[1],
                    al_conf_data->vlan_info[i].ip_address[2],
                    al_conf_data->vlan_info[i].ip_address[3]);
      memcpy(p, buffer, strlen(buffer));
      p += ret;
      *p = '\n';
      p++;


      if (al_conf_data->vlan_info[i].tag == AL_CONF_VLAN_INFO_UNTAGGED)
      {
         memset(buffer, 0, 1024);
         ret = sprintf(buffer, "\t\t\t\t tag=untagged,\n");
         memcpy(p, buffer, strlen(buffer));
         p += ret;
         *p = '\n';
         p++;
      }
      else
      {
         memset(buffer, 0, 1024);
         ret = sprintf(buffer, "\t\t\t\t tag=%d,\n", al_conf_data->vlan_info[i].tag);
         memcpy(p, buffer, strlen(buffer));
         p += ret;
         *p = '\n';
         p++;
      }
      memset(buffer, 0, 1024);
      ret = sprintf(buffer, "\t\t\t\tdot11e_enabled=%d,", al_conf_data->vlan_info[i].dot11e_enabled);
      memcpy(p, buffer, strlen(buffer));
      p += ret;
      *p = '\n';
      p++;

      memset(buffer, 0, 1024);
      ret = sprintf(buffer, "\t\t\t\tdot1p_priority=%d,", al_conf_data->vlan_info[i].dot1p_priority);
      memcpy(p, buffer, strlen(buffer));
      p += ret;
      *p = '\n';
      p++;

      memset(buffer, 0, 1024);
      ret = sprintf(buffer, "\t\t\t\tdot11e_priority=%d,", al_conf_data->vlan_info[i].dot11e_category);
      memcpy(p, buffer, strlen(buffer));
      p += ret;
      *p = '\n';
      p++;

      memset(buffer, 0, 1024);
      ret = sprintf(buffer, "\t\t\t\tessid=%s,", al_conf_data->vlan_info[i].essid);
      memcpy(p, buffer, strlen(buffer));
      p += ret;
      *p = '\n';
      p++;

      memset(buffer, 0, 1024);
      ret = sprintf(buffer, "\t\t\t\trts_th=%d,", al_conf_data->vlan_info[i].rts_th);
      memcpy(p, buffer, strlen(buffer));
      p += ret;
      *p = '\n';
      p++;

      memset(buffer, 0, 1024);
      ret = sprintf(buffer, "\t\t\t\tfrag_th=%d,", al_conf_data->vlan_info[i].frag_th);
      memcpy(p, buffer, strlen(buffer));
      p += ret;
      *p = '\n';
      p++;

      memset(buffer, 0, 1024);
      ret = sprintf(buffer, "\t\t\t\tbeacon_interval=%d,", al_conf_data->vlan_info[i].beacon_interval);
      memcpy(p, buffer, strlen(buffer));
      p += ret;
      *p = '\n';
      p++;

      memset(buffer, 0, 1024);
      ret = sprintf(buffer, "\t\t\t\tservice=%d,", al_conf_data->vlan_info[i].service);
      memcpy(p, buffer, strlen(buffer));
      p += ret;
      *p = '\n';
      p++;

      memset(buffer, 0, 1024);
      ret = sprintf(buffer, "\t\t\t\ttxpower=%d,", al_conf_data->vlan_info[i].txpower);
      memcpy(p, buffer, strlen(buffer));
      p += ret;
      *p = '\n';
      p++;

      memset(buffer, 0, 1024);
      ret = sprintf(buffer, "\t\t\t\ttxrate=%d,", al_conf_data->vlan_info[i].txrate);
      memcpy(p, buffer, strlen(buffer));
      p += ret;
      *p = '\n';
      p++;

      _write_security_info(&p, &al_conf_data->vlan_info[i].security_info, 4);

      *p = '\n';
      p++;
      memset(buffer, 0, 1024);
      ret = sprintf(buffer, "\t\t}");
      memcpy(p, buffer, strlen(buffer));
      p += ret;
      *p = '\n';
      p++;
   }

   *p = '\n';
   p++;
   memset(buffer, 0, 1024);
   ret = sprintf(buffer, "}");
   memcpy(p, buffer, strlen(buffer));
   p += ret;
   *p = '\n';
   p++;

   *conf_string = p;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return 0;
}

#if MDE_80211N_SUPPORT 

static int _write_ht_capab_info(char **conf_string, al_ht_capab_t *ht_capab, int tab_count)		  
{
	char buffer[1024];
	char *ret_buffer;
	int  ret, i;
	char *p;
	char tab_char[10];
    al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

	memset(tab_char, '\0', sizeof(tab_char));

	for (i = 0; i < tab_count; i++)
	{
		tab_char[i] = '\t';
	}

	ret_buffer = *conf_string;

	p = ret_buffer;

	memset(buffer, 0, 1024);
	ret = sprintf(buffer, "%sht_capab {", tab_char);
	memcpy(p, buffer, strlen(buffer));
	p += ret;
	*p = '\n';
	p++;

	memset(buffer, 0, 1024);
	if (ht_capab->ldpc == AL_CONF_IF_HT_PARAM_ENABLED)
	{
		ret = sprintf(buffer, "%s\tldpc=enabled,",tab_char);
	}
	else if (ht_capab->ldpc== AL_CONF_IF_HT_PARAM_DISABLED)
	{
		ret = sprintf(buffer, "%s\tldpc=disabled,",tab_char);
	}
	memcpy(p, buffer, strlen(buffer));
	p += ret;
	*p = '\n';
	p++;

	memset(buffer, 0, 1024);
	if (ht_capab->ht_bandwidth== AL_CONF_IF_HT_PARAM_40_ABOVE)
	{
		ret = sprintf(buffer, "%s\tht_bandwidth=40+,",tab_char);
	}
	else if (ht_capab->ht_bandwidth== AL_CONF_IF_HT_PARAM_40_BELOW)
	{
		ret = sprintf(buffer, "%s\tht_bandwidth=40-,",tab_char);
	}
	else if (ht_capab->ht_bandwidth== AL_CONF_IF_HT_PARAM_20)
	{
		ret = sprintf(buffer, "%s\tht_bandwidth=20,",tab_char);
	}

	memcpy(p, buffer, strlen(buffer));
	p += ret;
	*p = '\n';
	p++;

	memset(buffer, 0, 1024);
	if (ht_capab->gfmode == AL_CONF_IF_HT_PARAM_ENABLED)
	{
		ret = sprintf(buffer, "%s\tgfmode=enabled,",tab_char);
	}
	else if (ht_capab->gfmode == AL_CONF_IF_HT_PARAM_DISABLED)
	{
		ret = sprintf(buffer, "%s\tgfmode=disabled,",tab_char);
	}
	memcpy(p, buffer, strlen(buffer));
	p += ret;
	*p = '\n';
	p++;

	memset(buffer, 0, 1024);
	if (ht_capab->smps == AL_CONF_IF_HT_PARAM_SMPS_DISABLED)
	{
		ret = sprintf(buffer, "%s\tsmps=disabled,",tab_char);
	}
	else if (ht_capab->smps == AL_CONF_IF_HT_PARAM_STATIC)
	{
		ret = sprintf(buffer, "%s\tsmps=static,",tab_char);
	}
	else if (ht_capab->smps== AL_CONF_IF_HT_PARAM_DYNAMIC)
	{
		ret = sprintf(buffer, "%s\tsmps=dynamic,",tab_char);
	}
	memcpy(p, buffer, strlen(buffer));
	p += ret;
	*p = '\n';
	p++;

	memset(buffer, 0, 1024);
	if (ht_capab->gi_20 == AL_CONF_IF_HT_PARAM_LONG)
	{
		ret = sprintf(buffer, "%s\tgi_20=long,",tab_char);
	}
	else if (ht_capab->gi_20 == AL_CONF_IF_HT_PARAM_AUTO)
	{
		ret = sprintf(buffer, "%s\tgi_20=auto,",tab_char);
	}
	else if (ht_capab->gi_20 == AL_CONF_IF_HT_PARAM_SHORT)
	{
		ret = sprintf(buffer, "%s\tgi_20=short,",tab_char);
	}
	memcpy(p, buffer, strlen(buffer));
	p += ret;
	*p = '\n';
	p++;

	memset(buffer, 0, 1024);
	if (ht_capab->gi_40 == AL_CONF_IF_HT_PARAM_LONG)
	{
		ret = sprintf(buffer, "%s\tgi_40=long,",tab_char);
	}
	else if (ht_capab->gi_40 == AL_CONF_IF_HT_PARAM_AUTO)
	{
		ret = sprintf(buffer, "%s\tgi_40=auto,",tab_char);
	}
	else if (ht_capab->gi_40 == AL_CONF_IF_HT_PARAM_SHORT)
	{
		ret = sprintf(buffer, "%s\tgi_40=short,",tab_char);
	}
	memcpy(p, buffer, strlen(buffer));
	p += ret;
	*p = '\n';
	p++;

	memset(buffer, 0, 1024);
	if (ht_capab->tx_stbc == AL_CONF_IF_HT_PARAM_ENABLED)
	{
		ret = sprintf(buffer, "%s\ttx_stbc=enabled,",tab_char);
	}
	else if (ht_capab->tx_stbc == AL_CONF_IF_HT_PARAM_DISABLED)
	{
		ret = sprintf(buffer, "%s\ttx_stbc=disabled,",tab_char);
	}
	memcpy(p, buffer, strlen(buffer));
	p += ret;
	*p = '\n';
	p++;


	memset(buffer, 0, 1024);
	if (ht_capab->rx_stbc == AL_CONF_IF_HT_PARAM_ENABLED)
	{
		ret = sprintf(buffer, "%s\trx_stbc=enabled,",tab_char);
	}
	else if (ht_capab->rx_stbc == AL_CONF_IF_HT_PARAM_DISABLED)
	{
		ret = sprintf(buffer, "%s\trx_stbc=disabled,",tab_char);
	}
	memcpy(p, buffer, strlen(buffer));
	p += ret;
	*p = '\n';
	p++;

	memset(buffer, 0, 1024);
	if (ht_capab->delayed_ba == AL_CONF_IF_HT_PARAM_ENABLED)
	{
		ret = sprintf(buffer, "%s\tdelayed_ba=enabled,",tab_char);
	}
	else if (ht_capab->delayed_ba == AL_CONF_IF_HT_PARAM_DISABLED)
	{
		ret = sprintf(buffer, "%s\tdelayed_ba=disabled,",tab_char);
	}
	memcpy(p, buffer, strlen(buffer));
	p += ret;
	*p = '\n';
	p++;

	memset(buffer, 0, 1024);
	ret = sprintf(buffer, "%s\tmax_amsdu_len=%d,", tab_char, ht_capab->max_amsdu_len);
	memcpy(p, buffer, strlen(buffer));
	p += ret;
	*p = '\n';
	p++;

	memset(buffer, 0, 1024);
	if (ht_capab->dsss_cck_40 == AL_CONF_IF_HT_PARAM_ALLOW)
	{
		ret = sprintf(buffer, "%s\tdsss_cck_40=allow,",tab_char);
	}
	else if (ht_capab->dsss_cck_40 == AL_CONF_IF_HT_PARAM_DENY)
	{
		ret = sprintf(buffer, "%s\tdsss_cck_40=deny,",tab_char);
	}
	memcpy(p, buffer, strlen(buffer));
	p += ret;
	*p = '\n';
	p++;

	memset(buffer, 0, 1024);
	if (ht_capab->intolerant == AL_CONF_IF_HT_PARAM_ENABLED)
	{
		ret = sprintf(buffer, "%s\tintolerant=enabled,",tab_char);
	}
	else if (ht_capab->intolerant == AL_CONF_IF_HT_PARAM_DISABLED)
	{
		ret = sprintf(buffer, "%s\tintolerant=disabled,",tab_char);
	}
	memcpy(p, buffer, strlen(buffer));
	p += ret;
	*p = '\n';
	p++;

	memset(buffer, 0, 1024);
	if (ht_capab->lsig_txop == AL_CONF_IF_HT_PARAM_ENABLED)
	{
		ret = sprintf(buffer, "%s\tlsig_txop=enabled",tab_char);
	}
	else if (ht_capab->lsig_txop == AL_CONF_IF_HT_PARAM_DISABLED)
	{
		ret = sprintf(buffer, "%s\tlsig_txop=disabled",tab_char);
	}
	memcpy(p, buffer, strlen(buffer));
	p += ret;
	*p = '\n';
	p++;

	memset(buffer, 0, 1024);
	ret = sprintf(buffer, "%s\t},", tab_char);    
	memcpy(p, buffer, strlen(buffer));
	p += ret;
	*p = '\n';
	p++;

	*conf_string = p;
    al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);

	return 0;
}

static int _write_fram_agre_info(char **conf_string, al_fram_agre_t *frame_aggregation, int tab_count)
{

	char buffer[1024];
	char *ret_buffer;
	int  ret, i;
	char *p;
	char tab_char[10];
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

	memset(tab_char, '\0', sizeof(tab_char));

	for (i = 0; i < tab_count; i++)
	{
		tab_char[i] = '\t';
	}

	ret_buffer = *conf_string;

	p = ret_buffer;

	memset(buffer, 0, 1024);
	ret = sprintf(buffer, "%sframe_aggregation {", tab_char);
	memcpy(p, buffer, strlen(buffer));
	p += ret;
	*p = '\n';
	p++;

	memset(buffer, 0, 1024);
	ret = sprintf(buffer, "%s\tampdu_enable=%d,", tab_char, frame_aggregation->ampdu_enable);
	memcpy(p, buffer, strlen(buffer));
	p += ret;
	*p = '\n';
	p++;

	memset(buffer, 0, 1024);
	ret = sprintf(buffer, "%s\t max_ampdu_len=%d", tab_char, frame_aggregation->max_ampdu_len);
	memcpy(p, buffer, strlen(buffer));
	p += ret;
	*p = '\n';
	p++;

	memset(buffer, 0, 1024);
	ret = sprintf(buffer, "%s\t }", tab_char);
	memcpy(p, buffer, strlen(buffer));
	p += ret;

	*conf_string = p;
    al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);

	return 0;
}

#endif

#if MDE_80211AC_SUPPORT  

static int _write_vht_capab_info(char **conf_string, al_vht_capab_t *vht_capab, int tab_count)		  
{
	char buffer[1024];
	char *ret_buffer;
	int  ret, i;
	char *p;
	char tab_char[10];
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

	memset(tab_char, '\0', sizeof(tab_char));

	for (i = 0; i < tab_count; i++)
	{
		tab_char[i] = '\t';
	}

	ret_buffer = *conf_string;

	p = ret_buffer;

	memset(buffer, 0, 1024);
	ret = sprintf(buffer, "%svht_capab {", tab_char);
	memcpy(p, buffer, strlen(buffer));
	p += ret;
	*p = '\n';
	p++;

	memset(buffer, 0, 1024);
	ret = sprintf(buffer, "%s\tmax_mpdu_len=%d,", tab_char, vht_capab->max_mpdu_len);
	memcpy(p, buffer, strlen(buffer));
	p += ret;
	*p = '\n';
	p++;

	memset(buffer, 0, 1024);
	ret = sprintf(buffer, "%s\tsupported_channel_width=%d,", tab_char, vht_capab->supported_channel_width);
	memcpy(p, buffer, strlen(buffer));
	p += ret;
	*p = '\n';
	p++;

	memset(buffer, 0, 1024);
	if (vht_capab->rx_ldpc == AL_CONF_IF_HT_PARAM_ENABLED)
	{
		ret = sprintf(buffer, "%s\trx_ldpc=enabled,",tab_char);
	}
	if (vht_capab->rx_ldpc == AL_CONF_IF_HT_PARAM_DISABLED)
	{
		ret = sprintf(buffer, "%s\trx_ldpc=disabled,",tab_char);
	}
	memcpy(p, buffer, strlen(buffer));
	p += ret;
	*p = '\n';
	p++;

	memset(buffer, 0, 1024);
	if (vht_capab->gi_80 == AL_CONF_IF_HT_PARAM_LONG)
	{
		ret = sprintf(buffer, "%s\tgi_80=long,",tab_char);
	}
	else if (vht_capab->gi_80 == AL_CONF_IF_HT_PARAM_AUTO)
	{
		ret = sprintf(buffer, "%s\tgi_80=auto,",tab_char);
	}
	else if (vht_capab->gi_80 == AL_CONF_IF_HT_PARAM_SHORT)
	{
		ret = sprintf(buffer, "%s\tgi_80=short,",tab_char);
	}
	memcpy(p, buffer, strlen(buffer));
	p += ret;
	*p = '\n';
	p++;

	memset(buffer, 0, 1024);
	if (vht_capab->gi_160 == AL_CONF_IF_HT_PARAM_LONG)
	{
		ret = sprintf(buffer, "%s\tgi_160=long,",tab_char);
	}
	else if (vht_capab->gi_160 == AL_CONF_IF_HT_PARAM_AUTO)
	{
		ret = sprintf(buffer, "%s\tgi_160=auto,",tab_char);
	}
	else if (vht_capab->gi_160 == AL_CONF_IF_HT_PARAM_SHORT)
	{
		ret = sprintf(buffer, "%s\tgi_160=short,",tab_char);
	}
	memcpy(p, buffer, strlen(buffer));
	p += ret;
	*p = '\n';
	p++;

	memset(buffer, 0, 1024);
	if (vht_capab->tx_stbc == AL_CONF_IF_HT_PARAM_ENABLED)
	{
		ret = sprintf(buffer, "%s\ttx_stbc=enabled,",tab_char);
	}
	else if (vht_capab->tx_stbc == AL_CONF_IF_HT_PARAM_DISABLED)
	{
		ret = sprintf(buffer, "%s\ttx_stbc=disabled,",tab_char);
	}
	memcpy(p, buffer, strlen(buffer));
	p += ret;
	*p = '\n';
	p++;

	memset(buffer, 0, 1024);
	if (vht_capab->rx_stbc == AL_CONF_IF_HT_PARAM_ENABLED)
	{
		ret = sprintf(buffer, "%s\trx_stbc=enabled,",tab_char);
	}
	else if (vht_capab->rx_stbc == AL_CONF_IF_HT_PARAM_DISABLED)
	{
		ret = sprintf(buffer, "%s\trx_stbc=disabled,",tab_char);
	}
	memcpy(p, buffer, strlen(buffer));
	p += ret;
	*p = '\n';
	p++;

	memset(buffer, 0, 1024);
	if (vht_capab->su_beamformer_cap == AL_CONF_IF_VHT_PARAM_YES)
	{
		ret = sprintf(buffer, "%s\tsu_beamformer_cap=yes,",tab_char);
	}
	else if (vht_capab->su_beamformer_cap == AL_CONF_IF_VHT_PARAM_NO)
	{
		ret = sprintf(buffer, "%s\tsu_beamformer_cap=no,",tab_char);
	}
	memcpy(p, buffer, strlen(buffer));
	p += ret;
	*p = '\n';
	p++;

	memset(buffer, 0, 1024);
	if (vht_capab->su_beamformee_cap == AL_CONF_IF_VHT_PARAM_YES)
	{
		ret = sprintf(buffer, "%s\tsu_beamformee_cap=yes,",tab_char);
	}
	else if (vht_capab->su_beamformee_cap == AL_CONF_IF_VHT_PARAM_NO)
	{
		ret = sprintf(buffer, "%s\tsu_beamformee_cap=no,",tab_char);
	}
	memcpy(p, buffer, strlen(buffer));
	p += ret;
	*p = '\n';
	p++;

	memset(buffer, 0, 1024);
	ret = sprintf(buffer, "%s\tbeamformee_sts_count=%d,", tab_char, vht_capab->beamformee_sts_count);
	memcpy(p, buffer, strlen(buffer));
	p += ret;
	*p = '\n';
	p++;

	memset(buffer, 0, 1024);
	ret = sprintf(buffer, "%s\tsounding_dimensions=%d,", tab_char, vht_capab->sounding_dimensions);
	memcpy(p, buffer, strlen(buffer));
	p += ret;
	*p = '\n';
	p++;

	memset(buffer, 0, 1024);
	if (vht_capab->mu_beamformer_cap == AL_CONF_IF_VHT_PARAM_YES)
	{
		ret = sprintf(buffer, "%s\tmu_beamformer_cap=yes,",tab_char);
	}
	else if (vht_capab->mu_beamformer_cap == AL_CONF_IF_VHT_PARAM_NO)
	{
		ret = sprintf(buffer, "%s\tmu_beamformer_cap=no,",tab_char);
	}
	memcpy(p, buffer, strlen(buffer));
	p += ret;
	*p = '\n';
	p++;

	memset(buffer, 0, 1024);
	if (vht_capab->mu_beamformee_cap == AL_CONF_IF_VHT_PARAM_YES)
	{
		ret = sprintf(buffer, "%s\tmu_beamformee_cap=yes,",tab_char);
	}
	else if (vht_capab->mu_beamformee_cap == AL_CONF_IF_VHT_PARAM_NO)
	{
		ret = sprintf(buffer, "%s\tmu_beamformee_cap=no,",tab_char);
	}
	memcpy(p, buffer, strlen(buffer));
	p += ret;
	*p = '\n';
	p++;

	memset(buffer, 0, 1024);
	if (vht_capab->vht_txop_ps == AL_CONF_IF_VHT_PARAM_YES)
	{
		ret = sprintf(buffer, "%s\tvht_txop_ps=yes,",tab_char);
	}
	else if (vht_capab->vht_txop_ps == AL_CONF_IF_VHT_PARAM_NO)
	{
		ret = sprintf(buffer, "%s\tvht_txop_ps=no,",tab_char);
	}
	memcpy(p, buffer, strlen(buffer));
	p += ret;
	*p = '\n';
	p++;

	memset(buffer, 0, 1024);
	if (vht_capab->htc_vht_cap == AL_CONF_IF_VHT_PARAM_YES)
	{
		ret = sprintf(buffer, "%s\thtc_vht_cap=yes,",tab_char);
	}
	else if (vht_capab->htc_vht_cap == AL_CONF_IF_VHT_PARAM_NO)
	{
		ret = sprintf(buffer, "%s\thtc_vht_cap=no,",tab_char);
	}
	memcpy(p, buffer, strlen(buffer));
	p += ret;
	*p = '\n';
	p++;

	memset(buffer, 0, 1024);
	if (vht_capab->rx_ant_pattern_consistency == AL_CONF_IF_VHT_PARAM_YES)
	{
		ret = sprintf(buffer, "%s\trx_ant_pattern_consistency=yes,",tab_char);
	}
	else if (vht_capab->rx_ant_pattern_consistency == AL_CONF_IF_VHT_PARAM_NO)
	{
		ret = sprintf(buffer, "%s\trx_ant_pattern_consistency=no,",tab_char);
	}
	memcpy(p, buffer, strlen(buffer));
	p += ret;
	*p = '\n';
	p++;

	memset(buffer, 0, 1024);
	if (vht_capab->tx_ant_pattern_consistency == AL_CONF_IF_VHT_PARAM_YES)
	{
		ret = sprintf(buffer, "%s\ttx_ant_pattern_consistency=yes,",tab_char);
	}
	else if (vht_capab->tx_ant_pattern_consistency == AL_CONF_IF_VHT_PARAM_NO)
	{
		ret = sprintf(buffer, "%s\ttx_ant_pattern_consistency=no,",tab_char);
	}
	memcpy(p, buffer, strlen(buffer));
	p += ret;
	*p = '\n';
	p++;

	memset(buffer, 0, 1024);
	ret = sprintf(buffer, "%s\tvht_oper_bandwidth=%d,", tab_char, vht_capab->vht_oper_bandwidth);
	memcpy(p, buffer, strlen(buffer));
	p += ret;
	*p = '\n';
	p++;

	memset(buffer, 0, 1024);
	ret = sprintf(buffer, "%s\tseg0_center_freq=%d,", tab_char, vht_capab->seg0_center_freq);
	memcpy(p, buffer, strlen(buffer));
	p += ret;
	*p = '\n';
	p++;

	memset(buffer, 0, 1024);
	ret = sprintf(buffer, "%s\tseg1_center_freq=%d", tab_char, vht_capab->seg1_center_freq);
	memcpy(p, buffer, strlen(buffer));
	p += ret;
	*p = '\n';
	p++;

	memset(buffer, 0, 1024);
	ret = sprintf(buffer, "%s\t}", tab_char);    
	memcpy(p, buffer, strlen(buffer));
	p += ret;
	*p = '\n';
	p++;

	*conf_string = p;
    al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);

	return 0;
}
#endif

int al_conf_create_config_string_from_data(AL_CONTEXT_PARAM_DECL unsigned int al_conf_handle, int *al_conf_string_length, char **al_conf_string)
{
   _al_conf_data_t   *data;
   char              *p;
   char              buffer[1024];
   char              *ret_buffer;
   int               ret;
   int               i, j;
   al_conf_if_info_t *if_info;
   al_conf_option_t  *options;

   data = (_al_conf_data_t *)al_conf_handle;
   *al_conf_string_length = 0;
   *al_conf_string        = NULL;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   if (data == NULL)
   {
      return -1;
   }

   ret_buffer = (char *)al_heap_alloc(AL_CONTEXT 256 * 1024 AL_HEAP_DEBUG_PARAM);

   memset(ret_buffer, 0, 256 * 1024);
   p = ret_buffer;

   memset(buffer, 0, 1024);
   ret = sprintf(buffer, "# MeshDynamics H300 config file\n\n");
   memcpy(p, buffer, strlen(buffer));
   p += ret;
   *p = '\n';
   p++;

   memset(buffer, 0, 1024);
   ret = sprintf(buffer, "name			= %s\n", data->name);
   memcpy(p, buffer, strlen(buffer));
   p += ret;
   *p = '\n';
   p++;

   memset(buffer, 0, 1024);
   ret = sprintf(buffer, "description		= %s\n", data->description);
   memcpy(p, buffer, strlen(buffer));
   p += ret;
   *p = '\n';
   p++;

   memset(buffer, 0, 1024);
   ret = sprintf(buffer, "mesh_id			= %s", data->mesh_id);
   memcpy(p, buffer, strlen(buffer));
   p += ret;
   *p = '\n';
   p++;

   memset(buffer, 0, 1024);
   ret = sprintf(buffer, "\t# The mesh_id helps segregate a large mesh network into smaller manageable entities\n\n");
   memcpy(p, buffer, strlen(buffer));
   p += ret;
   *p = '\n';
   p++;

   memset(buffer, 0, 1024);
   ret = sprintf(buffer, "mesh_imcp_key {");
   memcpy(p, buffer, strlen(buffer));
   p += ret;
   *p = '\n';
   p++;

   memset(buffer, 0, 1024);
   ret = sprintf(buffer, "%s", data->mesh_imcp_key_buffer);
   memcpy(p, buffer, strlen(buffer));
   p += ret;
   *p = '\n';
   p++;

   memset(buffer, 0, 1024);
   ret = sprintf(buffer, "}\n\n\n");
   memcpy(p, buffer, strlen(buffer));
   p += ret;
   *p = '\n';
   p++;

   memset(buffer, 0, 1024);
   ret = sprintf(buffer, "# Access Point information\n\n");
   memcpy(p, buffer, strlen(buffer));
   p += ret;
   *p = '\n';
   p++;

   memset(buffer, 0, 1024);
   ret = sprintf(buffer, "essid			= %s\n", data->essid);
   memcpy(p, buffer, strlen(buffer));
   p += ret;
   *p = '\n';
   p++;

   memset(buffer, 0, 1024);
   ret = sprintf(buffer, "rts_th			= %u\n", data->rts_th);
   memcpy(p, buffer, strlen(buffer));
   p += ret;
   *p = '\n';
   p++;

   memset(buffer, 0, 1024);
   ret = sprintf(buffer, "frag_th			= %u\n", data->frag_th);
   memcpy(p, buffer, strlen(buffer));
   p += ret;
   *p = '\n';
   p++;

   memset(buffer, 0, 1024);
   ret = sprintf(buffer, "beacon_interval \t= %u\n\n", data->beacon_interval);
   memcpy(p, buffer, strlen(buffer));
   p += ret;
   *p = '\n';
   p++;

   memset(buffer, 0, 1024);
   ret = sprintf(buffer, "dynamic_channel_allocation	\t= %d\t\t# Global DCA flag overrides individual DCA settings\n", data->dynamic_channel_allocation);
   memcpy(p, buffer, strlen(buffer));
   p += ret;
   *p = '\n';
   p++;

   memset(buffer, 0, 1024);
   ret = sprintf(buffer, "stay_awake_count \t= %u\n\n", data->stay_awake_count);
   memcpy(p, buffer, strlen(buffer));
   p += ret;
   *p = '\n';
   p++;

   memset(buffer, 0, 1024);
   ret = sprintf(buffer, "bridge_ageing_time \t= %u\t\t# Ageing time in seconds\n\n", data->bridge_ageing_time);
   memcpy(p, buffer, strlen(buffer));
   p += ret;
   *p = '\n';
   p++;

   memset(buffer, 0, 1024);
   ret = sprintf(buffer, "wds_encrypted \t= %d\n\n", data->wds_encrypted);
   memcpy(p, buffer, strlen(buffer));
   p += ret;
   *p = '\n';
   p++;

   memset(buffer, 0, 1024);
   ret = sprintf(buffer, "model			= %s\n", data->model);
   memcpy(p, buffer, strlen(buffer));
   p += ret;
   *p = '\n';
   p++;

   memset(buffer, 0, 1024);
   ret = sprintf(buffer, "gps_x_coordinate			= %s\n", data->gps_x_coordinate);
   memcpy(p, buffer, strlen(buffer));
   p += ret;
   *p = '\n';
   p++;

   memset(buffer, 0, 1024);
   ret = sprintf(buffer, "gps_y_coordinate			= %s\n", data->gps_y_coordinate);
   memcpy(p, buffer, strlen(buffer));
   p += ret;
   *p = '\n';
   p++;

   memset(buffer, 0, 1024);
   ret = sprintf(buffer, "fcc_certified_operation \t= %d\n\n", data->fcc_certified_operation);
   memcpy(p, buffer, strlen(buffer));
   p += ret;
   *p = '\n';
   p++;

   memset(buffer, 0, 1024);
   ret = sprintf(buffer, "etsi_certified_operation \t= %d\n\n", data->etsi_certified_operation);
   memcpy(p, buffer, strlen(buffer));
   p += ret;
   *p = '\n';
   p++;

   memset(buffer, 0, 1024);
   ret = sprintf(buffer, "regulatory_domain \t= %d\n\n", data->regulatory_domain);
   memcpy(p, buffer, strlen(buffer));
   p += ret;
   *p = '\n';
   p++;

   memset(buffer, 0, 1024);
   ret = sprintf(buffer, "country_code \t= %d\n\n", data->country_code);
   memcpy(p, buffer, strlen(buffer));
   p += ret;
   *p = '\n';
   p++;


   memset(buffer, 0, 1024);
   ret = sprintf(buffer, "ds_tx_rate \t= %d\n\n", data->ds_tx_rate);
   memcpy(p, buffer, strlen(buffer));
   p += ret;
   *p = '\n';
   p++;

   memset(buffer, 0, 1024);
   ret = sprintf(buffer, "ds_tx_power \t= %d\n\n", data->ds_tx_power);
   memcpy(p, buffer, strlen(buffer));
   p += ret;
   *p = '\n';
   p++;

   memset(buffer, 0, 1024);
   ret = sprintf(buffer, "config_sqnr \t= %d\n\n", data->config_sqnr);
   memcpy(p, buffer, strlen(buffer));
   p += ret;
   *p = '\n';
   p++;

   memset(buffer, 0, 1024);
   ret = sprintf(buffer, "# Information about interfaces\n\n");
   memcpy(p, buffer, strlen(buffer));
   p += ret;
   *p = '\n';
   p++;

   memset(buffer, 0, 1024);
   ret = sprintf(buffer, "interface_info (%d) {\n", data->if_count);
   memcpy(p, buffer, strlen(buffer));
   p += ret;
   *p = '\n';
   p++;

   for (i = 0; i < data->if_count; i++)
   {
      if_info = &data->if_info[i];

      memset(buffer, 0, 1024);
      ret = sprintf(buffer, "\t%s {\n", if_info->name);
      memcpy(p, buffer, strlen(buffer));
      p += ret;

      memset(buffer, 0, 1024);
      if (if_info->phy_type == AL_CONF_IF_PHY_TYPE_ETHERNET)
      {
         ret = sprintf(buffer, "\t\tmedium_type=e,\n");
      }
      else if (if_info->phy_type == AL_CONF_IF_PHY_TYPE_802_11)
      {
         ret = sprintf(buffer, "\t\tmedium_type=w,\n");
      }
      else if (if_info->phy_type == AL_CONF_IF_PHY_TYPE_802_11_VIRTUAL)
      {
         ret = sprintf(buffer, "\t\tmedium_type=v,\n");
      }
      memcpy(p, buffer, strlen(buffer));
      p += ret;

      memset(buffer, 0, 1024);

      switch (if_info->phy_sub_type)
      {
      case AL_CONF_IF_PHY_SUB_TYPE_802_11_A:
         ret = sprintf(buffer, "\t\tmedium_sub_type=a,\n");
         break;

      case AL_CONF_IF_PHY_SUB_TYPE_802_11_B:
         ret = sprintf(buffer, "\t\tmedium_sub_type=b,\n");
         break;

      case AL_CONF_IF_PHY_SUB_TYPE_802_11_G:
         ret = sprintf(buffer, "\t\tmedium_sub_type=g,\n");
         break;

      case AL_CONF_IF_PHY_SUB_TYPE_802_11_BG:
         ret = sprintf(buffer, "\t\tmedium_sub_type=bg,\n");
         break;

      case AL_CONF_IF_PHY_SUB_TYPE_802_11_PSQ:
         ret = sprintf(buffer, "\t\tmedium_sub_type=psq,\n");
         break;

      case AL_CONF_IF_PHY_SUB_TYPE_802_11_PSH:
         ret = sprintf(buffer, "\t\tmedium_sub_type=psh,\n");
         break;

      case AL_CONF_IF_PHY_SUB_TYPE_802_11_PSF:
         ret = sprintf(buffer, "\t\tmedium_sub_type=psf,\n");
         break;
#if (MDE_80211N_SUPPORT || MDE_80211AC_SUPPORT)  
			case AL_CONF_IF_PHY_SUB_TYPE_802_11_N_2_4GHZ:
                                    al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "al_conf.c(%s,%d):\n",__func__,__LINE__);
				ret = sprintf(buffer, "\t\tmedium_sub_type=n_2_4GHZ,\n");
				break;
			case AL_CONF_IF_PHY_SUB_TYPE_802_11_N_5GHZ:
                                    al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "al_conf.c(%s,%d):\n",__func__,__LINE__);
				ret = sprintf(buffer, "\t\tmedium_sub_type=n_5GHZ,\n");
				break;
			case AL_CONF_IF_PHY_SUB_TYPE_802_11_AC:
				ret = sprintf(buffer, "\t\tmedium_sub_type=ac,\n");
				break;

			case AL_CONF_IF_PHY_SUB_TYPE_802_11_BGN:
				ret = sprintf(buffer, "\t\tmedium_sub_type=bgn,\n");
				break;

			case AL_CONF_IF_PHY_SUB_TYPE_802_11_AN:
				ret = sprintf(buffer, "\t\tmedium_sub_type=an,\n");
				break;

			case AL_CONF_IF_PHY_SUB_TYPE_802_11_ANAC:
				ret = sprintf(buffer, "\t\tmedium_sub_type=anac,\n");
				break;
#endif
      default:
         ret = sprintf(buffer, "\t\tmedium_sub_type=x,\n");
      }

      memcpy(p, buffer, strlen(buffer));
      p += ret;

      memset(buffer, 0, 1024);

      switch (if_info->use_type)
      {
      case AL_CONF_IF_USE_TYPE_DS:
         ret = sprintf(buffer, "\t\tusage_type=ds,\n");
         break;

      case AL_CONF_IF_USE_TYPE_WM:
         ret = sprintf(buffer, "\t\tusage_type=wm,\n");
         break;

      case AL_CONF_IF_USE_TYPE_AP:
         ret = sprintf(buffer, "\t\tusage_type=ap,\n");
         break;

      case AL_CONF_IF_USE_TYPE_PMON:
         ret = sprintf(buffer, "\t\tusage_type=pmon,\n");
         break;

      case AL_CONF_IF_USE_TYPE_AMON:
         ret = sprintf(buffer, "\t\tusage_type=amon,\n");

      default:
         ret = sprintf(buffer, "\t\tusage_type=ds,\n");
      }

      memcpy(p, buffer, strlen(buffer));
      p += ret;

#if 1 
		if(if_info->phy_type!=AL_CONF_IF_PHY_TYPE_ETHERNET)
		{
#endif
      memset(buffer, 0, 1024);
      ret = sprintf(buffer, "\t\tchannel=%d,\n", if_info->wm_channel);
      memcpy(p, buffer, strlen(buffer));
      p += ret;

      memset(buffer, 0, 1024);

      switch (if_info->bonding)
      {
      case AL_CONF_IF_BONDING_SINGLE:
         ret = sprintf(buffer, "\t\tbonding=s,\n");
         break;

      case AL_CONF_IF_BONDING_DOUBLE:
         ret = sprintf(buffer, "\t\tbonding=d,\n");
         break;

      default:
         ret = sprintf(buffer, "\t\tbonding=x,\n");
      }

      memcpy(p, buffer, strlen(buffer));
      p += ret;

      memset(buffer, 0, 1024);
      ret = sprintf(buffer, "\t\tessid=%s,\n", if_info->essid);
      memcpy(p, buffer, strlen(buffer));
      p += ret;

      memset(buffer, 0, 1024);
      ret = sprintf(buffer, "\t\trts_th=%d,\n", if_info->rts_th);
      memcpy(p, buffer, strlen(buffer));
      p += ret;

      memset(buffer, 0, 1024);
      ret = sprintf(buffer, "\t\tfrag_th=%d,\n", if_info->frag_th);
      memcpy(p, buffer, strlen(buffer));
      p += ret;

      memset(buffer, 0, 1024);
      ret = sprintf(buffer, "\t\tbeacon_interval=%d,\n", if_info->beacon_interval);
      memcpy(p, buffer, strlen(buffer));
      p += ret;

      memset(buffer, 0, 1024);
      ret = sprintf(buffer, "\t\tdca=%d,\n", if_info->dca);
      memcpy(p, buffer, strlen(buffer));
      p += ret;



      memset(buffer, 0, 1024);
      ret = sprintf(buffer, "\t\ttxpower=%d,\n", if_info->txpower);
      memcpy(p, buffer, strlen(buffer));
      p += ret;

      memset(buffer, 0, 1024);
      ret = sprintf(buffer, "\t\ttxant=%d,\n", if_info->txant);
      memcpy(p, buffer, strlen(buffer));
      p += ret;

	}

      memset(buffer, 0, 1024);
      ret = sprintf(buffer, "\t\ttxrate=%d,\n", if_info->txrate);
      memcpy(p, buffer, strlen(buffer));
      p += ret;

	if(if_info->phy_type!=AL_CONF_IF_PHY_TYPE_ETHERNET)
	{

      if (if_info->dca_list_count > 0)
      {
         memset(buffer, 0, 1024);
         ret = sprintf(buffer, "\t\tdca_list (%d) {\t\t# dca_list for the interface\n", if_info->dca_list_count);
         memcpy(p, buffer, strlen(buffer));
         p += ret;


         memset(buffer, 0, 1024);
         ret = sprintf(buffer, "\t\t\t");
         memcpy(p, buffer, strlen(buffer));
         p += ret;


         for (j = 0; j < if_info->dca_list_count - 1; j++)
         {
            memset(buffer, 0, 1024);
            ret = sprintf(buffer, "\t\t\t%d,", if_info->dca_list[j]);
            memcpy(p, buffer, strlen(buffer));
            p += ret;
            *p = '\n';
            p++;
         }

         memset(buffer, 0, 1024);
         ret = sprintf(buffer, "%d\n", if_info->dca_list[if_info->dca_list_count - 1]);
         memcpy(p, buffer, strlen(buffer));
         p += ret;

         memset(buffer, 0, 1024);
         ret = sprintf(buffer, "\t\t},\n");
         memcpy(p, buffer, strlen(buffer));
         p += ret;
         *p = '\n';
         p++;
      }

      memset(buffer, 0, 1024);
      ret = sprintf(buffer, "\t\tpriv_chan_bw=%d,\n", if_info->priv_channel_bw);
      memcpy(p, buffer, strlen(buffer));
      p += ret;

      memset(buffer, 0, 1024);
      ret = sprintf(buffer, "\t\tpriv_chan_ant_max=%d,\n", if_info->priv_channel_ant_max);
      memcpy(p, buffer, strlen(buffer));
      p += ret;

      memset(buffer, 0, 1024);
      ret = sprintf(buffer, "\t\tpriv_chan_ctl=%d,\n", if_info->priv_channel_ctl);
      memcpy(p, buffer, strlen(buffer));
      p += ret;

      memset(buffer, 0, 1024);
      ret = sprintf(buffer, "\t\tpriv_chan_power=%d,\n", if_info->priv_channel_max_power);
      memcpy(p, buffer, strlen(buffer));
      p += ret;

      if (if_info->priv_channel_count > 0)
      {
         memset(buffer, 0, 1024);
         ret = sprintf(buffer, "\t\tpriv_chan_list (%d) {\t\t# priv_chan_list for the interface\n", if_info->priv_channel_count);
         memcpy(p, buffer, strlen(buffer));
         p += ret;


         memset(buffer, 0, 1024);
         ret = sprintf(buffer, "\t\t\t");
         memcpy(p, buffer, strlen(buffer));
         p += ret;


         for (j = 0; j < if_info->priv_channel_count - 1; j++)
         {
            memset(buffer, 0, 1024);
            ret = sprintf(buffer, "\t\t\t%d:%d:%d,", if_info->priv_frequencies[j],
                          if_info->priv_channel_numbers[j],
                          if_info->priv_channel_flags[j]);
            memcpy(p, buffer, strlen(buffer));
            p += ret;
            *p = '\n';
            p++;
         }

         memset(buffer, 0, 1024);
         ret = sprintf(buffer, "%d:%d:%d\n", if_info->priv_frequencies[if_info->priv_channel_count - 1],
                       if_info->priv_channel_numbers[if_info->priv_channel_count - 1],
                       if_info->priv_channel_flags[if_info->priv_channel_count - 1]);

         memcpy(p, buffer, strlen(buffer));
         p += ret;

         memset(buffer, 0, 1024);
         ret = sprintf(buffer, "\t\t},\n");
         memcpy(p, buffer, strlen(buffer));
         p += ret;
         *p = '\n';
         p++;
      }

      memset(buffer, 0, 1024);
      if (if_info->preamble_type == AL_CONF_IF_PREAMBLE_TYPE_LONG)
      {
         ret = sprintf(buffer, "\t\tpreamble_type=long,\n");
      }
      else if (if_info->preamble_type == AL_CONF_IF_PREAMBLE_TYPE_SHORT)
      {
         ret = sprintf(buffer, "\t\tpreamble_type=short,\n");
      }
      memcpy(p, buffer, strlen(buffer));
      p += ret;
		}

      memset(buffer, 0, 1024);
      if (if_info->slot_time_type == AL_CONF_IF_SLOT_TIME_TYPE_LONG)
      {
         ret = sprintf(buffer, "\t\tslot_time_type=long,\n");
      }
      else if (if_info->slot_time_type == AL_CONF_IF_SLOT_TIME_TYPE_SHORT)
      {
         ret = sprintf(buffer, "\t\tslot_time_type=short,\n");
      }
      memcpy(p, buffer, strlen(buffer));
      p += ret;

      memset(buffer, 0, 1024);
      ret = sprintf(buffer, "\t\tservice=%d", if_info->service);
      memcpy(p, buffer, strlen(buffer));
      p += ret;
      *p = ',';
      p++;
      *p = '\n';
      p++;

      memset(buffer, 0, 1024);
      ret = sprintf(buffer, "\t\tack_timeout=%d", if_info->ack_timeout);
      memcpy(p, buffer, strlen(buffer));
      p += ret;

      if(if_info->phy_type!=AL_CONF_IF_PHY_TYPE_ETHERNET)
      {
	      *p = ',';
	      p++;
      }

      *p = '\n';
      p++;

      if(if_info->phy_type!=AL_CONF_IF_PHY_TYPE_ETHERNET)
      {
	      memset(buffer, 0, 1024);
	      ret = sprintf(buffer, "\t\thide_ssid=%d", if_info->hide_ssid);
	      memcpy(p, buffer, strlen(buffer));
	      p += ret;
	      *p = ',';
	      p++;
	      *p = '\n';
	      p++;

	      memset(buffer, 0, 1024);
	      ret = sprintf(buffer, "\t\tdot11e_if_enabled=%d", if_info->dot11e_enabled);
	      memcpy(p, buffer, strlen(buffer));
	      p += ret;
	      *p = ',';
	      p++;
	      *p = '\n';
	      p++;

	      memset(buffer, 0, 1024);
	      ret = sprintf(buffer, "\t\tdot11e_if_category=%d", if_info->dot11e_category);
	      memcpy(p, buffer, strlen(buffer));
	      p += ret;
	      *p = ',';
	      p++;
	      *p = '\n';
	      p++;


	      /* add security here */
	      ret = _write_security_info(&p, &if_info->security_info, 2);
#if (MDE_80211N_SUPPORT || MDE_80211AC_SUPPORT)   

	      if (CHECK_FOR_N_AND_AC_SUBTYPES(if_info->phy_sub_type))
	      {
		      *p=',';
		      p++;

	      }
#endif

	      *p = '\n';
	      p++;

#if (MDE_80211N_SUPPORT || MDE_80211AC_SUPPORT) 

	      if (CHECK_FOR_N_AND_AC_SUBTYPES(if_info->phy_sub_type))
	      {

		      ret = _write_ht_capab_info(&p, &if_info->ht_capab, 2);
		      *p = '\n';
		      p++;

		      ret= _write_fram_agre_info(&p, &if_info->frame_aggregation, 2);
		      if( (if_info->phy_sub_type == AL_CONF_IF_PHY_SUB_TYPE_802_11_AC) || (if_info->phy_sub_type == AL_CONF_IF_PHY_SUB_TYPE_802_11_ANAC) )
		      {
			      *p=',';
			      p++;
		      }
		      *p = '\n';
		      p++;
	      }

	      if( (if_info->phy_sub_type == AL_CONF_IF_PHY_SUB_TYPE_802_11_AC) || (if_info->phy_sub_type == AL_CONF_IF_PHY_SUB_TYPE_802_11_ANAC) )
	      {

		      ret= _write_vht_capab_info(&p, &if_info->vht_capab, 2);
		      *p = '\n';
		      p++;
	      }
      }

#endif
      /*	ret = _write_ht_capab_info(&p, &if_info->ht_capab, 2);
       *p = '\n';
       p++;

       ret= _write_fram_agre_info(&p, &if_info->frame_aggregation, 2);
       *p = '\n';
       p++;

       ret= _write_vht_capab_info(&p, &if_info->vht_capab, 2);
       *p = '\n';
       p++; */

      memset(buffer, 0, 1024);
      ret = sprintf(buffer, "\t}\n");
      memcpy(p, buffer, strlen(buffer));
      p += ret;
      *p = '\n';
      p++;
   }

   memset(buffer, 0, 1024);
   ret = sprintf(buffer, "}\n\n");
   memcpy(p, buffer, strlen(buffer));
   p += ret;
   *p = '\n';
   p++;

   /* Add vlan info here */

   _write_vlan_info(&p, al_conf_handle);

   *p = '\n';
   p++;

   p += _write_effistream_info(p, data);

   *p = '\n';
   p++;

   /** GPS Info */

   p += sprintf(p,
                "gps {\n"
                "\tenabled=%d,\n"
                "\tinput=%s,\n"
                "\tpush_dest=%d.%d.%d.%d:%d,\n"
                "\tpush_int=%d\n"
                "}\n",
                data->gps_info.gps_enabled,
                data->gps_info.gps_input_dev,
                data->gps_info.gps_push_dest_ip[0],
                data->gps_info.gps_push_dest_ip[1],
                data->gps_info.gps_push_dest_ip[2],
                data->gps_info.gps_push_dest_ip[3],
                data->gps_info.gps_push_port,
                data->gps_info.gps_push_interval);

   *p = '\n';
   p++;

   /** DHCP Info */

   p += sprintf(p,
                "dhcp {\n"
                "\tnet_id=%d.%d.%d.%d,\n"
                "\tmask=%d.%d.%d.%d,\n"
                "\tmode=%d,\n"
                "\tgateway=%d.%d.%d.%d,\n"
                "\tdns=%d.%d.%d.%d,\n"
                "\tlease=%d\n"
                "}\n",
                data->dhcp_info.dhcp_net_id[0],
                data->dhcp_info.dhcp_net_id[1],
                data->dhcp_info.dhcp_net_id[2],
                data->dhcp_info.dhcp_net_id[3],
                data->dhcp_info.dhcp_mask[0],
                data->dhcp_info.dhcp_mask[1],
                data->dhcp_info.dhcp_mask[2],
                data->dhcp_info.dhcp_mask[3],
                data->dhcp_info.dhcp_mode,
                data->dhcp_info.dhcp_gateway[0],
                data->dhcp_info.dhcp_gateway[1],
                data->dhcp_info.dhcp_gateway[2],
                data->dhcp_info.dhcp_gateway[3],
                data->dhcp_info.dhcp_dns[0],
                data->dhcp_info.dhcp_dns[1],
                data->dhcp_info.dhcp_dns[2],
                data->dhcp_info.dhcp_dns[3],
                data->dhcp_info.dhcp_lease_time);

   *p = '\n';
   p++;

   /** LOGMON Info */

   p += sprintf(p,
                "logmon {\n"
                "\tpush_dest=%d.%d.%d.%d:%d\n"
                "}\n",
                data->logmon_info.logmon_dest_ip[0],
                data->logmon_info.logmon_dest_ip[1],
                data->logmon_info.logmon_dest_ip[2],
                data->logmon_info.logmon_dest_ip[3],
                data->logmon_info.logmon_dest_port);

   *p = '\n';
   p++;

   /** Options Info */

   p += sprintf(p, "options {\n");

   for (options = data->option_info; options != NULL; options = options->next)
   {
      *p = '\t';
      p++;

      for (i = 0; i < 16; i++)
      {
         p += sprintf(p,
                      "%s%02X",
                      (i != 0) ? ":" : "",
                      options->key[i]);
      }

      if (options->next != NULL)
      {
         *p = ',';
         p++;
      }

      *p = '\n';
      p++;
   }

   *p = '}';
   p++;
   *p = '\n';
   p++;

   memset(buffer, 0, 1024);
   ret = sprintf(buffer, "# Information for mesh\n\n");
   memcpy(p, buffer, strlen(buffer));
   p += ret;
   *p = '\n';
   p++;

   memset(buffer, 0, 1024);
   ret = sprintf(buffer, "preferred_parent			= %02x:%02x:%02x:%02x:%02x:%02x\n",
                 data->preferred_parent[0], data->preferred_parent[1],
                 data->preferred_parent[2], data->preferred_parent[3],
                 data->preferred_parent[4], data->preferred_parent[5]
                 );
   memcpy(p, buffer, strlen(buffer));
   p += ret;
   *p = '\n';
   p++;

   memset(buffer, 0, 1024);
   ret = sprintf(buffer, "signal_map				= %d,%d,%d,%d,%d,%d,%d,%d\n",
                 data->signal_map[0], data->signal_map[1],
                 data->signal_map[2], data->signal_map[3],
                 data->signal_map[4], data->signal_map[5],
                 data->signal_map[6], data->signal_map[7]
                 );
   memcpy(p, buffer, strlen(buffer));
   p += ret;
   *p = '\n';
   p++;

   memset(buffer, 0, 1024);
   ret = sprintf(buffer, "heartbeat_interval			= %d\n", data->heartbeat_interval);
   memcpy(p, buffer, strlen(buffer));
   p += ret;
   *p = '\n';
   p++;

   memset(buffer, 0, 1024);
   ret = sprintf(buffer, "heartbeat_miss_count		\t= %d\n", data->heartbeat_miss_count);
   memcpy(p, buffer, strlen(buffer));
   p += ret;
   *p = '\n';
   p++;

   memset(buffer, 0, 1024);
   ret = sprintf(buffer, "hop_cost				= %d\n", data->hop_cost);
   memcpy(p, buffer, strlen(buffer));
   p += ret;
   *p = '\n';
   p++;

   memset(buffer, 0, 1024);
   ret = sprintf(buffer, "max_allowable_hops			= %d\n", data->max_allowable_hops);
   memcpy(p, buffer, strlen(buffer));
   p += ret;
   *p = '\n';
   p++;

   memset(buffer, 0, 1024);
   ret = sprintf(buffer, "las_interval				= %d\n", data->las_interval);
   memcpy(p, buffer, strlen(buffer));
   p += ret;
   *p = '\n';
   p++;

   memset(buffer, 0, 1024);
   ret = sprintf(buffer, "change_resistance			= %d\n", data->change_resistance);
   memcpy(p, buffer, strlen(buffer));
   p += ret;
   *p = '\n';
   p++;

   memset(buffer, 0, 1024);
   ret = sprintf(buffer, "use_virt_if			= %d\n", data->use_virt_if);
   memcpy(p, buffer, strlen(buffer));
   p += ret;
   *p = '\n';
   p++;

   memset(buffer, 0, 1024);
   ret = sprintf(buffer, "failover_enabled         = %d\n", data->failover_enabled);
   memcpy(p, buffer, strlen(buffer));
   p += ret;
   *p = '\n';
   p++;

   memset(buffer, 0, 1024);
   ret = sprintf(buffer, "power_on_default           = %d\n", data->power_on_default);
   memcpy(p, buffer, strlen(buffer));
   p += ret;
   *p = '\n';
   p++;

   memset(buffer, 0, 1024);
   ret = sprintf(buffer,"signal_threshold		= %d\n", data->sig_threshold);
   memcpy(p, buffer, strlen(buffer));
   p += ret;
   *p = '\n';
   p++;

   if (data->server_ip_addr[0] != '\0')
   {
       memset(buffer, 0, 1024);
       ret = sprintf(buffer,"server_ip_addr		= %s\n", data->server_ip_addr);
       memcpy(p, buffer, strlen(buffer));
       p += ret;
       *p = '\n';
       p++;
   }

   if (data->mgmt_gw_addr[0] != '\0')
   {
       memset(buffer, 0, 1024);
       ret = sprintf(buffer, "mgmt_gw_addr 		= %s\n", data->mgmt_gw_addr);
       memcpy(p, buffer, strlen(buffer));
       p += ret;
       *p = '\n';
       p++;
   }
    if (data->mgmt_gw_certificates[0] != '\0')
    {
        memset(buffer, 0, 1024);
        ret = sprintf(buffer, "mgmt_gw_certificates       = %s\n", data->mgmt_gw_certificates);
        memcpy(p, buffer, strlen(buffer));
        p += ret;
        *p = '\n';
        p++;
    }
	memset(buffer, 0, 1024);
	ret = sprintf(buffer, "mgmt_gw_enable 		= %d\n", data->mgmt_gw_enable);
	memcpy(p, buffer, strlen(buffer));
	p += ret;
	*p = '\0';
	
   ret = p - ret_buffer;

   *al_conf_string = (char *)al_heap_alloc(AL_CONTEXT ret + 1 AL_HEAP_DEBUG_PARAM);
   strncpy(*al_conf_string, ret_buffer, ret);
   *al_conf_string_length = ret;

   al_heap_free(AL_CONTEXT ret_buffer);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);

   return 0;
}


int al_conf_put(AL_CONTEXT_PARAM_DECL unsigned int al_conf_handle)
{
   char *config_string;
   int  config_string_length;
   int  ret, handle;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   if (al_conf_handle <= 0)
   {
	  al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : al_conf_handle error : %s<%d>\n", __func__, __LINE__);
      return -1;
   }

   ret = al_conf_create_config_string_from_data(AL_CONTEXT al_conf_handle, &config_string_length, &config_string);
	/*#ifdef __KERNEL__
	  printk("DEBUG : %s\n", config_string);
#else
printf("DEBUG : %s\n", config_string);
#endif*/

   if (ret != 0)
   {
	  al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : al_conf_create_config_string_from_data() failed : %s<%d>\n", __func__, __LINE__);
      return ret;
   }

   handle = al_open_file(AL_CONTEXT AL_FILE_ID_MESHAP_CONFIG, AL_OPEN_FILE_MODE_WRITE);

   al_write_file(AL_CONTEXT handle, config_string, config_string_length);
   al_close_file(AL_CONTEXT handle);

   al_heap_free(AL_CONTEXT config_string);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);

   return 0;
}
#endif /* _MESHAP_AL_CONF_NO_SET_AND_PUT */
