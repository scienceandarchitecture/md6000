/********************************************************************************
* MeshDynamics
* --------------
* File     : sip_conf.c
* Comments : SIP configuration parser implementation.
* Created  : 1/16/2008
* Author   : Abhijit Ayarekar
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  2  |8/27/2008 | Added AD-HOC only option flag                   | Sriram |
* -----------------------------------------------------------------------------
* |  1  |8/26/2008 | Used common configuration parser routines       | Sriram |
* -----------------------------------------------------------------------------
* |  0  |1/16/2008 | Created.                                        |Abhijit |
* -----------------------------------------------------------------------------
********************************************************************************/

#ifdef __KERNEL__
#include <linux/string.h>
#include <linux/kernel.h>
#else
#include <stdio.h>
#include <strings.h>
#include <string.h>
#endif
#include "al.h"
#include "al_context.h"
#include "al_impl_context.h"
#include "sip_conf.h"
#include "conf_common.h"

#define _MAC_ADDRESS_LENGTH              6
#define _IP_ADDRESS_LENGTH               4

#define _MAX_TOKEN_SIZE                  256

#define _SUCCESS                         0
#define _ERR                             -1

#define _GET_TOKEN_SUCCESS               0
#define _GET_TOKEN_EOF                   -1
#define _GET_TOKEN_UNKNOWN               -2
#define _GET_TOKEN_ERROR                 -3

#define _SIP_CONF_TOKEN_NUMBER           1
#define _SIP_CONF_TOKEN_ALPHA_NUMERIC    2
#define _SIP_CONF_TOKEN_STRING           3
#define _SIP_CONF_TOKEN_HASH             4
#define _SIP_CONF_TOKEN_OPEN_BRACE       5
#define _SIP_CONF_TOKEN_CLOSE_BRACE      6
#define _SIP_CONF_TOKEN_OPEN_BRACKET     7
#define _SIP_CONF_TOKEN_CLOSE_BRACKET    8
#define _SIP_CONF_TOKEN_COMMA            9
#define _SIP_CONF_TOKEN_EQUALS           10
#define _SIP_CONF_TOKEN_COLON            11
#define _SIP_CONF_TOKEN_DOT              12
#define _SIP_CONF_TOKEN_ENABLED          13
#define _SIP_CONF_TOKEN_PORT             14
#define _SIP_CONF_TOKEN_SERVER_IP        15
#define _SIP_CONF_TOKEN_STA_LIST         16
#define _SIP_CONF_TOKEN_MAC              17
#define _SIP_CONF_TOKEN_EXTN             18
#define _SIP_CONF_TOKEN_ADHOC_ONLY       19

/**
 * The various states or modes of the parser.
 */

#define _PARSE_MODE_NONE                   -1
#define _PARSE_MODE_STA_INFO               1
#define _PARSE_MODE_STA_INFO_ITEM_START    2
#define _PARSE_MODE_STA_INFO_ITEM_END      3
#define _PARSE_MODE_ENABLED                4
#define _PARSE_MODE_PORT                   5
#define _PARSE_MODE_SERVER_IP              6
#define _PARSE_MODE_MAC                    7
#define _PARSE_MODE_EXTN                   8
#define _PARSE_MODE_ADHOC_ONLY             9

struct _sip_conf_data
{
   sip_conf_info_t sip_info;
   char            *parser_buffer;
   char            *pos;
   char            current_token[_MAX_TOKEN_SIZE + 1];
   char            *curent_token_pos;
   int             current_token_code;
   int             line_number;
   int             column_number;
   int             parse_mode;
   int             current_sta_item_index;
};

typedef struct _sip_conf_data   _sip_conf_data_t;

static struct
{
   int        code;
   const char *token;
}
_token_info[] =
{
   { _SIP_CONF_TOKEN_OPEN_BRACE,    "{"          },
   { _SIP_CONF_TOKEN_CLOSE_BRACE,   "}"          },
   { _SIP_CONF_TOKEN_OPEN_BRACKET,  "("          },
   { _SIP_CONF_TOKEN_CLOSE_BRACKET, ")"          },
   { _SIP_CONF_TOKEN_COMMA,         ","          },
   { _SIP_CONF_TOKEN_EQUALS,        "="          },
   { _SIP_CONF_TOKEN_COLON,         ":"          },
   { _SIP_CONF_TOKEN_DOT,           "."          },
   { _SIP_CONF_TOKEN_ENABLED,       "enabled"    },
   { _SIP_CONF_TOKEN_PORT,          "port"       },
   { _SIP_CONF_TOKEN_SERVER_IP,     "server_ip"  },
   { _SIP_CONF_TOKEN_STA_LIST,      "sta_list"   },
   { _SIP_CONF_TOKEN_MAC,           "mac"        },
   { _SIP_CONF_TOKEN_EXTN,          "extn"       },
   { _SIP_CONF_TOKEN_ADHOC_ONLY,    "adhoc_only" }
};

#define _MAP_TOKEN_CODE_TO_PARSE_MODE(tc, pm) \
case tc:                                      \
   conf_data->parse_mode = pm;                \
   break

#define _BEGIN_MAP_BLOCK(tc) \
   switch (tc) {
#define _END_MAP_BLOCK() \
   }

#define _MAP_TOKEN_CODES(tc)                                                            \
   _BEGIN_MAP_BLOCK(tc)                                                                 \
   _MAP_TOKEN_CODE_TO_PARSE_MODE(_SIP_CONF_TOKEN_ENABLED, _PARSE_MODE_ENABLED);         \
   _MAP_TOKEN_CODE_TO_PARSE_MODE(_SIP_CONF_TOKEN_PORT, _PARSE_MODE_PORT);               \
   _MAP_TOKEN_CODE_TO_PARSE_MODE(_SIP_CONF_TOKEN_SERVER_IP, _PARSE_MODE_SERVER_IP);     \
   _MAP_TOKEN_CODE_TO_PARSE_MODE(_SIP_CONF_TOKEN_STA_LIST, _PARSE_MODE_STA_INFO);       \
   _MAP_TOKEN_CODE_TO_PARSE_MODE(_SIP_CONF_TOKEN_MAC, _PARSE_MODE_MAC);                 \
   _MAP_TOKEN_CODE_TO_PARSE_MODE(_SIP_CONF_TOKEN_EXTN, _PARSE_MODE_EXTN);               \
   _MAP_TOKEN_CODE_TO_PARSE_MODE(_SIP_CONF_TOKEN_ADHOC_ONLY, _PARSE_MODE_ADHOC_ONLY);   \
default:                                                                                \
   al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "sip_conf.c(%d,%d): ignoring token %s\n", \
                conf_data->line_number,                                                 \
                conf_data->column_number,                                               \
                conf_data->current_token);                                              \
   _END_MAP_BLOCK()


static int _get_token(AL_CONTEXT_PARAM_DECL _sip_conf_data_t *conf_data)
{
   /**
    * Here we skip white spaces, comment lines, etc
    * and return then copy the token onto current_token
    * and update current_token_code. We also update the line_number
    * and column_number
    */

   int inside_comment;
   int token_found;
   int identifier_found;
   int i;

   inside_comment = 0;
   token_found    = 0;
   conf_data->curent_token_pos  = conf_data->current_token;
   identifier_found             = 0;
   *conf_data->curent_token_pos = 0;

   while (*conf_data->pos)
   {
      if (inside_comment)
      {
         if (*conf_data->pos == '\n')
         {
            ++conf_data->line_number;
            conf_data->column_number = 0;
            inside_comment           = 0;
         }
         goto label;
      }
      switch (*conf_data->pos)
      {
      case ' ':
      case '\t':
      case '\r':
         if (identifier_found)
         {
            token_found = 1;
         }
         break;

      case '\n':
         /** Skip but increment line_number */
         ++conf_data->line_number;
         conf_data->column_number = -1;
         if (identifier_found)
         {
            token_found = 1;
            conf_data->pos++;
         }
         break;

      case '#':
         /** From here to end of line skip everything */
         inside_comment = 1;
         break;

      case '=':
      case '(':
      case '{':
      case ',':
      case ')':
      case '}':
      case ':':
      case '.':
         if (!identifier_found)
         {
            *conf_data->curent_token_pos++ = *conf_data->pos++;
         }
         token_found = 1;
         break;

      default:
         *conf_data->curent_token_pos++ = *conf_data->pos;
         identifier_found = 1;
      }
      ++conf_data->column_number;
label:
      if (token_found)
      {
         break;
      }
      conf_data->pos++;
   }

   *conf_data->curent_token_pos = 0;

   if (*conf_data->current_token == 0)
   {
      return -1;
   }

   conf_data->current_token_code = 0;

   for (i = 0; i < sizeof(_token_info) / sizeof(*_token_info); i++)
   {
      if (!strcmp(conf_data->current_token, _token_info[i].token))
      {
         conf_data->current_token_code = _token_info[i].code;
         break;
      }
   }

   return 0;
}


static int _parse_int_attribute(AL_CONTEXT_PARAM_DECL _sip_conf_data_t *conf_data, int *value)
{
   int ret;

   ret = _get_token(AL_CONTEXT conf_data);
   if (ret == -1)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "sip_conf.c(%d,%d): unexpected eof file while looking for integer token data\n",
                   conf_data->line_number,
                   conf_data->column_number);
      goto label_ret;
   }

   ret = al_conf_atoi(conf_data->current_token, value);
   if (ret == -1)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "sip_conf.c(%d,%d): token %s was found instead of an integer\n",
                   conf_data->line_number,
                   conf_data->column_number,
                   conf_data->current_token);
      goto label_ret;
   }

label_ret:
   return ret;
}


static int _parse_ip_attribute(AL_CONTEXT_PARAM_DECL _sip_conf_data_t *conf_data, unsigned char *ip)
{
   int j;
   int ret;
   int val;

#define _PARSE_INTERMEDIATE_TOKEN(tok, tok_code)                                                                       \
   do {                                                                                                                \
      ret = _get_token(AL_CONTEXT conf_data);                                                                          \
      if (ret == -1) {                                                                                                 \
         al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "sip_conf.c(%d,%d): unexpected eof file while looking for '%c'\n", \
                      conf_data->line_number,                                                                          \
                      conf_data->column_number,                                                                        \
                      tok);                                                                                            \
         goto label_ret;                                                                                               \
      }                                                                                                                \
      if (conf_data->current_token_code != tok_code) {                                                                 \
         al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "sip_conf.c(%d,%d): expecting token '%c' but found %s\n",          \
                      conf_data->line_number,                                                                          \
                      conf_data->column_number,                                                                        \
                      tok,                                                                                             \
                      conf_data->current_token);                                                                       \
         goto label_ret;                                                                                               \
      }                                                                                                                \
   } while (0)

   for (j = 0; j < 4; j++)
   {
      ret = _get_token(AL_CONTEXT conf_data);
      if (ret == -1)
      {
         al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "sip_conf.c(%d,%d): unexpected eof file while looking for ip value\n",
                      conf_data->line_number,
                      conf_data->column_number
                      );
         goto label_ret;
      }

      ret   = al_conf_atoi(conf_data->current_token, &val);
      ip[j] = (unsigned char)val;

      if (ret == -1)
      {
         al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "sip_conf.c(%d,%d): token %s was found instead of an integer\n",
                      conf_data->line_number,
                      conf_data->column_number,
                      conf_data->current_token);
         goto label_ret;
      }
      if (j != 3)
      {
         _PARSE_INTERMEDIATE_TOKEN('.', _SIP_CONF_TOKEN_DOT);
      }
   }

#undef _PARSE_INTERMEDIATE_TOKEN

label_ret:
   return ret;
}


static int _parse_mac_attribute(AL_CONTEXT_PARAM_DECL _sip_conf_data_t *conf_data, unsigned char *mac)
{
   int j;
   int ret;
   int val;

#define _PARSE_INTERMEDIATE_TOKEN(tok, tok_code)                                                                       \
   do {                                                                                                                \
      ret = _get_token(AL_CONTEXT conf_data);                                                                          \
      if (ret == -1) {                                                                                                 \
         al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "sip_conf.c(%d,%d): unexpected eof file while looking for '%c'\n", \
                      conf_data->line_number,                                                                          \
                      conf_data->column_number,                                                                        \
                      tok);                                                                                            \
         goto label_ret;                                                                                               \
      }                                                                                                                \
      if (conf_data->current_token_code != tok_code) {                                                                 \
         al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "sip_conf.c(%d,%d): expecting token '%c' but found %s\n",          \
                      conf_data->line_number,                                                                          \
                      conf_data->column_number,                                                                        \
                      tok,                                                                                             \
                      conf_data->current_token);                                                                       \
         goto label_ret;                                                                                               \
      }                                                                                                                \
   } while (0)

   for (j = 0; j < 6; j++)
   {
      ret = _get_token(AL_CONTEXT conf_data);
      if (ret == -1)
      {
         al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "sip_conf.c(%d,%d): unexpected eof file while looking for mac value\n",
                      conf_data->line_number,
                      conf_data->column_number
                      );
         goto label_ret;
      }

      ret    = al_conf_hex_atoi(conf_data->current_token, &val);
      mac[j] = (unsigned char)val;

      if (ret == -1)
      {
         al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "sip_conf.c(%d,%d): token %s was found instead of an integer\n",
                      conf_data->line_number,
                      conf_data->column_number,
                      conf_data->current_token);
         goto label_ret;
      }
      if (j != 5)
      {
         _PARSE_INTERMEDIATE_TOKEN(':', _SIP_CONF_TOKEN_COLON);
      }
   }

#undef _PARSE_INTERMEDIATE_TOKEN

label_ret:
   return ret;
}


static int _parse_string_attribute(AL_CONTEXT_PARAM_DECL _sip_conf_data_t *conf_data, char *str)
{
   int ret;
   int token_length;

   ret = _get_token(AL_CONTEXT conf_data);
   if (ret == -1)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "sip_conf.c(%d,%d): unexpected eof file while looking for a string\n",
                   conf_data->line_number,
                   conf_data->column_number);
      goto label_ret;
   }

   token_length = strlen(conf_data->current_token);
   if (token_length >= SIP_CONF_MAX_STR_LEN)
   {
      token_length = SIP_CONF_MAX_STR_LEN - 1;
   }

   memcpy(str, conf_data->current_token, token_length);
   str[SIP_CONF_MAX_STR_LEN] = 0;

label_ret:
   return ret;
}


static int _parse_sip(AL_CONTEXT_PARAM_DECL _sip_conf_data_t *conf_data)
{
   int ret;
   int val;

#define _PARSE_INTERMEDIATE_TOKEN(tok, tok_code)                                                                       \
   do {                                                                                                                \
      ret = _get_token(AL_CONTEXT conf_data);                                                                          \
      if (ret == -1) {                                                                                                 \
         al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "sip_conf.c(%d,%d): unexpected eof file while looking for '%c'\n", \
                      conf_data->line_number,                                                                          \
                      conf_data->column_number,                                                                        \
                      tok);                                                                                            \
         goto label_ret;                                                                                               \
      }                                                                                                                \
      if (conf_data->current_token_code != tok_code) {                                                                 \
         al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "sip_conf.c(%d,%d): expecting token '%c' but found %s\n",          \
                      conf_data->line_number,                                                                          \
                      conf_data->column_number,                                                                        \
                      tok,                                                                                             \
                      conf_data->current_token);                                                                       \
         goto label_ret;                                                                                               \
      }                                                                                                                \
   } while (0)


   conf_data->parse_mode = _PARSE_MODE_NONE;

   while (1)
   {
      switch (conf_data->parse_mode)
      {
      case _PARSE_MODE_NONE:
         ret = _get_token(AL_CONTEXT conf_data);
         if (ret == _GET_TOKEN_EOF)
         {
            return 0;
         }
         else if ((ret == _GET_TOKEN_ERROR) || (ret == _GET_TOKEN_UNKNOWN))
         {
            goto label_ret;
         }
         _MAP_TOKEN_CODES(conf_data->current_token_code);
         break;

      case _PARSE_MODE_ADHOC_ONLY:
         _PARSE_INTERMEDIATE_TOKEN('=', _SIP_CONF_TOKEN_EQUALS);
         ret = _parse_int_attribute(AL_CONTEXT conf_data, &val);
         if (ret != 0)
         {
            goto label_ret;
         }

         conf_data->sip_info.adhoc_only = (unsigned char)val;
         conf_data->parse_mode          = _PARSE_MODE_NONE;
         break;

      case _PARSE_MODE_ENABLED:
         _PARSE_INTERMEDIATE_TOKEN('=', _SIP_CONF_TOKEN_EQUALS);
         ret = _parse_int_attribute(AL_CONTEXT conf_data, &val);
         if (ret != 0)
         {
            goto label_ret;
         }

         conf_data->sip_info.sip_enabled = (unsigned char)val;
         conf_data->parse_mode           = _PARSE_MODE_NONE;
         break;

      case _PARSE_MODE_PORT:
         _PARSE_INTERMEDIATE_TOKEN('=', _SIP_CONF_TOKEN_EQUALS);
         ret = _parse_int_attribute(AL_CONTEXT conf_data, &val);
         if (ret != 0)
         {
            goto label_ret;
         }

         conf_data->sip_info.port = (unsigned short)val;
         conf_data->parse_mode    = _PARSE_MODE_NONE;
         break;

      case _PARSE_MODE_SERVER_IP:
         _PARSE_INTERMEDIATE_TOKEN('=', _SIP_CONF_TOKEN_EQUALS);
         ret = _parse_ip_attribute(AL_CONTEXT conf_data, conf_data->sip_info.server_ip);
         if (ret != 0)
         {
            goto label_ret;
         }

         conf_data->parse_mode = _PARSE_MODE_NONE;
         break;

      case _PARSE_MODE_STA_INFO:

         _PARSE_INTERMEDIATE_TOKEN('(', _SIP_CONF_TOKEN_OPEN_BRACKET);

         ret = _parse_int_attribute(AL_CONTEXT conf_data, &conf_data->sip_info.sta_count);

         if ((ret != 0) || (conf_data->sip_info.sta_count < 0))
         {
            goto label_ret;
         }

         conf_data->sip_info.sta_info = (sip_conf_sta_info_t *)al_heap_alloc(AL_CONTEXT sizeof(sip_conf_sta_info_t) * conf_data->sip_info.sta_count);
         memset(conf_data->sip_info.sta_info, 0, sizeof(sip_conf_sta_info_t) * conf_data->sip_info.sta_count);

         _PARSE_INTERMEDIATE_TOKEN(')', _SIP_CONF_TOKEN_CLOSE_BRACKET);
         _PARSE_INTERMEDIATE_TOKEN('{', _SIP_CONF_TOKEN_OPEN_BRACE);

         conf_data->current_sta_item_index = 0;
         conf_data->parse_mode             = _PARSE_MODE_STA_INFO_ITEM_START;

         break;

      case _PARSE_MODE_STA_INFO_ITEM_START:

         if (conf_data->current_sta_item_index >= conf_data->sip_info.sta_count)
         {
            conf_data->parse_mode = _PARSE_MODE_STA_INFO_ITEM_END;
         }
         else
         {
            _PARSE_INTERMEDIATE_TOKEN('{', _SIP_CONF_TOKEN_OPEN_BRACE);
            conf_data->parse_mode = _PARSE_MODE_NONE;
         }

         break;

      case _PARSE_MODE_STA_INFO_ITEM_END:

         ret = _get_token(AL_CONTEXT conf_data);

         if (ret == _GET_TOKEN_EOF)
         {
            al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "sip_conf.c(%d,%d): unexpected eof file while looking for '}' or ','\n",
                         conf_data->line_number,
                         conf_data->column_number);
            goto label_ret;
         }
         else if ((ret == _GET_TOKEN_ERROR) || (ret == _GET_TOKEN_UNKNOWN))
         {
            al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "sip_conf.c(%d,%d): unexpected error while looking for '}' or ','\n",
                         conf_data->line_number,
                         conf_data->column_number);
            goto label_ret;
         }

         if (conf_data->current_token_code == _SIP_CONF_TOKEN_CLOSE_BRACE)
         {
            conf_data->parse_mode = _PARSE_MODE_NONE;
         }
         else if (conf_data->current_token_code == _SIP_CONF_TOKEN_COMMA)
         {
            conf_data->current_sta_item_index++;
            conf_data->parse_mode = _PARSE_MODE_STA_INFO_ITEM_START;
         }
         break;

      case _PARSE_MODE_MAC:

         _PARSE_INTERMEDIATE_TOKEN('=', _SIP_CONF_TOKEN_EQUALS);
         ret = _parse_mac_attribute(AL_CONTEXT conf_data, conf_data->sip_info.sta_info[conf_data->current_sta_item_index].mac);
         if (ret != 0)
         {
            goto label_ret;
         }

         ret = _get_token(AL_CONTEXT conf_data);
         if (ret == _GET_TOKEN_EOF)
         {
            al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "sip_conf.c(%d,%d): unexpected eof file while looking for '}' or ','\n",
                         conf_data->line_number,
                         conf_data->column_number);
            goto label_ret;
         }
         else if ((ret == _GET_TOKEN_ERROR) || (ret == _GET_TOKEN_UNKNOWN))
         {
            al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "sip_conf.c(%d,%d): unexpected error while looking for '}' or ','\n",
                         conf_data->line_number,
                         conf_data->column_number);
            goto label_ret;
         }

         if (conf_data->current_token_code == _SIP_CONF_TOKEN_CLOSE_BRACE)
         {
            conf_data->parse_mode = _PARSE_MODE_STA_INFO_ITEM_END;
         }
         else if (conf_data->current_token_code == _SIP_CONF_TOKEN_COMMA)
         {
            conf_data->parse_mode = _PARSE_MODE_NONE;
         }
         break;

      case _PARSE_MODE_EXTN:

         _PARSE_INTERMEDIATE_TOKEN('=', _SIP_CONF_TOKEN_EQUALS);
         ret = _parse_string_attribute(AL_CONTEXT conf_data, conf_data->sip_info.sta_info[conf_data->current_sta_item_index].extn);
         if (ret != 0)
         {
            goto label_ret;
         }

         ret = _get_token(AL_CONTEXT conf_data);
         if (ret == _GET_TOKEN_EOF)
         {
            al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "sip_conf.c(%d,%d): unexpected eof file while looking for '}' or ','\n",
                         conf_data->line_number,
                         conf_data->column_number);
            goto label_ret;
         }
         else if ((ret == _GET_TOKEN_ERROR) || (ret == _GET_TOKEN_UNKNOWN))
         {
            al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "sip_conf.c(%d,%d): unexpected error while looking for '}' or ','\n",
                         conf_data->line_number,
                         conf_data->column_number);
            goto label_ret;
         }

         if (conf_data->current_token_code == _SIP_CONF_TOKEN_CLOSE_BRACE)
         {
            conf_data->parse_mode = _PARSE_MODE_STA_INFO_ITEM_END;
         }
         else if (conf_data->current_token_code == _SIP_CONF_TOKEN_COMMA)
         {
            conf_data->parse_mode = _PARSE_MODE_NONE;
         }
         break;
      }
   }


label_ret:
   return -1;
}


sip_conf_handle_t sip_conf_parse(AL_CONTEXT_PARAM_DECL const char *sip_conf_string)
{
   _sip_conf_data_t *data;
   int              ret;

   data = (_sip_conf_data_t *)al_heap_alloc(AL_CONTEXT sizeof(_sip_conf_data_t)AL_HEAP_DEBUG_PARAM);

   memset(data, 0, sizeof(_sip_conf_data_t));

   data->parser_buffer = (char *)al_heap_alloc(AL_CONTEXT strlen(sip_conf_string) + 1  AL_HEAP_DEBUG_PARAM);
   strcpy(data->parser_buffer, sip_conf_string);

   data->pos         = data->parser_buffer;
   data->line_number = 1;

   ret = _parse_sip(AL_CONTEXT data);

   if (ret != 0)
   {
      al_heap_free(AL_CONTEXT data->parser_buffer);
      if (data->sip_info.sta_info != NULL)
      {
         al_heap_free(AL_CONTEXT data->sip_info.sta_info);
      }
      al_heap_free(AL_CONTEXT data);
      return 0;
   }

   return (sip_conf_handle_t)data;
}


void sip_conf_close(AL_CONTEXT_PARAM_DECL sip_conf_handle_t sip_conf_handle)
{
   _sip_conf_data_t *data;

   if (sip_conf_handle == NULL)
   {
      return;
   }

   data = (_sip_conf_data_t *)sip_conf_handle;

   al_heap_free(AL_CONTEXT data->parser_buffer);
   if (data->sip_info.sta_info != NULL)
   {
      al_heap_free(AL_CONTEXT data->sip_info.sta_info);
   }
   al_heap_free(AL_CONTEXT data);
}


int sip_conf_get_sip_enabled(AL_CONTEXT_PARAM_DECL sip_conf_handle_t sip_conf_handle)
{
   _sip_conf_data_t *data;

   if (sip_conf_handle == NULL)
   {
      return -1;
   }

   data = (_sip_conf_data_t *)sip_conf_handle;

   return data->sip_info.sip_enabled;
}


int sip_conf_get_adhoc_only(AL_CONTEXT_PARAM_DECL sip_conf_handle_t sip_conf_handle)
{
   _sip_conf_data_t *data;

   if (sip_conf_handle == NULL)
   {
      return -1;
   }

   data = (_sip_conf_data_t *)sip_conf_handle;

   return data->sip_info.adhoc_only;
}


int sip_conf_get_sip_port(AL_CONTEXT_PARAM_DECL sip_conf_handle_t sip_conf_handle)
{
   _sip_conf_data_t *data;

   if (sip_conf_handle == NULL)
   {
      return -1;
   }

   data = (_sip_conf_data_t *)sip_conf_handle;

   return data->sip_info.port;
}


int sip_conf_get_server_ip(AL_CONTEXT_PARAM_DECL sip_conf_handle_t sip_conf_handle, unsigned char *out_ip)
{
   _sip_conf_data_t *data;

   if (sip_conf_handle == NULL)
   {
      return -1;
   }

   data = (_sip_conf_data_t *)sip_conf_handle;

   memcpy(out_ip, data->sip_info.server_ip, 4);

   return 0;
}


int sip_conf_get_sta_count(AL_CONTEXT_PARAM_DECL sip_conf_handle_t sip_conf_handle)
{
   _sip_conf_data_t *data;

   if (sip_conf_handle == NULL)
   {
      return -1;
   }

   data = (_sip_conf_data_t *)sip_conf_handle;

   return data->sip_info.sta_count;
}


int sip_conf_get_sta_info(AL_CONTEXT_PARAM_DECL sip_conf_handle_t sip_conf_handle, int index, sip_conf_sta_info_t *info)
{
   _sip_conf_data_t *data;

   if (sip_conf_handle == NULL)
   {
      return -1;
   }

   data = (_sip_conf_data_t *)sip_conf_handle;

   if ((index < 0) || (index >= data->sip_info.sta_count))
   {
      return -2;
   }

   memcpy(info, &data->sip_info.sta_info[index], sizeof(sip_conf_sta_info_t));

   return 0;
}


#ifndef _MESHAP_SIP_CONF_NO_SET_AND_PUT

int sip_conf_set_sip_enabled(AL_CONTEXT_PARAM_DECL sip_conf_handle_t sip_conf_handle, int enabled)
{
   _sip_conf_data_t *data;

   if (sip_conf_handle == NULL)
   {
      return -1;
   }

   data = (_sip_conf_data_t *)sip_conf_handle;

   data->sip_info.sip_enabled = enabled;

   return 0;
}


int sip_conf_set_adhoc_only(AL_CONTEXT_PARAM_DECL sip_conf_handle_t sip_conf_handle, int enabled)
{
   _sip_conf_data_t *data;

   if (sip_conf_handle == NULL)
   {
      return -1;
   }

   data = (_sip_conf_data_t *)sip_conf_handle;

   data->sip_info.adhoc_only = enabled;

   return 0;
}


int sip_conf_set_server_ip(AL_CONTEXT_PARAM_DECL sip_conf_handle_t sip_conf_handle, unsigned char *in_ip)
{
   _sip_conf_data_t *data;

   if (sip_conf_handle == NULL)
   {
      return -1;
   }

   data = (_sip_conf_data_t *)sip_conf_handle;

   memcpy(data->sip_info.server_ip, in_ip, 4);

   return 0;
}


int sip_conf_set_sip_port(AL_CONTEXT_PARAM_DECL sip_conf_handle_t sip_conf_handle, int port)
{
   _sip_conf_data_t *data;

   if (sip_conf_handle == NULL)
   {
      return -1;
   }

   data = (_sip_conf_data_t *)sip_conf_handle;

   data->sip_info.port = port;

   return 0;
}


int sip_conf_set_sta_count(AL_CONTEXT_PARAM_DECL sip_conf_handle_t sip_conf_handle, int count)
{
   _sip_conf_data_t *data;

   if (sip_conf_handle == NULL)
   {
      return -1;
   }

   data = (_sip_conf_data_t *)sip_conf_handle;

   if (data->sip_info.sta_info != NULL)
   {
      al_heap_free(AL_CONTEXT data->sip_info.sta_info);
   }

   data->sip_info.sta_info  = (sip_conf_sta_info_t *)al_heap_alloc(AL_CONTEXT sizeof(sip_conf_sta_info_t) * count);
   data->sip_info.sta_count = count;

   return 0;
}


int sip_conf_set_sta_info(AL_CONTEXT_PARAM_DECL sip_conf_handle_t sip_conf_handle, int index, sip_conf_sta_info_t *info)
{
   _sip_conf_data_t *data;

   if (sip_conf_handle == NULL)
   {
      return -1;
   }

   data = (_sip_conf_data_t *)sip_conf_handle;

   if ((index < 0) || (index >= data->sip_info.sta_count))
   {
      return -2;
   }

   memcpy(&data->sip_info.sta_info[index], info, sizeof(sip_conf_sta_info_t));

   return 0;
}


int sip_conf_put(AL_CONTEXT_PARAM_DECL sip_conf_handle_t sip_conf_handle)
{
   char *config_string;
   int  config_string_length;
   int  ret, handle;

   if (sip_conf_handle == NULL)
   {
      return -1;
   }

   ret = sip_conf_create_config_string_from_data(AL_CONTEXT sip_conf_handle, &config_string_length, &config_string);

   if (ret != 0)
   {
      return ret;
   }

   handle = al_open_file(AL_CONTEXT AL_FILE_ID_SIP_CONFIG, AL_OPEN_FILE_MODE_WRITE);
   al_write_file(AL_CONTEXT handle, config_string, config_string_length);
   al_close_file(AL_CONTEXT handle);

   al_heap_free(AL_CONTEXT config_string);

   return 0;
}


int sip_conf_create_config_string_from_data(AL_CONTEXT_PARAM_DECL sip_conf_handle_t sip_conf_handle, int *config_string_length, char **sip_conf_string)
{
   _sip_conf_data_t    *data;
   sip_conf_sta_info_t *info;
   char                *p;
   char                buffer[1024];
   char                *buffer_out;
   int                 ret;
   int                 i;

   if (sip_conf_handle == NULL)
   {
      return -1;
   }

   data = (_sip_conf_data_t *)sip_conf_handle;

   buffer_out = (char *)al_heap_alloc(AL_CONTEXT 64 * 1024 AL_HEAP_DEBUG_PARAM);
   memset(buffer_out, 0, 10240);

   memset(buffer, 0, 1024);

   p = buffer_out;

   memset(buffer, 0, 1024);
   ret = sprintf(buffer, "enabled = %d\n", data->sip_info.sip_enabled);
   memcpy(p, buffer, strlen(buffer));
   p += ret;
   *p = '\n';
   p += 1;

   memset(buffer, 0, 1024);
   ret = sprintf(buffer, "adhoc_only = %d\n", data->sip_info.adhoc_only);
   memcpy(p, buffer, strlen(buffer));
   p += ret;
   *p = '\n';
   p += 1;

   memset(buffer, 0, 1024);
   ret = sprintf(buffer, "port = %d\n", data->sip_info.port);
   memcpy(p, buffer, strlen(buffer));
   p += ret;
   *p = '\n';
   p += 1;

   memset(buffer, 0, 1024);

   ret = sprintf(buffer, "server_ip = %d.%d.%d.%d\n", data->sip_info.server_ip[0],
                 data->sip_info.server_ip[1],
                 data->sip_info.server_ip[2],
                 data->sip_info.server_ip[3]);
   memcpy(p, buffer, strlen(buffer));
   p += ret;
   *p = '\n';
   p += 1;

   memset(buffer, 0, 1024);
   ret = sprintf(buffer, "sta_list(%d) {", data->sip_info.sta_count);
   memcpy(p, buffer, strlen(buffer));
   p += ret;
   *p = '\n';
   p += 1;

   for (i = 0; i < data->sip_info.sta_count; i++)
   {
      *p = '\t';
      p += 1;
      *p = '{';
      p += 1;
      *p = '\n';
      p += 1;

      info = &data->sip_info.sta_info[i];

      memset(buffer, 0, 1024);
      ret = sprintf(buffer, "\t\tmac = %02x:%02x:%02x:%02x:%02x:%02x,",
                    info->mac[0], info->mac[1],
                    info->mac[2], info->mac[3],
                    info->mac[4], info->mac[5]);
      memcpy(p, buffer, strlen(buffer));
      p += ret;
      *p = '\n';
      p += 1;

      memset(buffer, 0, 1024);
      ret = sprintf(buffer, "\t\textn = %s", info->extn);
      memcpy(p, buffer, strlen(buffer));
      p += ret;
      *p = '\n';
      p += 1;

      *p = '\t';
      p += 1;
      *p = '}';
      p += 1;

      if (i != data->sip_info.sta_count - 1)
      {
         *p = ',';
         p += 1;
      }

      *p = '\n';
      p += 1;
   }

   *p = '}';
   p += 1;
   *p = 0;
   p += 1;

   ret = p - buffer_out;
   *sip_conf_string = (char *)al_heap_alloc(AL_CONTEXT ret AL_HEAP_DEBUG_PARAM);
   memset(*sip_conf_string, 0, ret);

   strcpy(*sip_conf_string, buffer_out);
   *config_string_length = ret;

   al_heap_free(AL_CONTEXT buffer_out);

   return 0;
}
#endif /* _MESHAP_SIP_CONF_NO_SET_AND_PUT */
