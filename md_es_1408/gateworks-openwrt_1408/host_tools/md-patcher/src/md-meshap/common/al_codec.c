/********************************************************************************
* MeshDynamics
* --------------
* File     : al_codec.c
* Comments : Encoder/Decoder routines
* Created  : 9/26/2004
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* ------------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author  |
* ------------------------------------------------------------------------------
* |  6  |8/22/2008 | Changes for FIPS                                |Abhijit |
* -----------------------------------------------------------------------------
* |  5  |11/16/2006| Misc changes                                    | Sriram  |
* ------------------------------------------------------------------------------
* |  4  |10/29/2004| bug fixed - AES KEY SIZE                        | Anand   |
* ------------------------------------------------------------------------------
* |  3  |10/28/2004| Com. Errors removed for Rover                   | Anand   |
* ------------------------------------------------------------------------------
* |  2  |10/1/2004 | Added _encode_key /_decode_key		          | Prachiti|
* ------------------------------------------------------------------------------
* |  1  |9/30/2004 | AES implementation _encrypt/_decrypt            | Prachiti|
* ------------------------------------------------------------------------------
* |  0  |9/26/2004 | Created.                                        | Sriram  |
* ------------------------------------------------------------------------------
********************************************************************************/

#include <linux/string.h>
#include "al.h"
#include "al_context.h"
#include "al_print_log.h"
#include "codec_sys.h"
#include "codec.h"


int al_encrypt(AL_CONTEXT_PARAM_DECL const unsigned char *input, int input_length, const unsigned char *key, int key_length, unsigned char *output, int enc_dec_method)
{
   return encrypt_helper((unsigned char *)input, input_length, key, key_length, output, enc_dec_method);
}


int al_decrypt(AL_CONTEXT_PARAM_DECL const unsigned char *input, int input_length, const unsigned char *key, int key_length, unsigned char *output, int enc_dec_method)
{
   return decrypt_helper((unsigned char *)input, input_length, key, key_length, output, enc_dec_method);
}


int al_encode_key(AL_CONTEXT_PARAM_DECL unsigned char *text_key, int text_key_len, unsigned char *mac_address, unsigned char *output)
{
   return _encode_key((unsigned char *)text_key, text_key_len, (unsigned char *)mac_address, output);
}


int al_decode_key(AL_CONTEXT_PARAM_DECL unsigned char *buffer, unsigned char *mac_address, unsigned char *output)
{
   return _decode_key((unsigned char *)buffer, (unsigned char *)mac_address, output);
}


void *codec_alloc(unsigned int size)
{
#ifdef _AL_ROVER_
   return al_heap_alloc(NULL, size AL_HEAP_DEBUG_PARAM);

#else
   return al_heap_alloc(size AL_HEAP_DEBUG_PARAM);
#endif
}


void codec_free(void *memblock)
{
#ifdef _AL_ROVER_
   al_heap_free(NULL, memblock);
#else
   al_heap_free(memblock);
#endif
}


unsigned char *codec_get_gen_buffer(void)
{
   return al_get_gen_2KB_buffer();
}


void codec_release_gen_buffer(unsigned char *buffer)
{
   al_release_gen_2KB_buffer(buffer);
}


int codec_print_log(const char *format, ...)
{
#ifndef _AL_ROVER_
   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, format);
#endif
   return 0;
}
