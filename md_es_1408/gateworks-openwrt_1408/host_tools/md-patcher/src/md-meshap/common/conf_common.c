/********************************************************************************
* MeshDynamics
* --------------
* File     : conf_common.c
* Comments : Common Configuration Parser Routines
* Created  : 8/26/2008
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  0  |8/26/2008 | Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

#include <linux/string.h>

int al_conf_atoi(const char *string, int *value)
{
   int  c;              /* current char */
   long total;          /* current total */
   int  sign;           /* if '-', then negative, otherwise positive */
   int  err;
   char *p;
   char buf[32];

#define _isspace(c)    (c == '\t' || c == '\r' || c == '\n' || c == '\f' || c == ' ')
#define _isdigit(c)    (c >= 48 && c <= 48 + 9)

   err = 0;

   /* skip whitespace */

   while (_isspace((int)(unsigned char)*string))
   {
      ++string;
   }

   strcpy(buf, string);

   p = buf;
   while (*p != 0)
   {
      p++;
   }
   p--;
   while (_isspace(*p))
   {
      *p = 0;
      p--;
   }

   p = buf;

   c    = (int)(unsigned char)*p++;
   sign = c;            /* save sign indication */
   if ((c == '-') || (c == '+'))
   {
      c = (int)(unsigned char)*p++;      /* skip sign */
   }
   total = 0;

   while (_isdigit(c))
   {
      total = 10 * total + (c - '0');    /* accumulate digit */
      c     = (int)(unsigned char)*p++;  /* get next char */
   }

   if (c != 0)
   {
      err = -1;
   }

   if (sign == '-')
   {
      *value = -total;
   }
   else
   {
      *value = total;     /* return result, negated if necessary */
   }
   return err;

#undef _isspace
#undef _isdigit
}


int al_conf_hex_atoi(const char *string, int *value)
{
   int  c;              /* current char */
   long total;          /* current total */
   int  err;
   char *p;
   char buf[32];

#define _ishexalpha(c)             ((c >= 'A' && c <= 'F') || (c >= 'a' && c <= 'f'))
#define _isspace(c)                (c == '\t' || c == '\r' || c == '\n' || c == '\f' || c == ' ')
#define _isdigit(c)                (c >= 48 && c <= 48 + 9)
#define _ishexdigit(c)             (_isdigit(c) || _ishexalpha(c))
#define _get_hex_alpha_value(c)    ((c >= 'A' && c <= 'F') ? (10 + c - 'A') : (10 + c - 'a'))
#define _get_hex_value(c)          (_isdigit(c) ? (c - '0') : _get_hex_alpha_value(c))

   err = 0;

   /* skip whitespace */

   while (_isspace((int)(unsigned char)*string))
   {
      ++string;
   }

   strcpy(buf, string);

   p = buf;
   while (*p != 0)
   {
      p++;
   }
   p--;
   while (_isspace(*p))
   {
      *p = 0;
      p--;
   }

   p     = buf;
   c     = (int)(unsigned char)*p++;
   total = 0;

   while (_ishexdigit(c))
   {
      total = 16 * total + _get_hex_value(c);       /* accumulate digit */
      c     = (int)(unsigned char)*p++;             /* get next char */
   }

   if (c != 0)
   {
      err = -1;
   }

   *value = total;        /* return result, negated if necessary */

   return err;

#undef _isspace
#undef _isdigit
#undef _ishexalpha
#undef _ishexdigit
#undef _get_hex_alpha_value
#undef _get_hex_value
}
