/********************************************************************************
* MeshDynamics
* --------------
* File     : 11e_conf.c
* Comments : 11e Config Parser
* Created  : 1/17/2006
* Author   : Bindu Parakala
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  3  |8/26/2008 | Used common configuration parser routines       | Sriram |
* -----------------------------------------------------------------------------
* |  2  |11/14/2006| Changes for heap debug                          | Sriram |
* -----------------------------------------------------------------------------
* |  1  |2/27/2006 | Added \n when writing configuration             | Sriram |
* -----------------------------------------------------------------------------
* |  0  |1/17/2006 | Created                                         | Bindu  |
* -----------------------------------------------------------------------------
********************************************************************************/

#ifdef __KERNEL__
#include <linux/string.h>
#include <linux/kernel.h>
#else
#include <stdio.h>
#include <strings.h>
#include <string.h>
#endif
#include "al.h"
#include "al_context.h"
#include "al_impl_context.h"
#include "al_802_11.h"
#include "dot11e_conf.h"
#include "conf_common.h"

#define _CATEGORY_TYPE_AC_BK      0
#define _CATEGORY_TYPE_AC_BE      1
#define _CATEGORY_TYPE_AC_VI      2
#define _CATEGORY_TYPE_AC_VO      3

#define _MAX_TOKEN_SIZE           256

#define _SUCCESS                  0
#define _ERR                      -1

#define _GET_TOKEN_SUCCESS        0
#define _GET_TOKEN_EOF            -1
#define _GET_TOKEN_UNKNOWN        -2
#define _GET_TOKEN_ERROR          -3

#define _TOKEN_CATEGORY_INFO      1
#define _TOKEN_AC_BK              2
#define _TOKEN_AC_BE              3
#define _TOKEN_AC_VI              4
#define _TOKEN_AC_VO              5
#define _TOKEN_ACWMIN             6
#define _TOKEN_ACWMAX             7
#define _TOKEN_AIFSN              8
#define _TOKEN_DISABLE_BACKOFF    9
#define _TOKEN_BURST_TIME         10
#define _TOKEN_EQUALS             11
#define _TOKEN_COMMA              12
#define _TOKEN_NUMBER             13
#define _TOKEN_STRING             14
#define _TOKEN_HASH               15
#define _TOKEN_OPEN_BRACE         16
#define _TOKEN_CLOSE_BRACE        17
#define _TOKEN_OPEN_BRACK         18
#define _TOKEN_CLOSE_BRACK        19

/**
 * The various states or modes of the parser.
 */

#define _PARSE_MODE_NONE                   -1
#define _PARSE_MODE_CATEGORY_INFO          1
#define _PARSE_MODE_CATEGORY_INFO_ITEM     2
#define _PARSE_MODE_CATEGORY_INFO_AC_BK    3
#define _PARSE_MODE_CATEGORY_INFO_AC_BE    4
#define _PARSE_MODE_CATEGORY_INFO_AC_VI    5
#define _PARSE_MODE_CATEGORY_INFO_AC_VO    6


struct _token_info
{
   int        token_code;
   const char *token;
   char       token_data[_MAX_TOKEN_SIZE];
};

typedef struct _token_info   _token_info_t;

struct _tokenizer_state
{
   char       token_data[_MAX_TOKEN_SIZE];
   const char *current_pos;
   char       *error;
   int        current_line_number;
   int        current_col_number;
};

typedef struct _tokenizer_state   _tokenizer_state_t;

//static const char arr[] = {"", ""};
static const _token_info_t _tokens[] =
{
   { _TOKEN_CATEGORY_INFO,   "category_info",   "" },
   { _TOKEN_AC_BK,           "ac_bk",           "" },
   { _TOKEN_AC_BE,           "ac_be",           "" },
   { _TOKEN_AC_VI,           "ac_vi",           "" },
   { _TOKEN_AC_VO,           "ac_vo",           "" },
   { _TOKEN_ACWMIN,          "acwmin",          "" },
   { _TOKEN_ACWMAX,          "acwmax",          "" },
   { _TOKEN_AIFSN,           "aifsn",           "" },
   { _TOKEN_DISABLE_BACKOFF, "disable_backoff", "" },
   { _TOKEN_BURST_TIME,      "burst_time",      "" },
   { _TOKEN_EQUALS,          "=",               "" },
   { _TOKEN_COMMA,           ",",               "" },
   { _TOKEN_OPEN_BRACE,      "{",               "" },
   { _TOKEN_CLOSE_BRACE,     "}",               "" },
   { _TOKEN_OPEN_BRACK,      "(",               "" },
   { _TOKEN_CLOSE_BRACK,     ")",               "" }
};

#define _MAP_TOKEN_CODE_TO_PARSE_MODE(tc, pm) \
case tc:                                      \
   conf_out->parse_mode = pm; break

/*#define _MAP_TOKEN_CODE_TO_PARSE_MODE(tc,pm) \
 * case tc: \
 *      conf_out->parse_mode = pm; \
 *      break
 */
#define _BEGIN_MAP_BLOCK(tc)    switch (tc) {
#define _END_MAP_BLOCK()        }

#define _MAP_TOKEN_CODES(tc)                                                       \
   _BEGIN_MAP_BLOCK(tc)                                                            \
   _MAP_TOKEN_CODE_TO_PARSE_MODE(_TOKEN_CATEGORY_INFO, _PARSE_MODE_CATEGORY_INFO); \
   _MAP_TOKEN_CODE_TO_PARSE_MODE(_TOKEN_AC_BK, _PARSE_MODE_CATEGORY_INFO_AC_BK);   \
   _MAP_TOKEN_CODE_TO_PARSE_MODE(_TOKEN_AC_BE, _PARSE_MODE_CATEGORY_INFO_AC_BE);   \
   _MAP_TOKEN_CODE_TO_PARSE_MODE(_TOKEN_AC_VI, _PARSE_MODE_CATEGORY_INFO_AC_VI);   \
   _MAP_TOKEN_CODE_TO_PARSE_MODE(_TOKEN_AC_VO, _PARSE_MODE_CATEGORY_INFO_AC_VO);   \
   _END_MAP_BLOCK()

static int _is_token_integer(char *data, int length)
{
   char *ptr;
   int  i;

#define _isdigit(c)    (c >= 48 && c <= 48 + 9)

   ptr = data;
   for (i = 0; i < length; i++)
   {
      char c = *ptr;
      if (!(_isdigit(c)))
      {
         return _ERR;
      }
   }

   return _SUCCESS;

#undef _isdigit
}


static int _get_token(_tokenizer_state_t *state, _token_info_t *token_out)
{
   const char    *p;
   const char    *current_marker;
   unsigned char leading_white_space;
   int           token_data_pos;
   int           i;

   p = state->current_pos;
   current_marker      = p;
   leading_white_space = 1;
   token_data_pos      = 0;

   while (*p != 0)
   {
      switch (*p)
      {
      case ' ':
      case '\t':
      case '\r':
      case '\n':
         if (*p == '\n')
         {
            ++state->current_line_number;
         }
         if (leading_white_space)
         {
            ++p;
            ++state->current_col_number;
            continue;
         }
         else
         {
            goto _token_found;
         }
         break;

      case '=':
      case '(':
      case ')':
      case ',':
         leading_white_space = 0;
         if (token_data_pos == 0)
         {
            state->token_data[token_data_pos++] = *p++;
            goto _token_found;
         }
         else
         {
            goto _token_found;
         }
         break;

      default:
         leading_white_space = 0;
         state->token_data[token_data_pos++] = *p++;
      }

      ++state->current_col_number;
   }

   return _GET_TOKEN_EOF;

_token_found:

   state->token_data[token_data_pos++] = 0;

   for (i = 0; i < sizeof(_tokens) / sizeof(*_tokens); i++)
   {
      if (!(strcmp(_tokens[i].token, state->token_data)))
      {
         memcpy(token_out, &_tokens[i], sizeof(_token_info_t));
         state->current_pos = p;
         return _GET_TOKEN_SUCCESS;
      }
   }

   memcpy((char *)token_out->token_data, state->token_data, token_data_pos - 1);
   if (_is_token_integer(token_out->token_data, token_data_pos - 1) == _SUCCESS)
   {
      token_out->token_code = _TOKEN_NUMBER;
   }
   else
   {
      return _GET_TOKEN_ERROR;
   }

   state->current_pos = p;

   return _GET_TOKEN_SUCCESS;
}


static int _parse_integer(_tokenizer_state_t *state, _token_info_t *token, unsigned int *value_out)
{
   int value;

   if (_get_token(state, token) != _GET_TOKEN_SUCCESS)
   {
      return _GET_TOKEN_ERROR;
   }

   if (token->token_code != _TOKEN_NUMBER)
   {
      return _GET_TOKEN_ERROR;
   }

   if (al_conf_atoi(token->token_data, &value) != 0)
   {
      return _GET_TOKEN_ERROR;
   }

   *value_out = value;

   return _GET_TOKEN_SUCCESS;
}


int _parse(const char *buffer, char *error_out, dot11e_conf_category_t *conf_out)
{
   _tokenizer_state_t state;
   _token_info_t      token;
   unsigned char      ip[4];
   unsigned int       value;
   int                i;
   int                ret;

#define _PARSE_INTERMEDIATE_TOKEN(tok, tok_code)                      \
   do {                                                               \
      ret = _get_token(&state, &token);                               \
      if ((ret == _GET_TOKEN_UNKNOWN) || (ret == _GET_TOKEN_ERROR)) { \
         goto label_ret;                                              \
      }                                                               \
      if (token.token_code != tok_code) {                             \
         goto label_ret;                                              \
      }                                                               \
   } while (0)


   memset(ip, 0, 4);
   memset(&state, 0, sizeof(_tokenizer_state_t));
   memset(&token, 0, sizeof(_token_info_t));
   memset(conf_out, 0, sizeof(dot11e_conf_category_t));

   state.current_pos         = buffer;
   state.error               = error_out;
   state.current_line_number = 1;
   state.current_col_number  = 1;

   conf_out->parse_mode = _PARSE_MODE_NONE;

   while (1)
   {
      switch (conf_out->parse_mode)
      {
      case _PARSE_MODE_NONE:
         ret = _get_token(&state, &token);

         if (ret == _GET_TOKEN_EOF)
         {
            return 0;
         }
         else if ((ret == _GET_TOKEN_ERROR) || (ret == _GET_TOKEN_UNKNOWN))
         {
            goto label_ret;
         }

         _MAP_TOKEN_CODES(token.token_code);

         break;

      case _PARSE_MODE_CATEGORY_INFO:
         _PARSE_INTERMEDIATE_TOKEN('(', _TOKEN_OPEN_BRACK);

         /* get total no of categories */
         if (_parse_integer(&state, &token, &value) != _GET_TOKEN_SUCCESS)
         {
            goto label_ret;
         }

         conf_out->count         = value;
         conf_out->category_info = (dot11e_conf_category_details_t *)al_heap_alloc(sizeof(dot11e_conf_category_details_t) * conf_out->count AL_HEAP_DEBUG_PARAM);
         memset(conf_out->category_info, 0, sizeof(dot11e_conf_category_details_t) * conf_out->count);

         _PARSE_INTERMEDIATE_TOKEN(')', _TOKEN_CLOSE_BRACK);
         _PARSE_INTERMEDIATE_TOKEN('{', _TOKEN_OPEN_BRACE);

         conf_out->parse_mode = _PARSE_MODE_CATEGORY_INFO_ITEM;

         break;

      case _PARSE_MODE_CATEGORY_INFO_ITEM:
         /** First get the Category name */
         for (i = 0; i < conf_out->count; i++)
         {
            /** Read Category name */
            ret = _get_token(&state, &token);
            if (ret == -1)
            {
               goto label_ret;
            }

            switch (token.token_code)
            {
            case _TOKEN_AC_BK:
            case _TOKEN_AC_BE:
            case _TOKEN_AC_VI:
            case _TOKEN_AC_VO:

               if (token.token_code == _TOKEN_AC_BK)
               {
                  conf_out->category_info[i].category = _CATEGORY_TYPE_AC_BK;
               }
               else if (token.token_code == _TOKEN_AC_BE)
               {
                  conf_out->category_info[i].category = _CATEGORY_TYPE_AC_BE;
               }
               else if (token.token_code == _TOKEN_AC_VI)
               {
                  conf_out->category_info[i].category = _CATEGORY_TYPE_AC_VI;
               }
               else if (token.token_code == _TOKEN_AC_VO)
               {
                  conf_out->category_info[i].category = _CATEGORY_TYPE_AC_VO;
               }

               _PARSE_INTERMEDIATE_TOKEN('{', _TOKEN_OPEN_BRACE);

               while (1)
               {
                  ret = _get_token(&state, &token);
                  if (ret == -1)
                  {
                     goto label_ret;
                  }

                  switch (token.token_code)
                  {
                  case _TOKEN_ACWMIN:
                     _PARSE_INTERMEDIATE_TOKEN('=', _TOKEN_EQUALS);

                     if (_parse_integer(&state, &token, &value) != _GET_TOKEN_SUCCESS)
                     {
                        goto label_ret;
                     }

                     conf_out->category_info[i].acwmin = value;

                     break;

                  case _TOKEN_ACWMAX:
                     _PARSE_INTERMEDIATE_TOKEN('=', _TOKEN_EQUALS);

                     if (_parse_integer(&state, &token, &value) != _GET_TOKEN_SUCCESS)
                     {
                        goto label_ret;
                     }

                     conf_out->category_info[i].acwmax = value;

                     break;

                  case _TOKEN_AIFSN:
                     _PARSE_INTERMEDIATE_TOKEN('=', _TOKEN_EQUALS);

                     if (_parse_integer(&state, &token, &value) != _GET_TOKEN_SUCCESS)
                     {
                        goto label_ret;
                     }

                     conf_out->category_info[i].aifsn = value;

                     break;

                  case _TOKEN_DISABLE_BACKOFF:
                     _PARSE_INTERMEDIATE_TOKEN('=', _TOKEN_EQUALS);

                     if (_parse_integer(&state, &token, &value) != _GET_TOKEN_SUCCESS)
                     {
                        goto label_ret;
                     }

                     conf_out->category_info[i].disable_backoff = value;

                     break;

                  case _TOKEN_BURST_TIME:
                     _PARSE_INTERMEDIATE_TOKEN('=', _TOKEN_EQUALS);

                     if (_parse_integer(&state, &token, &value) != _GET_TOKEN_SUCCESS)
                     {
                        goto label_ret;
                     }

                     conf_out->category_info[i].burst_time = value;

                     break;
                  }                               // switch

                  /** Now we expect either a COMMA or a CLOSE BRACE */
                  ret = _get_token(&state, &token);
                  if (ret == -1)
                  {
                     goto label_ret;
                  }
                  if (token.token_code == _TOKEN_COMMA)
                  {
                     continue;
                  }
                  else if (token.token_code == _TOKEN_CLOSE_BRACE)
                  {
                     break;
                  }
                  else
                  {
                     goto label_ret;
                  }
               }                          //while
            }
         }

         _PARSE_INTERMEDIATE_TOKEN('}', _TOKEN_CLOSE_BRACE);
         conf_out->parse_mode = _PARSE_MODE_NONE;
      }
   }

label_ret:
   return -1;

#undef _PARSE_INTERMEDIATE_TOKEN
}


int dot11e_conf_parse(AL_CONTEXT_PARAM_DECL const char *dot11e_conf_string)
{
   dot11e_conf_category_t *data;
   int  ret;
   char *buffer;
   char err_out;

   data = (dot11e_conf_category_t *)al_heap_alloc(AL_CONTEXT sizeof(dot11e_conf_category_t)AL_HEAP_DEBUG_PARAM);
   memset(data, 0, sizeof(dot11e_conf_category_t));

   buffer = (char *)al_heap_alloc(AL_CONTEXT strlen(dot11e_conf_string) + 1  AL_HEAP_DEBUG_PARAM);
   strcpy(buffer, dot11e_conf_string);

   ret = _parse(buffer, &err_out, data);


   if (ret != 0)
   {
      al_heap_free(AL_CONTEXT buffer);
      if (data->category_info != NULL)
      {
         al_heap_free(AL_CONTEXT data->category_info);
      }
      al_heap_free(AL_CONTEXT data);
      return 0;
   }

   return (int)data;
}


void dot11e_conf_close(AL_CONTEXT_PARAM_DECL int dot11e_conf_handle)
{
   dot11e_conf_category_t *data;

   data = (dot11e_conf_category_t *)dot11e_conf_handle;

   if (data == NULL)
   {
      return;
   }

   if (data->category_info != NULL)
   {
      al_heap_free(AL_CONTEXT data->category_info);
   }

   al_heap_free(AL_CONTEXT data);
}


int dot11e_conf_get_category_info_count(AL_CONTEXT_PARAM_DECL int dot11e_conf_handle)
{
   dot11e_conf_category_t *data;

   data = (dot11e_conf_category_t *)dot11e_conf_handle;

   return data->count;
}


int dot11e_conf_get_category_info(AL_CONTEXT_PARAM_DECL int dot11e_conf_handle, int index, dot11e_conf_category_details_t *info)
{
   dot11e_conf_category_t *data;

   data = (dot11e_conf_category_t *)dot11e_conf_handle;

   if (index >= data->count)
   {
      return -1;
   }

   memcpy(info, &data->category_info[index], sizeof(dot11e_conf_category_details_t));

   return 0;
}


#ifndef _MESHAP_DOT11E_CONF_NO_SET_AND_PUT

int dot11e_conf_set_category_info_count(AL_CONTEXT_PARAM_DECL int dot11e_conf_handle, int count)
{
   dot11e_conf_category_t *data;

   data = (dot11e_conf_category_t *)dot11e_conf_handle;

   if (data->count > 0)
   {
      al_heap_free(AL_CONTEXT data->category_info);
      data->category_info = NULL;
   }

   data->count = count;

   if (data->count > 0)
   {
      data->category_info = (dot11e_conf_category_details_t *)al_heap_alloc(AL_CONTEXT sizeof(dot11e_conf_category_details_t) * data->count AL_HEAP_DEBUG_PARAM);
      memset(data->category_info, 0, sizeof(dot11e_conf_category_details_t) * data->count);
   }

   return 0;
}


int dot11e_conf_set_category_info(AL_CONTEXT_PARAM_DECL int dot11e_conf_handle, int index, dot11e_conf_category_details_t *info)
{
   dot11e_conf_category_t *data;

   data = (dot11e_conf_category_t *)dot11e_conf_handle;

   if (index >= data->count)
   {
      return -1;
   }

   memcpy(&data->category_info[index], info, sizeof(dot11e_conf_category_details_t));

   return 0;
}


int dot11e_conf_put(AL_CONTEXT_PARAM_DECL int dot11e_conf_handle)
{
   char *config_string;
   int  config_string_length;
   int  ret;
   int  handle;

   if (dot11e_conf_handle <= 0)
   {
      return -1;
   }

   ret = dot11e_conf_create_config_string_from_data(AL_CONTEXT dot11e_conf_handle, &config_string_length, &config_string);

   if (ret != 0)
   {
      return ret;
   }

   handle = al_open_file(AL_CONTEXT AL_FILE_ID_DOT11E_CONFIG, AL_OPEN_FILE_MODE_WRITE);
   al_write_file(AL_CONTEXT handle, config_string, config_string_length);
   al_close_file(AL_CONTEXT handle);

   al_heap_free(AL_CONTEXT config_string);

   return 0;
}


int dot11e_conf_create_config_string_from_data(AL_CONTEXT_PARAM_DECL int dot11e_conf_handle, int *dot11e_config_string_length, char **dot11e_conf_string)
{
   dot11e_conf_category_t         *data;
   dot11e_conf_category_details_t *category_info;
   char *p;
   char buffer[1024];
   char *buffer_out;
   int  ret;
   int  i;
   char *category_name;
   int  category_name_length = 5;

   data = (dot11e_conf_category_t *)dot11e_conf_handle;
   if (data == NULL)
   {
      return -1;
   }

   buffer_out = (char *)al_heap_alloc(AL_CONTEXT 64 * 1024 AL_HEAP_DEBUG_PARAM);
   memset(buffer_out, 0, 64 * 1024);

   category_name = (char *)al_heap_alloc(category_name_length AL_HEAP_DEBUG_PARAM);
   memset(category_name, 0, category_name_length);

   memset(buffer, 0, 1024);

   p = buffer_out;

   memset(buffer, 0, 1024);
   ret = sprintf(buffer, "category_info (%d) {\n", data->count);
   memcpy(p, buffer, strlen(buffer));
   p += ret;
   *p = '\n';
   p += 1;


   for (i = 0; i < data->count; i++)
   {
      category_info = &data->category_info[i];

      memset(buffer, 0, 1024);

      if (category_info->category == DOT11E_CATEGORY_TYPE_AC_BK)
      {
         ret = sprintf(buffer, "\tac_bk {\n");
      }
      else if (category_info->category == DOT11E_CATEGORY_TYPE_AC_BE)
      {
         ret = sprintf(buffer, "\tac_be {\n");
      }
      else if (category_info->category == DOT11E_CATEGORY_TYPE_AC_VI)
      {
         ret = sprintf(buffer, "\tac_vi {\n");
      }
      else if (category_info->category == DOT11E_CATEGORY_TYPE_AC_VO)
      {
         ret = sprintf(buffer, "\tac_vo {\n");
      }

      memcpy(p, buffer, strlen(buffer));
      p += ret;

      memset(buffer, 0, 1024);
      ret = sprintf(buffer, "\t\tacwmin=%d,\n", category_info->acwmin);
      memcpy(p, buffer, strlen(buffer));
      p += ret;

      memset(buffer, 0, 1024);
      ret = sprintf(buffer, "\t\tacwmax=%d,\n", category_info->acwmax);
      memcpy(p, buffer, strlen(buffer));
      p += ret;

      memset(buffer, 0, 1024);
      ret = sprintf(buffer, "\t\taifsn=%d,\n", category_info->aifsn);
      memcpy(p, buffer, strlen(buffer));
      p += ret;

      memset(buffer, 0, 1024);
      ret = sprintf(buffer, "\t\tdisable_backoff=%d,\n", category_info->disable_backoff);
      memcpy(p, buffer, strlen(buffer));
      p += ret;

      memset(buffer, 0, 1024);
      ret = sprintf(buffer, "\t\tburst_time=%d\n", category_info->burst_time);
      memcpy(p, buffer, strlen(buffer));
      p += ret;

      memset(buffer, 0, 1024);
      ret = sprintf(buffer, "\t}\n");
      memcpy(p, buffer, strlen(buffer));
      p += ret;
   }

   memset(buffer, 0, 1024);
   ret = sprintf(buffer, "}\n");
   memcpy(p, buffer, strlen(buffer));
   p += ret;
   *p = '\n';
   p++;

   *p = '\0';

   ret = p - buffer_out;
   *dot11e_conf_string = (char *)al_heap_alloc(AL_CONTEXT ret AL_HEAP_DEBUG_PARAM);
   memset(*dot11e_conf_string, 0, ret);

   strcpy(*dot11e_conf_string, buffer_out);
   *dot11e_config_string_length = ret;

   al_heap_free(AL_CONTEXT buffer_out);

   return 0;
}
#endif /* _MESHAP_DOT11E_CONF_NO_SET_AND_PUT */
