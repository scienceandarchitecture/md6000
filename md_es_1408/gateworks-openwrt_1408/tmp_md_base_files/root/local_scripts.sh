
#######################################
#  script to generate restart_meshd   #
#######################################
echo "" > /sbin/restart_meshd
echo "meshd stop &" >> /sbin/restart_meshd
echo "killall logread; sleep 6; killall meshd;" >> /sbin/restart_meshd
echo "killall configd hostapd miscd watchd; sleep 1 " >> /sbin/restart_meshd
echo "/root/startap.sh;sleep 10 " >> /sbin/restart_meshd
echo "/sbin/logread -f -F /overlay/md_syslog.txt -p /var/run/logread.1.pid -S 10000  &" >> /sbin/restart_meshd
chmod 777 /sbin/restart_meshd

sleep 60
iptables --flush
echo 0 > /proc/sys/kernel/printk
