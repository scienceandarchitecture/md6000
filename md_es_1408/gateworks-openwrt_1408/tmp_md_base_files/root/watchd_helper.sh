
##********************************************************************************
 # MeshDynamics 
 # -------------- 
 # File     : watchd_helper.sh
 # Comments : Script for checking daemons
 # Created  : 5/17/2005
 # Author   : Sriram Dayanandan
 # Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
 #
 # File Revision History 
 # -----------------------------------------------------------------------------
 # | No  |Date      |  Comment                                        | Author |
 # -----------------------------------------------------------------------------
 # |  0  |5/17/2005 | Created.                                        | Sriram |
 # -----------------------------------------------------------------------------
 #*******************************************************************************/

PID=`pidof $1`

if [ "$PID" == "" ]
then
	echo "Process $1 was not found"
	exit 1
fi

exit 0

