#MAC=04:F0:21:25:1D:6B
MAC=$1

sudo rm -rf images_1408/imx6/root-fs-imx6
sudo rm -rf images_1408/imx6

sudo mkdir -p images_1408/imx6 
sudo chmod 777 images_1408/imx6
mkdir -p images_1408/imx6/root-fs-imx6

fakeroot tar --numeric-owner -xvf ./trunk/bin/imx6/openwrt-imx6-ventana-rootfs.tar.gz -C images_1408/imx6/root-fs-imx6

sudo cp tmp_md_base_files/root/* images_1408/imx6/root-fs-imx6/root/
sudo cp tmp_md_base_files/etc/*.conf images_1408/imx6/root-fs-imx6/etc/
sudo cp md_configs/MD4455-AAIA.conf images_1408/imx6/root-fs-imx6/etc/meshap.conf
sudo cp tmp_md_base_files/etc/config/network images_1408/imx6/root-fs-imx6/etc/config/network

sudo cp md_configs/meshdynamics images_1408/imx6/root-fs-imx6/etc/init.d/meshdynamics
sudo ln -sf ../init.d/meshdynamics images_1408/imx6/root-fs-imx6/etc/rc.d/S50meshdynamics
sudo ln -sf ../init.d/meshdynamics images_1408/imx6/root-fs-imx6/etc/rc.d/K50meshdynamics

sudo chmod 777 images_1408/imx6/root-fs-imx6/etc/meshap.conf
sudo tmp_files/patcher -m `find images_1408/imx6/root-fs-imx6/ -name meshap.ko` -d images_1408/imx6/root-fs-imx6/sbin/configd -c images_1408/imx6/root-fs-imx6/etc/meshap.conf $MAC
sudo chmod 777 images_1408/imx6/root-fs-imx6/etc/meshap.conf

mkdir -p images_1408/imx6/root-fs-imx6/boot
cp  trunk/bin/imx6/openwrt-imx6-uImage trunk/bin/imx6/openwrt-imx6-*-gw*.dtb images_1408/imx6/root-fs-imx6/boot/
./trunk/staging_dir/host/bin/mkfs.ubifs -F -m 2048 -e 124KiB -c 1912 -x zlib -o images_1408/imx6/root.ubifs -d images_1408/imx6/root-fs-imx6

cp tmp_files/ubinize.cfg images_1408/imx6/
sudo chmod 777 images_1408/imx6/ubinize.cfg

./trunk/staging_dir/host/bin/ubinize -m 2048 -p 128KiB -s 2048 -o images_1408/imx6/image.ubi images_1408/imx6/ubinize.cfg
./scripts/mkimage_jtag ./bootloader/ventana/SPL ./bootloader/ventana/u-boot.img > ./bootloader/ventana/u-boot_spl.bin;
./scripts/mkimage_jtag ./bootloader/ventana/SPL ./bootloader/ventana/u-boot.img ./images_1408/imx6/image.ubi > images_1408/imx6/ventana_normal.bin;
#./scripts/mkimage_jtag ./bootloader/ventana/SPL ./bootloader/ventana/u-boot.img ./images_1408/imx6/image.ubi > images_1408/imx6/ventana_large.bin;


