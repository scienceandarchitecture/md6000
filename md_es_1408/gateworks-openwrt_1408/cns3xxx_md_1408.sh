#!/bin/bash

#MAC=30:14:4A:EA:88:59
MAC=$1

PATH_DIR=trunk
GATEWORKS=openwrt

sudo rm -rf images_1408/cns3xxx/*.bin images_1408/cns3xxx/root images_1408/cns3xxx/*squashfs.img
sudo mkdir -p images_1408/cns3xxx
sudo chmod 777 images_1408/cns3xxx -R
sudo cp -r $PATH_DIR/bin/cns3xxx/* images_1408/cns3xxx/
sudo cp -fpr $PATH_DIR/build_dir/target-arm_mpcore+vfp_uClibc-0.9.33.2_eabi/root-cns3xxx images_1408/cns3xxx/root

sudo cp tmp_md_base_files/root/* images_1408/cns3xxx/root/root/
sudo cp tmp_md_base_files/etc/*.conf images_1408/cns3xxx/root/etc/
sudo cp md_configs/MD4455-AAIA.conf images_1408/cns3xxx/root/etc/meshap.conf
sudo cp tmp_md_base_files/etc/config/network images_1408/cns3xxx/root/etc/config/network
sudo cp atftp images_1408/cns3xxx/root/bin/ 

sudo cp md_configs/meshdynamics images_1408/cns3xxx/root/etc/init.d/meshdynamics
sudo ln -sf ../init.d/meshdynamics images_1408/cns3xxx/root/etc/rc.d/S50meshdynamics
sudo ln -sf ../init.d/meshdynamics images_1408/cns3xxx/root/etc/rc.d/K50meshdynamics


#sudo cp md_configs/MD4350-AAIx.conf images_1408/cns3xxx/root/etc/meshap.conf
#sudo cp md_configs/startap.sh images_1408/cns3xxx/root/root/startap.sh
#sudo cp md_configs/load_torna images_1408/cns3xxx/root/root/load_torna
#sudo cp md_configs/acl.conf images_1408/cns3xxx/root/etc/acl.conf
#sudo cp md_configs/dot11e.conf images_1408/cns3xxx/root/etc/dot11e.conf
#sudo cp md_configs/map_if_info.conf images_1408/cns3xxx/root/etc/map_if_info.conf
#sudo cp md_configs/mobility.conf images_1408/cns3xxx/root/etc/mobility.conf
#sudo cp md_configs/sip.conf images_1408/cns3xxx/root/etc/sip.conf

sudo tmp_files/patcher -m `find images_1408/cns3xxx/root -name meshap.ko` -d images_1408/cns3xxx/root/sbin/configd -c images_1408/cns3xxx/root/etc/meshap.conf $MAC
sudo $PATH_DIR/staging_dir/host/bin/mksquashfs4 images_1408/cns3xxx/root images_1408/cns3xxx/$GATEWORKS-cns3xxx-squashfs.img -nopad -noappend -root-owned -comp xz -Xpreset 9 -Xe -Xlc 0 -Xlp 2 -Xpb 2 -b 256k -processors 1
sudo $PATH_DIR/staging_dir/host/bin/padjffs2 images_1408/cns3xxx/$GATEWORKS-cns3xxx-squashfs.img 4 8 64 128 256
sudo chmod 777 images_1408/cns3xxx/* -R
sudo tr "\000" "\377" < /dev/zero | sudo dd bs=1k count=16k of=images_1408/cns3xxx/md-nor_16M_$MAC.bin
sudo tr "\000" "\377" < /dev/zero | sudo dd bs=1k count=384 of=images_1408/cns3xxx/step2.bin
sudo dd if=bootloader/laguna/u-boot_nor.bin of=images_1408/cns3xxx/step2.bin bs=393216 conv=notrunc
sudo chmod 777 images_1408/cns3xxx/* -R
sudo cat $PATH_DIR/bin/cns3xxx/$GATEWORKS-cns3xxx-uImage >> images_1408/cns3xxx/step2.bin;
sudo tr "\000" "\377" < /dev/zero | sudo dd bs=1k count=2432 of=images_1408/cns3xxx/step3.bin
sudo dd if=images_1408/cns3xxx/step2.bin of=images_1408/cns3xxx/step3.bin bs=2490368 conv=sync
sudo chmod 777 images_1408/cns3xxx/* -R
sudo cat images_1408/cns3xxx/$GATEWORKS-cns3xxx-squashfs.img >> images_1408/cns3xxx/step3.bin  
sudo dd if=images_1408/cns3xxx/step3.bin of=images_1408/cns3xxx/md-nor_16M_$MAC.bin conv=notrunc
#sudo dd if=images_1408/cns3xxx/step3.bin of=images_1408/cns3xxx/md-nor_16M_(sed "s/:/_/g" <<<"$MAC").bin conv=notrunc
#sudo dd if=images_1408/cns3xxx/step3.bin of=images_1408/cns3xxx/md-nor_16M_(sed -i -e 's/:/_/g'$MAC).bin conv=notrunc
