#include "md_minstrel_rt.h"
#include <linux/version.h>

int mins_rt_ctrl_get_item(struct meshap_instance *instance, unsigned char *address, meshap_rate_ctrl_info_t *rate_ctrl_info)
{
    al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MAC80211 : FLOW : Enter %s : %d\n", __func__,__LINE__);
    struct minstrel_rate *msr;
    struct ieee80211_sub_if_data *sdata = IEEE80211_DEV_TO_SUB_IF(instance->dev);
    void *priv_sta                      = NULL;
    if ((address[0] == 0x00) && (address[1] == 0x00) && (address[2] == 0x00))
    {
        printk(KERN_DEBUG "address is zero\n");
        return 0;
    }

    struct sta_info *sta                = sta_info_get(sdata, address);

    if (sta && test_sta_flag(sta, WLAN_STA_RATE_CONTROL))
    {
        if (sta->rate_ctrl_priv != NULL)
        {
            priv_sta = sta->rate_ctrl_priv;
        }
        else
            return 0;
    }
    else
    {
        return 0;
    }
    msr = minstrel_get_sta_rate_ctrl_info(priv_sta);
    if (msr == NULL)
    {
        printk(KERN_EMERG "msr is null\n");
        return 0;
    }
    if (rate_ctrl_info != NULL)
    {
        rate_ctrl_info->rate_index = msr->rix;
        rate_ctrl_info->rate_in_mbps = msr->bitrate / 2;
#if (LINUX_VERSION_CODE < KERNEL_VERSION(4, 4, 0))
        rate_ctrl_info->tx_success = msr->success;
        rate_ctrl_info->tx_error = (msr->attempts - msr->success);
        rate_ctrl_info->tx_retry = msr->attempts;
#else
        rate_ctrl_info->tx_success = msr->stats.success;
        rate_ctrl_info->tx_error = (msr->stats.attempts - msr->stats.success);
        rate_ctrl_info->tx_retry = msr->stats.attempts;
#endif
    }
    al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MAC80211 : FLOW : Exit %s : %d\n", __func__,__LINE__);
    return msr->bitrate / 2;
}
