/********************************************************************************
* MeshDynamics
* --------------
* File     : meshap_instance.h
* Comments : Atheros driver instance structure
* Created  : 9/21/2004
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  59 |2/12/2009 | Changes for action frame support				  |Abhijit |
* -----------------------------------------------------------------------------
* |  58 |2/10/2009 | Additions for mixed mode						  |Abhijit |
* -----------------------------------------------------------------------------
* |  57 |12/30/2008| Changes for I386 generic platform               | Sriram |
* -----------------------------------------------------------------------------
* |  56 |10/13/2008| Added MESHAP_INSTANCE_FLAGS_RX_ON_ALL_ANTENNA  | Sriram |
* -----------------------------------------------------------------------------
* |  55 |8/22/2008 | Changes for FIPS                                |Abhijit |
* -----------------------------------------------------------------------------
* |  54 |05/09/2008| Changes for Beaconing Uplink		              |Prachiti|
* -----------------------------------------------------------------------------
* |  53 |01/07/2008| Additions mixed mode		.                     |Prachiti|
* -----------------------------------------------------------------------------
* |  52 |11/7/2007 | Changes for queued retry                        |Prachiti|
* -----------------------------------------------------------------------------
* |  51 |8/6/2007  | deauth_count added to instance                  | Sriram |
* -----------------------------------------------------------------------------
* |  50 |8/6/2007  | Added MESHAP_INSTANCE_FLAGS_DROP_RX            | Sriram |
* -----------------------------------------------------------------------------
* |  49 |8/2/2007  | Added MESHAP_INSTANCE_FLAGS_DROP_TX            | Sriram |
* -----------------------------------------------------------------------------
* |  48 |7/23/2007 | last_rx_time added for STA mode                 | Sriram |
* -----------------------------------------------------------------------------
* |  47 |7/20/2007 | Changes for high noise floor protection         | Sriram |
* -----------------------------------------------------------------------------
* |  46 |7/11/2007 | Added MESHAP_INSTANCE_FLAGS_NO_PROBE_RESP flag | Sriram |
* -----------------------------------------------------------------------------
* |  45 |7/6/2007  | Added CTRL/Probe Req/Resp counters              | Sriram |
* -----------------------------------------------------------------------------
* |  44 |7/3/2007  | max_last_beacon_time replaced with bmiss_count  | Sriram |
* -----------------------------------------------------------------------------
* |  43 |7/3/2007  | Added MESHAP_INSTANCE_FLAGS_TX_ERROR_PRINT     | Sriram |
* -----------------------------------------------------------------------------
* |  42 |6/11/2007 | Added address to Key cache info struct          | Sriram |
* -----------------------------------------------------------------------------
* |  41 |5/25/2007 | Changes for TX Fragmentation                    | Sriram |
* -----------------------------------------------------------------------------
* |  40 |4/20/2007 | Added No Calibration Instance Flag              | Sriram |
* -----------------------------------------------------------------------------
* |  39 |4/20/2007 |  Packet count added to RX INFO                  | Sriram |
* -----------------------------------------------------------------------------
* |  38 |4/11/2007 | Changes for EEPROM override/calibration         | Sriram |
* -----------------------------------------------------------------------------
* |  37 |3/22/2007 | Added counters for PHY error details            | Sriram |
* -----------------------------------------------------------------------------
* |  36 |3/21/2007 | Alternate rate code filtering added             | Sriram |
* -----------------------------------------------------------------------------
* |  35 |3/12/2007 | Added PHY and CRC counters                      |Prachiti|
* -----------------------------------------------------------------------------
* |  34 |3/7/2007  | Added interrupt counters                        |Prachiti|
* -----------------------------------------------------------------------------
* |  33 |3/1/2007  | Added tx_retry_count experimental variable      |Prachiti|
* -----------------------------------------------------------------------------
* |  32 |1/24/2007 | Changes for PS-POLL                             | Sriram |
* -----------------------------------------------------------------------------
* |  31 |1/24/2007 | Changes for Private Information in Rate Ctrl    | Sriram |
* -----------------------------------------------------------------------------
* |  30 |11/22/2006| Changes for DFS                                 | Sriram |
* -----------------------------------------------------------------------------
* |  29 |11/8/2006 | Changes for compression                         | Sriram |
* -----------------------------------------------------------------------------
* |  28 |10/18/2006| Changes for HAL 0.9.17.1                        | Sriram |
* -----------------------------------------------------------------------------
* |  27 |3/29/2006 | TSF field removed due to compiler problems      | Sriram |
* -----------------------------------------------------------------------------
* |  26 |3/16/2006 | TSF time field added                            | Sriram |
* -----------------------------------------------------------------------------
* |  25 |2/28/2006 | Added queue for Regular traffic                 | Sriram |
* -----------------------------------------------------------------------------
* |  24 |02/15/2006| Changes for dot11e                              | Bindu  |
* -----------------------------------------------------------------------------
* |  23 |02/08/2006| Changes for Hide SSID                           | Sriram |
* -----------------------------------------------------------------------------
* |  22 |01/17/2006| Changes for PS mode and RX Frag support         | Sriram |
* -----------------------------------------------------------------------------
* |  21 |12/9/2005 | Changes for HAL 0.9.14.25                       | Sriram |
* -----------------------------------------------------------------------------
* |  20 |10/4/2005 | Lock semantics changed                          | Sriram |
* -----------------------------------------------------------------------------
* |  19 |9/22/2005 | ESSID length changes                            | Sriram |
* -----------------------------------------------------------------------------
* |  18 |9/20/2005 | TX Timeout recovery + Lock logic enhancement    | Sriram |
* -----------------------------------------------------------------------------
* |  17 |6/29/2005 | Misc changes                                    | Sriram |
* -----------------------------------------------------------------------------
* |  16 |6/12/2005 | Instance locking mutex logic fixed              | Sriram |
* -----------------------------------------------------------------------------
* |  15 |6/11/2005 | TX Desc recovery procedures added               | Sriram |
* -----------------------------------------------------------------------------
* |  14 |6/2/2005  | 11a/b/g flags added                             | Sriram |
* -----------------------------------------------------------------------------
* |  13 |5/20/2005 | Fixed TX Rate changes                           | Sriram |
* -----------------------------------------------------------------------------
* |  12 |4/12/2005 | Beacon miss logic changed                       | Sriram |
* -----------------------------------------------------------------------------
* |  11 |4/7/2005  | Misc changes                                    | Sriram |
* -----------------------------------------------------------------------------
* |  10 |4/6/2005  | Security changes                                | Sriram |
* -----------------------------------------------------------------------------
* |  9  |4/7/2005  | essid_info added                                | Abhijit|
* -----------------------------------------------------------------------------
* |  8  |03/08/2005| Beacon Vendor Info added						  | Anand  |
* -----------------------------------------------------------------------------
* |  7  |1/11/2005 | RSSI calculation modified                       | Sriram |
* -----------------------------------------------------------------------------
* |  6  |1/7/2005  | Implemented get_last_beacon_time                | Sriram |
* -----------------------------------------------------------------------------
* |  5  |12/23/2004| Implemented link notify watchdog                | Sriram |
* -----------------------------------------------------------------------------
* |  4  |12/23/2004| Changes for proc FS                             | Sriram |
* -----------------------------------------------------------------------------
* |  3  |12/22/2004| Changes for beacon interrupt watchdog           | Sriram |
* -----------------------------------------------------------------------------
* |  2  |12/22/2004| Changes for beacon miss verification            | Sriram |
* -----------------------------------------------------------------------------
* |  1  |12/14/2004| Misc changes                                    | Sriram |
* -----------------------------------------------------------------------------
* |  0  |9/21/2004 | Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/
#include <linux/proc_fs.h>
#include <linux/if_ether.h>
#include <linux/wireless.h>
#include <linux/netdevice.h>
#include <linux/kfifo.h>
#include <meshap_queue.h>

#include "al.h"
#include "meshap.h"

#ifndef __MESHAP_INSTANCE_H__
#define __MESHAP_INSTANCE_H__

#include <torna_wlan.h>
#include <torna_byte_order.h>

/**
 * Virtual Definitions
 */

#define MESHAP_IWMODE_VIRTUAL_MASTER    1
#define MESHAP_IWMODE_VIRTUAL_INFRA     2

/**
 * Used for checking mixed mode
 */

#define MESHAP_DEV_MODE_MIXED     1
#define MESHAP_DEV_MODE_NORMAL    2


#define MESHAP_INSTANCE_MARK_KEY_SLOT(instance, slot)     (instance)->key_bitmap[(slot) / 8] |= (1 << (slot) % 8)
#define MESHAP_INSTANCE_CLEAR_KEY_SLOT(instance, slot)    (instance)->key_bitmap[(slot) / 8] &= ~(1 << (slot) % 8)
#define MESHAP_INSTANCE_IS_SLOT_MARKED(instance, slot)    ((instance)->key_bitmap[(slot) / 8] & (1 << (slot) % 8))

#define MESHAP_STA_FSM_STATE_STOPPED          0
#define MESHAP_STA_FSM_STATE_DISCONNECTED     1
#define MESHAP_STA_FSM_STATE_RUNNING          2

#define MESHAP_MGMT_PKT                       1
#define MESHAP_DATA_PKT                       2

#define MESHAP_BMISS_VERIFICATION_INTERVAL    5                 /** 5 seconds */
#define TORNA_GET_LE_SHORT(s, p)    \
   ({                               \
      memcpy(&s, p, sizeof(short)); \
      le16_to_cpu(s);               \
   }                                \
   )

#define TORNA_GET_SHORT(s, p)       \
   ({                               \
      memcpy(&s, p, sizeof(short)); \
      s;                            \
   }                                \
   )

#define _GET_LE_SHORT()             \
   ({                               \
      s = TORNA_GET_LE_SHORT(s, p); \
      p += sizeof(short);           \
      s;                            \
   }                                \
   )

#define _GET_SHORT()         \
   ({                        \
      TORNA_GET_SHORT(s, p); \
      p += sizeof(short);    \
      s;                     \
   }                         \
   )

#define _PUT_LE_SHORT(_val)           \
   do {                               \
      TORNA_PUT_LE_SHORT(_val, s, p); \
      p += sizeof(short);             \
   } while (0)


#ifndef MESHAP_BEACON_INTERRUPT_WATCHDOG_INTERVAL
#define MESHAP_BEACON_INTERRUPT_WATCHDOG_INTERVAL    10000             /** 10 seconds */
#endif

static const unsigned char _vendor_info_header[] =
{
   0x00, 0x12, 0xCE, 0x01
};

void meshap_free_mem(struct sk_buff *skb);

struct meshap_lock
{
   const char *name;
   spinlock_t lock;
};

typedef struct meshap_lock   meshap_lock_t;

static inline void _meshap_lock_init(meshap_lock_t *pal, const char *name)
{
   spin_lock_init(&pal->lock);
   pal->name = name;
}


static inline void _meshap_lock_destroy(meshap_lock_t *pal)
{
}


#define MESHAP_LOCK_LOCK(_lck, _flags)      spin_lock(&(_lck)->lock)
#define MESHAP_LOCK_UNLOCK(_lck, _flags)    spin_unlock(&(_lck)->lock)
#define MESHAP_CALIBRATION_INTERVAL_LONG     30
#define MESHAP_CALIBRATION_INTERVAL_SHORT    1
#define TORNA_BIT(x)                        (1 << (x))
#define MAC2STR(a)                          (a)[0], (a)[1], (a)[2], (a)[3], (a)[4], (a)[5]
#define MACSTR                               "%02x:%02x:%02x:%02x:%02x:%02x"


#define PACKET_DUMP_2(type, bytes, length, message)                \
   ({                                                              \
      int i, j;                                                    \
      printk(type "%s\n", message);                                \
      for (i = 0, j = 0; i<length; i++) {                          \
                              if (j >= 16) {                       \
                                 j = 0;                            \
                                 printk("\n");                     \
                              }                                    \
                              if (j == 0) {                        \
                                 printk(type "%02x ", bytes[i]); } \
                              else{                                \
                                 printk("%02x ", bytes[i]); }      \
                              j++;                                 \
                           }                                       \
                           printk("\n");                           \
                           }                                       \
                           )


/**
 * Data transmit queue state.  One of these exists for each
 * hardware transmit queue.  Packets sent to us from above
 * are assigned to queues based on their priority.  Not all
 * devices support a complete set of hardware transmit queues.
 * For those devices the array q will map multiple
 * priorities to fewer hardware queues (typically all to one
 * hardware queue).
 */

struct meshap_instance;

struct meshap_txq
{
   unsigned char          initialized;                          /* Flag to indicate a real Q as oppossed to a virtual */
   u_int                  hal_qnum;                             /* hardware q number */
   u_int32_t              *link;                                /* link ptr in last TX desc */
   void                   *last_fc_ptr;                         /* Ptr to the last buffer's Frame control */
   unsigned short         last_fc;                              /* The value of last buffer's Frame control */
   MESHAP_TAILQ_DEFINE_LIST(meshap_buf)    q;                   /* transmit queue */
   meshap_lock_t          lock;                                 /* lock on q and link */
   int                    tx_buf_count;                         /* Number of buffer transmitted without interrupt */
   unsigned int           total_tx;                             /* Total number of transmissions on this queue */
   unsigned int           total_tx_out;                         /* Total number of transmissions going *out* of the queue */

   unsigned char          *comp_buf_virt;                       /* Compression buffer virtual address */
   unsigned char          *comp_buf_dma;                        /* Compression buffer physical address */
   unsigned int           comp_buf_size;                        /* Compression buffer size */
   struct meshap_instance *pinstance;
};

typedef struct meshap_txq   meshap_txq_t;

#define MESHAP_TXQ_LOCK_INIT(_tq)       _meshap_lock_init(&(_tq)->lock, "txq")
#define MESHAP_TXQ_LOCK_DESTROY(_tq)    _meshap_lock_destroy(&(_tq)->lock)
#define MESHAP_TXQ_LOCK(_tq, _flg)      MESHAP_LOCK_LOCK(&(_tq)->lock, _flg)
#define MESHAP_TXQ_UNLOCK(_tq, _flg)    MESHAP_LOCK_UNLOCK(&(_tq)->lock, _flg)

struct meshap_tx_pool;
struct meshap_rate_ctrl;

#define MESHAP_INSTANCE_SLOT_TIME_STATUS_OK        0
#define MESHAP_INSTANCE_SLOT_TIME_STATUS_UPDATE    1
#define MESHAP_INSTANCE_SLOT_TIME_STATUS_COMMIT    2

#define MESHAP_INSTANCE_RX_INFO_MAX                3

#ifndef MESHAP_BEACONING_UPLINK_MAX_TIMEOUT
#define MESHAP_BEACONING_UPLINK_MAX_TIMEOUT        1 /*mins*/
#endif

#define WLAN_CHANNEL_802_11_A                      (WLAN_CHANNEL_5GHZ | WLAN_CHANNEL_OFDM)
#define WLAN_CHANNEL_802_11_B                      (WLAN_CHANNEL_2GHZ | WLAN_CHANNEL_CCK)
#define WLAN_CHANNEL_PURE_802_11_G                 (WLAN_CHANNEL_2GHZ | WLAN_CHANNEL_OFDM)
#define WLAN_CHANNEL_802_11_G                      (WLAN_CHANNEL_2GHZ | WLAN_CHANNEL_OFDM)
#define WLAN_CHANNEL_MESHAP_TURBO                  (WLAN_CHANNEL_5GHZ | WLAN_CHANNEL_OFDM | WLAN_CHANNEL_ATHEROS_TURBO_MODE)
#define WLAN_CHANNEL_108_G                         (WLAN_CHANNEL_2GHZ | WLAN_CHANNEL_OFDM | WLAN_CHANNEL_ATHEROS_TURBO_MODE)
#define WLAN_CHANNEL_MESHAP_X                      (WLAN_CHANNEL_802_11_A | WLAN_CHANNEL_MESHAP_XTENDED_RANGE)
#define WLAN_CHANNEL_802_11_N                      (WLAN_CHANNEL_OFDM)
#define WLAN_CHANNEL_802_11_AC                      (WLAN_CHANNEL_5GHZ | WLAN_CHANNEL_OFDM)
/**
 * 802.11 PHY modes
 */

#define WLAN_PHY_MODE_AUTO             0               /** Auto select */
#define WLAN_PHY_MODE_802_11_A         1               /** 5 GHz OFDM */
#define WLAN_PHY_MODE_802_11_B         2               /** 2.4 GHz DSSS */
#define WLAN_PHY_MODE_802_11_G         3               /** 2.4 GHz CCK-OFDM */
#define WLAN_PHY_MODE_802_11_FH        4               /** 2.4 GHz FSSS */
#define WLAN_PHY_MODE_MESHAP_TURBO     5               /** 5GHz Atheros Turbo mode */
#define WLAN_PHY_MODE_802_11_PURE_G    6               /** 2.4 GHz OFDM without backward compatibility */
#define WLAN_PHY_MODE_802_11_PSQ       7               /** 5 MHz 4.9 GHz US Public safety */
#define WLAN_PHY_MODE_802_11_PSH       8               /** 10 MHz 4.9 GHz US Public safety */
#define WLAN_PHY_MODE_802_11_PSF       9               /** 20 MHz 4.9 GHz US Public safety */
#define WLAN_PHY_MODE_802_11_N      10
#define WLAN_PHY_MODE_802_11_AC     11
#define WLAN_PHY_MODE_802_11_BGN    12
#define WLAN_PHY_MODE_802_11_AN     13
#define WLAN_PHY_MODE_802_11_ANAC   14
#define WLAN_PHY_MODE_802_11_2GHZ_PURE_N  15        
#define WLAN_PHY_MODE_802_11_5GHZ_PURE_N  16        
#define HT_CAP_INFO_SUPP_CHANNEL_WIDTH_SET  ((u16) BIT(1))
#define HT_INFO_HT_PARAM_SECONDARY_CHNL_ABOVE       ((u8) BIT(0))
#define HT_INFO_HT_PARAM_SECONDARY_CHNL_BELOW       ((u8) BIT(0) | BIT(1))

struct meshap_rx_info
{
   int           rate_min;
   int           rate_max;
   int           rssi_min;
   int           rssi_max;
   int           rssi_avg;
   int           rssi_current;
   unsigned char sample_count;
   unsigned long last_rx;
   unsigned long packet_count;
};

typedef struct meshap_rx_info   meshap_rx_info_t;

struct meshap_key_info
{
   int           cipher_type;
   u32           wep_iv;
   u64           key_rsc;
   u64           key_tsc;
   unsigned char address[ETH_ALEN];
   unsigned char key[32];                       // used only for CCMP
};

typedef struct meshap_key_info   meshap_key_info_t;

#define MESHAP_ESSID_MAX_SIZE                     32

#define MESHAP_ESSID_SECURITY_INFO_WEP_ENABLED    0x001
#define MESHAP_ESSID_SECURITY_INFO_RSN_ENABLED    0x002

struct  atheros_essid_security_info
{
   unsigned int  flags;
   unsigned char rsn_ie_buffer[256];
   int           rsn_ie_length;
};

typedef struct atheros_essid_security_info   atheros_essid_security_info_t;

struct atheros_essid_info
{
   atheros_essid_security_info_t sec_info;
   unsigned short                vlan_tag;
   char                          essid[MESHAP_ESSID_MAX_SIZE + 1];
   int                           essid_length;
};

typedef struct atheros_essid_info   atheros_essid_info_t;

#define MESHAP_TX_INTR_INFO_MAX    16

struct meshap_tx_intr_info
{
   unsigned char              dest_mac[ETH_ALEN];
   unsigned short             frame_control;
   unsigned short             seq_num;
   unsigned char              status;
   unsigned char              rate_index;
   unsigned char              short_retry;
   unsigned char              long_retry;
   unsigned long              time;
   unsigned char              virt_collision_count;
   unsigned char              hal_qnum;
   struct meshap_tx_intr_info *next;
};

typedef struct meshap_tx_intr_info   meshap_tx_intr_info_t;

struct meshap_rx_frag_item
{
   unsigned short             sequence_number;
   unsigned char              last_frag_number;
   unsigned int               last_fragment_time;
   int                        total_len;
   struct sk_buff             *skb;
   struct meshap_rx_frag_item *next_list;
   struct meshap_rx_frag_item *prev_list;
};
typedef struct meshap_rx_frag_item   meshap_rx_frag_item_t;


/** 802.11e Categories
 *
 *	AC_BK    = 0		(background)
 *	AC_BE	= 1		(best_effort)
 *	AC_VI	= 2		(video)
 *	AC_VO	= 3		(voice)
 */

struct atheros_dot11e_category_details
{
   unsigned char  category;
   unsigned short acwmin;
   unsigned short acwmax;
   unsigned char  aifsn;
   unsigned char  disable_backoff;
   unsigned int   burst_time;
};

typedef struct atheros_dot11e_category_details   atheros_dot11e_category_details_t;


struct atheros_dot11e_category
{
   unsigned char                     count;
   atheros_dot11e_category_details_t *category_info;                    /* 802.11e Category Info */
};

typedef struct atheros_dot11e_category   atheros_dot11e_category_t;

#define MESHAP_RX_FRAG_HASH_SIZE    4096

struct _meshap_mac80211_sta_info
{
   unsigned char         power_save_mode;                                       /* 0=Awake, 1= Doze */
   unsigned short        aid;                                                   /* The Association ID (for TIM bitmap purposes) */
   atomic_t              power_save_queued_count;                               /* Current queued packet count */
   unsigned int          total_ps_poll_received;                                /* Count of number of PS-POLL frames received */
   unsigned int          total_power_save_queued_count;                         /* Total queued packet count */
   unsigned int          total_power_save_dropped_count;                        /* Total queued but dropped packet count */
   unsigned int          total_switch_count;                                    /* Number of times switched from Awake to Doze and vice-versa */
   atomic_t              ps_poll_locked;                                        /* 1=PS-POLL received, packet sent but not yet acked */
   meshap_txq_t          packet_queue;                                          /* The queue itself */
   unsigned short        last_rx_sqnr;                                          /* Last received sequence number */
   unsigned int          rx_duplicate_count;                                    /* Count of duplicate packets received */
   meshap_rx_frag_item_t *rx_fragment_hash[MESHAP_RX_FRAG_HASH_SIZE];           /* Hash table for incomplete fragments */
   unsigned int          rx_frag_total;                                         /* Total count of fragments recived */
   unsigned int          rx_frag_complete;                                      /* Total count of fragmented packets completed */
   unsigned long         last_rx;                                               /* Last time we received a packet */

   unsigned long         q_retry_success_count;                                 /* For queued retry successfully transmitted packets*/
   unsigned long         q_retry_queued_count;
};
typedef struct _meshap_mac80211_sta_info   _meshap_mac80211_sta_info_t;

typedef enum
{
   AH_FALSE = 0,        /* NB: lots of code assumes false is zero */
   AH_TRUE  = 1
} MESHAP_BOOL;


static const unsigned char _meshap_802_11_A_rates[] =
{
   0x80 + (6 * 2),
   0x00 + (9 * 2),
   0x80 + (12 * 2),
   0x00 + (18 * 2),
   0x80 + (24 * 2),
   0x00 + (36 * 2),
   0x00 + (48 * 2),
   0x00 + (54 * 2)
};

static const unsigned char _meshap_802_11_A_Qtr_rates[] =
{
   0x80 + (6 * 2),
   0x00 + (4),
   0x80 + (3 * 2),
   0x00 + (9),
   0x80 + (6 * 2),
   0x00 + (9 * 2),
   0x00 + (12 * 2),
   0x00 + (27)
};
static const unsigned char _meshap_802_11_A_Half_rates[] =
{
   0x80 + (6),
   0x00 + (9),
   0x80 + (12),
   0x00 + (18),
   0x80 + (24),
   0x00 + (36),
   0x00 + (48),
   0x00 + (54)
};

static const unsigned char _meshap_802_11_A_Turbo_rates[] =
{
   0x00 + (6 * 4),
   0x00 + (9 * 4),
   0x00 + (12 * 4),
   0x00 + (18 * 4),
   0x00 + (24 * 4),
   0x00 + (36 * 4),
   0x00 + (48 * 4),
   0x00 + (54 * 4)
};

static const unsigned char _meshap_802_11_B_rates[] =
{
   0x80 + (1 * 2),
   0x80 + (2 * 2),
   0x80 + (11),
   0x80 + (11 * 2)
};

static const unsigned char _meshap_802_11_G_rates[] =
{
   0x80 + (1 * 2),
   0x80 + (2 * 2),
   0x80 + (11),
   0x80 + (11 * 2),
   0x00 + (18 * 2),
   0x00 + (24 * 2),
   0x00 + (36 * 2),
   0x00 + (54 * 2),
};

static const unsigned char _meshap_802_11_G_ext_rates[] =
{
   0x00 + (6 * 2),
   0x00 + (9 * 2),
   0x00 + (12 * 2),
   0x00 + (48 * 2)
};

enum
{
   CTRY_DEBUG     = 0x1ff,      /* debug country code */
   CTRY_DEFAULT   = 0,          /* default country code */
   CTRY_PRIVATE_1 = 1,          /* Meshdynamics private */
};


struct meshap_buf
{
   MESHAP_TAILQ_DECLARE_MEMBER(meshap_buf)   list;

   struct ath_desc *hal_descriptor;                             /** Valid for RX and TX buffers only */
   dma_addr_t      hal_descriptor_dma_address;                  /** Valid for RX and TX buffers only */

   struct sk_buff  *rx_skb;                                     /** Valid for RX buffers only */
   dma_addr_t      rx_skb_dma_addr;                             /** Valid for RX buffers only */
   int             rx_skb_usable_length;                        /** Valid for RX buffers only */

   void            *tx_pool_item;                               /** Valid for TX buffers only */
   unsigned char   dst_addr[ETH_ALEN];                          /** Valid for TX buffers only */
   unsigned char   no_rate_ctrl;                                /** Valid for TX buffers only */
   unsigned short  frame_ctrl;                                  /** Valid for TX buffers only */

   unsigned char   ps_poll_sent;                                /** Valid for TX buffers that were sent on PS-POLL */
   void            *orig_txq;                                   /** Valid for TX buffers that are PS-POLL queued */
   unsigned long   queuing_time;                                /** Valid for TX buffers that are PS-POLL queued */
   unsigned char   queued_retry;                                /** SpectraLink Phone changes **/
   int             queued_retry_ctr;                            /** SpectraLink Phone changes **/
   unsigned short  queued_retry_sqnr;
};

typedef struct meshap_buf   meshap_buf_t;

typedef struct
{
   int      rateCount;           /* NB: for proper padding */
   u_int8_t rateCodeToIndex[32]; /* back mapping */
   struct
   {
      u_int8_t  valid;          /* valid for rate control use */
      u_int8_t  phy;            /* CCK/OFDM/XR */
      u_int32_t rateKbps;       /* transfer rate in kbs */
      u_int8_t  rateCode;       /* rate for h/w descriptors */
      u_int8_t  shortPreamble;  /* mask for enabling short
                                 * preamble in CCK rate code */
      u_int8_t  dot11Rate;      /* value for supported rates
                                 * info element of MLME */
      u_int8_t  controlRate;    /* index of next lower basic
                                 * rate; used for dur. calcs */
      u_int16_t lpAckDuration;  /* long preamble ACK duration */
      u_int16_t spAckDuration;  /* short preamble ACK duration*/
   }        info[32];
} HAL_RATE_TABLE;

typedef struct
{
   u_int    rs_count;           /* number of valid entries */
   u_int8_t rs_rates[32];       /* rates */
} HAL_RATE_SET;

typedef enum
{
   HAL_ANT_VARIABLE = 0,            /* variable by programming */
   HAL_ANT_FIXED_A  = 1,            /* fixed to 11a frequencies */
   HAL_ANT_FIXED_B  = 2,            /* fixed to 11b frequencies */
} HAL_ANT_SETTING;

typedef enum
{
   HAL_M_STA     = 1,        /* infrastructure station */
   HAL_M_IBSS    = 0,        /* IBSS (adhoc) station */
   HAL_M_HOSTAP  = 6,        /* Software Access Point */
   HAL_M_MONITOR = 8         /* Monitor mode */
} HAL_OPMODE;

typedef struct meshap_duty_cycle_ap_data {
    unsigned char                     essid[IW_ESSID_MAX_SIZE];
    unsigned char                     bssid[ETH_ALEN];
    unsigned char                     signal;
    unsigned int                      transmit_duration;
    unsigned int                      retry_duration;
    struct meshap_duty_cycle_ap_data* next;
}meshap_duty_cycle_ap_data_t;

typedef struct meshap_duty_cycle_data {
    meshap_duty_cycle_info_t       info;
    meshap_duty_cycle_ap_data_t    *ap_list;
    struct meshap_duty_cycle_data  *next;
}meshap_duty_cycle_data_t;

struct meshap_802_11_vht_operation {
    unsigned char  vht_op_info_chwidth;
    unsigned char  vht_op_info_chan_center_freq_seg0_idx;
    unsigned char  vht_op_info_chan_center_freq_seg1_idx;
    unsigned short int  vht_basic_mcs_set;
};

typedef struct meshap_802_11_vht_operation meshap_802_11_vht_operation_t ;

struct meshap_instance
{
   /**
    * Informational and software state members
    */
   struct net_device         *dev;                /* The linux net_device */
   struct meshap_instance*     next;              /*create linked list of instances, in case of if we create vlan*/
   unsigned int              flags;               /** MESHAP_INSTANCE_FLAGS_* */
   meshap_lock_t             lock;                /** Spin lock for the instance */
   void                      *dev_token;  
   int                       max_keys;            /** Max number of keys supported by H/W */
   unsigned char             key_bitmap[16];      /** Max 128 entries */
   unsigned char             original_mac[ETH_ALEN];
   int                       use_type;            /** Use type: WM, AP or DS **/
   int                       beacon_conf_complete;/** Indicates that all the params of instance are populated and master mode can be initialized*/

   struct net_device_stats   dev_stats;        /* device statistics */

   /**
    * 802.11 state members
    */

   atheros_dot11e_category_t *dot11e_category_config;                     /* 802.11e Category Info */

   int                       country_code;                                                /* Override default country code */
   int                       xchanmode;                                                   /* Enable/disable extended channel mode */
   int                       regdomain;                                                   /* Regulatory domain */
   unsigned char             priv_channel_bw;                                             /* Bandwidth to use for private channels */

   wlan_channel_t            *channels;                                                           /** Information about channel */
   int                       channel_count;
   wlan_channel_t            *current_channel;

   const HAL_RATE_TABLE      *hal_rates[WLAN_PHY_MODE_COUNT];                           /** Rates in the adaptor for every mode */
   const HAL_RATE_TABLE      *qtr_a_rates_table;                                        /** 5 MHz bandwidth rate table for 802.11a */
   const HAL_RATE_TABLE      *half_a_rates_table;                                       /** 10 MHz bandwidth rate table for 802.11a  */
   const HAL_RATE_TABLE      *turbo_a_rates_table;                                      /** 40 MHz bandwidth rate table for 802.11a  */
   const HAL_RATE_TABLE      *qtr_g_rates_table;                                        /** 5 MHz bandwidth rate table for 802.11a */
   const HAL_RATE_TABLE      *half_g_rates_table;                                       /** 10 MHz bandwidth rate table for 802.11a  */
   const HAL_RATE_TABLE      *turbo_g_rates_table;                                      /** 40 MHz bandwidth rate table for 802.11g  */
   unsigned char             rate_mask;                                                 /** Is either WLAN_RATE_MASK or is 0xFF */
   unsigned char             rate_ctrl_flag;                                            /** Hw/Sw Rate control related flags */

   int                       current_phy_mode;                                          /** One of WLAN_PHY_MODE_* */
   int                       current_phy_mode_channel_count;                            /** Number of channels in the current PHY mode */
   int                       iw_mode;                                                   /** One of IW_MODE_* */
   void                      *fsm_data;                                                 /** FSM private data */
   int                       fsm_state;                                                 /** MESHAP_STA_FSM_STATE_**/
   const HAL_RATE_TABLE      *current_rate_table;                                       /** hal_rates[current_phy_mode] */

   const unsigned char       *supported_rates;                                          /** 802.11 supported rates IE content */
   unsigned char             supported_rates_length;                                    /** 802.11 supported rates IE length */
   const unsigned char       *extended_rates;                                           /** 802.11 ex-supported rates IE content */
   unsigned char             extended_rates_length;                                     /** 802.11 ex-supported rates IE length */

   int                       beacon_interval;                                           /** Valid for STA and AP mode */
   int                       rts_threshold;                                             /** Valid for STA and AP mode */
   int                       frag_threshold;                                            /** Valid for STA and AP mode */
   int                       tx_power;                                                  /** Valid for STA and AP mode */
   char                      current_essid[WLAN_ESSID_MAX_SIZE + 1];                    /** Valid for STA and AP mode */
   int                       current_essid_length;                                      /** Valid for STA and AP mode */
   int                       hide_essid;                                                /** Valid for AP mode */

   u64                       beacon_timestamp;                                          /** Valid in STA mode only (little-endian)*/
   unsigned char             current_bssid[ETH_ALEN];                                   /** Valid in STA mode only */
   unsigned char             current_virtual_bssid[ETH_ALEN];                           /** Valid in STA mode only */
   unsigned char             connected;                                                 /** Valid in STA mode only */
   unsigned char             old_connected;                                             /** Valid in STA mode only */
   unsigned long             last_beacon_time;                                          /** Valid in STA mode only */
   unsigned long             last_rx_time;                                              /** Valid in STA mode only */
   int                       bmiss_count;                                               /** Valid in STA mode only */
   int                       deauth_count;                                              /** Valid in STA mode only */

   unsigned short            default_capability;                                        /** Valid for STA and AP mode */
   unsigned short            capability;                                                /** Valid for AP mode */
   unsigned char             extended_rate_phy_info;                                    /** Valid for STA and AP mode in 802.11bg and 802.11g phy modes */
   unsigned char             slot_time_status;                                          /** Valid for AP mode (MESHAP_INSTANCE_SLOT_TIME_STATUS_*) */

   struct open_rt_ctrl       *rate_ctrl;                                                /** Valid for STA and AP mode */
   atomic_t                  power_save_sta_count;                                      /** Valid for AP mode */
   unsigned int              power_save_sta_verification_count;                         /** Valid for AP mode */

   atheros_essid_info_t      *essid_info;                                               /** essid info for vlan essid checking */
   int                       essid_info_count;

   /**
    * TX members
    */

   MESHAP_TAILQ_DEFINE_LIST(meshap_buf)  tx_buf;
   meshap_lock_t             tx_buf_lock;                                                                       /* txbuf lock */
   meshap_txq_t              cab_q;                                                                                     /* Crap after beacon queue */
   struct meshap_tx_pool     *tx_pool;

   /**
    * Monitor mode mask
    */

   unsigned int              monitor_mode_mask;                                                                 /* Valid only in monitor mode */

   /**
    * Beacon members
    */

   u_int                     hal_beacon_queue;                                                                          /* HAL queue number for outgoing beacons */
   meshap_buf_t              *beacon_buffer;
   int                       beacon_buffer_padding;                                                                     /* 32 bit padding for beacon buffer */
   int                       beacon_length;

   /**
    * Timer queues
    */

   struct timer_list         calibration_timer;                                                                 /* calibration timer */
   int                       calibration_interval;
   unsigned long             last_calibration_load;
   int                       calibration_count;
   unsigned long             last_nol_check;

   struct timer_list         bmiss_verification_timer;                                                  /* verifies if the parent AP is really gone */
   unsigned long             last_on_phy_link_notify;
   struct timer_list         on_phy_link_notify_watchdog;

   struct timer_list         beacon_interrupt_timer;                                                    /* watchdog timer for beacon interrupt */
   unsigned long             last_beacon_interrupt_time;
   unsigned int              max_beacon_tx_pending;
   unsigned int              beacon_skip_count;
   unsigned long             max_intra_beacon_interval;

   meshap_rx_info_t          rx_info[MESHAP_INSTANCE_RX_INFO_MAX];                      /* Per-antenna RX information */

   /*used for beaconing uplink*/

   struct timer_list         beaconing_uplink_start_timer;                                      /* timer for beacon TX */
   unsigned long             last_beacon_sent_time;
   unsigned long             first_beacon_sent_time;
   unsigned int              max_beaconing_uplink_timeout;

   struct proc_dir_entry     *proc_entry;

   unsigned char             rsn_ie_buffer[256];
   int                       rsn_ie_length;

   int                       ds_encryption_key_index;

   int                       fixed_tx_rate_index;                                       /** 0 means Auto rate selection */

   /**
    *	Beacon Vendor Info
    */

   unsigned char             beacon_vendor_info[256];
   unsigned char             beacon_vendor_info_length;
   unsigned char             beacon_vendor_info_updated;

   /**
    * TX POOL/DESC recovery
    */

   unsigned int              distance_in_meters;
   int                       dtim_count;
   int                       dtim_count_current;

#ifdef MESHAP_NO_POWER_SAVE_MODE
   unsigned char             *beacon_vendor_info_offset;
   int                       dtim_count_offset;
   int                       beacon_capability_offset;
   int                       beacon_erp_offset;
#else
   unsigned char             tim_bitmap[WLAN_TIM_MAX_BITMAP_SIZE];
   atomic_t                  bcast_ps_queued_count;
#endif

   void                      *duty_cycle_data;

   void                      *round_robin_info;
   void                      *round_robin_hook;
   void                      *radar_hook;
   void                      *probe_request_hook;
   unsigned char             virtual_associated;
   unsigned char             ds_verification_enabled;

   struct timer_list         dummy_timer;

   /**
    * The tx_retry_count is used when EFFISTREAM is enabled. It is
    * meant for experimental use only. The variable is initialized to 1.
    */

   int                       tx_retry_count;

   /* interrupt counters for debugging purpose */
   unsigned int              probe_req_rx_count;
   unsigned int              probe_resp_tx_count;

   /* entries for virtual interfaces */

   unsigned char             device_mode;  /** NORMAL or MIXED **/
   u16			 ht_capab_info; /*part of EID 45*/
   u16			 ht_opera; /*part of EID 61*/
   int sec_chan_offset;   /*required to check 40+/40- channel bandwidth supported*/
   u32			 vht_capab_info; /*part of EID 45*/
   u8            a_mpdu_params ;
   meshap_802_11_vht_operation_t    vht_opera ;
   int scan_state;
   unsigned long scan_start_time;
   unsigned long scan_completion_time;
   al_net_if_stat_t net_if_stats;

   /*Duty cycle param*/
   struct kfifo             dci_fifo;
   struct completion        completion;
   meshap_duty_cycle_data_t dutycycle_data;
   unsigned int             duty_cycle_flags;
   int                      duty_cycle_set_channel_flag;                          /* Set the flag while doing duty cycle to set the channel */
   int                      wm_duty_cycle_flag;
};

typedef struct meshap_instance   meshap_instance_t;

void meshap_free_mem(struct sk_buff *skb);

#ifdef ATHEROS_NO_POWER_SAVE_MODE
int meshap_setup_beacon_frame(meshap_instance_t *instance, int *capability_ofset, int *erp_offset, unsigned char **beacon_vendor_info_offset, int *dtim_count_offset);

#else
int meshap_setup_beacon_frame(meshap_instance_t *instance);
#endif

void meshap_setup_defaults(meshap_instance_t *instance);
meshap_instance_t *meshap_mac80211_alloc_instance(void);
meshap_instance_t *meshap_get_instance(struct net_device *dev);
meshap_instance_t *meshap_add_instance(struct net_device *phy_instance, struct net_device *vif_instance);

void meshap_init_infra_mode(struct meshap_instance *instance);
void meshap_uninit_infra_mode(struct meshap_instance *instance);
void meshap_init_master_mode(struct meshap_instance *instance);
void meshap_uninit_master_mode(struct meshap_instance *instance);
void _meshap_init_beacon_buffer(struct meshap_instance *instance);
int meshap_setup_beacon_buffer(meshap_instance_t *instance, int length);
void _meshap_init_channels(struct net_device *dev);
void _meshap_set_use_type(struct net_device *dev, int use_type);
void _meshap_set_all_conf_complete(struct net_device *dev, int value);
void meshap_bmiss_verification_timer(unsigned long arg);
int is_11n_supported(meshap_instance_t *instance, char band);
int meshap_build_ht_capability_ie(meshap_instance_t *instance, u8 *eid);
int meshap_build_ht_operation_ie(meshap_instance_t *instance, u8 *eid);
int meshap_build_wmm_ie(meshap_instance_t *instance, u8 *eid);
int meshap_build_vht_capability_ie(meshap_instance_t *instance, u8 *eid);
int is_11ac_supported(meshap_instance_t *instance);
int meshap_build_vht_operation_ie(meshap_instance_t *instance, u8 *eid);
int meshap_80211n_allowed_ht40_channel_pair(meshap_instance_t *instance);
#define MESHAP_INSTANCE_SIGNATURE                      0x29531299

#define MESHAP_INSTANCE_FLAGS_INITIALIZED              0x00000001
#define MESHAP_INSTANCE_FLAGS_MR_RETRY                 0x00000002
#define MESHAP_INSTANCE_FLAGS_SOFT_LED                 0x00000004
#define MESHAP_INSTANCE_FLAGS_SPLIT_MIC                0x00000008
#define MESHAP_INSTANCE_FLAGS_NEED_MIB                 0x00000010
#define MESHAP_INSTANCE_FLAGS_WEP_ENABLE               0x00000020
#define MESHAP_INSTANCE_FLAGS_RSN_IE_ENABLE            0x00000040
#define MESHAP_INSTANCE_FLAGS_DS_LINK_ENCRYPTED        0x00000080
#define MESHAP_INSTANCE_FLAGS_A_SUPPORTED              0x00000100
#define MESHAP_INSTANCE_FLAGS_B_SUPPORTED              0x00000200
#define MESHAP_INSTANCE_FLAGS_G_SUPPORTED              0x00000400
#define MESHAP_INSTANCE_FLAGS_PURE_G_SUPPORTED         0x00000800
#define MESHAP_INSTANCE_FLAGS_DUTY_CYCLE               0x00001000
#define MESHAP_INSTANCE_FLAGS_MASTER_QUIET             0x00002000
#define MESHAP_INSTANCE_FLAGS_DS_LINK_COMPRESSED       0x00004000
#define MESHAP_INSTANCE_FLAGS_NO_CALIBRATION           0x00008000
#define MESHAP_INSTANCE_FLAGS_TX_ERROR_PRINT           0x00010000
#define MESHAP_INSTANCE_FLAGS_NO_PROBE_RESP            0x00020000
#define MESHAP_INSTANCE_FLAGS_NO_BEACON_DMA_CHECK      0x00040000
#define MESHAP_INSTANCE_FLAGS_DROP_TX                  0x00080000
#define MESHAP_INSTANCE_FLAGS_DROP_RX                  0x00100000
#define MESHAP_INSTANCE_FLAGS_RX_ON_ALL_ANTENNA        0x00200000
#define MESHAP_INSTANCE_FLAGS_IGNORE_TX_ERROR_CHECK    0x00400000
#define MESHAP_INSTANCE_FLAGS_N_SUPPORTED              0x00800000
#define MESHAP_INSTANCE_FLAGS_PURE_N_SUPPORTED         0x01000000
#define MESHAP_INSTANCE_FLAGS_AC_SUPPORTED             0x02000000
#define MESHAP_INSTANCE_FLAGS_ANAC_SUPPORTED           0x04000000

#define MESHAP_INSTANCE_INITIALIZED(instance)           ((instance)->flags & MESHAP_INSTANCE_FLAGS_INITIALIZED)

#define MESHAP_INSTANCE_LOCK_INIT(instance)             _meshap_lock_init(&(instance)->lock, "inst")
#define MESHAP_INSTANCE_LOCK_DESTROY(instance)          _meshap_lock_destroy(&(instance)->lock)

#define MESHAP_INSTANCE_LOCK(_inst, _flg)               MESHAP_LOCK_LOCK(&(_inst)->lock, _flg)
#define MESHAP_INSTANCE_UNLOCK(_inst, _flg)             MESHAP_LOCK_UNLOCK(&(_inst)->lock, _flg)

#define MESHAP_INSTANCE_TXBUF_LOCK_INIT(instance)       _meshap_lock_init(&(instance)->tx_buf_lock, "txbuf")
#define MESHAP_INSTANCE_TXBUF_LOCK_DESTROY(instance)    _meshap_lock_destroy(&(instance)->tx_buf_lock)

#define MESHAP_INSTANCE_TXBUF_LOCK(_inst, _flg)         MESHAP_LOCK_LOCK(&(_inst)->tx_buf_lock, _flg)
#define MESHAP_INSTANCE_TXBUF_UNLOCK(_inst, _flg)       MESHAP_LOCK_UNLOCK(&(_inst)->tx_buf_lock, _flg)


#define MESHAP_INSTANCE_TXQ_IS_SETUP(instance, qnum)    ((instance)->txq_setup_flags & (1 << qnum))


#ifndef MESHAP_NO_POWER_SAVE_MODE

#define MESHAP_TXQ_LOCK_INIT(_tq)       _meshap_lock_init(&(_tq)->lock, "txq")
#define MESHAP_TXQ_LOCK_DESTROY(_tq)    _meshap_lock_destroy(&(_tq)->lock)
#define MESHAP_TXQ_LOCK(_tq, _flg)      MESHAP_LOCK_LOCK(&(_tq)->lock, _flg)
#define MESHAP_TXQ_UNLOCK(_tq, _flg)    MESHAP_LOCK_UNLOCK(&(_tq)->lock, _flg)

static inline void meshap_clear_dtim_bitmap_bit(struct meshap_instance *instance, unsigned short aid)
{
   unsigned short index;
   unsigned char  bit;
   unsigned long  flags;

   index = aid / 8;
   bit   = aid % 8;

   MESHAP_INSTANCE_LOCK(instance, flags);

   instance->tim_bitmap[index] &= ~(1 << bit);

   MESHAP_INSTANCE_UNLOCK(instance, flags);
}
#endif

/* Rate control related flags */
#define MESHAP_HW_HAS_RATE_CONTROL              (0x1)
#define MESHAP_SW_BASED_RATE_CONTROL            (0x2)

#endif /*__MESHAP_INSTANCE_H__*/
