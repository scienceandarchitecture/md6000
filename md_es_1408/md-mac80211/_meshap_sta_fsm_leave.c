/********************************************************************************
* MeshDynamics
* --------------
* File     : _meshap_sta_fsm_leave.c
* Comments : Atheros STA fsm leave entry point
* Created  : 10/18/2004
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  3  |01/07/2008| Check for mixed mode added.                     |Prachiti|
* -----------------------------------------------------------------------------
* |  2  |4/6/2005  | Security changes                                | Sriram |
* -----------------------------------------------------------------------------
* |  1  |12/23/2004| Implemented link notify watchdog                | Sriram |
* -----------------------------------------------------------------------------
* |  0  |10/18/2004| Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

/**
 * This is a private source file included by meshap_sta_fsm.c, hence
 * the name begins with an underscore.
 * It was created to improve readability.
 */
void meshap_sta_fsm_reset(meshap_instance_t *instance)
{
   _meshap_sta_fsm_data_t *data;
   unsigned long          flags;

   data = (_meshap_sta_fsm_data_t *)instance->fsm_data;

   OPENFSM_LOCK(data->fsm, flags);

   openfsm_reset(data->fsm, &_states[_MESHAP_STA_FSM_DISCONNECTED]);
   openfsm_set_state_data(data->fsm, NULL);
   instance->connected = 0;
   instance->fsm_state = MESHAP_STA_FSM_STATE_DISCONNECTED;
   instance->last_on_phy_link_notify = jiffies;

   if (instance->device_mode != MESHAP_DEV_MODE_MIXED)
   {
      instance->beacon_interval = 0;
   }

   OPENFSM_UNLOCK(data->fsm, flags);
}


int meshap_sta_fsm_leave(meshap_instance_t *instance, int notify, unsigned short reason)
{
    int ret;
   _meshap_sta_fsm_data_t            *data;
   unsigned long                     flags;
   struct cfg80211_disassoc_request  req;
   struct ieee80211_channel          *chan;
   struct wireless_dev               *wdev = NULL;
   struct cfg80211_registered_device *rdev = NULL;
   u16 freq = 0;

   data = (_meshap_sta_fsm_data_t *)instance->fsm_data;

   /**
    * Here we send of a deauth notification and simply move to the
    * disconnected state. We also inform meshap.o about the incident
    * using on link notify.
    */

   if (!instance->connected)
   {
      return 0;
   }

   if (notify)
   {
       wdev = instance->dev->ieee80211_ptr;

       rdev = WIPHY_TO_DEV(wdev->wiphy);

       memset(&req, 0, sizeof(req));

       freq = instance->current_channel->frequency;
       chan = ieee80211_get_channel(wdev->wiphy, freq);
       req.bss = cfg80211_get_bss(wdev->wiphy, chan, instance->current_bssid, instance->current_essid, instance->current_essid_length,
           instance->default_capability, instance->default_capability);

       if (req.bss)
       {
           req.reason_code        = cpu_to_le16(reason);
           req.local_state_change = 0;
           ASSERT_WDEV_LOCK(wdev);
           ret = rdev->ops->disassoc(wdev->wiphy, instance->dev, &req);
           if (ret != 0)
           {
               al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP:ERROR: %s<%d> Looks like MDE parent and kernel "
			   "parent data mismatch. req.bss.bssid ="MACSTR" and current_bssid = "MACSTR"\n", __func__, __LINE__,
			   MAC2STR(req.bss->bssid), MAC2STR(instance->current_bssid));
           }
       }
       instance->connected = 0;
   }

   OPENFSM_LOCK(data->fsm, flags);

   openfsm_reset(data->fsm, &_states[_MESHAP_STA_FSM_DISCONNECTED]);
   openfsm_set_state_data(data->fsm, NULL);
   instance->fsm_state = MESHAP_STA_FSM_STATE_DISCONNECTED;
   instance->last_on_phy_link_notify = jiffies;

   if (instance->device_mode != MESHAP_DEV_MODE_MIXED)
   {
      instance->beacon_interval = 0;
   }

   OPENFSM_UNLOCK(data->fsm, flags);

   if (instance->flags & MESHAP_INSTANCE_FLAGS_DS_LINK_ENCRYPTED)
   {
      instance->flags &= ~MESHAP_INSTANCE_FLAGS_DS_LINK_ENCRYPTED;
   }


   meshap_on_link_notify(instance->dev,
                         instance->dev_token,
                         MESHAP_ON_LINK_NOTIFY_STATE_LINK_OFF | MESHAP_ON_LINK_NOTIFY_STATE_DISASSOCIATED);

   return 0;
}


int meshap_sta_fsm_leave_beaconmiss(meshap_instance_t *instance, int notify, unsigned short reason)
{
    int ret;
   _meshap_sta_fsm_data_t            *data;
   unsigned long                     flags;
   struct cfg80211_disassoc_request  req;
   struct ieee80211_channel          *chan;
   struct wireless_dev               *wdev = NULL;
   struct cfg80211_registered_device *rdev = NULL;
   u16 freq = 0;

   data = (_meshap_sta_fsm_data_t *)instance->fsm_data;

   /**
    * Here we send of a deauth notification and simply move to the
    * disconnected state. We also inform meshap.o about the incident
    * using on link notify.
    */

   if (!instance->connected)
   {
      return 0;
   }

   OPENFSM_LOCK(data->fsm, flags);

   openfsm_reset(data->fsm, &_states[_MESHAP_STA_FSM_DISCONNECTED]);
   openfsm_set_state_data(data->fsm, NULL);
   instance->fsm_state = MESHAP_STA_FSM_STATE_DISCONNECTED;
   instance->last_on_phy_link_notify = jiffies;

   if (instance->device_mode != MESHAP_DEV_MODE_MIXED)
   {
      instance->beacon_interval = 0;
   }

   OPENFSM_UNLOCK(data->fsm, flags);

   if (instance->flags & MESHAP_INSTANCE_FLAGS_DS_LINK_ENCRYPTED)
   {
      instance->flags &= ~MESHAP_INSTANCE_FLAGS_DS_LINK_ENCRYPTED;
   }

  // meshap_set_mesh_network_unstable();

   meshap_on_link_notify(instance->dev,
                         instance->dev_token,
                         MESHAP_ON_LINK_NOTIFY_STATE_LINK_OFF | MESHAP_ON_LINK_NOTIFY_BMISS);

   return 0;
}

void md_meshap_sta_fsm_leave_beaconmiss(struct ieee80211_sub_if_data *sdata)
{
   int use_type;
   use_type = sdata->local->hw.instance->use_type;
   if (use_type == AL_CONF_IF_USE_TYPE_DS) {

             al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"STA_FSM:INFO: %s<%d> %s: BEACONMISS NEW::: "
			 "Beacons missed and no RX, disassociating...\n",__func__, __LINE__, sdata->local->hw.instance->dev->name);
				/*BEACONMISS: do only clearing of mesh state machine, let kernel send disassoc() */
            meshap_sta_fsm_leave_beaconmiss(sdata->local->hw.instance, 0, WLAN_REASON_DISASSOC_DUE_TO_INACTIVITY);
            al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"STA_FSM:INFO: %s<%d> %s: Done Disassociating...\n",
			__func__, __LINE__, sdata->local->hw.instance->dev->name);
            /*Enable tx queues which were disabled by mac80211 while sending disassoc frame */
            netif_tx_start_all_queues(sdata->dev);
            netif_carrier_on(sdata->dev);
	}
}
