/********************************************************************************
* MeshDynamics
* --------------
* File     : _meshap_sta_fsm_active_scan.c
* Comments : Atheros STA FSM active scan entry function
* Created  : 10/18/2004
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  4  |4/11/2005 | Misc changes                                    | Sriram |
* -----------------------------------------------------------------------------
* |  3  |12/23/2004| Parameter added to meshap_reset                | Sriram |
* -----------------------------------------------------------------------------
* |  1  |12/22/2004| Misc changes                                    | Sriram |
* -----------------------------------------------------------------------------
* |  0  |10/18/2004| Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

/**
 * This is a private source file included by meshap_sta_fsm.c, hence
 * the name begins with an underscore.
 * It was created to improve readability.
 */

/**
 * Both the active scan and the passive scan
 * assume that the FSM is now in the disconnected
 * state. Assumes current PHY mode has been set.
 */
#include <net/wireless/core.h>
int ieee80211_set_channel(struct wiphy              *wiphy,
                          struct net_device         *netdev,
                          struct ieee80211_channel  *chan,
                          enum nl80211_channel_type channel_type);

int meshap_sta_fsm_active_scan(meshap_instance_t            *instance,
                               int                          channel_count,
                               unsigned char                *channels,
                               int                          dwell_timeout,
                               meshap_sta_fsm_scan_result_t **results,
                               int                          *results_count)
{
   _meshap_sta_fsm_data_t *data;
   int channel;
   int freq;
   _scan_param_t             *param;
   enum nl80211_channel_type channel_type = NL80211_CHAN_NO_HT;
//1602MIG replacing ieee80211_set_channel with ieee80211_vif_use_channel
   struct wireless_dev *wdev = instance->dev->ieee80211_ptr;
   struct cfg80211_registered_device *rdev;
   struct cfg80211_chan_def chandef;
   struct ieee80211_sub_if_data *sdata = IEEE80211_DEV_TO_SUB_IF(instance->dev);   //1602MIG from 1602
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Enter %s: %d\n", __func__, __LINE__);
   DEFINE_WAIT(wait);

   data    = (_meshap_sta_fsm_data_t *)instance->fsm_data;
   channel = -1;
   param   = (_scan_param_t *)kmalloc(sizeof(_scan_param_t), GFP_ATOMIC);
   if (param == NULL)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MAC80211 : ERROR : %s<%d> Memory alloc failed \n ",__func__, __LINE__);
      return -1;
   }

   memset(param, 0, sizeof(_scan_param_t));

   instance->connected = 0;
   instance->fsm_state = MESHAP_STA_FSM_STATE_DISCONNECTED;

   openfsm_reset(data->fsm, &_states[_MESHAP_STA_FSM_DISCONNECTED]);
   openfsm_set_state_data(data->fsm, NULL);

   param->total_results = 0;

   /*update channel_type based on secondary channel offset*/
	if(instance->sec_chan_offset == 1) {
		channel_type = NL80211_CHAN_HT40PLUS;
	}else if(instance->sec_chan_offset == -1) {
		channel_type = NL80211_CHAN_HT40MINUS;
	}

   while (1)
   {
      if ((channel_count == 0) || (channels == NULL) ||
          ((channel_count == 1) && (channels[0] == 0)) /* Paranoid protection againts illegal values */)
      {
         channel = _set_next_channel(instance, channel);
         if (channel < 0)
         {
            break;
         }
      }
      else
      {
         if (++channel >= channel_count)
         {
            break;
         }
         instance->current_channel = &instance->channels[channels[channel]];
      }

      /**
       * Reset the device so that channel change takes place
       */

      //mesh_mac80211_reset_device(instance->dev,0);
      freq = instance->current_channel->frequency;

//1602MIG replacing ieee80211_set_channel with ieee80211_vif_use_channel
      //ieee80211_set_channel(wdev->wiphy, instance->dev, chan, channel_type);
      rdev = WIPHY_TO_DEV(wdev->wiphy);  //Commented like IXP
      memset(&chandef,0,sizeof(chandef));
 	  //chandef.chan = ieee80211_get_channel(rdev, freq);
	  cfg80211_chandef_create(&chandef, ieee80211_get_channel((wdev->wiphy), freq) /*&chandef.chan*/, channel_type);
	  if (!chandef.chan) {
	    break;
	  }
	  mutex_lock(&sdata->local->mtx);
      if (ieee80211_vif_use_channel(sdata, &chandef, IEEE80211_CHANCTX_SHARED))
	  {
         al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MAC80211 : ERROR : Failed to set channel for %s %s: %d\n",instance->dev->name,  __func__, __LINE__);
	  }
	  mutex_unlock(&sdata->local->mtx);


      /**
       * Transition to state _MESHAP_STA_FSM_ASCAN_1, which
       * sends probe request and transitions to _MESHAP_STA_FSM_ASCAN_2
       * and waits for response until timeout and notes down all responses.
       * It then transitions back to disconnected state.
       */

      openfsm_transition(data->fsm, &_states[_MESHAP_STA_FSM_ASCAN_1]);

      param->dwell_timeout = dwell_timeout;
      param->total_results = 0;

      /**
       * Transition simply leaves the previous state and enters the
       * new one. After that we need to explicitly call the work
       * function, which does the real work.
       */

      openfsm_work(data->fsm, (void *)param, NULL);

      /**
       * Goto sleep and get awakened by _MESHAP_STA_FSM_ASCAN_2
       */

      INTERRUPTIBLE_SLEEP_ON_TIMEOUT(&data->wqh, ((dwell_timeout * 2 * HZ)  / 1000), &wait);

      /**
       * Now we have the results, continue to the next channel
       */
   }

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : DEBUG : Changing state to _MESHAP_STA_FSM_DISCONNECTED %s: %d\n", __func__, __LINE__);
   openfsm_reset(data->fsm, &_states[_MESHAP_STA_FSM_DISCONNECTED]);
   openfsm_set_state_data(data->fsm, NULL);

   /**
    * Allocate memory for the results o/p parameter
    */

   *results = (meshap_sta_fsm_scan_result_t *)kmalloc(param->total_results * sizeof(meshap_sta_fsm_scan_result_t), GFP_ATOMIC);

   memcpy(*results, param->results, param->total_results * sizeof(meshap_sta_fsm_scan_result_t));

   *results_count = param->total_results;

   kfree(param);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Exit %s: %d\n", __func__, __LINE__);
   return 0;
}
