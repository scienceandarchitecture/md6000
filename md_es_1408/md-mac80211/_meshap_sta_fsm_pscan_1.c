/********************************************************************************
* MeshDynamics
* --------------
* File     : _meshap_sta_fsm_pscan_1.c
* Comments : Atheros 802.11 FSM pscan_1 state code
* Created  : 10/13/2004
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  0  |10/13/2004| Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

/**
 * This is a private source file included by meshap_sta_fsm.c, hence
 * the name begins with an underscore.
 * It was created to improve readability.
 */

OPENFSM_ENTER_STATE_FUNCTION(pscan_1)
{
   return 0;
}

OPENFSM_STATE_FUNCTION(pscan_1)
{
   return 0;
}

OPENFSM_LEAVE_STATE_FUNCTION(pscan_1)
{
   return 0;
}

OPENFSM_STATE_TIMER_FUNCTION(pscan_1)
{
#if 0
   _scan_param_t          *param;
   _meshap_sta_fsm_data_t *data;
   unsigned long          flags;

   /**
    * state_timer_count indicates the number of time units that
    * have elapsed in this state. If the total time is greater than
    * dwell_timeout, we wake up the sleeping thread.
    */

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Enter %s: %d\n", __func__, __LINE__);
   OPENFSM_LOCK(fsm, flags);

   param = (_scan_param_t *)state_data;
   data  = (_meshap_sta_fsm_data_t *)openfsm_get_fsm_data(fsm);

   if (state_timer_count * MESHAP_STA_FSM_TIMER_INTERVAL > param->dwell_timeout)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_DEBUG, "MAC80211 : DEBUG : Wakeup thread %s: %d\n", __func__, __LINE__);
      wake_up(&data->wqh);
   }

   OPENFSM_UNLOCK(fsm, flags);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Exit %s: %d\n", __func__, __LINE__);
#endif
   return 0;
}
