/********************************************************************************
* MeshDynamics
* --------------
* File     : meshap_round_robin.h
* Comments : Atheros Mesh downlink round robin implementation
* Created  : 4/4/2006
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  0  |4/4/2006 | Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

#ifndef __MESHAP_ROUND_ROBIN_H__
#define __MESHAP_ROUND_ROBIN_H__

extern const unsigned char meshap_round_robin_info_header[4];

void meshap_round_robin_initialize(meshap_instance_t *instance);
void meshap_round_robin_uninitialize(meshap_instance_t *instance);

void meshap_round_robin_set_count(meshap_instance_t *instance, int count);
void meshap_round_robin_add_address(meshap_instance_t *instance, unsigned char *address);
void meshap_round_robin_remove_address(meshap_instance_t *instance, unsigned char *address);
int meshap_round_robin_set_beacon_content(meshap_instance_t *instance, unsigned char *p);

#endif /*__MESHAP_ROUND_ROBIN_H__*/
