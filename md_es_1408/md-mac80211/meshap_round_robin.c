/********************************************************************************
* MeshDynamics
* --------------
* File     : meshap_round_robin.c
* Comments : Atheros Downlink Round Robin Implementation
* Created  : 4/4/2006
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  0  |4/4/2006  | Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

#include <linux/version.h>

#if (LINUX_VERSION_CODE < KERNEL_VERSION(2, 6, 19))
#include <linux/config.h>
#else
#include <generated/autoconf.h>
#endif

#include <linux/version.h>
#include <linux/module.h>
#include <linux/init.h>
#include <linux/if.h>
#include <linux/netdevice.h>
#include <linux/skbuff.h>
#include <linux/cache.h>
#include <linux/pci.h>
#include <linux/wireless.h>
#include <asm/uaccess.h>
#include <net/iw_handler.h>
#include <linux/delay.h>
#include <linux/interrupt.h>

#include "meshap.h"

#include "meshap_instance.h"
#include "meshap_round_robin.h"

struct _meshap_round_robin_list
{
   unsigned char                   address[ETH_ALEN];
   struct _meshap_round_robin_list *prev;
   struct _meshap_round_robin_list *next;
};

typedef struct _meshap_round_robin_list   _meshap_round_robin_list_t;

struct _meshap_round_robin_data
{
   int                        count;
   int                        current_count;
   _meshap_round_robin_list_t *head;
   _meshap_round_robin_list_t *tail;
   _meshap_round_robin_list_t *current_ptr;
};

typedef struct _meshap_round_robin_data   _meshap_round_robin_data_t;

const unsigned char meshap_round_robin_info_header[4] =
{
   0x00, 0x12, 0xCE, 0x02
};

void meshap_round_robin_initialize(meshap_instance_t *instance)
{
   _meshap_round_robin_data_t *data;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Enter %s<%d>\n",__func__,__LINE__);
   data = (_meshap_round_robin_data_t *)kmalloc(sizeof(_meshap_round_robin_data_t), GFP_ATOMIC);

   memset(data, 0, sizeof(_meshap_round_robin_data_t));

   data->count         = -1;
   data->current_count = -1;

   instance->round_robin_info = data;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Exit %s<%d>\n",__func__,__LINE__);
}


void meshap_round_robin_uninitialize(meshap_instance_t *instance)
{
   _meshap_round_robin_data_t *data;
   _meshap_round_robin_list_t *temp;
   unsigned long              flags;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Enter %s<%d>\n",__func__,__LINE__);
   MESHAP_INSTANCE_LOCK(instance, flags);
   data = (_meshap_round_robin_data_t *)instance->round_robin_info;

   temp = data->head;

   while (temp != NULL)
   {
      data->head = temp->next;
      kfree(temp);
      temp = data->head;
   }
   MESHAP_INSTANCE_UNLOCK(instance, flags);

   kfree(data);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Exit %s<%d>\n",__func__,__LINE__);
}


void meshap_round_robin_set_count(meshap_instance_t *instance, int count)
{
   _meshap_round_robin_data_t *data;
   unsigned long              flags;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Enter %s<%d>\n",__func__,__LINE__);
   MESHAP_INSTANCE_LOCK(instance, flags);

   data = (_meshap_round_robin_data_t *)instance->round_robin_info;

   data->count         = count;
   data->current_count = count;
   data->current_ptr   = NULL;
   MESHAP_INSTANCE_UNLOCK(instance, flags);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Exit %s<%d> %s: Setting downlink round robin time to %d intervals\n",
   __func__,__LINE__, instance->dev->name, count);
}


/** Assumes instance has been locked */
static _meshap_round_robin_list_t *_get_round_robin_address(_meshap_round_robin_data_t *data, unsigned char *address)
{
   _meshap_round_robin_list_t *temp;

   temp = data->head;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Enter %s<%d>\n",__func__,__LINE__);
   while (temp != NULL)
   {
      if (!memcmp(temp->address, address, ETH_ALEN))
      {
         break;
      }
      temp = temp->next;
   }
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Exit %s<%d>\n",__func__,__LINE__);

   return temp;
}


void meshap_round_robin_add_address(meshap_instance_t *instance, unsigned char *address)
{
   _meshap_round_robin_data_t *data;
   unsigned long              flags;
   _meshap_round_robin_list_t *temp;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Enter %s<%d>\n",__func__,__LINE__);
   MESHAP_INSTANCE_LOCK(instance, flags);

   data = (_meshap_round_robin_data_t *)instance->round_robin_info;

   temp = _get_round_robin_address(data, address);

   if (temp != NULL)
   {
      MESHAP_INSTANCE_UNLOCK(instance, flags);
      return;
   }

   MESHAP_INSTANCE_UNLOCK(instance, flags);

   temp = (_meshap_round_robin_list_t *)kmalloc(sizeof(_meshap_round_robin_list_t), GFP_ATOMIC);
   memset(temp, 0, sizeof(_meshap_round_robin_list_t));
   memcpy(temp->address, address, ETH_ALEN);

   MESHAP_INSTANCE_LOCK(instance, flags);

   if (data->head == NULL)
   {
      data->head = temp;
      data->tail = temp;
   }
   else
   {
      temp->prev       = data->tail;
      temp->next       = NULL;
      data->tail->next = temp;
      data->tail       = temp;
   }

   MESHAP_INSTANCE_UNLOCK(instance, flags);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Exit %s<%d> %s: Added downlink round robin address "MACSTR "\n",
   __func__,__LINE__, instance->dev->name, MAC2STR(address));
}


void meshap_round_robin_remove_address(meshap_instance_t *instance, unsigned char *address)
{
   _meshap_round_robin_data_t *data;
   unsigned long              flags;
   _meshap_round_robin_list_t *temp;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Enter %s<%d>\n",__func__,__LINE__);
   MESHAP_INSTANCE_LOCK(instance, flags);

   data = (_meshap_round_robin_data_t *)instance->round_robin_info;

   temp = _get_round_robin_address(data, address);

   if (temp != NULL)
   {
      if (temp->prev != NULL)
      {
         temp->prev->next = temp->next;
      }
      if (temp->next != NULL)
      {
         temp->next->prev = temp->prev;
      }

      if (temp == data->head)
      {
         data->head = data->head->next;
      }
      if (temp == data->tail)
      {
         data->tail = data->tail->prev;
      }
      if (temp == data->current_ptr)
      {
         data->current_ptr = data->current_ptr->prev;
      }
   }

   MESHAP_INSTANCE_UNLOCK(instance, flags);

   if (temp != NULL)
   {
      kfree(temp);
   }

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Exit %s<%d> %s: Removed downlink round robin address "MACSTR "\n",
   __func__,__LINE__, instance->dev->name, MAC2STR(address));
}


int meshap_round_robin_set_beacon_content(meshap_instance_t *instance, unsigned char *p)
{
   _meshap_round_robin_data_t *data;
   unsigned long              flags;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Enter %s<%d>\n",__func__,__LINE__);
   MESHAP_INSTANCE_LOCK(instance, flags);

   data = (_meshap_round_robin_data_t *)instance->round_robin_info;

   if (data->count == -1)
   {
      MESHAP_INSTANCE_UNLOCK(instance, flags);
      return 0;
   }

   if (--data->current_count <= 0)
   {
      data->current_count = data->count;

      if (data->current_ptr != NULL)
      {
         data->current_ptr = data->current_ptr->next;
      }

      if (data->current_ptr == NULL)
      {
         data->current_ptr = data->head;
      }

      if (data->current_ptr == NULL)
      {
         MESHAP_INSTANCE_UNLOCK(instance, flags);
         return 0;
      }

      p[0] = WLAN_EID_VENDOR_PRIVATE;
      p[1] = sizeof(meshap_round_robin_info_header) + ETH_ALEN;
      memcpy(&p[2], meshap_round_robin_info_header, sizeof(meshap_round_robin_info_header));
      memcpy(&p[2] + sizeof(meshap_round_robin_info_header), data->current_ptr->address, ETH_ALEN);

      MESHAP_INSTANCE_UNLOCK(instance, flags);

      return sizeof(meshap_round_robin_info_header) + ETH_ALEN + 2;
   }

   MESHAP_INSTANCE_UNLOCK(instance, flags);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Exit %s<%d>\n",__func__,__LINE__);

   return 0;
}
