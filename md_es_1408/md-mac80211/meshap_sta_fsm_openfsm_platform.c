/**
 * This is a private source file included by meshap_sta_fsm.c, hence
 * the name begins with an underscore.
 * It was created to improve readability.
 */

#include <linux/version.h>
#include <linux/module.h>
#include <linux/init.h>
#include <linux/if.h>
#include <linux/netdevice.h>
#include <linux/skbuff.h>
#include <linux/cache.h>
#include <linux/pci.h>
#include <linux/wireless.h>

#include <openfsm/openfsm-impl.h>

static void *_openfsm_platform_malloc(unsigned int size)
{
   return kmalloc(size, GFP_ATOMIC);
}


static void _openfsm_platform_free(void *memblock)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Enter %s: %d\n", __func__, __LINE__);
   kfree(memblock);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Exit %s: %d\n", __func__, __LINE__);
}


static unsigned int _openfsm_platform_create_lock(void)
{
   _openfsm_lock_t *l;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Enter %s: %d\n", __func__, __LINE__);
   l = (_openfsm_lock_t *)kmalloc(sizeof(_openfsm_lock_t), GFP_ATOMIC);

   spin_lock_init(&l->sl);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Exit %s: %d\n", __func__, __LINE__);
   return (unsigned int)l;
}


static void _openfsm_platform_destroy_lock(unsigned int lock)
{
   _openfsm_lock_t *l;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Enter %s: %d\n", __func__, __LINE__);
   l = (_openfsm_lock_t *)lock;

   kfree(l);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Exit %s: %d\n", __func__, __LINE__);
}
