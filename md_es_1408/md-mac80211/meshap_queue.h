#ifndef __MESHAP_QUEUE_H__
#define __MESHAP_QUEUE_H__

#include "queue.h"

#define MESHAP_TAILQ_DECLARE_MEMBER(type)                     STAILQ_ENTRY(type)
#define MESHAP_TAILQ_DEFINE_LIST(type)                        STAILQ_HEAD(, type)
#define MESHAP_TAILQ_INIT_LIST(list)                          STAILQ_INIT(list)
#define MESHAP_TAILQ_INSERT_TAIL(list, item, member)          STAILQ_INSERT_TAIL(list, item, member)
#define MESHAP_TAILQ_HEAD(list)                               STAILQ_FIRST(list)
#define MESHAP_TAILQ_REMOVE_HEAD(list, member)                STAILQ_REMOVE_HEAD(list, member)
#define MESHAP_TAILQ_FOR_EACH(var, list, member)              STAILQ_FOREACH(var, list, member)
#define MESHAP_TAILQ_IS_EMPTY(list)                           STAILQ_EMPTY(list)
#define MESHAP_TAILQ_NEXT(var, list)                          STAILQ_NEXT(var, list)
#define MESHAP_TAILQ_INSERT_AFTER(list, prev, elm, member)    STAILQ_INSERT_AFTER(list, prev, elm, member)
#define MESHAP_TAILQ_INSERT_HEAD(list, item, member)          STAILQ_INSERT_HEAD(list, item, member)

#endif /*__MESHAP_QUEUE_H__*/
