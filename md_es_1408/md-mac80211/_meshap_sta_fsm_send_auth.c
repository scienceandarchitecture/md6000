/********************************************************************************
* MeshDynamics
* --------------
* File     : _meshap_sta_fsm_send_auth.c
* Comments : Atheros 802.11 FSM Send auth state code
* Created  : 10/13/2004
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  2  |01/07/2008| Check for mixed mode added.                     |Prachiti|
* -----------------------------------------------------------------------------
* |  1  |12/23/2004| Parameter added to meshap_reset                | Sriram |
* -----------------------------------------------------------------------------
* |  0  |10/13/2004| Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

/**
 * This is a private source file included by meshap_sta_fsm.c, hence
 * the name begins with an underscore.
 * It was created to improve readability.
 */

#include <linux/version.h>

#if LINUX_VERSION_CODE == 199947
#define x86_ONLY
#endif

OPENFSM_ENTER_STATE_FUNCTION(send_auth)
{
   return 0;
}


int meshap_set_scan_mode(meshap_instance_t  *instance, 
                        _join_param_t *param,
                        struct ieee80211_channel *chan)

{
    int err = 0, i;
    struct ieee80211_sub_if_data *sdata = IEEE80211_DEV_TO_SUB_IF(instance->dev);
    struct cfg80211_registered_device *rdev;
    struct wireless_dev               *wdev = instance->dev->ieee80211_ptr;


    struct cfg80211_scan_request *request;
    int n_ssids = 0;
    int n_channels = 0;
    size_t ie_len = 0;

    al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"STA_FSM:FLOW: Enter %s<%d>\n",__func__,__LINE__);
    rdev = WIPHY_TO_DEV(wdev->wiphy);

    /* Before triggering scan mode check interface current scan state is NO_SCAN and 
     * no previous scan request reference hanging around */
    if ((instance->scan_state == SCAN_SW_SCANNING) || rdev->scan_req)
    {
        al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"STA_FSM:ERROR: %s<%d> Already in scan mode for assoc. scan_req : %p\n",
		__func__, __LINE__, rdev->scan_req);
        return -1;
    }

    instance->scan_state = NO_SCAN;

    /* Prepare scan mode and send scan request */
    n_channels = 1;
    n_ssids = 1;

    request = kzalloc(sizeof(*request)
        + sizeof(*request->channels) * n_channels
        + sizeof(*request->ssids) * n_ssids
        + ie_len, GFP_KERNEL);

    if (!request) {
        al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"STA_FSM:ERROR: %s<%d> No memory to compose scan request\n",
		__func__, __LINE__);
        return ENOMEM; 
    }
    if (n_ssids)
        request->ssids = (void *)&request->channels[n_channels];

    request->n_ssids = n_ssids;
    for(i = 0; i < n_ssids; i++) {
        request->ssids[i].ssid_len = (u8)param->essid_length;
        memcpy(request->ssids[i].ssid, param->essid, param->essid_length);
    }
    request->ie = NULL;
    request->channels[0] = chan;
    request->n_channels = n_channels;

    request->wdev = wdev;
    request->wiphy = &rdev->wiphy;
    request->scan_start = jiffies;

    rdev->scan_req = request;
    err = rdev->ops->scan(&rdev->wiphy, request);
    if(err) {
        al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"STA_FSM:ERROR: %s<%d> scan failed\n", __func__, __LINE__);
        rdev->scan_req = NULL;
        kfree(request);
    } 
    else if (err == 0) {
        instance->scan_state = SCAN_SW_SCANNING;
        instance->scan_start_time = jiffies;
        al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"STA_FSM:ERROR: %s<%d> Probe req scan start time -- %lu --\n",
		__func__, __LINE__, instance->scan_start_time);
    }
    al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"STA_FSM:FLOW: Exit %s<%d>\n",__func__,__LINE__);
    return err;
}

int meshap_get_bss_proberesp(meshap_instance_t  *instance, 
                        struct cfg80211_auth_request *req, 
                        _join_param_t *param,
                        struct ieee80211_channel *chan)
{
#define PROBE_RESP_CHECK_INTERVAL           (5)     // 5 ms
    int err = 0, i = 0;
    struct ieee80211_sub_if_data *sdata = IEEE80211_DEV_TO_SUB_IF(instance->dev);

    al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"STA_FSM:FLOW: Enter %s<%d> param channel =%d \n",__func__,__LINE__, param->channel);
    /* Check probe response IEs presence. If not present then trigger scan and send probe req */
    if (req->bss->proberesp_ies) {
        al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"STA_FSM:INFO: %s<%d> Already got probresp IEs of "MACSTR" \n",
		__func__, __LINE__, MAC2STR(param->bssid));
        return 0;
    }


    /* Move interface to active scan mode so that we send probe request and get probe response from interested AP */
    if(meshap_set_scan_mode(instance, param, chan)) {
        return EINVAL;
    }

    /* Wait here for probe response until we get probe response from interested AP or till we are in active scan state 
     * or probe request timeout period */
    do
    {
        msleep(PROBE_RESP_CHECK_INTERVAL);

        /* Check probe response received for the interested AP within active scan duration */
        if (!req->bss->proberesp_ies) {
            al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"STA_FSM:INFO: %s<%d> probresp not recv within %dms\n",
			__func__, __LINE__, (PROBE_RESP_CHECK_INTERVAL * ++i));
        }
        else if ((req->bss->proberesp_ies) && (sdata->local->scan_req)) {
            al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"STA_FSM:INFO: %s<%d> Recieved probe response from "MACSTR" \n",
			__func__, __LINE__, MAC2STR(param->bssid));
            ieee80211_scan_cancel(sdata->local);
        }
    } while((instance->scan_state == SCAN_SW_SCANNING) && (MESHAP_GET_TIME_SPENT_IN_SCAN(instance) < MESHAP_STA_FSM_PROBE_REQ_TIMEOUT));

    if (instance->scan_state == SCAN_SW_SCANNING) {
        al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"STA_FSM:INFO: %s<%d> It seems we didn't get probe resp from "MACSTR" "
		"within (%d)ms period\n", MAC2STR(param->bssid), MESHAP_GET_TIME_SPENT_IN_SCAN(instance));
    }
    al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"STA_FSM:FLOW: Exit %s<%d> \n",__func__,__LINE__);

    return err;
}

OPENFSM_STATE_FUNCTION(send_auth)
{
   int i, ret;
   int data_len = 0;
   int freq = 0;    //scan_mode
   struct ieee80211_mgmt             *mgmt;
   struct wireless_dev               *wdev;
   struct cfg80211_registered_device *rdev;
   _join_param_t                *param;
   _meshap_sta_fsm_data_t       *data;
   meshap_instance_t            *instance;
   struct ieee80211_channel     *chan;
   struct cfg80211_auth_request req;
   char q[64] = { '\0' };
   int  len   = 0;
   char *ie   = q;
   struct ieee80211_sub_if_data *sdata;

   memset(&req, 0, sizeof(req));
   /**
    * Here we send the authentication frame and move onto the AUTH_SENT
    * state. We first need to set the channel
    */

   param    = (_join_param_t *)param1;
   data     = openfsm_get_fsm_data(fsm);
   instance = data->instance;
   sdata = IEEE80211_DEV_TO_SUB_IF(instance->dev);   //1602MIG from 1602


   wdev = instance->dev->ieee80211_ptr;
   rdev = WIPHY_TO_DEV(wdev->wiphy);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"STA_FSM:FLOW: Enter %s<%d> param channel =%d \n",__func__,__LINE__, param->channel);
   for (i = 0; i < instance->channel_count; i++)
   {
      if (instance->channels[i].number == param->channel)
      {
         instance->current_channel = &instance->channels[i];
         freq = instance->current_channel->frequency;
      }
   }
   if (freq == 0) {
       al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "STA_FSM:ERROR: %s <%d> %s: Channel:%d not found in the supported"
	   "channel list\n", __func__, __LINE__, instance->dev->name, param->channel);
       return EINVAL;
   }
   param->algorithm = WLAN_AUTH_OPEN;
   param->atsn      = 1;

   ie[0] = WLAN_EID_SSID;
   len   = strlen(param->essid);
   ie[1] = len;
   memcpy(&ie[2], param->essid, len);
   len += 2;

   if (param->channel >= 1 && param->channel <= 14)
      freq = ieee80211_channel_to_frequency(param->channel, IEEE80211_BAND_2GHZ);
   else if (param->channel >= 36 && param->channel <= 196)
      freq = ieee80211_channel_to_frequency(param->channel, IEEE80211_BAND_5GHZ);
   else {
         al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"STA_FSM:ERROR: %s<%d> Invalid channel %d\n",
			__func__, __LINE__, param->channel);
      return -EINVAL;
   }

   chan = ieee80211_get_channel(wdev->wiphy, freq);
   if (chan == NULL)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"STA_FSM:ERROR: %s<%d> channel is NULL\n",
		__func__, __LINE__);
      return EINVAL;
   }

   if (!(is_11n_supported(instance, 0)) && !( is_11ac_supported(instance))) {
       req.flags |= AUTH_REQ_DISABLE_HT | AUTH_REQ_DISABLE_VHT;
   } else if (!(is_11ac_supported(instance)))
   {
       req.flags |= AUTH_REQ_DISABLE_VHT;
   }

   req.ie        = ie;
   req.ie_len    = len;
   req.auth_type = WLAN_AUTH_OPEN;

#ifdef x86_ONLY
    cfg80211_inform_bss(wdev->wiphy, chan, CFG80211_BSS_FTYPE_UNKNOWN, param->bssid,
#else
	cfg80211_inform_bss(wdev->wiphy, chan, param->bssid,
#endif
		                       jiffies, instance->default_capability, 100, ie, len, 64, GFP_KERNEL);

   req.bss = cfg80211_get_bss(wdev->wiphy, chan, param->bssid, param->essid, param->essid_length,
#ifdef x86_ONLY
				   IEEE80211_BSS_TYPE_ESS,	/* 1602MIG passing BSS_TYPE and PRIVACY to send auth req*/
				   IEEE80211_PRIVACY_ANY);
#else
                   instance->default_capability, instance->default_capability);
#endif

   if (req.bss == NULL)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"STA_FSM:ERROR: %s<%d> bss is NULL\n",__func__, __LINE__);
      return EINVAL;
   }

   if (meshap_get_bss_proberesp(instance, &req, param, chan) != 0) {
       al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"STA_FSM:ERROR: %s<%d> Failed to get probe response from "MACSTR"\n",
	    __func__, __LINE__, MAC2STR(param->bssid));
   }

   if (!req.bss->proberesp_ies) {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"STA_FSM:ERROR: %s<%d> prob_resp_ies is NULL\n",__func__, __LINE__);
      return EINVAL;
   }

   ASSERT_WDEV_LOCK(wdev);

   /*
    * Fix for ieee80211_vif_use_channel & release channel & station connection loss related WARNING's
    * lets bring down the interface before sending auth
    */
#define MESHAP_INTERFACE_DOWN(instance)  netif_carrier_off(instance->dev)
   MESHAP_INTERFACE_DOWN(instance);

   req.ie = NULL;
   req.ie_len = 0;
   rdev->ops->auth(wdev->wiphy, instance->dev, &req);

   /**
    * Move onto the ascan_2 state and set the state data
    * as the param
    */

   openfsm_set_state_data(data->fsm, (void *)param);

   openfsm_transition(data->fsm, &_states[_MESHAP_STA_FSM_AUTH_SENT]);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"STA_FSM:INFO: %s<%d> state moved to _MESHAP_STA_FSM_AUTH_SENT\n",
			__func__, __LINE__);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"STA_FSM:FLOW: Exit %s<%d> data->fsm=%p instance->dev->name=%s \n",__func__,__LINE__,
   data->fsm, instance->dev->name);

   return 0;
}

OPENFSM_LEAVE_STATE_FUNCTION(send_auth)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"STA_FSM:FLOW: Enter and Exit %s<%d> \n",__func__,__LINE__);
   return 0;
}

OPENFSM_STATE_TIMER_FUNCTION(send_auth)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"STA_FSM:FLOW: Enter and Exit %s<%d> \n",__func__,__LINE__);
   return 0;
}
