/********************************************************************************
* MeshDynamics
* --------------
* File     : openrt-ctrl.c
* Comments : 802.11 automatic rate control
* Created  : 12/2/2004
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  9  |7/20/2007 | Changes for high noise floor protection         | Sriram |
* -----------------------------------------------------------------------------
* |  8  |6/14/2007 | Added fixed_upper_rate_index                    | Sriram |
* -----------------------------------------------------------------------------
* |  7  |1/24/2007 | Changes for Private Information                 | Sriram |
* -----------------------------------------------------------------------------
* |  6  |01/17/2006| Changes for PS mode and RX Frag support         | Sriram |
* -----------------------------------------------------------------------------
* |  5  |1/14/2005 | N_H Overflow fixed                              | Sriram |
* -----------------------------------------------------------------------------
* |  4  |12/25/2004| Misc changes                                    | Sriram |
* -----------------------------------------------------------------------------
* |  3  |12/23/2004| Misc changes                                    | Sriram |
* -----------------------------------------------------------------------------
* |  2  |12/23/2004| Data reporting feature implemented              | Sriram |
* -----------------------------------------------------------------------------
* |  1  |12/22/2004| Misc changes                                    | Sriram |
* -----------------------------------------------------------------------------
* |  0  |12/2/2004 | Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

//#include "openrt-ctrl-impl.h"
//#include "openobj.h"
//#include "openrt-ctrl.h"
#include <linux/netdevice.h>
#include <linux/types.h>
#include <linux/skbuff.h>
#include <linux/ieee80211.h>
#include <net/mac80211.h>
#include <meshap_tx_pool.h>
#include <meshap_queue.h>
#include <meshap_instance.h>
#include <rate.h>
#include "openrt-ctrl-impl.h"
#include <openobj/openobj.h>
#include "openrt-ctrl.h"

#define __OPEN_RT_CTRL_MAC2STR(a)             (a)[0], (a)[1], (a)[2], (a)[3], (a)[4], (a)[5]
#define __OPEN_RT_CTRL_MACSTR    "%02x:%02x:%02x:%02x:%02x:%02x"

#define __OPEN_RT_CTRL_ELAPSED(from, time)    (time_after_eq(OPENOBJ_TIMESTAMP(), ((from + (time) * HZ))))

#define __OPEN_RT_CTRL_GET_C_1(C, SGN)               \
   (                                                 \
      (OPEN_RT_CTRL_C_MAX - 2 * C) * ((SGN + 1) / 2) \
   )

#define __OPEN_RT_CTRL_GET_C_2(C, SGN)   \
   (                                     \
      C + __OPEN_RT_CTRL_GET_C_1(C, SGN) \
   )

#define __OPEN_RT_CTRL_GET_C_3(C, SGN, T_EL)  \
   (                                          \
      (__OPEN_RT_CTRL_GET_C_2(C, SGN)) * T_EL \
   )

#define __OPEN_RT_CTRL_GET_C_4(C, SGN, T_EL, T_EL_MAX)    \
   (                                                      \
      (__OPEN_RT_CTRL_GET_C_3(C, SGN, T_EL)) / (T_EL_MAX) \
   )

#define _OPEN_RT_CTRL_GET_DELTA_C(C, SGN, T_EL, T_EL_MAX)    \
   (                                                         \
      (SGN * __OPEN_RT_CTRL_GET_C_4(C, SGN, T_EL, T_EL_MAX)) \
   )

#define _OPEN_RT_CTRL_TOTAL_PACKETS(item)    (item->tx_success + item->tx_error)

struct _open_rt_ctrl
{
   struct ieee80211_hw  *hw;

   open_rt_ctrl_t       rate_ctrl;
   struct _open_rt_ctrl *prev_list;
   struct _open_rt_ctrl *next_list;
};

typedef struct _open_rt_ctrl   _open_rt_ctrl_t;

struct _open_rt_ctrl_iterator
{
   open_rt_ctrl_t      *rate_ctrl;
   open_rt_ctrl_item_t *item;
};

typedef struct _open_rt_ctrl_iterator   _open_rt_ctrl_iterator_t;

static _open_rt_ctrl_t *_list_head = NULL;
static OPENOBJ_ATOMIC  _continue;
static int             _duration;

static int _open_rt_ctrl_thread(void *data);
static void _open_rt_ctrl_destroy(void *rate_ctrl);
static OPENOBJ_INLINE void _open_rt_ctrl_thread_helper(void);
static OPENOBJ_INLINE void _open_rt_ctrl_process(open_rt_ctrl_t *rate_ctrl);
static OPENOBJ_INLINE void _open_rt_ctrl_item_process(open_rt_ctrl_t *rate_ctrl, open_rt_ctrl_item_t *item);
static OPENOBJ_INLINE unsigned int _open_rt_ctrl_get_N_H(int current_SGN, int new_SGN, unsigned int current_N_H);


#define _OPEN_RT_CTRL_CHANGE_N_H        1
#define _OPEN_RT_CTRL_CHANGE_SGN        2
#define _OPEN_RT_CTRL_CHANGE_RATE       4
#define _OPEN_RT_CTRL_CHANGE_NOTHING    0
#define _OPEN_RT_CTRL_CHANGE_ALL        (_OPEN_RT_CTRL_CHANGE_N_H | _OPEN_RT_CTRL_CHANGE_SGN | _OPEN_RT_CTRL_CHANGE_RATE)

static OPENOBJ_INLINE void _open_rt_ctrl_change(open_rt_ctrl_t *rate_ctrl, open_rt_ctrl_item_t *item, int new_SGN, unsigned int flags, int type);

OPEN_RT_CTRL_DIRECTIVE void open_rt_ctrl_initialize(int duration)
{
   //_list_head = NULL;
   _duration  = duration;

   OPENOBJ_ATOMIC_SET(_continue, 1);

//1602MIG
	OPENOBJ_CREATE_THREAD(_open_rt_ctrl_thread,NULL,"open_rt_ctrl");

   OPENOBJ_PRINTF("openrt-ctrl: Rate control initialized\n");
}


OPEN_RT_CTRL_DIRECTIVE void open_rt_ctrl_uninitialize(void)
{
   _open_rt_ctrl_t *node;
   _open_rt_ctrl_t *next;
   unsigned int    flags;

   OPEN_RT_CTRL_LOCK(flags)
   {
      node = _list_head;
      while (node != NULL)
      {
         next = node->next_list;
         open_rt_ctrl_destroy(&node->rate_ctrl);
         node = next;
      }
      OPENOBJ_ATOMIC_DEC(_continue);
   }
   OPEN_RT_CTRL_UNLOCK(flags);

   OPENOBJ_PRINTF("openrt-ctrl: Rate control un-initialized\n");
}


OPEN_RT_CTRL_DIRECTIVE void open_rt_ctrl_start(open_rt_ctrl_t *rate_ctrl,
                                               int            t_el_max,
                                               int            t_el_min,
                                               int            qualification,
                                               int            max_retry_percent,
                                               int            max_error_percent,
                                               int            max_errors,
                                               int            min_retry_percent,
                                               int            min_qualification,
                                               int            intial_rate)
{
   rate_ctrl->t_el_max          = t_el_max;
   rate_ctrl->t_el_min          = t_el_min;
   rate_ctrl->qualification     = qualification;
   rate_ctrl->max_retry_percent = max_retry_percent;
   rate_ctrl->max_error_percent = max_error_percent;
   rate_ctrl->max_errors        = max_errors;
   rate_ctrl->min_retry_percent = min_retry_percent;
   rate_ctrl->min_qualification = min_qualification;
   rate_ctrl->intial_rate       = intial_rate;
   rate_ctrl->initialized       = 1;

   OPENOBJ_PRINTF("openrt-ctrl: Initialized instance %s with the following parameters :\n"
                  "T_EL_MAX = %d, T_EL_MIN = %d, QUAL = %d, MIN_QUAL = %d, INIT_RATE_INDEX=%d\n"
                  "MAX_RETRY_PERC = %d, MAX_ERROR_PERC = %d, MAX_ERRORS = %d, MIN_RETRY_PERC = %d\n",
                  rate_ctrl->name,
                  t_el_max,
                  t_el_min,
                  qualification,
                  min_qualification,
                  intial_rate,
                  max_retry_percent,
                  max_error_percent,
                  max_errors,
                  min_retry_percent);
}


OPEN_RT_CTRL_DIRECTIVE open_rt_ctrl_t *
open_rt_ctrl_create(meshap_instance_t                   *instance,
                    int                                 hash_length,
                    int                                 rate_count,
                    void                                *priv_id,
                    open_rt_ctrl_on_create_priv_info_t  on_create,
                    open_rt_ctrl_on_destroy_priv_info_t on_destroy,
                    open_rt_ctrl_on_process_priv_info_t on_process,
                    open_rt_ctrl_on_process_begin_t     on_begin,
                    open_rt_ctrl_on_process_end_t       on_end)
{
   _open_rt_ctrl_t              *_rate_ctrl;
   open_rt_ctrl_t               *rate_ctrl;
   unsigned int                 flags;
   struct ieee80211_sub_if_data *sdata = IEEE80211_DEV_TO_SUB_IF(instance->dev);
   struct wireless_dev          *wdev  = instance->dev->ieee80211_ptr;
   struct ieee80211_hw          *hw    = wiphy_to_ieee80211_hw(wdev->wiphy);

   if (sdata->local->rate_ctrl == NULL) {
	   printk(KERN_EMERG "RAMESH16MIG : @@@ RATE_INIT HW HAS RATE CTRL func : %s line : %d\n", __func__, __LINE__);
	   return NULL;
   }

   _rate_ctrl = (_open_rt_ctrl_t *)openobj_create(OPEN_RT_CTRL_SIGNATURE,
                                                  sizeof(_open_rt_ctrl_t),
                                                  _open_rt_ctrl_destroy);

   rate_ctrl                         = &_rate_ctrl->rate_ctrl;
   rate_ctrl->hash                   = (open_rt_ctrl_item_t **)OPENOBJ_MALLOC(sizeof(open_rt_ctrl_item_t *) * hash_length);
   rate_ctrl->hash_length            = hash_length;
   rate_ctrl->rate_count             = instance->supported_rates_length;
   rate_ctrl->priv_id                = priv_id;
   rate_ctrl->on_create_item         = on_create;
   rate_ctrl->on_destroy_item        = on_destroy;
   rate_ctrl->on_process_item        = on_process;
   rate_ctrl->on_process_begin       = on_begin;
   rate_ctrl->on_process_end         = on_end;
   rate_ctrl->fixed_upper_rate_index = (instance->supported_rates_length / 2);
   rate_ctrl->initialized            = 0;

   strcpy(rate_ctrl->name, instance->dev->name);

   memset(rate_ctrl->hash, 0, sizeof(open_rt_ctrl_item_t *) * hash_length);

   OPEN_RT_CTRL_LOCK(flags)
   {
      _rate_ctrl->next_list = _list_head;
      if (_list_head != NULL)
      {
         _list_head->prev_list = _rate_ctrl;
      }
      _list_head = _rate_ctrl;
      OPENOBJ_ADD_REFERENCE(rate_ctrl);                         /** Add reference for putting on list (ref count = 2)*/
   }
   OPEN_RT_CTRL_UNLOCK(flags);

   /* populationg the rate_ptr in MAC*/

   _rate_ctrl->hw = hw;

   sdata->local->rate_ctrl->priv = _rate_ctrl;

   OPENOBJ_RELEASE_REFERENCE(rate_ctrl);        /** Release reference of local pointer (ref count = 1)*/

   OPENOBJ_PRINTF("openrt-ctrl: Created instance %s in uninitialized mode\n", instance->dev->name);

   OPENOBJ_RETURN_REFERENCE(rate_ctrl);         /** Add reference for caller (ref count = 2)*/
}


OPEN_RT_CTRL_DIRECTIVE void open_rt_ctrl_destroy(open_rt_ctrl_t *rate_ctrl)
{
   _open_rt_ctrl_t *_rate_ctrl;
   unsigned int    flags;

   OPEN_RT_CTRL_LOCK(flags)
   {
      _rate_ctrl = (_open_rt_ctrl_t *)rate_ctrl;

      if (_rate_ctrl->prev_list != NULL)
      {
         _rate_ctrl->prev_list->next_list = _rate_ctrl->next_list;
      }
      if (_rate_ctrl->next_list != NULL)
      {
         _rate_ctrl->next_list->prev_list = _rate_ctrl->prev_list;
      }
      if (_rate_ctrl == _list_head)
      {
         _list_head = _list_head->next_list;
      }
   }
   OPEN_RT_CTRL_UNLOCK(flags);

   OPENOBJ_RELEASE_REFERENCE(rate_ctrl);        /** Release reference from list */
   OPENOBJ_RELEASE_REFERENCE(rate_ctrl);        /** Release given reference */
}


static int _open_rt_ctrl_thread(void *data)
{
   OPENOBJ_THREAD_INIT();

   OPENOBJ_PRINTF("\nopenrt-ctrl: Rate control thread starting\n");

   do
   {
      OPENOBJ_THREAD_SLEEP(_duration);

      OPENOBJ_ATOMIC_INC(_continue);
      if (OPENOBJ_ATOMIC_DEC_AND_TEST(_continue))
      {
         break;
      }

      _open_rt_ctrl_thread_helper();
   } while (1);

   OPENOBJ_PRINTF("\nopenrt-ctrl: Rate control thread quitting\n");

   OPENOBJ_THREAD_UNINIT();

   return 0;
}


static void _open_rt_ctrl_destroy(void *object)
{
   open_rt_ctrl_item_t *item;
   unsigned int        flags;
   open_rt_ctrl_t      *rate_ctrl;

   rate_ctrl = (open_rt_ctrl_t *)object;

   OPEN_RT_CTRL_LOCK(flags)
   {
      /**
       * Clear off all items
       */

      item = rate_ctrl->list_head;

      while (item != NULL)
      {
         rate_ctrl->list_head = item->next_list;
         if (rate_ctrl->on_destroy_item != NULL)
         {
            rate_ctrl->on_destroy_item(rate_ctrl->priv_id, item->priv_info);
         }
         OPENOBJ_RELEASE_REFERENCE(item);                       /** Release reference from list */
         OPENOBJ_RELEASE_REFERENCE(item);                       /** Release reference from hash */
         item = rate_ctrl->list_head;
      }
   }
   OPEN_RT_CTRL_UNLOCK(flags);

   OPENOBJ_FREE(rate_ctrl->hash);
}


static OPENOBJ_INLINE void _open_rt_ctrl_thread_helper(void)
{
   _open_rt_ctrl_t *node;
   _open_rt_ctrl_t *next;
   unsigned int    flags;

   OPEN_RT_CTRL_LOCK(flags)
   {
      node = _list_head;
      OPENOBJ_ADD_REFERENCE(node);
   }
   OPEN_RT_CTRL_UNLOCK(flags);

   while (node != NULL)
   {
      if (node->rate_ctrl.initialized == 1)
      {
         _open_rt_ctrl_process(&node->rate_ctrl);
      }

      OPEN_RT_CTRL_LOCK(flags)
      {
         next = node->next_list;
         OPENOBJ_ADD_REFERENCE(next);
         OPENOBJ_RELEASE_REFERENCE(node);
         node = next;
      }
      OPEN_RT_CTRL_UNLOCK(flags);
   }
}


static OPENOBJ_INLINE void _open_rt_ctrl_process(open_rt_ctrl_t *rate_ctrl)
{
   open_rt_ctrl_item_t *item;
   open_rt_ctrl_item_t *next;
   unsigned int        flags;

   if (rate_ctrl->on_process_begin != NULL)
   {
      rate_ctrl->on_process_begin(rate_ctrl->priv_id);
   }

   OPEN_RT_CTRL_LOCK(flags)
   {
      item = rate_ctrl->list_head;
      OPENOBJ_ADD_REFERENCE(item);
   }
   OPEN_RT_CTRL_UNLOCK(flags);

   while (item != NULL)
   {
      if (__OPEN_RT_CTRL_ADDR_IS_BROADCAST(item->address) ||
          __OPEN_RT_CTRL_ADDR_IS_MULTICAST(item->address))
      {
         goto _lb_next;
      }

      if (__OPEN_RT_CTRL_ELAPSED(item->last_tx, OPEN_RT_CTRL_INACTIVITY_TIMEOUT))
      {
         OPENOBJ_PRINTF("openrt-ctrl-%s: Removing entry for address "__OPEN_RT_CTRL_MACSTR "...\n",
                        rate_ctrl->name,
                        __OPEN_RT_CTRL_MAC2STR(item->address));
         OPEN_RT_CTRL_LOCK(flags)
         {
            /** Remove item from hash table */
            if (item->next_hash != NULL)
            {
               item->next_hash->prev_hash = item->prev_hash;
            }
            if (item->prev_hash != NULL)
            {
               item->prev_hash->next_hash = item->next_hash;
            }
            if (item == rate_ctrl->hash[item->bucket])
            {
               rate_ctrl->hash[item->bucket] = item->next_hash;
            }
            /** Remove item from list */
            if (item->next_list != NULL)
            {
               item->next_list->prev_list = item->prev_list;
            }
            if (item->prev_list != NULL)
            {
               item->prev_list->next_list = item->next_list;
            }
            if (item == rate_ctrl->list_head)
            {
               rate_ctrl->list_head = item->next_list;
            }
         }
         OPEN_RT_CTRL_UNLOCK(flags);

         if (rate_ctrl->on_destroy_item != NULL)
         {
            rate_ctrl->on_destroy_item(rate_ctrl->priv_id, item->priv_info);
         }
         OPENOBJ_RELEASE_REFERENCE(item);                       /** For hash */
         OPENOBJ_RELEASE_REFERENCE(item);                       /** For list */
         OPENOBJ_PRINTF("openrt-ctrl-%s: Done removing entry for address "__OPEN_RT_CTRL_MACSTR "...\n",
                        rate_ctrl->name,
                        __OPEN_RT_CTRL_MAC2STR(item->address));
      }
      else
      {
         _open_rt_ctrl_item_process(rate_ctrl, item);
         if (rate_ctrl->on_process_item != NULL)
         {
            rate_ctrl->on_process_item(rate_ctrl->priv_id, item->address, item->priv_info);
         }
      }

_lb_next:
      OPEN_RT_CTRL_LOCK(flags)
      {
         next = item->next_list;
         OPENOBJ_ADD_REFERENCE(next);
         OPENOBJ_RELEASE_REFERENCE(item);                       /** Release our reference */
         item = next;
      }
      OPEN_RT_CTRL_UNLOCK(flags);
   }

   if (rate_ctrl->on_process_end != NULL)
   {
      rate_ctrl->on_process_end(rate_ctrl->priv_id);
   }
}


static OPENOBJ_INLINE void
_open_rt_ctrl_item_process(open_rt_ctrl_t *rate_ctrl, open_rt_ctrl_item_t *item)
{
   ++item->T_EL;
   ++item->last_check;

   /**
    * First see if packets have been transmitted
    */

   if (_OPEN_RT_CTRL_TOTAL_PACKETS(item) == 0)
   {
      return;
   }

   if (_OPEN_RT_CTRL_TOTAL_PACKETS(item) < rate_ctrl->min_qualification)
   {
      return;
   }

   /**
    * Now see if there have been errors, or if the number of retries exceeds 30%
    * or the error rate exceeds 10%
    */

   if ((item->tx_success == 0) ||
       (item->tx_retry > ((item->tx_success * rate_ctrl->max_retry_percent) / (100))) ||
       (item->tx_error > ((_OPEN_RT_CTRL_TOTAL_PACKETS(item) * rate_ctrl->max_error_percent) / (100))) ||
       (item->tx_error >= rate_ctrl->max_errors))
   {
      _open_rt_ctrl_change(rate_ctrl, item, OPEN_RT_CTRL_SGN_NEGATIVE, _OPEN_RT_CTRL_CHANGE_ALL, 0);
      return;
   }

   if (_OPEN_RT_CTRL_TOTAL_PACKETS(item) < rate_ctrl->qualification)
   {
      return;
   }

   if ((item->T_EL > item->T_CH) ||
       (item->T_EL > rate_ctrl->t_el_max))
   {
      if ((item->tx_error == 0) &&
          (item->tx_retry <= ((item->tx_success * rate_ctrl->min_retry_percent) / (100))))
      {
         _open_rt_ctrl_change(rate_ctrl, item, OPEN_RT_CTRL_SGN_POSSITIVE, _OPEN_RT_CTRL_CHANGE_ALL, 1);
      }
      else if (item->tx_error >= ((_OPEN_RT_CTRL_TOTAL_PACKETS(item) * rate_ctrl->max_error_percent) / (100)))
      {
         _open_rt_ctrl_change(rate_ctrl, item, OPEN_RT_CTRL_SGN_NEGATIVE, _OPEN_RT_CTRL_CHANGE_NOTHING, 2);
      }
      else
      {
         _open_rt_ctrl_change(rate_ctrl, item, OPEN_RT_CTRL_SGN_POSSITIVE, _OPEN_RT_CTRL_CHANGE_NOTHING, 3);
      }
   }
   else
   {
      int DELTA_C;
      DELTA_C = 0;
      if ((item->tx_error == 0) &&
          (item->tx_retry <= ((item->tx_success * rate_ctrl->min_retry_percent) / (100))))
      {
         DELTA_C = _OPEN_RT_CTRL_GET_DELTA_C(item->C, OPEN_RT_CTRL_SGN_POSSITIVE, item->T_EL, rate_ctrl->t_el_max);
         if (DELTA_C == 0)
         {
            DELTA_C = OPEN_RT_CTRL_SGN_POSSITIVE;
         }
      }
      else if (item->tx_error > ((_OPEN_RT_CTRL_TOTAL_PACKETS(item) * rate_ctrl->max_error_percent) / (100)))
      {
         DELTA_C = _OPEN_RT_CTRL_GET_DELTA_C(item->C, OPEN_RT_CTRL_SGN_NEGATIVE, item->T_EL, rate_ctrl->t_el_max);
         if (DELTA_C == 0)
         {
            DELTA_C = OPEN_RT_CTRL_SGN_NEGATIVE;
         }
      }
      item->C += DELTA_C;
      if (item->C < OPEN_RT_CTRL_C_MIN)
      {
         item->C = OPEN_RT_CTRL_C_MIN;
      }
      else if (item->C > OPEN_RT_CTRL_C_MAX)
      {
         item->C = OPEN_RT_CTRL_C_MAX;
      }
   }
}


static OPENOBJ_INLINE void
_open_rt_ctrl_change(open_rt_ctrl_t *rate_ctrl, open_rt_ctrl_item_t *item,
                     int new_SGN, unsigned int flags, int type)
{
   int DELTA_C;

   DELTA_C = _OPEN_RT_CTRL_GET_DELTA_C(item->C, new_SGN, item->T_EL, rate_ctrl->t_el_max);

   if (DELTA_C == 0)
   {
      switch (new_SGN)
      {
      case OPEN_RT_CTRL_SGN_POSSITIVE:
         DELTA_C = 1;
         break;

      default:
         DELTA_C = -1;
      }
   }

   item->C += DELTA_C;

   if (item->C < OPEN_RT_CTRL_C_MIN)
   {
      item->C = OPEN_RT_CTRL_C_MIN;
   }
   else if (item->C > OPEN_RT_CTRL_C_MAX)
   {
      item->C = OPEN_RT_CTRL_C_MAX;
   }

   if (flags & _OPEN_RT_CTRL_CHANGE_N_H)
   {
      item->N_H = _open_rt_ctrl_get_N_H(item->SGN, new_SGN, item->N_H);
   }

   if (flags & _OPEN_RT_CTRL_CHANGE_SGN)
   {
      item->SGN = new_SGN;
   }

   item->T_EL = 0;
   item->T_CH = ((100 * item->N_H) / (item->C)) * rate_ctrl->t_el_min;

   if (item->T_CH > rate_ctrl->t_el_max)
   {
      item->T_CH = rate_ctrl->t_el_max;
   }

   if (flags & _OPEN_RT_CTRL_CHANGE_RATE)
   {
      switch (new_SGN)
      {
      case -1:
         if (item->rate_index > 0)
         {
            --item->rate_index;
         }
         break;

      default:
         if (item->rate_index < rate_ctrl->rate_count - 1)
         {
            ++item->rate_index;
         }
      }

      /** First retry rate */
      if (item->rate_index - 1 > 0)
      {
         item->rate_index1 = item->rate_index - 1;
      }
      else
      {
         item->rate_index1 = 0;
      }
      /** Second retry rate */
      if (item->rate_index - 2 > 0)
      {
         item->rate_index2 = item->rate_index - 2;
      }
      else
      {
         item->rate_index2 = 0;
      }
      /** Third retry rate */
      if (item->rate_index - 3 > 0)
      {
         item->rate_index3 = item->rate_index - 3;
      }
      else
      {
         item->rate_index3 = 0;
      }
   }

   item->tx_error   = 0;
   item->tx_success = 0;
   item->tx_retry   = 0;
}


static OPENOBJ_INLINE unsigned int _open_rt_ctrl_get_N_H(int current_SGN, int new_SGN, unsigned int current_N_H)
{
   switch (current_SGN)
   {
   case OPEN_RT_CTRL_SGN_NEGATIVE:
      switch (new_SGN)
      {
      case OPEN_RT_CTRL_SGN_NEGATIVE:
         return 1;

      default:
         return current_N_H;
      }

   default:
      switch (new_SGN)
      {
      case OPEN_RT_CTRL_SGN_NEGATIVE:
         if (current_N_H >= OPEN_RT_CTRL_N_H_MAX)
         {
            return OPEN_RT_CTRL_N_H_MAX;
         }
         else
         {
            return(current_N_H * 2);
         }

      default:
         return (current_N_H != OPEN_RT_CTRL_N_H_MIN) ? current_N_H / 2 : current_N_H;
      }
   }
}


static void
openrt_ctrl_tx_status(void *priv, struct ieee80211_supported_band *sband,
                      struct ieee80211_sta *sta, void *priv_sta,
                      struct sk_buff *skb)
{
   struct ieee80211_tx_info *info = IEEE80211_SKB_CB(skb);
   struct ieee80211_tx_rate *ar   = info->status.rates;
   open_rt_ctrl_item_t      *item;
   _open_rt_ctrl_t          *rate_control;

   if (priv == NULL)
   {
      printk("DEBUG: priv is NULL<%s>:<%d>\n", __func__, __LINE__);
      return;
   }

   rate_control = ( _open_rt_ctrl_t *)priv;
   if ((item = open_rt_ctrl_get_item(&rate_control->rate_ctrl, sta->addr, 1)) == NULL)
   {
      return;
   }
   item->tx_retry    = ar[0].count;
   item->tx_success += (info->flags & IEEE80211_TX_STAT_ACK);
   item->tx_error   += !(info->flags & IEEE80211_TX_STAT_ACK);
   item->last_tx     = OPENOBJ_TIMESTAMP();
}


static void openrt_ctrl_get_rate(void *priv, struct ieee80211_sta *sta,
                                 void *priv_sta, struct ieee80211_tx_rate_control *txrc)
{
   struct sk_buff           *skb  = txrc->skb;
   struct ieee80211_tx_info *info = IEEE80211_SKB_CB(skb);
   struct ieee80211_tx_rate *ar   = info->control.rates;
   _open_rt_ctrl_t          *rate_control;
   open_rt_ctrl_item_t      *item;

   rate_control = ( _open_rt_ctrl_t *)priv;
   if (rate_control_send_low(sta, priv_sta, txrc))
   {
      return;
   }

   if (priv == NULL)
   {
      printk("DEBUG: priv is NULL<%s>:<%d>\n", __func__, __LINE__);
      return;
   }

   if ((item = open_rt_ctrl_get_item(&rate_control->rate_ctrl, sta->addr, 1)) == NULL)
   {
      printk("DEBUG: item is NULL<%s>:<%d>\n", __func__, __LINE__);
      return;
   }

   ar[0].idx   = item->rate_index;
   ar[0].count = item->tx_retry;
   ar[1].idx   = item->rate_index1;
   ar[2].idx   = item->rate_index2;
   ar[3].idx   = item->rate_index3;
}


static void openrt_ctrl_rate_init(void *priv, struct ieee80211_supported_band *sband,
                                  struct ieee80211_sta *sta, void *priv_sta)
{
   _open_rt_ctrl_t *rate_control = (_open_rt_ctrl_t *)priv;
   open_rt_ctrl_t  *rate_ctrl    = &rate_control->rate_ctrl;

   open_rt_ctrl_start(rate_ctrl,
                      rate_ctrl->t_el_max,
                      rate_ctrl->t_el_min,
                      rate_ctrl->qualification,
                      rate_ctrl->max_retry_percent,
                      rate_ctrl->max_error_percent,
                      rate_ctrl->max_errors,
                      rate_ctrl->min_qualification,
                      rate_ctrl->min_retry_percent,
                      rate_ctrl->intial_rate);
}


static void *openrt_ctrl_alloc(struct ieee80211_hw *hw, struct dentry *debugfsdir)
{
#if 0
   _open_rt_ctrl_t *openrt_ctrl;
   openrt_ctrl = kzalloc(sizeof(struct _open_rt_ctrl), GFP_ATOMIC);
   if (!openrt_ctrl)
   {
      return NULL;
   }

   openrt_ctrl->hw = hw;
   //printk(KERN_EMERG "DEBUG: <%s>:<%d> alloc=%p HW=%p\n", __func__, __LINE__,openrt_ctrl,openrt_ctrl->hw);
#endif
   return 0;
}


int openrt_ctrl_free(void *priv)
{
#if 0
#ifdef CONFIG_MAC80211_DEBUGF
   debugfs_remove(((struct minstrel_priv *)priv)->dbg_fixed_rate);//Need to check about this
#endif
#endif
   kfree(priv);
   return 0;
}


static void *openrt_ctrl_alloc_sta(void *priv, struct ieee80211_sta *sta, gfp_t gfp)
{
   _meshap_mac80211_sta_info_t *sta_info;
   int                 bucket;
   unsigned long       flags;
   _open_rt_ctrl_t     *rate_control;
   open_rt_ctrl_item_t *item;

   item = (open_rt_ctrl_item_t *)kmalloc(sizeof(open_rt_ctrl_item_t), GFP_ATOMIC);

   if (priv == NULL)
   {
      printk("DEBUG: priv is NULL<%s>:<%d>\n", __func__, __LINE__);
      return NULL;
   }

   rate_control = (_open_rt_ctrl_t *)priv;
   if ((item = open_rt_ctrl_get_item(&rate_control->rate_ctrl, sta->addr, 1)) == NULL)
   {
      printk("DEBUG: item is NULL<%s>:<%d>\n", __func__, __LINE__);
      return NULL;
   }


   return item->priv_info;
}


static void openrt_ctrl_free_sta(void *priv, struct ieee80211_sta *sta, void *priv_sta)
{
   unsigned long       flags;
   _open_rt_ctrl_t     *rate_control;
   open_rt_ctrl_item_t *item;

   rate_control = (_open_rt_ctrl_t *)priv;
   item         = open_rt_ctrl_get_item(&rate_control->rate_ctrl, sta->addr, 1);

   OPEN_RT_CTRL_LOCK(flags)
   {
      /** Remove item from hash table */
      if (item->next_hash != NULL)
      {
         item->next_hash->prev_hash = item->prev_hash;
      }
      if (item->prev_hash != NULL)
      {
         item->prev_hash->next_hash = item->next_hash;
      }
      if (item == rate_control->rate_ctrl.hash[item->bucket])
      {
         rate_control->rate_ctrl.hash[item->bucket] = item->next_hash;
      }
      /** Remove item from list */
      if (item->next_list != NULL)
      {
         item->next_list->prev_list = item->prev_list;
      }
      if (item->prev_list != NULL)
      {
         item->prev_list->next_list = item->next_list;
      }
      if (item == rate_control->rate_ctrl.list_head)
      {
         rate_control->rate_ctrl.list_head = item->next_list;
      }
   }
   OPEN_RT_CTRL_UNLOCK(flags);
   rate_control->rate_ctrl.on_destroy_item(rate_control->rate_ctrl.priv_id, item->priv_info);
}


//1602MIG adding void as parameter
int dummy7(void)
{
   printk(KERN_EMERG "DEBUG: <%s>:<%d>\n", __func__, __LINE__);
   return 0;
}

//1602MIG adding void as parameter
int dummy8(void)
{
   printk(KERN_EMERG "DEBUG: <%s>:<%d>\n", __func__, __LINE__);
   return 0;
}


static void _meshap_clear_rx_fragments(meshap_instance_t           *instance,
                                       _meshap_mac80211_sta_info_t *sta_info,
                                       int                         clear_all)
{
   unsigned long         flags;
   int                   i;
   meshap_rx_frag_item_t *item;
   meshap_rx_frag_item_t *temp;

#define __C(ca, ft)    ((ca) ? 1 : (jiffies - (ft) > 10 * HZ))

   MESHAP_INSTANCE_LOCK(instance, flags);

   for (i = 0; i < MESHAP_RX_FRAG_HASH_SIZE; i++)
   {
      item = sta_info->rx_fragment_hash[i];

      while (item != NULL)
      {
         temp = item->next_list;

         if (__C(clear_all, item->last_fragment_time))
         {
            if (item->prev_list != NULL)
            {
               item->prev_list->next_list = item->next_list;
            }
            if (item->next_list != NULL)
            {
               item->next_list->prev_list = item->prev_list;
            }
            if (item == sta_info->rx_fragment_hash[i])
            {
               sta_info->rx_fragment_hash[i] = item->next_list;
            }

            tx_rx_pkt_stats.packet_drop[136]++;
            dev_kfree_skb(item->skb);
            kfree(item);
         }

         item = temp;
      }
   }

   MESHAP_INSTANCE_UNLOCK(instance, flags);

#undef __C
}


#if 0
static void _meshap_clear_txq_helper(meshap_instance_t *instance, meshap_txq_t *txq)
{
//    struct ath_hal*     ah;
   meshap_buf_t  *buf;
   unsigned long flags;

   //  ah  = instance->ah;

   while (1)
   {
      MESHAP_TXQ_LOCK(txq, flags);
      buf = MESHAP_TAILQ_HEAD(&txq->q);
      if (buf == NULL)
      {
         txq->link         = NULL;
         txq->last_fc_ptr  = NULL;
         txq->tx_buf_count = 0;
         MESHAP_TXQ_UNLOCK(txq, flags);
         break;
      }
      MESHAP_TAILQ_REMOVE_HEAD(&txq->q, list);
      MESHAP_TXQ_UNLOCK(txq, flags);

      MESHAP_INSTANCE_TXBUF_LOCK(instance, flags);
      if (buf->tx_pool_item != NULL)
      {
         meshap_tx_pool_item_release(instance->tx_pool, (meshap_tx_pool_item_t *)buf->tx_pool_item);
         buf->tx_pool_item = NULL;
      }
      MESHAP_TAILQ_INSERT_TAIL(&instance->tx_buf, buf, list);
      MESHAP_INSTANCE_TXBUF_UNLOCK(instance, flags);
   }
}
#endif
#if 1
OPEN_RT_CTRL_DIRECTIVE void *_on_open_rt_ctrl_create_item(void *priv_id)
{
   meshap_instance_t           *instance;
   _meshap_mac80211_sta_info_t *sta_info;
   unsigned long               flags;

   instance = (meshap_instance_t *)priv_id;

   sta_info = (_meshap_mac80211_sta_info_t *)kmalloc(sizeof(_meshap_mac80211_sta_info_t), GFP_ATOMIC);

   sta_info->aid             = 0;
   sta_info->power_save_mode = 0;
   sta_info->total_power_save_dropped_count = 0;
   sta_info->total_power_save_queued_count  = 0;
   sta_info->total_switch_count             = 0;
   sta_info->total_ps_poll_received         = 0;
   sta_info->last_rx_sqnr       = 0xFFFF;
   sta_info->rx_duplicate_count = 0;

   sta_info->q_retry_success_count = 0;
   sta_info->q_retry_queued_count  = 0;

   /**
    * Initialize the fragment hash table
    */

   MESHAP_INSTANCE_LOCK(instance, flags);
   memset(sta_info->rx_fragment_hash, 0, sizeof(sta_info->rx_fragment_hash));
   MESHAP_INSTANCE_UNLOCK(instance, flags);

   sta_info->rx_frag_total    = 0;
   sta_info->rx_frag_complete = 0;

   atomic_set(&sta_info->power_save_queued_count, 0);
   atomic_set(&sta_info->ps_poll_locked, 0);
   sta_info->packet_queue.initialized   = 0;
   sta_info->packet_queue.hal_qnum      = instance->cab_q.hal_qnum;
   sta_info->packet_queue.link          = NULL;
   sta_info->packet_queue.last_fc_ptr   = NULL;
   sta_info->packet_queue.last_fc       = 0;
   sta_info->packet_queue.tx_buf_count  = 0;
   sta_info->packet_queue.comp_buf_virt = NULL;
   sta_info->packet_queue.comp_buf_dma  = NULL;
   sta_info->packet_queue.comp_buf_size = 0;

   MESHAP_TAILQ_INIT_LIST(&sta_info->packet_queue.q);
   MESHAP_TXQ_LOCK_INIT(&sta_info->packet_queue);
   return (void *)sta_info;
}


//EXPORT_SYMBOL(_on_open_rt_ctrl_create_item);

OPEN_RT_CTRL_DIRECTIVE void _on_open_rt_ctrl_destroy_item(void *priv_id, void *priv_info)
{
   meshap_instance_t           *instance;
   _meshap_mac80211_sta_info_t *sta_info;


   instance = (meshap_instance_t *)priv_id;
   sta_info = (_meshap_mac80211_sta_info_t *)priv_info;

   _meshap_clear_rx_fragments(instance, sta_info, 1);

   // _meshap_clear_txq_helper(instance,&sta_info->packet_queue);

   kfree(sta_info);
}


//EXPORT_SYMBOL(_on_open_rt_ctrl_destroy_item);

OPEN_RT_CTRL_DIRECTIVE void _on_open_rt_ctrl_process_item(void *priv_id, unsigned char *address, void *priv_info)
{
   meshap_instance_t           *instance;
   _meshap_mac80211_sta_info_t *sta_info;
   unsigned long               flags;
   unsigned long               flags2;
   meshap_buf_t                *buf;

   instance = (meshap_instance_t *)priv_id;
   sta_info = (_meshap_mac80211_sta_info_t *)priv_info;

   /**
    * If we have not heard from this STA for 12 seconds, we reset its PS state
    */

   if ((sta_info->power_save_mode) && ((jiffies - sta_info->last_rx) > (12 * HZ)))
   {
      sta_info->power_save_mode = 0;
      /*printk(KERN_INFO"%s: Resetting PS mode for "MACSTR"\n",instance->dev->name,MAC2STR(address));*/
   }

   if (sta_info->power_save_mode)
   {
      ++instance->power_save_sta_verification_count;
   }

   /**
    * Clear the PS queued packets older than 5 seconds
    */

   MESHAP_TXQ_LOCK(&sta_info->packet_queue, flags);

   while (1)
   {
      buf = MESHAP_TAILQ_HEAD(&sta_info->packet_queue.q);

      if (buf == NULL)
      {
         break;
      }
      if (jiffies - buf->queuing_time >= 5 * HZ)
      {
         MESHAP_TAILQ_REMOVE_HEAD(&sta_info->packet_queue.q, list);
         ++sta_info->total_power_save_dropped_count;
         if ((sta_info->aid != 0) &&
             (atomic_read(&sta_info->power_save_queued_count) > 0) &&
             atomic_dec_and_test(&sta_info->power_save_queued_count))
         {
#ifndef MESHAP_NO_POWER_SAVE_MODE
            meshap_clear_dtim_bitmap_bit(instance, sta_info->aid);
#endif
         }
         MESHAP_INSTANCE_TXBUF_LOCK(instance, flags2);
         if (buf->tx_pool_item != NULL)
         {
            meshap_tx_pool_item_release(instance->tx_pool, (meshap_tx_pool_item_t *)buf->tx_pool_item);
            buf->tx_pool_item = NULL;
         }
         MESHAP_TAILQ_INSERT_TAIL(&instance->tx_buf, buf, list);
         MESHAP_INSTANCE_TXBUF_UNLOCK(instance, flags2);
      }
      else
      {
         break;
      }
   }

   MESHAP_TXQ_UNLOCK(&sta_info->packet_queue, flags);

   /**
    * Clear the incomplete fragments older than 10 seconds.
    */
   _meshap_clear_rx_fragments(instance, sta_info, 0);
}


OPEN_RT_CTRL_DIRECTIVE void _open_rt_ctrl_on_process_begin(void *priv_id)
{
   meshap_instance_t *instance;

   instance = (meshap_instance_t *)priv_id;

   instance->power_save_sta_verification_count = 0;
}


OPEN_RT_CTRL_DIRECTIVE void _open_rt_ctrl_on_process_end(void *priv_id)
{
   meshap_instance_t *instance;

   instance = (meshap_instance_t *)priv_id;

   if (atomic_read(&instance->power_save_sta_count) != instance->power_save_sta_verification_count)
   {
      printk(KERN_INFO "%s: Setting PS mode STA count to %d (%d)\n",
             instance->dev->name,
             instance->power_save_sta_verification_count,
             atomic_read(&instance->power_save_sta_count));
      atomic_set(&instance->power_save_sta_count, instance->power_save_sta_verification_count);
   }
}


OPEN_RT_CTRL_DIRECTIVE void _setup_supported_rates(meshap_instance_t *instance)
{
   switch (instance->current_phy_mode)
   {
   case WLAN_PHY_MODE_802_11_AN:
   case WLAN_PHY_MODE_802_11_N:
   case WLAN_PHY_MODE_802_11_5GHZ_PURE_N:
   case WLAN_PHY_MODE_802_11_A:
      instance->supported_rates_length = sizeof(_meshap_802_11_A_rates);
      instance->supported_rates        = _meshap_802_11_A_rates;
      instance->extended_rates         = NULL;
      instance->extended_rates_length  = 0;
      break;

   case WLAN_PHY_MODE_802_11_B:
      instance->supported_rates_length = sizeof(_meshap_802_11_B_rates);
      instance->supported_rates        = _meshap_802_11_B_rates;
      instance->extended_rates         = NULL;
      instance->extended_rates_length  = 0;
      break;

   case WLAN_PHY_MODE_802_11_BGN:
   case WLAN_PHY_MODE_802_11_2GHZ_PURE_N:
   case WLAN_PHY_MODE_802_11_G:
      instance->supported_rates_length = sizeof(_meshap_802_11_G_rates);
      instance->supported_rates        = _meshap_802_11_G_rates;
      instance->extended_rates_length  = sizeof(_meshap_802_11_G_ext_rates);
      instance->extended_rates         = _meshap_802_11_G_ext_rates;
      break;

   case WLAN_PHY_MODE_802_11_PURE_G:
      instance->supported_rates_length = sizeof(_meshap_802_11_A_rates);
      instance->supported_rates        = _meshap_802_11_A_rates;
      instance->extended_rates         = NULL;
      instance->extended_rates_length  = 0;
      break;
   }
}


OPEN_RT_CTRL_DIRECTIVE void _openrt_ctrl_calc_channel_count(meshap_instance_t *instance)
{
   int i;

   instance->current_phy_mode_channel_count = 0;
   for (i = 0; i < instance->channel_count; i++)
   {
      if (instance->channels[i].frequency != 0)
      {
         switch (instance->current_phy_mode)
         {
         case WLAN_PHY_MODE_802_11_A:
            if ((instance->channels[i].flags & WLAN_CHANNEL_802_11_A) == WLAN_CHANNEL_802_11_A)
            {
               ++instance->current_phy_mode_channel_count;
            }
            break;

         case WLAN_PHY_MODE_802_11_B:
            if ((instance->channels[i].flags & WLAN_CHANNEL_802_11_B) == WLAN_CHANNEL_802_11_B)
            {
               ++instance->current_phy_mode_channel_count;
            }
            break;

         case WLAN_PHY_MODE_802_11_PURE_G:
         case WLAN_PHY_MODE_802_11_G:
            if ((instance->channels[i].flags & WLAN_CHANNEL_802_11_G) == WLAN_CHANNEL_802_11_G)
            {
               ++instance->current_phy_mode_channel_count;
            }
            break;
         }
      }
   }
}


struct rate_control_ops mac80211_openrt_ctrl =
{
   .name      = "openrt_ctrl",
   .tx_status = openrt_ctrl_tx_status,
   .get_rate  = openrt_ctrl_get_rate,
   .rate_init = openrt_ctrl_rate_init,
   .alloc     = openrt_ctrl_alloc,
   .free      = openrt_ctrl_free,
   .alloc_sta = openrt_ctrl_alloc_sta,
   .free_sta  = openrt_ctrl_free_sta,
#ifdef CONFIG_MAC80211_DEBUGFS
   .add_sta_debugfs    = dummy7,
   .remove_sta_debugfs = dummy8,
#endif
};

int __init
rc80211_openrt_ctrl_init(void)
{
   return ieee80211_rate_control_register(&mac80211_openrt_ctrl);
}


void
rc80211_openrt_ctrl_exit(void)
{
   ieee80211_rate_control_unregister(&mac80211_openrt_ctrl);
}
#endif
