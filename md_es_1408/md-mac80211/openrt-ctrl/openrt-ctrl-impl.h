/********************************************************************************
* MeshDynamics
* --------------
* File     : openrt-ctrl-impl.h
* Comments : Linux kernel mode implementation for openobj/openrt-ctrl
* Created  : 12/2/2004
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  1  |12/22/2004| Fixed mistake in OPENOBJ_TIMESTAMP macro        | Sriram |
* -----------------------------------------------------------------------------
* |  0  |12/2/2004 | Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

#ifndef __OPENRT_CTRL_IMPL_H__
#define __OPENRT_CTRL_IMPL_H__

#include <linux/version.h>

#if (LINUX_VERSION_CODE<KERNEL_VERSION(2, 6, 19))
#include <linux/config.h>
#else
#include <generated/autoconf.h>
#endif

#include <linux/module.h>
#include <linux/init.h>
#include <linux/if.h>
#include <linux/netdevice.h>
#include <linux/skbuff.h>
#include <linux/cache.h>
#include <linux/pci.h>
#include <linux/wireless.h>
#include <asm/uaccess.h>
#include <net/iw_handler.h>
#include <linux/delay.h>
#include <linux/sched.h>

//1602MIG including linux/kthread.h for kthread_run
#include <linux/kthread.h>

#define OPENOBJ_INLINE    inline
#define OPEN_RT_CTRL_DIRECTIVE

#define OPENOBJ_ATOMIC    atomic_t
#define OPENOBJ_ATOMIC_SET(variable, value)      atomic_set(&variable, value)
#define OPENOBJ_ATOMIC_GET(variable)             atomic_read(&variable)
#define OPENOBJ_ATOMIC_INC(variable)             atomic_inc(&variable)
#define OPENOBJ_ATOMIC_DEC(variable)             atomic_dec(&variable)
#define OPENOBJ_ATOMIC_DEC_AND_TEST(variable)    atomic_dec_and_test(&variable)
#define OPENOBJ_MALLOC(size)                     kmalloc(size, GFP_ATOMIC)
#define OPENOBJ_FREE(ptr)                        kfree(ptr)

#define OPENOBJ_TIMESTAMP_TYPE    u64

u64 openrt_ctrl_impl_timestamp(void);

#define OPENOBJ_TIMESTAMP()            openrt_ctrl_impl_timestamp()

//1602MIG replacing kernel_thread with kthread_run
#define OPENOBJ_CREATE_THREAD(f,d,name)		kthread_run(f,d,name)

#define OPENOBJ_PRINTF(fmt ...)        printk(KERN_INFO fmt)

#if (LINUX_VERSION_CODE<KERNEL_VERSION(2, 6, 0))

#define OPENOBJ_THREAD_INIT()                          \
   do {                                                \
      /*daemonize();*/                                     \
      /*reparent_to_init();*/                              \
      spin_lock_irq(&current->sigmask_lock);           \
      sigemptyset(&current->blocked);                  \
      recalc_sigpending(current);                      \
      spin_unlock_irq(&current->sigmask_lock);         \
      memset(current->comm, 0, sizeof(current->comm)); \
      strcpy(current->comm, "openrt-ctrl");            \
   } while (0)

#else

#define OPENOBJ_THREAD_INIT()                          \
   do {                                                \
      memset(current->comm, 0, sizeof(current->comm)); \
      strcpy(current->comm, "openrt-ctrl");            \
/*	1602MIG daemonize("openrt-ctrl");*/\
   } while (0)
#endif

#define OPENOBJ_THREAD_UNINIT()

#define OPENOBJ_INTERRUPT_LOCK() \
   ({                            \
      unsigned long flags;       \
      local_irq_save(flags);     \
      flags;                     \
   }                             \
   )

#define OPENOBJ_INTERRUPT_UNLOCK(f)    local_irq_restore(f)

#define OPENOBJ_THREAD_SLEEP(m)              \
   do {                                      \
      set_current_state(TASK_INTERRUPTIBLE); \
      schedule_timeout((m * HZ) / 1000);     \
      set_current_state(TASK_RUNNING);       \
   } while (0)



#endif /*__OPENRT_CTRL_IMPL_H__*/
