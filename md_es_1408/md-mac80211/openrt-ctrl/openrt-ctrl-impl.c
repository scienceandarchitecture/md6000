/********************************************************************************
* MeshDynamics
* --------------
* File     : openrt-ctrl-impl.c
* Comments : Open Rate Control Implementation for Linux Kernel
* Created  : 11/10/2008
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  0  |11/10/2008| Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/


#include "openrt-ctrl-impl.h"
#include <asm/types.h>

static unsigned long _last_jiffies = 0;

u64 openrt_ctrl_impl_timestamp()
{
#if 0
   u64 tick;

   /**
    * We only support 64 bit millisecond counting (Basically 1 overflow of jiffies)
    * This makes it 584942417.35 years of millisecond tick counting!!!
    * Jiffies overlfow after 490 days of operation.
    */

   if (jiffies < _last_jiffies)
   {
      tick  = 0xFFFFFFFF;
      tick += jiffies;
   }
   else
   {
      _last_jiffies = jiffies;
      tick          = jiffies;
   }

#define _FACTOR    (1000) / (HZ)

   tick *= _FACTOR;

#undef _FACTOR
#endif
   return jiffies;
}


EXPORT_SYMBOL(openrt_ctrl_impl_timestamp);
