/********************************************************************************
* MeshDynamics
* --------------
* File     : openrt-ctrl.h
* Comments : 802.11 Automatic Rate Control
* Created  : 12/2/2004
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* | 11  |7/20/2007 | Changes for high noise floor protection         | Sriram |
* -----------------------------------------------------------------------------
* | 10  |6/14/2007 | Added fixed_upper_rate_index                    | Sriram |
* -----------------------------------------------------------------------------
* |  9  |6/13/2007 | open_rt_ctrl_reset_item added                   | Sriram |
* -----------------------------------------------------------------------------
* |  8  |1/24/2007 | Changes for Private Info                        | Sriram |
* -----------------------------------------------------------------------------
* |  7  |01/17/2006| Changes for PS mode and RX Frag support         | Sriram |
* -----------------------------------------------------------------------------
* |  6  |4/14/2005 | Changed default TX Rate                         | Sriram |
* -----------------------------------------------------------------------------
* |  5  |1/14/2005 | OPEN_RT_CTRL_N_H_MAX added                      | Sriram |
* -----------------------------------------------------------------------------
* |  4  |1/7/2005  | Misc changes                                    | Sriram |
* -----------------------------------------------------------------------------
* |  3  |12/23/2004| Data reporting feature implemented              | Sriram |
* -----------------------------------------------------------------------------
* |  1  |12/22/2004| Misc changes                                    | Sriram |
* -----------------------------------------------------------------------------
* |  0  |12/2/2004 | Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

#ifndef __OPENRT_CTRL_H__
#define __OPENRT_CTRL_H__

#define __OPEN_RT_CTRL_ADDR_IS_BROADCAST(addr) \
   (                                           \
      (addr)[0] == 0xFF &&                     \
      (addr)[1] == 0xFF &&                     \
      (addr)[2] == 0xFF &&                     \
      (addr)[3] == 0xFF &&                     \
      (addr)[4] == 0xFF &&                     \
      (addr)[5] == 0xFF                        \
   )

#define __OPEN_RT_CTRL_ADDR_IS_MULTICAST(addr) \
   (                                           \
      (addr)[0] == 0x01 &&                     \
      (addr)[1] == 0x00 &&                     \
      (addr)[2] == 0x5E                        \
   )


#ifndef OPEN_RT_CTRL_INACTIVITY_TIMEOUT
#define OPEN_RT_CTRL_INACTIVITY_TIMEOUT    (60 * 60 * 1000)                                     /** 60 minutes */
#endif

#define OPEN_RT_CTRL_C_MAX                 100
#define OPEN_RT_CTRL_C_MIN                 1

#define OPEN_RT_CTRL_SGN_POSSITIVE         1
#define OPEN_RT_CTRL_SGN_NEGATIVE          -1

#define OPEN_RT_CTRL_N_H_MIN               1
#define OPEN_RT_CTRL_N_H_MAX               65536

#define OPEN_RT_CTRL_ITEM_SIGNATURE        0x11192004

#ifndef OPEN_RT_CTRL_ETH_ALEN
#define OPEN_RT_CTRL_ETH_ALEN              6
#endif

struct open_rt_ctrl_item
{
   open_object_t            object;
   unsigned char            address[OPEN_RT_CTRL_ETH_ALEN];
   int                      bucket;
   void                     *priv_info;

   /**
    * The input variables to the rate control system
    */

   unsigned int             tx_success;
   unsigned int             tx_error;
   unsigned int             tx_retry;
   OPENOBJ_TIMESTAMP_TYPE   last_tx;
   unsigned int             last_check;
   unsigned char            average_ack_rssi;

   /**
    * The output variables from the rate control system
    */

   unsigned int             rate_index;
   unsigned int             rate_index1;
   unsigned int             rate_index2;
   unsigned int             rate_index3;

   /**
    * Variables used by the rate control system. Purposely used
    * captial letters for these variables, to make it easier
    * and consistent with the mathematical document.
    */

   unsigned int             C;                                                  /** Value between 0 and 100 */
   int                      SGN;                                                /** Either -1 or + 1 */
   unsigned long            T_EL;                                               /** Time in OPEN_RT_CTRL_MIN_DURATION units at current rate */
   unsigned int             N_H;                                                /** Number of times hurt because of over confidence */
   unsigned long            T_CH;                                               /** Time in OPEN_RT_CTRL_MIN_DURATION units at which to increase rate */

   struct open_rt_ctrl_item *prev_hash;
   struct open_rt_ctrl_item *next_hash;
   struct open_rt_ctrl_item *prev_list;
   struct open_rt_ctrl_item *next_list;
};

typedef struct open_rt_ctrl_item   open_rt_ctrl_item_t;

typedef void *   (*open_rt_ctrl_on_create_priv_info_t)   (void *priv_id);
typedef void (*open_rt_ctrl_on_destroy_priv_info_t)  (void *priv_id, void *object);
typedef void (*open_rt_ctrl_on_process_priv_info_t)  (void *priv_id, unsigned char *address, void *object);
typedef void (*open_rt_ctrl_on_process_begin_t)              (void *priv_id);
typedef void (*open_rt_ctrl_on_process_end_t)                (void *priv_id);

#define OPEN_RT_CTRL_SIGNATURE    0x11212004

struct open_rt_ctrl
{
   open_object_t                       object;
   open_rt_ctrl_item_t                 **hash;
   int                                 hash_length;
   int                                 rate_count;
   int                                 initialized;
   int                                 t_el_max;
   int                                 t_el_min;
   int                                 qualification;
   int                                 max_retry_percent;
   int                                 max_error_percent;
   int                                 max_errors;
   int                                 min_retry_percent;
   int                                 min_qualification;
   int                                 intial_rate;
   int                                 fixed_upper_rate_index;
   char                                name[32];
   open_rt_ctrl_item_t                 *list_head;
   open_rt_ctrl_on_create_priv_info_t  on_create_item;
   open_rt_ctrl_on_destroy_priv_info_t on_destroy_item;
   open_rt_ctrl_on_process_priv_info_t on_process_item;
   open_rt_ctrl_on_process_begin_t     on_process_begin;
   open_rt_ctrl_on_process_end_t       on_process_end;
   void                                *priv_id;
};

typedef struct open_rt_ctrl   open_rt_ctrl_t;

#ifdef  __GNUC__
#define OPEN_RT_CTRL_HASH_FUNCTION(addr) \
   ({                                    \
      int hash;                          \
      hash = (addr)[0] * 1               \
             + (addr)[1] * 2             \
             + (addr)[2] * 3             \
             + (addr)[3] * 4             \
             + (addr)[4] * 5             \
             + (addr)[5] * 6;            \
      hash;                              \
   }                                     \
   )
#else
static int OPEN_RT_CTRL_HASH_FUNCTION(unsigned char *addr)
{                          \
   int hash;               \
   hash = (addr)[0] * 1    \
          + (addr)[1] * 2  \
          + (addr)[2] * 3  \
          + (addr)[3] * 4  \
          + (addr)[4] * 5  \
          + (addr)[5] * 6; \
   return hash;            \
}
#endif

static OPENOBJ_INLINE unsigned long __open_rt_ctrl_lock(void)
{
   unsigned long flags;

   flags = OPENOBJ_INTERRUPT_LOCK();
   return flags;
}


static OPENOBJ_INLINE void __open_rt_ctrl_unlock(unsigned long flags)
{
   OPENOBJ_INTERRUPT_UNLOCK(flags);
}


#define OPEN_RT_CTRL_LOCK(x)      (x) = __open_rt_ctrl_lock();
#define OPEN_RT_CTRL_UNLOCK(x)    __open_rt_ctrl_unlock((x))

OPEN_RT_CTRL_DIRECTIVE static OPENOBJ_INLINE open_rt_ctrl_item_t *open_rt_ctrl_get_item(open_rt_ctrl_t *rate_ctrl, unsigned char *address, int create_if_not_found)
{
   int bucket;
   open_rt_ctrl_item_t *item;
   unsigned long       flags;

   if /*1602MIG RRREVIEW*/(!rate_ctrl || !rate_ctrl->initialized)
   {
      return NULL;
   }

   bucket  = OPEN_RT_CTRL_HASH_FUNCTION(address);
   bucket %= rate_ctrl->hash_length;

   OPEN_RT_CTRL_LOCK(flags)
   {
      item = rate_ctrl->hash[bucket];
      while (item != NULL)
      {
         if (!memcmp(item->address, address, OPEN_RT_CTRL_ETH_ALEN))
         {
            OPENOBJ_INTERRUPT_UNLOCK(flags);
            OPENOBJ_RETURN_REFERENCE(item);
         }
         item = item->next_hash;
      }
   }
   OPEN_RT_CTRL_UNLOCK(flags);

   if (!create_if_not_found)
   {
      return NULL;
   }


   item = (open_rt_ctrl_item_t *)openobj_create(OPEN_RT_CTRL_ITEM_SIGNATURE,
                                                sizeof(open_rt_ctrl_item_t),
                                                NULL);

   memcpy(item->address, address, OPEN_RT_CTRL_ETH_ALEN);

   item->last_tx = OPENOBJ_TIMESTAMP();
   item->bucket  = bucket;

   item->C                = (OPEN_RT_CTRL_C_MAX / 3);
   item->N_H              = OPEN_RT_CTRL_N_H_MIN;
   item->SGN              = OPEN_RT_CTRL_SGN_NEGATIVE;
   item->T_EL             = 0;
   item->T_CH             = rate_ctrl->t_el_min;
   item->last_check       = 0;
   item->average_ack_rssi = 0xFF;

   if (rate_ctrl->on_create_item != NULL)
   {
      item->priv_info = rate_ctrl->on_create_item(rate_ctrl->priv_id);
   }
   else
   {
      item->priv_info = NULL;
   }

   if (__OPEN_RT_CTRL_ADDR_IS_BROADCAST(item->address) ||
       __OPEN_RT_CTRL_ADDR_IS_MULTICAST(item->address))
   {
      item->rate_index = 0;
   }
   else
   {
      if (rate_ctrl->fixed_upper_rate_index != -1)
      {
         item->rate_index = rate_ctrl->fixed_upper_rate_index;
      }
      else
      {
         item->rate_index = rate_ctrl->intial_rate;
      }

      /** First retry rate */
      if (item->rate_index - 1 > 0)
      {
         item->rate_index1 = item->rate_index - 1;
      }
      else
      {
         item->rate_index1 = 0;
      }
      /** Second retry rate */
      if (item->rate_index - 2 > 0)
      {
         item->rate_index2 = item->rate_index - 2;
      }
      else
      {
         item->rate_index2 = 0;
      }
      /** Third retry rate */
      if (item->rate_index - 3 > 0)
      {
         item->rate_index3 = item->rate_index - 3;
      }
      else
      {
         item->rate_index3 = 0;
      }
   }

   OPEN_RT_CTRL_LOCK(flags)
   {
      if (rate_ctrl->list_head != NULL)
      {
         rate_ctrl->list_head->prev_list = item;
      }

      item->next_list      = rate_ctrl->list_head;
      rate_ctrl->list_head = item;

      OPENOBJ_ADD_REFERENCE(item);                      /** Ref count = 2 */

      if (rate_ctrl->hash[bucket] != NULL)
      {
         rate_ctrl->hash[bucket]->prev_hash = item;
      }

      item->next_hash         = rate_ctrl->hash[bucket];
      rate_ctrl->hash[bucket] = item;

      OPENOBJ_ADD_REFERENCE(item);                      /** Ref count = 3 */
   }
   OPEN_RT_CTRL_UNLOCK(flags);

//	printk(KERN_EMERG "DEBUG: <%s>:<%d>\n", __func__, __LINE__);

   OPENOBJ_RELEASE_REFERENCE(item);             /** Ref count = 2 */

   OPENOBJ_RETURN_REFERENCE(item);              /** Ref count = 3 */
}


OPEN_RT_CTRL_DIRECTIVE static OPENOBJ_INLINE void open_rt_ctrl_reset_item(open_rt_ctrl_t *rate_ctrl, unsigned char *address)
{
   open_rt_ctrl_item_t *item;

   item = open_rt_ctrl_get_item(rate_ctrl, address, 0);

   if (item != NULL)
   {
      item->C                = (OPEN_RT_CTRL_C_MAX / 3);
      item->N_H              = OPEN_RT_CTRL_N_H_MIN;
      item->SGN              = OPEN_RT_CTRL_SGN_NEGATIVE;
      item->T_EL             = 0;
      item->T_CH             = rate_ctrl->t_el_min;
      item->average_ack_rssi = 0xFF;

      if (rate_ctrl->fixed_upper_rate_index != -1)
      {
         item->rate_index = rate_ctrl->fixed_upper_rate_index;
      }
      else
      {
         item->rate_index = rate_ctrl->intial_rate;
      }

      /** First retry rate */
      if (item->rate_index - 1 > 0)
      {
         item->rate_index1 = item->rate_index - 1;
      }
      else
      {
         item->rate_index1 = 0;
      }
      /** Second retry rate */
      if (item->rate_index - 2 > 0)
      {
         item->rate_index2 = item->rate_index - 2;
      }
      else
      {
         item->rate_index2 = 0;
      }
      /** Third retry rate */
      if (item->rate_index - 3 > 0)
      {
         item->rate_index3 = item->rate_index - 3;
      }
      else
      {
         item->rate_index3 = 0;
      }

      OPENOBJ_RELEASE_REFERENCE(item);
   }
}


typedef void * open_rt_ctrl_iterator_t;

OPEN_RT_CTRL_DIRECTIVE void open_rt_ctrl_initialize(int duration);
OPEN_RT_CTRL_DIRECTIVE void open_rt_ctrl_uninitialize(void);

OPEN_RT_CTRL_DIRECTIVE open_rt_ctrl_t *open_rt_ctrl_create(meshap_instance_t                   *instance,
                                                           int                                 hash_length,
                                                           int                                 rate_count,
                                                           void                                *priv_id,
                                                           open_rt_ctrl_on_create_priv_info_t  on_create,
                                                           open_rt_ctrl_on_destroy_priv_info_t on_destroy,
                                                           open_rt_ctrl_on_process_priv_info_t on_process,
                                                           open_rt_ctrl_on_process_begin_t     on_begin,
                                                           open_rt_ctrl_on_process_end_t       on_end);

OPEN_RT_CTRL_DIRECTIVE void open_rt_ctrl_start(open_rt_ctrl_t *rate_ctrl,
                                               int            t_el_max,
                                               int            t_el_min,
                                               int            qualification,
                                               int            max_retry_percent,
                                               int            max_error_percent,
                                               int            max_errors,
                                               int            min_retry_percent,
                                               int            min_qualification,
                                               int            intial_rate);

OPEN_RT_CTRL_DIRECTIVE void open_rt_ctrl_destroy(open_rt_ctrl_t *rate_ctrl);

OPEN_RT_CTRL_DIRECTIVE void _openrt_ctrl_setup_rate_table(meshap_instance_t *instance, int phy_mode_changed, struct net_device *dev);
OPEN_RT_CTRL_DIRECTIVE void _setup_supported_rates(meshap_instance_t *instance);
OPEN_RT_CTRL_DIRECTIVE void _openrt_ctrl_calc_channel_count(meshap_instance_t *instance);


OPEN_RT_CTRL_DIRECTIVE void *_on_open_rt_ctrl_create_item(void *priv_id);

OPEN_RT_CTRL_DIRECTIVE void _on_open_rt_ctrl_destroy_item(void *priv_id, void *priv_info);
OPEN_RT_CTRL_DIRECTIVE void _on_open_rt_ctrl_process_item(void *priv_id, unsigned char *address, void *priv_info);
OPEN_RT_CTRL_DIRECTIVE void _open_rt_ctrl_on_process_begin(void *priv_id);
OPEN_RT_CTRL_DIRECTIVE void _open_rt_ctrl_on_process_end(void *priv_id);

int dummy7(void);
int dummy8(void);

#endif /*__OPENRT_CTRL_H__*/
