/********************************************************************************
* MeshDynamics
* --------------
* File     : _meshap_sta_fsm_connected.c
* Comments : Atheros 802.11 FSM Connected state code
* Created  : 10/13/2004
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  0  |10/13/2004| Created                                         | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

/**
 * This is a private source file included by meshap_sta_fsm.c, hence
 * the name begins with an underscore.
 * It was created to improve readability.
 */

OPENFSM_ENTER_STATE_FUNCTION(connected)
{
   return 0;
}

OPENFSM_STATE_FUNCTION(connected)
{
   return 0;
}

OPENFSM_LEAVE_STATE_FUNCTION(connected)
{
   return 0;
}

OPENFSM_STATE_TIMER_FUNCTION(connected)
{
   return 0;
}
