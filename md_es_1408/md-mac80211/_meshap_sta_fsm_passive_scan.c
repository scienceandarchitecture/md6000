/********************************************************************************
* MeshDynamics
* --------------
* File     : _meshap_sta_fsm_passive_scan.c
* Comments : Atheros STA FSM passive scan entry function
* Created  : 10/18/2004
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  4  |1/27/2009 | Changes for Uplink Scan Functionality           | Sriram |
* -----------------------------------------------------------------------------
* |  3  |4/11/2005 | Misc changes                                    | Sriram |
* -----------------------------------------------------------------------------
* |  2  |12/23/2004| Parameter added to meshap_reset                | Sriram |
* -----------------------------------------------------------------------------
* |  1  |12/22/2004| Misc changes                                    | Sriram |
* -----------------------------------------------------------------------------
* |  0  |10/18/2004| Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

/**
 * This is a private source file included by meshap_sta_fsm.c, hence
 * the name begins with an underscore.
 * It was created to improve readability.
 */

/**
 * Both the active scan and the passive scan
 * assume that the FSM is now in the disconnected
 * state. Assumes current PHY mode has been set.
 */
#include <net/wireless/core.h>

int meshap_get_current_band_channel_count(struct wireless_dev *wdev,
                                       meshap_instance_t *instance)
{
    enum ieee80211_band band;
    struct ieee80211_supported_band *sband;

    band = meshap_get_current_band(instance->current_phy_mode);
    sband = wdev->wiphy->bands[band];
    if (!sband)
    {
        al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "STA_FSM :ERROR:  <%s> band <%d> is not defined. Can't request scan %s : %d\n",
                      instance->dev->name, band, __func__, __LINE__);
        return 0;
    }
    return sband->n_channels;
}

int meshap_prepare_scan_channel_list(meshap_instance_t *instance,
                                   enum ieee80211_band band,
                                   struct wireless_dev *wdev,
                                   unsigned char *channels,
                                   int channel_count,
                                   struct cfg80211_scan_request *request)
{
    int freq, j, i = 0;
    struct ieee80211_supported_band   *sband;
    struct ieee80211_channel          *chan;

    sband = wdev->wiphy->bands[band];
    if (!sband)
    {
        al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "STA_FSM :ERROR: <%s> band <%d> is not defined. Can't request scan %s : %d\n",
                   instance->dev->name, band, __func__, __LINE__);
        return -1;
    }
    if (channels == NULL)
    {
        for (j = 0; j < sband->n_channels; j++)
        {
            chan = &sband->channels[j];
            if (!chan->center_freq || chan->flags & IEEE80211_CHAN_DISABLED)
            {
                if (!chan->center_freq)
                    break;
                continue;
            }
            if (chan->center_freq >= 5260 && chan->center_freq <= 5320)
                continue;
            else
            {
            request->channels[i] = chan;
            }
            request->channels[i] = chan;
            i++;
        }
    }
    else
    {
        for (j = 0; j < channel_count; j++)
        {
            instance->current_channel = &instance->channels[channels[j]];
            freq = instance->current_channel->frequency;
            request->channels[i] = ieee80211_get_channel((wdev->wiphy), freq);
            i++;
        }
    }
    return i;
}

int meshap_trigger_sw_scan(meshap_instance_t *instance,
                        unsigned char *channels,
                        int channel_count)
{
    int err = 0, n_ssids = 0, n_channels = 0;
    size_t ie_len = 0;
    enum ieee80211_band                band;
    struct cfg80211_scan_request      *request;
    struct cfg80211_registered_device *rdev;
    struct wireless_dev               *wdev = instance->dev->ieee80211_ptr;

    rdev = WIPHY_TO_DEV(wdev->wiphy);

    /* Before triggering scan mode check interface current scan state is NO_SCAN and 
     * no previous scan request reference hanging around */
    if ((instance->scan_state == SCAN_SW_SCANNING) || rdev->scan_req)
    {
        al_print_log(AL_CONTEXT AL_LOG_TYPE_DEBUG, "STA_FSM :ERROR: %s<%d>  Already in scanning. scan_req : %p\n",
        __func__, __LINE__, rdev->scan_req);
        return -1;
    }

    instance->scan_state = NO_SCAN;
    if (channels == NULL)
        n_channels = meshap_get_current_band_channel_count(wdev, instance);
    else
        n_channels = channel_count;

    request = kzalloc(sizeof(*request)
        +sizeof(*request->ssids) * n_ssids
        +sizeof(*request->channels) * n_channels
        +ie_len, GFP_KERNEL);

    if (!request) {
        al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "STA_FSM :ERROR: %s<%d> No memory to compose scan request\n",
        __func__, __LINE__);
        return ENOMEM;
    }
    band = meshap_get_current_band(instance->current_phy_mode);
    if ((n_channels = meshap_prepare_scan_channel_list(instance, band, wdev, channels, channel_count, request)) <= 0)
    {
        err = -1;
        al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "STA_FSM :ERROR: %s<%d> <%s> No channel found TODO scan in band <%d> : "
        ,__func__, __LINE__, instance->dev->name, band);
        goto clean_exit;
    }

    request->n_ssids = n_ssids;
    request->ie = NULL;
    request->n_channels = n_channels;
    request->wdev = wdev;
    request->wiphy = &rdev->wiphy;
    request->scan_start = jiffies;
    rdev->scan_req = request;
    rdev->scan_req->flags = NL80211_SCAN_FLAG_FLUSH;

    err = rdev->ops->scan(&rdev->wiphy, request);
    if(err) {
        goto clean_exit;
    }
    else if (err == 0) {
        instance->scan_state = SCAN_SW_SCANNING;
        instance->scan_start_time = jiffies;
        al_print_log(AL_CONTEXT AL_LOG_TYPE_DEBUG, "STA_FSM :ERROR: %s<%d> Scan start time -- %lu --\n ",__func__, __LINE__,
        instance->scan_start_time);
    }
    return err;
clean_exit:
    kfree(request);
    return err;
}

int meshap_sta_fsm_passive_scan(meshap_instance_t            *instance,
                                int                          channel_count,
                                unsigned char                *channels,
                                int                          dwell_timeout,
                                meshap_sta_fsm_scan_result_t **results,
                                int                          *results_count)
{
   _meshap_sta_fsm_data_t *data;
   int                               channel, ret = 0;
   _scan_param_t                     *param;
   unsigned long                     flags;
   int                               freq;
   enum nl80211_channel_type         channel_type = NL80211_CHAN_NO_HT;
   struct wireless_dev               *wdev        = instance->dev->ieee80211_ptr;
   struct cfg80211_registered_device *rdev;
   struct cfg80211_chan_def chandef;
   struct ieee80211_sub_if_data *sdata = IEEE80211_DEV_TO_SUB_IF(instance->dev);   //1602MIG from 1602
   DEFINE_WAIT(wait);

   rdev = WIPHY_TO_DEV(wdev->wiphy);

   data    = (_meshap_sta_fsm_data_t *)instance->fsm_data;
   channel = -1;
   param   = (_scan_param_t *)kmalloc(sizeof(_scan_param_t), GFP_ATOMIC);
   if (param == NULL)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "STA_FSM :ERROR: %s<%d> Memory alloc failed \n ",__func__, __LINE__);
      return -1;
   }

   memset(param, 0, sizeof(_scan_param_t));

   if((rdev->ops->scan != NULL ) && ((instance->use_type == AL_CONF_IF_USE_TYPE_DS) || (instance->use_type == AL_CONF_IF_USE_TYPE_PMON))) {
       if (channel_count == 0)
           dwell_timeout = SCAN_WAIT_TIME_PER_CHANNEL * meshap_get_current_band_channel_count(wdev, instance);
       else
           dwell_timeout = SCAN_WAIT_TIME_PER_CHANNEL * channel_count;
       if (instance->use_type == AL_CONF_IF_USE_TYPE_DS)
           dwell_timeout = dwell_timeout * 2;
   }

   param->dwell_timeout = dwell_timeout;
   param->total_results = 0;

   OPENFSM_LOCK(data->fsm, flags);
   
   instance->connected = 0;
   instance->fsm_state = MESHAP_STA_FSM_STATE_DISCONNECTED;

   openfsm_reset(data->fsm, &_states[_MESHAP_STA_FSM_DISCONNECTED]);
   openfsm_set_state_data(data->fsm, param);
   OPENFSM_UNLOCK(data->fsm, flags);

   /*update channel_type based on secondary channel offset*/
   if(is_11n_supported(instance , 0)){
       channel_type = NL80211_CHAN_HT20 ;
   }

   if(instance->sec_chan_offset == 1) {
       channel_type = NL80211_CHAN_HT40PLUS;
   }else if(instance->sec_chan_offset == -1) {
       channel_type = NL80211_CHAN_HT40MINUS;
   }

    /* We have to put interface in scan mode when we have to perform scan on BH-Uplink interface.
     * If interface is Monitor, then no need trigger scan mode to perform scan */
   if((rdev->ops->scan != NULL ) && ((instance->use_type == AL_CONF_IF_USE_TYPE_DS )|| (instance->use_type == AL_CONF_IF_USE_TYPE_PMON))){
       if(meshap_trigger_sw_scan(instance, channels, channel_count) != 0)
       {
        al_print_log(AL_CONTEXT AL_LOG_TYPE_DEBUG, "STA_FSM :ERROR: %s<%d> scan_trigger_mode  FAILED\n ",__func__, __LINE__);
           //msleep(UPLINK_SCAN_REATTEMPT_DELAY);
           goto exit_passive_scan;
       }
       if (instance->use_type == AL_CONF_IF_USE_TYPE_DS) {
         al_print_log(AL_CONTEXT AL_LOG_TYPE_DEBUG, "STA_FSM :INFO: %s: Transition to state _MESHAP_STA_FSM_PSCAN_1 : %s: %d\n",
								 instance->dev->name, __func__, __LINE__);
         openfsm_transition(data->fsm, &_states[_MESHAP_STA_FSM_PSCAN_1]);
       }
       INTERRUPTIBLE_SLEEP_ON_TIMEOUT(&data->wqh, ((dwell_timeout * HZ)  / 1000), &wait);

       /* Look for scan completion status only when scan performed on BH-Uplink */
       if (sdata->local->hw.instance->scan_state == SCAN_COMPLETED) {
           _process_scan_results(instance);
           sdata->local->hw.instance->scan_state = NO_SCAN;
           ret = 0;
       }
       else {
        al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "STA_FSM :INFO: %s<%d> <%s> Scan not completed in Channel = %d : %d\n",
        __func__, __LINE__, instance->dev->name, ieee80211_frequency_to_channel(sdata->local->scan_chandef.center_freq1),
        sdata->local->scan_chandef.center_freq1);
        ret = -1;
       }
       goto exit_passive_scan;
   }
#if 0
   while (1)
   {
       if ((channel_count == 0) || (channels == NULL) ||
           ((channel_count == 1) && (channels[0] == 0)) /* Paranoid protection againts illegal values */)
       {
           channel = _set_next_channel(instance, channel);
           if (channel < 0)
           {
               break;
           }
       }
       else
       {
           if (++channel >= channel_count)
           {
               break;
           }
           instance->current_channel = &instance->channels[channels[channel]];
       }

       freq = instance->current_channel->frequency;

       /**
        * Reset the device so that channel change takes place
        */
       cfg80211_chandef_create(&chandef, ieee80211_get_channel((wdev->wiphy), freq) /*&chandef.chan*/, channel_type);
       if (!chandef.chan) {
           al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "STA_FSM :ERROR: %s<%d> Failed to chandef for channel = %d\n ",
           __func__, __LINE__, ieee80211_frequency_to_channel(freq));
           break;
       }
       mutex_lock(&sdata->local->mtx);
       if (ieee80211_vif_use_channel(sdata, &chandef, IEEE80211_CHANCTX_SHARED))
       {
           al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "STA_FSM :ERROR: %s<%d> Failed to set channel for %s\n ",
           __func__, __LINE__, instance->dev->name);
       }
       mutex_unlock(&sdata->local->mtx);
       /**
        * Transition to state _MESHAP_STA_FSM_PSCAN_1, which listens to
        * beacons and notes them down, before getting back to disconnected
        * state after a timeout. We do not call openfsm_work. Instead
        * it is called by meshap_sta_fsm_process_mgmt_frame, for beacons
        * in case the FSM is in the pscan1 state.
        */

       openfsm_transition(data->fsm, &_states[_MESHAP_STA_FSM_PSCAN_1]);

       INTERRUPTIBLE_SLEEP_ON_TIMEOUT(&data->wqh, ((2 * dwell_timeout * HZ)  / 1000), &wait);

   }
#endif

exit_passive_scan:
   OPENFSM_LOCK(data->fsm, flags);

   openfsm_set_state_data(data->fsm, NULL);
   openfsm_reset(data->fsm, &_states[_MESHAP_STA_FSM_DISCONNECTED]);
   instance->scan_state = NO_SCAN;

   OPENFSM_UNLOCK(data->fsm, flags);

   /**
    * Allocate memory for the results o/p parameter
    */

   *results = (meshap_sta_fsm_scan_result_t *)kmalloc(param->total_results * sizeof(meshap_sta_fsm_scan_result_t), GFP_ATOMIC);

   memcpy(*results, param->results, param->total_results * sizeof(meshap_sta_fsm_scan_result_t));

   *results_count = param->total_results;

   kfree(param);

   return ret;
}
