/********************************************************************************
* MeshDynamics
* --------------
* File     : _meshap_sta_fsm_assoc_sent.c
* Comments : Atheros 802.11 FSM Assoc sent state code
* Created  : 10/13/2004
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  0  |10/13/2004| Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

/**
 * This is a private source file included by meshap_sta_fsm.c, hence
 * the name begins with an underscore.
 * It was created to improve readability.
 */

OPENFSM_ENTER_STATE_FUNCTION(assoc_sent)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Enter %s: %d\n", __func__, __LINE__);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Exit %s: %d\n", __func__, __LINE__);
   return 0;
}

OPENFSM_STATE_FUNCTION(assoc_sent)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Enter %s: %d\n", __func__, __LINE__);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Exit %s: %d\n", __func__, __LINE__);
   return 0;
}

OPENFSM_LEAVE_STATE_FUNCTION(assoc_sent)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Enter %s: %d\n", __func__, __LINE__);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Exit %s: %d\n", __func__, __LINE__);
   return 0;
}

OPENFSM_STATE_TIMER_FUNCTION(assoc_sent)
{
#if 0
   _join_param_t          *param;
   _meshap_sta_fsm_data_t *data;
   unsigned long          flags;

   /**
    * state_timer_count indicates the number of time units that
    * have elapsed in this state. If the total time is greater than
    * MESHAP_STA_FSM_AUTH_RESP_TIMEOUT, we wake up the sleeping thread.
    */

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Enter %s: %d\n", __func__, __LINE__);
   OPENFSM_LOCK(fsm, flags);

   param = (_join_param_t *)state_data;
   data  = (_meshap_sta_fsm_data_t *)openfsm_get_fsm_data(fsm);

   if (state_timer_count * MESHAP_STA_FSM_TIMER_INTERVAL > MESHAP_STA_FSM_ASSOC_RESP_TIMEOUT)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_DEBUG, "MAC80211 : DEBUG : wakeup thread %s: %d\n", __func__, __LINE__);
      wake_up(&data->wqh);
   }

   OPENFSM_UNLOCK(fsm, flags);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Exit %s: %d\n", __func__, __LINE__);
#endif
   return 0;
}
