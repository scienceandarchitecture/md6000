#include <meshap_instance.h>
#include "meshap_sta_fsm.h"
#include "meshap.h"
#include "al.h"
#include "al_conf.h"
#include "linux/ieee80211.h"
#include <../ieee80211_i.h>
#include "meshap_round_robin.h"
#include "meshap_mac80211.h"
#include <net/rtnetlink.h>
#include <linux/netdevice.h>
#include <net/sch_generic.h>
#include <net/wireless/core.h>
#include <net/mac80211.h>
#include "openrt-ctrl/openrt-ctrl-impl.h"
#include "openobj/openobj.h"
#include "openrt-ctrl/openrt-ctrl.h"


#if (LINUX_VERSION_CODE == 199947)
#define x86_ONLY
#endif


#define TRUE    1
#define EID_MAX_LENGTH                256

unsigned int MESHAP_INSTANCE_DUTY_CYCLE_FLAG = 0x00000001;

struct task_struct *meshap_process_task = NULL; 
static void meshap_process_pkt_que(void *data);
spinlock_t skb_mgmt_que_splock;

int ieee80211_set_channel(struct wiphy              *wiphy,
                          struct net_device         *netdev,
                          struct ieee80211_channel  *chan,
                          enum nl80211_channel_type channel_type);

void ieee80211_bss_info_change_notify(struct ieee80211_sub_if_data *sdata,
                                      u32                          changed);

char *meshap_get_parent_name(struct wiphy *wiphy);

void __ieee80211_disconnect(struct ieee80211_sub_if_data *sdata);

static const unsigned char _snap_header[] = { 0xAA, 0xAA, 0x03, 0x00, 0x00, 0x00 };
void dump_bytes(char *D, int count);
void dump_bytes1(char *D, int count);

int reboot_in_progress;
#ifndef MESHAP_NO_POWER_SAVE_MODE
static void _meshap_clear_power_save_count(meshap_instance_t *instance)
{
}
#endif

/*check current phy mode if it supports 11n return 1*/
int is_11n_supported(meshap_instance_t *instance, char band)
{

	if (NULL == instance) {
		al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MD-MAC80211:ERROR: %s<%d> INSTANCE is NULL\n",__func__, __LINE__);
		return -1;
	}

	switch (band) {
		case 0: /*If N supports in eighter 2.4Ghz or in 5Ghz*/
			switch (instance->current_phy_mode) {
				case WLAN_PHY_MODE_802_11_AN:
				case WLAN_PHY_MODE_802_11_BGN:
                case WLAN_PHY_MODE_802_11_N:
				case WLAN_PHY_MODE_802_11_2GHZ_PURE_N:
				case WLAN_PHY_MODE_802_11_5GHZ_PURE_N:
                case WLAN_PHY_MODE_802_11_ANAC:
                case WLAN_PHY_MODE_802_11_AC:
					return 1;
				default:
					return 0;
			}
		case 1: /*If N supports in 5Ghz*/
			switch (instance->current_phy_mode) {
				case WLAN_PHY_MODE_802_11_AN:
                case WLAN_PHY_MODE_802_11_N:
				case WLAN_PHY_MODE_802_11_5GHZ_PURE_N:
                case WLAN_PHY_MODE_802_11_ANAC:
                case WLAN_PHY_MODE_802_11_AC:
					return 1;
				default:
					return 0;
			}
		case 2: /*If N supports in 2.4Ghz*/
			switch (instance->current_phy_mode) {
				case WLAN_PHY_MODE_802_11_BGN:
                case WLAN_PHY_MODE_802_11_N:
				case WLAN_PHY_MODE_802_11_2GHZ_PURE_N:
					return 1;
				default:
					return 0;
			}
		default :
			return 0;
	}
}

int is_11ac_supported(meshap_instance_t *instance)
{

    if (NULL == instance) {
		  al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MD-MAC80211:ERROR: %s<%d> INSTANCE is NULL\n",__func__, __LINE__);
        return -1;
    }

    switch (instance->current_phy_mode) {
        case WLAN_PHY_MODE_802_11_AC:
        case WLAN_PHY_MODE_802_11_ANAC:
            return 1;
        default:
            return 0;
    }
}

void meshap_set_sta_ht_capab(meshap_instance_t *instance, struct al_802_11_ht_capabilities *ht_cap)
{
    u16 cap;

    al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MD-MAC80211:FLOW: Enter %s<%d>\n",__func__,__LINE__);
    if (ht_cap == NULL) {
        al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MD-MAC80211:ERROR: %s<%d> HT_CAP is NULL\n",__func__,__LINE__);
        return;
    }
    cap = le16_to_cpu(ht_cap->ht_capabilities_info);

    /*
     * Mask out HT features we don't support, but don't overwrite
     * non-symmetric features like STBC and SMPS. Just because
     * we're not in dynamic SMPS mode the STA might still be.
     */
    cap &= (instance->ht_capab_info | AL_HT_CAP_INFO_RX_STBC_MASK |
           AL_HT_CAP_INFO_TX_STBC | AL_HT_CAP_INFO_SMPS_MASK);

    /*
     * STBC needs to be handled specially
     * if we don't support RX STBC, mask out TX STBC in the STA's HT caps
     * if we don't support TX STBC, mask out RX STBC in the STA's HT caps
     */
    if (!(instance->ht_capab_info & AL_HT_CAP_INFO_RX_STBC_MASK))
        cap &= ~AL_HT_CAP_INFO_TX_STBC;
    if (!(instance->ht_capab_info & AL_HT_CAP_INFO_TX_STBC))
        cap &= ~AL_HT_CAP_INFO_RX_STBC_MASK;

    ht_cap->ht_capabilities_info = cpu_to_le16(cap);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MD-MAC80211:FLOW: Exit %s<%d>\n",__func__,__LINE__);

}


void meshap_set_sta_vht_capab(meshap_instance_t *instance, struct al_802_11_vht_capabilities  *vht_cap )
{
    u32 cap, own_cap, sym_caps;

    al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MD-MAC80211:FLOW: Enter %s<%d>\n",__func__,__LINE__);
    if (vht_cap == NULL) {
        al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MD-MAC80211:ERROR: %s<%d> VHT_CAP is NULL\n",__func__,__LINE__);
        return;
    }

    cap = le32_to_cpu(vht_cap->vht_capabilities_info);
    own_cap = instance->vht_capab_info;

    /* mask out symmetric VHT capabilities we don't support */
    sym_caps = AL_VHT_CAP_SHORT_GI_80 | AL_VHT_CAP_SHORT_GI_160;
    cap &= ~sym_caps | (own_cap & sym_caps);

    /* mask out beamformer/beamformee caps if not supported */
    if (!(own_cap & AL_VHT_CAP_SU_BEAMFORMER_CAPABLE))
        cap &= ~(AL_VHT_CAP_SU_BEAMFORMEE_CAPABLE |
                AL_VHT_CAP_BEAMFORMEE_STS_MAX);

    if (!(own_cap & AL_VHT_CAP_SU_BEAMFORMEE_CAPABLE))
        cap &= ~(AL_VHT_CAP_SU_BEAMFORMER_CAPABLE |
                AL_VHT_CAP_SOUNDING_DIMENSION_MAX);

    if (!(own_cap & AL_VHT_CAP_MU_BEAMFORMER_CAPABLE))
        cap &= ~AL_VHT_CAP_MU_BEAMFORMEE_CAPABLE;

    if (!(own_cap & AL_VHT_CAP_MU_BEAMFORMEE_CAPABLE))
        cap &= ~AL_VHT_CAP_MU_BEAMFORMER_CAPABLE;

    /* mask channel widths we don't support */
    switch (own_cap & AL_VHT_CAP_SUPP_CHAN_WIDTH_MASK) {
        case AL_VHT_CAP_SUPP_CHAN_WIDTH_160_80PLUS80MHZ:
            break;
        case AL_VHT_CAP_SUPP_CHAN_WIDTH_160MHZ:
            if (cap & AL_VHT_CAP_SUPP_CHAN_WIDTH_160_80PLUS80MHZ) {
                cap &= ~AL_VHT_CAP_SUPP_CHAN_WIDTH_160_80PLUS80MHZ;
                cap |= AL_VHT_CAP_SUPP_CHAN_WIDTH_160MHZ;
            }
            break;
        default:
            cap &= ~AL_VHT_CAP_SUPP_CHAN_WIDTH_MASK;
            break;
    }

    if (!(cap & AL_VHT_CAP_SUPP_CHAN_WIDTH_MASK))
        cap &= ~AL_VHT_CAP_SHORT_GI_160;

    /*
     *      * if we don't support RX STBC, mask out TX STBC in the STA's HT caps
     *           * if we don't support TX STBC, mask out RX STBC in the STA's HT caps
     *                */
    if (!(own_cap & AL_VHT_CAP_RXSTBC_MASK))
        cap &= ~AL_VHT_CAP_TXSTBC;
    if (!(own_cap & AL_VHT_CAP_TXSTBC))
        cap &= ~AL_VHT_CAP_RXSTBC_MASK;

    vht_cap->vht_capabilities_info = cpu_to_le32(cap);
    al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MD-MAC80211:FLOW: Exit %s<%d>\n",__func__,__LINE__);
}

static inline u8 wmm_aci_aifsn(int aifsn, int acm, int aci)
{
    u8 ret;
    ret = (aifsn << WMM_AC_AIFNS_SHIFT) & WMM_AC_AIFSN_MASK;
    if (acm)
        ret |= WMM_AC_ACM;
    ret |= (aci << WMM_AC_ACI_SHIFT) & WMM_AC_ACI_MASK;
    return ret;
}


static inline u8 wmm_ecw(int ecwmin, int ecwmax)
{
    return ((ecwmin << WMM_AC_ECWMIN_SHIFT) & WMM_AC_ECWMIN_MASK) |
        ((ecwmax << WMM_AC_ECWMAX_SHIFT) & WMM_AC_ECWMAX_MASK);
}



int meshap_build_wmm_ie(meshap_instance_t *instance, u8 *eid){
    struct meshap_wmm_parameter_element wmm ;
    int e;
    wmm.oui[0] = 0x00;
    wmm.oui[1] = 0x50;
    wmm.oui[2] = 0xf2;
    wmm.oui_type = WMM_OUI_TYPE;
    wmm.oui_subtype = WMM_OUI_SUBTYPE_PARAMETER_ELEMENT;
    wmm.version = WMM_VERSION;
    wmm.qos_info = 0x00;
    wmm.reserved = 0;
    wmm.ac[0].aci_aifsn = 0x03 ;
    wmm.ac[0].cw  = 0xa4;
    wmm.ac[0].txop_limit = 0 ;
    wmm.ac[1].aci_aifsn = 0x27 ;
    wmm.ac[1].cw  = 0xa4;
    wmm.ac[1].txop_limit = 0 ;
    wmm.ac[2].aci_aifsn = 0x42 ;
    wmm.ac[2].cw  = 0xa4;
    wmm.ac[2].txop_limit = 0 ;
    wmm.ac[3].aci_aifsn = 0x62 ;
    wmm.ac[3].cw  = 0xa4;
    wmm.ac[3].txop_limit = 0 ;
    /* fill in a parameter set record for each AC */
    eid[0] = WLAN_EID_VENDOR_SPECIFIC;
    eid[1] = sizeof(wmm);
    memcpy((void *)&eid[2], (void *)&wmm, eid[1]);
    return 0;


}


/****************************************************************************
 * This function checks weather configured secondary channel is valid or not for HT40 operation.
 * @input : meshap_instance.
 * @return : sets the proper channel for operation.
 * @ret : 
 * ***************************************************************************/

int meshap_80211n_allowed_ht40_channel_pair(meshap_instance_t *instance)
{
    int sec_chan, ok, j, first;
    int allowed[] = { 36, 44, 52, 60, 100, 108, 116, 124, 132, 149, 157,
        184, 192 };
    size_t k;
    al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MD-MAC80211: FLOW : Enter: %s : %d\n", __func__,__LINE__);

    if (!instance->sec_chan_offset)
        return 1; /* HT40 not used */

    sec_chan = instance->current_channel->number + instance->sec_chan_offset * 4;

    /* Verify that HT40 secondary channel is an allowed 20 MHz channel */
    ok = 0;

    for (j = 0; j < instance->channel_count; j++) {
        if (instance->channels[j].number == sec_chan) {
            ok = 1;
            break;
        }
    }
    if (!ok) {
        al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MD-MAC80211: ERROR : HT40 secondary channel %d not allowed %s : %d\n", sec_chan, __func__,__LINE__);
        return 0;
    }
    /*
     * Verify that HT40 primary,secondary channel pair is allowed per
     * IEEE 802.11n Annex J. This is only needed for 5 GHz band since
     * 2.4 GHz rules allow all cases where the secondary channel fits into
     * the list of allowed channels (already checked above).
     */
    if (instance->current_phy_mode == WLAN_PHY_MODE_802_11_BGN)
        return 1;

    if (instance->sec_chan_offset > 0)
        first = instance->current_channel->number;
    else
        first = sec_chan;

    ok = 0;
    for (k = 0; k < ARRAY_SIZE(allowed); k++) {
        if (first == allowed[k]) {
            ok = 1;
            break;
        }
    }
    if (!ok) {
        al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MD-MAC80211: ERROR : HT40 channel pair (%d, %d) not allowed %s : %d\n",
                         instance->current_channel->number, instance->sec_chan_offset, __func__,__LINE__);
        return 0;
    }

    al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MD-MAC80211: FLOW : Exit: %s : %d\n", __func__,__LINE__);
    return 1;
}

/*build HT Capability IE(EID 45), return 0 on success */
int meshap_build_ht_capability_ie(meshap_instance_t *instance, u8 *eid)
{
	struct wireless_dev *wdev;
	enum ieee80211_band band;
	struct ieee80211_supported_band *sband;
	struct ieee80211_ht_cap ht_capab;
//   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MD-MAC80211: FLOW : Enter: %s : %d\n", __func__,__LINE__);

	if (NULL == instance) {
		al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MD-MAC80211:ERROR: %s<%d> INSTANCE is NULL\n",__func__, __LINE__);
		return -1;
	}

	wdev = instance->dev->ieee80211_ptr;
	if (NULL == wdev) {
		al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MD-MAC80211:ERROR: %s<%d> WDEV is NULL\n",__func__, __LINE__);
		return -1;
	}

	for (band = 0; band < IEEE80211_NUM_BANDS; band++) {
		sband = wdev->wiphy->bands[band];
		if (!sband) {
			continue;
		}else {
			memset((void *)&ht_capab, 0, sizeof(ht_capab));
			ht_capab.cap_info = cpu_to_le16(instance->ht_capab_info);
			ht_capab.ampdu_params_info = instance->a_mpdu_params & 0x3; 
			ht_capab.ampdu_params_info |= sband->ht_cap.ampdu_density << 2; 
			memcpy((void *)&ht_capab.mcs, (void *)&sband->ht_cap.mcs, sizeof(sband->ht_cap.mcs));
			/* TODO: ht_extended_capabilities (now fully disabled) */
			/* TODO: tx_bf_capability_info (now fully disabled) */
			/* TODO: asel_capabilities (now fully disabled) */

			eid[0] = WLAN_EID_HT_CAPABILITY;
			eid[1] = sizeof(ht_capab);
			memcpy((void *)&eid[2], (void *)&ht_capab, eid[1]);
  //       al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MD-MAC80211: FLOW : Exit: %s : %d\n", __func__,__LINE__);
			return 0;
		}
	}

   al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MD-MAC80211: ERROR : band %d %s : %d\n", band, __func__,__LINE__);
	return -1;
}

/*Build HT Capability IE(EID 61), return 0 on success */
int meshap_build_ht_operation_ie(meshap_instance_t *instance, u8 *eid)
{
	struct ieee80211_ht_operation ht_opera;
//   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MD-MAC80211: FLOW : Enter: %s : %d\n", __func__,__LINE__);

	if (NULL == instance) {
		al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MD-MAC80211:ERROR: %s<%d> INSTANCE is NULL\n",__func__, __LINE__);
		return -1;
	}

	memset((void *)&ht_opera, 0, sizeof(ht_opera));

	ht_opera.primary_chan = instance->current_channel->number;
	if(instance->sec_chan_offset == 1) {
		ht_opera.ht_param |= IEEE80211_HT_PARAM_CHAN_WIDTH_ANY
							| IEEE80211_HT_PARAM_CHA_SEC_ABOVE;
	}else if (instance->sec_chan_offset == -1){
		ht_opera.ht_param |= IEEE80211_HT_PARAM_CHAN_WIDTH_ANY
							| IEEE80211_HT_PARAM_CHA_SEC_BELOW;
	}
	ht_opera.operation_mode = cpu_to_le16(instance->ht_opera);
	 
	eid[0] = WLAN_EID_HT_OPERATION;
	eid[1] = sizeof(ht_opera);
	memcpy((void *)&eid[2], (void *)&ht_opera, eid[1]);

 //  al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MD-MAC80211: FLOW : Exit: %s : %d\n", __func__,__LINE__);
	return 0;
}

int meshap_build_vht_capability_ie(meshap_instance_t *instance, u8 *eid)
{
	struct wireless_dev *wdev;
	enum ieee80211_band band;
	struct ieee80211_supported_band *sband;
	struct ieee80211_vht_cap vht_capab;
//   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MD-MAC80211: FLOW : Enter: %s : %d\n", __func__,__LINE__);

	if (NULL == instance) {
		al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MD-MAC80211:ERROR: %s<%d> INSTANCE is NULL\n",__func__, __LINE__);
		return -1;
	}

	wdev = instance->dev->ieee80211_ptr;
	if (NULL == wdev) {
		al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MD-MAC80211:ERROR: %s<%d> WDEV is NULL\n",__func__, __LINE__);
		return -1;
	}

	for (band = 0; band < IEEE80211_NUM_BANDS; band++) {
		sband = wdev->wiphy->bands[band];
		if (!sband) {
			continue;
		}else {
			memset((void *)&vht_capab, 0, sizeof(vht_capab));
			vht_capab.vht_cap_info = cpu_to_le32(instance->vht_capab_info);
			memcpy((void *)&vht_capab.supp_mcs, (void *)&sband->vht_cap.vht_mcs, sizeof(sband->vht_cap.vht_mcs));



			eid[0] = WLAN_EID_VHT_CAPABILITY;
			eid[1] = sizeof(vht_capab);
			memcpy((void *)&eid[2], (void *)&vht_capab, eid[1]);
  //       al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MD-MAC80211: FLOW : Exit: %s : %d\n", __func__,__LINE__);
			return 0;
		}
	}

   al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MD-MAC80211: ERROR : band %d %s : %d\n", band, __func__,__LINE__);
	return -1;
}



int meshap_build_vht_operation_ie(meshap_instance_t *instance, u8 *eid)
{
	struct ieee80211_vht_operation vht_opera;
//   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MD-MAC80211: FLOW : Enter: %s : %d\n", __func__,__LINE__);

	if (NULL == instance) {
		al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MD-MAC80211:ERROR: %s<%d> INSTANCE is NULL\n",__func__, __LINE__);
		return -1;
	}

	memset((void *)&vht_opera, 0, sizeof(vht_opera));

    vht_opera.chan_width = instance->vht_opera.vht_op_info_chwidth;
    vht_opera.center_freq_seg1_idx = instance->vht_opera.vht_op_info_chan_center_freq_seg0_idx;
    vht_opera.center_freq_seg2_idx = instance->vht_opera.vht_op_info_chan_center_freq_seg1_idx;
    /* VHT Basic MCS set comes from hw */
    /* Hard code 1 stream, MCS0-7 is a min Basic VHT MCS rates */
    vht_opera.basic_mcs_set = cpu_to_le16(0xfffc);
	 
	eid[0] = WLAN_EID_VHT_OPERATION;
	eid[1] = sizeof(vht_opera);
	memcpy((void *)&eid[2], (void *)&vht_opera, eid[1]);
 //  al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MD-MAC80211: FLOW : Exit: %s : %d\n", __func__,__LINE__);

	return 0;
}

int _get_next_ie(_mgmt_frame_ie_t *ie)
{
   if (ie->buffer_length < 2)
   {
      return -1;
   }

   ie->eid    = *ie->buffer++;
   ie->length = *ie->buffer++;
   ie->data   = ie->buffer;

   ie->buffer        += ie->length;
   ie->buffer_length -= (ie->length + 2);

   return 0;
}


int meshap_reset(struct net_device *dev, int phy_mode_changed)
{
   return 0;
}


void meshap_uninit_master_mode(struct meshap_instance *instance)
{
   struct cfg80211_registered_device *rdev = NULL;
   struct net_device                 *dev  = instance->dev;
   struct wireless_dev               *wdev = instance->dev->ieee80211_ptr;
   struct vif_params                 params;
   int err, old_flags = 0, flags = 0;
   struct ieee80211_sub_if_data *sdata = netdev_priv(dev);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MD-MAC80211: FLOW : Enter: %s : %d\n", __func__,__LINE__);

   if (instance->use_type != AL_CONF_IF_USE_TYPE_WM)
   {
      return;
   }

   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"MD-MAC80211: INFO : %s: Un-initializing 802.11 Master mode... %s : %d\n",
                       instance->dev->name, __func__, __LINE__);
   memset(&params, 0, sizeof(params));
   rdev = WIPHY_TO_DEV(wdev->wiphy);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"MD-MAC80211: INFO : Deleting beacons %s : %d\n", __func__, __LINE__);

   ASSERT_WDEV_LOCK(wdev);
   wdev_lock(wdev);
   err = rdev->ops->stop_ap(&rdev->wiphy, dev);
   if (!err)
   {
      wdev->beacon_interval = 0;
   }
   wdev_unlock(wdev);
   /* Change interface back to Infra mode*/

   old_flags = instance->dev->flags;

   old_flags &= ~IFF_UP;

   rtnl_lock();
   dev_change_flags(instance->dev, old_flags);
   err        = cfg80211_change_iface(rdev, instance->dev, NL80211_IFTYPE_STATION, &flags, &params);

   old_flags |= IFF_UP;
   dev_change_flags(instance->dev, old_flags);
   rtnl_unlock();

   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"MD-MAC80211: INFO : %s: Done un-initializing 802.11 Master mode %s : %d\n",
                          instance->dev->name, __func__, __LINE__);

   instance->beacon_vendor_info_length  = 0;
   instance->beacon_vendor_info_updated = 0;

#ifdef MESHAP_NO_POWER_SAVE_MODE
   instance->beacon_vendor_info_offset = NULL;
#endif
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MD-MAC80211: FLOW : Exit: %s : %d\n", __func__,__LINE__);
}


void meshap_uninit_infra_mode(struct meshap_instance *instance)
{
   /**
    * 1) Stop the FSM
    */
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MD-MAC80211: FLOW : Enter: %s : %d\n", __func__,__LINE__);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"MD-MAC80211: INFO : %s: Un-initializing 802.11 Infrastructure mode... %s : %d\n",
                         instance->dev->name, __func__, __LINE__);

   if (instance->connected)
   {
      meshap_sta_fsm_leave(instance, 1, WLAN_REASON_DEAUTH_LEAVING);
   }

   meshap_sta_fsm_stop(instance);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"MD-MAC80211: INFO : %s: Done un-initializing 802.11 Infrastructure mode... %s : %d\n",
                  instance->dev->name, __func__, __LINE__);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MD-MAC80211: FLOW : Exit: %s : %d\n", __func__,__LINE__);
}


void meshap_init_infra_mode(struct meshap_instance *instance)
{
   /**
    * 1) Start the FSM
    * 2) Start the rate control timer
    * 3) Start the calibration timer
    */

   struct cfg80211_registered_device *rdev = NULL;
   struct wireless_dev               *wdev = instance->dev->ieee80211_ptr;
   struct vif_params                 params;
   int flags = 0, old_flags = 0;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MD-MAC80211: FLOW : Enter: %s : %d\n", __func__,__LINE__);

   if (!strcmp(instance->dev->name, "wlan3") || instance->use_type == AL_CONF_IF_USE_TYPE_AP)
   {
      return;
   }
   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"MD-MAC80211: INFO : %s: Initializing 802.11 Infrastructure mode... %s : %d\n",
                instance->dev->name, __func__, __LINE__);
   memset(&params, 0, sizeof(params));

   params.use_4addr = 1;

   rdev       = WIPHY_TO_DEV(wdev->wiphy);
   old_flags  = dev_get_flags(instance->dev);
   old_flags &= ~IFF_UP;

   rtnl_lock();
   dev_change_flags(instance->dev, old_flags);
   cfg80211_change_iface(rdev, instance->dev, NL80211_IFTYPE_STATION, &flags, &params);
   old_flags |= IFF_UP;
   dev_change_flags(instance->dev, old_flags);
   rtnl_unlock();

   meshap_sta_fsm_start(instance);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"MD-MAC80211: INFO : %s: Done initializing 802.11 Infrastructure mode...%s : %d\n",
            instance->dev->name, __func__, __LINE__);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MD-MAC80211: FLOW : Exit: %s : %d\n", __func__,__LINE__);
}


void meshap_init_master_mode(struct meshap_instance *instance)
{
   /**
    * 1) Initialize the beacon buffer
    * 2) Configure the beacon
    * 3) Start the calibration timer
    */
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MD-MAC80211: FLOW : Enter: %s : %d\n", __func__,__LINE__);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"MD-MAC80211: INFO : %s: Initializing 802.11 Master in %s mode... %s : %d\n",
       instance->dev->name, instance->flags & MESHAP_INSTANCE_FLAGS_MASTER_QUIET ? "quiet" : "normal", __func__, __LINE__);

   if (instance->use_type != AL_CONF_IF_USE_TYPE_WM)
   {
      return;
   }
   instance->slot_time_status = MESHAP_INSTANCE_SLOT_TIME_STATUS_OK;

   if (instance->flags & MESHAP_INSTANCE_FLAGS_WEP_ENABLE ||
       instance->flags & MESHAP_INSTANCE_FLAGS_RSN_IE_ENABLE)
   {
      instance->capability |= WLAN_CAPABILITY_PRIVACY;
   }
   else
   {
      instance->capability &= ~WLAN_CAPABILITY_PRIVACY;
   }

   /*As per meshap.conf, if use_type is wm, we shall run Mesh-AP on that interface and therefore need to configure beacons for this interface.
    * For the interface marked with use_type ap, hostapd will take care of beacons.
    */
   if ((instance->use_type == AL_CONF_IF_USE_TYPE_WM) && instance->beacon_conf_complete)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"MD-MAC80211: INFO : USE TYPE = %d intf <%s> %s : %d\n",
                        instance->use_type, instance->dev->name, __func__, __LINE__);
      /* Set AP mode in mac80211*/
      if (meshap_mac80211_set_ap_mode(instance))
      {
         al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MD-MAC80211: ERROR : Failed to set Master mode for interface <%s> %s : %d\n",
                        instance->dev->name, __func__,__LINE__);
         return;
      }

   /*
    * Post Bug-220: Lets make the call here (i.e) post mode
    * to set the WMM(wi-fi multi media) params
    */
       meshap_mac80211_set_tx_params(instance);

      _meshap_init_beacon_buffer(instance);

      /*TODO:Do we need this? */
      instance->max_beacon_tx_pending      = 0;
      instance->max_intra_beacon_interval  = 0;
      instance->beacon_skip_count          = 0;
      instance->last_beacon_interrupt_time = jiffies;
   }

#ifndef MESHAP_NO_POWER_SAVE_MODE
   memset(instance->tim_bitmap, 0, WLAN_TIM_MAX_BITMAP_SIZE);
   _meshap_clear_power_save_count(instance);
#endif

   //MeshapTag Start TBD: Needed now?.
   //ath_hal_setassocid(instance->ah,instance->dev->dev_addr,0);
   //MeshapTag End
   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"MD-MAC80211: INFO : %s: Done initializing 802.11 Master in %s mode... %s : %d\n",
            instance->dev->name, instance->flags & MESHAP_INSTANCE_FLAGS_MASTER_QUIET ? "quiet" : "normal", __func__, __LINE__);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MD-MAC80211: FLOW : Exit: %s : %d\n", __func__,__LINE__);
}


meshap_instance_t *meshap_mac80211_alloc_instance(void)
{
   meshap_instance_t *instance = NULL;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MD-MAC80211: FLOW : Enter: %s : %d\n", __func__,__LINE__);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"MD-MAC80211: INFO : Allocating instance %s : %d\n", __func__, __LINE__);
   instance = (meshap_instance_t *)kmalloc(sizeof(meshap_instance_t), GFP_ATOMIC);
   if (instance == NULL)
   {
		al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MD-MAC80211:ERROR: FAILED to allocate INSTANCE %s : %d \n",__func__, __LINE__);
      return NULL;
   }
   /* Setup default values of instance */
   memset(instance, 0, sizeof(meshap_instance_t));
   meshap_setup_defaults(instance);

   meshap_sta_fsm_initialize(instance);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MD-MAC80211: FLOW : Exit: %s : %d\n", __func__,__LINE__);
   return instance;
}


static int meshap_getchannels(struct net_device *dev, u_int cc)
{
   int                 i, channel, c = 0;
   enum ieee80211_band band;
   struct wireless_dev *wdev     = dev->ieee80211_ptr;
   struct iw_range     *range    = (struct iw_range *)kmalloc(sizeof(struct iw_range), GFP_ATOMIC);
   meshap_instance_t   *instance = NULL;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MD-MAC80211: FLOW : Enter: %s : %d\n", __func__,__LINE__);

   instance = meshap_get_instance(dev);
   if (NULL == instance)
   {
		al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MD-MAC80211:ERROR: Could not find Instance, cannot configure channels %s : %d \n",__func__, __LINE__);
      return 1;
   }

   instance->channel_count = WLAN_MAX_CHANNELS;
   instance->channels      = (wlan_channel_t *)kmalloc(sizeof(wlan_channel_t) * WLAN_MAX_CHANNELS, GFP_ATOMIC);
   memset(instance->channels, 0, sizeof(wlan_channel_t) * WLAN_MAX_CHANNELS);

   instance->flags &= ~MESHAP_INSTANCE_FLAGS_A_SUPPORTED;
   instance->flags &= ~MESHAP_INSTANCE_FLAGS_B_SUPPORTED;
   instance->flags &= ~MESHAP_INSTANCE_FLAGS_G_SUPPORTED;
   instance->flags &= ~MESHAP_INSTANCE_FLAGS_PURE_G_SUPPORTED;
   instance->flags &= ~MESHAP_INSTANCE_FLAGS_N_SUPPORTED;
   instance->flags &= ~MESHAP_INSTANCE_FLAGS_PURE_N_SUPPORTED;
   instance->flags &= ~MESHAP_INSTANCE_FLAGS_AC_SUPPORTED;
   instance->flags &= ~MESHAP_INSTANCE_FLAGS_ANAC_SUPPORTED;
   for (band = 0; band < IEEE80211_NUM_BANDS; band++)
   {
      struct ieee80211_supported_band *sband;

      sband = wdev->wiphy->bands[band];

      if (!sband)
      {
         continue;
      }

      for (i = 0; i < sband->n_channels && c < IW_MAX_FREQUENCIES; i++)
      {
         struct ieee80211_channel *chan = &sband->channels[i];

         if ((chan->flags & IEEE80211_CHAN_DISABLED))
         {
            c++;
            continue;
         }

         range->freq[c].i = ieee80211_frequency_to_channel(chan->center_freq);
         channel          = range->freq[c].i;
         instance->channels[channel].number    = channel;
         instance->channels[channel].frequency = chan->center_freq;
         instance->channels[channel].max_power = chan->max_power;
         if (band == IEEE80211_BAND_5GHZ)
         {
			 instance->channels[channel].flags |= WLAN_CHANNEL_5GHZ | WLAN_CHANNEL_802_11_A
												| WLAN_CHANNEL_802_11_N | WLAN_CHANNEL_802_11_AC;
				 instance->flags |= MESHAP_INSTANCE_FLAGS_A_SUPPORTED
								| MESHAP_INSTANCE_FLAGS_N_SUPPORTED | MESHAP_INSTANCE_FLAGS_PURE_N_SUPPORTED
                                | MESHAP_INSTANCE_FLAGS_AC_SUPPORTED | MESHAP_INSTANCE_FLAGS_ANAC_SUPPORTED;
         }
         else if (band == IEEE80211_BAND_2GHZ)
         {
            instance->channels[channel].flags |= WLAN_CHANNEL_2GHZ | WLAN_CHANNEL_802_11_B | WLAN_CHANNEL_802_11_G 
												| WLAN_CHANNEL_802_11_N;
            instance->flags |= MESHAP_INSTANCE_FLAGS_B_SUPPORTED
                               | MESHAP_INSTANCE_FLAGS_G_SUPPORTED
                               | MESHAP_INSTANCE_FLAGS_PURE_G_SUPPORTED
							| MESHAP_INSTANCE_FLAGS_N_SUPPORTED | MESHAP_INSTANCE_FLAGS_PURE_N_SUPPORTED;
         }
      }
   }

   kfree(range);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MD-MAC80211: FLOW : Exit: %s : %d\n", __func__,__LINE__);
   return 0;
}


void _meshap_init_channels(struct net_device *dev)
{
   int               error;
   unsigned long     flags;
   int               chan_mask;
   int               i;
   meshap_instance_t *instance = NULL;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MD-MAC80211: FLOW : Enter: %s : %d\n", __func__,__LINE__);

   instance = meshap_get_instance(dev);
   if (NULL == instance)
   {
		al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MD-MAC80211:ERROR: Failed to get instance %s : %d \n",__func__, __LINE__);
      return;
   }

   MESHAP_INSTANCE_LOCK(instance, flags);

   error = meshap_getchannels(instance->dev, instance->country_code);

   if (error)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MD-MAC80211: ERROR : Failed to setup channels: %d %s : %d\n", error, __func__,__LINE__);
      goto _bad;
   }

   instance = meshap_get_instance(dev);

   chan_mask = WLAN_CHANNEL_5GHZ;
   if ((!is_11n_supported(instance, 1) || !(instance->flags & MESHAP_INSTANCE_FLAGS_N_SUPPORTED)) && 
	   ((!(instance->flags & MESHAP_INSTANCE_FLAGS_A_SUPPORTED)) || (instance->current_phy_mode != WLAN_PHY_MODE_802_11_A)))
   {
      if (is_11n_supported(instance, 1) || (instance->current_phy_mode == WLAN_PHY_MODE_802_11_A))
      {
         instance->current_phy_mode = WLAN_PHY_MODE_802_11_G;
      }
      chan_mask = WLAN_CHANNEL_2GHZ;
   }

   /**
    * By default we set the channel to the first valid channel
    */

   for (i = 0, instance->current_channel = NULL; i < instance->channel_count; i++)
   {
      if (instance->channels[i].frequency != 0)
      {
         if (instance->channels[i].flags & chan_mask)
         {
            instance->current_channel = &instance->channels[i];
            printk(KERN_INFO "%s: Setting channel %d (%d MHz) as default\n", instance->dev->name, i, instance->channels[i].frequency);
            break;
         }
      }
   }

_bad:

   MESHAP_INSTANCE_UNLOCK(instance, flags);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"MD-MAC80211: INFO : Returning from _meshap_init_channels %s : %d\n", __func__, __LINE__);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MD-MAC80211: FLOW : Exit: %s : %d\n", __func__,__LINE__);
}


void _meshap_set_use_type(struct net_device *dev, int use_type)
{
   meshap_instance_t *instance;

   instance           = meshap_get_instance(dev);
   if (NULL == instance)
   {
		al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MD-MAC80211:ERROR: Failed to get instance %s<%d> \n",__func__, __LINE__);
      return;
   }
   instance->use_type = use_type;
}


void _meshap_set_all_conf_complete(struct net_device *dev, int value)
{
   meshap_instance_t *instance;

   instance = meshap_get_instance(dev);
   if (NULL == instance)
   {
		al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MD-MAC80211:ERROR:  Failed to get instance %s<%d> \n",__func__, __LINE__);
      return;
   }
   instance->beacon_conf_complete = value;

   if (instance->iw_mode == IW_MODE_MASTER)
   {
      meshap_init_master_mode(instance);
   }
}


int meshap_mac80211_tx_mgmt_packet(meshap_instance_t *instance,
                                   struct ieee80211_mgmt *mgmt, int data_len)
{
   u32                               freq;
   u64                               cookie;
   struct net_device                 *dev  = instance->dev;
   struct wireless_dev               *wdev = dev->ieee80211_ptr;
   struct cfg80211_registered_device *rdev;
   struct ieee80211_channel          *chan;
   enum nl80211_channel_type         channel_type       = NL80211_CHAN_NO_HT;
   int                               ret                = 0;
   unsigned int                      wait               = 0;
   bool                              channel_type_valid = false;
   bool                              offchan            = MESHAP_FALSE;
   struct cfg80211_mgmt_tx_params params;
//   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MD-MAC80211: FLOW : Enter: %s : %d\n", __func__,__LINE__);

   if (!wdev)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MD-MAC80211: ERROR : WDEV is NULL %s : %d\n", __func__,__LINE__);
      (tx_rx_pkt_stats.packet_drop[119])++;
      return MESHAP_FAIL;
   }
   rdev = WIPHY_TO_DEV(wdev->wiphy);

   freq = instance->current_channel->frequency;

   chan = ieee80211_get_channel(wdev->wiphy, freq);

   if (!chan || chan->flags & IEEE80211_CHAN_DISABLED)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MD-MAC80211: ERROR : chan %p chan->flags %d %s : %d\n", chan, chan->flags, __func__,__LINE__);
      (tx_rx_pkt_stats.packet_drop[120])++;
      return MESHAP_FAIL;
   }
   /*update channel_type based on secondary channel offset*/
	if(instance->sec_chan_offset == 1) {
		channel_type = NL80211_CHAN_HT40PLUS;
	}else if(instance->sec_chan_offset == -1) {
		channel_type = NL80211_CHAN_HT40MINUS;
	}

   /* Removing parameter channel_type and channel_type_valid and initializing 
    * other parameters to params and passing wdev instead of dev
    */
	memset(&params, 0, sizeof(params));
	params.chan = chan;
	params.offchan = offchan;
	params.wait = wait;
	params.buf = (u8*)mgmt;
	params.len = data_len;
    ret = cfg80211_mlme_mgmt_tx(rdev, wdev, &params, &cookie);
	if(ret < 0) {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MD-MAC80211: ERROR : cfg80211_mlme_mgmt_tx ret val %d %s : %d\n", ret, __func__,__LINE__);
      (tx_rx_pkt_stats.packet_drop[121])++;
		return MESHAP_FAIL;
	}

 //  al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MD-MAC80211: FLOW : Exit: %s : %d\n", __func__,__LINE__);
   return 0;
}


void meshap_setup_auth_response_pkt(u8 *address0, u8 *address1, struct ieee80211_mgmt **mgmt_pkt, int *data_len)
{
   struct ieee80211_mgmt      mgmt = { '\0' };
   static const unsigned char _auth_resp_data[] =
   {
      0x00, 0x00, 0x02, 0x00, 0x00, 0x00
   };
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MD-MAC80211: FLOW : Enter: %s : %d\n", __func__,__LINE__);

   WLAN_FC_SET_TYPE(mgmt.frame_control, WLAN_FC_TYPE_MGMT);
   WLAN_FC_SET_STYPE(mgmt.frame_control, AL_802_11_FC_STYPE_AUTH);

   mgmt.frame_control = cpu_to_le16(mgmt.frame_control);

   memcpy(mgmt.da, address1, ETH_ALEN);
   memcpy(mgmt.sa, address0, ETH_ALEN);
   memcpy(mgmt.bssid, address0, ETH_ALEN);

   memcpy(&mgmt.u.auth, _auth_resp_data, sizeof(mgmt.u.auth));

   *mgmt_pkt = (struct ieee80211_mgmt *)kmalloc(sizeof(struct ieee80211_mgmt), GFP_ATOMIC);
   memcpy(*mgmt_pkt, &mgmt, sizeof(struct ieee80211_mgmt));

   *data_len = 0;    //No variable size data
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MD-MAC80211: FLOW : Exit: %s : %d\n", __func__,__LINE__);
}


void meshap_setup_mgmt_deauth_pkt(u8 *address0, u8 *address1, struct ieee80211_mgmt **mgmt_pkt, int *data_len)
{
   struct ieee80211_mgmt mgmt = { '\0' };
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MD-MAC80211: FLOW : Enter: %s : %d\n", __func__,__LINE__);

   printk(KERN_EMERG "DEBUG:<%s><%d>\n", __func__, __LINE__);
   WLAN_FC_SET_TYPE(mgmt.frame_control, WLAN_FC_TYPE_MGMT);
   WLAN_FC_SET_STYPE(mgmt.frame_control, AL_802_11_FC_STYPE_DEAUTH);

   mgmt.frame_control = cpu_to_le16(mgmt.frame_control);

   memcpy(mgmt.da, address1, ETH_ALEN);
   memcpy(mgmt.sa, address0, ETH_ALEN);
   memcpy(mgmt.bssid, address0, ETH_ALEN);
   mgmt.u.deauth.reason_code = cpu_to_le16(AL_802_11_REASON_PREV_AUTH_NOT_VALID);

   *mgmt_pkt = (struct ieee80211_mgmt *)kmalloc(sizeof(struct ieee80211_mgmt), GFP_ATOMIC);
   memcpy(*mgmt_pkt, &mgmt, sizeof(struct ieee80211_mgmt));

   *data_len = 0;        //No variable size data
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MD-MAC80211: FLOW : Exit: %s : %d\n", __func__,__LINE__);
}


static inline int _check_essid(meshap_instance_t *instance, _mgmt_frame_ie_t *ie)
{
   int i;

   for (i = 0; i < instance->essid_info_count; i++)
   {
      if ((ie->length == instance->essid_info[i].essid_length) &&
          !memcmp(ie->data, instance->essid_info[i].essid, ie->length))
      {
         return i;
      }
   }

   return -1;
}


int meshap_mac80211_setup_mgmt_probe_req(meshap_instance_t *instance, struct ieee80211_mgmt **mgmt_pkt, int *data_len)
{
   char *base;
   char p[128];
   int  mgmt_len = 0, len = 0;
   struct ieee80211_mgmt mgmt;
   char bcast_addr[6] = { 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF };
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MD-MAC80211: FLOW : Enter: %s : %d\n", __func__,__LINE__);

   base = &p[0];
   memset(base, 0, 128);
   memset(&mgmt, 0, sizeof(struct ieee80211_mgmt));

   WLAN_FC_SET_TYPE(mgmt.frame_control, WLAN_FC_TYPE_MGMT);
   WLAN_FC_SET_STYPE(mgmt.frame_control, WLAN_FC_STYPE_PROBE_REQ);
   mgmt.frame_control = cpu_to_le16(mgmt.frame_control);

   memcpy(mgmt.da, bcast_addr, ETH_ALEN);
   memcpy(mgmt.sa, instance->dev->dev_addr, ETH_ALEN);
   memcpy(mgmt.bssid, bcast_addr, ETH_ALEN);

   base[0]   = WLAN_EID_SSID;
   base[1]   = 0;   //Blank SSID
   base     += len + 2;
   mgmt_len += len + 2;

   /** Supported rates */
   base[0] = WLAN_EID_SUPP_RATES;
   base[1] = instance->supported_rates_length;
   memcpy(&base[2], instance->supported_rates, instance->supported_rates_length);
   base     += instance->supported_rates_length + 2;
   mgmt_len += instance->supported_rates_length + 2;

   /*Check Is 80211n supported If yes add appropriate IE's In packets*/
   if(is_11n_supported(instance, 0)) {
       /*HT capability IE*/
       if (!meshap_build_ht_capability_ie(instance, base)){
           mgmt_len += base[1] +2;
           base       += base[1] + 2;
       }
   }
   if(is_11ac_supported(instance)) {
       /*HT capability IE*/
       if (!meshap_build_vht_capability_ie(instance, base)){
           mgmt_len += base[1] +2;
           base       += base[1] + 2;
       }
   }
   /*WMM/WME IE*/
   if (!meshap_build_wmm_ie(instance, base)) {
       mgmt_len += base[1] +2;
       base       += base[1] + 2;
   }

   *mgmt_pkt = (struct ieee80211_mgmt *)kmalloc(sizeof(struct ieee80211_mgmt) + mgmt_len, GFP_ATOMIC);
   memcpy(*mgmt_pkt, &mgmt, sizeof(struct ieee80211_mgmt));

   memcpy(&((*mgmt_pkt)->u.probe_req.variable[0]), base - mgmt_len - 1, mgmt_len); //Need to verify this packet.
   *data_len = mgmt_len;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MD-MAC80211: FLOW : Exit: %s : %d\n", __func__,__LINE__);
   return 0;
}


int meshap_setup_mgmt_probe_response_pkt(struct sk_buff *skb, meshap_instance_t *instance, struct ieee80211_mgmt **mgmt_pkt, int *data_len)
{
   torna_mac_hdr_t       *hdr;
   unsigned char         *p;
   char                  q[256];
   char                  *base;
   char                  *essid;
   int                   mgmt_len = 0;
   struct ieee80211_mgmt mgmt;
   _mgmt_frame_ie_t      ie;
   unsigned char         *rsn_ie_buffer;
   int                   rsn_ie_length;
   char                  *current_essid;
   int                   current_essid_length;
   int                   ie_match_index, len = 0;
//   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MD-MAC80211: FLOW : Enter: %s : %d\n", __func__,__LINE__);

   base = &q[0];
   memset(base, 0, 256);

   memset(&mgmt, 0, sizeof(struct ieee80211_mgmt));

   WLAN_FC_SET_TYPE(mgmt.frame_control, WLAN_FC_TYPE_MGMT);
   WLAN_FC_SET_STYPE(mgmt.frame_control, WLAN_FC_STYPE_PROBE_RESP);

   mgmt.frame_control = cpu_to_le16(mgmt.frame_control);
   hdr = _meshap_get_torna_hdr(skb);

   memcpy(mgmt.da, hdr->addresses[1], ETH_ALEN);
   memcpy(mgmt.sa, instance->dev->dev_addr, ETH_ALEN);
   memcpy(mgmt.bssid, instance->dev->dev_addr, ETH_ALEN);
   mgmt.u.probe_resp.timestamp  = jiffies;
   mgmt.u.probe_resp.beacon_int = cpu_to_le16(instance->beacon_interval);

   p                = skb->data;
   ie.buffer        = p;
   ie.buffer_length = skb->len;

   /**
    * Respond back with a probe response under the following
    * circumstances:
    *  1) If the given SSID is blank
    *  2) If the given SSID matches our SSID
    */

   while (_get_next_ie(&ie) != -1)
   {
      if (ie.eid == WLAN_EID_SSID)
      {
         /*
          *  check essid with current essid and with list of essid_info
          */

         essid         = NULL;
         rsn_ie_buffer = NULL;
         rsn_ie_length = 0;

         current_essid        = instance->current_essid;
         current_essid_length = instance->current_essid_length;
         if (((ie.length == 0) && instance->hide_essid) ||
             ((ie.length == current_essid_length) && !memcmp(ie.data, current_essid, ie.length)))
         {
            essid = current_essid;
            if (instance->flags & MESHAP_INSTANCE_FLAGS_RSN_IE_ENABLE)
            {
               rsn_ie_buffer = instance->rsn_ie_buffer;
               rsn_ie_length = instance->rsn_ie_length;
            }
         }
         else
         {
            ie_match_index = _check_essid(instance, &ie);
            if (ie_match_index >= 0)
            {
               essid = instance->essid_info[ie_match_index].essid;
               if (instance->essid_info[ie_match_index].sec_info.flags & MESHAP_ESSID_SECURITY_INFO_RSN_ENABLED)
               {
                  rsn_ie_buffer = instance->essid_info[ie_match_index].sec_info.rsn_ie_buffer;
                  rsn_ie_length = instance->essid_info[ie_match_index].sec_info.rsn_ie_length;
               }
            }
            else
            {
//               al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MD-MAC80211: ERROR : ie_match_index %d %s : %d\n", ie_match_index, __func__,__LINE__);
               return -1;
            }
         }

         if (strcmp(current_essid, essid))
         {
            if ((rsn_ie_length > 0) && (rsn_ie_buffer != NULL))
            {
               instance->capability |= WLAN_CAPABILITY_PRIVACY;
            }
            else
            {
               instance->capability &= ~WLAN_CAPABILITY_PRIVACY;
            }
         }

         mgmt.u.probe_resp.capab_info = cpu_to_le16(instance->capability);

		 if(!instance->hide_essid) {
			 base[0] = WLAN_EID_SSID;
			 len     = strlen(essid);
			 base[1] = len;
			 memcpy(&base[2], essid, len);
		 }else {
			 base[0] = WLAN_EID_SSID;
			 base[1] = 0;
		 }
         base     += len + 2;
         mgmt_len += len + 2;

         /** Supported rates */
         base[0] = WLAN_EID_SUPP_RATES;
         base[1] = instance->supported_rates_length;
         memcpy(&base[2], instance->supported_rates, instance->supported_rates_length);
         base     += instance->supported_rates_length + 2;
         mgmt_len += instance->supported_rates_length + 2;

         /** DS Parameter set */
         base[0]   = WLAN_EID_DS_PARAMS;
         base[1]   = 1;
         base[2]   = instance->current_channel->number;
         base     += 1 + 2;
         mgmt_len += 1 + 2;
         /*Check Is 80211n supported If yes add appropriate IE's In packets*/
         if(is_11n_supported(instance, 0)) {
             /*HT capability IE*/
             if (!meshap_build_ht_capability_ie(instance, base)){
                 mgmt_len += base[1] +2;
                 base       += base[1] + 2;
             }

             /*HT Operation IE*/
             if (!meshap_build_ht_operation_ie(instance, base)){
                 mgmt_len += base[1] +2;
                 base       += base[1] + 2;
             }
         }
         if(is_11ac_supported(instance)) {
             /*HT capability IE*/
             if (!meshap_build_vht_capability_ie(instance, base)){
                 mgmt_len += base[1] +2;
                 base       += base[1] + 2;
             }

             /*HT Operation IE*/
             if (!meshap_build_vht_operation_ie(instance, base)){
                 mgmt_len += base[1] +2;
                 base       += base[1] + 2;
             }
         }

         /* WMM/WME IE */
         if (!meshap_build_wmm_ie(instance, base)) {
             mgmt_len += base[1] +2;
             base       += base[1] + 2;
         }

		 /** set RSN IE **/
		 if ((rsn_ie_length > 0) && (rsn_ie_buffer != NULL))
		 {
			 memcpy(base, rsn_ie_buffer, rsn_ie_length);
			 base     += rsn_ie_length;
			 mgmt_len += rsn_ie_length;
		 }
      }
   }

   *mgmt_pkt = (struct ieee80211_mgmt *)kmalloc(sizeof(struct ieee80211_mgmt) + mgmt_len, GFP_ATOMIC);
   memcpy(*mgmt_pkt, &mgmt, sizeof(struct ieee80211_mgmt));

   memcpy(&((*mgmt_pkt)->u.probe_resp.variable[0]), base - mgmt_len, mgmt_len);  //need to verify probe response trailer.

   //NOT NEEDED: as error in len is rectified
   //mgmt_len += 6; /*FIXME: Without this 6 bytes the Probe resp is seen malformed at Wireshark with 3 bytes on SUPP_RATES missing and no DS params */

   *data_len = mgmt_len;

   /**
    * Inform meshap about the probe request
    */

   if (instance->probe_request_hook != NULL)
   {
      meshap_probe_request_notify_t hook = (meshap_probe_request_notify_t)instance->probe_request_hook;
      if (hook != NULL)
      {
         hook(instance->dev, instance->dev_token, hdr->addresses[1], hdr->rssi);
      }
   }
//   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MD-MAC80211: FLOW : Exit: %s : %d\n", __func__,__LINE__);
   return 0;
}


void meshap_setup_mgmt_assoc_rsp_pkt(meshap_instance_t *instance, unsigned short stype, char *address0, char *address1, unsigned short aid,
                                     unsigned char ie_out_valid, al_802_11_information_element_t *ie_out, struct ieee80211_mgmt **mgmt_pkt,
                                     int *data_len, struct meshap_mac80211_sta_info *sta)
{
   unsigned char         *p, *base;
   int                   mgmt_len = 0;
   struct ieee80211_mgmt mgmt;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MD-MAC80211: FLOW : Enter: %s : %d\n", __func__,__LINE__);

   p = (unsigned char *)kmalloc(EID_MAX_LENGTH, GFP_ATOMIC);
   memset(p, 0, EID_MAX_LENGTH);
   memset(&mgmt, 0, sizeof(struct ieee80211_mgmt));

   WLAN_FC_SET_TYPE(mgmt.frame_control, WLAN_FC_TYPE_MGMT);
   WLAN_FC_SET_STYPE(mgmt.frame_control, stype);

   mgmt.frame_control = cpu_to_le16(mgmt.frame_control);

   memcpy(mgmt.da, address1, ETH_ALEN);
   memcpy(mgmt.sa, address0, ETH_ALEN);
   memcpy(mgmt.bssid, address0, ETH_ALEN);

   mgmt.u.assoc_resp.capab_info  = cpu_to_le16(instance->capability);
   mgmt.u.assoc_resp.status_code = 0;      //TBD ?
   mgmt.u.assoc_resp.aid         = cpu_to_le16(aid | 0xC000);

   base = p;
   p[0] = AL_802_11_EID_SUPP_RATES;
   p[1] = instance->supported_rates_length;

   memcpy(&p[2], instance->supported_rates, instance->supported_rates_length);
   mgmt_len += instance->supported_rates_length + 2;
   p        += instance->supported_rates_length + 2;

   if (instance->extended_rates_length > 0)
   {
      p[0] = AL_802_11_EID_EX_SUPP_RATES;
      p[1] = instance->extended_rates_length;
      memcpy(&p[2], instance->extended_rates, instance->extended_rates_length);
      mgmt_len += instance->extended_rates_length + 2;
      p        += instance->extended_rates_length + 2;
   }

     /* WMM/WME IE */
   if (sta->wmm_flag_set) {
      if (!meshap_build_wmm_ie(instance, p)) {
          mgmt_len += p[1] +2;
          p       += p[1] + 2;
      }
   }

   /*Check Is 80211n supported If yes add appropriate IE's In packets*/
   if(is_11n_supported(instance, 0) && sta->ht_flag_set) {
       /*HT capability IE*/
       if (!meshap_build_ht_capability_ie(instance, p)){
           mgmt_len += p[1] +2;
           p       += p[1] + 2;
       }
       /*HT Operation IE*/
       if (!meshap_build_ht_operation_ie(instance, p)){
           mgmt_len += p[1] +2;
           p       += p[1] + 2;
       }
   }
   if(is_11ac_supported(instance) && sta->vht_flag_set) {
       /*VHT capability IE*/
       if (!meshap_build_vht_capability_ie(instance, p)){
           mgmt_len += p[1] +2;
           p       += p[1] + 2;
       }
       /*VHT Operation IE*/
       if (!meshap_build_vht_operation_ie(instance, p)){
           mgmt_len += p[1] +2;
           p       += p[1] + 2;
       }
   }


   if (ie_out_valid)
   {
      p[0] = AL_802_11_EID_VENDOR_PRIVATE;
      p[1] = ie_out->length;
      memcpy(&p[2], ie_out->data, ie_out->length);
      mgmt_len += ie_out->length + 2;
      p        += ie_out->length + 2;
   }

   *mgmt_pkt = (struct ieee80211_mgmt *)kmalloc(sizeof(struct ieee80211_mgmt) + mgmt_len, GFP_ATOMIC);
   memcpy(*mgmt_pkt, &mgmt, sizeof(struct ieee80211_mgmt));

   memcpy(&((*mgmt_pkt)->u.assoc_resp.variable[0]), base, mgmt_len);

   *data_len = mgmt_len;
   kfree(base);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MD-MAC80211: FLOW : Exit: %s : %d\n", __func__,__LINE__);
}


void _meshap_send_deauth(struct net_device *dev, u8 *address0, u8 *address1)
{
   meshap_instance_t     *instance;
   struct ieee80211_mgmt *mgmt;
   int data_len = 0;
   struct wireless_dev               *wdev;
   struct cfg80211_registered_device *rdev;

#ifdef x86_ONLY
   struct station_del_parameters params = {
		.mac = address1,
		.subtype = 12,
		.reason_code = 0
	};
#endif
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MD-MAC80211: FLOW : Enter: %s : %d\n", __func__,__LINE__);

   instance = meshap_get_instance(dev);
   if (NULL == instance)
   {
		al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MD-MAC80211:ERROR: Failed to get instance %s<%d> \n",__func__, __LINE__);
      return;
   }
   if (instance->use_type == AL_CONF_IF_USE_TYPE_AP)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MAC80211 : INFO : For AP interface hostapd will handle this %s: %d\n", __func__, __LINE__);
      return;
   }
   meshap_setup_mgmt_deauth_pkt(address0, address1, &mgmt, &data_len);

   data_len += sizeof(struct ieee80211_hdr);
   meshap_mac80211_tx_mgmt_packet(instance, mgmt, data_len);

   wdev = instance->dev->ieee80211_ptr;

   rdev = WIPHY_TO_DEV(wdev->wiphy);
   if (!rdev)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MD-MAC80211: ERROR : redev is NULL %s : %d\n", __func__,__LINE__);
      return;
   }
   /** Delete the station from mac80211*/

   if (rdev->ops->del_station)
   {
#ifdef x86_ONLY
      rdev->ops->del_station(&rdev->wiphy, dev, &params);
#else
	  rdev->ops->del_station(&rdev->wiphy, dev, address1);
#endif 
   }

   kfree(mgmt);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MD-MAC80211: FLOW : Exit: %s : %d\n", __func__,__LINE__);
}


int _meshap_mac80211_send_dissassoc_from_station(struct net_device *dev, int flag)
{
   meshap_instance_t            *instance;
   struct ieee80211_sub_if_data *sdata = IEEE80211_DEV_TO_SUB_IF(dev);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MD-MAC80211: FLOW : Enter: %s : %d\n", __func__,__LINE__);

   instance = meshap_get_instance(dev);
   if (NULL == instance)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MD-MAC80211: ERROR : Failed to get instance %s : %d\n", __func__,__LINE__);
      return -1;
   }

   if (!instance->connected)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"MD-MAC80211: INFO : instance not connected %s %s : %d\n", dev->name, __func__,__LINE__);
      return -1;
   }
   meshap_sta_fsm_reset(instance);

   __ieee80211_disconnect(sdata);

   msleep(1);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MD-MAC80211: FLOW : Exit: %s : %d\n", __func__,__LINE__);
   return 0;
}


void _meshap_send_auth_response(struct net_device *dev, u8 *address0, u8 *address1)
{
   meshap_instance_t     *instance;
   struct ieee80211_mgmt *mgmt;
   int data_len = 0;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MD-MAC80211: FLOW : Enter: %s : %d\n", __func__,__LINE__);

   instance = meshap_get_instance(dev);
   if (NULL == instance)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MD-MAC80211: ERROR : Failed to get instance %s : %d\n", __func__,__LINE__);
      (tx_rx_pkt_stats.packet_drop[118])++;
      return;
   }
   if (instance->use_type == AL_CONF_IF_USE_TYPE_AP)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MAC80211 : INFO : For AP interface hostapd will handle this %s: %d\n", __func__, __LINE__);
      return;
   }

   meshap_setup_auth_response_pkt(address0, address1, &mgmt, &data_len);

   data_len += sizeof(struct ieee80211_hdr);
   meshap_mac80211_tx_mgmt_packet(instance, mgmt, data_len);
   kfree(mgmt);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MD-MAC80211: FLOW : Exit: %s : %d\n", __func__,__LINE__);
}


void _send_assoc_response(meshap_instance_t *instance, unsigned short stype, u8 *address0, u8 *address1, unsigned short aid,
                          unsigned char ie_out_valid, al_802_11_information_element_t *ie_out, struct meshap_mac80211_sta_info sta)
{
   struct ieee80211_mgmt             *mgmt;
   struct cfg80211_registered_device *rdev;
   struct station_parameters         params;
   struct wireless_dev               *wdev = instance->dev->ieee80211_ptr;
   int err = 0, data_len = 0, pkt_len = 0, ret = 0;
   static unsigned char supported_rates[8] =
   {
      0x8c, 0x12, 0x98, 0x24, 0xb0, 0x48, 0x60, 0x6c
   };
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MD-MAC80211: FLOW : Enter: %s : %d\n", __func__,__LINE__);

   rdev = WIPHY_TO_DEV(wdev->wiphy);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"MD-MAC80211: INFO : ifname %s %s : %d\n",  instance->dev->name, __func__, __LINE__);
   meshap_setup_mgmt_assoc_rsp_pkt(instance, stype, address0, address1, aid, ie_out_valid, ie_out, &mgmt, &data_len, &sta);
   pkt_len = data_len + sizeof(struct ieee80211_hdr);

   ret = meshap_mac80211_tx_mgmt_packet(instance, mgmt, pkt_len);

   if (ret != MESHAP_FAIL)
   {
      memset(&params, 0, sizeof(struct station_parameters));
      params.supported_rates = (u8 *)kmalloc(AL_802_11_SUPPORTED_RATES_LENGTH, GFP_ATOMIC);
      memset(params.supported_rates, 0, AL_802_11_SUPPORTED_RATES_LENGTH);
      memcpy(params.supported_rates, supported_rates, AL_802_11_SUPPORTED_RATES_LENGTH);
      params.listen_interval     = sta.listen_interval;
      params.aid                 = sta.aid;
      params.supported_rates_len = AL_802_11_SUPPORTED_RATES_LENGTH;
      if(is_11n_supported(instance, 0)) {
          meshap_set_sta_ht_capab(instance,&sta.ht_capab);
          if(sta.ht_capab.ht_capabilities_info){
              params.ht_capa = &sta.ht_capab;
          }
          else{
              params.ht_capa = NULL;
          }
      }
      if(is_11ac_supported(instance)) {
          meshap_set_sta_vht_capab(instance,&sta.vht_capab);
          if(sta.vht_capab.vht_capabilities_info){
              params.vht_capa = &sta.vht_capab;
          }else {
              params.vht_capa = NULL;
          }
      }
     al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"MD-MAC80211: INFO : ifname %s Sending Assoc response to station = %2X:%2X:%2X:%2X:%2X:%2X: %s : %d\n",
              instance->dev->name, sta.sta_addr[0], sta.sta_addr[1], sta.sta_addr[2], sta.sta_addr[3], sta.sta_addr[4], sta.sta_addr[5],  __func__, __LINE__);

	  params.sta_flags_set  |= (BIT(NL80211_STA_FLAG_AUTHORIZED) | BIT(NL80211_STA_FLAG_AUTHENTICATED) | BIT(NL80211_STA_FLAG_ASSOCIATED));
	  params.sta_flags_mask |= (BIT(NL80211_STA_FLAG_AUTHORIZED) | BIT(NL80211_STA_FLAG_AUTHENTICATED) | BIT(NL80211_STA_FLAG_ASSOCIATED));
      if (instance->capability & WLAN_CAPABILITY_SHORT_PREAMBLE)
      {
         params.sta_flags_set  |= BIT(NL80211_STA_FLAG_SHORT_PREAMBLE);
         params.sta_flags_mask |= BIT(NL80211_STA_FLAG_SHORT_PREAMBLE);
      }

      if(sta.wmm_flag_set)
      {
          params.sta_flags_set  |= BIT(NL80211_STA_FLAG_WME);
          params.sta_flags_mask |= BIT(NL80211_STA_FLAG_WME);
      }
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"MD-MAC80211: INFO : Adding station %2X:%2X:%2X:%2X:%2X:%2X set=%x mask=%x  %s : %d\n",
             sta.sta_addr[0], sta.sta_addr[1], sta.sta_addr[2], sta.sta_addr[3], sta.sta_addr[4], sta.sta_addr[5],
             params.sta_flags_set, params.sta_flags_mask,  __func__, __LINE__);
      if ((err = rdev->ops->add_station(&rdev->wiphy, instance->dev, sta.sta_addr, &params)))
      {
         _meshap_del_station(instance->dev, sta.sta_addr);
         rdev->ops->add_station(&rdev->wiphy, instance->dev, sta.sta_addr, &params);
      }
   }
   kfree(mgmt);
   kfree(params.supported_rates);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MD-MAC80211: FLOW : Exit: %s : %d\n", __func__,__LINE__);
}


void _meshap_del_station(struct net_device *dev, u8 *station_addr)
{
   meshap_instance_t                 *instance = meshap_get_instance(dev);
   struct wireless_dev               *wdev     = instance->dev->ieee80211_ptr;
   struct cfg80211_registered_device *rdev;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MD-MAC80211: FLOW : Enter: %s : %d\n", __func__,__LINE__);

   if (NULL == instance)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MD-MAC80211: ERROR :  Failed to get instance %s : %d\n", __func__,__LINE__);
      return;
   }
   if (instance->use_type == AL_CONF_IF_USE_TYPE_AP)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MAC80211 : INFO : For AP interface hostapd will handle this %s: %d\n", __func__, __LINE__);
      return 0;
   }
#ifdef x86_ONLY
   struct station_del_parameters params = {
	   .mac = station_addr,
	   .subtype = 12,
	   .reason_code = 0
   };
#endif

   rdev = WIPHY_TO_DEV(wdev->wiphy);
   if (!rdev)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MD-MAC80211: ERROR : WDEV is NULL %s : %d\n", __func__,__LINE__);
      return;
   }

   /** Delete the station from mac80211*/
   if (rdev->ops->del_station)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"MD-MAC80211: INFO : Deleting station from mac80211 %s : %d\n", __func__, __LINE__);
#ifdef x86_ONLY
      rdev->ops->del_station(&rdev->wiphy, dev, &params);
#else
      rdev->ops->del_station(&rdev->wiphy, dev, station_addr);
#endif

   }
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MD-MAC80211: FLOW : Exit: %s : %d\n", __func__,__LINE__);
}


void _meshap_send_assoc_response(struct net_device *dev, unsigned short stype, u8 *address0, u8 *address1, unsigned short aid,
                                 unsigned char ie_out_valid, al_802_11_information_element_t *ie_out, struct meshap_mac80211_sta_info sta)
{
   meshap_instance_t *instance;

   instance = meshap_get_instance(dev);
   if (NULL == instance)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MD-MAC80211: ERROR : Failed to get instance %s : %d\n", __func__,__LINE__);
      (tx_rx_pkt_stats.packet_drop[122])++;
      return;
   }
   if (instance->use_type == AL_CONF_IF_USE_TYPE_AP)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MAC80211 : INFO : For AP interface hostapd will handle this %s: %d\n", __func__, __LINE__);
      return;
   }

   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"MD-MAC80211: INFO : sending Assoc Response ifname %s %s : %d\n", dev->name, __func__, __LINE__);
   _send_assoc_response(instance, stype, address0, address1, aid,
                        ie_out_valid, ie_out, sta);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MD-MAC80211: FLOW : Exit: %s : %d\n", __func__,__LINE__);
}


int _meshap_send_disassoc_from_station(struct net_device *dev, int flags)
{
   meshap_instance_t            *instance;
   struct ieee80211_sub_if_data *sdata = IEEE80211_DEV_TO_SUB_IF(dev);

   instance = meshap_get_instance(dev);

   if (!instance->connected)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MD-MAC80211: ERROR : instance not connected %s : %d\n", __func__,__LINE__);
      return -1;
   }

   meshap_sta_fsm_reset(instance);
   if (instance->use_type == AL_CONF_IF_USE_TYPE_AP)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MAC80211 : INFO : For AP interface hostapd will handle this %s: %d\n", __func__, __LINE__);
      return -1;
   }

   __ieee80211_disconnect(sdata);

   return 0;
}


meshap_instance_t *meshap_get_instance(struct net_device *dev)
{
	struct wireless_dev *wdev;
	struct ieee80211_hw *hw;
	if (NULL == dev)
	{
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MD-MAC80211: ERROR : dev is NULL %s : %d\n", __func__,__LINE__);
		return NULL;
	}
	wdev = dev->ieee80211_ptr;
	if (NULL == wdev)
	{
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MD-MAC80211: ERROR : wdev is NULL %s : %d\n", __func__,__LINE__);
		return NULL;
	}
	hw = wiphy_to_ieee80211_hw(wdev->wiphy);
	if (NULL == hw)
	{
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MD-MAC80211: ERROR : hw is NULL %s : %d\n", __func__,__LINE__);
		return NULL;
	}
	meshap_instance_t *p_instance = hw->instance, *instance;
	instance = p_instance;
	if (NULL == instance)
	{
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MD-MAC80211: ERROR : instance is NULL %s : %d\n", __func__,__LINE__);
		return instance;
	}
	if(!instance->dev)
		return instance;
	if(!strcmp(instance->dev->name, dev->name)) {
		return instance;
	}else {
		while(instance->next) {
			instance = instance->next;
			if(!strcmp(instance->dev->name, dev->name))
				return instance;
		}
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"MD-MAC80211: INFO : Instance for:%s Not found returning parent instance %s : %d\n",
                     dev->name, __func__, __LINE__);
		return p_instance;
	}
}


meshap_instance_t *meshap_add_instance(struct net_device *phy_instance, struct net_device *vif_instance)
{
   struct wireless_dev *wdev = phy_instance->ieee80211_ptr;
   meshap_instance_t   *instance;
   struct ieee80211_hw *hw = wiphy_to_ieee80211_hw(wdev->wiphy);

   instance = hw->instance;
   while (instance->next != NULL)
   {
      instance = instance->next;
   }
   instance->next       = meshap_mac80211_alloc_instance();
   instance->next->dev  = vif_instance;
   instance->next->next = NULL;

   return instance;
}


/* mgmt_work_queue is used to queue the management frames */
struct sk_buff_head mgmt_work_queue;

void meshap_setup_defaults(meshap_instance_t *instance)
{
   static char mgmt_work_queue_init = 0;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MD-MAC80211: FLOW : Enter: %s : %d\n", __func__,__LINE__);

   instance->dot11e_category_config = NULL;
   instance->country_code           = 840;
   instance->xchanmode        = AH_TRUE;
   instance->regdomain        = 0;
   instance->current_phy_mode = WLAN_PHY_MODE_802_11_A;
   /** Vivek: Change to set the defalut mode for the interface as AUTO. THis is because, if the default is INFRA,
     * and we want to change it again to infra then in the mesh code no processing happens for that mode
     */
   instance->iw_mode = IW_MODE_AUTO;
   instance->calibration_interval = MESHAP_CALIBRATION_INTERVAL_SHORT;                 /** 30 seconds */
   instance->fsm_state            = MESHAP_STA_FSM_STATE_STOPPED;
   instance->default_capability = WLAN_CAPABILITY_ESS;
   instance->capability         = WLAN_CAPABILITY_ESS;
   instance->slot_time_status   = MESHAP_INSTANCE_SLOT_TIME_STATUS_OK;
   instance->beacon_timestamp               = 0;
   instance->beacon_interval                = 100;
   instance->connected                      = 0;
   instance->rts_threshold                  = 2432;
   instance->frag_threshold                 = 2342;
   instance->tx_power                       = 100;
   instance->supported_rates                = _meshap_802_11_A_rates;
   instance->supported_rates_length         = sizeof(_meshap_802_11_A_rates);
   instance->extended_rates                 = NULL;
   instance->extended_rates_length          = 0;
   instance->distance_in_meters             = 0;
   instance->ds_verification_enabled        = 0;
   instance->bmiss_count                    = 0;
   instance->deauth_count                   = 0;
   instance->rate_ctrl                      = NULL;
   instance->last_on_phy_link_notify        = 0;
   instance->rate_mask                      = WLAN_RATE_MASK;
   instance->max_beacon_tx_pending          = 0;
   instance->max_intra_beacon_interval      = 0;
   instance->beacon_skip_count              = 0;
   instance->current_phy_mode_channel_count = 0;
   instance->use_type                       = -1;
   instance->scan_state                     = NO_SCAN;

   instance->beacon_vendor_info_length = 0;
   memset(instance->beacon_vendor_info, 0, 256);
   instance->beacon_vendor_info_updated = 0;

   instance->essid_info_count    = 0;
   instance->essid_info          = NULL;
   instance->fixed_tx_rate_index = -1;
   memset(instance->current_essid, 0, sizeof(instance->current_essid));
   memset(instance->current_bssid, 0, ETH_ALEN);
   instance->dtim_count_current = 0;
   instance->dtim_count         = 1;

   instance->last_calibration_load = 0;
   instance->calibration_count     = 0;
   instance->last_nol_check        = 0;
   instance->last_rx_time          = 0;
#ifdef MESHAP_NO_POWER_SAVE_MODE
   instance->beacon_vendor_info_offset = NULL;
   instance->dtim_count_offset         = 0;
#else
   memset(instance->tim_bitmap, 0, WLAN_TIM_MAX_BITMAP_SIZE);
   atomic_set(&instance->bcast_ps_queued_count, 0);
#endif

   instance->tx_retry_count = 1;
   instance->probe_req_rx_count = 0;

   if (!mgmt_work_queue_init)
   {
      /* Initialising the mgmt packets processing work queue at once*/
      mgmt_work_queue_init = 1;
      skb_queue_head_init(&mgmt_work_queue);
	  meshap_process_task = kthread_create(meshap_process_pkt_que, NULL, "meshap_process_pkt_work");
	  kthread_bind(meshap_process_task,0);
	  spin_lock_init(&skb_mgmt_que_splock);

   }

   meshap_round_robin_initialize(instance);

   instance->first_beacon_sent_time     = 0;
   instance->last_beacon_interrupt_time = 0;
   instance->max_beaconing_uplink_timeout = MESHAP_BEACONING_UPLINK_MAX_TIMEOUT;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MD-MAC80211: FLOW : Exit: %s : %d\n", __func__,__LINE__);
}


static inline void meshap_process_mgmt(meshap_instance_t *instance,
                                       struct sk_buff    *skb,
                                       unsigned char     *p,
                                       unsigned char     *base,
                                       torna_mac_hdr_t   *hdr,
                                       int               len)
{
   /**
    * In STA mode we forward all management frames to the FSM
    * in AP mode we forward all management frames to the meshap
    * Now we are ready for the real data. Currently we have advanced
    * (p - base) number of bytes ahead. Hence we call skb_reserve
    * and move the data pointer downwards, those many bytes.
    */
   if (WLAN_FC_GET_STYPE(hdr->frame_control) == WLAN_FC_STYPE_ACTION)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"MD-MAC80211: INFO : Action Frame %s : %d\n", __func__, __LINE__);
      (tx_rx_pkt_stats.packet_drop[85])++;
	   dev_kfree_skb(skb);
      return;
   }

   switch (instance->iw_mode)
   {
   case IW_MODE_MASTER:
      skb_pull(skb, (p - base));
      switch (WLAN_FC_GET_STYPE(hdr->frame_control))
      {
      case WLAN_FC_STYPE_PROBE_REQ:
         ++instance->probe_req_rx_count;
         if (!(instance->flags & MESHAP_INSTANCE_FLAGS_NO_PROBE_RESP))
         {
            meshap_sta_fsm_process_mgmt_frame(instance, skb);
         }
         tx_rx_pkt_stats.rx_master_mode_mgmt_probe_req_frame++;
         dev_kfree_skb(skb);
         break;

      case WLAN_FC_STYPE_ASSOC_REQ:
      case WLAN_FC_STYPE_REASSOC_REQ:
      case WLAN_FC_STYPE_DISASSOC:
      case WLAN_FC_STYPE_AUTH:
      case WLAN_FC_STYPE_DEAUTH:

         /**
          * For all these packets, make sure the BSSID matches, as duty
          * cycle checks where we are in promiscous mode, we may get other
          * BSSID packets
          */
         if (!memcmp(hdr->addresses[0], instance->dev->dev_addr, ETH_ALEN))
         {
            al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"MD-MAC80211: INFO : %s: Processing MGMT frame type %d from "MACSTR " and dev_addr" 
                      MACSTR " and "MACSTR "%s : %d\n", instance->dev->name, WLAN_FC_GET_STYPE(hdr->frame_control), MAC2STR(hdr->addresses[1]),
                      MAC2STR(instance->dev->dev_addr), MAC2STR(hdr->addresses[0]), __func__, __LINE__);
            meshap_process_mgmt_frame(instance->dev, instance->dev_token, skb);
         }
         else
         {
			   (tx_rx_pkt_stats.packet_drop[86])++;
            dev_kfree_skb(skb);
         }
         break;

      default:
		   (tx_rx_pkt_stats.packet_drop[87])++;
         dev_kfree_skb(skb);
      }
      break;

   case IW_MODE_INFRA:
      skb_pull(skb, (p - base));
      if (WLAN_FC_GET_STYPE(hdr->frame_control) != WLAN_FC_STYPE_PROBE_REQ)
      {
         meshap_sta_fsm_process_mgmt_frame(instance, skb);
      }
		tx_rx_pkt_stats.rx_infra_mode_mgmt_frame++;
      dev_kfree_skb(skb);
      break;

   case IW_MODE_MONITOR:
      {
         int dump = 0;
         switch (WLAN_FC_GET_STYPE(hdr->frame_control))
         {
         case WLAN_FC_STYPE_PROBE_REQ:
            dump = (instance->monitor_mode_mask & WLAN_MONITOR_MODE_FLAG_PROBE_REQ);
            break;

         case WLAN_FC_STYPE_PROBE_RESP:
            dump = (instance->monitor_mode_mask & WLAN_MONITOR_MODE_FLAG_PROBE_RESP);
            break;

         case WLAN_FC_STYPE_ASSOC_REQ:
            dump = (instance->monitor_mode_mask & WLAN_MONITOR_MODE_FLAG_ASSOC_REQ);
            break;

         case WLAN_FC_STYPE_ASSOC_RESP:
            dump = (instance->monitor_mode_mask & WLAN_MONITOR_MODE_FLAG_ASSOC_RESP);
            break;

         case WLAN_FC_STYPE_BEACON:
            dump = (instance->monitor_mode_mask & WLAN_MONITOR_MODE_FLAG_BEACON);
            break;

         case WLAN_FC_STYPE_DISASSOC:
            dump = (instance->monitor_mode_mask & WLAN_MONITOR_MODE_FLAG_DISASSOC);
            break;

         case WLAN_FC_STYPE_DEAUTH:
            dump = (instance->monitor_mode_mask & WLAN_MONITOR_MODE_FLAG_DEAUTH);
            break;
         }
         if (dump)
         {
            PACKET_DUMP_2(KERN_INFO, skb->data, skb->len, "meshap_process_mgmt");
         }
      }
      dev_kfree_skb(skb);
      tx_rx_pkt_stats.packet_drop[135]++;
      break;
   default:
	  (tx_rx_pkt_stats.packet_drop[88])++;
      dev_kfree_skb(skb);
   }
}


static inline void meshap_process_data(meshap_instance_t *instance,
                                       struct sk_buff    *skb,
                                       unsigned char     *p,
                                       unsigned char     *base,
                                       torna_mac_hdr_t   *hdr,
                                       int               len,
                                       int               key_index,
                                       unsigned short    sequence_control)
{
   unsigned short s;
   struct ieee80211_rx_status *rxs;
   rxs = IEEE80211_SKB_RXCB(skb);

   if (memcmp(p, _snap_header, sizeof(_snap_header)))
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"MD-MAC80211: INFO : SNAP header drop %s : %d\n", __func__, __LINE__);
      ++instance->dev_stats.rx_dropped;
	   (tx_rx_pkt_stats.packet_drop[96])++;
	   kfree(rxs->drv_data);
      dev_kfree_skb(skb);
      return;
   }

   p += sizeof(_snap_header);

   hdr->ethernet.h_proto = _GET_SHORT();

   /* copy vlan info into header if packet type is vlan */
   if (hdr->ethernet.h_proto == ntohs(ETH_P_8021Q))
   {
      memcpy(&hdr->vlan_info, p, 2);
      p += 2;
      memcpy(&hdr->vlan_original_type, p, 2);
      p += 2;
   }

   if (hdr->frame_control & WLAN_FC_TODS && hdr->frame_control & WLAN_FC_FROMDS)
   {
      memcpy(hdr->ethernet.h_dest, hdr->addresses[2], ETH_ALEN);
      memcpy(hdr->ethernet.h_source, hdr->addresses[3], ETH_ALEN);
   }
   else if (hdr->frame_control & WLAN_FC_TODS)
   {
      memcpy(hdr->ethernet.h_dest, hdr->addresses[2], ETH_ALEN);
      memcpy(hdr->ethernet.h_source, hdr->addresses[1], ETH_ALEN);
   }
   else
   {
      memcpy(hdr->ethernet.h_dest, hdr->addresses[0], ETH_ALEN);
      memcpy(hdr->ethernet.h_source, hdr->addresses[2], ETH_ALEN);
   }

   skb->protocol = hdr->ethernet.h_proto;

   /**
    * Now we are ready for the real data. Currently we have advanced
    * (p - base) number of bytes ahead. Hence we call skb_reserve
    * and move the data pointer downwards, those many bytes.
    */

   skb_pull(skb, (p - base));

   if (*hdr->ethernet.h_dest & 1)
   {
      if (memcmp(hdr->ethernet.h_dest, instance->dev->broadcast, ETH_ALEN) == 0)
      {
         skb->pkt_type = PACKET_BROADCAST;
      }
      else
      {
         skb->pkt_type = PACKET_MULTICAST;
      }
   }
   else if (1 /*dev->flags&IFF_PROMISC*/)
   {
      if (memcmp(hdr->ethernet.h_dest, instance->dev->dev_addr, ETH_ALEN))
      {
         skb->pkt_type = PACKET_OTHERHOST;
      }
   }

   instance->dev_stats.rx_bytes += skb->len;
   ++instance->dev_stats.rx_packets;

   if (instance->virtual_associated == 0)
   {
      meshap_core_process_data_frame(instance->dev, instance->dev_token, skb);
   }
   else
   {
      if (hdr->ethernet.h_proto == 0xDEDE)
      {
         hdr->flags |= TORNA_MAC_HDR_FLAG_VIRTUAL;
         meshap_core_process_data_frame(instance->dev, instance->dev_token, skb);
      }
      else
      {
		 kfree(rxs->drv_data);
	     (tx_rx_pkt_stats.packet_drop[89])++;
         dev_kfree_skb(skb);
      }
   }
}

extern meshap_rate_table_t  meshap_rate_table;

static inline int meshap_rate_code_to_mbps(struct meshap_instance *instance, int rate_code, int band)
{
   /**
    * We use it as an index to instance->current_rate_table->rateCodeToIndex,
    * and get the rate index which is then used as an index to
    * instance->current_rate_table->info to get us the rate
    * in terms of Kbps, which we convert to Mbps
    */

    struct ieee80211_sub_if_data *sdata;
    struct ieee80211_rate *rate = NULL;
    struct ieee80211_supported_band *sband;
    sdata = IEEE80211_DEV_TO_SUB_IF(instance->dev);
    struct ieee80211_local *local = hw_to_local(&sdata->local->hw);
    sband = local->hw.wiphy->bands[band];

    if (WARN_ON(!sband)) {
        return -1;
    }
    rate = &sband->bitrates[rate_code];
    if (rate->bitrate >= 0 ) {
        return (rate->bitrate / 10);
    }
    else {
        return 0;
    }
}


void meshap_process_pkt(struct sk_buff *skb)
{
   unsigned char     *p, *base;
   torna_mac_hdr_t   *hdr;
   int               len = 0;
   unsigned char     type = 0;
   unsigned char     stype = 0;
   int               base_length = 0, band, i;
   unsigned short    s, tx_rate = 0, sequence_ctrl;
   meshap_instance_t *instance = NULL;
   struct ieee80211_rx_status *rxs;
   /**
    * Process a regular 802.11 frame, extracting
    *    1) Frame type
    *    2) Sub-type
    *    3) FROM-DS/TO-DS
    *    4) 3 or 4 addresses
    *    5) Data
    *    6) TORNA WDS HEADER if present
    */

   if (skb == NULL)
   {
      printk(KERN_ERR "Meshap_process_pkt: skb NULL\n");
	  (tx_rx_pkt_stats.packet_drop[95])++;
      return;
   }

   /* For torna header and other driver parameters */
   hdr         = _meshap_get_torna_hdr(skb);
   len         = hdr->data_len;
   base        = skb->data;
   base_length = len;

   len -= 4;              /** 802.11 CRC */
   p    = base;

   hdr->signature[0] = TORNA_MAC_HDR_SIGNATURE_1;
   hdr->signature[1] = TORNA_MAC_HDR_SIGNATURE_2;
   hdr->type         = TORNA_MAC_HDR_TYPE_SK_BUFF;

   hdr->frame_control = _GET_LE_SHORT();                        /* Frame control */
   p += 2;                                                      /* Skip Duration/ID */

   type  = WLAN_FC_GET_TYPE(hdr->frame_control);
   stype = WLAN_FC_GET_STYPE(hdr->frame_control);

   /**
    * Meshdynamics uses the Order bit of the Frame Control when
    * there needs to be sent a BCAST/MCAST packet to be ignored
    * by Mesh nodes, and only processed by regular STAs
    */

   if (hdr->frame_control & WLAN_FC_ORDER)
   {
      hdr->flags         |= TORNA_MAC_HDR_FLAG_MESH_IGNORE;
      hdr->frame_control &= ~WLAN_FC_ORDER;
      type  = WLAN_FC_GET_TYPE(hdr->frame_control);
      stype = WLAN_FC_GET_STYPE(hdr->frame_control);
   }

   rxs = IEEE80211_SKB_RXCB(skb);
   if ((type == WLAN_FC_TYPE_MGMT) || (type == WLAN_FC_TYPE_DATA))
   {
      memcpy(hdr->addresses[0], p, ETH_ALEN);
      p += ETH_ALEN;                                                /* Address 1 */
      memcpy(hdr->addresses[1], p, ETH_ALEN);
      p += ETH_ALEN;                                                /* Address 2 */

      memcpy(hdr->addresses[2], p, ETH_ALEN);
      p += ETH_ALEN;                                                /* Address 3 */
      memcpy(&sequence_ctrl, p, 2);
      p += 2;                                                       /* Sequence control */

      sequence_ctrl = le16_to_cpu(sequence_ctrl);

      len -= (2                 /* Frame control */
              + 2               /* Duration/ID */
              + (3 * ETH_ALEN)  /* addresses */
              + 2               /* Sequence */
              );

      if (hdr->frame_control & WLAN_FC_TODS && hdr->frame_control & WLAN_FC_FROMDS)
      {
         memcpy(hdr->addresses[3], p, ETH_ALEN);
         p   += ETH_ALEN;                                          /* Address 4 */
         len -= ETH_ALEN;
      }

      /* Locate the instance structure */
      instance = meshap_get_instance(skb->dev);
      if (NULL == instance)
      {
		 if(type == WLAN_FC_TYPE_DATA) {
			 kfree(rxs->drv_data);
		 }
	     (tx_rx_pkt_stats.packet_drop[90])++;
         dev_kfree_skb(skb);
         return;
      }

      struct ieee80211_rx_status *status = IEEE80211_SKB_RXCB(skb);
      band = status->band;
      tx_rate      = meshap_rate_code_to_mbps(instance, hdr->tx_rate, band);
      if (tx_rate == -1)
      {
          dev_kfree_skb(skb);
          return;
      }
      hdr->tx_rate = tx_rate;

      if ((instance->duty_cycle_flags & MESHAP_INSTANCE_DUTY_CYCLE_FLAG)&& (hdr->tx_rate != 0))
          meshap_capture_duty_cycle_frames(skb, skb->dev, hdr->rssi, hdr->tx_rate);
      else
      switch (type)
      {
      case WLAN_FC_TYPE_MGMT:
         instance->net_if_stats.rx_mgmt++;
         instance->net_if_stats.total_rx_mgmt[stype]++;
         meshap_process_mgmt(instance, skb, p, base, hdr, len);
         break;

      case WLAN_FC_TYPE_DATA:
         instance->net_if_stats.rx_data++;
         instance->net_if_stats.total_rx_data[stype]++;
         if(stype &  WLAN_FC_STYPE_QOS)
         {
            memcpy(&hdr->qos_control, p, 2);
            hdr->qos_control = le16_to_cpu(hdr->qos_control);
            p += 2;
            len -= 2;
         }
         meshap_process_data(instance, skb, p, base, hdr, len, hdr->key_index, sequence_ctrl);
         break;

      default:
	     (tx_rx_pkt_stats.packet_drop[117])++;
         dev_kfree_skb(skb);
      }
   }
}


void dump_bytes1(char *D, int count)
{
   int i, base = 0;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"-------- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- ----------------\n");

   for (i = 0; i < count; i++)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "%02X ", D[i]);
   }
   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "\n");
   return 0;

   for ( ; base + 16 < count; base += 16)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "%08X ", base);

      for (i = 0; i < 16; i++)
      {
         al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "%02X ", D[base + i]);
      }

      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "\n");
   }
   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "%08X ", base);
   for (i = base; i < count; i++)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "%02X ", D[i]);
   }
   for ( ; i < base + 16; i++)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "   ");
   }
   for ( ; i < base + 16; i++)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, " ");
   }
   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "\n");
   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "-------- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- ----------------\n");
   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "\n");
}


void dump_bytes(char *D, int count)
{
   int i, base = 0;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "-------- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- ----------------\n");
   for ( ; base + 16 < count; base += 16)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "%08X ", base);

      for (i = 0; i < 16; i++)
      {
         al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "%02X ", D[base + i]);
      }

      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "\n");
   }
   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "%08X ", base);
   for (i = base; i < count; i++)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "%02X ", D[i]);
   }
   for ( ; i < base + 16; i++)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "   ");
   }
   for ( ; i < base + 16; i++)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION," ");
   }
   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "\n");
   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "-------- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- ----------------\n");
   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "\n");
}


void meshap_fill_drv_data(struct ieee80211_rx_status *rxs, int rate)
{
   rxs->drv_data->rs_rssi = 96 + rxs->signal;
   rxs->drv_data->rs_rate = rate;
   rxs->drv_data->rs_antenna = rxs->antenna;
}


/* workqueue is implemented to avoid processing management frame
 * in interrupt contect.
 */
static void meshap_process_pkt_que(void *data)
{
	struct sk_buff             *skb;
	struct ieee80211_rx_status *rxs = NULL;
	meshap_drv_data_t          drv_data;
	int rate;
	int cpu;

	while(1) {

		while ((skb = meshap_skb_dequeue(&mgmt_work_queue, &skb_mgmt_que_splock)))
		{
			rxs           = IEEE80211_SKB_RXCB(skb);
			rate          = rxs->rate_idx;
			rxs->drv_data = &drv_data;
			memset(&drv_data, 0, sizeof(meshap_drv_data_t));

			meshap_fill_drv_data(rxs, rate);
			meshap_process_pkt(skb);
		}

		set_current_state(TASK_INTERRUPTIBLE);
		schedule();
	}

}

void meshap_hook(struct sk_buff *skb, unsigned char pkt_type, uint8_t mon_flag)
{
   struct sk_buff       *skb_local = NULL;
   struct net_device    *dev;
   unsigned char        *name = "wlan3";
   struct ieee80211_hdr *hdr;
   unsigned char        stype = 0;
   int rate;
   struct ieee80211_rx_status *rxs;

   if (NULL == skb)
   {
	   (tx_rx_pkt_stats.packet_drop[92])++;
      return;
   }


   if (MESHAP_MGMT_PKT == pkt_type)   //TODO: add check for use_type AP. Copy skb for use type AP only. for WM, we dont need
   {
      skb_local = skb_copy(skb, GFP_ATOMIC);
      if ((NULL == skb_local) || reboot_in_progress)
      {
		  if(skb_local) {
			  dev_kfree_skb(skb_local);
		  }else {
           al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MD-MAC80211: ERROR : Failed to copy skb for meshap's processing %s : %d\n", __func__,__LINE__);
		  }
		 (tx_rx_pkt_stats.packet_drop[91])++;
         return;
      }
      if (mon_flag == TRUE)
      {
         dev            = dev_get_by_name(&init_net, name);
         skb_local->dev = dev;
         hdr            = (struct ieee80211_hdr *)skb_local->data;
         stype          = WLAN_FC_GET_STYPE(hdr->frame_control);
      }

      /*
       * queueing the management packet to workqueue.
       * This is to avoid processing the management packets in interrupt context
       */
      meshap_skb_enqueue(&mgmt_work_queue, skb_local, &skb_mgmt_que_splock);
	  /* Trigger a Signal to meshap_process_pkt_que */
	   wake_up_process(meshap_process_task);

      return;
   }
   else
   {
   if (reboot_in_progress)
   {
	   dev_kfree_skb(skb);
	   (tx_rx_pkt_stats.packet_drop[93])++;
	   return;
   }
      rxs = IEEE80211_SKB_RXCB(skb);
      rate = rxs->rate_idx;
      rxs->drv_data = (meshap_drv_data_t*)kmalloc(sizeof(meshap_drv_data_t),GFP_ATOMIC);
      if (NULL == rxs->drv_data)
      {
         al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MD-MAC80211: ERROR : Failed to allocate memory for meshap private data %s : %d\n", __func__,__LINE__);
		   dev_kfree_skb(skb);
		   (tx_rx_pkt_stats.packet_drop[94])++;
         return;
      }
      memset(rxs->drv_data, 0, sizeof(meshap_drv_data_t));
      meshap_fill_drv_data(rxs, rate);
      meshap_process_pkt(skb);
   }
}


int mesh_mac80211_reset_device(struct net_device *dev, int phy_mode_change)
{
   return 0;
}


int
meshap_mac80211_dev_set_channel(struct meshap_instance *instance,
                                int                    channel,
                                int                    quite)
{
   int                       i, freq;
   unsigned long             flags;
   enum nl80211_channel_type channel_type = NL80211_CHAN_NO_HT;
   struct net_device         *dev         = instance->dev;
   struct wireless_dev       *wdev        = dev->ieee80211_ptr;
   struct cfg80211_registered_device *rdev = WIPHY_TO_DEV(wdev->wiphy);
   struct cfg80211_chan_def chandef;
   struct ieee80211_sub_if_data *sdata = IEEE80211_DEV_TO_SUB_IF(instance->dev);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MD-MAC80211: FLOW : Enter: %s : %d\n", __func__,__LINE__);

   if (instance->current_channel && instance->current_channel->number == channel)
   {
      if (!instance->duty_cycle_set_channel_flag)
          instance->wm_duty_cycle_flag = 1;
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"MD-MAC80211: INFO : current_channel %d channel need to change %d %s : %d\n",
               instance->current_channel->number, channel,  __func__, __LINE__);
      return 0;
   }

   if (instance->country_code == CTRY_PRIVATE_1)
   {
      for (i = 0; i < instance->channel_count; i++)
      {
         if (instance->channels[i].number == channel)
         {
            goto _lb_change_channel;
         }
      }
   }
   else
   {
      for (i = 0; i < instance->channel_count; i++)
      {
         if (instance->channels[i].number == channel)
         {
            switch (instance->current_phy_mode)
            {
            case WLAN_PHY_MODE_802_11_A:
               if ((instance->channels[i].flags & WLAN_CHANNEL_802_11_A) == WLAN_CHANNEL_802_11_A)
               {
                  goto _lb_change_channel;
               }
               break;

            case WLAN_PHY_MODE_802_11_B:
               if ((instance->channels[i].flags & WLAN_CHANNEL_802_11_B) == WLAN_CHANNEL_802_11_B)
               {
                  goto _lb_change_channel;
               }
               break;

            case WLAN_PHY_MODE_802_11_G:
            case WLAN_PHY_MODE_802_11_PURE_G:
               if ((instance->channels[i].flags & WLAN_CHANNEL_802_11_G) == WLAN_CHANNEL_802_11_G)
               {
                  goto _lb_change_channel;
               }
               break;

            case WLAN_PHY_MODE_802_11_AN:
            case WLAN_PHY_MODE_802_11_N:
            case WLAN_PHY_MODE_802_11_BGN:
            case WLAN_PHY_MODE_802_11_2GHZ_PURE_N:
            case WLAN_PHY_MODE_802_11_5GHZ_PURE_N:
               if ((instance->channels[i].flags & WLAN_CHANNEL_802_11_N) == WLAN_CHANNEL_802_11_N)
               {
                  goto _lb_change_channel;
               }
               break;
            case WLAN_PHY_MODE_802_11_AC:
            case WLAN_PHY_MODE_802_11_ANAC:
               if ((instance->channels[i].flags & WLAN_CHANNEL_802_11_AC) == WLAN_CHANNEL_802_11_AC)
               {
                  goto _lb_change_channel;
               }
               break;
            }
         }
      }

      for (i = 0; i < instance->channel_count; i++)
      {
         switch (instance->current_phy_mode)
         {
         case WLAN_PHY_MODE_802_11_A:
            if ((instance->channels[i].flags & WLAN_CHANNEL_802_11_A) == WLAN_CHANNEL_802_11_A)
            {
               goto _lb_change_channel;
            }
            break;

         case WLAN_PHY_MODE_802_11_B:
            if ((instance->channels[i].flags & WLAN_CHANNEL_802_11_B) == WLAN_CHANNEL_802_11_B)
            {
               goto _lb_change_channel;
            }
            break;

         case WLAN_PHY_MODE_802_11_G:
         case WLAN_PHY_MODE_802_11_PURE_G:
            if ((instance->channels[i].flags & WLAN_CHANNEL_802_11_G) == WLAN_CHANNEL_802_11_G)
            {
               goto _lb_change_channel;
            }
            break;

            case WLAN_PHY_MODE_802_11_AN:
            case WLAN_PHY_MODE_802_11_N:
            case WLAN_PHY_MODE_802_11_BGN:
            case WLAN_PHY_MODE_802_11_2GHZ_PURE_N:
            case WLAN_PHY_MODE_802_11_5GHZ_PURE_N:
               if ((instance->channels[i].flags & WLAN_CHANNEL_802_11_N) == WLAN_CHANNEL_802_11_N)
               {
                  goto _lb_change_channel;
               }
               break;
            case WLAN_PHY_MODE_802_11_AC:
            case WLAN_PHY_MODE_802_11_ANAC:
               if ((instance->channels[i].flags & WLAN_CHANNEL_802_11_AC) == WLAN_CHANNEL_802_11_AC)
               {
                  goto _lb_change_channel;
               }
               break;
         }
      }
   }
   return -EINVAL;

_lb_change_channel:

   if (!quite)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"MD-MAC80211: INFO : %s: Changing channel to %d MHz %s : %d\n",
                     instance->dev->name, instance->channels[i].frequency, __func__, __LINE__);
   }

   MESHAP_INSTANCE_LOCK(instance, flags);
   instance->current_channel = &(instance->channels[i]); 
   freq = instance->current_channel->frequency;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"MD-MAC80211: INFO : Changing channel to %s : %d MHz %s : %d\n", instance->dev->name, freq, __func__, __LINE__);
   /* Primary channel not allowed */
   MESHAP_INSTANCE_UNLOCK(instance, flags);

   if (!instance->duty_cycle_set_channel_flag)
   {
       instance->wm_duty_cycle_flag = 1;
       return -EINVAL;
   }
   /*update channel_type based on secondary channel offset*/
	if(instance->sec_chan_offset == 1) {
		channel_type = NL80211_CHAN_HT40PLUS;
	}else if(instance->sec_chan_offset == -1) {
		channel_type = NL80211_CHAN_HT40MINUS;
	}
   memset(&chandef,0,sizeof(chandef));
   cfg80211_chandef_create(&chandef, ieee80211_get_channel(wdev->wiphy, freq), channel_type);
   if (!chandef.chan) {
       return -EINVAL;
   }

	mutex_lock(&sdata->local->mtx);
   if (ieee80211_vif_use_channel(sdata, &chandef, IEEE80211_CHANCTX_SHARED))
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MD-MAC80211: ERROR : Failed to set channel for %s %s : %d\n", instance->dev->name, __func__,__LINE__);
   }
	mutex_unlock(&sdata->local->mtx);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MD-MAC80211: FLOW : Exit: %s : %d\n", __func__,__LINE__);
   return 0;
}


int
meshap_mac80211_set_short_slot_time(struct net_device *dev)
{
   struct cfg80211_registered_device *rdev;
   struct wireless_dev               *wdev = dev->ieee80211_ptr;
   struct bss_parameters             params;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MD-MAC80211: FLOW : Enter: %s : %d\n", __func__,__LINE__);

   memset(&params, 0, sizeof(params));
   /* default to not changing parameters */
   params.use_cts_prot        = -1;
   params.use_short_preamble  = -1;
   params.use_short_slot_time = -1;
   params.ap_isolate          = -1;
   params.ht_opmode           = -1;
   params.use_short_slot_time = 1;
   rdev = WIPHY_TO_DEV(wdev->wiphy);

   wdev_lock(wdev);
   return rdev->ops->change_bss(&rdev->wiphy, dev, &params);
   wdev_unlock(wdev);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MD-MAC80211: FLOW : Exit: %s : %d\n", __func__,__LINE__);
}


int meshap_mac80211_set_ap_mode(meshap_instance_t *instance)
{
   int err       = 0;
   int flags     = 0;          /* Not valid for AP mode. But we need to pass it to the generic API of cfg80211_change_iface */
   int old_flags = 0;
   struct cfg80211_registered_device *rdev = NULL;
   struct wireless_dev               *wdev = instance->dev->ieee80211_ptr;
   struct vif_params                 params;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MD-MAC80211: FLOW : Enter: %s : %d\n", __func__,__LINE__);

   rdev = WIPHY_TO_DEV(wdev->wiphy);
   memset(&params, 0, sizeof(params));

   params.use_4addr = 1; /* TBD: Shall AP/Master interface use 4 addresses? */

   rtnl_lock();          /*Following functions need to be called under rtnl lock*/
   old_flags = instance->dev->flags;
   old_flags &= ~IFF_UP;

   /*mac80211 does not support changing interface mode when the interface is UP. So put down the interface and then change the mode to AP*/
   dev_change_flags(instance->dev, old_flags); 
   err        = cfg80211_change_iface(rdev, instance->dev, NL80211_IFTYPE_AP, &flags, &params);
   old_flags |= IFF_UP;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"MD-MAC80211: INFO : old_flags:%x err=%d, dev_name:%s %s : %d\n",
            old_flags, err, instance->dev->name, __func__, __LINE__);
	/*Following functions need to be called under rtnl lock*/
   dev_change_flags(instance->dev, old_flags);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"MD-MAC80211: INFO : old_flags:%x err=%d, dev_name:%s SUCCESSFULL out of LOCK %s : %d\n",
           old_flags, err, instance->dev->name, __func__, __LINE__);
   rtnl_unlock();
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MD-MAC80211: FLOW : Exit: %s : %d\n", __func__,__LINE__);
   return err;
}


char *meshap_get_parent_name(struct wiphy *wiphy)
{
   struct cfg80211_registered_device *rdev;
   struct wireless_dev               *wdev;
   char *parent_name = NULL;

   rdev = WIPHY_TO_DEV(wiphy);

   list_for_each_entry(wdev, &rdev->wdev_list, list)
   {
      if (wdev->netdev)
      {
         parent_name = wdev->netdev->name;
      }
   }

   return parent_name;
}

int meshap_mac80211_set_tx_params(meshap_instance_t *instance)
{
   int err       = 0;
   int flags     = 0;          /* Not valid  for AP mode. But we need to pass it to the generic API of cfg80211_change_iface */
   int old_flags = 0;
   struct cfg80211_registered_device *rdev = NULL;
   struct wireless_dev               *wdev = instance->dev->ieee80211_ptr;
   struct ieee80211_txq_params txq_params;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MD-MAC80211: FLOW : Enter: %s : %d\n", __func__,__LINE__);

   rdev = WIPHY_TO_DEV(wdev->wiphy);

   txq_params.ac = 0;
   txq_params.txop = 0x0000;
   txq_params.cwmin = 0x0003;
   txq_params.cwmax = 0x007F;
   txq_params.aifs = 0x02;
   int ret ;
   ret = rdev->ops->set_txq_params(&rdev->wiphy, instance->dev, &txq_params);
   txq_params.ac = 1;
   txq_params.txop = 0x567E;
   txq_params.cwmin = 0x000F;
   txq_params.cwmax = 0x03FF;
   txq_params.aifs = 0x02;
   ret = rdev->ops->set_txq_params(&rdev->wiphy, instance->dev, &txq_params);
   txq_params.ac = 2;
   txq_params.txop = 0x0000;
   txq_params.cwmin = 0x001F;
   txq_params.cwmax = 0x003FF;
   txq_params.aifs = 0x06;
   ret = rdev->ops->set_txq_params(&rdev->wiphy, instance->dev, &txq_params);
   txq_params.ac = 3;
   txq_params.txop = 0x00;
   txq_params.cwmin = 0x003F;
   txq_params.cwmax = 0x03FF;
   txq_params.aifs = 0x06;
   ret = rdev->ops->set_txq_params(&rdev->wiphy, instance->dev, &txq_params);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MD-MAC80211: FLOW : Exit: %s : %d\n", __func__,__LINE__);
}

/*get band*/
enum ieee80211_band meshap_get_current_band(int current_phy_mode)
{
    switch(current_phy_mode)
    {
        case WLAN_PHY_MODE_802_11_A:
        case WLAN_PHY_MODE_802_11_N:
        case WLAN_PHY_MODE_802_11_AC:
        case WLAN_PHY_MODE_802_11_AN:
        case WLAN_PHY_MODE_802_11_ANAC:
        case WLAN_PHY_MODE_802_11_5GHZ_PURE_N:
           return IEEE80211_BAND_5GHZ;
        case WLAN_PHY_MODE_802_11_B:
        case WLAN_PHY_MODE_802_11_G:
        case WLAN_PHY_MODE_802_11_BGN:
        case WLAN_PHY_MODE_802_11_PURE_G:
        case WLAN_PHY_MODE_802_11_2GHZ_PURE_N:
          return IEEE80211_BAND_2GHZ;
        default:
          return IEEE80211_BAND_5GHZ;
    }
}
