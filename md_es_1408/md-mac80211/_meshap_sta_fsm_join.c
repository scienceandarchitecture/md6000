/********************************************************************************
* MeshDynamics
* --------------
* File     : _meshap_sta_fsm_join.c
* Comments : Atheros STA FSM join entry point
* Created  : 10/18/2004
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  6  |4/12/2005 | Beacon miss logic changed                       | Sriram |
* -----------------------------------------------------------------------------
* |  5  |4/6/2005  | Security changes                                | Sriram |
* -----------------------------------------------------------------------------
* |  4  |1/14/2005 | RSSI calculation modified                       | Sriram |
* -----------------------------------------------------------------------------
* |  3  |1/7/2005  | Implemented get_last_beacon_time                | Sriram |
* -----------------------------------------------------------------------------
* |  2  |12/23/2004| Implemented link notify watchdog                | Sriram |
* -----------------------------------------------------------------------------
* |  1  |12/22/2004| Changes for beacon miss verification            | Sriram |
* -----------------------------------------------------------------------------
* |  0  |10/18/2004| Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

/**
 * This is a private source file included by meshap_sta_fsm.c, hence
 * the name begins with an underscore.
 * It was created to improve readability.
 */

/**
 * Joining is a multi-state operation, leading up to either the
 * connected state or the disconnected state on failure.
 */
int meshap_sta_fsm_join(meshap_instance_t *instance,
                        const char *essid, int essid_length,
                        unsigned char *bssid, int channel,
                        unsigned char *ie_in, int ie_in_length,
                        unsigned char *ie_out)
{
   _meshap_sta_fsm_data_t *data;
   _join_param_t          *param;
   int timeout;
//1602MIG defining DEFINE_WAIT
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Enter %s: %d\n", __func__, __LINE__);
   DEFINE_WAIT(wait);


   data  = (_meshap_sta_fsm_data_t *)instance->fsm_data;
   param = (_join_param_t *)kmalloc(sizeof(_join_param_t), GFP_ATOMIC);

   instance->connected = 0;
   instance->fsm_state = MESHAP_STA_FSM_STATE_DISCONNECTED;
   instance->last_on_phy_link_notify = 0;

   memcpy(instance->current_bssid, bssid, ETH_ALEN);

   openfsm_reset(data->fsm, &_states[_MESHAP_STA_FSM_DISCONNECTED]);
   openfsm_set_state_data(data->fsm, NULL);

   memset(param, 0, sizeof(_join_param_t));

   param->essid_length = essid_length;
   strcpy(param->essid, essid);
   memcpy(param->bssid, bssid, ETH_ALEN);
   param->channel      = channel;
   param->ie_in        = ie_in;
   param->ie_in_length = ie_in_length;
   param->ie_out       = ie_out;

   /**
    * Transition to the _MESHAP_STA_FSM_SEND_AUTH state and do
    * its work
    */

   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MAC80211 : INFO : %s: Changing state to _MESHAP_STA_FSM_SEND_AUTH %s: %d\n",instance->dev->name,
				  __func__, __LINE__);
   openfsm_transition(data->fsm, &_states[_MESHAP_STA_FSM_SEND_AUTH]);

   openfsm_work(data->fsm, param, NULL);

   timeout = MESHAP_STA_FSM_AUTH_RESP_TIMEOUT + MESHAP_STA_FSM_ASSOC_RESP_TIMEOUT;

   /**
    * Goto sleep and get awakened by one of the following events
    * 1) Auth response timeout
    * 2) Negative auth response
    * 3) Assoc response timeout
    * 4) Negative assoc response
    * 5) Possitive assoc response
    */

   INTERRUPTIBLE_SLEEP_ON_TIMEOUT(&data->wqh, ((timeout * HZ)  / 1000), &wait);

   /**
    * We are successful if we find ourselves in the CONNECTED state
    */

   instance->connected = (openfsm_get_current_state(data->fsm) == &_states[_MESHAP_STA_FSM_CONNECTED]);

   if (!instance->connected)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MAC80211 : INFO : %s: Changing state to _MESHAP_STA_FSM_DISCONNECTED, connected:"
         "%d %s: %d\n", instance->dev->name, instance->connected, __func__, __LINE__);
      openfsm_reset(data->fsm, &_states[_MESHAP_STA_FSM_DISCONNECTED]);
      openfsm_set_state_data(data->fsm, NULL);
   }
   else
   {
      instance->last_beacon_time = jiffies;
      instance->last_rx_time     = jiffies;
      memset(instance->rx_info, 0, sizeof(instance->rx_info));
   }
   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MAC80211 : INFO : connected: %d %s: %d\n", instance->connected, __func__, __LINE__);

   kfree(param);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Exit %s: %d\n", __func__, __LINE__);
   return instance->connected;
}


void meshap_sta_fsm_join_virtual(meshap_instance_t *instance)
{
   _meshap_sta_fsm_data_t *data;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Enter %s: %d\n", __func__, __LINE__);
   data = (_meshap_sta_fsm_data_t *)instance->fsm_data;

   if (instance->fsm_state != MESHAP_STA_FSM_STATE_RUNNING)
   {
      openfsm_transition(data->fsm, &_states[_MESHAP_STA_FSM_CONNECTED]);
      instance->fsm_state = MESHAP_STA_FSM_STATE_RUNNING;
   }
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Exit %s: %d\n", __func__, __LINE__);
}
