/********************************************************************************
* MeshDynamics
* --------------
* File     : openfsm.h
* Comments : Main OpenFSM header
* Created  : 10/11/2004
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  0  |10/11/2004| Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

#ifndef __OPENFSM_H__
#define __OPENFSM_H__

typedef void * openfsm_t;

struct openfsm_platform
{
   void         * (*malloc)               (unsigned int size);
   void         (*free)                 (void *memblock);
   unsigned int (*create_lock)  (void);
   void         (*destroy_lock) (unsigned int lock);
};

typedef struct openfsm_platform   openfsm_platform_t;

struct openfsm_state;

struct openfsm_state
{
   int (*enter_state)              (openfsm_t fsm, const struct openfsm_state *state, void *state_data);
   int (*state_function)   (openfsm_t fsm, const struct openfsm_state *state, void *state_data, void *param1, void *param2);
   int (*leave_state)              (openfsm_t fsm, const struct openfsm_state *state, void *state_data);
   int (*timer)                    (openfsm_t fsm, const struct openfsm_state *state, void *state_data, unsigned int state_timer_count);
};

typedef struct openfsm_state   openfsm_state_t;

struct openfsm_data
{
   const openfsm_platform_t *platform;
   const openfsm_state_t    *current_state;
   unsigned int             lock;
   void                     *state_data;
   void                     *fsm_data;
   unsigned int             state_timer_count;
};

typedef struct openfsm_data   openfsm_data_t;

#define OPENFSM_DECLARE_STATE_FUNCTIONS(name)                                                                                       \
   static int __enter_ ## name ## _state(openfsm_t fsm, const struct openfsm_state *state, void *state_data);                       \
   static int __ ## name ## _state(openfsm_t fsm, const struct openfsm_state *state, void *state_data, void *param1, void *param2); \
   static int __leave_ ## name ## _state(openfsm_t fsm, const struct openfsm_state *state, void *state_data);                       \
   static int __ ## name ## _state_timer(openfsm_t fsm, const struct openfsm_state *state, void *state_data, unsigned int state_timer_count)

#define OPENFSM_STATE(name) \
   { __enter_ ## name ## _state, __ ## name ## _state, __leave_ ## name ## _state, __ ## name ## _state_timer }

#define OPENFSM_ENTER_STATE_FUNCTION(name) \
   static int __enter_ ## name ## _state(openfsm_t fsm, const struct openfsm_state *state, void *state_data)

#define OPENFSM_STATE_FUNCTION(name) \
   static int __ ## name ## _state(openfsm_t fsm, const struct openfsm_state *state, void *state_data, void *param1, void *param2)

#define OPENFSM_LEAVE_STATE_FUNCTION(name) \
   static int __leave_ ## name ## _state(openfsm_t fsm, const struct openfsm_state *state, void *state_data)

#define OPENFSM_STATE_TIMER_FUNCTION(name) \
   static int __ ## name ## _state_timer(openfsm_t fsm, const struct openfsm_state *state, void *state_data, unsigned int state_timer_count)



#define OPENFSM_LOCK(fsm, flags)      OPENFSM_PLATFORM_LOCK(((openfsm_data_t *)fsm)->lock, flags)
#define OPENFSM_UNLOCK(fsm, flags)    OPENFSM_PLATFORM_UNLOCK(((openfsm_data_t *)fsm)->lock, flags)

openfsm_t openfsm_create(const openfsm_platform_t *platform, const openfsm_state_t *init_state);
void openfsm_destroy(openfsm_t fsm);
void openfsm_set_fsm_data(openfsm_t fsm, void *fsm_data);
void *openfsm_get_fsm_data(openfsm_t fsm);
const openfsm_state_t *openfsm_get_current_state(openfsm_t fsm);

int openfsm_start(openfsm_t fsm);
int openfsm_set_state_data(openfsm_t fsm, void *state_data);
void *openfsm_get_state_data(openfsm_t fsm);
int openfsm_transition(openfsm_t fsm, const openfsm_state_t *to_state);
int openfsm_timer(openfsm_t fsm);
int openfsm_work(openfsm_t fsm, void *param1, void *param2);
int openfsm_reset(openfsm_t fsm, const openfsm_state_t *to_state);

#endif /*__OPENFSM_H__*/
