/********************************************************************************
* MeshDynamics
* --------------
* File     : openfsm-impl.h
* Comments : OPENFSM Implementation for Linux Kernel Mode
* Created  : 8/24/2007
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  0  |8/24/2007 | Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

#ifndef __OPENFSM_IMPL_H__
#define __OPENFSM_IMPL_H__

struct _openfsm_lock
{
   spinlock_t sl;
};

typedef struct _openfsm_lock   _openfsm_lock_t;
#if 0
#define OPENFSM_PLATFORM_LOCK(l, flags)      spin_lock_irqsave((&((_openfsm_lock_t *)l)->sl), flags);
#define OPENFSM_PLATFORM_UNLOCK(l, flags)    spin_unlock_irqrestore((&((_openfsm_lock_t *)l)->sl), flags);
#else
#define OPENFSM_PLATFORM_LOCK(l, flags)      spin_lock((&((_openfsm_lock_t *)l)->sl));
#define OPENFSM_PLATFORM_UNLOCK(l, flags)    spin_unlock((&((_openfsm_lock_t *)l)->sl));
#endif
#endif /*__OPENFSM_IMPL_H__*/
