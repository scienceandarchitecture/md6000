/********************************************************************************
* MeshDynamics
* --------------
* File     : openfsm.c
* Comments : Main OpenFSM source
* Created  : 10/11/2004
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  0  |10/11/2004| Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

#include "openfsm.h"
#include "al.h"
#include <linux/kernel.h>
#include "al.h"
#ifndef NULL
#define NULL    0
#endif


openfsm_t openfsm_create(const openfsm_platform_t *platform, const openfsm_state_t *init_state)
{
   openfsm_data_t *data;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Enter %s: %d\n", __func__, __LINE__);
   data = (openfsm_data_t *)platform->malloc(sizeof(openfsm_data_t));

   data->platform          = platform;
   data->current_state     = init_state;
   data->state_data        = NULL;
   data->fsm_data          = NULL;
   data->lock              = data->platform->create_lock();
   data->state_timer_count = 0;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MAC80211 : FLOW : Exit %s : %d\n", __func__,__LINE__);
   return (openfsm_t)data;
}


void openfsm_destroy(openfsm_t fsm)
{
   openfsm_data_t *data;

   data = (openfsm_data_t *)fsm;

   if (data->current_state->leave_state != NULL)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"MAC80211 : INFO : open fsm state current state func update : %s : %d\n", __func__,__LINE__);
      data->current_state->leave_state(fsm, data->current_state, data->state_data);
   }

   data->platform->destroy_lock(data->lock);
   data->platform->free(data);
}


void openfsm_set_fsm_data(openfsm_t fsm, void *fsm_data)
{
   openfsm_data_t *data;

   data = (openfsm_data_t *)fsm;

   data->fsm_data = fsm_data;
}


void *openfsm_get_fsm_data(openfsm_t fsm)
{
   openfsm_data_t *data;

   data = (openfsm_data_t *)fsm;

   return data->fsm_data;
}


const openfsm_state_t *openfsm_get_current_state(openfsm_t fsm)
{
   openfsm_data_t *data;

   data = (openfsm_data_t *)fsm;

   return data->current_state;
}


int openfsm_start(openfsm_t fsm)
{
   openfsm_data_t *data;

   data = (openfsm_data_t *)fsm;

   /**
    * Simply enter the initial state
    */

   data->state_timer_count = 0;

   if (data->current_state->enter_state != NULL)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"MAC80211 : INFO : open fsm enter_state : %s : %d\n", __func__,__LINE__);
      return data->current_state->enter_state(fsm, data->current_state, data->state_data);
   }

   return 0;
}


int openfsm_set_state_data(openfsm_t fsm, void *state_data)
{
   openfsm_data_t *data;

   data = (openfsm_data_t *)fsm;

   data->state_data = state_data;

   return 0;
}


void *openfsm_get_state_data(openfsm_t fsm)
{
   openfsm_data_t *data;

   data = (openfsm_data_t *)fsm;

   return data->state_data;
}


int openfsm_transition(openfsm_t fsm, const openfsm_state_t *to_state)
{
   openfsm_data_t *data;

   data = (openfsm_data_t *)fsm;

   /**
    * Leave current state, and enter new state but
    * do not do its work
    */

   if (data->current_state->leave_state != NULL)
   {
      data->current_state->leave_state(fsm, data->current_state, data->state_data);
   }

   data->current_state     = to_state;
   data->state_timer_count = 0;

   if (data->current_state->enter_state != NULL)
   {
      return data->current_state->enter_state(fsm, data->current_state, data->state_data);
   }

   return 0;
}


int openfsm_timer(openfsm_t fsm)
{
   openfsm_data_t *data;

   data = (openfsm_data_t *)fsm;

   ++data->state_timer_count;

   if (data->current_state->timer != NULL)
   {
      return data->current_state->timer(fsm, data->current_state, data->state_data, data->state_timer_count);
   }

   return 0;
}


int openfsm_work(openfsm_t fsm, void *param1, void *param2)
{
   openfsm_data_t *data;

   data = (openfsm_data_t *)fsm;

   if (data->current_state->state_function != NULL)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"MAC80211 : INFO : open fsm state current state func update : %s : %d\n", __func__,__LINE__);
      return data->current_state->state_function(fsm, data->current_state, data->state_data, param1, param2);
   }

   return 0;
}


int openfsm_reset(openfsm_t fsm, const openfsm_state_t *to_state)
{
   openfsm_data_t *data;

   data = (openfsm_data_t *)fsm;

   data->current_state = to_state;

   return 0;
}
