/********************************************************************************
* MeshDynamics
* --------------
* File     : _meshap_sta_fsm_auth.c
* Comments : Atheros 802.11 FSM Auth state code
* Created  : 10/13/2004
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  2  |01/07/2008| Check for mixed mode added.                     |Prachiti|
* -----------------------------------------------------------------------------
* |  1  |4/6/2005  | Security changes                                | Sriram |
* -----------------------------------------------------------------------------
* |  0  |10/13/2004| Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

/**
 * This is a private source file included by meshap_sta_fsm.c, hence
 * the name begins with an underscore.
 * It was created to improve readability.
 */
void dump_bytes(char *a, int b);

#include <linux/version.h>
#if ( LINUX_VERSION_CODE == 199947 )
#define x86_ONLY
#endif

OPENFSM_ENTER_STATE_FUNCTION(auth)
{
   return 0;
}

OPENFSM_STATE_FUNCTION(auth)
{
    int err;
    _join_param_t          *param;
    _meshap_sta_fsm_data_t *data;
    meshap_instance_t      *instance;
    char q[64] = { '\0' };
    int  len   = 0;
    char *ie   = q;
    struct cfg80211_assoc_request     req;
    struct ieee80211_channel          *chan;
    struct wireless_dev               *wdev = NULL;
    struct cfg80211_registered_device *rdev = NULL;
    u16 freq = 0;
    unsigned char assoc_req_ie_in[256];         /* TODO : Change this to Dynamic Mem space */ 
    unsigned char *assoc_ie = &assoc_req_ie_in;
    unsigned char assoc_ie_len = 0;
    struct ieee80211_supported_band *sband = NULL;
    struct ieee80211_sta_ht_cap *ht_cap = NULL;
    struct ieee80211_ht_cap *ht_capa_mask = NULL;
    struct ieee80211_sta_vht_cap *vht_cap = NULL;
    struct ieee80211_sta_vht_cap *vht_capa_mask = NULL;
    struct wmm_information_element {
        /* Element ID: 221 (0xdd); Length: 7 */
        /* required fields for WMM version 1 */
        u8 oui[3]; /* 00:50:f2 */
        u8 oui_type; /* 2 */
        u8 oui_subtype; /* 0 */
        u8 version; /* 1 for WMM version 1.0 */
        u8 qos_info; /* AP/STA specific QoS info */

    } STRUCT_PACKED;

    struct wmm_information_element wmm_cap ;
    wmm_cap.oui[0] = 0x00 ;
    wmm_cap.oui[1] = 0x50 ;
    wmm_cap.oui[2] = 0xf2 ;
    wmm_cap.oui_type = WMM_OUI_TYPE ;
    wmm_cap.oui_subtype = WMM_OUI_SUBTYPE_INFORMATION_ELEMENT ;
    wmm_cap.version = WMM_VERSION ;
    wmm_cap.qos_info = 0x00   ;
    /**
     * The work of the AUTH state is to send the assoc request
     * and move into the ASSOC sent state
     */

    param    = (_join_param_t *)param1;
    data     = openfsm_get_fsm_data(fsm);
    instance = data->instance;

    wdev = instance->dev->ieee80211_ptr;

    rdev = WIPHY_TO_DEV(wdev->wiphy);

    al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"STA_FSM:FLOW: Enter %s<%d>\n",__func__,__LINE__);
    memset(&req, 0, sizeof(req));

    freq = ieee80211_channel_to_frequency(param->channel, IEEE80211_BAND_5GHZ);
    chan = ieee80211_get_channel(wdev->wiphy, freq);

    /*Update MAC80211 db*/

    /* Kinda ie for cfg apis to get going*/
    ie[0] = WLAN_EID_SSID;
    len   = strlen(param->essid);
    ie[1] = len;
    memcpy(&ie[2], param->essid, len);
    len += 2;

#ifdef x86_ONLY
    cfg80211_inform_bss(wdev->wiphy, chan, CFG80211_BSS_FTYPE_UNKNOWN, param->bssid,
#else 
	cfg80211_inform_bss(wdev->wiphy, chan, param->bssid,
#endif
            jiffies, instance->default_capability, 100, ie, len, 64, GFP_KERNEL);

    req.bss = cfg80211_get_bss(wdev->wiphy, chan, param->bssid, param->essid, param->essid_length,
#ifdef x86_ONLY
            IEEE80211_BSS_TYPE_ESS,
            IEEE80211_PRIVACY_ANY);
#else
    instance->default_capability, instance->default_capability);
#endif

    sband = wdev->wiphy->bands[IEEE80211_BAND_5GHZ];
    if (sband == NULL) {
        al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"STA_FSM:INFO: %s<%d> Unable to find 5GHz band info on %s\n",
		__func__, __LINE__, wdev->netdev->name);
    }else {
        ht_cap = &sband->ht_cap;


        /* Add HT capability IE only when Wireless Device and MDE configuration enables HT support */
        if (ht_cap->ht_supported && is_11n_supported(instance, 0)) {
            req.ht_capa.cap_info = cpu_to_le16(instance->ht_capab_info);

            req.ht_capa.ampdu_params_info &= ~0x3;
            req.ht_capa.ampdu_params_info |= instance->a_mpdu_params & 0x3;
            req.ht_capa.ampdu_params_info &= ~(0x1C);
            req.ht_capa.ampdu_params_info |= (ht_cap->ampdu_density << 2) & 0x1C;
            memcpy(&req.ht_capa.mcs, &ht_cap->mcs, sizeof(struct ieee80211_mcs_info)); //TODO: Validate size of the MCS set
            req.ht_capa.extended_ht_cap_info = 0;
            req.ht_capa.tx_BF_cap_info = 0;
            req.ht_capa.antenna_selection_info = 0;

            if (wdev->wiphy->ht_capa_mod_mask)  {
                ht_capa_mask = wdev->wiphy->ht_capa_mod_mask;
                req.ht_capa_mask.cap_info = ht_capa_mask->cap_info;
                req.ht_capa_mask.ampdu_params_info = ht_capa_mask->ampdu_params_info;
                memcpy(&req.ht_capa_mask.mcs, &ht_cap->mcs, sizeof(struct ieee80211_mcs_info)); //TODO: Validate size of the MCS set
                req.ht_capa_mask.extended_ht_cap_info = 0;
                req.ht_capa_mask.tx_BF_cap_info = 0;
                req.ht_capa_mask.antenna_selection_info = 0;
            }
        }
        if (is_11ac_supported(instance)){
            vht_cap = &sband->vht_cap;
            if(vht_cap->vht_supported){
                req.vht_capa.vht_cap_info = cpu_to_le32(instance->vht_capab_info);
                memcpy(&req.vht_capa.supp_mcs, &vht_cap->vht_mcs, sizeof(struct ieee80211_vht_mcs_info)); //TODO: Validate size of the MCS set

                if (wdev->wiphy->vht_capa_mod_mask)  {
                    req.vht_capa_mask.vht_cap_info =  wdev->wiphy->vht_capa_mod_mask->vht_cap_info;
                    memcpy(&req.vht_capa_mask.supp_mcs, &vht_cap->vht_mcs, sizeof(struct ieee80211_vht_mcs_info)); //TODO: Validate size of the MCS set
                }
            }
        }
    }

    memcpy((void *)&assoc_ie[assoc_ie_len], param->ie_in, param->ie_in_length);
    assoc_ie_len +=  param->ie_in_length;

    if (!(is_11n_supported(instance, 0)) && !( is_11ac_supported(instance))){
        req.flags |= ASSOC_REQ_DISABLE_HT | ASSOC_REQ_DISABLE_VHT ;
    }else if (!(is_11ac_supported(instance))){
        req.flags |= ASSOC_REQ_DISABLE_VHT ;
    }
    req.ie     = assoc_ie;
    req.ie_len = assoc_ie_len;

    if (req.bss != NULL)
    {
        ASSERT_WDEV_LOCK(wdev); /* 1408MIG */
        err = rdev->ops->assoc(wdev->wiphy, instance->dev, &req);
        if (!err) {
		    cfg80211_hold_bss(bss_from_pub(req.bss));
        }
        else {
            cfg80211_put_bss(wdev->wiphy, req.bss);
        }
    }

    openfsm_set_state_data(data->fsm, (void *)param);

    openfsm_transition(data->fsm, &_states[_MESHAP_STA_FSM_ASSOC_SENT]);
    al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"STA_FSM:FLOW: Exit %s<%d>\n",__func__,__LINE__);
    return 0;
}

OPENFSM_LEAVE_STATE_FUNCTION(auth)
{
   return 0;
}

OPENFSM_STATE_TIMER_FUNCTION(auth)
{
   return 0;
}
