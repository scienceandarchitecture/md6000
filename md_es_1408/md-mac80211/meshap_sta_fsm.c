/********************************************************************************
* MeshDynamics
* --------------
* File     : meshap_sta_fsm.c
* Comments : Atheros AR 5210/5211/5212 state machine
* Created  : 10/1/2004
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* | 18  |12/30/2008| Changes for I386 generic platform               | Sriram |
* -----------------------------------------------------------------------------
* | 17  |01/07/2008| Changes for mixed mode						  |Prachiti|
* -----------------------------------------------------------------------------
* | 16  |8/6/2007  | deauth_count added to instance                  | Sriram |
* -----------------------------------------------------------------------------
* | 15  |02/08/2006| Changes for Hide SSID                           | Sriram |
* -----------------------------------------------------------------------------
* | 14  |01/17/2006| Changes for PS mode and RX Frag support         | Sriram |
* -----------------------------------------------------------------------------
* | 13  |12/8/2005 | Changes for HAL 0.9.14.25                       | Sriram |
* -----------------------------------------------------------------------------
* | 12  |9/22/2005 | ESSID length checking added                     | Sriram |
* -----------------------------------------------------------------------------
* | 11  |8/20/2005 | Misc changes                                    | Sriram |
* -----------------------------------------------------------------------------
* | 10  |6/29/2005 | Misc changes                                    | Sriram |
* -----------------------------------------------------------------------------
* |  9  |6/11/2005 | Mesh init status detection implemented          | Sriram |
* -----------------------------------------------------------------------------
* |  8  |4/21/2005 | Misc changes                                    | Sriram |
* -----------------------------------------------------------------------------
* |  7  |4/12/2005 | Beacon miss logic changed                       | Sriram |
* -----------------------------------------------------------------------------
* |  6  |4/11/2005 | Changes after merging                           | Sriram |
* -----------------------------------------------------------------------------
* |  5  |4/6/2005  | Security changes                                | Sriram |
* -----------------------------------------------------------------------------
* |  4  |4/7/2005  | Changes for processing essid info               | Abhijit|
* -----------------------------------------------------------------------------
* |  3  |03/08/2005| Beacon Vendor Info added						  | Anand  |
* -----------------------------------------------------------------------------
* |  2  |1/7/2005  | Implemented get_last_beacon_time                | Sriram |
* -----------------------------------------------------------------------------
* |  1  |12/22/2004| Changes for beacon miss verification            | Sriram |
* -----------------------------------------------------------------------------
* |  0  |10/1/2004 | Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

#include <linux/version.h>

#if (LINUX_VERSION_CODE < KERNEL_VERSION(2, 6, 19))
#include <linux/config.h>
#else
#include <generated/autoconf.h>
#endif

#include <linux/version.h>
#include <linux/module.h>
#include <linux/init.h>
#include <linux/if.h>
#include <linux/netdevice.h>
#include <linux/skbuff.h>
#include <linux/cache.h>
#include <linux/pci.h>
#include <linux/wireless.h>
#include <asm/uaccess.h>
#include <net/iw_handler.h>
#include <linux/delay.h>
#include <linux/interrupt.h>
#include <linux/sched.h>
#include "meshap_instance.h"
#include "torna_wlan.h"
#include "meshap.h"
#include "al_conf.h"
#include "meshap_sta_fsm.h"
#include "meshap_mac80211.h"
#include <openfsm/openfsm-impl.h>
#include <openfsm/openfsm.h>

#include "torna_types.h"
#include "torna_mac_hdr.h"
#include "torna_wds.h"
#include "mesh_ng_scan.h"
#include "meshap_round_robin.h"

#define _MESHAP_MESH_INIT_STATUS_SSID           "MESH-INIT-"
#define _MESHAP_MESH_INIT_STATUS_SSID_LENGTH    10

#define _MESHAP_STA_FSM_DISCONNECTED            0
#define _MESHAP_STA_FSM_SEND_AUTH               1
#define _MESHAP_STA_FSM_AUTH_SENT               2
#define _MESHAP_STA_FSM_AUTH                    3
#define _MESHAP_STA_FSM_ASSOC_SENT              4
#define _MESHAP_STA_FSM_CONNECTED               5
#define _MESHAP_STA_FSM_ASCAN_1                 6
#define _MESHAP_STA_FSM_ASCAN_2                 7
#define _MESHAP_STA_FSM_PSCAN_1                 8

/**
 * The FSM state machine is triggered either by the timer function
 * or by one of the five functions meshap_sta_fsm_active_scan,
 * meshap_sta_fsm_passive_scan, meshap_sta_fsm_join,
 * meshap_sta_fsm_process_mgmt_frame and
 * meshap_sta_fsm_miss_beacon.
 */

struct _meshap_sta_fsm_data
{
   struct timer_list fsm_timer;
   openfsm_t         fsm;
   meshap_instance_t *instance;
   wait_queue_head_t wqh;
};

typedef struct _meshap_sta_fsm_data   _meshap_sta_fsm_data_t;

#define _SCAN_PARAM_MAX_RESULTS    64

typedef struct _scan_param            _scan_param_t;

struct _scan_param
{
   int                          dwell_timeout;
   meshap_sta_fsm_scan_result_t results[_SCAN_PARAM_MAX_RESULTS];
   int                          total_results;
};


void dump_bytes1(char *, int);
static void *_openfsm_platform_malloc(unsigned int size);
static void _openfsm_platform_free(void *memblock);
static unsigned int _openfsm_platform_create_lock(void);
static void _openfsm_platform_destroy_lock(unsigned int lock);

static void _meshap_sta_fsm_timer(unsigned long arg);
static int _set_next_channel(meshap_instance_t *instance, int start_at);

static inline void _process_beacon(meshap_instance_t *instance, struct sk_buff *skb);
static inline void _process_auth(meshap_instance_t *instance, struct sk_buff *skb);
static inline void _process_assoc_resp(meshap_instance_t *instance, struct sk_buff *skb);
static inline void _process_probe_resp(meshap_instance_t *instance, struct sk_buff *skb);
static inline void _process_deauth(meshap_instance_t *instance, struct sk_buff *skb);
static inline void _process_disassoc(meshap_instance_t *instance, struct sk_buff *skb);
static inline void _process_probe_request(meshap_instance_t *instance, struct sk_buff *skb);
static inline void _process_scan_results(meshap_instance_t *instance);


static const openfsm_platform_t _openfsm_platform =
{
   malloc                  :       _openfsm_platform_malloc,
   free                    :       _openfsm_platform_free,
   create_lock             :       _openfsm_platform_create_lock,
   destroy_lock    :       _openfsm_platform_destroy_lock
};

OPENFSM_DECLARE_STATE_FUNCTIONS(disconnected);
OPENFSM_DECLARE_STATE_FUNCTIONS(send_auth);
OPENFSM_DECLARE_STATE_FUNCTIONS(auth_sent);
OPENFSM_DECLARE_STATE_FUNCTIONS(auth);
OPENFSM_DECLARE_STATE_FUNCTIONS(assoc_sent);
OPENFSM_DECLARE_STATE_FUNCTIONS(connected);
OPENFSM_DECLARE_STATE_FUNCTIONS(ascan_1);
OPENFSM_DECLARE_STATE_FUNCTIONS(ascan_2);
OPENFSM_DECLARE_STATE_FUNCTIONS(pscan_1);

static const openfsm_state_t _states[] =
{
   OPENFSM_STATE(disconnected),
   OPENFSM_STATE(send_auth),
   OPENFSM_STATE(auth_sent),
   OPENFSM_STATE(auth),
   OPENFSM_STATE(assoc_sent),
   OPENFSM_STATE(connected),
   OPENFSM_STATE(ascan_1),
   OPENFSM_STATE(ascan_2),
   OPENFSM_STATE(pscan_1)
};

int meshap_sta_fsm_initialize(meshap_instance_t *instance)
{
   _meshap_sta_fsm_data_t *data;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Enter %s: %d\n", __func__, __LINE__);
   data           = (_meshap_sta_fsm_data_t *)kmalloc(sizeof(_meshap_sta_fsm_data_t), GFP_ATOMIC);
   data->fsm      = openfsm_create(&_openfsm_platform, &_states[_MESHAP_STA_FSM_DISCONNECTED]);
   data->instance = instance;

   openfsm_set_fsm_data(data->fsm, data);

   init_timer(&data->fsm_timer);

   data->fsm_timer.function = _meshap_sta_fsm_timer;
   data->fsm_timer.data     = (unsigned long)instance;

   instance->fsm_data  = (void *)data;
   instance->fsm_state = MESHAP_STA_FSM_STATE_STOPPED;

   init_waitqueue_head(&data->wqh);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Exit %s: %d\n", __func__, __LINE__);
   return 0;
}


int meshap_sta_fsm_uninitialize(meshap_instance_t *instance)
{
   _meshap_sta_fsm_data_t *data;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Enter %s: %d\n", __func__, __LINE__);

   data = (_meshap_sta_fsm_data_t *)instance->fsm_data;

   meshap_sta_fsm_stop(instance);

   openfsm_destroy(data->fsm);

   kfree(data);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Exit %s: %d\n", __func__, __LINE__);
   return 0;
}


int meshap_sta_fsm_start(meshap_instance_t *instance)
{
   _meshap_sta_fsm_data_t *data;

   data = (_meshap_sta_fsm_data_t *)instance->fsm_data;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Enter %s: %d\n", __func__, __LINE__);

   /**
    * In Mixed mode we start the FSM as though in INFRA mode
    */
#if 0
   if (instance->device_mode == MESHAP_DEV_MODE_MIXED)
   {
      data->fsm_timer.expires = jiffies + (MESHAP_STA_FSM_TIMER_INTERVAL * HZ / 1000);
      mod_timer(&data->fsm_timer, data->fsm_timer.expires);
      openfsm_start(data->fsm);
      instance->fsm_state = MESHAP_STA_FSM_STATE_DISCONNECTED;
      return 0;
   }
#endif

   /**
    * Assumption is that the iw_mode of the instance has
    * been set to the desired value. We startoff the timer
    * in STA mode and set the fsm_state in the instance to
    * DISCONNECTED. For AP mode we do not start the timer, and
    * directly set the fsm_state to RUNNING.
    */

   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MAC80211 : INFO : mode: %d %s: %d\n",instance->iw_mode,  __func__, __LINE__);
   switch (instance->iw_mode)
   {
   case IW_MODE_INFRA:
      data->fsm_timer.expires    = jiffies + (MESHAP_STA_FSM_TIMER_INTERVAL * HZ / 1000);
      data->fsm_timer.entry.next = NULL;
      //GPRASADH_RELAY_wlan1 reverting as per 1210 code, as its breaking IMX RELAY mode
      add_timer(&data->fsm_timer);
      openfsm_start(data->fsm);
      instance->fsm_state = MESHAP_STA_FSM_STATE_DISCONNECTED;
      break;

   case IW_MODE_MASTER:
      instance->fsm_state = MESHAP_STA_FSM_STATE_RUNNING;
      break;
   }
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Exit %s: %d\n", __func__, __LINE__);
   return 0;
}


int meshap_sta_fsm_stop(meshap_instance_t *instance)
{
   _meshap_sta_fsm_data_t *data;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Enter %s: %d\n", __func__, __LINE__);

   data = (_meshap_sta_fsm_data_t *)instance->fsm_data;

   if (instance->device_mode == MESHAP_DEV_MODE_MIXED)
   {
      /**
       * In mixed mode the FSM is always used, hence we only need
       * to check if it is already started.
       */

      if (instance->fsm_state != MESHAP_STA_FSM_STATE_STOPPED)
      {
         openfsm_reset(data->fsm, &_states[_MESHAP_STA_FSM_DISCONNECTED]);
         del_timer(&data->fsm_timer);
      }

      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MAC80211 : INFO : FSM is already started %s: %d\n", __func__, __LINE__);
      return 0;
   }

   if ((instance->fsm_state != MESHAP_STA_FSM_STATE_STOPPED) &&
       (instance->iw_mode == IW_MODE_INFRA))
   {
      openfsm_reset(data->fsm, &_states[_MESHAP_STA_FSM_DISCONNECTED]);
      del_timer(&data->fsm_timer);
   }

   instance->fsm_state = MESHAP_STA_FSM_STATE_STOPPED;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Exit %s: %d\n", __func__, __LINE__);

   return 0;
}


#include "_meshap_sta_fsm_active_scan.c"
#include "_meshap_sta_fsm_passive_scan.c"
#include "_meshap_sta_fsm_join.c"
#include "_meshap_sta_fsm_leave.c"

/**
 * Process management frames in STA mode
 */
int meshap_sta_fsm_process_mgmt_frame(meshap_instance_t *instance, struct sk_buff *skb)
{
   torna_mac_hdr_t *hdr;
   int             type;
   int             sub_type;

   hdr      = _meshap_get_torna_hdr(skb);
   type     = WLAN_FC_GET_TYPE(hdr->frame_control);
   sub_type = WLAN_FC_GET_STYPE(hdr->frame_control);

   if (type != WLAN_FC_TYPE_MGMT)
   {
     al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MAC80211 : ERROR : Packet is not of mgmt type %d %s: %d\n", type, __func__, __LINE__);
	  (tx_rx_pkt_stats.packet_drop[97])++;
      return -1;           /** TODO: Drop skb ?? */
   }
   switch (sub_type)
   {
   case WLAN_FC_STYPE_ASSOC_RESP:
      //printk(KERN_EMERG "case WLAN_FC_STYPE_ASSOC_RESP\n");
      _process_assoc_resp(instance, skb);
      break;

   case WLAN_FC_STYPE_PROBE_RESP:
      //printk(KERN_EMERG "case WLAN_FC_STYPE_PROBE_RESP\n");
      _process_probe_resp(instance, skb);
      break;

   case WLAN_FC_STYPE_BEACON:
      //printk(KERN_EMERG "case WLAN_FC_STYPE_BEACON\n");
      _process_beacon(instance, skb);
      break;

   case WLAN_FC_STYPE_DISASSOC:
      printk(KERN_EMERG "case WLAN_FC_STYPE_DISASSOC\n");
      _process_disassoc(instance, skb);
      break;

   case WLAN_FC_STYPE_AUTH:
      printk(KERN_EMERG "case WLAN_FC_STYPE_AUTH\n");
      _process_auth(instance, skb);
      break;

   case WLAN_FC_STYPE_DEAUTH:
      printk(KERN_EMERG "case WLAN_FC_STYPE_DEAUTH\n");
      _process_deauth(instance, skb);
      break;

   case WLAN_FC_STYPE_PROBE_REQ:
      //printk(KERN_EMERG "case WLAN_FC_STYPE_PROBE_REQ\n");
      if (instance->iw_mode == IW_MODE_MASTER)
      {
          if (instance->use_type == AL_CONF_IF_USE_TYPE_AP)
          {
              /*BUGFIX: for DualSSID problem, that both meshap and hostapd
                are processing Probe_Req() and here we are returning from
                meshap when the Req() is for AP node */
              /*printk(KERN_EMERG"%s: DualSSID Oh! we are about to process "
                "PROBE_REQ for WLAN2 let me return -1 \n", instance->dev->name);*/
	          (tx_rx_pkt_stats.packet_drop[123])++;
              return 0;
          }
         _process_probe_request(instance, skb);
      }
      break;

   default:
	  (tx_rx_pkt_stats.packet_drop[116])++;
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MAC80211 : ERROR : Invalid sub_type %s: %d\n", __func__, __LINE__);
      return -1;
   }

   return 0;
}


/**
 * Trigger to connected state in STA mode.
 */
int meshap_sta_fsm_miss_beacon(meshap_instance_t *instance)
{
   _meshap_sta_fsm_data_t *data;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Enter %s: %d\n", __func__, __LINE__);
   data = (_meshap_sta_fsm_data_t *)instance->fsm_data;

   if (openfsm_get_current_state(data->fsm) != &_states[_MESHAP_STA_FSM_CONNECTED])
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MAC80211 : ERROR : Received bmiss in disconnected state %s: %d\n", __func__, __LINE__);
      return -1;
   }

   /**
    * TODO: Call connected state work function and pass appropriate parameter
    */

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Exit %s: %d\n", __func__, __LINE__);
   return 0;
}


static void _meshap_sta_fsm_timer(unsigned long arg)
{
   meshap_instance_t      *instance;
   _meshap_sta_fsm_data_t *data;

   //al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Enter %s: %d\n", __func__, __LINE__);
   instance = (meshap_instance_t *)arg;
   data     = instance->fsm_data;
//RRREVIEW
//   data->fsm_timer.function = _meshap_sta_fsm_timer;
//   data->fsm_timer.data     = (unsigned long)instance;
	
   openfsm_timer(data->fsm);

   data->fsm_timer.expires = jiffies + (MESHAP_STA_FSM_TIMER_INTERVAL * HZ / 1000);
   add_timer(&data->fsm_timer);
   //al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Exit %s: %d\n", __func__, __LINE__);
}


static int _set_next_channel(meshap_instance_t *instance, int start_at)
{
   int i;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Enter %s: %d\n", __func__, __LINE__);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MAC80211 : INFO : channel_count: %d phy_mode: %d %s: %d\n", instance->channel_count, instance->current_phy_mode,  __func__, __LINE__);
   for (i = start_at + 1; i < instance->channel_count; i++)
   {
      if (instance->channels[i].frequency != 0)
      {
         switch (instance->current_phy_mode)
         {
         case WLAN_PHY_MODE_802_11_A:
            if (instance->channels[i].flags & WLAN_CHANNEL_5GHZ &&
                instance->channels[i].flags & WLAN_CHANNEL_OFDM &&
                !(instance->channels[i].flags & WLAN_CHANNEL_ATHEROS_TURBO_MODE))
            {
               instance->current_channel = &instance->channels[i];
               al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MAC80211 : INFO : index : %d %s: %d\n", i,  __func__, __LINE__);
               return i;
            }
            break;

         case WLAN_PHY_MODE_802_11_B:
            if ((instance->channels[i].flags & WLAN_CHANNEL_802_11_B) == WLAN_CHANNEL_802_11_B)
            {
               instance->current_channel = &instance->channels[i];
               al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MAC80211 : INFO : index : %d %s: %d\n", i,  __func__, __LINE__);
               return i;
            }
            break;

         case WLAN_PHY_MODE_802_11_PURE_G:
         case WLAN_PHY_MODE_802_11_G:
            if ((instance->channels[i].flags & WLAN_CHANNEL_802_11_G) == WLAN_CHANNEL_802_11_G)
            {
               instance->current_channel = &instance->channels[i];
               al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MAC80211 : INFO : index : %d %s: %d\n", i,  __func__, __LINE__);
               return i;
            }
            break;

         case WLAN_PHY_MODE_MESHAP_TURBO:
            if ((instance->channels[i].flags & WLAN_CHANNEL_MESHAP_TURBO) == WLAN_CHANNEL_MESHAP_TURBO)
            {
               instance->current_channel = &instance->channels[i];
               al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MAC80211 : INFO : index : %d %s: %d\n", i,  __func__, __LINE__);
               return i;
            }
            break;
         }
      }
   }

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Exit %s: %d\n", __func__, __LINE__);
   return -1;
}


int meshap_process_ds_beacon_update_jiffies(struct ieee80211_sub_if_data *sdata, struct sk_buff *skb)
{
    meshap_instance_t *instance = meshap_get_instance(skb->dev);
    struct ieee80211_rx_status *rxs;
    struct ieee80211_mgmt *mgmt;
    meshap_drv_data_t drv_data;
    int rate;
    int use_type;
    u16 fc;
    _mgmt_frame_ie_t ie;
    char           current_essid[WLAN_ESSID_MAX_SIZE + 1];


    if (NULL != instance) {
        use_type = instance->use_type;
        mgmt     = (struct ieee80211_mgmt *) skb->data;
        fc       = le16_to_cpu(mgmt->frame_control);
        ie.buffer        = (u8 *)mgmt->u.beacon.variable;
        ie.buffer_length = skb->len - WLAN_BEACON_SSID_POSITION;

        if((use_type == AL_CONF_IF_USE_TYPE_DS && ieee80211_is_beacon(fc))){
            if(instance->connected) {
                if (!memcmp(instance->current_bssid, mgmt->bssid, ETH_ALEN) && (instance->virtual_associated == 0))
                {
                    while (_get_next_ie(&ie) != -1)
                    {
                        if (ie.eid == WLAN_EID_SSID)
                        {
                            if (ie.length <= WLAN_ESSID_MAX_SIZE)
                            {
                                memcpy(current_essid, ie.data, ie.length);
                                current_essid[ie.length] = 0;
                                if (!memcmp(current_essid, _MESHAP_MESH_INIT_STATUS_SSID, _MESHAP_MESH_INIT_STATUS_SSID_LENGTH))
                                {
                                   /* Oh Yes trigger beacon miss */
                                    ieee80211_connection_loss(&sdata->vif);
                                    dev_kfree_skb(skb);
                                    tx_rx_pkt_stats.packet_drop[137]++;
                                    return -1;
                                }
                                else {
                                    ieee80211_sta_reset_beacon_monitor(sdata);
                                    return 1;
                                }
                            }
                        }

                    }
					 }
                }
            }
        }
    return 0;
}

/**
 * We process beacons only in the pscan1 or in connected state.
 */
static inline void _process_beacon(meshap_instance_t *instance, struct sk_buff *skb)
{
   _meshap_sta_fsm_data_t *data;
   unsigned char          *p;
   unsigned short         s;
   unsigned char          ssid_length;
   unsigned char          supported_rates_length;
   torna_mac_hdr_t        *hdr;
   int              i;
   _mgmt_frame_ie_t ie;
   unsigned long    flags;
   struct ieee80211_rx_status *rxs;
   unsigned char   output[256];

   data                   = (_meshap_sta_fsm_data_t *)instance->fsm_data;
   ssid_length            = 0;
   supported_rates_length = 0;

   if ((openfsm_get_current_state(data->fsm) != &_states[_MESHAP_STA_FSM_PSCAN_1]) &&
       (openfsm_get_current_state(data->fsm) != &_states[_MESHAP_STA_FSM_CONNECTED]))
   {
	  (tx_rx_pkt_stats.packet_drop[104])++;
      return;
   }

   OPENFSM_LOCK(data->fsm, flags);
   p     = skb->data;

   hdr = _meshap_get_torna_hdr(skb);
   rxs = IEEE80211_SKB_RXCB(skb);

   if (openfsm_get_current_state(data->fsm) == &_states[_MESHAP_STA_FSM_CONNECTED])
   {
      /**
       * Read out beacon interval if the beacon is from our AP
       */

      if (!memcmp(instance->current_bssid, hdr->addresses[1], ETH_ALEN) && (instance->virtual_associated == 0))
      {
         unsigned short capability;
         unsigned char  extended_rate_phy_info;
         char           current_essid[WLAN_ESSID_MAX_SIZE + 1];

         capability             = instance->capability;
         extended_rate_phy_info = instance->extended_rate_phy_info;

         memcpy(&s, &p[WLAN_BEACON_CAPABILITY_POSITION], 2);
         capability = le16_to_cpu(s);

         memcpy(&s, &p[WLAN_BEACON_BEACON_INTERVAL_POSITION], 2);

         if (instance->beacon_interval == 0)
         {
            instance->beacon_interval = le16_to_cpu(s);
            memcpy(&instance->beacon_timestamp, &p[WLAN_BEACON_TIMESTAMP_POSITION], 8);
         }

         /**
          * Check if the ESSID changed to MESH-INIT-xxx. In that case we do
          * not update the last_beacon_time, which in turn would make sure
          * a on_phy_link_notify is sent.
          */
         ie.buffer        = &p[WLAN_BEACON_SSID_POSITION];
         ie.buffer_length = skb->len - WLAN_BEACON_SSID_POSITION;

         while (_get_next_ie(&ie) != -1)
         {
            if (ie.eid == WLAN_EID_SSID)
            {
               if (ie.length <= WLAN_ESSID_MAX_SIZE)
               {
                  memcpy(current_essid, ie.data, ie.length);
                  current_essid[ie.length] = 0;
                  if (!memcmp(current_essid, _MESHAP_MESH_INIT_STATUS_SSID, _MESHAP_MESH_INIT_STATUS_SSID_LENGTH))
                  {
                     goto _out;
                  }
               }
            }
            else if (ie.eid == WLAN_EID_VENDOR_PRIVATE)
            {
               if (!memcmp(ie.data, meshap_round_robin_info_header, sizeof(meshap_round_robin_info_header)))
               {
                  if (!memcmp(ie.data + sizeof(meshap_round_robin_info_header), instance->dev->dev_addr, ETH_ALEN))
                  {
                     meshap_round_robin_notify_t hook = (meshap_round_robin_notify_t)instance->round_robin_hook;
                     if (hook != NULL)
                     {
                        hook(instance->dev, instance->dev_token);
                     }
                  }
               }
               else if (!memcmp(ie.data, _vendor_info_header, sizeof(_vendor_info_header)))
               {
                   memcpy(output, ie.data, ie.length);
                   mesh_ng_check_for_parent_state(output + sizeof(_vendor_info_header), ie.length - sizeof(_vendor_info_header));
               }
            }
         }

         instance->last_beacon_time = jiffies;
#if 0 //we are not calling from meshap
         if ((instance->current_phy_mode == WLAN_PHY_MODE_802_11_G) ||
             (instance->current_phy_mode == WLAN_PHY_MODE_802_11_PURE_G))
         {
            ie.buffer        = &p[WLAN_BEACON_SSID_POSITION];
            ie.buffer_length = skb->len - WLAN_BEACON_SSID_POSITION;

            while (_get_next_ie(&ie) != -1)
            {
               if (ie.eid == WLAN_EID_ERP_INFO)
               {
                  extended_rate_phy_info = ie.data[0];
                  break;
               }
            }
            if (instance->extended_rate_phy_info != extended_rate_phy_info)
            {
               instance->extended_rate_phy_info = extended_rate_phy_info;
            }
//MeshapTag
#if 0
            if (!(instance->current_hal_channel.channelFlags & CHANNEL_TURBO))
            {
               if (instance->capability & WLAN_CAPABILITY_SHORT_SLOT_TIME)
               {
                  if (!(capability & WLAN_CAPABILITY_SHORT_SLOT_TIME))
                  {
                     printk(KERN_INFO "%s: Setting slot time to 20us\n", instance->dev->name);
                     meshap_set_slot_time(instance, HAL_SLOT_TIME_20);
                     instance->capability &= ~WLAN_CAPABILITY_SHORT_SLOT_TIME;
                  }
               }
               else
               {
                  if ((capability & WLAN_CAPABILITY_SHORT_SLOT_TIME))
                  {
                     printk(KERN_INFO "%s: Setting slot time to 9us\n", instance->dev->name);
                     meshap_set_slot_time(instance, HAL_SLOT_TIME_20);
                     instance->capability |= WLAN_CAPABILITY_SHORT_SLOT_TIME;
                  }
               }
            }
#endif
            if (instance->capability & WLAN_CAPABILITY_SHORT_PREAMBLE)
            {
               if (!(capability & WLAN_CAPABILITY_SHORT_PREAMBLE) ||
                   instance->extended_rate_phy_info & WLAN_ERP_INFO_BARKER_PREAMBLE_MODE)
               {
                  instance->capability &= ~WLAN_CAPABILITY_SHORT_PREAMBLE;
               }
            }
            else
            {
               if ((capability & WLAN_CAPABILITY_SHORT_PREAMBLE) &&
                   !(instance->extended_rate_phy_info & WLAN_ERP_INFO_BARKER_PREAMBLE_MODE))
               {
                  instance->capability |= WLAN_CAPABILITY_SHORT_PREAMBLE;
               }
            }
         }
#endif
      }
      goto _out;
   }
_out:
   OPENFSM_UNLOCK(data->fsm, flags);
   //al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Exit %s: %d\n", __func__, __LINE__);
}


/**
 * We process probe responses only in the ascan2 state.
 */
static inline void _process_probe_resp(meshap_instance_t *instance, struct sk_buff *skb)
{
   _meshap_sta_fsm_data_t *data;
   unsigned char          *p;
   _scan_param_t          *param;
   unsigned short         s;
   unsigned char          ssid_length;
   unsigned char          supported_rates_length;
   torna_mac_hdr_t        *hdr;
   int              i;
   _mgmt_frame_ie_t ie;
   unsigned long    flags;

//   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Enter %s: %d\n", __func__, __LINE__);
   data                   = (_meshap_sta_fsm_data_t *)instance->fsm_data;
   ssid_length            = 0;
   supported_rates_length = 0;

   hdr = _meshap_get_torna_hdr(skb);

   if (openfsm_get_current_state(data->fsm) != &_states[_MESHAP_STA_FSM_ASCAN_2])
   {
	  (tx_rx_pkt_stats.packet_drop[102])++;
      return;
   }

   /**
    * After the introduction of duty cycle scan where we set promiscous mode,
    * we need to put a check on hdr->addresses[0]
    */

   if (memcmp(hdr->addresses[0], instance->dev->dev_addr, ETH_ALEN))
   {
	  (tx_rx_pkt_stats.packet_drop[103])++;
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MAC80211 :ERROR: %s is in promiscous mode %s: %d\n", instance->dev->name, __func__, __LINE__);
      return;
   }

   OPENFSM_LOCK(data->fsm, flags);

   param = openfsm_get_state_data(data->fsm);
   p     = skb->data;

   for (i = 0; i < param->total_results; i++)
   {
      if (!memcmp(hdr->addresses[1], param->results[i].bssid, ETH_ALEN))
      {
         goto _out;
      }
   }

   if (param->total_results >= _SCAN_PARAM_MAX_RESULTS)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MAC80211 : INFO : %s scan cache is full %s: %d\n", instance->dev->name, __func__, __LINE__);
      goto _out;
   }

   memset(&param->results[param->total_results], 0, sizeof(meshap_sta_fsm_scan_result_t));


   /** Beacon Interval */

   memcpy(&s, &p[WLAN_BEACON_BEACON_INTERVAL_POSITION], 2);
   param->results[param->total_results].beacon_interval = le16_to_cpu(s);

   /** Capability */

   memcpy(&s, &p[WLAN_BEACON_CAPABILITY_POSITION], 2);
   param->results[param->total_results].capability = le16_to_cpu(s);

   ie.buffer        = &p[WLAN_BEACON_SSID_POSITION];
   ie.buffer_length = skb->len - WLAN_BEACON_SSID_POSITION;

   param->results[param->total_results].channel = instance->current_channel->number;

   while (_get_next_ie(&ie) != -1)
   {
      switch (ie.eid)
      {
      case WLAN_EID_SSID:
         memset(param->results[param->total_results].ssid, 0, WLAN_ESSID_MAX_SIZE + 1);
         memcpy(param->results[param->total_results].ssid, ie.data, ie.length);
         break;

      case WLAN_EID_SUPP_RATES:
         memset(param->results[param->total_results].supported_rates, 0, WLAN_MAX_BIT_RATES);
         memcpy(param->results[param->total_results].supported_rates, ie.data, ie.length);
         supported_rates_length = ie.length;
         break;

      case WLAN_EID_DS_PARAMS:
         if (instance->current_phy_mode == WLAN_PHY_MODE_802_11_A)
         {
            param->results[param->total_results].channel = instance->current_channel->number;
         }
         else
         {
            if (ie.data[0] != instance->current_channel->number)
            {
               goto _out;
            }
         }
         break;

      case WLAN_EID_EX_SUPP_RATES:
         memcpy(param->results[param->total_results].supported_rates + supported_rates_length,
                ie.data,
                ie.length);
         break;
      }
   }

   /** BSSID */

   memcpy(param->results[param->total_results].bssid, hdr->addresses[1], ETH_ALEN);

   param->results[param->total_results].signal = hdr->rssi;

   ++param->total_results;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MAC80211 : INFO : %s AP Count : %d %s: %d\n", instance->dev->name, param->total_results, __func__, __LINE__);

_out:

   OPENFSM_UNLOCK(data->fsm, flags);
 //  al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Exit %s: %d\n", __func__, __LINE__);
}


/**
 * We process auth packets only in auth sent state
 */
static inline void _process_auth(meshap_instance_t *instance, struct sk_buff *skb)
{
   _meshap_sta_fsm_data_t *data;
   _join_param_t          *param;
   torna_mac_hdr_t        *hdr;
   unsigned short         s;
   unsigned char          *p;
   unsigned long          flags;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Enter %s: %d\n", __func__, __LINE__);
   data = (_meshap_sta_fsm_data_t *)instance->fsm_data;

   hdr = _meshap_get_torna_hdr(skb);

   if (openfsm_get_current_state(data->fsm) != &_states[_MESHAP_STA_FSM_AUTH_SENT])
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MAC80211 : INFO : %s: already in auth sent state %s: %d\n", instance->dev->name, __func__, __LINE__);
	  (tx_rx_pkt_stats.packet_drop[106])++;
      return;
   }

   /**
    * After the introduction of duty cycle scan where we set promiscous mode,
    * we need to put a check on hdr->addresses[0]
    */

   if (memcmp(hdr->addresses[0], instance->dev->dev_addr, ETH_ALEN))
   {
	  (tx_rx_pkt_stats.packet_drop[107])++;
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MAC80211 : INFO : %s: in promiscous mode %s: %d\n", instance->dev->name, __func__, __LINE__);
      return;
   }

   OPENFSM_LOCK(data->fsm, flags);

   /**
    * In the auth sent state, the state data is the join param.
    * We use that to compare it with the sender address
    */

   param = (_join_param_t *)openfsm_get_state_data(data->fsm);

   if (memcmp(param->bssid, hdr->addresses[1], ETH_ALEN))
   {
      /** Not from the AP that we sent the AUTH request to */
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MAC80211 : INFO : %s: Not from the AP that we sent to AUTH request %s: %d\n", instance->dev->name, __func__, __LINE__);
	  (tx_rx_pkt_stats.packet_drop[108])++;
      goto _out;
   }

   p = skb->data;

   /**
    * Now we dig into the AUTH packet to see if it is a negtive
    * response
    */

   memcpy(&s, &p[WLAN_AUTHENTICATION_ALGORITHM_POSITION], 2);
   s = le16_to_cpu(s);

   if (s != param->algorithm)
   {
      /** Algorithm mismatch */
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MAC80211 : INFO : %s: Drop, algorithm mismatch in auth resp %s: %d\n", instance->dev->name, __func__, __LINE__);
	  (tx_rx_pkt_stats.packet_drop[109])++;
      goto _out;
   }

   memcpy(&s, &p[WLAN_AUTHENTICATION_ATSN_POSITION], 2);
   s = le16_to_cpu(s);

   if (s != param->atsn + 1)
   {
      /** Algorithm mismatch */
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MAC80211 : INFO : %s Drop, Transaction number mismatch in auth resp %s: %d\n", instance->dev->name, __func__, __LINE__);
	  (tx_rx_pkt_stats.packet_drop[110])++;
      goto _out;
   }

   memcpy(&s, &p[WLAN_AUTHENTICATION_STATUS_POSITION], 2);
   s = le16_to_cpu(s);

   if (s != 0)
   {
      /**
       * We received a negative authentication response.
       * Wake up the sleeping thread and quit.
       */
      wake_up(&data->wqh);
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MAC80211 : INFO : %s Received negative authentication response from "MACSTR " status %d %s: %d\n", instance->dev->name, MAC2STR(param->bssid), s, __func__, __LINE__);
	  (tx_rx_pkt_stats.packet_drop[111])++;
      goto _out;
   }

   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MAC80211 : INFO : %s Authenticated with "MACSTR "%s: %d\n", instance->dev->name, MAC2STR(param->bssid), __func__, __LINE__);

   /**
    * We have received a possitive authentication response.
    * We now go into the AUTH state and do its work.
    * We pass our state data as parameter to the AUTH state.
    * The AUTH state sends the assoc request and trasitions
    * into the ASSOC sent state.
    */

   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MAC80211 : INFO : Changing state to _MESHAP_STA_FSM_AUTH %s: %d\n", __func__, __LINE__);
   openfsm_transition(data->fsm, &_states[_MESHAP_STA_FSM_AUTH]);

   OPENFSM_UNLOCK(data->fsm, flags);

   openfsm_work(data->fsm, param, NULL);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Exit %s: %d\n", __func__, __LINE__);
   return;

_out:

   OPENFSM_UNLOCK(data->fsm, flags);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Exit %s: %d\n", __func__, __LINE__);
}


/**
 * We process auth packets only in assoc sent state
 */
static inline void _process_assoc_resp(meshap_instance_t *instance, struct sk_buff *skb)
{
   _meshap_sta_fsm_data_t     *data;
   _join_param_t              *param;
   torna_mac_hdr_t            *hdr;
   unsigned short             s;
   unsigned char              supported_rates_length;
   unsigned char              *p;
   _mgmt_frame_ie_t           ie;
   unsigned char              found_md_ie;
   unsigned long              flags;
   static const unsigned char md_oui[] = { 0x00, 0x12, 0xCE };

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Enter %s: %d\n", __func__, __LINE__);
   data = (_meshap_sta_fsm_data_t *)instance->fsm_data;

   hdr = _meshap_get_torna_hdr(skb);

   if (openfsm_get_current_state(data->fsm) != &_states[_MESHAP_STA_FSM_ASSOC_SENT])
   {
      printk("%s: Ignoring Association response frame from "MACSTR "\n", instance->dev->name, MAC2STR(hdr->addresses[1]));
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MAC80211 : INFO : Assoc response in non assoc sent state %s: %d\n", __func__, __LINE__);
	  (tx_rx_pkt_stats.packet_drop[98])++;
      return;
   }

   /**
    * After the introduction of duty cycle scan where we set promiscous mode,
    * we need to put a check on hdr->addresses[0]
    */

   if (memcmp(hdr->addresses[0], instance->dev->dev_addr, ETH_ALEN))
   {
     al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MAC80211 : INFO : ASSOC resp in promiscous mode %s: %d\n", __func__, __LINE__);
	  (tx_rx_pkt_stats.packet_drop[99])++;
      return;
   }

   OPENFSM_LOCK(data->fsm, flags);

   /**
    * In the assoc sent state, the state data is the join param.
    * We use that to compare it with the sender address
    */

   param = (_join_param_t *)openfsm_get_state_data(data->fsm);

   if (memcmp(param->bssid, hdr->addresses[1], ETH_ALEN))
   {
      /** Not from the AP that we sent the ASSOC request to */
	  (tx_rx_pkt_stats.packet_drop[100])++;
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MAC80211 : INFO : Not the AP we sent ASSOC request to. %s: %d\n", __func__, __LINE__);
      goto _out;
   }

   p = skb->data;

   /**
    * Now we dig into the ASSOC packet to see if it is a negtive
    * response
    */

   memcpy(&s, &p[WLAN_ASSOCIATION_RESPONSE_STATUS_POSITION], 2);
   s = le16_to_cpu(s);

   if (s != 0)
   {
      /**
       * We received a negative association response.
       * Wake up the sleeping thread and quit.
       */
      wake_up(&data->wqh);
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MAC80211 : INFO : Received a negative association response. %s: %d\n", __func__, __LINE__);
	  (tx_rx_pkt_stats.packet_drop[101])++;
      goto _out;
   }

   /**
    * In Mixed mode the ESSID is copied onto current_essid_infra
    */


   strcpy(instance->current_essid, param->essid);
   instance->current_essid_length = param->essid_length;

   memcpy(instance->current_bssid, param->bssid, ETH_ALEN);

   /**
    * We have received a possitive association response.
    * We move onto the connected state, and wake up
    * the sleeping thread.
    */

   memcpy(&s, &p[WLAN_ASSOCIATION_RESPONSE_AID_POSITION], 2);
   param->aid = le16_to_cpu(s);

   if (p[WLAN_ASSOCIATION_RESPONSE_SUPPORTEDRATES_POSITION] == WLAN_EID_SUPP_RATES)
   {
      supported_rates_length = p[WLAN_ASSOCIATION_RESPONSE_SUPPORTEDRATES_POSITION + 1];
      memset(param->supported_rates, 0, sizeof(param->supported_rates));
      memcpy(param->supported_rates, &p[WLAN_ASSOCIATION_RESPONSE_SUPPORTEDRATES_POSITION + 2], supported_rates_length);
   }

   found_md_ie = 0;

   if (param->ie_out != NULL)
   {
      ie.buffer        = &p[WLAN_ASSOCIATION_RESPONSE_SUPPORTEDRATES_POSITION];
      ie.buffer_length = skb->len - WLAN_ASSOCIATION_RESPONSE_SUPPORTEDRATES_POSITION;

      while (_get_next_ie(&ie) != -1)
      {
         if ((ie.eid == WLAN_EID_VENDOR_PRIVATE) &&
             (ie.length >= 4) &&
             !memcmp(ie.data, md_oui, 3))
         {
            memcpy(param->ie_out, ie.data - 2, ie.length + 2);
            found_md_ie = 1;
            break;
         }
      }
   }

   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MAC80211 : INFO : _MESHAP_STA_FSM_CONNECTED %s: %d\n", __func__, __LINE__);
   openfsm_transition(data->fsm, &_states[_MESHAP_STA_FSM_CONNECTED]);

   instance->fsm_state = MESHAP_STA_FSM_STATE_RUNNING;

   /**
    * In MIXED mode we do not 0 the beacon interval as the MASTER mode\
    * needs to continue beaconing. Setting it to 0 makes the beacon
    * watchdog interrupt fire.
    */

   if (instance->device_mode != MESHAP_DEV_MODE_MIXED)
   {
      instance->beacon_interval = 0;
   }

   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MAC80211 : INFO : %s Associated with %s "MACSTR " %s: %d\n", instance->dev->name, found_md_ie ? "IMCP" : "", MAC2STR(param->bssid),  __func__, __LINE__);

   wake_up(&data->wqh);

   OPENFSM_UNLOCK(data->fsm, flags);

   meshap_on_link_notify(instance->dev,
                         instance->dev_token,
                         MESHAP_ON_LINK_NOTIFY_STATE_LINK_ON | MESHAP_ON_LINK_NOTIFY_STATE_ASSOCIATED);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Exit %s: %d\n", __func__, __LINE__);
   return;

_out:

   OPENFSM_UNLOCK(data->fsm, flags);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Exit %s: %d\n", __func__, __LINE__);
}


/**
 * We process deauth packets only in assoc sent,auth and connected states.
 */
static inline void _process_deauth(meshap_instance_t *instance, struct sk_buff *skb)
{
   _meshap_sta_fsm_data_t *data;
   const openfsm_state_t  *state;
   torna_mac_hdr_t        *hdr;
   unsigned long          flags;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Enter %s: %d\n", __func__, __LINE__);
   data = (_meshap_sta_fsm_data_t *)instance->fsm_data;

   OPENFSM_LOCK(data->fsm, flags);

   state = openfsm_get_current_state(data->fsm);

   hdr = _meshap_get_torna_hdr(skb);

   /**
    * After the introduction of duty cycle scan where we set promiscous mode,
    * we need to put a check on hdr->addresses[0]
    */

   if (memcmp(hdr->addresses[0], instance->dev->dev_addr, ETH_ALEN))
   {
	  (tx_rx_pkt_stats.packet_drop[112])++;
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MAC80211 : INFO : Deauth while duty cycle is in progress %s: %d\n", __func__, __LINE__);
      goto _out;
   }

   if (state == &_states[_MESHAP_STA_FSM_ASSOC_SENT])
   {
      /**
       * We are in the assoc sent state, waiting for an assoc response.
       * We shall treat the deauth as a negative assoc response, and wake up
       * the sleeping thread.
       */

      if (memcmp(instance->current_bssid, hdr->addresses[1], ETH_ALEN))
      {
         /** Not from the AP that we sent the ASSOC request to */
	     (tx_rx_pkt_stats.packet_drop[113])++;
        al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MAC80211 : INFO : Deauth received not from the AP which we sent an assoc request %s: %d\n", __func__, __LINE__);
         goto _out;
      }

      wake_up(&data->wqh);
   }
   else if (state == &_states[_MESHAP_STA_FSM_AUTH])
   {
      /**
       * We are in the auth state, we shall treat the deauth as a
       * negative assoc response, and wake up
       * the sleeping thread.
       */

      if (memcmp(instance->current_bssid, hdr->addresses[1], ETH_ALEN))
      {
         /** Not from the AP that we sent the ASSOC request to */
	     (tx_rx_pkt_stats.packet_drop[114])++;
        al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MAC80211 : INFO : Disassoc received in assoc sent state %s: %d\n", __func__, __LINE__);
         goto _out;
      }

      wake_up(&data->wqh);
   }
   else if ((state == &_states[_MESHAP_STA_FSM_CONNECTED]) && (instance->virtual_associated == 0))
   {
      if (!memcmp(instance->current_bssid, hdr->addresses[1], ETH_ALEN))
      {
         /**
          * We are fully connected, hence we need to inform meshap
          * about this incident.
          */

         instance->fsm_state = MESHAP_STA_FSM_STATE_DISCONNECTED;

         openfsm_reset(data->fsm, &_states[_MESHAP_STA_FSM_DISCONNECTED]);
         openfsm_set_state_data(data->fsm, NULL);
         instance->connected = 0;

         ++instance->deauth_count;

         al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MAC80211 : INFO : Received DEAUTH notification from "MACSTR" on iface: %s %s: %d\n", MAC2STR(instance->current_bssid), instance->dev->name, __func__, __LINE__);

         instance->last_on_phy_link_notify = jiffies;

         meshap_on_link_notify(instance->dev,
                               instance->dev_token,
                               MESHAP_ON_LINK_NOTIFY_STATE_LINK_OFF | MESHAP_ON_LINK_NOTIFY_STATE_DISASSOCIATED);
      }
   }

_out:

   OPENFSM_UNLOCK(data->fsm, flags);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Exit %s: %d\n", __func__, __LINE__);
}


/**
 * We process disassoc packets only in assoc sent and connected states
 */
static inline void _process_disassoc(meshap_instance_t *instance, struct sk_buff *skb)
{
   _meshap_sta_fsm_data_t *data;
   const openfsm_state_t  *state;
   torna_mac_hdr_t        *hdr;
   unsigned long          flags;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Enter %s: %d\n", __func__, __LINE__);

   data = (_meshap_sta_fsm_data_t *)instance->fsm_data;
//printk(KERN_EMERG "1602MIG :@@@ func : %s line : %d data:%p instance_name:%s instance_ptr:%p\n", __func__, __LINE__, data, instance->dev->name, instance);

   OPENFSM_LOCK(data->fsm, flags);

   state = openfsm_get_current_state(data->fsm);

   hdr = _meshap_get_torna_hdr(skb);

   /**
    * After the introduction of duty cycle scan where we set promiscous mode,
    * we need to put a check on hdr->addresses[0]
    */

   if (memcmp(hdr->addresses[0], instance->dev->dev_addr, ETH_ALEN))
   {
	  (tx_rx_pkt_stats.packet_drop[104])++;
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MAC80211 : INFO : Disassoc while duty cycle is in progress %s: %d\n", __func__, __LINE__);
      goto _out;
   }

   if (state == &_states[_MESHAP_STA_FSM_ASSOC_SENT])
   {
      /**
       * We are in the assoc sent state, waiting for an assoc response.
       * We shall treat the disassoc as a negative assoc response, and wake up
       * the sleeping thread.
       */

      if (memcmp(instance->current_bssid, hdr->addresses[1], ETH_ALEN))
      {
         /** Not from the AP that we sent the ASSOC request to */
	     (tx_rx_pkt_stats.packet_drop[105])++;
        al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MAC80211 : INFO : Disassoc received in assoc sent state %s: %d\n", __func__, __LINE__);
         goto _out;
      }

      wake_up(&data->wqh);
   }
   else if ((state == &_states[_MESHAP_STA_FSM_CONNECTED]) && (instance->virtual_associated == 0))
   {
      if (!memcmp(instance->current_bssid, hdr->addresses[1], ETH_ALEN))
      {
         /**
          * We are fully connected, hence we need to inform meshap
          * about this incident.
          */

         instance->fsm_state = MESHAP_STA_FSM_STATE_DISCONNECTED;

         openfsm_reset(data->fsm, &_states[_MESHAP_STA_FSM_DISCONNECTED]);
         openfsm_set_state_data(data->fsm, NULL);
         instance->connected = 0;

         ++instance->deauth_count;

         al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MAC80211 : INFO : Received DISASSOC notification from "MACSTR" on iface: %s %s: %d\n", MAC2STR(instance->current_bssid), instance->dev->name, __func__, __LINE__);
         instance->last_on_phy_link_notify = jiffies;

         meshap_on_link_notify(instance->dev,
                               instance->dev_token,
                               MESHAP_ON_LINK_NOTIFY_STATE_LINK_OFF | MESHAP_ON_LINK_NOTIFY_STATE_DISASSOCIATED);
      }
   }

_out:
   OPENFSM_UNLOCK(data->fsm, flags);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Exit %s: %d\n", __func__, __LINE__);
}


static inline void _process_probe_request(meshap_instance_t *instance, struct sk_buff *skb)
{
   struct ieee80211_mgmt *mgmt;
   int data_len = 0, ret = 0;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Enter %s: %d\n", __func__, __LINE__);
   ret = meshap_setup_mgmt_probe_response_pkt(skb, instance, &mgmt, &data_len);

   if (ret < 0)
   {
	 (tx_rx_pkt_stats.packet_drop[115])++;
      al_print_log(AL_CONTEXT AL_LOG_TYPE_DEBUG, "MAC80211 : DEBUG : Unable to setup probe response ret: %d %s: %d\n", ret, __func__, __LINE__);
      return;
   }
   data_len += sizeof(struct ieee80211_mgmt);
//1602MIG TEMPFIX for ProbeResp() extra 14bytes added
//   data_len -= 14;
   //printk(KERN_EMERG "data len = %d\n", data_len);
   meshap_mac80211_tx_mgmt_packet(instance, mgmt, data_len);
   kfree(mgmt);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Exit %s: %d\n", __func__, __LINE__);
}


#include "_meshap_sta_fsm_openfsm_platform.c"
#include "_meshap_sta_fsm_disconnected.c"
#include "_meshap_sta_fsm_send_auth.c"
#include "_meshap_sta_fsm_auth_sent.c"
#include "_meshap_sta_fsm_auth.c"
#include "_meshap_sta_fsm_assoc_sent.c"
#include "_meshap_sta_fsm_connected.c"
#include "_meshap_sta_fsm_ascan_1.c"
#include "_meshap_sta_fsm_ascan_2.c"
#include "_meshap_sta_fsm_pscan_1.c"

static inline void _process_scan_results(meshap_instance_t *instance)
{
  struct cfg80211_registered_device *rdev;
  struct cfg80211_internal_bss *bss;
  _meshap_sta_fsm_data_t *data;
  _scan_param_t          *param;
  const struct cfg80211_bss_ies *ies;
  const u8 *ie;
  int rem;
  struct wireless_dev    *wdev = instance->dev->ieee80211_ptr;
  unsigned long                     flags;
  unsigned char          supported_rates_length = 0;
  struct ieee80211_sub_if_data *sdata = IEEE80211_DEV_TO_SUB_IF(instance->dev);

  rdev = WIPHY_TO_DEV(wdev->wiphy);
  data = (_meshap_sta_fsm_data_t *)instance->fsm_data;

  OPENFSM_LOCK(data->fsm, flags);

  param = openfsm_get_state_data(data->fsm);

  if (param == NULL){
	goto _exit;
  }

  if (param->total_results >= _SCAN_PARAM_MAX_RESULTS){
	goto _exit;
  }

  spin_lock_bh(&rdev->bss_lock);

  list_for_each_entry(bss, &rdev->bss_list, list){

	rcu_read_lock();

	memset(&param->results[param->total_results], 0, sizeof(meshap_sta_fsm_scan_result_t));
	instance->current_channel = &instance->channels[ieee80211_frequency_to_channel(bss->pub.channel->center_freq)];
	param->results[param->total_results].channel = instance->current_channel->number;
	param->results[param->total_results].beacon_interval = bss->pub.beacon_interval;
	param->results[param->total_results].capability = bss->pub.capability;

	ies = rcu_dereference(bss->pub.ies);
	rem = ies->len;
	ie = ies->data;

	while (rem >= 2){
	  if (ie[1] > rem - 2)
	    break;

	  switch (ie[0]){
		 case WLAN_EID_SSID:
		   memset(param->results[param->total_results].ssid, 0, WLAN_ESSID_MAX_SIZE + 1);
			memcpy(param->results[param->total_results].ssid, ie + 2, ie[1]);
			break;
		 case WLAN_EID_SUPP_RATES:
			memset(param->results[param->total_results].supported_rates, 0, WLAN_MAX_BIT_RATES);
			memcpy(param->results[param->total_results].supported_rates, ie + 2, ie[1]);
			supported_rates_length = ie[1];
			break;
		 case WLAN_EID_DS_PARAMS:
			if (instance->current_phy_mode == WLAN_PHY_MODE_802_11_A){
				param->results[param->total_results].channel = instance->current_channel->number;
			}else {
				if ((ie + 2)[0] != instance->current_channel->number){
					goto _out;
				}
			}
			break;
		 case WLAN_EID_EX_SUPP_RATES:
			memcpy(param->results[param->total_results].supported_rates + supported_rates_length,ie + 2,ie[1]);
			break;
		 case WLAN_EID_HT_CAPABILITY:
			memcpy(&param->results[param->total_results].ap_ht_capab, ie + 2, ie[1]);
			break ;
		 case WLAN_EID_VHT_CAPABILITY:
			memcpy(&param->results[param->total_results].ap_vht_capab, ie + 2, ie[1]);
			break ;
		 case WLAN_EID_VENDOR_PRIVATE:
			memcpy(param->results[param->total_results].vendor_info, ie + 2, ie[1]);
			param->results[param->total_results].vendor_info_length = ie[1];
			break;
		}
		rem -= ie[1] + 2;
		ie += ie[1] + 2;
	}

	memcpy(param->results[param->total_results].bssid, bss->pub.bssid, ETH_ALEN);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_DEBUG, "MAC80211 : bssid %2x:%2x:%2x:%2x:%2x:%2x \n", param->results[param->total_results].bssid[0],
																															param->results[param->total_results].bssid[1],
																															param->results[param->total_results].bssid[2],
																															param->results[param->total_results].bssid[3],
																															param->results[param->total_results].bssid[4],
																															param->results[param->total_results].bssid[5]);
	param->results[param->total_results].signal = bss->pub.signal;
	++param->total_results;
_out:
  rcu_read_unlock();
 }
  spin_unlock_bh(&rdev->bss_lock);
_exit:
  OPENFSM_UNLOCK(data->fsm, flags);
}

/* Handle scan completed notification from mac80211 layer */
void meshap_notify_scan_completed(struct ieee80211_local *local)
{
	 _meshap_sta_fsm_data_t *data;
    al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Enter %s: %d\n", __func__, __LINE__);
    if (local && local->hw.instance)
    {
        if (local->hw.instance->scan_state == SCAN_SW_SCANNING)
        {
          data   = (_meshap_sta_fsm_data_t *)local->hw.instance->fsm_data;
          local->hw.instance->scan_state = SCAN_COMPLETED;
          local->hw.instance->scan_completion_time = jiffies - local->hw.instance->scan_start_time;
          al_print_log(AL_CONTEXT AL_LOG_TYPE_DEBUG, "MAC80211 : DEBUG : Scan completed time --%lu-- %s: %d\n", local->hw.instance->scan_completion_time, __func__, __LINE__);
          if (local->hw.instance->use_type == AL_CONF_IF_USE_TYPE_PMON)
            wake_up(&data->wqh);
        }
    }
    al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Exit %s: %d\n", __func__, __LINE__);
}
