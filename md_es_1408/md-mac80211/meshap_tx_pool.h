/********************************************************************************
* MeshDynamics
* --------------
* File     : meshap_tx_pool.h
* Comments : Atheros tx pool for TX
* Created  : 10/7/2004
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  2  |5/23/2007 | Changes for TX Fragmentation                    | Sriram |
* -----------------------------------------------------------------------------
* |  1  |1/11/2005 | Min/Max/Current added to TX Pool                | Sriram |
* -----------------------------------------------------------------------------
* |  0  |10/7/2004 | Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

#ifndef __MESHAP_TX_POOL_H__
#define __MESHAP_TX_POOL_H__

#define MESHAP_TX_FRAGMENT_TYPE_SINGLE      1
#define MESHAP_TX_FRAGMENT_TYPE_SEPERATE    2

#define MESHAP_TX_DESC_PER_BUFFER           10

struct meshap_tx_pool_item
{
   unsigned char              *data;
   dma_addr_t                 data_dma_addr;
   struct meshap_tx_pool_item *next;
   int                        buffer_size;
   unsigned char              fragment_type;                /* MESHAP_TX_FRAGMENT_TYPE_* */
   int                        fragment_count;
   struct
   {
      unsigned char *data;
      dma_addr_t    data_dma_addr;
      int           fragment_size;
   }
                              fragment_info[MESHAP_TX_DESC_PER_BUFFER];
};

typedef struct meshap_tx_pool_item   meshap_tx_pool_item_t;

struct meshap_tx_pool
{
   meshap_tx_pool_item_t *head;
   meshap_tx_pool_item_t *tail;
   int                   max;
   int                   min;
   int                   level;
};

typedef struct meshap_tx_pool   meshap_tx_pool_t;

#define MESHAP_TX_POOL_INIT(_p)    do { (_p)->head = NULL; (_p)->tail = NULL; } while (0)

struct meshap_instance;
#if 0
static inline void meshap_tx_pool_create(struct meshap_instance *instance, meshap_tx_pool_t *pool, int pool_count, int buffer_size)
{
   pool->head  = NULL;
   pool->tail  = NULL;
   pool->max   = pool_count;
   pool->level = pool_count;
   pool->min   = pool_count;

   while (pool_count--)
   {
      if (pool->head == NULL)
      {
         pool->head = (meshap_tx_pool_item_t *)kmalloc(sizeof(meshap_tx_pool_item_t), GFP_KERNEL);
         pool->tail = pool->head;
      }
      else
      {
         pool->tail->next = (meshap_tx_pool_item_t *)kmalloc(sizeof(meshap_tx_pool_item_t), GFP_KERNEL);
         pool->tail       = pool->tail->next;
      }
      pool->tail->next          = NULL;
      pool->tail->data          = (unsigned char *)kmalloc(buffer_size, GFP_KERNEL);
      pool->tail->buffer_size   = buffer_size;
      pool->tail->data_dma_addr = meshap_bus_map_single(instance->bus_dev,
                                                        pool->tail->data,
                                                        buffer_size,
                                                        MESHAP_BUS_DMA_TO_DEVICE);
   }
}


static inline void meshap_tx_pool_destroy(struct meshap_instance *instance, meshap_tx_pool_t *pool)
{
   pool->tail = pool->head;

   while (pool->tail != NULL)
   {
      meshap_bus_unmap_single(instance->bus_dev,
                              pool->tail->data_dma_addr,
                              pool->tail->buffer_size,
                              MESHAP_BUS_DMA_TO_DEVICE);
      kfree(pool->tail->data);
      pool->head = pool->tail->next;
      kfree(pool->tail);
      pool->tail = pool->head;
   }
}


static inline meshap_tx_pool_item_t *meshap_tx_pool_item_get(meshap_tx_pool_t *pool)
{
   meshap_tx_pool_item_t *ret;

   ret = pool->head;
   if (ret != NULL)
   {
      ret->fragment_count = 0;
      ret->fragment_type  = MESHAP_TX_FRAGMENT_TYPE_SINGLE;
      --pool->level;
      if (pool->level < pool->min)
      {
         pool->min = pool->level;
      }
      pool->head = pool->head->next;
      if (pool->head == NULL)
      {
         pool->tail = NULL;
      }
   }

   return ret;
}
#endif
static inline void meshap_tx_pool_item_release(meshap_tx_pool_t *pool, meshap_tx_pool_item_t *item)
{
   if (pool->tail == NULL)
   {
      pool->head = item;
      pool->tail = item;
   }
   else
   {
      pool->tail->next = item;
      pool->tail       = pool->tail->next;
   }
   pool->tail->next = NULL;

   ++pool->level;
}


#endif /*__MESHAP_TX_POOL_H__*/
