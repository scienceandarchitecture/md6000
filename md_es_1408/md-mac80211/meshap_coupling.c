#include <linux/kernel.h>
#include <meshap_instance.h>
#include "meshap_sta_fsm.h"
#include "meshap.h"
#include "al.h"
#include "al_conf.h"
#include "linux/ieee80211.h"
#include <../ieee80211_i.h>
#include "meshap_mac80211.h"
#include "meshap_round_robin.h"
#include <net/wireless/reg.h>
#include <net/wireless/core.h>
#include <openrt-ctrl/openrt-ctrl-impl.h>
#include <openobj/openobj.h>
#include <openrt-ctrl/openrt-ctrl.h>
#include <net/rtnetlink.h>

#include "md_minstrel_rt.h"

#define print_MAC(addr) printk("%s-line-%d addr = %02x:%02x:%02x:%02x:%02x:%02x\n", \
	__func__,__LINE__,addr[0],addr[1],addr[2],addr[3],addr[4],addr[5]);

//SPAWAR
#define WIPHY_PARAM_COVERAGE_CLASS	     (1 << 4)
#define AR5K_INIT_SLOT_TIME_DEFAULT		9
#define	AR5K_INIT_SIFS_DEFAULT_BG		10
#define	AR5K_INIT_SIFS_DEFAULT_A		16
#define	MIN_ACK_TIMEOUT_VALUE_BG		19
#define	MIN_ACK_TIMEOUT_VALUE_A			25
//SPAWAR_END

#if 1 //sowndarya validate changes
#define TDDI_802_11_IS_N(sub_type)\
        ((sub_type == AL_CONF_IF_PHY_SUB_TYPE_802_11_N_2_4GHZ) || (sub_type == AL_CONF_IF_PHY_SUB_TYPE_802_11_N_5GHZ)\
			|| (sub_type == AL_CONF_IF_PHY_SUB_TYPE_802_11_BGN) || (sub_type == AL_CONF_IF_PHY_SUB_TYPE_802_11_AN))

#define TDDI_802_11_IS_AC(sub_type)\
        ((sub_type == AL_CONF_IF_PHY_SUB_TYPE_802_11_AC) || (sub_type == AL_CONF_IF_PHY_SUB_TYPE_802_11_ANAC))

//#define AL_CONF_IF_PHY_SUB_TYPE_802_11_N      10
#define AL_CONF_IF_PHY_SUB_TYPE_802_11_AC     11
#define AL_CONF_IF_PHY_SUB_TYPE_802_11_BGN    12
#define AL_CONF_IF_PHY_SUB_TYPE_802_11_AN     13
#define AL_CONF_IF_PHY_SUB_TYPE_802_11_ANAC   14
#define AL_CONF_IF_PHY_SUB_TYPE_802_11_N_2_4GHZ      15
#define AL_CONF_IF_PHY_SUB_TYPE_802_11_N_5GHZ   16

#endif

#define FC_LEN                2
#define DURATION_ID_LEN       2
#define NO_OF_ADDRESS         3
#define SEQUENCE_CTL_LEN      2
#define WLAN_EID_ESSID        0
#define IW_ESSID_MAX_SIZE    32


extern int MESHAP_INSTANCE_DUTY_CYCLE_FLAG;

extern void dump_bytes(char *D, int count);
int meshap_sta_fsm_initialize(meshap_instance_t *instance);
int meshap_sta_fsm_uninitialize(meshap_instance_t *instance);

/* Function prototypes for meshap vectors*/

static int _meshap_mac80211_net_dev_set_slot_time_type(struct net_device *dev, int slot_time_type);

static int _meshap_mac80211_net_dev_get_slot_time_type(struct net_device *dev);

static int _meshap_mac80211_net_dev_set_tx_power(struct net_device *dev, meshap_tx_power_info_t *power_info);

static int _meshap_mac80211_net_dev_get_channel_count(struct net_device *dev);

static int _meshap_mac80211_net_dev_get_channel(struct net_device *dev);

static const unsigned char *_meshap_mac80211_net_dev_get_supported_rates(struct net_device *dev, int *length);

static const unsigned char *_meshap_mac80211_net_dev_get_extended_rates(struct net_device *dev, int *length);

static int _meshap_mac80211_net_dev_get_tx_power(struct net_device *dev, meshap_tx_power_info_t *power_info);

static int _meshap_mac80211_net_dev_set_phy_mode(struct net_device *dev, int phy_mode);

static int _meshap_mac80211_net_dev_get_phy_mode(struct net_device *dev);

static int _meshap_mac80211_net_dev_get_preamble_type(struct net_device *dev);

static int _meshap_mac80211_net_dev_set_preamble_type(struct net_device *dev, int preamble_type);

static int _meshap_mac80211_net_dev_get_hide_ssid(struct net_device *dev);

static int _meshap_mac80211_net_dev_set_hide_ssid(struct net_device *dev, unsigned char hide);

static int _meshap_mac80211_net_dev_set_essid_info(struct net_device *dev, int count, meshap_essid_info_t *info);

static int _meshap_mac80211_net_dev_set_ack_timeout(struct net_device *dev, unsigned short ack_timeout);

static int _meshap_mac80211_net_dev_set_channel(struct net_device *dev, int channel);

static int _meshap_mac80211_net_dev_set_beacon_interval(struct net_device *dev, int interval);

static int _meshap_mac80211_net_dev_set_mode(struct net_device *dev, int mode, unsigned char master_quiet);

static int _meshap_mac80211_net_dev_get_mode(struct net_device *dev);

static int _meshap_mac80211_net_dev_set_dev_token(struct net_device *dev, void *token);

static int _meshap_mac80211_net_dev_get_essid(struct net_device *dev, char *essid_buffer, int length);

static int _meshap_mac80211_net_dev_set_essid(struct net_device *dev, char *essid_buffer);

static int _meshap_mac80211_net_dev_set_rts_threshold(struct net_device *dev, int threshold);

static int _meshap_mac80211_net_dev_get_rts_threshold(struct net_device *dev);

static int _meshap_mac80211_net_dev_get_frag_threshold(struct net_device *dev);

static int _meshap_mac80211_net_dev_set_frag_threshold(struct net_device *dev, int threshold);

static void _meshap_mac80211_net_dev_set_mesh_downlink_round_robin_time(struct net_device *dev, int beacon_intervals);

static u64 _meshap_mac80211_net_dev_get_last_beacon_time(struct net_device *dev);

static int _meshap_mac80211_net_dev_disassociate(struct net_device *dev);

static int _meshap_mac80211_net_dev_set_hw_addr(struct net_device *dev, unsigned char *hw_addr);

static int _meshap_mac80211_net_dev_associate(struct net_device *dev, unsigned char *bssid, int channel, const char *essid,
                                              int length, int timeout, unsigned char *ie_in, int ie_in_length, unsigned char *ie_out);

static int _meshap_mac80211_net_dev_get_default_capabilities(struct net_device *dev);

static int _meshap_mac80211_net_dev_get_capabilities(struct net_device *dev);

static int _meshap_mac80211_net_dev_get_bssid(struct net_device *dev, unsigned char *bssid);

static int _meshap_mac80211_net_dev_scan_access_points_passive(struct net_device *dev,
                                                               meshap_scan_access_point_info_t **access_points, int *count,
                                                               int scan_duration_in_millis, int channel_count, unsigned char *channels);

static int _meshap_mac80211_net_dev_scan_access_points_active(struct net_device *dev,
                                                              meshap_scan_access_point_info_t **access_points, int *count,
                                                              int scan_duration_in_millis, int channel_count, unsigned char *channels);

static void _meshap_mac80211_net_dev_add_downlink_round_robin_child(struct net_device *dev,
                                                                    unsigned char     *addr);

static void _meshap_mac80211_net_dev_remove_downlink_round_robin_child(struct net_device *dev,
                                                                       unsigned char     *addr);

static int _meshap_mac80211_net_dev_virtual_associate(struct net_device *dev, unsigned char *ap_addr,
                                                      int channel, int start);

static int _meshap_mac80211_net_dev_get_duty_cycle_info(struct net_device        *dev,
                                                        int                      channel_count,
                                                        meshap_duty_cycle_info_t *info,
                                                        unsigned int             dwell_time_minis,
                                                        int flag);

static void _meshap_mac80211_net_dev_set_round_robin_notify_hook(struct net_device           *dev,
                                                                 meshap_round_robin_notify_t hook);

static void _meshap_mac80211_net_dev_enable_ds_verification_opertions(struct net_device *dev, int enable);

static int _meshap_mac80211_net_dev_get_beacon_interval(struct net_device *dev);

static int _meshap_mac80211_net_dev_get_erp_info(struct net_device *dev);

static int _meshap_mac80211_net_dev_set_erp_info(struct net_device *dev, int erp_info);

static int _meshap_mac80211_net_dev_set_beacon_vendor_info(struct net_device *dev, unsigned char *vendor_info_buffer, int length);

static int _meshap_mac80211_get_supported_channels(struct net_device *dev, meshap_channel_info_t **supported_channels, int *count);

static unsigned char _meshap_mac80211_get_device_type(struct net_device *dev);

static void _meshap_mac80211_set_device_type(struct net_device *dev, unsigned char device_type);

static int _meshap_mac80211_rate_init(struct net_device *dev);

static int _meshap_mac80211_start_openrt_thread(int duration);

static int _meshap_mac80211_stop_openrt_thread(void);

static void _meshap_mac80211_net_dev_get_rate_table(struct net_device *dev, meshap_rate_table_t *rate_table);

static void _meshap_mac80211_net_dev_reset_rate_ctrl(struct net_device *dev, unsigned char *addr);

static void _meshap_mac80211_net_dev_get_rate_ctrl_info(struct net_device *dev, unsigned char *addr, meshap_rate_ctrl_info_t *rate_ctrl_info);

static void _meshap_mac80211_net_dev_set_rate_control_parameters(struct net_device *dev, meshap_rate_ctrl_param_t *rate_ctrl_param);

static int meshap_get_rate(struct meshap_instance *instance, unsigned char *mac_addr, meshap_rate_ctrl_info_t *info);

static int _meshap_mac80211_net_dev_get_bit_rate(struct net_device *dev);

static int _meshap_mac80211_net_dev_set_bit_rate(struct net_device *dev, int bit_rate);
static int _meshap_mac80211_set_reg_domain_info(struct net_device *dev, meshap_reg_domain_info_t *reg_domain_info);

static int _meshap_mac80211_get_reg_domain_info(struct net_device *dev, meshap_reg_domain_info_t *reg_domain_info);
int meshap_mac80211_set_country_code(int country_code);

int _meshap_send_disassoc_from_station(struct net_device *dev, int flags);

static int _meshap_mac80211_set_ds_security_key(struct net_device *dev, unsigned char *
                                                group_key, unsigned char group_key_type, unsigned char group_key_index, unsigned char *pairwise_key, int
                                                enable_compression);
static int _meshap_mac80211_net_dev_set_security_key(struct net_device *dev, meshap_security_key_info_t *key_info, int enable_compression);

static int _meshap_mac80211_net_dev_enable_wep(struct net_device *dev);
static int _meshap_mac80211_net_dev_disable_wep(struct net_device *dev);
static int _meshap_mac80211_net_dev_set_rsn_ie(struct net_device *dev, unsigned char *rsn_ie, int rsn_ie_length);
static int _meshap_mac80211_release_security_key(struct net_device *dev, int index, const u8 *mac_addr);
static int _meshap_mac80211_get_security_key_data(struct net_device *dev, int
                                                  index, meshap_security_key_data_t *security_key_data, const u8 *mac_addr);

static void dummy_func(void);

static int _meshap_mac80211_net_dev_set_ht_capab(struct net_device *dev, al_net_if_ht_vht_capab_t* ht_vht_capab);
static int _meshap_mac80211_net_dev_set_ht_opera(struct net_device *dev, u16 ht_opera);

static int _meshap_mac80211_net_dev_set_vht_capab(struct net_device *dev, al_net_if_ht_vht_capab_t* ht_vht_capab);
static int _meshap_mac80211_net_dev_set_vht_opera(struct net_device *dev, meshap_802_11_vht_operation_t  vht_opera);
static int _meshap_mac80211_get_stats(struct net_device *dev, al_net_if_stat_t *stats);
static int _meshap_mac80211_get_debug_info(struct net_device *dev, debug_infra_info_t *debug_infra_info);

#if 1 //sowndarya validate changes
static int _meshap_mac80211_get_hw_support_ht_capab(struct net_device *dev,tddi_ht_cap_t *ht_cap,tddi_vht_cap_t *vht_cap,unsigned char sub_type);
#endif
static int _meshap_mac80211_set_reboot_in_progress_state(int value);
extern int reboot_in_progress;
static HAL_RATE_TABLE* meshap_setup_rate_table(meshap_instance_t *instance);
static int _meshap_mac80211_get_wm_duty_cycle_flag(struct net_device *dev, int *duty_cycle_flag, int *operating_channel);

static const struct _us_fcc_49_info
{
   unsigned char number;
   unsigned int  frequency;
}
_us_49_5_mhz[] =
{
   {  5, 4942 },
   { 15, 4947 },
   { 25, 4952 },
   { 35, 4957 },
   { 45, 4962 },
   { 55, 4967 },
   { 65, 4972 },
   { 75, 4977 },
   { 85, 4982 },
   { 95, 4987 },
}, _us_49_10_mhz[] =
{
   { 10, 4945 },
   { 20, 4950 },
   { 30, 4955 },
   { 40, 4960 },
   { 50, 4965 },
   { 60, 4970 },
   { 70, 4975 },
   { 80, 4980 },
   { 90, 4985 },
}, _us_49_20_mhz[] =
{
   { 20, 4950 },
   { 30, 4955 },
   { 40, 4960 },
   { 50, 4965 },
   { 60, 4970 },
   { 70, 4975 },
   { 80, 4980 },
};

unsigned char zeroed_mac_addr[] = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00};

typedef struct _meshap_duty_cycle_info{
    struct sk_buff *skb;
    struct net_device *ndev;
    int rssi;
    unsigned short rx_rate;
}_meshap_duty_cycle_info_t;
    
int no_of_aps;
int enqueue_pkts;
int DUTY_CYCLE_FLAG;

meshap_net_dev_t meshap_net_dev =
{
   encapsulation                       : MESHAP_NET_DEV_ENCAPSULATION_802_11,
   phy_hw_type                         : MESHAP_NET_DEV_PHY_HW_TYPE_802_11_A,
   set_hw_addr                         : _meshap_mac80211_net_dev_set_hw_addr,
   associate                           : _meshap_mac80211_net_dev_associate,
   dis_associate                       : _meshap_mac80211_net_dev_disassociate,
   get_bssid                           : _meshap_mac80211_net_dev_get_bssid,
   scan_access_points                  : _meshap_mac80211_net_dev_scan_access_points_passive,
   scan_access_points_active           : _meshap_mac80211_net_dev_scan_access_points_active,
   scan_access_points_passive          : _meshap_mac80211_net_dev_scan_access_points_passive,
   get_last_beacon_time                : _meshap_mac80211_net_dev_get_last_beacon_time,
   set_mesh_downlink_round_robin_time  : _meshap_mac80211_net_dev_set_mesh_downlink_round_robin_time,
   add_downlink_round_robin_child      : _meshap_mac80211_net_dev_add_downlink_round_robin_child,
   remove_downlink_round_robin_child   : _meshap_mac80211_net_dev_remove_downlink_round_robin_child,
   virtual_associate                   : _meshap_mac80211_net_dev_virtual_associate,
   get_duty_cycle_info                 : _meshap_mac80211_net_dev_get_duty_cycle_info,
   set_rate_ctrl_parameters            : _meshap_mac80211_net_dev_set_rate_control_parameters,
   reset_rate_ctrl                     : _meshap_mac80211_net_dev_reset_rate_ctrl,
   get_rate_ctrl_info                  : _meshap_mac80211_net_dev_get_rate_ctrl_info,
   set_round_robin_notify_hook         : _meshap_mac80211_net_dev_set_round_robin_notify_hook,
   enable_ds_verification_opertions    : _meshap_mac80211_net_dev_enable_ds_verification_opertions,
   dfs_scan                            : dummy_func,
   set_radar_notify_hook               : dummy_func,
   get_mode                            : _meshap_mac80211_net_dev_get_mode,
   set_mode                            : _meshap_mac80211_net_dev_set_mode,
   get_essid                           : _meshap_mac80211_net_dev_get_essid,
   set_essid                           : _meshap_mac80211_net_dev_set_essid,
   get_rts_threshold                   : _meshap_mac80211_net_dev_get_rts_threshold,
   set_rts_threshold                   : _meshap_mac80211_net_dev_set_rts_threshold,
   get_frag_threshold                  : _meshap_mac80211_net_dev_get_frag_threshold,
   set_frag_threshold                  : _meshap_mac80211_net_dev_set_frag_threshold,
   get_beacon_interval                 : _meshap_mac80211_net_dev_get_beacon_interval,
   set_beacon_interval                 : _meshap_mac80211_net_dev_set_beacon_interval,
   get_default_capabilities            : _meshap_mac80211_net_dev_get_default_capabilities,
   get_capabilities                    : _meshap_mac80211_net_dev_get_capabilities,
   get_slot_time_type                  : _meshap_mac80211_net_dev_get_slot_time_type,
   set_slot_time_type                  : _meshap_mac80211_net_dev_set_slot_time_type,
   get_erp_info                        : _meshap_mac80211_net_dev_get_erp_info,
   set_erp_info                        : _meshap_mac80211_net_dev_set_erp_info,
   set_beacon_vendor_info              : _meshap_mac80211_net_dev_set_beacon_vendor_info,
   enable_wep                          : _meshap_mac80211_net_dev_enable_wep,
   disable_wep                         : _meshap_mac80211_net_dev_disable_wep,
   set_rsn_ie                          : _meshap_mac80211_net_dev_set_rsn_ie,
   set_security_key                    : _meshap_mac80211_net_dev_set_security_key,
   release_security_key                : _meshap_mac80211_release_security_key,
   get_security_key_data               : _meshap_mac80211_get_security_key_data,
   set_ds_security_key                 : _meshap_mac80211_set_ds_security_key,
   get_supported_rates                 : _meshap_mac80211_net_dev_get_supported_rates,
   get_extended_rates                  : _meshap_mac80211_net_dev_get_extended_rates,
   get_bit_rate                        : _meshap_mac80211_net_dev_get_bit_rate,
   set_bit_rate                        : _meshap_mac80211_net_dev_set_bit_rate,
   get_rate_table                      : _meshap_mac80211_net_dev_get_rate_table,
   get_tx_power                        : _meshap_mac80211_net_dev_get_tx_power,
   set_tx_power                        : _meshap_mac80211_net_dev_set_tx_power,
   get_channel_count                   : _meshap_mac80211_net_dev_get_channel_count,
   get_channel                         : _meshap_mac80211_net_dev_get_channel,
   set_channel                         : _meshap_mac80211_net_dev_set_channel,
   get_phy_mode                        : _meshap_mac80211_net_dev_get_phy_mode,
   set_phy_mode                        : _meshap_mac80211_net_dev_set_phy_mode,
   get_preamble_type                   : _meshap_mac80211_net_dev_get_preamble_type,
   set_preamble_type                   : _meshap_mac80211_net_dev_set_preamble_type,
   set_dev_token                       : _meshap_mac80211_net_dev_set_dev_token,
   set_essid_info                      : _meshap_mac80211_net_dev_set_essid_info,
   get_ack_timeout                     : dummy_func,
   set_ack_timeout                     : _meshap_mac80211_net_dev_set_ack_timeout,
   get_reg_domain_info                 : _meshap_mac80211_get_reg_domain_info,
   set_reg_domain_info                 : _meshap_mac80211_set_reg_domain_info,
   get_supported_channels              : _meshap_mac80211_get_supported_channels,
   get_hide_ssid                       : _meshap_mac80211_net_dev_get_hide_ssid,
   set_hide_ssid                       : _meshap_mac80211_net_dev_set_hide_ssid,
   set_dot11e_category_info            : dummy_func,
   set_tx_antenna                      : dummy_func,
   set_radio_data                      : dummy_func,
   radio_diagnostic_command            : dummy_func,
   set_probe_request_notify_hook       : dummy_func,
   set_virtual_mode                    : dummy_func,
   set_device_type                     : _meshap_mac80211_set_device_type,
   get_device_type                     : _meshap_mac80211_get_device_type,
   initialize_mixed_mode               : dummy_func,
   enable_beaconing_uplink             : dummy_func,
   disable_beaconing_uplink            : dummy_func,
   set_action_hook                     : dummy_func,
   send_action                         : dummy_func,
   meshap_get_torna_hdr_cb             : _meshap_get_torna_hdr,
   meshap_get_drv_data                 : _meshap_get_drv_data,
   init_channels                       : _meshap_init_channels,
   meshap_get_tx_meshap_mac_hdr_cb     : _meshap_get_tx_meshap_mac_hdr,
   set_use_type                        : _meshap_set_use_type,
   set_all_conf_complete               : _meshap_set_all_conf_complete,
   send_assoc_response                 : _meshap_send_assoc_response,
   send_disassoc_from_station          : _meshap_send_disassoc_from_station,
   del_station                         : _meshap_del_station,
   send_deauth                         : _meshap_send_deauth,
   send_auth_response                  : _meshap_send_auth_response,
   meshap_rate_init                    : _meshap_mac80211_rate_init,
   meshap_start_openrt_thread          : _meshap_mac80211_start_openrt_thread,
   meshap_stop_openrt_thread           : _meshap_mac80211_stop_openrt_thread,
   get_hw_support_ht_capab             : _meshap_mac80211_get_hw_support_ht_capab,
   set_reboot_in_progress_state        : _meshap_mac80211_set_reboot_in_progress_state,
   set_ht_capab                        : _meshap_mac80211_net_dev_set_ht_capab,
   set_ht_opera                        : _meshap_mac80211_net_dev_set_ht_opera,
   set_vht_capab                       : _meshap_mac80211_net_dev_set_vht_capab,
   set_vht_operation                   : _meshap_mac80211_net_dev_set_vht_opera,
   get_stats                           : _meshap_mac80211_get_stats,
   get_debug_info                      : _meshap_mac80211_get_debug_info,
   get_wm_duty_cycle_flag              : _meshap_mac80211_get_wm_duty_cycle_flag,
};

static int _meshap_mac80211_rate_init(struct net_device *dev)
{
   meshap_instance_t *instance;
   struct ieee80211_sub_if_data *sdata = IEEE80211_DEV_TO_SUB_IF(dev);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Enter %s: %d\n", __func__, __LINE__);
   if (sdata->local == NULL)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MAC80211 : ERROR : local pointer is NULL %s: %d\n", __func__, __LINE__);
      return -1;
   }

   instance = meshap_get_instance(dev);
   if (NULL == instance)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MAC80211 : ERROR : instance is NULL %s: %d\n", __func__, __LINE__);
      return -1;
   }

   if (meshap_setup_rate_table(instance) == NULL)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MAC80211 : ERROR : Unable to setup rate table for %s %s: %d\n",instance->dev->name, __func__, __LINE__);
      return -1;
   }

/* HW_RC_SUPPORT - Start */
   if (IS_HW_HAS_RATE_CTRL(instance->rate_ctrl_flag)) {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MAC80211 : INFO : HW rate control flag is set for %s %s: %d\n",instance->dev->name, __func__, __LINE__);
       return 0;
   }
   else {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MAC80211 : INFO : set SW Rate control flag for %s %s: %d\n",instance->dev->name, __func__, __LINE__);
        instance->rate_ctrl_flag |= MESHAP_SW_BASED_RATE_CONTROL; 
   }
/* HW_RC_SUPPORT - End */

   if (instance->rate_ctrl == NULL || sdata->local->rate_ctrl->ops == NULL)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MAC80211 : ERROR : Rate control is NULL for %s %s: %d\n",instance->dev->name, __func__, __LINE__);
      return -1;
   }
   if (!strcmp(sdata->local->rate_ctrl->ops->name, "openrt_ctrl"))
       if (instance->rate_ctrl == NULL)
       {
          al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MAC80211 : INFO : Rate control is NULL for %s %s: %d\n",instance->dev->name, __func__, __LINE__);
          _openrt_ctrl_setup_rate_table(instance, 1, dev);
       }
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Exit %s: %d\n", __func__, __LINE__);
   return 0;
}


static int _meshap_mac80211_start_openrt_thread(int duration)
{
    /* TODO: Check this thread can be avoided in HW RTC */
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Enter %s: %d\n", __func__, __LINE__);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MAC80211 : INFO : duration: %d %s: %d\n", duration, __func__, __LINE__);
   open_rt_ctrl_initialize(duration);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Exit %s: %d\n", __func__, __LINE__);
   return 0;
}


static int _meshap_mac80211_stop_openrt_thread()
{
    /* TODO: Check this thread can be avoided in HW RTC */
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Enter %s: %d\n", __func__, __LINE__);
   open_rt_ctrl_uninitialize();
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Exit %s: %d\n", __func__, __LINE__);
   return 0;
}


static void _meshap_mac80211_net_dev_get_rate_table(struct net_device *dev, meshap_rate_table_t *rate_table)
{
   meshap_instance_t *instance;
   int               i;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Enter %s: %d\n", __func__, __LINE__);
   instance = meshap_get_instance(dev);
   if (NULL == instance)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MAC80211 : ERROR : instance is NULL %s: %d\n", __func__, __LINE__);
      return;
   }

	memset(rate_table,0,sizeof(meshap_rate_table_t));
   if (instance->current_rate_table == NULL)
   {	
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MAC80211 : ERROR : current_rate_table is NULL for %s %s: %d\n",instance->dev->name,  __func__, __LINE__);
      return;
   }

   rate_table->count = instance->current_rate_table->rateCount;
   for (i = 0; i < instance->current_rate_table->rateCount; i++)
   {
      rate_table->rates[i].index        = i;
      rate_table->rates[i].rate_in_mbps = ((instance->current_rate_table->info[i].rateKbps * 100) / 1000);
   }
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Exit %s: %d\n", __func__, __LINE__);
}


static void _meshap_mac80211_net_dev_reset_rate_ctrl(struct net_device *dev, unsigned char *addr)
{
   meshap_instance_t *instance;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Enter %s: %d\n", __func__, __LINE__);
   instance = meshap_get_instance(dev);
/*1602MIG */ print_MAC(addr);
   if (NULL == instance)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MAC80211 : ERROR : instance is NULL %s: %d\n", __func__, __LINE__);
      return;
   }
   open_rt_ctrl_reset_item(instance->rate_ctrl, addr);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Exit %s: %d\n", __func__, __LINE__);
}


static void _meshap_mac80211_net_dev_get_rate_ctrl_info(struct net_device *dev, unsigned char *addr, meshap_rate_ctrl_info_t *rate_ctrl_info)
{
   meshap_instance_t   *instance;
   //open_rt_ctrl_item_t *rate_ctrl_item;
   //int                 rate_index;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Enter %s: %d\n", __func__, __LINE__);
   instance = meshap_get_instance(dev);

   memset(rate_ctrl_info, 0, sizeof(meshap_rate_ctrl_info_t));

   meshap_get_rate(instance, addr, rate_ctrl_info);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Exit %s: %d\n", __func__, __LINE__);
}

/* HW_RC_SUPPORT - Start */
static inline int meshap_get_sta_rate_from_drv(struct ieee80211_local *local,
				                    u8 *sta_addr,
				                    struct meshap_rate_info *rate_info)
{
	int ret = -1;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Enter %s: %d\n", __func__, __LINE__);
	if (local->ops->get_sta_rate)
        ret = local->ops->get_sta_rate(&local->hw, sta_addr, rate_info);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Exit %s: %d\n", __func__, __LINE__);

	return ret;
}


int meshap_get_rate_from_hw(struct net_device *dev, unsigned char *mac_addr, meshap_rate_ctrl_info_t *rate_ctrl_info)
{
    struct meshap_rate_info rate_info;
	struct ieee80211_sub_if_data *sdata = IEEE80211_DEV_TO_SUB_IF(dev);
	struct ieee80211_local *local = sdata->local;
    int ret;
    int tx_rate_mbps = 0;

    al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Enter %s: %d\n", __func__, __LINE__);
    if (memcmp(zeroed_mac_addr, mac_addr, 6) == 0) {
        al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MAC80211 : ERROR : Invalid mac address %s: %d\n", __func__, __LINE__);
        goto exit_get_rate_from_hw;
    }
    memset(&rate_info, 0, sizeof(struct meshap_rate_info));                           
    ret = meshap_get_sta_rate_from_drv(local, mac_addr, &rate_info);
    tx_rate_mbps = rate_info.tx_rate_kbps/1000;

    if(!ret) {
        //printk("%s: tx_rate_in_mbps : %d, avg_rssi : %d, rx_rate_in_mbps : %d \n", __func__, (rate_info.tx_rate_kbps/1000), rate_info.avg_rssi, (rate_info.rx_rate_kbps/1000));
        if (rate_ctrl_info != NULL)
        {
            rate_ctrl_info->confidence   = 0; 
            rate_ctrl_info->outlook      = 0;
            rate_ctrl_info->rate_index   = 0;   /*TODO: Is it necessary to find corresponding rate index */
            rate_ctrl_info->rate_in_mbps = tx_rate_mbps;
            rate_ctrl_info->t_ch         = 0;
            rate_ctrl_info->t_elapsed    = 0;
            rate_ctrl_info->tx_error     = 0;
            rate_ctrl_info->tx_retry     = 0;
            rate_ctrl_info->tx_success   = 0;
            rate_ctrl_info->last_check   = 0;
            rate_ctrl_info->avg_ack_rssi = rate_info.avg_rssi;
        }
    }
    al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MAC80211 : INFO : tx_rate_mbps: %d %s: %d\n", tx_rate_mbps, __func__, __LINE__);
exit_get_rate_from_hw:
    al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Exit %s: %d\n", __func__, __LINE__);
    return tx_rate_mbps;
}
/* HW_RC_SUPPORT - End */

int meshap_get_rate(struct meshap_instance *instance, unsigned char *mac_addr, meshap_rate_ctrl_info_t *rate_ctrl_info)
{
   open_rt_ctrl_item_t *rate_ctrl_item;
   int                 rate_index;
   int                 tx_rate_mbps = 0;
   struct ieee80211_sub_if_data *sdata = IEEE80211_DEV_TO_SUB_IF(instance->dev);
   struct ieee80211_local *local = sdata->local;

/* HW_RC_SUPPORT - Start */
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Enter %s: %d\n", __func__, __LINE__);
   if (local == NULL) 
   {
       al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MAC80211 : ERROR : local pointer is NULL %s: %d\n", __func__, __LINE__);
       return 0;
   }

   if (IS_HW_HAS_RATE_CTRL(instance->rate_ctrl_flag)) { 
        if(instance->rate_ctrl != NULL) { 
           al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MAC80211 : INFO : OpenRT rate tables initialized through HW support rate cntrl for %s %s: %d\n", instance->dev->name, __func__, __LINE__);
            //return -1;
        }
        /* Get rate in Mpbs units from Hw/Fw */
        return meshap_get_rate_from_hw(instance->dev, mac_addr, rate_ctrl_info);
    }
/* HW_RC_SUPPORT - End */
   if (!strcmp(local->rate_ctrl->ops->name, "minstrel") || !strcmp(local->rate_ctrl->ops->name, "minstrel_ht"))
       tx_rate_mbps = mins_rt_ctrl_get_item(instance, mac_addr, rate_ctrl_info);
   if (!strcmp(local->rate_ctrl->ops->name, "openrt_ctrl"))
   {
      if(instance->rate_ctrl == NULL) {
         al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MAC80211 : ERROR : Rate control is not initialized for %s %s: %d\n", instance->dev->name, __func__, __LINE__);
         return 0;
      }
      rate_ctrl_item = open_rt_ctrl_get_item(instance->rate_ctrl, mac_addr, 0);
      if (rate_ctrl_item != NULL)
      {
         if ((instance->fixed_tx_rate_index == -1) ||
             (rate_ctrl_item->rate_index < instance->fixed_tx_rate_index))
         {
             rate_index = rate_ctrl_item->rate_index;
         }
         else
         {
             rate_index = instance->fixed_tx_rate_index;
         }
         
         tx_rate_mbps = (instance->current_rate_table->info[rate_index].rateKbps/10);

         if (rate_ctrl_info != NULL)
         {
             rate_ctrl_info->confidence   = rate_ctrl_item->C;
             rate_ctrl_info->outlook      = rate_ctrl_item->SGN;
             rate_ctrl_info->rate_index   = rate_index;
             rate_ctrl_info->rate_in_mbps = tx_rate_mbps;
             rate_ctrl_info->t_ch         = rate_ctrl_item->T_CH;
             rate_ctrl_info->t_elapsed    = rate_ctrl_item->T_EL;
             rate_ctrl_info->tx_error     = rate_ctrl_item->tx_error;
             rate_ctrl_info->tx_retry     = rate_ctrl_item->tx_retry;
             rate_ctrl_info->tx_success   = rate_ctrl_item->tx_success;
             rate_ctrl_info->last_check   = rate_ctrl_item->last_check;
             rate_ctrl_info->avg_ack_rssi = rate_ctrl_item->average_ack_rssi;
         }
         OPENOBJ_RELEASE_REFERENCE(rate_ctrl_item);
      }
      else
      {
          rate_index = 0;
      }
   }
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Exit %s: %d\n", __func__, __LINE__);
   return tx_rate_mbps;
}


int _meshap_mac80211_net_dev_get_bit_rate(struct net_device *dev)
{
   meshap_instance_t *instance;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Enter %s: %d\n", __func__, __LINE__);
   instance = meshap_get_instance(dev);
   if (NULL == instance)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MAC80211 : ERROR : instance is NULL %s: %d\n", __func__, __LINE__);
      return 0;
   }
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Exit %s: %d\n", __func__, __LINE__);
   return meshap_get_rate(instance, instance->current_bssid, NULL);
}


static int _meshap_mac80211_net_dev_set_bit_rate(struct net_device *dev, int bit_rate)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Enter %s: %d\n", __func__, __LINE__);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Exit %s: %d\n", __func__, __LINE__);
}

static void _meshap_mac80211_net_dev_set_rate_control_parameters(struct net_device *dev, meshap_rate_ctrl_param_t *rate_ctrl_param)
{
   meshap_instance_t *instance;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Enter %s: %d\n", __func__, __LINE__);
   instance = meshap_get_instance(dev);
   if (NULL == instance)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MAC80211 : ERROR : instance is NULL %s: %d\n", __func__, __LINE__);
      return;
   }
    if(instance->rate_ctrl == NULL) {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MAC80211 : ERROR : OPEN-RT-CTRL is NULL for %s %s: %d\n", instance->dev->name, __func__, __LINE__);
	    return;
	}


   open_rt_ctrl_start(instance->rate_ctrl,
                      rate_ctrl_param->t_el_max,
                      rate_ctrl_param->t_el_min,
                      rate_ctrl_param->qualification,
                      rate_ctrl_param->max_retry_percent,
                      rate_ctrl_param->max_error_percent,
                      rate_ctrl_param->max_errors,
                      rate_ctrl_param->max_retry_percent,
                      rate_ctrl_param->min_qualification,
                      rate_ctrl_param->initial_rate_index);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Exit %s: %d\n", __func__, __LINE__);
}

void *_meshap_get_drv_data(struct sk_buff *skb)
{
   struct ieee80211_rx_status *rxs = NULL;
   void *drv_data = NULL;
   rxs = IEEE80211_SKB_RXCB(skb);
   drv_data = (void*)rxs->drv_data;
   rxs->drv_data = NULL;
   return (void*)drv_data;
}

torna_mac_hdr_t *_meshap_get_torna_hdr(struct sk_buff *skb)
{
   struct ieee80211_rx_status *rxs = NULL;

   rxs = IEEE80211_SKB_RXCB(skb);
   rxs->drv_data->hdr.rssi      = rxs->drv_data->rs_rssi;
   rxs->drv_data->hdr.data_len  = rxs->drv_data->rs_datalen;
   rxs->drv_data->hdr.tx_rate   = rxs->drv_data->rs_rate;
   rxs->drv_data->hdr.key_index = rxs->drv_data->rs_keyix;
   return &rxs->drv_data->hdr;
}

meshap_mac_hdr_t *_meshap_get_tx_meshap_mac_hdr(struct sk_buff *skb)
{
   struct ieee80211_tx_info *info;

   info = IEEE80211_SKB_CB(skb);

   memset(info, 0, sizeof(*info));
/*Bz-99: Crash is observed when accessing torna hdr*/
   info->meshap_info.signature[0] = TORNA_MAC_HDR_SIGNATURE_1;
   info->meshap_info.signature[1] = TORNA_MAC_HDR_SIGNATURE_2;
   return &info->meshap_info.mac_hdr;
}


static int _meshap_mac80211_set_ds_security_key(struct net_device *dev, unsigned char *group_key, unsigned char group_key_type, unsigned char group_key_index, unsigned char *pairwise_key, int enable_compression)
{
   struct key_params                 *params;
   struct wireless_dev               *wdev;
   struct cfg80211_registered_device *rdev;
   struct ieee80211_sub_if_data      *sdata    = IEEE80211_DEV_TO_SUB_IF(dev);
   meshap_instance_t                 *instance;
   instance = meshap_get_instance(dev);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Enter %s: %d\n", __func__, __LINE__);
   if (NULL == instance)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MAC80211 : ERROR : Instance is NULL %s: %d\n", __func__, __LINE__);
      return -1;
   }
   if (instance->use_type == AL_CONF_IF_USE_TYPE_AP)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MAC80211 : INFO : For AP interface hostapd will handle this %s: %d\n", __func__, __LINE__);
      return 0;
   }
//1602MIG
   al_print_log(AL_CONTEXT AL_LOG_TYPE_DEBUG, "MAC80211 : DEBUG : Pairwise_key %s: %d\n", __func__, __LINE__);
   dump_bytes(pairwise_key, 16);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_DEBUG, "MAC80211 : DEBUG : Group_key %s: %d\n", __func__, __LINE__);
   dump_bytes(group_key, 16);

   wdev = instance->dev->ieee80211_ptr;

   params = (struct key_params *)kmalloc(sizeof(struct key_params), GFP_ATOMIC);

   if (params == NULL)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MAC80211 : ERROR : Instance is NULL %s: %d\n", __func__, __LINE__);
      return 0;
   }

   params->key = (u8 *)kmalloc(sizeof(u8) * 16, GFP_ATOMIC);
   params->seq = (u8 *)kmalloc(sizeof(u8) * 6, GFP_ATOMIC);

   if ((params->key == NULL) || (params->seq == NULL))
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MAC80211 : ERROR : Key or sequence is NULL %s: %d\n", __func__, __LINE__);
      return 0;
   }

   params->cipher = WLAN_CIPHER_SUITE_CCMP;
   memcpy(params->key, group_key, 16);
   memset(params->seq, '0', 6);
   params->key_len = 16;
   params->seq_len = 6;

   rdev = WIPHY_TO_DEV(wdev->wiphy);

   if (!rdev)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MAC80211 : ERROR : rdev is NULL %s: %d\n", __func__, __LINE__);
      return 0;
   }

   if (rdev->ops->add_key)
   {
//RELAYKEY:::
     al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MAC80211 : INFO : Setting GTK with gtk_index : %d %s: %d\n",group_key_index, __func__, __LINE__);
	  wdev_lock(dev->ieee80211_ptr); /* 1408MIG */
      rdev->ops->add_key(wdev->wiphy, dev, group_key_index, 0, NULL, params);
	  wdev_unlock(dev->ieee80211_ptr); /* 1408MIG */
	  wdev_lock(wdev); /* 1408MIG */
      rdev->ops->set_default_key(wdev->wiphy, dev, group_key_index, false, true);
	  wdev_unlock(wdev); /* 1408MIG */

      memcpy(params->key, pairwise_key, 16);

     al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MAC80211 : INFO : Setting PTK %s: %d\n", __func__, __LINE__);
	  wdev_lock(dev->ieee80211_ptr); /* 1408MIG */
      rdev->ops->add_key(wdev->wiphy, dev, 0, 1, instance->current_bssid, params);
	  wdev_unlock(dev->ieee80211_ptr); /* 1408MIG */
   }
   kfree(params->key);
   kfree(params->seq);
   kfree(params);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Exit %s: %d\n", __func__, __LINE__);
   return 0;
}


static int _meshap_mac80211_net_dev_set_security_key(struct net_device *dev, meshap_security_key_info_t *key_info, int enable_compression)
{
   struct key_params                 *params;
   meshap_instance_t                 *instance = meshap_get_instance(dev);
   struct wireless_dev               *wdev     = instance->dev->ieee80211_ptr;
   struct cfg80211_registered_device *rdev;
   struct ieee80211_sub_if_data      *sdata = IEEE80211_DEV_TO_SUB_IF(dev);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Enter %s: %d\n", __func__, __LINE__);
   if (NULL == instance)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MAC80211 : ERROR : Instance is NULL %s: %d\n", __func__, __LINE__);
      return 0;
   }
   if (instance->use_type == AL_CONF_IF_USE_TYPE_AP)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MAC80211 : INFO : For AP interface hostapd will handle this %s: %d\n", __func__, __LINE__);
      return 0;
   }
   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MAC80211 : INFO : Setting security key %s: %d\n", __func__, __LINE__);
   dump_bytes(key_info->key, 16);

   params = (struct key_params *)kmalloc(sizeof(struct key_params), GFP_ATOMIC);
   if (params == NULL)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MAC80211 : ERROR : Failed to allocate memory %s: %d\n", __func__, __LINE__);
      return 0;
   }
   memset(params, 0, sizeof(struct key_params));
   params->key = (u8 *)kmalloc(sizeof(u8) * 16, GFP_ATOMIC);

   if (params->key == NULL)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MAC80211 : ERROR : Failed to allocate memory %s: %d\n", __func__, __LINE__);
      return 0;
   }
   params->seq = (u8 *)kmalloc(sizeof(u8) * 6, GFP_ATOMIC);

   if (params->seq == NULL)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MAC80211 : ERROR : Failed to allocate memory %s: %d\n", __func__, __LINE__);
      return 0;
   }

   params->cipher = WLAN_CIPHER_SUITE_CCMP;
   memcpy(params->key, key_info->key, 16);
   memset(params->seq, 0, 6);
   params->key_len = 16;
   params->seq_len = 6;

   rdev = WIPHY_TO_DEV(wdev->wiphy);

   if (!rdev)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MAC80211 : ERROR : rdev is NULL %s: %d\n", __func__, __LINE__);
      return 0;
   }

   if (ADDR_IS_BROADCAST(key_info->destination))
   {
     al_print_log(AL_CONTEXT AL_LOG_TYPE_DEBUG, "MAC80211 : DEBUG : Setting GTK with gtk_index : %d for %s %s: %d\n", key_info->key_index, dev->name, __func__, __LINE__);
	  wdev_lock(dev->ieee80211_ptr); /* 1408MIG */
     rdev->ops->add_key(wdev->wiphy, dev, key_info->key_index, 0, NULL, params);
	  wdev_unlock(dev->ieee80211_ptr); /* 1408MIG */
	  wdev_lock(wdev); /* 1408MIG */
     rdev->ops->set_default_key(wdev->wiphy, dev, key_info->key_index, false, true);
	  wdev_unlock(wdev); /* 1408MIG */
   }
   else
   {
     al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MAC80211 : INFO : Setting PTK with ptk_idx : %d for %s %s: %d\n", key_info->key_index, dev->name, __func__, __LINE__);
     al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MAC80211 : INFO : STA_MAC "MACSTR " %s : %d\n", MAC2STR(key_info->destination), __func__, __LINE__);
	  wdev_lock(dev->ieee80211_ptr); /* 1408MIG */
      rdev->ops->add_key(wdev->wiphy, dev, key_info->key_index, 1, key_info->destination, params);
	  wdev_unlock(dev->ieee80211_ptr); /* 1408MIG */
   }

   kfree(params->key);
   kfree(params->seq);
   kfree(params);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Exit %s: %d\n", __func__, __LINE__);
   return 0;
}


static void dummy_func(void)
{
}


static int _meshap_mac80211_get_reg_domain_info(struct net_device *dev, meshap_reg_domain_info_t *reg_domain_info)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Enter %s: %d\n", __func__, __LINE__);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Exit %s: %d\n", __func__, __LINE__);
   return 0;
}


static int _meshap_mac80211_set_reg_domain_info(struct net_device *dev, meshap_reg_domain_info_t *reg_domain_info)
{
   int               ret;
   meshap_instance_t *instance;

   /**
    * Starting with the custom channel change, set_reg_domain_info
    * now assumes that it is called after a call to set_phy_mode.
    * Moreover it is not recommended to have a custom channel list
    * spanning accross the 2 and 5 bands. Only have either 2 GHz
    * or 5 GHz frequencies in the list.
    */

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Enter %s: %d\n", __func__, __LINE__);
   instance = meshap_get_instance(dev);
   if (NULL == instance)
   {
      printk(KERN_ERR "%s: Failed to get instance\n", __func__);
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MAC80211 : ERROR : instance is NULL %s: %d\n", __func__, __LINE__);
      return -1;
   }

   instance->country_code = reg_domain_info->country_code;
   if (instance->use_type == AL_CONF_IF_USE_TYPE_AP)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MAC80211 : INFO : For AP interface hostapd will handle this %s: %d\n", __func__, __LINE__);
      return 0;
   }
   ret = meshap_mac80211_set_country_code(instance->country_code);

   if (ret < 0)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MAC80211 : ERROR : %s: Failed to set country code %d %s: %d\n", dev->name, instance->country_code, __func__, __LINE__);
      return 0;
   }
   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MAC80211 : INFO : %s: Setting Country code to %d %s: %d\n", dev->name, instance->country_code, __func__, __LINE__);

   _meshap_init_channels(dev);

   meshap_reset(instance->dev, 1);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Exit %s: %d\n", __func__, __LINE__);
   return 0;
}


int meshap_mac80211_set_country_code(int country_code)
{
   const char *alpha2;
   int        ret;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Enter %s: %d\n", __func__, __LINE__);
   switch (country_code)
   {
   case 36:
      alpha2 = "AU";
      break;

   case 156:
      alpha2 = "CN";
      break;

   case 276:
      alpha2 = "DE";
      break;

   case 380:
      alpha2 = "IT";
      break;

   case 410:
      alpha2 = "KP";
      break;

   case 411:
      alpha2 = "KR";
      break;

   case 484:
      alpha2 = "MX";
      break;

   case 554:
      alpha2 = "NZ";
      break;

   case 408:
      alpha2 = "KP";
      break;

   case 724:
      alpha2 = "ES";
      break;

   case 826:
      alpha2 = "GB";
      break;

   case 840:
      alpha2 = "US";
      break;

   default:
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MAC80211 : INFO : Invalid CC : %d %s: %d\n", country_code, __func__, __LINE__);
      return -1;
   }
   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MAC80211 : INFO : %c %c %s: %d\n", alpha2[0], alpha2[1], __func__, __LINE__);

   ret = regulatory_hint_user(alpha2, 0);
 
   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MAC80211 : INFO : ret: %d %s: %d\n", ret, __func__, __LINE__);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Exit %s: %d\n", __func__, __LINE__);
   return ret;
}


static int _meshap_mac80211_net_dev_get_phy_mode(struct net_device *dev)
{
   meshap_instance_t *instance;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Enter %s: %d\n", __func__, __LINE__);
   instance = meshap_get_instance(dev);
   if (NULL == instance)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MAC80211 : ERROR : Instance is NULL %s: %d\n", __func__, __LINE__);
      return -1;
   }

   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MAC80211 : INFO : current_phy_mode: %d %s: %d\n", instance->current_phy_mode, __func__, __LINE__);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Exit %s: %d\n", __func__, __LINE__);
   return instance->current_phy_mode;
}


static int _meshap_mac80211_net_dev_set_phy_mode(struct net_device *dev, int phy_mode)
{
   int               i;
   int               orig_phy_mode;
   meshap_instance_t *instance;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Enter %s: %d\n", __func__, __LINE__);
   instance = meshap_get_instance(dev);
   if (NULL == instance)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MAC80211 : ERROR : Instance is NULL %s: %d\n", __func__, __LINE__);
      return -1;
   }

   /**
    * The PS phy_modes use WLAN_PHY_MODE_802_11_A internally.
    */

   orig_phy_mode = phy_mode;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MAC80211 : INFO : phy_mode : %d %s: %d\n", phy_mode, __func__, __LINE__);
   switch (phy_mode)
   {
   case WLAN_PHY_MODE_802_11_PSQ:
   case WLAN_PHY_MODE_802_11_PSH:
   case WLAN_PHY_MODE_802_11_PSF:
      phy_mode = WLAN_PHY_MODE_802_11_A;
   }
   if ((phy_mode >= WLAN_PHY_MODE_802_11_A) && (phy_mode <= WLAN_PHY_MODE_802_11_5GHZ_PURE_N) &&
       (instance->current_phy_mode != phy_mode))
   {
      switch (phy_mode)
      {
      case WLAN_PHY_MODE_802_11_A:
         if (!(instance->flags & MESHAP_INSTANCE_FLAGS_A_SUPPORTED))
         {
            al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MAC80211 : ERROR : phy_mode : %d is not supported for %s %s: %d\n", phy_mode, instance->dev->name, __func__, __LINE__);
            return -EINVAL;
         }
         break;

      case WLAN_PHY_MODE_802_11_B:
         if (!(instance->flags & MESHAP_INSTANCE_FLAGS_B_SUPPORTED))
         {
            al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MAC80211 : ERROR : phy_mode : %d is not supported for %s %s: %d\n", phy_mode, instance->dev->name, __func__, __LINE__);
            return -EINVAL;
         }
         break;

      case WLAN_PHY_MODE_802_11_G:
         if (!(instance->flags & MESHAP_INSTANCE_FLAGS_G_SUPPORTED))
         {
            al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MAC80211 : ERROR : phy_mode : %d is not supported for %s %s: %d\n", phy_mode, instance->dev->name, __func__, __LINE__);
            return -EINVAL;
         }
         break;

      case WLAN_PHY_MODE_802_11_PURE_G:
         if (!(instance->flags & MESHAP_INSTANCE_FLAGS_PURE_G_SUPPORTED))
         {
            al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MAC80211 : ERROR : phy_mode : %d is not supported for %s %s: %d\n", phy_mode, instance->dev->name, __func__, __LINE__);
            return -EINVAL;
         }
         break;
	  case WLAN_PHY_MODE_802_11_AN:
	  case WLAN_PHY_MODE_802_11_N:
	  case WLAN_PHY_MODE_802_11_BGN:
		 if (!(instance->flags & MESHAP_INSTANCE_FLAGS_N_SUPPORTED))
		 {
          al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MAC80211 : ERROR : phy_mode : %d is not supported for %s %s: %d\n", phy_mode, instance->dev->name, __func__, __LINE__);
			 return -EINVAL;
		 }
		 break;

	  case WLAN_PHY_MODE_802_11_2GHZ_PURE_N:
	  case WLAN_PHY_MODE_802_11_5GHZ_PURE_N:
		 if (!(instance->flags & MESHAP_INSTANCE_FLAGS_PURE_N_SUPPORTED))
		 {
          al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MAC80211 : ERROR : phy_mode : %d is not supported for %s %s: %d\n", phy_mode, instance->dev->name, __func__, __LINE__);
			 return -EINVAL;
		 }
		 break;

	  case WLAN_PHY_MODE_802_11_AC:
		 if (!(instance->flags & MESHAP_INSTANCE_FLAGS_AC_SUPPORTED))
		 {
          al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MAC80211 : ERROR : phy_mode : %d is not supported for %s %s: %d\n", phy_mode, instance->dev->name, __func__, __LINE__);
			 return -EINVAL;
		 }
		 break;

	  case WLAN_PHY_MODE_802_11_ANAC:
		 if (!(instance->flags & MESHAP_INSTANCE_FLAGS_ANAC_SUPPORTED))
		 {
          al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MAC80211 : ERROR : phy_mode : %d is not supported for %s %s: %d\n", phy_mode, instance->dev->name, __func__, __LINE__);
			 return -EINVAL;
		 }
		 break;
      }

      instance->current_phy_mode = phy_mode;
      for (i = 0; i < instance->channel_count; i++)
      {
         if (instance->channels[i].frequency != 0)
         {
            switch (instance->current_phy_mode)
            {
            case WLAN_PHY_MODE_802_11_A:
               instance->default_capability = WLAN_CAPABILITY_ESS;
               instance->capability         = WLAN_CAPABILITY_ESS;
               if ((instance->channels[i].flags & WLAN_CHANNEL_802_11_A) == WLAN_CHANNEL_802_11_A)
               {
                  ++instance->current_phy_mode_channel_count;
               }
               break;

            case WLAN_PHY_MODE_802_11_B:
               instance->default_capability = WLAN_CAPABILITY_ESS;
               instance->capability         = WLAN_CAPABILITY_ESS;
               if ((instance->channels[i].flags & WLAN_CHANNEL_802_11_B) == WLAN_CHANNEL_802_11_B)
               {
                  ++instance->current_phy_mode_channel_count;
               }
               break;

            case WLAN_PHY_MODE_802_11_G:
            case WLAN_PHY_MODE_802_11_PURE_G:
               instance->default_capability     = WLAN_CAPABILITY_ESS | WLAN_CAPABILITY_SHORT_PREAMBLE | WLAN_CAPABILITY_SHORT_SLOT_TIME;
               instance->capability             = WLAN_CAPABILITY_ESS | WLAN_CAPABILITY_SHORT_PREAMBLE | WLAN_CAPABILITY_SHORT_SLOT_TIME;
               instance->extended_rate_phy_info = WLAN_ERP_INFO_USE_PROTECTION;

               if ((instance->channels[i].flags & WLAN_CHANNEL_802_11_G) == WLAN_CHANNEL_802_11_G)
               {
                  ++instance->current_phy_mode_channel_count;
               }
               break;
			case WLAN_PHY_MODE_802_11_AN:
			case WLAN_PHY_MODE_802_11_N:
			case WLAN_PHY_MODE_802_11_5GHZ_PURE_N:
			   instance->default_capability     = WLAN_CAPABILITY_ESS;
			   instance->capability             = WLAN_CAPABILITY_ESS;

			   if ((instance->channels[i].flags & WLAN_CHANNEL_802_11_N) == WLAN_CHANNEL_802_11_N)
			   {
				   ++instance->current_phy_mode_channel_count;
			   }
			   break;
			case WLAN_PHY_MODE_802_11_BGN:
			case WLAN_PHY_MODE_802_11_2GHZ_PURE_N:
			   instance->default_capability     = WLAN_CAPABILITY_ESS | WLAN_CAPABILITY_SHORT_PREAMBLE | WLAN_CAPABILITY_SHORT_SLOT_TIME;
			   instance->capability             = WLAN_CAPABILITY_ESS | WLAN_CAPABILITY_SHORT_PREAMBLE | WLAN_CAPABILITY_SHORT_SLOT_TIME;
			   instance->extended_rate_phy_info = WLAN_ERP_INFO_USE_PROTECTION;

			   if ((instance->channels[i].flags & WLAN_CHANNEL_802_11_N) == WLAN_CHANNEL_802_11_N)
			   {
				   ++instance->current_phy_mode_channel_count;
			   }
			   break;
			case WLAN_PHY_MODE_802_11_AC:
			case WLAN_PHY_MODE_802_11_ANAC:
			   instance->default_capability     = WLAN_CAPABILITY_ESS;
			   instance->capability             = WLAN_CAPABILITY_ESS;

			   if ((instance->channels[i].flags & WLAN_CHANNEL_802_11_AC) == WLAN_CHANNEL_802_11_AC)
			   {
				   ++instance->current_phy_mode_channel_count;
			   }
			   break;
            }
         }
      }

      for (i = 0; i < instance->channel_count; i++)
      {
         if (instance->channels[i].frequency != 0)
         {
            switch (instance->current_phy_mode)
            {
            case WLAN_PHY_MODE_802_11_A:
               if ((instance->channels[i].flags & WLAN_CHANNEL_802_11_A) == WLAN_CHANNEL_802_11_A)
               {
                  goto _lb_change_mode;
               }
               break;

            case WLAN_PHY_MODE_802_11_B:
               if ((instance->channels[i].flags & WLAN_CHANNEL_802_11_B) == WLAN_CHANNEL_802_11_B)
               {
                  goto _lb_change_mode;
               }
               break;

            case WLAN_PHY_MODE_802_11_G:
            case WLAN_PHY_MODE_802_11_PURE_G:
               if ((instance->channels[i].flags & WLAN_CHANNEL_802_11_G) == WLAN_CHANNEL_802_11_G)
               {
                  goto _lb_change_mode;
               }
               break;
			case WLAN_PHY_MODE_802_11_AN:
			case WLAN_PHY_MODE_802_11_N:
			case WLAN_PHY_MODE_802_11_BGN:
			case WLAN_PHY_MODE_802_11_2GHZ_PURE_N:
			case WLAN_PHY_MODE_802_11_5GHZ_PURE_N:
			   if ((instance->channels[i].flags & WLAN_CHANNEL_802_11_N) == WLAN_CHANNEL_802_11_N)
			   {
				   goto _lb_change_mode;
			   }
               break;
			case WLAN_PHY_MODE_802_11_AC:
			case WLAN_PHY_MODE_802_11_ANAC:
			   if ((instance->channels[i].flags & WLAN_CHANNEL_802_11_AC) == WLAN_CHANNEL_802_11_AC)
			   {
				   goto _lb_change_mode;
			   }
               break;
            }
         }
      }
   }

   /**
    * Currently orig_phy_mode != phy_mode only occurs for US FCC 4.9 GHz modes.
    * When further modes get added, the condition below will have to be modified
    * appropriately.
    */

   if ((orig_phy_mode != phy_mode) && (instance->current_phy_mode == phy_mode))
   {
      goto _lb_49_mode;
   }

   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MAC80211 : INFO : current_phy_mode: %d phy_mode: %d %s: %d\n", instance->current_phy_mode, phy_mode, __func__, __LINE__);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Exit %s: %d\n", phy_mode, __func__, __LINE__);
   return (instance->current_phy_mode != phy_mode) ? -EINVAL : 0;

_lb_change_mode:

   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MAC80211 : INFO : iw_mode: %d %s: %d\n", instance->iw_mode, __func__, __LINE__);
   switch (instance->iw_mode)
   {
   case IW_MODE_MASTER:
      meshap_uninit_master_mode(instance);
      instance->current_channel = &instance->channels[i];
      meshap_reset(dev, 1);
      meshap_init_master_mode(instance);
      break;

   case IW_MODE_INFRA:
      meshap_uninit_infra_mode(instance);
      instance->current_channel = &instance->channels[i];
      meshap_reset(dev, 1);
      meshap_init_infra_mode(instance);
      break;

   case IW_MODE_MONITOR:
      instance->current_channel = &instance->channels[i];
      meshap_reset(dev, 1);
      break;
   }

   _setup_supported_rates(instance);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MAC80211 : INFO : %s: %d channels available %s: %d\n", instance->dev->name, instance->current_phy_mode_channel_count, __func__, __LINE__);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MAC80211 : INFO : %s: Supported rates %s : %d", instance->dev->name, __func__, __LINE__);
   for (i = 0; i < instance->supported_rates_length; i++)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MAC80211 : INFO : %d Mbps %s : %d", (instance->supported_rates[i] & instance->rate_mask) / 2, __func__, __LINE__);
   }

   if (instance->extended_rates_length > 0)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MAC80211 : INFO : %s: Supported rates %s : %d", instance->dev->name, __func__, __LINE__);
      for (i = 0; i < instance->extended_rates_length; i++)
      {
         al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MAC80211 : INFO : %d Mbps %s : %d", (instance->supported_rates[i] & instance->rate_mask) / 2, __func__, __LINE__);
      }
   }

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Exit %s: %d\n", __func__, __LINE__);
   return 0;

_lb_49_mode:

   /**
    * For US FCC 4.9 GHz we use custom channels as the HAL channels for country code 842
    * are not accurate.
    */

   //_set_us_49_channels(instance,orig_phy_mode);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Exit %s: %d\n", __func__, __LINE__);
   return 0;
}


static int _meshap_mac80211_net_dev_get_preamble_type(struct net_device *dev)
{
   meshap_instance_t *instance;
   int               ret;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Enter %s: %d\n", __func__, __LINE__);
   instance = meshap_get_instance(dev);
   if (NULL == instance)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MAC80211 : ERROR : Instance is NULL %s: %d\n", __func__, __LINE__);
      return -1;
   }

   if (instance->capability & WLAN_CAPABILITY_SHORT_PREAMBLE)
   {
      ret = WLAN_PREAMBLE_TYPE_SHORT;
   }
   else
   {
      ret = WLAN_PREAMBLE_TYPE_LONG;
   }

   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MAC80211 : INFO : ret %d %s: %d\n", ret, __func__, __LINE__);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Exit %s: %d\n", __func__, __LINE__);
   return ret;
}


static int _meshap_mac80211_net_dev_set_preamble_type(struct net_device *dev, int preamble_type)
{
   meshap_instance_t *instance;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Enter %s: %d\n", __func__, __LINE__);
   instance = meshap_get_instance(dev);
   if (NULL == instance)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MAC80211 : ERROR : Instance is NULL %s: %d\n", __func__, __LINE__);
      return -1;
   }
   if (instance->use_type == AL_CONF_IF_USE_TYPE_AP)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MAC80211 : INFO : For AP interface hostapd will handle this %s: %d\n", __func__, __LINE__);
      return 0;
   }

   if ((instance->current_phy_mode != WLAN_PHY_MODE_802_11_G) &&
       (instance->current_phy_mode != WLAN_PHY_MODE_802_11_PURE_G) &&
       (instance->current_phy_mode != WLAN_PHY_MODE_802_11_B))
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MAC80211 : ERROR : Current phy mode %d doesnot support this preamble_type %s: %d\n", instance->current_phy_mode, __func__, __LINE__);
      return -1;
   }

   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MAC80211 : INFO : preamble_type: %d %s: %d\n", preamble_type, __func__, __LINE__);
   switch (preamble_type)
   {
   case WLAN_PREAMBLE_TYPE_LONG:
      if (!(instance->capability & WLAN_CAPABILITY_SHORT_PREAMBLE))
      {
         return 0;
      }
      instance->capability &= ~WLAN_CAPABILITY_SHORT_PREAMBLE;
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MAC80211 : INFO : Using long preamble for %s %s: %d\n", instance->dev->name, __func__, __LINE__);
      break;

   case WLAN_PREAMBLE_TYPE_SHORT:
      if (instance->capability & WLAN_CAPABILITY_SHORT_PREAMBLE)
      {
         return 0;
      }
      instance->capability |= WLAN_CAPABILITY_SHORT_PREAMBLE;
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MAC80211 : INFO : Using short preamble for %s %s: %d\n", instance->dev->name, __func__, __LINE__);
      break;

   default:
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MAC80211 : ERROR : Invalid preamble type %s : %d\n", __func__, __LINE__);
      return -1;
   }

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Exit %s: %d\n", __func__, __LINE__);
   return 0;
}


static int _meshap_mac80211_net_dev_set_essid_info(struct net_device *dev, int count, meshap_essid_info_t *info)
{
   unsigned long     flags;
   meshap_instance_t *instance;
   int               i;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Enter %s: %d\n", __func__, __LINE__);
   instance = (meshap_instance_t *)netdev_priv(dev);
   if (NULL == instance)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MAC80211 : ERROR : Instance is NULL %s: %d\n", __func__, __LINE__);
      return -1;
   }
   if (instance->iw_mode != IW_MODE_MASTER)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MAC80211 : ERROR : interface not in master mode %s: %d\n", __func__, __LINE__);
      return 0;
   }

   MESHAP_INSTANCE_LOCK(instance, flags);

   if (instance->essid_info != NULL)
   {
      kfree(instance->essid_info);
   }

   instance->essid_info_count = count;

   instance->essid_info = (meshap_essid_info_t *)kmalloc(sizeof(meshap_essid_info_t) * count, GFP_ATOMIC);
   memset(instance->essid_info, 0, sizeof(meshap_essid_info_t) * count);

   for (i = 0; i < count; i++)
   {
      instance->essid_info[i].essid_length = strlen(info[i].essid);
      strcpy(instance->essid_info[i].essid, info[i].essid);
      instance->essid_info[i].vlan_tag = info[i].vlan_tag;

      if (info[i].sec_info.flags & MESHAP_ESSID_SECURITY_INFO_WEP_ENABLED)
      {
         instance->essid_info[i].sec_info.flags |= MESHAP_ESSID_SECURITY_INFO_WEP_ENABLED;
      }

      if (info[i].sec_info.flags & MESHAP_ESSID_SECURITY_INFO_RSN_ENABLED)
      {
         instance->essid_info[i].sec_info.flags        |= MESHAP_ESSID_SECURITY_INFO_RSN_ENABLED;
         instance->essid_info[i].sec_info.rsn_ie_length = info[i].sec_info.rsn_ie_length;
         memcpy(instance->essid_info[i].sec_info.rsn_ie_buffer,
                info[i].sec_info.rsn_ie_buffer,
                instance->essid_info[i].sec_info.rsn_ie_length);
      }
   }

   MESHAP_INSTANCE_UNLOCK(instance, flags);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Exit %s: %d\n", __func__, __LINE__);

   return 0;
}


static int _meshap_mac80211_net_dev_get_hide_ssid(struct net_device *dev)
{
   meshap_instance_t *instance;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Enter %s: %d\n", __func__, __LINE__);
   instance = meshap_get_instance(dev);
   if (NULL == instance)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MAC80211 : ERROR : Instance is NULL %s: %d\n", __func__, __LINE__);
      return -1;
   }

   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MAC80211 : INFO : instance->hide_essid: %d %s: %d\n", instance->hide_essid, __func__, __LINE__);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Enter %s: %d\n", __func__, __LINE__);
   return instance->hide_essid;
}


static int _meshap_mac80211_net_dev_set_hide_ssid(struct net_device *dev, unsigned char hide)
{
   meshap_instance_t *instance;
   unsigned long     flags;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Enter %s: %d\n", __func__, __LINE__);
   instance = meshap_get_instance(dev);
   if (NULL == instance)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MAC80211 : ERROR : Instance is NULL %s: %d\n", __func__, __LINE__);
      return 0;
   }

   MESHAP_INSTANCE_LOCK(instance, flags);
   instance->hide_essid = hide;
   MESHAP_INSTANCE_UNLOCK(instance, flags);
   if (instance->use_type == AL_CONF_IF_USE_TYPE_AP)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MAC80211 : INFO : For AP interface hostapd will handle this %s: %d\n", __func__, __LINE__);
      return 0;
   }

   if (instance->iw_mode == IW_MODE_MASTER && instance->beacon_conf_complete)
   {
       //VENDOR_INFO: its not needed here to take lock, as round_robin() is doing so
       meshap_setup_beacon_frame(instance);
   }

   if (hide)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MAC80211 : INFO : %s Hiding main ESSID %s: %d\n", dev->name, __func__, __LINE__);
   }
   else
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MAC80211 : INFO : %s Un-Hiding main ESSID %s: %d\n", dev->name, __func__, __LINE__);
   }

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Exit %s: %d\n", __func__, __LINE__);
   return 0;
}


static int _meshap_mac80211_net_dev_set_ack_timeout(struct net_device *dev, unsigned short ack_timeout)
{
//SPAWAR
	meshap_instance_t                 *instance;
	struct wireless_dev               *wdev;
	struct cfg80211_registered_device *rdev;
	int ret = 0;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Enter %s: %d\n", __func__, __LINE__);
	instance = meshap_get_instance(dev);
	if (NULL == instance)
	{
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MAC80211 : ERROR : Instance is NULL %s: %d\n", __func__, __LINE__);
		return -1;
	}
   if (instance->use_type == AL_CONF_IF_USE_TYPE_AP)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MAC80211 : INFO : For AP interface hostapd will handle this %s: %d\n", __func__, __LINE__);
      return 0;
   }
	wdev = instance->dev->ieee80211_ptr;

	rdev = WIPHY_TO_DEV(wdev->wiphy);

	if (!rdev)
	{
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MAC80211 : ERROR : rdev is NULL %s: %d\n", __func__, __LINE__);
		return -1;
	}
	if (rdev->ops->set_wiphy_params)
	{
		switch (instance->current_phy_mode) {
			case WLAN_PHY_MODE_802_11_A:
				if (ack_timeout > MIN_ACK_TIMEOUT_VALUE_A) {
					/*ack timeout value calculating as follow ack_timeout = (default_sifs + default_slot_time + 3 * coverage_class) in ath5k driver
					 *so find proper coverage_class value and pass the same for ath driver to set ack_timeout*/
					wdev->wiphy->coverage_class = (u8) ((ack_timeout - AR5K_INIT_SLOT_TIME_DEFAULT - AR5K_INIT_SIFS_DEFAULT_A)/3);
					ret = rdev->ops->set_wiphy_params(&rdev->wiphy, WIPHY_PARAM_COVERAGE_CLASS);
				}else {
               al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MAC80211 : INFO : user configured ack_timeout value is lower than 5Ghz ack_timout min value,so set value to min value 25usec %s: %d\n", __func__, __LINE__);
				}
				break;
			default:
				if (ack_timeout > MIN_ACK_TIMEOUT_VALUE_BG) {
					/*ack timeout value calculating as follow ack_timeout = (default_sifs + default_slot_time + 3 * coverage_class) in ath5k driver
					 *so find proper coverage_class value and pass the same for ath driver to set ack_timeout*/
					wdev->wiphy->coverage_class = (u8) ((ack_timeout - AR5K_INIT_SLOT_TIME_DEFAULT - AR5K_INIT_SIFS_DEFAULT_BG)/3);
					ret = rdev->ops->set_wiphy_params(&rdev->wiphy, WIPHY_PARAM_COVERAGE_CLASS);
				}else {
               al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MAC80211 : INFO : user configured ack_timeout value is lower than 2.4Ghz ack_timout min value,so set value to min value 19usec %s: %d\n", __func__, __LINE__);
				}
				break;
		}
	}
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Exit %s: %d\n", __func__, __LINE__);
	return ret;
//SPAWAR_END
}


static int _meshap_mac80211_net_dev_set_channel(struct net_device *dev, int channel)
{
   meshap_instance_t *instance = NULL;
   int ret;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Enter %s: %d\n", __func__, __LINE__);

   instance = meshap_get_instance(dev);
   if (NULL == instance)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MAC80211 : ERROR : Instance is NULL %s: %d\n", __func__, __LINE__);
      return 0;
   }
   if (instance->use_type == AL_CONF_IF_USE_TYPE_AP)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MAC80211 : INFO : For AP interface hostapd will handle this %s: %d\n", __func__, __LINE__);
      return 0;
   }

   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MAC80211 : INFO : %s: Setting channel to %d %s: %d\n", dev->name, channel,  __func__, __LINE__);

   ret = meshap_mac80211_dev_set_channel(instance, channel, 0);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Exit %s: %d\n", __func__, __LINE__);
   return ret;
}


static int _meshap_mac80211_net_dev_get_tx_power(struct net_device *dev, meshap_tx_power_info_t *power_info)
{
   meshap_instance_t *instance;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Enter %s: %d\n", __func__, __LINE__);
   instance = meshap_get_instance(dev);
   if (NULL == instance)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MAC80211 : ERROR : Instance is NULL %s: %d\n", __func__, __LINE__);
      return -1;
   }
   power_info->flags = MESHAP_NET_DEV_TXPOW_PERC;
   power_info->power = instance->tx_power;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MAC80211 : INFO : %s: power_info->power: %d %s: %d\n", dev->name, power_info->power,  __func__, __LINE__);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Exit %s: %d\n", __func__, __LINE__);
   return 0;
}


static const unsigned char *_meshap_mac80211_net_dev_get_supported_rates(struct net_device *dev, int *length)
{
   meshap_instance_t *instance;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Enter %s: %d\n", __func__, __LINE__);
   instance = meshap_get_instance(dev);
   if (NULL == instance)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MAC80211 : ERROR : Instance is NULL %s: %d\n", __func__, __LINE__);
      return 0;
   }

   *length = instance->supported_rates_length;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Exit %s: %d\n", __func__, __LINE__);
   return instance->supported_rates;
}


static const unsigned char *_meshap_mac80211_net_dev_get_extended_rates(struct net_device *dev, int *length)
{
   meshap_instance_t *instance;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Enter %s: %d\n", __func__, __LINE__);
   instance = meshap_get_instance(dev);
   if (NULL == instance)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MAC80211 : ERROR : Instance is NULL %s: %d\n", __func__, __LINE__);
      return 0;
   }

   *length = instance->extended_rates_length;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Exit %s: %d\n", __func__, __LINE__);
   return instance->extended_rates;
}


static int _meshap_mac80211_net_dev_set_tx_power(struct net_device *dev, meshap_tx_power_info_t *power_info)
{
   struct wireless_dev *wdev = dev->ieee80211_ptr;
   meshap_instance_t   *instance;
   int                 index;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Enter %s: %d\n", __func__, __LINE__);
   instance = meshap_get_instance(dev);
   if (NULL == instance)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MAC80211 : ERROR : Instance is NULL %s: %d\n", __func__, __LINE__);
      return -1;
   }
   if (instance->use_type == AL_CONF_IF_USE_TYPE_AP)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MAC80211 : INFO : For AP interface hostapd will handle this %s: %d\n", __func__, __LINE__);
      return 0;
   }

   /**
    * Convert from percentage to a scale from 0-63
    *
    */

   index = power_info->power * 64;
   index = index / 100;
   index = index - 1;    /* Bring it between 0 and 63 */

   if (index < 0)
   {
      index = 0;
   }
   if (index > 63)
   {
      index = 63;
   }

   instance->tx_power = index;

   //ath_hal_settxpowlimit(instance->ah,instance->tx_power);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MAC80211 : INFO : %s: Setting TX Power index to %d %s: %d\n", dev->name, index, __func__, __LINE__);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Exit %s: %d\n", __func__, __LINE__);

   return 0;
}


static int _meshap_mac80211_net_dev_get_channel_count(struct net_device *dev)
{
   meshap_instance_t *instance;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Enter %s: %d\n", __func__, __LINE__);
   instance = meshap_get_instance(dev);
   if (NULL == instance)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MAC80211 : ERROR : Instance is NULL %s: %d\n", __func__, __LINE__);
      return -1;
   }

   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MAC80211 : INFO : %s: chan_count: %d %s: %d\n", instance->dev->name, instance->current_phy_mode_channel_count, __func__, __LINE__);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Exit %s: %d\n", __func__, __LINE__);
   return instance->current_phy_mode_channel_count;
}


static int _meshap_mac80211_net_dev_get_channel(struct net_device *dev)
{
   meshap_instance_t *instance;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Enter %s: %d\n", __func__, __LINE__);
   instance = meshap_get_instance(dev);
   if (NULL == instance)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MAC80211 : ERROR : Instance is NULL %s: %d\n", __func__, __LINE__);
      return 0;
   }

   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MAC80211 : INFO : %s: channel: %d %s: %d\n", instance->dev->name, instance->current_channel->number, __func__, __LINE__);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Exit %s: %d\n", __func__, __LINE__);
   return instance->current_channel->number;
}


static int _meshap_mac80211_net_dev_set_beacon_interval(struct net_device *dev, int interval)
{
   meshap_instance_t *instance;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Enter %s: %d\n", __func__, __LINE__);
   instance = meshap_get_instance(dev);
   if (NULL == instance)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MAC80211 : ERROR : Instance is NULL %s: %d\n", __func__, __LINE__);
      return -1;
   }

#if 1 /*LFRS*/
   if (instance->use_type == AL_CONF_IF_USE_TYPE_AP)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MAC80211 : INFO : For AP interface hostapd will handle this %s: %d\n", __func__, __LINE__);
      return 0;
   }
#endif

   if (instance->beacon_interval == interval)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MAC80211 : INFO : Same beacon interval is set %s: %d\n", __func__, __LINE__);
      return 0;
   }
   instance->beacon_interval = interval;

   if (instance->iw_mode == IW_MODE_MASTER)
   {
      meshap_uninit_master_mode(instance);
   }

   meshap_reset(dev, 0);

   if (instance->iw_mode == IW_MODE_MASTER)
   {
      meshap_init_master_mode(instance);
   }
   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MAC80211 : INFO : %s: Setting Beacon interval to %d %s: %d\n", dev->name, interval, __func__, __LINE__);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Exit %s: %d\n", __func__, __LINE__);
   return 0;
}


static int _meshap_mac80211_net_dev_get_frag_threshold(struct net_device *dev)
{
   meshap_instance_t *instance;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Enter %s: %d\n", __func__, __LINE__);
   //Locate the instance structure
   instance = meshap_get_instance(dev);
   if (NULL == instance)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MAC80211 : ERROR : Instance is NULL %s: %d\n", __func__, __LINE__);
      return -1;
   }
   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MAC80211 : INFO : %s: Frag threshold %d %s: %d\n", dev->name, instance->frag_threshold, __func__, __LINE__);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Exit %s: %d\n", __func__, __LINE__);
   return instance->frag_threshold;
}


static int _meshap_mac80211_net_dev_set_frag_threshold(struct net_device *dev, int threshold)
{
   meshap_instance_t *instance;
   struct wireless_dev               *wdev;
   struct cfg80211_registered_device *rdev;
   int ret; 

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Enter %s: %d\n", __func__, __LINE__);
   //Locate the instance structure
   instance = meshap_get_instance(dev);

   if (NULL == instance)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MAC80211 : ERROR : Instance is NULL %s: %d\n", __func__, __LINE__);
      return -1;
   }
   if (instance->use_type == AL_CONF_IF_USE_TYPE_AP)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MAC80211 : INFO : For AP interface hostapd will handle this %s: %d\n", __func__, __LINE__);
      return 0;
   }

   if (threshold < 160)
   {
      threshold = 160;
   }

   instance->frag_threshold = threshold;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MAC80211 : INFO : %s: Frag threshold %d %s: %d\n", dev->name, instance->frag_threshold, __func__, __LINE__);

   wdev = instance->dev->ieee80211_ptr;
   if (NULL == wdev) {
       al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MAC80211 : ERROR : wdev is NULL %s: %d\n", __func__, __LINE__);
       return -1;
   }

   rdev = WIPHY_TO_DEV(wdev->wiphy);

   if (rdev->ops->set_wiphy_params){
       wdev->wiphy->frag_threshold = instance->frag_threshold;
       ret = rdev->ops->set_wiphy_params(&rdev->wiphy, WIPHY_PARAM_FRAG_THRESHOLD);
       if (ret)
       {
           al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MAC80211 : ERROR : Failed to set frag threshold %s: %d\n", __func__, __LINE__);
       }
   }

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Exit %s: %d\n", __func__, __LINE__);
   return 0;
}


static int _meshap_mac80211_net_dev_get_rts_threshold(struct net_device *dev)
{
   meshap_instance_t *instance;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Enter %s: %d\n", __func__, __LINE__);
   instance = meshap_get_instance(dev);
   if (NULL == instance)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MAC80211 : ERROR : Instance is NULL %s: %d\n", __func__, __LINE__);
      return -1;
   }

   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MAC80211 : INFO : instance->rts_threshold : %d %s: %d\n", instance->rts_threshold,  __func__, __LINE__);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Exit %s: %d\n", __func__, __LINE__);
   return instance->rts_threshold;
}


static int _meshap_mac80211_net_dev_set_rts_threshold(struct net_device *dev, int threshold)
{
   meshap_instance_t *instance;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Enter %s: %d\n", __func__, __LINE__);
   instance = meshap_get_instance(dev);
   if (NULL == instance)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MAC80211 : ERROR : Instance is NULL %s: %d\n", __func__, __LINE__);
      return -1;
   }

   if (instance->use_type == AL_CONF_IF_USE_TYPE_AP)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MAC80211 : INFO : For AP interface hostapd will handle this %s: %d\n", __func__, __LINE__);
      return 0;
   }
   instance->rts_threshold = threshold;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MAC80211 : INFO : instance->rts_threshold : %d %s: %d\n", instance->rts_threshold,  __func__, __LINE__);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Exit %s: %d\n", __func__, __LINE__);
   return 0;
}


static int _meshap_mac80211_net_dev_set_essid(struct net_device *dev, char *essid_buffer)
{
   meshap_instance_t *instance;
   unsigned long     flags;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Enter %s: %d\n", __func__, __LINE__);
   instance = meshap_get_instance(dev);
   if (NULL == instance)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MAC80211 : ERROR : Instance is NULL %s: %d\n", __func__, __LINE__);
      return -1;
   }
   MESHAP_INSTANCE_LOCK(instance, flags);
   strcpy(instance->current_essid, essid_buffer);
   instance->current_essid_length = strlen(essid_buffer);
   MESHAP_INSTANCE_UNLOCK(instance, flags);
   
   if (instance->use_type == AL_CONF_IF_USE_TYPE_AP)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MAC80211 : INFO : For AP interface hostapd will handle this %s: %d\n", __func__, __LINE__);
      return 0;
   }

   /*Beacon parameter is changed, update beacon buffer for 'wm' interfaces*/
   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MAC80211 : INFO : Use_type: %d conf_complete: %d essid: %s %s: %d\n", instance->use_type, instance->beacon_conf_complete, instance->current_essid, __func__, __LINE__);
   if ((instance->use_type == AL_CONF_IF_USE_TYPE_WM) && instance->beacon_conf_complete)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MAC80211 : INFO : Modifying beacon buffer %s: %d\n", instance->rts_threshold,  __func__, __LINE__);
      meshap_setup_beacon_frame(instance);
   }

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Exit %s: %d\n", __func__, __LINE__);
   return 0;
}


static int _meshap_mac80211_net_dev_get_essid(struct net_device *dev, char *essid_buffer, int length)
{
   meshap_instance_t *instance;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Enter %s: %d\n", __func__, __LINE__);
   instance = meshap_get_instance(dev);
   if (NULL == instance)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MAC80211 : ERROR : Instance is NULL %s: %d\n", __func__, __LINE__);
      return -1;
   }

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : INFO : Setting ESSID %s for %s %s: %d\n", instance->current_essid, dev->name, __func__, __LINE__);
   strcpy(essid_buffer, instance->current_essid);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Exit %s: %d\n", __func__, __LINE__);
   return 0;
}


static int _meshap_mac80211_net_dev_get_mode(struct net_device *dev)
{
   meshap_instance_t *instance;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Enter %s: %d\n", __func__, __LINE__);
   instance = meshap_get_instance(dev);
   if (NULL == instance)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MAC80211 : ERROR : Instance is NULL %s: %d\n", __func__, __LINE__);
      return -1;
   }

   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MAC80211 : INFO : %s : iw_mode : %d %s: %d\n", dev->name, instance->iw_mode, __func__, __LINE__);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Exit %s: %d\n", __func__, __LINE__);
   return instance->iw_mode;
}


static int _meshap_mac80211_net_dev_set_dev_token(struct net_device *dev, void *token)
{
   meshap_instance_t *instance;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Enter %s: %d\n", __func__, __LINE__);

   instance = meshap_get_instance(dev);
   if (NULL == instance)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MAC80211 : ERROR : Instance is NULL %s: %d\n", __func__, __LINE__);
      return -1;
   }

   instance->dev_token = token;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MAC80211 : INFO : %s : token : %d %s: %d\n", dev->name, instance->dev_token, __func__, __LINE__);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Exit %s: %d\n", __func__, __LINE__);
   return 0;
}


static int _meshap_mac80211_net_dev_set_mode(struct net_device *dev, int mode, unsigned char master_quiet)
{
   meshap_instance_t *instance = NULL;

   instance = meshap_get_instance(dev);

   if (instance == NULL)
   {
      printk("Got instance NULL\n");
      return 0;
   }
   if (instance->iw_mode == mode) {
        /* As the requested mode is already operating */
        return 0;
   } else {
        al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MAC80211 : INFO : %s: Changing mode from %d to %d %s: %d\n", instance->dev->name, instance->iw_mode, mode, __func__, __LINE__);
   }

   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MAC80211 : INFO : Mode: %d %s: %d\n", mode, __func__, __LINE__);
   switch (mode)
   {
   case IW_MODE_MASTER:
      if (instance->iw_mode == IW_MODE_INFRA)
      {
         meshap_uninit_infra_mode(instance);
      }
      if (master_quiet)
      {
         instance->flags |= MESHAP_INSTANCE_FLAGS_MASTER_QUIET;
      }
      else
      {
         instance->flags &= ~MESHAP_INSTANCE_FLAGS_MASTER_QUIET;
      }
      instance->iw_mode = mode;
      meshap_reset(instance->dev, 0);
      meshap_init_master_mode(instance);
      break;

   case IW_MODE_INFRA:
      if (instance->iw_mode == IW_MODE_MASTER)
      {
         meshap_uninit_master_mode(instance);
      }
      instance->flags  &= ~MESHAP_INSTANCE_FLAGS_MASTER_QUIET;
      instance->iw_mode = mode;
      meshap_init_infra_mode(instance);
      break;

   case IW_MODE_MONITOR:
      instance->monitor_mode_mask = 0;
      instance->flags            &= ~MESHAP_INSTANCE_FLAGS_MASTER_QUIET;
      instance->iw_mode           = mode;
      meshap_reset(instance->dev, 0);
      break;

   default:
      return -EINVAL;
   }
   return 0;
}


static int
_meshap_mac80211_net_dev_set_hw_addr(struct net_device *dev, unsigned char *hw_addr)
{
   char                         addr[6];
   struct wireless_dev          *wdev  = dev->ieee80211_ptr;
   struct ieee80211_hw          *hw    = wiphy_to_ieee80211_hw(wdev->wiphy);
   struct ieee80211_sub_if_data *sdata = IEEE80211_DEV_TO_SUB_IF(dev);
   unsigned long                flags;

   memset(addr, 0, 6);
   memcpy(addr, hw_addr, 6);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Enter %s: %d\n", __func__, __LINE__);
   local_irq_save(flags);
   memcpy(dev->dev_addr, hw_addr, ETH_ALEN);

   /* Update the vif.addr field in mac80211. This is needed because the default mac address of the DS interface is changed by meshap but vif.addr
    * does not get updated. Therefore the Root node starts rejecting the managment frames from this unassociated mac address */
   memcpy(hw->wiphy->perm_addr, hw_addr, 6);
   memcpy(sdata->vif.addr, hw_addr, ETH_ALEN);
   local_irq_restore(flags);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Exit %s: %d\n", __func__, __LINE__);
   return 0;
}


static int
_meshap_mac80211_net_dev_associate(struct net_device *dev,
                                   unsigned char     *bssid,
                                   int               channel,
                                   const char        *essid,
                                   int               length,
                                   int               timeout,
                                   unsigned char     *ie_in,
                                   int               ie_in_length,
                                   unsigned char     *ie_out)
{
   meshap_instance_t *instance;
   int               ret;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Enter %s: %d\n", __func__, __LINE__);
   instance = meshap_get_instance(dev);
   if (NULL == instance)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MAC80211 : ERROR : Instance is NULL %s: %d\n", __func__, __LINE__);
      return -1;
   }

   if (instance->iw_mode != IW_MODE_INFRA)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MAC80211 : ERROR : Not in infra mode %s: %d\n", __func__, __LINE__);
      return -1;
   }

   ret = meshap_sta_fsm_join(instance, essid, length, bssid, channel, ie_in, ie_in_length, ie_out);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MAC80211 : INFO : ret: %d %s: %d\n", ret,  __func__, __LINE__);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Exit %s: %d\n", __func__, __LINE__);
   return(ret != 1);
}


static int
_meshap_mac80211_net_dev_disassociate(struct net_device *dev)
{
   meshap_instance_t *instance;
   int               ret;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Enter %s: %d\n", __func__, __LINE__);
   instance = meshap_get_instance(dev);
   if (NULL == instance)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MAC80211 : ERROR : Instance is NULL %s: %d\n", __func__, __LINE__);
      return -1;
   }
   if (instance->iw_mode != IW_MODE_INFRA)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MAC80211 : ERROR : Not in infra mode %s: %d\n", __func__, __LINE__);
      return -1;
   }

   ret = meshap_sta_fsm_leave(instance, 1, WLAN_REASON_UNSPECIFIED);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Exit %s: %d\n", __func__, __LINE__);

   return ret;
}


static int
_meshap_mac80211_net_dev_get_bssid(struct net_device *dev, unsigned char *bssid)
{
   meshap_instance_t *instance;

//   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Enter %s: %d\n", __func__, __LINE__);
   instance = meshap_get_instance(dev);
   if (NULL == instance)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MAC80211 : ERROR : Instance is NULL %s: %d\n", __func__, __LINE__);
      return -1;
   }

   if (instance->iw_mode != IW_MODE_INFRA)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MAC80211 : ERROR : Not in infra mode %s: %d\n", __func__, __LINE__);
      return -1;
   }

   memcpy(bssid, instance->current_bssid, ETH_ALEN);
//   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Exit %s: %d\n", __func__, __LINE__);

   return 0;
}


static int _meshap_mac80211_net_dev_scan_access_points_active(struct net_device *dev, meshap_scan_access_point_info_t **access_points, int *count, int scan_duration_in_millis, int channel_count, unsigned char *channels)
{
   struct _temp_list
   {
      meshap_scan_access_point_info_t ap_info;
      struct _temp_list               *next;
   };

   typedef struct _temp_list   _temp_list_t;

   meshap_instance_t            *instance;
   meshap_sta_fsm_scan_result_t *results;
   int result_count;
   meshap_scan_access_point_info_t *aps;
   int          repeat_count;
   _temp_list_t *head;
   _temp_list_t *node;
   int          ap_count;
   int          i;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Enter %s: %d\n", __func__, __LINE__);
   instance = (meshap_instance_t *)netdev_priv(dev);

   if (instance->iw_mode != IW_MODE_INFRA)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MAC80211 : ERROR : Not in infra mode %s: %d\n", __func__, __LINE__);
      return -1;
   }

   repeat_count = 1;
   head         = NULL;
   ap_count     = 0;

   while (repeat_count--)
   {
      if (meshap_sta_fsm_active_scan(instance, channel_count, channels, scan_duration_in_millis, &results, &result_count))
      {
         continue;
      }

      for (i = 0; i < result_count; i++)
      {
         node = head;
         while (node != NULL)
         {
            if (!memcmp(node->ap_info.bssid, results[i].bssid, ETH_ALEN))
            {
               break;
            }
            node = node->next;
         }
         if (node != NULL)
         {
            /** TODO write code to average out signal */
         }
         else
         {
            node = (_temp_list_t *)kmalloc(sizeof(_temp_list_t), GFP_ATOMIC);

            memset(node, 0, sizeof(_temp_list_t));
            node->ap_info.beacon_interval = results[i].beacon_interval;
            memcpy(node->ap_info.bssid, results[i].bssid, ETH_ALEN);
            node->ap_info.capabilities = results[i].capability;
            node->ap_info.channel      = results[i].channel;
            strcpy(node->ap_info.essid, results[i].ssid);
            node->ap_info.mode   = IW_MODE_MASTER;
            node->ap_info.signal = results[i].signal;

            if (results[i].vendor_info_length > 0)
            {
               memcpy(node->ap_info.vendor_info,
                      results[i].vendor_info + sizeof(_vendor_info_header),
                      results[i].vendor_info_length - sizeof(_vendor_info_header));

               node->ap_info.vendor_info_length = results[i].vendor_info_length - sizeof(_vendor_info_header);
            }
            else
            {
               node->ap_info.vendor_info_length = 0;
            }

            memcpy(&node->ap_info.ap_ht_capab, &results[i].ap_ht_capab, sizeof(struct meshap_ht_capabilities));
            memcpy(&node->ap_info.ap_vht_capab, &results[i].ap_vht_capab, sizeof(struct meshap_vht_capabilities));
            node->next = head;
            head       = node;
            ++ap_count;
         }
      }
      kfree(results);
   }

   /**
    * Convert the results as per meshap al format
    */

   aps = (meshap_scan_access_point_info_t *)kmalloc(sizeof(meshap_scan_access_point_info_t) * ap_count, GFP_ATOMIC);
   if (aps == NULL)
   {
      for (i = 0, node = head; i < ap_count && node != NULL; i++, node = head)
      {
         head = node->next;
         kfree(node);
      }
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MAC80211 : ERROR : %s<%d> Memory alloc failed \n ",__func__, __LINE__);
      return -1;
   }

   memset(aps, 0, sizeof(meshap_scan_access_point_info_t) * ap_count);

   for (i = 0, node = head; i < ap_count && node != NULL; i++, node = head)
   {
      head = node->next;
      memcpy(&aps[i], &node->ap_info, sizeof(meshap_scan_access_point_info_t));
      kfree(node);
   }

   *count         = ap_count;
   *access_points = aps;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MAC80211 : INFO : count: %d %s: %d\n", *count, __func__, __LINE__);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Exit %s: %d\n", __func__, __LINE__);
   return 0;
}


static int
_meshap_mac80211_net_dev_scan_access_points_passive(struct net_device               *dev,
                                                    meshap_scan_access_point_info_t **access_points,
                                                    int                             *count,
                                                    int                             scan_duration_in_millis,
                                                    int                             channel_count,
                                                    unsigned char                   *channels)
{
   struct _temp_list
   {
      meshap_scan_access_point_info_t ap_info;
      struct _temp_list               *next;
   };

   typedef struct _temp_list   _temp_list_t;

   meshap_instance_t            *instance;
   meshap_sta_fsm_scan_result_t *results;
   int result_count;
   meshap_scan_access_point_info_t *aps;
   int          repeat_count;
   _temp_list_t *head;
   _temp_list_t *node;
   int          ap_count;
   int          i, ret = 0;

   instance = meshap_get_instance(dev);
   if (NULL == instance)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MAC80211 : ERROR : Instance is NULL %s : %d\n", __func__, __LINE__);
      return -1;
   }

   if (instance->iw_mode != IW_MODE_INFRA)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MAC80211 : ERROR : Not in Infra mode %s : %d\n", __func__, __LINE__);
      return -1;
   }

   repeat_count = 1;
   head         = NULL;
   ap_count     = 0;

   while (repeat_count--)
   {
      ret = meshap_sta_fsm_passive_scan(instance, channel_count, channels, scan_duration_in_millis, &results, &result_count);

      if (ret < 0)
      {
         break;
      }

      for (i = 0; i < result_count; i++)
      {
         node = head;
         while (node != NULL)
         {
            if (!memcmp(node->ap_info.bssid, results[i].bssid, ETH_ALEN))
            {
               break;
            }
            node = node->next;
         }
         if (node != NULL)
         {
            /** TODO write code to average out signal */
         }
         else
         {
            node = (_temp_list_t *)kmalloc(sizeof(_temp_list_t), GFP_ATOMIC);

            memset(node, 0, sizeof(_temp_list_t));
            node->ap_info.beacon_interval = results[i].beacon_interval;
            memcpy(node->ap_info.bssid, results[i].bssid, ETH_ALEN);
            node->ap_info.capabilities = results[i].capability;
            node->ap_info.channel      = results[i].channel;
            strcpy(node->ap_info.essid, results[i].ssid);
            node->ap_info.mode   = IW_MODE_MASTER;
            node->ap_info.signal = results[i].signal;

            if (results[i].vendor_info_length > 0)
            {
               memcpy(node->ap_info.vendor_info,
                      results[i].vendor_info + sizeof(_vendor_info_header),
                      results[i].vendor_info_length - sizeof(_vendor_info_header));

               node->ap_info.vendor_info_length = results[i].vendor_info_length - sizeof(_vendor_info_header);
            }
            else
            {
               node->ap_info.vendor_info_length = 0;
            }

          // Need to ask look  about Size mismatch with respect to 2 structures.
            memcpy(&node->ap_info.ap_ht_capab ,&results[i].ap_ht_capab, sizeof(struct meshap_ht_capabilities));
            memcpy(&node->ap_info.ap_vht_capab, &results[i].ap_vht_capab, sizeof(struct meshap_vht_capabilities));
            node->next = head;
            head       = node;
            ++ap_count;
         }
      }
      kfree(results);
   }

   /**
    * Convert the results as per meshap al format
    */

   aps = (meshap_scan_access_point_info_t *)kmalloc(sizeof(meshap_scan_access_point_info_t) * ap_count, GFP_ATOMIC);
   if (aps == NULL)
   {
      for (i = 0, node = head; i < ap_count && node != NULL; i++, node = head)
      {
         head = node->next;
         kfree(node);
      }
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MAC80211 : ERROR : %s<%d> Memory alloc failed \n ",__func__, __LINE__);
      return -1;
   }

   memset(aps, 0, sizeof(meshap_scan_access_point_info_t) * ap_count);

   for (i = 0, node = head; i < ap_count && node != NULL; i++, node = head)
   {
      head = node->next;
      memcpy(&aps[i], &node->ap_info, sizeof(meshap_scan_access_point_info_t));
      kfree(node);
   }

   *count         = ap_count;
   *access_points = aps;

   return ret;
}


static u64
_meshap_mac80211_net_dev_get_last_beacon_time(struct net_device *dev)
{
   meshap_instance_t *instance;

   instance = meshap_get_instance(dev);
   if (NULL == instance)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MAC80211 : ERROR : Instance is NULL %s: %d\n", __func__, __LINE__);
      return 0;
   }

   return instance->last_beacon_time;
}


static void
_meshap_mac80211_net_dev_set_mesh_downlink_round_robin_time(struct net_device *dev,
                                                            int               beacon_intervals)
{
   meshap_instance_t *instance;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Enter %s: %d\n", __func__, __LINE__);
   instance = meshap_get_instance(dev);
   if (NULL == instance)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MAC80211 : ERROR : Instance is NULL %s: %d\n", __func__, __LINE__);
      return;
   }
   if (instance->use_type == AL_CONF_IF_USE_TYPE_AP)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MAC80211 : INFO : For AP interface hostapd will handle this %s: %d\n", __func__, __LINE__);
      return;
   }

   meshap_round_robin_set_count(instance, beacon_intervals);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Exit %s: %d\n", __func__, __LINE__);
}


static void
_meshap_mac80211_net_dev_add_downlink_round_robin_child(struct net_device *dev,
                                                        unsigned char     *addr)
{
   meshap_instance_t *instance;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Enter %s: %d\n", __func__, __LINE__);
   instance = meshap_get_instance(dev);
   if (NULL == instance)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MAC80211 : ERROR : Instance is NULL %s: %d\n", __func__, __LINE__);
      return;
   }
   if (instance->use_type == AL_CONF_IF_USE_TYPE_AP)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MAC80211 : INFO : For AP interface hostapd will handle this %s: %d\n", __func__, __LINE__);
      return;
   }

   meshap_round_robin_add_address(instance, addr);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Exit %s: %d\n", __func__, __LINE__);
}


static void
_meshap_mac80211_net_dev_remove_downlink_round_robin_child(struct net_device *dev,
                                                           unsigned char     *addr)
{
   meshap_instance_t *instance;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Enter %s: %d\n", __func__, __LINE__);
   instance = meshap_get_instance(dev);
   if (NULL == instance)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MAC80211 : ERROR : Instance is NULL %s: %d\n", __func__, __LINE__);
      return;
   }
   if (instance->use_type == AL_CONF_IF_USE_TYPE_AP)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MAC80211 : INFO : For AP interface hostapd will handle this %s: %d\n", __func__, __LINE__);
      return;
   }

   meshap_round_robin_remove_address(instance, addr);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Exit %s: %d\n", __func__, __LINE__);
}


static int
_meshap_mac80211_net_dev_virtual_associate(struct net_device *dev,
                                           unsigned char     *ap_addr,
                                           int               channel,
                                           int               start)
{
   meshap_instance_t *instance;
   unsigned long     flags;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Enter %s: %d\n", __func__, __LINE__);
   instance = meshap_get_instance(dev);
   if (NULL == instance)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MAC80211 : ERROR : Instance is NULL %s: %d\n", __func__, __LINE__);
      return -1;
   }

   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MAC80211 : INFO : start : %d %s: %d\n", start, __func__, __LINE__);
   switch (start)
   {
   case 0:

      meshap_mac80211_dev_set_channel(instance, channel, 1);

      MESHAP_INSTANCE_LOCK(instance, flags);
      memcpy(instance->current_bssid, ap_addr, ETH_ALEN);
      memset(instance->current_virtual_bssid, 0, ETH_ALEN);
      instance->connected          = instance->old_connected;
      instance->virtual_associated = start;
      instance->last_beacon_time   = jiffies;
      if (instance->connected)
      {
         meshap_sta_fsm_join_virtual(instance);
      }
      MESHAP_INSTANCE_UNLOCK(instance, flags);

      break;

   case 1:

      MESHAP_INSTANCE_LOCK(instance, flags);
      instance->old_connected = instance->connected;
      memcpy(instance->current_bssid, ap_addr, ETH_ALEN);
      memcpy(instance->current_virtual_bssid, instance->current_bssid, ETH_ALEN);
      instance->virtual_associated = start;
      instance->connected          = 1;
      MESHAP_INSTANCE_UNLOCK(instance, flags);

      meshap_mac80211_dev_set_channel(instance, channel, 1);

      break;

   case 2:

      MESHAP_INSTANCE_LOCK(instance, flags);
      instance->old_connected      = instance->connected;
      instance->virtual_associated = 0;
      instance->connected          = 1;
      MESHAP_INSTANCE_UNLOCK(instance, flags);

      break;
   }
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Exit %s: %d\n", __func__, __LINE__);
   return 0;
}


static void
_meshap_mac80211_net_dev_set_round_robin_notify_hook(struct net_device           *dev,
                                                     meshap_round_robin_notify_t hook)
{
   meshap_instance_t *instance;
   unsigned long     flags;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Enter %s: %d\n", __func__, __LINE__);
   instance = meshap_get_instance(dev);
   if (NULL == instance)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MAC80211 : ERROR : Instance is NULL %s: %d\n", __func__, __LINE__);
      return;
   }
   if (instance->use_type == AL_CONF_IF_USE_TYPE_AP)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MAC80211 : INFO : For AP interface hostapd will handle this %s: %d\n", __func__, __LINE__);
      return;
   }

   MESHAP_INSTANCE_LOCK(instance, flags);

   instance->round_robin_hook = (void *)hook;

   MESHAP_INSTANCE_UNLOCK(instance, flags);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Exit %s: %d\n", __func__, __LINE__);
}

meshap_duty_cycle_ap_data_t* find_duty_cycle_ap_data (meshap_instance_t* instance,
                                                          meshap_duty_cycle_data_t* data,
                                                          unsigned char* bssid)
{
    meshap_duty_cycle_ap_data_t* ap_data = NULL;

    if (data != NULL)
        ap_data = data->ap_list;
    else
        return NULL;
    while (ap_data != NULL)
    {
        if (NULL != ap_data->bssid && NULL != bssid)
        {
            if (!memcmp(ap_data->bssid, bssid, ETH_ALEN))
                return ap_data;
            else
                ap_data = ap_data->next;
        }
    }
    return NULL;
}

meshap_duty_cycle_ap_data_t *meshap_add_new_duty_cycle_ap_data(meshap_instance_t *instance,
                                                               meshap_duty_cycle_data_t *data,
                                                               u8 *bssid,
                                                               u8 signal,
                                                               u8 *essid_element,
                                                               u32 tx_time,
                                                               meshap_duty_cycle_info_t *info)
{
    meshap_duty_cycle_ap_data_t* ap_data;

    ap_data = (meshap_duty_cycle_ap_data_t*)kzalloc(sizeof(meshap_duty_cycle_ap_data_t), GFP_ATOMIC);
    if (NULL == ap_data)
    {
        al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "%s: Failed to allocate memory\n", __func__);
        return NULL;
    }

    memcpy(ap_data->bssid, bssid, ETH_ALEN);
    ap_data->signal             = signal;
    ap_data->transmit_duration  = tx_time;
    ++data->info.number_of_aps;

    if (NULL != essid_element)
    {
        if (essid_element[0] == WLAN_EID_ESSID)
        {
            if (essid_element[1] <= IW_ESSID_MAX_SIZE)
            {
                memcpy(ap_data->essid, &essid_element[2], essid_element[1]);
            }
            else
            {
                memcpy(ap_data->essid, &essid_element[2], IW_ESSID_MAX_SIZE);
            }
        }
    }
    ap_data->next = NULL;
    if (NULL == data->ap_list)
    {
        data->ap_list = ap_data;
    }
    else
    {
        ap_data->next = data->ap_list;
        data->ap_list = ap_data;
    }

    return data->ap_list;
}

int meshap_process_beacon_duty_cycle(meshap_instance_t        *instance,
                                     meshap_duty_cycle_data_t *data,
                                     struct sk_buff*           skb,
                                     unsigned char*            p,
                                     unsigned int              rssi,
                                     unsigned int              rate,
                                     unsigned int              tx_time,
                                     meshap_duty_cycle_info_t *info)
{
    __le16                       fc;
    int                          length;
    meshap_duty_cycle_ap_data_t* ap_data;
    struct ieee80211_hdr*        hdr = ( struct ieee80211_hdr*)skb->data;

    fc = hdr->frame_control;

    ap_data = find_duty_cycle_ap_data( instance, data, ((struct ieee80211_hdr*)skb->data)->addr3);
    if (ap_data != NULL)
    {
        ap_data->signal             = (ap_data->signal + rssi)/2;
        ap_data->transmit_duration += tx_time;

        if (ieee80211_has_retry(fc))
            ap_data->retry_duration  += tx_time;

        length = p[WLAN_BEACON_SSID_POSITION + 1];
        memset(ap_data->essid, 0, 32);
        memcpy(ap_data->essid, &p[WLAN_BEACON_SSID_POSITION + 2], length);
    }
    else
    {
        if ((hdr->addr1[0] != 0xff) || (hdr->addr1[1] != 0xff) || (hdr->addr1[2] != 0xff) ||
            (hdr->addr1[3] != 0xff) || (hdr->addr1[4] != 0xff) || (hdr->addr1[5] != 0xff))
        {
            return 0;
        }
        if (memcmp(hdr->addr2, hdr->addr3, ETH_ALEN) != 0)
        {
            return 0;
        }
        ap_data = meshap_add_new_duty_cycle_ap_data(instance, data, ((struct ieee80211_hdr*)skb->data)->addr3, rssi, &p[WLAN_BEACON_SSID_POSITION], tx_time, info);
    }

    return 0;
}

int meshap_process_data_duty_cycle(meshap_instance_t        *instance,
                                   meshap_duty_cycle_data_t *data,
                                   struct sk_buff*           skb,
                                   unsigned char*            p,
                                   unsigned int              rssi,
                                   unsigned int              rate,
                                   unsigned int              tx_time,
                                   meshap_duty_cycle_info_t *info)
{
    __le16                       fc;
    int                          length;
    meshap_duty_cycle_ap_data_t* ap_data;
    struct ieee80211_hdr*        hdr = ( struct ieee80211_hdr*)skb->data;

    fc = hdr->frame_control;
    if (ieee80211_has_tods(fc))
    {
        ap_data = find_duty_cycle_ap_data( instance, data, ((struct ieee80211_hdr*)skb->data)->addr1);
    }
    else if (ieee80211_has_fromds(fc))
    {
        ap_data = find_duty_cycle_ap_data( instance, data, ((struct ieee80211_hdr*)skb->data)->addr2);
    }
    else if (ieee80211_has_a4(fc))
    {
        ap_data = find_duty_cycle_ap_data( instance, data, ((struct ieee80211_hdr*)skb->data)->addr2);
    }
    else
    {
        ap_data = find_duty_cycle_ap_data( instance, data, ((struct ieee80211_hdr*)skb->data)->addr3);
    }

    if (ap_data != NULL)
    {
        ap_data->signal              = (ap_data->signal + rssi)/2;
        ap_data->transmit_duration  +=  tx_time;
        if (ieee80211_has_retry(fc) )
            ap_data->retry_duration   += tx_time;

        length = p[WLAN_BEACON_SSID_POSITION + 1];
        memset(ap_data->essid, 0, 32);
        memcpy(ap_data->essid, &p[WLAN_BEACON_SSID_POSITION + 2], length);
    }
    else
    {
        if ((hdr->addr1[0] != 0xff) || (hdr->addr1[1] != 0xff) || (hdr->addr1[2] != 0xff) ||
            (hdr->addr1[3] != 0xff) || (hdr->addr1[4] != 0xff) || (hdr->addr1[5] != 0xff))
        {
            return 0;
        }
        if (memcmp(hdr->addr2, hdr->addr3, ETH_ALEN) != 0)
        {
            return 0;
        }
        if (ieee80211_has_tods(fc))
            ap_data = meshap_add_new_duty_cycle_ap_data(instance, data, ((struct ieee80211_hdr*)skb->data)->addr1, rssi, &p[WLAN_BEACON_SSID_POSITION], tx_time, info);
        else if (ieee80211_has_fromds(fc))
            ap_data = meshap_add_new_duty_cycle_ap_data(instance, data, ((struct ieee80211_hdr*)skb->data)->addr2, rssi, &p[WLAN_BEACON_SSID_POSITION], tx_time, info);
        else if (ieee80211_has_a4(fc))
            ap_data = meshap_add_new_duty_cycle_ap_data(instance, data, ((struct ieee80211_hdr*)skb->data)->addr2, rssi, &p[WLAN_BEACON_SSID_POSITION], tx_time, info);
        else
            ap_data = meshap_add_new_duty_cycle_ap_data(instance, data, ((struct ieee80211_hdr*)skb->data)->addr3, rssi, &p[WLAN_BEACON_SSID_POSITION], tx_time, info);
    }

}

int meshap_capture_duty_cycle_frames(struct sk_buff *skb,
                                     struct net_device *dev,
                                     int rssi,
                                     int rate)
{
    __le16                     fc;
    int                        ret;
    meshap_instance_t         *instance;
    _meshap_duty_cycle_info_t  dci_info;

    fc = ((struct ieee80211_hdr*)skb->data)->frame_control;

    rssi = 100 - rssi;

    instance = meshap_get_instance(dev);
    if (NULL == instance)
    {
        al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "Failed to get instance\n");
        return;
    }

    dci_info.skb     = skb;
    dci_info.ndev    = dev;
    dci_info.rssi    = rssi;
    dci_info.rx_rate = rate;
    if (ieee80211_is_beacon(fc)|| ieee80211_is_data(fc))
    {
        ret = kfifo_in(&instance->dci_fifo, &dci_info, sizeof(_meshap_duty_cycle_info_t));
        if (ret <= 0)
        {
            instance->duty_cycle_flags &= ~MESHAP_INSTANCE_DUTY_CYCLE_FLAG;
            complete(&instance->completion);
            al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "Queue is full\n");
            return;
        }
    }
    return;
}

void meshap_process_duty_cycle(struct sk_buff *skb, struct net_device *dev, int rssi, int rate, int channel, unsigned int dwell_time, meshap_duty_cycle_info_t *info)
{
    __le16                       fc;
    int                          tag_length;
    int                          base_len, length;
    unsigned int                 transmit_time;
    unsigned char               *p;
    unsigned char               *base;
    meshap_duty_cycle_data_t*    data;
    meshap_instance_t           *instance;
    struct ieee80211_hdr*        hdr = ( struct ieee80211_hdr*)skb->data;

    fc = hdr->frame_control;
    base            = skb->data;
    base_len        = skb->len;
    p  = base;
    p += FC_LEN;         /* Frame Control */
    p += DURATION_ID_LEN;     /* Skip Duration/ID */

    if (!base_len)
        goto exit;

    if (ieee80211_is_mgmt(fc) || ieee80211_is_data(fc))
    {
        p   += ( ETH_ALEN * NO_OF_ADDRESS) + SEQUENCE_CTL_LEN; /* 3 address + 2 seq_cntrl */
        length =  p[WLAN_BEACON_SSID_POSITION + 1];
    }
    else
    {
        goto exit;
    }

    instance = meshap_get_instance(dev);
    if (instance)
    {
        data = &instance->dutycycle_data;
        data->info.channel = channel;

        if (data->info.max_signal < rssi)
            data->info.max_signal  = rssi;

        if (0 == data->info.avg_signal)
            data->info.avg_signal  = rssi;
        else
            data->info.avg_signal  = (data->info.avg_signal + rssi)/2;

        if (0 == rate)
        {
            transmit_time = 0;
        }
        else
        {
            transmit_time        =  base_len * 8;
            transmit_time       *=  1000;
            transmit_time       /=  rate;
            transmit_time       /=  1048;
        }

        data->info.transmit_duration += transmit_time;
        data->info.total_duration     = dwell_time;
        if (ieee80211_has_retry(fc))
            data->info.retry_duration  += transmit_time;

        if (ieee80211_is_mgmt(fc))
        {
            if (ieee80211_is_beacon(fc))
            {
                meshap_process_beacon_duty_cycle(instance, data, skb, p, rssi, rate, transmit_time, info);
            }
        }
        else if (ieee80211_is_data(fc))
        {
            meshap_process_data_duty_cycle(instance, data, skb, p, rssi, rate, transmit_time, info);
        }
    }
    else
    {
        al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "%s: %d Failed to get instance\n", __func__, __LINE__);
    }

exit:
    dev_kfree_skb(skb);
    return;
}

void process_recvd_duty_cycle_frames(struct net_device *dev,
                                     meshap_duty_cycle_info_t *info,
                                     int channel,
                                     unsigned int dwell_time)
{
    int                          ret = 0, i = 0;
    meshap_instance_t           *instance;
    _meshap_duty_cycle_info_t    dci_info;
    meshap_duty_cycle_data_t*    data;
    meshap_duty_cycle_ap_data_t* ap_data;

    instance = meshap_get_instance(dev);
    if (NULL == instance)
    {
        al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "instance became NULL\n");
        return 0;
    }

    while (0 != (ret = kfifo_out(&instance->dci_fifo, &dci_info, sizeof(_meshap_duty_cycle_info_t))))
    {
        meshap_process_duty_cycle(dci_info.skb, dev, dci_info.rssi, dci_info.rx_rate, channel, dwell_time, info);
    }

    data                    = &instance->dutycycle_data;
    info->channel           = data->info.channel;
    info->avg_signal        = data->info.avg_signal;
    info->max_signal        = data->info.max_signal;
    info->number_of_aps     = data->info.number_of_aps;
    info->total_duration    = data->info.total_duration;
    info->transmit_duration = data->info.transmit_duration;

    info->aps = kmalloc(sizeof(*info->aps) * data->info.number_of_aps, GFP_KERNEL);
    if (NULL == info->aps)
    {
        al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "Failed to allocate memory\n");
        return -1;
    }
    ap_data = data->ap_list;
    while (NULL != ap_data && i < data->info.number_of_aps)
    {
        info->aps[i].retry_duration = ap_data->retry_duration;
        info->aps[i].transmit_duration = ap_data->transmit_duration;
        info->aps[i].signal = ap_data->signal;
        memcpy(info->aps[i].essid, ap_data->essid, sizeof(ap_data->essid));
        memcpy(info->aps[i].bssid, ap_data->bssid, sizeof(ap_data->bssid));
        i++;
        ap_data = ap_data->next;
    }
    ap_data = data->ap_list;

    while (NULL != ap_data)
    {
        meshap_duty_cycle_ap_data_t *temp = ap_data;
        ap_data = ap_data->next;
        kfree(temp);
    }
    data->ap_list = NULL;
    ap_data = NULL;

    return;
}
 
static int
_meshap_mac80211_net_dev_get_duty_cycle_info(struct net_device        *dev,
                                             int                      channel_count,
                                             meshap_duty_cycle_info_t *info,
                                             unsigned int             dwell_time_minis,
                                             int                      flag)
{
   meshap_instance_t *instance;
   int               index;
   long int          ret;
   struct net_device *device;
   unsigned char *name = "wlan1";
   int timeout = (dwell_time_minis*HZ)/1000;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Enter %s: %d\n", __func__, __LINE__);
   if (!flag)
   {
       device = dev;
       instance = meshap_get_instance(dev);
   }
   else
   {
      device = dev_get_by_name(&init_net, name);
      instance = meshap_get_instance(device);
   }
   if (NULL == instance)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MAC80211 : ERROR : Instance is NULL %s: %d\n", __func__, __LINE__);
      return -1;
   }

   struct ieee80211_sub_if_data *sdata = IEEE80211_DEV_TO_SUB_IF(instance->dev);
   if (channel_count == 0)
   {
      info->channel = instance->current_channel->number;
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MAC80211 : INFO : info->channel : %d %s: %d\n", info->channel, __func__, __LINE__);
      return 0;
   }

   instance->wm_duty_cycle_flag = 0;
   instance->duty_cycle_set_channel_flag = 1;
   for (index = 0; index < channel_count; index++)
   {
       ret = kfifo_alloc(&instance->dci_fifo, sizeof(_meshap_duty_cycle_info_t) * 32, GFP_KERNEL);
       if(ret)
       {
           al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "%s: %d Failed to allocate queue\n", __func__, __LINE__);
           return -1;
       }

       memset(&instance->dutycycle_data.info, 0, sizeof(instance->dutycycle_data.info));
       instance->dutycycle_data.ap_list = NULL;
       instance->dutycycle_data.next    = NULL;

       ret = _meshap_mac80211_net_dev_set_channel(device, info[index].channel);
       if (ret != 0)
       {
           al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, " Channel is not set... %s: %d\n", __func__, __LINE__);
           kfifo_free(&instance->dci_fifo);
           continue;
       }
       init_completion(&instance->completion);
       /*Need to check for wlan3 interface but for now we are not calling duty cycle in do_wm_saturation*/
       rtnl_lock();
       if (!(dev->flags & IFF_PROMISC))
           dev_set_promiscuity(device, 1);
       rtnl_unlock();

       instance->duty_cycle_flags |= MESHAP_INSTANCE_DUTY_CYCLE_FLAG;
       ret = wait_for_completion_interruptible_timeout(&instance->completion, timeout);
       if (ret < 0)
       {
           al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "Wait for completion failed: ret : %ld\n", ret);

           /*Need to check for wlan3 interface but for now we are not calling duty cycle in do_wm_saturation*/
           rtnl_lock();
           if (dev->flags & IFF_PROMISC)
               dev_set_promiscuity(device, -1);
           rtnl_unlock();

           instance->duty_cycle_flags &= ~MESHAP_INSTANCE_DUTY_CYCLE_FLAG;
           kfifo_free(&instance->dci_fifo);
           ieee80211_vif_release_channel(sdata);
           instance->current_channel = NULL;

           continue;
       }

       /*Need to check for wlan3 interface but for now we are not calling duty cycle in do_wm_saturation*/
       rtnl_lock();
       if (dev->flags & IFF_PROMISC)
           dev_set_promiscuity(device, -1);
       rtnl_unlock();

       instance->duty_cycle_flags &= ~MESHAP_INSTANCE_DUTY_CYCLE_FLAG;
       process_recvd_duty_cycle_frames(device, &info[index], info[index].channel, dwell_time_minis);
       kfifo_free(&instance->dci_fifo);
       ieee80211_vif_release_channel(sdata);
       instance->current_channel = NULL;
   }

   instance->duty_cycle_set_channel_flag = 0;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Exit %s: %d\n", __func__, __LINE__);
   return 0;
}

static void
_meshap_mac80211_net_dev_enable_ds_verification_opertions(struct net_device *dev, int enable)
{
   meshap_instance_t *instance;
   unsigned long     flags;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Enter %s: %d\n", __func__, __LINE__);
   instance = meshap_get_instance(dev);
   if (NULL == instance)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MAC80211 : ERROR : Instance is NULL %s: %d\n", __func__, __LINE__);
      return;
   }

   MESHAP_INSTANCE_LOCK(instance, flags);

   //TODO: review comment: use this FLAG before triggering BEACONMISS
   instance->ds_verification_enabled = enable;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MAC80211 : INFO : enable: %d %s: %d\n", enable, __func__, __LINE__);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Exit %s: %d\n", __func__, __LINE__);
   MESHAP_INSTANCE_UNLOCK(instance, flags);
}


static int
_meshap_mac80211_net_dev_get_beacon_interval(struct net_device *dev)
{
   meshap_instance_t *instance;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Enter %s: %d\n", __func__, __LINE__);
   instance = meshap_get_instance(dev);
   if (NULL == instance)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MAC80211 : ERROR : Instance is NULL %s: %d\n", __func__, __LINE__);
      return 0;
   }

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Exit %s: %d\n", __func__, __LINE__);
   return instance->beacon_interval;
}


static int
_meshap_mac80211_net_dev_get_default_capabilities(struct net_device *dev)
{
   meshap_instance_t *instance;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Enter %s: %d\n", __func__, __LINE__);
   instance = meshap_get_instance(dev);
   if (NULL == instance)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MAC80211 : ERROR : Instance is NULL %s: %d\n", __func__, __LINE__);
      return -1;
   }

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Exit %s: %d\n", __func__, __LINE__);
   return instance->default_capability;
}


static int
_meshap_mac80211_net_dev_get_capabilities(struct net_device *dev)
{
   meshap_instance_t *instance;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Enter %s: %d\n", __func__, __LINE__);
   instance = meshap_get_instance(dev);
   if (NULL == instance)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MAC80211 : ERROR : Instance is NULL %s: %d\n", __func__, __LINE__);
      return -1;
   }

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Exit %s: %d\n", __func__, __LINE__);
   return instance->capability;
}


static int
_meshap_mac80211_net_dev_get_slot_time_type(struct net_device *dev)
{
   meshap_instance_t *instance;
   int               ret;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Enter %s: %d\n", __func__, __LINE__);
   instance = meshap_get_instance(dev);
   if (NULL == instance)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MAC80211 : ERROR : Instance is NULL %s: %d\n", __func__, __LINE__);
      return -1;
   }

   if (instance->capability & WLAN_CAPABILITY_SHORT_SLOT_TIME)
   {
      ret = WLAN_SLOT_TIME_TYPE_SHORT;
   }
   else
   {
      ret = WLAN_SLOT_TIME_TYPE_LONG;
   }
   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MAC80211 : INFO : ret: %d %s: %d\n", ret, __func__, __LINE__);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Exit %s: %d\n", __func__, __LINE__);

   return ret;
}


static int _meshap_mac80211_net_dev_set_slot_time_type(struct net_device *dev, int slot_time_type)
{
   meshap_instance_t *instance;
   unsigned long     flags;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Enter %s: %d\n", __func__, __LINE__);
   instance = meshap_get_instance(dev);
   if (NULL == instance)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MAC80211 : ERROR : Instance is NULL %s: %d\n", __func__, __LINE__);
      return -1;
   }
   if (instance->use_type == AL_CONF_IF_USE_TYPE_AP)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MAC80211 : INFO : For AP interface hostapd will handle this %s: %d\n", __func__, __LINE__);
      return;
   }

   if (instance->iw_mode != IW_MODE_MASTER)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MAC80211 : ERROR : Not in master mode %s: %d\n", __func__, __LINE__);
      return -1;
   }

   if (instance->current_phy_mode != WLAN_PHY_MODE_802_11_G)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MAC80211 : ERROR : Not in pure G mode %s: %d\n", __func__, __LINE__);
      return -1;
   }

   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MAC80211 : INFO : slot_time_type: %d %s: %d\n", slot_time_type, __func__, __LINE__);
   switch (slot_time_type)
   {
   case WLAN_SLOT_TIME_TYPE_SHORT:
      if (instance->capability & WLAN_CAPABILITY_SHORT_SLOT_TIME)
      {
         return 0;
      }
      local_irq_save(flags);
      instance->capability |= WLAN_CAPABILITY_SHORT_SLOT_TIME;
      break;

   case WLAN_SLOT_TIME_TYPE_LONG:
      if (!(instance->capability & WLAN_CAPABILITY_SHORT_SLOT_TIME))
      {
         return 0;
      }
      local_irq_save(flags);
      instance->capability &= ~WLAN_CAPABILITY_SHORT_SLOT_TIME;
      break;

   default:
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MAC80211 : ERROR : Invalid slot_time_type : %d %s: %d\n", slot_time_type, __func__, __LINE__);
      return -1;
   }

   instance->slot_time_status = MESHAP_INSTANCE_SLOT_TIME_STATUS_UPDATE;

   local_irq_restore(flags);

   /* Call the ieee80211 API to set the value there as well */
   meshap_mac80211_set_short_slot_time(dev);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Exit %s: %d\n", __func__, __LINE__);
   return 0;
}


static int
_meshap_mac80211_net_dev_get_erp_info(struct net_device *dev)
{
   meshap_instance_t *instance;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Enter %s: %d\n", __func__, __LINE__);
   instance = meshap_get_instance(dev);
   if (NULL == instance)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MAC80211 : ERROR : Instance is NULL %s: %d\n", __func__, __LINE__);
      return -1;
   }

   if ((instance->current_phy_mode != WLAN_PHY_MODE_802_11_G) &&
       (instance->current_phy_mode != WLAN_PHY_MODE_802_11_PURE_G))
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MAC80211 : ERROR : Not in pure G mode %s: %d\n", __func__, __LINE__);
      return -1;
   }

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Exit %s: %d\n", __func__, __LINE__);
   return instance->extended_rate_phy_info;
}


static int _meshap_mac80211_net_dev_set_beacon_vendor_info(struct net_device *dev, unsigned char *vendor_info_buffer, int length)
{
   meshap_instance_t *instance;
   unsigned long     flags;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Enter %s: %d\n", __func__, __LINE__);
   instance = meshap_get_instance(dev);
   if (NULL == instance)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MAC80211 : ERROR : Instance is NULL %s: %d\n", __func__, __LINE__);
      return -1;
   }

   if (instance->iw_mode != IW_MODE_MASTER)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MAC80211 : ERROR : %s: Not in master mode %s: %d\n", instance->dev->name, __func__, __LINE__);
      return -1;
   }

   if (instance->use_type == AL_CONF_IF_USE_TYPE_AP)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MAC80211 : ERROR : %s: Hostapd will take care of this interface %s: %d\n", instance->dev->name, __func__, __LINE__);
      return 0;
   }

#if 0  //Todo: Open this when we implement DFS
   if (instance->current_channel->state_flags & CHANNEL_DFS &&
       !(instance->current_channel->state_flags & CHANNEL_DFS_CLEAR))
   {
      printk(KERN_INFO "%s: Ignoring beacon vendor info call when not cleared for DFS\n", dev->name);
      return -1;
   }
#endif

   if (instance->beacon_vendor_info_length > 0)
   {
      if ((vendor_info_buffer != NULL) && (length > 0))
      {
         if (memcmp(instance->beacon_vendor_info + sizeof(_vendor_info_header), vendor_info_buffer, length)) {
   		MESHAP_INSTANCE_LOCK(instance, flags);
         memcpy(instance->beacon_vendor_info, _vendor_info_header, sizeof(_vendor_info_header));
         memcpy(instance->beacon_vendor_info + sizeof(_vendor_info_header), vendor_info_buffer, length);
         instance->beacon_vendor_info_length = sizeof(_vendor_info_header) + length;
         instance->beacon_vendor_info_updated = 1;
   		MESHAP_INSTANCE_UNLOCK(instance, flags);
         }
      }
#if 0
      else
      {
         if (instance->iw_mode == IW_MODE_MASTER)
         {
            meshap_uninit_master_mode(instance);
         }
         meshap_reset(dev, 0);

         if (instance->iw_mode == IW_MODE_MASTER)
         {
            meshap_init_master_mode(instance);
         }
      }
#endif
   }
   else
   {
      if (length > 0)
      {
         if (instance->iw_mode == IW_MODE_MASTER)
         {
            meshap_uninit_master_mode(instance);
         }

         meshap_reset(dev, 0);

   		MESHAP_INSTANCE_LOCK(instance, flags);
         memcpy(instance->beacon_vendor_info, _vendor_info_header, sizeof(_vendor_info_header));
         memcpy(instance->beacon_vendor_info + sizeof(_vendor_info_header), vendor_info_buffer, length);
         instance->beacon_vendor_info_length = sizeof(_vendor_info_header) + length;
   		MESHAP_INSTANCE_UNLOCK(instance, flags);

         if (instance->iw_mode == IW_MODE_MASTER)
         {
            meshap_init_master_mode(instance);
         }

         al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MAC80211 : INFO : %s: Setting up Beacon vendor information of length %d %s: %d\n", dev->name, length, __func__, __LINE__);
      }
   }

   /* Bug: Bz-213 ieee80211_start_ap() was crying as 'mode' was INFRA, so set beacon
    * only when mode is MASTER and beacon configuration is completed */
   if (instance->iw_mode == IW_MODE_MASTER && instance->beacon_conf_complete)
   {
      //VENDOR_INFO: its not needed here to take lock, as round_robin() is doing so
      meshap_setup_beacon_frame(instance);
   }
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Exit %s: %d\n", __func__, __LINE__);
   return 0;
}


static int
_meshap_mac80211_net_dev_set_erp_info(struct net_device *dev, int erp_info)
{
#define _BIT_GOT_SET(b)      ((erp_info & b) && !(instance->extended_rate_phy_info & b))
#define _BIT_GOT_UNSET(b)    (!(erp_info & b) && (instance->extended_rate_phy_info & b))

   meshap_instance_t *instance;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Enter %s: %d\n", __func__, __LINE__);
   instance = meshap_get_instance(dev);
   if (NULL == instance)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MAC80211 : ERROR : Instance is NULL %s: %d\n", __func__, __LINE__);
      return -1;
   }
   if (instance->use_type == AL_CONF_IF_USE_TYPE_AP)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MAC80211 : INFO : For AP interface hostapd will handle this %s: %d\n", __func__, __LINE__);
      return;
   }

   if (instance->iw_mode != IW_MODE_MASTER)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MAC80211 : ERROR : %s: Not in master mode %s: %d\n", instance->dev->name, __func__, __LINE__);
      return -1;
   }

   if (instance->current_phy_mode != WLAN_PHY_MODE_802_11_G)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MAC80211 : ERROR : %s: Not in G mode %s: %d\n", instance->dev->name, __func__, __LINE__);
      return -1;
   }

   if (_BIT_GOT_SET(WLAN_ERP_INFO_NON_ERP_PRESENT))
   {
      instance->extended_rate_phy_info |= WLAN_ERP_INFO_NON_ERP_PRESENT;
   }
   else if (_BIT_GOT_UNSET(WLAN_ERP_INFO_NON_ERP_PRESENT))
   {
      instance->extended_rate_phy_info &= ~WLAN_ERP_INFO_NON_ERP_PRESENT;
   }

   if (_BIT_GOT_SET(WLAN_ERP_INFO_USE_PROTECTION))
   {
      instance->extended_rate_phy_info |= WLAN_ERP_INFO_USE_PROTECTION;
   }
   else if (_BIT_GOT_UNSET(WLAN_ERP_INFO_USE_PROTECTION))
   {
      instance->extended_rate_phy_info &= ~WLAN_ERP_INFO_USE_PROTECTION;
   }

   if (_BIT_GOT_SET(WLAN_ERP_INFO_BARKER_PREAMBLE_MODE))
   {
      instance->extended_rate_phy_info |= WLAN_ERP_INFO_BARKER_PREAMBLE_MODE;
   }
   else if (_BIT_GOT_UNSET(WLAN_ERP_INFO_BARKER_PREAMBLE_MODE))
   {
      instance->extended_rate_phy_info &= ~WLAN_ERP_INFO_BARKER_PREAMBLE_MODE;
   }

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Exit %s: %d\n", __func__, __LINE__);
   return 0;
}


static int _meshap_mac80211_get_supported_channels(struct net_device *dev, meshap_channel_info_t **supported_channels, int *count)
{
   meshap_instance_t     *instance;
   int                   i, j;
   int                   channel_count;
   meshap_channel_info_t *channels;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Enter %s: %d\n", __func__, __LINE__);
   instance = meshap_get_instance(dev);
   if (NULL == instance)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MAC80211 : ERROR : Instance is NULL %s: %d\n", __func__, __LINE__);
      return -1;
   }

   for (i = 0, channel_count = 0; i < instance->channel_count; i++)
   {
      if ((instance->channels[i].number == 0) ||
          (instance->channels[i].frequency == 0))
      {
         continue;
      }

      switch (instance->current_phy_mode)
      {
      case WLAN_PHY_MODE_802_11_A:
         if (!(instance->channels[i].flags & WLAN_CHANNEL_5GHZ &&
               instance->channels[i].flags & WLAN_CHANNEL_OFDM &&
               !(instance->channels[i].flags & WLAN_CHANNEL_ATHEROS_TURBO_MODE)))
         {
            continue;
         }
         break;

      case WLAN_PHY_MODE_802_11_B:
         if (!((instance->channels[i].flags & WLAN_CHANNEL_802_11_B) == WLAN_CHANNEL_802_11_B))
         {
            continue;
         }
         break;

      case WLAN_PHY_MODE_802_11_PURE_G:
      case WLAN_PHY_MODE_802_11_G:
         if (!((instance->channels[i].flags & WLAN_CHANNEL_802_11_G) == WLAN_CHANNEL_802_11_G))
         {
            continue;
         }
         break;
      case WLAN_PHY_MODE_802_11_AN:
      case WLAN_PHY_MODE_802_11_N:
      case WLAN_PHY_MODE_802_11_BGN:
      case WLAN_PHY_MODE_802_11_2GHZ_PURE_N:
      case WLAN_PHY_MODE_802_11_5GHZ_PURE_N:
         if (!((instance->channels[i].flags & WLAN_CHANNEL_802_11_N) == WLAN_CHANNEL_802_11_N))
         {
            continue;
         }
         break;
      case WLAN_PHY_MODE_802_11_AC:
      case WLAN_PHY_MODE_802_11_ANAC:
         if (!((instance->channels[i].flags & WLAN_CHANNEL_802_11_AC) == WLAN_CHANNEL_802_11_AC))
         {
            continue;
         }
         break;
      }
      ++channel_count;
   }

   channels = (meshap_channel_info_t *)kmalloc(sizeof(meshap_channel_info_t) * channel_count, GFP_ATOMIC);

   memset(channels, 0, sizeof(meshap_channel_info_t) * channel_count);

   for (i = 0, j = 0; i < instance->channel_count && j < channel_count; i++)
   {
      if ((instance->channels[i].number == 0) ||
          (instance->channels[i].frequency == 0))
      {
         continue;
      }

      switch (instance->current_phy_mode)
      {
      case WLAN_PHY_MODE_802_11_A:
         if (!(instance->channels[i].flags & WLAN_CHANNEL_5GHZ &&
               instance->channels[i].flags & WLAN_CHANNEL_OFDM &&
               !(instance->channels[i].flags & WLAN_CHANNEL_ATHEROS_TURBO_MODE)))
         {
            continue;
         }
         break;

      case WLAN_PHY_MODE_802_11_B:
         if (!((instance->channels[i].flags & WLAN_CHANNEL_802_11_B) == WLAN_CHANNEL_802_11_B))
         {
            continue;
         }
         break;

      case WLAN_PHY_MODE_802_11_PURE_G:
      case WLAN_PHY_MODE_802_11_G:
         if (!((instance->channels[i].flags & WLAN_CHANNEL_802_11_G) == WLAN_CHANNEL_802_11_G))
         {
            continue;
         }
         break;
      case WLAN_PHY_MODE_802_11_AN:
      case WLAN_PHY_MODE_802_11_N:
      case WLAN_PHY_MODE_802_11_BGN:
      case WLAN_PHY_MODE_802_11_2GHZ_PURE_N:
      case WLAN_PHY_MODE_802_11_5GHZ_PURE_N:
         if (!((instance->channels[i].flags & WLAN_CHANNEL_802_11_N) == WLAN_CHANNEL_802_11_N))
         {
            continue;
         }
         break;
      case WLAN_PHY_MODE_802_11_AC:
      case WLAN_PHY_MODE_802_11_ANAC:
         if (!((instance->channels[i].flags & WLAN_CHANNEL_802_11_AC) == WLAN_CHANNEL_802_11_AC))
         {
            continue;
         }
         break;
      }

      channels[j].flags         = instance->channels[i].flags;
      channels[j].frequency     = instance->channels[i].frequency;
      channels[j].max_power     = instance->channels[i].max_power;
      channels[j].max_reg_power = instance->channels[i].max_reg_power;
      channels[j].min_power     = instance->channels[i].min_power;
      channels[j].number        = instance->channels[i].number;

      j++;
   }

   *supported_channels = channels;
   *count = channel_count;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MAC80211 : INFO : count : %d %s: %d\n", *count,  __func__, __LINE__);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Exit %s: %d\n", __func__, __LINE__);
   return 0;
}


static unsigned char _meshap_mac80211_get_device_type(struct net_device *dev)
{
   meshap_instance_t *instance;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Enter %s: %d\n", __func__, __LINE__);
   if (dev == NULL)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MAC80211 : ERROR : DEV is NULL %s: %d\n", __func__, __LINE__);
      return -1;
   }

   instance = meshap_get_instance(dev);
   if (NULL == instance)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MAC80211 : ERROR : Instance is NULL %s: %d\n", __func__, __LINE__);
      return -1;
   }

   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MAC80211 : INFO : dev_type: %d %s: %d\n", instance->device_mode,  __func__, __LINE__);
   switch (instance->device_mode)
   {
   case MESHAP_DEV_MODE_NORMAL:
      return MESHAP_DEV_MODE_NORMAL;
   }

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Exit %s: %d\n", __func__, __LINE__);
   return -1;
}


static void _meshap_mac80211_set_device_type(struct net_device *dev, unsigned char device_type)
{
   meshap_instance_t *instance;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Enter %s: %d\n", __func__, __LINE__);
   instance = meshap_get_instance(dev);
   if (NULL == instance)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MAC80211 : ERROR : Instance is NULL %s: %d\n", __func__, __LINE__);
      return;
   }

   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MAC80211 : INFO : dev_type: %d %s: %d\n", device_type,  __func__, __LINE__);
   switch (device_type)
   {
   case MESHAP_DEV_MODE_NORMAL:
      instance->device_mode = MESHAP_DEV_MODE_NORMAL;
      break;
   }
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Exit %s: %d\n", __func__, __LINE__);
}

int meshap_get_interface_band(meshap_instance_t *instance)
{
    al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Enter %s: %d\n", __func__, __LINE__);
    al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MAC80211 : INFO : current_phy_mode: %d %s: %d\n", instance->current_phy_mode, __func__, __LINE__);
    switch(instance->current_phy_mode)
    {
        case WLAN_PHY_MODE_802_11_A:
        case WLAN_PHY_MODE_802_11_AN:
        case WLAN_PHY_MODE_802_11_5GHZ_PURE_N:
        case WLAN_PHY_MODE_802_11_ANAC:
        case WLAN_PHY_MODE_802_11_AC:
            return IEEE80211_BAND_5GHZ;
        case WLAN_PHY_MODE_802_11_B:
        case WLAN_PHY_MODE_802_11_G:
        case WLAN_PHY_MODE_802_11_PURE_G:
        case WLAN_PHY_MODE_802_11_BGN:
        case WLAN_PHY_MODE_802_11_2GHZ_PURE_N:
            return IEEE80211_BAND_2GHZ;
    }
    al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Exit %s: %d\n", __func__, __LINE__);
}


static HAL_RATE_TABLE* meshap_setup_rate_table(meshap_instance_t *instance)
{
   int                             i;
   HAL_RATE_TABLE                  *rate_table;
   unsigned char                   rate_index;
   struct wireless_dev             *wdev = instance->dev->ieee80211_ptr;
   struct ieee80211_supported_band *sband;
   struct ieee80211_sub_if_data    *sdata = IEEE80211_DEV_TO_SUB_IF(instance->dev);
   int                              ch_band = IEEE80211_BAND_2GHZ;


   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Enter %s: %d\n", __func__, __LINE__);
   rate_table = kmalloc(sizeof(HAL_RATE_TABLE), GFP_ATOMIC);
   if (rate_table == NULL)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MAC80211 : ERROR : Memory allocation failed %s: %d\n", __func__, __LINE__);
      return NULL;
   }

   ch_band = meshap_get_interface_band(instance); 
   sband = wdev->wiphy->bands[ch_band];	

   if (sband == NULL)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MAC80211 : ERROR : Memory allocation failed %s: %d\n", __func__, __LINE__);
      return NULL;
   }

   rate_table->rateCount = sband->n_bitrates;
   for (i = 0; i <= sband->n_bitrates; i++)
   {
      rate_table->info[i].rateKbps = sband->bitrates[i].bitrate;
   }

   instance->current_phy_mode_channel_count = sband->n_channels;
   instance->current_rate_table             = rate_table;
   _setup_supported_rates(instance);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Exit %s: %d\n", __func__, __LINE__);

    return rate_table;
}

void _openrt_ctrl_setup_rate_table(meshap_instance_t *instance, int phy_mode_changed, struct net_device *dev)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Enter %s: %d\n", __func__, __LINE__);
   if ((phy_mode_changed) && (instance->rate_ctrl != NULL))
   {
      open_rt_ctrl_destroy(instance->rate_ctrl);
      instance->rate_ctrl = NULL;
   }
   if (instance->rate_ctrl == NULL && instance->current_rate_table != NULL)
   {
      instance->rate_ctrl = open_rt_ctrl_create(instance,
                                                17,
                                                instance->current_rate_table->rateCount,
                                                (void *)instance,
                                                _on_open_rt_ctrl_create_item,
                                                _on_open_rt_ctrl_destroy_item,
                                                _on_open_rt_ctrl_process_item,
                                                _open_rt_ctrl_on_process_begin,
                                                _open_rt_ctrl_on_process_end);
	  if (instance->rate_ctrl == NULL) {
        al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MAC80211 : ERROR : Rate ctrl is NULL %s: %d\n", __func__, __LINE__);
		  return 0;
	  }
      atomic_set(&instance->power_save_sta_count, 0);
   }
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Exit %s: %d\n", __func__, __LINE__);
}


static int _meshap_mac80211_net_dev_enable_wep(struct net_device *dev)
{
   meshap_instance_t *instance;
   unsigned long     flags;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Enter %s: %d\n", __func__, __LINE__);
   instance = meshap_get_instance(dev);
   if (NULL == instance)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MAC80211 : ERROR : Instance is NULL %s: %d\n", __func__, __LINE__);
      return 0;
   }

   if (instance->use_type == AL_CONF_IF_USE_TYPE_AP)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MAC80211 : INFO : %s: Hostapd will take care of this interface %s: %d\n", instance->dev->name, __func__, __LINE__);
      return 0;
   }
   MESHAP_INSTANCE_LOCK(instance, flags);
   if (instance->flags & MESHAP_INSTANCE_FLAGS_WEP_ENABLE)
   {
      MESHAP_INSTANCE_UNLOCK(instance, flags);
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MAC80211 : INFO : %s: WEP is already enabled %s: %d\n", instance->dev->name, __func__, __LINE__);
      return 0;
   }
   instance->flags |= MESHAP_INSTANCE_FLAGS_WEP_ENABLE;
   MESHAP_INSTANCE_UNLOCK(instance, flags);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Exit %s: %d\n", __func__, __LINE__);
   return 0;
}

static int _meshap_mac80211_net_dev_disable_wep(struct net_device *dev)
{
   meshap_instance_t *instance;
   unsigned long     flags;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Enter %s: %d\n", __func__, __LINE__);
   instance = meshap_get_instance(dev);
   if (NULL == instance)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MAC80211 : ERROR : Instance is NULL %s: %d\n", __func__, __LINE__);
      return 0;
   }

   if (instance->use_type == AL_CONF_IF_USE_TYPE_AP)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MAC80211 : INFO : %s: Hostapd will take care of this interface %s: %d\n", instance->dev->name, __func__, __LINE__);
      return 0;
   }
   MESHAP_INSTANCE_LOCK(instance, flags);
   if (!(instance->flags & MESHAP_INSTANCE_FLAGS_WEP_ENABLE))
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MAC80211 : INFO : %s: WEP is already disabled %s: %d\n", instance->dev->name, __func__, __LINE__);
      MESHAP_INSTANCE_UNLOCK(instance, flags);
      return 0;
   }
   instance->flags &= ~MESHAP_INSTANCE_FLAGS_WEP_ENABLE;
   MESHAP_INSTANCE_UNLOCK(instance, flags);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Exit %s: %d\n", __func__, __LINE__);
   return 0;
}

static int _meshap_mac80211_net_dev_set_rsn_ie(struct net_device *dev, unsigned char *rsn_ie, int rsn_ie_length)
{
   meshap_instance_t *instance;
   unsigned long     flags;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Enter %s: %d\n", __func__, __LINE__);
   instance = meshap_get_instance(dev);
   if (NULL == instance)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MAC80211 : ERROR : Instance is NULL %s: %d\n", __func__, __LINE__);
      return 0;
   }

   if (instance->use_type == AL_CONF_IF_USE_TYPE_AP)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MAC80211 : INFO : %s: Hostapd will take care of this interface %s: %d\n", instance->dev->name, __func__, __LINE__);
      return 0;
   }
   MESHAP_INSTANCE_LOCK(instance, flags);
   if ((rsn_ie != NULL) && (rsn_ie_length > 0))
   {
      instance->rsn_ie_length = rsn_ie_length;
      memcpy(instance->rsn_ie_buffer, rsn_ie, instance->rsn_ie_length);
      instance->flags |= MESHAP_INSTANCE_FLAGS_RSN_IE_ENABLE;
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MAC80211 : INFO : %s: Enabling RSN IE %s: %d\n", instance->dev->name, __func__, __LINE__);
   }
   else
   {
      instance->flags &= ~MESHAP_INSTANCE_FLAGS_RSN_IE_ENABLE;
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MAC80211 : INFO : %s: Disabling RSN IE %s: %d\n", instance->dev->name, __func__, __LINE__);
   }

   MESHAP_INSTANCE_UNLOCK(instance, flags);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Exit %s: %d\n", __func__, __LINE__);
   return 0;
}


static int _meshap_mac80211_release_security_key(struct net_device *dev, int index, const u8 *mac_addr)
{
   meshap_instance_t                 *instance = meshap_get_instance(dev);
   struct wireless_dev               *wdev     = instance->dev->ieee80211_ptr;
   struct cfg80211_registered_device *rdev     = WIPHY_TO_DEV(wdev->wiphy);
   int ret;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Enter %s: %d\n", __func__, __LINE__);
   if (NULL == instance)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MAC80211 : ERROR : Instance is NULL %s: %d\n", __func__, __LINE__);
      return 0;
   }
   ASSERT_WDEV_LOCK(wdev); /* 1408MIG */

   wdev_lock(dev->ieee80211_ptr); /* 1408MIG */
   ret = rdev->ops->del_key(wdev->wiphy, dev, index, 1, mac_addr);
   wdev_unlock(dev->ieee80211_ptr); /* 1408MIG */
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Exit %s: %d\n", __func__, __LINE__);
   return ret;
}


void get_seq(void *cookie, struct key_params *params)
{
   struct key_params *params1 = (struct key_params *)cookie;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Enter %s: %d\n", __func__, __LINE__);
   params1->cipher  = params->cipher;
   params1->key     = params->key;
   params1->key_len = params->key_len;
   params1->seq     = params->seq;
   params1->seq_len = params->seq_len;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Exit %s: %d\n", __func__, __LINE__);
}


static int _meshap_mac80211_get_security_key_data(struct net_device *dev, int
                                                  index, meshap_security_key_data_t *security_key_data, const u8 *mac_addr)
{
   int               ret = -1;
   unsigned long     flags;
   struct key_params *params;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Enter %s: %d\n", __func__, __LINE__);
   meshap_instance_t   *instance = meshap_get_instance(dev);
   if (NULL == instance)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MAC80211 : ERROR : Instance is NULL %s: %d\n", __func__, __LINE__);
      return 0;
   }
   struct wireless_dev *wdev     =
      instance->dev->ieee80211_ptr;
   struct cfg80211_registered_device *rdev =
      WIPHY_TO_DEV(wdev->wiphy);
   params = (struct key_params *)kmalloc(sizeof(struct key_params), GFP_ATOMIC);

   if (params == NULL)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MAC80211 : ERROR : Params is NULL %s: %d\n", __func__, __LINE__);
      return 0;
   }

   ret = rdev->ops->get_key(wdev->wiphy, dev, index, 1, mac_addr, (void *)params, get_seq);
   if (ret != 0)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MAC80211 : ERROR : get_key failed %s: %d\n", __func__, __LINE__);
      kfree(params);
      return -1;
   }
   memset(security_key_data, 0, sizeof(meshap_security_key_data_t));

   MESHAP_INSTANCE_LOCK(instance, flags);

   if (index = 0 && (index > instance->max_keys) &&
               MESHAP_INSTANCE_IS_SLOT_MARKED(instance, index))
   {
      //printk(KERN_EMERG"DEBUG ----> <%s><%d>",__func__,__LINE__);
      security_key_data->cipher_type = params->cipher;
      // memcpy(&security_key_data->key_rsc,&instance->key_info[index].key_rsc,sizeof(security_key_data->key_rsc));
      memcpy(&security_key_data->key_tsc, params->seq, sizeof(params->seq_len));
      memcpy(&security_key_data->wep_iv, params->key, sizeof(params->key_len));
      ret = 0;
   }
   MESHAP_INSTANCE_UNLOCK(instance, flags);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Exit %s: %d\n", __func__, __LINE__);
   kfree(params);
   return ret;
}

static int _meshap_mac80211_net_dev_set_ht_capab(struct net_device *dev, al_net_if_ht_vht_capab_t* ht_vht_capab)
{
	meshap_instance_t *instance;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Enter %s: %d\n", __func__, __LINE__);
	instance = meshap_get_instance(dev);
	if (NULL == instance) {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MAC80211 : ERROR : Instance is NULL %s: %d\n", __func__, __LINE__);
		return -1;
	}
   if (instance->use_type == AL_CONF_IF_USE_TYPE_AP)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MAC80211 : INFO : %s: Hostapd will take care of this interface %s: %d\n", instance->dev->name, __func__, __LINE__);
      return 0;
   }

	instance->ht_capab_info = *((u16 *)ht_vht_capab->ht_vht_capabilities);
	instance->a_mpdu_params = ht_vht_capab->a_mpdu_params;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MAC80211 : INFO : a_mpdu_params: %u %s: %d\n", instance->a_mpdu_params, __func__, __LINE__);
	if(instance->ht_capab_info & HT_CAP_INFO_SUPP_CHANNEL_WIDTH_SET) {
		instance->sec_chan_offset = ht_vht_capab->sec_chan_offset;
	}
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Exit %s: %d\n", __func__, __LINE__);
	return 0;
}

static int _meshap_mac80211_net_dev_set_vht_capab(struct net_device *dev, al_net_if_ht_vht_capab_t* ht_vht_capab)
{
	meshap_instance_t *instance;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Enter %s: %d\n", __func__, __LINE__);
	instance = meshap_get_instance(dev);
	if (NULL == instance) {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MAC80211 : ERROR : Instance is NULL %s: %d\n", __func__, __LINE__);
		return -1;
	}
   if (instance->use_type == AL_CONF_IF_USE_TYPE_AP)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MAC80211 : INFO : %s: Hostapd will take care of this interface %s: %d\n", instance->dev->name, __func__, __LINE__);
      return 0;
   }

	instance->vht_capab_info = *((u32 *)ht_vht_capab->ht_vht_capabilities);
	instance->a_mpdu_params = ht_vht_capab->a_mpdu_params;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MAC80211 : INFO : a_mpdu_params: %u %s: %d\n", instance->a_mpdu_params, __func__, __LINE__);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Exit %s: %d\n", __func__, __LINE__);
	return 0;
}
static int _meshap_mac80211_net_dev_set_vht_opera(struct net_device *dev, meshap_802_11_vht_operation_t  vht_opera)
{
	meshap_instance_t *instance;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Enter %s: %d\n", __func__, __LINE__);
	instance = meshap_get_instance(dev);
	if (NULL == instance) {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MAC80211 : ERROR : Instance is NULL %s: %d\n", __func__, __LINE__);
		return -1;
	}
   if (instance->use_type == AL_CONF_IF_USE_TYPE_AP)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MAC80211 : INFO : %s: Hostapd will take care of this interface %s: %d\n", instance->dev->name, __func__, __LINE__);
      return 0;
   }

	instance->vht_opera.vht_op_info_chwidth = vht_opera.vht_op_info_chwidth;
	instance->vht_opera.vht_op_info_chan_center_freq_seg0_idx = vht_opera.vht_op_info_chan_center_freq_seg0_idx;
	instance->vht_opera.vht_op_info_chan_center_freq_seg1_idx = vht_opera.vht_op_info_chan_center_freq_seg1_idx;
	instance->vht_opera.vht_basic_mcs_set = vht_opera.vht_basic_mcs_set;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MAC80211 : INFO : mcs_set: %u center_freq_seg1_idx: %u center_freq_seg0_idx: %u vht_op_info_chwidth: %d %s: %d\n", vht_opera.vht_basic_mcs_set, vht_opera.vht_op_info_chan_center_freq_seg1_idx, vht_opera.vht_op_info_chan_center_freq_seg0_idx, vht_opera.vht_op_info_chwidth,  __func__, __LINE__);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Exit %s: %d\n", __func__, __LINE__);

	return 0;
}

static int _meshap_mac80211_get_stats(struct net_device *dev, al_net_if_stat_t *stats)
{
   meshap_instance_t *instance;

   instance = meshap_get_instance(dev);
   if (NULL == instance)
      return -1;

   memcpy(stats, &instance->net_if_stats, sizeof(al_net_if_stat_t));

   return 0;
}

static int _meshap_mac80211_get_debug_info(struct net_device *dev, debug_infra_info_t *debug_infra_info)
{
   meshap_instance_t *instance;
   wlan_channel_t wlan_channel;
   al_net_addr_t bssid;
   meshap_802_11_vht_operation_t  vht_operation;
   instance = meshap_get_instance(dev);
   if (NULL == instance)
   {
     return -1;
   }
   debug_infra_info->current_essid_length = instance->current_essid_length;
   strcpy(debug_infra_info->current_essid, instance->current_essid);
   debug_infra_info->current_phy_mode = instance->current_phy_mode;
   debug_infra_info->iw_mode = instance->iw_mode;
   debug_infra_info->use_type = instance->use_type;
   debug_infra_info->beacon_conf_complete = instance->beacon_conf_complete;
   debug_infra_info->channel_count = instance->channel_count;
   debug_infra_info->beacon_interval = instance->beacon_interval;
   debug_infra_info->rts_threshold = instance->rts_threshold;
   debug_infra_info->frag_threshold = instance->frag_threshold;
   debug_infra_info->tx_power = instance->tx_power;
   debug_infra_info->sec_chan_offset = instance->sec_chan_offset;
   debug_infra_info->scan_state = instance->scan_state;
   debug_infra_info->country_code = instance->country_code;
   debug_infra_info->ht_capab_info = instance->ht_capab_info;
   debug_infra_info->ht_opera = instance->ht_opera;
   debug_infra_info->vht_capab_info = instance->vht_capab_info;
   debug_infra_info->scan_start_time = instance->scan_start_time;
   debug_infra_info->scan_completion_time = instance->scan_completion_time;
   debug_infra_info->beacon_vendor_info_length = instance->beacon_vendor_info_length;
   strcpy(debug_infra_info->beacon_vendor_info, instance->beacon_vendor_info);
   debug_infra_info->connected = instance->connected;
   debug_infra_info->a_mpdu_params = instance->a_mpdu_params;
   debug_infra_info->fsm_state = instance->fsm_state;
   memcpy(&debug_infra_info->current_bssid, instance->current_bssid, sizeof(al_net_addr_t));
   memcpy(&debug_infra_info->current_channel, instance->current_channel, sizeof(wlan_channel_t));
   memcpy(&debug_infra_info->vht_opera, &instance->vht_opera, sizeof(meshap_802_11_vht_operation_t));
   return 0;
}


static int _meshap_mac80211_net_dev_set_ht_opera(struct net_device *dev, u16 ht_opera)
{
	meshap_instance_t *instance;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Enter %s: %d\n", __func__, __LINE__);
	instance = meshap_get_instance(dev);
	if (NULL == instance) {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MAC80211 : ERROR : Instance is NULL %s: %d\n", __func__, __LINE__);
		return -1;
	}
   if (instance->use_type == AL_CONF_IF_USE_TYPE_AP)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MAC80211 : INFO : %s: Hostapd will take care of this interface %s: %d\n", instance->dev->name, __func__, __LINE__);
      return 0;
   }

	instance->ht_opera = ht_opera;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MAC80211 : INFO : ht_operation: %u %s: %d\n", ht_opera, __func__, __LINE__);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Exit %s: %d\n", __func__, __LINE__);

	return 0;
}

static int _meshap_mac80211_set_reboot_in_progress_state(int value)
{
   struct sk_buff       *skb = NULL;
   extern struct sk_buff_head mgmt_work_queue;

	reboot_in_progress = value;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Enter %s: %d\n", __func__, __LINE__);
   //RESTART_MESHD: dequeue every skb
   if (reboot_in_progress) {
      skb = skb_dequeue(&mgmt_work_queue);
      while (skb)
      {
         tx_rx_pkt_stats.packet_drop[134]++;
         dev_kfree_skb(skb);
         skb = skb_dequeue(&mgmt_work_queue);
      }
   }
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Exit %s: %d\n", __func__, __LINE__);
	return 0;
}

static int _meshap_mac80211_get_hw_support_ht_capab(struct net_device *dev,tddi_ht_cap_t *ht_cap,tddi_vht_cap_t *vht_cap,unsigned char sub_type) //sowndarya validate changes
{
	meshap_instance_t *instance;
	struct wireless_dev *wdev;
	enum ieee80211_band band;
	struct ieee80211_supported_band *sband;	//sowndarya validate changes

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Enter %s: %d\n", __func__, __LINE__);
	instance = meshap_get_instance(dev);
	if (NULL == instance) {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MAC80211 : ERROR : Instance is NULL %s: %d\n", __func__, __LINE__);
		return -1;
	}

	wdev = instance->dev->ieee80211_ptr;
	if (NULL == wdev) {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MAC80211 : ERROR : wdev is NULL %s: %d\n", __func__, __LINE__);
		return -1;
	}

	if(TDDI_802_11_IS_N(sub_type))
	{
      if ((sub_type == AL_CONF_IF_PHY_SUB_TYPE_802_11_N_5GHZ) ||
			 (sub_type == AL_CONF_IF_PHY_SUB_TYPE_802_11_ANAC ) ||
			 (sub_type == AL_CONF_IF_PHY_SUB_TYPE_802_11_AN)) {
		    sband = wdev->wiphy->bands[NL80211_BAND_5GHZ];

      } else {
		    sband = wdev->wiphy->bands[NL80211_BAND_2GHZ];
      }
		memcpy(ht_cap,(tddi_ht_cap_t *)&(sband->ht_cap),sizeof(tddi_ht_cap_t));
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MAC80211 : INFO : phy type is N set HT Caps %s: %d\n", __func__, __LINE__);
		return 0;
	}
	else {
		if(TDDI_802_11_IS_AC(sub_type))
		{
			sband = wdev->wiphy->bands[IEEE80211_BAND_5GHZ];
			memcpy(ht_cap,(tddi_ht_cap_t *)&(sband->ht_cap),sizeof(tddi_ht_cap_t));
			memcpy(vht_cap,(tddi_vht_cap_t *)&(sband->vht_cap),sizeof(tddi_vht_cap_t));
         al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MAC80211 : INFO : phy type is AC set VHT Caps %s: %d\n", __func__, __LINE__);
			return 0;
		}
	}
   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MAC80211 : INFO : HW HT Capability info is not available for interface %s %s: %d\n", dev->name, __func__, __LINE__);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Exit %s: %d\n", __func__, __LINE__);
	return -1;
}

static int _meshap_mac80211_get_wm_duty_cycle_flag(struct net_device *dev, int *duty_cycle_flag, int *operating_channel)
{
    meshap_instance_t *instance;

    al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Enter %s: %d\n", __func__, __LINE__);
    instance = meshap_get_instance(dev);
    if (NULL == instance) {
        al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MAC80211 : ERROR : Instance is NULL %s: %d\n", __func__, __LINE__);
        return -1;
    }
    *duty_cycle_flag = instance->wm_duty_cycle_flag;
    if (instance->current_channel)
        *operating_channel = instance->current_channel->number;
    else
        *operating_channel = 0;
    return 0;
}
