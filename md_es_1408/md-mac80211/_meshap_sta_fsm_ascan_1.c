/********************************************************************************
* MeshDynamics
* --------------
* File     : _meshap_sta_fsm_ascan_1.c
* Comments : Atheros 802.11 FSM ascan_1 state code
* Created  : 10/13/2004
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  1  |01/07/2008| Check for mixed mode added.                     |Prachiti|
* -----------------------------------------------------------------------------
* |  0  |10/13/2004| Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

/**
 * This is a private source file included by meshap_sta_fsm.c, hence
 * the name begins with an underscore.
 * It was created to improve readability.
 */

OPENFSM_ENTER_STATE_FUNCTION(ascan_1)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Enter %s: %d\n", __func__, __LINE__);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Exit %s: %d\n", __func__, __LINE__);
   return 0;
}

OPENFSM_STATE_FUNCTION(ascan_1)
{
   _meshap_sta_fsm_data_t *fsm_data;
   _scan_param_t          *param;
   meshap_instance_t      *instance;
   int data_len = 0;
   struct ieee80211_mgmt *mgmt;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Enter %s: %d\n", __func__, __LINE__);
   fsm_data = (_meshap_sta_fsm_data_t *)openfsm_get_fsm_data(fsm);
   param    = (_scan_param_t *)param1;
   instance = fsm_data->instance;

   /**
    * Here we send out a probe request and move into the ascan_2 state
    * We are called from a non-interrupt context. We transmit at the
    * lowest rate.
    */
   meshap_mac80211_setup_mgmt_probe_req(instance, &mgmt, &data_len);

   data_len = sizeof(struct ieee80211_hdr);

   /* Call the mac80211 routine to send the PROBE request out */
   meshap_mac80211_tx_mgmt_packet(instance, mgmt, data_len);

   /**
    * Move onto the ascan_2 state and set the state data
    * as the param
    */

   kfree(mgmt);
   openfsm_set_state_data(fsm_data->fsm, (void *)param);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_DEBUG, "MAC80211 : DEBUG : Changing state to _MESHAP_STA_FSM_ASCAN_2 %s: %d\n", __func__, __LINE__);
   openfsm_transition(fsm_data->fsm, &_states[_MESHAP_STA_FSM_ASCAN_2]);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Exit %s: %d\n", __func__, __LINE__);
   return 0;
}

OPENFSM_LEAVE_STATE_FUNCTION(ascan_1)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Enter %s: %d\n", __func__, __LINE__);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Exit %s: %d\n", __func__, __LINE__);
   return 0;
}

OPENFSM_STATE_TIMER_FUNCTION(ascan_1)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Enter %s: %d\n", __func__, __LINE__);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Exit %s: %d\n", __func__, __LINE__);
   return 0;
}
