/********************************************************************************
* MeshDynamics
* --------------
* File     : _meshap_sta_fsm_openfsm_platform.c
* Comments : OpenFSM platform code
* Created  : 10/14/2004
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  0  |10/14/2004| Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

/**
 * This is a private source file included by meshap_sta_fsm.c, hence
 * the name begins with an underscore.
 * It was created to improve readability.
 */
static void *_openfsm_platform_malloc(unsigned int size)
{
   return kmalloc(size, GFP_ATOMIC);
}


static void _openfsm_platform_free(void *memblock)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Enter %s: %d\n", __func__, __LINE__);
   kfree(memblock);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Exit %s: %d\n", __func__, __LINE__);
}


static unsigned int _openfsm_platform_create_lock(void)
{
   _openfsm_lock_t *l;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Enter %s: %d\n", __func__, __LINE__);

   l = (_openfsm_lock_t *)kmalloc(sizeof(_openfsm_lock_t), GFP_ATOMIC);

   spin_lock_init(&l->sl);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Exit %s: %d\n", __func__, __LINE__);
   return (unsigned int)l;
}


static void _openfsm_platform_destroy_lock(unsigned int lock)
{
   _openfsm_lock_t *l;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Enter %s: %d\n", __func__, __LINE__);

   l = (_openfsm_lock_t *)lock;

   kfree(l);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Exit %s: %d\n", __func__, __LINE__);
}
