/********************************************************************************
* MeshDynamics
* --------------
* File     : meshap_sta_fsm.h
* Comments : Atheros AR 5210/5211/5212 state machine
* Created  : 10/1/2004
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  3  |4/6/2005  | Security changes                                | Sriram |
* -----------------------------------------------------------------------------
* |  2  |03/08/2005| Beacon Vendor Info added                        | Anand  |
* -----------------------------------------------------------------------------
* |  1  |1/15/2005 | Changed MESHAP_STA_FSM_TIMER_INTERVAL          | Sriram |
* -----------------------------------------------------------------------------
* |  0  |10/1/2004 | Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

#ifndef __MESHAP_STA_FSM_H__
#define __MESHAP_STA_FSM_H__

#if 1 //VIJAY_STA_HT : included the header
#include "linux/ieee80211.h"
#endif



#if !defined(MESHAP_STA_FSM_AUTH_RESP_TIMEOUT)
#define MESHAP_STA_FSM_AUTH_RESP_TIMEOUT    500
#endif

#if !defined(MESHAP_STA_FSM_ASSOC_RESP_TIMEOUT)
#define MESHAP_STA_FSM_ASSOC_RESP_TIMEOUT    500
#endif

#if !defined(MESHAP_STA_FSM_AUTH_STATE_TIMEOUT)
#define MESHAP_STA_FSM_AUTH_STATE_TIMEOUT    2000
#endif

#if !defined(MESHAP_STA_FSM_TIMER_INTERVAL)
#define MESHAP_STA_FSM_TIMER_INTERVAL    75
#endif

#if !defined(MESHAP_STA_FSM_PROBE_REQ_TIMEOUT)
#define MESHAP_STA_FSM_PROBE_REQ_TIMEOUT            (200)       /* in msecs */
#endif

#define MESHAP_GET_TIME_SPENT_IN_SCAN(instance)      (jiffies_to_msecs(jiffies - instance->scan_start_time))

struct meshap_sta_fsm_scan_result
{
   int            signal;
   int            channel;
   unsigned char  bssid[ETH_ALEN];                          /** The MAC address */
   unsigned short beacon_interval;                          /** Beacon interval in units of 1024 uS ~= 1 ms */
   unsigned short capability;                               /** 802.11 capability info field */
   char           ssid[WLAN_ESSID_MAX_SIZE + 1];            /** ESSID */
   unsigned char  supported_rates[WLAN_MAX_BIT_RATES];      /** Supported rates in 802.11 format */
   unsigned char  vendor_info_length;
   unsigned char  vendor_info[256];
   struct ieee80211_ht_cap ap_ht_capab;
   struct ieee80211_vht_cap ap_vht_capab;
};

typedef struct meshap_sta_fsm_scan_result   meshap_sta_fsm_scan_result_t;


struct _join_param
{
   char           essid[WLAN_ESSID_MAX_SIZE + 1];
   int            essid_length;
   unsigned char  bssid[6];
   int            channel;
   unsigned short algorithm;
   unsigned short atsn;
   unsigned short aid;
   unsigned char  supported_rates[8];
   unsigned char  *ie_in;
   int            ie_in_length;
   unsigned char  *ie_out;
};

typedef struct _join_param   _join_param_t;

int meshap_sta_fsm_initialize(meshap_instance_t *instance);
int meshap_sta_fsm_uninitialize(meshap_instance_t *instance);
int meshap_sta_fsm_start(meshap_instance_t *instance);
int meshap_sta_fsm_stop(meshap_instance_t *instance);

int meshap_sta_fsm_active_scan(meshap_instance_t *instance, int channel_count, unsigned char *channels, int dwell_timeout, meshap_sta_fsm_scan_result_t **results, int *results_count);
int meshap_sta_fsm_passive_scan(meshap_instance_t *instance, int channel_count, unsigned char *channels, int dwell_timeout, meshap_sta_fsm_scan_result_t **results, int *results_count);
int meshap_sta_fsm_join(meshap_instance_t *instance, const char *essid, int essid_length, unsigned char *bssid, int channel, unsigned char *ie_in, int ie_in_length, unsigned char *ie_out);
int meshap_sta_fsm_leave(meshap_instance_t *instance, int notify, unsigned short reason);
void meshap_sta_fsm_join_virtual(meshap_instance_t *instance);

int meshap_sta_fsm_process_mgmt_frame(meshap_instance_t *instance, struct sk_buff *skb);
int meshap_sta_fsm_miss_beacon(meshap_instance_t *instance);
void meshap_sta_fsm_reset(meshap_instance_t *instance);


#endif /*__MESHAP_STA_FSM_H__*/
