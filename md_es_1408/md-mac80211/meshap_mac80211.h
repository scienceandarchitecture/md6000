/********************************************************************************
* MeshDynamics
* --------------
* File     : meshap_mac80211.h
* Comments : Defines the prototype for the functions used for interfacing with mac80211
*            code
* Created  : 8/27/2014
* Author   : Vivek Gupta
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
********************************************************************************/

#include "al.h"
#include "al_802_11.h"


#include <linux/wait.h>
#include "../ieee80211_i.h"
#define INTERRUPTIBLE_SLEEP_ON_TIMEOUT(LOCKD_EXIT, HZ, WAIT) do {\
				prepare_to_wait(LOCKD_EXIT, WAIT, TASK_INTERRUPTIBLE);\
				schedule_timeout(HZ);\
				finish_wait(LOCKD_EXIT, WAIT);\
				} while(0)
#define WIPHY_TO_DEV wiphy_to_rdev


#define MESHAP_FALSE    0
#define MESHAP_TRUE     1
#define MESHAP_FAIL     -1

#define ADDR_IS_MULTICAST(addr) \
   (                            \
      (addr)[0] == 0x01 &&      \
      (addr)[1] == 0x00 &&      \
      (addr)[2] == 0x5E         \
   )

#define ADDR_IS_BROADCAST(addr) \
   (                            \
      (addr)[0] == 0xFF &&      \
      (addr)[1] == 0xFF &&      \
      (addr)[2] == 0xFF &&      \
      (addr)[3] == 0xFF &&      \
      (addr)[4] == 0xFF &&      \
      (addr)[5] == 0xFF         \
   )

#define SCAN_WAIT_TIME_PER_CHANNEL (300) //300 MILLI SEC

#define UPLINK_SCAN_REATTEMPT_DELAY       (10)  //10 MILLI SEC
#define NO_SCAN (-1)

struct _mgmt_frame_ie
{
   unsigned char *buffer;
   int           buffer_length;
   unsigned char eid;
   unsigned char length;
   unsigned char *data;
};

typedef struct _mgmt_frame_ie   _mgmt_frame_ie_t;

#define WMM_OUI_TYPE 2
#define WMM_OUI_SUBTYPE_INFORMATION_ELEMENT 0
#define WMM_OUI_SUBTYPE_PARAMETER_ELEMENT 1
#define WMM_OUI_SUBTYPE_TSPEC_ELEMENT 2
#define WMM_VERSION 1

#define WMM_AC_AIFSN_MASK 0x0f
#define WMM_AC_AIFNS_SHIFT 0
#define WMM_AC_ACM 0x10
#define WMM_AC_ACI_MASK 0x60
#define WMM_AC_ACI_SHIFT 5

#define WMM_AC_ECWMIN_MASK 0x0f
#define WMM_AC_ECWMIN_SHIFT 0
#define WMM_AC_ECWMAX_MASK 0xf0
#define WMM_AC_ECWMAX_SHIFT 4




struct meshap_wmm_ac_parameter {
    u8 aci_aifsn; /* AIFSN, ACM, ACI */
    u8 cw; /* ECWmin, ECWmax (CW = 2^ECW - 1) */
    __le16 txop_limit;
} __packed;

struct meshap_wmm_parameter_element {
    /* Element ID: 221 (0xdd); Length: 24 */
    /* required fields for WMM version 1 */
    u8 oui[3]; /* 00:50:f2 */
    u8 oui_type; /* 2 */
    u8 oui_subtype; /* 1 */
    u8 version; /* 1 for WMM version 1.0 */
    u8 qos_info; /* AP/STA specific QoS info */
    u8 reserved; /* 0 */
    struct meshap_wmm_ac_parameter ac[4]; /* AC_BE, AC_BK, AC_VI, AC_VO */

} __packed;


#define IS_HW_HAS_RATE_CTRL(rate_ctrl_flag)   (rate_ctrl_flag & MESHAP_HW_HAS_RATE_CONTROL)

int meshap_capture_duty_cycle_frames(struct sk_buff *skb,
                                     struct net_device *dev,
                                     int rssi,
                                     int rate);

void meshap_hook(struct sk_buff *skb, unsigned char pkt_type,uint8_t mon_flag);

/* Meshap scan completion handler function */
void meshap_notify_scan_completed(struct ieee80211_local *local);

int _get_next_ie(_mgmt_frame_ie_t *ie);

/* API used by mesahp to transmit the mgmt frame to meshap */
int meshap_mac80211_tx_mgmt_packet(meshap_instance_t *instance,
                                   struct ieee80211_mgmt *mgmt, int data_len);


/* API used by meshap to reset the device */
int mesh_mac80211_reset_device(struct net_device *dev, int phy_mode_change);

/* API used by meshap to set the channel for any device */
int meshap_mac80211_dev_set_channel(struct meshap_instance *instance,
                                    int channel, int quite);

int meshap_mac80211_set_short_slot_time(struct net_device *dev);

/* API to return torna_mac_hdr */
torna_mac_hdr_t *_meshap_get_torna_hdr(struct sk_buff *skb);

void *_meshap_get_drv_data(struct sk_buff *skb);

/* API to return TX torna_mac_hdr */
meshap_mac_hdr_t *_meshap_get_tx_meshap_mac_hdr(struct sk_buff *skb);
int meshap_reset(struct net_device *dev, int phy_mode_changed);

int meshap_mac80211_set_ap_mode(meshap_instance_t *instance);

int meshap_mac80211_set_tx_params(meshap_instance_t *instance);

void _meshap_send_assoc_response(struct net_device *dev, unsigned short stype, u8 *address0, u8 *address1, unsigned short aid,
                                 unsigned char ie_out_valid, al_802_11_information_element_t *ie_out, struct meshap_mac80211_sta_info sta);
void _meshap_send_deauth(struct net_device *dev, u8 *address0, u8 *address1);

void _meshap_send_auth_response(struct net_device *dev, u8 *address0, u8 *address1);

int meshap_setup_mgmt_probe_response_pkt(struct sk_buff *skb, meshap_instance_t *instance, struct ieee80211_mgmt **mgmt_pkt, int *data_len);

void _meshap_del_station(struct net_device *dev, u8 *station_addr);

int meshap_mac80211_setup_mgmt_probe_req(meshap_instance_t *instance, struct ieee80211_mgmt **mgmt_pkt, int *data_len);

void meshap_fill_drv_data(struct ieee80211_rx_status *rxs, int rate);
int _meshap_mac80211_send_dissassoc_from_station(struct net_device *dev, int flag);
enum ieee80211_band meshap_get_current_band(int current_phy_mode);
