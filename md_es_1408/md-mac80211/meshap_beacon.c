#include <meshap_instance.h>
#include <meshap_sta_fsm.h>
#include "meshap_round_robin.h"
#include <linux/module.h>
#include "linux/ieee80211.h"
#include <net/cfg80211.h>
#include <net/wireless/core.h>
#include <mac80211.h>
#include <net/mac80211/ieee80211_i.h>
#include "al.h"
//RRREVIEW
#define WIPHY_TO_DEV wiphy_to_rdev

void dump_bytes(char *, int);

#ifdef MESHAP_NO_POWER_SAVE_MODE
int meshap_setup_beacon_contents(meshap_instance_t *instance,
                                 unsigned char     for_probe_response,
                                 unsigned char     *buffer,
                                 int               *capability_ofset,
                                 int               *erp_offset,
                                 unsigned char     **beacon_vendor_info_offset,
                                 char              *essid,
                                 unsigned char     *rsn_ie_buffer,
                                 int               rsn_ie_length,
                                 int               *dtim_count_offset,
                                 int               hide_essid)
#else

int meshap_setup_beacon_contents(meshap_instance_t *instance,
                                 unsigned char     for_probe_response,
                                 unsigned char     *buffer,
                                 char              *essid,
                                 unsigned char     *rsn_ie_buffer,
                                 int               rsn_ie_length,
                                 int               hide_essid)
#endif
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MAC80211 : FLOW : Enter %s : %d\n", __func__,__LINE__);
   unsigned char                     *tail, *tailpos;
   unsigned char                     *p, *pos;
   unsigned short                    s;
   int                               head_len = 0, tail_len = 0, len = 0, err;
   struct wireless_dev               *wdev = instance->dev->ieee80211_ptr;
   struct cfg80211_registered_device *rdev = WIPHY_TO_DEV(wdev->wiphy);
   struct cfg80211_ap_settings       params;
   struct ieee80211_mgmt             *head;
   char                              length = 0;
   enum nl80211_channel_type channel_type = NL80211_CHAN_NO_HT;

   if(is_11n_supported(instance , 0)){
          channel_type = NL80211_CHAN_HT20 ;
   }

#define host_to_le16(n)              (n)
#define BEACON_HEAD_BUF_SIZE    256
#define BEACON_TAIL_BUF_SIZE    512
#define IEEE80211_FC(type, stype)    host_to_le16((type << 2) | (stype << 4))


   head = (struct ieee80211_mgmt *)kmalloc(BEACON_HEAD_BUF_SIZE, GFP_ATOMIC);
   if (NULL == head)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MAC80211 : ERROR : Failed to allocate memory %s: %d\n", __func__, __LINE__);
      return 0;
   }

   memset(head, 0, sizeof(struct ieee80211_mgmt));
   tail_len = BEACON_TAIL_BUF_SIZE;

   tailpos = tail = kmalloc(tail_len, GFP_ATOMIC);
   if (NULL == tail)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MAC80211 : ERROR : Failed to allocate memory %s: %d\n", __func__, __LINE__);
      kfree(head);
      return 0;
   }

   memset(tail, 0, tail_len);
   head->frame_control = cpu_to_le16(IEEE80211_FC(WLAN_FC_TYPE_MGMT, WLAN_FC_STYPE_BEACON));

   head->duration = cpu_to_le16(0);
   memset(head->da, 0xff, ETH_ALEN);
   memcpy(head->sa, instance->dev->dev_addr, ETH_ALEN);
   memcpy(head->bssid, instance->dev->dev_addr, ETH_ALEN);
   head->u.beacon.beacon_int = cpu_to_le16(instance->beacon_interval);
   head->u.beacon.capab_info = cpu_to_le16(instance->capability);
   memset(&head->u.beacon.timestamp, 0, sizeof(head->u.beacon.timestamp));
   pos = &head->u.beacon.variable[0];

   p = buffer;

   memset(p, 0, 8);
   p += 8;                  /** Timestamp (TSF) */

   /** Beacon interval */

   _PUT_LE_SHORT(cpu_to_le16(instance->beacon_interval));

   /** Capability */

#ifdef MESHAP_NO_POWER_SAVE_MODE
   *capability_ofset = (p - buffer);
#endif

   s = instance->capability;

   if (for_probe_response)
   {
      char *current_essid;
      int  current_essid_length;

      current_essid_length = 0;
      current_essid        = instance->current_essid;
      current_essid_length = instance->current_essid_length;

      if (strcmp(current_essid, essid))
      {
         if ((rsn_ie_length > 0) && (rsn_ie_buffer != NULL))
         {
            s |= WLAN_CAPABILITY_PRIVACY;
         }
         else
         {
            s &= ~WLAN_CAPABILITY_PRIVACY;
         }
      }
   }
   _PUT_LE_SHORT(s);

   /** ESSID */

   if (!hide_essid)
   {
      p[0] = WLAN_EID_SSID;
      len  = strlen(essid);
      p[1] = len;
      memcpy(&p[2], essid, len);
      memcpy(pos, p, len + 2);
      p   += len + 2;
      pos += len + 2;
   }
   else
   {
      p[0] = WLAN_EID_SSID;
      p[1] = 0;
      memcpy(pos, p, 2);
      p   += 2;
      pos += 2;
   }

   /** Supported rates */
   p[0] = WLAN_EID_SUPP_RATES;
   p[1] = instance->supported_rates_length;
   memcpy(&p[2], instance->supported_rates, instance->supported_rates_length);
   memcpy(pos, p, instance->supported_rates_length + 2);
   p   += instance->supported_rates_length + 2;
   pos += instance->supported_rates_length + 2;

   /** DS Parameter set */

   p[0] = WLAN_EID_DS_PARAMS;
   p[1] = 1;
   p[2] = instance->current_channel->number;
   memcpy(pos, p, 3);
   p       += 1 + 2;
   pos     += 1 + 2;
   head_len = pos - (u8 *)head;

   /**
    * ERP Information
    */

   if ((instance->current_phy_mode == WLAN_PHY_MODE_802_11_G) ||
       (instance->current_phy_mode == WLAN_PHY_MODE_802_11_PURE_G))
   {
#ifdef MESHAP_NO_POWER_SAVE_MODE
      *erp_offset = (p - buffer);
#endif

      p[0] = WLAN_EID_ERP_INFO;
      p[1] = 1;
      p[2] = instance->extended_rate_phy_info;
      memcpy(tailpos, p, 3);
      p       += 1 + 2;
      tailpos += 1 + 2;
   }

   /*Check Is 80211n supported If yes add appropriate IE's In packets*/
   if(is_11n_supported(instance, 0)) {
	   /*HT capability IE(45)*/
	   if (!meshap_build_ht_capability_ie(instance, p)){
		   memcpy(tailpos, p, p[1]+2);
		   tailpos += p[1] + 2;
		   p       += p[1] + 2;
	   }

	   /*HT Operation IE(61)*/
	   if (!meshap_build_ht_operation_ie(instance, p)){
		   memcpy(tailpos, p, p[1]+2);
		   tailpos += p[1] + 2;
		   p       += p[1] + 2;
	   }
   }

   if(is_11ac_supported(instance)) {
       /*VHT capability IE(191)*/
       if (!meshap_build_vht_capability_ie(instance, p)){
           memcpy(tailpos, p, p[1]+2);
           tailpos += p[1] + 2;
           p       += p[1] + 2;
       }

       /*VHT Operation IE(192)*/
       if (!meshap_build_vht_operation_ie(instance, p)){
           memcpy(tailpos, p, p[1]+2);
           tailpos += p[1] + 2;
           p       += p[1] + 2;
       }
   }

   /*WMM/WME IE(221)*/
   if (!meshap_build_wmm_ie(instance, p)) {
       memcpy(tailpos, p, p[1]+2);
       tailpos += p[1] + 2;
       p       += p[1] + 2;
   }

   /**
    * Extended supported rates
    */

   if (instance->extended_rates_length > 0)
   {
      if ((for_probe_response && (instance->beacon_vendor_info_length <= 0)) ||
          !for_probe_response)
      {
         p[0] = WLAN_EID_EX_SUPP_RATES;
         p[1] = instance->extended_rates_length;
         memcpy(&p[2], instance->extended_rates, instance->extended_rates_length);
         memcpy(tailpos, p, instance->extended_rates_length + 2);
         p       += (instance->extended_rates_length + 2);
         tailpos += (instance->extended_rates_length + 2);
      }
   }

   if ((rsn_ie_length > 0) && (rsn_ie_buffer != NULL))
   {
      memcpy(p, rsn_ie_buffer, rsn_ie_length);
      memcpy(tailpos, p, rsn_ie_length);
      p       += rsn_ie_length;
      tailpos += rsn_ie_length;
   }

   if (!for_probe_response)
   {
      length = meshap_round_robin_set_beacon_content(instance, p);
   }
   memcpy(tailpos, p, length);
   p       += length;
   tailpos += length;

   /**
    * Make sure the original IMCP handshake Beacon Vendor Info
    * is always the last WLAN_EID_VENDOR_PRIVATE based element so as to
    * not break compatibility with older version mesh nodes
    */

   if ((instance->beacon_vendor_info_length > 0) &&
       !for_probe_response)
   {
      p[0] = WLAN_EID_VENDOR_PRIVATE;
      p[1] = instance->beacon_vendor_info_length;
#ifdef MESHAP_NO_POWER_SAVE_MODE
      *beacon_vendor_info_offset = p + 2;
#endif
      memcpy(&p[2], instance->beacon_vendor_info, instance->beacon_vendor_info_length);
      memcpy(tailpos, p, instance->beacon_vendor_info_length + 2);
      p       += (instance->beacon_vendor_info_length + 2);
      tailpos += (instance->beacon_vendor_info_length + 2);
   }

   tail_len = tailpos > tail ? tailpos - tail : 0;

   /*Send the beacon*/
   memset(&params, 0, sizeof(struct cfg80211_ap_settings));

   params.beacon_interval = instance->beacon_interval;
#if 0
   /*RRREVIEW*/printk("DEBUG: BEACON INTERVAL = %d inst = %d\n",params.beacon_interval, instance->beacon_interval);
#endif
   params.dtim_period     = instance->dtim_count;
   params.beacon.head     = (u8 *)head;
   params.beacon.head_len = head_len;
   params.beacon.tail     = tail;
   params.beacon.tail_len = tail_len;
   params.ssid            = (char *)kmalloc(strlen(essid), GFP_ATOMIC);
   memcpy((char *)params.ssid, essid, strlen(essid));
   params.ssid_len = strlen(essid);


   if(is_11n_supported(instance, 0) || is_11ac_supported(instance)) {
       int ret_val = 0;
       ret_val = meshap_80211n_allowed_ht40_channel_pair(instance);
       if (!ret_val){
            al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MAC80211 : INFO : HT40 channel configuration failed %s: %d\n", __func__, __LINE__);
       }else
       {
   /*update channel_type based on secondary channel offset*/
	if(instance->sec_chan_offset == 1) {
		channel_type = NL80211_CHAN_HT40PLUS;
	}else if(instance->sec_chan_offset == -1) {
		channel_type = NL80211_CHAN_HT40MINUS;
	}
       }
   }

   /*update channel_type based on secondary channel offset*/
    cfg80211_chandef_create(&params.chandef, 
						ieee80211_get_channel(wdev->wiphy, instance->current_channel->frequency),
						channel_type);

           

    if(is_11ac_supported(instance)){
        if (instance->vht_opera.vht_op_info_chwidth == 1 ){
            /* primary 40 part must match the HT configuration */
            int tmp = (30 + instance->current_channel->frequency - 5000 - instance->vht_opera.vht_op_info_chan_center_freq_seg0_idx * 5)/20;
            tmp /= 2;
            if (params.chandef.center_freq1 != 5000 + instance->vht_opera.vht_op_info_chan_center_freq_seg0_idx * 5 - 20 + 40 * tmp)
                goto out ;
            params.chandef.center_freq1 = 5000 + instance->vht_opera.vht_op_info_chan_center_freq_seg0_idx * 5;
            params.chandef.center_freq2 = 0 ;
            params.chandef.width = NL80211_CHAN_WIDTH_80 ;
        }
        else if ((instance->vht_opera.vht_op_info_chwidth == 2)){
            al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MAC80211 : INFO : currently 160MHz channel width not supported for VHT operation %s: %d\n", __func__, __LINE__);
            params.chandef.width = NL80211_CHAN_WIDTH_20 ;
        }
        else if ((instance->vht_opera.vht_op_info_chwidth == 3)){
            al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MAC80211 : INFO : currently 80+80MHz channel width not supported for VHT operation %s: %d\n", __func__, __LINE__);
            params.chandef.width = NL80211_CHAN_WIDTH_20 ;
        }
    }

out:

   params.beacon.probe_resp     = NULL;
   params.beacon.probe_resp_len = 0;

   /*TBD: Add other params fields like auth later */
   wdev_lock(wdev); /* 1408MIG */
   err = rdev->ops->change_beacon(&(rdev->wiphy), instance->dev, &params.beacon);
   wdev_unlock(wdev); /* 1408MIG */

	if(instance->beacon_conf_complete) {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_DEBUG, "MAC80211 : DEBUG : Invoke start_ap %s: %d\n", __func__, __LINE__);
      err = rdev->ops->start_ap(&rdev->wiphy, instance->dev, &params);
	}

   kfree(head);
   kfree(tail);
   kfree(params.ssid);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MAC80211 : FLOW : Exit %s : %d\n", __func__,__LINE__);
   return(p - buffer);
}


#ifdef MESHAP_NO_POWER_SAVE_MODE
int meshap_setup_beacon_frame(meshap_instance_t *instance, int *capability_ofset, int *erp_offset, unsigned char **beacon_vendor_info_offset, int *dtim_count_offset)
#else
int meshap_setup_beacon_frame(meshap_instance_t *instance)
#endif
{
   unsigned char  *p;
   unsigned char  *buf = NULL;
   unsigned short s         = 0;
   int            content_length;
   unsigned char  *rsn_ie_buffer;
   int            rsn_ie_length;
   char           *current_essid;
   int            current_essid_length;
   unsigned char  bcast_addr[6] = { 0xff, 0xff, 0xff, 0xff, 0xff, 0xff };

   buf = (unsigned char*)kmalloc(1024, GFP_ATOMIC);
   if(buf == NULL) {
	   return 0;
   }
   memset(buf, 0, 1024);

   p = buf;
   current_essid_length = 0;

   WLAN_FC_SET_TYPE(s, WLAN_FC_TYPE_MGMT);
   WLAN_FC_SET_STYPE(s, WLAN_FC_STYPE_BEACON);

   _PUT_LE_SHORT(cpu_to_le16(s)); /** Frame control */
   _PUT_LE_SHORT(0);              /** Duration/Id 0 for now */

   memcpy(p, bcast_addr, ETH_ALEN);
   p += ETH_ALEN;       /* Address 1 */
   memcpy(p, instance->dev->dev_addr, ETH_ALEN);
   p += ETH_ALEN;       /* Address 2 (BSSID) */
   memcpy(p, instance->dev->dev_addr, ETH_ALEN);
   p += ETH_ALEN;       /* Address 3 (Source) */

   _PUT_LE_SHORT(0);    /* SQNR */

   if (instance->flags & MESHAP_INSTANCE_FLAGS_RSN_IE_ENABLE)
   {
      rsn_ie_buffer = instance->rsn_ie_buffer;
      rsn_ie_length = instance->rsn_ie_length;
   }
   else
   {
      rsn_ie_buffer = NULL;
      rsn_ie_length = 0;
   }

   current_essid        = instance->current_essid;
   current_essid_length = instance->current_essid_length;

#ifdef MESHAP_NO_POWER_SAVE_MODE
   content_length = meshap_setup_beacon_contents(instance,
                                                 0,
                                                 p,
                                                 capability_ofset,
                                                 erp_offset,
                                                 beacon_vendor_info_offset,
                                                 current_essid,
                                                 rsn_ie_buffer,
                                                 rsn_ie_length,
                                                 dtim_count_offset,
                                                 instance->hide_essid);

   *capability_ofset  += (p - (item->data + instance->beacon_buffer_padding));
   *erp_offset        += (p - (item->data + instance->beacon_buffer_padding));
   *dtim_count_offset += (p - (item->data + instance->beacon_buffer_padding));
#else
   content_length = meshap_setup_beacon_contents(instance,
                                                 0,
                                                 p,
                                                 current_essid,
                                                 rsn_ie_buffer,
                                                 rsn_ie_length,
                                                 instance->hide_essid);
#endif
   kfree(buf);
   return 0;
}


void _meshap_init_beacon_buffer(meshap_instance_t *instance)
{
   int length;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MAC80211 : FLOW : Enter %s : %d\n", __func__,__LINE__);
#ifdef MESHAP_NO_POWER_SAVE_MODE
   instance->beacon_vendor_info_offset = NULL;
#endif

   instance->beacon_vendor_info_updated = 0;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Enter %s: %d\n", __func__, __LINE__);
#ifdef MESHAP_NO_POWER_SAVE_MODE
   length = meshap_setup_beacon_frame(instance,
                                      &instance->beacon_capability_offset,
                                      &instance->beacon_erp_offset,
                                      &instance->beacon_vendor_info_offset,
                                      &instance->dtim_count_offset);
#else
   length = meshap_setup_beacon_frame(instance);
#endif
   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MAC80211 : INFO : length : %d %s: %d\n", length, __func__, __LINE__);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MAC80211 : FLOW : Exit %s : %d\n", __func__,__LINE__);
}

