/********************************************************************************
* MeshDynamics
* --------------
* File     : openobj.h
* Comments : Open Object Main Header
* Created  : 11/21/2004
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  1  |12/14/2004| Misc changes                                    | Sriram |
* -----------------------------------------------------------------------------
* |  0  |11/21/2004| Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

#ifndef __OPENOBJ_H__
#define __OPENOBJ_H__
#include "al.h"
/**
 * Implementations need to define the following
 * macros. The
 *
 * Mandatory
 * ---------
 *	#define OPENOBJ_ATOMIC												< type of atomic integer >
 *	#define OPENOBJ_ATOMIC_SET(OPENOBJ_ATOMIC variable,int value)		< way to set the value >
 *	#define OPENOBJ_ATOMIC_GET(OPENOBJ_ATOMIC variable)					< way to get the value >
 *	#define OPENOBJ_ATOMIC_INC(OPENOBJ_ATOMIC variable)					< way to incr the value >
 *	#define OPENOBJ_ATOMIC_DEC_AND_TEST(OPENOBJ_ATOMIC variable)		< way to decr and test the value >
 *	#define OPENOBJ_ATOMIC_DEC(OPENOBJ_ATOMIC variable)					< way to decr the value >
 *	#define OPENOBJ_MALLOC(int size)									< way to allocate memory >
 *	#define OPENOBJ_FREE(void* ptr)										< way to free memory >
 * %	#define OPENOBJ_INLINE												< way to make functions inline >
 *
 * Optional
 * --------
 *	#define OPENOBJ_TIMESTAMP()											< timestamp in millisecond resolution >
 *	#define OPENOBJ_INTERRUPT_LOCK()									< way to disable interrupts >
 *	#define OPENOBJ_INTERRUPT_UNLOCK(unsigned int flags)				< way to enable interrupts >
 *	#define OPENOBJ_PRINTF												< way to print messages >
 *	#define OPENOBJ_CREATE_THREAD(int (*func)(void* data),void* data)	< way to create a thread >
 *	#define OPENOBJ_THREAD_INIT()										< way to initialize a thread >
 *	#define OPENOBJ_THREAD_UNINIT()										< way to un-initialize a thread >
 *	#define OPENOBJ_THREAD_SLEEP(int milliseconds)						< way to sleep >
 */

typedef void (*openobj_destroy_t)(void *object);

struct open_object
{
   unsigned int      signature;
   OPENOBJ_ATOMIC    reference_count;
   openobj_destroy_t destroy;
};

typedef struct open_object   open_object_t;

static OPENOBJ_INLINE void *openobj_create(unsigned int signature, unsigned int object_size, openobj_destroy_t destroy)
{
   open_object_t *obj;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MAC80211 : FLOW : Enter %s: %d\n", __func__, __LINE__);
   obj = (open_object_t *)OPENOBJ_MALLOC(object_size);

   memset(obj, 0, object_size);

   obj->signature = signature;
   obj->destroy   = destroy;

   OPENOBJ_ATOMIC_SET(obj->reference_count, 1);


   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MAC80211 : FLOW : Exit %s : %d\n", __func__,__LINE__);
   return (void *)obj;
}


#define OPENOBJ_RETURN_REFERENCE(obj)                                 \
   do {                                                               \
      if (obj != NULL) {                                              \
         OPENOBJ_ATOMIC_INC(((open_object_t *)obj)->reference_count); \
      }                                                               \
      return obj;                                                     \
   } while (0)

#define OPENOBJ_ADD_REFERENCE(obj)                                    \
   do {                                                               \
      if (obj != NULL) {                                              \
         OPENOBJ_ATOMIC_INC(((open_object_t *)obj)->reference_count); \
      }                                                               \
   } while (0)

#define OPENOBJ_RELEASE_REFERENCE(obj)                                               \
   do {                                                                              \
      if (obj != NULL) {                                                             \
         if (OPENOBJ_ATOMIC_DEC_AND_TEST(((open_object_t *)obj)->reference_count)) { \
            if (((open_object_t *)obj)->destroy != NULL) {                           \
               ((open_object_t *)obj)->destroy(obj);                                 \
            }                                                                        \
            OPENOBJ_FREE(obj);                                                       \
         }                                                                           \
      }                                                                              \
   } while (0)

#endif /*__OPENOBJ_H__*/
