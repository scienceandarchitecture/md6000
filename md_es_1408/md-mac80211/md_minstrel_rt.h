#ifndef __MD_MINSTREL_H
#define __MD_MINSTREL_H

#include <linux/netdevice.h>
#include <linux/types.h>
#include <linux/skbuff.h>
#include <linux/debugfs.h>
#include <linux/random.h>
#include <linux/ieee80211.h>
#include <linux/slab.h>
#include <net/mac80211.h>
#include "../rate.h"
#include "../rc80211_minstrel.h"
#include "../sta_info.h"
#include "meshap.h"

int mins_rt_ctrl_get_item(struct meshap_instance *instance, unsigned char *address, meshap_rate_ctrl_info_t *rate_ctrl_info);
struct minstrel_rate *minstrel_get_sta_rate_ctrl_info(void *priv_sta);

#endif
