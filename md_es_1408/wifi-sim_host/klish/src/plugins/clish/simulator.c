/*
 *simulator.c
 */
#include "private.h"
#include "lub/string.h"
#include "lub/argv.h"
#include "lub/conv.h"

#include <assert.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/file.h>
#include <signal.h>
#include <fcntl.h>

#include <sys/socket.h>
#include <sys/types.h>
#include <netdb.h>

#include <arpa/inet.h>

#include <netinet/in.h>

#include <unistd.h>

#define SERVERPORT "4950"

int get_klish_socket();
int sim_cmd_handler (const char *cmd);
int klish_send_command( int sock, const char* cmd );
/*----------------------------------------------------------- */
/* Show the status of the simulator Network */
CLISH_PLUGIN_SYM(sim_clbk_handler)
{
    clish_shell_t *this = clish_context__get_shell(clish_context);

    const char *arg = script;
    char str[100]="python Config_Helper.py ";
    int ret1,ret = 0;

//    printf ( " Calling the Call back \n");
//    printf ( " Received cmd is %s  \n", arg );
    strcat(str,arg);
    ret1= system(str);
  //  printf ("%s%d",str,ret1);
    if (ret1!=NULL)
	return -1;
    else
    /**
     * Create a client socket to the MasterAppClient 
     * Send the cmd as a buffer 
     * wait for the response - BLOCKING Call 
     */

    ret = sim_cmd_handler(arg);

    return 0;
}


CLISH_PLUGIN_SYM(sim_clbk_delete_handler)
{
    clish_shell_t *this = clish_context__get_shell(clish_context);

    const char *arg = script;
    int ret = 0;

  //  printf ( " Calling the Call back \n");
  //  printf ( " Received cmd is %s  \n", arg );

    /**
     * Create a client socket to the MasterAppClient 
     * Send the cmd as a buffer 
     * wait for the response - BLOCKING Call 
     */

    ret = sim_cmd_handler(arg);
 
    return 0;
}


int send_request (const char *cmd)
{
  //  printf( "Received command is %s \n", cmd );

    int ret = 0;

    ret = sim_cmd_handler(cmd);
    if ( ret < -1){
        printf ( " < %s > : send command failed \n",__func__);
        return ret;
    }

    return 0;
}

int sim_cmd_handler( const char *cmd)
{
    int ret = 0;
    int sock = 0;
    int numbytes = 0;

    struct addrinfo hints;
    struct addrinfo *servinfo = NULL ;
    struct addrinfo *p = NULL;


    memset(&hints,0x00,sizeof(struct addrinfo));
    hints.ai_family     = PF_INET;
    hints.ai_socktype   = SOCK_DGRAM;
    hints.ai_flags      = AI_PASSIVE;

    /** Get the server info to communicate **/
    if ( ( ret = getaddrinfo(NULL, SERVERPORT, &hints, &servinfo) ) != 0 ){
        printf ( " < %s > : Command send failed \n",__func__);
        return -1;
    }

    /** Create a socket similar to the Server **/
    for ( p = servinfo; p != NULL; p = p->ai_next){
        sock = socket(p->ai_family, p->ai_socktype, p->ai_protocol);

        if ( sock < -1 ){
            printf ( " < %s > : Creation failed \n",__func__);
            continue;
        }
        break; //connect hence breakout 
    }


    /** Send the msg to MAC **/
    printf(" Sending the cmd to MAC : \" %s \" \n", cmd);
    if ((numbytes = sendto(sock, cmd, strlen(cmd), 0, 
                    p->ai_addr, p->ai_addrlen)) == -1) {
        printf (" sento failed ");
        return -1;
    }
    //printf(" Klish : Sent %d bytes \n", numbytes);


    /** waiting for the response **/
   // printf(" Klish: waiting to recv response ...\n");

    socklen_t addr_len;
    struct sockaddr_storage their_addr;

    addr_len = sizeof( struct sockaddr_storage);
    char buf[4096];
    if ((numbytes = recvfrom(sock, &buf, 4096 , 0,
                    (struct sockaddr *)&their_addr, &addr_len)) == -1) {
        perror("recvfrom");
        exit(1);
    }

  //  printf(" Received : packet is %d bytes long\n", numbytes);
    buf[numbytes] = '\0';
    printf(" Klish response : \" %s \" \n", buf);

    freeaddrinfo(servinfo);
    close(sock);

    return 0;
}
