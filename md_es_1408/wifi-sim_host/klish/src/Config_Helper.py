from subprocess import Popen,PIPE,STDOUT
import sys
import re
vm_param_list=list()

vm_param_check_list=["mgmt_ip","mgmt_mac","mip_ip","model_number","server_ip","server_port","vm_imag_path","vm_coordinates"]

def vm_data(output,param_name):
	output= iter(output.split("\n"))
	for line in output:
		vm = re.search(param_name, line)
  		if vm:
	       		vm_param_list.append(line.split()[1])


def vm_helper(nw_name,vm_name,param_name,param_val):
        cmd="./bin/konf -- -d"
        p=Popen(cmd, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
        output = p.stdout.read()
	vm_data(output,param_name)
	if param_val in vm_param_list:
		print "given value is already exist,enter another value"
		sys.exit(1)


def vm_status_helper(nw_name,vm_name):
	cmd="./bin/konf -- -d \"nw_name "+nw_name+"\""+" -r "+"\"vm_name "+vm_name+"\""
	p=Popen(cmd, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
        output = p.stdout.read()
	for j in vm_param_check_list:
		if j not in output:
			return 1
			break


def nw_status_helper(nw_name,param_name,param_val):
	if param_val == "enable":
		cmd="./bin/konf -- -d \"nw_name "+nw_name+"\""
		p=Popen(cmd, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
        	output = p.stdout.read()
		vm_data(output,"vm_name")
		print vm_param_list
		for i in vm_param_list:
			return_data=vm_status_helper(nw_name,i)
			if return_data:
				print "please first fill all the required data for ",i
                       		sys.exit(1)
		if len(vm_param_list) == 0:
			print "At least one vm should be there"
			sys.exit(1)

def vm_mac_helper(nw_name,vm_name,param_name,param_val):
	cmd="./bin/konf -- -d"
        p=Popen(cmd, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
        output = p.stdout.read()
	vm_data(output,param_name)
	if len(vm_param_list) == 0:
		data_user=param_val.split(":")
		data_user_str=data_user[3]+data_user[4]+data_user[5]
		if int(data_user_str,16) > 16777194:
			print "given range mac address already exist or not valid, please enter different mac address"
			sys.exit(1)
	else:
		for i in vm_param_list:
			data=i.split(":")
			data_user=param_val.split(":")
			data_str=data[3]+data[4]+data[5]
			data_user_str=data_user[3]+data_user[4]+data_user[5]
			mac_diff= abs(int(data_user_str,16)-int(data_str,16))
			if mac_diff < 20 or int(data_user_str,16) > 16777194:
				print "given range mac address already exist or not valid, please enter different mac address"
				sys.exit(1)



if __name__ == "__main__":
        length= len(sys.argv)
        if length == 5:
                nw_name=sys.argv[1]
                vm_name=sys.argv[2]
                param_name=sys.argv[3]
		param_val=sys.argv[4]
		if (param_name == "mgmt_ip" ) or (param_name == "mip_ip"):
			vm_helper(nw_name,vm_name,param_name,param_val)
		elif (param_name == "mgmt_mac"):
			vm_mac_helper(nw_name,vm_name,param_name,param_val)
		elif (param_name == "vm_status"):
			return_data=vm_status_helper(nw_name,vm_name)
			if return_data:
				print "please first fill all the required data for ",vm_name
				sys.exit(1)
	elif length == 4:
                nw_name=sys.argv[1]
                param_name=sys.argv[2]
		param_val=sys.argv[3]
		nw_status_helper(nw_name,param_name,param_val)



