#! /usr/bin/python
#****************************************************************************************
#                MASTER APPLICATION CLIENT
#****************************************************************************************

from subprocess import Popen,PIPE,STDOUT
import re
from copy import copy
import ConfigParser
import threading


import sys, os, socket, time, json
Local_database = {}
parameter_list=list()

MAX_BYTES = 1024
mac_address = ('localhost', 4950)
mas_local_address = ('',4965)


vm_param_list=["mgmt_ip","mgmt_mac","mip_ip","model_number","server_ip","server_port","vm_imag_path","vm_coordinates"]

Config1=ConfigParser.RawConfigParser()
Config1.read("local_database_status_file.ini")
Config1.sections()

def ConfigSectionMap(section):    #function to read data from local_database_status_file.ini
    dict1 = {}
    options = Config1.options(section)
    for option in options:
        try:
            dict1[option] = Config1.get(section, option)
            if dict1[option] == -1:
                DebugPrint("skip: %s" % option)
        except:
            print("exception on %s!" % option)
            dict1[option] = None
    return dict1

def status_file_write(vm_data):
        section_identifier = vm_data['mgmt_ip']
        cfgfile=open("local_database_status_file.ini",'w')
        Config1.add_section(section_identifier)
        Config1.set(section_identifier,'vm_name',vm_data['vm_name'])
        Config1.set(section_identifier,'nw_name',vm_data['nw_name'])
        Config1.set(section_identifier,'status',"SET")
        Config1.write(cfgfile)
        cfgfile.close

def status_file_delete(vm_data):
        section_identifier = vm_data['mgmt_ip']
        Config1.remove_section(section_identifier)
        cfgfile=open("local_database_status_file.ini",'w')
        Config1.write(cfgfile)
        cfgfile.close


def status_file_update(vm_data):
        section_identifier = vm_data['mgmt_ip']
        cfgfile=open("local_database_status_file.ini",'w')
        Config1.set(section_identifier,'status',vm_data['status'])
        Config1.write(cfgfile)
        cfgfile.close


def local_database_status_file(vm_data):
	if vm_data['action_tag'] == "CREATE":
		status_file_write(vm_data)

def initial_Local_database_setup():
	section_list = Config1.sections()
	for i in section_list:
		section_identifier = i
		nw_name = ConfigSectionMap(section_identifier)['nw_name']
		vm_name = ConfigSectionMap(section_identifier)['vm_name']
		status  = ConfigSectionMap(section_identifier)['status']
		if nw_name in Local_database:
			Local_database[nw_name][vm_name]=status
		else:
			Local_database[nw_name]={}
			Local_database[nw_name][vm_name]=status
	print Local_database

def running_Local_database_update(vm_data):
        print "<MAC>:Receiving status from MAS"
	section_identifier = vm_data['mgmt_ip']
       	nw_name = ConfigSectionMap(section_identifier)['nw_name']
        vm_name = ConfigSectionMap(section_identifier)['vm_name']
	status =  vm_data['status']
	if status == "DELETE":
		del Local_database[nw_name][vm_name]
            	if len(Local_database[nw_name]) == 0:
                	del Local_database[nw_name]
	else:
		if nw_name in Local_database:
			Local_database[nw_name][vm_name] = status
		else:
                        Local_database[nw_name]={}
                        Local_database[nw_name][vm_name]=status





def json_string_design_handler(nw_name,vm_name,action_tag):
	cmd="./../bin/konf -- -d \"nw_name "+nw_name+"\""+" -r "+"\"vm_name "+vm_name+"\""
	output=command_output_buffer(cmd)
	output_duplicate= copy(output)
	json_string=dict()
	cmd_role="./../bin/konf -- -d \"nw_name "+nw_name+"\""+" -I "+"\"vm_name "+vm_name+"\""+" -r "+"\"vm_role"+"\""
        output_role=command_output_buffer(cmd_role)
	if len(output_role)==0:
	        VM_ROLE="RELAY"
	else:
    		output_role = output_role.rstrip("\n")
	    	output_vm_role = output_role.split(" ")
		VM_ROLE = output_vm_role[1].upper()
                if VM_ROLE == "MOBILE":
	            cmd_speed="./../bin/konf -- -d \"nw_name "+nw_name+"\""+" -I "+"\"vm_name "+vm_name+"\""+" -r "+"\"vm_mobile_speed"+"\""
                    output_speed=command_output_buffer(cmd_speed)
    		    output_speed = output_speed.rstrip("\n")
	            output_vm_mobile_speed = output_speed.split(" ")
		    VM_MOBILE_SPEED = output_vm_mobile_speed[1]
	            json_string['vm_mobile_speed'] = VM_MOBILE_SPEED

	            cmd_distance="./../bin/konf -- -d \"nw_name "+nw_name+"\""+" -I "+"\"vm_name "+vm_name+"\""+" -r "+"\"vm_mobile_distance"+"\""
                    output_distance=command_output_buffer(cmd_distance)
    		    output_distance = output_distance.rstrip("\n")
	            output_vm_mobile_distance = output_distance.split(" ")
		    VM_MOBILE_DISTANCE = output_vm_mobile_distance[1]
	            json_string['vm_mobile_distance'] = VM_MOBILE_DISTANCE

	json_string['nw_name']=nw_name
	json_string['vm_name']=vm_name
	json_string['action_tag']=action_tag
	json_string['vm_role']=VM_ROLE
	for i in vm_param_list:
		output=copy(output_duplicate)
		output=iter(output.split("\n"))
		for line in output:
			vm = re.search(i, line)
                	if vm:
				json_string[i]=line.split()[1]
				break
	#print json_string
	local_database_status_file(json_string)
	str_mas_output = mas_send_recv_server(json_string)
        #sending config to other server
        if action_tag == "CREATE" or action_tag == "UNDEFINE" or action_tag == "DELETE_UNDEFINE":
            mas_send_config_other_server(json_string,action_tag)
	return str_mas_output




def vm_data(output,param_name):
        output= iter(output.split("\n"))
        for line in output:
                vm = re.search(param_name, line)
                if vm:
			global parameter_list
                        parameter_list.append(line.split()[1])

def command_output_buffer(cmd):
        p=Popen(cmd, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
        output = p.stdout.read()
	return output

def network_delete_show_handler(data):
	if data[1] == "show":
		nw_name = data[0]
		print "performing show operation inside network_delete_show_handler"
		if data[0].upper() == "ALL":
			str_network_show = Local_database
		else:
			if nw_name in Local_database:
				str_network_show = Local_database[nw_name]
			else:
				str_network_show = "No data is available to show for the given network name"
		return str_network_show
	elif data[1] == "delete":
		nw_name = data[0]
		cmd="./../bin/konf -- -d \"nw_name "+nw_name+"\""
		output=command_output_buffer(cmd)
		param_name="vm_name"
        	vm_data(output,param_name)
		global parameter_list
		if nw_name in Local_database:
			for i in parameter_list:
				print "inside for loop ",i
				if i in Local_database[nw_name]:
					get_status=Local_database[nw_name][i]
					if get_status =='enable':
						action_tag="DELETE_UNDEFINE"
						json_handler_output= json_string_design_handler(nw_name,i,action_tag)
					elif get_status == 'disable':
						action_tag="UNDEFINE"
						json_handler_output= json_string_design_handler(nw_name,i,action_tag)

			parameter_list = []
			str_network_delete = "Given Network name configuration and vm successfully deleted"
		else:
			str_network_delete = "Given Network configuration successfully deleted"

		return str_network_delete

def vm_delete_show_handler(data):

	if data[2] == "show":
        	nw_name = data[0]
		vm_name = data[1]
                print "performing show operation inside network_delete_show_handler"
              	if nw_name in Local_database:
			if vm_name in Local_database[nw_name]:
                     		str_vm_show = Local_database[nw_name][vm_name]
			else:
                 		str_vm_show = "No data is available to show for the given vm name"
               	else:
                 	str_vm_show = "No data is available to show for the given vm name"
                return str_vm_show

	elif data[2] == "delete":
       	        nw_name = data[0]
		vm_name = data[1]
                cmd="./../bin/konf -- -d \"nw_name "+nw_name+"\""+" -r "+"\"vm_name "+vm_name+"\""
                output=command_output_buffer(cmd)
		if vm_name in output:
                	if nw_name in Local_database:
                                if vm_name in Local_database[nw_name]:
                                        get_status=Local_database[nw_name][vm_name]
                                        if get_status =='enable':
                                                action_tag="DELETE_UNDEFINE"
                                                json_handler_output= json_string_design_handler(nw_name,vm_name,action_tag)
                                        elif get_status == 'disable':
                                                action_tag="UNDEFINE"
                                                json_handler_output= json_string_design_handler(nw_name,vm_name,action_tag)
                        		str_vm_delete = "given vm_name configuration and machine successfully deleted."
				else:
                        		str_vm_delete = "given vm_name configuration successfully deleted."
			else:
                        	str_vm_delete = "given vm_name configuration successfully deleted."
                else:
                        str_vm_delete = "given vm_name configuration does not exist"

                return str_vm_delete

def network_status_handler(data):
	global parameter_list
	if data[2] =="disable":
		nw_name = data[0]
		cmd="./../bin/konf -- -d \"nw_name "+nw_name+"\""
                output=command_output_buffer(cmd)
                param_name="vm_name"
                vm_data(output,param_name)
		str_multiple_vm_response_disable = {}
                if nw_name in Local_database:
			str_multiple_vm_response_disable[nw_name] = {}
                        for i in parameter_list:
                                if i in Local_database[nw_name]:
                                        get_status=Local_database[nw_name][i]
                                        if get_status =='enable':
                                                action_tag="POWER_OFF"
                                                json_handler_output= json_string_design_handler(nw_name,i,action_tag)
                                        	if json_handler_output["status"] == "SUCCESS":
                                         		str_multiple_vm_response_disable[nw_name][i] = "DISABLED"
					elif get_status =='disable':
                                         	str_multiple_vm_response_disable[nw_name][i] = "ALREADY_DISABLED"


                        parameter_list = []
                        str_network_status = copy(str_multiple_vm_response_disable)

                else:
			print "nothing to do"
                        parameter_list = []
                        str_network_status = "Given Network status configuration successfully saved"
                return str_network_status

	elif data[2] == "enable":
		nw_name = data[0]
                cmd="./../bin/konf -- -d \"nw_name "+nw_name+"\""
                output=command_output_buffer(cmd)
                param_name="vm_name"
                vm_data(output,param_name)
		str_multiple_vm_response_enable = {}
                if nw_name in Local_database:
			str_multiple_vm_response_enable[nw_name]={}
                        for i in parameter_list:
                                if i in Local_database[nw_name]:
                                        get_status=Local_database[nw_name][i]
					cmd="./../bin/konf -- -d \"nw_name "+nw_name+"\""+" -I "+"\"vm_name "+i+"\""+" -r "+"\"vm_status"+"\""
                			output=command_output_buffer(cmd)

					if len(output)==0:
						config_status="disable"
					else:
						output = output.rstrip("\n")
						output_status = output.split(" ")
						config_status = output_status[1]

                                        if get_status =='disable' and config_status =='disable':
						print "nothing to do"
						str_multiple_vm_response_enable[nw_name][i] = "ALREADY_DISABLED"

                                        elif get_status =='disable' and config_status =='enable':
                                                action_tag="POWER_ON"
                                                json_handler_output= json_string_design_handler(nw_name,i,action_tag)
                                                if json_handler_output["status"] == "SUCCESS":
							str_multiple_vm_response_enable[nw_name][i] = "ENABLED"

                                        elif get_status =='enable' and config_status =='disable':
                                                action_tag="POWER_OFF"
                                                json_handler_output= json_string_design_handler(nw_name,i,action_tag)
                                                if json_handler_output["status"] == "SUCCESS":
							str_multiple_vm_response_enable[nw_name][i] = "DISABLED"

                                        elif get_status =='enable' and config_status =='enable':
						print "nothing to do"
						str_multiple_vm_response_enable[nw_name][i] = "ALREADY_ENABLED"
				else:
					cmd="./../bin/konf -- -d \"nw_name "+nw_name+"\""+" -I "+"\"vm_name "+i+"\""+" -r "+"\"vm_status"+"\""
                                        output=command_output_buffer(cmd)

                                        if len(output)==0:
                                                config_status="disable"
                                        else:
						output = output.rstrip("\n")
                                                output_status = output.split(" ")
                                                config_status = output_status[1]

					if config_status == 'disable':
						print "nothing to do"
						str_multiple_vm_response_enable[nw_name][i] = "NOT_CREATED"

					elif config_status == 'enable':
                                                action_tag="CREATE"
                                                json_handler_output= json_string_design_handler(nw_name,i,action_tag)
                                                if json_handler_output["status"] == "SUCCESS":
							str_multiple_vm_response_enable[nw_name][i] = "ENABLED"




                        parameter_list = []
			str_network_status = copy(str_multiple_vm_response_enable)
                else:
			str_multiple_vm_response_enable[nw_name]={}
			for i in parameter_list:
				cmd="./../bin/konf -- -d \"nw_name "+nw_name+"\""+" -I "+"\"vm_name "+i+"\""+" -r "+"\"vm_status"+"\""
                                output=command_output_buffer(cmd)

                                if len(output)==0:
                                    	config_status="disable"
                                else:
					output = output.rstrip("\n")
                      	               	output_status = output.split(" ")
                                       	config_status = output_status[1]

				if config_status == 'disable':
					print "nothing to do"
					str_multiple_vm_response_enable[nw_name][i] = "NOT_CREATED"
				elif config_status == 'enable':
                 			action_tag="CREATE"
                                        json_handler_output= json_string_design_handler(nw_name,i,action_tag)
                                  	if json_handler_output["status"] == "SUCCESS":
						str_multiple_vm_response_enable[nw_name][i] = "ENABLED"

			parameter_list = []
			str_network_status = copy(str_multiple_vm_response_enable)

		return str_network_status

def vm_status_handler(data):
	if data[3] =="disable":
		nw_name = data[0]
		vm_name = data[1]
		cmd="./../bin/konf -- -d \"nw_name "+nw_name+"\""+" -r "+"\"network_status\""
                output=command_output_buffer(cmd)
		if len(output)==0:
              		nw_config_status="disable"
                else:
			output = output.rstrip("\n")
			output_status = output.split(" ")
                        nw_config_status = output_status[1]

		print nw_config_status
		if nw_config_status == "disable":
			print "nothing to do"
             		str_vm_status = "Given vm_status configuration saved."
		elif nw_config_status == "enable":
               		if nw_name in Local_database:
                                if vm_name in Local_database[nw_name]:
                                        get_status=Local_database[nw_name][vm_name]
					if get_status =='disable':
                                                print "nothing to do"
                        			str_vm_status = "Given vm_name already in power off state."
                                        elif get_status =='enable':
                                                action_tag="POWER_OFF"
                                                json_handler_output= json_string_design_handler(nw_name,vm_name,action_tag)
						str_vm_status = json_handler_output['output']
				else:
                                	print "nothing to do"
                        		str_vm_status = "Given vm_status configuration saved."
			else:
				print "nothing to do"
                        	str_vm_status = "Given vm_status configuration saved."
		return str_vm_status

	elif data[3] == "enable":
		nw_name = data[0]
                vm_name = data[1]
                cmd="./../bin/konf -- -d \"nw_name "+nw_name+"\""+" -r "+"\"network_status\""
                output=command_output_buffer(cmd)
                if len(output)==0:
                        nw_config_status="disable"
                else:
			output = output.rstrip("\n")
                        output_status = output.split(" ")
                        nw_config_status = output_status[1]

                if nw_config_status == "disable":
                        print "nothing to do"
             		str_vm_status = "Given vm_status configuration saved."
                elif nw_config_status == "enable":
                        if nw_name in Local_database:
                                if vm_name in Local_database[nw_name]:
                                        get_status=Local_database[nw_name][vm_name]
                                        if get_status =='enable':
                                                print "nothing to do"
						str_vm_status = "Given vm already in power on state."
                                        elif get_status =='disable':
                                                action_tag="POWER_ON"
                                                json_handler_output= json_string_design_handler(nw_name,vm_name,action_tag)
						str_vm_status = json_handler_output['output']
                                else:
                                  	action_tag="CREATE"
                                        json_handler_output= json_string_design_handler(nw_name,vm_name,action_tag)
					str_vm_status = json_handler_output['output']
                	else:
                              	action_tag="CREATE"
                                json_handler_output= json_string_design_handler(nw_name,vm_name,action_tag)
				str_vm_status = json_handler_output['output']

		return str_vm_status


def mas_send_config_other_server(json_string,action_tag):
        cmd="./../bin/konf -- -d \"nw_name "+json_string['nw_name']+"\""+" -r "+"\"vm_name\""
        output=command_output_buffer(cmd)
        other_server = list()
        output=iter(output.split("\n"))
        for line in output:
                vm = re.search("server_ip", line)
                if vm:
                        server_data = line.split()[1]
                        if server_data not in other_server:
                                other_server.append(server_data)
                        else:
                                continue

        if len(other_server) > 1:
                other_server.remove(json_string["server_ip"])
                mas_other_sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
                if json_string['action_tag'] == "CREATE":
                        json_string['action_tag'] = "CREATE_CONFIG"
                else:
                        json_string['action_tag'] = "DELETE_CONFIG"
                for data in other_server:
                        mas_other_address = (data,int(json_string["server_port"]))
                        message_sent = json.dumps(json_string)
                        mas_other_sock.sendto(message_sent,mas_other_address)

                mas_other_sock.close()

def mas_send_recv_server(json_string):
        print "<MAC>: Sending Configs to MAS"

	mas_sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
	mas_address = (json_string["server_ip"],int(json_string["server_port"]))
	#print mas_address
	message_sent = json.dumps(json_string)
	# send message to mas
	mas_sock.sendto(message_sent,mas_address)

	#waiting for received message from mas
	message_recv,mas_address = mas_sock.recvfrom(MAX_BYTES)
	message_recv = json.loads(message_recv)
	mas_sock.close()
	return message_recv



def klish_recv_send_server():


	 #Creation of global socket for MAC
    	sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

   	 #Bind the socket to the MAC port on the local host
    	sock.bind (mac_address)

    	#infinite loop to handle incoming msgs, handle the response
    	while True:

        # receive cmd from klish
                print "<MAC>: Waiting To Receive Action from klish"
        	data, klish_address = sock.recvfrom(MAX_BYTES)
                print "<MAC>: Received Action from klish : " , data
		str_output= ""
		data = data.rstrip()
		data = data.split(" ")
		if data[1]=="delete" or data[1]== "show":
			str_output=network_delete_show_handler(data)
		elif data[2] == "delete" or data[2] == "show":
			str_output=vm_delete_show_handler(data)
		elif data[1] =="network_status":
			str_output=network_status_handler(data)
		elif data[2] =="vm_status":
			str_output=vm_status_handler(data)

        #convert the str_output data into json format
       		str_send_klish = json.dumps(str_output)
	# send output to klish
		sentBytes = sock.sendto(str_send_klish,klish_address)
                print "<MAC>:Response Send to klish"

    	sock.close()

def mac_localdatabase_recv_status():

	initial_Local_database_setup()
	sock_local_database = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        sock_local_database.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
	sock_local_database.bind (mas_local_address)
	while True:

                local_database_status_data,local_database_sender_address  =  sock_local_database.recvfrom(MAX_BYTES)
                mac_status_data = json.loads(local_database_status_data)

	        section_available_list = Config1.sections()
	        mac_status_section_identifier = mac_status_data['mgmt_ip']
                if mac_status_section_identifier in section_available_list:
                        if mac_status_data['status'] == "DELETE":
                                running_Local_database_update(mac_status_data)
		                status_file_delete(mac_status_data)
                        else:
		                status_file_update(mac_status_data)
		                running_Local_database_update(mac_status_data)




# main function

if __name__ == "__main__":
        print "*****************************************"
        print "        Master Application Client        "
        print "*****************************************"
	KLISH_SERVER = threading.Thread(target=klish_recv_send_server)
        MAC_LOCALDATABASE   = threading.Thread(target=mac_localdatabase_recv_status)
        KLISH_SERVER.start()
        MAC_LOCALDATABASE.start()
