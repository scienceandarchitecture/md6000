#****************************************************************************************
#                MASTER APPLICATION SERVER
#****************************************************************************************

#!/usr/bin/python

from subprocess import Popen,PIPE,STDOUT
from copy import copy
import re
import sys, threading, os, socket, json, time, shlex
import ConfigParser
MAX_BYTES = 1024

local_database_address = ('192.168.122.1',4960)
mas_address = ('', 4955)
MAC_LOCALDATABASE_PORT = 4965
VM_COORDINATES_PORT = 4970

POWER_ON_COORDINATES_VALUE = []

Config  = ConfigParser.RawConfigParser()
Config.read("local_status_database.ini")
Config1=ConfigParser.RawConfigParser()
Config1.read("config_file.ini")
def ConfigSectionMap(section):    #function to read data from config_file.ini
    dict1 = {}
    options = Config1.options(section)
    for option in options:
        try:
            dict1[option] = Config1.get(section, option)
            if dict1[option] == -1:
                DebugPrint("skip: %s" % option)
        except:
            print("exception on %s!" % option)
            dict1[option] = None
    return dict1

def initial_local_database_handler():
	initial_database = list()
	command_str = "virsh list --all"
	initial_database.append(command_str)
	str_initial_database = vm_command_executer(initial_database)
	section_available_list = Config1.sections()
	for i in section_available_list:
		status = "NOT_FOUND"
		n_name  = ConfigSectionMap(i)['nw_name']
		v_name  = ConfigSectionMap(i)['vm_name']
		str_name = n_name+"-"+v_name
		str_initial_database_copy = copy(str_initial_database['output'])
 		str_initial_database_copy = iter(str_initial_database_copy.split("\n"))
                for line in str_initial_database_copy:
                        NAME_FOUND = re.search(str_name, line)
                        if NAME_FOUND:
                                status = line.split()[2]
                                break
		if status == "running":
			VALUE = "enable"
		elif status == "shut":
			VALUE = "disable"
		elif status == "NOT_FOUND":
			VALUE = "DELETE"
		local_database_handler(i,VALUE)


def local_database_handler(mgmt_ip,data):
	if data == "DELETE":
		section_identifier = mgmt_ip
		Config.remove_section(section_identifier)
		cfgfile=open("local_status_database.ini",'w')
        	Config.write(cfgfile)
        	cfgfile.close
	else:
		section_identifier = mgmt_ip
        	section_list= Config.sections()
		if section_identifier not in section_list:
        		Config.add_section(section_identifier)

        	cfgfile=open("local_status_database.ini",'w')
        	Config.set(section_identifier,'current_status',data)
		Config.write(cfgfile)
        	cfgfile.close
	mac_localdatabase_send_status(mgmt_ip,data)


def vm_command_executer(commands):
	for i in commands:
		print i
		p=Popen(i, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
        	output = p.stdout.read()
		exit_code = p.wait()
		print output
	str_vm_output = {'output':output}
	if exit_code:
		status = "FAILED"
	else:
		status = "SUCCESS"
	str_vm_output['status'] = status
	return str_vm_output

def config_file_read(str_vm_data):
	str_config_data = {}
        section_identifier= str_vm_data['mgmt_ip']
        str_config_data['mgmt_ip']= str_vm_data['mgmt_ip']
        str_config_data['mip_ip']  = ConfigSectionMap(section_identifier)['mip_ip']
        str_config_data['vm_name'] = ConfigSectionMap(section_identifier)['vm_name']
        str_config_data['nw_name'] = ConfigSectionMap(section_identifier)['nw_name']
        str_config_data['mgmt_mac'] = ConfigSectionMap(section_identifier)['mgmt_mac']
        str_config_data['model_number'] = ConfigSectionMap(section_identifier)['model_number']
        str_config_data['vm_role'] = ConfigSectionMap(section_identifier)['vm_role']
        if str_config_data['vm_role'] == "MOBILE":
            str_config_data['vm_mobile_speed'] = ConfigSectionMap(section_identifier)['vm_mobile_speed']
            str_config_data['vm_mobile_distance'] = ConfigSectionMap(section_identifier)['vm_mobile_distance']

        str_config_data['vm_coordinates'] = ConfigSectionMap(section_identifier)['vm_coordinates']
	return str_config_data

def config_file_write(vm_data):
	section_identifier = vm_data['mgmt_ip']
	cfgfile=open("config_file.ini",'w')
     	Config1.add_section(section_identifier)
	Config1.set(section_identifier,'vm_name',vm_data['vm_name'])
        Config1.set(section_identifier,'mgmt_mac',vm_data['mgmt_mac'])
        Config1.set(section_identifier,'model_number',vm_data['model_number'])
        Config1.set(section_identifier,'mip_ip',vm_data['mip_ip'])
        Config1.set(section_identifier,'nw_name',vm_data['nw_name'])
        Config1.set(section_identifier,'vm_role',vm_data['vm_role'])
        Config1.set(section_identifier,'vm_coordinates',vm_data['vm_coordinates'])
        if vm_data['vm_role'] == "MOBILE":
            Config1.set(section_identifier,'vm_mobile_speed',vm_data['vm_mobile_speed'])
            Config1.set(section_identifier,'vm_mobile_distance',vm_data['vm_mobile_distance'])

        Config1.write(cfgfile)
        cfgfile.close

def config_file_delete(vm_data):
	section_identifier = vm_data['mgmt_ip']
	Config1.remove_section(section_identifier)
	cfgfile=open("config_file.ini",'w')
	Config1.write(cfgfile)
        cfgfile.close


def vm_creation_handler(vm_data):
	print "<MAS>: Creation Of Virtual Node"
        creation_parameter = list()
        creation_parameter.append("virsh pool-refresh default")
	str_name = vm_data['nw_name']+"-"+vm_data['vm_name']
	command_str ="virsh net-update default add ip-dhcp-host \"<host mac='"+vm_data['mgmt_mac']+"' name='"+str_name+"' ip='"+vm_data['mgmt_ip']+"' />\""+" --live --config"
	creation_parameter.append(command_str)
        duplicate_image_path = "/var/lib/libvirt/images/"+str_name+".img"
	command_str = "cp "+vm_data['vm_imag_path']+" "+duplicate_image_path
        creation_parameter.append(command_str)

	temp_eth2_mac = vm_data['mgmt_mac']
	data = temp_eth2_mac.split(":")
        BASE_ETH2_MAC = data[0]+":"+data[1]+":"+data[2]
        data_prev = data[3]+data[4]+data[5]
        data_new1  = hex(int(data_prev,16)+int('1',16))
        data_len = len(data_new1)
        if data_len == 3:
                data_new = "00000"+data_new1[2:]
        elif data_len == 4:
                data_new = "0000"+data_new1[2:]
        elif data_len == 5:
                data_new = "000"+data_new1[2:]
        elif data_len == 6:
                data_new = "00"+data_new1[2:]
        elif data_len == 7:
                data_new = "0"+data_new1[2:]
        elif data_len == 8:
                data_new = data_new1[2:]

        FINAL_ETH2_MAC = BASE_ETH2_MAC+":"+data_new[0]+data_new[1]+":"+data_new[2]+data_new[3]+":"+data_new[4]+data_new[5]

	command_str = "virt-install --connect qemu:///system -n "+str_name+" -r 128 --vcpus=1 --disk path="+duplicate_image_path+",bus=sata --os-type linux --noautoconsole --network bridge:virbr1,model=e1000 --network bridge:virbr3,model=e1000 --network bridge:virbr2,model=e1000,mac="+FINAL_ETH2_MAC+" --network bridge:virbr0,model=e1000,mac="+vm_data['mgmt_mac']+" --import"
        creation_parameter.append(command_str)
	config_file_write(vm_data)
        local_database_handler(vm_data['mgmt_ip'],"SET")
	str_vm_create = vm_command_executer(creation_parameter)
	if str_vm_create['status'] == 'SUCCESS':
        	local_database_handler(vm_data['mgmt_ip'],"INIT")
	elif str_vm_create['status'] == 'FAILED':
        	deletion_parameter = list()
		command_del ="virsh net-update default delete ip-dhcp-host \"<host mac='"+vm_data['mgmt_mac']+"' name='"+str_name+"' ip='"+vm_data['mgmt_ip']+"' />\""+" --live --config"
		deletion_parameter.append(command_del)
        	command_del = "rm -rf /var/lib/libvirt/images/"+str_name+".img"
		deletion_parameter.append(command_del)
		vm_command_executer(deletion_parameter)
        	local_database_handler(vm_data['mgmt_ip'],"DELETE")
		config_file_delete(vm_data)

	return str_vm_create

def vm_delete_undefine_handler(vm_data):
	if vm_data['action_tag'] == 'UNDEFINE':
	        print "<MAS>: Deletion Of Virtual Node"
		creation_parameter = list()
		str_name = vm_data['nw_name']+"-"+vm_data['vm_name']
		command_str = "virsh undefine "+str_name
		creation_parameter.append(command_str)
		command_str ="virsh net-update default delete ip-dhcp-host \"<host mac='"+vm_data['mgmt_mac']+"' name='"+str_name+"' ip='"+vm_data['mgmt_ip']+"' />\""+" --live --config"
		creation_parameter.append(command_str)
        	command_str = "rm -rf /var/lib/libvirt/images/"+str_name+".img"
		creation_parameter.append(command_str)
		str_vm_delete = vm_command_executer(creation_parameter)
		if str_vm_delete['status'] == 'SUCCESS':
                        vm_coordinates_send_to_others_server(vm_data['mgmt_ip'],"DELETE",vm_data['nw_name'])
			config_file_delete(vm_data)
        		local_database_handler(vm_data['mgmt_ip'],"DELETE")

		return str_vm_delete
	elif vm_data['action_tag'] == 'DELETE_UNDEFINE':
	        print "<MAS>: Deletion Of Virtual Node"
		str_name = vm_data['nw_name']+"-"+vm_data['vm_name']
		creation_parameter = list()
		command_str = "virsh destroy "+str_name
		creation_parameter.append(command_str)
		command_str = "virsh undefine "+str_name
		creation_parameter.append(command_str)
		command_str ="virsh net-update default delete ip-dhcp-host \"<host mac='"+vm_data['mgmt_mac']+"' name='"+str_name+"' ip='"+vm_data['mgmt_ip']+"' />\""+" --live --config"
		creation_parameter.append(command_str)
        	command_str = "rm -rf /var/lib/libvirt/images/"+str_name+".img"
		creation_parameter.append(command_str)
		str_vm_undefine = vm_command_executer(creation_parameter)
		if str_vm_undefine['status'] == 'SUCCESS':
                        vm_coordinates_send_to_others_server(vm_data['mgmt_ip'],"DELETE",vm_data['nw_name'])
			config_file_delete(vm_data)
        		local_database_handler(vm_data['mgmt_ip'],"DELETE")

		return str_vm_undefine

def vm_on_off_handler(vm_data):
	if vm_data['action_tag'] == 'POWER_ON':
	        print "<MAS>: Enable The Virtual Node"
		str_name = vm_data['nw_name']+"-"+vm_data['vm_name']
		creation_parameter = list()
		command_str = "virsh start "+str_name
		creation_parameter.append(command_str)
		str_vm_on = vm_command_executer(creation_parameter)
		POWER_ON_COORDINATES_VALUE.append(vm_data['mgmt_ip'])
		if str_vm_on['status'] == 'SUCCESS':
        		local_database_handler(vm_data['mgmt_ip'],"enable")

		return str_vm_on
	elif vm_data['action_tag'] == 'POWER_OFF':
	        print "<MAS>: Disable The Virtual Node"
		creation_parameter = list()
		str_name = vm_data['nw_name']+"-"+vm_data['vm_name']
		command_str = "virsh destroy "+str_name
		creation_parameter.append(command_str)
		str_vm_off = vm_command_executer(creation_parameter)
		if str_vm_off['status'] == 'SUCCESS':
        		local_database_handler(vm_data['mgmt_ip'],"disable")

		return str_vm_off


def mas_recv_send_server():
    	#Creation of global socket for MAC
    	sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

    	#Bind the socket to the MAC port on the local host
    	sock.bind (mas_address)

    	while True:

        	data, mac_address = sock.recvfrom(MAX_BYTES)
                print "<MAS>: Received Configs From MAC"
	        data = json.loads(data)

        	print data['action_tag']

		if data['action_tag'] == "CREATE":
			str_send_data = vm_creation_handler(data)

		elif data['action_tag'] =="POWER_ON" or data['action_tag'] == "POWER_OFF":
			str_send_data = vm_on_off_handler(data)

		elif data['action_tag'] == "UNDEFINE" or data['action_tag'] == "DELETE_UNDEFINE":
			str_send_data = vm_delete_undefine_handler(data)

		elif data['action_tag'] == "CREATE_CONFIG":
			str_send_data = config_file_write(data)
			vm_coordinates_send_to_others_server(data['mgmt_ip'],"CREATE",data['nw_name'])
			continue

		elif data['action_tag'] == "DELETE_CONFIG":
			vm_coordinates_send_to_others_server(data['mgmt_ip'],"DELETE",data['nw_name'])
			str_send_data = config_file_delete(data)
			continue


		str_send_data = json.dumps(str_send_data)

           	sentBytes = sock.sendto(str_send_data, mac_address)

    	sock.close()

def mas_vm_recv_send_status():
	sock_local = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

        #Bind the socket to the MAC port on the local host
        sock_local.bind (local_database_address)

        while True:

                str_vm_data,vm_address  =  sock_local.recvfrom(MAX_BYTES)
	        str_vm_data = json.loads(str_vm_data)

                section_available_list = Config.sections()
                mas_status_section_identifier = str_vm_data['mgmt_ip']
                if mas_status_section_identifier in section_available_list:
	        	print "<MAS>: Received Status Message From Vconfigd" , str_vm_data['mgmt_ip']
		        if str_vm_data['CONFIG'] == "YES":
			        local_database_handler(str_vm_data['mgmt_ip'],str_vm_data['status'])
			        config_data = config_file_read(str_vm_data)
				config_nw_name = config_data['nw_name']
			        config_data = json.dumps(config_data)
             		        sentBytes = sock_local.sendto(config_data,vm_address)
				time.sleep(1)
                                vm_coordinates_send_to_others_server(str_vm_data['mgmt_ip'],"CREATE",config_nw_name)
				time.sleep(1)
                                vm_coordinates_send_to_ownself_server(str_vm_data['mgmt_ip'],"CREATE",config_nw_name)
				time.sleep(1)
		        else:
                                if str_vm_data['mgmt_ip'] in POWER_ON_COORDINATES_VALUE:
					config_nw_name = ConfigSectionMap(str_vm_data['mgmt_ip'])['nw_name']
                                        vm_coordinates_send_to_everyone_server(config_nw_name)
					POWER_ON_COORDINATES_VALUE.remove(str_vm_data['mgmt_ip'])

			        local_database_handler(str_vm_data['mgmt_ip'],str_vm_data['status'])

def vm_coordinates_send_to_others_server(mgmt_ip,vm_coordinates_action,config_nw_name):
        print "<MAS> vm_coordinates_send_to_others_server to Vconfigd:"
        section_available_list = Config1.sections()

	print section_available_list
        if len(section_available_list) > 1:
                str_vm_coordinates_data = {}
                str_vm_coordinates_data['action_tag'] = vm_coordinates_action

                if vm_coordinates_action == "CREATE":
                        vm_coordinates_create_data = {}
                        vm_coordinates_create_data['mgmt_ip'] = mgmt_ip
                        vm_coordinates_create_data['vm_coordinates'] = ConfigSectionMap(mgmt_ip)['vm_coordinates']
                        vm_coordinates_create_data['mgmt_mac'] = ConfigSectionMap(mgmt_ip)['mgmt_mac']
                        str_vm_coordinates_data['data_1'] = vm_coordinates_create_data
                        str_vm_coordinates_data = json.dumps(str_vm_coordinates_data)
                elif vm_coordinates_action == "DELETE":
                        vm_coordinates_delete_data = {}
                        vm_coordinates_delete_data['mgmt_ip'] = mgmt_ip
                        vm_coordinates_delete_data['vm_coordinates'] = ConfigSectionMap(mgmt_ip)['vm_coordinates']
                        vm_coordinates_delete_data['mgmt_mac'] = ConfigSectionMap(mgmt_ip)['mgmt_mac']
                        str_vm_coordinates_data['data_1'] = vm_coordinates_delete_data
                        str_vm_coordinates_data = json.dumps(str_vm_coordinates_data)

		section_available_list.remove(mgmt_ip)
                for vm_coordinates_section_identifier in section_available_list:
			vm_coordinates_section_identifier_nw_name = ConfigSectionMap(vm_coordinates_section_identifier)['nw_name']
			if vm_coordinates_section_identifier_nw_name == config_nw_name:
				print str_vm_coordinates_data
                     		vm_coordinates_sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
                        	vm_coordinates_sock.sendto(str_vm_coordinates_data,(vm_coordinates_section_identifier,VM_COORDINATES_PORT))
                        	vm_coordinates_sock.close()

def vm_coordinates_send_to_ownself_server(mgmt_ip,vm_coordinates_action,config_nw_name):
        print "<MAS> vm_coordinates_send_to_ownself_server to Vconfigd:"
        section_available_list = Config1.sections()
	print section_available_list
        if len(section_available_list) > 1:
                str_vm_coordinates_data = {}
                VALUE = 1
                str_vm_coordinates_data['action_tag'] = vm_coordinates_action

		section_available_list.remove(mgmt_ip)
                for vm_coordinates_section_identifier in section_available_list:
			vm_coordinates_section_identifier_nw_name = ConfigSectionMap(vm_coordinates_section_identifier)['nw_name']
			if vm_coordinates_section_identifier_nw_name == config_nw_name:
				print vm_coordinates_section_identifier
				vm_coordinates_create_data = {}
				DATA = "data_"+str(VALUE)
				vm_coordinates_create_data['mgmt_ip'] = vm_coordinates_section_identifier
				vm_coordinates_create_data['vm_coordinates'] = ConfigSectionMap(vm_coordinates_section_identifier)['vm_coordinates']
				vm_coordinates_create_data['mgmt_mac'] = ConfigSectionMap(vm_coordinates_section_identifier)['mgmt_mac']
				str_vm_coordinates_data[DATA] = vm_coordinates_create_data
				VALUE= VALUE+1

                str_vm_coordinates_data = json.dumps(str_vm_coordinates_data)
		print str_vm_coordinates_data
                vm_coordinates_sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
                vm_coordinates_sock.sendto(str_vm_coordinates_data,(mgmt_ip,VM_COORDINATES_PORT))
                vm_coordinates_sock.close()


def vm_coordinates_send_to_everyone_server(config_nw_name):
        print "<MAS> vm_coordinates_send_to_everyone_server to Vconfigd:"
        section_available_list = Config1.sections()
	print section_available_list
        if len(section_available_list) > 1:
                str_vm_coordinates_data = {}
                VALUE = 1
                str_vm_coordinates_data['action_tag'] = "CREATE"

                for vm_coordinates_section_identifier in section_available_list:
			vm_coordinates_section_identifier_nw_name = ConfigSectionMap(vm_coordinates_section_identifier)['nw_name']
			if vm_coordinates_section_identifier_nw_name == config_nw_name:
		        	print vm_coordinates_section_identifier
				vm_coordinates_create_data = {}
				DATA = "data_"+str(VALUE)
				vm_coordinates_create_data['mgmt_ip'] = vm_coordinates_section_identifier
				vm_coordinates_create_data['vm_coordinates'] = ConfigSectionMap(vm_coordinates_section_identifier)['vm_coordinates']
				vm_coordinates_create_data['mgmt_mac'] = ConfigSectionMap(vm_coordinates_section_identifier)['mgmt_mac']
				str_vm_coordinates_data[DATA] = vm_coordinates_create_data
				VALUE= VALUE+1

                str_vm_coordinates_data = json.dumps(str_vm_coordinates_data)
		print str_vm_coordinates_data
                for vm_coordinates_section_identifier in section_available_list:
			vm_coordinates_section_identifier_nw_name = ConfigSectionMap(vm_coordinates_section_identifier)['nw_name']
                        if vm_coordinates_section_identifier_nw_name == config_nw_name:
                        	vm_coordinates_sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
                        	vm_coordinates_sock.sendto(str_vm_coordinates_data,(vm_coordinates_section_identifier,VM_COORDINATES_PORT))
                        	vm_coordinates_sock.close()



def mac_localdatabase_send_status(mgmt_ip,data):
        print "<MAS> Send Status Message To MAC:"
	str_mac_status_data = {}
	str_mac_status_data['mgmt_ip'] = mgmt_ip
	str_mac_status_data['status'] = data
	mac_status_data = json.dumps(str_mac_status_data)

        mac_status_sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

	mac_status_sock.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)

	mac_status_sock.sendto(mac_status_data,('<broadcast>', MAC_LOCALDATABASE_PORT))

	mac_status_sock.close()



if __name__ == "__main__":
        print "*****************************************"
        print "        Master Application Server        "
        print "*****************************************"

	initial_local_database_handler()
        MAS_SERVER = threading.Thread(target=mas_recv_send_server)
        LOCAL_DATABASE	 = threading.Thread(target=mas_vm_recv_send_status)
        MAS_SERVER.start()
        LOCAL_DATABASE.start()


