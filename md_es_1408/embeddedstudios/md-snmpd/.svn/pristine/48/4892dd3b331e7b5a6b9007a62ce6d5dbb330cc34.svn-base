/* General includes */
#include <stdio.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <signal.h>
#include <sys/termios.h>
#include <sys/time.h>

/* SNMP includes */
#include "asn1.h"
#include "snmp.h"
#include "agt_mib.h"
#include "agt_engine.h"
#include "HOST-RESOURCES-MIB.h"

static int	_getloadavg(double *r_ave, size_t s_ave);
static int	_get_cpu_usage();
 
/* HOST_RESOURCES_MIB initialisation (must also register the MIB module tree) */
void init_HOST_RESOURCES_MIB()
{
   register_subtrees_of_HOST_RESOURCES_MIB();
}

/* The amount of time since this host was last
 * initialized.  Note that this is different from
 * sysUpTime in the SNMPv2-MIB [RFC1907] because
 * sysUpTime is the uptime of the network management
 * portion of the system.
 */

unsigned char *
var_hrSystemUptime(int *var_len, snmp_info_t *mesg,
        int (**write_method)())
{
    /* Add value computations */

    /* Set size (in bytes) and return address of the variable */
    *var_len = sizeof(long);
    return (unsigned char *) NO_MIBINSTANCE;
}

/* The host's notion of the local date and time of day.
 */
/*
int	write_hrSystemDate(int action,
	unsigned char *var_val, unsigned char varval_type, int var_val_len,
	unsigned char *statP, oid *name, int name_len)
{
    switch (action) {
    case RESERVE1:
    case RESERVE2:
    case COMMIT:
    case FREE:
    }
}
*/

unsigned char *
var_hrSystemDate(int *var_len, snmp_info_t *mesg,
        int (**write_method)())
{
    /* Add value computations */

    /* Set write-function (uncomment if you want to implement it)  */
    /* *write_method = &write_hrSystemDate(); */
    /* Set size (in bytes) and return address of the variable */
    *var_len = sizeof(long);
    return (unsigned char *) NO_MIBINSTANCE;
}

/* The index of the hrDeviceEntry for the device from
 * which this host is configured to load its initial
 * operating system configuration (i.e., which operating
 * system code and/or boot parameters).
 * 
 * Note that writing to this object just changes the
 * configuration that will be used the next time the
 * operating system is loaded and does not actually cause
 * the reload to occur.
 */
/*
int	write_hrSystemInitialLoadDevice(int action,
	unsigned char *var_val, unsigned char varval_type, int var_val_len,
	unsigned char *statP, oid *name, int name_len)
{
    switch (action) {
    case RESERVE1:
    case RESERVE2:
    case COMMIT:
    case FREE:
    }
}
*/

unsigned char *
var_hrSystemInitialLoadDevice(int *var_len, snmp_info_t *mesg,
        int (**write_method)())
{
    /* Add value computations */

    /* Set write-function (uncomment if you want to implement it)  */
    /* *write_method = &write_hrSystemInitialLoadDevice(); */
    /* Set size (in bytes) and return address of the variable */
    *var_len = sizeof(long);
    return (unsigned char *) NO_MIBINSTANCE;
}

/* This object contains the parameters (e.g. a pathname
 * and parameter) supplied to the load device when
 * requesting the initial operating system configuration
 * from that device.
 * 
 * Note that writing to this object just changes the
 * configuration that will be used the next time the
 * operating system is loaded and does not actually cause
 * the reload to occur.
 */
/*
int	write_hrSystemInitialLoadParameters(int action,
	unsigned char *var_val, unsigned char varval_type, int var_val_len,
	unsigned char *statP, oid *name, int name_len)
{
    switch (action) {
    case RESERVE1:
    case RESERVE2:
    case COMMIT:
    case FREE:
    }
}
*/

unsigned char *
var_hrSystemInitialLoadParameters(int *var_len, snmp_info_t *mesg,
        int (**write_method)())
{
    /* Add value computations */

    /* Set write-function (uncomment if you want to implement it)  */
    /* *write_method = &write_hrSystemInitialLoadParameters(); */
    /* Set size (in bytes) and return address of the variable */
    *var_len = sizeof(long);
    return (unsigned char *) NO_MIBINSTANCE;
}

/* The number of user sessions for which this host is
 * storing state information.  A session is a collection
 * of processes requiring a single act of user
 * authentication and possibly subject to collective job
 * control.
 */

unsigned char *
var_hrSystemNumUsers(int *var_len, snmp_info_t *mesg,
        int (**write_method)())
{
    /* Add value computations */

    /* Set size (in bytes) and return address of the variable */
    *var_len = sizeof(long);
    return (unsigned char *) NO_MIBINSTANCE;
}

/* The number of process contexts currently loaded or
 * running on this system.
 */

unsigned char *
var_hrSystemProcesses(int *var_len, snmp_info_t *mesg,
        int (**write_method)())
{
    /* Add value computations */

    /* Set size (in bytes) and return address of the variable */
    *var_len = sizeof(long);
    return (unsigned char *) NO_MIBINSTANCE;
}

/* The maximum number of process contexts this system
 * can support.  If there is no fixed maximum, the value
 * should be zero.  On systems that have a fixed maximum,
 * this object can help diagnose failures that occur when
 * this maximum is reached.
 */

unsigned char *
var_hrSystemMaxProcesses(int *var_len, snmp_info_t *mesg,
        int (**write_method)())
{
    /* Add value computations */

    /* Set size (in bytes) and return address of the variable */
    *var_len = sizeof(long);
    return (unsigned char *) NO_MIBINSTANCE;
}

/* The amount of physical read-write main memory,
 * typically RAM, contained by the host.
 */

unsigned char *
var_hrMemorySize(int *var_len, snmp_info_t *mesg,
        int (**write_method)())
{
    /* Add value computations */

    /* Set size (in bytes) and return address of the variable */
    *var_len = sizeof(long);
    return (unsigned char *) NO_MIBINSTANCE;
}

/* A (conceptual) entry for one logical storage area on
 * the host.  As an example, an instance of the
 * hrStorageType object might be named hrStorageType.3
 */

unsigned char *
var_hrStorageEntry(int *var_len,
        Oid *newoid, Oid *reqoid, int searchType,
        snmp_info_t *mesg, int (**write_method)())
{
/* Last subOID of COLUMNAR OID is column */
int	column = newoid->name[(newoid->namelen - 1)];
int	result;

    while (0 /* Test whether the entry has been found */) {
        /* Add indexes for the entry to OID newname */
        /* Determine whether it is the requested OID  */
        result = compare(reqoid, newoid);
        if (((searchType == EXACT) && (result != 0)) ||
                ((searchType == NEXT) && (result >= 0))) {
            return NO_MIBINSTANCE;
        }
    }
    /* Place here the computation functionality */

    /* Set write-function */
    *write_method = 0;
    /* Set size (in bytes) and return address of the variable */
    *var_len = sizeof(long);
    switch (column) {
	case I_hrStorageIndex:
	    return (unsigned char *) NO_MIBINSTANCE;
	case I_hrStorageType:
	    return (unsigned char *) NO_MIBINSTANCE;
	case I_hrStorageDescr:
	    return (unsigned char *) NO_MIBINSTANCE;
	case I_hrStorageAllocationUnits:
	    return (unsigned char *) NO_MIBINSTANCE;
	case I_hrStorageSize:
	    return (unsigned char *) NO_MIBINSTANCE;
	case I_hrStorageUsed:
	    return (unsigned char *) NO_MIBINSTANCE;
	case I_hrStorageAllocationFailures:
	    return (unsigned char *) NO_MIBINSTANCE;
        default:
            return NO_MIBINSTANCE;
    }
}

/* A (conceptual) entry for one device contained by the
 * host.  As an example, an instance of the hrDeviceType
 * object might be named hrDeviceType.3
 */

unsigned char *
var_hrDeviceEntry(int *var_len,
        Oid *newoid, Oid *reqoid, int searchType,
        snmp_info_t *mesg, int (**write_method)())
{
/* Last subOID of COLUMNAR OID is column */
int	column = newoid->name[(newoid->namelen - 1)];
int	result;

    while (0 /* Test whether the entry has been found */) {
        /* Add indexes for the entry to OID newname */
        /* Determine whether it is the requested OID  */
        result = compare(reqoid, newoid);
        if (((searchType == EXACT) && (result != 0)) ||
                ((searchType == NEXT) && (result >= 0))) {
            return NO_MIBINSTANCE;
        }
    }
    /* Place here the computation functionality */

    /* Set write-function */
    *write_method = 0;
    /* Set size (in bytes) and return address of the variable */
    *var_len = sizeof(long);
    switch (column) {
	case I_hrDeviceIndex:
	    return (unsigned char *) NO_MIBINSTANCE;
	case I_hrDeviceType:
	    return (unsigned char *) NO_MIBINSTANCE;
	case I_hrDeviceDescr:
	    return (unsigned char *) NO_MIBINSTANCE;
	case I_hrDeviceID:
	    return (unsigned char *) NO_MIBINSTANCE;
	case I_hrDeviceStatus:
	    return (unsigned char *) NO_MIBINSTANCE;
	case I_hrDeviceErrors:
	    return (unsigned char *) NO_MIBINSTANCE;
        default:
            return NO_MIBINSTANCE;
    }
}

/* A (conceptual) entry for one processor contained by
 * the host.  The hrDeviceIndex in the index represents
 * the entry in the hrDeviceTable that corresponds to the
 * hrProcessorEntry.
 * 
 * As an example of how objects in this table are named,
 * an instance of the hrProcessorFrwID object might be
 * named hrProcessorFrwID.3
 */

unsigned char *
var_hrProcessorEntry(int *var_len,
        Oid *newoid, Oid *reqoid, int searchType,
        snmp_info_t *mesg, int (**write_method)())
{
/* Last subOID of COLUMNAR OID is column */
	
	static int		int_return;
	int				result,column,pos,count;

	column	= newoid->name[(newoid->namelen - 1)];
	pos		= newoid->namelen++;

	count = 1;
    
	newoid->name[ pos ] = 1;
    result	= compare(reqoid, newoid);
	
	if (((searchType == EXACT) && (result == 0))||((searchType == NEXT) && (result < 0))) {
		*var_len = sizeof(long);   
    }else{
		return NULL;
	}
	
	*write_method = 0;
    
    switch (column) {
	case I_hrProcessorFrwID:
	    *var_len = sizeof(int);
        return (u_char *) NO_MIBINSTANCE;
	case I_hrProcessorLoad:
		*var_len	= sizeof(int);
		int_return  = _get_cpu_usage();
		return (u_char *) & int_return;
        default:
            return NO_MIBINSTANCE;
    }
}

/* A (conceptual) entry for one network device contained
 * by the host.  The hrDeviceIndex in the index
 * represents the entry in the hrDeviceTable that
 * corresponds to the hrNetworkEntry.
 * 
 * As an example of how objects in this table are named,
 * an instance of the hrNetworkIfIndex object might be
 * named hrNetworkIfIndex.3
 */

unsigned char *
var_hrNetworkEntry(int *var_len,
        Oid *newoid, Oid *reqoid, int searchType,
        snmp_info_t *mesg, int (**write_method)())
{
/* Last subOID of COLUMNAR OID is column */
int	column = newoid->name[(newoid->namelen - 1)];
int	result;

    while (0 /* Test whether the entry has been found */) {
        /* Add indexes for the entry to OID newname */
        /* Determine whether it is the requested OID  */
        result = compare(reqoid, newoid);
        if (((searchType == EXACT) && (result != 0)) ||
                ((searchType == NEXT) && (result >= 0))) {
            return NO_MIBINSTANCE;
        }
    }
    /* Place here the computation functionality */

    /* Set write-function */
    *write_method = 0;
    /* Set size (in bytes) and return address of the variable */
    *var_len = sizeof(long);
    switch (column) {
	case I_hrNetworkIfIndex:
	    return (unsigned char *) NO_MIBINSTANCE;
        default:
            return NO_MIBINSTANCE;
    }
}

/* A (conceptual) entry for one printer local to the
 * host.  The hrDeviceIndex in the index represents the
 * entry in the hrDeviceTable that corresponds to the
 * hrPrinterEntry.
 * 
 * As an example of how objects in this table are named,
 * an instance of the hrPrinterStatus object might be
 * named hrPrinterStatus.3
 */

unsigned char *
var_hrPrinterEntry(int *var_len,
        Oid *newoid, Oid *reqoid, int searchType,
        snmp_info_t *mesg, int (**write_method)())
{
/* Last subOID of COLUMNAR OID is column */
int	column = newoid->name[(newoid->namelen - 1)];
int	result;

    while (0 /* Test whether the entry has been found */) {
        /* Add indexes for the entry to OID newname */
        /* Determine whether it is the requested OID  */
        result = compare(reqoid, newoid);
        if (((searchType == EXACT) && (result != 0)) ||
                ((searchType == NEXT) && (result >= 0))) {
            return NO_MIBINSTANCE;
        }
    }
    /* Place here the computation functionality */

    /* Set write-function */
    *write_method = 0;
    /* Set size (in bytes) and return address of the variable */
    *var_len = sizeof(long);
    switch (column) {
	case I_hrPrinterStatus:
	    return (unsigned char *) NO_MIBINSTANCE;
	case I_hrPrinterDetectedErrorState:
	    return (unsigned char *) NO_MIBINSTANCE;
        default:
            return NO_MIBINSTANCE;
    }
}

/* A (conceptual) entry for one long-term storage device
 * contained by the host.  The hrDeviceIndex in the index
 * represents the entry in the hrDeviceTable that
 * corresponds to the hrDiskStorageEntry. As an example,
 * an instance of the hrDiskStorageCapacity object might
 * be named hrDiskStorageCapacity.3
 */

unsigned char *
var_hrDiskStorageEntry(int *var_len,
        Oid *newoid, Oid *reqoid, int searchType,
        snmp_info_t *mesg, int (**write_method)())
{
/* Last subOID of COLUMNAR OID is column */
int	column = newoid->name[(newoid->namelen - 1)];
int	result;

    while (0 /* Test whether the entry has been found */) {
        /* Add indexes for the entry to OID newname */
        /* Determine whether it is the requested OID  */
        result = compare(reqoid, newoid);
        if (((searchType == EXACT) && (result != 0)) ||
                ((searchType == NEXT) && (result >= 0))) {
            return NO_MIBINSTANCE;
        }
    }
    /* Place here the computation functionality */

    /* Set write-function */
    *write_method = 0;
    /* Set size (in bytes) and return address of the variable */
    *var_len = sizeof(long);
    switch (column) {
	case I_hrDiskStorageAccess:
	    return (unsigned char *) NO_MIBINSTANCE;
	case I_hrDiskStorageMedia:
	    return (unsigned char *) NO_MIBINSTANCE;
	case I_hrDiskStorageRemoveble:
	    return (unsigned char *) NO_MIBINSTANCE;
	case I_hrDiskStorageCapacity:
	    return (unsigned char *) NO_MIBINSTANCE;
        default:
            return NO_MIBINSTANCE;
    }
}

/* A (conceptual) entry for one partition.  The
 * hrDeviceIndex in the index represents the entry in the
 * hrDeviceTable that corresponds to the
 * hrPartitionEntry.
 * 
 * As an example of how objects in this table are named,
 * an instance of the hrPartitionSize object might be
 * named hrPartitionSize.3.1
 */

unsigned char *
var_hrPartitionEntry(int *var_len,
        Oid *newoid, Oid *reqoid, int searchType,
        snmp_info_t *mesg, int (**write_method)())
{
/* Last subOID of COLUMNAR OID is column */
int	column = newoid->name[(newoid->namelen - 1)];
int	result;

    while (0 /* Test whether the entry has been found */) {
        /* Add indexes for the entry to OID newname */
        /* Determine whether it is the requested OID  */
        result = compare(reqoid, newoid);
        if (((searchType == EXACT) && (result != 0)) ||
                ((searchType == NEXT) && (result >= 0))) {
            return NO_MIBINSTANCE;
        }
    }
    /* Place here the computation functionality */

    /* Set write-function */
    *write_method = 0;
    /* Set size (in bytes) and return address of the variable */
    *var_len = sizeof(long);
    switch (column) {
	case I_hrPartitionIndex:
	    return (unsigned char *) NO_MIBINSTANCE;
	case I_hrPartitionLabel:
	    return (unsigned char *) NO_MIBINSTANCE;
	case I_hrPartitionID:
	    return (unsigned char *) NO_MIBINSTANCE;
	case I_hrPartitionSize:
	    return (unsigned char *) NO_MIBINSTANCE;
	case I_hrPartitionFSIndex:
	    return (unsigned char *) NO_MIBINSTANCE;
        default:
            return NO_MIBINSTANCE;
    }
}

/* A (conceptual) entry for one file system local to
 * this host or remotely mounted from a file server.
 * File systems that are in only one user's environment
 * on a multi-user system will not be included in this
 * table.
 * 
 * As an example of how objects in this table are named,
 * an instance of the hrFSMountPoint object might be
 * named hrFSMountPoint.3
 */

unsigned char *
var_hrFSEntry(int *var_len,
        Oid *newoid, Oid *reqoid, int searchType,
        snmp_info_t *mesg, int (**write_method)())
{
/* Last subOID of COLUMNAR OID is column */
int	column = newoid->name[(newoid->namelen - 1)];
int	result;

    while (0 /* Test whether the entry has been found */) {
        /* Add indexes for the entry to OID newname */
        /* Determine whether it is the requested OID  */
        result = compare(reqoid, newoid);
        if (((searchType == EXACT) && (result != 0)) ||
                ((searchType == NEXT) && (result >= 0))) {
            return NO_MIBINSTANCE;
        }
    }
    /* Place here the computation functionality */

    /* Set write-function */
    *write_method = 0;
    /* Set size (in bytes) and return address of the variable */
    *var_len = sizeof(long);
    switch (column) {
	case I_hrFSIndex:
	    return (unsigned char *) NO_MIBINSTANCE;
	case I_hrFSMountPoint:
	    return (unsigned char *) NO_MIBINSTANCE;
	case I_hrFSRemoteMountPoint:
	    return (unsigned char *) NO_MIBINSTANCE;
	case I_hrFSType:
	    return (unsigned char *) NO_MIBINSTANCE;
	case I_hrFSAccess:
	    return (unsigned char *) NO_MIBINSTANCE;
	case I_hrFSBootable:
	    return (unsigned char *) NO_MIBINSTANCE;
	case I_hrFSStorageIndex:
	    return (unsigned char *) NO_MIBINSTANCE;
	case I_hrFSLastFullBackupDate:
	    return (unsigned char *) NO_MIBINSTANCE;
	case I_hrFSLastPartialBackupDate:
	    return (unsigned char *) NO_MIBINSTANCE;
        default:
            return NO_MIBINSTANCE;
    }
}

/* The value of the hrSWRunIndex for the hrSWRunEntry
 * that represents the primary operating system running
 * on this host.  This object is useful for quickly and
 * uniquely identifying that primary operating system.
 */

unsigned char *
var_hrSWOSIndex(int *var_len, snmp_info_t *mesg,
        int (**write_method)())
{
    /* Add value computations */

    /* Set size (in bytes) and return address of the variable */
    *var_len = sizeof(long);
    return (unsigned char *) NO_MIBINSTANCE;
}

/* A (conceptual) entry for one piece of software
 * running on the host Note that because the installed
 * software table only contains information for software
 * stored locally on this host, not every piece of
 * running software will be found in the installed
 * software table.  This is true of software that was
 * loaded and run from a non-local source, such as a
 * network-mounted file system.
 * 
 * As an example of how objects in this table are named,
 * an instance of the hrSWRunName object might
 */

unsigned char *
var_hrSWRunEntry(int *var_len,
        Oid *newoid, Oid *reqoid, int searchType,
        snmp_info_t *mesg, int (**write_method)())
{
/* Last subOID of COLUMNAR OID is column */
int	column = newoid->name[(newoid->namelen - 1)];
int	result;

    while (0 /* Test whether the entry has been found */) {
        /* Add indexes for the entry to OID newname */
        /* Determine whether it is the requested OID  */
        result = compare(reqoid, newoid);
        if (((searchType == EXACT) && (result != 0)) ||
                ((searchType == NEXT) && (result >= 0))) {
            return NO_MIBINSTANCE;
        }
    }
    /* Place here the computation functionality */

    /* Set write-function */
    *write_method = 0;
    /* Set size (in bytes) and return address of the variable */
    *var_len = sizeof(long);
    switch (column) {
	case I_hrSWRunIndex:
	    return (unsigned char *) NO_MIBINSTANCE;
	case I_hrSWRunName:
	    return (unsigned char *) NO_MIBINSTANCE;
	case I_hrSWRunID:
	    return (unsigned char *) NO_MIBINSTANCE;
	case I_hrSWRunPath:
	    return (unsigned char *) NO_MIBINSTANCE;
	case I_hrSWRunParameters:
	    return (unsigned char *) NO_MIBINSTANCE;
	case I_hrSWRunType:
	    return (unsigned char *) NO_MIBINSTANCE;
	case I_hrSWRunStatus:
	    return (unsigned char *) NO_MIBINSTANCE;
        default:
            return NO_MIBINSTANCE;
    }
}

/* A (conceptual) entry containing software performance
 * metrics.  As an example, an instance of the
 * hrSWRunPerfCPU object might be named
 * hrSWRunPerfCPU.1287
 */

unsigned char *
var_hrSWRunPerfEntry(int *var_len,
        Oid *newoid, Oid *reqoid, int searchType,
        snmp_info_t *mesg, int (**write_method)())
{
/* Last subOID of COLUMNAR OID is column */
int	column = newoid->name[(newoid->namelen - 1)];
int	result;

    while (0 /* Test whether the entry has been found */) {
        /* Add indexes for the entry to OID newname */
        /* Determine whether it is the requested OID  */
        result = compare(reqoid, newoid);
        if (((searchType == EXACT) && (result != 0)) ||
                ((searchType == NEXT) && (result >= 0))) {
            return NO_MIBINSTANCE;
        }
    }
    /* Place here the computation functionality */

    /* Set write-function */
    *write_method = 0;
    /* Set size (in bytes) and return address of the variable */
    *var_len = sizeof(long);
    switch (column) {
	case I_hrSWRunPerfCPU:
	    return (unsigned char *) NO_MIBINSTANCE;
	case I_hrSWRunPerfMem:
	    return (unsigned char *) NO_MIBINSTANCE;
        default:
            return NO_MIBINSTANCE;
    }
}

/* The value of sysUpTime when an entry in the
 * hrSWInstalledTable was last added, renamed, or
 * deleted.  Because this table is likely to contain many
 * entries, polling of this object allows a management
 * station to determine when re-downloading of the table
 * might be useful.
 */

unsigned char *
var_hrSWInstalledLastChange(int *var_len, snmp_info_t *mesg,
        int (**write_method)())
{
    /* Add value computations */

    /* Set size (in bytes) and return address of the variable */
    *var_len = sizeof(long);
    return (unsigned char *) NO_MIBINSTANCE;
}

/* The value of sysUpTime when the hrSWInstalledTable
 * was last completely updated.  Because caching of this
 * data will be a popular implementation strategy,
 * retrieval of this object allows a management station
 * to obtain a guarantee that no data in this table is
 * older than the indicated time.
 */

unsigned char *
var_hrSWInstalledLastUpdateTime(int *var_len, snmp_info_t *mesg,
        int (**write_method)())
{
    /* Add value computations */

    /* Set size (in bytes) and return address of the variable */
    *var_len = sizeof(long);
    return (unsigned char *) NO_MIBINSTANCE;
}

/* A (conceptual) entry for a piece of software
 * installed on this host.
 * 
 * As an example of how objects in this table are named,
 * an instance of the hrSWInstalledName object might be
 * named hrSWInstalledName.96
 */

unsigned char *
var_hrSWInstalledEntry(int *var_len,
        Oid *newoid, Oid *reqoid, int searchType,
        snmp_info_t *mesg, int (**write_method)())
{
/* Last subOID of COLUMNAR OID is column */
int	column = newoid->name[(newoid->namelen - 1)];
int	result;

    while (0 /* Test whether the entry has been found */) {
        /* Add indexes for the entry to OID newname */
        /* Determine whether it is the requested OID  */
        result = compare(reqoid, newoid);
        if (((searchType == EXACT) && (result != 0)) ||
                ((searchType == NEXT) && (result >= 0))) {
            return NO_MIBINSTANCE;
        }
    }
    /* Place here the computation functionality */

    /* Set write-function */
    *write_method = 0;
    /* Set size (in bytes) and return address of the variable */
    *var_len = sizeof(long);
    switch (column) {
	case I_hrSWInstalledIndex:
	    return (unsigned char *) NO_MIBINSTANCE;
	case I_hrSWInstalledName:
	    return (unsigned char *) NO_MIBINSTANCE;
	case I_hrSWInstalledID:
	    return (unsigned char *) NO_MIBINSTANCE;
	case I_hrSWInstalledType:
	    return (unsigned char *) NO_MIBINSTANCE;
	case I_hrSWInstalledDate:
	    return (unsigned char *) NO_MIBINSTANCE;
        default:
            return NO_MIBINSTANCE;
    }
}

static oid hrSystem_oid[] = { O_hrSystem };
static Object hrSystem_variables[] = {
    { SNMP_TIMETICKS, (RONLY| SCALAR), var_hrSystemUptime,
                 {2, { I_hrSystemUptime, 0 }}},
    { SNMP_STRING, (RWRITE| SCALAR), var_hrSystemDate,
                 {2, { I_hrSystemDate, 0 }}},
    { SNMP_INTEGER, (RWRITE| SCALAR), var_hrSystemInitialLoadDevice,
                 {2, { I_hrSystemInitialLoadDevice, 0 }}},
    { SNMP_STRING, (RWRITE| SCALAR), var_hrSystemInitialLoadParameters,
                 {2, { I_hrSystemInitialLoadParameters, 0 }}},
    { SNMP_GAUGE, (RONLY| SCALAR), var_hrSystemNumUsers,
                 {2, { I_hrSystemNumUsers, 0 }}},
    { SNMP_GAUGE, (RONLY| SCALAR), var_hrSystemProcesses,
                 {2, { I_hrSystemProcesses, 0 }}},
    { SNMP_INTEGER, (RONLY| SCALAR), var_hrSystemMaxProcesses,
                 {2, { I_hrSystemMaxProcesses, 0 }}}
    };
static SubTree hrSystem_tree =  { NULL, hrSystem_variables,
	        (sizeof(hrSystem_oid)/sizeof(oid)), hrSystem_oid};

static oid hrStorage_oid[] = { O_hrStorage };
static Object hrStorage_variables[] = {
    { SNMP_INTEGER, (RONLY| SCALAR), var_hrMemorySize,
                 {2, { I_hrMemorySize, 0 }}},
    { SNMP_INTEGER, (RONLY| COLUMN), var_hrStorageEntry,
                {3, { I_hrStorageTable, I_hrStorageEntry, I_hrStorageIndex }}},
    { SNMP_OBJID, (RONLY| COLUMN), var_hrStorageEntry,
                {3, { I_hrStorageTable, I_hrStorageEntry, I_hrStorageType }}},
    { SNMP_STRING, (RONLY| COLUMN), var_hrStorageEntry,
                {3, { I_hrStorageTable, I_hrStorageEntry, I_hrStorageDescr }}},
    { SNMP_INTEGER, (RONLY| COLUMN), var_hrStorageEntry,
                {3, { I_hrStorageTable, I_hrStorageEntry, I_hrStorageAllocationUnits }}},
    { SNMP_INTEGER, (RWRITE| COLUMN), var_hrStorageEntry,
                {3, { I_hrStorageTable, I_hrStorageEntry, I_hrStorageSize }}},
    { SNMP_INTEGER, (RONLY| COLUMN), var_hrStorageEntry,
                {3, { I_hrStorageTable, I_hrStorageEntry, I_hrStorageUsed }}},
    { SNMP_COUNTER, (RONLY| COLUMN), var_hrStorageEntry,
                {3, { I_hrStorageTable, I_hrStorageEntry, I_hrStorageAllocationFailures }}}
    };
static SubTree hrStorage_tree =  { NULL, hrStorage_variables,
	        (sizeof(hrStorage_oid)/sizeof(oid)), hrStorage_oid};

static oid hrSWRun_oid[] = { O_hrSWRun };
static Object hrSWRun_variables[] = {
    { SNMP_INTEGER, (RONLY| SCALAR), var_hrSWOSIndex,
                 {2, { I_hrSWOSIndex, 0 }}},
    { SNMP_INTEGER, (RONLY| COLUMN), var_hrSWRunEntry,
                {3, { I_hrSWRunTable, I_hrSWRunEntry, I_hrSWRunIndex }}},
    { SNMP_STRING, (RONLY| COLUMN), var_hrSWRunEntry,
                {3, { I_hrSWRunTable, I_hrSWRunEntry, I_hrSWRunName }}},
    { SNMP_OBJID, (RONLY| COLUMN), var_hrSWRunEntry,
                {3, { I_hrSWRunTable, I_hrSWRunEntry, I_hrSWRunID }}},
    { SNMP_STRING, (RONLY| COLUMN), var_hrSWRunEntry,
                {3, { I_hrSWRunTable, I_hrSWRunEntry, I_hrSWRunPath }}},
    { SNMP_STRING, (RONLY| COLUMN), var_hrSWRunEntry,
                {3, { I_hrSWRunTable, I_hrSWRunEntry, I_hrSWRunParameters }}},
    { SNMP_INTEGER, (RONLY| COLUMN), var_hrSWRunEntry,
                {3, { I_hrSWRunTable, I_hrSWRunEntry, I_hrSWRunType }}},
    { SNMP_INTEGER, (RWRITE| COLUMN), var_hrSWRunEntry,
                {3, { I_hrSWRunTable, I_hrSWRunEntry, I_hrSWRunStatus }}}
    };
static SubTree hrSWRun_tree =  { NULL, hrSWRun_variables,
	        (sizeof(hrSWRun_oid)/sizeof(oid)), hrSWRun_oid};

static oid hrSWInstalled_oid[] = { O_hrSWInstalled };
static Object hrSWInstalled_variables[] = {
    { SNMP_TIMETICKS, (RONLY| SCALAR), var_hrSWInstalledLastChange,
                 {2, { I_hrSWInstalledLastChange, 0 }}},
    { SNMP_TIMETICKS, (RONLY| SCALAR), var_hrSWInstalledLastUpdateTime,
                 {2, { I_hrSWInstalledLastUpdateTime, 0 }}},
    { SNMP_INTEGER, (RONLY| COLUMN), var_hrSWInstalledEntry,
                {3, { I_hrSWInstalledTable, I_hrSWInstalledEntry, I_hrSWInstalledIndex }}},
    { SNMP_STRING, (RONLY| COLUMN), var_hrSWInstalledEntry,
                {3, { I_hrSWInstalledTable, I_hrSWInstalledEntry, I_hrSWInstalledName }}},
    { SNMP_OBJID, (RONLY| COLUMN), var_hrSWInstalledEntry,
                {3, { I_hrSWInstalledTable, I_hrSWInstalledEntry, I_hrSWInstalledID }}},
    { SNMP_INTEGER, (RONLY| COLUMN), var_hrSWInstalledEntry,
                {3, { I_hrSWInstalledTable, I_hrSWInstalledEntry, I_hrSWInstalledType }}},
    { SNMP_STRING, (RONLY| COLUMN), var_hrSWInstalledEntry,
                {3, { I_hrSWInstalledTable, I_hrSWInstalledEntry, I_hrSWInstalledDate }}}
    };
static SubTree hrSWInstalled_tree =  { NULL, hrSWInstalled_variables,
	        (sizeof(hrSWInstalled_oid)/sizeof(oid)), hrSWInstalled_oid};

static oid hrDeviceEntry_oid[] = { O_hrDeviceEntry };
static Object hrDeviceEntry_variables[] = {
    { SNMP_INTEGER, (RONLY| COLUMN), var_hrDeviceEntry,
                {1, { I_hrDeviceIndex }}},
    { SNMP_OBJID, (RONLY| COLUMN), var_hrDeviceEntry,
                {1, { I_hrDeviceType }}},
    { SNMP_STRING, (RONLY| COLUMN), var_hrDeviceEntry,
                {1, { I_hrDeviceDescr }}},
    { SNMP_OBJID, (RONLY| COLUMN), var_hrDeviceEntry,
                {1, { I_hrDeviceID }}},
    { SNMP_INTEGER, (RONLY| COLUMN), var_hrDeviceEntry,
                {1, { I_hrDeviceStatus }}},
    { SNMP_COUNTER, (RONLY| COLUMN), var_hrDeviceEntry,
                {1, { I_hrDeviceErrors }}}
    };
static SubTree hrDeviceEntry_tree =  { NULL, hrDeviceEntry_variables,
	        (sizeof(hrDeviceEntry_oid)/sizeof(oid)), hrDeviceEntry_oid};

static oid hrProcessorEntry_oid[] = { O_hrProcessorEntry };
static Object hrProcessorEntry_variables[] = {
    { SNMP_OBJID, (RONLY| COLUMN), var_hrProcessorEntry,
                {1, { I_hrProcessorFrwID }}},
    { SNMP_INTEGER, (RONLY| COLUMN), var_hrProcessorEntry,
                {1, { I_hrProcessorLoad }}}
    };

static SubTree hrProcessorEntry_tree =  { NULL, hrProcessorEntry_variables,
	        (sizeof(hrProcessorEntry_oid)/sizeof(oid)), hrProcessorEntry_oid};

static oid hrNetworkEntry_oid[] = { O_hrNetworkEntry };
static Object hrNetworkEntry_variables[] = {
    { SNMP_INTEGER, (RONLY| COLUMN), var_hrNetworkEntry,
                {1, { I_hrNetworkIfIndex }}}
    };

static SubTree hrNetworkEntry_tree =  { NULL, hrNetworkEntry_variables,
	        (sizeof(hrNetworkEntry_oid)/sizeof(oid)), hrNetworkEntry_oid};

static oid hrPrinterEntry_oid[] = { O_hrPrinterEntry };
static Object hrPrinterEntry_variables[] = {
    { SNMP_INTEGER, (RONLY| COLUMN), var_hrPrinterEntry,
                {1, { I_hrPrinterStatus }}},
    { SNMP_STRING, (RONLY| COLUMN), var_hrPrinterEntry,
                {1, { I_hrPrinterDetectedErrorState }}}
    };

static SubTree hrPrinterEntry_tree =  { NULL, hrPrinterEntry_variables,
	        (sizeof(hrPrinterEntry_oid)/sizeof(oid)), hrPrinterEntry_oid};

static oid hrDiskStorageEntry_oid[] = { O_hrDiskStorageEntry };
static Object hrDiskStorageEntry_variables[] = {
    { SNMP_INTEGER, (RONLY| COLUMN), var_hrDiskStorageEntry,
                {1, { I_hrDiskStorageAccess }}},
    { SNMP_INTEGER, (RONLY| COLUMN), var_hrDiskStorageEntry,
                {1, { I_hrDiskStorageMedia }}},
    { SNMP_INTEGER, (RONLY| COLUMN), var_hrDiskStorageEntry,
                {1, { I_hrDiskStorageRemoveble }}},
    { SNMP_INTEGER, (RONLY| COLUMN), var_hrDiskStorageEntry,
                {1, { I_hrDiskStorageCapacity }}}
    };

static SubTree hrDiskStorageEntry_tree =  { NULL, hrDiskStorageEntry_variables,
	        (sizeof(hrDiskStorageEntry_oid)/sizeof(oid)), hrDiskStorageEntry_oid};

static oid hrPartitionEntry_oid[] = { O_hrPartitionEntry };
static Object hrPartitionEntry_variables[] = {
    { SNMP_INTEGER, (RONLY| COLUMN), var_hrPartitionEntry,
                {1, { I_hrPartitionIndex }}},
    { SNMP_STRING, (RONLY| COLUMN), var_hrPartitionEntry,
                {1, { I_hrPartitionLabel }}},
    { SNMP_STRING, (RONLY| COLUMN), var_hrPartitionEntry,
                {1, { I_hrPartitionID }}},
    { SNMP_INTEGER, (RONLY| COLUMN), var_hrPartitionEntry,
                {1, { I_hrPartitionSize }}},
    { SNMP_INTEGER, (RONLY| COLUMN), var_hrPartitionEntry,
                {1, { I_hrPartitionFSIndex }}}
    };

static SubTree hrPartitionEntry_tree =  { NULL, hrPartitionEntry_variables,
	        (sizeof(hrPartitionEntry_oid)/sizeof(oid)), hrPartitionEntry_oid};

static oid hrFSEntry_oid[] = { O_hrFSEntry };
static Object hrFSEntry_variables[] = {
    { SNMP_INTEGER, (RONLY| COLUMN), var_hrFSEntry,
                {1, { I_hrFSIndex }}},
    { SNMP_STRING, (RONLY| COLUMN), var_hrFSEntry,
                {1, { I_hrFSMountPoint }}},
    { SNMP_STRING, (RONLY| COLUMN), var_hrFSEntry,
                {1, { I_hrFSRemoteMountPoint }}},
    { SNMP_OBJID, (RONLY| COLUMN), var_hrFSEntry,
                {1, { I_hrFSType }}},
    { SNMP_INTEGER, (RONLY| COLUMN), var_hrFSEntry,
                {1, { I_hrFSAccess }}},
    { SNMP_INTEGER, (RONLY| COLUMN), var_hrFSEntry,
                {1, { I_hrFSBootable }}},
    { SNMP_INTEGER, (RONLY| COLUMN), var_hrFSEntry,
                {1, { I_hrFSStorageIndex }}},
    { SNMP_STRING, (RWRITE| COLUMN), var_hrFSEntry,
                {1, { I_hrFSLastFullBackupDate }}},
    { SNMP_STRING, (RWRITE| COLUMN), var_hrFSEntry,
                {1, { I_hrFSLastPartialBackupDate }}},
    };

static SubTree hrFSEntry_tree =  { NULL, hrFSEntry_variables,
	        (sizeof(hrFSEntry_oid)/sizeof(oid)), hrFSEntry_oid};

static oid hrSWRunPerfEntry_oid[] = { O_hrSWRunPerfEntry };
static Object hrSWRunPerfEntry_variables[] = {
    { SNMP_INTEGER, (RONLY| COLUMN), var_hrSWRunPerfEntry,
                {1, { I_hrSWRunPerfCPU }}},
    { SNMP_INTEGER, (RONLY| COLUMN), var_hrSWRunPerfEntry,
                {1, { I_hrSWRunPerfMem }}},
    };

static SubTree hrSWRunPerfEntry_tree =  { NULL, hrSWRunPerfEntry_variables,
	        (sizeof(hrSWRunPerfEntry_oid)/sizeof(oid)), hrSWRunPerfEntry_oid};

/* This is the MIB registration function. This should be called */
/* within the init_HOST_RESOURCES_MIB-function */
void register_subtrees_of_HOST_RESOURCES_MIB()
{
    insert_group_in_mib(&hrSystem_tree);
    insert_group_in_mib(&hrStorage_tree);
    insert_group_in_mib(&hrSWRun_tree);
    insert_group_in_mib(&hrSWInstalled_tree);
    insert_group_in_mib(&hrDeviceEntry_tree);
    insert_group_in_mib(&hrProcessorEntry_tree);
    insert_group_in_mib(&hrNetworkEntry_tree);
    insert_group_in_mib(&hrPrinterEntry_tree);
    insert_group_in_mib(&hrDiskStorageEntry_tree);
    insert_group_in_mib(&hrPartitionEntry_tree);
    insert_group_in_mib(&hrFSEntry_tree);
    insert_group_in_mib(&hrSWRunPerfEntry_tree);
}


/************  internal implementation functions ************/

static int _getloadavg(double *r_ave, size_t s_ave)
{
  double *pave = r_ave;
#ifdef HAVE_SYS_FIXPOINT_H
  fix favenrun[3];
#endif
#if (defined(ultrix) || defined(sun) || defined(__alpha)) || defined(dynix)
  int i;
#if (defined(sun) || defined(__alpha)) || defined(dynix)
  long favenrun[3];
  if (s_ave > 3) /* bounds check */
    return (-1); 
#define FIX_TO_DBL(_IN) (((double) _IN)/((double) FSCALE))
#endif
#endif
#if defined(aix4) || defined(aix5)
  int favenrun[3];
#endif
#if defined(hpux10) || defined(hpux11)
  struct pst_dynamic pst_buf;
#endif


#ifdef HAVE_GETLOADAVG
  if (getloadavg(pave, s_ave) == -1)
    return(-1);
#elif defined(linux)
  { FILE *in = fopen("/proc/loadavg", "r");
    if (!in) {
      printf("snmpd: cannot open /proc/loadavg\n");
      return (-1);
    }
    fscanf(in, "%lf %lf %lf", pave, (pave + 1), (pave + 2));
    fclose(in);
  }
#elif defined(ultrix) || defined(sun) || defined(__alpha) || defined(dynix)
  if (auto_nlist(LOADAVE_SYMBOL,(char *) favenrun, sizeof(favenrun)) == 0)
    return(-1);
  for(i=0;i<s_ave;i++)
    *(pave+i) = FIX_TO_DBL(favenrun[i]);
#elif defined(hpux10) || defined(hpux11)
  if (pstat_getdynamic(&pst_buf, sizeof(struct pst_dynamic), 1, 0) < 0)
    return(-1);
  r_ave[0] = pst_buf.psd_avg_1_min;
  r_ave[1] = pst_buf.psd_avg_5_min;
  r_ave[2] = pst_buf.psd_avg_15_min;
#elif !defined(cygwin)
	#ifdef CAN_USE_NLIST
		#if defined(aix4) || defined(aix5)
			if (auto_nlist(LOADAVE_SYMBOL,(char *) favenrun, sizeof(favenrun)) == 0)
				return -1;
				r_ave[0] = favenrun[0]/65536.0;
				r_ave[1] = favenrun[1]/65536.0;
				r_ave[2] = favenrun[2]/65536.0;
				return 0;
		#else
				if (auto_nlist(LOADAVE_SYMBOL,(char *) pave, sizeof(double)*s_ave) == 0)
		#endif
	#endif
		return (-1);
#endif
/*
 * XXX
 *   To calculate this, we need to compare
 *   successive values of the kernel array
 *   '_cp_times', and calculate the resulting
 *   percentage changes.
 *     This calculation needs to be performed
 *   regularly - perhaps as a background process.
 *
 *   See the source to 'top' for full details.
 *
 * The linux SNMP HostRes implementation
 *   uses 'avenrun[0]*100' as an approximation.
 *   This is less than accurate, but has the
 *   advantage of being simple to implement!
 *
 * I'm also assuming a single processor
 */
  return 0;
}

/********************  implementation from snapgear *******************/

struct stats {
	unsigned int	user;
	unsigned int	nice;
	unsigned int	system;
	unsigned int	idle;
	unsigned long	total;
};

typedef struct stats stats_t;


static int _getdata(FILE *fp,stats_t* st)
{
	unsigned char	buf[80];

	if (fseek(fp, 0, SEEK_SET) < 0)
		return(-1);
	fscanf(fp, "%s %d %d %d %d", &buf[0], &st->user, &st->nice,&st->system,&st->idle);

	st->total = st->user + st->nice + st->system + st->idle;
	return(0);
}


static int _get_cpu_usage()
{

	FILE			*fp;
	struct stats	st, stold;
	unsigned int	curtotal;
	char			*procdevice;
	int				busy,c,delay;
	int				cnt;
	
	delay		= 1;
	cnt			= 1;
	busy		= 0;
	procdevice	= "/proc/stat";
	
	if ((fp = fopen(procdevice, "r")) == NULL) {
		fprintf(stderr, "ERROR: failed to open %s, errno=%d\n",
			procdevice, errno);
			return -1;
	}

	if (setvbuf(fp, NULL, _IONBF, 0) != 0) {
		fprintf(stderr, "ERROR: failed to setvbuf(%s), errno=%d\n",
			procdevice, errno);
		return -1;
	}
	
	_getdata(fp,&st);
		
	for (c = 0; (c < cnt); c++) {
		sleep(delay);
		stold = st;
		_getdata(fp,&st);
		curtotal	= st.total - stold.total;
		
		/*
		printf("CPU:  busy %d%%  (system=%d%% user=%d%% "
			"nice=%d%% idle=%d%%)\n",
			((st.system + st.user + st.nice) -
			 (stold.system + stold.user + stold.nice)) *
			 100 / curtotal,
			(st.system - stold.system) * 100 / curtotal,
			(st.user - stold.user) * 100 / curtotal,
			(st.nice - stold.nice) * 100 / curtotal,
			idlepercent);
		*/
	}
	
	busy = 	((st.system + st.user + st.nice) - 
			(stold.system + stold.user + stold.nice)) *
			 100 / curtotal;
	
	fclose(fp);
	return busy;
	
}
