/* General includes */
#include <stdio.h>
#include <string.h>
#include <sys/time.h>
#include <sys/utsname.h>
#include <unistd.h>

/* SNMP includes */
#include "asn1.h"
#include "snmp.h"
#include "agt_mib.h"
#include "snmpv2-mib.h"

struct timeval snmp_boottime;
char sysDescr_value[ 128 ];
char sysLocation_value[ 128 ] = "[no location given]";
char sysContact_value[ 128 ] = "[no location given]";

unsigned long sysORLastChange_value = 0;
struct sysOREntry_struct snmpV2_value = { NULL, 0, {7, {O_snmpMIB}}, "The MIB module for SNMPv2 entities", 0};
struct sysOREntry_struct *sysOREntry_root = &snmpV2_value;


/* SNMPv2_MIB initialisation (must also register the MIB module tree) */

void init_SNMPv2_MIB()
{
    gettimeofday(&snmp_boottime, (struct timezone *)0);
    init_sysDescr();

    register_subtrees_of_SNMPv2_MIB();
}

int
init_sysDescr()
{
struct utsname  sysident;

    if (uname(&sysident)) {
	return(-1);
    }
    sprintf(sysDescr_value, "%s %s %s", sysident.sysname, sysident.release,
					sysident.machine);
    return(0);
}

char *
set_sysDescription(FILE *f, char *line)
{
    strncpy(sysDescr_value, line, 127);
    sysDescr_value[127] = '\0';
    return(0);
}

/* A textual description of the entity.  This value should
 * include the full name and version identification of the
 * system's hardware type, software operating-system, and
 * networking software.
 */

unsigned char *
var_sysDescr(int *var_len, Access_rec *access_id,int (**write_method)())
{
    *var_len = strlen(sysDescr_value);
    return (unsigned char *) &sysDescr_value;
}

/* The vendor's authoritative identification of the network
 * allocated within the SMI enterprises subtree (1.3.6.1.4.1)
 * and provides an easy and unambiguous means for determining
 * `Flintstones, Inc.' was assigned the subtree
 * 1.3.6.1.4.1.4242, it could assign the identifier
 * 1.3.6.1.4.1.4242.1.1 to its `Fred Router'.
 */

unsigned char *
var_sysObjectID(int *var_len, Access_rec *access_id,int (**write_method)())
{
static oid sysObjectID_value[] = {1, 3, 6, 1, 4, 1, 288330, 1};

    *var_len = sizeof(sysObjectID_value);
    return (unsigned char *) &sysObjectID_value;
}

unsigned long    current_sysUpTime()
{
struct timeval  now;

    gettimeofday(&now, (struct timezone *)0);
    return ((now.tv_sec - snmp_boottime.tv_sec) * 100
                + (now.tv_usec - snmp_boottime.tv_usec) / 10000);
}

/* The time (in hundredths of a second) since the network
 * management portion of the system was last re-initialized.
 */

unsigned char *
var_sysUpTime(int *var_len, Access_rec *access_id,
        int (**write_method)())
{
    *write_method = 0;
    *var_len = sizeof(long_return);
    long_return = current_sysUpTime();
    return (unsigned char *) &long_return;
}

char *
set_sysContact(FILE *f, char *line)
{
    strncpy(sysContact_value, line, 128);
    return(0);
}

/* The textual identification of the contact person for this
 * this person.  If no contact information is known, the value
 * is the zero-length string.
 */

int	write_sysContact(int action,
	unsigned char *var_val, unsigned char varval_type, int var_val_len,
	unsigned char *statP, oid *name, int name_len)
{
	/*
    switch (action) {
    case RESERVE1:
    case RESERVE2:
    case COMMIT:
    case FREE:
    }
	*/

	return 0;
}

unsigned char *
var_sysContact(int *var_len, Access_rec *access_id,
        int (**write_method)())
{
    /* Set write-function (uncomment if you want to implement it)  */
    /* *write_method = &write_sysContact(); */
    *var_len = strlen(sysContact_value);
    return (unsigned char *) sysContact_value;
}

/* An administratively-assigned name for this managed node.
 * By convention, this is the node's fully-qualified domain
 * name.  If the name is unknown, the value is the zero-length
 * string.
 */
/*
int	write_sysName(int action,
	unsigned char *var_val, unsigned char varval_type, int var_val_len,
	unsigned char *statP, oid *name, int name_len)
{
    switch (action) {
    case RESERVE1:
    case RESERVE2:
    case COMMIT:
    case FREE:
    }
}
*/

unsigned char *
var_sysName(int *var_len, Access_rec *access_id,
        int (**write_method)())
{
    *write_method = 0;
    *var_len = sizeof(long);
    if (0 == gethostname(return_buf, 50)) {
        *var_len = strlen(return_buf);
        return (unsigned char *) return_buf;
        }
    /* Should be GENERR */
    *var_len = sizeof(long);
    long_return = 0;
    return (unsigned char *) &long_return;
}

char *
set_sysLocation(FILE *f, char *line)
{
    strncpy(sysLocation_value, line, 128);
    return(0);
}

/* The physical location of this node (e.g., `telephone
 * closet, 3rd floor').  If the location is unknown, the value
 * is the zero-length string.
 */

int	write_sysLocation(int action,
	unsigned char *var_val, unsigned char varval_type, int var_val_len,
	unsigned char *statP, oid *name, int name_len)
{
	/*
    switch (action) {
    case RESERVE1:
    case RESERVE2:
    case COMMIT:
    case FREE:
    }*/
	return 0;
}


unsigned char *
var_sysLocation(int *var_len, Access_rec *access_id,
        int (**write_method)())
{
    /* Set write-function (uncomment if you want to implement it)  */
    *write_method = &write_sysLocation;
    *var_len = strlen(sysLocation_value);
    return (unsigned char *) sysLocation_value;
}

/* A value which indicates the set of services that this
 * entity may potentially offers.  The value is a sum.  This
 * sum initially takes the value zero, Then, for each layer, L,
 * in the range 1 through 7, that this node performs
 * transactions for, 2 raised to (L - 1) is added to the sum.
 * For example, a node which performs only routing functions
 * would have a value of 4 (2^(3-1)).  In contrast, a node
 * which is a host offering application services would have a
 * value of 72 (2^(4-1) + 2^(7-
 */

unsigned char *
var_sysServices(int *var_len, Access_rec *access_id,
        int (**write_method)())
{
    long_return = 72;
    /* Set size (in bytes) and return address of the variable */
    *var_len = sizeof(long);
    return (unsigned char *) &long_return;
}

int insert_sysOREntry(struct sysOREntry_struct *value)  
{
struct sysOREntry_struct *current = sysOREntry_root;

    while (current->next) {
            current = current->next;
    }
    value->index = current->index + 1;
    current->next = value;
    sysORLastChange_value = current_sysUpTime();
    current->lastChange = sysORLastChange_value;
    return(0); 
}   

/* The value of sysUpTime at the time of the most recent
 * change in state or value of any instance of sysORID.
 */

unsigned char *
var_sysORLastChange(int *var_len, Access_rec *access_id,
        int (**write_method)())
{
    *write_method = 0;
    *var_len = sizeof(sysORLastChange_value);
    return (unsigned char *) &sysORLastChange_value;
}

/* An entry (conceptual row) in the sysORTable.
 */

unsigned char *
var_sysOREntry(int *var_len,
        Oid *newoid, Oid *reqoid, int searchType,
        Access_rec *access_id, int (**write_method)())
{
/* Last subOID of COLUMNAR OID is column */
int     column		= newoid->name[(newoid->namelen - 1)];
int     sysORIndex	= newoid->namelen++;
/* Variables defined */
int     result;
struct sysOREntry_struct *current = sysOREntry_root;

    while (current) { 
        newoid->name[ sysORIndex ] = current->index;
        result = compare(reqoid, newoid);
        if (((searchType == EXACT) && (result == 0)) ||
                ((searchType == NEXT) && (result < 0))) {
           break;
        }
        current = current->next;
    }
    if (current == NULL) return(NULL);

    *write_method = 0;

    switch (column) {
        case I_sysORIndex:
            return (unsigned char *) NO_MIBINSTANCE;
        case I_sysORID:
            *var_len = current->oid_value.namelen * sizeof(oid);
            return (unsigned char *) &(current->oid_value.name);
        case I_sysORDescr:
            *var_len = strlen(current->descr);
            return (unsigned char *) current->descr;
        case I_sysORUpTime:
            *var_len = sizeof(unsigned long);
            return (unsigned char *) &(current->lastChange);
        default:
            return NO_MIBINSTANCE;
    }
}

/* The total number of messages delivered to the SNMP entity
 * from the transport service.
 */

unsigned char *
var_snmpInPkts(int *var_len, Access_rec *access_id,
        int (**write_method)())
{
extern long     snmpInPkts_instance;

    *write_method = 0;
    *var_len = sizeof(long);
    return (unsigned char *) &snmpInPkts_instance;
}

/* The total number of SNMP messages which were delivered to
 * the SNMP entity and were for an unsupported SNMP version.
 */

unsigned char *
var_snmpInBadVersions(int *var_len, Access_rec *access_id,
        int (**write_method)())
{
extern long     snmpInBadVersions_instance;

    *write_method = 0;
    *var_len = sizeof(long);
    return (unsigned char *) &snmpInBadVersions_instance;
}

/* The total number of SNMP messages delivered to the SNMP
 * entity which used a SNMP community name not known to said
 * entity.
 */

unsigned char *
var_snmpInBadCommunityNames(int *var_len, Access_rec *access_id,
        int (**write_method)())
{
extern long     snmpInBadCommunityNames_instance;

    *write_method = 0;
    *var_len = sizeof(long);
    return (unsigned char *) &snmpInBadCommunityNames_instance;
}

/* The total number of SNMP messages delivered to the SNMP
 * entity which represented an SNMP operation which was not
 * allowed by the SNMP community named in the message.
 */

unsigned char *
var_snmpInBadCommunityUses(int *var_len, Access_rec *access_id,
        int (**write_method)())
{
extern long     snmpInBadCommunityUses_instance;

    *write_method = 0;
    *var_len = sizeof(long);
    return (unsigned char *) &snmpInBadCommunityUses_instance;
}

/* The total number of ASN.1 or BER errors encountered by the
 * SNMP entity when decoding received SNMP messages.
 */

unsigned char *
var_snmpInASNParseErrs(int *var_len, Access_rec *access_id,
        int (**write_method)())
{
extern long     snmpInASNParseErrs_instance;

    *write_method = 0;
    *var_len = sizeof(long);
    return (unsigned char *) &snmpInASNParseErrs_instance;
}

/* Indicates whether the SNMP entity is permitted to generate
 * authenticationFailure traps.  The value of this object
 * overrides any configuration information; as such, it
 * provides a means whereby all authenticationFailure traps may
 * be disabled.
 * 
 * Note that it is strongly recommended that this object be
 * stored in non-volatile memory so that it remains constant
 * across re-initializations of the network management system.
 */
/*
int	write_snmpEnableAuthenTraps(int action,
	unsigned char *var_val, unsigned char varval_type, int var_val_len,
	unsigned char *statP, oid *name, int name_len)
{
    switch (action) {
    case RESERVE1:
    case RESERVE2:
    case COMMIT:
    case FREE:
    }
}
*/

unsigned char *
var_snmpEnableAuthenTraps(int *var_len, Access_rec *access_id,
        int (**write_method)())
{
extern long     snmpEnableAuthenTraps_instance;

    /* Set write-function (uncomment if you want to implement it)  */
    /* *write_method = &write_snmpEnableAuthenTraps(); */
    snmpEnableAuthenTraps_instance = 2;
    *write_method = 0;
    *var_len = sizeof(long);
    return (unsigned char *) &snmpEnableAuthenTraps_instance;
}

/* The total number of GetRequest-PDUs, GetNextRequest-PDUs,
 * GetBulkRequest-PDUs, SetRequest-PDUs, and InformRequest-PDUs
 * delivered to the SNMP entity which were silently dropped
 * because the size of a reply containing an alternate
 * Response-PDU with an empty variable-bindings field was
 * greater than either a local constraint or the maximum
 * message size associated with the originator of the request.
 */

unsigned char *
var_snmpSilentDrops(int *var_len, Access_rec *access_id,
        int (**write_method)())
{
 extern long    snmpSilentDrops_instance;
 
    *write_method = 0;
    *var_len = sizeof(long);
    return (unsigned char *) &snmpSilentDrops_instance;
}

/* The total number of GetRequest-PDUs, GetNextRequest-PDUs,
 * GetBulkRequest-PDUs, SetRequest-PDUs, and InformRequest-PDUs
 * delivered to the SNMP entity which were silently dropped
 * because the transmission of the (possibly translated)
 * time-out) such that no Response-PDU could be returned.
 */

unsigned char *
var_snmpProxyDrops(int *var_len, Access_rec *access_id,
        int (**write_method)())
{
extern long     snmpProxyDrops_instance;

    *write_method = 0;
    *var_len = sizeof(long);
    return (unsigned char *) &snmpProxyDrops_instance;
}

/* An advisory lock used to allow several cooperating SNMPv2
 * use of the SNMPv2 set operation.
 * 
 * This object is used for coarse-grain coordination.  To
 * achieve fine-grain coordination, one or more similar objects
 * might be defined within each MIB group, as appropriate.
 */
/*
int	write_snmpSetSerialNo(int action,
	unsigned char *var_val, unsigned char varval_type, int var_val_len,
	unsigned char *statP, oid *name, int name_len)
{
    switch (action) {
    case RESERVE1:
    case RESERVE2:
    case COMMIT:
    case FREE:
    }
}
*/

unsigned char *
var_snmpSetSerialNo(int *var_len, Access_rec *access_id,
        int (**write_method)())
{
    /* Add value computations */

    /* Set write-function (uncomment if you want to implement it)  */
    /* *write_method = &write_snmpSetSerialNo(); */
    /* Set size (in bytes) and return address of the variable */
    *var_len = sizeof(long);
    return (unsigned char *) NO_MIBINSTANCE;
}

static oid system_oid[] = { O_system };
static Object system_variables[] = {
    { SNMP_STRING, (RONLY| SCALAR), var_sysDescr,
                 {2, { I_sysDescr, 0 }}},
    { SNMP_OBJID, (RONLY| SCALAR), var_sysObjectID,
                 {2, { I_sysObjectID, 0 }}},
    { SNMP_TIMETICKS, (RONLY| SCALAR), var_sysUpTime,
                 {2, { I_sysUpTime, 0 }}},
    { SNMP_STRING, (RWRITE| SCALAR), var_sysContact,
                 {2, { I_sysContact, 0 }}},
    { SNMP_STRING, (RWRITE| SCALAR), var_sysName,
                 {2, { I_sysName, 0 }}},
    { SNMP_STRING, (RWRITE| SCALAR), var_sysLocation,
                 {2, { I_sysLocation, 0 }}},
    { SNMP_INTEGER, (RONLY| SCALAR), var_sysServices,
                 {2, { I_sysServices, 0 }}},
    { SNMP_TIMETICKS, (RONLY| SCALAR), var_sysORLastChange,
                 {2, { I_sysORLastChange, 0 }}},
    { SNMP_OBJID, (RONLY| COLUMN), var_sysOREntry,
                {3, { I_sysORTable, I_sysOREntry, I_sysORID }}},
    { SNMP_STRING, (RONLY| COLUMN), var_sysOREntry,
                {3, { I_sysORTable, I_sysOREntry, I_sysORDescr }}},
    { SNMP_TIMETICKS, (RONLY| COLUMN), var_sysOREntry,
                {3, { I_sysORTable, I_sysOREntry, I_sysORUpTime }}}
    };
static SubTree system_tree =  { NULL, system_variables,
	        (sizeof(system_oid)/sizeof(oid)), system_oid};

static oid snmp_oid[] = { O_snmp };
static Object snmp_variables[] = {
    { SNMP_COUNTER, (RONLY| SCALAR), var_snmpInPkts,
                 {2, { I_snmpInPkts, 0 }}},
    { SNMP_COUNTER, (RONLY| SCALAR), var_snmpInBadVersions,
                 {2, { I_snmpInBadVersions, 0 }}},
    { SNMP_COUNTER, (RONLY| SCALAR), var_snmpInBadCommunityNames,
                 {2, { I_snmpInBadCommunityNames, 0 }}},
    { SNMP_COUNTER, (RONLY| SCALAR), var_snmpInBadCommunityUses,
                 {2, { I_snmpInBadCommunityUses, 0 }}},
    { SNMP_COUNTER, (RONLY| SCALAR), var_snmpInASNParseErrs,
                 {2, { I_snmpInASNParseErrs, 0 }}},
    { SNMP_INTEGER, (RWRITE| SCALAR), var_snmpEnableAuthenTraps,
                 {2, { I_snmpEnableAuthenTraps, 0 }}},
    { SNMP_COUNTER, (RONLY| SCALAR), var_snmpSilentDrops,
                 {2, { I_snmpSilentDrops, 0 }}},
    { SNMP_COUNTER, (RONLY| SCALAR), var_snmpProxyDrops,
                 {2, { I_snmpProxyDrops, 0 }}}
    };
static SubTree snmp_tree =  { NULL, snmp_variables,
	        (sizeof(snmp_oid)/sizeof(oid)), snmp_oid};


/* This is the MIB registration function. This should be called */
/* within the init_SNMPv2_MIB-function */
void register_subtrees_of_SNMPv2_MIB()
{
    insert_group_in_mib(&system_tree);
    insert_group_in_mib(&snmp_tree);

}
