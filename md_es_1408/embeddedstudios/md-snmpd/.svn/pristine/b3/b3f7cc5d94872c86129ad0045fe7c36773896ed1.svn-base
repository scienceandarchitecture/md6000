/* ====================================================================
 * Copyright (c) 1997, 1998, 
 *                      SMASH, Harrie Hazewinkel.  All rights reserved.
 *
 * This product is developed by Harrie Hazewinkel and updates the
 * original SMUT compiler made as his graduation project at the
 * University of Twente.
 *
 * SMASH is a software package containing an SNMP MIB compiler and
 * an SNMP agent system. The package can be used for development
 * of monolithic SNMP agents and contains a compiler which compiles
 * MIB definitions into C-code to developed an SNMP agent.
 * More information about him and this software product can
 * be found on http://www-musiq.jrc.it/~harrie/SMASH/.
 *
 * With this file you should have received a file called
 * 'License'. If not you can pick one up from
 *
 *      http://www-musiq.jrc.it/~harrie/SMASH/licence.txt
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer. 
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 *
 * 3. All materials mentioning features or use of this
 *    software must display the following acknowledgment:
 *    "This product includes software developed by Harrie Hazewinkel"
 *
 * 4. The name of the Copyright holder must not be used to
 *    endorse or promote products derived from this software without
 *    prior written permission.
 *
 * 5. Redistributions of any form whatsoever must retain the following
 *    acknowledgment:
 *    "This product includes software developed by Harrie Hazewinkel"
 *    Also acknowledged are:
 *    - The Simple group of the University of Twente,
 *          http://wwwsnmp.cs.utwente.nl/
 *    - The MUSIQ workpackage of the DESIRE project,
 *          http://www-musiq.jrc.it/
 *    - The CEO programme managing the MUSIQ workpackage,
 *          http://www.ceo.org/
 *
 * THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR, ITS DISTRIBUTORS
 * ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 * ====================================================================*/

#include	<stdio.h>
#include	<ctype.h>
#include	<stdlib.h>
#include	<string.h>
#include	<sys/types.h>
#include	<sys/time.h>
#include	<unistd.h>
#include	<netinet/in.h>
#include	<sys/socket.h>
#include	<errno.h>
#include	<net/if.h>
#include	<sys/ioctl.h>
#include	<sys/file.h>

#include	<asn1.h>
#include	<snmp.h>
#include	<agt_engine.h>

#include	<snmp_community.h>
#include	"snmpv2-mib.h"
#include	"IF-MIB.h"
#include	"MDMESH.h"
#include	"HOST-RESOURCES-MIB.h"
#include	"MeshRuntimeMIB.h"

extern int  errno;
extern int	debug_packet;
int	log_addresses	= 0;
int	snmp_port		= SNMP_PORT;

void setDefaults()
{
	char	model[64];
	char	version[64];
	char	descr[128];
	FILE*	fp;

	system("cat /etc/meshap.conf | grep model | cut -d= -f2 > /etc/temp.txt");
	fp	= fopen("/etc/temp.txt","rt");
	fgets(model,64,fp);
	fclose(fp);

	system("cat /proc/net/meshap/version > /etc/temp.txt");
	fp	= fopen("/etc/temp.txt","rt");
	fgets(version,64,fp);
	fclose(fp);

	unlink("/etc/temp.txt");

	sprintf(descr,"Meshdynamics %s F/W %s\n",model,version);
	
	set_sysDescription(NULL,descr);
	set_sysContact(NULL,"Info@meshdynamics.com");
	set_sysLocation(NULL,"Outdoor");
	set_community(NULL,"public");
}

/* 
 * The main routine with which the SNMP-agent is started.
 */
int 	main(argc, argv)
    int	    argc;
    char    *argv[];
{
int	count;
int	snmp_socket;
int	numfds;
fd_set	fdset;

    /* Grab the arguments */
    for(count = 1; count < argc; count++){
	if (argv[count][0] == '-'){
	    switch(argv[count][1]){
		case 'd':
		    debug_packet++;
		    break;
                case 'p':
                    snmp_port = atoi(argv[++count]);
                    break;
		case 'a':
		    log_addresses++;
		    break;
		default:
		    printf("invalid option: -%c\n", argv[count][1]);
		    break;
	    }
	    continue;
	}
    }
     
    /* SNMP-agent initialisation functions */
    init_SNMPv2_MIB();
	init_IF_MIB(); 
	init_MDMESH();
	init_HOST_RESOURCES_MIB();
	init_RUNTIME_MIB();
	
	setDefaults();

	ensure_communities();
    
    /* Open the network */
    snmp_socket = snmp_open_connection(snmp_port);

    /* Listen to the network */
    while(1){
        numfds = 0;
        FD_ZERO(&fdset);
        numfds = snmp_socket + 1;
        FD_SET(snmp_socket, &fdset);
        count = select(numfds, &fdset, 0, 0, 0);
        if (count > 0){
            if (FD_ISSET(snmp_socket, &fdset)) {
                snmp_process_message(snmp_socket);
            }
        } else switch(count){
            case 0:
                break;
            case -1:
                if (errno == EINTR) {	continue;
                } else {		perror("select");
                }
            default:
                fprintf(stdout, "select returned %d\n", count);
        }
    }
}

