/********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : MeshRuntime.c
 * Comments : Mesh Runtime configuration
 * Created  : 6/19/2008
 * Author   : Prachiti Gaikwad
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * -----------------------------------------------------------------------------
 * | No  |Date      |  Comment                                        | Author |
 * -----------------------------------------------------------------------------
 * |  0  |6/19/2008 | Created.                                        |Prachiti|
 * -----------------------------------------------------------------------------
 * |  1  |6/19/2008 | count in get sta and kap is intialised          |Prachiti|
 * -----------------------------------------------------------------------------
 ********************************************************************************/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/types.h>
#include "MeshRuntime.h"
#include <sys/stat.h>
#include <unistd.h>
#include <getopt.h>

#define LEN		128


#define KAP_INFO "/proc/net/meshap/mesh/kap"
#define STA_INFO "/proc/net/meshap/access-point/sta-list"


void get_kap_info(unsigned char* file_data) 
{
	FILE*			fp;
	int				offset;
	
	offset = 0;

	fp = fopen(KAP_INFO,"r");
	if(fp == NULL) {
		fprintf(stderr,"SNMP:\tError opening KAP_INFO for reading...\n");
		return;
	}
	
	while (!feof(fp)) { 
		char ch;

		ch = fgetc(fp);
		file_data[offset++] = ch;
	}

	fclose(fp);

	file_data[offset] = 0;
	
}

int parse_kap_info (char* data,kap_info_t* kap_list,int* count) 
{

kap_info_t		kap_info;

#define _COPY_TO_BAR(dst,src) { char* pdst; pdst = dst; while(*src != 0 && *src != '|') { *pdst++ = *src++; } *pdst++ = 0; src++; }

	const char* p;
	int			ret,kap_count;
	char		temp[LEN];	
	
 enum {
	state_line_begin,
	state_skip_restofline
 }state;
	
	ret				= -1;
	p				= data;
	state			= state_line_begin;
	kap_count		= 0;

	while(*p) {
 
		switch(state) {
			case state_line_begin:
 
	   /**
		* Skip lines dashed lines
		*/
 
			if(*p == '-') {
    
				state = state_skip_restofline;
				p++;
 				continue;
 			}
 
			_COPY_TO_BAR(kap_info.kap_addr,p);
			_COPY_TO_BAR(kap_info.channel,p);
			_COPY_TO_BAR(kap_info.signal,p);
			_COPY_TO_BAR(kap_info.rate,p);
			_COPY_TO_BAR(kap_info.trate,p);
			_COPY_TO_BAR(temp,p);
			_COPY_TO_BAR(kap_info.disc,p);
			_COPY_TO_BAR(kap_info.flags,p);
			
			if(strcmp(kap_info.channel,"CHN")) {
				memcpy(&kap_list[kap_count++],&kap_info,sizeof(kap_info_t));
			}
			
			if(ret == 0) {
				return ret;
			}
			
			state = state_skip_restofline;
			p++;
 			break;

			case state_skip_restofline:
				if(*p == '\n') {
					state = state_line_begin;
				} 
				p++;
			break;
		}
	}
 
#undef _COPY_TO_BAR
	
	
	*count = kap_count;
	return ret;
}


int get_known_ap_list(kap_info_t* kap_info,int* count ) 
{
	char	data[4096];
	
	memset(data,0,sizeof(data));

	get_kap_info(data);		
	
	parse_kap_info(data,kap_info,count);
	
	return 0;
}

void get_sta_info(unsigned char*  file_data) 
{
	FILE*				fp;
	int					offset;
	
	
	offset = 0; 

	fp = fopen(STA_INFO,"r");

	if(fp == NULL) {
		fprintf(stderr,"SNMP:\tError opening STA_INFO for reading...\n");
		return;
	}
	
	while (!feof(fp)) { 
		char ch;

		ch = fgetc(fp);
		file_data[offset++] = ch;
	}

	fclose(fp);
	file_data[offset] = 0;

}

int parse_sta_info (char* data,sta_info_t* sta_list,int* count) 
{

sta_info_t		sta_info;

#define _COPY_TO_BAR(dst,src) { char* pdst; pdst = dst; while(*src != 0 && *src != '|') { *pdst++ = *src++; } *pdst++ = 0; src++; }

	const char* p;
	int			ret,sta_count;
	
 enum {
	state_line_begin,
	state_skip_restofline
 }state;
	
	ret				= -1;
	p				= data;
	state			= state_line_begin;
	sta_count		= 0;
				
	while(*p) {
 
		switch(state) {
			case state_line_begin:
 
	   /**
		* Skip lines dashed lines
		*/
 
			if(*p == '-') {
    
				state = state_skip_restofline;
				p++;
 				continue;
 			}
 
			_COPY_TO_BAR(sta_info.sta_addr,p);
			_COPY_TO_BAR(sta_info.ifname,p);
			_COPY_TO_BAR(sta_info.vtag,p);
			_COPY_TO_BAR(sta_info.signal,p);
			_COPY_TO_BAR(sta_info.rate,p);
			_COPY_TO_BAR(sta_info.last_packet_rx,p);
			_COPY_TO_BAR(sta_info.ref_count,p);
			_COPY_TO_BAR(sta_info.acl,p);
			
			

			if(strcmp(sta_info.ifname,"IFNAME")) {
				memcpy(&sta_list[sta_count++],&sta_info,sizeof(sta_info_t));
			}
			
			if(ret == 0) {
				return ret;
			}
			
			state = state_skip_restofline;
			p++;
 			break;

			case state_skip_restofline:
				if(*p == '\n') {
					state = state_line_begin;
				} 
				p++;
			break;
		}
	}
 
#undef _COPY_TO_BAR
	
	
	*count = sta_count;
	return ret;
}

int get_sta_list(sta_info_t* sta_info,int* count ) 
{
	unsigned char	data[4096];
	
	memset(data,0,sizeof(data));

	get_sta_info(data);		
	
	parse_sta_info(data,sta_info,count);
	
	return 0;
}

/*
void main (int argc, char** argv) 
{
	
	kap_info_t		kap_list[LEN];
	sta_info_t		sta_list[LEN];
	
	int count,i;
	
	memset(kap_list,0,sizeof(kap_list));
	memset(sta_list,0,sizeof(sta_list));
	
	get_known_ap_list(kap_list,&count);	
	
	for(i=0; i<count; i++) {
		printf("cha=%s\n", kap_list[i].channel);
	}

	get_sta_list(sta_list,&count);	
	
		for(i=0; i<count; i++) {
		printf("cha=%s\n", sta_list[i].ifname);
	}
}

*/
