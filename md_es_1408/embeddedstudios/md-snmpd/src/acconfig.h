#ifndef _snmp_config_h_
#define _snmp_config_h_

#ifdef WIN32
typedef unsigned long    u_long;
typedef unsigned char    u_char;
typedef unsigned short   u_short;

#define HAVE_ALLOCA_H
#define HAVE_CTYPE_H
#define HAVE_ERRNO_H
#define HAVE_IN_H
#define HAVE_IO_H
#define HAVE_MALLOC_H
#define HAVE_MEMORY_H
#define HAVE_NET_IF_H
#define HAVE_NETINET_IN_H
#define HAVE_SETJMP_H
#define HAVE_STDIO_H
#define HAVE_STDLIB_H
#define HAVE_STRING_H
#define HAVE_STRINGS_H
#define HAVE_SYS_FILE_H
#define HAVE_SYS_IOCTL_H
#define HAVE_SYS_SOCKET_H
#define HAVE_SYS_TIME_H
#define HAVE_SYS_TYPES_H
#define HAVE_TIME_H
#define HAVE_UNISTD_H
#define HAVE_WINSOCK_H

#else /* WIN32 not defined */
@TOP@
   @BOTTOM@
#endif /* WIN32 not defined */

#if defined(__STDC__) || defined(__GNUC__)
#define SNMP_VOID    void
#else
#define SNMP_VOID
#endif

#endif /* _snmp_config_h_ */
