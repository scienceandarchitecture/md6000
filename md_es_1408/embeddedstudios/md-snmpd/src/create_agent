#!/bin/sh

create_skeleton () {
	echo "Create MIB-module '$1' in file '$2'"
	echo "$1 DEFINITIONS ::= BEGIN" > $2
	echo "	-- the imports"		>> $2
	echo "	IMPORTS ;"		>> $2
	echo ""				>> $2
	echo "	-- The assignments"	>> $2
	echo ""				>> $2
	echo "END"			>> $2
}

echo "Set up of an SNMP-agent development directory."


if [ "x$1" = "x" ]; then
	echo "Usage: create_agent <name> <agenttype>"
	echo "Example: create-agent web_agent"
	echo "Result : web_agent-directory"
	echo "Example: create_agent web_agent ucd"
	echo "Result : ucd_web_agent-directory"
        exit 1
fi;

echo "Set up of an SNMP agent development."
if [ "$2" = "ucd" ]; then
	dir=ucd_$1
	echo "Type ucd in directory $dir"
else
	dir=$1
	echo "Type smash in directory $dir"
fi;

if [ -x $dir ]; then 
	echo "The directory $dir already exists. Therefore, do you."
	echo "want create a new Makefile (yes/NO)?"
	read overwrite
	if [ $overwrite != yes ]; then
		exit
	fi;
else
	mkdir $dir
	cp src_snmp/snmpd.c-dist $dir/snmpd.c
	cp src_snmp/snmpd.conf-dist $dir/snmpd.conf
fi;

if [ -r $dir/mapping ]; then
        rm $dir/mapping
fi;
touch $dir/mapping

echo "Give now the MIB-modules and MIB-files."
echo "MIB module?"
read module
while [ "x$module" != "x" ]; do
	echo "MIB file?"
	read file
	mibmodules="$module $mibmodules"
	#echo "modules $mibmodules"
	mibfiles="$file $mibfiles"
	#echo "files $mibfiles"
	# insert in mapping file
	echo "$file:$module"	>> $dir/mapping

	# check for existance of MIB module
	if [ -r mib_defs/$file ]; then
		echo "mib_defs/$file"
		cp mib_defs/$file $dir
	else
		echo "MIB-module '$module' does not exist."
		echo "Do you want to create a skeleton (yes/NO)?"
		read skeleton
		if [ "$skeleton" = "yes" ]; then
			file=$dir/$file
			echo "create_skeleton $module $file"
			create_skeleton $module $file
		fi
	fi

	echo "MIB module?"
	read module
done
cat `pwd`/mib_defs/mapping >> $dir/mapping
echo "+`pwd`/mib_defs/" >> $dir/mapping

if [ -r src_snmp/snmp_config.h ]; then
	have_config_h="-DHAVE_CONFIG_H"
else
	have_config_h=""
fi

os=`uname -s`
case "$os" in
    FreeBSD)
	extra_libs=""
	;;
    Solaris)
	extra_libs="-lnsl -lsocket"
	;;
esac

makefile=$dir/Makefile

echo "# Makefile for SNMP-agent $type in $1."			>  $makefile
echo "# Generated at `date`."					>> $makefile
echo "MC = ../src_compiler/smash"				>> $makefile
echo ""								>> $makefile
if [ "$2" = "ucd" ]; then
	echo "SNMP_INCL ="					>> $makefile
	echo "SNMP_LIBS ="					>> $makefile
else
	echo "SNMP_INCL = ../src_snmp"				>> $makefile
	echo "SNMP_LIBS = ../src_snmp/libsnmp_agt.a"		>> $makefile
fi;
echo ""								>> $makefile
echo "#"							>> $makefile
echo "# The MIB definitions which which the"			>> $makefile
echo "# SNMP-agent has to be implemented."			>> $makefile
echo "#"							>> $makefile
echo "MIB_DEFS = $mibmodules"					>> $makefile
echo "MIB_FILE = $mibfiles"					>> $makefile
echo "MIB_SRCS = \${MIB_FILE:txt=c}"				>> $makefile
echo "MIB_INCL = \${MIB_FILE:txt=h}"				>> $makefile
echo "MIB_OBJS = \${MIB_FILE:txt=o}"				>> $makefile
echo ""								>> $makefile
echo "#"							>> $makefile
echo "# These indicate the required source code to develop"	>> $makefile
echo "# the basic SNMP-agent."					>> $makefile
echo "#"							>> $makefile
if [ "$2" = "ucd" ]; then
	echo "AGT_SRCS ="					>> $makefile
else
	echo "AGT_SRCS = snmpd.c mib_vars.c"			>> $makefile
fi;
echo "AGT_OBJS = \${AGT_SRCS:c=o}"				>> $makefile
echo "#"							>> $makefile
echo "# Put here your extra required C-sources, object-files,"	>> $makefile
echo "# include-files, and libraries."				>> $makefile
echo "#"							>> $makefile
echo "YOUR_SRCS ="						>> $makefile
echo "YOUR_OBJS ="						>> $makefile
echo "YOUR_INCL ="						>> $makefile
echo "YOUR_LIBS ="						>> $makefile
echo ""								>> $makefile
echo "#"							>> $makefile
echo "# The USED FLAGS for Compiling the SNMP-agent"		>> $makefile
echo "#"							>> $makefile
echo "# MIB compiler flags"					>> $makefile
echo "#"							>> $makefile
if [ "$2" = "ucd" ]; then
	echo "MCFLAGS = -u"					>> $makefile
else
	echo "MCFLAGS = -c"					>> $makefile
fi;
echo "#"							>> $makefile
echo "# C compiler flags"					>> $makefile
echo "#"							>> $makefile
echo "CFLAGS = -Wall -I\${SNMP_INCL} $have_config_h"		>> $makefile
echo "LFLAGS ="							>> $makefile
echo "DEFINES ="						>> $makefile
echo "OBJS = \${MIB_OBJS} \${AGT_OBJS} \${YOUR_OBJS}"		>> $makefile
echo "LIBS = \${SNMP_LIBS} \${YOUR_LIBS} $extra_libs"		>> $makefile
echo ""								>> $makefile
echo ""								>> $makefile
echo "all: snmpd"						>> $makefile
echo ""								>> $makefile
echo ".c.o:"							>> $makefile
echo "	\$(CC) \$(CFLAGS) -c -o \$@ \$<"			>> $makefile
echo ""								>> $makefile
echo "snmpd:	\${OBJS} \${LIBS}"				>> $makefile
echo "	\${CC} -o \$@ \${OBJS} \${LIBS}"			>> $makefile
echo ""								>> $makefile
echo ""								>> $makefile
echo "stamp: \${MIB_FILE}"					>> $makefile
echo ""								>> $makefile
echo "\${MIB_SRCS} \${MIB_INCL}: stamp"				>> $makefile
echo "	\${MC} \${MCFLAGS} \${MIB_DEFS}"			>> $makefile
echo "	touch stamp"						>> $makefile
echo ""								>> $makefile
echo "clean:"							>> $makefile
echo "	rm *.o snmpd"						>> $makefile
echo ""								>> $makefile
echo "\${MIB_OBJS}:	\${MIB_SRCS} \${MIB_INCL}"		>> $makefile
if [ "$2" = "ucd" ]; then
	echo ""							>> $makefile
else
	echo "mib_vars.o: mib_vars.c ../src_snmp/agt_mib.h"	>> $makefile
fi;
