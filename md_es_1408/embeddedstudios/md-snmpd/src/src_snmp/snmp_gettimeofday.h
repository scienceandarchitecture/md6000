#ifndef _snmp_gettimeofday_h_
#define _snmp_gettimeofday_h_

#ifdef WIN32
#  define gettimeofday    snmp_gettimeofday
/* "struct timeval" is defined in winsock.h on NT */
#  include <winsock.h>
int gettimeofday(struct timeval *, void *);

#elif defined TIME_WITH_SYS_TIME
#  include <sys/time.h>
#  include <time.h>
#else
#  ifdef HAVE_SYS_TIME_H
#    include <sys/time.h>
#  else
#    include <time.h>
#  endif
#endif

#endif /* _snmp_gettimeofday_h_ */
