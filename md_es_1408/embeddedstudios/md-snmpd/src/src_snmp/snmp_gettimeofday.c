#include "snmp_gettimeofday.h"

int
snmp_gettimeofday(tp, ptr)
struct timeval *tp;

void *ptr;
{
#ifdef WIN32
   SYSTEMTIME st;
   GetSystemTime(&st);
   tp->tv_sec  = st.wHour * 3600 + st.wMinute * 60 + st.wSecond;
   tp->tv_usec = st.wMilliseconds * 1000;
   return 0;

#else
   return -1;
#endif
}
