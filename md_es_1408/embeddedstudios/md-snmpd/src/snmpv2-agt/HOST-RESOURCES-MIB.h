#ifndef _HOST_RESOURCES_MIB_
#define _HOST_RESOURCES_MIB_

/* required include files (IMPORTS) */
#include        "IF-MIB.h"
#include        "IANAifType-MIB.h"
#include        "snmpv2-mib.h"
#include        "snmpv2-conf.h"
#include        "snmpv2-tc.h"
#include        "snmpv2-smi.h"


void init_HOST_RESOURCES_MIB();
void register_subtrees_of_HOST_RESOURCES_MIB();

/* defined objects in this module */


/* MIB object host = mib_2, 25 */
#define I_host                             25
#define O_host                             1, 3, 6, 1, 2, 1, 25

/* MIB object hostResourcesMibModule = hrMIBAdminInfo, 1 */
#define I_hostResourcesMibModule           1
#define O_hostResourcesMibModule           1, 3, 6, 1, 2, 1, 25, 7, 1

/* MIB object hrSystem = host, 1 */
#define I_hrSystem                         1
#define O_hrSystem                         1, 3, 6, 1, 2, 1, 25, 1

/* MIB object hrStorage = host, 2 */
#define I_hrStorage                        2
#define O_hrStorage                        1, 3, 6, 1, 2, 1, 25, 2

/* MIB object hrDevice = host, 3 */
#define I_hrDevice                         3
#define O_hrDevice                         1, 3, 6, 1, 2, 1, 25, 3

/* MIB object hrSWRun = host, 4 */
#define I_hrSWRun                          4
#define O_hrSWRun                          1, 3, 6, 1, 2, 1, 25, 4

/* MIB object hrSWRunPerf = host, 5 */
#define I_hrSWRunPerf                      5
#define O_hrSWRunPerf                      1, 3, 6, 1, 2, 1, 25, 5

/* MIB object hrSWInstalled = host, 6 */
#define I_hrSWInstalled                    6
#define O_hrSWInstalled                    1, 3, 6, 1, 2, 1, 25, 6

/* MIB object hrMIBAdminInfo = host, 7 */
#define I_hrMIBAdminInfo                   7
#define O_hrMIBAdminInfo                   1, 3, 6, 1, 2, 1, 25, 7

/* MIB object hrSystemUptime = hrSystem, 1 */
#define I_hrSystemUptime                   1
#define O_hrSystemUptime                   1, 3, 6, 1, 2, 1, 25, 1, 1

/* MIB object hrSystemDate = hrSystem, 2 */
#define I_hrSystemDate                     2
#define O_hrSystemDate                     1, 3, 6, 1, 2, 1, 25, 1, 2

/* MIB object hrSystemInitialLoadDevice = hrSystem, 3 */
#define I_hrSystemInitialLoadDevice        3
#define O_hrSystemInitialLoadDevice        1, 3, 6, 1, 2, 1, 25, 1, 3

/* MIB object hrSystemInitialLoadParameters = hrSystem, 4 */
#define I_hrSystemInitialLoadParameters    4
#define O_hrSystemInitialLoadParameters    1, 3, 6, 1, 2, 1, 25, 1, 4

/* MIB object hrSystemNumUsers = hrSystem, 5 */
#define I_hrSystemNumUsers                 5
#define O_hrSystemNumUsers                 1, 3, 6, 1, 2, 1, 25, 1, 5

/* MIB object hrSystemProcesses = hrSystem, 6 */
#define I_hrSystemProcesses                6
#define O_hrSystemProcesses                1, 3, 6, 1, 2, 1, 25, 1, 6

/* MIB object hrSystemMaxProcesses = hrSystem, 7 */
#define I_hrSystemMaxProcesses             7
#define O_hrSystemMaxProcesses             1, 3, 6, 1, 2, 1, 25, 1, 7

/* MIB object hrStorageTypes = hrStorage, 1 */
#define I_hrStorageTypes                   1
#define O_hrStorageTypes                   1, 3, 6, 1, 2, 1, 25, 2, 1

/* MIB object hrMemorySize = hrStorage, 2 */
#define I_hrMemorySize                     2
#define O_hrMemorySize                     1, 3, 6, 1, 2, 1, 25, 2, 2

/* MIB object hrStorageTable = hrStorage, 3 */
#define I_hrStorageTable                   3
#define O_hrStorageTable                   1, 3, 6, 1, 2, 1, 25, 2, 3

/* MIB object hrStorageEntry = hrStorageTable, 1 */
#define I_hrStorageEntry                   1
#define O_hrStorageEntry                   1, 3, 6, 1, 2, 1, 25, 2, 3, 1

/* MIB object hrStorageIndex = hrStorageEntry, 1 */
#define I_hrStorageIndex                   1
#define O_hrStorageIndex                   1, 3, 6, 1, 2, 1, 25, 2, 3, 1, 1

/* MIB object hrStorageType = hrStorageEntry, 2 */
#define I_hrStorageType                    2
#define O_hrStorageType                    1, 3, 6, 1, 2, 1, 25, 2, 3, 1, 2

/* MIB object hrStorageDescr = hrStorageEntry, 3 */
#define I_hrStorageDescr                   3
#define O_hrStorageDescr                   1, 3, 6, 1, 2, 1, 25, 2, 3, 1, 3

/* MIB object hrStorageAllocationUnits = hrStorageEntry, 4 */
#define I_hrStorageAllocationUnits         4
#define O_hrStorageAllocationUnits         1, 3, 6, 1, 2, 1, 25, 2, 3, 1, 4

/* MIB object hrStorageSize = hrStorageEntry, 5 */
#define I_hrStorageSize                    5
#define O_hrStorageSize                    1, 3, 6, 1, 2, 1, 25, 2, 3, 1, 5

/* MIB object hrStorageUsed = hrStorageEntry, 6 */
#define I_hrStorageUsed                    6
#define O_hrStorageUsed                    1, 3, 6, 1, 2, 1, 25, 2, 3, 1, 6

/* MIB object hrStorageAllocationFailures = hrStorageEntry, 7 */
#define I_hrStorageAllocationFailures      7
#define O_hrStorageAllocationFailures      1, 3, 6, 1, 2, 1, 25, 2, 3, 1, 7

/* MIB object hrDeviceTypes = hrDevice, 1 */
#define I_hrDeviceTypes                    1
#define O_hrDeviceTypes                    1, 3, 6, 1, 2, 1, 25, 3, 1

/* MIB object hrDeviceTable = hrDevice, 2 */
#define I_hrDeviceTable                    2
#define O_hrDeviceTable                    1, 3, 6, 1, 2, 1, 25, 3, 2

/* MIB object hrDeviceEntry = hrDeviceTable, 1 */
#define I_hrDeviceEntry                    1
#define O_hrDeviceEntry                    1, 3, 6, 1, 2, 1, 25, 3, 2, 1

/* MIB object hrDeviceIndex = hrDeviceEntry, 1 */
#define I_hrDeviceIndex                    1
#define O_hrDeviceIndex                    1, 3, 6, 1, 2, 1, 25, 3, 2, 1, 1

/* MIB object hrDeviceType = hrDeviceEntry, 2 */
#define I_hrDeviceType                     2
#define O_hrDeviceType                     1, 3, 6, 1, 2, 1, 25, 3, 2, 1, 2

/* MIB object hrDeviceDescr = hrDeviceEntry, 3 */
#define I_hrDeviceDescr                    3
#define O_hrDeviceDescr                    1, 3, 6, 1, 2, 1, 25, 3, 2, 1, 3

/* MIB object hrDeviceID = hrDeviceEntry, 4 */
#define I_hrDeviceID                       4
#define O_hrDeviceID                       1, 3, 6, 1, 2, 1, 25, 3, 2, 1, 4

/* MIB object hrDeviceStatus = hrDeviceEntry, 5 */
#define I_hrDeviceStatus                   5
#define O_hrDeviceStatus                   1, 3, 6, 1, 2, 1, 25, 3, 2, 1, 5

/* MIB object hrDeviceErrors = hrDeviceEntry, 6 */
#define I_hrDeviceErrors                   6
#define O_hrDeviceErrors                   1, 3, 6, 1, 2, 1, 25, 3, 2, 1, 6

/* MIB object hrProcessorTable = hrDevice, 3 */
#define I_hrProcessorTable                 3
#define O_hrProcessorTable                 1, 3, 6, 1, 2, 1, 25, 3, 3

/* MIB object hrProcessorEntry = hrProcessorTable, 1 */
#define I_hrProcessorEntry                 1
#define O_hrProcessorEntry                 1, 3, 6, 1, 2, 1, 25, 3, 3, 1

/* MIB object hrProcessorFrwID = hrProcessorEntry, 1 */
#define I_hrProcessorFrwID                 1
#define O_hrProcessorFrwID                 1, 3, 6, 1, 2, 1, 25, 3, 3, 1, 1

/* MIB object hrProcessorLoad = hrProcessorEntry, 2 */
#define I_hrProcessorLoad                  2
#define O_hrProcessorLoad                  1, 3, 6, 1, 2, 1, 25, 3, 3, 1, 2

/* MIB object hrNetworkTable = hrDevice, 4 */
#define I_hrNetworkTable                   4
#define O_hrNetworkTable                   1, 3, 6, 1, 2, 1, 25, 3, 4

/* MIB object hrNetworkEntry = hrNetworkTable, 1 */
#define I_hrNetworkEntry                   1
#define O_hrNetworkEntry                   1, 3, 6, 1, 2, 1, 25, 3, 4, 1

/* MIB object hrNetworkIfIndex = hrNetworkEntry, 1 */
#define I_hrNetworkIfIndex                 1
#define O_hrNetworkIfIndex                 1, 3, 6, 1, 2, 1, 25, 3, 4, 1, 1

/* MIB object hrPrinterTable = hrDevice, 5 */
#define I_hrPrinterTable                   5
#define O_hrPrinterTable                   1, 3, 6, 1, 2, 1, 25, 3, 5

/* MIB object hrPrinterEntry = hrPrinterTable, 1 */
#define I_hrPrinterEntry                   1
#define O_hrPrinterEntry                   1, 3, 6, 1, 2, 1, 25, 3, 5, 1

/* MIB object hrPrinterStatus = hrPrinterEntry, 1 */
#define I_hrPrinterStatus                  1
#define O_hrPrinterStatus                  1, 3, 6, 1, 2, 1, 25, 3, 5, 1, 1

/* MIB object hrPrinterDetectedErrorState = hrPrinterEntry, 2 */
#define I_hrPrinterDetectedErrorState      2
#define O_hrPrinterDetectedErrorState      1, 3, 6, 1, 2, 1, 25, 3, 5, 1, 2

/* MIB object hrDiskStorageTable = hrDevice, 6 */
#define I_hrDiskStorageTable               6
#define O_hrDiskStorageTable               1, 3, 6, 1, 2, 1, 25, 3, 6

/* MIB object hrDiskStorageEntry = hrDiskStorageTable, 1 */
#define I_hrDiskStorageEntry               1
#define O_hrDiskStorageEntry               1, 3, 6, 1, 2, 1, 25, 3, 6, 1

/* MIB object hrDiskStorageAccess = hrDiskStorageEntry, 1 */
#define I_hrDiskStorageAccess              1
#define O_hrDiskStorageAccess              1, 3, 6, 1, 2, 1, 25, 3, 6, 1, 1

/* MIB object hrDiskStorageMedia = hrDiskStorageEntry, 2 */
#define I_hrDiskStorageMedia               2
#define O_hrDiskStorageMedia               1, 3, 6, 1, 2, 1, 25, 3, 6, 1, 2

/* MIB object hrDiskStorageRemoveble = hrDiskStorageEntry, 3 */
#define I_hrDiskStorageRemoveble           3
#define O_hrDiskStorageRemoveble           1, 3, 6, 1, 2, 1, 25, 3, 6, 1, 3

/* MIB object hrDiskStorageCapacity = hrDiskStorageEntry, 4 */
#define I_hrDiskStorageCapacity            4
#define O_hrDiskStorageCapacity            1, 3, 6, 1, 2, 1, 25, 3, 6, 1, 4

/* MIB object hrPartitionTable = hrDevice, 7 */
#define I_hrPartitionTable                 7
#define O_hrPartitionTable                 1, 3, 6, 1, 2, 1, 25, 3, 7

/* MIB object hrPartitionEntry = hrPartitionTable, 1 */
#define I_hrPartitionEntry                 1
#define O_hrPartitionEntry                 1, 3, 6, 1, 2, 1, 25, 3, 7, 1

/* MIB object hrPartitionIndex = hrPartitionEntry, 1 */
#define I_hrPartitionIndex                 1
#define O_hrPartitionIndex                 1, 3, 6, 1, 2, 1, 25, 3, 7, 1, 1

/* MIB object hrPartitionLabel = hrPartitionEntry, 2 */
#define I_hrPartitionLabel                 2
#define O_hrPartitionLabel                 1, 3, 6, 1, 2, 1, 25, 3, 7, 1, 2

/* MIB object hrPartitionID = hrPartitionEntry, 3 */
#define I_hrPartitionID                    3
#define O_hrPartitionID                    1, 3, 6, 1, 2, 1, 25, 3, 7, 1, 3

/* MIB object hrPartitionSize = hrPartitionEntry, 4 */
#define I_hrPartitionSize                  4
#define O_hrPartitionSize                  1, 3, 6, 1, 2, 1, 25, 3, 7, 1, 4

/* MIB object hrPartitionFSIndex = hrPartitionEntry, 5 */
#define I_hrPartitionFSIndex               5
#define O_hrPartitionFSIndex               1, 3, 6, 1, 2, 1, 25, 3, 7, 1, 5

/* MIB object hrFSTypes = hrDevice, 9 */
#define I_hrFSTypes                        9
#define O_hrFSTypes                        1, 3, 6, 1, 2, 1, 25, 3, 9

/* MIB object hrFSTable = hrDevice, 8 */
#define I_hrFSTable                        8
#define O_hrFSTable                        1, 3, 6, 1, 2, 1, 25, 3, 8

/* MIB object hrFSEntry = hrFSTable, 1 */
#define I_hrFSEntry                        1
#define O_hrFSEntry                        1, 3, 6, 1, 2, 1, 25, 3, 8, 1

/* MIB object hrFSIndex = hrFSEntry, 1 */
#define I_hrFSIndex                        1
#define O_hrFSIndex                        1, 3, 6, 1, 2, 1, 25, 3, 8, 1, 1

/* MIB object hrFSMountPoint = hrFSEntry, 2 */
#define I_hrFSMountPoint                   2
#define O_hrFSMountPoint                   1, 3, 6, 1, 2, 1, 25, 3, 8, 1, 2

/* MIB object hrFSRemoteMountPoint = hrFSEntry, 3 */
#define I_hrFSRemoteMountPoint             3
#define O_hrFSRemoteMountPoint             1, 3, 6, 1, 2, 1, 25, 3, 8, 1, 3

/* MIB object hrFSType = hrFSEntry, 4 */
#define I_hrFSType                         4
#define O_hrFSType                         1, 3, 6, 1, 2, 1, 25, 3, 8, 1, 4

/* MIB object hrFSAccess = hrFSEntry, 5 */
#define I_hrFSAccess                       5
#define O_hrFSAccess                       1, 3, 6, 1, 2, 1, 25, 3, 8, 1, 5

/* MIB object hrFSBootable = hrFSEntry, 6 */
#define I_hrFSBootable                     6
#define O_hrFSBootable                     1, 3, 6, 1, 2, 1, 25, 3, 8, 1, 6

/* MIB object hrFSStorageIndex = hrFSEntry, 7 */
#define I_hrFSStorageIndex                 7
#define O_hrFSStorageIndex                 1, 3, 6, 1, 2, 1, 25, 3, 8, 1, 7

/* MIB object hrFSLastFullBackupDate = hrFSEntry, 8 */
#define I_hrFSLastFullBackupDate           8
#define O_hrFSLastFullBackupDate           1, 3, 6, 1, 2, 1, 25, 3, 8, 1, 8

/* MIB object hrFSLastPartialBackupDate = hrFSEntry, 9 */
#define I_hrFSLastPartialBackupDate        9
#define O_hrFSLastPartialBackupDate        1, 3, 6, 1, 2, 1, 25, 3, 8, 1, 9

/* MIB object hrSWOSIndex = hrSWRun, 1 */
#define I_hrSWOSIndex                      1
#define O_hrSWOSIndex                      1, 3, 6, 1, 2, 1, 25, 4, 1

/* MIB object hrSWRunTable = hrSWRun, 2 */
#define I_hrSWRunTable                     2
#define O_hrSWRunTable                     1, 3, 6, 1, 2, 1, 25, 4, 2

/* MIB object hrSWRunEntry = hrSWRunTable, 1 */
#define I_hrSWRunEntry                     1
#define O_hrSWRunEntry                     1, 3, 6, 1, 2, 1, 25, 4, 2, 1

/* MIB object hrSWRunIndex = hrSWRunEntry, 1 */
#define I_hrSWRunIndex                     1
#define O_hrSWRunIndex                     1, 3, 6, 1, 2, 1, 25, 4, 2, 1, 1

/* MIB object hrSWRunName = hrSWRunEntry, 2 */
#define I_hrSWRunName                      2
#define O_hrSWRunName                      1, 3, 6, 1, 2, 1, 25, 4, 2, 1, 2

/* MIB object hrSWRunID = hrSWRunEntry, 3 */
#define I_hrSWRunID                        3
#define O_hrSWRunID                        1, 3, 6, 1, 2, 1, 25, 4, 2, 1, 3

/* MIB object hrSWRunPath = hrSWRunEntry, 4 */
#define I_hrSWRunPath                      4
#define O_hrSWRunPath                      1, 3, 6, 1, 2, 1, 25, 4, 2, 1, 4

/* MIB object hrSWRunParameters = hrSWRunEntry, 5 */
#define I_hrSWRunParameters                5
#define O_hrSWRunParameters                1, 3, 6, 1, 2, 1, 25, 4, 2, 1, 5

/* MIB object hrSWRunType = hrSWRunEntry, 6 */
#define I_hrSWRunType                      6
#define O_hrSWRunType                      1, 3, 6, 1, 2, 1, 25, 4, 2, 1, 6

/* MIB object hrSWRunStatus = hrSWRunEntry, 7 */
#define I_hrSWRunStatus                    7
#define O_hrSWRunStatus                    1, 3, 6, 1, 2, 1, 25, 4, 2, 1, 7

/* MIB object hrSWRunPerfTable = hrSWRunPerf, 1 */
#define I_hrSWRunPerfTable                 1
#define O_hrSWRunPerfTable                 1, 3, 6, 1, 2, 1, 25, 5, 1

/* MIB object hrSWRunPerfEntry = hrSWRunPerfTable, 1 */
#define I_hrSWRunPerfEntry                 1
#define O_hrSWRunPerfEntry                 1, 3, 6, 1, 2, 1, 25, 5, 1, 1

/* MIB object hrSWRunPerfCPU = hrSWRunPerfEntry, 1 */
#define I_hrSWRunPerfCPU                   1
#define O_hrSWRunPerfCPU                   1, 3, 6, 1, 2, 1, 25, 5, 1, 1, 1

/* MIB object hrSWRunPerfMem = hrSWRunPerfEntry, 2 */
#define I_hrSWRunPerfMem                   2
#define O_hrSWRunPerfMem                   1, 3, 6, 1, 2, 1, 25, 5, 1, 1, 2

/* MIB object hrSWInstalledLastChange = hrSWInstalled, 1 */
#define I_hrSWInstalledLastChange          1
#define O_hrSWInstalledLastChange          1, 3, 6, 1, 2, 1, 25, 6, 1

/* MIB object hrSWInstalledLastUpdateTime = hrSWInstalled, 2 */
#define I_hrSWInstalledLastUpdateTime      2
#define O_hrSWInstalledLastUpdateTime      1, 3, 6, 1, 2, 1, 25, 6, 2

/* MIB object hrSWInstalledTable = hrSWInstalled, 3 */
#define I_hrSWInstalledTable               3
#define O_hrSWInstalledTable               1, 3, 6, 1, 2, 1, 25, 6, 3

/* MIB object hrSWInstalledEntry = hrSWInstalledTable, 1 */
#define I_hrSWInstalledEntry               1
#define O_hrSWInstalledEntry               1, 3, 6, 1, 2, 1, 25, 6, 3, 1

/* MIB object hrSWInstalledIndex = hrSWInstalledEntry, 1 */
#define I_hrSWInstalledIndex               1
#define O_hrSWInstalledIndex               1, 3, 6, 1, 2, 1, 25, 6, 3, 1, 1

/* MIB object hrSWInstalledName = hrSWInstalledEntry, 2 */
#define I_hrSWInstalledName                2
#define O_hrSWInstalledName                1, 3, 6, 1, 2, 1, 25, 6, 3, 1, 2

/* MIB object hrSWInstalledID = hrSWInstalledEntry, 3 */
#define I_hrSWInstalledID                  3
#define O_hrSWInstalledID                  1, 3, 6, 1, 2, 1, 25, 6, 3, 1, 3

/* MIB object hrSWInstalledType = hrSWInstalledEntry, 4 */
#define I_hrSWInstalledType                4
#define O_hrSWInstalledType                1, 3, 6, 1, 2, 1, 25, 6, 3, 1, 4

/* MIB object hrSWInstalledDate = hrSWInstalledEntry, 5 */
#define I_hrSWInstalledDate                5
#define O_hrSWInstalledDate                1, 3, 6, 1, 2, 1, 25, 6, 3, 1, 5

/* MIB object hrMIBCompliances = hrMIBAdminInfo, 2 */
#define I_hrMIBCompliances                 2
#define O_hrMIBCompliances                 1, 3, 6, 1, 2, 1, 25, 7, 2

/* MIB object hrMIBGroups = hrMIBAdminInfo, 3 */
#define I_hrMIBGroups                      3
#define O_hrMIBGroups                      1, 3, 6, 1, 2, 1, 25, 7, 3

/* MIB object hrMIBCompliance = hrMIBCompliances, 1 */
#define I_hrMIBCompliance                  1
#define O_hrMIBCompliance                  1, 3, 6, 1, 2, 1, 25, 7, 2, 1

/* MIB object hrSystemGroup = hrMIBGroups, 1 */
#define I_hrSystemGroup                    1
#define O_hrSystemGroup                    1, 3, 6, 1, 2, 1, 25, 7, 3, 1

/* MIB object hrStorageGroup = hrMIBGroups, 2 */
#define I_hrStorageGroup                   2
#define O_hrStorageGroup                   1, 3, 6, 1, 2, 1, 25, 7, 3, 2

/* MIB object hrDeviceGroup = hrMIBGroups, 3 */
#define I_hrDeviceGroup                    3
#define O_hrDeviceGroup                    1, 3, 6, 1, 2, 1, 25, 7, 3, 3

/* MIB object hrSWRunGroup = hrMIBGroups, 4 */
#define I_hrSWRunGroup                     4
#define O_hrSWRunGroup                     1, 3, 6, 1, 2, 1, 25, 7, 3, 4

/* MIB object hrSWRunPerfGroup = hrMIBGroups, 5 */
#define I_hrSWRunPerfGroup                 5
#define O_hrSWRunPerfGroup                 1, 3, 6, 1, 2, 1, 25, 7, 3, 5

/* MIB object hrSWInstalledGroup = hrMIBGroups, 6 */
#define I_hrSWInstalledGroup               6
#define O_hrSWInstalledGroup               1, 3, 6, 1, 2, 1, 25, 7, 3, 6

/* Put here additional MIB specific include definitions */

#endif  //_HOST_RESOURCES_MIB_
