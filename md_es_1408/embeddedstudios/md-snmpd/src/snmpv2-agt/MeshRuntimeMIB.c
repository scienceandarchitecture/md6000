/********************************************************************************
* MeshDynamics
* --------------
* File     : MeshRuntimeMIB.c
* Comments : Mesh Runtime configuration MIB
* Created  : 6/19/2008
* Author   : Prachiti Gaikwad
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  1  |6/24/2008 | Added Voltage & Temp                            |Prachiti|
* -----------------------------------------------------------------------------
* |  0  |6/19/2008 | Created.                                        |Prachiti|
* -----------------------------------------------------------------------------
********************************************************************************/

/* General includes */
#include <stdio.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>

/* SNMP includes */
#include "asn1.h"
#include "snmp.h"
#include "agt_mib.h"
#include "agt_engine.h"
#include "MeshRuntimeMIB.h"
#include "MeshRuntime.h"

#define _SPRINT_MAX_LEN    256
#define LEN                128

static int  int_return;
static char _buffer[_SPRINT_MAX_LEN];


/* MESH_MIB initialisation (must also register the MIB module tree) */
void init_RUNTIME_MIB()
{
   register_subtrees_of_RUNTIME_MIB();
}


/* Row Description
 */
unsigned char *
var_kapEntry(int *var_len,
             Oid *newoid, Oid *reqoid, int searchType,
             snmp_info_t *mesg, int(**write_method)())
{
/* Last subOID of COLUMNAR OID is column */
   int column;
   int result;
   int pos;
   int count, i;


   column = newoid->name[(newoid->namelen - 1)];
   pos    = newoid->namelen++;

   memset(_buffer, 0, _SPRINT_MAX_LEN);

   kap_info_t kap_list[LEN];
   kap_info_t *kap_info;

   memset(kap_list, 0, sizeof(kap_list));
   kap_info = NULL;
   count    = 0;

   get_known_ap_list(kap_list, &count);

   for (i = 1; i <= count; i++)
   {
      kap_info          = &kap_list[i - 1];
      newoid->name[pos] = i;
      result            = compare(reqoid, newoid);
      if (((searchType == EXACT) && (result == 0)) ||
          ((searchType == NEXT) && (result < 0)))
      {
         break;
      }
   }

   if (i > count - 1)
   {
      return NULL;
   }


   /* Set size (in bytes) and return address of the variable */
   *var_len = sizeof(long);

   if (kap_info == NULL)
   {
      return NO_MIBINSTANCE;
   }

   switch (column)
   {
   case I_kapaddr:
      sprintf(_buffer, kap_info->kap_addr);
      *var_len = strlen(_buffer);
      break;

   case I_kapsignal:
      sprintf(_buffer, kap_info->signal);
      *var_len = strlen(_buffer);
      break;

   case I_kapchannel:
      sprintf(_buffer, kap_info->channel);
      *var_len = sizeof(int);
      break;

   case I_kaprate:
      sprintf(_buffer, kap_info->rate);
      *var_len = strlen(_buffer);
      break;

   case I_kaptrate:
      sprintf(_buffer, kap_info->trate);
      *var_len = strlen(_buffer);
      break;

   case I_kapflags:
      sprintf(_buffer, kap_info->flags);
      *var_len = strlen(_buffer);
      break;

   case I_kapdisc:
      sprintf(_buffer, kap_info->disc);
      *var_len = strlen(_buffer);
      break;

   case I_kapindex:
      *var_len = sizeof(int);
      return (unsigned char *)&i;

   default:
      return NO_MIBINSTANCE;
   }

   return (unsigned char *)_buffer;
}


unsigned char *
var_staEntry(int *var_len,
             Oid *newoid, Oid *reqoid, int searchType,
             snmp_info_t *mesg, int(**write_method)())
{
/* Last subOID of COLUMNAR OID is column */
   int        column;
   int        result;
   int        pos;
   int        count, i;
   sta_info_t sta_list[LEN];
   sta_info_t *sta_info;

   memset(sta_list, 0, sizeof(sta_list));
   sta_info = NULL;
   count    = 0;
   column   = newoid->name[(newoid->namelen - 1)];
   pos      = newoid->namelen++;

   memset(_buffer, 0, _SPRINT_MAX_LEN);

   get_sta_list(sta_list, &count);

   for (i = 1; i <= count; i++)
   {
      sta_info          = &sta_list[i - 1];
      newoid->name[pos] = i;
      result            = compare(reqoid, newoid);
      if (((searchType == EXACT) && (result == 0)) ||
          ((searchType == NEXT) && (result < 0)))
      {
         break;
      }
   }

   if (i > count - 1)
   {
      return NULL;
   }


   /* Set size (in bytes) and return address of the variable */
   *var_len = sizeof(long);

   if (sta_info == NULL)
   {
      return NO_MIBINSTANCE;
   }

   switch (column)
   {
   case I_staaddr:
      sprintf(_buffer, sta_info->sta_addr);
      *var_len = strlen(_buffer);
      break;

   case I_staifname:
      sprintf(_buffer, sta_info->ifname);
      *var_len = strlen(_buffer);
      break;

   case I_starate:
      sprintf(_buffer, sta_info->rate);
      *var_len = strlen(_buffer);
      break;

   case I_stasignal:
      sprintf(_buffer, sta_info->signal);
      *var_len = strlen(_buffer);
      break;

   case I_stavtag:
      sprintf(_buffer, sta_info->vtag);
      *var_len = strlen(_buffer);
      break;

   case I_stalastpacketrx:
      sprintf(_buffer, sta_info->last_packet_rx);
      *var_len = strlen(_buffer);
      break;

   case I_starefcount:
      sprintf(_buffer, sta_info->ref_count);
      *var_len = strlen(_buffer);
      break;

   case I_staacl:
      sprintf(_buffer, sta_info->acl);
      *var_len = strlen(_buffer);
      break;

   case I_staindex:
      *var_len = sizeof(int);
      return (unsigned char *)&i;

   default:
      return NO_MIBINSTANCE;
   }


   return (unsigned char *)_buffer;
}


unsigned char *
var_temperature(int *var_len, snmp_info_t *mesg, int(**write_method)())

{
   FILE  *fp;
   float temperature;

   system("cat /proc/brdinfo/temp | cut -dC  -f1 > /etc/brd1.txt");
   fp = fopen("/etc/brd1.txt", "rt");

   fscanf(fp, "%f", &temperature);

   fclose(fp);

   unlink("/etc/brd1.txt");

   int_return = (int)temperature;
   *var_len   = sizeof(unsigned int);

   return (unsigned char *)&int_return;
}


unsigned char *
var_voltage(int *var_len, snmp_info_t *mesg, int(**write_method)())

{
   FILE  *fp;
   float voltage;

   system("cat /proc/brdinfo/voltage | cut -dV  -f1 > /etc/brd2.txt");
   fp = fopen("/etc/brd2.txt", "rt");

   fscanf(fp, "%f", &voltage);

   fclose(fp);

   unlink("/etc/brd2.txt");

   int_return = (int)voltage;
   *var_len   = sizeof(unsigned int);

   return (unsigned char *)&int_return;
}


static oid    kapEntry_oid[] = { O_kapEntry };
static Object kapEntry_variables[] =
{
   { SNMP_INTEGER, (RONLY | COLUMN),  var_kapEntry,
     {            1, { I_kapindex } } },
   { SNMP_STRING,  (RONLY | COLUMN),  var_kapEntry,
     {            1, { I_kapaddr } } },
   { SNMP_STRING,  (RONLY | COLUMN),  var_kapEntry,
     {            1, { I_kapsignal } } },
   { SNMP_STRING,  (RONLY | COLUMN),  var_kapEntry,
     {            1, { I_kapchannel } } },
   { SNMP_STRING,  (RONLY | COLUMN),  var_kapEntry,
     {            1, { I_kaprate } } },
   { SNMP_STRING,  (RWRITE | COLUMN), var_kapEntry,
     {            1, { I_kaptrate } } },
   { SNMP_STRING,  (RWRITE | COLUMN), var_kapEntry,
     {            1, { I_kapflags } } },
   { SNMP_STRING,  (RWRITE | COLUMN), var_kapEntry,
     {            1, { I_kapdisc } } },
};
static SubTree kapEntry_tree =
{
   NULL,                                 kapEntry_variables,
   (sizeof(kapEntry_oid) / sizeof(oid)), kapEntry_oid
};

static oid    staEntry_oid[] = { O_staEntry };
static Object staEntry_variables[] =
{
   { SNMP_INTEGER, (RONLY | COLUMN), var_staEntry,
     {            1, { I_staindex } } },
   { SNMP_STRING,  (RONLY | COLUMN), var_staEntry,
     {            1, { I_staaddr } } },
   { SNMP_STRING,  (RONLY | COLUMN), var_staEntry,
     {            1, { I_staifname } } },
   { SNMP_STRING,  (RONLY | COLUMN), var_staEntry,
     {            1, { I_starate } } },
   { SNMP_STRING,  (RONLY | COLUMN), var_staEntry,
     {            1, { I_stasignal } } },
   { SNMP_STRING,  (RONLY | COLUMN), var_staEntry,
     {            1, { I_stavtag } } },
   { SNMP_STRING,  (RONLY | COLUMN), var_staEntry,
     {            1, { I_stalastpacketrx } } },
   { SNMP_STRING,  (RONLY | COLUMN), var_staEntry,
     {            1, { I_starefcount } } },
   { SNMP_STRING,  (RONLY | COLUMN), var_staEntry,
     {            1, { I_staacl } } },
};

static SubTree staEntry_tree =
{
   NULL,                                 staEntry_variables,
   (sizeof(staEntry_oid) / sizeof(oid)), staEntry_oid
};

static oid    boardInfo_oid[] = { O_boardInfo };
static Object boardInfo_variables[] =
{
   { SNMP_INTEGER, (RONLY | SCALAR),  var_temperature,
     {            2, { I_temperature,           0 } } },
   { SNMP_INTEGER, (RWRITE | SCALAR), var_voltage,
     {            2, { I_voltage, 0 } } }
};

static SubTree boardInfo_tree =
{
   NULL,                                  boardInfo_variables,
   (sizeof(boardInfo_oid) / sizeof(oid)), boardInfo_oid
};



/* This is the MIB registration function. This should be called */
/* within the init_MESH_MIB-function */

void register_subtrees_of_RUNTIME_MIB()
{
   insert_group_in_mib(&kapEntry_tree);
   insert_group_in_mib(&staEntry_tree);
   insert_group_in_mib(&boardInfo_tree);
}


/* Runtime implementation functions */
