#ifndef _SNMPv2_TC_
#define _SNMPv2_TC_

/* required include files (IMPORTS) */
#include        "snmpv2-smi.h"


void init_SNMPv2_TC();
void register_subtrees_of_SNMPv2_TC();

/* defined objects in this module */


/* Put here additional MIB specific include definitions */

#endif  //_SNMPv2_TC_
