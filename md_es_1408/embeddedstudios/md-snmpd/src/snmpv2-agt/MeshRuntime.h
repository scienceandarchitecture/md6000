/********************************************************************************
* MeshDynamics
* --------------
* File     : MeshRuntime.h
* Comments : Mesh Runtime configuration
* Created  : 6/19/2008
* Author   : Prachiti Gaikwad
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  0  |6/19/2008 | Created.                                        |Prachiti|
* -----------------------------------------------------------------------------
* |  1  |6/19/2008 | count in get sta and kap is intialised          |Prachiti|
* -----------------------------------------------------------------------------
********************************************************************************/

#ifndef _MDMESHRUNTIME_
#define _MDMESHRUNTIME_


#define LEN    128

struct kap_parsed_info
{
   char kap_addr[LEN];
   char channel[LEN];
   char signal[LEN];
   char rate[LEN];
   char trate[LEN];
   char disc[LEN];
   char flags[LEN];
};

typedef struct kap_parsed_info   kap_info_t;

struct stalist_parsed_info
{
   char sta_addr[LEN];
   char ifname[LEN];
   char vtag[LEN];
   char signal[LEN];
   char rate[LEN];
   char last_packet_rx[LEN];
   char ref_count[LEN];
   char acl[LEN];
};

typedef struct stalist_parsed_info   sta_info_t;

int get_sta_list(sta_info_t *sta_info, int *count);

int get_known_ap_list(kap_info_t *kap_info, int *count);


#endif
