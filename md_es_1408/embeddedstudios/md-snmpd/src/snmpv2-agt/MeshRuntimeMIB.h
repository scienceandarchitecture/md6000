/********************************************************************************
* MeshDynamics
* --------------
* File     : MeshRuntimeMIB.c
* Comments : Mesh Runtime configuration MIB
* Created  : 6/19/2008
* Author   : Prachiti Gaikwad
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  1  |6/24/2008 | Added Voltage & Temp OIDs                       |Prachiti|
* -----------------------------------------------------------------------------
* |  0  |6/19/2008 | Created.                                        |Prachiti|
* -----------------------------------------------------------------------------
********************************************************************************/

#ifndef _RUNTIME_MIB_
#define _RUNTIME_MIB_

/* required include files (IMPORTS) */
#include        "snmpv2-smi.h"
#include        "snmpv2-conf.h"
#include        "snmpv2-tc.h"


void init_RUNTIME_MIB();
void register_subtrees_of_RUNTIME_MIB();

/* defined objects in this module */


/* MIB object mdmeshmib = enterprises, 1 */
#define I_mdmeshmib       1
#define O_mdmeshmib       1, 3, 6, 1, 4, 1, 1

/* MIB object meshdynamics = enterprises, 288330 */
#define I_meshdynamics    288330
#define O_meshdynamics    1, 3, 6, 1, 4, 1, 288330

/* MIB object runtimeconf = meshdynamics, 4 */
#define I_runtimeconf     4
#define O_runtimeconf     1, 3, 6, 1, 4, 1, 288330, 4

/* MIB object kapTable = runtimeconf, 1 */
#define I_kapTable        1
#define O_kapTable        1, 3, 6, 1, 4, 1, 288330, 4, 1

/* MIB object kapEntry = kapTable, 1 */
#define I_kapEntry        1
#define O_kapEntry        1, 3, 6, 1, 4, 1, 288330, 4, 1, 1

/* MIB object kapindex = kapEntry, 1 */
#define I_kapindex        1
#define O_kapindex        1, 3, 6, 1, 4, 1, 288330, 4, 1, 1, 1


/* MIB object parentaddr = kapEntry, 2 */
#define I_kapaddr       2
#define O_kapaddr       1, 3, 6, 1, 4, 1, 288330, 4, 1, 1, 2

/* MIB object signal = kapEntry, 3 */
#define I_kapsignal     3
#define O_kapsignal     1, 3, 6, 1, 4, 1, 288330, 4, 1, 1, 3

/* MIB object channel = kapEntry, 4 */
#define I_kapchannel    4
#define O_kapchannel    1, 3, 6, 1, 4, 1, 288330, 4, 1, 1, 4

/* MIB object rate = kapEntry, 5 */
#define I_kaprate       5
#define O_kaprate       1, 3, 6, 1, 4, 1, 288330, 4, 1, 1, 5

/* MIB object trate = kapEntry, 6 */
#define I_kaptrate      6
#define O_kaptrate      1, 3, 6, 1, 4, 1, 288330, 4, 1, 1, 6

/* MIB object flags = kapEntry, 7 */
#define I_kapflags      7
#define O_kapflags      1, 3, 6, 1, 4, 1, 288330, 4, 1, 1, 7

/* MIB object disc = kapEntry, 8 */
#define I_kapdisc       8
#define O_kapdisc       1, 3, 6, 1, 4, 1, 288330, 4, 1, 1, 8

/* MIB object staTable = runtimeconf, 3 */
#define I_staTable      2
#define O_staTable      1, 3, 6, 1, 4, 1, 288330, 4, 2

/* MIB object staEntry = staTable, 1 */
#define I_staEntry      1
#define O_staEntry      1, 3, 6, 1, 4, 1, 288330, 4, 2, 1

/* MIB object staindex = staEntry, 1 */
#define I_staindex      1
#define O_staindex      1, 3, 6, 1, 4, 1, 288330, 4, 2, 1, 1


/* MIB object staaddr = staEntry, 2 */
#define I_staaddr            2
#define O_staaddr            1, 3, 6, 1, 4, 1, 288330, 4, 2, 1, 2

/* MIB object ifname = staEntry, 3 */
#define I_staifname          3
#define O_staifname          1, 3, 6, 1, 4, 1, 288330, 4, 2, 1, 3

/* MIB object starate = staEntry, 4 */
#define I_starate            4
#define O_starate            1, 3, 6, 1, 4, 1, 288330, 4, 2, 1, 4

/* MIB object stasignal = staEntry, 5 */
#define I_stasignal          5
#define O_stasignal          1, 3, 6, 1, 4, 1, 288330, 4, 2, 1, 5

/* MIB object vtag = staEntry, 6 */
#define I_stavtag            6
#define O_stavtag            1, 3, 6, 1, 4, 1, 288330, 4, 2, 1, 6

/* MIB object lastpacketrx = staEntry, 7 */
#define I_stalastpacketrx    7
#define O_stalastpacketrx    1, 3, 6, 1, 4, 1, 288330, 4, 2, 1, 7

/* MIB object refcount = staEntry, 8 */
#define I_starefcount        8
#define O_starefcount        1, 3, 6, 1, 4, 1, 288330, 4, 2, 1, 8

/* MIB object acl = staEntry, 9 */
#define I_staacl             9
#define O_staacl             1, 3, 6, 1, 4, 1, 288330, 4, 2, 1, 9


#define I_boardInfo          3
#define O_boardInfo          1, 3, 6, 1, 4, 1, 288330, 4, 3

#define I_temperature        1
#define O_temperature        1, 3, 6, 1, 4, 1, 288330, 4, 3, 1

#define I_voltage            2
#define O_voltage            1, 3, 6, 1, 4, 1, 288330, 4, 3, 2


/* Put here additional MIB specific include definitions */

#endif
