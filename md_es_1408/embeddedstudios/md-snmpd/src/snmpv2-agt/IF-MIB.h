#ifndef _IF_MIB_
#define _IF_MIB_

/* required include files (IMPORTS) */
#include        "IANAifType-MIB.h"
#include        "snmpv2-mib.h"
#include        "snmpv2-conf.h"
#include        "snmpv2-tc.h"
#include        "snmpv2-smi.h"


void init_IF_MIB();
void register_subtrees_of_IF_MIB();

/* defined objects in this module */


/* MIB object ifMIB = mib_2, 31 */
#define I_ifMIB                           31
#define O_ifMIB                           1, 3, 6, 1, 2, 1, 31

/* MIB object ifMIBObjects = ifMIB, 1 */
#define I_ifMIBObjects                    1
#define O_ifMIBObjects                    1, 3, 6, 1, 2, 1, 31, 1

/* MIB object interfaces = mib_2, 2 */
#define I_interfaces                      2
#define O_interfaces                      1, 3, 6, 1, 2, 1, 2

/* MIB object ifNumber = interfaces, 1 */
#define I_ifNumber                        1
#define O_ifNumber                        1, 3, 6, 1, 2, 1, 2, 1

/* MIB object ifTableLastChange = ifMIBObjects, 5 */
#define I_ifTableLastChange               5
#define O_ifTableLastChange               1, 3, 6, 1, 2, 1, 31, 1, 5

/* MIB object ifTable = interfaces, 2 */
#define I_ifTable                         2
#define O_ifTable                         1, 3, 6, 1, 2, 1, 2, 2

/* MIB object ifEntry = ifTable, 1 */
#define I_ifEntry                         1
#define O_ifEntry                         1, 3, 6, 1, 2, 1, 2, 2, 1

/* MIB object ifIndex = ifEntry, 1 */
#define I_ifIndex                         1
#define O_ifIndex                         1, 3, 6, 1, 2, 1, 2, 2, 1, 1

/* MIB object ifDescr = ifEntry, 2 */
#define I_ifDescr                         2
#define O_ifDescr                         1, 3, 6, 1, 2, 1, 2, 2, 1, 2

/* MIB object ifType = ifEntry, 3 */
#define I_ifType                          3
#define O_ifType                          1, 3, 6, 1, 2, 1, 2, 2, 1, 3

/* MIB object ifMtu = ifEntry, 4 */
#define I_ifMtu                           4
#define O_ifMtu                           1, 3, 6, 1, 2, 1, 2, 2, 1, 4

/* MIB object ifSpeed = ifEntry, 5 */
#define I_ifSpeed                         5
#define O_ifSpeed                         1, 3, 6, 1, 2, 1, 2, 2, 1, 5

/* MIB object ifPhysAddress = ifEntry, 6 */
#define I_ifPhysAddress                   6
#define O_ifPhysAddress                   1, 3, 6, 1, 2, 1, 2, 2, 1, 6

/* MIB object ifAdminStatus = ifEntry, 7 */
#define I_ifAdminStatus                   7
#define O_ifAdminStatus                   1, 3, 6, 1, 2, 1, 2, 2, 1, 7

/* MIB object ifOperStatus = ifEntry, 8 */
#define I_ifOperStatus                    8
#define O_ifOperStatus                    1, 3, 6, 1, 2, 1, 2, 2, 1, 8

/* MIB object ifLastChange = ifEntry, 9 */
#define I_ifLastChange                    9
#define O_ifLastChange                    1, 3, 6, 1, 2, 1, 2, 2, 1, 9

/* MIB object ifInOctets = ifEntry, 10 */
#define I_ifInOctets                      10
#define O_ifInOctets                      1, 3, 6, 1, 2, 1, 2, 2, 1, 10

/* MIB object ifInUcastPkts = ifEntry, 11 */
#define I_ifInUcastPkts                   11
#define O_ifInUcastPkts                   1, 3, 6, 1, 2, 1, 2, 2, 1, 11

/* MIB object ifInNUcastPkts = ifEntry, 12 */
#define I_ifInNUcastPkts                  12
#define O_ifInNUcastPkts                  1, 3, 6, 1, 2, 1, 2, 2, 1, 12

/* MIB object ifInDiscards = ifEntry, 13 */
#define I_ifInDiscards                    13
#define O_ifInDiscards                    1, 3, 6, 1, 2, 1, 2, 2, 1, 13

/* MIB object ifInErrors = ifEntry, 14 */
#define I_ifInErrors                      14
#define O_ifInErrors                      1, 3, 6, 1, 2, 1, 2, 2, 1, 14

/* MIB object ifInUnknownProtos = ifEntry, 15 */
#define I_ifInUnknownProtos               15
#define O_ifInUnknownProtos               1, 3, 6, 1, 2, 1, 2, 2, 1, 15

/* MIB object ifOutOctets = ifEntry, 16 */
#define I_ifOutOctets                     16
#define O_ifOutOctets                     1, 3, 6, 1, 2, 1, 2, 2, 1, 16

/* MIB object ifOutUcastPkts = ifEntry, 17 */
#define I_ifOutUcastPkts                  17
#define O_ifOutUcastPkts                  1, 3, 6, 1, 2, 1, 2, 2, 1, 17

/* MIB object ifOutNUcastPkts = ifEntry, 18 */
#define I_ifOutNUcastPkts                 18
#define O_ifOutNUcastPkts                 1, 3, 6, 1, 2, 1, 2, 2, 1, 18

/* MIB object ifOutDiscards = ifEntry, 19 */
#define I_ifOutDiscards                   19
#define O_ifOutDiscards                   1, 3, 6, 1, 2, 1, 2, 2, 1, 19

/* MIB object ifOutErrors = ifEntry, 20 */
#define I_ifOutErrors                     20
#define O_ifOutErrors                     1, 3, 6, 1, 2, 1, 2, 2, 1, 20

/* MIB object ifOutQLen = ifEntry, 21 */
#define I_ifOutQLen                       21
#define O_ifOutQLen                       1, 3, 6, 1, 2, 1, 2, 2, 1, 21

/* MIB object ifSpecific = ifEntry, 22 */
#define I_ifSpecific                      22
#define O_ifSpecific                      1, 3, 6, 1, 2, 1, 2, 2, 1, 22

/* MIB object linkDown = snmpTraps, 3 */
#define I_linkDown                        3
#define O_linkDown                        1, 3, 6, 1, 6, 3, 1, 1, 5, 3

/* MIB object linkUp = snmpTraps, 4 */
#define I_linkUp                          4
#define O_linkUp                          1, 3, 6, 1, 6, 3, 1, 1, 5, 4

/* MIB object ifConformance = ifMIB, 2 */
#define I_ifConformance                   2
#define O_ifConformance                   1, 3, 6, 1, 2, 1, 31, 2

/* MIB object ifGroups = ifConformance, 1 */
#define I_ifGroups                        1
#define O_ifGroups                        1, 3, 6, 1, 2, 1, 31, 2, 1

/* MIB object ifCompliances = ifConformance, 2 */
#define I_ifCompliances                   2
#define O_ifCompliances                   1, 3, 6, 1, 2, 1, 31, 2, 2

/* MIB object ifCompliance3 = ifCompliances, 3 */
#define I_ifCompliance3                   3
#define O_ifCompliance3                   1, 3, 6, 1, 2, 1, 31, 2, 2, 3

/* MIB object ifGeneralInformationGroup = ifGroups, 10 */
#define I_ifGeneralInformationGroup       10
#define O_ifGeneralInformationGroup       1, 3, 6, 1, 2, 1, 31, 2, 1, 10

/* MIB object ifFixedLengthGroup = ifGroups, 2 */
#define I_ifFixedLengthGroup              2
#define O_ifFixedLengthGroup              1, 3, 6, 1, 2, 1, 31, 2, 1, 2

/* MIB object ifHCFixedLengthGroup = ifGroups, 3 */
#define I_ifHCFixedLengthGroup            3
#define O_ifHCFixedLengthGroup            1, 3, 6, 1, 2, 1, 31, 2, 1, 3

/* MIB object ifPacketGroup = ifGroups, 4 */
#define I_ifPacketGroup                   4
#define O_ifPacketGroup                   1, 3, 6, 1, 2, 1, 31, 2, 1, 4

/* MIB object ifHCPacketGroup = ifGroups, 5 */
#define I_ifHCPacketGroup                 5
#define O_ifHCPacketGroup                 1, 3, 6, 1, 2, 1, 31, 2, 1, 5

/* MIB object ifVHCPacketGroup = ifGroups, 6 */
#define I_ifVHCPacketGroup                6
#define O_ifVHCPacketGroup                1, 3, 6, 1, 2, 1, 31, 2, 1, 6

/* MIB object ifRcvAddressGroup = ifGroups, 7 */
#define I_ifRcvAddressGroup               7
#define O_ifRcvAddressGroup               1, 3, 6, 1, 2, 1, 31, 2, 1, 7

/* MIB object ifStackGroup2 = ifGroups, 11 */
#define I_ifStackGroup2                   11
#define O_ifStackGroup2                   1, 3, 6, 1, 2, 1, 31, 2, 1, 11

/* MIB object ifCounterDiscontinuityGroup = ifGroups, 13 */
#define I_ifCounterDiscontinuityGroup     13
#define O_ifCounterDiscontinuityGroup     1, 3, 6, 1, 2, 1, 31, 2, 1, 13

/* MIB object linkUpDownNotificationsGroup = ifGroups, 14 */
#define I_linkUpDownNotificationsGroup    14
#define O_linkUpDownNotificationsGroup    1, 3, 6, 1, 2, 1, 31, 2, 1, 14

/* MIB object ifGeneralGroup = ifGroups, 1 */
#define I_ifGeneralGroup                  1
#define O_ifGeneralGroup                  1, 3, 6, 1, 2, 1, 31, 2, 1, 1

/* MIB object ifOldObjectsGroup = ifGroups, 12 */
#define I_ifOldObjectsGroup               12
#define O_ifOldObjectsGroup               1, 3, 6, 1, 2, 1, 31, 2, 1, 12

/* MIB object ifCompliance = ifCompliances, 1 */
#define I_ifCompliance                    1
#define O_ifCompliance                    1, 3, 6, 1, 2, 1, 31, 2, 2, 1

/* MIB object ifCompliance2 = ifCompliances, 2 */
#define I_ifCompliance2                   2
#define O_ifCompliance2                   1, 3, 6, 1, 2, 1, 31, 2, 2, 2

/* Put here additional MIB specific include definitions */

struct in_ifaddr;


struct ifnet
{
   char            *if_name;      /* name, e.g. ``en'' or ``lo'' */
   char            *if_unit;      /* sub-unit for lower level driver */
   short           if_mtu;        /* maximum transmission unit */
   short           if_flags;      /* up/down, broadcast, etc. */
   int             if_metric;     /* routing metric (external only) */
   char            if_hwaddr [6]; /* ethernet address */
   int             if_type;       /* interface type: 1=generic,28=slip, ether=6, loopback=24 */
   u_long          if_speed;      /* interface speed: in bits/sec */

   struct sockaddr if_addr;       /* interface's address */
   struct sockaddr ifu_broadaddr; /* broadcast address */
   struct sockaddr ia_subnetmask; /* interface's mask */

   struct  ifqueue
   {
      int ifq_len;
      int ifq_drops;
   }
                   if_snd;        /* output queue */

   u_long          if_ibytes;     /* octets received on interface */
   u_long          if_iqdrops;    /* input queue overruns */
   u_long          if_ipackets;   /* packets received on interface */
   u_long          if_ierrors;    /* input errors on interface */
   u_long          if_obytes;     /* octets sent on interface */
   u_long          if_opackets;   /* packets sent on interface */
   u_long          if_oerrors;    /* output errors on interface */
   u_long          if_collisions; /* collisions on csma interfaces */
/* end statistics */
   struct  ifnet   *if_next;
};

#define STRUCT_IFNET_HAS_IF_IBYTES    1
#define STRUCT_IFNET_HAS_IF_OBYTES    1

struct  ifnet;
struct  in_ifaddr
{
   int dummy;
};

void free_interface_config(void);
void parse_interface_config(const char *token, char *cptr);
int Interface_Index_By_Name(char *, int);
int Interface_Scan_Init();
int Interface_Scan_Next(short *, char *, struct ifnet *, struct in_ifaddr *);
void init_interfaces(void);
int Interface_Scan_Get_Count(void);
int Interface_Get_Ether_By_Index(int Index, u_char *EtherAddr);
int Interface_Scan_By_Index(int Index, char *Name, struct ifnet *Retifnet, struct in_ifaddr *Retin_ifaddr);

#endif  /*_IF_MIB_*/
