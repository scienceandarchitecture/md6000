/********************************************************************************
* MeshDynamics
* --------------
* File     : MDMESH.h
* Comments : Mesh configuration
* Created  : 6/19/2007
* Author   : Prachiti Gaikwad
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  0  |6/19/2008 | Created.                                        |Prachiti|
* -----------------------------------------------------------------------------
********************************************************************************/

#ifndef _MDMESH_
#define _MDMESH_

/* required include files (IMPORTS) */
#include        "snmpv2-smi.h"
#include        "snmpv2-conf.h"
#include        "snmpv2-tc.h"


void init_MDMESH();
void register_subtrees_of_MDMESH();

/* defined objects in this module */



/* MIB object mdmeshmib = enterprises, 1 */
#define I_mdmeshmib             1
#define O_mdmeshmib             1, 3, 6, 1, 4, 1, 1

/* MIB object meshdynamics = enterprises, 288330 */
#define I_meshdynamics          288330
#define O_meshdynamics          1, 3, 6, 1, 4, 1, 288330

/* MIB object configuration = meshdynamics, 1 */
#define I_configuration         1
#define O_configuration         1, 3, 6, 1, 4, 1, 288330, 1

/* MIB object notificationReboot = meshdynamics, 2 */
#define I_notificationReboot    2
#define O_notificationReboot    1, 3, 6, 1, 4, 1, 288330, 2

/* MIB object notificationGroup = meshdynamics, 3 */
#define I_notificationGroup     3
#define O_notificationGroup     1, 3, 6, 1, 4, 1, 288330, 3

/* MIB object configGroup = configuration, 1 */
#define I_configGroup           1
#define O_configGroup           1, 3, 6, 1, 4, 1, 288330, 1, 1

/* MIB object nodename = configGroup, 2 */
#define I_nodename              2
#define O_nodename              1, 3, 6, 1, 4, 1, 288330, 1, 1, 2

/* MIB object description = configGroup, 3 */
#define I_description           3
#define O_description           1, 3, 6, 1, 4, 1, 288330, 1, 1, 3

/* MIB object meshid = configGroup, 4 */
#define I_meshid                4
#define O_meshid                1, 3, 6, 1, 4, 1, 288330, 1, 1, 4

/* MIB object model = configGroup, 5 */
#define I_model                 5
#define O_model                 1, 3, 6, 1, 4, 1, 288330, 1, 1, 5

/* MIB object latitude = configGroup, 6 */
#define I_latitude              6
#define O_latitude              1, 3, 6, 1, 4, 1, 288330, 1, 1, 6

/* MIB object longitude = configGroup, 7 */
#define I_longitude             7
#define O_longitude             1, 3, 6, 1, 4, 1, 288330, 1, 1, 7

/* MIB object preferredparent = configGroup, 8 */
#define I_preferredparent       8
#define O_preferredparent       1, 3, 6, 1, 4, 1, 288330, 1, 1, 8

/* MIB object heartbeatinterval = configGroup, 9 */
#define I_heartbeatinterval     9
#define O_heartbeatinterval     1, 3, 6, 1, 4, 1, 288330, 1, 1, 9

/* MIB object vlanGroup = configuration, 2 */
#define I_vlanTable             2
#define O_vlanTable             1, 3, 6, 1, 4, 1, 288330, 1, 2

/* MIB object vlanname = vlanGroup, 1 */
#define I_vlanEntry             1
#define O_vlanEntry             1, 3, 6, 1, 4, 1, 288330, 1, 2, 1

#define I_vlanindex             1
#define O_vlanindex             1, 3, 6, 1, 4, 1, 288330, 1, 2, 1, 1

#define I_vlanName              2
#define O_vlanName              1, 3, 6, 1, 4, 1, 288330, 1, 2, 1, 2

#define I_vlanTag               3
#define O_vlanTag               1, 3, 6, 1, 4, 1, 288330, 1, 2, 1, 3

#define I_vlanEssid             4
#define O_vlanEssid             1, 3, 6, 1, 4, 1, 288330, 1, 2, 1, 4


/* MIB object interfacesTable = configuration, 3 */
#define I_interfacesTable    3
#define O_interfacesTable    1, 3, 6, 1, 4, 1, 288330, 1, 3

/* MIB object interfacesEntry = interfacesTable, 1 */
#define I_interfacesEntry    1
#define O_interfacesEntry    1, 3, 6, 1, 4, 1, 288330, 1, 3, 1

/* MIB object ifname = interfacesEntry, 1 */
#define I_ifindex            1
#define O_ifindex            1, 3, 6, 1, 4, 1, 288330, 1, 3, 1, 1

/* MIB object ifname = interfacesEntry, 1 */
#define I_ifname             2
#define O_ifname             1, 3, 6, 1, 4, 1, 288330, 1, 3, 1, 2

/* MIB object mediumtype = interfacesEntry, 2 */
#define I_mediumtype         3
#define O_mediumtype         1, 3, 6, 1, 4, 1, 288330, 1, 3, 1, 3

/* MIB object mediumsubtype = interfacesEntry, 3 */
#define I_mediumsubtype      4
#define O_mediumsubtype      1, 3, 6, 1, 4, 1, 288330, 1, 3, 1, 4

/* MIB object usagetype = interfacesEntry, 4 */
#define I_usagetype          5
#define O_usagetype          1, 3, 6, 1, 4, 1, 288330, 1, 3, 1, 5

/* MIB object channel = interfacesEntry, 5 */
#define I_channel            6
#define O_channel            1, 3, 6, 1, 4, 1, 288330, 1, 3, 1, 6

/* MIB object essid = interfacesEntry, 6 */
#define I_essid              7
#define O_essid              1, 3, 6, 1, 4, 1, 288330, 1, 3, 1, 7

/* MIB object service = interfacesEntry, 7 */
#define I_service            8
#define O_service            1, 3, 6, 1, 4, 1, 288330, 1, 3, 1, 8

/* MIB object txpower = interfacesEntry, 8 */
#define I_txpower            9
#define O_txpower            1, 3, 6, 1, 4, 1, 288330, 1, 3, 1, 9

/* MIB object txrate = interfacesEntry, 9 */
#define I_txrate             10
#define O_txrate             1, 3, 6, 1, 4, 1, 288330, 1, 3, 1, 10

/* MIB object rowStatus = interfacesEntry, 10 */
#define I_rowStatus          11
#define O_rowStatus          1, 3, 6, 1, 4, 1, 288330, 1, 3, 1, 11


/* Put here additional MIB specific include definitions */

#endif  //_MDMESH_
