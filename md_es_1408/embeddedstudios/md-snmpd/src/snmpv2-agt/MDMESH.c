/********************************************************************************
* MeshDynamics
* --------------
* File     : MDMESH.c
* Comments : Mesh configuration
* Created  : 6/19/2007
* Author   : Prachiti Gaikwad
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  0  |6/19/2008 | Created.                                        |Prachiti|
* -----------------------------------------------------------------------------
********************************************************************************/

/* General includes */
#include <stdio.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <string.h>
#include <pthread.h>
#include <unistd.h>
#include <stdlib.h>
#include "MDMESH.h"

/* SNMP includes */
#include "asn1.h"
#include "snmp.h"
#include "agt_mib.h"
#include "agt_engine.h"
#include "al.h"
#include "al_context.h"
#include "al_impl_context.h"
#include "al_conf.h"
#include "dot11e_conf.h"
#include "acl_conf.h"
#include "al_codec.h"
#include "al_conf_lib.h"

#define _SPRINT_MAX_LEN    256

static int  al_conf_handle;
static int  int_return;
static char string_buffer[_SPRINT_MAX_LEN];

typedef int (WriteMethod)(int           action,
                          unsigned char *var_val,
                          unsigned char var_val_type,
                          int           var_val_len,
                          unsigned char *statP,
                          oid           *name,
                          int           name_len);



#define FS_CHANGE_THREAD_PARAM        2
#define POST_FS_PARAM_TYPE_BINARY     1
#define POST_FS_PARAM_TYPE_CONFIG     2
#define POST_FS_PARAM_TYPE_NOTHING    3

#define EXECUTE_MESHD_REBOOT          system("/sbin/meshd reboot");
#define EXECUTE_PRE_FS_CHANGE         system("/root/pre_fs_change.sh");
#define EXECUTE_POST_FS_CHANGE(TYPE)    execute_post_fs_change(TYPE);
#define EXECUTE_MESHD_START           system("/sbin/meshd start");
#define EXECUTE_MESHD_STOP            system("/sbin/meshd stop");
#define EXECUTE_MESHD_CONFIGURE       system("/sbin/meshd configure");
#define EXECUTE_UNLOAD_TORNA          system("/root/unload_torna");
#define EXECUTE_STARTUP_SH            system("/root/startap.sh");
#define EXECUTE_SYSTEM_REBOOT         system("reboot");
#define EXECUTE_MESH_UPDATE_SERVER    system("/sbin/mesh_update_server");
#define EXECUTE_LOCK_REBOOT           system("meshd lock_reboot 1");
#define EXECUTE_UNLOCK_REBOOT         system("meshd lock_reboot 0");
#define EXECUTE_COMMAND(command)    do { printf("\nExecuting command : \"%s\"", command); system(command); } while (0);

int increment_config_sqnr(int al_conf_handle)
{
   int config_sqnr;

   config_sqnr  = al_conf_get_config_sqnr(AL_CONTEXT al_conf_handle);
   config_sqnr += 1;

   /* if sqnr value overflows then reset it to 1 */
   if (config_sqnr < 0)
   {
      config_sqnr = 1;
   }

   al_conf_set_config_sqnr(AL_CONTEXT al_conf_handle, config_sqnr);

   return config_sqnr;
}


int set_mesh_config_sqnr(int sqnr)
{
   char buffer[1024];

   sprintf(buffer, "/sbin/meshd config_sqnr %d", sqnr);
   EXECUTE_COMMAND(buffer)
   return 0;
}


#include "fs_change_thread.c"

int execute_post_fs_change(int type)
{
   char command[1024];

#ifndef CONFIGD_NO_FS_CHANGE_THREAD
   if (type == POST_FS_PARAM_TYPE_CONFIG)
   {
      _fs_change_update_wait_count += FS_CHANGE_THREAD_PARAM;
      if (!_fs_change_update_needed)
      {
         _fs_change_update_needed = 1;
         EXECUTE_LOCK_REBOOT;
      }
      return 0;
   }
#endif

   switch (type)
   {
   case POST_FS_PARAM_TYPE_BINARY:
      sprintf(command, "/root/post_fs_change.sh  binary");
      break;

   case POST_FS_PARAM_TYPE_CONFIG:
      sprintf(command, "/root/post_fs_change.sh  config");
      break;

   case POST_FS_PARAM_TYPE_NOTHING:
      sprintf(command, "/root/post_fs_change.sh nothing");
      break;

   default:
      printf("\nCONFIGD	: EXECUTE_POST_FS_CHANGE invalid type");
      return -1;
   }

   EXECUTE_COMMAND(command);
   return 0;
}


/* MDMESH initialisation (must also register the MIB module tree) */
void init_MDMESH()
{
   al_conf_handle = 0;
   register_subtrees_of_MDMESH();
   fs_change_thead_create();
}


int write_nodename(int action,
                   unsigned char *var_val, unsigned char var_val_type, int var_val_len,
                   unsigned char *statP, oid *name, int name_len)
{
   int size;

   switch (action)
   {
   case RESERVE1:
      if (var_val_type != ASN_OCTET_STR)
      {
         fprintf(stderr, "write to nodeName not ASN_OCTET_STR\n");
         return SNMP_ERROR_WRONGTYPE;
      }
      if (var_val_len > sizeof(string_buffer))
      {
         fprintf(stderr, "write to nodeName: bad length\n");
         return SNMP_ERROR_WRONGLENGTH;
      }
      break;

   case RESERVE2:
      break;

   case ACTION:
      memset(string_buffer, 0, _SPRINT_MAX_LEN);
      size = var_val_len;
      memcpy(string_buffer, var_val, size);

      if (al_conf_handle == 0)
      {
         fprintf(stderr, "write to nodeName: al_conf_handle = 0\n");
         return SNMP_ERROR_RESOURCEUNAVAILABLE;
      }

      al_conf_handle = libalconf_read_config_data();
      EXECUTE_PRE_FS_CHANGE
      set_mesh_config_sqnr(increment_config_sqnr(al_conf_handle));

      al_conf_set_name(al_conf_handle, var_val, var_val_len);
      al_conf_put(al_conf_handle);
      EXECUTE_POST_FS_CHANGE(POST_FS_PARAM_TYPE_CONFIG)
      al_conf_close(AL_CONTEXT al_conf_handle);

      break;

   case FREE:
      break;

   default:
      break;
   }
   return SNMP_ERROR_NOERROR;
}


unsigned char *
var_nodename(int *var_len, snmp_info_t *mesg, WriteMethod **write_method)
{
   *write_method = write_nodename;
   memset(string_buffer, 0, _SPRINT_MAX_LEN);

   al_conf_handle = libalconf_read_config_data();
   al_conf_get_name(al_conf_handle, string_buffer, _SPRINT_MAX_LEN);
   *var_len = strlen(string_buffer);
   al_conf_close(al_conf_handle);

   return (unsigned char *)string_buffer;
}


int write_description(int action,
                      unsigned char *var_val, unsigned char var_val_type, int var_val_len,
                      unsigned char *statP, oid *name, int name_len)
{
   int size;

   switch (action)
   {
   case RESERVE1:
      if (var_val_type != ASN_OCTET_STR)
      {
         fprintf(stderr, "write to description not ASN_OCTET_STR\n");
         return SNMP_ERROR_WRONGTYPE;
      }
      if (var_val_len > sizeof(string_buffer))
      {
         fprintf(stderr, "write to description: bad length\n");
         return SNMP_ERROR_WRONGLENGTH;
      }
      break;

   case RESERVE2:
      break;

   case ACTION:
      memset(string_buffer, 0, _SPRINT_MAX_LEN);
      size = var_val_len;
      memcpy(string_buffer, var_val, size);

      if (al_conf_handle == 0)
      {
         fprintf(stderr, "write to description: al_conf_handle = 0\n");
         return SNMP_ERROR_RESOURCEUNAVAILABLE;
      }

      al_conf_handle = libalconf_read_config_data();
      EXECUTE_PRE_FS_CHANGE
      set_mesh_config_sqnr(increment_config_sqnr(al_conf_handle));

      al_conf_set_description(al_conf_handle, var_val, var_val_len);
      al_conf_put(al_conf_handle);
      EXECUTE_POST_FS_CHANGE(POST_FS_PARAM_TYPE_CONFIG)
      al_conf_close(AL_CONTEXT al_conf_handle);

      break;

   case FREE:
      break;

   default:
      fprintf(stderr, "wrong oid\n");
   }

   return SNMP_ERROR_NOERROR;
}


unsigned char *
var_description(int *var_len, snmp_info_t *mesg, WriteMethod **write_method)
{
   memset(string_buffer, 0, _SPRINT_MAX_LEN);

   *write_method  = write_description;
   al_conf_handle = libalconf_read_config_data();
   al_conf_get_description(al_conf_handle, string_buffer, _SPRINT_MAX_LEN);
   *var_len = strlen(string_buffer);
   al_conf_close(al_conf_handle);
   return (unsigned char *)string_buffer;
}


int write_meshid(int action,
                 unsigned char *var_val, unsigned char var_val_type, int var_val_len,
                 unsigned char *statP, oid *name, int name_len)
{
   int size;

   switch (action)
   {
   case RESERVE1:
      if (var_val_type != ASN_OCTET_STR)
      {
         fprintf(stderr, "write to meshid not ASN_OCTET_STR\n");
         return SNMP_ERROR_WRONGTYPE;
      }
      if (var_val_len > sizeof(string_buffer))
      {
         fprintf(stderr, "write to meshid: bad length\n");
         return SNMP_ERROR_WRONGLENGTH;
      }
      break;

   case RESERVE2:
      break;

   case ACTION:
      memset(string_buffer, 0, _SPRINT_MAX_LEN);
      size = var_val_len;
      memcpy(string_buffer, var_val, size);

      if (al_conf_handle == 0)
      {
         fprintf(stderr, "write to meshid: al_conf_handle = 0\n");
         return SNMP_ERROR_RESOURCEUNAVAILABLE;
      }

      al_conf_handle = libalconf_read_config_data();
      EXECUTE_PRE_FS_CHANGE
      set_mesh_config_sqnr(increment_config_sqnr(al_conf_handle));

      al_conf_set_mesh_id(al_conf_handle, var_val, var_val_len);
      al_conf_put(al_conf_handle);
      EXECUTE_POST_FS_CHANGE(POST_FS_PARAM_TYPE_CONFIG)
      al_conf_close(AL_CONTEXT al_conf_handle);
      break;

   case FREE:
      break;

   default:
      fprintf(stderr, "wrong oid\n");
   }

   return SNMP_ERROR_NOERROR;
}


unsigned char *
var_meshid(int *var_len, snmp_info_t *mesg, WriteMethod **write_method)

{
   memset(string_buffer, 0, _SPRINT_MAX_LEN);

   *write_method  = write_meshid;
   al_conf_handle = libalconf_read_config_data();
   al_conf_get_mesh_id(al_conf_handle, string_buffer, _SPRINT_MAX_LEN);
   *var_len = strlen(string_buffer);
   al_conf_close(al_conf_handle);
   return (unsigned char *)string_buffer;
}


int write_model(int action,
                unsigned char *var_val, unsigned char var_val_type, int var_val_len,
                unsigned char *statP, oid *name, int name_len)
{
   int size;

   switch (action)
   {
   case RESERVE1:
      if (var_val_type != ASN_OCTET_STR)
      {
         fprintf(stderr, "write to model not ASN_OCTET_STR\n");
         return SNMP_ERROR_WRONGTYPE;
      }
      if (var_val_len > sizeof(string_buffer))
      {
         fprintf(stderr, "write to model: bad length\n");
         return SNMP_ERROR_WRONGLENGTH;
      }
      break;

   case RESERVE2:
      break;

   case ACTION:
      memset(string_buffer, 0, _SPRINT_MAX_LEN);
      size = var_val_len;
      memcpy(string_buffer, var_val, size);

      if (al_conf_handle == 0)
      {
         fprintf(stderr, "write to model: al_conf_handle = 0\n");
         return SNMP_ERROR_RESOURCEUNAVAILABLE;
      }

      al_conf_handle = libalconf_read_config_data();
      EXECUTE_PRE_FS_CHANGE
      set_mesh_config_sqnr(increment_config_sqnr(al_conf_handle));

      al_conf_set_model(al_conf_handle, var_val, var_val_len);
      al_conf_put(al_conf_handle);
      EXECUTE_POST_FS_CHANGE(POST_FS_PARAM_TYPE_CONFIG)
      al_conf_close(AL_CONTEXT al_conf_handle);
      break;

   case FREE:
      break;

   default:
      fprintf(stderr, "wrong oid\n");
   }

   return SNMP_ERROR_NOERROR;
}


unsigned char *
var_model(int *var_len, snmp_info_t *mesg, WriteMethod **write_method)
{
   memset(string_buffer, 0, _SPRINT_MAX_LEN);

   *write_method  = write_model;
   al_conf_handle = libalconf_read_config_data();
   al_conf_get_model(al_conf_handle, string_buffer, _SPRINT_MAX_LEN);
   *var_len = strlen(string_buffer);
   al_conf_close(al_conf_handle);
   return (unsigned char *)string_buffer;
}


int write_latitude(int action,
                   unsigned char *var_val, unsigned char var_val_type, int var_val_len,
                   unsigned char *statP, oid *name, int name_len)
{
   int size;

   switch (action)
   {
   case RESERVE1:
      if (var_val_type != ASN_OCTET_STR)
      {
         fprintf(stderr, "write to latitude not ASN_OCTET_STR\n");
         return SNMP_ERROR_WRONGTYPE;
      }
      if (var_val_len > sizeof(string_buffer))
      {
         fprintf(stderr, "write to latitude: bad length\n");
         return SNMP_ERROR_WRONGLENGTH;
      }
      break;

   case RESERVE2:
      break;

   case ACTION:
      memset(string_buffer, 0, _SPRINT_MAX_LEN);
      size = var_val_len;
      memcpy(string_buffer, var_val, size);

      if (al_conf_handle == 0)
      {
         fprintf(stderr, "write to latitude: al_conf_handle = 0\n");
         return SNMP_ERROR_RESOURCEUNAVAILABLE;
      }

      al_conf_handle = libalconf_read_config_data();
      EXECUTE_PRE_FS_CHANGE
      set_mesh_config_sqnr(increment_config_sqnr(al_conf_handle));

      al_conf_set_gps_x_coordinate(al_conf_handle, var_val, var_val_len);
      al_conf_put(al_conf_handle);
      EXECUTE_POST_FS_CHANGE(POST_FS_PARAM_TYPE_CONFIG)
      al_conf_close(AL_CONTEXT al_conf_handle);
      break;

   case FREE:
      break;

   default:
      fprintf(stderr, "wrong oid\n");
   }

   return SNMP_ERROR_NOERROR;
}


unsigned char *
var_latitude(int *var_len, snmp_info_t *mesg, WriteMethod **write_method)
{
   int len;

   *write_method = write_latitude;
   len           = 0;

   memset(string_buffer, 0, _SPRINT_MAX_LEN);

   al_conf_handle = libalconf_read_config_data();
   al_conf_get_gps_x_coordinate(al_conf_handle, string_buffer, _SPRINT_MAX_LEN);

   len = strlen(string_buffer);
   string_buffer[len] = '\0';

   *var_len = strlen(string_buffer) + 1;

   al_conf_close(al_conf_handle);
   return (unsigned char *)string_buffer;
}


int write_longitude(int action,
                    unsigned char *var_val, unsigned char var_val_type, int var_val_len,
                    unsigned char *statP, oid *name, int name_len)
{
   int size;

   switch (action)
   {
   case RESERVE1:
      if (var_val_type != ASN_OCTET_STR)
      {
         fprintf(stderr, "write to longitude not ASN_OCTET_STR\n");
         return SNMP_ERROR_WRONGTYPE;
      }
      if (var_val_len > sizeof(string_buffer))
      {
         fprintf(stderr, "write to longitude: bad length\n");
         return SNMP_ERROR_WRONGLENGTH;
      }
      break;

   case RESERVE2:
      break;

   case ACTION:
      memset(string_buffer, 0, _SPRINT_MAX_LEN);
      size = var_val_len;
      memcpy(string_buffer, var_val, size);

      if (al_conf_handle == 0)
      {
         fprintf(stderr, "write to longitude: al_conf_handle = 0\n");
         return SNMP_ERROR_RESOURCEUNAVAILABLE;
      }

      al_conf_handle = libalconf_read_config_data();
      EXECUTE_PRE_FS_CHANGE
      set_mesh_config_sqnr(increment_config_sqnr(al_conf_handle));

      al_conf_set_gps_y_coordinate(al_conf_handle, var_val, var_val_len);
      al_conf_put(al_conf_handle);
      EXECUTE_POST_FS_CHANGE(POST_FS_PARAM_TYPE_CONFIG)
      al_conf_close(AL_CONTEXT al_conf_handle);
      break;

   case FREE:
      break;

   default:
      fprintf(stderr, "wrong oid\n");
   }

   return SNMP_ERROR_NOERROR;
}


unsigned char *
var_longitude(int *var_len, snmp_info_t *mesg, WriteMethod **write_method)

{
   int len;

   *write_method = write_longitude;
   len           = 0;
   memset(string_buffer, 0, _SPRINT_MAX_LEN);

   al_conf_handle = libalconf_read_config_data();
   al_conf_get_gps_y_coordinate(al_conf_handle, string_buffer, _SPRINT_MAX_LEN);

   len = strlen(string_buffer);
   string_buffer[len] = '\0';

   *var_len = strlen(string_buffer) + 1;

   al_conf_close(al_conf_handle);
   return (unsigned char *)string_buffer;
}


int write_preferredparent(int action,
                          unsigned char *var_val, unsigned char var_val_type, int var_val_len,
                          unsigned char *statP, oid *name, int name_len)
{
   int           size;
   al_net_addr_t pref_parent;

   switch (action)
   {
   case RESERVE1:
      if (var_val_type != ASN_OCTET_STR)
      {
         fprintf(stderr, "write to preferredparent not ASN_OCTET_STR\n");
         return SNMP_ERROR_WRONGTYPE;
      }
      if (var_val_len > sizeof(string_buffer))
      {
         fprintf(stderr, "write to preferredparent: bad length\n");
         return SNMP_ERROR_WRONGLENGTH;
      }
      break;

   case RESERVE2:
      break;

   case ACTION:
      memset(string_buffer, 0, _SPRINT_MAX_LEN);
      size = var_val_len;
      memcpy(string_buffer, var_val, size);

      if (al_conf_handle == 0)
      {
         fprintf(stderr, "write to preferredparent: al_conf_handle = 0\n");
         return SNMP_ERROR_RESOURCEUNAVAILABLE;
      }

      memset(&pref_parent, 0, sizeof(al_net_addr_t));
      sscanf((const char *)var_val, "%x:%x:%x:%x:%x:%x"
             , &pref_parent.bytes[0],
             &pref_parent.bytes[1],
             &pref_parent.bytes[2],
             &pref_parent.bytes[3],
             &pref_parent.bytes[4],
             &pref_parent.bytes[5]);

      pref_parent.length = 6;

      al_conf_handle = libalconf_read_config_data();
      EXECUTE_PRE_FS_CHANGE
      set_mesh_config_sqnr(increment_config_sqnr(al_conf_handle));

      al_conf_set_preferred_parent(al_conf_handle, &pref_parent);
      al_conf_put(al_conf_handle);
      EXECUTE_POST_FS_CHANGE(POST_FS_PARAM_TYPE_CONFIG)
      al_conf_close(AL_CONTEXT al_conf_handle);
      break;

   case FREE:
      break;

   default:
      fprintf(stderr, "wrong oid\n");
   }

   return SNMP_ERROR_NOERROR;
}


unsigned char *
var_preferredparent(int *var_len, snmp_info_t *mesg, WriteMethod **write_method)
{
   al_net_addr_t pref_parent;

   *write_method = write_preferredparent;
   memset(&pref_parent, 0, sizeof(al_net_addr_t));
   memset(&string_buffer, 0, _SPRINT_MAX_LEN);

   al_conf_handle = libalconf_read_config_data();
   al_conf_get_preferred_parent(al_conf_handle, &pref_parent);
   sprintf(string_buffer, "%2x:%2x:%2x:%2x:%2x:%2x",
           pref_parent.bytes[0], pref_parent.bytes[1], pref_parent.bytes[2],
           pref_parent.bytes[3], pref_parent.bytes[4], pref_parent.bytes[5]);

   *var_len = strlen(string_buffer);
   al_conf_close(al_conf_handle);
   return (unsigned char *)string_buffer;
}


int write_heartbeatinterval(int action,
                            unsigned char *var_val, unsigned char var_val_type, int var_val_len,
                            unsigned char *statP, oid *name, int name_len)
{
   unsigned int intval;

   intval = 0;

   printf("heartbeatinterval Set called action %d\n", action);
   switch (action)
   {
   case RESERVE1:
      if (var_val_type != SNMP_INTEGER)
      {
         fprintf(stderr, "write to heartbeatinterval not ASN_INTEGER\n");
         return SNMP_ERROR_WRONGTYPE;
      }
      break;

   case RESERVE2:
      break;

   case ACTION:
      intval = 0;

      memcpy(&intval, var_val, var_val_len);
      intval >>= (sizeof(int) - var_val_len) * 8;

      if (al_conf_handle == 0)
      {
         fprintf(stderr, "write to nodeName: al_conf_handle = 0\n");
         return SNMP_ERROR_RESOURCEUNAVAILABLE;
      }


      al_conf_handle = libalconf_read_config_data();
      EXECUTE_PRE_FS_CHANGE
      set_mesh_config_sqnr(increment_config_sqnr(al_conf_handle));

      al_conf_set_heartbeat_interval(al_conf_handle, intval);
      al_conf_put(al_conf_handle);
      EXECUTE_POST_FS_CHANGE(POST_FS_PARAM_TYPE_CONFIG)
      al_conf_close(AL_CONTEXT al_conf_handle);

      break;

   case FREE:
      break;

   default:
      break;
   }


   return SNMP_ERROR_NOERROR;
}


unsigned char *
var_heartbeatinterval(int *var_len, snmp_info_t *mesg, WriteMethod **write_method)

{
   *write_method  = write_heartbeatinterval;
   int_return     = 0;
   al_conf_handle = libalconf_read_config_data();
   int_return     = al_conf_get_heartbeat_interval(al_conf_handle);
   *var_len       = sizeof(unsigned int);
   al_conf_close(al_conf_handle);
   return (unsigned char *)&int_return;
}


/* description
 */

/*
 * int	write_vlanname(int action,
 *      unsigned char *var_val, unsigned char varval_type, int var_val_len,
 *      unsigned char *statP, oid *name, int name_len)
 * {
 *  switch (action) {
 *  case RESERVE1:
 *  case RESERVE2:
 *  case COMMIT:
 *  case FREE:
 *  }
 * }
 */
unsigned char *
var_vlanname(int *var_len, snmp_info_t *mesg, WriteMethod **write_method)
{
   memset(string_buffer, 0, _SPRINT_MAX_LEN);

   strcpy(string_buffer, "vlanName");

   /* Set write-function (uncomment if you want to implement it)  */
   /* *write_method = &write_vlanname(); */
   /* Set size (in bytes) and return address of the variable */
   *var_len = strlen(string_buffer);
   return (unsigned char *)string_buffer;
}


/* Row Description
 */
unsigned char *
var_interfacesEntry(int *var_len,
                    Oid *newoid, Oid *reqoid, int searchType, snmp_info_t *mesg, int(**write_method)())
{
/* Last subOID of COLUMNAR OID is column */
   int               column;
   int               result;
   int               pos;
   int               al_conf_handle, count, i;
   al_conf_if_info_t if_info;


   column = newoid->name[(newoid->namelen - 1)];
   pos    = newoid->namelen++;

   memset(string_buffer, 0, _SPRINT_MAX_LEN);

   al_conf_handle = libalconf_read_config_data();
   count          = al_conf_get_if_count(al_conf_handle);

   for (i = 1; i <= count; i++)
   {
      al_conf_get_if(al_conf_handle, i - 1, &if_info);
      newoid->name[pos] = i;
      result            = compare(reqoid, newoid);
      if (((searchType == EXACT) && (result == 0)) ||
          ((searchType == NEXT) && (result < 0)))
      {
         break;
      }
   }

   if (i > count)
   {
      al_conf_close(al_conf_handle);
      return NULL;
   }


   /* Set size (in bytes) and return address of the variable */
   *var_len = sizeof(long);
   switch (column)
   {
   case I_ifname:
      sprintf(string_buffer, "%s", if_info.name);
      *var_len = strlen(string_buffer);
      break;

   case I_mediumtype:
      switch (if_info.phy_type)
      {
      case AL_CONF_IF_PHY_TYPE_ETHERNET:
         sprintf(string_buffer, "e");
         break;

      case AL_CONF_IF_PHY_TYPE_802_11:
         sprintf(string_buffer, "w");
         break;
      case AL_CONF_IF_PHY_TYPE_802_11_VIRTUAL:
         sprintf(string_buffer, "v");
         break;
      }
      *var_len = strlen(string_buffer);
      break;

   case I_mediumsubtype:
      switch (if_info.phy_sub_type)
      {
      case AL_CONF_IF_PHY_SUB_TYPE_802_11_A:
         sprintf(string_buffer, "a");
         break;

      case AL_CONF_IF_PHY_SUB_TYPE_802_11_B:
         sprintf(string_buffer, "b");
         break;

      case AL_CONF_IF_PHY_SUB_TYPE_802_11_G:
         sprintf(string_buffer, "g");
         break;

      case AL_CONF_IF_PHY_SUB_TYPE_802_11_BG:
         sprintf(string_buffer, "bg");
         break;

      case AL_CONF_IF_PHY_SUB_TYPE_802_11_PSQ:
         sprintf(string_buffer, "psq");
         break;

      case AL_CONF_IF_PHY_SUB_TYPE_802_11_PSH:
         sprintf(string_buffer, "psh");
         break;

      case AL_CONF_IF_PHY_SUB_TYPE_802_11_PSF:
         sprintf(string_buffer, "psf");
         break;

      default:
         sprintf(string_buffer, "x");
      }

      *var_len = strlen(string_buffer);
      break;

   case I_usagetype:
      switch (if_info.use_type)
      {
      case AL_CONF_IF_USE_TYPE_DS:
         sprintf(string_buffer, "ds");
         break;

      case AL_CONF_IF_USE_TYPE_WM:
         sprintf(string_buffer, "wm");
         break;

      case AL_CONF_IF_USE_TYPE_AP:
         sprintf(string_buffer, "ap");
         break;

      default:
         sprintf(string_buffer, "<<unknown %d>>\n", if_info.use_type);
      }

      *var_len = strlen(string_buffer);
      break;

   case I_channel:
      sprintf(string_buffer, "%d", if_info.wm_channel);
      *var_len = strlen(string_buffer);
      break;

   case I_essid:
      sprintf(string_buffer, "%s", if_info.essid);
      *var_len = strlen(string_buffer);
      break;

   case I_service:
      switch (if_info.service)
      {
      case AL_CONF_IF_SERVICE_BACKHAUL_ONLY:
         sprintf(string_buffer, "service backhaul only");
         break;

      case AL_CONF_IF_SERVICE_CLIENT_ONLY:
         sprintf(string_buffer, "service client only");
         break;

      case AL_CONF_IF_SERVICE_ALL:
         sprintf(string_buffer, "service all");
         break;

      default:
         sprintf(string_buffer, "<<unknown>> %d", if_info.service);
      }

      *var_len = strlen(string_buffer);

      break;

   case I_txpower:
      switch (if_info.phy_type)
      {
      case AL_CONF_IF_PHY_TYPE_ETHERNET:
         sprintf(string_buffer, "<<eth>> %d %%", if_info.txpower);
         break;

      case AL_CONF_IF_PHY_TYPE_802_11:
         sprintf(string_buffer, "<<wm>> %d %%", if_info.txpower);
         break;
      }

      *var_len = strlen(string_buffer);
      break;

   case I_txrate:
      if (if_info.txrate == 0)
      {
         sprintf(string_buffer, "%s", "Auto");
      }
      else
      {
         sprintf(string_buffer, "%d", if_info.txrate);
      }

      *var_len = strlen(string_buffer);
      break;

   case I_rowStatus:
      return (unsigned char *)NO_MIBINSTANCE;

   case I_ifindex:
      *var_len = sizeof(int);
      return (unsigned char *)&i;

   default:
      return NO_MIBINSTANCE;
   }

   al_conf_close(al_conf_handle);
   return (unsigned char *)string_buffer;
}


unsigned char *
var_vlanEntry(int *var_len,
              Oid *newoid, Oid *reqoid, int searchType, snmp_info_t *mesg, WriteMethod **write_method)
{
/* Last subOID of COLUMNAR OID is column */
   int                 column;
   int                 result;
   int                 pos;
   int                 al_conf_handle, count, i;
   al_conf_vlan_info_t vlan_info;


   column = newoid->name[(newoid->namelen - 1)];
   pos    = newoid->namelen++;

   memset(string_buffer, 0, _SPRINT_MAX_LEN);

   al_conf_handle = libalconf_read_config_data();
   count          = al_conf_get_vlan_count(al_conf_handle);

   for (i = 1; i <= count; i++)
   {
      al_conf_get_vlan(al_conf_handle, i - 1, &vlan_info);
      newoid->name[pos] = i;
      result            = compare(reqoid, newoid);
      if (((searchType == EXACT) && (result == 0)) ||
          ((searchType == NEXT) && (result < 0)))
      {
         break;
      }
   }

   if (i > count)
   {
      al_conf_close(al_conf_handle);
      return NULL;
   }


   /* Set size (in bytes) and return address of the variable */
   *var_len = sizeof(long);
   switch (column)
   {
   case I_vlanName:
      sprintf(string_buffer, "%s", vlan_info.name);
      *var_len = strlen(string_buffer);
      break;

   case I_vlanTag:
      if (strcmp(vlan_info.name, "default") == 0)
      {
         sprintf(string_buffer, "%s", "untagged");
      }
      else
      {
         sprintf(string_buffer, "%d", vlan_info.tag);
      }

      *var_len = strlen(string_buffer);
      break;

   case I_vlanEssid:
      if (strcmp(vlan_info.name, "default") == 0)
      {
         sprintf(string_buffer, "%s", "undefined");
      }
      else
      {
         sprintf(string_buffer, "%s", vlan_info.essid);
      }

      *var_len = strlen(string_buffer);
      break;

   default:
      return NO_MIBINSTANCE;
   }

   al_conf_close(al_conf_handle);
   return (unsigned char *)string_buffer;
}


static oid    configGroup_oid[] = { O_configGroup };
static Object configGroup_variables[] =
{
   { SNMP_STRING,  (RWRITE | SCALAR),   var_nodename,
     {            2, { I_nodename,                      0 } } },
   { SNMP_STRING,  (RWRITE | SCALAR),   var_description,
     {            2, { I_description,                   0 } } },
   { SNMP_STRING,  (RWRITE | SCALAR),   var_meshid,
     {            2, { I_meshid,                        0 } } },
   { SNMP_STRING,  (RWRITE | SCALAR),   var_model,
     {            2, { I_model,                         0 } } },
   { SNMP_STRING,  (RWRITE | SCALAR),   var_latitude,
     {            2, { I_latitude,                      0 } } },
   { SNMP_STRING,  (RWRITE | SCALAR),   var_longitude,
     {            2, { I_longitude,                     0 } } },
   { SNMP_STRING,  (RWRITE | SCALAR),   var_preferredparent,
     {            2, { I_preferredparent,               0 } } },
   { SNMP_INTEGER, (RWRITE | SCALAR),   var_heartbeatinterval,
     {            2, { I_heartbeatinterval, 0 } } }
};
static SubTree configGroup_tree =
{
   NULL,                                    configGroup_variables,
   (sizeof(configGroup_oid) / sizeof(oid)), configGroup_oid
};


static oid    vlanEntry_oid[] = { O_vlanEntry };
static Object vlanEntry_variables[] =
{
   { SNMP_INTEGER, (RONLY | COLUMN), var_vlanEntry,
     {            1, { I_vlanindex } } },
   { SNMP_STRING,  (RONLY | COLUMN), var_vlanEntry,
     {            1, { I_vlanName } } },
   { SNMP_STRING,  (RONLY | COLUMN), var_vlanEntry,
     {            1, { I_vlanTag } } },
   { SNMP_STRING,  (RONLY | COLUMN), var_vlanEntry,
     {            1, { I_vlanEssid } } },
};

static SubTree vlanEntry_tree =
{
   NULL,                                  vlanEntry_variables,
   (sizeof(vlanEntry_oid) / sizeof(oid)), vlanEntry_oid
};


static oid    interfacesEntry_oid[] = { O_interfacesEntry };
static Object interfacesEntry_variables[] =
{
   { SNMP_INTEGER, (RONLY | COLUMN),    var_interfacesEntry,
     {            1, { I_ifindex } } },
   { SNMP_STRING,  (RONLY | COLUMN),    var_interfacesEntry,
     {            1, { I_ifname } } },
   { SNMP_STRING,  (RONLY | COLUMN),    var_interfacesEntry,
     {            1, { I_mediumtype } } },
   { SNMP_STRING,  (RONLY | COLUMN),    var_interfacesEntry,
     {            1, { I_mediumsubtype } } },
   { SNMP_STRING,  (RONLY | COLUMN),    var_interfacesEntry,
     {            1, { I_usagetype } } },
   { SNMP_STRING,  (RWRITE | COLUMN),   var_interfacesEntry,
     {            1, { I_channel } } },
   { SNMP_STRING,  (RWRITE | COLUMN),   var_interfacesEntry,
     {            1, { I_essid } } },
   { SNMP_STRING,  (RWRITE | COLUMN),   var_interfacesEntry,
     {            1, { I_service } } },
   { SNMP_STRING,  (RWRITE | COLUMN),   var_interfacesEntry,
     {            1, { I_txpower } } },
   { SNMP_STRING,  (RWRITE | COLUMN),   var_interfacesEntry,
     {            1, { I_txrate } } },
   { SNMP_STRING,  (NOACCESS | COLUMN), var_interfacesEntry,
     {            1, { I_rowStatus } } }
};

static SubTree interfacesEntry_tree =
{
   NULL,                                        interfacesEntry_variables,
   (sizeof(interfacesEntry_oid) / sizeof(oid)), interfacesEntry_oid
};


/* This is the MIB registration function. This should be called */
/* within the init_MDMESH-function */

void register_subtrees_of_MDMESH()
{
   insert_group_in_mib(&configGroup_tree);
   insert_group_in_mib(&vlanEntry_tree);
   insert_group_in_mib(&interfacesEntry_tree);
}
