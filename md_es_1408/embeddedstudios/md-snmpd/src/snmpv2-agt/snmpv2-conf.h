#ifndef _SNMPv2_CONF_
#define _SNMPv2_CONF_

/* required include files (IMPORTS) */


void init_SNMPv2_CONF();
void register_subtrees_of_SNMPv2_CONF();

/* defined objects in this module */


/* Put here additional MIB specific include definitions */

#endif  //_SNMPv2_CONF_
