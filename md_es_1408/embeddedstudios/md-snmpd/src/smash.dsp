# Microsoft Developer Studio Project File - Name="smash" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Console Application" 0x0103

CFG=smash - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "smash.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "smash.mak" CFG="smash - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "smash - Win32 Release" (based on "Win32 (x86) Console Application")
!MESSAGE "smash - Win32 Debug" (based on "Win32 (x86) Console Application")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
RSC=rc.exe

!IF  "$(CFG)" == "smash - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_CONSOLE" /D "_MBCS" /YX /FD /c
# ADD CPP /nologo /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_CONSOLE" /D "_MBCS" /YX /FD /c
# ADD BASE RSC /l 0x409 /d "NDEBUG"
# ADD RSC /l 0x409 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib  kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:console /machine:I386
# ADD LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib  kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:console /machine:I386

!ELSEIF  "$(CFG)" == "smash - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_CONSOLE" /D "_MBCS" /YX /FD /GZ  /c
# ADD CPP /nologo /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_CONSOLE" /D "_MBCS" /FR /YX /FD /GZ  /c
# ADD BASE RSC /l 0x409 /d "_DEBUG"
# ADD RSC /l 0x409 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib  kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:console /debug /machine:I386 /pdbtype:sept
# ADD LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib  kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:console /debug /machine:I386 /pdbtype:sept

!ENDIF 

# Begin Target

# Name "smash - Win32 Release"
# Name "smash - Win32 Debug"
# Begin Group "src_snmp"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\src_snmp\agt_engine.c
# End Source File
# Begin Source File

SOURCE=.\src_snmp\agt_engine.h
# End Source File
# Begin Source File

SOURCE=.\src_snmp\agt_mib.c
# End Source File
# Begin Source File

SOURCE=.\src_snmp\agt_mib.h
# End Source File
# Begin Source File

SOURCE=.\src_snmp\asn1.c
# End Source File
# Begin Source File

SOURCE=.\src_snmp\asn1.h
# End Source File
# Begin Source File

SOURCE=.\src_snmp\snmp.h
# End Source File
# Begin Source File

SOURCE=.\src_snmp\snmp_community.c
# End Source File
# Begin Source File

SOURCE=.\src_snmp\snmp_community.h
# End Source File
# Begin Source File

SOURCE=.\src_snmp\snmp_config.h
# End Source File
# Begin Source File

SOURCE=.\src_snmp\snmp_gettimeofday.c
# End Source File
# Begin Source File

SOURCE=.\src_snmp\snmp_gettimeofday.h
# End Source File
# Begin Source File

SOURCE=.\src_snmp\snmp_printf.h
# End Source File
# Begin Source File

SOURCE=.\src_snmp\snmp_strcasecmp.c
# End Source File
# Begin Source File

SOURCE=.\src_snmp\snmp_string.h
# End Source File
# End Group
# Begin Group "src_compiler"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\src_compiler\agent_code.c
# End Source File
# Begin Source File

SOURCE=.\src_compiler\agent_code.h
# End Source File
# Begin Source File

SOURCE=.\src_compiler\agent_include.c
# End Source File
# Begin Source File

SOURCE=.\src_compiler\agent_include.h
# End Source File
# Begin Source File

SOURCE=.\src_compiler\agent_mibtree.c
# End Source File
# Begin Source File

SOURCE=.\src_compiler\agent_mibtree.h
# End Source File
# Begin Source File

SOURCE=.\src_compiler\agent_stubs.c
# End Source File
# Begin Source File

SOURCE=.\src_compiler\agent_stubs.h
# End Source File
# Begin Source File

SOURCE=.\src_compiler\code.c
# End Source File
# Begin Source File

SOURCE=.\src_compiler\code.h
# End Source File
# Begin Source File

SOURCE=.\src_compiler\k.c
# End Source File
# Begin Source File

SOURCE=.\src_compiler\k.h
# End Source File
# Begin Source File

SOURCE=.\src_compiler\main.c
# End Source File
# Begin Source File

SOURCE=.\src_compiler\main.h
# End Source File
# Begin Source File

SOURCE=.\src_compiler\moduletable.c
# End Source File
# Begin Source File

SOURCE=.\src_compiler\moduletable.h
# End Source File
# Begin Source File

SOURCE=.\src_compiler\parser.c
# End Source File
# Begin Source File

SOURCE=.\src_compiler\parser.h
# End Source File
# Begin Source File

SOURCE=.\src_compiler\scanner.c
# End Source File
# Begin Source File

SOURCE=.\src_compiler\semantics.c
# End Source File
# Begin Source File

SOURCE=.\src_compiler\semantics.h
# End Source File
# Begin Source File

SOURCE=.\src_compiler\symboltable.c
# End Source File
# Begin Source File

SOURCE=.\src_compiler\symboltable.h
# End Source File
# Begin Source File

SOURCE=.\src_compiler\treetypes.c
# End Source File
# Begin Source File

SOURCE=.\src_compiler\treetypes.h
# End Source File
# Begin Source File

SOURCE=.\src_compiler\unpk.c
# End Source File
# Begin Source File

SOURCE=.\src_compiler\unpk.h
# End Source File
# End Group
# Begin Group "snmpv2-agt"

# PROP Default_Filter ""
# Begin Source File

SOURCE=".\snmpv2-agt\snmpd.c"
# End Source File
# Begin Source File

SOURCE=".\snmpv2-agt\snmpv2-conf.h"
# End Source File
# Begin Source File

SOURCE=".\snmpv2-agt\snmpv2-mib.c"
# End Source File
# Begin Source File

SOURCE=".\snmpv2-agt\snmpv2-mib.h"
# End Source File
# Begin Source File

SOURCE=".\snmpv2-agt\snmpv2-smi.h"
# End Source File
# Begin Source File

SOURCE=".\snmpv2-agt\snmpv2-tc.h"
# End Source File
# End Group
# Begin Source File

SOURCE=.\acconfig.h
# End Source File
# End Target
# End Project
