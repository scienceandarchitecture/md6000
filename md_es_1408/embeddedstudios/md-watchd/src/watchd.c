/********************************************************************************
* MeshDynamics
* --------------
* File     : watchd.c
* Comments : Meshdynamics daemon watchdog
* Created  : 5/17/2005
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  2  |6/19/2008 | Added check for meshsnmpd                       | Sriram |
* -----------------------------------------------------------------------------
* |  2  |5/1/2007  | Watchd Check time increased to 5 mins           | Sriram |
* -----------------------------------------------------------------------------
* |  1  |5/1/2007  | Added check for Iperf daemon                    | Sriram |
* -----------------------------------------------------------------------------
* |  0  |5/17/2005 | Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/ioctl.h>
#include <sys/wait.h>

#include "torna_ddi.h"

static const unsigned char _IMCP_SIGNATURE [] = { 'I', 'M', 'C', 'P', 5, 0 };
static const unsigned char _IMCP_DATA              [] = { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H' };

#define IMCP_CONFIGD_WATCHDOG_REQUEST    27
#define IMCP_DEFAULT_FLAG_VALUE         0

static void _check_configd()
{
   int                ret;
   int                sock_fd;
   struct sockaddr_in addr;
   unsigned char      buffer[64];
   unsigned char      *p;
   int                len;
   int                from_len;
   fd_set             select_set;
   struct timeval     timeout;
   unsigned short     temp;

   /**
    * First see if configd is alive
    */

   ret = system("/root/watchd_helper.sh configd");

   if (WEXITSTATUS(ret) == 1)
   {
      printf("watchd: configd was not found!, restarting....\n");
      system("/sbin/configd&");
      return;
   }

   /**
    * Now send the watchdog request packet to configd
    * and get a response
    */

   sock_fd = socket(AF_INET, SOCK_DGRAM, 0);

   FD_ZERO(&select_set);
   FD_SET(sock_fd, &select_set);

   timeout.tv_sec  = 30;
   timeout.tv_usec = 0;

   addr.sin_family      = AF_INET;
   addr.sin_port        = htons(0xDEDE);
   addr.sin_addr.s_addr = inet_addr("127.0.0.1");

   p = buffer;

   memcpy(p, _IMCP_SIGNATURE, sizeof(_IMCP_SIGNATURE));
   p   += sizeof(_IMCP_SIGNATURE);
		
   temp = IMCP_DEFAULT_FLAG_VALUE;
   memcpy(p, &temp, sizeof(unsigned short));
   p += sizeof(unsigned short);

   temp = IMCP_CONFIGD_WATCHDOG_REQUEST;
   memcpy(p, &temp, sizeof(unsigned short));
   p += sizeof(unsigned short);
	
   *p++ = 0;            /** Mesh id length */
   memcpy(p, _IMCP_DATA, sizeof(_IMCP_DATA));
   p += sizeof(_IMCP_DATA);

   len = p - buffer;

   sendto(sock_fd, buffer, len, 0, (const struct sockaddr *)&addr, sizeof(addr));

   ret = select(sock_fd + 1, &select_set, NULL, NULL, &timeout);

   if (ret > 0)
   {
      from_len = sizeof(addr);
      len      = recvfrom(sock_fd, buffer, 64, 0, (struct sockaddr *)&addr, &from_len);
      if (len <= 0)
      {
         ret = 0;
      }
   }

   if (ret == 0)
   {
      printf("watchd: No response from configd, restarting....\n");
      system("kill -9 `pidof configd`");
      system("/sbin/configd&");
   }

   close(sock_fd);
}


static void _check_miscd()
{
   int ret;

   /**
    * See if miscd is alive
    */

   ret = system("/root/watchd_helper.sh miscd");

   if (WEXITSTATUS(ret) == 1)
   {
      printf("watchd: miscd was not found!, restarting....\n");
      system("/sbin/miscd&");
   }
}


static void _check_iperf()
{
   int ret;

   /**
    * See if iperf is alive
    */

   ret = system("/root/watchd_helper.sh iperf");

   if (WEXITSTATUS(ret) == 1)
   {
      printf("watchd: iperf was not found!, restarting....\n");
      system("iperf -s -D");
   }
}


static void _check_snmp()
{
   int ret;

   /**
    * See if iperf is alive
    */

   ret = system("/root/watchd_helper.sh meshsnmpd");

   if (WEXITSTATUS(ret) == 1)
   {
      printf("watchd: meshsnmpd was not found!, restarting....\n");
      system("/sbin/meshsnmpd&");
   }
}


static int _get_ds_netif()
{
   int                       fd;
   tddi_packet_t             packet;
   int                       ret;
   unsigned char             buffer[sizeof(tddi_packet_t) + sizeof(tddi_get_ds_if_response_t) + 128];
   tddi_packet_t             *response_packet;
   tddi_get_ds_if_response_t *response_data;
   char                      current_if_name[256];

   fd = open(TDDI_FILE_NAME, O_RDWR);

   if (fd == -1)
   {
      fprintf(stderr, "Error opening "TDDI_FILE_NAME "\n");
      return -1;
   }

   packet.signature = TDDI_PACKET_SIGNATURE;
   packet.version   = TDDI_CURRENT_VERSION;
   packet.data_size = 0;
   packet.type      = TDDI_PACKET_TYPE_REQUEST;
   packet.sub_type  = TDDI_REQUEST_TYPE_GET_DS_IF;

   ret = write(fd, &packet, sizeof(tddi_packet_t));

   if (ret != sizeof(tddi_packet_t))
   {
      fprintf(stderr, "Error writing to "TDDI_FILE_NAME " ret=%d\n", ret);
      return -1;
   }

   lseek(fd, 0, SEEK_CUR);

   ret = read(fd, buffer, sizeof(buffer));

   if (ret < sizeof(tddi_packet_t) + sizeof(tddi_get_ds_if_response_t))
   {
      return -1;
   }

   close(fd);

   response_packet = (tddi_packet_t *)buffer;
   response_data   = (tddi_get_ds_if_response_t *)(buffer + sizeof(tddi_packet_t));

   switch (response_data->response)
   {
   case TDDI_GET_DS_IF_STARTED:
      memset(current_if_name, 0, 256);
      memcpy(current_if_name, buffer + sizeof(tddi_packet_t) + sizeof(tddi_get_ds_if_response_t), response_data->if_name_length);
      fprintf(stderr, "\nWATCHD:\tDS net_if is %s\n", current_if_name);
      return 0;

   case TDDI_GET_DS_IF_RESPONSE_SCANNING:
      printf("DS_IF_RESPONSE_SCANNING\n");
      return -1;

   case TDDI_GET_DS_IF_RESPONSE_NOT_STARTED:
      printf("DS_IF_RESPONSE_NOT_STARTED\n");
      return -1;
   }

   return 0;
}


static void _wait_for_ap_to_start()
{
   int max;
   int ret;

   max = 6000;

   while (max--)
   {
      ret = _get_ds_netif();
      if (ret != 0)
      {
         sleep(1);
      }
      else
      {
         break;
      }
   }
}


int main(int argc, char **argv)
{
   printf("watchd: Waiting for AP to start...\n");

   _wait_for_ap_to_start();

   printf("watchd: Starting...\n");

   while (1)
   {
      sleep(300);
      _check_configd();
      _check_miscd();
      _check_iperf();
      _check_snmp();
   }

   printf("watchd: Quitting...\n");

   return 0;
}
