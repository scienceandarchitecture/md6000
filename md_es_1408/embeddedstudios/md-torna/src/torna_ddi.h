/********************************************************************************
* MeshDynamics
* --------------
* File     : torna_ddi.h
* Comments : Torna Driver-Daemon Interface
* Created  : 5/20/2004
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* | 20  |8/22/2008 | Changes for SIP                                 |Abhijit |
* -----------------------------------------------------------------------------
* | 19  |01/07/2008| Added virtual configure command				  |Abhijit |
* -----------------------------------------------------------------------------
* | 18  |7/11/2007 | TDDI_REQUEST_TYPE_FULL_DISCONNECT added         | Sriram |
* -----------------------------------------------------------------------------
* | 17  |7/9/2007  | interval field added to reset gen               | Sriram |
* -----------------------------------------------------------------------------
* | 16  |7/9/2007  | Added Block CPU command                         | Sriram |
* -----------------------------------------------------------------------------
* | 15  |5/17/2007 | Added VLAN command                              | Sriram |
* -----------------------------------------------------------------------------
* | 14  |12/14/2006| Reset generator changes                         | Sriram |
* -----------------------------------------------------------------------------
* | 13  |5/12/2006 | Added SET_REBOOT_FLAG                           | Bindu  |
* -----------------------------------------------------------------------------
* | 12  |3/14/2006 | Added ACL Request & Response                    | Bindu  |
* -----------------------------------------------------------------------------
* | 11  |02/15/2006| Added dot11e request                            | Bindu  |
* -----------------------------------------------------------------------------
* | 10  |6/17/2005 | Added mesh/ap flags request                     | Sriram |
* -----------------------------------------------------------------------------
* |  9  |6/16/2005 | Added Random request/response                   | Sriram |
* -----------------------------------------------------------------------------
* |  8  |6/15/2005 | Added kick child request/response               | Sriram |
* -----------------------------------------------------------------------------
* |  7  |5/20/2005 | Added preferred parent request/response         | Sriram |
* -----------------------------------------------------------------------------
* |  6  |4/14/2005 | Added  Tests 13_1 and 14_1                      | Sriram |
* -----------------------------------------------------------------------------
* |  5  |4/2/2005  | Added Test 12_1                                 | Sriram |
* -----------------------------------------------------------------------------
* |  4  |12/26/2004| Added reboot request                            | Sriram |
* -----------------------------------------------------------------------------
* |  3  |12/15/2004| Added get_ds_if request                         | Sriram |
* -----------------------------------------------------------------------------
* |  1  |7/22/2004 | Added TEST 11_1                                 | Sriram |
* -----------------------------------------------------------------------------
* |  0  |5/20/2004 | Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

#ifndef __TORNA_DDI_H__
#define __TORNA_DDI_H__

#define TDDI_MAKE_VERSION(major, minor, rev)    ((((major) & 0xFF) << 8) | (((minor) & 0xF) << 4) | ((rev) & 0xF))
#define TDDI_VERSION_MAJOR(version)             ((version) >> 8)
#define TDDI_VERSION_MINOR(version)             (((version) >> 4) & 0xF)
#define TDDI_VERSION_REVISION(version)          ((version) & 0xF)

/**
 * The current TDDI version
 */

#define TDDI_CURRENT_VERSION    TDDI_MAKE_VERSION(_TORNA_VERSION_MAJOR_, _TORNA_VERSION_MINOR_, _TORNA_VERSION_VARIANT_)

/**
 * The TDDI file name
 */

#define TDDI_FILE_NAME               "/dev/tddi"
//SPAWAR
#define TEMP_STATS_FILE_NAME         "/sys/class/hwmon/hwmon0/device/temp0_input"
#define VOLT_STATS_FILE_NAME         "/sys/class/hwmon/hwmon0/device/in0_input"
//SPAWAR_END

#define TDDI_PACKET_SIGNATURE        0x0ABCDEF0
#define TDDI_PACKET_TYPE_REQUEST     1
#define TDDI_PACKET_TYPE_RESPONSE    2

/**
 * Important: The fields signature and version must never be modified or moved
 * New fields *must* be added anywhere after signature and version.
 */

struct tddi_packet
{
   unsigned int   signature;            /** TDDI_PACKET_SIGNATURE */
   unsigned short version;              /** TDDI version (8-BIT-MAJOR, 4-BIT-MINOR, 4-BIT-REVISION) */
   unsigned char  type;                 /** TDDI_PACKET_TYPE_* */
   unsigned char  sub_type;
   unsigned short data_size;
};

typedef struct tddi_packet   tddi_packet_t;

/**
 * The various Request Types
 */

#define TDDI_REQUEST_TYPE_START_MESH            1       /** HAS NO DATA */
#define TDDI_REQUEST_TYPE_STOP_MESH             2       /** HAS NO DATA */
#define TDDI_REQUEST_TYPE_CONFIGURE_MESH        3       /** tddi_configure_mesh_request_t */
#define TDDI_REQUEST_TYPE_TEST_1_1              4       /** HAS NO DATA */
#define TDDI_REQUEST_TYPE_TEST_1_2              5       /** HAS NO DATA */
#define TDDI_REQUEST_TYPE_TEST_2_1              6       /** HAS NO DATA */
#define TDDI_REQUEST_TYPE_TEST_2_2              7       /** HAS NO DATA */
#define TDDI_REQUEST_TYPE_TEST_3_1              8       /** HAS NO DATA */
#define TDDI_REQUEST_TYPE_TEST_3_2              9       /** HAS NO DATA */
#define TDDI_REQUEST_TYPE_TEST_4_1              10      /** HAS NO DATA */
#define TDDI_REQUEST_TYPE_TEST_5_1              11      /** HAS NO DATA */
#define TDDI_REQUEST_TYPE_TEST_6_1              12      /** HAS NO DATA */
#define TDDI_REQUEST_TYPE_TEST_7_1              13      /** HAS NO DATA */
#define TDDI_REQUEST_TYPE_TEST_8_1              14      /** HAS NO DATA */
#define TDDI_REQUEST_TYPE_TEST_9_1              15      /** HAS NO DATA */
#define TDDI_REQUEST_TYPE_TEST_10_1             16      /** HAS NO DATA */
#define TDDI_REQUEST_TYPE_TEST_11_1             20      /** HAS NO DATA */
#define TDDI_REQUEST_TYPE_GET_DS_IF             21      /** HAS NO DATA */
#define TDDI_REQUEST_TYPE_REBOOT                22      /** HAS NO DATA */
#define TDDI_REQUEST_TYPE_TEST_12_1             23      /** HAS NO DATA */
#define TDDI_REQUEST_TYPE_TEST_13_1             24      /** HAS NO DATA */
#define TDDI_REQUEST_TYPE_TEST_14_1             25      /** HAS NO DATA */
#define TDDI_REQUEST_TYPE_PREFERRED_PARENT      26      /** tddi_preferred_parent_request_t */
#define TDDI_REQUEST_TYPE_KICK_CHILD            27      /** tddi_kick_child_request_t */
#define TDDI_REQUEST_TYPE_RANDOM                28      /** tddi_random_request_t */
#define TDDI_REQUEST_TYPE_MESH_FLAGS            29      /** tddi_mesh_flags_request_t */
#define TDDI_REQUEST_TYPE_AP_FLAGS              30      /** tddi_ap_flags_request_t */
#define TDDI_REQUEST_TYPE_MESH_CONFIG_SQNR      31      /** tddi_mesh_config_sqnr_request_t */
#define TDDI_REQUEST_TYPE_CONFIGURE_DOT11E      32      /** tddi_configure_do11e_request_t */
#define TDDI_REQUEST_TYPE_CONFIGURE_ACL         33      /** tddi_configure_acl_request_t */
#define TDDI_REQUEST_TYPE_CONFIGURE_MOBILITY    34      /** tddi_configure_mobility_request_t */
#define TDDI_REQUEST_TYPE_LOCK_REBOOT           35      /** tddi_lock_reboot_request_t */
#define TDDI_REQUEST_TYPE_SET_REBOOT_FLAG       36      /** tddi_set_reboot_flag_request_t */
#define TDDI_REQUEST_TYPE_ENABLE_RESET_GEN      37      /** tddi_enable_reset_gen_request_t */
#define TDDI_REQUEST_TYPE_SET_VLAN              38      /** tddi_set_vlan_request_t */
#define TDDI_REQUEST_TYPE_BLOCK_CPU             39      /** tddi_block_cpu_request_t */
#define TDDI_REQUEST_TYPE_FULL_DISCONNECT       40      /** HAS NO DATA */
#define TDDI_REQUEST_TYPE_GET_DHCP_INFO         41      /** HAS NO DATA */
#define TDDI_REQUEST_TYPE_GPIO_READ_WRITE       42      /** tddi_gpio_request_t */
#define TDDI_REQUEST_TYPE_SET_GPS_INFO          43      /** tddi_gps_info_t */
#define TDDI_REQUEST_TYPE_VIRTUAL_CONFIGURE     44      /** tddi_vconfigure_request_t */
#define TDDI_REQUEST_TYPE_CONFIGURE_SIP         45      /** tddi_configure_sip_request_t */
#define TDDI_REQUEST_TYPE_ETH_LINK_FLAG         46      /** tddi_eth_link_status_t */
#define TDDI_REQUEST_TYPE_QUERY_BOARD_STATS     47      /** tddi_query_board_stats */ //SPAWAR
//SOWNDARYAHT_VHT
#define TDDI_REQUEST_TYPE_GET_HW_CONFIG         48
#define TDDI_RESPONSE_TYPE_GET_HW_CONFIG        49
#define TDDI_REQUEST_TYPE_GET_NODE_TYPE			50
#define TDDI_RESPONSE_TYPE_GET_NODE_TYPE			51
#define TDDI_REQUEST_TYPE_HALT_MESH					52
#define TDDI_REQUEST_TYPE_SEND_HW_INFO				53			/** tddi_hw_info_request_t */
#define TDDI_RESPONSE_TYPE_SEND_HW_INFO			54			/** tddi_hw_info_response_t */
#define TDDI_REQUEST_TYPE_SEND_STATION_INFO		55			/** tddi_station_info_request_t */
#define TDDI_RESPONSE_TYPE_SEND_STATION_INFO		56			/** tddi_station_info_response_t */
/*Duty cycle wm flag*/
#define TDDI_REQUEST_TYPE_GET_WM_CHANNEL_FLAG   57
/**
 * Request structures
 */

#define STA_INFO_PKT_TYPE_1							1
struct tddi_station_info_request {
   unsigned char sta_mac[6];
   unsigned char ds_mac[6];
   unsigned int  sta_info_pkt_type;
};

typedef struct tddi_station_info_request tddi_station_info_request_t;


struct tddi_configure_mesh_request
{
   unsigned int config_string_length;
};

typedef struct tddi_configure_mesh_request   tddi_configure_mesh_request_t;

struct tddi_virtual_configure_request
{
   unsigned int virtual_config_string_length;
};

typedef struct tddi_virtual_configure_request   tddi_virtual_configure_request_t;

//SOWNDRAYAHT_VHT
#define IEEE80211_HT_MCS_MASK_LEN               10

struct tddi_get_hw_config_request
{
	char if_name[12];
	unsigned char sub_type;
};
typedef struct tddi_get_hw_config_request tddi_get_hw_config_request_t;

/*BOOBALAN Getting duty_cycle_flag */

struct tddi_get_duty_cycle_flag_request
{
    char name[12];
};
typedef struct tddi_get_duty_cycle_flag_request tddi_get_duty_cycle_flag_request_t;

struct tddi_get_duty_cycle_flag
{
    int duty_cycle_flag;
    int operating_channel;
};
typedef struct tddi_get_duty_cycle_flag tddi_get_duty_cycle_flag_t;

struct __attribute__((__packed__)) tddi_mcs_info
{
	unsigned char rx_mask[IEEE80211_HT_MCS_MASK_LEN];
	unsigned short rx_highest;
	unsigned char tx_params;
	unsigned char reserved[3];
};
typedef struct tddi_mcs_info tddi_mcs_info_t;

struct tddi_ht_cap {
	unsigned short cap;
	unsigned char  ht_supported;
	unsigned char  ampdu_factor;
	unsigned char  ampdu_density;
	struct tddi_mcs_info mcs;
};
typedef struct tddi_ht_cap tddi_ht_cap_t;

struct __attribute__((__packed__)) tddi_vht_mcs_info
{
	unsigned short rx_mcs_map;
	unsigned short rx_highest;
	unsigned short tx_mcs_map;
	unsigned short tx_highest;
};
typedef struct tddi_vht_mcs_info tddi_vht_mcs_info_t;

struct tddi_vht_cap {
	unsigned char vht_supported;
	unsigned int cap;
	struct tddi_vht_mcs_info vht_mcs;
};
typedef struct tddi_vht_cap tddi_vht_cap_t;


struct tddi_get_hw_config
{
	int response;                /* TDDI_CONFIGURE_MESH_RESPONSE_* */
	tddi_ht_cap_t ht_cap;
	tddi_vht_cap_t vht_cap;
};
typedef struct tddi_get_hw_config tddi_get_hw_config_t;

//SOWNDRAYAHT_VHT END


/**
 * The various Response Types
 */

#define TDDI_RESPONSE_TYPE_START_MESH            1
#define TDDI_RESPONSE_TYPE_STOP_MESH             2
#define TDDI_RESPONSE_TYPE_CONFIGURE_MESH        3
#define TDDI_RESPONSE_TYPE_TEST                  4
#define TDDI_RESPONSE_TYPE_GET_DS_IF             5
#define TDDI_RESPONSE_TYPE_REBOOTING             6
#define TDDI_RESPONSE_TYPE_PREFERRED_PARENT      7          /** tddi_preferred_parent_response_t */
#define TDDI_RESPONSE_TYPE_KICK_CHILD            8          /** tddi_kick_child_response_t */
#define TDDI_RESPONSE_TYPE_RANDOM                9          /** tddi_random_response_t */
#define TDDI_RESPONSE_TYPE_MESH_FLAGS            10         /** tddi_mesh_flags_response_t */
#define TDDI_RESPONSE_TYPE_AP_FLAGS              11         /** tddi_ap_flags_response_t */
#define TDDI_RESPONSE_TYPE_MESH_CONFIG_SQNR      12         /** tddi_mesh_config_sqnr_response_t */
#define TDDI_RESPONSE_TYPE_CONFIGURE_DOT11E      13         /** tddi_configure_dot11e_response_t */
#define TDDI_RESPONSE_TYPE_CONFIGURE_ACL         14         /** tddi_configure_acl_response_t */
#define TDDI_RESPONSE_TYPE_CONFIGURE_MOBILITY    15         /** tddi_configure_mobility_response_t */
#define TDDI_RESPONSE_TYPE_LOCK_REBOOT           16         /** tddi_lock_reboot_response_t */
#define TDDI_RESPONSE_TYPE_SET_REBOOT_FLAG       17         /** tddi_set_reboot_flag_response_t */
#define TDDI_RESPONSE_TYPE_ENABLE_RESET_GEN      18         /** tddi_enable_reset_gen_response_t */
#define TDDI_RESPONSE_TYPE_SET_VLAN              19         /** tddi_set_vlan_response_t */
#define TDDI_RESPONSE_TYPE_BLOCK_CPU             20         /** No data */
#define TDDI_RESPONSE_TYPE_FULL_DISCONNECT       21         /** No data */
#define TDDI_RESPONSE_TYPE_DHCP_INFO             22         /** tddi_dhcp_response_t */
#define TDDI_RESPONSE_TYPE_GPIO_RESPONSE         23         /** tddi_gpio_response_t */
#define TDDI_RESPONSE_TYPE_GPS_RESPONSE          24         /** No data */
#define TDDI_RESPONSE_TYPE_VIRTUAL_CONFIGURE     25         /** tddi_vconfigure_response_t */
#define TDDI_RESPONSE_TYPE_CONFIGURE_SIP         26         /** tddi_configure_sip_response_t */
#define TDDI_RESPONSE_TYPE_HALT_MESH				 27			/** tddi_halt_response_t*/
/**
 * Response structures
 */

#define TDDI_START_MESH_RESPONSE_SUCCESS            0
#define TDDI_START_MESH_RESPONSE_NO_CONFIG          -1
#define TDDI_START_MESH_RESPONSE_ALREADY_STARTED    -2

struct tddi_start_mesh_response
{
   int response;                /* TDDI_START_MESH_RESPONSE_* */
};

typedef struct tddi_start_mesh_response   tddi_start_mesh_response_t;

#define TDDI_STOP_MESH_RESPONSE_SUCCESS        0
#define TDDI_STOP_MESH_RESPONSE_NOT_STARTED    -1

struct tddi_stop_mesh_response
{
   int response;                /* TDDI_STOP_MESH_RESPONSE_* */
};

typedef struct tddi_stop_mesh_response   tddi_stop_mesh_response_t;

#define TDDI_HALT_MESH_RESPONSE_SUCCESS 0

struct tddi_halt_mesh_response
{
	int response;					  /* TDDI_HALT_MESH_RESPONSE_* */
};

typedef struct tddi_halt_mesh_response   tddi_halt_mesh_response_t;

#define TDDI_CONFIGURE_MESH_RESPONSE_SUCCESS    0
#define TDDI_CONFIGURE_MESH_RESPONSE_BUSY       -1

struct tddi_configure_mesh_response
{
   int response;                /* TDDI_CONFIGURE_MESH_RESPONSE_* */
};

typedef struct tddi_configure_mesh_response   tddi_configure_mesh_response_t;

struct tddi_test_response
{
   int response;                /* Always 0 */
};

typedef struct tddi_test_response   tddi_test_response_t;

#define TDDI_GET_DS_IF_RESPONSE_SCANNING       -2
#define TDDI_GET_DS_IF_RESPONSE_NOT_STARTED    -1
#define TDDI_GET_DS_IF_STARTED                 0

struct tddi_get_ds_if_response
{
   int response;
   int if_name_length;
   /** Followed by the name of the DS interface */
};

typedef struct tddi_get_ds_if_response   tddi_get_ds_if_response_t;

struct tddi_reboot_response
{
   int response;                /* Always 0 */
};

typedef struct tddi_reboot_response   tddi_reboot_response_t;

struct tddi_preferred_parent_request
{
   unsigned char enabled;
   unsigned char preferred_parent[6];
};
typedef struct tddi_preferred_parent_request   tddi_preferred_parent_request_t;

#define TDDI_PREFERRED_PARENT_NOT_APPLICABLE          -3
#define TDDI_PREFERRED_PARENT_RESPONSE_NOT_FOUND      -2
#define TDDI_PREFERRED_PARENT_RESPONSE_NOT_STARTED    -1
#define TDDI_PREFERRED_PARENT_RESPONSE_ACCEPTED       0

struct tddi_preferred_parent_response
{
   int response;
};
typedef struct tddi_preferred_parent_response   tddi_preferred_parent_response_t;

struct tddi_kick_child_request
{
   unsigned char address[6];
};
typedef struct tddi_kick_child_request   tddi_kick_child_request_t;

#define TDDI_KICK_CHILD_RESPONSE_NOT_STARTED    -2
#define TDDI_KICK_CHILD_RESPONSE_NOT_FOUND      -1
#define TDDI_KICK_CHILD_RESPONSE_KICKED         0

struct tddi_kick_child_response
{
   int response;
};

typedef struct tddi_kick_child_response   tddi_kick_child_response_t;

struct tddi_random_request
{
   int min;
   int max;
};

typedef struct tddi_random_request   tddi_random_request_t;

struct tddi_random_response
{
   int random;
};

typedef struct tddi_random_response   tddi_random_response_t;

struct tddi_mesh_flags_request
{
   unsigned int flags;
};

typedef struct tddi_mesh_flags_request   tddi_mesh_flags_request_t;

struct tddi_eth_link_state
{
   unsigned int link_state;
};

typedef struct tddi_eth_link_state   tddi_eth_link_state_t;
//SPAWAR
struct tddi_board_stats
{
   int voltage;
   int temp;
};
typedef struct tddi_board_stats tddi_board_stats_t;
//SPAWAR_END

struct tddi_ap_flags_request
{
   unsigned int flags;
};

typedef struct tddi_ap_flags_request   tddi_ap_flags_request_t;

struct tddi_mesh_flags_response
{
   int response;
};

typedef struct tddi_mesh_flags_response   tddi_mesh_flags_response_t;

struct tddi_ap_flags_response
{
   int response;
};

typedef struct tddi_ap_flags_response   tddi_ap_flags_response_t;

struct tddi_mesh_config_sqnr_request
{
   unsigned int sqnr;
};

typedef struct tddi_mesh_config_sqnr_request   tddi_mesh_config_sqnr_request_t;

#define TDDI_MESH_CONFIG_SQNR_RESPONSE_UPDATED    0

struct tddi_mesh_config_sqnr_response
{
   int response;
};

typedef struct tddi_mesh_config_sqnr_response   tddi_mesh_config_sqnr_response_t;

struct tddi_configure_dot11e_request
{
   unsigned int config_string_length;
};

typedef struct tddi_configure_dot11e_request   tddi_configure_dot11e_request_t;

#define TDDI_CONFIGURE_DOT11E_RESPONSE_SUCCESS    0
#define TDDI_CONFIGURE_DOT11E_RESPONSE_BUSY       -1

struct tddi_configure_dot11e_response
{
   int response;                /* TDDI_CONFIGURE_DOT11E_RESPONSE_* */
};

typedef struct tddi_configure_dot11e_response   tddi_configure_dot11e_response_t;


struct tddi_configure_acl_request
{
   unsigned int config_string_length;
};

typedef struct tddi_configure_acl_request   tddi_configure_acl_request_t;

#define TDDI_CONFIGURE_ACL_RESPONSE_SUCCESS    0
#define TDDI_CONFIGURE_ACL_RESPONSE_BUSY       -1

struct tddi_configure_acl_response
{
   int response;                /* TDDI_CONFIGURE_ACL_RESPONSE_* */
};

typedef struct tddi_configure_acl_response   tddi_configure_acl_response_t;

struct tddi_configure_mobility_request
{
   unsigned int config_string_length;
};

typedef struct tddi_configure_mobility_request   tddi_configure_mobility_request_t;

#define TDDI_CONFIGURE_MOBILITY_RESPONSE_SUCCESS    0
#define TDDI_CONFIGURE_MOBILITY_RESPONSE_BUSY       -1


struct tddi_configure_mobility_response
{
   int response;                /* TDDI_CONFIGURE_MOBILITY_RESPONSE_* */
};

typedef struct tddi_configure_mobility_response   tddi_configure_mobility_response_t;

struct tddi_lock_reboot_request
{
   int lock_or_unlock;
};

typedef struct tddi_lock_reboot_request   tddi_lock_reboot_request_t;

struct tddi_lock_reboot_response
{
   int lock_count;              /* TDDI_LOCK_REBOOT_RESPONSE_* */
};

typedef struct tddi_lock_reboot_response   tddi_lock_reboot_response_t;

struct tddi_set_reboot_flag_request
{
   unsigned int flag;
};

typedef struct tddi_set_reboot_flag_request   tddi_set_reboot_flag_request_t;

#define TDDI_SET_REBOOT_FLAG_RESPONSE_SUCCESS    0
#define TDDI_SET_REBOOT_FLAG_RESPONSE_BUSY       -1

struct tddi_set_reboot_flag_response
{
   int response;                /* TDDI_SET_REBOOT_FLAG_RESPONSE_* */
};

typedef struct tddi_set_reboot_flag_response   tddi_set_reboot_flag_response_t;

struct tddi_enable_reset_gen_request
{
   unsigned int flag;
   unsigned int interval;
};

typedef struct tddi_enable_reset_gen_request   tddi_enable_reset_gen_request_t;

struct tddi_enable_reset_gen_response
{
   int ignore;
};

typedef struct tddi_enable_reset_gen_response   tddi_enable_reset_gen_response_t;

struct tddi_set_vlan_request
{
   unsigned char  address[6];
   unsigned short vlan_tag;
};
typedef struct tddi_set_vlan_request   tddi_set_vlan_request_t;

#define TDDI_SET_VLAN_RESPONSE_NOT_STARTED    -3
#define TDDI_SET_VLAN_RESPONSE_NOT_FOUND      -2
#define TDDI_SET_VLAN_RESPONSE_WRONG_PARAM    -1
#define TDDI_SET_VLAN_RESPONSE_OK             0

struct tddi_set_vlan_response
{
   int response;
};

typedef struct tddi_set_vlan_response   tddi_set_vlan_response_t;

#define TDDI_BLOCK_CPU_REQUEST_TYPE_LOOP        1
#define TDDI_BLOCK_CPU_REQUEST_TYPE_IRQ_LOCK    2
#define TDDI_BLOCK_CPU_REQUEST_TYPE_CRASH       3
#define TDDI_BLOCK_CPU_REQUEST_TYPE_PANIC       4

struct tddi_block_cpu_request
{
   unsigned int type;           /* TDDI_BLOCK_CPU_REQUEST_TYPE_* */
};
typedef struct tddi_block_cpu_request   tddi_block_cpu_request_t;

#define TDDI_GPIO_REQUEST_READ     1
#define TDDI_GPIO_REQUEST_WRITE    2

struct tddi_gpio_request
{
   unsigned char gpio;                  /* GPIO number */
   unsigned char read_write;            /* 1 = READ, 2 = WRITE */
   unsigned char value;                 /* Value 0/1 */
};
typedef struct tddi_gpio_request   tddi_gpio_request_t;

struct tddi_dhcp_response
{
   unsigned char dhcp_enabled;
   unsigned char self_dhcp_ip_address[4];
};
typedef struct tddi_dhcp_response   tddi_dhcp_response_t;

#define TDDI_GPIO_RESPONSE_READ_SUCCESS     1
#define TDDI_GPIO_RESPONSE_WRITE_SUCCESS    2
#define TDDI_GPIO_RESPONSE_FAILURE          -1

struct tddi_gpio_response
{
   int           response;
   unsigned char value;                 /* Value 0/1 */
};
typedef struct tddi_gpio_response   tddi_gpio_response_t;

struct tddi_gps_info
{
   char latitude[32];
   char longitude[32];
   char altitude[32];
   char speed[32];
};
typedef struct tddi_gps_info   tddi_gps_info_t;

#define TDDI_VIRTUAL_CONFIGURE_RESPONSE_SUCCESS    0
#define TDDI_VIRTUAL_CONFIGURE_RESPONSE_BUSY       -1

struct tddi_virtual_configure_response
{
   int response;                /* TDDI_VIRTUAL_CONFIGURE_RESPONSE_* */
};

typedef struct tddi_virtual_configure_response   tddi_virtual_configure_response_t;

struct tddi_configure_sip_request
{
   unsigned int config_string_length;
};

typedef struct tddi_configure_sip_request   tddi_configure_sip_request_t;

#define TDDI_CONFIGURE_SIP_RESPONSE_SUCCESS    0
#define TDDI_CONFIGURE_SIP_RESPONSE_BUSY       -1

struct tddi_configure_sip_response
{
   int response;                /* TDDI_CONFIGURE_SIP_RESPONSE_* */
};

typedef struct tddi_configure_sip_response   tddi_configure_sip_response_t;

#endif /*__TORNA_DDI_H__*/
