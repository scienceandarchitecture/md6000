/********************************************************************************
* MeshDynamics
* --------------
* File     : torna_types.h
* Comments : Torna type definitions
* Created  : 5/11/2004
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  0  |5/11/2004 | Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

#ifndef __TORNA_TYPES_H__
#define __TORNA_TYPES_H__

#define TORNA_BIT(x)    (1 << (x))

#define MAC2STR(a)      (a)[0], (a)[1], (a)[2], (a)[3], (a)[4], (a)[5]
#define MACSTR    "%02x:%02x:%02x:%02x:%02x:%02x"

#define PACKET_DUMP(type, bytes, length)                           \
   ({                                                              \
      int i, j;                                                    \
      for (i = 0, j = 0; i<length; i++) {                          \
                              if (j >= 16) {                       \
                                 j = 0;                            \
                                 printk("\n");                     \
                              }                                    \
                              if (j == 0) {                        \
                                 printk(type "%02x ", bytes[i]); } \
                              else{                                \
                                 printk("%02x ", bytes[i]); }      \
                              j++;                                 \
                           }                                       \
                           printk("\n");                           \
                           }                                       \
                           )

#define PACKET_DUMP_EX(type, name, bytes, length)             \
   ({                                                         \
      int i, j;                                               \
      printk(type "\r\nunsigned char %s[]={\n", name);        \
      for (i = 0, j = 0; i<length; i++) {                     \
                              if (j >= 16) {                  \
                                 j = 0;                       \
                                 printk("\n");                \
                              }                               \
                              printk(type "0x%X,", bytes[i]); \
                              j++;                            \
                           }                                  \
                           printk(type "\n};\n");             \
                           }                                  \
                           )

#define PACKET_DUMP_2(type, bytes, length, message)                \
   ({                                                              \
      int i, j;                                                    \
      printk(type "%s\n", message);                                \
      for (i = 0, j = 0; i<length; i++) {                          \
                              if (j >= 16) {                       \
                                 j = 0;                            \
                                 printk("\n");                     \
                              }                                    \
                              if (j == 0) {                        \
                                 printk(type "%02x ", bytes[i]); } \
                              else{                                \
                                 printk("%02x ", bytes[i]); }      \
                              j++;                                 \
                           }                                       \
                           printk("\n");                           \
                           }                                       \
                           )

#define ADDR_IS_MULTICAST(addr) \
   (                            \
      (addr)[0] == 0x01 &&      \
      (addr)[1] == 0x00 &&      \
      (addr)[2] == 0x5E         \
   )

#define ADDR_IS_BROADCAST(addr) \
   (                            \
      (addr)[0] == 0xFF &&      \
      (addr)[1] == 0xFF &&      \
      (addr)[2] == 0xFF &&      \
      (addr)[3] == 0xFF &&      \
      (addr)[4] == 0xFF &&      \
      (addr)[5] == 0xFF         \
   )
#endif /*__TORNA_TYPES_H__*/
