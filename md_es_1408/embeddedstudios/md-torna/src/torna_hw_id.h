/********************************************************************************
* MeshDynamics
* --------------
* File     : torna_hw_id.h
* Comments : Torna Hardware ID header file
* Created  : 11/12/2004
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  2  |8/6/2007  | Flip-bit flag added to REBOOT header            | Sriram |
* -----------------------------------------------------------------------------
* |  1  |7/24/2007 | REBOOT logging changes                          | Sriram |
* -----------------------------------------------------------------------------
* |  0  |11/12/2004| Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

#ifndef __TORNA_HW_ID_H__
#define __TORNA_HW_ID_H__

/**
 *
 *	+---+---+---+---+---+---+
 *  |   MD OUI  |  UNUSED   |  6   Meshdynamics OUI (00-12-CE)
 *  +---+---+---+---+---+---+
 *  |    OUI    |  PROD ID  |  6   OUI of OEM or of Meshdynamics
 *  +---+---+---+---+---+---+
 *	| GENERIC 5 BYTE ID |CNT|  6   Generic ID can be used for manufacturing
 *	+---+---+---+---+---+---+
 *	|       ADDRESS 0       |  6
 *	+---+---+---+---+---+---+
 *	:                       :
 *	:                       :
 *	+---+---+---+---+---+---+
 *	|   ADDRESS CNT - 1     |  6
 *	+---+---+---+---+---+---+
 */

struct torna_hw_id_header
{
   unsigned char md_oui[3];
   unsigned char unused[3];
   unsigned char product_oui[3];
   unsigned char product_id[3];
   unsigned char generic_id[5];
   unsigned char address_count;
}
__attribute__((packed));

typedef struct torna_hw_id_header   torna_hw_id_header_t;

struct torna_reboot_info
{
   unsigned char code;
   unsigned char temp;
   unsigned char voltage;
   unsigned char mem_used;
   char          process_name[16];
   unsigned char pc_register[4];
   unsigned char caller_register[4];
   unsigned char stack_dump_count;
}
__attribute__((packed));

typedef struct torna_reboot_info   torna_reboot_info_t;

/**
 * To be called by drivers to consume a MAC address. This function is
 * implemented by Hardware ID adapters for specific platforms that
 * Torna will runon.
 */

#define TORNA_HW_ID_NO_MORE_ADDRESSES    -1
#define TORNA_HW_ID_SUCCESS              0

int torna_hw_id_get_address(unsigned char *address);
int torna_get_product_oui_id(unsigned char *product_oui_id);
int torna_get_generic_id(unsigned char *generic_id);
int torna_put_reboot_info(torna_reboot_info_t *info, unsigned int *stack_dump_info);
int torna_get_reboot_info(torna_reboot_info_t *info, unsigned char *flag, unsigned int *stack_dump_info);

#define TORNA_HW_ID_OUI_IS_MESHDYNAMICS(oui) \
   (                                         \
      (oui)[0] == 0x00 &&                    \
      (oui)[1] == 0x12 &&                    \
      (oui)[2] == 0xCE                       \
   )

#endif /*__TORNA_HW_ID_H__*/
