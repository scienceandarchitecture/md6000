
/********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : patcher.c
 * Comments : Main Patching Utility (Runs directly on Board)
 * Created  : 11/23/2004
 * Author   : Sriram Dayanandan
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * -----------------------------------------------------------------------------
 * | No  |Date      |  Comment                                        | Author |
 * -----------------------------------------------------------------------------
 * |  3  |11/21/2006| Added AL gen2k Implementation                   | Sriram |
 * -----------------------------------------------------------------------------
 * |  2  |21/20/2005| ack timeout set to default                      |prachiti|
 * -----------------------------------------------------------------------------
 * |  1  |4/11/2005 | Misc changes                                    | Sriram |
 * -----------------------------------------------------------------------------
 * |  0  |11/23/2004| Created.                                        | Sriram |
 * -----------------------------------------------------------------------------
 ********************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <getopt.h>
#include <string.h>
#include <strings.h>

#include "patch.h"
#include "mac_address.h"
#include "al.h"
#include "al_impl.h"
#include "al_impl_context.h"
#include "al_conf.h"

/**
 * The patcher utlity does three things
 *		1) Patch meshap.o
 *		2) Patch configd
 *		3) Patch meshsnmpd
 *		4) Patch alconfset
 *		5) Patch /etc/meshap.conf
 */
#define MAX_RADIO_MAC 7
#define MAX_VLAN_MAC 5
#define MIN_RADIO_MAC 1
#define MAX_APP_MAC 4
#define MAX_VIRTUAL_MAC 1
#define MAX_ETH_MAC 1

static void		_usage				();
static void		_patch_config_file	(char* text_key,unsigned char* mac_address, char *filename);
static int		_open_meshap_conf	();
char	config_name[512];

int main(int argc,char** argv)
{
	char			mac_address_ascii[32];
	unsigned char mac_address[200];
	char			path[512];
	char			text_key[512];
	int           ch,  j, i;
	int           ret, mac_found;
	int vlanCount = 0, phyRadcount = 0, appCount = 0, boardmac = 0 ,virtualCount = 1, ethcount = 0;
	unsigned   char *radMAC_ascii=NULL, radMAC[MAX_RADIO_MAC * 6];			/*Size for 6 Radio Interface*/
	unsigned char *vlanMAC_ascii=NULL, vlanMAC[MAX_VLAN_MAC * 6], *tmpAddr=NULL;  /*Size for 5 VLANs*/
	unsigned char *appMAC_ascii=NULL, appMAC[MAX_APP_MAC * 6];
	unsigned char *virtualMAC_ascii=NULL, virtualMAC[MAX_VIRTUAL_MAC * 6];
    unsigned char *ethMAC_ascii = NULL, ethMAC[MAX_ETH_MAC * 6];
	unsigned char *tmpptr = NULL;
	char	module_name[512];
	char	daemon_name[512];

	strcpy(text_key,_PATCHER_TEXT_KEY_DEFAULT_);
	memset(module_name, 0, 512);
	memset(daemon_name, 0, 512);
	memset(config_name, 0, 512);

	while ((ch = getopt(argc, argv, "m:d:c:vhk:r:b:j:a:e:")) != EOF)
	{
		switch(ch) {
		case 'v':
			fprintf(stdout,
				    "\nMeshDynamics Patcher version %s\n"
					"Copyright (c) 1992-2004 Advanced Cybernetics Group, Inc\n"
					"All rights reserved.\n\n",
					_TORNA_VERSION_STRING_);
			return 0;
		case 'h':
			_usage();
			return 0;
		case 'k':
			strcpy(text_key,optarg);
			break;
	
		case 'b':				/* b: option for Board MAC*/
			strcpy(mac_address_ascii, optarg);
			fprintf(stdout, "BOARD MAC:\t\t%s\n", mac_address_ascii);
			mac_address_ascii_to_binary(mac_address_ascii, mac_address);
			boardmac = 1;
		break;
	
		case 'r':				/* r: option for Physical interface MACs*/
		tmpptr = radMAC;
		do{
			if(!phyRadcount)
			{
				radMAC_ascii = strtok_r(optarg, " ", &tmpAddr);			//Read First MAC from Arguments
			}
			else
			{
				radMAC_ascii = strtok_r(NULL, " ", &tmpAddr);			// Read All VLAN MAC's
			}
			if(!radMAC_ascii)
			{
				break;
			}
			mac_address_ascii_to_binary(radMAC_ascii, tmpptr);			
			tmpptr = tmpptr + 6;
			phyRadcount++;
			fprintf(stdout, "RADIO_MAC:\t\t%s\n", radMAC_ascii);
		}while(radMAC_ascii);
	
		break; 
	
		case 'j':				/* j: Option for VLAN MACs*/
		tmpptr = vlanMAC;
		do{
			if(!vlanCount)
			{
				vlanMAC_ascii = strtok_r(optarg, " ", &tmpAddr);		// Read First VLAN MAc
			}
			else
			{
				vlanMAC_ascii = strtok_r(NULL, " ", &tmpAddr);			// Read All VLAN MAC's
			}
			if(!vlanMAC_ascii)
				break;
			mac_address_ascii_to_binary(vlanMAC_ascii, tmpptr);
			tmpptr = tmpptr + 6;
			vlanCount++;
			fprintf(stdout, "VLAN_MAC:\t\t%s\n", vlanMAC_ascii);
		}while(vlanMAC_ascii);
		break;

		case 'a':				/*a: option for APP MACs*/
			tmpptr = appMAC;
			do{
                if(!appCount)
                {
                    appMAC_ascii = strtok_r(optarg, " ", &tmpAddr);        // Read First APP MAC
                }
                else
                {
                    appMAC_ascii = strtok_r(NULL, " ", &tmpAddr);           // Read All APP MAC's
                }
                if(!appMAC_ascii)
                    break;
                mac_address_ascii_to_binary(appMAC_ascii, tmpptr);
                tmpptr = tmpptr + 6;
                appCount++;
                fprintf(stdout, "APP_MAC:\t\t%s\n", appMAC_ascii);
            }while(appMAC_ascii);
			break;
		case 'm':
			strcpy(module_name,optarg);
			break;
		case 'd':
			strcpy(daemon_name,optarg);
			break;
		case 'c':
			strcpy(config_name,optarg);
			break;
        case 'e':
            tmpptr = ethMAC;
            ethMAC_ascii = strtok_r(optarg, " ", &tmpAddr);
            if (!ethMAC_ascii)
                break;
            mac_address_ascii_to_binary(ethMAC_ascii, tmpptr);
            fprintf(stdout, "ETH1 MAC:\t\t%s\n", ethMAC_ascii);
            tmpptr = tmpptr + 6;
            ethcount = 1;
        break;
		default:
			_usage();
			return 0;
		}		
	}

	if(!boardmac)
	{
		fprintf(stderr, "Board MAC is mandatory\n");
		_usage();
		return 0;
	}
		/*Check whether number of Physical interface or Virtual interface exceeds or deceeds*/

	if(phyRadcount > MAX_RADIO_MAC || phyRadcount < MIN_RADIO_MAC )
	{
		fprintf(stderr, "Number of Radio mac addresses incorrect[%d] (should be between 1 or 6)\n", phyRadcount);
		return 0; 
	}
	if(vlanCount > MAX_VLAN_MAC)
	{
		fprintf(stderr, "Number of VLAN mac address incorrect [%d] (Should be at most 5)\n", vlanCount);
		return 0;
	}
	if(appCount > MAX_APP_MAC)
	{
		fprintf(stderr, "Number of APP mac address incorrect [%d] (Should be at most45)\n", appCount);
      return 0;
	}
	if(virtualCount > MAX_VIRTUAL_MAC)
	{
		fprintf(stderr, "Number of Virtual  mac address incorrect [%d] (Should be at most 1)\n", virtualCount);
      return 0;
	}
    if(ethcount > MAX_ETH_MAC)
    {
        fprintf(stderr, "Number of eth mac address incorrect [%d] (Should be at most 1)\n", ethcount);
        return 0;
    }

	printf("text_key = %s\n", text_key);
	printf("module_name = %s\n", module_name);
	printf("daemon_name = %s\n", daemon_name);
	printf("config_name = %s\n", config_name);
	phyRadcount--;
	memcpy(virtualMAC, &radMAC[phyRadcount * 6], 6);

	tmpptr = mac_address;			// Point to Board MAC address
	tmpptr = tmpptr + 6;			// Skip Board MAC Address
	*tmpptr = phyRadcount;			// Store Physical Interface (Radio) Count
	tmpptr = tmpptr + 1;			// Point to next location to store Vlan Count
	*tmpptr = virtualCount;			// Store Physical Interface (Radio) Count
	tmpptr = tmpptr + 1;			// Point to next location to store Vlan Count
	*tmpptr = vlanCount;			// Store Virtual interface Count
	tmpptr = tmpptr + 1;			// Point to store 1st Radio MAC
	*tmpptr = appCount;
	tmpptr = tmpptr + 1;
    *tmpptr = ethcount;
    tmpptr = tmpptr + 2;

	memcpy(tmpptr, radMAC, (phyRadcount * 6));					//Store RADIO MAC's
	tmpptr = tmpptr + (phyRadcount * 6) + ((MAX_RADIO_MAC - phyRadcount - 1) * 6);	// Point to store 1st VLAN MAC 
	memcpy(tmpptr, virtualMAC, (virtualCount * 6));					//Store Virtual MAC's
	tmpptr = tmpptr + (virtualCount * 6) + ((MAX_VIRTUAL_MAC - virtualCount) * 6);	// Point to store 1st Virtual MAC 
	memcpy(tmpptr, vlanMAC, (vlanCount * 6));				// Store VLAN MAC's
    tmpptr = tmpptr + (vlanCount * 6) + ((MAX_VLAN_MAC - vlanCount) * 6 );
	memcpy(tmpptr, appMAC, (appCount * 6));				// Store APP MACs
	tmpptr = tmpptr + (appCount *6) + ((MAX_APP_MAC - appCount) * 6);				// Point to store 1st APP MAC
    memcpy(tmpptr, ethMAC, (ethcount * 6));
    tmpptr = tmpptr + (ethcount * 6) + ((MAX_ETH_MAC - ethcount) * 6);
	
	fprintf(stdout,
			"\nMeshDynamics Patcher version %s\n"
			"Copyright (c) 1992-2004 Advanced Cybernetics Group, Inc\n"
			"All rights reserved.\n\n",
			_TORNA_VERSION_STRING_);

	fprintf(stderr,"Patching meshap.ko...",ret);
	sprintf(path,"%s", module_name);
	ret = patch_file_new(path, mac_address);

	if(ret != PATCH_ERROR_SUCCESS) {
		fprintf(stderr,"[error:%d]\n",ret);
	} else {
		fprintf(stderr,"[success]\n",ret);
	}

	fprintf(stderr,"Patching configd...",ret);
	sprintf(path,"%s", daemon_name);
	ret = patch_file_new(path, mac_address);
	if (ret != PATCH_ERROR_SUCCESS)
	{
		fprintf(stderr,"[error:%d]\n",ret);
	} else {
		fprintf(stderr,"[success]\n",ret);
	}

	fprintf(stderr,"Patching meshap.conf...",ret);
	sprintf(path,"%s", config_name);	
	_patch_config_file(text_key,mac_address, path);

	fprintf(stderr,"\npatcher: Finished!\n");
	return 0;
}

int al_open_file(AL_CONTEXT_PARAM_DECL int file_id, int mode)
{

	int		fd;
	int		open_mode;

	if(mode == AL_OPEN_FILE_MODE_READ){
		open_mode = O_RDONLY;
	}else {
		unlink(config_name);
		open_mode = O_WRONLY | O_CREAT;
	}

	fd	= open(config_name,open_mode); 
	if(fd == -1) {

		perror("Error opening /etc/meshap.conf\n");

		printf("\tError opening /etc/meshap.conf for mode %d...\n",mode);
		return -1;
	}

	return fd;

}

int al_read_file_line(AL_CONTEXT_PARAM_DECL int file_handle, char *string, int n)
{
	if(read(file_handle, string, n) <= 0)

		return -1;
	else 
		return 0;

}

int al_write_file (AL_CONTEXT_PARAM_DECL int file_handle, unsigned char* buffer, int bytes){
	
	if(write(file_handle, buffer, bytes) <= 0)
		return -1;
	else 
		return 0;

}

int al_close_file(AL_CONTEXT_PARAM_DECL int file_handle)
{
	close(file_handle);

	return 0;

}

void* al_heap_alloc(AL_CONTEXT_PARAM_DECL size_t size)
{	
	void *temp;
	temp = (void*)malloc(size);
	return temp;

}

void al_heap_free(AL_CONTEXT_PARAM_DECL void* memblock)
{
	free(memblock);
}

unsigned char* al_get_gen_2KB_buffer(AL_CONTEXT_PARAM_DECL_SINGLE)
{
	return (unsigned char*)malloc(2048);
}

void al_release_gen_2KB_buffer(AL_CONTEXT_PARAM_DECL unsigned char* buffer)
{
	free(buffer);
}

int al_print_log(AL_CONTEXT_PARAM_DECL int log_type, const char* format,...)
{
	/*
	va_list		args_list;
	va_start(args_list,format);
	return vprintf(format,args_list);
	*/
	return 0;
}

static void _usage()
{
	fprintf(stdout,
			"\n"
           "usage: patcher [-v] [-h] [-k text_key] [-b address] [-r Radio MAC addresses] [-j VLAN MAC addresses] [-a APP MAC address] [-e eth mac address]\n"
		    "-v         : for knowing the version\n"
		    "-h         : for help\n"
		    "-k         : specifiy text_key (cybernetics! is default)\n"
           "-b         : board MAC address in XX:XX:XX:XX:XX:XX format\n"
           "-r         : Radio MAC addresses seperated by space\n"
           "-j         : VLAN MAC addresses seperated by space\n"
           "-a         : APP MAC address seperated by space\n"
           "-e         : ETH MAC address in XX:XX:XX:XX:XX:XX format\n\n"
			);
}

static int open_meshap_conf(char *filename)
{
	FILE*				fp;
	long				size;
	unsigned char*		buffer;
	int					al_conf_handle;

	fp	= fopen(filename,"rb");

	if(fp == NULL)
		return -1;

	fseek(fp,0,SEEK_END);
	size	= ftell(fp);
	fseek(fp,0,SEEK_SET);

	buffer	= (unsigned char*)malloc(size + 1);
	memset(buffer,0,size + 1);
	fread(buffer,1,size,fp);

	fclose(fp);

	al_conf_handle	= al_conf_parse(AL_CONTEXT buffer);

	if(al_conf_handle == 0) {
		free(buffer);
		fprintf(stderr,"\nError parsing config file\n");
		return -1;
	}

	free(buffer);
	
	return al_conf_handle;
}

static void	 _patch_config_file(char* text_key,unsigned char* mac_address, char *filename)
{
	unsigned int					al_conf_handle;
	int					text_key_len;
	unsigned char		encoded_key[1024];
	unsigned char		decoded_key[1024];
	int					ret;
	int					i,count;
	al_conf_if_info_t	if_info;	

	al_conf_handle	= open_meshap_conf(filename);

	if(al_conf_handle <= 0) {
		fprintf(stderr,"\nmeshap.conf not found\n");
		return;
	}

	text_key_len	= strlen(text_key);

	ret = al_encode_key(AL_CONTEXT (unsigned char*)text_key,text_key_len,mac_address,encoded_key);

	if(ret <= 0 ) {
		fprintf(stderr,"\nret = %d (Error in encoding)\n",ret);
		return;
	}

	ret = al_conf_set_mesh_imcp_key_buffer(AL_CONTEXT al_conf_handle,encoded_key,ret);

	if(ret != 0 ) {
		fprintf(stderr,"\nret = %d (Error in al_conf_set)\n",ret);
		return;
	}

	ret	= al_conf_put(AL_CONTEXT al_conf_handle);

	if(ret != 0 ) {
		fprintf(stderr,"\nret = %d (Error in al_conf_put)\n",ret);
		return;
	}

	al_conf_close(AL_CONTEXT  al_conf_handle);



	/*
	 *	Verify encoded key by reading it back from file and decode it
	 */

	al_conf_handle = open_meshap_conf(filename);
	
	if(al_conf_handle <= 0) {
		fprintf(stderr,"\nmeshap.conf not found\n");
		return;
	}
	
	ret = al_conf_get_mesh_imcp_key_buffer(AL_CONTEXT al_conf_handle,encoded_key,1024);
	
	fprintf(stderr,"\nEncoded Key : %s", encoded_key);
	
	memset(decoded_key,0,1024);

	ret = al_decode_key(AL_CONTEXT encoded_key,mac_address,decoded_key);

	if(ret <= 0) {
		fprintf(stderr,"ret = %d (Error in decoding)\n",ret);
		return;
	}
	
	fprintf(stderr,"\nDecoded key = %s\n",decoded_key);
	
	fprintf(stderr,"\nSetting ACKTimeout...\n");
	
	count	= al_conf_get_if_count(al_conf_handle);
	for(i = 0; i < count; i++) {
		al_conf_get_if(al_conf_handle,i,&if_info);
		fprintf(stderr,"\ni=%d,%s,%d\n",i,if_info.name,if_info.phy_sub_type);
		if(if_info.phy_sub_type == AL_CONF_IF_PHY_SUB_TYPE_802_11_B)
			if_info.ack_timeout = 10;
		else
			if_info.ack_timeout = 5;
		
		al_conf_set_if(al_conf_handle,i,&if_info);
	}
	
	al_conf_put(AL_CONTEXT  al_conf_handle);

	al_conf_close(AL_CONTEXT  al_conf_handle);

	//fprintf(stderr,"\nCreating meshap.conf backup...",ret);
	//system("cp /etc/meshap.conf /etc/orig.meshap.conf");	
}
