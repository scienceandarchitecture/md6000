/********************************************************************************
* MeshDynamics
* --------------
* File     : patcher.c
* Comments : Main Patching Utility (Runs directly on Board)
* Created  : 11/23/2004
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  3  |11/21/2006| Added AL gen2k Implementation                   | Sriram |
* -----------------------------------------------------------------------------
* |  2  |21/20/2005| ack timeout set to default                      |prachiti|
* -----------------------------------------------------------------------------
* |  1  |4/11/2005 | Misc changes                                    | Sriram |
* -----------------------------------------------------------------------------
* |  0  |11/23/2004| Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <getopt.h>
#include <string.h>
#include <strings.h>

#include "patch.h"
#include "mac_address.h"
#include "al.h"
#include "al_impl.h"
#include "al_impl_context.h"
#include "al_conf.h"

/**
 * The patcher utlity does three things
 *		1) Patch meshap.o
 *		2) Patch configd
 *		3) Patch meshsnmpd
 *		4) Patch alconfset
 *		5) Patch /etc/meshap.conf
 */

static void _usage();
static void _patch_config_file(char *text_key, unsigned char *mac_address);
static int _open_meshap_conf();

int main(int argc, char **argv)
{
   char          mac_address_ascii[32];
   unsigned char mac_address[6];
   char          path[512];
   char          text_key[512];
   int           ch;
   int           ret;

   strcpy(text_key, _PATCHER_TEXT_KEY_DEFAULT_);

   while ((ch = getopt(argc, argv, "vhk:")) != EOF)
   {
      switch (ch)
      {
      case 'v':
         fprintf(stdout,
                 "\nMeshDynamics Patcher version %s\n"
                 "Copyright (c) 1992-2004 Advanced Cybernetics Group, Inc\n"
                 "All rights reserved.\n\n",
                 _TORNA_VERSION_STRING_);
         return 0;

      case 'h':
         _usage();
         return 0;

      case 'k':
         strcpy(text_key, optarg);
         break;
      }
   }

   if (optind >= argc)
   {
      fprintf(stderr, "\nNo address specified\n");
      _usage();
      return -1;
   }

   strcpy(mac_address_ascii, argv[optind]);

   mac_address_ascii_to_binary(mac_address_ascii, mac_address);

   fprintf(stdout,
           "\nMeshDynamics Patcher version %s\n"
           "Copyright (c) 1992-2004 Advanced Cybernetics Group, Inc\n"
           "All rights reserved.\n\n",
           _TORNA_VERSION_STRING_);

   fprintf(stderr, "Patching meshap.ko...");
   sprintf(path, "%s/meshap.ko", _PATCHER_MESHAP_PATH_);
   ret = patch_file(path, mac_address);

   if (ret != PATCH_ERROR_SUCCESS)
   {
      fprintf(stderr, "[error:%d]\n", ret);
   }
   else
   {
      fprintf(stderr, "[success]\n", ret);
   }

   fprintf(stderr, "Patching configd...", ret);
   sprintf(path, "%s/configd", _PATCHER_CONFIGD_PATH_);
   ret = patch_file(path, mac_address);

   if (ret != PATCH_ERROR_SUCCESS)
   {
      fprintf(stderr, "[error:%d]\n", ret);
   }
   else
   {
      fprintf(stderr, "[success]\n", ret);
   }

   fprintf(stderr, "Patching meshap.conf...", ret);
   _patch_config_file(text_key, mac_address);

   fprintf(stderr, "\npatcher: Finished!\n");
}


int al_open_file(AL_CONTEXT_PARAM_DECL int file_id, int mode)
{
   int fd;
   int open_mode;

   if (mode == AL_OPEN_FILE_MODE_READ)
   {
      open_mode = O_RDONLY;
   }
   else
   {
      unlink("/etc/meshap.conf");
      open_mode = O_WRONLY | O_CREAT;
   }

   fd = open("/etc/meshap.conf", open_mode);
   if (fd == -1)
   {
      perror("Error opening /etc/meshap.conf\n");

      printf("\tError opening /etc/meshap.conf for mode %d...\n", mode);
      return -1;
   }

   return fd;
}


int al_read_file_line(AL_CONTEXT_PARAM_DECL int file_handle, char *string, int n)
{
   if (read(file_handle, string, n) <= 0)
   {
      return -1;
   }
   else
   {
      return 0;
   }
}


int al_write_file(AL_CONTEXT_PARAM_DECL int file_handle, unsigned char *buffer, int bytes)
{
   if (write(file_handle, buffer, bytes) <= 0)
   {
      return -1;
   }
   else
   {
      return 0;
   }
}


int al_close_file(AL_CONTEXT_PARAM_DECL int file_handle)
{
   close(file_handle);

   return 0;
}


void *al_heap_alloc(AL_CONTEXT_PARAM_DECL size_t size)
{
   void *temp;

   temp = (void *)malloc(size);

   if(temp) {
	   memset(temp, 0, size);
   }
   return temp;
}


void al_heap_free(AL_CONTEXT_PARAM_DECL void *memblock)
{
   free(memblock);
}


unsigned char *al_get_gen_2KB_buffer(AL_CONTEXT_PARAM_DECL_SINGLE)
{
   return (unsigned char *)malloc(2048);
}


void al_release_gen_2KB_buffer(AL_CONTEXT_PARAM_DECL unsigned char *buffer)
{
   free(buffer);
}


int al_print_log(AL_CONTEXT_PARAM_DECL int log_type, const char *format, ...)
{
   /*
    * va_list		args_list;
    * va_start(args_list,format);
    * return vprintf(format,args_list);
    */
   return 0;
}


static void _usage()
{
   fprintf(stdout,
           "\n"
           "usage: patcher [-v] [-h] [-k text_key] address\n"
           "-v         : for knowing the version\n"
           "-h         : for help\n"
           "-k         : specifiy text_key (cybernetics! is default)\n"
           "address	: MAC address in XX:XX:XX:XX:XX:XX format\n\n"
           );
}


static int open_meshap_conf()
{
   FILE          *fp;
   long          size;
   unsigned char *buffer;
   unsigned int           al_conf_handle;

   fp = fopen(_PATCHER_CONF_PATH_, "rb");

   if (fp == NULL)
   {
      return -1;
   }

   fseek(fp, 0, SEEK_END);
   size = ftell(fp);
   fseek(fp, 0, SEEK_SET);

   buffer = (unsigned char *)malloc(size + 1);
   memset(buffer, 0, size + 1);
   fread(buffer, 1, size, fp);

   fclose(fp);

   al_conf_handle = al_conf_parse(AL_CONTEXT buffer);

   if (al_conf_handle == 0)
   {
      free(buffer);
      fprintf(stderr, "\n %s: %d Error parsing config file\n", __func__, __LINE__);
      return -1;
   }

   free(buffer);

   return al_conf_handle;
}


static void _patch_config_file(char *text_key, unsigned char *mac_address)
{
   unsigned int               al_conf_handle;
   int               text_key_len;
   unsigned char     encoded_key[1024];
   unsigned char     decoded_key[1024];
   int               ret;
   int               i, count;
   al_conf_if_info_t if_info;

   al_conf_handle = open_meshap_conf();

   if (al_conf_handle <= 0)
   {
      fprintf(stderr, "\nmeshap.conf not found\n");
      return;
   }

   text_key_len = strlen(text_key);

   ret = al_encode_key(AL_CONTEXT(unsigned char *) text_key, text_key_len, mac_address, encoded_key);

   if (ret <= 0)
   {
      fprintf(stderr, "\nret = %d (Error in encoding)\n", ret);
      return;
   }

   ret = al_conf_set_mesh_imcp_key_buffer(AL_CONTEXT al_conf_handle, encoded_key, ret);

   if (ret != 0)
   {
      fprintf(stderr, "\nret = %d (Error in al_conf_set)\n", ret);
      return;
   }

   ret = al_conf_put(AL_CONTEXT al_conf_handle);

   if (ret != 0)
   {
      fprintf(stderr, "\nret = %d (Error in al_conf_put)\n", ret);
      return;
   }

   al_conf_close(AL_CONTEXT al_conf_handle);



   /*
    *	Verify encoded key by reading it back from file and decode it
    */

   al_conf_handle = open_meshap_conf();

   if (al_conf_handle <= 0)
   {
      fprintf(stderr, "\nmeshap.conf not found\n");
      return;
   }

   ret = al_conf_get_mesh_imcp_key_buffer(AL_CONTEXT al_conf_handle, encoded_key, 1024);

   fprintf(stderr, "\nEncoded Key : %s", encoded_key);

   memset(decoded_key, 0, 1024);

   ret = al_decode_key(AL_CONTEXT encoded_key, mac_address, decoded_key);

   if (ret <= 0)
   {
      fprintf(stderr, "ret = %d (Error in decoding)\n", ret);
      return;
   }

   fprintf(stderr, "\nDecoded key = %s\n", decoded_key);

   fprintf(stderr, "\nSetting ACKTimeout...\n");

   count = al_conf_get_if_count(al_conf_handle);
   for (i = 0; i < count; i++)
   {
      al_conf_get_if(al_conf_handle, i, &if_info);
      fprintf(stderr, "\ni=%d,%s,%d\n", i, if_info.name, if_info.phy_sub_type);
      if (if_info.phy_sub_type == AL_CONF_IF_PHY_SUB_TYPE_802_11_B)
      {
         if_info.ack_timeout = 10;
      }
      else
      {
         if_info.ack_timeout = 5;
      }

      al_conf_set_if(al_conf_handle, i, &if_info);
   }

   al_conf_put(AL_CONTEXT al_conf_handle);

   al_conf_close(AL_CONTEXT al_conf_handle);

   fprintf(stderr, "\nCreating meshap.conf backup...", ret);
   system("cp /etc/meshap.conf /etc/orig.meshap.conf");
}
