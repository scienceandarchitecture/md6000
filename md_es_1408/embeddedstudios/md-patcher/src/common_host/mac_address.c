
/********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : mac_address.c
 * Comments : Common MAC address routines
 * Created  : 11/23/2004
 * Author   : Sriram Dayanandan
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * -----------------------------------------------------------------------------
 * | No  |Date      |  Comment                                        | Author |
 * -----------------------------------------------------------------------------
 * |  0  |11/23/2004| Created.                                        | Sriram |
 * -----------------------------------------------------------------------------
 ********************************************************************************/

#include <string.h>

static int _hex_atoi(const char* string, int* value)
{
    int		c;		/* current char */
    long	total;	/* current total */
	int		err;
	char*	p;
	char	buf[32];
	
#define _ishexalpha(c)			((c >= 'A' && c <= 'F') || (c >='a' && c <= 'f'))
#define _isspace(c)				(c == '\t' || c == '\r' || c == '\n' || c == '\f' || c == ' ')
#define _isdigit(c)				(c >= 48 && c <= 48+9)
#define _ishexdigit(c)			(_isdigit(c) || _ishexalpha(c))
#define _get_hex_alpha_value(c)	((c >= 'A' && c <= 'F') ? (10 + c - 'A') : (10 + c - 'a'))
#define _get_hex_value(c)		(_isdigit(c) ? (c - '0') : _get_hex_alpha_value(c))

	err	= 0;

    /* skip whitespace */

    while ( _isspace((int)(unsigned char)*string) )
        ++string;

	strcpy(buf,string);

	p = buf;
	while(*p != 0)
		p++;
	p--;
	while(_isspace(*p)) {
		*p = 0;
		p--;
	}

	p		= buf;
    c		= (int)(unsigned char)*p++;
    total	= 0;

    while (_ishexdigit(c)) {
        total = 16 * total + _get_hex_value(c);     /* accumulate digit */
        c = (int)(unsigned char)*p++;				/* get next char */
    }

	if(c != 0) 
		err	 = -1;
	
	*value = total;   /* return result, negated if necessary */

	return err;

#undef _isspace
#undef _isdigit
#undef _ishexalpha
#undef _ishexdigit
#undef _get_hex_alpha_value
#undef _get_hex_value
}

int	mac_address_ascii_to_binary	(const char* mac_address_ascii,unsigned char* mac_address_binary)
{
	char				temp_address[256];
	const char*			seps = ":";
	char*				token;
	int					i;
	int					index;
	int					value;	

	index	= 0;
	i		= 0;
	
	strcpy(temp_address,mac_address_ascii);	
   
	token = strtok(temp_address,seps);

	while(token != NULL ) {
		_hex_atoi(token,&value); 		
		mac_address_binary[i] = value;
		token = strtok( NULL,seps);
		i++;
    }

	return 0;
}
