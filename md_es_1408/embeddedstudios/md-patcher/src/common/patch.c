/********************************************************************************
* MeshDynamics
* --------------
* File     : patch.c
* Comments : Torna Common Patch Utility Source
* Created  : 11/23/2004
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  0  |11/23/2004| Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include "patch.h"

int patch_file(const char *file_name, const unsigned char *mac_address)
{
   FILE          *fp;
   unsigned char *buffer;
   long          buffer_length;
   int           ret;

   fp = fopen(file_name, "rb");

   ret = PATCH_ERROR_READ_ACCESS_DENIED;

   if (fp == NULL)
   {
      goto _lb_ret_level0;
   }

   fseek(fp, 0, SEEK_END);
   buffer_length = ftell(fp);
   fseek(fp, 0, SEEK_SET);

   buffer = (unsigned char *)malloc(buffer_length);

   if (fread(buffer, 1, buffer_length, fp) != buffer_length)
   {
      fclose(fp);
      ret = PATCH_ERROR_GENERIC;
      goto _lb_ret_level1;
   }

   fclose(fp);

   ret = patch_buffer(buffer, buffer_length, mac_address);

   if (ret != PATCH_ERROR_SUCCESS)
   {
      goto  _lb_ret_level1;
   }

   ret = PATCH_ERROR_WRITE_ACCESS_DENIED;
   fp  = fopen(file_name, "wb");

   if (fp == NULL)
   {
      goto _lb_ret_level1;
   }

   if (fwrite(buffer, 1, buffer_length, fp) != buffer_length)
   {
      fclose(fp);
      ret = PATCH_ERROR_GENERIC;
      goto _lb_ret_level1;
   }

   ret = PATCH_ERROR_SUCCESS;

   fclose(fp);

_lb_ret_level1:
   free(buffer);
_lb_ret_level0:
   return ret;
}


#ifndef WIN32

#include <sys/time.h>
#include <time.h>

static int _random_chars(unsigned char *buf, int length)
{
   unsigned char   *p;
   struct timeval  tv;
   struct timezone tz;
   int             total;
   int             i;

   static unsigned char add[] =
   {
      80, 77, 23, 29, 16, 2, 5, 24, 78, 74
   };
   static unsigned int pos = 0;

   memset(buf, 0, length);

   for (i = 0; i < length; i++)
   {
      gettimeofday(&tv, &tz);
      total  = tv.tv_sec + tv.tv_usec;
      p      = (unsigned char *)&total;
      buf[i] = (p[0] + p[1]) + (p[2] + p[3]) + add[pos];
      pos    = (pos + 1) % sizeof(add);
      sleep(1);
   }

   return 0;
}


#else
#include <windows.h>
static int _random_chars(unsigned char *buf, int length)
{
   unsigned char *p;
   FILETIME      ft;
   int           i;

   static unsigned char add[] =
   {
      80, 77, 23, 29, 16, 2, 5, 24, 78, 74
   };
   static unsigned int pos = 0;

   memset(buf, 0, length);

   for (i = 0; i < length; i++)
   {
      GetSystemTimeAsFileTime(&ft);
      p      = (unsigned char *)&ft.dwLowDateTime;
      buf[i] = (p[0] + p[1]) + (p[2] + p[3]) + add[pos];
      pos    = (pos + 1) % sizeof(add);
      Sleep(10);
   }

   return 0;
}
#endif

int patch_buffer(unsigned char *buffer, long buffer_length, const unsigned char *mac_address)
{
   unsigned char Signature[] = { 'T', 'O', 'R', 'N', 'A', 'S', 'T', 'A', 'R', 'T' };
   int           i, j;
   unsigned char *p;

   for (i = 0; i < buffer_length; i++)
   {
      if (!memcmp(&buffer[i], Signature, sizeof(Signature)))
      {
         p = &buffer[i];
         _random_chars(p, sizeof(Signature));
         p += sizeof(Signature);
         for (j = 0; j < 6; j++)
         {
            p[j] = mac_address[j] + buffer[i + j];
         }
         return PATCH_ERROR_SUCCESS;
      }
   }

   return PATCH_ERROR_SIGNATURE_NOT_FOUND;
}
