/********************************************************************************
* MeshDynamics
* --------------
* File     : meshd.c
* Comments : Torna Mesh Daemon
* Created  : 5/20/2004
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* | 25  |8/27/2008 | Used default SIP configuration if not found     | Sriram |
* -----------------------------------------------------------------------------
* | 24  |8/22/2008 | Changes for SIP                                 |Abhijit |
* -----------------------------------------------------------------------------
* | 23  |1/22/2008 | use_virt_if setting checked for vconfigure      | Sriram |
* -----------------------------------------------------------------------------
* | 22  |01/07/2008| vconfigure command added                        | Abhijit|
* -----------------------------------------------------------------------------
* | 21  |11/21/2007| Set GPS command added                           | Sriram |
* -----------------------------------------------------------------------------
* | 20  |7/11/2007 | Full disconnect command added                   | Sriram |
* -----------------------------------------------------------------------------
* | 19  |7/9/2007  | Interval added to reset generator               | Sriram |
* -----------------------------------------------------------------------------
* | 18  |7/9/2007  | Block CPU command added                         | Sriram |
* -----------------------------------------------------------------------------
* | 17  |5/17/2007 | vlan command added                              | Sriram |
* -----------------------------------------------------------------------------
* | 16  |12/14/2006| Reset generator changes                         | Sriram |
* -----------------------------------------------------------------------------
* | 15  |5/12/2006 | Added set_reboot_flag                           | Bindu  |
* -----------------------------------------------------------------------------
* | 14  |03/13/2006| Added ACL request/response					  | Bindu  |
* -----------------------------------------------------------------------------
* | 13  |02/15/2006| Added dot11e request/response                   | Bindu  |
* -----------------------------------------------------------------------------
* | 12  |6/17/2005 | Added mesh/ap flags request                     | Sriram |
* -----------------------------------------------------------------------------
* | 11  |6/16/2005 | Added random request                            | Sriram |
* -----------------------------------------------------------------------------
* | 10  |6/15/2005 | Added kick child request                        | Sriram |
* -----------------------------------------------------------------------------
* |  9  |5/20/2005 | Preferred parent request added                  | Sriram |
* -----------------------------------------------------------------------------
* |  8  |4/15/2005 | Added meshap.conf auto recovery                 | Sriram |
* -----------------------------------------------------------------------------
* |  7  |4/14/2005 | Added test131 and test141                       | Sriram |
* -----------------------------------------------------------------------------
* |  6  |4/2/2005  | Added test121                                   | Sriram |
* -----------------------------------------------------------------------------
* |  5  |12/26/2004| Added reboot request                            | Sriram |
* -----------------------------------------------------------------------------
* |  4  |12/15/2004| Added get_ds_if operation                       | Sriram |
* -----------------------------------------------------------------------------
* |  3  |7/22/2004 | Added test111                                   | Sriram |
* -----------------------------------------------------------------------------
* |  2  |6/10/2004 | Added test101                                   | Sriram |
* -----------------------------------------------------------------------------
* |  1  |6/5/2004  | Misc changes                                    | Sriram |
* -----------------------------------------------------------------------------
* |  0  |5/20/2004 | Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

/**
 * Currently meshd is not a daemon in the real sense,
 * it basically is invoked during startup to pass on the
 * configuration to the meshap.o kernel module
 */

#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <getopt.h>
#include <string.h>
#include <strings.h>
#include <stdlib.h>

#include "torna_ddi.h"
#include "mac_address.h"

#include "al.h"
#include "al_context.h"
#include "al_impl_context.h"
#include "al_conf.h"
static void _usage();
static int _start(int argc, char **argv, const char *operation, const char *config_file_path);
static int _stop(int argc, char **argv, const char *operation, const char *config_file_path);
static int _halt(int argc, char **argv, const char *operation, const char *config_file_path);
static int _configure(int argc, char **argv, const char *operation, const char *config_file_path);
int init_configurations(int al_conf_handle, int dot11e_conf_handle, int flag, int start_hostapd);

#ifdef _MESHD_AL_TEST_SUITE_
static int _test11(int argc, char **argv, const char *operation, const char *config_file_path);
static int _test12(int argc, char **argv, const char *operation, const char *config_file_path);
static int _test21(int argc, char **argv, const char *operation, const char *config_file_path);
static int _test22(int argc, char **argv, const char *operation, const char *config_file_path);
static int _test31(int argc, char **argv, const char *operation, const char *config_file_path);
static int _test32(int argc, char **argv, const char *operation, const char *config_file_path);
static int _test41(int argc, char **argv, const char *operation, const char *config_file_path);
static int _test51(int argc, char **argv, const char *operation, const char *config_file_path);
static int _test61(int argc, char **argv, const char *operation, const char *config_file_path);
static int _test71(int argc, char **argv, const char *operation, const char *config_file_path);
static int _test81(int argc, char **argv, const char *operation, const char *config_file_path);
static int _test91(int argc, char **argv, const char *operation, const char *config_file_path);
static int _test101(int argc, char **argv, const char *operation, const char *config_file_path);
static int _test111(int argc, char **argv, const char *operation, const char *config_file_path);
static int _test121(int argc, char **argv, const char *operation, const char *config_file_path);
static int _test131(int argc, char **argv, const char *operation, const char *config_file_path);
static int _test141(int argc, char **argv, const char *operation, const char *config_file_path);
#endif /* _MESHD_AL_TEST_SUITE_ */

static int _get_ds_netif(int argc, char **argv, const char *operation, const char *config_file_path);
static int _reboot(int argc, char **argv, const char *operation, const char *config_file_path);
static int _preferred_parent(int argc, char **argv, const char *operation, const char *config_file_path);
static int _kick_child(int argc, char **argv, const char *operation, const char *config_file_path);
static int _random(int argc, char **argv, const char *operation, const char *config_file_path);
static int _mesh_flags(int argc, char **argv, const char *operation, const char *config_file_path);
static int _ap_flags(int argc, char **argv, const char *operation, const char *config_file_path);
static int _config_sqnr(int argc, char **argv, const char *operation, const char *config_file_path);
static int _configure_dot11e(int argc, char **argv, const char *operation, const char *config_file_path);
static int _configure_acl(int argc, char **argv, const char *operation, const char *config_file_path);
static int _configure_mobility(int argc, char **argv, const char *operation, const char *config_file_path);
static int _lock_reboot(int argc, char **argv, const char *operation, const char *config_file_path);
static int _set_reboot_flag(int argc, char **argv, const char *operation, const char *config_file_path);
static int _enable_reset_gen(int argc, char **argv, const char *operation, const char *config_file_path);
static int _vlan(int argc, char **argv, const char *operation, const char *config_file_path);
static int _block_cpu(int argc, char **argv, const char *operation, const char *config_file_path);
static int _full_disconnect(int argc, char **argv, const char *operation, const char *config_file_path);
static int _dhcp_request(int argc, char **argv, const char *operation, const char *config_file_path);
static int _gpio(int argc, char **argv, const char *operation, const char *config_file_path);
static int _set_gps(int argc, char **argv, const char *operation, const char *config_file_path);
static int _vconfigure(int argc, char **argv, const char *operation, const char *config_file_path);
static int _configure_sip(int argc, char **argv, const char *operation, const char *config_file_path);
static int _eth_link_status(int argc, char **argv, const char *operation, const char *config_file_path);
static int _query_board_stats(int argc, char **argv, const char *operation, const char *config_file_path); //SPAWAR

static struct
{
   const char *operation;
   int        (*function)(int argc, char **argv, const char *operation, const char *config_file_path);
}
_operations[] =
{
   { "start",              _start              },
   { "stop",               _stop               },
   { "configure",          _configure          },
#ifdef _MESHD_AL_TEST_SUITE_
   { "test11",             _test11             },
   { "test12",             _test12             },
   { "test21",             _test21             },
   { "test22",             _test22             },
   { "test31",             _test31             },
   { "test32",             _test32             },
   { "test41",             _test41             },
   { "test51",             _test51             },
   { "test61",             _test61             },
   { "test71",             _test71             },
   { "test81",             _test81             },
   { "test91",             _test91             },
   { "test101",            _test101            },
   { "test111",            _test111            },
   { "test121",            _test121            },
   { "test131",            _test131            },
   { "test141",            _test141            },
#endif /* _MESHD_AL_TEST_SUITE_ */
   { "get_ds_if",          _get_ds_netif       },
   { "reboot",             _reboot             },
   { "pref",               _preferred_parent   },
   { "kick",               _kick_child         },
   { "random",             _random             },
   { "mesh_flags",         _mesh_flags         },
   { "ap_flags",           _ap_flags           },
   { "config_sqnr",        _config_sqnr        },
   { "configure_dot11e",   _configure_dot11e   },
   { "configure_acl",      _configure_acl      },
   { "configure_mobility", _configure_mobility },
   { "lock_reboot",        _lock_reboot        },
   { "set_reboot_flag",    _set_reboot_flag    }, // Set reboot flag = true
   { "enable_reset_gen",   _enable_reset_gen   },
   { "vlan",               _vlan               },
   { "block_cpu",          _block_cpu          },
   { "full_disconnect",    _full_disconnect    },
   { "dhcp_info",          _dhcp_request       },
   { "gpio",               _gpio               },
   { "set_gps",            _set_gps            },
   { "vconfigure",         _vconfigure         },
   { "configure_sip",      _configure_sip      },
//SPAWAR
   { "eth_link_status",    _eth_link_status    },
   { "query_board_stats",   _query_board_stats },
//SPAWAR_END
	{ "halt",					_halt					  }
};

#define _DEFAULT_CONF_FILE_PATH        "/etc/meshap.conf"
#define _DEFAULT_11E_CONF_FILE_PATH    "/etc/dot11e.conf"
#define _DEFAULT_ACL_CONF_PATH         "/etc/acl.conf"
#define _DEFAULT_MOBILITY_CONF_PATH    "/etc/mobility.conf"
#define _DEFAULT_MAP_IF_CONF_PATH      "/etc/map_if_info.conf"
#define _DEFAULT_SIP_CONF_PATH         "/etc/sip.conf"

int main(int argc, char **argv)
{
   char config_file_path[256];
   char operation[64];
   int  ch;
   int  i;
   
   strcpy(config_file_path, _DEFAULT_CONF_FILE_PATH);
   *operation = 0;

   while ((ch = getopt(argc, argv, "vhf:")) != EOF)
   {
      switch (ch)
      {
      case 'v':
         fprintf(stdout,
                 "\nMeshDynamics Mesh Daemon version %s\n"
                 "Copyright (c) 1992-2004 Advanced Cybernetics Group, Inc\n"
                 "All rights reserved.\n\n",
                 _TORNA_VERSION_STRING_);
         return 0;

      case 'h':
         _usage();
         return 0;

      case 'f':
         strcpy(config_file_path, optarg);
         break;
      }
   }

   if (optind >= argc)
   {
      fprintf(stderr, "\nNo operation specified\n");
      _usage();
      return -1;
   }

   strcpy(operation, argv[optind]);

   for (i = 0; i < sizeof(_operations) / sizeof(*_operations); i++)
   {
      if (!strcmp(operation, _operations[i].operation))
      {
         return _operations[i].function(argc, argv, operation, config_file_path);
      }
   }

   fprintf(stderr, "\nUnknown operation %s\n", operation);
   _usage();

   return -1;
}


static void _usage()
{
   fprintf(stdout,
           "\n"
           "usage: meshd [-v] [-h] [-f configfile] operation\n"
           "-v        : for knowing the version\n"
           "-h        : for help\n"
           "-f        : specifiy configfile (/etc/meshap.conf is default)\n"
           "operation : {start,stop,configure,get_ds_if,reboot,pref,kick,random,mesh_flags,ap_flags,config_sqnr,configure_dot11e,configure_acl,configure_mobility,lock_reboot,set_reboot_flag,enable_reset_gen,vlan, block_cpu, full_disconnect,dhcp_info,gpio, set_gps, vconfigure, configure_sip}"
#ifdef _MESHD_AL_TEST_SUITE_
           "\ntests     : {test11,test12,test21,test22,test31,test32,test41,test51,test61,test71,test81,test91,test101,test111,test121,test131,test141}\n\n"
#else
           "\n\n"
#endif
           );
}


static int _start(int argc, char **argv, const char *operation, const char *config_file_path)
{
   int                        fd;
   tddi_packet_t              packet;
   int                        ret;
   char                       line[50];
   int                        al_conf_handle;
   int                        dot11e_conf_handle;
   FILE                       *cmd;
   unsigned char              buffer[sizeof(tddi_packet_t) + sizeof(tddi_start_mesh_response_t)];
   tddi_packet_t              *response_packet;
   tddi_start_mesh_response_t *response_data;

   unsigned char buffer1[512];

   fd = open(TDDI_FILE_NAME, O_RDWR);

   if (fd == -1)
   {
      fprintf(stderr, "Error opening "TDDI_FILE_NAME "\n");
      return -1;
   }

   packet.signature = TDDI_PACKET_SIGNATURE;
   packet.version   = TDDI_CURRENT_VERSION;
   packet.data_size = 0;
   packet.type      = TDDI_PACKET_TYPE_REQUEST;
   packet.sub_type  = TDDI_REQUEST_TYPE_START_MESH;

   ret = write(fd, &packet, sizeof(tddi_packet_t));

   if (ret != sizeof(tddi_packet_t))
   {
      fprintf(stderr, "Error writing to "TDDI_FILE_NAME " ret=%d\n", ret);
      return -1;
   }

   lseek(fd, 0, SEEK_CUR);

   ret = read(fd, buffer, sizeof(buffer));

   if (ret != sizeof(buffer))
   {
      fprintf(stderr, "Error reading from "TDDI_FILE_NAME " ret=%d,errno=%d\n", ret, errno);
      switch (errno)
      {
      case EINTR:
         fprintf(stderr, "EINTR occurred\n");
         break;

      case EAGAIN:
         fprintf(stderr, "EAGAIN occurred\n");
         break;

      case EIO:
         fprintf(stderr, "EIO occurred\n");
         break;

      case EBADF:
         fprintf(stderr, "EBADF occurred\n");
         break;

      case EINVAL:
         fprintf(stderr, "EINVAL occurred\n");
         break;

      case EFAULT:
         fprintf(stderr, "EFAULT occurred\n");
         break;
      }
      return -1;
   }

   close(fd);

   response_packet = (tddi_packet_t *)buffer;
   response_data   = (tddi_start_mesh_response_t *)(buffer + sizeof(tddi_packet_t));

   switch (response_data->response)
   {
   case TDDI_START_MESH_RESPONSE_SUCCESS:
      fprintf(stderr, "Start mesh successful\n");
      break;

   case TDDI_START_MESH_RESPONSE_NO_CONFIG:
      fprintf(stderr, "Not configured\n");
      break;

   case TDDI_START_MESH_RESPONSE_ALREADY_STARTED:
      fprintf(stderr, "Already started\n");
      break;
   }
#if 0
   al_conf_handle = libalconf_read_config_data();

   if (al_conf_handle == -1)
   {
      printf("\nCould not read configuration");
      return 0;
   }


   dot11e_conf_handle = read_dot11e_category_data();

   if (dot11e_conf_handle == -1)
   {
      printf("\nCould not read dot11e configuration");
      return 0;
   }



   cmd = popen("pidof hostapd", "r");
   fgets(line, 50, cmd);
//	pid_t pid  = strtoul(line, NULL,10);

   sprintf(buffer1, "kill -9 %s", line);
   printf("SIGHUP=%s\n", buffer1);
   pclose(cmd);

   init_configurations(al_conf_handle, dot11e_conf_handle, 0);
#endif

   return 0;
}

static int _halt(int argc, char **argv, const char *operation, const char *config_file_path)
{
	int                       fd;
	tddi_packet_t             packet;
	int                       ret;
	tddi_halt_mesh_response_t *response_data;
	unsigned char             buffer[sizeof(tddi_packet_t) + sizeof(tddi_halt_mesh_response_t)];
	tddi_packet_t             *response_packet;

	fd = open(TDDI_FILE_NAME, O_RDWR);
	if (fd == -1)
	{
		fprintf(stderr, "Error opening "TDDI_FILE_NAME "\n");
		return -1;
	}

	packet.signature = TDDI_PACKET_SIGNATURE;
	packet.version   = TDDI_CURRENT_VERSION;
	packet.data_size = 0;
	packet.type      = TDDI_PACKET_TYPE_REQUEST;
	packet.sub_type  = TDDI_REQUEST_TYPE_HALT_MESH;

	ret = write(fd, &packet, sizeof(tddi_packet_t));
	if (ret != sizeof(tddi_packet_t))
	{
		fprintf(stderr, "Error writing to "TDDI_FILE_NAME " ret=%d\n", ret);
		close(fd);
		return -1;
	}
	
	lseek(fd, 0, SEEK_CUR);
	
	ret = read(fd, buffer, sizeof(buffer));
	if (ret != sizeof(buffer))
	{
		close(fd);
		fprintf(stderr, "Error reading from "TDDI_FILE_NAME " ret=%d,errno=%d\n", ret, errno);
		switch (errno)
		{
		case EINTR:
	   	fprintf(stderr, "EINTR occurred\n");
	   	break;

		case EAGAIN:
			fprintf(stderr, "EAGAIN occurred\n");
			break;

		case EIO:
			fprintf(stderr, "EIO occurred\n");
			break;

		case EBADF:
			fprintf(stderr, "EBADF occurred\n");
			break;

		case EINVAL:
			fprintf(stderr, "EINVAL occurred\n");
			break;

		case EFAULT:
			fprintf(stderr, "EFAULT occurred\n");
			break;
		}
		return -1;
	}

	close(fd);

	response_packet = (tddi_packet_t *)buffer;
	response_data   = (tddi_halt_mesh_response_t *)(buffer + sizeof(tddi_packet_t));

	switch (response_data->response)
	{
	case TDDI_HALT_MESH_RESPONSE_SUCCESS:
		fprintf(stderr, "Halt mesh successful\n");
		break;	
	default:
		fprintf(stderr, "Failed to halt mesh\n");
	}

	return 0;
}

static int _stop(int argc, char **argv, const char *operation, const char *config_file_path)
{
   int                       fd;
   tddi_packet_t             packet;
   int                       ret;
   unsigned char             buffer[sizeof(tddi_packet_t) + sizeof(tddi_stop_mesh_response_t)];
   tddi_packet_t             *response_packet;
   tddi_stop_mesh_response_t *response_data;

   fd = open(TDDI_FILE_NAME, O_RDWR);

   if (fd == -1)
   {
      fprintf(stderr, "Error opening "TDDI_FILE_NAME "\n");
      return -1;
   }

   packet.signature = TDDI_PACKET_SIGNATURE;
   packet.version   = TDDI_CURRENT_VERSION;
   packet.data_size = 0;
   packet.type      = TDDI_PACKET_TYPE_REQUEST;
   packet.sub_type  = TDDI_REQUEST_TYPE_STOP_MESH;

   ret = write(fd, &packet, sizeof(tddi_packet_t));

   if (ret != sizeof(tddi_packet_t))
   {
      fprintf(stderr, "Error writing to "TDDI_FILE_NAME " ret=%d\n", ret);
      return -1;
   }

   lseek(fd, 0, SEEK_CUR);

   ret = read(fd, buffer, sizeof(buffer));

   if (ret != sizeof(buffer))
   {
      fprintf(stderr, "Error reading from "TDDI_FILE_NAME " ret=%d,errno=%d\n", ret, errno);
      switch (errno)
      {
      case EINTR:
         fprintf(stderr, "EINTR occurred\n");
         break;

      case EAGAIN:
         fprintf(stderr, "EAGAIN occurred\n");
         break;

      case EIO:
         fprintf(stderr, "EIO occurred\n");
         break;

      case EBADF:
         fprintf(stderr, "EBADF occurred\n");
         break;

      case EINVAL:
         fprintf(stderr, "EINVAL occurred\n");
         break;

      case EFAULT:
         fprintf(stderr, "EFAULT occurred\n");
         break;
      }
      return -1;
   }

   close(fd);

   response_packet = (tddi_packet_t *)buffer;
   response_data   = (tddi_stop_mesh_response_t *)(buffer + sizeof(tddi_packet_t));

   switch (response_data->response)
   {
   case TDDI_STOP_MESH_RESPONSE_SUCCESS:
      fprintf(stderr, "Stop mesh successful\n");
      break;

   case TDDI_STOP_MESH_RESPONSE_NOT_STARTED:
      fprintf(stderr, "Not started\n");
      break;

   case TDDI_START_MESH_RESPONSE_ALREADY_STARTED:
      fprintf(stderr, "Already started\n");
      break;
   }


   return 0;
}


static int _configure(int argc, char **argv, const char *operation, const char *config_file_path)
{
   int           fd;
   tddi_packet_t *packet;
   tddi_configure_mesh_request_t *request;
   int           ret;
   off_t         offset;
   unsigned char *request_packet;
   unsigned char *request_data;
   unsigned char response_buffer[sizeof(tddi_packet_t) + sizeof(tddi_configure_mesh_response_t)];
   tddi_packet_t *response_packet;
   tddi_configure_mesh_response_t *response_data;

   /**
    * First find out the length of the data
    */


   fd = open(config_file_path, O_RDONLY);

   if (fd == -1)
   {
      fprintf(stderr, "Error opening %s for reading\n", config_file_path);
      return -1;
   }

   offset         = lseek(fd, 0, SEEK_END);
   request_packet = (unsigned char *)malloc(sizeof(tddi_packet_t) + sizeof(tddi_configure_mesh_request_t) + offset);
   packet         = (tddi_packet_t *)request_packet;
   request        = (tddi_configure_mesh_request_t *)(request_packet + sizeof(tddi_packet_t));
   request_data   = request_packet + sizeof(tddi_packet_t) + sizeof(tddi_configure_mesh_request_t);

   lseek(fd, 0, SEEK_SET);

   read(fd, request_data, offset);

   close(fd);

#if 0
   al_conf_handle = al_conf_parse(AL_CONTEXT file_data);

   if (al_conf_handle == -1)
   {
      printf("Failed to parse the file\n");
   }
#endif

   packet->version   = TDDI_CURRENT_VERSION;
   packet->signature = TDDI_PACKET_SIGNATURE;
   packet->type      = TDDI_PACKET_TYPE_REQUEST;
   packet->data_size = sizeof(tddi_configure_mesh_request_t) + offset;
   packet->sub_type  = TDDI_REQUEST_TYPE_CONFIGURE_MESH;

   request->config_string_length = offset;

   fd = open(TDDI_FILE_NAME, O_RDWR);

   if (fd == -1)
   {
      fprintf(stderr, "Error opening "TDDI_FILE_NAME "\n");
      free(request_packet);
      return -1;
   }

   ret = write(fd, request_packet, sizeof(tddi_packet_t) + sizeof(tddi_configure_mesh_request_t) + offset);

   if (ret != sizeof(tddi_packet_t) + sizeof(tddi_configure_mesh_request_t) + offset)
   {
      fprintf(stderr, "Error writing to "TDDI_FILE_NAME " ret=%d\n", ret);
      free(request_packet);
      return -1;
   }


   lseek(fd, 0, SEEK_CUR);

   ret = read(fd, response_buffer, sizeof(response_buffer));

   if (ret != sizeof(response_buffer))
   {
      fprintf(stderr, "meshd : _configure : Error reading from "TDDI_FILE_NAME " ret=%d,errno=%d\n", ret, errno);
      switch (errno)
      {
      case EINTR:
         fprintf(stderr, "EINTR occurred\n");
         break;

      case EAGAIN:
         fprintf(stderr, "EAGAIN occurred\n");
         break;

      case EIO:
         fprintf(stderr, "EIO occurred\n");
         break;

      case EBADF:
         fprintf(stderr, "EBADF occurred\n");
         break;

      case EINVAL:
         fprintf(stderr, "EINVAL occurred\n");
         break;

      case EFAULT:
         fprintf(stderr, "EFAULT occurred\n");
         break;
      }
      free(request_packet);
      fprintf(stderr, "meshd : Restoring original meshap.conf and rebooting...");
      system("cp /etc/orig.meshap.conf /etc/meshap.conf");
      system("/root/post_fs_change.sh config");
      close(fd);
      _reboot(argc, argv, operation, config_file_path);
      return -1;
   }

   free(request_packet);
   close(fd);

   response_packet = (tddi_packet_t *)response_buffer;
   response_data   = (tddi_configure_mesh_response_t *)(response_buffer + sizeof(tddi_packet_t));

   switch (response_data->response)
   {
   case TDDI_CONFIGURE_MESH_RESPONSE_SUCCESS:
      fprintf(stderr, "Configure mesh successful\n");
      break;

   case TDDI_CONFIGURE_MESH_RESPONSE_BUSY:
      fprintf(stderr, "Mesh busy\n");
      break;
   }

   return 0;
}


/*
 * Configuration of 802.11e Categories
 */
static int _configure_dot11e(int argc, char **argv, const char *operation, const char *config_file_path)
{
   char          dot11e_config_file_path[256];
   int           fd;
   tddi_packet_t *packet;
   tddi_configure_dot11e_request_t *request;
   int           ret;
   off_t         offset;
   unsigned char *request_packet;
   unsigned char *request_data;
   unsigned char response_buffer[sizeof(tddi_packet_t) + sizeof(tddi_configure_dot11e_response_t)];
   tddi_packet_t *response_packet;
   tddi_configure_dot11e_response_t *response_data;

   strcpy(dot11e_config_file_path, _DEFAULT_11E_CONF_FILE_PATH);

   /**
    * First find out the length of the data
    */

   fd = open(dot11e_config_file_path, O_RDONLY);

   if (fd == -1)
   {
      fprintf(stderr, "Error opening %s for reading\n", dot11e_config_file_path);
      return -1;
   }

   offset         = lseek(fd, 0, SEEK_END);
   request_packet = (unsigned char *)malloc(sizeof(tddi_packet_t) + sizeof(tddi_configure_dot11e_request_t) + offset);
   packet         = (tddi_packet_t *)request_packet;
   request        = (tddi_configure_dot11e_request_t *)(request_packet + sizeof(tddi_packet_t));
   request_data   = request_packet + sizeof(tddi_packet_t) + sizeof(tddi_configure_dot11e_request_t);

   lseek(fd, 0, SEEK_SET);

   read(fd, request_data, offset);

   close(fd);

   packet->version   = TDDI_CURRENT_VERSION;
   packet->signature = TDDI_PACKET_SIGNATURE;
   packet->type      = TDDI_PACKET_TYPE_REQUEST;
   packet->data_size = sizeof(tddi_configure_dot11e_request_t) + offset;
   packet->sub_type  = TDDI_REQUEST_TYPE_CONFIGURE_DOT11E;

   request->config_string_length = offset;

   fd = open(TDDI_FILE_NAME, O_RDWR);

   if (fd == -1)
   {
      fprintf(stderr, "Error opening "TDDI_FILE_NAME "\n");
      free(request_packet);
      return -1;
   }

   ret = write(fd, request_packet, sizeof(tddi_packet_t) + sizeof(tddi_configure_dot11e_request_t) + offset);

   if (ret != sizeof(tddi_packet_t) + sizeof(tddi_configure_dot11e_request_t) + offset)
   {
      fprintf(stderr, "Error writing to "TDDI_FILE_NAME " ret=%d\n", ret);
      free(request_packet);
      return -1;
   }

   lseek(fd, 0, SEEK_CUR);

   ret = read(fd, response_buffer, sizeof(response_buffer));

   if (ret != sizeof(response_buffer))
   {
      fprintf(stderr, "meshd : _configure_dot11e : Error reading from "TDDI_FILE_NAME " ret=%d,errno=%d\n", ret, errno);
      switch (errno)
      {
      case EINTR:
         fprintf(stderr, "EINTR occurred\n");
         break;

      case EAGAIN:
         fprintf(stderr, "EAGAIN occurred\n");
         break;

      case EIO:
         fprintf(stderr, "EIO occurred\n");
         break;

      case EBADF:
         fprintf(stderr, "EBADF occurred\n");
         break;

      case EINVAL:
         fprintf(stderr, "EINVAL occurred\n");
         break;

      case EFAULT:
         fprintf(stderr, "EFAULT occurred\n");
         break;
      }
      free(request_packet);
      close(fd);
      return -1;
   }

   free(request_packet);
   close(fd);

   response_packet = (tddi_packet_t *)response_buffer;
   response_data   = (tddi_configure_dot11e_response_t *)(response_buffer + sizeof(tddi_packet_t));

   switch (response_data->response)
   {
   case TDDI_CONFIGURE_DOT11E_RESPONSE_SUCCESS:
      fprintf(stderr, "Configure MESH(802.11e parameters) Successful\n");
      break;

   case TDDI_CONFIGURE_DOT11E_RESPONSE_BUSY:
      fprintf(stderr, "MESH(802.11e parameters) busy\n");
      break;
   }

   return 0;
}


static int _configure_acl(int argc, char **argv, const char *operation, const char *config_file_path)
{
   char                          acl_config_file_path[256];
   int                           fd;
   tddi_packet_t                 *packet;
   tddi_configure_acl_request_t  *request;
   int                           ret;
   off_t                         offset;
   unsigned char                 *request_packet;
   unsigned char                 *request_data;
   unsigned char                 response_buffer[sizeof(tddi_packet_t) + sizeof(tddi_configure_acl_response_t)];
   tddi_packet_t                 *response_packet;
   tddi_configure_acl_response_t *response_data;

   strcpy(acl_config_file_path, _DEFAULT_ACL_CONF_PATH);

   /**
    * First find out the length of the data
    */

   fd = open(acl_config_file_path, O_RDONLY);

   if (fd == -1)
   {
      fprintf(stderr, "Error opening %s for reading\n", acl_config_file_path);
      return -1;
   }

   offset         = lseek(fd, 0, SEEK_END);
   request_packet = (unsigned char *)malloc(sizeof(tddi_packet_t) + sizeof(tddi_configure_acl_request_t) + offset);
   packet         = (tddi_packet_t *)request_packet;
   request        = (tddi_configure_acl_request_t *)(request_packet + sizeof(tddi_packet_t));
   request_data   = request_packet + sizeof(tddi_packet_t) + sizeof(tddi_configure_acl_request_t);

   lseek(fd, 0, SEEK_SET);

   read(fd, request_data, offset);

   close(fd);

   packet->version   = TDDI_CURRENT_VERSION;
   packet->signature = TDDI_PACKET_SIGNATURE;
   packet->type      = TDDI_PACKET_TYPE_REQUEST;
   packet->data_size = sizeof(tddi_configure_acl_request_t) + offset;
   packet->sub_type  = TDDI_REQUEST_TYPE_CONFIGURE_ACL;

   request->config_string_length = offset;

   fd = open(TDDI_FILE_NAME, O_RDWR);

   if (fd == -1)
   {
      fprintf(stderr, "Error opening "TDDI_FILE_NAME "\n");
      free(request_packet);
      return -1;
   }

   ret = write(fd, request_packet, sizeof(tddi_packet_t) + sizeof(tddi_configure_acl_request_t) + offset);

   if (ret != sizeof(tddi_packet_t) + sizeof(tddi_configure_acl_request_t) + offset)
   {
      fprintf(stderr, "Error writing to "TDDI_FILE_NAME " ret=%d\n", ret);
      free(request_packet);
      return -1;
   }

   lseek(fd, 0, SEEK_CUR);

   ret = read(fd, response_buffer, sizeof(response_buffer));

   if (ret != sizeof(response_buffer))
   {
      fprintf(stderr, "meshd : _configure_acl : Error reading from "TDDI_FILE_NAME " ret=%d,errno=%d\n", ret, errno);
      switch (errno)
      {
      case EINTR:
         fprintf(stderr, "EINTR occurred\n");
         break;

      case EAGAIN:
         fprintf(stderr, "EAGAIN occurred\n");
         break;

      case EIO:
         fprintf(stderr, "EIO occurred\n");
         break;

      case EBADF:
         fprintf(stderr, "EBADF occurred\n");
         break;

      case EINVAL:
         fprintf(stderr, "EINVAL occurred\n");
         break;

      case EFAULT:
         fprintf(stderr, "EFAULT occurred\n");
         break;
      }
      free(request_packet);
      close(fd);
      return -1;
   }

   free(request_packet);
   close(fd);

   response_packet = (tddi_packet_t *)response_buffer;
   response_data   = (tddi_configure_acl_response_t *)(response_buffer + sizeof(tddi_packet_t));

   switch (response_data->response)
   {
   case TDDI_CONFIGURE_ACL_RESPONSE_SUCCESS:
      fprintf(stderr, "Configure MESH(ACL parameters) Successful\n");
      break;

   case TDDI_CONFIGURE_ACL_RESPONSE_BUSY:
      fprintf(stderr, "MESH(ACL parameters) busy\n");
      break;
   }

   return 0;
}


#ifdef _MESHD_AL_TEST_SUITE_

static int _test11(int argc, char **argv, const char *operation, const char *config_file_path)
{
   int           fd;
   tddi_packet_t packet;
   int           ret;
   unsigned char buffer[sizeof(tddi_packet_t) + sizeof(tddi_test_response_t)];

   fd = open(TDDI_FILE_NAME, O_RDWR);

   if (fd == -1)
   {
      fprintf(stderr, "Error opening "TDDI_FILE_NAME "\n");
      return -1;
   }

   packet.signature = TDDI_PACKET_SIGNATURE;
   packet.version   = TDDI_CURRENT_VERSION;
   packet.data_size = 0;
   packet.type      = TDDI_PACKET_TYPE_REQUEST;
   packet.sub_type  = TDDI_REQUEST_TYPE_TEST_1_1;

   ret = write(fd, &packet, sizeof(tddi_packet_t));

   if (ret != sizeof(tddi_packet_t))
   {
      fprintf(stderr, "Error writing to "TDDI_FILE_NAME " ret=%d\n", ret);
      return -1;
   }

   lseek(fd, 0, SEEK_CUR);

   ret = read(fd, buffer, sizeof(buffer));

   if (ret != sizeof(buffer))
   {
      fprintf(stderr, "Error reading from "TDDI_FILE_NAME " ret=%d,errno=%d\n", ret, errno);
      switch (errno)
      {
      case EINTR:
         fprintf(stderr, "EINTR occurred\n");
         break;

      case EAGAIN:
         fprintf(stderr, "EAGAIN occurred\n");
         break;

      case EIO:
         fprintf(stderr, "EIO occurred\n");
         break;

      case EBADF:
         fprintf(stderr, "EBADF occurred\n");
         break;

      case EINVAL:
         fprintf(stderr, "EINVAL occurred\n");
         break;

      case EFAULT:
         fprintf(stderr, "EFAULT occurred\n");
         break;
      }
      return -1;
   }

   close(fd);

   return 0;
}


static int _test12(int argc, char **argv, const char *operation, const char *config_file_path)
{
   int           fd;
   tddi_packet_t packet;
   int           ret;
   unsigned char buffer[sizeof(tddi_packet_t) + sizeof(tddi_test_response_t)];

   fd = open(TDDI_FILE_NAME, O_RDWR);

   if (fd == -1)
   {
      fprintf(stderr, "Error opening "TDDI_FILE_NAME "\n");
      return -1;
   }

   packet.signature = TDDI_PACKET_SIGNATURE;
   packet.version   = TDDI_CURRENT_VERSION;
   packet.data_size = 0;
   packet.type      = TDDI_PACKET_TYPE_REQUEST;
   packet.sub_type  = TDDI_REQUEST_TYPE_TEST_1_2;

   ret = write(fd, &packet, sizeof(tddi_packet_t));

   if (ret != sizeof(tddi_packet_t))
   {
      fprintf(stderr, "Error writing to "TDDI_FILE_NAME " ret=%d,errno=%d\n", ret, errno);
      return -1;
   }

   lseek(fd, 0, SEEK_CUR);

   ret = read(fd, buffer, sizeof(buffer));

   if (ret != sizeof(buffer))
   {
      fprintf(stderr, "Error reading from "TDDI_FILE_NAME " ret=%d\n", ret);
      return -1;
   }

   close(fd);

   return 0;
}


static int _test21(int argc, char **argv, const char *operation, const char *config_file_path)
{
   int           fd;
   tddi_packet_t packet;
   int           ret;
   unsigned char buffer[sizeof(tddi_packet_t) + sizeof(tddi_test_response_t)];

   fd = open(TDDI_FILE_NAME, O_RDWR);

   if (fd == -1)
   {
      fprintf(stderr, "Error opening "TDDI_FILE_NAME "\n");
      return -1;
   }

   packet.signature = TDDI_PACKET_SIGNATURE;
   packet.version   = TDDI_CURRENT_VERSION;
   packet.data_size = 0;
   packet.type      = TDDI_PACKET_TYPE_REQUEST;
   packet.sub_type  = TDDI_REQUEST_TYPE_TEST_2_1;

   ret = write(fd, &packet, sizeof(tddi_packet_t));

   if (ret != sizeof(tddi_packet_t))
   {
      fprintf(stderr, "Error writing to "TDDI_FILE_NAME " ret=%d\n", ret);
      return -1;
   }

   lseek(fd, 0, SEEK_CUR);

   ret = read(fd, buffer, sizeof(buffer));

   if (ret != sizeof(buffer))
   {
      fprintf(stderr, "Error reading from "TDDI_FILE_NAME " ret=%d,errno=%d\n", ret, errno);
      switch (errno)
      {
      case EINTR:
         fprintf(stderr, "EINTR occurred\n");
         break;

      case EAGAIN:
         fprintf(stderr, "EAGAIN occurred\n");
         break;

      case EIO:
         fprintf(stderr, "EIO occurred\n");
         break;

      case EBADF:
         fprintf(stderr, "EBADF occurred\n");
         break;

      case EINVAL:
         fprintf(stderr, "EINVAL occurred\n");
         break;

      case EFAULT:
         fprintf(stderr, "EFAULT occurred\n");
         break;
      }
      return -1;
   }

   close(fd);

   return 0;
}


static int _test22(int argc, char **argv, const char *operation, const char *config_file_path)
{
   int           fd;
   tddi_packet_t packet;
   int           ret;
   unsigned char buffer[sizeof(tddi_packet_t) + sizeof(tddi_test_response_t)];

   fd = open(TDDI_FILE_NAME, O_RDWR);

   if (fd == -1)
   {
      fprintf(stderr, "Error opening "TDDI_FILE_NAME "\n");
      return -1;
   }

   packet.signature = TDDI_PACKET_SIGNATURE;
   packet.version   = TDDI_CURRENT_VERSION;
   packet.data_size = 0;
   packet.type      = TDDI_PACKET_TYPE_REQUEST;
   packet.sub_type  = TDDI_REQUEST_TYPE_TEST_2_2;

   ret = write(fd, &packet, sizeof(tddi_packet_t));

   if (ret != sizeof(tddi_packet_t))
   {
      fprintf(stderr, "Error writing to "TDDI_FILE_NAME " ret=%d,errno=%d\n", ret, errno);
      return -1;
   }

   lseek(fd, 0, SEEK_CUR);

   ret = read(fd, buffer, sizeof(buffer));

   if (ret != sizeof(buffer))
   {
      fprintf(stderr, "Error reading from "TDDI_FILE_NAME " ret=%d\n", ret);
      return -1;
   }

   close(fd);

   return 0;
}


static int _test31(int argc, char **argv, const char *operation, const char *config_file_path)
{
   int           fd;
   tddi_packet_t packet;
   int           ret;
   unsigned char buffer[sizeof(tddi_packet_t) + sizeof(tddi_test_response_t)];

   fd = open(TDDI_FILE_NAME, O_RDWR);

   if (fd == -1)
   {
      fprintf(stderr, "Error opening "TDDI_FILE_NAME "\n");
      return -1;
   }

   packet.signature = TDDI_PACKET_SIGNATURE;
   packet.version   = TDDI_CURRENT_VERSION;
   packet.data_size = 0;
   packet.type      = TDDI_PACKET_TYPE_REQUEST;
   packet.sub_type  = TDDI_REQUEST_TYPE_TEST_3_1;

   ret = write(fd, &packet, sizeof(tddi_packet_t));

   if (ret != sizeof(tddi_packet_t))
   {
      fprintf(stderr, "Error writing to "TDDI_FILE_NAME " ret=%d\n", ret);
      return -1;
   }

   lseek(fd, 0, SEEK_CUR);

   ret = read(fd, buffer, sizeof(buffer));

   if (ret != sizeof(buffer))
   {
      fprintf(stderr, "Error reading from "TDDI_FILE_NAME " ret=%d,errno=%d\n", ret, errno);
      switch (errno)
      {
      case EINTR:
         fprintf(stderr, "EINTR occurred\n");
         break;

      case EAGAIN:
         fprintf(stderr, "EAGAIN occurred\n");
         break;

      case EIO:
         fprintf(stderr, "EIO occurred\n");
         break;

      case EBADF:
         fprintf(stderr, "EBADF occurred\n");
         break;

      case EINVAL:
         fprintf(stderr, "EINVAL occurred\n");
         break;

      case EFAULT:
         fprintf(stderr, "EFAULT occurred\n");
         break;
      }
      return -1;
   }

   close(fd);

   return 0;
}


static int _test32(int argc, char **argv, const char *operation, const char *config_file_path)
{
   int           fd;
   tddi_packet_t packet;
   int           ret;
   unsigned char buffer[sizeof(tddi_packet_t) + sizeof(tddi_test_response_t)];

   fd = open(TDDI_FILE_NAME, O_RDWR);

   if (fd == -1)
   {
      fprintf(stderr, "Error opening "TDDI_FILE_NAME "\n");
      return -1;
   }

   packet.signature = TDDI_PACKET_SIGNATURE;
   packet.version   = TDDI_CURRENT_VERSION;
   packet.data_size = 0;
   packet.type      = TDDI_PACKET_TYPE_REQUEST;
   packet.sub_type  = TDDI_REQUEST_TYPE_TEST_3_2;

   ret = write(fd, &packet, sizeof(tddi_packet_t));

   if (ret != sizeof(tddi_packet_t))
   {
      fprintf(stderr, "Error writing to "TDDI_FILE_NAME " ret=%d,errno=%d\n", ret, errno);
      return -1;
   }

   lseek(fd, 0, SEEK_CUR);

   ret = read(fd, buffer, sizeof(buffer));

   if (ret != sizeof(buffer))
   {
      fprintf(stderr, "Error reading from "TDDI_FILE_NAME " ret=%d\n", ret);
      return -1;
   }

   close(fd);

   return 0;
}


static int _test41(int argc, char **argv, const char *operation, const char *config_file_path)
{
   int           fd;
   tddi_packet_t packet;
   int           ret;
   unsigned char buffer[sizeof(tddi_packet_t) + sizeof(tddi_test_response_t)];

   fd = open(TDDI_FILE_NAME, O_RDWR);

   if (fd == -1)
   {
      fprintf(stderr, "Error opening "TDDI_FILE_NAME "\n");
      return -1;
   }

   packet.signature = TDDI_PACKET_SIGNATURE;
   packet.version   = TDDI_CURRENT_VERSION;
   packet.data_size = 0;
   packet.type      = TDDI_PACKET_TYPE_REQUEST;
   packet.sub_type  = TDDI_REQUEST_TYPE_TEST_4_1;

   ret = write(fd, &packet, sizeof(tddi_packet_t));

   if (ret != sizeof(tddi_packet_t))
   {
      fprintf(stderr, "Error writing to "TDDI_FILE_NAME " ret=%d,errno=%d\n", ret, errno);
      return -1;
   }

   lseek(fd, 0, SEEK_CUR);

   ret = read(fd, buffer, sizeof(buffer));

   if (ret != sizeof(buffer))
   {
      fprintf(stderr, "Error reading from "TDDI_FILE_NAME " ret=%d\n", ret);
      return -1;
   }

   close(fd);

   return 0;
}


static int _test51(int argc, char **argv, const char *operation, const char *config_file_path)
{
   int           fd;
   tddi_packet_t packet;
   int           ret;
   unsigned char buffer[sizeof(tddi_packet_t) + sizeof(tddi_test_response_t)];

   fd = open(TDDI_FILE_NAME, O_RDWR);

   if (fd == -1)
   {
      fprintf(stderr, "Error opening "TDDI_FILE_NAME "\n");
      return -1;
   }

   packet.signature = TDDI_PACKET_SIGNATURE;
   packet.version   = TDDI_CURRENT_VERSION;
   packet.data_size = 0;
   packet.type      = TDDI_PACKET_TYPE_REQUEST;
   packet.sub_type  = TDDI_REQUEST_TYPE_TEST_5_1;

   ret = write(fd, &packet, sizeof(tddi_packet_t));

   if (ret != sizeof(tddi_packet_t))
   {
      fprintf(stderr, "Error writing to "TDDI_FILE_NAME " ret=%d,errno=%d\n", ret, errno);
      return -1;
   }

   lseek(fd, 0, SEEK_CUR);

   ret = read(fd, buffer, sizeof(buffer));

   if (ret != sizeof(buffer))
   {
      fprintf(stderr, "Error reading from "TDDI_FILE_NAME " ret=%d\n", ret);
      return -1;
   }

   close(fd);

   return 0;
}


static int _test61(int argc, char **argv, const char *operation, const char *config_file_path)
{
   int           fd;
   tddi_packet_t packet;
   int           ret;
   unsigned char buffer[sizeof(tddi_packet_t) + sizeof(tddi_test_response_t)];

   fd = open(TDDI_FILE_NAME, O_RDWR);

   if (fd == -1)
   {
      fprintf(stderr, "Error opening "TDDI_FILE_NAME "\n");
      return -1;
   }

   packet.signature = TDDI_PACKET_SIGNATURE;
   packet.version   = TDDI_CURRENT_VERSION;
   packet.data_size = 0;
   packet.type      = TDDI_PACKET_TYPE_REQUEST;
   packet.sub_type  = TDDI_REQUEST_TYPE_TEST_6_1;

   ret = write(fd, &packet, sizeof(tddi_packet_t));

   if (ret != sizeof(tddi_packet_t))
   {
      fprintf(stderr, "Error writing to "TDDI_FILE_NAME " ret=%d,errno=%d\n", ret, errno);
      return -1;
   }

   lseek(fd, 0, SEEK_CUR);

   ret = read(fd, buffer, sizeof(buffer));

   if (ret != sizeof(buffer))
   {
      fprintf(stderr, "Error reading from "TDDI_FILE_NAME " ret=%d\n", ret);
      return -1;
   }

   close(fd);

   return 0;
}


static int _test71(int argc, char **argv, const char *operation, const char *config_file_path)
{
   int           fd;
   tddi_packet_t packet;
   int           ret;
   unsigned char buffer[sizeof(tddi_packet_t) + sizeof(tddi_test_response_t)];

   fd = open(TDDI_FILE_NAME, O_RDWR);

   if (fd == -1)
   {
      fprintf(stderr, "Error opening "TDDI_FILE_NAME "\n");
      return -1;
   }

   packet.signature = TDDI_PACKET_SIGNATURE;
   packet.version   = TDDI_CURRENT_VERSION;
   packet.data_size = 0;
   packet.type      = TDDI_PACKET_TYPE_REQUEST;
   packet.sub_type  = TDDI_REQUEST_TYPE_TEST_7_1;

   ret = write(fd, &packet, sizeof(tddi_packet_t));

   if (ret != sizeof(tddi_packet_t))
   {
      fprintf(stderr, "Error writing to "TDDI_FILE_NAME " ret=%d,errno=%d\n", ret, errno);
      return -1;
   }

   lseek(fd, 0, SEEK_CUR);

   ret = read(fd, buffer, sizeof(buffer));

   if (ret != sizeof(buffer))
   {
      fprintf(stderr, "Error reading from "TDDI_FILE_NAME " ret=%d\n", ret);
      return -1;
   }

   close(fd);

   return 0;
}


static int _test81(int argc, char **argv, const char *operation, const char *config_file_path)
{
   int           fd;
   tddi_packet_t packet;
   int           ret;
   unsigned char buffer[sizeof(tddi_packet_t) + sizeof(tddi_test_response_t)];

   fd = open(TDDI_FILE_NAME, O_RDWR);

   if (fd == -1)
   {
      fprintf(stderr, "Error opening "TDDI_FILE_NAME "\n");
      return -1;
   }

   packet.signature = TDDI_PACKET_SIGNATURE;
   packet.version   = TDDI_CURRENT_VERSION;
   packet.data_size = 0;
   packet.type      = TDDI_PACKET_TYPE_REQUEST;
   packet.sub_type  = TDDI_REQUEST_TYPE_TEST_8_1;

   ret = write(fd, &packet, sizeof(tddi_packet_t));

   if (ret != sizeof(tddi_packet_t))
   {
      fprintf(stderr, "Error writing to "TDDI_FILE_NAME " ret=%d,errno=%d\n", ret, errno);
      return -1;
   }

   lseek(fd, 0, SEEK_CUR);

   ret = read(fd, buffer, sizeof(buffer));

   if (ret != sizeof(buffer))
   {
      fprintf(stderr, "Error reading from "TDDI_FILE_NAME " ret=%d\n", ret);
      return -1;
   }

   close(fd);

   return 0;
}


static int _test91(int argc, char **argv, const char *operation, const char *config_file_path)
{
   int           fd;
   tddi_packet_t packet;
   int           ret;
   unsigned char buffer[sizeof(tddi_packet_t) + sizeof(tddi_test_response_t)];

   fd = open(TDDI_FILE_NAME, O_RDWR);

   if (fd == -1)
   {
      fprintf(stderr, "Error opening "TDDI_FILE_NAME "\n");
      return -1;
   }

   packet.signature = TDDI_PACKET_SIGNATURE;
   packet.version   = TDDI_CURRENT_VERSION;
   packet.data_size = 0;
   packet.type      = TDDI_PACKET_TYPE_REQUEST;
   packet.sub_type  = TDDI_REQUEST_TYPE_TEST_9_1;

   ret = write(fd, &packet, sizeof(tddi_packet_t));

   if (ret != sizeof(tddi_packet_t))
   {
      fprintf(stderr, "Error writing to "TDDI_FILE_NAME " ret=%d,errno=%d\n", ret, errno);
      return -1;
   }

   lseek(fd, 0, SEEK_CUR);

   ret = read(fd, buffer, sizeof(buffer));

   if (ret != sizeof(buffer))
   {
      fprintf(stderr, "Error reading from "TDDI_FILE_NAME " ret=%d\n", ret);
      return -1;
   }

   close(fd);

   return 0;
}


static int _test101(int argc, char **argv, const char *operation, const char *config_file_path)
{
   int           fd;
   tddi_packet_t packet;
   int           ret;
   unsigned char buffer[sizeof(tddi_packet_t) + sizeof(tddi_test_response_t)];

   fd = open(TDDI_FILE_NAME, O_RDWR);

   if (fd == -1)
   {
      fprintf(stderr, "Error opening "TDDI_FILE_NAME "\n");
      return -1;
   }

   packet.signature = TDDI_PACKET_SIGNATURE;
   packet.version   = TDDI_CURRENT_VERSION;
   packet.data_size = 0;
   packet.type      = TDDI_PACKET_TYPE_REQUEST;
   packet.sub_type  = TDDI_REQUEST_TYPE_TEST_10_1;

   ret = write(fd, &packet, sizeof(tddi_packet_t));

   if (ret != sizeof(tddi_packet_t))
   {
      fprintf(stderr, "Error writing to "TDDI_FILE_NAME " ret=%d,errno=%d\n", ret, errno);
      return -1;
   }

   lseek(fd, 0, SEEK_CUR);

   ret = read(fd, buffer, sizeof(buffer));

   if (ret != sizeof(buffer))
   {
      fprintf(stderr, "Error reading from "TDDI_FILE_NAME " ret=%d\n", ret);
      return -1;
   }

   close(fd);

   return 0;
}


static int _test111(int argc, char **argv, const char *operation, const char *config_file_path)
{
   int           fd;
   tddi_packet_t packet;
   int           ret;
   unsigned char buffer[sizeof(tddi_packet_t) + sizeof(tddi_test_response_t)];

   fd = open(TDDI_FILE_NAME, O_RDWR);

   if (fd == -1)
   {
      fprintf(stderr, "Error opening "TDDI_FILE_NAME "\n");
      return -1;
   }

   packet.signature = TDDI_PACKET_SIGNATURE;
   packet.version   = TDDI_CURRENT_VERSION;
   packet.data_size = 0;
   packet.type      = TDDI_PACKET_TYPE_REQUEST;
   packet.sub_type  = TDDI_REQUEST_TYPE_TEST_11_1;

   ret = write(fd, &packet, sizeof(tddi_packet_t));

   if (ret != sizeof(tddi_packet_t))
   {
      fprintf(stderr, "Error writing to "TDDI_FILE_NAME " ret=%d,errno=%d\n", ret, errno);
      return -1;
   }

   lseek(fd, 0, SEEK_CUR);

   ret = read(fd, buffer, sizeof(buffer));

   if (ret != sizeof(buffer))
   {
      fprintf(stderr, "Error reading from "TDDI_FILE_NAME " ret=%d\n", ret);
      return -1;
   }

   close(fd);

   return 0;
}


static int _test121(int argc, char **argv, const char *operation, const char *config_file_path)
{
   int           fd;
   tddi_packet_t packet;
   int           ret;
   unsigned char buffer[sizeof(tddi_packet_t) + sizeof(tddi_test_response_t)];

   fd = open(TDDI_FILE_NAME, O_RDWR);

   if (fd == -1)
   {
      fprintf(stderr, "Error opening "TDDI_FILE_NAME "\n");
      return -1;
   }

   packet.signature = TDDI_PACKET_SIGNATURE;
   packet.version   = TDDI_CURRENT_VERSION;
   packet.data_size = 0;
   packet.type      = TDDI_PACKET_TYPE_REQUEST;
   packet.sub_type  = TDDI_REQUEST_TYPE_TEST_12_1;

   ret = write(fd, &packet, sizeof(tddi_packet_t));

   if (ret != sizeof(tddi_packet_t))
   {
      fprintf(stderr, "Error writing to "TDDI_FILE_NAME " ret=%d,errno=%d\n", ret, errno);
      return -1;
   }

   lseek(fd, 0, SEEK_CUR);

   ret = read(fd, buffer, sizeof(buffer));

   if (ret != sizeof(buffer))
   {
      fprintf(stderr, "Error reading from "TDDI_FILE_NAME " ret=%d\n", ret);
      return -1;
   }

   close(fd);

   return 0;
}


static int _test131(int argc, char **argv, const char *operation, const char *config_file_path)
{
   int           fd;
   tddi_packet_t packet;
   int           ret;
   unsigned char buffer[sizeof(tddi_packet_t) + sizeof(tddi_test_response_t)];

   fd = open(TDDI_FILE_NAME, O_RDWR);

   if (fd == -1)
   {
      fprintf(stderr, "Error opening "TDDI_FILE_NAME "\n");
      return -1;
   }

   packet.signature = TDDI_PACKET_SIGNATURE;
   packet.version   = TDDI_CURRENT_VERSION;
   packet.data_size = 0;
   packet.type      = TDDI_PACKET_TYPE_REQUEST;
   packet.sub_type  = TDDI_REQUEST_TYPE_TEST_13_1;

   ret = write(fd, &packet, sizeof(tddi_packet_t));

   if (ret != sizeof(tddi_packet_t))
   {
      fprintf(stderr, "Error writing to "TDDI_FILE_NAME " ret=%d,errno=%d\n", ret, errno);
      return -1;
   }

   lseek(fd, 0, SEEK_CUR);

   ret = read(fd, buffer, sizeof(buffer));

   if (ret != sizeof(buffer))
   {
      fprintf(stderr, "Error reading from "TDDI_FILE_NAME " ret=%d\n", ret);
      return -1;
   }

   close(fd);

   return 0;
}


static int _test141(int argc, char **argv, const char *operation, const char *config_file_path)
{
   int           fd;
   tddi_packet_t packet;
   int           ret;
   unsigned char buffer[sizeof(tddi_packet_t) + sizeof(tddi_test_response_t)];

   fd = open(TDDI_FILE_NAME, O_RDWR);

   if (fd == -1)
   {
      fprintf(stderr, "Error opening "TDDI_FILE_NAME "\n");
      return -1;
   }

   packet.signature = TDDI_PACKET_SIGNATURE;
   packet.version   = TDDI_CURRENT_VERSION;
   packet.data_size = 0;
   packet.type      = TDDI_PACKET_TYPE_REQUEST;
   packet.sub_type  = TDDI_REQUEST_TYPE_TEST_14_1;

   ret = write(fd, &packet, sizeof(tddi_packet_t));

   if (ret != sizeof(tddi_packet_t))
   {
      fprintf(stderr, "Error writing to "TDDI_FILE_NAME " ret=%d,errno=%d\n", ret, errno);
      return -1;
   }

   lseek(fd, 0, SEEK_CUR);

   ret = read(fd, buffer, sizeof(buffer));

   if (ret != sizeof(buffer))
   {
      fprintf(stderr, "Error reading from "TDDI_FILE_NAME " ret=%d\n", ret);
      return -1;
   }

   close(fd);

   return 0;
}
#endif /* _MESHD_AL_TEST_SUITE_*/

static int _get_ds_netif(int argc, char **argv, const char *operation, const char *config_file_path)
{
   int                       fd;
   tddi_packet_t             packet;
   int                       ret;
   unsigned char             buffer[sizeof(tddi_packet_t) + sizeof(tddi_get_ds_if_response_t) + 128];
   tddi_packet_t             *response_packet;
   tddi_get_ds_if_response_t *response_data;
   char                      name[129];

   fd = open(TDDI_FILE_NAME, O_RDWR);

   if (fd == -1)
   {
      fprintf(stderr, "Error opening "TDDI_FILE_NAME "\n");
      return -1;
   }

   packet.signature = TDDI_PACKET_SIGNATURE;
   packet.version   = TDDI_CURRENT_VERSION;
   packet.data_size = 0;
   packet.type      = TDDI_PACKET_TYPE_REQUEST;
   packet.sub_type  = TDDI_REQUEST_TYPE_GET_DS_IF;

   ret = write(fd, &packet, sizeof(tddi_packet_t));

   if (ret != sizeof(tddi_packet_t))
   {
      fprintf(stderr, "Error writing to "TDDI_FILE_NAME " ret=%d\n", ret);
      return -1;
   }

   lseek(fd, 0, SEEK_CUR);

   ret = read(fd, buffer, sizeof(buffer));

   if (ret < sizeof(tddi_packet_t) + sizeof(tddi_get_ds_if_response_t))
   {
      fprintf(stderr, "Error reading from "TDDI_FILE_NAME " ret=%d,errno=%d\n", ret, errno);
      switch (errno)
      {
      case EINTR:
         fprintf(stderr, "EINTR occurred\n");
         break;

      case EAGAIN:
         fprintf(stderr, "EAGAIN occurred\n");
         break;

      case EIO:
         fprintf(stderr, "EIO occurred\n");
         break;

      case EBADF:
         fprintf(stderr, "EBADF occurred\n");
         break;

      case EINVAL:
         fprintf(stderr, "EINVAL occurred\n");
         break;

      case EFAULT:
         fprintf(stderr, "EFAULT occurred\n");
         break;
      }
      return -1;
   }

   close(fd);

   response_packet = (tddi_packet_t *)buffer;
   response_data   = (tddi_get_ds_if_response_t *)(buffer + sizeof(tddi_packet_t));

   switch (response_data->response)
   {
   case TDDI_GET_DS_IF_STARTED:
      memset(name, 0, 129);
      memcpy(name, buffer + sizeof(tddi_packet_t) + sizeof(tddi_get_ds_if_response_t), response_data->if_name_length);
      fprintf(stderr, "DS net_if is %s\n", name);
      break;

   case TDDI_GET_DS_IF_RESPONSE_SCANNING:
      fprintf(stderr, "Scan in progress\n");
      break;

   case TDDI_GET_DS_IF_RESPONSE_NOT_STARTED:
      fprintf(stderr, "Not started\n");
      break;
   }

   return 0;
}


static int _reboot(int argc, char **argv, const char *operation, const char *config_file_path)
{
   int           fd;
   tddi_packet_t packet;
   int           ret;
   unsigned char buffer[sizeof(tddi_packet_t) + sizeof(tddi_reboot_response_t)];

   fd = open(TDDI_FILE_NAME, O_RDWR);

   if (fd == -1)
   {
      fprintf(stderr, "Error opening "TDDI_FILE_NAME "\n");
      return -1;
   }

   packet.signature = TDDI_PACKET_SIGNATURE;
   packet.version   = TDDI_CURRENT_VERSION;
   packet.data_size = 0;
   packet.type      = TDDI_PACKET_TYPE_REQUEST;
   packet.sub_type  = TDDI_REQUEST_TYPE_REBOOT;

   ret = write(fd, &packet, sizeof(tddi_packet_t));

   if (ret != sizeof(tddi_packet_t))
   {
      fprintf(stderr, "Error writing to "TDDI_FILE_NAME " ret=%d\n", ret);
      return -1;
   }

   lseek(fd, 0, SEEK_CUR);

   ret = read(fd, buffer, sizeof(buffer));

   if (ret != sizeof(buffer))
   {
      fprintf(stderr, "Error reading from "TDDI_FILE_NAME " ret=%d,errno=%d\n", ret, errno);
      switch (errno)
      {
      case EINTR:
         fprintf(stderr, "EINTR occurred\n");
         break;

      case EAGAIN:
         fprintf(stderr, "EAGAIN occurred\n");
         break;

      case EIO:
         fprintf(stderr, "EIO occurred\n");
         break;

      case EBADF:
         fprintf(stderr, "EBADF occurred\n");
         break;

      case EINVAL:
         fprintf(stderr, "EINVAL occurred\n");
         break;

      case EFAULT:
         fprintf(stderr, "EFAULT occurred\n");
         break;
      }
      return -1;
   }

   close(fd);

   return 0;
}


static int _preferred_parent(int argc, char **argv, const char *operation, const char *config_file_path)
{
   int           fd;
   tddi_packet_t *packet;
   int           ret;
   tddi_preferred_parent_request_t  *request;
   tddi_preferred_parent_response_t *response;
   unsigned char buffer[sizeof(tddi_packet_t) + sizeof(tddi_preferred_parent_request_t)];

   if (argc < 3)
   {
      fprintf(stderr, "usage: meshd pref enable [mac_address]\n");
      return -1;
   }

   fd = open(TDDI_FILE_NAME, O_RDWR);

   if (fd == -1)
   {
      fprintf(stderr, "Error opening "TDDI_FILE_NAME "\n");
      return -1;
   }

   packet  = (tddi_packet_t *)buffer;
   request = (tddi_preferred_parent_request_t *)(buffer + sizeof(tddi_packet_t));

   memset(request, 0, sizeof(tddi_preferred_parent_request_t));

   packet->signature = TDDI_PACKET_SIGNATURE;
   packet->version   = TDDI_CURRENT_VERSION;
   packet->data_size = sizeof(tddi_preferred_parent_request_t);
   packet->type      = TDDI_PACKET_TYPE_REQUEST;
   packet->sub_type  = TDDI_REQUEST_TYPE_PREFERRED_PARENT;

   request->enabled = atoi(argv[2]);

   if (request->enabled && (argc < 4))
   {
      fprintf(stderr, "usage: meshd pref 1 mac_address\n");
      return -1;
   }

   mac_address_ascii_to_binary(argv[3], request->preferred_parent);

   ret = write(fd, buffer, sizeof(tddi_packet_t) + sizeof(tddi_preferred_parent_request_t));

   if (ret != sizeof(tddi_packet_t) + sizeof(tddi_preferred_parent_request_t))
   {
      fprintf(stderr, "Error writing to "TDDI_FILE_NAME " ret=%d\n", ret);
      return -1;
   }

   lseek(fd, 0, SEEK_CUR);

   ret = read(fd, buffer, sizeof(tddi_packet_t) + sizeof(tddi_preferred_parent_response_t));

   if (ret != sizeof(tddi_packet_t) + sizeof(tddi_preferred_parent_response_t))
   {
      fprintf(stderr, "Error reading from "TDDI_FILE_NAME " ret=%d,errno=%d\n", ret, errno);
      switch (errno)
      {
      case EINTR:
         fprintf(stderr, "EINTR occurred\n");
         break;

      case EAGAIN:
         fprintf(stderr, "EAGAIN occurred\n");
         break;

      case EIO:
         fprintf(stderr, "EIO occurred\n");
         break;

      case EBADF:
         fprintf(stderr, "EBADF occurred\n");
         break;

      case EINVAL:
         fprintf(stderr, "EINVAL occurred\n");
         break;

      case EFAULT:
         fprintf(stderr, "EFAULT occurred\n");
         break;
      }
      return -1;
   }

   response = (tddi_preferred_parent_response_t *)(buffer + sizeof(tddi_packet_t));

   switch (response->response)
   {
   case TDDI_PREFERRED_PARENT_RESPONSE_ACCEPTED:
      fprintf(stderr, "Accepted\n");
      break;

   case TDDI_PREFERRED_PARENT_RESPONSE_NOT_STARTED:
      fprintf(stderr, "Not started\n");
      break;

   case TDDI_PREFERRED_PARENT_RESPONSE_NOT_FOUND:
      fprintf(stderr, "Not found\n");
      break;

   case TDDI_PREFERRED_PARENT_NOT_APPLICABLE:
      fprintf(stderr, "Not applicable\n");
      break;
   }

   close(fd);

   return 0;
}


static int _kick_child(int argc, char **argv, const char *operation, const char *config_file_path)
{
   int                        fd;
   tddi_packet_t              *packet;
   int                        ret;
   tddi_kick_child_request_t  *request;
   tddi_kick_child_response_t *response;
   unsigned char              buffer[sizeof(tddi_packet_t) + sizeof(tddi_kick_child_request_t)];

   if (argc < 3)
   {
      fprintf(stderr, "usage: meshd kick mac_address\n");
      return -1;
   }

   fd = open(TDDI_FILE_NAME, O_RDWR);

   if (fd == -1)
   {
      fprintf(stderr, "Error opening "TDDI_FILE_NAME "\n");
      return -1;
   }

   packet  = (tddi_packet_t *)buffer;
   request = (tddi_kick_child_request_t *)(buffer + sizeof(tddi_packet_t));

   memset(request, 0, sizeof(tddi_kick_child_request_t));

   packet->signature = TDDI_PACKET_SIGNATURE;
   packet->version   = TDDI_CURRENT_VERSION;
   packet->data_size = sizeof(tddi_kick_child_request_t);
   packet->type      = TDDI_PACKET_TYPE_REQUEST;
   packet->sub_type  = TDDI_REQUEST_TYPE_KICK_CHILD;

   mac_address_ascii_to_binary(argv[2], request->address);

   ret = write(fd, buffer, sizeof(tddi_packet_t) + sizeof(tddi_kick_child_request_t));

   if (ret != sizeof(tddi_packet_t) + sizeof(tddi_kick_child_request_t))
   {
      fprintf(stderr, "Error writing to "TDDI_FILE_NAME " ret=%d\n", ret);
      return -1;
   }

   lseek(fd, 0, SEEK_CUR);

   ret = read(fd, buffer, sizeof(tddi_packet_t) + sizeof(tddi_kick_child_response_t));

   if (ret != sizeof(tddi_packet_t) + sizeof(tddi_kick_child_response_t))
   {
      fprintf(stderr, "Error reading from "TDDI_FILE_NAME " ret=%d,errno=%d\n", ret, errno);
      switch (errno)
      {
      case EINTR:
         fprintf(stderr, "EINTR occurred\n");
         break;

      case EAGAIN:
         fprintf(stderr, "EAGAIN occurred\n");
         break;

      case EIO:
         fprintf(stderr, "EIO occurred\n");
         break;

      case EBADF:
         fprintf(stderr, "EBADF occurred\n");
         break;

      case EINVAL:
         fprintf(stderr, "EINVAL occurred\n");
         break;

      case EFAULT:
         fprintf(stderr, "EFAULT occurred\n");
         break;
      }
      return -1;
   }

   response = (tddi_kick_child_response_t *)(buffer + sizeof(tddi_packet_t));

   switch (response->response)
   {
   case TDDI_KICK_CHILD_RESPONSE_KICKED:
      fprintf(stderr, "Kicked\n");
      break;

   case TDDI_KICK_CHILD_RESPONSE_NOT_STARTED:
      fprintf(stderr, "Not started\n");
      break;

   case TDDI_KICK_CHILD_RESPONSE_NOT_FOUND:
      fprintf(stderr, "Not found\n");
      break;
   }

   close(fd);

   return 0;
}


static int _random(int argc, char **argv, const char *operation, const char *config_file_path)
{
   int                    fd;
   tddi_packet_t          *packet;
   int                    ret;
   tddi_random_request_t  *request;
   tddi_random_response_t *response;
   unsigned char          buffer[sizeof(tddi_packet_t) + sizeof(tddi_random_request_t)];

   if (argc < 4)
   {
      fprintf(stderr, "usage: meshd random min max\n");
      return -1;
   }

   fd = open(TDDI_FILE_NAME, O_RDWR);

   if (fd == -1)
   {
      fprintf(stderr, "Error opening "TDDI_FILE_NAME "\n");
      return -1;
   }

   packet  = (tddi_packet_t *)buffer;
   request = (tddi_random_request_t *)(buffer + sizeof(tddi_packet_t));

   memset(request, 0, sizeof(tddi_random_request_t));

   packet->signature = TDDI_PACKET_SIGNATURE;
   packet->version   = TDDI_CURRENT_VERSION;
   packet->data_size = sizeof(tddi_random_request_t);
   packet->type      = TDDI_PACKET_TYPE_REQUEST;
   packet->sub_type  = TDDI_REQUEST_TYPE_RANDOM;

   request->min = atoi(argv[2]);
   request->max = atoi(argv[3]);

   ret = write(fd, buffer, sizeof(tddi_packet_t) + sizeof(tddi_random_request_t));

   if (ret != sizeof(tddi_packet_t) + sizeof(tddi_random_request_t))
   {
      fprintf(stderr, "Error writing to "TDDI_FILE_NAME " ret=%d\n", ret);
      return -1;
   }

   lseek(fd, 0, SEEK_CUR);

   ret = read(fd, buffer, sizeof(tddi_packet_t) + sizeof(tddi_random_response_t));

   if (ret != sizeof(tddi_packet_t) + sizeof(tddi_random_response_t))
   {
      fprintf(stderr, "Error reading from "TDDI_FILE_NAME " ret=%d,errno=%d\n", ret, errno);
      switch (errno)
      {
      case EINTR:
         fprintf(stderr, "EINTR occurred\n");
         break;

      case EAGAIN:
         fprintf(stderr, "EAGAIN occurred\n");
         break;

      case EIO:
         fprintf(stderr, "EIO occurred\n");
         break;

      case EBADF:
         fprintf(stderr, "EBADF occurred\n");
         break;

      case EINVAL:
         fprintf(stderr, "EINVAL occurred\n");
         break;

      case EFAULT:
         fprintf(stderr, "EFAULT occurred\n");
         break;
      }
      return -1;
   }

   response = (tddi_random_response_t *)(buffer + sizeof(tddi_packet_t));

   fprintf(stderr, "Random is %d\n", response->random);

   close(fd);

   return 0;
}


static int _mesh_flags(int argc, char **argv, const char *operation, const char *config_file_path)
{
   int                        fd;
   tddi_packet_t              *packet;
   int                        ret;
   tddi_mesh_flags_request_t  *request;
   tddi_mesh_flags_response_t *response;
   unsigned char              buffer[sizeof(tddi_packet_t) + sizeof(tddi_mesh_flags_request_t)];

   if (argc < 3)
   {
      fprintf(stderr, "usage: meshd mesh_flags flags\n");
      return -1;
   }

   fd = open(TDDI_FILE_NAME, O_RDWR);

   if (fd == -1)
   {
      fprintf(stderr, "Error opening "TDDI_FILE_NAME "\n");
      return -1;
   }

   packet  = (tddi_packet_t *)buffer;
   request = (tddi_mesh_flags_request_t *)(buffer + sizeof(tddi_packet_t));

   memset(request, 0, sizeof(tddi_mesh_flags_request_t));

   packet->signature = TDDI_PACKET_SIGNATURE;
   packet->version   = TDDI_CURRENT_VERSION;
   packet->data_size = sizeof(tddi_mesh_flags_request_t);
   packet->type      = TDDI_PACKET_TYPE_REQUEST;
   packet->sub_type  = TDDI_REQUEST_TYPE_MESH_FLAGS;

   request->flags = atoi(argv[2]);

   ret = write(fd, buffer, sizeof(tddi_packet_t) + sizeof(tddi_mesh_flags_request_t));

   if (ret != sizeof(tddi_packet_t) + sizeof(tddi_mesh_flags_request_t))
   {
      fprintf(stderr, "Error writing to "TDDI_FILE_NAME " ret=%d\n", ret);
      return -1;
   }

   lseek(fd, 0, SEEK_CUR);

   ret = read(fd, buffer, sizeof(tddi_packet_t) + sizeof(tddi_mesh_flags_response_t));

   if (ret != sizeof(tddi_packet_t) + sizeof(tddi_mesh_flags_response_t))
   {
      fprintf(stderr, "Error reading from "TDDI_FILE_NAME " ret=%d,errno=%d\n", ret, errno);
      switch (errno)
      {
      case EINTR:
         fprintf(stderr, "EINTR occurred\n");
         break;

      case EAGAIN:
         fprintf(stderr, "EAGAIN occurred\n");
         break;

      case EIO:
         fprintf(stderr, "EIO occurred\n");
         break;

      case EBADF:
         fprintf(stderr, "EBADF occurred\n");
         break;

      case EINVAL:
         fprintf(stderr, "EINVAL occurred\n");
         break;

      case EFAULT:
         fprintf(stderr, "EFAULT occurred\n");
         break;
      }
      return -1;
   }

   close(fd);

   return 0;
}


static int _ap_flags(int argc, char **argv, const char *operation, const char *config_file_path)
{
   int                      fd;
   tddi_packet_t            *packet;
   int                      ret;
   tddi_ap_flags_request_t  *request;
   tddi_ap_flags_response_t *response;
   unsigned char            buffer[sizeof(tddi_packet_t) + sizeof(tddi_ap_flags_request_t)];

   if (argc < 3)
   {
      fprintf(stderr, "usage: meshd ap_flags flags\n");
      return -1;
   }

   fd = open(TDDI_FILE_NAME, O_RDWR);

   if (fd == -1)
   {
      fprintf(stderr, "Error opening "TDDI_FILE_NAME "\n");
      return -1;
   }

   packet  = (tddi_packet_t *)buffer;
   request = (tddi_ap_flags_request_t *)(buffer + sizeof(tddi_packet_t));

   memset(request, 0, sizeof(tddi_ap_flags_request_t));

   packet->signature = TDDI_PACKET_SIGNATURE;
   packet->version   = TDDI_CURRENT_VERSION;
   packet->data_size = sizeof(tddi_ap_flags_request_t);
   packet->type      = TDDI_PACKET_TYPE_REQUEST;
   packet->sub_type  = TDDI_REQUEST_TYPE_AP_FLAGS;

   request->flags = atoi(argv[2]);

   ret = write(fd, buffer, sizeof(tddi_packet_t) + sizeof(tddi_ap_flags_request_t));

   if (ret != sizeof(tddi_packet_t) + sizeof(tddi_ap_flags_request_t))
   {
      fprintf(stderr, "Error writing to "TDDI_FILE_NAME " ret=%d\n", ret);
      return -1;
   }

   lseek(fd, 0, SEEK_CUR);

   ret = read(fd, buffer, sizeof(tddi_packet_t) + sizeof(tddi_ap_flags_response_t));

   if (ret != sizeof(tddi_packet_t) + sizeof(tddi_ap_flags_response_t))
   {
      fprintf(stderr, "Error reading from "TDDI_FILE_NAME " ret=%d,errno=%d\n", ret, errno);
      switch (errno)
      {
      case EINTR:
         fprintf(stderr, "EINTR occurred\n");
         break;

      case EAGAIN:
         fprintf(stderr, "EAGAIN occurred\n");
         break;

      case EIO:
         fprintf(stderr, "EIO occurred\n");
         break;

      case EBADF:
         fprintf(stderr, "EBADF occurred\n");
         break;

      case EINVAL:
         fprintf(stderr, "EINVAL occurred\n");
         break;

      case EFAULT:
         fprintf(stderr, "EFAULT occurred\n");
         break;
      }
      return -1;
   }

   close(fd);

   return 0;
}


static int _config_sqnr(int argc, char **argv, const char *operation, const char *config_file_path)
{
   int           fd;
   tddi_packet_t *packet;
   int           ret;
   tddi_mesh_config_sqnr_request_t  *request;
   tddi_mesh_config_sqnr_response_t *response;
   unsigned char buffer[sizeof(tddi_packet_t) + sizeof(tddi_mesh_config_sqnr_request_t)];

   if (argc < 3)
   {
      fprintf(stderr, "usage: meshd config_sqnr sqnr\n");
      return -1;
   }

   fd = open(TDDI_FILE_NAME, O_RDWR);

   if (fd == -1)
   {
      fprintf(stderr, "Error opening "TDDI_FILE_NAME "\n");
      return -1;
   }

   packet  = (tddi_packet_t *)buffer;
   request = (tddi_mesh_config_sqnr_request_t *)(buffer + sizeof(tddi_packet_t));

   memset(request, 0, sizeof(tddi_mesh_config_sqnr_request_t));

   packet->signature = TDDI_PACKET_SIGNATURE;
   packet->version   = TDDI_CURRENT_VERSION;
   packet->data_size = sizeof(tddi_mesh_config_sqnr_request_t);
   packet->type      = TDDI_PACKET_TYPE_REQUEST;
   packet->sub_type  = TDDI_REQUEST_TYPE_MESH_CONFIG_SQNR;

   request->sqnr = atoi(argv[2]);

   ret = write(fd, buffer, sizeof(tddi_packet_t) + sizeof(tddi_mesh_config_sqnr_request_t));

   if (ret != sizeof(tddi_packet_t) + sizeof(tddi_mesh_config_sqnr_request_t))
   {
      fprintf(stderr, "Error writing to "TDDI_FILE_NAME " ret=%d\n", ret);
      return -1;
   }

   lseek(fd, 0, SEEK_CUR);

   ret = read(fd, buffer, sizeof(tddi_packet_t) + sizeof(tddi_mesh_config_sqnr_response_t));

   if (ret != sizeof(tddi_packet_t) + sizeof(tddi_mesh_config_sqnr_response_t))
   {
      fprintf(stderr, "Error reading from "TDDI_FILE_NAME " ret=%d,errno=%d\n", ret, errno);
      switch (errno)
      {
      case EINTR:
         fprintf(stderr, "EINTR occurred\n");
         break;

      case EAGAIN:
         fprintf(stderr, "EAGAIN occurred\n");
         break;

      case EIO:
         fprintf(stderr, "EIO occurred\n");
         break;

      case EBADF:
         fprintf(stderr, "EBADF occurred\n");
         break;

      case EINVAL:
         fprintf(stderr, "EINVAL occurred\n");
         break;

      case EFAULT:
         fprintf(stderr, "EFAULT occurred\n");
         break;
      }
      return -1;
   }

   close(fd);

   return 0;
}


static int _configure_mobility(int argc, char **argv, const char *operation, const char *config_file_path)
{
   char          file_path[256];
   int           fd;
   tddi_packet_t *packet;
   tddi_configure_mobility_request_t *request;
   int           ret;
   off_t         offset;
   unsigned char *request_packet;
   unsigned char *request_data;
   unsigned char response_buffer[sizeof(tddi_packet_t) + sizeof(tddi_configure_mobility_request_t)];
   tddi_packet_t *response_packet;
   tddi_configure_mobility_response_t *response_data;

   strcpy(file_path, _DEFAULT_MOBILITY_CONF_PATH);

   /**
    * First find out the length of the data
    */

   fd = open(file_path, O_RDONLY);

   if (fd == -1)
   {
      fprintf(stderr, "Error opening %s for reading\n", file_path);
      return -1;
   }

   offset         = lseek(fd, 0, SEEK_END);
   request_packet = (unsigned char *)malloc(sizeof(tddi_packet_t) + sizeof(tddi_configure_mobility_request_t) + offset);
   packet         = (tddi_packet_t *)request_packet;
   request        = (tddi_configure_mobility_request_t *)(request_packet + sizeof(tddi_packet_t));
   request_data   = request_packet + sizeof(tddi_packet_t) + sizeof(tddi_configure_mobility_request_t);

   lseek(fd, 0, SEEK_SET);

   read(fd, request_data, offset);


   close(fd);

   packet->version   = TDDI_CURRENT_VERSION;
   packet->signature = TDDI_PACKET_SIGNATURE;
   packet->type      = TDDI_PACKET_TYPE_REQUEST;
   packet->data_size = sizeof(tddi_configure_mobility_request_t) + offset;
   packet->sub_type  = TDDI_REQUEST_TYPE_CONFIGURE_MOBILITY;

   request->config_string_length = offset;

   fd = open(TDDI_FILE_NAME, O_RDWR);

   if (fd == -1)
   {
      fprintf(stderr, "Error opening "TDDI_FILE_NAME "\n");
      free(request_packet);
      return -1;
   }

   ret = write(fd, request_packet, sizeof(tddi_packet_t) + sizeof(tddi_configure_mobility_request_t) + offset);

   if (ret != sizeof(tddi_packet_t) + sizeof(tddi_configure_mobility_request_t) + offset)
   {
      fprintf(stderr, "Error writing to "TDDI_FILE_NAME " ret=%d\n", ret);
      free(request_packet);
      return -1;
   }


   lseek(fd, 0, SEEK_CUR);

   ret = read(fd, response_buffer, sizeof(response_buffer));

   if (ret != sizeof(response_buffer))
   {
      fprintf(stderr, "meshd : _configure_mobility : Error reading from "TDDI_FILE_NAME " ret=%d,errno=%d\n", ret, errno);
      switch (errno)
      {
      case EINTR:
         fprintf(stderr, "EINTR occurred\n");
         break;

      case EAGAIN:
         fprintf(stderr, "EAGAIN occurred\n");
         break;

      case EIO:
         fprintf(stderr, "EIO occurred\n");
         break;

      case EBADF:
         fprintf(stderr, "EBADF occurred\n");
         break;

      case EINVAL:
         fprintf(stderr, "EINVAL occurred\n");
         break;

      case EFAULT:
         fprintf(stderr, "EFAULT occurred\n");
         break;
      }
      free(request_packet);
      close(fd);
      return -1;
   }

   free(request_packet);
   close(fd);

   response_packet = (tddi_packet_t *)response_buffer;
   response_data   = (tddi_configure_mobility_response_t *)(response_buffer + sizeof(tddi_packet_t));

   switch (response_data->response)
   {
   case TDDI_CONFIGURE_MOBILITY_RESPONSE_SUCCESS:
      fprintf(stderr, "Configure mobility Successful\n");
      break;

   case TDDI_CONFIGURE_MOBILITY_RESPONSE_BUSY:
      fprintf(stderr, "Configure mobility busy\n");
      break;
   }

   return 0;
}


static int _lock_reboot(int argc, char **argv, const char *operation, const char *config_file_path)
{
   int                         fd;
   tddi_packet_t               *packet;
   int                         ret;
   tddi_lock_reboot_request_t  *request;
   tddi_lock_reboot_response_t *response;
   unsigned char               buffer[sizeof(tddi_packet_t) + sizeof(tddi_lock_reboot_request_t)];

   if (argc < 3)
   {
      fprintf(stderr, "usage: meshd lock_reboot [1=lock, 0=unlock]\n");
      return -1;
   }

   fd = open(TDDI_FILE_NAME, O_RDWR);

   if (fd == -1)
   {
      fprintf(stderr, "Error opening "TDDI_FILE_NAME "\n");
      return -1;
   }

   packet  = (tddi_packet_t *)buffer;
   request = (tddi_lock_reboot_request_t *)(buffer + sizeof(tddi_packet_t));

   memset(request, 0, sizeof(tddi_lock_reboot_request_t));

   packet->signature = TDDI_PACKET_SIGNATURE;
   packet->version   = TDDI_CURRENT_VERSION;
   packet->data_size = sizeof(tddi_lock_reboot_request_t);
   packet->type      = TDDI_PACKET_TYPE_REQUEST;
   packet->sub_type  = TDDI_REQUEST_TYPE_LOCK_REBOOT;

   request->lock_or_unlock = atoi(argv[2]);

   ret = write(fd, buffer, sizeof(tddi_packet_t) + sizeof(tddi_lock_reboot_request_t));

   if (ret != sizeof(tddi_packet_t) + sizeof(tddi_lock_reboot_request_t))
   {
      fprintf(stderr, "Error writing to "TDDI_FILE_NAME " ret=%d\n", ret);
      return -1;
   }

   lseek(fd, 0, SEEK_CUR);

   ret = read(fd, buffer, sizeof(tddi_packet_t) + sizeof(tddi_lock_reboot_response_t));

   if (ret != sizeof(tddi_packet_t) + sizeof(tddi_lock_reboot_response_t))
   {
      fprintf(stderr, "Error reading from "TDDI_FILE_NAME " ret=%d,errno=%d\n", ret, errno);
      switch (errno)
      {
      case EINTR:
         fprintf(stderr, "EINTR occurred\n");
         break;

      case EAGAIN:
         fprintf(stderr, "EAGAIN occurred\n");
         break;

      case EIO:
         fprintf(stderr, "EIO occurred\n");
         break;

      case EBADF:
         fprintf(stderr, "EBADF occurred\n");
         break;

      case EINVAL:
         fprintf(stderr, "EINVAL occurred\n");
         break;

      case EFAULT:
         fprintf(stderr, "EFAULT occurred\n");
         break;
      }
      return -1;
   }

   close(fd);

   response = (tddi_lock_reboot_response_t *)(buffer + sizeof(tddi_packet_t));

   fprintf(stderr, "Current lock count = %d\n", response->lock_count);

   return 0;
}


static int _set_reboot_flag(int argc, char **argv, const char *operation, const char *config_file_path)
{
   int           fd;
   tddi_packet_t *packet;
   int           ret;
   tddi_set_reboot_flag_request_t  *request;
   tddi_set_reboot_flag_response_t *response;
   unsigned char buffer[sizeof(tddi_packet_t) + sizeof(tddi_set_reboot_flag_request_t)];

   if (argc < 3)
   {
      fprintf(stderr, "usage: meshd reboot_flag flag[1=reboot, 0=no reboot]\n");
      return -1;
   }

   fd = open(TDDI_FILE_NAME, O_RDWR);

   if (fd == -1)
   {
      fprintf(stderr, "Error opening "TDDI_FILE_NAME "\n");
      return -1;
   }

   packet  = (tddi_packet_t *)buffer;
   request = (tddi_set_reboot_flag_request_t *)(buffer + sizeof(tddi_packet_t));

   memset(request, 0, sizeof(tddi_set_reboot_flag_request_t));

   packet->signature = TDDI_PACKET_SIGNATURE;
   packet->version   = TDDI_CURRENT_VERSION;
   packet->data_size = sizeof(tddi_set_reboot_flag_request_t);
   packet->type      = TDDI_PACKET_TYPE_REQUEST;
   packet->sub_type  = TDDI_REQUEST_TYPE_SET_REBOOT_FLAG;

   request->flag = atoi(argv[2]);

   ret = write(fd, buffer, sizeof(tddi_packet_t) + sizeof(tddi_set_reboot_flag_request_t));

   if (ret != sizeof(tddi_packet_t) + sizeof(tddi_set_reboot_flag_request_t))
   {
      fprintf(stderr, "Error writing to "TDDI_FILE_NAME " ret=%d\n", ret);
      return -1;
   }

   lseek(fd, 0, SEEK_CUR);

   ret = read(fd, buffer, sizeof(tddi_packet_t) + sizeof(tddi_set_reboot_flag_response_t));

   if (ret != sizeof(tddi_packet_t) + sizeof(tddi_set_reboot_flag_response_t))
   {
      fprintf(stderr, "Error reading from "TDDI_FILE_NAME " ret=%d,errno=%d\n", ret, errno);
      switch (errno)
      {
      case EINTR:
         fprintf(stderr, "EINTR occurred\n");
         break;

      case EAGAIN:
         fprintf(stderr, "EAGAIN occurred\n");
         break;

      case EIO:
         fprintf(stderr, "EIO occurred\n");
         break;

      case EBADF:
         fprintf(stderr, "EBADF occurred\n");
         break;

      case EINVAL:
         fprintf(stderr, "EINVAL occurred\n");
         break;

      case EFAULT:
         fprintf(stderr, "EFAULT occurred\n");
         break;
      }
      return -1;
   }

   close(fd);

   return 0;
}


static int _enable_reset_gen(int argc, char **argv, const char *operation, const char *config_file_path)
{
   int           fd;
   tddi_packet_t *packet;
   int           ret;
   tddi_enable_reset_gen_request_t  *request;
   tddi_enable_reset_gen_response_t *response;
   unsigned char buffer[sizeof(tddi_packet_t) + sizeof(tddi_enable_reset_gen_request_t)];

   if (argc < 4)
   {
      fprintf(stderr, "usage: meshd enable_reset_gen flag[1=enable, 0=disable] interval\n");
      return -1;
   }

   fd = open(TDDI_FILE_NAME, O_RDWR);

   if (fd == -1)
   {
      fprintf(stderr, "Error opening "TDDI_FILE_NAME "\n");
      return -1;
   }

   packet  = (tddi_packet_t *)buffer;
   request = (tddi_enable_reset_gen_request_t *)(buffer + sizeof(tddi_packet_t));

   memset(request, 0, sizeof(tddi_enable_reset_gen_request_t));

   packet->signature = TDDI_PACKET_SIGNATURE;
   packet->version   = TDDI_CURRENT_VERSION;
   packet->data_size = sizeof(tddi_enable_reset_gen_request_t);
   packet->type      = TDDI_PACKET_TYPE_REQUEST;
   packet->sub_type  = TDDI_REQUEST_TYPE_ENABLE_RESET_GEN;

   request->flag     = atoi(argv[2]);
   request->interval = atoi(argv[3]);

   ret = write(fd, buffer, sizeof(tddi_packet_t) + sizeof(tddi_enable_reset_gen_request_t));

   if (ret != sizeof(tddi_packet_t) + sizeof(tddi_enable_reset_gen_request_t))
   {
      fprintf(stderr, "Error writing to "TDDI_FILE_NAME " ret=%d\n", ret);
      return -1;
   }

   lseek(fd, 0, SEEK_CUR);

   ret = read(fd, buffer, sizeof(tddi_packet_t) + sizeof(tddi_enable_reset_gen_response_t));

   if (ret != sizeof(tddi_packet_t) + sizeof(tddi_enable_reset_gen_response_t))
   {
      fprintf(stderr, "Error reading from "TDDI_FILE_NAME " ret=%d,errno=%d\n", ret, errno);
      switch (errno)
      {
      case EINTR:
         fprintf(stderr, "EINTR occurred\n");
         break;

      case EAGAIN:
         fprintf(stderr, "EAGAIN occurred\n");
         break;

      case EIO:
         fprintf(stderr, "EIO occurred\n");
         break;

      case EBADF:
         fprintf(stderr, "EBADF occurred\n");
         break;

      case EINVAL:
         fprintf(stderr, "EINVAL occurred\n");
         break;

      case EFAULT:
         fprintf(stderr, "EFAULT occurred\n");
         break;
      }
      return -1;
   }

   close(fd);

   return 0;
}


static int _vlan(int argc, char **argv, const char *operation, const char *config_file_path)
{
   int                      fd;
   tddi_packet_t            *packet;
   int                      ret;
   tddi_set_vlan_request_t  *request;
   tddi_set_vlan_response_t *response;
   unsigned char            buffer[sizeof(tddi_packet_t) + sizeof(tddi_set_vlan_request_t)];

   if (argc < 4)
   {
      fprintf(stderr, "usage: meshd vlan mac_address vlan_tag\n");
      return -1;
   }

   fd = open(TDDI_FILE_NAME, O_RDWR);

   if (fd == -1)
   {
      fprintf(stderr, "Error opening "TDDI_FILE_NAME "\n");
      return -1;
   }

   packet  = (tddi_packet_t *)buffer;
   request = (tddi_set_vlan_request_t *)(buffer + sizeof(tddi_packet_t));

   memset(request, 0, sizeof(tddi_set_vlan_request_t));

   packet->signature = TDDI_PACKET_SIGNATURE;
   packet->version   = TDDI_CURRENT_VERSION;
   packet->data_size = sizeof(tddi_set_vlan_request_t);
   packet->type      = TDDI_PACKET_TYPE_REQUEST;
   packet->sub_type  = TDDI_REQUEST_TYPE_SET_VLAN;

   mac_address_ascii_to_binary(argv[2], request->address);

   request->vlan_tag = atoi(argv[3]);

   ret = write(fd, buffer, sizeof(tddi_packet_t) + sizeof(tddi_set_vlan_request_t));

   if (ret != sizeof(tddi_packet_t) + sizeof(tddi_set_vlan_request_t))
   {
      fprintf(stderr, "Error writing to "TDDI_FILE_NAME " ret=%d\n", ret);
      return -1;
   }

   lseek(fd, 0, SEEK_CUR);

   ret = read(fd, buffer, sizeof(tddi_packet_t) + sizeof(tddi_set_vlan_response_t));

   if (ret != sizeof(tddi_packet_t) + sizeof(tddi_set_vlan_response_t))
   {
      fprintf(stderr, "Error reading from "TDDI_FILE_NAME " ret=%d,errno=%d\n", ret, errno);
      switch (errno)
      {
      case EINTR:
         fprintf(stderr, "EINTR occurred\n");
         break;

      case EAGAIN:
         fprintf(stderr, "EAGAIN occurred\n");
         break;

      case EIO:
         fprintf(stderr, "EIO occurred\n");
         break;

      case EBADF:
         fprintf(stderr, "EBADF occurred\n");
         break;

      case EINVAL:
         fprintf(stderr, "EINVAL occurred\n");
         break;

      case EFAULT:
         fprintf(stderr, "EFAULT occurred\n");
         break;
      }
      return -1;
   }

   response = (tddi_set_vlan_response_t *)(buffer + sizeof(tddi_packet_t));

   switch (response->response)
   {
   case TDDI_SET_VLAN_RESPONSE_OK:
      fprintf(stderr, "OK\n");
      break;

   case TDDI_SET_VLAN_RESPONSE_NOT_STARTED:
      fprintf(stderr, "Not started\n");
      break;

   case TDDI_SET_VLAN_RESPONSE_NOT_FOUND:
      fprintf(stderr, "Not found\n");
      break;

   case TDDI_SET_VLAN_RESPONSE_WRONG_PARAM:
      fprintf(stderr, "Incorrect parameters\n");
      break;
   }

   close(fd);

   return 0;
}


static int _block_cpu(int argc, char **argv, const char *operation, const char *config_file_path)
{
   int                      fd;
   tddi_packet_t            *packet;
   int                      ret;
   tddi_block_cpu_request_t *request;
   unsigned char            buffer[sizeof(tddi_packet_t) + sizeof(tddi_block_cpu_request_t)];

   if (argc < 3)
   {
      fprintf(stderr, "usage: meshd block_cpu type\n");
      return -1;
   }

   fd = open(TDDI_FILE_NAME, O_RDWR);

   if (fd == -1)
   {
      fprintf(stderr, "Error opening "TDDI_FILE_NAME "\n");
      return -1;
   }

   packet  = (tddi_packet_t *)buffer;
   request = (tddi_block_cpu_request_t *)(buffer + sizeof(tddi_packet_t));

   memset(request, 0, sizeof(tddi_block_cpu_request_t));

   packet->signature = TDDI_PACKET_SIGNATURE;
   packet->version   = TDDI_CURRENT_VERSION;
   packet->data_size = sizeof(tddi_block_cpu_request_t);
   packet->type      = TDDI_PACKET_TYPE_REQUEST;
   packet->sub_type  = TDDI_REQUEST_TYPE_BLOCK_CPU;

   request->type = atoi(argv[2]);

   ret = write(fd, buffer, sizeof(tddi_packet_t) + sizeof(tddi_block_cpu_request_t));

   if (ret != sizeof(tddi_packet_t) + sizeof(tddi_block_cpu_request_t))
   {
      fprintf(stderr, "Error writing to "TDDI_FILE_NAME " ret=%d\n", ret);
      return -1;
   }

   lseek(fd, 0, SEEK_CUR);

   ret = read(fd, buffer, sizeof(tddi_packet_t));

   if (ret != sizeof(tddi_packet_t))
   {
      fprintf(stderr, "Error reading from "TDDI_FILE_NAME " ret=%d,errno=%d\n", ret, errno);
      switch (errno)
      {
      case EINTR:
         fprintf(stderr, "EINTR occurred\n");
         break;

      case EAGAIN:
         fprintf(stderr, "EAGAIN occurred\n");
         break;

      case EIO:
         fprintf(stderr, "EIO occurred\n");
         break;

      case EBADF:
         fprintf(stderr, "EBADF occurred\n");
         break;

      case EINVAL:
         fprintf(stderr, "EINVAL occurred\n");
         break;

      case EFAULT:
         fprintf(stderr, "EFAULT occurred\n");
         break;
      }
      return -1;
   }

   close(fd);

   return 0;
}


static int _full_disconnect(int argc, char **argv, const char *operation, const char *config_file_path)
{
   int           fd;
   tddi_packet_t *packet;
   int           ret;
   unsigned char buffer[sizeof(tddi_packet_t)];


   fd = open(TDDI_FILE_NAME, O_RDWR);

   if (fd == -1)
   {
      fprintf(stderr, "Error opening "TDDI_FILE_NAME "\n");
      return -1;
   }

   packet = (tddi_packet_t *)buffer;

   packet->signature = TDDI_PACKET_SIGNATURE;
   packet->version   = TDDI_CURRENT_VERSION;
   packet->data_size = 0;
   packet->type      = TDDI_PACKET_TYPE_REQUEST;
   packet->sub_type  = TDDI_REQUEST_TYPE_FULL_DISCONNECT;

   ret = write(fd, buffer, sizeof(tddi_packet_t));

   if (ret != sizeof(tddi_packet_t))
   {
      fprintf(stderr, "Error writing to "TDDI_FILE_NAME " ret=%d\n", ret);
      return -1;
   }

   lseek(fd, 0, SEEK_CUR);

   ret = read(fd, buffer, sizeof(tddi_packet_t));

   if (ret != sizeof(tddi_packet_t))
   {
      fprintf(stderr, "Error reading from "TDDI_FILE_NAME " ret=%d,errno=%d\n", ret, errno);
      switch (errno)
      {
      case EINTR:
         fprintf(stderr, "EINTR occurred\n");
         break;

      case EAGAIN:
         fprintf(stderr, "EAGAIN occurred\n");
         break;

      case EIO:
         fprintf(stderr, "EIO occurred\n");
         break;

      case EBADF:
         fprintf(stderr, "EBADF occurred\n");
         break;

      case EINVAL:
         fprintf(stderr, "EINVAL occurred\n");
         break;

      case EFAULT:
         fprintf(stderr, "EFAULT occurred\n");
         break;
      }
      return -1;
   }

   close(fd);

   return 0;
}


static int _dhcp_request(int argc, char **argv, const char *operation, const char *config_file_path)
{
   int                  fd;
   tddi_packet_t        packet;
   int                  ret;
   unsigned char        buffer[sizeof(tddi_packet_t) + sizeof(tddi_dhcp_response_t)];
   tddi_packet_t        *response_packet;
   tddi_dhcp_response_t *response_data;

   fd = open(TDDI_FILE_NAME, O_RDWR);

   if (fd == -1)
   {
      fprintf(stderr, "Error opening "TDDI_FILE_NAME "\n");
      return -1;
   }

   packet.signature = TDDI_PACKET_SIGNATURE;
   packet.version   = TDDI_CURRENT_VERSION;
   packet.data_size = 0;
   packet.type      = TDDI_PACKET_TYPE_REQUEST;
   packet.sub_type  = TDDI_REQUEST_TYPE_GET_DHCP_INFO;

   ret = write(fd, &packet, sizeof(tddi_packet_t));

   if (ret != sizeof(tddi_packet_t))
   {
      fprintf(stderr, "Error writing to "TDDI_FILE_NAME " ret=%d\n", ret);
      return -1;
   }

   lseek(fd, 0, SEEK_CUR);

   ret = read(fd, buffer, sizeof(buffer));

   if (ret < sizeof(tddi_packet_t) + sizeof(tddi_dhcp_response_t))
   {
      fprintf(stderr, "Error reading from "TDDI_FILE_NAME " ret=%d,errno=%d\n", ret, errno);
      switch (errno)
      {
      case EINTR:
         fprintf(stderr, "EINTR occurred\n");
         break;

      case EAGAIN:
         fprintf(stderr, "EAGAIN occurred\n");
         break;

      case EIO:
         fprintf(stderr, "EIO occurred\n");
         break;

      case EBADF:
         fprintf(stderr, "EBADF occurred\n");
         break;

      case EINVAL:
         fprintf(stderr, "EINVAL occurred\n");
         break;

      case EFAULT:
         fprintf(stderr, "EFAULT occurred\n");
         break;
      }
      return -1;
   }

   close(fd);

   response_packet = (tddi_packet_t *)buffer;
   response_data   = (tddi_dhcp_response_t *)(buffer + sizeof(tddi_packet_t));

   fprintf(stderr,
           "DHCP Enabled : %s IP : %d.%d.%d.%d\n",
           response_data->dhcp_enabled ? "YES" : "NO",
           response_data->self_dhcp_ip_address[0],
           response_data->self_dhcp_ip_address[1],
           response_data->self_dhcp_ip_address[2],
           response_data->self_dhcp_ip_address[3]);

   return 0;
}


static int _gpio(int argc, char **argv, const char *operation, const char *config_file_path)
{
   int                  fd;
   tddi_packet_t        *packet;
   int                  ret;
   tddi_gpio_request_t  *request;
   tddi_gpio_response_t *response;
   unsigned char        buffer[sizeof(tddi_packet_t) + sizeof(tddi_gpio_request_t)];

   if (argc < 4)
   {
      fprintf(stderr, "usage: meshd gpio gpio_number R/W [value]\n");
      return -1;
   }

   fd = open(TDDI_FILE_NAME, O_RDWR);

   if (fd == -1)
   {
      fprintf(stderr, "Error opening "TDDI_FILE_NAME "\n");
      return -1;
   }

   packet  = (tddi_packet_t *)buffer;
   request = (tddi_gpio_request_t *)(buffer + sizeof(tddi_packet_t));

   memset(request, 0, sizeof(tddi_gpio_request_t));

   packet->signature = TDDI_PACKET_SIGNATURE;
   packet->version   = TDDI_CURRENT_VERSION;
   packet->data_size = sizeof(tddi_gpio_request_t);
   packet->type      = TDDI_PACKET_TYPE_REQUEST;
   packet->sub_type  = TDDI_REQUEST_TYPE_GPIO_READ_WRITE;

   request->gpio       = atoi(argv[2]);
   request->read_write = atoi(argv[3]);

   if (request->read_write == TDDI_GPIO_REQUEST_WRITE)
   {
      if (argc < 5)
      {
         fprintf(stderr, "usage: meshd gpio gpio_number R/W [value]\n");
         return -1;
      }
      request->value = atoi(argv[4]);
   }

   ret = write(fd, buffer, sizeof(tddi_packet_t) + sizeof(tddi_gpio_request_t));

   if (ret != sizeof(tddi_packet_t) + sizeof(tddi_gpio_request_t))
   {
      fprintf(stderr, "Error writing to "TDDI_FILE_NAME " ret=%d\n", ret);
      return -1;
   }

   lseek(fd, 0, SEEK_CUR);

   ret = read(fd, buffer, sizeof(tddi_packet_t) + sizeof(tddi_gpio_response_t));

   if (ret != sizeof(tddi_packet_t) + sizeof(tddi_gpio_response_t))
   {
      fprintf(stderr, "Error reading from "TDDI_FILE_NAME " ret=%d,errno=%d\n", ret, errno);
      switch (errno)
      {
      case EINTR:
         fprintf(stderr, "EINTR occurred\n");
         break;

      case EAGAIN:
         fprintf(stderr, "EAGAIN occurred\n");
         break;

      case EIO:
         fprintf(stderr, "EIO occurred\n");
         break;

      case EBADF:
         fprintf(stderr, "EBADF occurred\n");
         break;

      case EINVAL:
         fprintf(stderr, "EINVAL occurred\n");
         break;

      case EFAULT:
         fprintf(stderr, "EFAULT occurred\n");
         break;
      }
      return -1;
   }

   response = (tddi_gpio_response_t *)(buffer + sizeof(tddi_packet_t));

   switch (response->response)
   {
   case TDDI_GPIO_RESPONSE_READ_SUCCESS:
      fprintf(stderr, "GPIO Read Success : %d\n", response->value);
      break;

   case TDDI_GPIO_RESPONSE_WRITE_SUCCESS:
      fprintf(stderr, "GPIO Write Success : %d\n", response->value);
      break;

   case TDDI_GPIO_RESPONSE_FAILURE:
      fprintf(stderr, "ERROR\n");
      break;
   }

   close(fd);

   return 0;
}


static int _set_gps(int argc, char **argv, const char *operation, const char *config_file_path)
{
   int             fd;
   tddi_packet_t   *packet;
   int             ret;
   tddi_gps_info_t *info;
   char            corrdinate[64];
   unsigned char   buffer[sizeof(tddi_packet_t) + sizeof(tddi_gps_info_t)];

   if (argc < 6)
   {
      fprintf(stderr,
              "usage: meshd set_gps longitude latitude altitude speed\n"
              "       Long/Lat specified in the form DIRECTION COORDINATE\ne.g. W121.56789 or N36.90876\n"
              );
      return -1;
   }

   packet = (tddi_packet_t *)buffer;
   info   = (tddi_gps_info_t *)(buffer + sizeof(tddi_packet_t));

   memset(info, 0, sizeof(tddi_gps_info_t));

   packet->signature = TDDI_PACKET_SIGNATURE;
   packet->version   = TDDI_CURRENT_VERSION;
   packet->data_size = sizeof(tddi_gps_info_t);
   packet->type      = TDDI_PACKET_TYPE_REQUEST;
   packet->sub_type  = TDDI_REQUEST_TYPE_SET_GPS_INFO;

   if (argv[2][0] == 'W')
   {
      info->longitude[0] = '-';
      strcpy(info->longitude + 1, &argv[2][1]);
   }
   else if (argv[2][0] == 'E')
   {
      strcpy(info->longitude, &argv[2][1]);
   }
   else
   {
      fprintf(stderr, "Invalid longitude specification %s\n", argv[2]);
      return -1;
   }

   if (argv[3][0] == 'S')
   {
      info->latitude[0] = '-';
      strcpy(info->latitude + 1, &argv[3][1]);
   }
   else if (argv[3][0] == 'N')
   {
      strcpy(info->latitude, &argv[3][1]);
   }
   else
   {
      fprintf(stderr, "Invalid latitude specification %s\n", argv[3]);
      return -1;
   }

   strcpy(info->altitude, argv[4]);
   strcpy(info->speed, argv[5]);

   fd = open(TDDI_FILE_NAME, O_RDWR);

   if (fd == -1)
   {
      fprintf(stderr, "Error opening %s\n", TDDI_FILE_NAME);
      return -1;
   }

   ret = write(fd, buffer, sizeof(tddi_packet_t) + sizeof(tddi_gps_info_t));

   if (ret != sizeof(tddi_packet_t) + sizeof(tddi_gps_info_t))
   {
      fprintf(stderr, "Error writing to %s(%d)\n", TDDI_FILE_NAME, ret);
      close(fd);
      return -1;
   }

   lseek(fd, 0, SEEK_CUR);

   ret = read(fd, buffer, sizeof(tddi_packet_t));

   close(fd);

   return 0;
}


static int _check_use_virt_if(const char *config_file_path)
{
   int           fd;
   off_t         offset;
   unsigned char *file_data;
   int           al_conf_handle;
   int           use_virt_if;

   /**
    * Read in the config file into memory
    */

   fd = open(config_file_path, O_RDONLY);

   if (fd == -1)
   {
      fprintf(stderr, "Error opening %s for reading\n", config_file_path);
      return 0;
   }

   offset = lseek(fd, 0, SEEK_END);

   file_data = (unsigned char *)malloc(offset);

   lseek(fd, 0, SEEK_SET);

   read(fd, file_data, offset);

   close(fd);

   al_conf_handle = al_conf_parse(AL_CONTEXT file_data);

   if (al_conf_handle == -1)
   {
      free(file_data);
      return 0;
   }

   free(file_data);

   use_virt_if = al_conf_get_use_virt_if(AL_CONTEXT al_conf_handle);

   al_conf_close(AL_CONTEXT al_conf_handle);

   return use_virt_if;
}


static int _vconfigure(int argc, char **argv, const char *operation, const char *config_file_path)
{
   int           fd;
   tddi_packet_t *packet;
   tddi_virtual_configure_request_t *request;
   int           ret;
   off_t         offset;
   unsigned char *request_packet;
   unsigned char *request_data;
   unsigned char response_buffer[sizeof(tddi_packet_t) + sizeof(tddi_virtual_configure_response_t)];
   tddi_packet_t *response_packet;
   tddi_virtual_configure_response_t *response_data;
   char map_file_path[256];

   if (!_check_use_virt_if(config_file_path))
   {
      fprintf(stderr, "meshd: vconfigure command ignored\n");
      return 0;
   }

   strcpy(map_file_path, _DEFAULT_MAP_IF_CONF_PATH);

   /**
    * First find out the length of the data
    */

   fd = open(map_file_path, O_RDONLY);

   if (fd == -1)
   {
      fprintf(stderr, "Error opening %s for reading\n", map_file_path);
      return -1;
   }

   offset         = lseek(fd, 0, SEEK_END);
   request_packet = (unsigned char *)malloc(sizeof(tddi_packet_t) + sizeof(tddi_virtual_configure_request_t) + offset);
   packet         = (tddi_packet_t *)request_packet;
   request        = (tddi_virtual_configure_request_t *)(request_packet + sizeof(tddi_packet_t));
   request_data   = request_packet + sizeof(tddi_packet_t) + sizeof(tddi_virtual_configure_request_t);

   lseek(fd, 0, SEEK_SET);

   read(fd, request_data, offset);

   close(fd);

   packet->version   = TDDI_CURRENT_VERSION;
   packet->signature = TDDI_PACKET_SIGNATURE;
   packet->type      = TDDI_PACKET_TYPE_REQUEST;
   packet->data_size = sizeof(tddi_virtual_configure_request_t) + offset;
   packet->sub_type  = TDDI_REQUEST_TYPE_VIRTUAL_CONFIGURE;

   request->virtual_config_string_length = offset;

   fd = open(TDDI_FILE_NAME, O_RDWR);

   if (fd == -1)
   {
      fprintf(stderr, "Error opening "TDDI_FILE_NAME "\n");
      free(request_packet);
      return -1;
   }

   ret = write(fd, request_packet, sizeof(tddi_packet_t) + sizeof(tddi_virtual_configure_request_t) + offset);

   if (ret != sizeof(tddi_packet_t) + sizeof(tddi_virtual_configure_request_t) + offset)
   {
      fprintf(stderr, "Error writing to "TDDI_FILE_NAME " ret=%d\n", ret);
      free(request_packet);
      return -1;
   }

   lseek(fd, 0, SEEK_CUR);

   ret = read(fd, response_buffer, sizeof(response_buffer));

   if (ret != sizeof(response_buffer))
   {
      fprintf(stderr, "meshd : _configure : Error reading from "TDDI_FILE_NAME " ret=%d,errno=%d\n", ret, errno);
      switch (errno)
      {
      case EINTR:
         fprintf(stderr, "EINTR occurred\n");
         break;

      case EAGAIN:
         fprintf(stderr, "EAGAIN occurred\n");
         break;

      case EIO:
         fprintf(stderr, "EIO occurred\n");
         break;

      case EBADF:
         fprintf(stderr, "EBADF occurred\n");
         break;

      case EINVAL:
         fprintf(stderr, "EINVAL occurred\n");
         break;

      case EFAULT:
         fprintf(stderr, "EFAULT occurred\n");
         break;
      }
      free(request_packet);
      return -1;
   }

   free(request_packet);
   close(fd);

   response_packet = (tddi_packet_t *)response_buffer;
   response_data   = (tddi_virtual_configure_response_t *)(response_buffer + sizeof(tddi_packet_t));

   switch (response_data->response)
   {
   case TDDI_VIRTUAL_CONFIGURE_RESPONSE_SUCCESS:
      fprintf(stderr, "Virtual configure mesh successful\n");
      break;

   case TDDI_VIRTUAL_CONFIGURE_RESPONSE_BUSY:
      fprintf(stderr, "Mesh busy\n");
      break;
   }

   return 0;
}


static int _configure_sip(int argc, char **argv, const char *operation, const char *config_file_path)
{
   char                          sip_config_file_path[256];
   int                           fd;
   tddi_packet_t                 *packet;
   tddi_configure_sip_request_t  *request;
   int                           ret;
   off_t                         offset;
   unsigned char                 *request_packet;
   unsigned char                 *request_data;
   unsigned char                 response_buffer[sizeof(tddi_packet_t) + sizeof(tddi_configure_sip_response_t)];
   tddi_packet_t                 *response_packet;
   tddi_configure_sip_response_t *response_data;

   static const char _default_sip_conf[] = "enabled=0\nadhoc_only=0\nport=5060\nserver_ip=0.0.0.0\nsta_list(0) {}";

   strcpy(sip_config_file_path, _DEFAULT_SIP_CONF_PATH);

   /**
    * First find out the length of the data
    */

   fd = open(sip_config_file_path, O_RDONLY);

   if (fd == -1)
   {
      fprintf(stderr, "Error opening %s for reading, using defaults\n", sip_config_file_path);

      offset = strlen(_default_sip_conf);

      request_packet = (unsigned char *)malloc(sizeof(tddi_packet_t) + sizeof(tddi_configure_sip_request_t) + offset);
      packet         = (tddi_packet_t *)request_packet;
      request        = (tddi_configure_sip_request_t *)(request_packet + sizeof(tddi_packet_t));
      request_data   = request_packet + sizeof(tddi_packet_t) + sizeof(tddi_configure_sip_request_t);

      memcpy(request_data, _default_sip_conf, offset);

      fd = open(sip_config_file_path, O_WRONLY | O_CREAT);

      write(fd, _default_sip_conf, offset);

      close(fd);
   }
   else
   {
      offset         = lseek(fd, 0, SEEK_END);
      request_packet = (unsigned char *)malloc(sizeof(tddi_packet_t) + sizeof(tddi_configure_sip_request_t) + offset);
      packet         = (tddi_packet_t *)request_packet;
      request        = (tddi_configure_sip_request_t *)(request_packet + sizeof(tddi_packet_t));
      request_data   = request_packet + sizeof(tddi_packet_t) + sizeof(tddi_configure_sip_request_t);

      lseek(fd, 0, SEEK_SET);

      read(fd, request_data, offset);

      close(fd);
   }

   packet->version   = TDDI_CURRENT_VERSION;
   packet->signature = TDDI_PACKET_SIGNATURE;
   packet->type      = TDDI_PACKET_TYPE_REQUEST;
   packet->data_size = sizeof(tddi_configure_sip_request_t) + offset;
   packet->sub_type  = TDDI_REQUEST_TYPE_CONFIGURE_SIP;

   request->config_string_length = offset;

   fd = open(TDDI_FILE_NAME, O_RDWR);

   if (fd == -1)
   {
      fprintf(stderr, "Error opening "TDDI_FILE_NAME "\n");
      free(request_packet);
      return -1;
   }

   ret = write(fd, request_packet, sizeof(tddi_packet_t) + sizeof(tddi_configure_sip_request_t) + offset);

   if (ret != sizeof(tddi_packet_t) + sizeof(tddi_configure_sip_request_t) + offset)
   {
      fprintf(stderr, "Error writing to "TDDI_FILE_NAME " ret=%d\n", ret);
      free(request_packet);
      return -1;
   }

   lseek(fd, 0, SEEK_CUR);

   ret = read(fd, response_buffer, sizeof(response_buffer));

   if (ret != sizeof(response_buffer))
   {
      fprintf(stderr, "meshd : _configure_sip : Error reading from "TDDI_FILE_NAME " ret=%d,errno=%d\n", ret, errno);
      switch (errno)
      {
      case EINTR:
         fprintf(stderr, "EINTR occurred\n");
         break;

      case EAGAIN:
         fprintf(stderr, "EAGAIN occurred\n");
         break;

      case EIO:
         fprintf(stderr, "EIO occurred\n");
         break;

      case EBADF:
         fprintf(stderr, "EBADF occurred\n");
         break;

      case EINVAL:
         fprintf(stderr, "EINVAL occurred\n");
         break;

      case EFAULT:
         fprintf(stderr, "EFAULT occurred\n");
         break;
      }
      free(request_packet);
      close(fd);
      return -1;
   }

   free(request_packet);
   close(fd);

   response_packet = (tddi_packet_t *)response_buffer;
   response_data   = (tddi_configure_sip_response_t *)(response_buffer + sizeof(tddi_packet_t));

   switch (response_data->response)
   {
   case TDDI_CONFIGURE_SIP_RESPONSE_SUCCESS:
      fprintf(stderr, "Configure SIP Successful\n");
      break;

   case TDDI_CONFIGURE_SIP_RESPONSE_BUSY:
      fprintf(stderr, "Configure SIP busy\n");
      break;
   }

   return 0;
}

//SPAWAR
static int _query_board_stats(int argc, char **argv, const char *operation, const char *config_file_path)
{
   int                   fd1;
   int                   fd2;
   int                   fd3;
   int                   temp = 0, voltage = 0;
   tddi_packet_t         *packet;
   int                   ret;
   tddi_board_stats_t    *request;
   tddi_board_stats_t    *response;
   unsigned char         buffer[sizeof(tddi_packet_t) + sizeof(tddi_board_stats_t)];
   char buf1[256];
   if (argc < 2)
   {
      fprintf(stderr, "usage: meshd _query_board_stats\n");
      return -1;
   }

   fd1 = open(TDDI_FILE_NAME, O_RDWR);

   if (fd1 == -1)
   {
      fprintf(stderr, "Error opening "TDDI_FILE_NAME "\n");
      return -1;
   }

   fd2 = open(TEMP_STATS_FILE_NAME, O_RDONLY);
   if (fd2 == -1)
   {
      fprintf(stderr, "Error opening "TEMP_STATS_FILE_NAME " errno: %d\n", errno);
      return -1;
   }
   memset(buf1, 0, 256);
   ret = read(fd2, buf1, sizeof(buf1));
   if (ret == -1)
   {
       fprintf(stderr, "Error reading "TEMP_STATS_FILE_NAME"\n");
       return -1;
   }
   temp = atoi(buf1);
   temp /=10;
   printf("tbuffer: %s, temp: %d\n", buf1, temp);
   fd3 = open(VOLT_STATS_FILE_NAME, O_RDONLY);
   if (fd3 == -1)
   {
      fprintf(stderr, "Error opening "VOLT_STATS_FILE_NAME "error %d\n", errno);
      return -1;
   }
   memset(buf1, 0, 256);
   ret = read(fd3, buf1, 256);
   if (ret == -1)
   {
       fprintf(stderr, "Error reading "VOLT_STATS_FILE_NAME"\n");
       return -1;
   }
   voltage = atoi(buf1);
   voltage /=1000;
   printf("vbuffer: %s voltage: %d\n", buf1, voltage);
   packet  = (tddi_packet_t *)buffer;
   request = (tddi_board_stats_t *)(buffer + sizeof(tddi_packet_t));

   memset(request, 0, sizeof(tddi_board_stats_t));

   packet->signature = TDDI_PACKET_SIGNATURE;
   packet->version   = TDDI_CURRENT_VERSION;
   packet->data_size = sizeof(tddi_board_stats_t);
   packet->type      = TDDI_PACKET_TYPE_REQUEST;
   packet->sub_type  = TDDI_REQUEST_TYPE_QUERY_BOARD_STATS;

   request->voltage = voltage;
   request->temp = temp;

   printf("%s: volt: %d, temp: %d\n", __func__, request->voltage, request->temp);
   ret = write(fd1, packet, sizeof(tddi_packet_t) + sizeof(tddi_board_stats_t));

   if (ret != sizeof(tddi_packet_t) + sizeof(tddi_board_stats_t))
   {
      fprintf(stderr, "Error writing to "TDDI_FILE_NAME " ret=%d\n", ret);
      return -1;
   }
   lseek(fd1, 0, SEEK_CUR);

   ret = read(fd1, buffer, sizeof(tddi_packet_t) + sizeof(tddi_board_stats_t));

   if (ret != sizeof(tddi_packet_t) + sizeof(tddi_board_stats_t))
   {
      fprintf(stderr, "Error reading from "TDDI_FILE_NAME " ret=%d,errno=%d\n", ret, errno);
      switch (errno)
      {
      case EINTR:
         fprintf(stderr, "EINTR occurred\n");
         break;

      case EAGAIN:
         fprintf(stderr, "EAGAIN occurred\n");
         break;

      case EIO:
         fprintf(stderr, "EIO occurred\n");
         break;

      case EBADF:
         fprintf(stderr, "EBADF occurred\n");
         break;

      case EINVAL:
         fprintf(stderr, "EINVAL occurred\n");
         break;

      case EFAULT:
         fprintf(stderr, "EFAULT occurred\n");
         break;
      }
      return -1;
   }
   close(fd1);
   close(fd2);
   close(fd3);

   return 0;
}
//SPAWAR_END

static int _eth_link_status(int argc, char **argv, const char *operation, const char *config_file_path)
{
   int                   fd;
   tddi_packet_t         *packet;
   int                   ret;
   tddi_eth_link_state_t *request;
   tddi_eth_link_state_t *response;
   unsigned char         buffer[sizeof(tddi_packet_t) + sizeof(tddi_eth_link_state_t)];

   if (argc < 3)
   {
      fprintf(stderr, "usage: meshd _eth_link_status status\n");
      return -1;
   }

   fd = open(TDDI_FILE_NAME, O_RDWR);

   if (fd == -1)
   {
      fprintf(stderr, "Error opening "TDDI_FILE_NAME "\n");
      return -1;
   }

   packet  = (tddi_packet_t *)buffer;
   request = (tddi_eth_link_state_t *)(buffer + sizeof(tddi_packet_t));

   memset(request, 0, sizeof(tddi_eth_link_state_t));

   packet->signature = TDDI_PACKET_SIGNATURE;
   packet->version   = TDDI_CURRENT_VERSION;
   packet->data_size = sizeof(tddi_eth_link_state_t);
   packet->type      = TDDI_PACKET_TYPE_REQUEST;
   packet->sub_type  = TDDI_REQUEST_TYPE_ETH_LINK_FLAG;

   request->link_state = atoi(argv[2]);
   printf("Setting flag as %d\n", request->link_state);

   ret = write(fd, packet, sizeof(tddi_packet_t) + sizeof(tddi_eth_link_state_t));

   if (ret != sizeof(tddi_packet_t) + sizeof(tddi_eth_link_state_t))
   {
      fprintf(stderr, "Error writing to "TDDI_FILE_NAME " ret=%d\n", ret);
      return -1;
   }

   lseek(fd, 0, SEEK_CUR);

   ret = read(fd, buffer, sizeof(tddi_packet_t) + sizeof(tddi_mesh_flags_response_t));

   if (ret != sizeof(tddi_packet_t) + sizeof(tddi_mesh_flags_response_t))
   {
      fprintf(stderr, "Error reading from "TDDI_FILE_NAME " ret=%d,errno=%d\n", ret, errno);
      switch (errno)
      {
      case EINTR:
         fprintf(stderr, "EINTR occurred\n");
         break;

      case EAGAIN:
         fprintf(stderr, "EAGAIN occurred\n");
         break;

      case EIO:
         fprintf(stderr, "EIO occurred\n");
         break;

      case EBADF:
         fprintf(stderr, "EBADF occurred\n");
         break;

      case EINVAL:
         fprintf(stderr, "EINVAL occurred\n");
         break;

      case EFAULT:
         fprintf(stderr, "EFAULT occurred\n");
         break;
      }
      return -1;
   }

   close(fd);

   return 0;
}
