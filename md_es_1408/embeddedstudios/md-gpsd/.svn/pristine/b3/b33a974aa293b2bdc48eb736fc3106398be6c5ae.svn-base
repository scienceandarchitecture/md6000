
/********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : gpsd.c
 * Comments : Meshdynamics Generic NMEA GPS Reader for Linux
 * Created  : 9/6/2007
 * Author   : Sriram Dayanandan
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * -----------------------------------------------------------------------------
 * | No  |Date      |  Comment                                        | Author |
 * -----------------------------------------------------------------------------
 * |  0  |9/6/2007  | Created.                                        | Sriram |
 * -----------------------------------------------------------------------------
 ********************************************************************************/

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <fcntl.h>
#include <pthread.h>

#include <string.h>
#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>

#include <sys/stat.h>
#include <termios.h>
#include <getopt.h>

#include "torna_ddi.h"

#if !defined(_DEFAULT_SERIAL_DEV)
	#define _DEFAULT_SERIAL_DEV	"/dev/ttyS0"
#endif

#if !defined(_DEFAULT_TDDI_DEV)
	#define _DEFAULT_TDDI_DEV	"/dev/tddi"
#endif

struct _config {
	char			serial_dev[64];
	char			tddi_dev[64];
	unsigned char	udp_tx_enabled;
	char			ip_address[32];
	unsigned short	udp_port;
	char			udp_device_id[64];
	unsigned char	tx_interval;
	unsigned char	verbose;
};
typedef struct _config _config_t;

struct _gpsd_data {
	int				sock;
	int				fd;
	struct termios	old_termio;
	struct termios	new_termio;
	char			latitude[32];
	char			longitude[32];
	char			altitude[32];
	char			speed[32];
};
typedef struct _gpsd_data _gpsd_data_t;

#define _COPY_TO_COMMA(dst,src) while(*src != 0 && *src != ',') { *dst++ = *src++; }

static _config_t	_config;
static _gpsd_data_t	_gpsd_data;

#define _GGA_PREFIX_SIZE	7
#define _VTG_PREFIX_SIZE	7

static const char _gga_prefix[] = {'$','G','P','G','G','A',','};
static const char _vtg_prefix[] = {'$','G','P','V','T','G',','};

static void _usage()
{
	fprintf(stdout,
			"\n"
			"usage: gpsd [-p serial_device] [-t ip,port,id,interval] [-o tddi_device] [-v]\n"
			"-p			: Specify serial port device (default: "_DEFAULT_SERIAL_DEV")\n"
			"-t			: Transmit UDP packet every 'interval' seconds to 'ip':'port'\n"
			"UDP data transmitted is of the form $MDGPS,id,long,lat,alt,speed(CRLF)\n"
			"-o			: Specify TDDI device (default: "_DEFAULT_TDDI_DEV")\n"
			"-v			: Verbose mode\n"
			"-h			: Display this message\n\n"
			);
}

static void _parse_tx_details(_config_t* conf, const char* arg)
{
	char*		dst;
	const char*	src;
	char		temp[16];

	conf->udp_tx_enabled	= 0;

	src	= arg;

	dst	= conf->ip_address;
	_COPY_TO_COMMA(dst,src);
	if(*src == 0)
		return;
	src++;
	*dst = 0;

	dst	= temp;
	_COPY_TO_COMMA(dst,src);
	if(*src == 0)
		return;
	*dst = 0;
	src++;
	conf->udp_port	= atoi(temp);

	dst	= conf->udp_device_id;
	_COPY_TO_COMMA(dst,src);
	if(*src == 0)
		return;
	src++;
	*dst = 0;

	dst	= temp;
	strcpy(dst,src);
	conf->tx_interval	= atoi(temp);

	if(conf->tx_interval != 0 && conf->udp_port != 0)
		conf->udp_tx_enabled = 1;
	else
		conf->udp_tx_enabled = 0;
}

static void _init_serial_port(int fd,struct termios* old, struct termios* new_termio)
{
	tcgetattr(fd,old); /* save current port settings */	

	memcpy(new_termio,old,sizeof(struct termios));

	cfsetispeed(new_termio,B4800);
	cfsetospeed(new_termio,B4800);

	new_termio->c_cc[VMIN]		= 5;	/* blocking read until read_bytes chars received */
	new_termio->c_cc[VTIME]		= 0;

	tcflush(fd, TCIFLUSH);
	tcsetattr(fd,TCSANOW,new_termio);

}

static void _gga_normalize_nmea_latitude(char* latitude, char north_south)
{
	char	str_degrees[16];
	char	str_minutes[32];
	double	degrees;
	double	minutes;

	/**
	 * The NMEA latitude is specified in the form DDMM.mmm (2 digits for Degrees and Rest for Minutes)
	 */

	memcpy(str_degrees,latitude,2);
	str_degrees[2] = 0;
	degrees	= atof(str_degrees);

	strcpy(str_minutes,latitude + 2);
	minutes = atof(str_minutes);

	degrees	= degrees + (minutes/60.0);

	if(north_south == 'S')
		degrees *= -1;

	sprintf(latitude,"%.06f",degrees);
}

static void _gga_normalize_nmea_longitude(char* longitude, char east_west)
{
	char	str_degrees[16];
	char	str_minutes[32];
	double	degrees;
	double	minutes;

	/**
	 * The NMEA longitude is specified in the form DDDMM.mmm (3 digits for Degrees and Rest for Minutes)
	 */

	memcpy(str_degrees,longitude,3);
	str_degrees[3] = 0;
	degrees	= atof(str_degrees);

	strcpy(str_minutes,longitude + 3);
	minutes = atof(str_minutes);

	degrees	= degrees + (minutes/60.0);

	if(east_west == 'W')
		degrees *= -1;

	sprintf(longitude,"%.06f",degrees);
}

static void _write_tddi(_config_t* conf, _gpsd_data_t* data)
{
	int					fd;
	tddi_packet_t*		packet;
	int					ret;
	tddi_gps_info_t*	info;
	unsigned char		buffer[sizeof(tddi_packet_t) + sizeof(tddi_gps_info_t)];

	packet		= (tddi_packet_t*)buffer;
	info		= (tddi_gps_info_t*)(buffer + sizeof(tddi_packet_t));

	memset(info,0,sizeof(tddi_gps_info_t));

	packet->signature	= TDDI_PACKET_SIGNATURE;
	packet->version		= TDDI_CURRENT_VERSION;
	packet->data_size	= sizeof(tddi_gps_info_t);
	packet->type		= TDDI_PACKET_TYPE_REQUEST;
	packet->sub_type	= TDDI_REQUEST_TYPE_SET_GPS_INFO;

	strcpy(info->altitude,data->altitude);
	strcpy(info->latitude,data->latitude);
	strcpy(info->longitude,data->longitude);
	strcpy(info->speed,data->speed);

	fd	= open(conf->tddi_dev,O_RDWR);

	if(fd == -1) {
		if(conf->verbose)
			fprintf(stderr,"gpsd: Error opening %s\n",conf->tddi_dev);
		return;
	}

	ret	= write(fd,buffer,sizeof(tddi_packet_t) + sizeof(tddi_gps_info_t));

	if(ret != sizeof(tddi_packet_t) + sizeof(tddi_gps_info_t)) {
		if(conf->verbose)
			fprintf(stderr,"gpsd: Error writing to %s(%d)\n",conf->tddi_dev,ret);
		close(fd);
		return;
	}

	lseek(fd,0,SEEK_CUR);

	ret	= read(fd,buffer,sizeof(tddi_packet_t));

	close(fd);
}

static void _transmit_line(_config_t* conf, _gpsd_data_t* data)
{
	int					clilen;
	int					ret;
	struct sockaddr_in 	serv_addr;
	int					data_length;
	char				buffer[1024];

	
    bzero((char *) &serv_addr, sizeof(serv_addr));

	serv_addr.sin_family		= AF_INET;
	serv_addr.sin_port			= htons(conf->udp_port);
	serv_addr.sin_addr.s_addr	= inet_addr(conf->ip_address);
	clilen						= sizeof(struct sockaddr_in);

	sprintf(buffer,
		    "$MDGPS,%s,%s,%s,%s,%s\r\n",
			conf->udp_device_id,
			data->longitude,
			data->latitude,
			data->altitude,
			data->speed);

	if(conf->verbose) {
		fprintf(stderr,"GPSD Transmitting: %s\n",buffer);
	}

	data_length	= strlen(buffer);

	ret = sendto(data->sock,buffer,data_length,0,(const struct sockaddr*)&serv_addr,clilen);

	if(ret == -1) {
		perror("sendto");
	}
}

#define _GET_FIELD() do {dst = field;_COPY_TO_COMMA(dst,src); if(*src == 0){return;} src++;*dst = 0;}while(0)

static void _process_gga(_config_t* conf, _gpsd_data_t* data, const char* gga_data)
{
	char*		dst;
	const char*	src;
	char		field[128];

	src	= gga_data;

	_GET_FIELD();	/** UTC */

	_GET_FIELD();	/** Latitude */
	strcpy(data->latitude,field);
	_GET_FIELD();	/** Latitude N or S */
	_gga_normalize_nmea_latitude(data->latitude,field[0]);

	_GET_FIELD();	/** Longitude */
	strcpy(data->longitude,field);
	_GET_FIELD();	/** Longitude E or W */
	_gga_normalize_nmea_longitude(data->longitude,field[0]);

	_GET_FIELD();	/** Quality */

	_GET_FIELD();	/** Number of Satellites */

	_GET_FIELD();	/** HDOP */

	_GET_FIELD();	/** Altitude */
	strcpy(data->altitude,field);

	if(conf->verbose)
		fprintf(stderr,"gpsd GGA: %s,%s,%s\n",data->longitude,data->latitude,data->altitude);

	_write_tddi(conf,data);

	if(conf->udp_tx_enabled)
		_transmit_line(conf,data);

}

static void _process_vtg(_config_t* conf, _gpsd_data_t* data, const char* vtg_data)
{
	char*		dst;
	const char*	src;
	char		field[128];

	src	= vtg_data;

	_GET_FIELD();	/** Degrees */
	_GET_FIELD();	/** T */

	_GET_FIELD();	/** Degrees */
	_GET_FIELD();	/** M */

	_GET_FIELD();	/** Speed in knots */
	_GET_FIELD();	/** N */

	_GET_FIELD();	/** Speed in kmph */
	strcpy(data->speed,field);

	if(conf->verbose)
		fprintf(stderr,"gpsd VTG: %s\n",data->speed);

}

#undef _GET_FIELD

static void _process_nmea_line(_config_t* conf, _gpsd_data_t* data, const char* line)
{
	if(conf->verbose) {
		fprintf(stderr,"gpsd: %s",line);
	}
		
	if(!memcmp(line,_gga_prefix,_GGA_PREFIX_SIZE)) {
		_process_gga(conf,data,line + _GGA_PREFIX_SIZE);
	} else if(!memcmp(line,_vtg_prefix,_VTG_PREFIX_SIZE)) {
		_process_vtg(conf,data,line + _VTG_PREFIX_SIZE);
	}
}

int main(int argc, char** argv)
{
	char	nmea_line[256];
	char	read_buffer[128];
	char*	current;
	int		count;
	int		i;
	int		ch;

	memset(&_config,0,sizeof(_config_t));
	memset(&_gpsd_data,0,sizeof(_gpsd_data_t));

	strcpy(_config.serial_dev,_DEFAULT_SERIAL_DEV);
	strcpy(_config.tddi_dev,_DEFAULT_TDDI_DEV);

	fprintf(stdout,
			"\nMeshdynamics NMEA GPS Daemon v 1.0\n"
			"Copyright (c) 2002-2007 Meshdynamics, Inc\n"
			"All rights reserved.\n\n");

	while ((ch = getopt(argc, argv, "vhp:t:o")) != EOF) {
		switch(ch) {
		case 'p':
			strcpy(_config.serial_dev,optarg);
			break;
		case 't':
			_parse_tx_details(&_config,optarg);
			break;
		case 'o':
			strcpy(_config.tddi_dev,optarg);			
			break;
		case 'v':
			_config.verbose	= 1;
			break;
		case 'h':
			_usage();
			return 0;
		}		
	}

	fprintf(stdout,
			"GPSD: Reading from %s with transmissions to %s:%d every %d seconds using ID %s\n\n",
			_config.serial_dev,
			_config.ip_address,
			_config.udp_port,
			_config.tx_interval,
			_config.udp_device_id);

	_gpsd_data.fd	= open(_config.serial_dev, O_RDONLY | O_NOCTTY);

	if(_gpsd_data.fd < 0) {
		perror(_config.serial_dev); 
		return -1;
	}

	_init_serial_port(_gpsd_data.fd,&_gpsd_data.old_termio,&_gpsd_data.new_termio);

	if(_config.udp_tx_enabled) {
		if((_gpsd_data.sock = socket(AF_INET,SOCK_DGRAM,0)) < 0){
			perror("socket");
			return -2;
		}
	}
	
	current		= nmea_line;
	*current	= 0;
	
	while(1) {

		count = read(_gpsd_data.fd,read_buffer,81);

		for(i = 0; i < count; i++) {
			
			*current++ = read_buffer[i];

			if(read_buffer[i] == '\n') {				
				*current = 0;
				_process_nmea_line(&_config,&_gpsd_data,nmea_line);
				current		= nmea_line;
				*current	= 0;
			}

		}
	}

}

