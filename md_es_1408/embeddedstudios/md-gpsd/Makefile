#
# Copyright (C) 2008-2010 OpenWrt.org
#
# This is free software, licensed under the GNU General Public License v2.
# See /LICENSE for more information.
#

include $(TOPDIR)/rules.mk

PKG_NAME:=md-gpsd
PKG_RELEASE:=10
PKG_BUILD_DEPENDS:=md-torna

include $(INCLUDE_DIR)/package.mk
include $(INCLUDE_DIR)/meshap.mk

define Package/md-gpsd
	SECTION:=MeshDynamics
	CATEGORY:=MeshDynamics Applications
  TITLE:=MeshDynamics gpsd
  DEPENDS:=+libalconf
endef

ifeq ($(TARGET),imx6)
	_TARGET:=\"\\\""imx6"\\\"\"
	_GPS_DEV_NODE:=\"\\\""/dev/ttymxc4"\\\"\"
endif
ifeq ($(TARGET),cns3xxx)
	_TARGET:=\"\\\""cns3xxx"\\\"\"
	_GPS_DEV_NODE:=\"\\\""/dev/ttyS2"\\\"\"
endif
ifeq ($(TARGET),imx6_1k)
	_TARGET:=\"\\\""imx6_1k"\\\"\"
	_GPS_DEV_NODE:=\"\\\""/dev/ttyS0"\\\"\"
endif

TARGET_CFLAGS+= \
	-I$(STAGING_DIR)/usr/share/md-meshap \
	-I$(STAGING_DIR)/usr/share/md-meshap/aes \
	-I$(STAGING_DIR)/usr/include/md-meshap \
	-I$(STAGING_DIR)/usr/include/md-meshap/aes \
	-I$(STAGING_DIR)/usr/include/md-torna \
	-I$(STAGING_DIR)/usr/include/libalconf \
	-I$(STAGING_DIR)/usr/include/patcher \
	-I$(STAGING_DIR)/usr/share/patcher \
	-D_MESHAP_USE_ATOMIC_ \
	-D_TORNA_ENABLE_LOGGER_ \
	-D_TORNA_DEBUG_MASK_VALUE_=0xE \
	-D_TORNA_MESHAP_PACKET_POOL_SIZE_=1024 \
	-DACCESS_POINT_PACKET_POOL_SIZE=2048 \
	-DAL_USE_ENCRYPTION=1 \
	-DAL_CONF_PUT_SEPERATE \
	-D_TORNA_MESHAP_REBOOT_TIME_=10 \
	-DACCESS_POINT_VLAN_BCAST_OPTIMIZE=1 \
	-DFIPS_1402_COMPLIANT=1 \
	-DACCESS_POINT_STA_VERIFICATION_TIME=5000 \
	-DAL_IMPL_INLINE=inline \
	-D_AL_PRINT_LOG_ENABLED \
	-D_AL_PRINT_LOG_FILTER=0x201 \
	-D_MESHAP_AL_LOG_MASK_=0x200 \
	-DACCESS_POINT_STA_AGEING_TIME=5000 \
	-DACCESS_POINT_STAY_AWAKE_COUNT=500 \
	-D_MESHAP_OPTION_DECODE \
	-DAES_BIG_ENDIAN=4321 \
	-DAES_LITTLE_ENDIAN=1234 \
	-DINTERNAL_BYTE_ORDER=AES_LITTLE_ENDIAN \
	-DDOT1X_IMPL_BYTE_ORDER=DOT1X_BE \
	-DRC4_INLINE=inline \
	-D_TARGET=${_TARGET} \
	-D_DEFAULT_SERIAL_DEV=${_GPS_DEV_NODE}


ifeq ($(CONFIG_BIG_ENDIAN),y)
	TARGET_CFLAGS+= \
		-DCODEC_SYS_BIG_ENDIAN \
		-DPLATFORM_BYTE_ORDER=AES_BIG_ENDIAN
else
	TARGET_CFLAGS+= \
		-DCODEC_SYS_LITTLE_ENDIAN \
		-DPLATFORM_BYTE_ORDER=AES_LITTLE_ENDIAN
endif

define Build/Prepare
	mkdir -p $(PKG_BUILD_DIR)
	$(CP) ./src/* $(PKG_BUILD_DIR)/
endef

define Build/Compile
	$(MAKE_VARS) $(MAKE) -C $(PKG_BUILD_DIR) \
		$(TARGET_CONFIGURE_OPTS) \
		CFLAGS="$(TARGET_CFLAGS)" \
		LDFLAGS="$(TARGET_LDFLAGS)"
endef

define Package/md-gpsd/install
	$(INSTALL_DIR) $(1)/sbin
	$(INSTALL_BIN) $(PKG_BUILD_DIR)/gpsd $(1)/sbin
endef

$(eval $(call BuildPackage,md-gpsd))
