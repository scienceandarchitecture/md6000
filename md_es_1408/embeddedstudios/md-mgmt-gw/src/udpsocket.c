#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <netdb.h>
#include <string.h> 
#include "wssocket.h"

int sock = 0;
struct sockaddr_in bcastAddr;

int start_udpserver(int port){
		int bcastPermission = 1;
      al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MGMT_GW: FLOW: entering port:%d %s:%d\n",port, __func__, __LINE__);
		if ((sock = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP)) < 0){
            al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MGMT_GW: ERROR: socket opening Failed %s:%d\n", __func__, __LINE__);
				fprintf(stderr, "socket error");
				exit(1);
		}

		bcastPermission = 1;
		if (setsockopt(sock, SOL_SOCKET, SO_BROADCAST, (void *) &bcastPermission,sizeof(bcastPermission)) < 0){
				fprintf(stderr, "setsockopt error");
				exit(1);
		}

		//set timer for recv_socket
		int timeout = 100;
		setsockopt(sock, SOL_SOCKET, SO_RCVTIMEO,(char*)&timeout,sizeof(timeout));

		char loopch=0;

		if (setsockopt(sock, IPPROTO_IP, IP_MULTICAST_LOOP,
								(char *)&loopch, sizeof(loopch)) < 0) {
				perror("setting IP_MULTICAST_LOOP:");
				close(sock);
				exit(1);
		}


		/* Construct local address structure */
		memset(&bcastAddr, 0, sizeof(bcastAddr));   
		bcastAddr.sin_family = AF_INET;                 
		bcastAddr.sin_addr.s_addr = INADDR_ANY;
		bcastAddr.sin_port = htons(port);       

		if (bind(sock,(struct sockaddr *)&bcastAddr,sizeof(bcastAddr))<0){ 
            al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MGMT_GW: ERROR: Bind Failed fd:%d %s:%d\n", sock, __func__, __LINE__);
				printf("socket binding failed...");
		}
      al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MGMT_GW: FLOW: exit %s:%d\n", __func__, __LINE__);
		return sock;
}

int send_udpbroadcast(int port, void *data, int len){

		char *bcast = "255.255.255.255";
      al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MGMT_GW: FLOW: entering port:%d, length:%d %s:%d\n", port, len, __func__, __LINE__);
		/* Construct local address structure */
		memset(&bcastAddr, 0, sizeof(bcastAddr));   
		bcastAddr.sin_family = AF_INET;                 
		//    bcastAddr.sin_addr.s_addr = inet_addr("192.168.10.45");
		bcastAddr.sin_addr.s_addr = inet_addr(bcast);
		bcastAddr.sin_port = htons(port);       
		if (sendto(sock, data, len, 0, (struct sockaddr *)&bcastAddr, sizeof(bcastAddr)) != len){
            al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MGMT_GW: ERROR: sendto failed socket fd: %d, len:%d %s:%d\n", sock, len, __func__, __LINE__);
				fprintf(stderr, "sendto error");
		}
      al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MGMT_GW: FLOW: exit %s:%d\n", __func__, __LINE__);
}

int send_udppacket( char *ip, int port, void *data, int len){
      al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MGMT_GW: FLOW: entering %s:%d\n", __func__, __LINE__);

		/* Construct local address structure */
		memset(&bcastAddr, 0, sizeof(bcastAddr));   
		bcastAddr.sin_family = AF_INET;                 
		bcastAddr.sin_addr.s_addr = inet_addr(ip);
		bcastAddr.sin_port = htons(port);       

		if (sendto(sock, data, len, 0, (struct sockaddr *)&bcastAddr, sizeof(bcastAddr)) != len){
            al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MGMT_GW: ERROR: sendto failed socket fd: %d, len:%d %s:%d\n", sock, len, __func__, __LINE__);
				fprintf(stderr, "sendto error");
		}
      al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MGMT_GW: FLOW: exit %s:%d\n", __func__, __LINE__);
}
