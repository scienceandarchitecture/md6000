

#include "libwebsockets.h"
#include "al.h"
#ifndef WSSOCKET_H
#define WSSOCKET_H

#define GW_SUCCESS		 0
#define GW_FAIL			-1
#define GW_DEACTIVATED   0	


#define FLAG_TRUE                      1
#define FLAG_FALSE                     0

#define MESH_IMCP_RX_PORT 	57055
#define MESH_IMCP_TX_PORT  57054

#define NMS_RECONNECTION_TIMEOUT (1000*5000)    // 5 seconds timeout

#define IMCP_RECEIVE_BUF_SIZE	 (8192)
#define WS_CONFIGFILE                      "/var/tmp/wsconfig"


#ifndef _AL_ROVER_
#define AL_DECLARE_GLOBAL(decl)           decl
#define AL_DECLARE_GLOBAL_EXTERN(decl)    extern decl
#define AL_CONTEXT_PARAM_DECL
#define AL_CONTEXT_PARAM_DECL_SINGLE    void
#define AL_CONTEXT
#define AL_CONTEXT_SINGLE
#else
#define AL_DECLARE_GLOBAL(decl)
#define AL_DECLARE_GLOBAL_EXTERN(decl)
#define AL_CONTEXT_PARAM_DECL           al_context_t * al_context,
#define AL_CONTEXT_PARAM_DECL_SINGLE    al_context_t * al_context
#define AL_CONTEXT                      al_context,
#define AL_CONTEXT_SINGLE               al_context
#endif

#define NMS_CON_TYPE_SECURE 2

typedef struct
{
	char nms_server_addr[256];
	int nms_server_port_num;
	char nms_server_url[256];
	char interface[256];
	int ssl_type;
	int mgmt_gw_enable;

	char ca_cert_filepath[256];
	char client_cert_filepath[256];
	char client_key_filepath[256];
	char passphrase[256];

//	unsigned char proxy_flag;
//	unsigned char proxy_uri[256];

}mgnt_gw_input_params_t;

enum demo_protocols {

    PROTOCOL_IMCP,
    /* always last */
    DEMO_PROTOCOL_COUNT
};
#if 0
    static int
callback_imcp(struct libwebsocket_context *this,
        struct libwebsocket *wsi,
        enum libwebsocket_callback_reasons reason,
        void *user, void *in, size_t len);
#endif
int handle_connection(struct lws_context_creation_info *info,mgnt_gw_input_params_t *);


#endif // END WSSOCKET_H
