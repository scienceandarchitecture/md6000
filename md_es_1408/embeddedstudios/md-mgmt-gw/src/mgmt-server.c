#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include "libwebsockets.h"
#include <pthread.h>
#include <sys/types.h>
#include <net/if.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <netdb.h>
#include <ifaddrs.h>

#define IMCP_RECEIVE_BUF_SIZE 8192
#define LWS_SERVER_OPTION_DO_SSL_GLOBAL_INIT (1<<12)

#define MAX_MUX_RECURSION 2
#define LWS_SEND_BUFFER_PREPADDING	( 4 + 10 + ( 2 * MAX_MUX_RECURSION ) )
#define LWS_SEND_BUFFER_POSTPADDING	4
#define IMCP_RECEIVE_BUFFER_SIZE	8192
#define IMCP_SIGNATURE_SIZE		4
#define MAX_INTERFACES 15

int fd = 0;
int debug_level = 7;
int gUdprx = 0;
int udp_running = 0;
pthread_t udp_tid;
int g_udpSockFd = -1;
char mgmt_gw_interface_name[16];

struct sockaddr_in  nms_addr;
struct sockaddr_in  local_addr;

void * udp_sock_fun();
struct lws *g_wsi = NULL;
struct lws_context *context;

struct if_info_addr
{
   char host[NI_MAXHOST];
}hosts[MAX_INTERFACES];

void dump_bytes(unsigned char *D, int count)
{
   int i, base = 0;

   printf("-------- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- ----------------\n");
   for ( ; base + 16 < count; base += 16)
   {
      printf("%08X ", base);

      for (i = 0; i < 16; i++)
      {
         printf("%02X ", D[base + i]);
      }

      printf("\n");
   }
   printf("%08X ", base);
   for (i = base; i < count; i++)
   {
      printf("%02X ", D[i]);
   }
   for ( ; i < base + 16; i++)
   {
      printf("   ");
   }
   for ( ; i < base + 16; i++)
   {
      printf(" ");
   }
   printf("\n");
   printf("-------- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- ----------------\n");
   printf("\n");
}

int callback_imcp_server(struct lws *wsi, enum lws_callback_reasons reason,
        void *user, void *in, size_t len);

static struct lws_protocols protocols[] =
{
	{
		"mgmt-gw-imcp",
		callback_imcp_server,
		0,
		IMCP_RECEIVE_BUF_SIZE,
	},
	{ NULL, NULL, 0, 0 } /* terminator */
};

int callback_imcp_server(struct lws *wsi, enum lws_callback_reasons reason,
        void *user, void *in, size_t len)
{
	int n, ret;
	struct sockaddr_in to_addr;
	pthread_t udp_sock;
	char *buff;
	char client_name[32] = {0}, client_ip[4] = {0};
	int b_cast = 1;

	switch (reason)
	{
		case LWS_CALLBACK_ESTABLISHED:
			printf("LWS_CALLBACK_ESTABLISHED\n");

			lws_get_peer_addresses(wsi, lws_get_socket_fd(wsi),
								client_name, sizeof(client_name),
								client_ip, sizeof(client_ip));

			printf("Client Name = %s \n", client_name);

			g_wsi = (struct lws *)wsi;

			if(udp_running)
			{
				close(g_udpSockFd);
				pthread_cancel(udp_tid);
			}

			ret = pthread_create(&udp_sock, NULL, udp_sock_fun, NULL);
			if(ret < 0)
			{
				perror("pthread_create()");
				return -1;
			}
			udp_tid = udp_sock;
			break;

		case LWS_CALLBACK_SERVER_WRITEABLE:
//			lwsl_info("%s: LWS_CALLBACK_SERVER_WRITABLE\n", __func__);
			printf("LWS_CALLBACK_SERVER_WRITABLE\n");
			break;

		case LWS_CALLBACK_RECEIVE:

			if(gUdprx == 0)
                            break;

			buff = (unsigned char *) in;
			memset(&to_addr, 0, sizeof(to_addr));
			to_addr.sin_family = AF_INET;
			to_addr.sin_addr.s_addr = inet_addr("255.255.255.255");
			to_addr.sin_port = htons(0xdede);
			if((sendto(fd, buff, len, 0, (struct sockaddr *)&to_addr, sizeof(to_addr))) < 0)
			{
				perror("ERRRORR\n");
                                close(fd);
				return 0;
			}
			break;

		default:
			;
	}
	return 0;
}

void * udp_sock_fun()
{
	unsigned char recv_buf[LWS_SEND_BUFFER_PREPADDING + IMCP_RECEIVE_BUFFER_SIZE + LWS_SEND_BUFFER_POSTPADDING ];
	static const unsigned char imcp_signature[] = {'I', 'M', 'C', 'P'};
	ssize_t recv_len = 0;
	int n = -1;
	int addrlen = 0;
	char *newbuf = "Hi websock Client";
	int data_retry_count = 0;
   int i = 0, if_count = -1, drop_packet = 0;
	char *addr;

   if_count = get_interfaces();
   if(if_count == -1)
   {
      printf("Err: Packet looping...Not dropping any packet\n");
   }

	udp_running = 1;
	if ((fd = socket(AF_INET, SOCK_DGRAM, 0)) < 0)
	{
		perror("cannot create socket");
		udp_running = 0;
		return NULL;
	}
	g_udpSockFd = fd;

	memset((char *)&nms_addr, 0, sizeof(nms_addr));
	
	nms_addr.sin_family = AF_INET;
	nms_addr.sin_addr.s_addr = (inet_addr("255.255.255.255"));
	nms_addr.sin_port = htons(0xdede);
	addrlen = sizeof(nms_addr);

	int bcast = 1;
	if((setsockopt(fd, SOL_SOCKET, SO_BROADCAST, &bcast, sizeof(bcast))) < 0)
        {
		perror("SO_BROADCAST");
		udp_running = 0;
		return NULL;
        }

   struct ifreq ifr;

   memset(&ifr, 0, sizeof(ifr));
   snprintf(ifr.ifr_name, sizeof(ifr.ifr_name), mgmt_gw_interface_name);
   if((setsockopt(fd, SOL_SOCKET, SO_BINDTODEVICE, (void *)&ifr, sizeof(ifr))<0))
   {
      perror("SO_BINDTODEVICE");
      udp_running = 0;
      return NULL;
   }

	if (bind(fd, (struct sockaddr *)&nms_addr, sizeof(nms_addr)) < 0)
	{
		perror("bind failed");
		close(fd);
		udp_running = 0;
		return NULL;
	}
	memset((char *)&local_addr, 0, sizeof(local_addr));
	
	gUdprx = 1;
	while(1)
	{
		recv_len = recvfrom(fd, &recv_buf[LWS_SEND_BUFFER_PREPADDING], 1024, 0, (struct sockaddr *)&nms_addr, &addrlen);
		if(recv_len < 0)
		{
			perror("recvfrom()");
			continue;
		}
		addr = inet_ntoa(nms_addr.sin_addr);

		if(memcmp(&recv_buf[LWS_SEND_BUFFER_PREPADDING], imcp_signature, IMCP_SIGNATURE_SIZE))
		{
			continue;
		}

      for(i = 0; i < if_count ; i++)
      {
         if(!strcmp(addr, hosts[i].host))
         {
            drop_packet = 1;
         }
      }

      if(drop_packet)
      {
         drop_packet = 0;
         continue;
      }
      /*For Testing Purpose- Uncomment following block and assign ip of mesh node*/
#if 0
		char *tmp_addr = "192.168.0.21";
      if(!strcmp(addr, tmp_addr))
      {
         continue;
      }
      tmp_addr = "192.168.50.127";
      if(!strcmp(addr, tmp_addr))
      {
         continue;
      }
#endif


		data_retry_count = 0;

		do{
			if(data_retry_count == 500)
			{
				break;
			}
			data_retry_count++;
			usleep(1000);
		}while(lws_send_pipe_choked(g_wsi));

		if(data_retry_count != 500)
		{
			n = lws_write(g_wsi, &recv_buf[LWS_SEND_BUFFER_PREPADDING], recv_len, LWS_WRITE_BINARY);
			if (n < 0)
			{
				perror("lws_write()");
			}
		}
	}
}

char *path = NULL;
int g_use_ssl = 0;
int port = 80;
void * web_sock_server_fun()
{
	struct lws_context_creation_info info;
	const char *iface = NULL;
	int uid = -1, gid = -1;
	int use_ssl = g_use_ssl;
	int opts = 0;
	int n = 0;
	int ret = 0;
	
	ret = pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, NULL);
	if(ret)
	{
		perror("pthread_setcancelstate()");
	}

	ret = pthread_setcanceltype(PTHREAD_CANCEL_ASYNCHRONOUS, NULL);
	if(ret)
	{
		perror("pthread_setcanceltype()");
	}

	memset(&info, 0, sizeof info);
	info.port = port;

	info.iface = iface;
	info.protocols = protocols;
	info.options = 0;
	if(use_ssl)
	{
		info.ssl_cert_filepath = "./domain.crt";
		info.ssl_private_key_filepath = "./domain.key";
		info.ssl_ca_filepath = "./domain.crt";
		//info.options |= LWS_SERVER_OPTION_DO_SSL_GLOBAL_INIT;
	}
	else
	{
		info.ssl_cert_filepath = NULL;
		info.ssl_private_key_filepath = NULL;
	}

	info.gid = gid;
	info.uid = uid;
	info.timeout_secs = 50;

	context = lws_create_context(&info);
	printf("Context Created sucessfully\n");
	if (context == NULL)
	{
		lwsl_err("libwebsocket init failed\n");
		return NULL;
	}

	while ( n >= 0 )
	{
		n = lws_service(context, 50);
	}

	lws_context_destroy(context);
	close(fd);
}

int main(int argc, char **argv)
{

	int ret = 0 ;
	pthread_t web_sock, udp_sock;
	
	if(argc < 3)
	{
		printf("Usage: websock_server <use_ssl: 0/1> <mgmt-gw-interface-name>\n");
		return 0;
	}

	g_use_ssl = atoi(argv[1]);
	if(g_use_ssl > 0)
	{
		g_use_ssl = 1;
		port = 443;
		path = "localhost:443";
	}
	else 
	{
		g_use_ssl = 0;
		port = 80;
		path = "localhost:80";
	}

	strcpy(mgmt_gw_interface_name, argv[2]);
	printf("USING MGMT_GW_INTERFACE_NAME:%s\n", mgmt_gw_interface_name);

	ret = pthread_create(&web_sock, NULL, web_sock_server_fun, NULL);
	if(ret < 0)
	{
		perror("pthread_create()");
		return -1;
	}
	pthread_join(web_sock, NULL);
	return 0;
}

int get_interfaces()
{
   struct ifaddrs *ifaddr, *ifa;
	int family, s;
	struct sockaddr_in *temp_sock;
   int i = 0;
	if(getifaddrs(&ifaddr) == -1)
	{
		perror("getifaddrs()");
		return -1;
	}

	for(ifa = ifaddr; ifa != NULL; ifa = ifa->ifa_next)
	{
		if(ifa->ifa_addr == NULL)
  		 	continue;

		s = getnameinfo(ifa->ifa_addr, sizeof(struct sockaddr_in), hosts[i].host, NI_MAXHOST, NULL, 0, NI_NUMERICHOST);
      if(s)
         continue;

      temp_sock = (struct sockaddr_in *)(ifa->ifa_addr);
      strcpy(hosts[i].host, inet_ntoa(temp_sock->sin_addr)); 
      printf("Interface: <%s>\tAddress: <%s>\n", ifa->ifa_name, hosts[i].host);
      i++;
   }
   return i;
}
