
#include <stdio.h>
#include "wssocket.h"


void display_pkt(char*msg, int len){
    int i = 0;
    al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MGMT_GW: FLOW: entering %s:%d\n", __func__, __LINE__);
    while(i<len) {
        if (i%8 == 0)
            fprintf(stderr,"\n");
        fprintf(stderr," 0x%.2x ,",*(msg+i) & 0xff );
        i++;
    }
    fprintf(stderr,"\n");
    al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MGMT_GW: FLOW: exit %s:%d\n", __func__, __LINE__);
}

