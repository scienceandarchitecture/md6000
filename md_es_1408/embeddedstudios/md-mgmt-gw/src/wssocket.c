
#include <signal.h>
#include <sys/time.h>
#include "wssocket.h"
#include "udpsocket.h"
#include "util.h"

int force_exit = 0;
int status = 0;
struct libwebsocket_pollargs *pa;
struct pollfd pollfds[2];
int nms_conn_status;
#define EXTERNAL_POLL
#define GW_NOT_CONNECTED 			-1
#define GW_CONNECTED				1
#define WS_SOCKET_WAIT_MAX_COUNT	500		//10 msec per unit,so (10*500) = 5 sec 
/* imcp protocol */

struct libwebsocket_context *context;
    static int
callback_imcp(struct libwebsocket_context *this,
        struct libwebsocket *wsi,
        enum libwebsocket_callback_reasons reason,
        void *user, void *in, size_t len)
{
    switch (reason) {
        case LWS_CALLBACK_ESTABLISHED:
            al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MGMT_GW : INFO : LWS_CALLBACK_ESTABLISHED %s:%d\n",__func__, __LINE__);
            fprintf(stderr, "callback_imcp: LWS_CALLBACK_ESTABLISHED\n");
            break;

        case LWS_CALLBACK_CLIENT_CONNECTION_ERROR:
            al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MGMT_GW : ERROR :LWS_CALLBACK_CLIENT_CONNECTION_ERROR %s:%d\n",__func__, __LINE__);
            status = 0;
			nms_conn_status = GW_NOT_CONNECTED;
            usleep(NMS_RECONNECTION_TIMEOUT);
            break;

        case LWS_CALLBACK_CLIENT_FILTER_PRE_ESTABLISH:
            fprintf(stderr, "callback_imcp: LWS_CALLBACK_CLIENT_FILTER_PRE_ESTABLISH\n");
            break;

        case LWS_CALLBACK_CLIENT_ESTABLISHED:
            status = 1;
            fprintf(stderr, "callback_imcp: LWS_CALLBACK_CLIENT_ESTABLISHED\n");
            break;

        case LWS_CALLBACK_CLOSED:
            fprintf(stderr, "callback_imcp: LWS_CALLBACK_CLOSED\n");
			nms_conn_status = GW_NOT_CONNECTED;
			status = 0;
			//	usleep(NMS_RECONNECTION_TIMEOUT);
            break;

        case LWS_CALLBACK_CLOSED_HTTP:
            fprintf(stderr, "callback_imcp: LWS_CALLBACK_CLOSED_HTTP\n");
            break;

        case LWS_CALLBACK_RECEIVE:
            fprintf(stderr, "callback_imcp: LWS_CALLBACK_RECEIVE\n");
            break;

        case LWS_CALLBACK_CLIENT_RECEIVE:
            fprintf(stderr, "callback_imcp: LWS_CALLBACK_CLIENT_RECEIVE\n");
            //((char *)in)[len] = '\0';
            //fprintf(stderr, "rx %d : '%s'\n", (int)len, (char *)in);
				printf("Message from Websock Server:%s\n", in);
#ifdef MGMT_GW_DEBUG	
            display_pkt(in, len);
#endif
            send_udpbroadcast(MESH_IMCP_TX_PORT, in , len);
			break;
        case LWS_CALLBACK_CLIENT_RECEIVE_PONG:
            fprintf(stderr, "callback_imcp: LWS_CALLBACK_CLIENT_RECEIVE_PONG\n");
            break;

        case LWS_CALLBACK_CLIENT_WRITEABLE:
            //fprintf(stderr, "callback_imcp: LWS_CALLBACK_CLIENT_WRITEABLE\n");
            break;

        case LWS_CALLBACK_SERVER_WRITEABLE:
            fprintf(stderr, "callback_imcp: LWS_CALLBACK_SERVER_WRITEABLE\n");
            break;

        case LWS_CALLBACK_HTTP:
            fprintf(stderr, "callback_imcp: LWS_CALLBACK_HTTP\n");
            break;

        case LWS_CALLBACK_HTTP_BODY:
            fprintf(stderr, "callback_imcp: LWS_CALLBACK_HTTP\n");
            break;

        case LWS_CALLBACK_HTTP_BODY_COMPLETION:
            fprintf(stderr, "callback_imcp: LWS_CALLBACK_HTTP_BODY_COMPLETION\n");
            break;

        case LWS_CALLBACK_HTTP_FILE_COMPLETION:
            fprintf(stderr, "callback_imcp: LWS_CALLBACK_HTTP_FILE_COMPLETION\n");
            break;

        case LWS_CALLBACK_HTTP_WRITEABLE:
            fprintf(stderr, "callback_imcp: LWS_CALLBACK_HTTP_WRITEABLE\n");
            break;

        case LWS_CALLBACK_FILTER_NETWORK_CONNECTION:
            fprintf(stderr, "callback_imcp: LWS_CALLBACK_FILTER_NETWORK_CONNECTION\n");
            break;

        case LWS_CALLBACK_FILTER_HTTP_CONNECTION:
            fprintf(stderr, "callback_imcp: LWS_CALLBACK_FILTER_HTTP_CONNECTION\n");
            break;

        case LWS_CALLBACK_SERVER_NEW_CLIENT_INSTANTIATED:
            fprintf(stderr, "callback_imcp: LWS_CALLBACK_SERVER_NEW_CLIENT_INSTANTIATED\n");
            break;

        case LWS_CALLBACK_FILTER_PROTOCOL_CONNECTION:
            fprintf(stderr, "callback_imcp: LWS_CALLBACK_FILTER_PROTOCOL_CONNECTION\n");
            break;

        case LWS_CALLBACK_OPENSSL_LOAD_EXTRA_CLIENT_VERIFY_CERTS:
            fprintf(stderr, "callback_imcp: LWS_CALLBACK_OPENSSL_LOAD_EXTRA_CLIENT_VERIFY_CERTS\n");
            break;

        case LWS_CALLBACK_OPENSSL_LOAD_EXTRA_SERVER_VERIFY_CERTS:
            fprintf(stderr, "callback_imcp: LWS_CALLBACK_OPENSSL_LOAD_EXTRA_SERVER_VERIFY_CERTS\n");
            break;

        case LWS_CALLBACK_OPENSSL_PERFORM_CLIENT_CERT_VERIFICATION:
            fprintf(stderr, "callback_imcp: LWS_CALLBACK_OPENSSL_PERFORM_CLIENT_CERT_VERIFICATION\n");
            break;

        case LWS_CALLBACK_CLIENT_APPEND_HANDSHAKE_HEADER:
            fprintf(stderr, "callback_imcp: LWS_CALLBACK_CLIENT_APPEND_HANDSHAKE_HEADER\n");
            break;

        case LWS_CALLBACK_CONFIRM_EXTENSION_OKAY:
            fprintf(stderr, "callback_imcp: LWS_CALLBACK_CONFIRM_EXTENSION_OKAY\n");
            break;

        case LWS_CALLBACK_CLIENT_CONFIRM_EXTENSION_SUPPORTED:
            fprintf(stderr, "callback_imcp: LWS_CALLBACK_CLIENT_CONFIRM_EXTENSION_SUPPORTED\n");
            break;

        case LWS_CALLBACK_PROTOCOL_INIT:
            fprintf(stderr, "callback_imcp: LWS_CALLBACK_PROTOCOL_INIT\n");
            break;

        case LWS_CALLBACK_PROTOCOL_DESTROY:
            al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MGMT_GW : INFO: LWS_CALLBACK_PROTOCOL_DESTROY %s:%d\n",__func__, __LINE__);
			   if( recreate_context() < 0)
            {
               fprintf(stderr, "recreate_context failed\n");
            }
            fprintf(stderr, "callback_imcp: LWS_CALLBACK_PROTOCOL_DESTROY\n");
            break;

        case LWS_CALLBACK_WSI_CREATE: /* always protocol[0] */
            fprintf(stderr, "callback_imcp: LWS_CALLBACK_WSI_CREATE\n");
            break;

        case LWS_CALLBACK_WSI_DESTROY: /* always protocol[0] */
            al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MGMT_GW: INFO: LWS_CALLBACK_WSI_DESTROY%s:%d\n",__func__, __LINE__);
            fprintf(stderr, "callback_imcp: LWS_CALLBACK_WSI_DESTROY\n");
				status = 0;
		    	nms_conn_status = GW_NOT_CONNECTED;
	        	usleep(NMS_RECONNECTION_TIMEOUT);
            break;

        case LWS_CALLBACK_GET_THREAD_ID:
            //fprintf(stderr, "callback_imcp: LWS_CALLBACK_GET_THREAD_ID\n");
            break;

            /* external poll() management support */
        case LWS_CALLBACK_ADD_POLL_FD:
				fprintf(stderr, "callback_imcp: LWS_CALLBACK_ADD_POLL_FD \n");
            pa = (struct libwebsocket_pollargs *)in;
            fprintf(stderr, "LWS_CALLBACK_ADD_POLL_FD\n");
            pollfds[1].fd = pa->fd;
            pollfds[1].events = pa->events;
            pollfds[1].revents = 0;
            break;

        case LWS_CALLBACK_DEL_POLL_FD:
            fprintf(stderr, "callback_imcp: LWS_CALLBACK_DEL_POLL_FD\n");
            break;

        case LWS_CALLBACK_CHANGE_MODE_POLL_FD:
            //fprintf(stderr, "#callback_imcp: LWS_CALLBACK_CHANGE_MODE_POLL_FD\n");
            pa = (struct libwebsocket_pollargs *)in;
            pollfds[1].events = pa->events;
            break;


        case LWS_CALLBACK_LOCK_POLL:
            //fprintf(stderr, "callback_imcp: LWS_CALLBACK_LOCK_POLL\n");
            break;

        case LWS_CALLBACK_UNLOCK_POLL:
            //fprintf(stderr, "callback_imcp: LWS_CALLBACK_UNLOCK_POLL\n");
            break;

        case LWS_CALLBACK_OPENSSL_CONTEXT_REQUIRES_PRIVATE_KEY:
            fprintf(stderr, "callback_imcp: LWS_CALLBACK_OPENSSL_CONTEXT_REQUIRES_PRIVATE_KEY\n");
            break;
        default:
				printf("reason = %d\n", reason);
            break;
    }
    return 0;
}

void sighandler(int sig) {
    force_exit = 1;
}

/* list of supported protocols and callbacks */
static struct libwebsocket_protocols protocols[] = {
    {
        "mgmt-gw-imcp",
        callback_imcp,
        0,
        IMCP_RECEIVE_BUF_SIZE,
    },
    { NULL, NULL, 0, 0 } /* end */
};

int recreate_context()
{
	struct lws_context_creation_info info;
	mgnt_gw_input_params_t *pmgnt_gw_input_params;
	int ret_val = 0;
	pmgnt_gw_input_params   = NULL;
   
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MGMT_GW: FLOW: entering %s:%d\n", __func__, __LINE__);
   
   pmgnt_gw_input_params   = (mgnt_gw_input_params_t *)malloc(sizeof(mgnt_gw_input_params_t));
   if(pmgnt_gw_input_params == NULL)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MGMT_GW: ERROR: Malloc Failed %s:%d\n",__func__, __LINE__);
      perror("malloc()");
      return -1;
   }

	ret_val = read_mngt_gw_config(pmgnt_gw_input_params);
	if(ret_val == GW_FAIL)
	{
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MGMT_GW: ERROR: Reading Configuration Failed %s:%d\n",__func__, __LINE__);
      printf("MGMT_GW\tread_mngt_gw_config\n");
      free(pmgnt_gw_input_params);
      return 0;
	}
	memset(&info, 0, sizeof info);

	info.gid = -1;
	info.uid = -1;
   info.options = 0;
   info.port = CONTEXT_PORT_NO_LISTEN;
   info.iface = NULL;
   info.protocols = protocols;
   /* Security Certificates Settings */
   if ( pmgnt_gw_input_params->ssl_type    == NMS_CON_TYPE_SECURE )
   {

      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MGMT_GW: INFO:  MGMT-GW: SSL is enabled %s:%d\n",__func__, __LINE__);
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MGMT_GW: INFO: certificate_filepath:%s\n \
            private_key_file_path:%s \nCA_Certicate_filepath:%s %s:%d\n", \
            pmgnt_gw_input_params->ca_cert_filepath, pmgnt_gw_input_params->client_key_filepath, \ 
            pmgnt_gw_input_params->ca_cert_filepath, __func__, __LINE__);

   	info.ssl_cert_filepath = pmgnt_gw_input_params->ca_cert_filepath;
      info.ssl_private_key_filepath = pmgnt_gw_input_params->client_key_filepath;
      info.ssl_ca_filepath = pmgnt_gw_input_params->ca_cert_filepath;

      printf("MGMT_GW:\tInput CA Certificate            : %s\n",info.ssl_ca_filepath);
      printf("MGMT_GW:\tInput Client Certificate          : %s\n",info.ssl_cert_filepath);
      printf("MGMT_GW:\tInput Client Key                 : %s\n",info.ssl_private_key_filepath);

   }
   context = libwebsocket_create_context(&info);
   if ( context == NULL )
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MGMT_GW: ERROR: Context recreation failed %s:%d\n",__func__, __LINE__);
		free(pmgnt_gw_input_params);
      return -1;
   }
	free(pmgnt_gw_input_params);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MGMT_GW: FLOW: exit %s:%d\n", __func__, __LINE__);
   return 0;
}

int handle_connection(struct lws_context_creation_info *info,mgnt_gw_input_params_t *pmgnt_gw_input_params)
{
	struct libwebsocket *wsi;
	char ads_port[256];
	int oldus = 0;
	int rate_us = 250000;
	int n;
	char *address ;
	int  port ; 
	int use_ssl ;
	int bytes_received=0;
	int sent_bytes=0;
	int data_retry_count = 0;
	struct timeval tv;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MGMT_GW: FLOW: entering interface:%s, server_addr:%s, server_url:%s, port_num:%d %s:%d\n", 
         pmgnt_gw_input_params->interface, pmgnt_gw_input_params->nms_server_addr, 
         pmgnt_gw_input_params->nms_server_url, pmgnt_gw_input_params->nms_server_port_num, __func__, __LINE__);

	address = pmgnt_gw_input_params->nms_server_addr;
	port 	= pmgnt_gw_input_params->nms_server_port_num;
	use_ssl	= pmgnt_gw_input_params->ssl_type;

	unsigned char buf[LWS_SEND_BUFFER_PRE_PADDING + IMCP_RECEIVE_BUF_SIZE + 
			LWS_SEND_BUFFER_POST_PADDING];
	char *p =(char*) &buf[LWS_SEND_BUFFER_PRE_PADDING];

    info->protocols = protocols;

	/* get upd port as cmd parameter */
	pollfds[0].fd = start_udpserver(MESH_IMCP_RX_PORT);
	if (pollfds[0].fd == -1) {
		lwsl_err(" udp server start failed on 57055\n");
		goto bail;
	}

	pollfds[0].events = POLLIN;
	signal(SIGINT, sighandler);

	n = 0;

	/* we are in client mode */

	address[strlen(address)] = '\0';
	sprintf(ads_port, "%s:%u", address, port & 65535);
	nms_conn_status = GW_NOT_CONNECTED;
	printf("##### Client connecting to %s:%u.... ads_port = %s\n", address, port, ads_port);

	context = libwebsocket_create_context(info);
	if ( context == NULL )
	{
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MGMT_GW: ERROR: Context creation Failed %s:%d\n",__func__, __LINE__);
		 printf("MGMT_GW:\tFailed to create websocket library context\n");
		 return -1;
	}

	while ((!force_exit) ) {
		if ( nms_conn_status == GW_NOT_CONNECTED )
		{   
         al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MGMT_GW: INFO: Connection is lost with Server or Not yet established...\
               Trying to connect with NMS server %s:%d\n",__func__, __LINE__);

			printf("MGMT_GW\t: Info : Trying to connect with NMS Server socket \n");
			wsi = NULL;
			wsi = libwebsocket_client_connect(context, address,
									port, use_ssl, "/"/*address*/, /*address*/ads_port ,
									/*ads_port*/ NULL, protocols->name, -1);

			if ( wsi == NULL )
			{
               al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MGMT_GW: ERROR: Failed to connect to Server %s:%d\n",__func__, __LINE__);
					printf("\nMGMT_GW\t: Error :Failed to connect to NMS Server ,trying to reconnect ...\n");
					usleep(NMS_RECONNECTION_TIMEOUT);
			}
			else
			{
               al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MGMT_GW: INFO:  Connection with GW server established successfully %s:%d\n",__func__, __LINE__);
					printf("MGMT_GW\t: Info : Connection with NMS Server socket successfully .Websocket handshaking inprogress.. \n");
					nms_conn_status = GW_CONNECTED;
					usleep(10000);
			}
		}
		else	
		{
        	gettimeofday(&tv, NULL);
			if (((unsigned int)tv.tv_usec - oldus) > (unsigned int)rate_us) {
				libwebsocket_callback_on_writable_all_protocol(&protocols[0]);
				oldus = tv.tv_usec;
      }
#ifdef EXTERNAL_POLL
        if (status ){
		//	printf("MGMT_GW\t: Info : Listening on webscoket and udp socket for data (status : %d)\n",status);
            pollfds[0].events = POLLIN;
            pollfds[1].events = POLLIN;
            n = poll(pollfds, 2, 50);
            if (n < 0)
                continue;

			/* Check for any IMCP packets from Dragon Node */
            if (pollfds[0].revents & POLLIN)
			{
				bytes_received=0;
                bytes_received = read(pollfds[0].fd, p, IMCP_RECEIVE_BUF_SIZE);
                printf("pkt received via UDP socket : %d\nBuf:%s\n", bytes_received, p);
				if ( bytes_received > 0 )
				{
               al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MGMT_GW: INFO: UDP packet received %s:%d\n",__func__, __LINE__);
					sent_bytes = 0;
					printf("\bSending %d bytes to the NMS server \n",bytes_received);
					/* Check the web socket is avaliable for write operation */
					data_retry_count = 0;
					do
					{
						printf(".");
						if ( data_retry_count == WS_SOCKET_WAIT_MAX_COUNT )
						{
                     al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MGMT_GW: INFO: Websocket is not available for write operations %s:%d\n",__func__, __LINE__);
							/* web socket is not avaliable for WS_SOCKET_WAIT_MAX_COUNT times 
							   performing websocket reconnection */
							/* NMS Connection State to not connected */
							printf("\nMGMT_GW\t: Info : WebSocket not avaliable for write operation for long time,closing current session\n");
							nms_conn_status = GW_NOT_CONNECTED;
							status = 0;
							/* Close the current client websocket connection */
							//libwebsocket_close_and_free_session(context,wsi,LWS_CLOSE_STATUS_NOSTATUS);
							libwebsocket_context_destroy(context);
							/* clean  websocket polling reevents */
							pollfds[1].revents = 0;
							usleep(NMS_RECONNECTION_TIMEOUT);
							break;
						}
						data_retry_count++;
						usleep(1000*10);
				    }while(lws_send_pipe_choked(wsi));

					printf("\n");
#ifdef MGMT_GW_DEBUG	
					display_pkt(p, bytes_received);
#endif
					if ( data_retry_count < WS_SOCKET_WAIT_MAX_COUNT )
					{
						printf("UDP from configd: %s\n", p);
						sent_bytes = libwebsocket_write(wsi,(unsigned char*)p,bytes_received,LWS_WRITE_BINARY);
						if ( sent_bytes == -1 )
						{
							/* NMS Connection State to not connected */
							nms_conn_status = GW_NOT_CONNECTED;
							status = 0;
							/* Close the current client websocket connection */
							//libwebsocket_close_and_free_session(context,wsi,LWS_CLOSE_STATUS_NOSTATUS);
							libwebsocket_context_destroy(context);
							/* clean  websocket polling reevents */
							pollfds[1].revents = 0;
							printf("\nMGMT_GW\t: Error in sending the bytes NMS \n");
							usleep(NMS_RECONNECTION_TIMEOUT);
						}
						else
						{
							printf("\nRequested bytes : %d , Actual Sent bytes : %d\n",bytes_received,sent_bytes);
						}
					}
				}
				else
				{
               al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MGMT_GW: ERROR: Invalid byte count received...Bytes_received:%d %s:%d\n", bytes_received, __func__, __LINE__);
					printf("\nUDP Invalid byte count \n");
				}
                pollfds[0].revents = 0;
            }

            if (pollfds[1].revents & POLLIN )
			{
            al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MGMT_GW: INFO: Data received on websocket calling service %s:%d\n",__func__, __LINE__);
				printf("MGMT_GW\t: Info : Data received on the websocket calling service \n");
                if (libwebsocket_service_fd(context,
                            &pollfds[1]) < 0){

                    printf("Error on websocket receive \n");
                    goto bail;
                }
                pollfds[1].revents = 0;
            }
        }else
#endif
            n = libwebsocket_service(context, 50);
		}
    }

bail:
    libwebsocket_context_destroy(context);
    close(pollfds[0].fd);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MGMT_GW: FLOW: exit%s:%d\n", __func__, __LINE__);
	return 0;
}
