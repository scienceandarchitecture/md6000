#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <string.h>
#include <assert.h>

#include <syslog.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>


#include "libwebsockets.h"
#include "util.h"
#include "wssocket.h"
#include "al.h"
#include "torna_ddi.h"

#define _TORNA_VERSION_MAJOR_ 		3
#define _TORNA_VERSION_MINOR_ 		3
#define _TORNA_VERSION_VARIANT_ 	3

#define TDDI_FILE_NAME				"/dev/tddi"

#define EXTERNAL_POLL

/* NMS Communication default settings */

#define MAX_INPUT_ARGS				5

#define NMS_SERVER_SIMPLE_PORT		80
#define NMS_SERVER_SECURE_PORT		443

#define NMS_CON_TYPE_SIMPLE			0

static void _wait_for_ap_to_start();

struct config_data_ctx
{
	//unsigned char node_type;
	unsigned char	mgmt_activate_flag;
	unsigned char	proxy_activate_flag;

	unsigned short  mgmt_uri_len;
	unsigned short  syslog_uri_len;

	unsigned short  ca_cert_path_len;
	unsigned short  client_cert_path_len;
	unsigned short  client_key_path_len;
	unsigned short  passphrase_path_len;
	unsigned short  proxy_uri_len;

	unsigned char	mgmt_uri[256];
	unsigned char	syslog_uri[256];

	unsigned char	client_cert_path[128];
	unsigned char	ca_cert_path[128];
	unsigned char	client_key_path[128];
	unsigned char	passphrase_path[256];
	unsigned char	proxy_uri[256];
};

typedef struct config_data_ctx  config_data_t;

int read_config_file(config_data_t  *config_data, unsigned char *config_filename)
{
	FILE *fptr;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MGMT_GW: FLOW: enter %s:%d\n", __func__, __LINE__);
	fptr = fopen (config_filename,"rb");
	if ( fptr == NULL )
	{
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MGMT_GW: ERROR: config file open failed: %s %s:%d\n", config_filename, __func__, __LINE__);
		return -1;
	}
	fread(config_data, sizeof(config_data_t), 1, fptr);
	fclose(fptr);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MGMT_GW: FLOW: exit %s:%d\n", __func__, __LINE__);
	return 0;
}

int is_root_node(void )
{
	int ret_val = GW_SUCCESS;
	int fd ;
	tddi_packet_t*		packet;
	unsigned char		 buffer[sizeof(tddi_packet_t)+sizeof(int)];
	int node_type;
	int tddi_pkt_size;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MGMT_GW: FLOW: enter %s:%d\n", __func__, __LINE__);
	packet = (tddi_packet_t *)buffer;			

	fd  = open(TDDI_FILE_NAME,O_RDWR);
	if(fd == -1) {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MGMT_GW: ERROR:  TDDI device open failed:%s fd:%d %s:%d\n",TDDI_FILE_NAME,fd,  __func__, __LINE__);
		return GW_FAIL;
	}

	tddi_pkt_size = sizeof(tddi_packet_t);

	memset(packet,0,tddi_pkt_size);

	/* Create node type request packet */
	packet->signature	= TDDI_PACKET_SIGNATURE;
	packet->version	  = TDDI_CURRENT_VERSION;
	packet->data_size	= 0;
	packet->type		  = TDDI_PACKET_TYPE_REQUEST;
	packet->sub_type	 = TDDI_REQUEST_TYPE_GET_NODE_TYPE;

	/* write node type request */
	ret_val = write(fd,buffer,sizeof(tddi_packet_t));
	if ( ret_val != sizeof(tddi_packet_t))
	{
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MGMT_GW: ERROR: Write failed fd:%d %s:%d\n", fd, __func__, __LINE__);
		close(fd);
		return GW_FAIL;
	}

	lseek(fd,0,SEEK_CUR);

	/* read response packet */
	memset(buffer,0x00,sizeof(buffer));
	ret_val = read(fd,buffer,sizeof(tddi_packet_t)+sizeof(int));
	if ( ret_val != (sizeof(tddi_packet_t)+ sizeof(int)))
	{
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MGMT_GW: ERROR: READ failed %s:%d\n",__func__, __LINE__);
		close(fd);
		return GW_FAIL;
	}

	/* close tddi device file descriptor */
	close(fd);

	node_type =*((int*)(&buffer[tddi_pkt_size]));
	printf("Node Type Value : %d\n",node_type);
	if ( node_type == 0 )
	{
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MGMT_GW: INFO: Node Type:%d (ROOT NODE) %s:%d\n", node_type, __func__, __LINE__);
		ret_val = GW_SUCCESS;
		printf("Device Node Type : Root Node \n");
	}
	else
	{
       al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MGMT_GW: INFO: Node Type:%d (RELAY NODE) %s:%d \n", node_type, __func__, __LINE__);
		ret_val = GW_FAIL;
		printf("Device Node Type: AP Node \n");
	}
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MGMT_GW: FLOW: exit %s:%d\n", __func__, __LINE__);
	return ret_val;
}

int read_mngt_gw_config(mgnt_gw_input_params_t *pmgnt_gw_input_params)
{
	int ret_val = GW_SUCCESS;
	unsigned int mgmt_gw_enable_flag = 0;
	char conn_type[5], path[32];
	char *server_url, *gw_certificates;
	int  handle;
	int mgmt_gw_enable;
	char cert_path[512] = {0}, key_path[512] = {0}, ca_cert_path[512] = {0};
  
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MGMT_GW: FLOW: entering %s:%d\n", __func__, __LINE__);
  	if (pmgnt_gw_input_params == NULL)
	{
		return GW_FAIL;
	}

	handle = libalconf_read_config_data();
	if (handle == 0)
	{
           al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MGMT_GW: ERROR: Invalid al_conf_handle %s:%d \n",__func__, __LINE__);
			  return GW_FAIL;
	}

	server_url = al_conf_get_mgmt_gw_addr(AL_CONTEXT handle);

	pmgnt_gw_input_params->mgmt_gw_enable = al_conf_get_mgmt_gw_enable(AL_CONTEXT handle);
	 sscanf(server_url,"%5[^:]://%99[^/]/%99[^\n]",conn_type,
				pmgnt_gw_input_params->nms_server_addr, path);

	if(!strcmp("ws",conn_type) || !strcmp("http",conn_type))
	{
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MGMT_GW: INFO: Connection type: %s %s:%d\n",conn_type,  __func__, __LINE__);
		/* Connetion Type Simple */
		printf("Connection Type : Simple \n");
		pmgnt_gw_input_params->nms_server_port_num		  = NMS_SERVER_SIMPLE_PORT;
		pmgnt_gw_input_params->ssl_type						= NMS_CON_TYPE_SIMPLE;
	}
	else if (!strcmp("wss",conn_type) || !strcmp("https",conn_type))
	{
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MGMT_GW: INFO: Connectio Type:%s %s:%d\n", conn_type, __func__, __LINE__);
		/* Connetion Type Secure */
		gw_certificates = al_conf_get_mgmt_gw_certificates(AL_CONTEXT handle);
		sscanf(gw_certificates, "%99[^:]:%99[^\n]", pmgnt_gw_input_params->client_cert_filepath, pmgnt_gw_input_params->client_key_filepath);
		strcpy(pmgnt_gw_input_params->ca_cert_filepath, pmgnt_gw_input_params->client_cert_filepath);

		al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"MGMT_GW: INFO: certificate path: %s \nkey path: %s\n \
            ca_certficate_path: %s %s:%d\n", pmgnt_gw_input_params->client_cert_filepath, \
            pmgnt_gw_input_params->client_key_filepath, pmgnt_gw_input_params->ca_cert_filepath, __func__, __LINE__);

		printf("Connection Type : Secure \n");
		pmgnt_gw_input_params->nms_server_port_num		= NMS_SERVER_SECURE_PORT;
		pmgnt_gw_input_params->ssl_type					  = NMS_CON_TYPE_SECURE;
	}
	else
	{
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MGMT_GW: ERROR: Invalid URL for Connection URL:%s %s:%d\n", server_url, __func__, __LINE__);
		pmgnt_gw_input_params->ssl_type						= NMS_CON_TYPE_SIMPLE;
		ret_val = GW_FAIL;
	}

	printf("MGW : Server  URL	  : %s \nMGW : enable	  : %d \n", server_url, pmgnt_gw_input_params->mgmt_gw_enable);
	printf("MGW : Server Host Address : %s \nMGW : Conn_type=%s\n",pmgnt_gw_input_params->nms_server_addr, conn_type);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MGMT_GW: FLOW: exit %s:%d\n", __func__, __LINE__);
	return ret_val;
}

void usage()
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MGMT_GW: FLOW: entering %s:%d\n", __func__, __LINE__);
	printf("\n Usage: ./mgmt-gwd \n"
			"-h <NMS server_address > \n"
			"-u <NMS URL>\n"
			"-i <Communication interface > e.g.: -i mip0 \n"
			"-d : to enable debugging \n");
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MGMT_GW: FLOW: exit%s:%d\n", __func__, __LINE__);
	exit(1);
}

int main(int argc, char*argv[]){

	struct lws_context_creation_info info;
	mgnt_gw_input_params_t *pmgnt_gw_input_params;
	config_data_t config_data;
	int is_iface =0;
	int opt = 0;
	int ret_val = GW_SUCCESS;
	int	filesize = 0;
	FILE *passphraseFptr = NULL;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MGMT_GW: FLOW: enter %s:%d\n", __func__, __LINE__);
	/* Verifying management gateway is running on root node .
		If current node is root node start mgmt-gw ,
		If crrent node is ap node , exit from the mgmt-gw
	 */
	printf("\nMGMT_GW :\t/**********MANAGEMENT GATEWAY STARTED****************/\n");
	_wait_for_ap_to_start();
	if (is_root_node() != GW_SUCCESS)
	{
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MGMT_GW: INFO: RELAY Node...MGMT Only runs on ROOT node %s:%d\n", __func__, __LINE__);
		printf("INFO : Not a root node ,Exiting from the MGW\n");
		return 0;
	}

	pmgnt_gw_input_params	= NULL;
	pmgnt_gw_input_params	= (mgnt_gw_input_params_t *)malloc(sizeof(mgnt_gw_input_params_t));
	if ( pmgnt_gw_input_params == NULL )
	{
		printf("MGMT_GW:\t Error : Failed to allocate memory for input param context\n");
		return GW_FAIL;
	}
	
	ret_val = read_mngt_gw_config(pmgnt_gw_input_params);
	if(ret_val == GW_FAIL)
	{
		printf("MGMT_GW\tread_mngt_gw_config\n");
		free(pmgnt_gw_input_params);
		return 0;
	}
	if ( pmgnt_gw_input_params->mgmt_gw_enable == GW_DEACTIVATED )
	{
		printf("MGMT_GW\t: Deactivated: Shuting down the management gateway on current node \n");
		free(pmgnt_gw_input_params);
		return 0;
	}
	
	lws_set_log_level(LLL_ERR, NULL);
	memset(&info, 0, sizeof info);

	info.gid = -1;
	info.uid = -1;
	info.options = 0;
	info.port = CONTEXT_PORT_NO_LISTEN;		
	info.iface = NULL;

	/* Security Certificates Settings */
	if ( pmgnt_gw_input_params->ssl_type	 == NMS_CON_TYPE_SECURE )
	{
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MGMT_GW: INFO: SSL Enabled...\
            Setting certificates path %s:%d\n", __func__, __LINE__);

		info.ssl_cert_filepath = pmgnt_gw_input_params->ca_cert_filepath;
		info.ssl_private_key_filepath = pmgnt_gw_input_params->client_key_filepath;
		info.ssl_ca_filepath = pmgnt_gw_input_params->ca_cert_filepath;

		printf("MGMT_GW:\tInput CA Certificate				  : %s\n",info.ssl_ca_filepath);
		printf("MGMT_GW:\tInput Client Certificate			 : %s\n",info.ssl_cert_filepath);
		printf("MGMT_GW:\tInput Client Key						: %s\n",info.ssl_private_key_filepath);

  }
	/* Management Gateway Execution Loop */
	handle_connection(&info, pmgnt_gw_input_params);
	free(pmgnt_gw_input_params);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MGMT_GW: FLOW: exit %s:%d\n", __func__, __LINE__);
	return 1;
}

static int _get_ds_netif()
{
	int                       fd;
	tddi_packet_t             packet;
	int                       ret;
	unsigned char             buffer[sizeof(tddi_packet_t) + sizeof(tddi_get_ds_if_response_t) + 128];
	tddi_packet_t             *response_packet;
	tddi_get_ds_if_response_t *response_data;
	char current_if_name[256];

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MGMT_GW: FLOW: entering %s:%d\n", __func__, __LINE__);
	fd = open(TDDI_FILE_NAME, O_RDWR);

	if (fd == -1)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MGMT_GW: ERROR: TDDI File open Failed %s %s:%d\n", TDDI_FILE_NAME, __func__, __LINE__);
		return -1;
	}

	packet.signature = TDDI_PACKET_SIGNATURE;
	packet.version   = TDDI_CURRENT_VERSION;
	packet.data_size = 0;
	packet.type      = TDDI_PACKET_TYPE_REQUEST;
	packet.sub_type  = TDDI_REQUEST_TYPE_GET_DS_IF;

	ret = write(fd, &packet, sizeof(tddi_packet_t));

	if (ret != sizeof(tddi_packet_t))
	{
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MGMT_GW: ERROR: TDDI Write failed fd:%d %s:%d\n", fd, __func__, __LINE__);
		fprintf(stderr, "Error writing to "TDDI_FILE_NAME " ret=%d\n", ret);
		return -1;
	}

	lseek(fd, 0, SEEK_CUR);

	ret = read(fd, buffer, sizeof(buffer));

	if (ret < sizeof(tddi_packet_t) + sizeof(tddi_get_ds_if_response_t))
	{
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MGMT_GW: ERROR: TDDI Read Failed fd:%d %s:%d\n", fd, __func__, __LINE__);
		return -1;
	}

	close(fd);

	response_packet = (tddi_packet_t *)buffer;
	response_data   = (tddi_get_ds_if_response_t *)(buffer + sizeof(tddi_packet_t));

	switch (response_data->response)
	{
		case TDDI_GET_DS_IF_STARTED:
         al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MGMT_GW: INFO: TDDI_GET_DS_IF_STARTED %s:%d\n", __func__, __LINE__);
			memset(current_if_name, 0, 256);
			memcpy(current_if_name, buffer + sizeof(tddi_packet_t) + sizeof(tddi_get_ds_if_response_t), response_data->if_name_length);
			fprintf(stderr, "\nCONFIGD:\tDS net_if is %s\n", current_if_name);
			return 0;

		case TDDI_GET_DS_IF_RESPONSE_SCANNING:
			return -1;

		case TDDI_GET_DS_IF_RESPONSE_NOT_STARTED:
			return -1;
	}

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MGMT_GW: FLOW: exit %s:%d\n", __func__, __LINE__);
	return 0;
}

static void _wait_for_ap_to_start()
{
	int max;
	int ret;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MGMT_GW: FLOW: entering %s:%d\n", __func__, __LINE__);
	max = 6000;
	while (max--)
	{
		ret = _get_ds_netif();
		if (ret != 0)
		{
			sleep(1);
		}
		else
		{
			break;
		}
	}
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MGMT_GW: FLOW: exit %s:%d\n", __func__, __LINE__);
}
