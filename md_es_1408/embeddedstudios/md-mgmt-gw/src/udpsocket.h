
#ifndef UDP_SOCKET_H
#define UDP_SOCKET_H
#include "wssocket.h"
int start_udpserver(int port);
int send_udppacket(char* ip, int port, void *data, int len);
int send_udpbroadcast(int port, void *data, int len);

#endif
