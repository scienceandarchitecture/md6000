#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include "libwebsockets.h"
#include <pthread.h>
#include <sys/types.h>

#define IMCP_RECEIVE_BUF_SIZE 1024
#define LWS_SERVER_OPTION_DO_SSL_GLOBAL_INIT (1<<12)
int fd = 0;
int debug_level = 7;
struct sockaddr_in  nms_addr;

struct lws *g_wsi = NULL;
struct lws_context *context;

#define MGMT_GW_TO_NMS_PORT 0xaaaa

int callback_imcp_server(struct lws *wsi, enum lws_callback_reasons reason,
        void *user, void *in, size_t len);

static struct lws_protocols protocols[] =
{
	{
		"mgmt-gw-imcp",
		callback_imcp_server,
		0,
		IMCP_RECEIVE_BUF_SIZE,
	},
	{ NULL, NULL, 0, 0 } /* terminator */
};

int callback_imcp_server(struct lws *wsi, enum lws_callback_reasons reason,
        void *user, void *in, size_t len)
{
	int n;
	char *buff;
	char client_name[32] = {0}, client_ip[4] = {0};
		
	switch (reason)
	{
		case LWS_CALLBACK_ESTABLISHED:
//			lwsl_info("%s: LWS_CALLBACK_ESTABLISHED\n", __func__);
			printf("LWS_CALLBACK_ESTABLISHED\n");

			lws_get_peer_addresses(wsi, lws_get_socket_fd(wsi),
								client_name, sizeof(client_name),
								client_ip, sizeof(client_ip));

			printf("Client Name = %s \tClient_ip = %s\n", client_name, client_ip);

			g_wsi = (struct lws *)wsi;
			break;

		case LWS_CALLBACK_SERVER_WRITEABLE:
//			lwsl_info("%s: LWS_CALLBACK_SERVER_WRITABLE\n", __func__);
			printf("LWS_CALLBACK_SERVER_WRITABLE\n");
			break;

		case LWS_CALLBACK_RECEIVE:
//			lwsl_info("%s: LWS_CALLBACK_RECEIVE\n", __func__);
			printf("LWS_CALLBACK_RECEIVE\n");
			printf("Message from websock Client:\t%s\n", in);
			buff = (unsigned char *) in;
			printf("Sending to UDP Socket\n");
			nms_addr.sin_family = AF_INET;
			nms_addr.sin_addr.s_addr = (inet_addr("127.0.0.1"));
			nms_addr.sin_port = htons(0xaaaa);
			sendto(fd, buff, strlen(buff), 0, (struct sockaddr *)&nms_addr, sizeof(nms_addr));
			printf("Message sent to UDP port\n");
			break;

		default:
			;
//			lwsl_info("%s: LWS_CALLBACK: Default Case");
	}
	return 0;
}

void * udp_sock_fun()
{
	unsigned char recv_buf[1024];
	ssize_t recv_len = 0;
	int n = -1;
	int addrlen = 0;
	char *newbuf = "Hi websock Client";

	if ((fd = socket(AF_INET, SOCK_DGRAM, 0)) < 0)
	{
		perror("cannot create socket");
		return NULL;
	}
	
	memset((char *)&nms_addr, 0, sizeof(nms_addr));
	
	nms_addr.sin_family = AF_INET;
	nms_addr.sin_addr.s_addr = (inet_addr("127.0.0.1"));
	nms_addr.sin_port = htons(0xbbbb);
	
	addrlen = sizeof(nms_addr);

	if (bind(fd, (struct sockaddr *)&nms_addr, sizeof(nms_addr)) < 0)
	{
		perror("bind failed");
		close(fd);
		return NULL;
	}
	while(1)
	{
		recv_len = recvfrom(fd, recv_buf, 1024, 0, (struct sockaddr *)&nms_addr, &addrlen);
		if(recv_len < 0)
		{
			perror("recvfrom()");
			continue;
		}
		printf("Received a packet from udp: %s\n", recv_buf);
		printf("Writing message to websock client\n");
		n = lws_write(g_wsi, recv_buf, recv_len, LWS_WRITE_TEXT);
		if (n < 0)
		{
			perror("lws_write()");
		}	
	}
}

char *path = NULL;
int g_use_ssl = 0;
int port = 80;
void * web_sock_server_fun()
{
	struct lws_context_creation_info info;
	const char *iface = NULL;
	int uid = -1, gid = -1;
	int use_ssl = g_use_ssl;
	int opts = 0;
	int n = 0;
	int ret = 0;
	
	memset(&info, 0, sizeof info);
	info.port = port;

	info.iface = iface;
	info.protocols = protocols;
	info.options = 0;
	if(use_ssl)
	{
		info.ssl_cert_filepath = "./domain.crt";
		info.ssl_private_key_filepath = "./domain.key";
		info.ssl_ca_filepath = "./domain.crt";
		//info.options |= LWS_SERVER_OPTION_DO_SSL_GLOBAL_INIT;
	}
	else
	{
		info.ssl_cert_filepath = NULL;
		info.ssl_private_key_filepath = NULL;
	}

	info.gid = gid;
	info.uid = uid;
	info.timeout_secs = 50;

	context = lws_create_context(&info);
	printf("Context Created sucessfully\n");
	if (context == NULL)
	{
		lwsl_err("libwebsocket init failed\n");
		return NULL;
	}

	while ( n >= 0 )
	{
		n = lws_service(context, 50);
	}

	lws_context_destroy(context);
	close(fd);
}

int main(int argc, char **argv)
{

	int ret = 0 ;
	pthread_t web_sock, udp_sock;
	
	if(argc < 2)
	{
		printf("Usage: websock_server <use_ssl: 0/1>\n");
		return 0;
	}

	g_use_ssl = atoi(argv[1]);
	if(g_use_ssl > 0)
	{
		g_use_ssl = 1;
		port = 443;
		path = "localhost:443";
	}
	else 
	{
		g_use_ssl = 0;
		port = 80;
		path = "localhost:80";
	} 
	
	ret = pthread_create(&udp_sock, NULL, udp_sock_fun, NULL);
	if(ret < 0)
	{
		perror("pthread_create()");
		return -1;
	}
	
	ret = pthread_create(&web_sock, NULL, web_sock_server_fun, NULL);
	if(ret < 0)
	{
		perror("pthread_create()");
		return -1;
	}
	
	pthread_join(web_sock, NULL);
	pthread_join(udp_sock, NULL);
	return 0;
}
