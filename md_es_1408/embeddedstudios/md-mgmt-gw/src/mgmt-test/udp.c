#include <sys/socket.h>
#include <sys/types.h>
#include <stdio.h>	/* for fprintf */
#include <string.h>	/* for memcpy */
#include <arpa/inet.h>

#define BUFSIZE 1024

int main()
{
  int sockfd; /* socket */
  int portno; /* port to listen on */
  int clientlen; /* byte size of client's address */
  struct sockaddr_in serveraddr; /* server's addr */
  struct sockaddr_in clientaddr; 
  int n = 0;
  char buf[BUFSIZE] = {0};
  char *newbuf = "This is message from UDP server back to websock sever";

  portno = 0xaaaa;
  sockfd = socket(AF_INET, SOCK_DGRAM, 0);
  if (sockfd < 0) 
    printf("ERROR opening socket");

  serveraddr.sin_family = AF_INET;
  serveraddr.sin_addr.s_addr = inet_addr("127.0.0.1");
  serveraddr.sin_port = htons(0xaaaa);

  if (bind(sockfd, (struct sockaddr *) &serveraddr, sizeof(serveraddr)) < 0) 
    perror("ERROR on binding");

  clientlen = sizeof(clientaddr);

  while(1)
  {
	n = recvfrom(sockfd, buf, BUFSIZE, 0,
		 (struct sockaddr *) &clientaddr, &clientlen);
	if (n < 0)
	  printf("ERROR in recvfrom");

	printf("Message from websock server:%s\n", buf);
	printf("Now sending message to websock server\n");
	n = sendto(sockfd, newbuf, strlen(newbuf), 0, 
	       (struct sockaddr *) &clientaddr, clientlen);
    	if (n < 0) 
      	  printf("ERROR in sendto");
  }
}
