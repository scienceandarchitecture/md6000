#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>
#include "libwebsockets.h"

#define IMCP_RECEIVE_BUF_SIZE 1024
#define LWS_SERVER_OPTION_DO_SSL_GLOBAL_INIT (1<<12)
int destroy_flag = 0;
int connection_flag = 0;
int writeable_flag = 0;
int callback_imcp_client(
                         struct lws *wsi,
                         enum lws_callback_reasons reason, void *user,
                         void *in, size_t len);


struct lws_protocols protocols[] =
{
        {
                "mgmt-gw-imcp",
                callback_imcp_client,
                0,
                IMCP_RECEIVE_BUF_SIZE,
        },
        { NULL, NULL, 0, 0 } /* terminator */
};

struct pthread_routine_tool {
    struct lws_context *context;
    struct lws *wsi;
};

int callback_imcp_client(
                         struct lws *wsi,
                         enum lws_callback_reasons reason, void *user,
                         void *in, size_t len)
{
	switch (reason)
	{
        	case LWS_CALLBACK_CLIENT_ESTABLISHED:
            		printf("[Main Service] Connect with server success.\n");
            		connection_flag = 1;
            		break;

		case LWS_CALLBACK_CLIENT_CONNECTION_ERROR:
            		printf("[Main Service] Connect with server error.\n");
            		destroy_flag = 1;
            		connection_flag = 0;
            		break;
		
		case LWS_CALLBACK_CLOSED:
            		printf("[Main Service] LWS_CALLBACK_CLOSED\n");
            		destroy_flag = 1;
            		connection_flag = 0;
            		break;

		case LWS_CALLBACK_CLIENT_RECEIVE:
            		printf("[Main Service] Client recvived:%s\n",(char *)in);
            		break;

		case LWS_CALLBACK_CLIENT_WRITEABLE :
            		printf("[Main Service] On writeable is called. send byebye message\n");
            		websocket_write_back(wsi, "Byebye! See you later", -1);
            		writeable_flag = 1;
            		break;

        	default:
            		break;
	}
	return 0;
}

int websocket_write_back(struct lws *wsi_in, char *str, int str_size_in) 
{
	if (str == NULL || wsi_in == NULL)
        return -1;
	
	int n;
	int len;
	char *out = NULL;

	if (str_size_in < 1) 
        	len = strlen(str);
	else
	        len = str_size_in;


	out = (char *)malloc(sizeof(char)*len);

	memcpy (out, str, len );

	n = lws_write(wsi_in, out, len, LWS_WRITE_TEXT);

	sleep(30);

	free(out);

	return n;

}

void *pthread_routine(void *tool_in)
{
	struct pthread_routine_tool *tool = tool_in;
	printf("this is pthread routine\n");

	while(!connection_flag)
        	sleep(1);

	printf("client is ready. sending message.......\n");
	
	websocket_write_back(tool->wsi, "Hello, Good Day Server", -1);

	lws_callback_on_writable(tool->wsi);
	sleep(10);


}

int main()
{
	struct lws_context *context = NULL;
	struct lws_context_creation_info info;
   	struct lws *wsi = NULL;
	struct pthread_routine_tool tool;
	struct lws_client_connect_info i;

	memset(&info, 0, sizeof info);
    	info.port = CONTEXT_PORT_NO_LISTEN;
    	info.iface = NULL;
    	info.protocols = protocols;
    	info.ssl_cert_filepath = "./domain.crt";
	info.ssl_ca_filepath = "./domain.crt";
    	info.ssl_private_key_filepath = "./domain.key"; 
    	//info.extensions = lws_get_internal_extensions();
    	info.gid = -1;
    	info.uid = -1;
    	info.options = 0;
	info.options |= LWS_SERVER_OPTION_DO_SSL_GLOBAL_INIT;

	printf("[DEBUG]:\t%s:%d\tCreating Context\n", __func__, __LINE__);

	context = lws_create_context(&info);

	if (context == NULL)
	{
        	printf("[Main] context is NULL.\n");
        	return -1;
    	}


	wsi = lws_client_connect(context, "localhost", 2222,0,
            "/","localhost:2222", NULL,
             protocols->name, -1);
	
	if (wsi == NULL)
	{
        	printf("[Main] wsi create error.\n");
        	return -1;
  	}
	
	printf("Debug 2\n");	
	while(!connection_flag)
		sleep(5);
	char *out = "Hello server!";
	printf("wsi created successfully\n");
	lws_write(wsi, out, strlen(out), LWS_WRITE_BINARY);
	lws_callback_on_writable(wsi);
	lws_context_destroy(context);

	return 0;
	
}
