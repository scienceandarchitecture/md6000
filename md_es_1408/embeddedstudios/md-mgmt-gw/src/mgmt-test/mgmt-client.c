#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>
#include "libwebsockets.h"

#define IMCP_RECEIVE_BUF_SIZE 1024

int destroy_flag = 0;
int connection_flag = 0;
int writeable_flag = 0;
int callback_imcp_client(
                         struct lws *wsi,
                         enum lws_callback_reasons reason, void *user,
                         void *in, size_t len);


struct lws_protocols protocols[] =
{
        {
                "mgmt-gw-imcp",
                callback_imcp_client,
                0,
                IMCP_RECEIVE_BUF_SIZE,
        },
        { NULL, NULL, 0, 0 } /* terminator */
};

struct pthread_routine_tool {
    struct lws_context *context;
    struct lws *wsi;
};

int callback_imcp_client(
                         struct lws *wsi,
                         enum lws_callback_reasons reason, void *user,
                         void *in, size_t len)
{
	switch (reason)
	{
        	case LWS_CALLBACK_CLIENT_ESTABLISHED:
            		printf("[Main Service] Connect with server success.\n");
            		connection_flag = 1;
            		break;

		case LWS_CALLBACK_CLIENT_CONNECTION_ERROR:
            		printf("[Main Service] Connect with server error.\n");
            		destroy_flag = 1;
            		connection_flag = 0;
            		break;
		
		case LWS_CALLBACK_CLOSED:
            		printf("[Main Service] LWS_CALLBACK_CLOSED\n");
            		destroy_flag = 1;
            		connection_flag = 0;
            		break;

		case LWS_CALLBACK_CLIENT_RECEIVE:
            		printf("[Main Service] Client recvived:%s\n",(char *)in);
            		break;

		case LWS_CALLBACK_CLIENT_WRITEABLE :
            		printf("[Main Service] On writeable is called. send byebye message\n");
            		websocket_write_back(wsi, "Byebye! See you later", -1);
            		writeable_flag = 1;
            		break;

        	default:
            		break;
	}
	return 0;
}

int websocket_write_back(struct lws *wsi_in, char *str, int str_size_in) 
{
	if (str == NULL || wsi_in == NULL)
        return -1;
	
	int n;
	int len;
	char *out = NULL;

	if (str_size_in < 1) 
        	len = strlen(str);
	else
	        len = str_size_in;


	out = (char *)malloc(sizeof(char)*len);

	memcpy (out, str, len );

	n = lws_write(wsi_in, out, len, LWS_WRITE_TEXT);

	//free(out);

	return n;

}

void *pthread_routine(void *tool_in)
{
	struct pthread_routine_tool *tool = tool_in;
	printf("this is pthread routine\n");

	while(!connection_flag)
        	sleep(1);

	printf("client is ready. sending message.......\n");
	
	websocket_write_back(tool->wsi, "Hello, Good Day Server", -1);

	lws_callback_on_writable(tool->wsi);
	sleep(10);


}

int main(int argc, char** argv)
{
	struct lws_context *context = NULL;
	struct lws_context_creation_info info;
   	struct lws *wsi = NULL;
	struct pthread_routine_tool tool;
	int use_ssl = 0, port = 80;
	char *path = NULL;
	memset(&info, 0, sizeof info);
    	info.port = CONTEXT_PORT_NO_LISTEN;
    	info.iface = NULL;
    	info.protocols = protocols;
    	info.gid = -1;
    	info.uid = -1;
    	info.options = 0;
	
	printf("[DEBUG]:\t%s:%d\tCreating Context\n", __func__, __LINE__);


	if(argc < 2)
	{
		printf ("usage: ./websock_client <use_ssl 0/1>\n");
		return 0;
	}

	use_ssl = 0;
	use_ssl = atoi(argv[1]);
	if(use_ssl > 0)
	{
		use_ssl = 1;
	}

	if(use_ssl)
	{
		printf("Using ssl\n");
		port = 443;
		path = "localhost:443";
		info.ssl_cert_filepath = "./domain.crt";
		info.ssl_ca_filepath = "./domain.crt";
		info.ssl_private_key_filepath = "./domain.key";
	}
	else
	{	port = 80;
		path = "localhost:80";
		info.ssl_cert_filepath = NULL;
		info.ssl_ca_filepath = NULL;
		info.ssl_private_key_filepath = NULL;
	}

	context = lws_create_context(&info);

	if (context == NULL)
	{
        	printf("[Main] context is NULL.\n");
        	return -1;
    	}

	printf("port = %d ssl = %d path = %s\n", port, use_ssl, path);
	wsi = lws_client_connect(context, "localhost", port, use_ssl,
            "/", path, NULL, protocols->name, -1);

	if (wsi == NULL)
	{
        	printf("[Main] wsi create error.\n");
        	return -1;
  	}
	printf("wsi created successfully\n");
	
	tool.wsi = wsi;
	tool.context = context;

	pthread_t pid;
	pthread_create(&pid, NULL, pthread_routine, &tool);
	//while(!destroy_flag)
	while(1)
	{
		lws_service(context, 50);
	}
	lws_context_destroy(context);
	pthread_join(&pid);
	return 0;
	
}
