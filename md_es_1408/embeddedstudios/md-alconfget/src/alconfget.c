/********************************************************************************
* MeshDynamics
* --------------
* File     : alconfget.c
* Comments : Generic alconf field reader
* Created  : 24/10/2017
* Author   : Kusuma D S, Soubhagya Panigrahi
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
********************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <getopt.h>
#include <string.h>
#include <strings.h>
#include "al.h"
#include "al_context.h"
#include "al_impl_context.h"
#include "al_conf.h"
#include "dot11e_conf.h"
#include "auto_start.h"

static int _get_name(int argc, int optind, char **argv, const char *operation, int conf_handle);
static int _get_mesh_id(int argc, int optind, char **argv, const char *operation, int conf_handle);
static int _get_mesh_imcp_key(int argc, int optind, char **argv, const char *operation, int conf_handle);
static int _get_global_dca(int argc, int optind, char **argv, const char *operation, int conf_handle);
static int _get_stay_awake_count(int argc, int optind, char **argv, const char *operation, int conf_handle);
static int _get_bridge_aging(int argc, int optind, char **argv, const char *operation, int conf_handle);
static int _get_reg_domain_code(int argc, int optind, char **argv, const char *operation, int conf_handle);
static int _get_country_code(int argc, int optind, char **argv, const char *operation, int conf_handle);
static int _get_medium_type(int argc, int optind, char **argv, const char *operation, int conf_handle);
static int _get_medium_sub_type(int argc, int optind, char **argv, const char *operation, int conf_handle);
static int _get_use_type(int argc, int optind, char **argv, const char *operation, int conf_handle);
static int _get_channel(int argc, int optind, char **argv, const char *operation, int conf_handle);
static int _get_essid(int argc, int optind, char **argv, const char *operation, int conf_handle);
static int _get_rts(int argc, int optind, char **argv, const char *operation, int conf_handle);
static int _get_frag(int argc, int optind, char **argv, const char *operation, int conf_handle);
static int _get_beacint(int argc, int optind, char **argv, const char *operation, int conf_handle);
static int _get_dca(int argc, int optind, char **argv, const char *operation, int conf_handle);
static int _get_dca_list(int argc, int optind, char **argv, const char *operation, int conf_handle);
static int _get_ant_port(int argc, int optind, char **argv, const char *operation, int conf_handle);
static int _get_txrate(int argc, int optind, char **argv, const char *operation, int conf_handle);
static int _get_txpower(int argc, int optind, char **argv, const char *operation, int conf_handle);
static int _get_preamble(int argc, int optind, char **argv, const char *operation, int conf_handle);

#if MDE_80211N_SUPPORT
static int _get_ldpc(int argc, int optind, char **argv, const char *operation, int conf_handle);
static int _get_smps(int argc, int optind, char **argv, const char *operation, int conf_handle);
static int _get_tx_stbc(int argc, int optind, char **argv, const char *operation, int conf_handle);
static int _get_rx_stbc(int argc, int optind, char **argv, const char *operation, int conf_handle);
static int _get_delayed_ba(int argc, int optind, char **argv, const char *operation, int conf_handle);
static int _get_intolerant(int argc, int optind, char **argv, const char *operation, int conf_handle);
static int _get_lsig_txop(int argc, int optind, char **argv, const char *operation, int conf_handle);
static int _get_gi_20(int argc, int optind, char **argv, const char *operation, int conf_handle);
static int _get_gi_40(int argc, int optind, char **argv, const char *operation, int conf_handle);
static int _get_dsss_cck_40(int argc, int optind, char **argv, const char *operation, int conf_handle);
static int _get_gfmode(int argc, int optind, char **argv, const char *operation, int conf_handle);
static int _get_ht_bandwidth(int argc, int optind, char **argv, const char *operation, int conf_handle);
static int _get_max_amsdu_len(int argc, int optind, char **argv, const char *operation, int conf_handle);
static int _get_ampdu_enable(int argc, int optind, char **argv, const char *operation, int conf_handle);
static int _get_max_ampdu_len(int argc, int optind, char **argv, const char *operation, int conf_handle);
#endif
#if MDE_80211AC_SUPPORT
static int _get_max_mpdu_len(int argc, int optind, char **argv, const char *operation, int conf_handle);
static int _get_supported_channel_width(int argc, int optind, char **argv, const char *operation, int conf_handle);
static int _get_rx_ldpc(int argc, int optind, char **argv, const char *operation, int conf_handle);
static int _get_gi_80(int argc, int optind, char **argv, const char *operation, int conf_handle);
static int _get_gi_160(int argc, int optind, char **argv, const char *operation, int conf_handle);
static int _get_vtx_stbc(int argc, int optind, char **argv, const char *operation, int conf_handle);
static int _get_vrx_stbc(int argc, int optind, char **argv, const char *operation, int conf_handle);
static int _get_su_beamformer_cap(int argc, int optind, char **argv, const char *operation, int conf_handle);
static int _get_su_beamformee_cap(int argc, int optind, char **argv, const char *operation, int conf_handle);
static int _get_beamformee_sts_count(int argc, int optind, char **argv, const char *operation, int conf_handle);
static int _get_sounding_dimensions(int argc, int optind, char **argv, const char *operation, int conf_handle);
static int _get_mu_beamformer_cap(int argc, int optind, char **argv, const char *operation, int conf_handle);
static int _get_mu_beamformee_cap(int argc, int optind, char **argv, const char *operation, int conf_handle);
static int _get_vht_txop_ps(int argc, int optind, char **argv, const char *operation, int conf_handle);
static int _get_htc_vht_cap(int argc, int optind, char **argv, const char *operation, int conf_handle);
static int _get_rx_ant_pattern_consistency(int argc, int optind, char **argv, const char *operation, int conf_handle);
static int _get_tx_ant_pattern_consistency(int argc, int optind, char **argv, const char *operation, int conf_handle);
static int _get_vht_oper_bandwidth(int argc, int optind, char **argv, const char *operation, int conf_handle);
static int _get_seg0_center_freq(int argc, int optind, char **argv, const char *operation, int conf_handle);
static int _get_seg1_center_freq(int argc, int optind, char **argv, const char *operation, int conf_handle);
#endif
static int _get_slot(int argc, int optind, char **argv, const char *operation, int conf_handle);
static int _get_link_opt(int argc, int optind, char **argv, const char *operation, int conf_handle);
static int _get_ack_time(int argc, int optind, char **argv, const char *operation, int conf_handle);
static int _get_hide_essid(int argc, int optind, char **argv, const char *operation, int conf_handle);
static int _get_preferred_parent(int argc, int optind, char **argv, const char *operation, int conf_handle);
static int _get_hb_interval(int argc, int optind, char **argv, const char *operation, int conf_handle);
static int _get_igmp(int argc, int optind, char **argv, const char *operation, int conf_handle);
static int _get_adhoc(int argc, int optind, char **argv, const char *operation, int conf_handle);
static int _get_dhcp(int argc, int optind, char **argv, const char *operation, int conf_handle);
static int _get_forced_root(int argc, int optind, char **argv, const char *operation, int conf_handle);
static int _get_fips(int argc, int optind, char **argv, const char *operation, int conf_handle);
static int _get_dfs(int argc, int optind, char **argv, const char *operation, int conf_handle);
static int _get_mobility_index(int argc, int optind, char **argv, const char *operation, int conf_handle);
static int _get_mobility_mode(int argc, int optind, char **argv, const char *operation, int conf_handle);
static int _get_gps(int argc, int optind, char **argv, const char *operation, int conf_handle);
static int _get_logmon(int argc, int optind, char **argv, const char *operation, int conf_handle);
//static int _get_autostart(int argc, int optind, char **argv, const char *operation, int conf_handle);
static int _get_location(int argc, int optind, char **argv, const char *operation, int conf_handle);
static int _get_acwmin(int argc, int optind, char **argv, const char *operation, int conf_handle);
static int _get_acwmax(int argc, int optind, char **argv, const char *operation, int conf_handle);
static int _get_aifsn(int argc, int optind, char **argv, const char *operation, int conf_handle);
static int _get_backoff(int argc, int optind, char **argv, const char *operation, int conf_handle);
static int _get_burst(int argc, int optind, char **argv, const char *operation, int conf_handle);
static int _get_use_virt(int argc, int optind, char **argv, const char *operation, int conf_handle);
//static int _get_option(int argc, int optind, char **argv, const char *operation, int conf_handle);
static int _fail_over_ethernet(int argc, int optind, char **argv, const char *operation, int conf_handle);
static int _get_server_ip_addr(int argc, int optind, char **argv, const char *openration, int conf_handle);
static int _get_mgmt_gw_addr(int argc, int optind, char **argv, const char *openration, int conf_handle);
static int _get_mgmt_gw_enable(int argc, int optind, char **argv, const char *openration, int conf_handle);
static int _get_mgmt_gw_certificates(int argc, int optind, char **argv, const char *openration, int conf_handle);
static int _get_disable_backhaul_security(int argc, int optind, char **argv, const char *operation, int conf_handle);

static struct
{
   const char *name;
   int        (*func)(int argc, int optind, char **argv, const char *operation, int conf_handle);
}
_field_retrievers_get[] =
{
   { "name",                       _get_name                       },
   { "mesh_id",                    _get_mesh_id                    },
   { "mesh_imcp_key",              _get_mesh_imcp_key              },
   { "globdca",                    _get_global_dca                 },
   { "sac",                        _get_stay_awake_count           },
   { "aging",                      _get_bridge_aging               },
   { "regdom",                     _get_reg_domain_code            },
   { "country",                    _get_country_code               },
   { "medtype",                    _get_medium_type                },
   { "subtype",                    _get_medium_sub_type            },
   { "usetype",                    _get_use_type                   },
   { "channel",                    _get_channel                    },
   { "essid",                      _get_essid                      },
   { "rts",                        _get_rts                        },
   { "frag",                       _get_frag                       },
   { "beacint",                    _get_beacint                    },
   { "dca",                        _get_dca                        },
   { "dcalist",                    _get_dca_list                   },
   { "antport",                    _get_ant_port                   },
   { "txrate",                     _get_txrate                     },
   { "preamble",                   _get_preamble                   },
#if MDE_80211N_SUPPORT
   { "ldpc",                       _get_ldpc                       },
   { "smps",                       _get_smps                       },
   { "tx_stbc",                    _get_tx_stbc                    },
   { "rx_stbc",                    _get_rx_stbc                    },
   { "delayed_ba",                 _get_delayed_ba                 },
   { "intolerant",                 _get_intolerant                 },
   { "lsig_txop",                  _get_lsig_txop                  },
   { "gi_20",                      _get_gi_20                      },
   { "gi_40",                      _get_gi_40                      },
   { "dsss_cck_40",                _get_dsss_cck_40                },
   { "ht_bandwidth",               _get_ht_bandwidth               },
   { "max_amsdu_len",              _get_max_amsdu_len              },
   { "ampdu_enable",               _get_ampdu_enable               },
   { "max_ampdu_len",              _get_max_ampdu_len              },
   { "gfmode",                     _get_gfmode                     },
#endif

#if MDE_80211AC_SUPPORT
   { "max_mpdu_len",               _get_max_mpdu_len               },
   { "supported_channel_width",    _get_supported_channel_width    },
   { "rx_ldpc",                    _get_rx_ldpc                    },
   { "gi_80",                      _get_gi_80                      },
   { "gi_160",                     _get_gi_160                     },
   { "vtx_stbc",                   _get_vtx_stbc                   },
   { "vrx_stbc",                   _get_vrx_stbc                   },
   { "su_beamformer_cap",          _get_su_beamformer_cap          },
   { "su_beamformee_cap",          _get_su_beamformee_cap          },
   { "beamformee_sts_count",       _get_beamformee_sts_count       },
   { "sounding_dimensions",        _get_sounding_dimensions        },
   { "mu_beamformer_cap",          _get_mu_beamformer_cap          },
   { "mu_beamformee_cap",          _get_mu_beamformee_cap          },
   { "vht_txop_ps",                _get_vht_txop_ps                },
   { "htc_vht_cap",                _get_htc_vht_cap                },
   { "rx_ant_pattern_consistency", _get_rx_ant_pattern_consistency },
   { "tx_ant_pattern_consistency", _get_tx_ant_pattern_consistency },
   { "vht_oper_bandwidth",         _get_vht_oper_bandwidth         },
   { "seg0_center_freq",           _get_seg0_center_freq           },
   { "seg1_center_freq",           _get_seg1_center_freq           },
#endif
   { "slot",                       _get_slot                       },
   { "linkopt",                    _get_link_opt                   },
   { "acktime",                    _get_ack_time                   },
   { "hidessid",                   _get_hide_essid                 },
   { "txpower",                    _get_txpower                    },
   { "prefpar",                    _get_preferred_parent           },
   { "hbint",                      _get_hb_interval                },
   { "igmp",                       _get_igmp                       },
   { "adhoc",                      _get_adhoc                      },
   { "dhcp",                       _get_dhcp                       },
   { "forcedroot",                 _get_forced_root                },
   { "fips",                       _get_fips                       },
   { "dfs",                        _get_dfs                        },
   { "mobindex",                   _get_mobility_index             },
   { "mobmode",                    _get_mobility_mode              },
   { "gps",                        _get_gps                        },
   { "logmon",                     _get_logmon                     },
   { "location",                   _get_location                   },
   { "acwmin",                     _get_acwmin                     },
   { "acwmax",                     _get_acwmax                     },
   { "aifsn",                      _get_aifsn                      },
   { "backoff",                    _get_backoff                    },
   { "burst",                      _get_burst                      },
   { "usevirt",                    _get_use_virt                   },
/*
   { "autostart",                  _get_autostart                  },
   { "option",                     _get_option                     },
*/
   { "failOverEthernet",           _fail_over_ethernet             },
   { "server_ip_addr",             _get_server_ip_addr             },
   { "mgmt_gw_addr",               _get_mgmt_gw_addr               },
   { "mgmt_gw_enable",             _get_mgmt_gw_enable             },
   { "mgmt_gw_certificates",       _get_mgmt_gw_certificates       },
   { "disable_backhaul_security",  _get_disable_backhaul_security  }

};

static void _usage(void)
{
   fprintf(stderr,
           "\n"
           "usage: alconfget [-v] [-h] [-f configfile] fieldname [fieldparams]\n"
           "-v         : for knowing the version\n"
           "-h         : for help\n"
           "-f         : specifiy configfile (/etc/meshap.conf is default)\n"
           "fieldname  : \n"
           "   mesh_id,mesh_imcp_key,name,globdca,sac,aging,regdom,\n"
           "   country,medtype,subtype,usetype,channel,essid,rts,frag,\n"
           "   beacint,dca,dcalist,antport,txrate,preamble,ldpc,smps,tx_stbc,rx_stbc,delayed_ba,gfmode,\n"
           "   max_amsdu_len,ht_bandwidth,dsss_cck_40,gi_40,gi_20,lsig_txop,intolerant,ampdu_enable,slot,linkopt,\n"
           "   max_ampdu_len,max_mpdu_len,supported_channel_width,acktime,hidessid,txpower,prefpar,hbint,igmp,adhoc,fips,\n"
           "   rx_ldpc,gi_80,gi_160,vtx_stbc,vrx_stbc,su_beamformer_cap,su_beamformee_cap,beamformee_sts_count,sounding_dimensions,\n"
           "   mu_beamformer_cap,mu_beamformee_cap,vht_txop_ps,htc_vht_cap,rx_ant_pattern_consistency,tx_ant_pattern_consistency,\n"
           "   vht_oper_bandwidth,seg0_center_freq,seg1_center_freq,\n"
           "   dhcp,gps,logmon,forcedroot,dfs,mobindex,mobmode,location,\n"
           "   usevirt,acwmin,acwmax,aifsn,backoff,burst,\n"
           "   failOverEthernet, server_ip_addr, mgmt_gw_enable, mgmt_gw_addr, mgmt_gw_certificates </path/xyz.crt>:</path/xyz.key>\n"
           );
}

#define _DEFAULT_CONF_FILE_PATH        "/etc/meshap.conf"
#define _DEFAULT_11E_CONF_FILE_PATH    "/etc/dot11e.conf"

#define NAME_MAX_LEN							128

#define SUB_TYPE_MAX_LEN 					10
#define USE_TYPE_MAX_LEN 					10
#define PREAMBLE_TYPE_MAX_LEN 			10
#define LDPC_MAX_LEN 						10
#define SLOT_MAX_LENGTH						10
#define LINKPORT_MAX_LEN					10
#define HT_BAND_MAX_LEN						10
#define DSSS_CCK_MAX_LEN					10
#define GI_40_MAX_LEN						10
#define GI_20_MAX_LEN						10
#define GFMODE_MAX_LEN						10
#define LSIG_MAX_LEN							10
#define INTOLERANT_MAX_LEN					10
#define DELAYED_BA_MAX_LEN					10
#define RX_STBC_MAX_LEN						10
#define TX_STBC_MAX_LEN						10
#define SMPS_MAX_LEN							10
#define DHCP_MODE_MAX_LEN					10
#define MAX_STATUS_LEN						10

#define MAC_MAX_LEN							56

static int _get_name(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   int  ret;
   char value[NAME_MAX_LEN];

   if (argc != optind + 1)
   {
      fprintf(stderr, "usage: alconfget name\n");
      return -4;
   }

   ret = al_conf_get_name(AL_CONTEXT conf_handle, value, NAME_MAX_LEN);

   if (!ret)
   {
      fprintf(stderr, "Success[name = %s]\n", value);
   }
   else
   {
      fprintf(stderr, "<<err %d>>\n", ret);
   }

   return ret;
}

static int _get_mesh_id(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   int  ret;
   char value[256];
   
	if (argc != optind + 1)
   {
      fprintf(stderr, "usage: alconfget mesh_id\n");
      return -4;
   }

   ret = al_conf_get_mesh_id(AL_CONTEXT conf_handle, value, 256);

   if (!ret)
   {
      fprintf(stderr, "Success[mesh_id=%s]\n", value);
   }
   else
   {
      fprintf(stdout, "<<err %d>>\n", ret);
   }
   return ret;
}

static int _get_mesh_imcp_key(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   int  ret;
   char value[256];
   
	if (argc != optind + 1)
   {
      fprintf(stderr, "usage: alconfget mesh_imcp_key\n");
      return -4;
   }

   ret = al_conf_get_mesh_imcp_key_buffer(AL_CONTEXT_PARAM_DECL conf_handle, value, 256);

   if (!ret)
   {
      fprintf(stderr, "Success[name=%s]\n", value);
   }
   else
   {
      fprintf(stderr, "error [%d]\n", ret);
    }

    return ret;
}

static int _get_global_dca(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   int  value;

   if (argc != optind + 1)
   {
      fprintf(stderr, "usage: alconfget globdca\n");
      return -4;
   }

   value = al_conf_get_dynamic_channel_allocation(AL_CONTEXT conf_handle);
   fprintf(stderr, "Success[globdca_dca = %d]\n", value);
   return value;
}

static int _get_stay_awake_count(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   int  value;

   if (argc != optind + 1)
   {
      fprintf(stderr, "usage: alconfget sac\n");
      return -4;
   }

   value = al_conf_get_stay_awake_count(AL_CONTEXT conf_handle);
   fprintf(stderr, "Success[sac = %d]\n", value);
   return value;
}

static int _get_bridge_aging(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   int  value;

   if (argc != optind + 1)
   {
      fprintf(stderr, "usage: alconfget aging\n");
      return -4;
   }

   value = al_conf_get_bridge_ageing_time(AL_CONTEXT conf_handle);
   fprintf(stderr, "Success[aging = %d]\n", value);
   return value;
}

static int _get_reg_domain_code(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   int value;

   if (argc != optind + 1)
   {
      fprintf(stderr, "usage: alconfget regdom\n");
      return -4;
   }

   value = al_conf_get_regulatory_domain(AL_CONTEXT conf_handle); //doubt
   fprintf(stderr, "Success[regdom = %d]\n", value);
   return value;
}

static int _get_country_code(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   int value;
   
	if (argc != optind + 1)
   {
      fprintf(stderr, "usage: alconfget country\n");
      return -4;
   }

   value = al_conf_get_country_code(AL_CONTEXT conf_handle);
   fprintf(stderr, "Success[country=%d]\n", value);
   return value;
}

typedef int (*conf_if_callback_t)(int argc, int optind, char **argv, al_conf_if_info_t *if_info);

static int _if_iterator(int argc, int optind, char **argv, int conf_handle, conf_if_callback_t func)
{
   char              *if_name;
   int               i;
   int               count;
   al_conf_if_info_t if_info;
   int               ret;

   if (argc != optind + 2)
   {
      return -4;
   }

   if_name = argv[optind + 1];

   count = al_conf_get_if_count(AL_CONTEXT conf_handle);
   for (i = 0; i < count; i++)
   {
      al_conf_get_if(AL_CONTEXT conf_handle, i, &if_info);

      if (!strcmp(if_info.name, if_name))
      {
         ret = func(argc, optind, argv, &if_info);
         return ret;
      }
   }

   fprintf(stderr, "<<err interface %s not found>>\n",if_name);

   return -5;
}

static int _med_type_callback(int argc, int optind, char **argv, al_conf_if_info_t *if_info)

{
		char med_type;

		if(if_info->phy_type == AL_CONF_IF_PHY_TYPE_ETHERNET)
			med_type = 'e';
		if(if_info->phy_type == AL_CONF_IF_PHY_TYPE_802_11)
			med_type = 'w';
		if(if_info->phy_type == AL_CONF_IF_PHY_TYPE_802_11_VIRTUAL)
			med_type = 'v';

		fprintf(stderr, "Success[Medtype for %s = %c]\n", if_info->name, med_type);

   return 0;
}

static int _get_medium_type(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   int ret;

   ret = _if_iterator(argc, optind, argv, conf_handle, _med_type_callback);

   if (ret == -4)
   {
      fprintf(stderr, "usage: alconfget medtype <<if-name>> \n");
   }

   return ret;
}

static int _sub_type_callback(int argc, int optind, char **argv, al_conf_if_info_t *if_info)
{
	char sub_type[SUB_TYPE_MAX_LEN];

   if (if_info->phy_sub_type == AL_CONF_IF_PHY_SUB_TYPE_IGNORE)
   {
      strcpy(sub_type, "x" );
   }
   else if (if_info->phy_sub_type == AL_CONF_IF_PHY_SUB_TYPE_802_11_A)
   {
      strcpy(sub_type, "a" );
   }
   else if (if_info->phy_sub_type == AL_CONF_IF_PHY_SUB_TYPE_802_11_B)
   {
      strcpy(sub_type, "b" );
   }
   else if (if_info->phy_sub_type == AL_CONF_IF_PHY_SUB_TYPE_802_11_G)
   {
      strcpy(sub_type, "g" );
   }
   else if (if_info->phy_sub_type == AL_CONF_IF_PHY_SUB_TYPE_802_11_BG)
   {
      strcpy(sub_type, "bg" );
   }
   else if (if_info->phy_sub_type == AL_CONF_IF_PHY_SUB_TYPE_802_11_PSQ)
   {
      strcpy(sub_type, "psq" );
   }
   else if (if_info->phy_sub_type == AL_CONF_IF_PHY_SUB_TYPE_802_11_PSH)
   {
      strcpy(sub_type, "psh" );
   }
   else if (if_info->phy_sub_type == AL_CONF_IF_PHY_SUB_TYPE_802_11_PSF)
   {
      strcpy(sub_type, "psf" );
   }
#if (MDE_80211N_SUPPORT || MDE_80211AC_SUPPORT)
        else if (if_info->phy_sub_type == AL_CONF_IF_PHY_SUB_TYPE_802_11_N_2_4GHZ)
        {
                strcpy(sub_type, "n_2_4G" );
        }
        else if (if_info->phy_sub_type == AL_CONF_IF_PHY_SUB_TYPE_802_11_N_5GHZ)
        {
                strcpy(sub_type, "n_5G" );
        }
        else if (if_info->phy_sub_type == AL_CONF_IF_PHY_SUB_TYPE_802_11_AC)
        {
                strcpy(sub_type, "ac" );
        }
        else if (if_info->phy_sub_type == AL_CONF_IF_PHY_SUB_TYPE_802_11_BGN)
        {
                strcpy(sub_type, "bgn" );
        }
        else if (if_info->phy_sub_type == AL_CONF_IF_PHY_SUB_TYPE_802_11_AN)
        {
                strcpy(sub_type, "an" );
        }
        else if (if_info->phy_sub_type == AL_CONF_IF_PHY_SUB_TYPE_802_11_ANAC)
        {
                strcpy(sub_type, "anac" );
        }
#endif
   else
   {
      return -4;
   }

	   fprintf(stderr, "Success[subtype of %s = %s]\n", if_info->name, sub_type);

	return 0;
}

static int _get_medium_sub_type(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   int ret;

   ret = _if_iterator(argc, optind, argv, conf_handle, _sub_type_callback);

   if (ret == -4)
   {
      fprintf(stderr, "usage: alconfget subtype <<if-name>>\n");
   }

   return ret;
}

static int _use_type_callback(int argc, int optind, char **argv, al_conf_if_info_t *if_info)
{

	char use_type[USE_TYPE_MAX_LEN];

   if (if_info->use_type == AL_CONF_IF_USE_TYPE_DS)
   {
        strcpy(use_type, "ds");
   }
   else if (if_info->use_type == AL_CONF_IF_USE_TYPE_WM)
   {
        strcpy(use_type, "wm");
   }
   else if (if_info->use_type == AL_CONF_IF_USE_TYPE_AP)
   {
        strcpy(use_type, "ap");
   }
   else if (if_info->use_type == AL_CONF_IF_USE_TYPE_PMON)
   {
        strcpy(use_type, "pmon");
   }
   else if (if_info->use_type == AL_CONF_IF_USE_TYPE_AMON)
   {
        strcpy(use_type, "amon");
   }
   else
   {
      return -4; 
   }

	fprintf(stderr, "Success[usetype of %s = %s]\n", if_info->name, use_type);

	return 0;
}

static int _get_use_type(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   int ret;

   ret = _if_iterator(argc, optind, argv, conf_handle, _use_type_callback);

   if (ret == -4)
   {
      fprintf(stderr, "usage: alconfget usetype <<if-name>>\n");
		fprintf(stderr, "<<value would be either : ds,wm,pmon,amon>>\n");
   }

   return ret;
}

static int _channel_callback(int argc, int optind, char **argv, al_conf_if_info_t *if_info)
{
	fprintf(stderr, "Success[channel of %s = %d]\n", if_info->name, if_info->wm_channel);
   return 0;
}

static int _get_channel(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   int ret;

   ret = _if_iterator(argc, optind, argv, conf_handle, _channel_callback);

   if (ret == -4)
   {
      fprintf(stderr, "usage: alconfget channel <<if-name>>\n");
   }

   return ret;
}

static int _dca_callback(int argc, int optind, char **argv, al_conf_if_info_t *if_info)
{
	fprintf(stderr, "Success[dca for %s = %d]\n", if_info->name, if_info->dca);
   return 0;
}

static int _get_dca(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   int ret;

   ret = _if_iterator(argc, optind, argv, conf_handle, _dca_callback);

   if (ret == -4)
   {
      fprintf(stderr, "usage: alconfget dca <<if-name>>\n");
   }

   return ret;
}

static int _dcalist_callback(int argc, int optind, char **argv, al_conf_if_info_t *if_info)
{
   int j = 0, count = 0;

   count = if_info->dca_list_count;
	fprintf(stderr, "dca count = %d and dcalist :\n", count);
   for (j = 0; j < count; j++)
	{
		fprintf(stderr, "%d\n", if_info->dca_list[j]);
	}
	return 0;
}

static int _get_dca_list(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   int ret;

   ret = _if_iterator(argc, optind, argv, conf_handle, _dcalist_callback);

   if (ret == -4)
   {
      fprintf(stderr, "usage: alconfget dcalist <<if-name>>\n");
   }

   return ret;
}

static int _antport_callback(int argc, int optind, char **argv, al_conf_if_info_t *if_info)
{
   fprintf(stderr, "Success[antport for %s = %d]\n", if_info->name, if_info->txant);
   return 0;
}

static int _get_ant_port(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   int ret;

   ret = _if_iterator(argc, optind, argv, conf_handle, _antport_callback);

   if (ret == -4)
   {
      fprintf(stderr, "usage: alconfget antport <<if-name>>\n");
   }

   return ret;
}

static int _txrate_callback(int argc, int optind, char **argv, al_conf_if_info_t *if_info)
{
	fprintf(stderr, "Success[txrate for %s = %d]\n", if_info->name, if_info->txrate);
	return 0;
}

static int _get_txrate(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   int ret;

   ret = _if_iterator(argc, optind, argv, conf_handle, _txrate_callback);

   if (ret == -4)
   {
      fprintf(stderr, "usage: alconfget txrate <<if-name>> <<mbps/0=auto>>\n");
   }

   return ret;
}

static int _preamble_callback(int argc, int optind, char **argv, al_conf_if_info_t *if_info)
{
	char preamble_type[PREAMBLE_TYPE_MAX_LEN];
	if(if_info->preamble_type == AL_CONF_IF_PREAMBLE_TYPE_LONG) {
		strcpy(preamble_type, "long");
	} else if (if_info->preamble_type == AL_CONF_IF_PREAMBLE_TYPE_SHORT) {
		strcpy(preamble_type, "short");
	} else
		return -4;
   fprintf(stderr, "Success[preamble for %s = %s]\n", if_info->name, preamble_type);
   return 0;
}

static int _get_preamble(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   int ret;

   ret = _if_iterator(argc, optind, argv, conf_handle, _preamble_callback);

   if (ret == -4)
   {
      fprintf(stderr, "usage: alconfget preamble <<if-name>>\n");
   }

   return ret;
}

static int _essid_callback(int argc, int optind, char **argv, al_conf_if_info_t *if_info)
{
	fprintf(stderr, "Success[essid for %s = %s]\n", if_info->name, if_info->essid);
   return 0;
}

static int _get_essid(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   int ret;

   ret = _if_iterator(argc, optind, argv, conf_handle, _essid_callback);

   if (ret == -4)
   {
      fprintf(stderr, "usage: alconfget essid <<if-name>>\n");
   }

   return ret;
}

static int _rts_callback(int argc, int optind, char **argv, al_conf_if_info_t *if_info)
{
	fprintf(stderr, "Success[rts for %s = %d]\n", if_info->name, if_info->rts_th);
   return 0;
}


static int _get_rts(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   int ret;

   ret = _if_iterator(argc, optind, argv, conf_handle, _rts_callback);

   if (ret == -4)
   {
      fprintf(stderr, "usage: alconfget rts <<if-name>>\n");
   }

   return ret;
}

static int _frag_callback(int argc, int optind, char **argv, al_conf_if_info_t *if_info)
{
	fprintf(stderr, "Success[frag for %s = %d]\n", if_info->name, if_info->frag_th);
   return 0;
}

static int _get_frag(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   int ret;

   ret = _if_iterator(argc, optind, argv, conf_handle, _frag_callback);

   if (ret == -4)
   {
      fprintf(stderr, "usage: alconfget frag <<if-name>>\n");
   }

   return ret;
}

static int _beacint_callback(int argc, int optind, char **argv, al_conf_if_info_t *if_info)
{
	fprintf(stderr, "Success[beacint for %s = %d]\n", if_info->name, if_info->beacon_interval);
   return 0;
}

static int _get_beacint(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   int ret;

   ret = _if_iterator(argc, optind, argv, conf_handle, _beacint_callback);

   if (ret == -4)
   {
      fprintf(stderr, "usage: alconfget beacint <<if-name>>\n");
   }

   return ret;
}

#if MDE_80211N_SUPPORT

static int _ldpc_callback(int argc, int optind, char **argv, al_conf_if_info_t *if_info)
{

	char *ldpc_status[LDPC_MAX_LEN];

	if (if_info->ht_capab.ldpc == AL_CONF_IF_HT_PARAM_ENABLED)
   {
      strcpy(ldpc_status, "enabled");
   }
	else if (if_info->ht_capab.ldpc == AL_CONF_IF_HT_PARAM_DISABLED)
   {
      strcpy(ldpc_status, "disabled");
   }
   else
   {
      return -4;
   }

   fprintf(stderr, "Success[ldcp for %s = %s]\n", if_info->name, ldpc_status);

   return 0;
}

static int _get_ldpc(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   int ret;

   ret = _if_iterator(argc, optind, argv, conf_handle, _ldpc_callback);

   if (ret == -4)
   {
      fprintf(stderr, "usage: alconfget ldpc <<if-name>>\n");
   }

   return ret;
}

static int _smps_callback(int argc, int optind, char **argv, al_conf_if_info_t *if_info)
{

	char smps_status[SMPS_MAX_LEN];

	if (if_info->ht_capab.smps == AL_CONF_IF_HT_PARAM_SMPS_DISABLED)
   {
		strcpy(smps_status, "disabled");
   }
	else if (if_info->ht_capab.smps == AL_CONF_IF_HT_PARAM_STATIC)
   {
		strcpy(smps_status, "static");
   }
	else if (if_info->ht_capab.smps == AL_CONF_IF_HT_PARAM_DYNAMIC)
   {
		strcpy(smps_status, "dynamic");
   }
   else
   {
      return -4;
   }

	fprintf(stderr, "Success[smps for %s = %s]\n", if_info->name, smps_status);

   return 0;
}


static int _get_smps(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   int ret;

   ret = _if_iterator(argc, optind, argv, conf_handle, _smps_callback);

   if (ret == -4)
   {
      fprintf(stderr, "usage: alconfget smps <<if-name>> \n");
   }

   return ret;
}

static int _tx_stbc_callback(int argc, int optind, char **argv, al_conf_if_info_t *if_info)
{

	char tx_stbc_status[TX_STBC_MAX_LEN];

	if (if_info->ht_capab.tx_stbc == AL_CONF_IF_HT_PARAM_ENABLED)
   {
		strcpy(tx_stbc_status, "enabled");
   }
	else if (if_info->ht_capab.tx_stbc == AL_CONF_IF_HT_PARAM_DISABLED)
   {
		strcpy(tx_stbc_status, "disabled");
   }
   else
   {
      return -4;
   }

	fprintf(stderr, "Success[tx_stbc for %s = %s]\n", if_info->name, tx_stbc_status);

   return 0;
}


static int _get_tx_stbc(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   int ret;

   ret = _if_iterator(argc, optind, argv, conf_handle, _tx_stbc_callback);

   if (ret == -4)
   {
      fprintf(stderr, "usage: alconfget tx_stbc <<if-name>>\n");
   }

   return ret;
}

static int _rx_stbc_callback(int argc, int optind, char **argv, al_conf_if_info_t *if_info)
{

	char rx_stbc_status[RX_STBC_MAX_LEN];

	if (if_info->ht_capab.rx_stbc == AL_CONF_IF_HT_PARAM_ENABLED)
   {
		strcpy(rx_stbc_status, "enabled");
   }
	else if (if_info->ht_capab.rx_stbc == AL_CONF_IF_HT_PARAM_DISABLED)
   {
		strcpy(rx_stbc_status, "disabled");
   }
   else
   {
      return -4;
   }
	fprintf(stderr, "Success[rx_stbc for %s = %s]\n", if_info->name, rx_stbc_status);

   return 0;
}


static int _get_rx_stbc(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   int ret;

   ret = _if_iterator(argc, optind, argv, conf_handle, _rx_stbc_callback);

   if (ret == -4)
   {
      fprintf(stderr, "usage: alconfget rx_stbc <<if-name>>\n");
   }

   return ret;
}

static int _delayed_ba_callback(int argc, int optind, char **argv, al_conf_if_info_t *if_info)
{

	char delayed_ba_status[DELAYED_BA_MAX_LEN];

	if(if_info->ht_capab.delayed_ba == AL_CONF_IF_HT_PARAM_ENABLED)
   {
		strcpy(delayed_ba_status, "enabled");
   }
	else if (if_info->ht_capab.delayed_ba == AL_CONF_IF_HT_PARAM_DISABLED)
   {
		strcpy(delayed_ba_status, "disabled");
   }
   else
   {
      return -4;
   }

   fprintf(stderr, "Success[delayed_ba for %s = %s]\n", if_info->name, delayed_ba_status);

   return 0;
}


static int _get_delayed_ba(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   int ret;

   ret = _if_iterator(argc, optind, argv, conf_handle, _delayed_ba_callback);

   if (ret == -4)
   {
      fprintf(stderr, "usage: alconfget delayed_ba <<if-name>> \n");
   }

   return ret;
}

static int _intolerant_callback(int argc, int optind, char **argv, al_conf_if_info_t *if_info)
{

	char intolerant_status[INTOLERANT_MAX_LEN];

	if(if_info->ht_capab.intolerant == AL_CONF_IF_HT_PARAM_ENABLED)
   {
		strcpy(intolerant_status, "enabled");
   }
	else if (if_info->ht_capab.intolerant == AL_CONF_IF_HT_PARAM_DISABLED)
   {
		strcpy(intolerant_status, "disabled");
   }
   else
   {
      return -4;
   }

	fprintf(stderr, "Success[intolerant for %s = %s]\n", if_info->name, intolerant_status);

   return 0;
}


static int _get_intolerant(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   int ret;

   ret = _if_iterator(argc, optind, argv, conf_handle, _intolerant_callback);

   if (ret == -4)
   {
      fprintf(stderr, "usage: alconfget intolerant <<if-name>> \n");
   }

   return ret;
}

static int _lsig_txop_callback(int argc, int optind, char **argv, al_conf_if_info_t *if_info)
{

	char lsig_txop_status[LSIG_MAX_LEN];

	if(if_info->ht_capab.lsig_txop == AL_CONF_IF_HT_PARAM_ENABLED)
   {
		strcpy(lsig_txop_status, "enabled");
   }
	else if(if_info->ht_capab.lsig_txop == AL_CONF_IF_HT_PARAM_DISABLED)
   {
		strcpy(lsig_txop_status, "disabled");
   }
   else
   {
      return -4;
   }

   fprintf(stderr, "Success[lsig_txop for %s = %s]\n", if_info->name, lsig_txop_status);

   return 0;
}


static int _get_lsig_txop(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   int ret;

   ret = _if_iterator(argc, optind, argv, conf_handle, _lsig_txop_callback);

   if (ret == -4)
   {
      fprintf(stderr, "usage: alconfget lsig_txop <<if-name>> \n");
   }

   return ret;
}

static int _gfmode_callback(int argc, int optind, char **argv, al_conf_if_info_t *if_info)
{

	char gfmode_status[GFMODE_MAX_LEN];

	if(if_info->ht_capab.gfmode == AL_CONF_IF_HT_PARAM_ENABLED)
   {
		strcpy(gfmode_status, "enabled");
   }
	else if (if_info->ht_capab.gfmode == AL_CONF_IF_HT_PARAM_DISABLED)
   {
		strcpy(gfmode_status, "disabled");
   }
   else
   {
      return -4;
   }

	fprintf(stderr, "Success[gfmode for %s = %s]\n", if_info->name, gfmode_status);

   return 0;
}


static int _get_gfmode(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   int ret;

   ret = _if_iterator(argc, optind, argv, conf_handle, _gfmode_callback);

   if (ret == -4)
   {
      fprintf(stderr, "usage: alconfget gfmode <<if-name>> \n");
   }

   return ret;
}

static int _gi_20_callback(int argc, int optind, char **argv, al_conf_if_info_t *if_info)
{

	char gi_20_val[GI_20_MAX_LEN];

	if (if_info->ht_capab.gi_20 == AL_CONF_IF_HT_PARAM_LONG)
   {
		strcpy(gi_20_val, "long");
   }
	else if (if_info->ht_capab.gi_20 == AL_CONF_IF_HT_PARAM_AUTO)
   {
		strcpy(gi_20_val, "auto");
   }
	else if (if_info->ht_capab.gi_20 == AL_CONF_IF_HT_PARAM_SHORT)
   {
		strcpy(gi_20_val, "short");
   }
   else
   {
      return -4;
   }

	fprintf(stderr, "Success[gi_20 for %s = %s]\n", if_info->name, gi_20_val);

   return 0;
}


static int _get_gi_20(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   int ret;

   ret = _if_iterator(argc, optind, argv, conf_handle, _gi_20_callback);

   if (ret == -4)
   {
      fprintf(stderr, "usage: alconfget gi_20 <<if-name>> \n");
   }

   return ret;
}

static int _gi_40_callback(int argc, int optind, char **argv, al_conf_if_info_t *if_info)
{

	char gi_40_val[GI_40_MAX_LEN];

	if (if_info->ht_capab.gi_40 == AL_CONF_IF_HT_PARAM_LONG)
   {
		strcpy(gi_40_val, "long");
   }
	else if (if_info->ht_capab.gi_40 == AL_CONF_IF_HT_PARAM_AUTO)
   {
		strcpy(gi_40_val, "auto");
   }
	else if (if_info->ht_capab.gi_40 == AL_CONF_IF_HT_PARAM_SHORT)
   {
		strcpy(gi_40_val, "short");
   }
   else
   {
      return -4;
   }

	fprintf(stderr, "Success[gi_40 for %s = %s]\n", if_info->name, gi_40_val);

   return 0;
}


static int _get_gi_40(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   int ret;

   ret = _if_iterator(argc, optind, argv, conf_handle, _gi_40_callback);

   if (ret == -4)
   {
      fprintf(stderr, "usage: alconfget gi_40 <<if-name>> \n");
   }

   return ret;
}

static int _dsss_cck_40_callback(int argc, int optind, char **argv, al_conf_if_info_t *if_info)
{

	char dsss_check[DSSS_CCK_MAX_LEN];

	if(if_info->ht_capab.dsss_cck_40 == AL_CONF_IF_HT_PARAM_ALLOW)
   {
		strcpy(dsss_check, "allow");
   }
	else if(if_info->ht_capab.dsss_cck_40 == AL_CONF_IF_HT_PARAM_DENY)
   {
		strcpy(dsss_check, "deny");
   }
   else
   {
      return -4;
   }

   fprintf(stderr, "Success[dsss_cck_40 for %s = %s]\n", if_info->name, dsss_check);

   return 0;
}


static int _get_dsss_cck_40(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   int ret;

   ret = _if_iterator(argc, optind, argv, conf_handle, _dsss_cck_40_callback);

   if (ret == -4)
   {
      fprintf(stderr, "usage: alconfget dsss_cck_40 <<if-name>>\n");
   }

   return ret;
}

static int _ht_bandwidth_callback(int argc, int optind, char **argv, al_conf_if_info_t *if_info)
{
	char ht_bandwidth_val[HT_BAND_MAX_LEN];

	if (if_info->ht_capab.ht_bandwidth == AL_CONF_IF_HT_PARAM_40_ABOVE)
   {
		strcpy(ht_bandwidth_val, "40+");
   }
	else if (if_info->ht_capab.ht_bandwidth == AL_CONF_IF_HT_PARAM_40_BELOW)
   {
		strcpy(ht_bandwidth_val, "40-");
   }
	else if (if_info->ht_capab.ht_bandwidth == AL_CONF_IF_HT_PARAM_20)
   {
		strcpy(ht_bandwidth_val, "20");
   }
   else
   {
      return -4;
   }

	fprintf(stderr, "Success[ht_bandwidth for %s = %s]\n", if_info->name, ht_bandwidth_val);

   return 0;
}


static int _get_ht_bandwidth(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   int ret;

   ret = _if_iterator(argc, optind, argv, conf_handle, _ht_bandwidth_callback);

   if (ret == -4)
   {
      fprintf(stderr, "usage: alconfget ht_bandwidth <<if-name>>\n");
   }

   return ret;
}

static int _max_amsdu_len_callback(int argc, int optind, char **argv, al_conf_if_info_t *if_info)
{
   fprintf(stderr, "Success[max_amsdu_len for %s = %d]\n", if_info->name, if_info->ht_capab.max_amsdu_len);
   return 0;
}


static int _get_max_amsdu_len(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   int ret;

   ret = _if_iterator(argc, optind, argv, conf_handle, _max_amsdu_len_callback);

   if (ret == -4)
   {
      fprintf(stderr, "usage: alconfget max_amsdu_len <<if-name>>\n");
   }

   return ret;
}

static int _ampdu_enable_callback(int argc, int optind, char **argv, al_conf_if_info_t *if_info)
{
   fprintf(stderr, "Success[ampdu_enable for %s = %d]\n", if_info->name, if_info->frame_aggregation.ampdu_enable);
   return 0;
}


static int _get_ampdu_enable(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   int ret;

   ret = _if_iterator(argc, optind, argv, conf_handle, _ampdu_enable_callback);

   if (ret == -4)
   {
      fprintf(stderr, "usage: alconfget ampdu_enable <<if-name>> \n");
   }

   return ret;
}

static int _max_ampdu_len_callback(int argc, int optind, char **argv, al_conf_if_info_t *if_info)
{

   fprintf(stderr, "Success[max_ampdu_len for %s = %d]\n", if_info->name, if_info->frame_aggregation.max_ampdu_len);
   return 0;
}

static int _get_max_ampdu_len(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   int ret;

   ret = _if_iterator(argc, optind, argv, conf_handle, _max_ampdu_len_callback);

   if (ret == -4)
   {
      fprintf(stderr, "usage: alconfget max_ampdu_len <<if-name>>\n");
   }

   return ret;
}
#endif

#if MDE_80211AC_SUPPORT

static int _max_mpdu_len_callback(int argc, int optind, char **argv, al_conf_if_info_t *if_info)
{
      fprintf(stderr, "Success[%s max_mpdu_len=%d]\n", if_info->name, if_info->vht_capab.max_mpdu_len);
   return 0;
}


static int _get_max_mpdu_len(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   int ret;

   ret = _if_iterator(argc, optind, argv, conf_handle, _max_mpdu_len_callback);

   if (ret == -4)
   {
      fprintf(stderr, "usage: alconfget max_mpdu_len <<if-name>>\n");
   }

   return ret;
}


static int _supported_channel_width_callback(int argc, int optind, char **argv, al_conf_if_info_t *if_info)
{
      fprintf(stderr, "Success[%s supported_channel_width=%d]\n", if_info->name, if_info->vht_capab.supported_channel_width);
   return 0;
}

static int _get_supported_channel_width(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   int ret;

   ret = _if_iterator(argc, optind, argv, conf_handle, _supported_channel_width_callback);

   if (ret == -4)
   {
      fprintf(stderr, "usage: alconfget supported_channel_width <<if-name>>\n");
   }
   return ret;
}

static int _rx_ldpc_callback(int argc, int optind, char **argv, al_conf_if_info_t *if_info)
{
		char value[MAX_STATUS_LEN];
      if (if_info->vht_capab.rx_ldpc == AL_CONF_IF_HT_PARAM_ENABLED)
        {
        strcpy(value, "enabled");
        }
      else if (if_info->vht_capab.rx_ldpc == AL_CONF_IF_HT_PARAM_DISABLED)
        {
        strcpy(value, "disabled");
        }
      fprintf(stderr, "Success[%s rx_ldpc=%s]\n", if_info->name, value);
   return 0;
}


static int _get_rx_ldpc(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   int ret;

   ret = _if_iterator(argc, optind, argv, conf_handle, _rx_ldpc_callback);

   if (ret == -4)
   {
      fprintf(stderr, "usage: alconfget rx_ldpc <<if-name>>\n");
   }

   return ret;
}


static int _gi_80_callback(int argc, int optind, char **argv, al_conf_if_info_t *if_info)
{
	char value[MAX_STATUS_LEN];

   if (if_info->vht_capab.gi_80 == AL_CONF_IF_HT_PARAM_LONG)
    {
        strcpy(value, "long");
    }
   else if (if_info->vht_capab.gi_80 == AL_CONF_IF_HT_PARAM_AUTO)
    {
        strcpy(value, "auto");
    }
   else if (if_info->vht_capab.gi_80 == AL_CONF_IF_HT_PARAM_SHORT)
    {
        strcpy(value, "short");
    }

   fprintf(stderr, "Success[%s gi_80=%s]\n", if_info->name, value);
   return 0;
}


static int _get_gi_80(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   int ret;

   ret = _if_iterator(argc, optind, argv, conf_handle, _gi_80_callback);

   if (ret == -4)
   {
      fprintf(stderr, "usage: alconfget gi_80 <<if-name>>\n");
   }

   return ret;
}

static int _gi_160_callback(int argc, int optind, char **argv, al_conf_if_info_t *if_info)
{

	char value[MAX_STATUS_LEN];

   if (if_info->vht_capab.gi_160 == AL_CONF_IF_HT_PARAM_LONG)
    {
        strcpy(value, "long");
    }
   else if (if_info->vht_capab.gi_160 == AL_CONF_IF_HT_PARAM_AUTO)
    {
        strcpy(value, "auto");
    }
   else if (if_info->vht_capab.gi_160 == AL_CONF_IF_HT_PARAM_SHORT)
    {
        strcpy(value, "short");
    }
   fprintf(stderr, "Success[%s gi_160=%s]\n", if_info->name, value);

   return 0;
}


static int _get_gi_160(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   int ret;

   ret = _if_iterator(argc, optind, argv, conf_handle, _gi_160_callback);

   if (ret == -4)
   {
      fprintf(stderr, "usage: alconfget gi_160 <<if-name>>\n");
   }

   return ret;
}


static int _vtx_stbc_callback(int argc, int optind, char **argv, al_conf_if_info_t *if_info)
{

		char value[MAX_STATUS_LEN];
      if (if_info->vht_capab.tx_stbc == AL_CONF_IF_HT_PARAM_ENABLED)
        {
        strcpy(value, "enabled");
        }
      else if (if_info->vht_capab.tx_stbc == AL_CONF_IF_HT_PARAM_DISABLED)
        {
        strcpy(value, "disabled");
        }

      fprintf(stderr, "Success[%s vtx_stbc=%s]\n", if_info->name, value);
   return 0;
}


static int _get_vtx_stbc(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   int ret;

   ret = _if_iterator(argc, optind, argv, conf_handle, _vtx_stbc_callback);

   if (ret == -4)
   {
      fprintf(stderr, "usage: alconfget vtx_stbc <<if-name>>\n");
   }

   return ret;
}


static int _vrx_stbc_callback(int argc, int optind, char **argv, al_conf_if_info_t *if_info)
{
		char value[MAX_STATUS_LEN];

      if (if_info->vht_capab.rx_stbc == AL_CONF_IF_HT_PARAM_ENABLED)
        {
        strcpy(value, "enabled");
        }
      else if (if_info->vht_capab.rx_stbc == AL_CONF_IF_HT_PARAM_DISABLED)
        {
        strcpy(value, "disabled");
        }

        fprintf(stderr, "Success[%s vrx_stbc=%s]\n", if_info->name, value);
   return 0;
}

static int _get_vrx_stbc(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   int ret;

   ret = _if_iterator(argc, optind, argv, conf_handle, _vrx_stbc_callback);

   if (ret == -4)
   {
      fprintf(stderr, "usage: alconfget vrx_stbc <<if-name>>\n");
   }

   return ret;
}


static int _su_beamformer_cap_callback(int argc, int optind, char **argv, al_conf_if_info_t *if_info)
{
	char value[MAX_STATUS_LEN];

   if (if_info->vht_capab.su_beamformer_cap == AL_CONF_IF_VHT_PARAM_YES)
    {
        strcpy(value, "yes");
    }
   else if (if_info->vht_capab.su_beamformer_cap == AL_CONF_IF_VHT_PARAM_NO)
    {
        strcpy(value, "no");
    }

    fprintf(stderr, "Success[%s su_beamformer_cap=%s]\n", if_info->name, value);
   return 0;
}


static int _get_su_beamformer_cap(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   int ret;

   ret = _if_iterator(argc, optind, argv, conf_handle, _su_beamformer_cap_callback);

   if (ret == -4)
   {
      fprintf(stderr, "usage: alconfget su_beamformer_cap <<if-name>>\n");
   }

   return ret;
}


static int _su_beamformee_cap_callback(int argc, int optind, char **argv, al_conf_if_info_t *if_info)
{
   char value[MAX_STATUS_LEN];
   if (if_info->vht_capab.su_beamformee_cap == AL_CONF_IF_VHT_PARAM_YES)
    {
        strcpy(value, "yes");
    }
   else if (if_info->vht_capab.su_beamformee_cap == AL_CONF_IF_VHT_PARAM_NO)
    {
        strcpy(value, "no");
    }

   fprintf(stderr, "Success[%s su_beamformee_cap=%s]\n", if_info->name, value);
   return 0;
}


static int _get_su_beamformee_cap(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   int ret;

   ret = _if_iterator(argc, optind, argv, conf_handle, _su_beamformee_cap_callback);

   if (ret == -4)
   {
      fprintf(stderr, "usage: alconfget su_beamformee_cap <<if-name>>\n");
   }

   return ret;
}


static int _beamformee_sts_count_callback(int argc, int optind, char **argv, al_conf_if_info_t *if_info)
{
      fprintf(stderr, "Success[%s beamformee_sts_count=%d]\n", if_info->name, if_info->vht_capab.beamformee_sts_count);
    return 0;
}

static int _get_beamformee_sts_count(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   int ret;

   ret = _if_iterator(argc, optind, argv, conf_handle, _beamformee_sts_count_callback);

   if (ret == -4)
   {
      fprintf(stderr, "usage: alconfget beamformee_sts_count <<if-name>>\n");
   }

   return ret;
}


static int _sounding_dimensions_callback(int argc, int optind, char **argv, al_conf_if_info_t *if_info)
{
      fprintf(stderr, "Success[%s sounding_dimensions=%d]\n", if_info->name, if_info->vht_capab.sounding_dimensions);
    return 0;
}


static int _get_sounding_dimensions(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   int ret;

   ret = _if_iterator(argc, optind, argv, conf_handle, _sounding_dimensions_callback);

   if (ret == -4)
   {
      fprintf(stderr, "usage: alconfget sounding_dimensions <<if-name>>\n");
   }

   return ret;
}

static int _mu_beamformer_cap_callback(int argc, int optind, char **argv, al_conf_if_info_t *if_info)
{
	char value[MAX_STATUS_LEN];

   if (if_info->vht_capab.mu_beamformer_cap == AL_CONF_IF_VHT_PARAM_YES)
    {
        strcpy(value, "yes");
    }
   else if (if_info->vht_capab.mu_beamformer_cap == AL_CONF_IF_VHT_PARAM_NO)
    {
        strcpy(value, "no");
    }

   fprintf(stderr, "Success[%s mu_beamformer_cap=%s]\n", if_info->name, value);
   return 0;
}


static int _get_mu_beamformer_cap(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   int ret;

   ret = _if_iterator(argc, optind, argv, conf_handle, _mu_beamformer_cap_callback);

   if (ret == -4)
   {
      fprintf(stderr, "usage: alconfget mu_beamformer_cap <<if-name>>\n");
   }

   return ret;
}


static int _mu_beamformee_cap_callback(int argc, int optind, char **argv, al_conf_if_info_t *if_info)
{
	char value[MAX_STATUS_LEN];

   if (if_info->vht_capab.mu_beamformee_cap == AL_CONF_IF_VHT_PARAM_YES)
    {
        strcpy(value, "yes");
    }
   else if (if_info->vht_capab.mu_beamformee_cap == AL_CONF_IF_VHT_PARAM_NO)
    {
        strcpy(value, "no");
    }
   fprintf(stderr, "Success[%s mu_beamformee_cap=%s]\n", if_info->name, value);
   return 0;
}


static int _get_mu_beamformee_cap(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   int ret;

   ret = _if_iterator(argc, optind, argv, conf_handle, _mu_beamformee_cap_callback);

   if (ret == -4)
   {
      fprintf(stderr, "usage: alconfget mu_beamformee_cap <<if-name>>\n");
   }

   return ret;
}


static int _vht_txop_ps_callback(int argc, int optind, char **argv, al_conf_if_info_t *if_info)
{
	char value[MAX_STATUS_LEN];

   if (if_info->vht_capab.vht_txop_ps == AL_CONF_IF_VHT_PARAM_YES)
    {
        strcpy(value, "yes");
    }
   else if (if_info->vht_capab.vht_txop_ps == AL_CONF_IF_VHT_PARAM_NO)
    {
        strcpy(value, "no");
    }

   fprintf(stderr, "Success[%s vht_txop_ps=%s]\n", if_info->name, value);
   return 0;
}


static int _get_vht_txop_ps(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   int ret;

   ret = _if_iterator(argc, optind, argv, conf_handle, _vht_txop_ps_callback);

   if (ret == -4)
   {
      fprintf(stderr, "usage: alconfget vht_txop_ps <<if-name>>\n");
   }

   return ret;
}


static int _htc_vht_cap_callback(int argc, int optind, char **argv, al_conf_if_info_t *if_info)
{
	char value[MAX_STATUS_LEN];

   if (if_info->vht_capab.htc_vht_cap == AL_CONF_IF_VHT_PARAM_YES)
    {
        strcpy(value, "yes");
    }
   else if (if_info->vht_capab.vht_txop_ps == AL_CONF_IF_VHT_PARAM_NO)
    {
        strcpy(value, "no");
    }

   fprintf(stderr, "Success[%s htc_vht_cap=%s]\n", if_info->name, value);
   return 0;
}


static int _get_htc_vht_cap(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   int ret;

   ret = _if_iterator(argc, optind, argv, conf_handle, _htc_vht_cap_callback);

   if (ret == -4)
   {
      fprintf(stderr, "usage: alconfget htc_vht_cap <<if-name>>\n");
   }

   return ret;
}

static int _rx_ant_pattern_consistency_callback(int argc, int optind, char **argv, al_conf_if_info_t *if_info)
{
	char value[MAX_STATUS_LEN];

   if (if_info->vht_capab.rx_ant_pattern_consistency == AL_CONF_IF_VHT_PARAM_YES)
    {
        strcpy(value, "yes");
    }
   else if (if_info->vht_capab.rx_ant_pattern_consistency == AL_CONF_IF_VHT_PARAM_NO)
    {
        strcpy(value, "no");
    }
   fprintf(stderr, "Success[%s rx_ant_pattern_consistency=%s]\n", if_info->name, value);
   return 0;
}


static int _get_rx_ant_pattern_consistency(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   int ret;

   ret = _if_iterator(argc, optind, argv, conf_handle, _rx_ant_pattern_consistency_callback);

   if (ret == -4)
   {
      fprintf(stderr, "usage: alconfget rx_ant_pattern_consistency <<if-name>>\n");
   }

   return ret;
}


static int _tx_ant_pattern_consistency_callback(int argc, int optind, char **argv, al_conf_if_info_t *if_info)
{
	char value[MAX_STATUS_LEN];

   if (if_info->vht_capab.tx_ant_pattern_consistency == AL_CONF_IF_VHT_PARAM_YES)
    {
        strcpy(value, "yes");
    }
   else if (if_info->vht_capab.tx_ant_pattern_consistency == AL_CONF_IF_VHT_PARAM_NO)
    {
        strcpy(value, "no");
    }
   fprintf(stderr, "Success[%s tx_ant_pattern_consistency=%s]\n", if_info->name, value);
   return 0;
}


static int _get_tx_ant_pattern_consistency(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   int ret;

   ret = _if_iterator(argc, optind, argv, conf_handle, _tx_ant_pattern_consistency_callback);

   if (ret == -4)
   {
      fprintf(stderr, "usage: alconfget tx_ant_pattern_consistency <<if-name>>\n");
   }

   return ret;
}


static int _vht_oper_bandwidth_callback(int argc, int optind, char **argv, al_conf_if_info_t *if_info)
{
      fprintf(stderr, "Success[%s vht_oper_bandwidth=%d]\n", if_info->name, if_info->vht_capab.vht_oper_bandwidth);
   return 0;
}


static int _get_vht_oper_bandwidth(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   int ret;

   ret = _if_iterator(argc, optind, argv, conf_handle, _vht_oper_bandwidth_callback);

   if (ret == -4)
   {
      fprintf(stderr, "usage: alconfget vht_oper_bandwidth <<if-name>>\n");
   }

   return ret;
}
static int _seg0_center_freq_callback(int argc, int optind, char **argv, al_conf_if_info_t *if_info)
{
      fprintf(stderr, "Success[%s seg0_center_freq=%d]\n", if_info->name, if_info->vht_capab.seg0_center_freq);
   return 0;
}


static int _get_seg0_center_freq(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   int ret;

   ret = _if_iterator(argc, optind, argv, conf_handle, _seg0_center_freq_callback);

   if (ret == -4)
   {
      fprintf(stderr, "usage: alconfget seg0_center_freq <<if-name>>\n");
   }

   return ret;
}


static int _seg1_center_freq_callback(int argc, int optind, char **argv, al_conf_if_info_t *if_info)
{
      fprintf(stderr, "Success[%s seg1_center_freq=%d]\n", if_info->name, if_info->vht_capab.seg1_center_freq);
   return 0;
}


static int _get_seg1_center_freq(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   int ret;

   ret = _if_iterator(argc, optind, argv, conf_handle, _seg1_center_freq_callback);

   if (ret == -4)
   {
      fprintf(stderr, "usage: alconfget seg1_center_freq <<if-name>>\n");
   }

   return ret;
}

#endif

static int _slot_callback(int argc, int optind, char **argv, al_conf_if_info_t *if_info)
{

   char slot_val[SLOT_MAX_LENGTH];

   if (if_info->slot_time_type == AL_CONF_IF_SLOT_TIME_TYPE_LONG)
   {
      strcpy(slot_val, "long");
   }
   else if (if_info->slot_time_type == AL_CONF_IF_SLOT_TIME_TYPE_SHORT)
   {
      strcpy(slot_val, "short");
   }
   else
   {
      return -4;
   }

   fprintf(stderr, "Success[slot for %s = %s]\n", if_info->name, slot_val);

   return 0;
}


static int _get_slot(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   int ret;

   ret = _if_iterator(argc, optind, argv, conf_handle, _slot_callback);

   if (ret == -4)
   {
      fprintf(stderr, "usage: alconfget slot <<if-name>>\n");
   }

   return ret;
}

static int _linkopt_callback(int argc, int optind, char **argv, al_conf_if_info_t *if_info)
{

   char linkopt_val[LINKPORT_MAX_LEN];

   if (if_info->service == AL_CONF_IF_SERVICE_BACKHAUL_ONLY)
   {
      strcpy(linkopt_val, "backhaul");
   }
   else if (if_info->service == AL_CONF_IF_SERVICE_CLIENT_ONLY)
   {
      strcpy(linkopt_val, "client");
   }
   else if (if_info->service == AL_CONF_IF_SERVICE_ALL)
   {
      strcpy(linkopt_val, "all");
   }
   else
   {
      return -4;
   }

   fprintf(stderr, "Success[linkopt for %s = %s]\n", if_info->name, linkopt_val);

   return 0;
}


static int _get_link_opt(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   int ret;

   ret = _if_iterator(argc, optind, argv, conf_handle, _linkopt_callback);

   if (ret == -4)
   {
      fprintf(stderr, "usage: alconfget linkopt <<if-name>>\n");
   }

   return ret;
}


static int _acktime_callback(int argc, int optind, char **argv, al_conf_if_info_t *if_info)
{
   fprintf(stderr, "Success[acktime for %s = %d]\n", if_info->name, if_info->ack_timeout );
   return 0;
}


static int _get_ack_time(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   int ret;

   ret = _if_iterator(argc, optind, argv, conf_handle, _acktime_callback);

   if (ret == -4)
   {
      fprintf(stderr, "usage: alconfget acktime <<if-name>>\n");
   }

   return ret;
}


static int _hidessid_callback(int argc, int optind, char **argv, al_conf_if_info_t *if_info)
{
   fprintf(stderr, "Success[hidessid for %s = %d]\n", if_info->name, if_info->hide_ssid);
   return 0;
}

static int _get_hide_essid(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   int ret;

   ret = _if_iterator(argc, optind, argv, conf_handle, _hidessid_callback);

   if (ret == -4)
   {
      fprintf(stderr, "usage: alconfget hidessid <<if-name>>\n");
   }

   return ret;
}

static int _get_preferred_parent(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   int           ret;
   char          value[MAC_MAX_LEN];
   al_net_addr_t addr;

	ret = al_conf_get_preferred_parent(AL_CONTEXT conf_handle, &addr);

   fprintf(stderr, "Success[prefpar = %02x:%02x:%02x:%02x:%02x:%02x]\n", addr.bytes[0], addr.bytes[1], addr.bytes[2], addr.bytes[3],addr.bytes[4], addr.bytes[5]);

   return ret;
}

static int _get_hb_interval(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   int  value;
   value = al_conf_get_heartbeat_interval(AL_CONTEXT conf_handle);
   fprintf(stderr, "Success[hbint = %d]\n", value);

   return value;
}

static int _get_igmp(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   int  value;
   value  = al_conf_get_hop_cost(AL_CONTEXT conf_handle);
   fprintf(stderr, "Success[igmp = %d]\n", value&1);
   return value;
}

static int _txpower_callback(int argc, int optind, char **argv, al_conf_if_info_t *if_info)
{
   fprintf(stderr, "Success[txpower for %s = %d]\n", if_info->name, if_info->txpower);
   return 0;
}


static int _get_txpower(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   int ret;

   ret = _if_iterator(argc, optind, argv, conf_handle, _txpower_callback);

   if (ret == -4)
   {
      fprintf(stderr, "usage: alconfget txpower <<if-name>>\n");
   }

   return ret;
}

static int _get_adhoc(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   int  ret;
   int  adhoc_mode;
   int  sectored_usage;
   int  begin_in_infra;
   int  curval;

	if (argc != optind + 1)
   {
      fprintf(stderr, "usage: alconfget adhoc\n");
      return -4;
   }

	curval = al_conf_get_hop_cost(AL_CONTEXT conf_handle);

	adhoc_mode = curval & 0x02;
	begin_in_infra = curval & 0x08;
	sectored_usage = curval & 0x40;
   if (adhoc_mode)
   {
       adhoc_mode = 1;
   }

   if (begin_in_infra)
   {
      begin_in_infra = 1;
   }

   if (sectored_usage)
   {
      sectored_usage = 1;
   }

      fprintf(stderr, "Success[adhoc = %d, begin_in_infra = %d, sectored_usage = %d]\n", adhoc_mode, begin_in_infra, sectored_usage);
   return 0;

}

static int _get_dhcp(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   int                 ret;
	char					  mode[DHCP_MODE_MAX_LEN];
   al_conf_dhcp_info_t dhcp_info;

   if (argc != optind + 1)
   {
      goto _param_err;
      return -4;
   }

	al_conf_get_dhcp_info(AL_CONTEXT conf_handle, &dhcp_info);

	if(dhcp_info.dhcp_mode ==  AL_CONF_DHCP_MODE_RANDOM)
		{
			strcpy(mode, "random");
      }
   else if (dhcp_info.dhcp_mode = AL_CONF_DHCP_MODE_FIXED)
      {
			strcpy(mode, "fixed");
      }
   else
      {
         goto _param_err;
      }

		fprintf(stderr,
					"mode = %s\n"
					"net id = %d.%d.%d.%d\n"
					"mask = %d.%d.%d.%d\n"
					"gateway = %d.%d.%d.%d\n"
					"dns = %d.%d.%d.%d\n"
					"lease = %d\n",
						mode,
						dhcp_info.dhcp_net_id[0],dhcp_info.dhcp_net_id[1],dhcp_info.dhcp_net_id[2],dhcp_info.dhcp_net_id[3],
						dhcp_info.dhcp_mask[0], dhcp_info.dhcp_mask[1], dhcp_info.dhcp_mask[2], dhcp_info.dhcp_mask[3],
						dhcp_info.dhcp_gateway[0], dhcp_info.dhcp_gateway[1], dhcp_info.dhcp_gateway[2], dhcp_info.dhcp_gateway[3],
						dhcp_info.dhcp_dns[0], dhcp_info.dhcp_dns[1], dhcp_info.dhcp_dns[2], dhcp_info.dhcp_dns[3],
						dhcp_info.dhcp_lease_time);

   return 0;

_param_err:

	fprintf(stderr, "usgae: alconfget dhcp\n");
   return -4;
}

static int _get_forced_root(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   int  value;

   if (argc != optind + 1)
   {
      fprintf(stderr, "usage: alconfget forcedroot\n");
      return -4;
   }

   value = al_conf_get_hop_cost(AL_CONTEXT conf_handle);
// debug	fprintf(stderr, "return of alconf value = %d\n", value);
	value &= 0x04;
	if(value)
		value = 1;
   fprintf(stderr, "Success[forcedroot = %d]\n", value);
   return value;
}

static int _get_fips(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   int            value;

   if (argc != optind + 1)
   {
      fprintf(stderr, "usage: alconfget fips\n");
      return -4;
   }

   value  = al_conf_get_regulatory_domain(AL_CONTEXT conf_handle);
	value &= 0x200;
	if(value)
		value = 1;
   fprintf(stderr, "Success[fips = %d]\n", value);
   return value;
}

static int _get_dfs(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   int            value;

   if (argc != optind + 1)
   {
      fprintf(stderr, "usage: alconfget dfs\n");
      return -4;
   }

   value  = al_conf_get_regulatory_domain(AL_CONTEXT conf_handle);
	value &= 0x100;
	if(value)
		value = 1;
   fprintf(stderr, "Success[dfs = %d]\n", value);
   return value;
}


static int _get_mobility_index(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   int  ret;
   char *value;
   int  curval;

   if (argc != optind + 1)
   {
      fprintf(stderr, "usage: alconfget mobindex\n");
      return -4;
   }

   curval  = al_conf_get_las_interval(AL_CONTEXT conf_handle);
	curval &= ~(0x80);
   fprintf(stderr, "Success[mobindex = %d]\n", curval);
   return curval;
}

static int _get_mobility_mode(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   int  ret;
   int  curval;

   if (argc != optind + 1)
   {
      fprintf(stderr, "usage: alconfget mobmode\n");
      return -4;
   }

   curval  = al_conf_get_las_interval(AL_CONTEXT conf_handle);
	curval &= 0x80;
	if(curval)
		curval = 1;
   fprintf(stderr, "Success[mobmode = %d]\n", curval);
   return curval;
}

static int _get_gps(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   int                ret;
   al_conf_gps_info_t gps_info;

   if (argc != optind + 1)
   {
      goto _param_err;
   }

   al_conf_get_gps_info(AL_CONTEXT conf_handle, &gps_info);
		fprintf(stderr, "for gps : \n"
					"enabled = %d\n"
					"input = %s\n"
					"push_dest = %d.%d.%d.%d : %d\n"
					"push_int = %d\n"
						, gps_info.gps_enabled,
						gps_info.gps_input_dev,
						gps_info.gps_push_dest_ip[0], gps_info.gps_push_dest_ip[1], gps_info.gps_push_dest_ip[2], gps_info.gps_push_dest_ip[3],
						gps_info.gps_push_port,
						gps_info.gps_push_interval);

   return 0;
_param_err:

   fprintf(stderr,
           "usage: <<alconfget gps>>\n");
   return -4;
}

static int _get_logmon(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   int  ret;
   al_conf_logmon_info_t logmon;

   if (argc != optind + 1)
   {
      goto _param_err;
   }

   al_conf_get_logmon_info(AL_CONTEXT conf_handle, &logmon);
   fprintf(stderr,
              "Success[logmon push_dest = %d.%d.%d.%d:%d]\n",
              logmon.logmon_dest_ip[0],
              logmon.logmon_dest_ip[1],
              logmon.logmon_dest_ip[2],
              logmon.logmon_dest_ip[3],
              logmon.logmon_dest_port);
   return 0;

_param_err:

   fprintf(stderr,
           "usage: alconfget logmon\n");
   return -4;
}


static int _get_location(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   int  curval;

   if (argc != optind + 1)
   {
      fprintf(stderr, "usage: alconfget location\n");
      return -4;
   }

   curval  = al_conf_get_hop_cost(AL_CONTEXT conf_handle);
	curval &= 0x20;
	if(curval)
		curval = 1;
   fprintf(stderr, "Success[location = %d]\n", curval);
   return curval;
}

#if 0
static void _auto_start_enum(void *cookie, int number, const char *command_block_string)
{
   fprintf(stderr,
           "------- BLOCK %d ----------------\n"
           "%s\n"
           "---------------------------------\n\n",
           number,
           command_block_string);
}


static void _autostart_list(auto_start_token_t token)
{
   fprintf(stderr, "\n");

   auto_start_enumerate_blocks(token, _auto_start_enum, NULL);

   fprintf(stderr, "\n");
}


static int _autostart_create(auto_start_token_t token)
{
   return auto_start_create_block(token);
}


static void _autostart_add(auto_start_token_t token, int command_block_number, const char *command)
{
   auto_start_append_block(token, command_block_number, command);
}


static void _autostart_del(auto_start_token_t token, int command_block_number)
{
   auto_start_delete_block(token, command_block_number);
}

static int _get_autostart(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   const char         *subop;
   int                command_block_number;
   const char         *command;
   auto_start_token_t token;
   int                ret;

   token = NULL;

   if (argc <= optind + 1)
   {
      goto _usage;
   }

   token = auto_start_open("/root/configd_spawn.sh");

   subop = argv[optind + 1];

   if (!strcmp(subop, "list"))
   {
      _autostart_list(token);
   }
   else if (!strcmp(subop, "create"))
   {
      ret = _autostart_create(token);

      fprintf(stderr, "Success[Block %d created]\n", ret);

      auto_start_save(token);
   }
   else if (!strcmp(subop, "add"))
   {
      if (argc <= optind + 3)
      {
         goto _usage;
      }

      command_block_number = atoi(argv[optind + 2]);
      command = argv[optind + 3];

     _autostart_add(token, command_block_number, command);

      auto_start_save(token);

      fprintf(stderr, "Success[Block %d appended]\n", command_block_number);
   }
   else if (!strcmp(subop, "del"))
   {
      if (argc <= optind + 2)
      {
         goto _usage;
      }

      command_block_number = atoi(argv[optind + 2]);

      _autostart_del(token, command_block_number);

      auto_start_save(token);

      fprintf(stderr, "Success[Block %d deleted]\n", command_block_number);
   }
   else
   {
      goto _usage;
   }

   return 0;

   auto_start_close(token);

_usage:

   if (token != NULL)
   {
      auto_start_close(token);
   }

   fprintf(stderr,
           "usage: alconfget autostart list\n"
           "       alconfget autostart create\n"
           "       alconfget autostart add <<command-block-number>> <<command>>\n"
           "       alconfget autostart del <<command-block-number>>\n");

   return -4;
}

static int _get_option(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   const char       *subop;
   al_conf_option_t *options;
   al_conf_option_t *temp;
   int              i;

   if (argc <= optind + 1)
   {
      goto _usage;
   }

   subop = argv[optind + 1];

   if (!strcmp(subop, "list"))
   {
      options = al_conf_get_options(AL_CONTEXT conf_handle);

      for (temp = options; temp != NULL; temp = temp->next)
      {
         for (i = 0; i < 16; i++)
         {
            fprintf(stderr, "%s%02X", (i != 0) ? ":" : "", temp->key[i]);
         }

         fprintf(stderr, "\n\n");
      }

      while (options != NULL)
      {
         temp = options->next;
         al_heap_free(AL_CONTEXT options);
         options = temp;
      }
   }
   else if (!strcmp(subop, "add"))
   {
      if (argc <= optind + 2)
      {
         goto _usage;
      }

      if (strlen(argv[optind + 2]) != (15 * 3) + 2)
      {
         fprintf(stderr, "Invalid key format\n");
         goto _usage;
      }

      temp = (al_conf_option_t *)al_heap_alloc(AL_CONTEXT sizeof(al_conf_option_t)AL_HEAP_DEBUG_PARAM);

      mac_address_ascii_to_binary(argv[optind + 2], temp->key);

      options = al_conf_get_options(AL_CONTEXT conf_handle);

      temp->next = options;

      al_conf_get_options(AL_CONTEXT conf_handle, temp);

      fprintf(stderr, "Success[option added]\n");
   }
   else if (!strcmp(subop, "del"))
   {
      al_conf_option_t *prev;
      int              index;

      if (argc <= optind + 2)
      {
         goto _usage;
      }

      index = atoi(argv[optind + 2]);

      if (index < 0)
      {
         fprintf(stderr, "Invalid option index\n");
         goto _usage;
      }
      options = al_conf_get_options(AL_CONTEXT conf_handle);

      for (i = 0, temp = options, prev = NULL; temp != NULL; prev = temp, temp = temp->next, i++)
      {
         if (i == index)
         {
            if (prev != NULL)
            {
               prev->next = temp->next;
            }
            else
            {
               options = options->next;
            }

            al_heap_free(AL_CONTEXT temp);

            break;
         }
      }

      al_conf_get_options(AL_CONTEXT conf_handle, options);

      fprintf(stderr, "Success[option deleted]\n");
   }
   else
   {
      goto _usage;
   }

   return 0;

_usage:

   fprintf(stderr,
           "usage: \n"
           "\talconfget option list\n"
           "\talconfget option add option-key\n"
           "\talconfget option del option-index\n");


   return -4;
}

#endif

typedef int (*dot11e_int_callback_t)(int category, dot11e_conf_category_details_t *category_info);

static int _dot11e_category_helper(int category, dot11e_int_callback_t callback)
{
   int           fd;
   off_t         offset;
   unsigned char *file_data;
   int           dot11e_handle;
   dot11e_conf_category_details_t category_info;

   /**
    * Read in the config file into memory
    */

   fd = open(_DEFAULT_11E_CONF_FILE_PATH, O_RDONLY);

   if (fd == -1)
   {
      fprintf(stderr, "Error in opening dot11e.conf for reading\n");
      return -5;
   }

   offset = lseek(fd, 0, SEEK_END);

   file_data = (unsigned char *)malloc(offset);
   memset(file_data, 0, offset + 1);

   lseek(fd, 0, SEEK_SET);

   read(fd, file_data, offset);

   close(fd);

   dot11e_handle = dot11e_conf_parse(AL_CONTEXT file_data);

   if (dot11e_handle == 0)
   {
      fprintf(stderr, "Error parsing "_DEFAULT_11E_CONF_FILE_PATH);
      return -6;
   }

   free(file_data);
   dot11e_conf_get_category_info(AL_CONTEXT dot11e_handle, category, &category_info);

   callback(category, &category_info);

   dot11e_conf_set_category_info(AL_CONTEXT dot11e_handle, category, &category_info);

   dot11e_conf_put(AL_CONTEXT dot11e_handle);

   dot11e_conf_close(AL_CONTEXT dot11e_handle);

   return 0;
}


static int _acwmin_callback(int category, dot11e_conf_category_details_t *category_info)
{
   fprintf(stderr, "Success[acwmin for category %d = %d]\n", category, category_info->acwmin);
   return 0;
}


static int _get_acwmin(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   int category;

   if (argc != optind + 2)
   {
      goto _usage;
   }

   category = atoi(argv[optind + 1]);

   if ((category < 0) || (category > 3))
   {
      goto _usage;
   }

   _dot11e_category_helper(category, _acwmin_callback);

   return 0;

_usage:

   fprintf(stderr, "usage: alconfget acwmin <<category [0-3]>>\n");

   return -4;
}


static int _acwmax_callback(int category, dot11e_conf_category_details_t *category_info, int value)
{
   fprintf(stderr, "Success[acwmax for category %d = %d]\n", category, category_info->acwmax);
   return 0;
}

static int _get_acwmax(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   int category;

   if (argc != optind + 2)
   {
      goto _usage;
   }

   category = atoi(argv[optind + 1]);

   if ((category < 0) || (category > 3))
   {
      goto _usage;
   }

   _dot11e_category_helper(category, _acwmax_callback);

   return 0;

_usage:

   fprintf(stderr, "usage: alconfget acwmax <<category [0-3]>>\n");

   return -4;
}

static int _aifsn_callback(int category, dot11e_conf_category_details_t *category_info)
{
   fprintf(stderr, "Success[aifsn for category %d = %d]\n", category, category_info->aifsn);
   return 0;
}

static int _get_aifsn(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   int category;

   if (argc != optind + 2)
   {
      goto _usage;
   }

   category = atoi(argv[optind + 1]);

   if ((category < 0) || (category > 3))
   {
      goto _usage;
   }

   _dot11e_category_helper(category, _aifsn_callback);

   return 0;

_usage:

   fprintf(stderr, "usage: alconfget aifsn <<category [0-3]>>\n");

   return -4;
}


static int _backoff_callback(int category, dot11e_conf_category_details_t *category_info, int value)
{
   fprintf(stderr, "Success[backoff for category %d = %d]\n", category, !category_info->disable_backoff);
   return 0;
}

static int _get_backoff(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   int category;

   if (argc != optind + 2)
   {
      goto _usage;
   }

   category = atoi(argv[optind + 1]);

   if ((category < 0) || (category > 3))
   {
      goto _usage;
   }

   _dot11e_category_helper(category, _backoff_callback);

   return 0;

_usage:

   fprintf(stderr, "usage: alconfget backoff <<category [0-3]>>\n");

   return -4;
}


static int _burst_callback(int category, dot11e_conf_category_details_t *category_info)
{
   fprintf(stderr, "Success[burst for category %d = %d]\n", category, category_info->burst_time);
   return 0;
}

static int _get_burst(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   int category;

   if (argc != optind + 2)
   {
      goto _usage;
  }

   category = atoi(argv[optind + 1]);

   if ((category < 0) || (category > 3))
   {
      goto _usage;
   }

   _dot11e_category_helper(category, _burst_callback);

   return 0;

_usage:

   fprintf(stderr, "usage: alconfget burst <<category [0-3]>>\n");

   return -4;
}

static int _get_use_virt(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   int  ret;
   char *value;
   int  nvalue;

   if (argc != optind + 1)
   {
      fprintf(stderr, "usage: alconfget usevirt\n");
      return -4;
   }

   ret = al_conf_get_use_virt_if(AL_CONTEXT conf_handle);
   fprintf(stderr, "Success[usevirt = %d]\n", ret);
   return ret;
}

static int _fail_over_ethernet(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   int  ret;
   char *value;
   int  on_off;
   int  power_on_default;
   int  scan_freq_secs;
   char *server_ip_addr;

   on_off = al_conf_get_failover_enabled(AL_CONTEXT_PARAM_DECL  conf_handle);
   power_on_default = al_conf_get_power_on_default(AL_CONTEXT_PARAM_DECL  conf_handle);
   server_ip_addr = al_conf_get_server_addr(AL_CONTEXT_PARAM_DECL conf_handle);

   fprintf(stderr, "Success[on_off=%d power_on_default = %d scan_freq_secs=%d] server_ip_addr = %s\n", on_off, power_on_default, scan_freq_secs, server_ip_addr);
}

static int _get_server_ip_addr(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   unsigned char *value;

   value = al_conf_get_server_addr(AL_CONTEXT_PARAM_DECL conf_handle);
   fprintf(stderr, "Success[IP_ADDR = %s]\n", value);
}

static int _get_mgmt_gw_addr(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   unsigned char *value;

   value = al_conf_get_mgmt_gw_addr(AL_CONTEXT_PARAM_DECL conf_handle);
   fprintf(stderr, "Success[MGMT_GW_ADDR = %s]\n", value);
}

static int _get_mgmt_gw_enable(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   int  nvalue;

   nvalue = al_conf_get_mgmt_gw_enable(AL_CONTEXT_PARAM_DECL conf_handle);
   fprintf(stderr, "Success[mgmt_gw_enable=%d]\n", nvalue);
}

static int _get_mgmt_gw_certificates(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   unsigned char *value;
   value = al_conf_get_mgmt_gw_certificates(AL_CONTEXT_PARAM_DECL conf_handle);
   fprintf(stderr, "Success[MGMT_GW_CERTIFICATES = %s]\n", value);
}

static int _get_disable_backhaul_security(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
    int  ret;
    int value;

    value = al_conf_get_disable_backhaul_security(AL_CONTEXT_PARAM_DECL conf_handle);
    fprintf(stderr, "Success[disable_backhaul_security=%d]\n", value);
}


int main(int argc, char**argv)
{
	char          config_file_path[256];
   char          field[64];
   int           ch;
   int           i;
   int           fd;
   off_t         offset;
   unsigned char *file_data;
   int           al_conf_handle;
   int           ret;

   strcpy(config_file_path, _DEFAULT_CONF_FILE_PATH);
   *field = 0;

   while ((ch = getopt(argc, argv, "vhf:")) != EOF)
   {
      switch (ch)
      {
      case 'v':
         fprintf(stdout,
                 "\nMeshDynamics alconfget Version %s\n"
                 "Copyright (c) 2002-2007 Meshdynamics, Inc\n"
                 "All rights reserved.\n\n",
                 _TORNA_VERSION_STRING_);
         return 0;

      case 'h':
         _usage();
         return 0;

      case 'f':
         strcpy(config_file_path, optarg);
         break;
      }
   }

   if (optind >= argc)
   {
      fprintf(stderr, "\nNo field specified\n");
      _usage();
      return -1;
   }

   fd = open(config_file_path, O_RDONLY);

   if (fd == -1)
   {
      fprintf(stderr, "Error in opening %s\n", config_file_path);
      return -2;
   }

   offset = lseek(fd, 0, SEEK_END);

   file_data = (unsigned char *)malloc(offset);
   memset(file_data, 0, offset + 1);

   lseek(fd, 0, SEEK_SET);

   read(fd, file_data, offset);

   close(fd);

   al_conf_handle = al_conf_parse(AL_CONTEXT file_data);

   if (al_conf_handle == -1)
   {
      free(file_data);
      return -3;
   }

   free(file_data);

   strcpy(field, argv[optind]);

   for (i = 0; i < (sizeof(_field_retrievers_get) / sizeof(*_field_retrievers_get)); i++)
   {
      if (!strcmp(field, _field_retrievers_get[i].name))
      {
         ret = _field_retrievers_get[i].func(argc, optind, argv, field, al_conf_handle);
         return ret;
      }
   }
   al_conf_close(AL_CONTEXT al_conf_handle);

   fprintf(stderr, "\nUnknown field %s\n", field);
   _usage();

   return -1;
}

