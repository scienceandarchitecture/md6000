/********************************************************************************
* MeshDynamics
* --------------
* File     : auto_start.c
* Comments : Auto Start Parser
* Created  : 10/16/2007
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  0  |10/16/2007 | Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include "al.h"
#include "al_context.h"
#include "al_impl_context.h"
#include "auto_start.h"

#define _MAX_LINE_LENGTH    256
#define _MAX_BLOCK_LINES    8

struct _block_info
{
   char               command_string[_MAX_LINE_LENGTH * _MAX_BLOCK_LINES];
   struct _block_info *next;
};
typedef struct _block_info   _block_info_t;

struct _auto_start_data
{
   char          file_name[128];
   _block_info_t *non_block_pre_head;
   _block_info_t *block_head;
   _block_info_t *non_block_post_head;
};
typedef struct _auto_start_data   _auto_start_data_t;

#define _AUTO_START_BEGIN        "#Auto-Start-Begin"
#define _AUTO_START_BEGIN_LEN    17
#define _AUTO_START_END          "#Auto-Start-End"
#define _AUTO_START_END_LEN      15
#define _BLOCK_BEGIN             "#Block-Begin"
#define _BLOCK_BEGIN_LEN         12
#define _BLOCK_END               "#Block-End"
#define _BLOCK_END_LEN           10

#define _MODE_PRE                1
#define _MODE_IN                 2
#define _MODE_IN_BLOCK           3
#define _MODE_POST               4

auto_start_token_t auto_start_open(const char *file_name)
{
   _auto_start_data_t *data;
   FILE               *fp;
   char               buffer[_MAX_LINE_LENGTH];
   int                mode;
   _block_info_t      *tail;
   _block_info_t      *block;

   data = (_auto_start_data_t *)al_heap_alloc(AL_CONTEXT sizeof(_auto_start_data_t)AL_HEAP_DEBUG_PARAM);

   strcpy(data->file_name, file_name);

   data->block_head          = NULL;
   data->non_block_post_head = NULL;
   data->non_block_pre_head  = NULL;

   fp = fopen(file_name, "rt");

   if (fp == NULL)
   {
      goto _out;
   }

   mode = _MODE_PRE;

   while (fgets(buffer, _MAX_LINE_LENGTH, fp))
   {
      switch (mode)
      {
      case _MODE_PRE:

         if (!memcmp(buffer, _AUTO_START_BEGIN, _AUTO_START_BEGIN_LEN))
         {
            mode = _MODE_IN;
         }
         else
         {
            block       = (_block_info_t *)al_heap_alloc(AL_CONTEXT sizeof(_block_info_t)AL_HEAP_DEBUG_PARAM);
            block->next = NULL;

            strcpy(block->command_string, buffer);

            if (data->non_block_pre_head == NULL)
            {
               data->non_block_pre_head = block;
            }
            else
            {
               tail->next = block;
            }

            tail = block;
         }

         break;

      case _MODE_IN:

         if (!memcmp(buffer, _AUTO_START_END, _AUTO_START_END_LEN))
         {
            mode = _MODE_POST;
         }
         else if (!memcmp(buffer, _BLOCK_BEGIN, _BLOCK_BEGIN_LEN))
         {
            mode        = _MODE_IN_BLOCK;
            block       = (_block_info_t *)al_heap_alloc(AL_CONTEXT sizeof(_block_info_t)AL_HEAP_DEBUG_PARAM);
            block->next = NULL;
            block->command_string[0] = 0;
         }

         break;

      case _MODE_IN_BLOCK:

         if (!memcmp(buffer, _BLOCK_END, _BLOCK_END_LEN))
         {
            mode = _MODE_IN;

            if (data->block_head == NULL)
            {
               data->block_head = block;
            }
            else
            {
               tail->next = block;
            }

            tail = block;
         }
         else
         {
            strcat(block->command_string, buffer);
         }

         break;

      case _MODE_POST:
      default:

         block       = (_block_info_t *)al_heap_alloc(AL_CONTEXT sizeof(_block_info_t)AL_HEAP_DEBUG_PARAM);
         block->next = NULL;

         strcpy(block->command_string, buffer);

         if (data->non_block_post_head == NULL)
         {
            data->non_block_post_head = block;
         }
         else
         {
            tail->next = block;
         }

         tail = block;

         break;
      }
   }

   fclose(fp);

_out:

   return (auto_start_token_t)data;
}


void auto_start_save(auto_start_token_t token)
{
   _auto_start_data_t *data;
   FILE               *fp;
   _block_info_t      *block;

   data = (_auto_start_data_t *)token;

   fp = fopen(data->file_name, "wt");

   if (fp == NULL)
   {
      return;
   }

   block = data->non_block_pre_head;

   while (block != NULL)
   {
      fputs(block->command_string, fp);
      block = block->next;
   }

   fputs("\n"_AUTO_START_BEGIN "\n", fp);

   block = data->block_head;

   while (block != NULL)
   {
      fputs("\n"_BLOCK_BEGIN "\n", fp);

      fputs(block->command_string, fp);

      fputs("\n"_BLOCK_END "\n", fp);

      block = block->next;
   }

   fputs("\n"_AUTO_START_END "\n", fp);

   block = data->non_block_post_head;

   while (block != NULL)
   {
      fputs(block->command_string, fp);
      block = block->next;
   }

   fclose(fp);
}


void auto_start_close(auto_start_token_t token)
{
   _auto_start_data_t *data;
   _block_info_t      *block;

   data = (_auto_start_data_t *)token;

   block = data->non_block_pre_head;

   while (block != NULL)
   {
      data->non_block_pre_head = block->next;
      al_heap_free(AL_CONTEXT block);
      block = data->non_block_pre_head;
   }

   block = data->block_head;

   while (block != NULL)
   {
      data->block_head = block->next;
      al_heap_free(AL_CONTEXT block);
      block = data->block_head;
   }

   block = data->non_block_post_head;

   while (block != NULL)
   {
      data->non_block_post_head = block->next;
      al_heap_free(AL_CONTEXT block);
      block = data->non_block_post_head;
   }

   al_heap_free(AL_CONTEXT data);
}


void auto_start_enumerate_blocks(auto_start_token_t token, auto_start_enum_t enum_func, void *cookie)
{
   _auto_start_data_t *data;
   _block_info_t      *block;
   int                i;

   data = (_auto_start_data_t *)token;

   for (i = 0, block = data->block_head; block != NULL; i++, block = block->next)
   {
      enum_func(cookie, i, block->command_string);
   }
}


void auto_start_append_block(auto_start_token_t token, int block_number, const char *command)
{
   _auto_start_data_t *data;
   _block_info_t      *block;
   int                i;

   data = (_auto_start_data_t *)token;

   for (i = 0, block = data->block_head; (block != NULL) && (i < block_number); i++, block = block->next)
   {
   }

   if (block == NULL)
   {
      return;
   }

   strcat(block->command_string, command);
   strcat(block->command_string, "\n");
}


int auto_start_create_block(auto_start_token_t token)
{
   _auto_start_data_t *data;
   _block_info_t      *tail;
   _block_info_t      *block;
   int                i;

   data = (_auto_start_data_t *)token;

   block       = (_block_info_t *)al_heap_alloc(AL_CONTEXT sizeof(_block_info_t)AL_HEAP_DEBUG_PARAM);
   block->next = NULL;
   block->command_string[0] = 0;

   tail = data->block_head;
   i    = 0;

   while (tail != NULL)
   {
      ++i;
      if (tail->next == NULL)
      {
         break;
      }
      tail = tail->next;
   }

   if (tail == NULL)
   {
      data->block_head = block;
   }
   else
   {
      tail->next = block;
   }

   return i;
}


void auto_start_delete_block(auto_start_token_t token, int block_number)
{
   _auto_start_data_t *data;
   _block_info_t      *prev;
   _block_info_t      *block;
   int                i;

   data = (_auto_start_data_t *)token;

   for (i = 0, prev = NULL, block = data->block_head; (block != NULL) && (i < block_number); i++, prev = block, block = block->next)
   {
   }

   if (block == NULL)
   {
      return;
   }

   if (block == data->block_head)
   {
      data->block_head = block->next;
   }
   if (prev != NULL)
   {
      prev->next = block->next;
   }

   al_heap_free(AL_CONTEXT block);
}
