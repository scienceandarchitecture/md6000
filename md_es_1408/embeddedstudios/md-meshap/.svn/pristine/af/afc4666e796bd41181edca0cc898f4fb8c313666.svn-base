
/********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : mobility_conf.h
 * Comments : Mobility information parser
 * Created  : 5/1/2006
 * Author   : Sriram Dayanandan
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * -----------------------------------------------------------------------------
 * | No  |Date      |  Comment                                        | Author |
 * -----------------------------------------------------------------------------
 * |  0  |05/1/2006 | Created.                                        | Sriram |
 * -----------------------------------------------------------------------------
 ********************************************************************************/

#ifndef __MOBILITY_CONF_H__
#define __MOBILITY_CONF_H__

struct mobility_index_data {
	unsigned int	t_el_max;
	unsigned int	t_el_min;
	unsigned int	qualification;
	unsigned int	max_retry_percent;
	unsigned int	max_error_percent;
	unsigned int	max_errors;
	unsigned int	min_retry_percent;
	unsigned int	min_qualification;
	unsigned int	initial_rate_index;
	unsigned int	round_robin_beacon_count;
	unsigned int	scanner_dwell_interval;		/* Total dwell time in milli-seconds */
	unsigned int	scan_count;					/* Number of times to scan before making decision */
	unsigned int	sampling_t_next_min;		/* Milli seconds */
	unsigned int	sampling_t_next_max;		/* Milli seconds */
	unsigned int	sampling_n_p;				/* Count of packets */
	unsigned int	disconnect_count_max;
	unsigned int	disconnect_multiplier;
	unsigned int	evaluation_damping_factor;
};

typedef struct mobility_index_data mobility_index_data_t;

int		mobility_conf_parse				(AL_CONTEXT_PARAM_DECL const char* mobility_conf_string);
void	mobility_conf_close				(AL_CONTEXT_PARAM_DECL int conf_handle);
int		mobility_conf_get_index_count	(AL_CONTEXT_PARAM_DECL int conf_handle);
int		mobility_conf_get_index_data	(AL_CONTEXT_PARAM_DECL int conf_handle,int index, mobility_index_data_t* data_out);

#endif /*__MOBILITY_CONF_H__*/
