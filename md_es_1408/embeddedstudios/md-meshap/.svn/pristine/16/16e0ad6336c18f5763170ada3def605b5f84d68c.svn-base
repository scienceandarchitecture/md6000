
/********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : dot11e_conf.h
 * Comments : dot11e Config Parser Header
 * Created  : 1/17/2006
 * Author   : Bindu Parakala
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * -----------------------------------------------------------------------------
 * | No  |Date      |  Comment                                        | Author |
 * -----------------------------------------------------------------------------
 * |  1  |8/27/2008 | Size optimization                               | Sriram |
 * -----------------------------------------------------------------------------
 * |  0  |1/17/2006 | Created                                         | Bindu  |
 * -----------------------------------------------------------------------------
 ********************************************************************************/

#ifndef __DOT11E_CONF_H__
#define __DOT11E_CONF_H__

#define DOT11E_CATEGORY_TYPE_AC_BK			0
#define DOT11E_CATEGORY_TYPE_AC_BE			1
#define DOT11E_CATEGORY_TYPE_AC_VI			2
#define DOT11E_CATEGORY_TYPE_AC_VO			3

	/** 802.11e Categories
	 *
	 *	AC_BK 	= 0		(background)
	 *	AC_BE	= 1		(best_effort)
	 *	AC_VI	= 2		(video)
	 *	AC_VO	= 3		(voice)
	 */

struct dot11e_conf_category_details {
	unsigned char	category;
	unsigned short	acwmin;
	unsigned short	acwmax;
	unsigned char	aifsn;
	unsigned char	disable_backoff;
	unsigned int	burst_time;
};

typedef struct dot11e_conf_category_details dot11e_conf_category_details_t;
 
struct dot11e_conf_category {	
	int								parse_mode;
	int								count;
	dot11e_conf_category_details_t*	category_info;
};

typedef struct dot11e_conf_category dot11e_conf_category_t; 


int		dot11e_conf_parse							(AL_CONTEXT_PARAM_DECL const char* dot11e_conf_string);
void	dot11e_conf_close							(AL_CONTEXT_PARAM_DECL int dot11e_conf_handle);


int		dot11e_conf_get_category_info_count			(AL_CONTEXT_PARAM_DECL int dot11e_conf_handle);
int		dot11e_conf_get_category_info				(AL_CONTEXT_PARAM_DECL int dot11e_conf_handle, int index, dot11e_conf_category_details_t* info);




#ifndef _MESHAP_DOT11E_CONF_NO_SET_AND_PUT

int		dot11e_conf_set_category_info_count			(AL_CONTEXT_PARAM_DECL int dot11e_conf_handle, int count);
int		dot11e_conf_set_category_info				(AL_CONTEXT_PARAM_DECL int dot11e_conf_handle, int index, dot11e_conf_category_details_t* info);
int		dot11e_conf_create_config_string_from_data	(AL_CONTEXT_PARAM_DECL int dot11e_conf_handle,int* config_string_length,char** dot11e_conf_string);
int		dot11e_conf_put								(AL_CONTEXT_PARAM_DECL int dot11e_conf_handle);

#endif /* _MESHAP_DOT11E_CONF_NO_SET_AND_PUT */

#endif /* __DOT11E_CONF_H__*/


