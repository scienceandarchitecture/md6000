
/********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : meshap.h
 * Comments : Torna MeshAP entry point header
 * Created  : 5/12/2004
 * Author   : Sriram Dayanandan
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * -----------------------------------------------------------------------------
 * | No  |Date      |  Comment                                        | Author |
 * -----------------------------------------------------------------------------
 * | 27  |02/12/2009| Changes for Action Frame Support				  |Abhijit |
 * -----------------------------------------------------------------------------
 * | 26  |05/09/2008| Changes for Beaconing Uplink		              |Prachiti|
 * -----------------------------------------------------------------------------
 * | 25  |01/07/2008| Changes for mixed mode						  |Prachiti|
 * -----------------------------------------------------------------------------
 * | 24  |11/28/2007| Probe Request Hook added                        | Sriram |
 * -----------------------------------------------------------------------------
 * | 23  |7/24/2007 | REBOOT logging changes                          | Sriram |
 * -----------------------------------------------------------------------------
 * | 22  |7/9/2007  | Reset generator architecture changed            | Sriram |
 * -----------------------------------------------------------------------------
 * | 21  |4/11/2007 | Changes for radio diagnostics                   | Sriram |
 * -----------------------------------------------------------------------------
 * | 20  |2/15/2007 | Added set_tx_antenna                            | Sriram |
 * -----------------------------------------------------------------------------
 * | 19  |1/9/2007  | Changes for channel specific DFS                |Prachiti|
 * -----------------------------------------------------------------------------
 * | 18  |12/14/2006| Reset generator changes                         | Sriram |
 * -----------------------------------------------------------------------------
 * | 17  |11/8/2006 | Changes for compression                         | Sriram |
 * -----------------------------------------------------------------------------
 * | 16  |10/31/2006| Custom frequency changes                        | Sriram |
 * -----------------------------------------------------------------------------
 * | 15  |10/18/2006| Changes for ETSI Radar                          | Sriram |
 * -----------------------------------------------------------------------------
 * | 14  |02/15/2006| Changes for dot11e                              | Bindu  |
 * -----------------------------------------------------------------------------
 * | 13  |02/08/2006| Changes for Hide SSID                           | Sriram |
 * -----------------------------------------------------------------------------
 * | 12  |12/16/2005|supported channels changes					      |prachiti|
 * -----------------------------------------------------------------------------
 * | 11  |12/14/2005| acktimeout and regdomain changes                |prachiti|
 * -----------------------------------------------------------------------------
 * | 10  |9/15/2005 | meshap_get_sta_info Added                       | Sriram |
 * -----------------------------------------------------------------------------
 * |  9  |8/20/2005 | get_bit_rate_info added                         | Sriram |
 * -----------------------------------------------------------------------------
 * |  8  |6/12/2005 | Misc changes                                    | Sriram |
 * -----------------------------------------------------------------------------
 * |  7  |4/12/2005 | set_hw_addr method added                        | Sriram |
 * -----------------------------------------------------------------------------
 * |  6  |4/8/2005  | Changes after merging                           | Sriram |
 * -----------------------------------------------------------------------------
 * |  5  |4/6/2005  | Security changes                                | Sriram |
 * -----------------------------------------------------------------------------
 * |  4  |03/09/2005| Vendor Info added to Beacons & ScanAP           | Anand  |
 * -----------------------------------------------------------------------------
 * |  3  |1/7/2005  | get_last_beacon_time added                      | Sriram |
 * -----------------------------------------------------------------------------
 * |  2  |10/14/2004| 3 radio changes                                 | Sriram |
 * -----------------------------------------------------------------------------
 * |  1  |6/5/2004  | Misc changes                                    | Sriram |
 * -----------------------------------------------------------------------------
 * |  0  |5/12/2004 | Created.                                        | Sriram |
 * -----------------------------------------------------------------------------
 ********************************************************************************/

#ifndef __MESHAP_H__
#define __MESHAP_H__

#define MESHAP_NET_DEV_ENCAPSULATION_ETHERNET				0
#define MESHAP_NET_DEV_ENCAPSULATION_802_3					2
#define MESHAP_NET_DEV_ENCAPSULATION_802_11					3

#define MESHAP_NET_DEV_PHY_HW_TYPE_802_3					0
#define MESHAP_NET_DEV_PHY_HW_TYPE_802_11_B					1
#define MESHAP_NET_DEV_PHY_HW_TYPE_802_11_A					2
#define MESHAP_NET_DEV_PHY_HW_TYPE_802_11_B_G				3
#define MESHAP_NET_DEV_PHY_HW_TYPE_802_11_G					4
#define MESHAP_NET_DEV_PHY_HW_TYPE_802_11_ATHEROS_TURBO		5 /* Proprietary Atheros turbo mode (double bonding) */

#define MESHAP_NET_DEV_TXPOW_DBM							0x0001  /* Value is in dBm */
#define MESHAP_NET_DEV_TXPOW_MWATT							0x0002  /* Value is in mW */
#define MESHAP_NET_DEV_TXPOW_PERC							0x0004	/* Value is in Percentage */
#define MESHAP_NET_DEV_TXPOW_FIXED							0x0008  /* Power fixed by software */

struct meshap_scan_access_point_info {
	int				mode;
	unsigned char	bssid[ETH_ALEN];
	int				channel;
	char			essid[IW_ESSID_MAX_SIZE + 1];
	unsigned char	signal;
	unsigned char	noise;
	unsigned short	beacon_interval;
	unsigned short	capabilities;
	unsigned char	supported_rates[15];
	unsigned char	probe_response_rate;
	unsigned char	vendor_info_length;
	unsigned char	vendor_info[256];
};

typedef struct meshap_scan_access_point_info meshap_scan_access_point_info_t;

struct meshap_tx_power_info {
	int		flags;
	int		power;
};

typedef struct meshap_tx_power_info meshap_tx_power_info_t;

#define MESHAP_SECURITY_KEY_INFO_STRENGTH_40			40
#define MESHAP_SECURITY_KEY_INFO_STRENGTH_104			104

#define MESHAP_SECURITY_KEY_INFO_CIPHER_NONE			1
#define MESHAP_SECURITY_KEY_INFO_CIPHER_WEP				2
#define MESHAP_SECURITY_KEY_INFO_CIPHER_TKIP			4
#define MESHAP_SECURITY_KEY_INFO_CIPHER_CCMP			8
#define MESHAP_SECURITY_KEY_INFO_CIPHER_IMCP			16
#define MESHAP_SECURITY_KEY_INFO_CIPHER_GROUP			32
#define MESHAP_SECURITY_KEY_INFO_CIPHER_WEP_104			64

struct meshap_security_key_info {
	unsigned char	destination[ETH_ALEN];
	unsigned short	vlan_tag;
	unsigned char	cipher_type;	/* MESHAP_SECURITY_INFO_RSN_CIPHER_* */
	unsigned char	key_index;		/* Only valid for MESHAP_SECURITY_INFO_CIPHER_WEP or broadcast dest */
	unsigned char	strength;		/* Only valid for MESHAP_SECURITY_INFO_CIPHER_WEP */
	/**
	 * For WEP this contains all the 4 keys concatenated together 
	 * 40 bit means 5 bytes each totalling to 20
	 * 104 bit means 13 bytes each totalling to 52
	 * For CCMP and TKIP only the first 32 bytes are valid
	 */
	unsigned char	key[52];
};

typedef struct meshap_security_key_info meshap_security_key_info_t;

struct meshap_security_key_data {
	int				cipher_type;	/** MESHAP_SECURITY_INFO_RSN_CIPHER_* */
	unsigned int	wep_iv;			/** The WEP Initialization Vector */
	u64				key_rsc;		/** The TKIP/CCMP Receive Sequence Counter */
	u64				key_tsc;		/** The TKIP/CCMP Transmit Sequence Counter */
};

typedef struct meshap_security_key_data meshap_security_key_data_t;

#define MESHAP_ESSID_MAX_SIZE					32

#define MESHAP_ESSID_SECURITY_INFO_WEP_ENABLED	0x001
#define MESHAP_ESSID_SECURITY_INFO_RSN_ENABLED	0x002

struct meshap_essid_security_info {
	unsigned int	flags;
	unsigned char	rsn_ie_buffer[256];
	int				rsn_ie_length;
};

typedef struct meshap_essid_security_info meshap_essid_security_info_t;

struct meshap_essid_info {
	meshap_essid_security_info_t	sec_info;
	unsigned short					vlan_tag;
	char							essid[MESHAP_ESSID_MAX_SIZE + 1];
};

typedef struct meshap_essid_info meshap_essid_info_t;

/**
 * F/W 2.5.26 onwards :
 * Regulatory domain code field is 16 bits and is formatted
 * as follows :
 *
 *    15   14   13   12   11   10   9    8  | 7    6    5    4    3    2     1   0
 *  +----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+
 *  |            RESERVED              |DFS |           REG DOMAIN CODE             |
 *  +----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+
 *                                          |
 * Format for the priv_channel_flags field to be used by F/W 2.5.30 onwards
 *
 * 07   06  05  04  03  02  01  00
 * ---+---+---+---+---+---+---+---+
 *|			RESERVED		  |DFS|
 * ---+---+---+---+---+---+---+---+
*/


#define MESHAP_REG_DOMAIN_NONE						0
#define MESHAP_REG_DOMAIN_FCC						1
#define MESHAP_REG_DOMAIN_ETSI						2
#define MESHAP_REG_DOMAIN_CUSTOM					3

#define MESHAP_REG_DOMAIN_COUNTRY_CODE_CUSTOM		1

#define MESHAP_REG_DOMAIN_CTL_NONE					0
#define MESHAP_REG_DOMAIN_CTL_FCC					1
#define MESHAP_REG_DOMAIN_CTL_ETSI					2
#define MESHAP_REG_DOMAIN_CTL_MKK					3


#define MESHAP_REG_DOMAIN_NEEDS_DFS(rdc)					(rdc & 0x0100)
#define MESHAP_REG_DOMAIN_CODE_GET_DOMAIN(rdc)				(rdc & 0x00FF)
#define MESHAP_GET_DFS_FROM_CHANNEL_FLAGS(channel_flags)	(channel_flags & 0x1)

struct meshap_reg_domain_info {
	unsigned short	country_code;	/* MESHAP_REG_DOMAIN_COUNTRY_CODE_CUSTOM or a valid code */
	unsigned short	reg_domain;
	/** The following fields are only valid for Custom Country code */
	unsigned char	bandwidth;
	unsigned char	max_power;
	unsigned char	channel_count;
	unsigned short*	frequencies;
	unsigned char*	channel_flags;
	unsigned char*	channel_numbers;
	unsigned char	antenna_max;
	unsigned char	conformance_test_limit;
};

typedef struct meshap_reg_domain_info meshap_reg_domain_info_t;

struct meshap_channel_info {
	unsigned char	number;
	unsigned int	frequency;		/** In MHz */
	unsigned int	flags;
	char			max_reg_power;
	char			max_power;
	char			min_power;
};

typedef struct meshap_channel_info meshap_channel_info_t;

/** 802.11e Categories
 *
 *	AC_BK 	= 0		(background)
 *	AC_BE	= 1		(best_effort)
 *	AC_VI	= 2		(video)
 *	AC_VO	= 3		(voice)
 */

struct meshap_dot11e_category_details {
	unsigned char	category;
	unsigned short	acwmin;
	unsigned short	acwmax;
	unsigned char	aifsn;
	unsigned char	disable_backoff;
	unsigned int	burst_time;
};

typedef struct meshap_dot11e_category_details meshap_dot11e_category_details_t;

struct meshap_dot11e_category_info {
	unsigned char						count;
	meshap_dot11e_category_details_t*	category_info;		/* 802.11e Category Info */	
};

typedef struct meshap_dot11e_category_info meshap_dot11e_category_info_t;

struct meshap_duty_cycle_info {
	int			channel;
	int			number_of_aps;							/* Count of All APs found */
	struct {
		char			essid[IW_ESSID_MAX_SIZE + 1];
		unsigned char	bssid[ETH_ALEN];
		unsigned char	signal;							/* Expressed as units above -96 dBm */
		unsigned int	transmit_duration;				/* Total transmit air time in Micro-seconds */
		unsigned int	retry_duration;					/* Total retry air time in Micro-seconds */
	}*aps;
	int					max_signal;						/* Max power expressed as units above -96 dBm */
	int					avg_signal;						/* Avg power expressed as units above -96 dBm */
	unsigned int		total_duration;					/** Total dwell time in Micro-seconds */
	unsigned int		transmit_duration;				/** Total transmit air time in Micro-seconds */
	unsigned int		retry_duration;					/* Total retry air time in Micro-seconds */
};

typedef struct meshap_duty_cycle_info meshap_duty_cycle_info_t;

struct meshap_rate_ctrl_param {
	 int	t_el_max;			/* Seconds */
	 int	t_el_min;			/* Seconds */
	 int	qualification;		/* Number of packets */
	 int	max_retry_percent;
	 int	max_error_percent;
	 int	max_errors;			/* Count */ 
	 int	min_retry_percent;
	 int	min_qualification;	/* Number of packets */
	 int	initial_rate_index;	/* Rate index in rate table */
};

typedef struct meshap_rate_ctrl_param meshap_rate_ctrl_param_t;

#define MESHAP_RATE_TABLE_MAX_COUNT	24

struct	meshap_rate_table {
	int		count;
	struct {
		int		index;
		int		rate_in_mbps;
	}rates[MESHAP_RATE_TABLE_MAX_COUNT];
};

typedef struct	meshap_rate_table meshap_rate_table_t;

#define MESHAP_RATE_CTRL_OUTLOOK_POSSITIVE	1
#define MESHAP_RATE_CTRL_OUTLOOK_NEGATIVE	-1

struct meshap_rate_ctrl_info {
	int				t_elapsed;
	int				t_ch;
	int				rate_index;
	int				rate_in_mbps;
	int				tx_success;
	int				tx_error;
	int				tx_retry;
	int				confidence;
	int				outlook;
	unsigned int	last_check;
	unsigned char	avg_ack_rssi;
};

typedef struct meshap_rate_ctrl_info meshap_rate_ctrl_info_t;

typedef void (*meshap_round_robin_notify_t)			(struct net_device* dev, void* dev_token);
typedef void (*meshap_radar_notify_t)				(struct net_device* dev, void* dev_token, int channel);
typedef void (*meshap_probe_request_notify_t)		(struct net_device* dev, void* dev_token, unsigned char* sta_addr, int rssi);
typedef void (*meshap_action_notify_t)				(struct net_device* dev, void* dev_token, unsigned char* sender, unsigned char* data, int length, int iw_mode);

struct meshap_net_dev {
	int		encapsulation;	/* MESHAP_NET_DEV_ENCAPSULATION_* */
	int		phy_hw_type;	/* MESHAP_NET_DEV_PHY_HW_TYPE_* */

	int							(*set_hw_addr)							(struct net_device* dev, unsigned char* hw_addr);
	int							(*associate)							(struct net_device* dev, unsigned char* bssid,int channel,const char* essid, int length, int timeout, unsigned char* ie_in, int ie_in_length,unsigned char* ie_out);
	int							(*dis_associate)						(struct net_device* dev);
	int							(*get_bssid)							(struct net_device* dev, unsigned char* bssid);
	int							(*scan_access_points)					(struct net_device* dev ,meshap_scan_access_point_info_t** access_points, int* count, int scan_duration_in_millis, int channel_count, unsigned char* channels);
	int							(*scan_access_points_active)			(struct net_device* dev, meshap_scan_access_point_info_t** access_points, int* count, int scan_duration_in_millis, int channel_count, unsigned char* channels);
	int							(*scan_access_points_passive)			(struct net_device* dev, meshap_scan_access_point_info_t** access_points, int* count, int scan_duration_in_millis, int channel_count, unsigned char* channels);
	u64							(*get_last_beacon_time)					(struct net_device* dev);
	void						(*set_mesh_downlink_round_robin_time)	(struct net_device* dev, int beacon_intervals);
	void						(*add_downlink_round_robin_child)		(struct net_device* dev, unsigned char* addr);
	void						(*remove_downlink_round_robin_child)	(struct net_device* dev, unsigned char* addr);
	int							(*virtual_associate)					(struct net_device* dev, unsigned char* ap_addr, int channel, int start);
	int							(*get_duty_cycle_info)					(struct net_device* dev, int channel_count,meshap_duty_cycle_info_t* info,unsigned int dwell_time_minis);
	void						(*set_rate_ctrl_parameters)				(struct net_device* dev, meshap_rate_ctrl_param_t* rate_ctrl_param);
	void						(*reset_rate_ctrl)						(struct net_device* dev, unsigned char* addr);
	void						(*get_rate_ctrl_info)					(struct net_device* dev, unsigned char* addr, meshap_rate_ctrl_info_t* rate_ctrl_info);
	void						(*set_round_robin_notify_hook)			(struct net_device* dev, meshap_round_robin_notify_t hook);
	void						(*set_probe_request_notify_hook)		(struct net_device* dev, meshap_probe_request_notify_t hook);
	void						(*enable_ds_verification_opertions)		(struct net_device* dev, int enable);

	int							(*dfs_scan)								(struct net_device* dev);
	void						(*set_radar_notify_hook)				(struct net_device* dev, meshap_radar_notify_t hook);

	int							(*get_mode)								(struct net_device* dev);
	int							(*set_mode)								(struct net_device* dev, int mode, unsigned char master_quiet);
	int							(*get_essid)							(struct net_device* dev, char* essid_buffer, int length);
	int							(*set_essid)							(struct net_device* dev, char* essid_buffer);
	int							(*get_rts_threshold)					(struct net_device* dev);
	int							(*set_rts_threshold)					(struct net_device* dev, int threshold);
	int							(*get_frag_threshold)					(struct net_device* dev);
	int							(*set_frag_threshold)					(struct net_device* dev, int threshold);
	int							(*get_beacon_interval)					(struct net_device* dev);
	int							(*set_beacon_interval)					(struct net_device* dev, int interval);
	int							(*get_default_capabilities)				(struct net_device* dev);
	int							(*get_capabilities)						(struct net_device* dev);
	int							(*get_slot_time_type)					(struct net_device* dev);
	int							(*set_slot_time_type)					(struct net_device* dev, int slot_time_type);
	int							(*get_erp_info)							(struct net_device* dev);
	int							(*set_erp_info)							(struct net_device* dev, int erp_info);
	int							(*set_beacon_vendor_info)				(struct net_device* dev, unsigned char* vendor_info_buffer, int length);
	int							(*get_ack_timeout)						(struct net_device* dev);
	int							(*set_ack_timeout)						(struct net_device* dev, unsigned short ack_timeout);
	int							(*get_hide_ssid)						(struct net_device* dev);
	int							(*set_hide_ssid)						(struct net_device* dev, unsigned char hide);

	int							(*enable_wep)							(struct net_device* dev);
	int							(*disable_wep)							(struct net_device* dev);
	int							(*set_rsn_ie)							(struct net_device* dev, unsigned char* rsn_ie,int rsn_ie_length);
	int							(*set_security_key)						(struct net_device* dev, meshap_security_key_info_t* key_info,int enable_compression);
	int							(*release_security_key)					(struct net_device* dev, int index);
	int							(*get_security_key_data)				(struct net_device* dev, int index,meshap_security_key_data_t* security_key_data);
	int							(*set_ds_security_key)					(struct net_device* dev, unsigned char* group_key,unsigned char group_key_type,unsigned char group_key_index,unsigned char* pairwise_key,int enable_compression);

	const unsigned char*		(*get_supported_rates)					(struct net_device* dev,int* length);
	const unsigned char*		(*get_extended_rates)					(struct net_device* dev,int* length);
	int							(*get_bit_rate)							(struct net_device* dev);
	int							(*set_bit_rate)							(struct net_device* dev, int bit_rate);
	void						(*get_rate_table)						(struct net_device* dev, meshap_rate_table_t* rate_table);
	int							(*get_tx_power)							(struct net_device* dev, meshap_tx_power_info_t* power_info);
	int							(*set_tx_power)							(struct net_device* dev, meshap_tx_power_info_t* power_info);
	int							(*get_channel_count)					(struct net_device* dev);
	int						 	(*get_channel)							(struct net_device* dev);
	int							(*set_channel)							(struct net_device* dev, int channel);
	int							(*get_phy_mode)							(struct net_device* dev);
	int							(*set_phy_mode)							(struct net_device* dev, int phy_mode);
	int							(*get_preamble_type)					(struct net_device* dev);
	int							(*set_preamble_type)					(struct net_device* dev, int preamble_type);
	int						  	(*get_reg_domain_info)					(struct net_device* dev, meshap_reg_domain_info_t* reg_domain_info);
	int							(*set_reg_domain_info)					(struct net_device* dev, meshap_reg_domain_info_t* reg_domain_info);
	int							(*get_supported_channels)				(struct net_device* dev, meshap_channel_info_t** supported_channels,int* count);
	int							(*set_tx_antenna)						(struct net_device* dev, int antenna_index);

	int							(*set_dev_token)						(struct net_device* dev, void* token);
	int							(*set_essid_info)						(struct net_device* dev, int count, meshap_essid_info_t* info);
	int							(*set_dot11e_category_info)				(struct net_device* dev, meshap_dot11e_category_info_t* info);

	int							(*set_radio_data)						(struct net_device* dev, unsigned char* radio_data, int data_length);
	int							(*radio_diagnostic_command)				(struct net_device* dev, unsigned char* diag_data_in, int data_in_length, unsigned char* diag_data_out, int data_out_buf_len);
	void						(*set_virtual_mode)						(struct net_device* dev, unsigned char virtual_mode);
	void						(*set_device_type)						(struct net_device* dev, unsigned char device_type);
	unsigned char				(*get_device_type)						(struct net_device* dev);
	int							(*initialize_mixed_mode)				(struct net_device* dev);
	int							(*enable_beaconing_uplink)				(struct net_device* dev, unsigned char* vendor_info_buffer, int length);
	int							(*disable_beaconing_uplink)				(struct net_device* dev);	

	int							(*set_action_hook)						(struct net_device* dev, meshap_action_notify_t action_hook);
	int							(*send_action)							(struct net_device* dev, unsigned char* addr, unsigned char* buffer, int length);
};

typedef struct meshap_net_dev meshap_net_dev_t;

#define MESHAP_STA_INFO_FLAG_ENCRYPTION			0x00000001
#define MESHAP_STA_INFO_FLAG_USE_802_11_B		0x00000002
#define MESHAP_STA_INFO_FLAG_COMPRESSION		0x00000004

struct meshap_sta_info {
	unsigned int	flags;
	int				key_index;

};

typedef struct meshap_sta_info meshap_sta_info_t;

#define MESHAP_ON_LINK_NOTIFY_STATE_LINK_OFF		0x00000001
#define MESHAP_ON_LINK_NOTIFY_STATE_LINK_ON			0x00000002
#define MESHAP_ON_LINK_NOTIFY_STATE_ASSOCIATED		0x00000004
#define MESHAP_ON_LINK_NOTIFY_STATE_DISASSOCIATED	0x00000008
#define MESHAP_ON_LINK_NOTIFY_STATE_ACCESS_POINT	0x00000010

void	meshap_process_mgmt_frame		(struct net_device* dev,void* dev_token,struct sk_buff* skb);
void	meshap_process_data_frame		(struct net_device* dev,void* dev_token,struct sk_buff* skb);
void	meshap_on_link_notify			(struct net_device* dev,void* dev_token,u32 state_flags);
void	meshap_on_net_device_create		(struct net_device* dev,meshap_net_dev_t* dev_info);
void	meshap_on_net_device_destroy	(struct net_device* dev,void* dev_token);
int		meshap_get_sta_info				(unsigned char* mac_addr,meshap_sta_info_t* sta_info);

#define MESHAP_REBOOT_MACHINE_CODE_DEVICE			1
#define MESHAP_REBOOT_MACHINE_CODE_PACKET			2
#define MESHAP_REBOOT_MACHINE_CODE_TX_TIMEOUT		3
#define MESHAP_REBOOT_MACHINE_CODE_DESC_BLOCKAGE	4
#define MESHAP_REBOOT_MACHINE_CODE_COMMAND_LINE		5
#define MESHAP_REBOOT_MACHINE_CODE_CRASH			6

void	meshap_reboot_machine			(unsigned char reboot_code);

/**
 * Implemented by board info driver
 */

int		meshap_get_board_temp			(void);
int		meshap_get_board_voltage		(void);
void	meshap_set_led_on				(void);
void	meshap_set_led_off				(void);
void	meshap_set_led_blink			(void);
void	meshap_set_led_blink_fast		(void);
void	meshap_set_led_blink_once		(void);
void	meshap_enable_reset_generator	(unsigned char enable_or_disable, int interval_in_millis);
void	meshap_strobe_reset_generator	(void);
int		meshap_get_gpio					(unsigned char line);
void	meshap_set_gpio					(unsigned char line, int value);
void	meshap_set_gps_info				(const char* longitude , const char* latitude, const char* altitude, const char* speed);
void	meshap_get_gps_info				(char* longitude , char* latitude, char* altitude, char* speed);


#define MESHAP_PHYSICAL_DEV_MODE_NONE		0
#define MESHAP_VIRTUAL_DEV_MODE_MASTER		1
#define MESHAP_VIRTUAL_DEV_MODE_INFRA		2

/* 
*	Used for checking mixed mode
*/

#define MESHAP_DEV_MODE_MIXED				1
#define MESHAP_DEV_MODE_NORMAL				2

#endif /*__MESHAP_H__*/

