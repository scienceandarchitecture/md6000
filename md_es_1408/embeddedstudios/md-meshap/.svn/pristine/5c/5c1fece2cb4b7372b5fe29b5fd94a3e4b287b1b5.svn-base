
/********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : imcp_heartbeat.c
 * Comments : MESH HeartBeat IMCP packet impl file.
 * Created  : 8/16/2004
 * Author   : Anand Bhange
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * -----------------------------------------------------------------------------
 * | No  |Date      |  Comment                                        | Author |
 * -----------------------------------------------------------------------------
 * |  9  |11/20/2007| GPS Information added to heartbeat              | Sriram |
 * -----------------------------------------------------------------------------
 * |  8  |11/15/2006| Memory leak problem fixed                       | Sriram |
 * -----------------------------------------------------------------------------
 * |  7  |02/03/2006| Misc changes                                    | Abhijit|
 * -----------------------------------------------------------------------------
 * |  6  |10/21/2005| Added imcp_snip_heartbeat2_get_buffer			  | Abhijit|
 * -----------------------------------------------------------------------------
 * |  5  |03/09/2005| Board Temp added to heartbeat info			  | Anand  |
 * -----------------------------------------------------------------------------
 * |  4  |11/30/2004| Bug Fixed for Tx Rate                           | Anand  |
 * -----------------------------------------------------------------------------
 * |  3  |11/25/2004| le2cpu funcs called for packets                 | Anand  |
 * -----------------------------------------------------------------------------
 * |  2  |10/6/2004 | Added tx_byte_count,rx_byte_count               | Abhijit|
 * -----------------------------------------------------------------------------
 * |  1  |10/6/2004 | Added tx_packet_count,rx_packet_count           | Abhijit|
 * -----------------------------------------------------------------------------
 * |  0  |8/16/2004 | Created.                                        | Anand  |
 * -----------------------------------------------------------------------------
 ********************************************************************************/

static int imcp_snip_heartbeat_get_buffer (AL_CONTEXT_PARAM_DECL imcp_packet_info_t* pi, unsigned char* buffer, unsigned int buf_length)
{
	unsigned char*	p;
	int				i;
	unsigned int	req_length;
	unsigned int	cpu2le;
	unsigned short	cpu2le16;

	AL_ASSERT("MESH_AP	: imcp_snip_heartbeat_get_buffer", buffer != NULL);
	AL_ASSERT("MESH_AP	: imcp_snip_heartbeat_get_buffer", pi != NULL);

	AL_PRINT_LOG_FLOW_0( "MESH_AP	: imcp_snip_heartbeat_get_buffer()");
	req_length	= (59 + (pi->u.heart_beat.known_ap_count * 8));
	req_length	+= pi->u.heart_beat.children_count  * 6;
	req_length	+= 12;	/** GPS (Long, Lat, altitude and speed) */

	/**
	 * Buffer length must be atleast req_length 
	 */

	if(buf_length < req_length)
		return IMCP_SNIP_ERROR_INADEQUATE_BUFFER_LENGTH;

	p	= buffer;

	/**
	 * Copy Sender DS MAC, SQNR, CTC, HPC, ROOT bssid, CRSSI, HI, CH, PARENT Bssid
	 * Known AP count, 
	 */
	memcpy(p,	pi->u.heart_beat.ds_mac.bytes,			MAC_ADDR_LENGTH);		p += MAC_ADDR_LENGTH;
	cpu2le		= al_impl_cpu_to_le32(pi->u.heart_beat.sqnr);
	memcpy(p,	&cpu2le,								4);						p += 4;
	cpu2le		= al_impl_cpu_to_le32(pi->u.heart_beat.ctc);
	memcpy(p,	&cpu2le,								4);						p += 4;
	memcpy(p,	&pi->u.heart_beat.hpc,					1);						p += 1;
	memcpy(p,	pi->u.heart_beat.root_bssid.bytes,		MAC_ADDR_LENGTH);		p += MAC_ADDR_LENGTH;
	cpu2le		= al_impl_cpu_to_le32(pi->u.heart_beat.crssi);
	memcpy(p,	&cpu2le,								4);						p += 4;
	memcpy(p,	&pi->u.heart_beat.hi,					1);						p += 1;
	memcpy(p,	&pi->u.heart_beat.hbi,					1);						p += 1;
	cpu2le		= al_impl_cpu_to_le32(pi->u.heart_beat.bit_rate);
	memcpy(p,	&cpu2le,								4);						p += 4;	
	memcpy(p,	&pi->u.heart_beat.children_count,		1);						p += 1;
	cpu2le		= al_impl_cpu_to_le32(pi->u.heart_beat.board_temp);
	memcpy(p,	&cpu2le,								4);						p += 4;	
	memcpy(p,	pi->u.heart_beat.parent_bssid.bytes,	MAC_ADDR_LENGTH);		p += MAC_ADDR_LENGTH;
	memcpy(p,	&pi->u.heart_beat.known_ap_count,		1);						p += 1;
	cpu2le		= al_impl_cpu_to_le32(pi->u.heart_beat.tx_packet_count);
	memcpy(p,	&cpu2le,								4);						p += 4;
	cpu2le		= al_impl_cpu_to_le32(pi->u.heart_beat.rx_packet_count);
	memcpy(p,	&cpu2le,								4);						p += 4;
	cpu2le		= al_impl_cpu_to_le32(pi->u.heart_beat.tx_byte_count);
	memcpy(p,	&cpu2le,								4);						p += 4;
	cpu2le		= al_impl_cpu_to_le32(pi->u.heart_beat.rx_byte_count);
	memcpy(p,	&cpu2le,								4);						p += 4;

	for(i = 0; i < pi->u.heart_beat.known_ap_count; i++) {
		memcpy(p,	&pi->u.heart_beat.known_aps_info[i].et,				1);					p += 1;
		memcpy(p,	pi->u.heart_beat.known_aps_info[i].ap_ds_mac.bytes,	MAC_ADDR_LENGTH);	p += MAC_ADDR_LENGTH;
		memcpy(p,	&pi->u.heart_beat.known_aps_info[i].st,				1);					p += 1;
	}

	for(i = 0; i < pi->u.heart_beat.children_count; i++) {
		memcpy(p,	&pi->u.heart_beat.immidigiate_sta[i].bytes,			MAC_ADDR_LENGTH);	p += MAC_ADDR_LENGTH;		
	}

	/**
	 * GPS coordinate informatiton
	 */

	cpu2le		= al_impl_cpu_to_le32(pi->u.heart_beat.longitude);
	memcpy(p,	&cpu2le,								4);						p += 4;

	cpu2le		= al_impl_cpu_to_le32(pi->u.heart_beat.latitude);
	memcpy(p,	&cpu2le,								4);						p += 4;

	cpu2le16	= al_impl_cpu_to_le16(pi->u.heart_beat.altitude);
	memcpy(p,	&cpu2le16,								2);						p += 2;	

	cpu2le16	= al_impl_cpu_to_le16(pi->u.heart_beat.speed);
	memcpy(p,	&cpu2le16,								2);						p += 2;	

	return req_length;

}

static int imcp_snip_heartbeat_get_packet (AL_CONTEXT_PARAM_DECL unsigned char* buffer, unsigned int buf_length, imcp_packet_info_t* pi)
{
	unsigned char*	p;
	int				i;
	unsigned int	le2cpu;

#define _REQ_LENGTH		59

	AL_PRINT_LOG_FLOW_0( "MESH_AP	: imcp_snip_heartbeat_get_packet()");
	AL_ASSERT("MESH_AP	: imcp_snip_heartbeat_get_packet", pi != NULL);
	AL_ASSERT("MESH_AP	: imcp_snip_heartbeat_get_packet", buffer != NULL);
	/**
	 * Buffer length must be atleast req_length 
	 */

	if(buf_length < _REQ_LENGTH)
		return IMCP_SNIP_ERROR_INADEQUATE_BUFFER_LENGTH;

	p = buffer;

	/**
	 * Copy Sender DS MAC, SQNR, CTC, HPC, ROOT bssid, CRSSI, HI, CH, PARENT Bssid
	 * Known AP count, 
	 */
	memcpy(pi->u.heart_beat.ds_mac.bytes,		p,		MAC_ADDR_LENGTH);	p += MAC_ADDR_LENGTH;
	pi->u.heart_beat.ds_mac.length = MAC_ADDR_LENGTH;
	
	memcpy(&le2cpu,				p,						4);					p += 4;
	pi->u.heart_beat.sqnr = al_impl_le32_to_cpu(le2cpu);
	memcpy(&le2cpu,				p,						4);					p += 4;
	pi->u.heart_beat.ctc = al_impl_le32_to_cpu(le2cpu);
	memcpy(&pi->u.heart_beat.hpc,				p,		1);					p += 1;
	memcpy(pi->u.heart_beat.root_bssid.bytes,	p,		MAC_ADDR_LENGTH);	p += MAC_ADDR_LENGTH;
	pi->u.heart_beat.root_bssid.length = MAC_ADDR_LENGTH;
	memcpy(&le2cpu,				p,						4);					p += 4;
	pi->u.heart_beat.crssi = al_impl_le32_to_cpu(le2cpu);
	memcpy(&pi->u.heart_beat.hi,				p,		1);					p += 1;
	memcpy(&pi->u.heart_beat.hbi,				p,		1);					p += 1;
	memcpy(&le2cpu,				p,						4);					p += 4;
	pi->u.heart_beat.bit_rate = al_impl_le32_to_cpu(le2cpu);
	memcpy(&pi->u.heart_beat.children_count,	p,		1);					p += 1;
	memcpy(&le2cpu,				p,						4);					p += 4;
	pi->u.heart_beat.board_temp = al_impl_le32_to_cpu(le2cpu);
	memcpy(pi->u.heart_beat.parent_bssid.bytes,	p,		MAC_ADDR_LENGTH);	p += MAC_ADDR_LENGTH;
	pi->u.heart_beat.parent_bssid.length = MAC_ADDR_LENGTH;
	memcpy(&pi->u.heart_beat.known_ap_count,	p,		1);					p += 1;
	memcpy(&le2cpu,				p,						4);					p += 4;
	pi->u.heart_beat.tx_packet_count = al_impl_le32_to_cpu(le2cpu);
	memcpy(&le2cpu,				p,						4);					p += 4;
	pi->u.heart_beat.rx_packet_count = al_impl_le32_to_cpu(le2cpu);
	memcpy(&le2cpu,				p,						4);					p += 4;
	pi->u.heart_beat.tx_byte_count = al_impl_le32_to_cpu(le2cpu);
	memcpy(&le2cpu,				p,						4);					p += 4;
	pi->u.heart_beat.rx_byte_count = al_impl_le32_to_cpu(le2cpu);

	if(pi->u.heart_beat.known_ap_count > 0) {
		pi->u.heart_beat.known_aps_info = (imcp_known_ap_info_t*)al_heap_alloc(AL_CONTEXT pi->u.heart_beat.known_ap_count	* sizeof(imcp_known_ap_info_t) AL_HEAP_DEBUG_PARAM);
		for(i = 0; i < pi->u.heart_beat.known_ap_count; i++) {
			memcpy(&pi->u.heart_beat.known_aps_info[i].et,			p,	1);					p += 1;
			memcpy(pi->u.heart_beat.known_aps_info[i].ap_ds_mac.bytes,p,	MAC_ADDR_LENGTH);	p += MAC_ADDR_LENGTH;
			pi->u.heart_beat.known_aps_info[i].ap_ds_mac.length = MAC_ADDR_LENGTH;
			memcpy(&pi->u.heart_beat.known_aps_info[i].st,			p,	1);					p += 1;
		}
	} else {
		pi->u.heart_beat.known_aps_info	= NULL;
	}

	if(pi->u.heart_beat.children_count > 0) {
		pi->u.heart_beat.immidigiate_sta = (al_net_addr_t*)al_heap_alloc(AL_CONTEXT pi->u.heart_beat.children_count	* sizeof(al_net_addr_t) AL_HEAP_DEBUG_PARAM);
		for(i = 0; i < pi->u.heart_beat.children_count; i++) {
			memcpy(&pi->u.heart_beat.immidigiate_sta[i].bytes,		p,		MAC_ADDR_LENGTH);	p += MAC_ADDR_LENGTH;
			pi->u.heart_beat.immidigiate_sta[i].length = MAC_ADDR_LENGTH;
		}
	} else {
		pi->u.heart_beat.immidigiate_sta = NULL;
	}

	p	+= 12; /** Skip over GPS information */

	return p - buffer;

#undef _REQ_LENGTH
}

static int imcp_snip_heartbeat2_get_buffer (AL_CONTEXT_PARAM_DECL imcp_packet_info_t* pi, unsigned char* buffer, unsigned int buf_length)
{
	unsigned char*	p;
	int				i;
	unsigned int	req_length;
	unsigned int	cpu2le32;

	AL_ASSERT("MESH_AP	: imcp_snip_heartbeat2_get_buffer", buffer != NULL);
	AL_ASSERT("MESH_AP	: imcp_snip_heartbeat2_get_buffer", pi != NULL);

	AL_PRINT_LOG_FLOW_0( "MESH_AP	: imcp_snip_heartbeat2_get_buffer()");

	req_length = (26 + (pi->u.heart_beat2.child_count * 11));

	/**
	 * Buffer length must be atleast req_length 
	 */

	if(buf_length < req_length)
		return IMCP_SNIP_ERROR_INADEQUATE_BUFFER_LENGTH;

	p	= buffer;

	/**
	 *	Copy Sender DS MAC, SQNR, uplink tx_rate,sta count
	 *	list of sta mac address ,tx rate and signal from sta
	 */

	memcpy(p,	pi->u.heart_beat2.ds_mac.bytes,			MAC_ADDR_LENGTH);				p += MAC_ADDR_LENGTH;
	cpu2le32	= al_impl_cpu_to_le32(pi->u.heart_beat2.sqnr);
	memcpy(p,	&cpu2le32,													4);			p += 4;
	cpu2le32	= al_impl_cpu_to_le32(pi->u.heart_beat2.up_tx_rate);
	memcpy(p,	&cpu2le32,													4);			p += 4;

	memcpy(p,	&pi->u.heart_beat2.child_count,								1);			p += 1;

	for(i = 0; i < pi->u.heart_beat2.child_count; i++) {
		memcpy(p,	pi->u.heart_beat2.child_info[i].mac_addr.bytes,	MAC_ADDR_LENGTH);	p += MAC_ADDR_LENGTH;
		cpu2le32	= al_impl_cpu_to_le32(pi->u.heart_beat2.child_info[i].down_tx_rate);
		memcpy(p,	&cpu2le32,												4);			p += 4;
		memcpy(p,	&pi->u.heart_beat2.child_info[i].signal_strength,		1);			p += 1;
	}

	cpu2le32	= al_impl_cpu_to_le32(pi->u.heart_beat2.flags);
	memcpy(p,	&cpu2le32,													4);			p += 4;
	cpu2le32	= al_impl_cpu_to_le32(pi->u.heart_beat2.last_update_number);
	memcpy(p,	&cpu2le32,													4);			p += 4;
	memcpy(p,	&pi->u.heart_beat2.imcp_packet_capabilities,				1);			p += 1;
	memcpy(p,	&pi->u.heart_beat2.imcp_config_capabilities,				1);			p += 1;
	memcpy(p,	&pi->u.heart_beat2.imcp_command_capabilities,				1);			p += 1;

	return req_length;

}

