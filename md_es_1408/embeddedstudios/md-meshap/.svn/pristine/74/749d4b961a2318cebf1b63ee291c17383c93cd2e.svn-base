
/********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : access_point.h
 * Comments : AccessPoint main header
 * Created  : 4/5/2004
 * Author   : Sriram Dayanandan
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * -----------------------------------------------------------------------------
 * | No  |Date      |  Comment                                        | Author |
 * -----------------------------------------------------------------------------
 * | 60  |1/27/2009 | Changes for Uplink Scan Functionality           | Sriram |
 * -----------------------------------------------------------------------------
 * | 59  |1/27/2009 | Changed default Minimum Bridge Timeout          | Sriram |
 * -----------------------------------------------------------------------------
 * | 58  |8/27/2008 | Changes for options                             | Sriram |
 * -----------------------------------------------------------------------------
 * | 57  |8/22/2008 | Changes for SIP                                 |Abhijit |
 * -----------------------------------------------------------------------------
 * | 56  |7/17/2007 | authentication_time Added to STA INFO           | Sriram |
 * -----------------------------------------------------------------------------
 * | 55  |6/20/2007 |  Changes for IGMP Snooping                      |Prachiti|
 * -----------------------------------------------------------------------------
 * | 54  |6/6/2007  | Changes for EFFISTREAM BITRATE                  |Prachiti|
 * -----------------------------------------------------------------------------
 * | 52  |6/5/2007  | stay_awake_count changed unsigned int           | Sriram |
 * -----------------------------------------------------------------------------
 * | 51  |5/17/2007 | access_point_set_sta_vlan added                 | Sriram |
 * -----------------------------------------------------------------------------
 * | 50  |4/19/2007 | Changes for forced disassociation               | Sriram |
 * -----------------------------------------------------------------------------
 * | 49  |2/26/2007 | Changes for EFFISTREAM                          | Sriram |
 * -----------------------------------------------------------------------------
 * | 48  |2/23/2007 | Changes for EFFISTREAM                          |Prachiti|
 * -----------------------------------------------------------------------------
 * | 47  |2/8/2007  | Fixes for In-direct VLAN                        | Sriram |
 * -----------------------------------------------------------------------------
 * | 46  |1/30/2007 | Changes for Ethernet VLAN restriction           | Sriram |
 * -----------------------------------------------------------------------------
 * | 45  |1/10/2007 | Changes for VLAN BCAST optimization             | Sriram |
 * -----------------------------------------------------------------------------
 * | 44  |12/6/2006 | Changes for In-direct VLAN                      | Sriram |
 * -----------------------------------------------------------------------------
 * | 43  |11/8/2006 | Changes for compression support                 | Sriram |
 * -----------------------------------------------------------------------------
 * | 42  |3/13/2006 | ACL list implemented                            | Sriram |
 * -----------------------------------------------------------------------------
 * | 41  |02/15/2006| Changes for dot11e                              | Bindu  |
 * -----------------------------------------------------------------------------
 * | 40  |02/08/2006| Changes for Hide SSID                           | Sriram |
 * -----------------------------------------------------------------------------
 * | 39  |02/01/2006| access_point_get_sta_essid added                | Abhijit|
 * -----------------------------------------------------------------------------    
 * | 38  |1/25/2006 | Removed dot1p_queue variable                    | Bindu  |
 * -----------------------------------------------------------------------------
 * | 37  |01/17/2006| Changes for aid                                 | Sriram |
 * -----------------------------------------------------------------------------
 * | 36  |01/04/2006| Added dot1p_queue implementation                | Bindu  |
 * -----------------------------------------------------------------------------
 * | 35  |01/01/2006| Analyser changes                                | Abhijit|
 * -----------------------------------------------------------------------------
 * | 34  |12/14/2005| acktimeout and regdomain changes                |prachiti|
 * -----------------------------------------------------------------------------
 * | 33  |9/19/2005 | DS Excerciser logic and other changes           | Sriram |
 * -----------------------------------------------------------------------------
 * | 32  |9/12/2005 | access_point_enable_wm_encryption Added         | Sriram |
 * -----------------------------------------------------------------------------
 * | 31  |6/15/2005 | Interrupt disable protection added              | Sriram |
 * -----------------------------------------------------------------------------
 * | 30  |6/9/2005  | Fixed TX Rate and Power for VLANs               | Sriram |
 * -----------------------------------------------------------------------------
 * | 29  |5/20/2005 | Fixed TX Rate changes                           | Sriram |
 * -----------------------------------------------------------------------------
 * | 28  |4/15/2005 | ds_group_cipher_type added to Globals           | Sriram |
 * -----------------------------------------------------------------------------
 * | 27  |4/8/2005  | Changes after merging                           | Sriram |
 * -----------------------------------------------------------------------------
 * | 26  |4/6/2005  | Changes for 802.1x/802.11i                      | Sriram |
 * -----------------------------------------------------------------------------
 * | 25  |3/24/2005 | access_point_get_sta_info Added                 | Sriram |
 * -----------------------------------------------------------------------------
 * | 24  |1/24/2005 | Added 802.1q members to sta_info                | Sriram |
 * -----------------------------------------------------------------------------
 * | 23  |1/24/2005 | 802.11i/WPA Changes                             | Sriram |
 * -----------------------------------------------------------------------------
 * | 22  |1/8/2005  | Changes for per-interface essid/etc             | Sriram |
 * -----------------------------------------------------------------------------
 * | 21  |1/6/2005  | Misc changes                                    | Sriram |
 * -----------------------------------------------------------------------------
 * | 20  |12/22/2004| Changes for 802.11b client in 802.11g mode      | Sriram |
 * -----------------------------------------------------------------------------
 * | 19  |12/20/2004| Merged changes by Anand for phy_sub_type        | Sriram |
 * -----------------------------------------------------------------------------
 * | 18  |12/14/2004| Added Min values                                | Sriram |
 * -----------------------------------------------------------------------------
 * | 17  |12/8/2004 | Modified access_point_set_values                | Sriram |
 * -----------------------------------------------------------------------------
 * | 16  |15/11/2004| capability parameter added to al_process_assoc  |Prachiti|
 * -----------------------------------------------------------------------------
 * | 15  |11/10/2004| b client changes								  |prachiti|
 * -----------------------------------------------------------------------------
 * | 14  |11/10/2004| access_point_get_stats					      |prachiti|
 * -----------------------------------------------------------------------------
 * | 13  |7/29/2004 | added fn set_values() for MESH to set - essid,  |        |
 * |	 |			| beacon_interval, frag_threshold & rts_threshold | Bindu  |
 * -----------------------------------------------------------------------------
 * | 12  |7/24/2004 | remove_all_sta, enable/disable processing added | Anand  |
 * -----------------------------------------------------------------------------
 * | 11  |7/23/2004 | SEND_UP_IMCP decision type added                | Anand  |
 * -----------------------------------------------------------------------------
 * | 10  |7/20/2004 | Removed the if_transmit_list                    | Sriram |
 * -----------------------------------------------------------------------------
 * |  9  |7/19/2004 | Rssi parameter added to on_data_reception       | Anand  |
 * -----------------------------------------------------------------------------
 * |  8  |6/9/2004  | Declared access_point_disassociate_sta          | Sriram |
 * -----------------------------------------------------------------------------
 * |  7  |5/10/2004 | New AP State STARTING added.                    | Anand  |
 * -----------------------------------------------------------------------------
 * |  6  |4/23/2004 | Changed channel to wm_net_if_channels in config | Sriram |
 * -----------------------------------------------------------------------------
 * |  5  |4/21/2004 | **Made (major) changes for AL_CONTEXT**         | Bindu P|
 * -----------------------------------------------------------------------------
 * |  4  |4/19/2004 | Revised access point state #define's            | Bindu P|
 * -----------------------------------------------------------------------------
 * |  3  |4/15/2004 | Added imcp handler                              | Sriram |
 * -----------------------------------------------------------------------------
 * |  2  |4/13/2004 | Included string.h                               | Bindu P|
 * -----------------------------------------------------------------------------
 * |  1  |4/7/2004 | Added parameter to initialize function           | Sriram |
 * -----------------------------------------------------------------------------
 * |  0  |4/5/2004  | Created.                                        | Sriram |
 * -----------------------------------------------------------------------------
 ********************************************************************************/

#include "al.h"
#include "access_point_queue.h"
#include "al_conf.h"
#include "al_802_11.h"
#include "sip_conf.h"

#ifndef __ACCESSPOINT_H__
#define __ACCESSPOINT_H__

#ifndef ACCESS_POINT_MIN_STAY_AWAKE_COUNT
	#define ACCESS_POINT_MIN_STAY_AWAKE_COUNT					500
#endif

#ifndef ACCESS_POINT_MIN_BRIDGE_AGEING_TIME
	#define ACCESS_POINT_MIN_BRIDGE_AGEING_TIME					15
#endif

#ifndef ACCESS_POINT_INTERRUPT_DISABLE_PROTECTION_COUNT
	#define ACCESS_POINT_INTERRUPT_DISABLE_PROTECTION_COUNT		65536
#endif

#ifndef ACCESS_POINT_DS_EXCERCISER_INTERVAL
	#define ACCESS_POINT_DS_EXCERCISER_INTERVAL					100
#endif

#define ACCESS_POINT_STATE_UNITIALIZED							0
#define ACCESS_POINT_STATE_INITIALIZED							1
#define ACCESS_POINT_STATE_STOPPED								1	/** Same as ACCESS_POINT_STATE_INITIALIZED */
#define ACCESS_POINT_STATE_STARTED								2
#define ACCESS_POINT_STATE_STARTING								3
#define ACCESS_POINT_STATE_STOPPING								4


#define ACCESS_POINT_STA_STATE_NOT_AUTHENTICATED				0
#define ACCESS_POINT_STA_STATE_AUTHENTICATED					1	/** 802.11 authentication complete */
#define ACCESS_POINT_STA_STATE_ASSOCIATED						2	/** 802.11 association complete */

#define ACCESS_POINT_STA_AUTH_STATE_NONE						0 
#define ACCESS_POINT_STA_AUTH_STATE_802_1X_INITIATED			1	/** 802.1x authentication initiated */
#define ACCESS_POINT_STA_AUTH_STATE_802_1X_AUTHENTICATED		2	/** 802.1x authentication complete */
#define ACCESS_POINT_STA_AUTH_STATE_ENCRYPTION					4	/** Encryption enabled for the STA */
#define ACCESS_POINT_STA_AUTH_STATE_RSN							8	/** STA is a RSN STA */
#define ACCESS_POINT_STA_AUTH_STATE_ACL							16	/** STA is a ACL STA */
#define ACCESS_POINT_STA_AUTH_STATE_BUFFERING					32	/** Packet buffering enabled  */
#define ACCESS_POINT_STA_AUTH_STATE_COMPRESSION					64	/** Compression enabled  */
#define ACCESS_POINT_STA_AUTH_STATE_INDIRECT_VLAN				128	/** VLAN membership was decided by RADIUS  */
#define ACCESS_POINT_STA_AUTH_STATE_EFFISTREAM					256	/** QOS    */

#define ACCESS_POINT_MODE_AP									0
#define ACCESS_POINT_MODE_TESTER								1
#define ACCESS_POINT_MODE_TESTEE								2

struct access_point_vlan_info;

struct access_point_sta_info {

	/** Information from association request */

	unsigned short					aid;
	al_net_if_t*					net_if;
	al_net_addr_t					address;
	unsigned char					state;				/** ACCESS_POINT_STA_STATE_ */
	unsigned int					auth_state;			/**ACCESS_POINT_STA_AUTH_STATE_ */
	unsigned char					b_client;			/** set to 1 if its a 802.11b client when in 802.11g mode */
	unsigned char					is_imcp;			/** set to 1 for clients that are IMCP relay nodes */
	unsigned short					listen_interval;
	unsigned char					supported_rates[AL_802_11_SUPPORTED_RATES_LENGTH];
	unsigned short					capability;
	int								key_index;			/** Only valid if ACCESS_POINT_STA_AUTH_STATE_ENCRYPTION is set */
	unsigned char					dot11e_enabled;		/** Only valid if STA is ACL and on default_vlan */
	unsigned char					dot11e_category;	/** Only valid if STA is ACL and on default_vlan */

	/** Information from operations */

	al_u64_t						authentication_time;
	al_u64_t						association_time;
	al_u64_t						last_packet_received;
	al_u64_t						last_packet_transmitted;
	al_u64_t						buffering_start_time;
	al_u64_t						buffering_end_time;
	unsigned int					bytes_received;
	unsigned int					bytes_transmitted;

	/** Information from sampling thread */
	int								transmit_bit_rate;
	int								rssi;
	
	al_atomic_t						ref_count;

	/** 802.1q VLAN tag */
	struct access_point_vlan_info*	vlan_info;

	al_802_11_information_element_t	rsn_ie;	/** Only valid if ACCESS_POINT_STA_AUTH_STATE_ENCRYPTION is set */
};

typedef struct access_point_sta_info access_point_sta_info_t;

struct access_point_security_info {
	al_802_11_security_info_t	dot11;
	al_ip_addr_t				dot1x_backend_addr;
	unsigned short				dot1x_backend_port;
	char						dot1x_backend_secret[64];
	int							group_key_renewal_period;
	unsigned char				radius_based_vlan;
};

typedef struct access_point_security_info access_point_security_info_t;

#define APSSI_REASON_CODE_OK						1
#define APSSI_REASON_CODE_POLICY_MISMATCH			2
#define APSSI_REASON_CODE_AUTH_FAILURE				3

struct access_point_sta_security_info {
	al_802_11_security_key_info_t	dot11;
	short							vlan_tag;		/** As returned by RADIUS server. -1 for invalid */
	unsigned char					authenticated;
	unsigned char					reason_code;
};

typedef struct access_point_sta_security_info access_point_sta_security_info_t;

#define ACCESS_POINT_ALLOW_NO_VLAN_ESSID		"MD-PRIV-SSID-NO-VLAN"
#define ACCESS_POINT_MBPS_TO_RATE_INDEX_COUNT	100

struct access_point_mcast_sta_list {
	al_net_addr_t						sta_addr;
	struct access_point_mcast_sta_list*	next;
	struct access_point_mcast_sta_list*	prev;
};
typedef struct access_point_mcast_sta_list access_point_mcast_sta_list_t;

struct access_point_mcast {
	al_net_addr_t					mcast_addr;
	int								indirect_sta_count;
	int								sta_count;
	access_point_mcast_sta_list_t*	indirect_stas;
	access_point_mcast_sta_list_t*	stas;
	struct access_point_mcast*		next_hash;
	struct access_point_mcast*		prev_hash;
	struct access_point_mcast*		next_list;
	struct access_point_mcast*		prev_list;
};
typedef struct access_point_mcast access_point_mcast_t;


struct access_point_netif_config_info {
	al_net_if_t*					net_if;
	int								channel;
	unsigned char					service;
	unsigned char					bonding;
	char							essid[AL_802_11_MAX_ESSID_LENGTH + 1];
	unsigned short					beacon_interval;
	unsigned short					rts_threshold;
	unsigned short					frag_threshold;
	unsigned char					txpower;
	int								txrate;
	unsigned char					ack_timeout;
	unsigned char					phy_type;
	unsigned char					phy_sub_type;			/*PHY_SUB_TYPE_CHANGE*/
	unsigned char					preamble_type;
	unsigned char					slot_time_type;
	int								long_slot_time_count;
	int								long_preamble_count;
	unsigned char					hide_ssid;
	access_point_security_info_t	security_info;
	al_802_11_security_key_info_t	group_key;				/* Only valid when net_if is non-null */
	unsigned char					aid_bitmap[(AL_802_11_AID_MAX_VALUE + 1)/8];
	unsigned char					dot11e_enabled;	
	unsigned char					dot11e_category;

	/**
	 * imcp_sta_count Counts the number of Mesh nodes
	 * connected to this net_if
	 * sta_count Counts the number of non-vlan 802.11 clients
	 * connected to this net_if
	 * When in Optimized mode, the vlan_child_count array stores
	 * for each VLAN the number of 802.11 clients connected to this net_if
	 * When not in Optimized mode vlan_child_count[0] stores the number of
	 * VLAN based 802.11 clients connected to this net_if
	 */
#ifdef ACCESS_POINT_VLAN_BCAST_OPTIMIZE
	unsigned char					vlan_child_count[4096];
#else
	unsigned char					vlan_child_count[1];
#endif
	al_atomic_t						sta_count;
	al_atomic_t						imcp_sta_count;
	unsigned int					tagged_bcast_count;
	unsigned int					untagged_bcast_count;
	unsigned int					untagged_bcast_reordered_count;
	
	/* Rate table required for effistream */
	unsigned char					mbps_to_rate_index[ACCESS_POINT_MBPS_TO_RATE_INDEX_COUNT];

	/* Multicast parameters */
	access_point_mcast_t**			multicast_hash;
	access_point_mcast_t*			multicast_list_head;
	access_point_mcast_t*			multicast_list_tail;
	int								multicast_hash_length;
};

typedef struct access_point_netif_config_info access_point_netif_config_info_t;

struct access_point_dhcp_info {
	unsigned char	net_id;
	unsigned char	host_id_1;
	unsigned char	host_id_2;
	unsigned char	subnet_mask[4];
	unsigned char	gateway_ip[4];
	unsigned char	dns_ip[4];
	unsigned int	lease_timeout;
};
typedef struct access_point_dhcp_info access_point_dhcp_info_t;

struct access_point_config {
	char								essid[AL_802_11_MAX_ESSID_LENGTH + 1];
	unsigned short						beacon_interval;
	unsigned short						rts_threshold;
	unsigned short						frag_threshold;
	access_point_netif_config_info_t	ds_net_if_info;
	access_point_netif_config_info_t*	wm_net_if_info;
	int									wm_net_if_count;
	unsigned int						stay_awake_count;
	int									bridge_ageing_time;
	unsigned char						mesh_imcp_key_length;
	unsigned char						mesh_imcp_key[256];
	unsigned short						reg_domain;
	unsigned short						country_code;
	unsigned char						igmp_support;
	unsigned char						dhcp_support;
	access_point_dhcp_info_t			dhcp_info;

	/* New Generation configuration parameters Begin*/
	unsigned short						round_robin_beacon_count;
	/* New Generation configuration parameters End*/
	al_conf_effistream_criteria_t*		criteria_root;
	sip_conf_info_t						sip_info;
	al_conf_option_t*					options;
};

typedef struct access_point_config access_point_config_t;

/** 802.11e Categories
 */
struct access_point_dot11e_category {
	al_dot11e_category_info_t			config_info;
};

typedef struct access_point_dot11e_category access_point_dot11e_category_t;

struct access_point_globals {

	int								queue_event_handle;
	int								stop_event_handle;
	int								access_point_thread_id;
	access_point_config_t			config;
	access_point_dot11e_category_t	dot11e_config;
	unsigned short					assoc_cnt;
	int								ap_state;
	int								monitor_exit_handle;
	unsigned char					ds_group_cipher_type;
	void*							dhcp_instance;				/* If DHCP support is enabled */
	void*							arp_instance;
	void*							sip_instance;
	al_atomic_t						ap_thread_disabled;

	/**
	 * Number of packets transmitted to the DS during the last
	 * interval. This is used by the DS excerciser thread
	 */
	volatile int					ds_packet_tx_count;
	/* packet statistics */
	al_net_if_stat_t				access_point_traffic_stats;
	access_point_queue_t*			ap_queue;

    	/* data for analyser mode */
	int								ap_mode;

};

typedef struct access_point_globals access_point_globals_t;

struct access_point_vlan_info {
	access_point_netif_config_info_t	config;
	char								name[AL_802_11_MAX_ESSID_LENGTH + 1];
	al_ip_addr_t						ip_addr;
	unsigned short						vlan_tag;
	unsigned char						dot11e_enabled;
	unsigned char						dot1p_priority;
	unsigned char						dot11e_category;
	unsigned char						txpower;
	int									txrate;
};

typedef struct access_point_vlan_info access_point_vlan_info_t;

struct access_point_acl_entry {
	al_net_addr_t	sta_mac;
	unsigned short	vlan_tag;
	unsigned char	allow;
	unsigned char	dot11e_enabled;
	unsigned char	dot11e_category;
};

typedef struct access_point_acl_entry access_point_acl_entry_t;

/**
 * Hook handler prototypes
 */

typedef int (*access_point_on_start_t)	(AL_CONTEXT_PARAM_DECL access_point_config_t* config, access_point_dot11e_category_t* dot11e_config);
typedef int (*access_point_on_stop_t)	(AL_CONTEXT_PARAM_DECL access_point_config_t* config, access_point_dot11e_category_t* dot11e_config);

typedef int (*access_point_on_auth_request_t)(AL_CONTEXT_PARAM_DECL al_net_if_t*	net_if, 
											  al_net_addr_t*	sta_addr,
											  unsigned short	algorithm_number,
											  unsigned short	atsn,
											  unsigned short	status_code,
											  unsigned char*	algorithm_specific_info,
											  int				algorithm_specific_info_length,
											  unsigned short*	status_code_out,
											  unsigned char**	algorithm_specific_info_out,
											  int*				algorithm_specific_info_length_out);

typedef int (*access_point_on_assoc_request_t)(AL_CONTEXT_PARAM_DECL al_net_if_t*		net_if, 
											   al_net_addr_t*		sta_addr,
											   unsigned char*		sta_supported_rates,
											   unsigned char		sta_supported_rates_length,
											   unsigned short*		status_code_out,
											   unsigned char*		supported_rates_out,
											   unsigned char*		supported_rates_length_out);

typedef void (*access_point_on_deauth_t)	(AL_CONTEXT_PARAM_DECL al_net_if_t*		net_if, 
										     al_net_addr_t*	sta_addr,
											 unsigned short		reason_code);

typedef void (*access_point_on_disassoc_t)	(AL_CONTEXT_PARAM_DECL al_net_if_t*		net_if, 
										     al_net_addr_t*	sta_addr,
											 unsigned short		reason_code);

typedef int (*access_point_on_probe_request_t) (AL_CONTEXT_PARAM_DECL al_net_if_t*		net_if, 
											   al_net_addr_t*		sta_addr,
											   unsigned char*		sta_supported_rates,
											   unsigned char		sta_supported_rates_length,
											   unsigned char*		supported_rates_out,
											   unsigned char*		supported_rates_length_out);


typedef int (*access_point_on_dot1x_auth_init_t)	(AL_CONTEXT_PARAM_DECL access_point_netif_config_info_t* net_if_config_info);

/**
 * Return values for access_point_on_dot1x_auth_start_t
 */

#define ACCESS_POINT_ON_DOT1X_AUTH_STATUS_ACCEPTED		0
#define ACCESS_POINT_ON_DOT1X_AUTH_STATUS_FORCE_UNAUTH	1
#define ACCESS_POINT_ON_DOT1X_AUTH_STATUS_FORCE_AUTH	2

typedef int (*access_point_on_dot1x_auth_start_t)	(AL_CONTEXT_PARAM_DECL al_net_if_t*		net_if,
													 access_point_netif_config_info_t*		net_if_config_info,
													 al_net_addr_t*							sta_addr,
													 al_802_11_information_element_t*		rsn_ie,
													 int*									enable_encryption_out,
													 int*									key_index_out);



typedef int (*access_point_on_eapol_rx_t)			(AL_CONTEXT_PARAM_DECL al_net_if_t*		net_if,
													 access_point_netif_config_info_t*		net_if_config_info,
													 al_packet_t*							al_packet);

typedef int (*access_point_on_dot1x_auth_delete_t)	(AL_CONTEXT_PARAM_DECL al_net_if_t*		net_if,
													 access_point_netif_config_info_t*		net_if_config_info,
													 al_net_addr_t*							sta_addr);


#define ACCESS_POINT_PACKET_DECISION_DROP			0
#define ACCESS_POINT_PACKET_DECISION_SEND_WM		1
#define ACCESS_POINT_PACKET_DECISION_SEND_DS		2
#define ACCESS_POINT_PACKET_DECISION_SEND_UP		3
#define ACCESS_POINT_PACKET_DECISION_SEND_UP_WM		4
#define ACCESS_POINT_PACKET_DECISION_SEND_UP_DS		5
#define ACCESS_POINT_PACKET_DECISION_SEND_UP_WM_DS	6
#define ACCESS_POINT_PACKET_DECISION_SEND_WM_DS		7
#define ACCESS_POINT_PACKET_DECISION_SEND_UP_IMCP	8


typedef int (*access_point_on_data_reception_t)(AL_CONTEXT_PARAM_DECL al_net_if_t*		net_if,
												unsigned int	rssi,
												al_net_addr_t*	immediate_src_addr_in,
												al_net_addr_t*	src_addr_in,
												al_net_addr_t*	dest_addr_in,
												unsigned short*		frame_control_out,
												al_net_addr_t*	immediate_dest_addr_out,
												al_net_if_t**		net_if_out);

typedef int (*access_point_on_imcp_packet_t)(AL_CONTEXT_PARAM_DECL al_net_if_t*		net_if, 
										     al_packet_t*		packet);


typedef int (*access_point_on_before_transmit_t)(AL_CONTEXT_PARAM_DECL al_net_addr_t*		dest_addr_in,
											     unsigned short*		frame_control_out,
												 al_net_addr_t*		immediate_dest_addr_out,
												 al_net_if_t**		net_if_out);


int									access_point_initialize						(AL_CONTEXT_PARAM_DECL_SINGLE);
int									access_point_start							(AL_CONTEXT_PARAM_DECL int sta_table_hash_length);
int									access_point_stop							(AL_CONTEXT_PARAM_DECL_SINGLE);
int									access_point_uninitialize					(AL_CONTEXT_PARAM_DECL_SINGLE);
void								access_point_get_sta_info					(AL_CONTEXT_PARAM_DECL al_net_addr_t* address,access_point_sta_info_t* sta_info);
void								access_point_get_sta_info_ex				(AL_CONTEXT_PARAM_DECL al_net_addr_t* address,access_point_sta_info_t* sta_info, int from_interrupt_context);

/**
 * Called by MESH only
 */

int									access_point_disassociate_sta				(AL_CONTEXT_PARAM_DECL al_net_addr_t* address);

/**
 * Called by MESH only
 */

int									access_point_remove_all_sta					(AL_CONTEXT_PARAM_DECL_SINGLE);

/**
 * Called by MESH only
 */

int									access_point_enable_disable_processing		(AL_CONTEXT_PARAM_DECL unsigned char enable);
void								access_point_enable_wm_encryption			(AL_CONTEXT_PARAM_DECL unsigned char enable);

int									access_point_set_start_handler				(AL_CONTEXT_PARAM_DECL access_point_on_start_t handler);
int									access_point_set_stop_handler				(AL_CONTEXT_PARAM_DECL access_point_on_stop_t handler);
int									access_point_set_auth_request_handler		(AL_CONTEXT_PARAM_DECL access_point_on_auth_request_t handler);
int									access_point_set_assoc_request_handler		(AL_CONTEXT_PARAM_DECL access_point_on_assoc_request_t handler);
int									access_point_set_deauth_handler				(AL_CONTEXT_PARAM_DECL access_point_on_deauth_t handler);
int									access_point_set_disassoc_handler			(AL_CONTEXT_PARAM_DECL access_point_on_disassoc_t handler);
int									access_point_set_probe_request_handler		(AL_CONTEXT_PARAM_DECL access_point_on_probe_request_t handler);
int									access_point_set_data_reception_handler		(AL_CONTEXT_PARAM_DECL access_point_on_data_reception_t handler);
int									access_point_set_before_transmit_handler	(AL_CONTEXT_PARAM_DECL access_point_on_before_transmit_t handler);
int									access_point_set_imcp_handler				(AL_CONTEXT_PARAM_DECL access_point_on_imcp_packet_t handler);

int									access_point_set_values						(AL_CONTEXT_PARAM_DECL char*		essid, 
																				 unsigned short						beacon_interval,
																				 unsigned short						rts_threshold,
																				 unsigned short						frag_threshold,
																				 unsigned char						igmp_support,
																				 int								netif_count,
																				 access_point_netif_config_info_t*	netif_config_info);

al_net_if_stat_t*					access_point_get_stats						(AL_CONTEXT_PARAM_DECL_SINGLE);		
access_point_netif_config_info_t*	access_point_get_netif_config_info			(AL_CONTEXT_PARAM_DECL al_net_if_t* al_net_if);

int									access_point_set_dot1x_handler				(AL_CONTEXT_PARAM_DECL access_point_on_dot1x_auth_init_t on_auth_init,
																				 access_point_on_dot1x_auth_start_t on_auth_start,
																				 access_point_on_dot1x_auth_delete_t on_auth_delete,
																				 access_point_on_eapol_rx_t on_eapol_rx);

int									access_point_set_dot1x_auth_result			(AL_CONTEXT_PARAM_DECL access_point_sta_security_info_t* sta_security_info);
access_point_vlan_info_t*			access_point_add_vlan_info					(AL_CONTEXT_PARAM_DECL access_point_vlan_info_t* vlan_info);
void								access_point_set_ds_group_cipher_type		(AL_CONTEXT_PARAM_DECL unsigned char type);

int									access_point_disassociate_sta_ex			(AL_CONTEXT_PARAM_DECL al_net_addr_t* address, int from_mesh, al_net_if_t* net_if);
int									access_point_update_sta_last_rx				(AL_CONTEXT_PARAM_DECL al_net_addr_t* address, int evaluate_link);

int									access_point_set_tester_testee_mode			(AL_CONTEXT_PARAM_DECL int mode);
int									access_point_get_sta_essid					(AL_CONTEXT_PARAM_DECL al_net_addr_t* address,char* sta_essid,unsigned char* essid_length); 
int									access_point_set_dot11e_category_info		(AL_CONTEXT_PARAM_DECL int count, al_dot11e_category_info_t* dot11e_catgory_info);
int									access_point_get_dot11e_category_info		(AL_CONTEXT_PARAM_DECL al_dot11e_category_info_t* dot11e_category_info);

int									access_point_set_acl_list					(AL_CONTEXT_PARAM_DECL int count,access_point_acl_entry_t* entries);

void								access_point_set_buffering					(AL_CONTEXT_PARAM_DECL al_net_addr_t* address,int start, unsigned int timeout);
int									access_point_set_sta_vlan					(AL_CONTEXT_PARAM_DECL al_net_addr_t* address, unsigned short vlan_tag);

int									access_point_remove_sta_from_multicast_table(AL_CONTEXT_PARAM_DECL al_net_if_t* al_net_if,al_net_addr_t*  src_sta_addr);

int									access_point_verify_option					(AL_CONTEXT_PARAM_DECL unsigned short option_code);

int									access_point_arp_add_entry					(AL_CONTEXT_PARAM_DECL al_net_addr_t* server_ip, int use_incoming_net_if_addr);
int									access_point_arp_remove_entry				(AL_CONTEXT_PARAM_DECL al_net_addr_t* server_ip);

int									access_point_enable_thread					(AL_CONTEXT_PARAM_DECL int enable);

#endif /*__ACCESSPOINT_H__*/
