
/********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : access_point_ext.h
 * Comments : External interface to AP
 * Created  : 6/15/2005
 * Author   : Sriram Dayanandan
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * -----------------------------------------------------------------------------
 * | No  |Date      |  Comment                                        | Author |
 * -----------------------------------------------------------------------------
 * |  8  |6/20/2007 | access_point_ext_print_mcast_stas added         |Prachiti|
 * -----------------------------------------------------------------------------
 * |  7  |5/17/2007 | access_point_ext_set_sta_vlan added             | Sriram |
 * -----------------------------------------------------------------------------
 * |  6  |1/13/2007 | access_point_ext_print_vlan_info added          | Sriram |
 * -----------------------------------------------------------------------------
 * |  5  |1/13/2007 | access_point_ext_print_wm_info added            | Sriram |
 * -----------------------------------------------------------------------------
 * |  4  |5/31/2006 | access_point_ext_print_hqueue_stat added        | Sriram |
 * -----------------------------------------------------------------------------
 * |  3  |01/04/2006| print queue stats added                         | Bindu  |
 * -----------------------------------------------------------------------------
 * |  2  |6/18/2005 | Enumerate STAs added                            | Sriram |
 * -----------------------------------------------------------------------------
 * |  1  |6/17/2005 | Added test flags setting                        | Sriram |
 * -----------------------------------------------------------------------------
 * |  0  |6/15/2005 | Created.                                        | Sriram |
 * -----------------------------------------------------------------------------
 ********************************************************************************/

#ifndef __ACCESS_POINT_EXT_H__
#define __ACCESS_POINT_EXT_H__

int		access_point_ext_disassociate_sta	(unsigned char* address);
int		access_point_ext_set_test_flags		(unsigned int flags);
int		access_point_ext_enumerate_stas		(void* callback,void (*enum_routine)(void* callback,const access_point_sta_info_t* sta));
int		access_point_ext_print_queue_stat	(char* print_p);
int		access_point_ext_print_hqueue_stat	(char* print_p);
int		access_point_ext_print_wm_info		(char* print_p);
int		access_point_ext_print_vlan_info	(char* print_p);
int		access_point_ext_set_sta_vlan		(unsigned char* address, unsigned short tag);
int		access_point_ext_print_mcast_stas 	(char* print_p);
void*	access_point_ext_get_dhcp_instance	(void);

/**
 *	Following options correspond to option codes in al_conf.h
 *	starting from AL_CONF_OPTION_CODE_SIP
 *	values of these option codes have to be in sync with values 
 *  in al_conf.h
 */
#define ACCESS_POINT_OPTION_CODE_START		8

#define ACCESS_POINT_OPTION_CODE_PBV		8

#define ACCESS_POINT_OPTION_CODE_END		8

int		access_point_ext_verify_option		(unsigned short option_code);

#endif /*__ACCESS_POINT_EXT_H__*/
