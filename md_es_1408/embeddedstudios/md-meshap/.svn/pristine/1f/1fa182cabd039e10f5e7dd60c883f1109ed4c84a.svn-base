
/********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : mesh_ng_fsm_uplink_buffer.c
 * Comments : Mesh new generation uplink buffer state
 * Created  : 2/11/2009
 * Author   : Abhijit Ayarekar
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * -----------------------------------------------------------------------------
 * | No  |Date      |  Comment                                        | Author |
 * -----------------------------------------------------------------------------
 * |  0  |2/11/2009 | Created.                                        |Abhijit |
 * -----------------------------------------------------------------------------
 ********************************************************************************/

/**
 * This is a private source file included by mesh_ng_fsm.c, hence
 * It was created to improve readability.
 */

static void _uplink_buffer_execute(AL_CONTEXT_PARAM_DECL void* event_data)
{
	al_802_11_operations_t*	ex_op;
	int						timeout;
	unsigned long			flags;

	timeout	= ((int)(event_data));

	ex_op	= (al_802_11_operations_t*)_DS_NET_IF->get_extended_operations(_DS_NET_IF);

	al_disable_interrupts(flags);

	al_reset_event(AL_CONTEXT mesh_ds_buffer_stop_event);

	al_enable_interrupts(flags);

	if(al_wait_on_event(AL_CONTEXT mesh_ds_buffer_stop_event, timeout) != AL_WAIT_STATE_SIGNAL)
		al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"MESH_NG: Resuming uplink transmissions after %d", timeout);

	AL_ATOMIC_SET(_DS_NET_IF->buffering_state,AL_NET_IF_BUFFERING_STATE_NONE);
	access_point_enable_thread(AL_CONTEXT 1);
	ex_op->enable_ds_verification_opertions(_DS_NET_IF,1);
	
	_mesh_ng_fsm_set_state(AL_CONTEXT MESH_NG_FSM_STATE_RUNNING);
}
