
/********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : meshap_tddi.h
 * Comments : Torna MeshAP TDDI
 * Created  : 5/25/2004
 * Author   : Sriram Dayanandan
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * -----------------------------------------------------------------------------
 * | No  |Date      |  Comment                                        | Author |
 * -----------------------------------------------------------------------------
 * |  4  |2/6/2007  | FIPS related changes                            | Sriram |
 * -----------------------------------------------------------------------------
 * |  3  |4/6/2005  | Added test 12-1                                 | Sriram |
 * -----------------------------------------------------------------------------
 * |  2  |3/24/2005 | Added get_ds_net_if                             | Sriram |
 * -----------------------------------------------------------------------------
 * |  1  |12/15/2004| Added set_ds_net_if routine                     | Sriram |
 * -----------------------------------------------------------------------------
 * |  0  |5/25/2004 | Created.                                        | Sriram |
 * -----------------------------------------------------------------------------
 ********************************************************************************/

#ifndef __MESHAP_TDDI_H__
#define __MESHAP_TDDI_H__

struct meshap_tddi_globals {
	int			major;
	int			locked;
#ifdef _MESHAP_AL_TEST_SUITE_
	unsigned int	test_1_token;
	unsigned int	test_2_token;
	unsigned int	test_3_token;
	unsigned int	test_2_count;
#endif
};

typedef struct meshap_tddi_globals meshap_tddi_globals_t;

extern meshap_tddi_globals_t meshap_tddi_globals;

int				meshap_tddi_initialize		(void);
int				meshap_tddi_uninitialize	(void);
void			meshap_tddi_set_ds_net_if	(al_net_if_t*	net_if);
al_net_if_t*	meshap_tddi_get_ds_net_if	(void);
unsigned short	meshap_tddi_get_regdomain	(void);

#endif /*__MESHAP_TDDI_H__*/
