
/********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : sip_conf.h
 * Comments : SIP configuration parser.
 * Created  : 1/16/2008
 * Author   : Abhijit Ayarekar
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * -----------------------------------------------------------------------------
 * | No  |Date      |  Comment                                        | Author |
 * -----------------------------------------------------------------------------
 * |  1  |8/27/2008 | Added AD-HOC only option flag                   | Sriram |
 * -----------------------------------------------------------------------------
 * |  0  |1/16/2008 | Created.                                        |Abhijit |
 * -----------------------------------------------------------------------------
 ********************************************************************************/

#ifndef __SIP_CONF_H__
#define __SIP_CONF_H__

#define SIP_CONF_MAX_STR_LEN	32

typedef void* sip_conf_handle_t;

struct sip_conf_sta_info {
	unsigned char	mac[6];
	char			extn[SIP_CONF_MAX_STR_LEN];
};

typedef struct sip_conf_sta_info sip_conf_sta_info_t;

struct sip_conf_info {
	unsigned char			adhoc_only;
	unsigned char			sip_enabled;
	unsigned short			port;
	unsigned char			server_ip[4];
	int						sta_count;
	sip_conf_sta_info_t*	sta_info;
};

typedef struct sip_conf_info sip_conf_info_t;

sip_conf_handle_t		sip_conf_parse							(AL_CONTEXT_PARAM_DECL const char* sip_conf_string);
void					sip_conf_close							(AL_CONTEXT_PARAM_DECL sip_conf_handle_t sip_conf_handle);
int						sip_conf_get_sip_enabled				(AL_CONTEXT_PARAM_DECL sip_conf_handle_t sip_conf_handle);
int						sip_conf_get_adhoc_only					(AL_CONTEXT_PARAM_DECL sip_conf_handle_t sip_conf_handle);
int						sip_conf_get_sip_port					(AL_CONTEXT_PARAM_DECL sip_conf_handle_t sip_conf_handle);
int						sip_conf_get_server_ip					(AL_CONTEXT_PARAM_DECL sip_conf_handle_t sip_conf_handle, unsigned char* out_ip);
int						sip_conf_get_sta_count					(AL_CONTEXT_PARAM_DECL sip_conf_handle_t sip_conf_handle);
int						sip_conf_get_sta_info					(AL_CONTEXT_PARAM_DECL sip_conf_handle_t sip_conf_handle, int index, sip_conf_sta_info_t* info);

#ifndef _MESHAP_SIP_CONF_NO_SET_AND_PUT

int						sip_conf_set_sip_enabled				(AL_CONTEXT_PARAM_DECL sip_conf_handle_t sip_conf_handle, int enabled);
int						sip_conf_set_adhoc_only					(AL_CONTEXT_PARAM_DECL sip_conf_handle_t sip_conf_handle, int enabled);
int						sip_conf_set_sip_port					(AL_CONTEXT_PARAM_DECL sip_conf_handle_t sip_conf_handle, int port);
int						sip_conf_set_server_ip					(AL_CONTEXT_PARAM_DECL sip_conf_handle_t sip_conf_handle, unsigned char* in_ip);
int						sip_conf_set_sta_count					(AL_CONTEXT_PARAM_DECL sip_conf_handle_t sip_conf_handle, int count);
int						sip_conf_set_sta_info					(AL_CONTEXT_PARAM_DECL sip_conf_handle_t sip_conf_handle, int index, sip_conf_sta_info_t* info);

int						sip_conf_put							(AL_CONTEXT_PARAM_DECL sip_conf_handle_t sip_conf_handle);

int						sip_conf_create_config_string_from_data	(AL_CONTEXT_PARAM_DECL sip_conf_handle_t sip_conf_handle, int* config_string_length, char** sip_conf_string);

#endif /* _MESHAP_SIP_CONF_NO_SET_AND_PUT */

#endif

