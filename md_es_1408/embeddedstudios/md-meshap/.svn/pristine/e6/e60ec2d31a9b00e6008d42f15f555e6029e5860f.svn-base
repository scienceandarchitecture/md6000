
/********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : dot1x-impl-inc.h
 * Comments : Meshap 802.1x Implementation
 * Created  : 3/25/2005
 * Author   : Sriram Dayanandan
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * -----------------------------------------------------------------------------
 * | No  |Date      |  Comment                                        | Author |
 * -----------------------------------------------------------------------------
 * |  2  |6/16/2008 | Lock primitives changed                         | Sriram |
 * -----------------------------------------------------------------------------
 * |  1  |3/25/2005 | Created                                         | Sriram |
 * -----------------------------------------------------------------------------
 ********************************************************************************/

#ifndef __DOT1X_IMPL_INC_H__
#define __DOT1X_IMPL_INC_H__

#include <generated/autoconf.h>

#include <linux/module.h>
#include <linux/init.h>
#include <linux/version.h>
#include <linux/netdevice.h>
#include <linux/skbuff.h>
#include <linux/wireless.h>

#define DOT1X_INLINE inline

#ifdef __KERNEL__

	struct _lock_impl {
		spinlock_t	lock;
	};

	typedef struct _lock_impl _lock_impl_t;

	#define dot1x_impl_lock(_l,_flags)		spin_lock_irqsave(&(((_lock_impl_t*)_l)->lock),_flags)
	#define dot1x_impl_unlock(_l,_flags)	spin_unlock_irqrestore(&(((_lock_impl_t*)_l)->lock),_flags)	

#else

	struct _lock_impl {
		int	dummy;
	};

	#define dot1x_impl_lock(_l,_flags)
	#define dot1x_impl_unlock(_l,_flags)

#endif	

#endif /*__DOT1X_IMPL_INC_H__*/
