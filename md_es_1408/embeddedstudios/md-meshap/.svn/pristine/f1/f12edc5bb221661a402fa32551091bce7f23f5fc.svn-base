
/********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : meshap_mesh_proc.c
 * Comments : Meshap MESH proc interface
 * Created  : 6/18/2005
 * Author   : Sriram Dayanandan
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * -----------------------------------------------------------------------------
 * | No  |Date      |  Comment                                        | Author |
 * -----------------------------------------------------------------------------
 * |  7  |6/17/2009 | Added ad-hoc mode entry                         | Sriram |
 * -----------------------------------------------------------------------------
 * |  6  |6/14/2007 | KAP listing changed for mobile mode             | Sriram |
 * -----------------------------------------------------------------------------
 * |  5  |6/13/2007 | Added mobility window                           | Sriram |
 * -----------------------------------------------------------------------------
 * |  4  |5/30/2006 | Macro usage corrected                           | Sriram |
 * -----------------------------------------------------------------------------
 * |  3  |02/01/2006| mesh table formatting changed					  | Abhijit|
 * -----------------------------------------------------------------------------
 * |  2  |10/5/2005 | More info displayed                             | Sriram |
 * -----------------------------------------------------------------------------
 * |  1  |10/4/2005 | Misc changes                                    | Sriram |
 * -----------------------------------------------------------------------------
 * |  0  |6/18/2005 | Created.                                        | Sriram |
 * -----------------------------------------------------------------------------
 ********************************************************************************/

#include <generated/autoconf.h>
#include <linux/version.h>
#include <linux/module.h>
#include <linux/init.h>
#include <linux/if.h>
#include <linux/netdevice.h>
#include <linux/skbuff.h>
#include <linux/cache.h>
#include <linux/pci.h>
#include <linux/wireless.h>
#include <asm/uaccess.h>
#include <net/iw_handler.h>
#include <linux/delay.h>

#include "al.h"
#include "al_802_11.h"
#include "al_impl_context.h"
#include "al_net_addr.h"
#include "mesh_ext.h"
#include "meshap_mesh_proc.h"
#include "meshap_hproc_format.h"

static struct proc_dir_entry*	_meshap_mesh_proc_entry	= NULL;

static void		_meshap_mesh_proc_kap_list_initialize		(void);
static int		_kap_list_routine							(char *page, char **start, off_t off,int count, int *eof, void *data);
static int		_mobwin_list_routine						(char *page, char **start, off_t off,int count, int *eof, void *data);
static void		_meshap_mesh_proc_table_list_initialize		(void);
static int		_table_list_routine							(char *page, char **start, off_t off,int count, int *eof, void *data);
static void		_meshap_mesh_proc_sample_list_initialize	(void);
static int		_sample_list_routine						(char *page, char **start, off_t off,int count, int *eof, void *data);
static int		_adhoc_list_routine							(char *page, char **start, off_t off,int count, int *eof, void *data);
static int		_adhoc_mode_routine							(char *page, char **start, off_t off,int count, int *eof, void *data);

static void		_enum_kap_routine							(void* callback,const mesh_ext_known_access_point_info_t* kap);
static void		_enum_table_routine							(void* callback,const mesh_ext_table_entry_t* entry);
static void		_enum_mobwin_routine						(void* callback,const mesh_ext_mobilty_window_t* item);

static int		_htable_list_routine						(char *page, char **start, off_t off,int count, int *eof, void *data);
static void		_henum_table_routine						(void* callback,const mesh_ext_table_entry_t* entry);

static int		_hkap_list_routine							(char *page, char **start, off_t off,int count, int *eof, void *data);
static void		_henum_kap_routine							(void* callback,const mesh_ext_known_access_point_info_t* kap);


struct _mesh_proc_data {
	char*		p;
	int			flag;
};

typedef struct _mesh_proc_data _mesh_proc_data_t;

int meshap_mesh_proc_initialize(void)
{
	_meshap_mesh_proc_entry	= meshap_proc_mkdir("mesh");
	_meshap_mesh_proc_kap_list_initialize();
	_meshap_mesh_proc_table_list_initialize();
	_meshap_mesh_proc_sample_list_initialize();
	return 0;
}

void meshap_mesh_proc_add_read_entry(const char*  name,meshap_proc_read_routine_t routine,void* data)
{
	create_proc_read_entry(name,0,_meshap_mesh_proc_entry,routine,data);	
}

static void _meshap_mesh_proc_kap_list_initialize(void)
{
	meshap_mesh_proc_add_read_entry("kap",_kap_list_routine,NULL);
	meshap_mesh_proc_add_read_entry("hkap",_hkap_list_routine,NULL);
	meshap_mesh_proc_add_read_entry("mobwin",_mobwin_list_routine,NULL);
	meshap_mesh_proc_add_read_entry("adhoc",_adhoc_list_routine,NULL);
	meshap_mesh_proc_add_read_entry("adhoc_mode",_adhoc_mode_routine,NULL);
}

static void _meshap_mesh_proc_table_list_initialize(void)
{
	meshap_mesh_proc_add_read_entry("table",_table_list_routine,NULL);
	meshap_mesh_proc_add_read_entry("htable",_htable_list_routine,NULL);
}

static void _meshap_mesh_proc_sample_list_initialize(void)
{
	meshap_mesh_proc_add_read_entry("sample_list",_sample_list_routine,NULL);
}

static int _sample_list_routine(char *page, char **start, off_t off,int count, int *eof, void *data)
{
	char				*p;
	_mesh_proc_data_t	proc_data;

	if(off != 0) {
		*eof	= 1;
		return 0;
	}

	*start				= page + off;
	p					= page;
	proc_data.p			= p;

	proc_data.p			+= sprintf(p,
					   "--------------------------------------------------------------------------------\r\n"
					   "PARENT ADDR      |CHN|SIG|RATE|TRATE|NEXTSAMPLE|DISC   |FLAGS    |SAMCNT|MAXSAM|\r\n"
					   "--------------------------------------------------------------------------------\r\n");

	mesh_ext_enumerate_sampling_list((void*)&proc_data,_enum_kap_routine);

	p					= proc_data.p;

	return (p - page);

}

static int _kap_list_routine(char *page, char **start, off_t off,int count, int *eof, void *data)
{
	char				*p;
	_mesh_proc_data_t	proc_data;

	if(off != 0) {
		*eof	= 1;
		return 0;
	}

	*start				= page + off;
	p					= page;
	proc_data.p			= p;
	proc_data.flag		= mesh_ext_is_mobile();

	if(proc_data.flag) {
		proc_data.p			+= sprintf(p,
						               "--------------------------------------------------------\r\n"
						               "PARENT ADDR      |CHN|SIG|RATE|TRATE|LAST SEEN |PATCNT |\r\n"
						               "--------------------------------------------------------\r\n");
	} else {
		proc_data.p			+= sprintf(p,
									   "--------------------------------------------------------------------------------\r\n"
						               "PARENT ADDR      |CHN|SIG|RATE|TRATE|NEXTSAMPLE|DISC   |FLAGS    |SAMCNT|MAXSAM|\r\n"
						               "--------------------------------------------------------------------------------\r\n");
	}

	mesh_ext_enumerate_known_aps((void*)&proc_data,_enum_kap_routine);

	p					= proc_data.p;

	return (p - page);
}

static void _enum_kap_routine(void* callback,const mesh_ext_known_access_point_info_t* kap)
{
	_mesh_proc_data_t*	proc_data;
	al_u64_t			tick;
	int					diff;

	proc_data	= (_mesh_proc_data_t*)callback;

	tick		= al_get_tick_count(AL_CONTEXT);

	if(!proc_data->flag) {
		diff		= (int)(kap->next_sample_time - tick);
	} else {
		diff		= (int)(tick - kap->last_seen_time);
	}

	if(diff < 0)
		diff = 0;

	if(proc_data->flag) {
		proc_data->p	+= sprintf(proc_data->p,
								   AL_NET_ADDR_STR"|%03d|%03d|%04d|%05d|%010d|%07d|\r\n",
								   AL_NET_ADDR_TO_STR(&kap->bssid),
								   kap->channel,
								   kap->signal,
								   kap->direct_bit_rate,
								   kap->tree_bit_rate,
								   diff ,
								   kap->pattern_count);
	} else {
		proc_data->p	+= sprintf(proc_data->p,
								   AL_NET_ADDR_STR"|%03d|%03d|%04d|%05d|%010d|%07d|%c %c %c %c %c|%06d|%06d|\r\n",
								   AL_NET_ADDR_TO_STR(&kap->bssid),
								   kap->channel,
								   kap->signal,
								   kap->direct_bit_rate,
								   kap->tree_bit_rate,
								   diff ,
								   kap->disconnect_count,
								   kap->flags & MESH_EXT_KAP_FLAG_PREFERRED ? 'P' : '-',
								   kap->flags & MESH_EXT_KAP_FLAG_MOBILE ? 'M' : '-',
								   kap->flags & MESH_EXT_KAP_FLAG_CHILD ? 'C' : '-',
								   kap->flags & MESH_EXT_KAP_FLAG_QUESTION ? 'Q' : '-',
								   kap->flags & MESH_EXT_KAP_FLAG_DISABLED ? 'D' : '-',
								   kap->sample_packets,
								   kap->max_sample_packets);
	}

}

static void _enum_mobwin_routine(void* callback,const mesh_ext_mobilty_window_t* item)
{
	_mesh_proc_data_t*	proc_data;

	proc_data	= (_mesh_proc_data_t*)callback;

	proc_data->p	+= sprintf(proc_data->p,
		                       AL_NET_ADDR_STR"|%03d|%03d|%08X|%03d|\r\n",
							   AL_NET_ADDR_TO_STR(&item->parent_bssid),
							   item->channel,
							   item->signal,
							   item->window_item_id,
							   item->pattern_count);
}

static int _mobwin_list_routine(char *page, char **start, off_t off,int count, int *eof, void *data)
{
	char				*p;
	_mesh_proc_data_t	proc_data;

	if(off != 0) {
		*eof	= 1;
		return 0;
	}

	*start				= page + off;
	p					= page;
	proc_data.p			= p;

	proc_data.p			+= sprintf(p,                         
					               "---------------------------------------\r\n"
					               "PARENT ADDR      |CHN|SIG|ITEM ID |CNT|\r\n"
					               "---------------------------------------\r\n");

	mesh_ext_enumerate_mobility_window((void*)&proc_data,_enum_mobwin_routine);

	p					= proc_data.p;

	return (p - page);
}


static int _table_list_routine(char *page, char **start, off_t off,int count, int *eof, void *data)
{
	char				*p;
	_mesh_proc_data_t	proc_data;

	if(off != 0) {
		*eof	= 1;
		return 0;
	}

	*start				= page + off;
	p					= page;
	proc_data.p			= p;

	proc_data.p			+= sprintf(p,
						   "------------------------------------------------------------------------\r\n"
					       "STATION ADDR : RELAY AP ADDR -> IMMEDIATE AP ADDR   \r\n"
					       "------------------------------------------------------------------------\r\n");

	mesh_ext_enumerate_table((void*)&proc_data,_enum_table_routine);

	p					= proc_data.p;

	return (p - page);

}


static void _enum_table_routine(void* callback,const mesh_ext_table_entry_t* entry)
{
	_mesh_proc_data_t*	proc_data;
	int					i;

	proc_data	= (_mesh_proc_data_t*)callback;

	for(i = 0; i < entry->level; i++) {
		proc_data->p	+=	sprintf(proc_data->p,"\t");
	}

	proc_data->p		+=	sprintf(proc_data->p,
		                            AL_NET_ADDR_STR" : "AL_NET_ADDR_STR" -> "AL_NET_ADDR_STR"\r\n",
									AL_NET_ADDR_TO_STR(&entry->sta_addr),
									AL_NET_ADDR_TO_STR(&entry->relay_ap_addr),
									AL_NET_ADDR_TO_STR(&entry->sta_ap_addr));
}

static int _htable_list_routine(char *page, char **start, off_t off,int count, int *eof, void *data)
{
	char				*p;
	char				*ptr;
	_mesh_proc_data_t	proc_data;

	if(off != 0) {
		*eof	= 1;
		return 0;
	}

	*start				= page + off;
	p					= page;
	proc_data.p			= p;
	ptr					= proc_data.p;

	TABLE_START(ptr);
	TABLE_ROW_HEAD_START(ptr);
	TABLE_ROW_HEAD		(ptr, "Client");
	TABLE_ROW_HEAD		(ptr, "Relay AP");	
	TABLE_ROW_HEAD		(ptr, "Immediate AP");
	TABLE_ROW_HEAD_END	(ptr);

	proc_data.p			= ptr;

	mesh_ext_enumerate_table((void*)&proc_data,_henum_table_routine);

	ptr					= proc_data.p;
	TABLE_END(ptr);
	proc_data.p			= ptr;

	p					= proc_data.p;

	return (p - page);

}

static void _henum_table_routine(void* callback,const mesh_ext_table_entry_t* entry)
{
	_mesh_proc_data_t*	proc_data;

	proc_data	= (_mesh_proc_data_t*)callback;

	TABLE_ROW_DATA_START(proc_data->p);	
	TABLE_ROW_DATA_MAC	(proc_data->p, AL_NET_ADDR_TO_STR(&entry->sta_addr));
	TABLE_ROW_DATA_MAC	(proc_data->p, AL_NET_ADDR_TO_STR(&entry->relay_ap_addr));
	TABLE_ROW_DATA_MAC	(proc_data->p, AL_NET_ADDR_TO_STR(&entry->sta_ap_addr));
	TABLE_ROW_DATA_END	(proc_data->p);
}

static int _hkap_list_routine(char *page, char **start, off_t off,int count, int *eof, void *data)
{
	char				*p;
	char				*ptr;
	_mesh_proc_data_t	proc_data;

	if(off != 0) {
		*eof	= 1;
		return 0;
	}

	*start				= page + off;
	p					= page;
	proc_data.p			= p;
	ptr					= proc_data.p;

	TABLE_START(ptr);
	TABLE_ROW_HEAD_START(ptr);
	TABLE_ROW_HEAD		(ptr, "Address");
	TABLE_ROW_HEAD		(ptr, "Channel");	
	TABLE_ROW_HEAD		(ptr, "Signal");
	TABLE_ROW_HEAD		(ptr, "Rate");
	TABLE_ROW_HEAD		(ptr, "Tree Rate");
	TABLE_ROW_HEAD		(ptr, "Flags");	
	TABLE_ROW_HEAD_END	(ptr);

	proc_data.p			= ptr;

	mesh_ext_enumerate_known_aps((void*)&proc_data,_henum_kap_routine);

	ptr					= proc_data.p;
	TABLE_END(ptr);
	proc_data.p			= ptr;

	p					= proc_data.p;

	return (p - page);
}

static void _henum_kap_routine(void* callback,const mesh_ext_known_access_point_info_t* kap)
{
	_mesh_proc_data_t*	proc_data;
	char				flags[6];

	proc_data	= (_mesh_proc_data_t*)callback;


	flags[0]	= kap->flags & MESH_EXT_KAP_FLAG_PREFERRED	? 'P' : '-';
	flags[1]	= kap->flags & MESH_EXT_KAP_FLAG_MOBILE		? 'M' : '-';
	flags[2]	= kap->flags & MESH_EXT_KAP_FLAG_CHILD		? 'C' : '-';
	flags[3]	= kap->flags & MESH_EXT_KAP_FLAG_QUESTION	? 'Q' : '-';
	flags[4]	= kap->flags & MESH_EXT_KAP_FLAG_DISABLED	? 'D' : '-';
	flags[4]	= 0;

	TABLE_ROW_DATA_START(proc_data->p);	
	TABLE_ROW_DATA_MAC	(proc_data->p, AL_NET_ADDR_TO_STR(&kap->bssid));
	TABLE_ROW_DATA_INT	(proc_data->p, kap->channel);
	TABLE_ROW_DATA_INT	(proc_data->p, kap->signal);
	TABLE_ROW_DATA_INT	(proc_data->p, kap->direct_bit_rate);
	TABLE_ROW_DATA_INT	(proc_data->p, kap->tree_bit_rate);
	TABLE_ROW_DATA_STR	(proc_data->p, flags);
	TABLE_ROW_DATA_END	(proc_data->p);
}

static const char _func_modes[][8] = {
	"UNK",
	"FFR",
	"FFN",
	"LFR",
	"LFN"
};

static void _adhoc_kap_routine(void* callback,const mesh_ext_known_access_point_info_t* kap)
{
	_mesh_proc_data_t*	proc_data;
	const char*			fnc;

	proc_data	= (_mesh_proc_data_t*)callback;

	if(kap->flags & MESH_EXT_KAP_FLAG_LIMITED) {
		if(kap->hpc	== 0)
			fnc	= _func_modes[MESH_EXT_FUNC_MODE_LFR];
		else
			fnc	= _func_modes[MESH_EXT_FUNC_MODE_LFN];
	} else {
		if(kap->hpc	== 0)
			fnc	= _func_modes[MESH_EXT_FUNC_MODE_FFR];
		else
			fnc	= _func_modes[MESH_EXT_FUNC_MODE_FFN];
	}

	proc_data->p	+= sprintf(proc_data->p,
							   AL_NET_ADDR_STR"|%03d|%03d|%04d|%05d|%c %c %c %c %c %c|%3s|%05d|\r\n",
							   AL_NET_ADDR_TO_STR(&kap->bssid),
							   kap->channel,
							   kap->signal,
							   kap->direct_bit_rate,
							   kap->tree_bit_rate,
							   kap->flags & MESH_EXT_KAP_FLAG_PREFERRED ? 'P' : '-',
							   kap->flags & MESH_EXT_KAP_FLAG_MOBILE ? 'M' : '-',
							   kap->flags & MESH_EXT_KAP_FLAG_CHILD ? 'C' : '-',
							   kap->flags & MESH_EXT_KAP_FLAG_AD_DISABLE ? 'W' : '-',
							   kap->flags & MESH_EXT_KAP_FLAG_DISABLED ? 'D' : '-',
							   kap->flags & MESH_EXT_KAP_FLAG_LIMITED ? 'L' : '-',
							   fnc,
							   kap->dhcp_r_value);
}

static int _adhoc_mode_routine(char *page, char **start, off_t off,int count, int *eof, void *data)
{
	char				*p;
	int					func_mode;
	const char*			fnc;

	if(off != 0) {
		*eof	= 1;
		return 0;
	}

	*start				= page + off;
	p					= page;

	func_mode			= mesh_ext_get_func_mode();

	switch(func_mode) {
	case MESH_EXT_FUNC_MODE_FFR:
	case MESH_EXT_FUNC_MODE_FFN:
	case MESH_EXT_FUNC_MODE_LFR:
	case MESH_EXT_FUNC_MODE_LFN:
		fnc	= _func_modes[func_mode];
		break;
	default:
		fnc	= "UNKNOWN";
	}

	p += sprintf(p, "%s\r\n", fnc);

	return (p - page);
}

static int _adhoc_list_routine(char *page, char **start, off_t off,int count, int *eof, void *data)
{
	char				*p;
	_mesh_proc_data_t	proc_data;
	int					func_mode;
	const char*			fnc;
	int					step_scan_index;

	if(off != 0) {
		*eof	= 1;
		return 0;
	}

	*start				= page + off;
	p					= page;
	proc_data.p			= p;
	proc_data.flag		= 0;

	func_mode			= mesh_ext_get_func_mode();
	step_scan_index		= mesh_ext_get_step_scan_channel_index();

	switch(func_mode) {
	case MESH_EXT_FUNC_MODE_FFR:
	case MESH_EXT_FUNC_MODE_FFN:
	case MESH_EXT_FUNC_MODE_LFR:
	case MESH_EXT_FUNC_MODE_LFN:
		fnc	= _func_modes[func_mode];
		break;
	default:
		fnc	= "UNKNOWN";
	}

	proc_data.p			+= sprintf(p,
		                           "NODE OPERATING AS %s STEP SCAN INDEX %d\r\n\r\n"
								   "-----------------------------------------------------------\r\n"
						           "PARENT ADDR      |CHN|SIG|RATE|TRATE|FLAGS      |FNC|R-VAL|\r\n"
						           "-----------------------------------------------------------\r\n",
								   fnc,
								   step_scan_index);

	mesh_ext_enumerate_known_aps((void*)&proc_data,_adhoc_kap_routine);

	p					= proc_data.p;

	return (p - page);
}
