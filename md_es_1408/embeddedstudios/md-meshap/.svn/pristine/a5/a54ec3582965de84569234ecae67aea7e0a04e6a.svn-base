
/********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : mesh_ng_fsm_heartbeat.c
 * Comments : Mesh new generation Heartbeat state
 * Created  : 4/18/2006
 * Author   : Sriram Dayanandan
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * -----------------------------------------------------------------------------
 * | No  |Date      |  Comment                                        | Author |
 * -----------------------------------------------------------------------------
 * |  3  |7/9/2007  | Sync only when fully ASSOCIATED                 | Sriram |
 * -----------------------------------------------------------------------------
 * |  2  |4/26/2007 | Re-enforcement logic added for Syncing          | Sriram |
 * -----------------------------------------------------------------------------
 * |  1  |4/26/2007 | Interrupts disabled when syncing                | Sriram |
 * -----------------------------------------------------------------------------
 * |  0  |4/18/2006 | Created.                                        | Sriram |
 * -----------------------------------------------------------------------------
 ********************************************************************************/

/**
 * This is a private source file included by mesh_ng_fsm.c, hence
 * It was created to improve readability.
 */

static void _sta_enum_routine(void* callback,const access_point_sta_info_t* sta)
{
	mesh_table_entry_t*		table_entry;
	mesh_table_entry_t*		table_entry_list;
	int						ret;
	int						check;

	check	= (int)callback;

	table_entry	= mesh_table_entry_find(AL_CONTEXT (al_net_addr_t*)&sta->address);

	if(table_entry == NULL) {

		if(sta->state == ACCESS_POINT_STA_STATE_ASSOCIATED) {
			ret = mesh_table_entry_add(AL_CONTEXT sta->net_if, (al_net_addr_t*)&sta->address, NULL, NULL,6);

			al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,
						 "MESH_NG	: Entry for "AL_NET_ADDR_STR" added with AP sync",
						 AL_NET_ADDR_TO_STR(&sta->address));
			
			if(!ret) {
				mesh_imcp_send_packet_sta_assoc_notification(AL_CONTEXT (al_net_addr_t*)&sta->address, (al_net_addr_t*)&sta->net_if->config.hw_addr);
			}
		}

	} else {

		if(check == 1 && mesh_mode == _MESH_AP_RELAY) {

			/**
			 * There are times when an entry exists in the hashtable but does not exist on the list
			 */

			table_entry_list	= mesh_table_entry_find_in_list(AL_CONTEXT (al_net_addr_t*)&sta->address);

			if(table_entry_list == NULL) {

				al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,
							 "MESH_NG	: Entry for "AL_NET_ADDR_STR" missing from Mesh table list",
							 AL_NET_ADDR_TO_STR(&sta->address));

				mesh_ng_fsm_send_event(AL_CONTEXT MESH_NG_FSM_EVENT_FULL_DISCONNECT,NULL);
				al_set_event(AL_CONTEXT mesh_thread_wait_event);
			}

		}
	}
}

static void _sync_direct_children(AL_CONTEXT_PARAM_DECL_SINGLE)
{
	al_u64_t	tick;
	int			check;

	tick	= al_get_tick_count(AL_CONTEXT_SINGLE);
	check	= 0;

	if(tick - last_table_check >= MESH_NG_TABLE_CHECK_INTERVAL
	|| tick < last_table_check) {
		check				= 1;
		last_table_check	= tick;
		al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,
					 "MESH_NG	: Performing integrity check for mesh table list");
	}

	access_point_ext_enumerate_stas((void*)check,_sta_enum_routine);

}


static void _heartbeat_execute(AL_CONTEXT_PARAM_DECL_SINGLE)
{
	al_notify_message_t		message;

	/**
	 * It has been observed that sometimes direct children's entries are not
	 * present in the mesh. This case is hard to reproduce, but as a precaution
	 * we enumerate the STAs with the access point, and add entries we may have
	 * missed.
	 */
	
	_sync_direct_children(AL_CONTEXT_SINGLE);

	if(mesh_mode == _MESH_AP_RELAY) {
		if(mesh_ng_core_purge_parents(AL_CONTEXT_SINGLE) != 0) {
			/** We Purged out our current parent, so now find an alternate one */
			_mesh_ng_fsm_set_state(AL_CONTEXT MESH_NG_FSM_STATE_ALTERNATE_SCAN);
			return;
		}
	}

	mesh_imcp_send_packet_heartbeat(AL_CONTEXT mesh_heart_beat_sqnr);
	mesh_imcp_send_packet_heartbeat2(AL_CONTEXT mesh_heart_beat_sqnr);
	mesh_heart_beat_sqnr++;

	if(_PROBE_REQUEST_LOCATION_SUPPORT) {
		mesh_location_send_info(AL_CONTEXT_SINGLE);
	}

	mesh_ng_fsm_update_downlink_vendor_info(AL_CONTEXT_SINGLE);

	message.message_type		= AL_IMPL_NOTIFY_MESSAGE_TYPE_STATUS;
	message.message_data		= (void*)'Y';
	al_notify_message(AL_CONTEXT &message);

	_mesh_ng_fsm_set_state(AL_CONTEXT MESH_NG_FSM_STATE_RUNNING);
}
