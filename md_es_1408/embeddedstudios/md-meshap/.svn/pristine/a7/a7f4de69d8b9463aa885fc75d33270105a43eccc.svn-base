
/********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : mesh_ng_fsm_alt_scan.c
 * Comments : Mesh New Generation Alternate scan state
 * Created  : 4/10/2006
 * Author   : Sriram Dayanandan
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * -----------------------------------------------------------------------------
 * | No  |Date      |  Comment                                        | Author |
 * -----------------------------------------------------------------------------
 * |  1  |2/2/2007  | Restore LED state after joining                 | Sriram |
 * -----------------------------------------------------------------------------
 * |  0  |4/10/2006 | Created.                                        | Sriram |
 * -----------------------------------------------------------------------------
 ********************************************************************************/

/**
 * This is a private source file included by mesh_ng_fsm.c, hence
 * It was created to improve readability.
 */

static void _alternate_scan_execute(AL_CONTEXT_PARAM_DECL_SINGLE)
{
	al_notify_message_t			message;

	message.message_type		= AL_NOTIFY_MESSAGE_TYPE_SET_STATE;
	message.message_data		= (void*)'Y';
	al_notify_message(AL_CONTEXT &message);

	mesh_ng_evaluate_list(AL_CONTEXT_SINGLE);

	al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MESH_NG	: Disabling AP processing before alternate parent search");
	access_point_enable_disable_processing(AL_CONTEXT 0);

	/**
	 * RE-ENABLE the NET IF after disabling AP processing so that ASSOC REQ can be sent
	 */

	AL_ATOMIC_SET(_DS_NET_IF->buffering_state,AL_NET_IF_BUFFERING_STATE_NONE);

	if(mesh_ng_core_join_best_parent(AL_CONTEXT_SINGLE)) {
		al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MESH_NG	: Alternate scan failed, performing fallback...");
		_mesh_ng_fsm_set_state(AL_CONTEXT MESH_NG_FSM_STATE_FULL_SCAN);
		return;
	}

	_mesh_ng_fsm_set_state(AL_CONTEXT MESH_NG_FSM_STATE_RUNNING);

	al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MESH_NG	: Enabling AP processing after <quick> alternate parent search");
	access_point_enable_disable_processing(AL_CONTEXT 1);

	/**
	 * RE-ENABLE the NET IF just as a precaution
	 */

	AL_ATOMIC_SET(_DS_NET_IF->buffering_state,AL_NET_IF_BUFFERING_STATE_NONE);
	
	mesh_imcp_send_packet_ap_sub_tree_info(AL_CONTEXT_SINGLE);

	message.message_type		= AL_NOTIFY_MESSAGE_TYPE_SET_STATE;
	message.message_data		= (void*)'G';
	al_notify_message(AL_CONTEXT &message);


	mesh_imcp_send_packet_heartbeat(AL_CONTEXT mesh_heart_beat_sqnr);
	mesh_imcp_send_packet_heartbeat2(AL_CONTEXT mesh_heart_beat_sqnr);
	mesh_heart_beat_sqnr++;

	al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MESH_NG	: Got best parent, (Parent quick shift, alternate), sub tree info sent.");

}
