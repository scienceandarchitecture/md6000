
/********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : meshap_arp.c
 * Comments : ARP Implementation
 * Created  : 1/15/2008
 * Author   : Abhijit Ayarekar
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * -----------------------------------------------------------------------------
 * | No  |Date      |  Comment                                        | Author |
 * -----------------------------------------------------------------------------
 * |  0  |1/15/2008 | Created.                                        |Abhijit |
 * -----------------------------------------------------------------------------
 ********************************************************************************/

#include <linux/string.h>
#include "al.h"
#include "al_802_11.h"
#include "al_ip.h"
#include "meshap_arp.h"

#define _IP_ADDR_SIZE				4

#define _ARP_POSITION_HTYPE				0
#define _ARP_POSITION_PTYPE				(_ARP_POSITION_HTYPE + 2)
#define _ARP_POSITION_HLEN				(_ARP_POSITION_PTYPE + 2)
#define _ARP_POSITION_PLEN				(_ARP_POSITION_HLEN + 1)
#define _ARP_POSITION_OPCODE			(_ARP_POSITION_PLEN + 1)
#define _ARP_POSITION_SRCMAC			(_ARP_POSITION_OPCODE + 2)
#define _ARP_POSITION_SRCIP				(_ARP_POSITION_SRCMAC + 6)
#define _ARP_POSITION_DSTMAC			(_ARP_POSITION_SRCIP + 4)
#define _ARP_POSITION_DSTIP				(_ARP_POSITION_DSTMAC + 6)

#define _ARP_PACKET_SIZE				(_ARP_POSITION_DSTIP + 4)

#define _ARP_HARDWARE_TYPE_ETH			1
#define _ARP_HARDWARE_TYPE_ETH_ALEN		6
#define _ARP_OPCODE_REQUEST				1
#define _ARP_OPCODE_REPLY				2

struct _addr_info {
	unsigned char		ip_addr[4];
	unsigned char		use_mac_addr;
	unsigned char		use_count;
};

typedef struct _addr_info _addr_info_t;

struct _arp_priv {
	al_net_addr_t	ds_mac_address;
	int				addr_count;
	_addr_info_t	addr_list[16];
};

typedef struct _arp_priv _arp_priv_t;


meshap_arp_instance_t	meshap_arp_initialize(al_net_addr_t* ds_mac_address)
{
	_arp_priv_t*	priv;

	priv = (_arp_priv_t*)al_heap_alloc(AL_CONTEXT sizeof(_arp_priv_t));

	memset(priv,0,sizeof(_arp_priv_t));
	
	memcpy(&priv->ds_mac_address, ds_mac_address, sizeof(al_net_addr_t));

	return (meshap_arp_instance_t)priv;
}

void meshap_arp_uninitialize(meshap_arp_instance_t instance)
{
	_arp_priv_t*	priv;

	if(instance == NULL) 
		return;

	priv	= (_arp_priv_t*)instance;
	al_heap_free(AL_CONTEXT priv);
	
	return;
}

int meshap_arp_add_entry(meshap_arp_instance_t instance, unsigned char* ip_addr, int use_addr)
{
	_arp_priv_t*	priv;
	int				i;

	if(instance == NULL) 
		return -1;

	al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,
		         "ARP: Adding entry : Ip : %d.%d.%d.%d, Use net if : %s", ip_addr[0], ip_addr[1],
																	ip_addr[2], ip_addr[3],
																	(use_addr == MESHAP_ARP_USE_ADDR_DS_NET_IF) ?
																	"DS NET IF" : "INCOMING NET_IF");

	priv	= (_arp_priv_t*)instance;
	
	for(i=0;i<priv->addr_count;i++) {

		if(memcmp(&priv->addr_list[i].ip_addr, ip_addr, _IP_ADDR_SIZE) == 0) {
			priv->addr_list[i].use_mac_addr = use_addr;
			priv->addr_list[i].use_count	+= 1;
			return 0;
		}

	}
	
	memcpy(&priv->addr_list[priv->addr_count].ip_addr, ip_addr, _IP_ADDR_SIZE);
	priv->addr_list[priv->addr_count].use_mac_addr	= use_addr;
	priv->addr_count				+= 1;
	priv->addr_list[i].use_count	+= 1;

	return 0;
}

static AL_INLINE int _delete_arp_entry(_arp_priv_t* priv, int entry_index)
{
	int				i;
	int				entries_moved;
	unsigned char*	p;

	memset(&priv->addr_list[entry_index], 0, sizeof(_addr_info_t));
	
	for(i=entry_index+1,entries_moved=0;i<priv->addr_count;i++,entries_moved++) {
		memcpy(&priv->addr_list[i-1], &priv->addr_list[i], sizeof(_addr_info_t));
	}

	p = (unsigned char*)&priv->addr_list[i];
	memset(p, 0, sizeof(_addr_info_t)*entries_moved);
	priv->addr_count	-= 1;
	
	return 0;
}

int	meshap_arp_remove_entry(meshap_arp_instance_t instance, unsigned char* ip_addr)
{
	_arp_priv_t*	priv;
	int				i;

	if(instance == NULL) 
		return -1;

	priv	= (_arp_priv_t*)instance;
	
	for(i=0;i<priv->addr_count;i++) {

		if(memcmp(&priv->addr_list[i].ip_addr, ip_addr, _IP_ADDR_SIZE) != 0)
			continue;

		priv->addr_list[i].use_count -= 1;

		if(priv->addr_list[i].use_count <= 0)
			return _delete_arp_entry(priv, i);

		return 0;

	}

	return -2;
}

/**
 * DHCP clients may send UNICAST messages to the address identified by the SERVER ID
 * Since we send net_id.m.n.1 as the SERVER ID, we would need to respond to 
 * ARP requests for net_id.m.n.1.
 */


static AL_INLINE int _do_arp(_arp_priv_t*		priv,
							 al_net_if_t*		net_if,
							 al_net_addr_t*		src_addr,
							 al_packet_t*		packet_in,
							 al_packet_t**		packet_out)
{

	unsigned short	opcode;
	unsigned char	arp_src_ip[_IP_ADDR_SIZE];
	unsigned char	arp_target_ip[_IP_ADDR_SIZE];
	unsigned char*	packet_data;
	al_packet_t*	packet;
	unsigned short	temp;
	int				i;
	int				addr_found;
	al_net_addr_t*	self_mac_addr;

	*packet_out	= NULL;
	packet_data	= packet_in->buffer + packet_in->position;

	memcpy(&opcode,packet_data + _ARP_POSITION_OPCODE,2);

	opcode	= al_be16_to_cpu(opcode);

	if(opcode != _ARP_OPCODE_REQUEST)
		return MESHAP_ARP_RETVAL_CONTINUE;

	memcpy(arp_src_ip, packet_data + _ARP_POSITION_SRCIP, _IP_ADDR_SIZE);
	memcpy(arp_target_ip, packet_data + _ARP_POSITION_DSTIP, _IP_ADDR_SIZE);
	
	addr_found = 0;
	for(i=0;i<priv->addr_count;i++) {
		if(memcmp(priv->addr_list[i].ip_addr, arp_target_ip, _IP_ADDR_SIZE) == 0) {
			addr_found = 1;
			break;
		}
	}

	if(addr_found == 0)
		return MESHAP_ARP_RETVAL_CONTINUE;

	/**
	 * Create a ARP reply packet 
	 */

	packet	= al_allocate_packet(AL_CONTEXT AL_PACKET_DIRECTION_OUT);

	if(packet == NULL) {
		return MESHAP_ARP_RETVAL_CONTINUE;
	}

	packet_data	= packet->buffer + packet->position - _ARP_PACKET_SIZE;

	temp		= _ARP_HARDWARE_TYPE_ETH;
	temp		= al_cpu_to_be16(temp);
	memcpy(packet_data + _ARP_POSITION_HTYPE,&temp,2);

	temp		= AL_PACKET_TYPE_IP;
	temp		= al_cpu_to_be16(temp);
	memcpy(packet_data + _ARP_POSITION_PTYPE,&temp,2);

	packet_data[_ARP_POSITION_HLEN] = _ARP_HARDWARE_TYPE_ETH_ALEN;
	packet_data[_ARP_POSITION_PLEN]	= _IP_ADDR_SIZE;

	temp		= _ARP_OPCODE_REPLY;
	temp		= al_cpu_to_be16(temp);
	memcpy(packet_data + _ARP_POSITION_OPCODE,&temp,2);	

	if(priv->addr_list[i].use_mac_addr == MESHAP_ARP_USE_ADDR_DS_NET_IF) 
		self_mac_addr = &priv->ds_mac_address;
	else
		self_mac_addr = &net_if->config.hw_addr;

	memcpy(packet_data + _ARP_POSITION_SRCMAC,self_mac_addr->bytes,_ARP_HARDWARE_TYPE_ETH_ALEN);
	memcpy(packet_data + _ARP_POSITION_SRCIP,priv->addr_list[i].ip_addr,_IP_ADDR_SIZE);
	memcpy(packet_data + _ARP_POSITION_DSTMAC,src_addr->bytes,_ARP_HARDWARE_TYPE_ETH_ALEN);
	memcpy(packet_data + _ARP_POSITION_DSTIP,arp_src_ip,_IP_ADDR_SIZE);

	packet->position	-= _ARP_PACKET_SIZE;
	packet->data_length	= _ARP_PACKET_SIZE;
	packet->type		= al_cpu_to_be16(AL_PACKET_TYPE_ARP);

	memcpy(&packet->addr_1,src_addr,sizeof(al_net_addr_t));
	memcpy(&packet->addr_2,&priv->ds_mac_address,sizeof(al_net_addr_t));

	*packet_out	= packet;

	return MESHAP_ARP_RETVAL_SUCCESS;
}

int meshap_arp_process_packet(meshap_arp_instance_t	instance, al_net_if_t* net_if, al_packet_t* packet_in, al_packet_t** packet_out)
{
	_arp_priv_t*	priv;
	al_net_addr_t*	src_addr;
	int				ret;

	priv			= (_arp_priv_t*)instance;
	src_addr		= &packet_in->addr_2;
	ret				= MESHAP_ARP_RETVAL_SUCCESS;
	*packet_out		= NULL;

	if(al_be16_to_cpu(packet_in->type) != AL_PACKET_TYPE_ARP) {
		return MESHAP_ARP_RETVAL_CONTINUE;
	}

	ret	= _do_arp(priv,net_if,src_addr,packet_in,packet_out);

	/**
	 * If *packet_out is not NULL, we need to setup the correct
	 * frame control and flags depending on the type of the net_if.
	 *  Moreover the *packet_out current has ADDR1 as DST, and ADDR2
	 * as the SRC. In case of 802.11 interfaces, ADDR3 needs to be
	 * set to the SRC address and ADDR2 is set to the BSSID
	 */

	if((ret == MESHAP_ARP_RETVAL_SUCCESS) && (*packet_out != NULL) && AL_NET_IF_IS_WIRELESS(net_if)) {
		(*packet_out)->frame_control	= AL_802_11_FC_FROMDS;
		memcpy(&(*packet_out)->addr_3,&(*packet_out)->addr_2,sizeof(al_net_addr_t));
		memcpy(&(*packet_out)->addr_2,&net_if->config.hw_addr,sizeof(al_net_addr_t));
	}

	return ret;
}

