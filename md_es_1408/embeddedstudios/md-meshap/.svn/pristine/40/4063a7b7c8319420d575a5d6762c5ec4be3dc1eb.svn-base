
/********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : fips-impl.h
 * Comments : Meshap FIPS 140-2 Compliance implementation
 * Created  : 01/11/2006
 * Author   : Prachiti Gaikwad
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * -----------------------------------------------------------------------------
 * | No  |Date      |  Comment                                        | Author |
 * -----------------------------------------------------------------------------
 * |  0  |01/11/2006| Created.                                        |Prachiti|
 * -----------------------------------------------------------------------------
 ********************************************************************************/


#ifndef __FIPS_IMPL_H__
#define __FIPS_IMPL_H__

#define AES_KEY_SIZE	16
#define BITS64			8
#define BITS128			16
#define BITS192			24
#define BITS256			32
#define BITS512			64

/* Error Codes */

#define GENERIC_ERROR			-1
#define AES_TEST_ERROR			-2
#define HMAC_TEST_ERROR			-3
#define RANDOM_NUMBER_REPEATED	-4
#define RNG_SELF_TEST_ERROR		-5

struct error_code{
	char	desc[50];
	int		type;
};

typedef struct error_code error_code_t; 

 
#ifdef AES_WRAP

#define AES_SET_KEY(k,l) 

#define AES_ENCRYPT(pt,ct,k) AES_encrypt(pt,ct,k)

#define AES_DECRYPT(ct,pt,k) AES_decrypt(ct,pt,k)

#else


#define AES_SET_KEY(k,l) if(aes_set_key(&s_aes_context,k,l,aes_both) == aes_bad){ \
return -1;\
}	

#define AES_ENCRYPT(pt,ct) if(aes_encrypt(&s_aes_context,pt,ct) == aes_bad){ \
return -1;\
}	

#define AES_DECRYPT(ct,pt) if(aes_decrypt(&s_aes_context,ct,pt) == aes_bad){ \
return -1;\
}

#endif


#define XOR(a,b,out,len) {int l=0; while(l<len){ out[l] = (a[l]^b[l]); l++; \
}} 


#define PRINT_ARRAY(a,len) {int l=0; while(l<len){ printf("%x,",a[l]); l++; \
}} 



int		init_fips_compliant_mode(void);
int		get_fips_compliant_random_bytes	(unsigned char* buffer,int length);
int		do_power_on_self_test_aes(void);
int		do_power_on_self_test_hmac_sha1(void);
int 	do_power_on_self_test_rng(void);

#endif
