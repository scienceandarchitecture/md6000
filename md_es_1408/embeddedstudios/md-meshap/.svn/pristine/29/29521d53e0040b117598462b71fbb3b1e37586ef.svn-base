
/********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : dot1x-backend-radius-impl.c
 * Comments : Linux Kernel Radius IMPL
 * Created  : 3/24/2005
 * Author   : Sriram Dayanandan
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * -----------------------------------------------------------------------------
 * | No  |Date      |  Comment                                        | Author |
 * -----------------------------------------------------------------------------
 * |  4  |7/19/2007 | Loop added to RADIUS TX                         | Sriram |
 * -----------------------------------------------------------------------------
 * |  3  |7/18/2007 | MSG_NOSIGNAL Added to RADIUS TX                 | Sriram |
 * -----------------------------------------------------------------------------
 * |  2  |7/13/2007 | RADIUS RX messages printed                      | Sriram |
 * -----------------------------------------------------------------------------
 * |  1  |4/6/2005  | Misc changes                                    | Sriram |
 * -----------------------------------------------------------------------------
 * |  0  |3/24/2005 | Created.                                        | Sriram |
 * -----------------------------------------------------------------------------
 ********************************************************************************/

#include <generated/autoconf.h>
#include <linux/module.h>
#include <linux/init.h>
#include <linux/version.h>
#include <linux/skbuff.h>
#include <net/sock.h>
#include <asm/uaccess.h>

#include "dot1x.h"
#include "dot1x-backend-radius.h"

static struct socket*	s		= NULL;
static unsigned char*	buffer	= NULL;
static int				pid		= -1;

static int _radius_thread(void* data);

int dot1x_backend_radius_init()
{
	buffer	= (unsigned char*)kmalloc(4096,GFP_ATOMIC);

	sock_create(PF_INET,SOCK_DGRAM,IPPROTO_UDP,&s);

	s->sk->sk_allocation = GFP_ATOMIC;

	pid	= kernel_thread(_radius_thread, NULL, CLONE_FS | CLONE_FILES | CLONE_SIGHAND);

	return 0;
}

int dot1x_backend_radius_uninit()
{
	struct task_struct *tsk;

	if(pid != -1) {
		tsk = pid_task(find_vpid(pid), PIDTYPE_PID);
		send_sig(SIGKILL, tsk, 1);
	}

	return 0;
}

int dot1x_backend_radius_create_instance(dot1x_t* instance,dot1x_radius_backend_param_t* param)
{
	return 0;
}

int dot1x_backend_radius_destroy_instance(dot1x_t* instance)
{
	return 0;
}

int dot1x_backend_radius_tx(dot1x_port_t* port,dot1x_radius_backend_param_t* param,unsigned char* packet, int len)
{
	struct sockaddr_in	addr;
	struct msghdr		msg;
	struct iovec		iov;
	int					ret;
	unsigned char*		p;
	mm_segment_t		oldfs;

	p						= (unsigned char*)&param->radius_server_addr;
	addr.sin_family			= AF_INET;
	addr.sin_port			= htons(param->radius_server_port);
	addr.sin_addr.s_addr	= htonl(param->radius_server_addr);

	msg.msg_name		= &addr;
	msg.msg_namelen		= sizeof(addr);
	msg.msg_iov			= &iov;
	msg.msg_iovlen		= 1;
	msg.msg_control		= NULL;
	msg.msg_controllen	= 0;
	msg.msg_flags		= MSG_NOSIGNAL|MSG_DONTWAIT;

	oldfs = get_fs();
	set_fs(KERNEL_DS);

	do {
		iov.iov_base		= packet;
		iov.iov_len			= len;
		ret					= sock_sendmsg(s,&msg, iov.iov_len);
		if(ret > 0) {
			packet	+= ret;
			len		-= ret;
		} else {
			printk(KERN_INFO"dot1x_backend_radius_tx: Error %d sending UDP packet of length %d\n",ret,len);
			break;
		}
	}while(len > 0);

	set_fs(oldfs);

	return 0;
}

static int _radius_thread(void* data)
{
	mm_segment_t		oldfs;
	struct msghdr		msg;
	struct iovec		iov;
	struct sockaddr_in	sin;
	
	struct sockaddr_in	bind_addr;
	int					len;

	printk(KERN_INFO"md_radius : Starting\n");

	daemonize ("md_radius");
	memset(current->comm,0,sizeof(current->comm));
	strcpy(current->comm,"md_radius");

	memset(&bind_addr,0,sizeof(bind_addr));

	bind_addr.sin_family		= AF_INET;
	bind_addr.sin_port			= htons(1812);
	bind_addr.sin_addr.s_addr	= INADDR_ANY;

	s->ops->bind(s,(struct sockaddr*)&bind_addr,sizeof(bind_addr));

	while(1) {

		if (signal_pending(current))
			break;

		iov.iov_base		= buffer;
		iov.iov_len			= 4096;
		
		msg.msg_name		= &sin;
		msg.msg_namelen		= sizeof(sin);
		msg.msg_iov			= &iov;
		msg.msg_iovlen		= 1;
		msg.msg_control		= NULL;
		msg.msg_controllen	= 0;
		msg.msg_flags		= 0;

		oldfs = get_fs();
		set_fs(KERNEL_DS);
		len = sock_recvmsg(s,&msg,iov.iov_len,0);
		set_fs(oldfs);

		if (signal_pending(current))
			break;

		printk(KERN_INFO"md_radius: Received RADIUS UDP packet of length %d, processing...\n",
			   len);

		dot1x_radius_backend_rx(buffer,len);

		printk(KERN_INFO"md_radius: Returned from processing RADIUS UDP packet\n");

	}

	sock_release(s);
	kfree(buffer);

	s		= NULL;
	buffer	= NULL;
	pid		= -1;

	printk(KERN_INFO"md_radius : Stopping\n");

	return 0;
}

