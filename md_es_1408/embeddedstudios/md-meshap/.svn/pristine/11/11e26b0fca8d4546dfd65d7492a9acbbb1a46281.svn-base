
/********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : mesh_on_start.c
 * Comments : Mesh on start impl file
 * Created  : 4/22/2004
 * Author   : Anand Bhange
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * -----------------------------------------------------------------------------
 * | No  |Date      |  Comment                                        | Author |
 * -----------------------------------------------------------------------------
 * | 70  |02/27/2009| Changes for Single Radio Adhoc Mode             | Sriram |
 * -----------------------------------------------------------------------------
 * | 69  |8/27/2008 | Changes for Options                             | Sriram |
 * -----------------------------------------------------------------------------
 * | 68  |8/22/2008 | Changes for SIP                                 |Abhijit |
 * -----------------------------------------------------------------------------
 * | 67  |6/20/2007 | hope_cost reused for igmp_support               |Prachiti|
 * -----------------------------------------------------------------------------
 * | 68  |2/26/2007 | Changes for EFFISTREAM                          | Sriram |
 * -----------------------------------------------------------------------------
 * | 67  |2/9/2007  | Changes for In-direct VLAN                      | Sriram |
 * -----------------------------------------------------------------------------
 * | 66  |2/6/2007  | FIPS related changes                            |Prachiti|
 * -----------------------------------------------------------------------------
 * | 65  |2/6/2007  | IF-Specific TXRate and TXPower changes          |Prachiti|
 * -----------------------------------------------------------------------------
 * | 64  |2/2/2007  | Changes for ALLOWED VLANS                       | Sriram |
 * -----------------------------------------------------------------------------
 * | 63  |10/18/2006| Changes for ETSI Radar                          | Sriram |
 * -----------------------------------------------------------------------------
 * | 62  |5/19/2006 | Checking wireless for monitor type removed      |Prachiti|
 * -----------------------------------------------------------------------------
 * | 61  |3/13/2006 | ACL list implemented                            | Sriram |
 * -----------------------------------------------------------------------------
 * | 60  |03/08/2006| Fix  - copying of dot11e IF params to AP struct | Bindu  |
 * -----------------------------------------------------------------------------
 * | 59  |03/03/2006| Misc Changes of memcpy for dot11e               | Bindu  |
 * -----------------------------------------------------------------------------
 * | 58  |02/15/2006| Changes for dot11e                              | Bindu  |
 * -----------------------------------------------------------------------------
 * | 57  |02/08/2006| Changes for Config SQNR and Hide SSID           | Sriram |
 * -----------------------------------------------------------------------------
 * | 56  |01/01/2006| fix for analyser                                | Abhijit|
 * -----------------------------------------------------------------------------
 * | 55  |12/14/2005| acktimeout and regdomain changes                |prachiti|
 * -----------------------------------------------------------------------------
 * | 54  |10/5/2005 | Oscillation related changes                     | Sriram |
 * -----------------------------------------------------------------------------
 * | 53  |10/4/2005 | Changes for IMCP WM Info                        | Sriram |
 * -----------------------------------------------------------------------------
 * | 52  |9/21/2005 | MESH_INIT_STATUS formatting enhanced            | Sriram |
 * -----------------------------------------------------------------------------
 * | 51  |9/13/2005 | PHY mode set of scanning radio + others         | Sriram |
 * -----------------------------------------------------------------------------
 * | 50  |9/12/2005 | mesh_on_start restructured                      | Sriram |
 * -----------------------------------------------------------------------------
 * | 49  |9/12/2005 | Encryption disabled on MESH_INIT_STATUS         | Sriram |
 * -----------------------------------------------------------------------------
 * | 48  |9/12/2005 | Channel power density used in DCA logic         | Sriram |
 * -----------------------------------------------------------------------------
 * | 47  |6/15/2005 | Misc changes                                    | Sriram |
 * -----------------------------------------------------------------------------
 * | 46  |5/23/2005 | TXRate changes                                  | Sriram |
 * -----------------------------------------------------------------------------
 * | 45  |5/16/2005 | MESH_INIT_STATUS channel set                    | Sriram |
 * -----------------------------------------------------------------------------
 * | 44  |4/17/2005 | mode parameter added to scan function           | Sriram |
 * -----------------------------------------------------------------------------
 * | 43  |4/12/2005 | set_hw_addr implemented                         | Sriram |
 * -----------------------------------------------------------------------------
 * | 42  |4/9/2005  | Chnages to config copying after merging         | Sriram |
 * -----------------------------------------------------------------------------
 * | 41  |04/06/2005| Security changes                                | Sriram |
 * -----------------------------------------------------------------------------
 * | 40  |04/07/2005| access_point_add_vlan_info call added           | Abhijit|
 * -----------------------------------------------------------------------------
 * | 39  |04/02/2005| Misc bug fixes while monitor radio testing      | Anand  |
 * -----------------------------------------------------------------------------
 * | 38  |1/14/2005 | Misc changes                                    | Sriram |
 * -----------------------------------------------------------------------------
 * | 37  |1/8/2005  | Changes for per-interface essid/etc             | Sriram |
 * -----------------------------------------------------------------------------
 * | 36  |1/6/2005  | Misc changes                                    | Sriram |
 * -----------------------------------------------------------------------------
 * | 35  |12/22/2004| prescan and postscan functions added            | Sriram |
 * -----------------------------------------------------------------------------
 * | 34  |12/15/2004| Phy mode set for DS and WM interfaces           | Sriram |
 * -----------------------------------------------------------------------------
 * | 33  |12/14/2004| Misc changes                                    | Sriram |
 * -----------------------------------------------------------------------------
 * | 32  |11/16/2004| Changes for DCA Logic                           | Anand  |
 * -----------------------------------------------------------------------------
 * | 31  |11/10/2004| mesh_on_start preamble_type,slot_time_type added| Abhijit|
 * -----------------------------------------------------------------------------
 * | 30  |10/28/2004| Changes for monitor radio                       | Anand  |
 * -----------------------------------------------------------------------------
 * | 29  |10/11/2004| changes for IMCP5.0                             | Abhijit|
 * -----------------------------------------------------------------------------
 * | 28  |8/26/2004 | Mesh Init Packet changes.                       | Anand  |
 * -----------------------------------------------------------------------------
 * | 27  |7/23/2004 | Misc changes                                    | Sriram |
 * -----------------------------------------------------------------------------
 * | 26  |7/23/2004 | changes in DCA logic for mesh_conf dca list     | Anand  |
 * -----------------------------------------------------------------------------
 * | 25  |7/19/2004 | ap_is_enabled added                             | Anand  |
 * -----------------------------------------------------------------------------
 * | 24  |7/5/2004  | no link state checking for pseudoWM             | Anand  |
 * -----------------------------------------------------------------------------
 * | 23  |7/5/2004  | proper exit fro stop even if scanning           | Anand  |
 * -----------------------------------------------------------------------------
 * | 22  |6/16/2004 | ASSERT statements for checking NULL values      | Anand  |
 * -----------------------------------------------------------------------------
 * | 21  |6/7/2004  | SetMode(Infra) before scanning for each net_if  | Anand  |
 * -----------------------------------------------------------------------------
 * | 20  |6/7/2004  | Filter macro used instead of al_print_log       | Anand  |
 * -----------------------------------------------------------------------------
 * | 19  |6/2/2004  | DCA impl changed (Rnd implemented)              | Anand  |
 * -----------------------------------------------------------------------------
 * | 18  |6/1/2004  | Bug Fixed - find_ds & find_wm impl changed.     | Anand  |
 * -----------------------------------------------------------------------------
 * | 17  |5/31/2004 | changes to print Device name                    | Anand  |
 * -----------------------------------------------------------------------------
 * | 16  |5/31/2004 | changes in include files for linux compilation  | Anand  |
 * -----------------------------------------------------------------------------
 * | 15  |5/28/2004 | known_aps operations moved to mesh.c            | Anand  |
 * -----------------------------------------------------------------------------
 * | 14  |5/26/2004 | Bug Fixed -  known_aps (deleted object)         | Anand  |
 * -----------------------------------------------------------------------------
 * | 13  |5/25/2004 | mesh state notify messages added                | Anand  |
 * -----------------------------------------------------------------------------
 * | 12  |5/21/2004 | free ap_data after processing all Scan AP       | Anand  |
 * -----------------------------------------------------------------------------
 * | 11  |5/21/2004 | Log type added to al_print_log                  | Anand  |
 * -----------------------------------------------------------------------------
 * | 10  |5/21/2004 | Reset event called before handshake request.    | Anand  |
 * -----------------------------------------------------------------------------
 * |  9  |5/14/2004 | Stop state checking added while startup         | Anand  |
 * -----------------------------------------------------------------------------
 * |  8  |5/12/2004 | mesh mode set                                   | Anand  |
 * -----------------------------------------------------------------------------
 * |  7  |5/11/2004 | Bug Fixed, i used in array                      | Anand  |
 * -----------------------------------------------------------------------------
 * |  6  |5/11/2004 | find_parent_for_wireless_ds added to header     | Anand  |
 * -----------------------------------------------------------------------------
 * |  5  |5/7/2004  | associate_with_best_parent moved to mesh.h      | Anand  |
 * -----------------------------------------------------------------------------
 * |  4  |5/5/2004  | Packet/Memory Leak Debug params added           | Anand  |
 * -----------------------------------------------------------------------------
 * |  3  |4/30/2004 | Log messages Added                              | Anand  |
 * -----------------------------------------------------------------------------
 * |  2  |4/30/2004 | WM interface related config bug fixed           | Anand  |
 * -----------------------------------------------------------------------------
 * |  1  |4/26/2004 | Testing Base Line                               | Anand  |
 * -----------------------------------------------------------------------------
 * |  0  |4/22/2004 | Created                                         | Anand  |
 * -----------------------------------------------------------------------------
 ********************************************************************************/


#include <linux/string.h>
#include <linux/kernel.h>
#include "al.h"
#include "al_print_log.h"
#include "access_point.h"
#include "al_impl_context.h"
#include "al_conf.h"
#include "sip_conf.h"
#include "mesh_defs.h"
#include "mesh_table.h"
#include "mesh.h"
#include "mesh_globals.h"
#include "mesh_errors.h"
#include "imcp_snip.h"
#include "mesh_imcp.h"
#include "mesh_intervals.h"
#include "mesh_monitor.h"
#include "mesh_on_start.h"
#include "mesh_ng.h"
#include "mesh_ng_scan.h"
#include "mesh_ng_core.h"
#include "mesh_ng_fsm.h"
#include "mesh_location_info.h"

AL_DECLARE_GLOBAL(mesh_hardware_config_t*			mesh_hardware_info);
AL_DECLARE_GLOBAL(mesh_config_info_t*				mesh_config);
AL_DECLARE_GLOBAL(mesh_dot11e_category_t*			mesh_dot11e_category_config);
AL_DECLARE_GLOBAL(int								scanning_current_phy_id);
AL_DECLARE_GLOBAL(int								mesh_scan_wait_objection_event);
AL_DECLARE_GLOBAL(int								mesh_scan_wait_unlock_event);
AL_DECLARE_GLOBAL(mesh_acl_config_t*				mesh_acl_config);

#include "mesh_on_start_if_init.c"

static int _parent_enum_func(AL_CONTEXT_PARAM_DECL void* enum_param,parent_t* parent)
{
	al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"MESH_NG :\t"AL_NET_ADDR_STR" CH=%d SG=%d SC=%d %s %s %s %s %s",
				 AL_NET_ADDR_TO_STR(&parent->bssid),
						parent->channel,
						parent->signal,
						parent->score,
						parent->flags & MESH_PARENT_FLAG_PREFERRED ? "PREF" : "",
						parent->flags & MESH_PARENT_FLAG_MOBILE ? "MOB" : "",
						parent->flags & MESH_PARENT_FLAG_CHILD ? "CHL" : "",
						parent->flags & MESH_PARENT_FLAG_DISABLED ? "DIS" : "",
						parent->flags & MESH_PARENT_FLAG_QUESTION ? "QUE" : "");

	return 0;
}


int find_parent_for_wireless_ds(AL_CONTEXT_PARAM_DECL_SINGLE)
{
	int		count;

	if(mesh_state == _MESH_STATE_STOPPPING)
			return RETURN_ERROR;
	else {
		mesh_state = _MESH_STATE_SEEKING_INITIAL_ASSOCIATION;
	}

	mesh_ng_clear_parents(AL_CONTEXT_SINGLE);

	count	= mesh_ng_scan_parents(AL_CONTEXT MESH_MONITOR_SCAN_MODE_DS_NET_IF);

	if(count <= 0)
		return RETURN_ERROR;

	mesh_state = _MESH_STATE_SEEKING_FINAL_ASSOCIATION;
		
	/* update the scaned ap list with path cost sorting */

	mesh_ng_evaluate_list(AL_CONTEXT_SINGLE);
		
	al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"MESH_NG : === PARENT LIST BEGIN ====");

	mesh_ng_enumerate_parents(AL_CONTEXT MESH_NG_ENUMERATE_PARENTS_TYPE_ALL,NULL,_parent_enum_func);

	al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"MESH_NG : === PARENT LIST END ====");

	/* associate with best parent */

	if(mesh_ng_core_join_best_parent(AL_CONTEXT_SINGLE)) {
		AL_PRINT_LOG_ERROR_0("MESH_NG	: Failed to get a best Parent");
		return RETURN_ERROR_NO_WIRELESS_PARENT_FOUND;
	}

	return RETURN_SUCCESS;

}

static int mesh_on_start_initialize_ds_interface(AL_CONTEXT_PARAM_DECL_SINGLE)
{
#define _SCAN_TIMEOUT	120000

	al_notify_message_t			message;
	al_802_11_operations_t*		operations;
	al_u64_t					begin_time;
	al_u64_t					tick;

	AL_PRINT_LOG_FLOW_0( "MESH_AP	: mesh_on_start_initialize_ds_interface()");

	/* loop here untill we find a working DS */

	begin_time	= al_get_tick_count(AL_CONTEXT_SINGLE);

	while(1) {
		
		if(mesh_state == _MESH_STATE_STOPPPING)
			return RETURN_ERROR;
		
		/* got DS interface - notify to application or hardware */
		message.message_type		= AL_NOTIFY_MESSAGE_TYPE_SET_INTERFACE_INFO;
		message.message_data		= mesh_hardware_info->ds_net_if;
		al_notify_message(AL_CONTEXT &message);

#ifdef _AL_ROVER_
		if(mesh_flags)
			mesh_add_mesh_name_entry(AL_CONTEXT_SINGLE);
#endif

		/*
		 * if ds is 802.3 then its working (state already checked) 
		 * if ds is 802.11 then must find suitable parent for him
		 */
		if(AL_NET_IF_IS_ETHERNET(mesh_hardware_info->ds_net_if)) {	
			mesh_mode		= _MESH_AP_ROOT;
			func_mode		= _FUNC_MODE_FFR;
			current_parent	= &dummy_lfr_parent;
			return RETURN_SUCCESS;
		}

		if(_DISJOINT_ADHOC_MODE) {
			func_mode	= _FUNC_MODE_LFN;
		} else {
			func_mode	= _FUNC_MODE_FFN;
		}

		mesh_imcp_send_packet_mesh_init_status(AL_CONTEXT "802.11(Wireless) DS found..");

		/* In the startup ds net if is used for scanning */

		mesh_hardware_info->monitor_type	= MONITOR_TYPE_ABSENT;
		mesh_hardware_info->monitor_net_if	= mesh_hardware_info->ds_net_if;
		
		operations	= mesh_hardware_info->ds_net_if->get_extended_operations(mesh_hardware_info->ds_net_if);
		operations->enable_ds_verification_opertions(mesh_hardware_info->ds_net_if,0);
	
		prescan_setup_wms(AL_CONTEXT 1);

		if(!find_parent_for_wireless_ds(AL_CONTEXT_SINGLE)) {

			if(current_parent->flags & MESH_PARENT_FLAG_LIMITED)
				func_mode	= _FUNC_MODE_LFN;
			else
				func_mode	= _FUNC_MODE_FFN;

			mesh_mode = _MESH_AP_RELAY;

			return RETURN_SUCCESS;
		}

		if(_DISJOINT_ADHOC_MODE && _DISJOINT_ADHOC_MODE_INFRA_BEGIN) {

			/**
			 * In disjoint adhoc mode if the INFRA BEGIN flag is set
			 * we stop our parent search and become a LFR after 
			 * _SCAN_TIMEOUT amount of time.
			 */

			tick	= al_get_tick_count(AL_CONTEXT_SINGLE);

			if(tick - begin_time > _SCAN_TIMEOUT) {
				return RETURN_ERROR;
			}
		}
	}
	
#undef 	_SCAN_TIMEOUT
}

#include "mesh_on_start_dca.c"

static int mesh_on_start_get_operating_channel(AL_CONTEXT_PARAM_DECL mesh_conf_if_info_t* wm_conf)
{
	int								channel;
	int								ret;

	AL_ASSERT("MESH_AP	: mesh_on_start_get_operating_channel", wm_conf != NULL);

	scan_state				= _CHANNEL_SCAN_STATE_START;
	scanning_current_phy_id	= wm_conf->phy_sub_type;

	/* check for dynamic channel allocation for each wm */

	if(wm_conf->dy_channel_alloc  == 0) {
		if(_DFS_REQUIRED) {
			return -1;
		} else {
			return wm_conf->config_channel;
		}
	} else if(wm_conf->dy_channel_alloc == 1
		   && wm_conf->dca_list_count == 0) {
		return -1;
	}

	ret	 = _do_dca_lock(AL_CONTEXT wm_conf);

	if(ret == RETURN_ERROR)
		return ret;

	channel	= _dca1(AL_CONTEXT wm_conf,-1);
	
	scan_state = _CHANNEL_SCAN_STATE_START;
	mesh_imcp_send_packet_channel_scan_unlock(AL_CONTEXT_SINGLE);

	return channel;	
}

static AL_INLINE void _set_regular_channel_power(al_net_if_t* net_if, al_802_11_operations_t* ex_op, int channel,int power)
{
	al_802_11_tx_power_info_t	power_info;

	power_info.flags	= AL_802_11_TXPOW_PERC;
	power_info.power	= power;

	ex_op->set_mode(net_if, AL_802_11_MODE_MASTER,0);
	_set_mesh_init_status_essid(net_if,ex_op);
	ex_op->set_beacon_interval(net_if, 100);
	ex_op->set_tx_power(net_if,&power_info);
	ex_op->set_channel(net_if,channel);
}

static AL_INLINE void _reg_domain_set_wm_channel_power(mesh_conf_if_info_t* wm_conf, al_net_if_t* net_if, al_802_11_operations_t* ex_op, int channel)
{
	char						vendor_info[256];
	char						vendor_info_len;

	if(channel == -1) {
		ex_op->set_mode(net_if, AL_802_11_MODE_INFRA,0);
		AL_ATOMIC_SET(net_if->buffering_state,AL_NET_IF_BUFFERING_STATE_DROP);
		return;
	}

	_set_regular_channel_power(net_if,ex_op, channel,wm_conf->txpower);

	if(wm_conf->service != AL_CONF_IF_SERVICE_CLIENT_ONLY) {
		vendor_info_len	= mesh_imcp_get_vendor_info_buffer(AL_CONTEXT vendor_info);
		al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,
					 "MESH_AP: Setting vendor information of length %d for %s",
					 vendor_info_len,
					 net_if->name);
		ex_op->set_beacon_vendor_info(net_if, vendor_info,vendor_info_len);				
	} else {
		ex_op->set_beacon_vendor_info(net_if, NULL,0);
	}
}

static int mesh_on_start_get_wm_interfaces_info(AL_CONTEXT_PARAM_DECL_SINGLE )
{
	unsigned int					i;
	al_notify_message_t				message;
	al_802_11_operations_t*			ex_op;

	AL_PRINT_LOG_FLOW_0( "MESH_AP	: mesh_on_start_get_wm_interfaces_info()");

	if(mesh_config->dy_channel_alloc  == 1) {
		mesh_state = _MESH_STATE_DYNAMIC_CHANNEL_SCAN;
		AL_PRINT_LOG_MSG_0("MESH_AP	: Starting Mesh");
		message.message_type		= AL_NOTIFY_MESSAGE_TYPE_SET_STATE;
		message.message_data		= (void*)'B';
		al_notify_message(AL_CONTEXT &message);
	}

	for(i = 0; i < mesh_hardware_info->wm_net_if_count; i++) {
		
		if(mesh_config->dy_channel_alloc  == 0 || AL_NET_IF_IS_ETHERNET(mesh_hardware_info->wms_conf[i]->net_if)) {
			mesh_hardware_info->wms_conf[i]->operating_channel = mesh_hardware_info->wms_conf[i]->config_channel;
		} else {
			mesh_hardware_info->wms_conf[i]->operating_channel = mesh_on_start_get_operating_channel(AL_CONTEXT mesh_hardware_info->wms_conf[i]);
		}

		ex_op = mesh_hardware_info->wms_conf[i]->net_if->get_extended_operations(mesh_hardware_info->wms_conf[i]->net_if);

		if(ex_op != NULL) {
			_reg_domain_set_wm_channel_power(mesh_hardware_info->wms_conf[i],
											 mesh_hardware_info->wms_conf[i]->net_if,
				                             ex_op,
											 mesh_hardware_info->wms_conf[i]->operating_channel);
		}

		mesh_hardware_info->wms_conf[i]->radar_found	= 0;

	}

	return RETURN_SUCCESS;
}

int mesh_on_start(AL_CONTEXT_PARAM_DECL access_point_config_t* config, access_point_dot11e_category_t* dot11e_config)
{
	int							i;
	al_dot11e_category_info_t*	ap_dot11e_info;
	al_dot11e_category_info_t*	mesh_dot11e_info;
	al_802_11_operations_t*		ex_op;
	unsigned int				temp1;
	unsigned int				temp2;

	AL_ASSERT("MESH_AP	: mesh_on_start", config != NULL);

	mesh_on_start_if_init();

	/* find DS interface first */

	while(mesh_on_start_find_ds(AL_CONTEXT_SINGLE) != RETURN_SUCCESS) {
		if(mesh_state == _MESH_STATE_STOPPPING)
			return RETURN_ERROR;
		al_thread_sleep(AL_CONTEXT 500);
	}

	/* get wm count and interfaces */
	
	mesh_on_start_find_wms(AL_CONTEXT_SINGLE);

	/* update ds interface parameters */
	config->ds_net_if_info.net_if		= mesh_hardware_info->ds_net_if;
	config->ds_net_if_info.ack_timeout  = mesh_hardware_info->ds_if_ack_timeout;  
	config->ds_net_if_info.txpower		= mesh_hardware_info->ds_if_tx_power;  
	config->ds_net_if_info.txrate 		= mesh_hardware_info->ds_if_tx_rate;  
	
	/**
	 * Copy ESSID for DS-NETIF for use with Ethernet and allowed VLANs
	 */

	strcpy(config->ds_net_if_info.essid,mesh_hardware_info->ds_conf->essid);
	

	/* update wm interface parameters */
	strcpy(config->essid, mesh_config->essid);

	/* New generation configuration parameters begin */
	
	config->round_robin_beacon_count	= mesh_config->round_robin_beacon_count;
	
	/* New generation configuration parameters end */


	memcpy(&temp1,mesh_hardware_info->ds_net_if->config.hw_addr.bytes,4);
	memcpy(&temp2,mesh_hardware_info->ds_net_if->config.hw_addr.bytes + 2,4);

	al_cryptographic_random_bytes(AL_CONTEXT (char*)&mesh_config->dhcp_r_value,2);

	mesh_config->dhcp_r_value		*= temp1;
	mesh_config->dhcp_r_value		*= temp2;
	mesh_config->dhcp_r_value		&= 0x7FFF;

	config->criteria_root			= mesh_config->criteria_root;

	config->beacon_interval			= mesh_config->beacon_int;
	config->rts_threshold			= mesh_config->rts_th;
	config->frag_threshold			= mesh_config->frag_th;
	config->stay_awake_count		= mesh_config->stay_awake_count;
	config->bridge_ageing_time		= mesh_config->bridge_ageing_time;
	config->mesh_imcp_key_length	= mesh_config->mesh_imcp_key_length;
	memcpy(config->mesh_imcp_key,mesh_config->mesh_imcp_key,config->mesh_imcp_key_length);
	
	config->reg_domain				= mesh_config->reg_domain;
	config->country_code			= mesh_config->country_code;
	config->igmp_support			= _IGMP_SUPPORT;
	config->dhcp_support			= _DHCP_SUPPORT;

	config->dhcp_info.net_id		= mesh_config->dhcp_info.dhcp_net_id[0];

	if( mesh_config->dhcp_info.dhcp_mode == AL_CONF_DHCP_MODE_RANDOM) {
		config->dhcp_info.host_id_1		= (mesh_config->dhcp_r_value) >> 8;
		config->dhcp_info.host_id_2		= (mesh_config->dhcp_r_value) & 0xFF;
	} else {
		config->dhcp_info.host_id_1		= mesh_config->dhcp_info.dhcp_net_id[1];
		config->dhcp_info.host_id_2		= mesh_config->dhcp_info.dhcp_net_id[2];
	}

	config->dhcp_info.lease_timeout		= mesh_config->dhcp_info.dhcp_lease_time;
	memcpy(config->dhcp_info.subnet_mask,mesh_config->dhcp_info.dhcp_mask,4);
	memcpy(config->dhcp_info.gateway_ip,mesh_config->dhcp_info.dhcp_gateway,4);
	memcpy(config->dhcp_info.dns_ip,mesh_config->dhcp_info.dhcp_dns,4);
	
	memcpy(&config->sip_info, &mesh_config->sip_info, sizeof(sip_conf_info_t));

	config->options = mesh_config->options;

	config->wm_net_if_count		= mesh_hardware_info->wm_net_if_count;
	config->wm_net_if_info		= (access_point_netif_config_info_t*)al_heap_alloc(AL_CONTEXT config->wm_net_if_count * sizeof(access_point_netif_config_info_t) AL_HEAP_DEBUG_PARAM);

	for(i = 0; i < config->wm_net_if_count; i++) {

		config->wm_net_if_info[i].net_if				= mesh_hardware_info->wms_conf[i]->net_if;
		strcpy(config->wm_net_if_info[i].essid,mesh_hardware_info->wms_conf[i]->essid);
		config->wm_net_if_info[i].rts_threshold			= mesh_hardware_info->wms_conf[i]->rts_th;
		config->wm_net_if_info[i].frag_threshold		= mesh_hardware_info->wms_conf[i]->frag_th;
		config->wm_net_if_info[i].beacon_interval 		= mesh_hardware_info->wms_conf[i]->beacon_int;
		config->wm_net_if_info[i].bonding				= mesh_hardware_info->wms_conf[i]->bonding;
		config->wm_net_if_info[i].phy_type				= mesh_hardware_info->wms_conf[i]->phy_sub_type;
		config->wm_net_if_info[i].txpower				= mesh_hardware_info->wms_conf[i]->txpower;
		config->wm_net_if_info[i].txrate				= mesh_hardware_info->wms_conf[i]->txrate;
		config->wm_net_if_info[i].service				= mesh_hardware_info->wms_conf[i]->service;
		config->wm_net_if_info[i].channel				= mesh_hardware_info->wms_conf[i]->operating_channel;
		config->wm_net_if_info[i].preamble_type			= mesh_hardware_info->wms_conf[i]->preamble_type;
		config->wm_net_if_info[i].slot_time_type		= mesh_hardware_info->wms_conf[i]->slot_time_type;
		config->wm_net_if_info[i].long_slot_time_count	= 0;
		
		config->wm_net_if_info[i].long_preamble_count 	= 0;		
		
		config->wm_net_if_info[i].ack_timeout			= mesh_hardware_info->wms_conf[i]->ack_timeout;  
		config->wm_net_if_info[i].hide_ssid				= mesh_hardware_info->wms_conf[i]->hide_ssid;

		memcpy(&config->wm_net_if_info[i].security_info.dot11,&mesh_hardware_info->wms_conf[i]->security_info,sizeof(al_802_11_security_info_t));
		memcpy(&config->wm_net_if_info[i].security_info.dot1x_backend_addr,&mesh_hardware_info->wms_conf[i]->dot1x_backend_addr,sizeof(al_ip_addr_t));
		strcpy(config->wm_net_if_info[i].security_info.dot1x_backend_secret,mesh_hardware_info->wms_conf[i]->dot1x_backend_secret);
		config->wm_net_if_info[i].security_info.dot1x_backend_port			= mesh_hardware_info->wms_conf[i]->dot1x_backend_port;
		config->wm_net_if_info[i].security_info.group_key_renewal_period	= mesh_hardware_info->wms_conf[i]->group_key_renewal_period;
		config->wm_net_if_info[i].security_info.radius_based_vlan			= mesh_hardware_info->wms_conf[i]->radius_based_vlan;

		config->wm_net_if_info[i].net_if->config.ext_config_info	= (void*)&config->wm_net_if_info[i];
		
		config->wm_net_if_info[i].dot11e_enabled					= mesh_hardware_info->wms_conf[i]->dot11e_enabled;
		config->wm_net_if_info[i].dot11e_category					= mesh_hardware_info->wms_conf[i]->dot11e_category;		
	}

	if(_DISJOINT_ADHOC_MODE) {

		if(_DISJOINT_ADHOC_MODE_INFRA_BEGIN) {

			/**
			 * In this mode we try to start off as a regular
			 * infrastructure node. If we can't find a parent
			 * we fallback to full ad-hoc mode.
			 */

			if(mesh_on_start_initialize_ds_interface(AL_CONTEXT_SINGLE) == RETURN_SUCCESS) {

				/**
				 * func_mode automatically set to FFN, carry on with regular DCA
				 */

				mesh_on_start_get_wm_interfaces_info(AL_CONTEXT_SINGLE);


			} else {

				/**
				 * Turn of the _DISJOINT_ADHOC_MODE_INFRA_BEGIN flag
				 */

				mesh_config->hope_cost	&= ~0x08;

				goto _lfr_fallback;
			}

		} else {

_lfr_fallback:

			if(AL_NET_IF_IS_ETHERNET(_DS_NET_IF)) {	
				mesh_mode	= _MESH_AP_ROOT;
				func_mode	= _FUNC_MODE_FFR;
				current_parent	= &dummy_lfr_parent;
			} else {
				
				mesh_mode		= _MESH_AP_RELAY;
				func_mode		= _FUNC_MODE_LFR;
				current_parent	= &dummy_lfr_parent;
				AL_ATOMIC_SET(_DS_NET_IF->buffering_state,AL_NET_IF_BUFFERING_STATE_DROP);

				if(mesh_config->use_virt_if) {

					int channel;

					channel = _DS_CONF->dca_list_count != 0 ? _DS_CONF->dca_list[0] : _DS_CONF->config_channel;

					current_parent->channel = channel;
					
					ex_op = _DS_NET_IF->get_extended_operations(_DS_NET_IF);

					if(ex_op != NULL) {
						al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"MESH_AP: Setting LFR Uplink/Downlink channel to %d\n",channel);
						ex_op->set_channel(_DS_NET_IF, channel);
					}
				}

			}

			/**
			 * Initialize each wireless downlink to be in the channel for hop-level 0
			 */

			for(i = 0; i < mesh_hardware_info->wm_net_if_count; i++) {

				if(mesh_config->dy_channel_alloc  == 0
				|| AL_NET_IF_IS_ETHERNET(_WMS_NET_IF(i)) 
				|| _WMS_CONF(i)->dca_list_count == 0) {
					_WMS_CONF(i)->operating_channel	= _WMS_CONF(i)->config_channel;
				} else if(AL_NET_IF_IS_WIRELESS(_WMS_NET_IF(i)) && _WMS_CONF(i)->dy_channel_alloc == 1) {
					_WMS_CONF(i)->operating_channel	= _WMS_CONF(i)->dca_list[0];
				}

				ex_op = mesh_hardware_info->wms_conf[i]->net_if->get_extended_operations(_WMS_NET_IF(i));

				if(ex_op != NULL) {
					_reg_domain_set_wm_channel_power(_WMS_CONF(i),
													 _WMS_NET_IF(i),
													 ex_op,
													 _WMS_CONF(i)->operating_channel);
				}

				_WMS_CONF(i)->radar_found	= 0;

			}
		}

	} else {

		/* Init ds interface */

		if(mesh_on_start_initialize_ds_interface(AL_CONTEXT_SINGLE) != RETURN_SUCCESS) {
			return RETURN_ERROR;
		}

		mesh_on_start_get_wm_interfaces_info(AL_CONTEXT_SINGLE);
	}

	mesh_on_start_get_mon_interfaces_info(AL_CONTEXT_SINGLE);

	for(i = 0; i < config->wm_net_if_count; i++) {
		config->wm_net_if_info[i].channel	= _WMS_CONF(i)->operating_channel;
	}


	for(i = 0; i < mesh_config->vlan_count;i++) {
		access_point_add_vlan_info(AL_CONTEXT &mesh_config->vlan_info[i]);
	}

	access_point_set_acl_list(AL_CONTEXT mesh_acl_config->count,mesh_acl_config->entries);

	/** 
	 *	Setting 802.11e Category Info of Accesspoint from Mesh 
	 */

	mesh_dot11e_info	= (al_dot11e_category_info_t*)mesh_dot11e_category_config;
	ap_dot11e_info		= (al_dot11e_category_info_t*)dot11e_config;

	ap_dot11e_info->count			= mesh_dot11e_info->count;
	ap_dot11e_info->category_info	= (al_dot11e_category_details_t*)al_heap_alloc(AL_CONTEXT sizeof(al_dot11e_category_details_t) * ap_dot11e_info->count AL_HEAP_DEBUG_PARAM);	
	
	for(i = 0; i < ap_dot11e_info->count; i++) {
		ap_dot11e_info->category_info[i].category			= mesh_dot11e_info->category_info[i].category;
		ap_dot11e_info->category_info[i].burst_time			= mesh_dot11e_info->category_info[i].burst_time;
		ap_dot11e_info->category_info[i].acwmin				= mesh_dot11e_info->category_info[i].acwmin;
		ap_dot11e_info->category_info[i].acwmax				= mesh_dot11e_info->category_info[i].acwmax;
		ap_dot11e_info->category_info[i].aifsn				= mesh_dot11e_info->category_info[i].aifsn;
		ap_dot11e_info->category_info[i].disable_backoff	= mesh_dot11e_info->category_info[i].disable_backoff;
	}
	
	if(_PROBE_REQUEST_LOCATION_SUPPORT) {
		mesh_location_initialize(AL_CONTEXT_SINGLE);
	}

	mesh_ng_fsm_send_event(AL_CONTEXT MESH_NG_FSM_EVENT_START,NULL);
	mesh_ng_fsm_process_events(AL_CONTEXT_SINGLE);

#ifdef FIPS_1402_COMPLIANT
	
	if(_FIPS_ENABLED) {
		SET_MESH_FLAG(MESH_FLAG_FIPS_COMPLIANT);
	}

#endif

	return RETURN_SUCCESS;
}

#ifndef _AL_ROVER_

static AL_INLINE void _prescan_set_regular_channel_power(al_net_if_t* net_if, al_802_11_operations_t* ex_op, int power)
{
	al_802_11_tx_power_info_t		power_info;
	int								j;

	power_info.flags	= AL_802_11_TXPOW_PERC;
	power_info.power	= power;

	ex_op->set_tx_power(net_if,&power_info);

	/**
	 * During MESH_INIT_STATUS_SSID we set the channel to
	 * either the manual channel, or the first channel in
	 * the DCA list, only if operating_channel is 0
	 */

	for(j = 0; j < mesh_config->if_count; j++) {
		if(!strcmp(mesh_config->if_info[j].name,net_if->name)
		&& mesh_config->if_info[j].operating_channel == 0) {
			if(mesh_config->if_info[j].dy_channel_alloc) {
				ex_op->set_channel(net_if,mesh_config->if_info[j].dca_list[0]);
			} else {
				ex_op->set_channel(net_if,mesh_config->if_info[j].config_channel);
			}
		}
	}
}

int	prescan_setup_wms(AL_CONTEXT_PARAM_DECL int before_dfs)
{
	int								i;
	al_net_if_t*					net_if;
	char							vendor_info[256];
	char							vendor_info_len;
	al_802_11_operations_t*			ex_op;

	access_point_enable_wm_encryption(AL_CONTEXT 0);

	for(i = 0; i < mesh_hardware_info->wm_net_if_count; i++) {

		net_if	= mesh_hardware_info->wms_conf[i]->net_if;

		if(AL_NET_IF_IS_WIRELESS(net_if)) {

			ex_op = (al_802_11_operations_t*)net_if->get_extended_operations(net_if);
			
			if(ex_op != NULL) {

				if(before_dfs && _DFS_REQUIRED) {
					ex_op->set_mode(net_if, AL_802_11_MODE_INFRA,0);
					continue;
				} else {
					_prescan_set_regular_channel_power(net_if,ex_op,mesh_hardware_info->wms_conf[i]->txpower);
					ex_op->set_mode(net_if, AL_802_11_MODE_MASTER,0);
					_set_mesh_init_status_essid(net_if,ex_op);
					ex_op->set_beacon_interval(net_if, 100);
				}

				if(mesh_hardware_info->wms_conf[i]->service != AL_CONF_IF_SERVICE_CLIENT_ONLY) {

					vendor_info_len = mesh_imcp_get_vendor_info_buffer(AL_CONTEXT vendor_info);			

					ex_op->set_beacon_vendor_info(net_if, vendor_info,vendor_info_len);
					
					al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,
								 "MESH_AP: Setting vendor information of length %d for %s",
								 vendor_info_len,
								 mesh_hardware_info->wms_conf[i]->net_if->name);

				} else {
					ex_op->set_beacon_vendor_info(net_if,NULL,0);
				}
			}
		}
	}

	return 0;

}

int	postscan_setup_wms(AL_CONTEXT_PARAM_DECL_SINGLE)
{
	unsigned int					i;
	al_802_11_operations_t*			ex_op;
	al_net_if_t*					net_if;
	al_802_11_tx_power_info_t		power_info;

	for(i = 0; i < mesh_hardware_info->wm_net_if_count; i++) {

		net_if	= mesh_hardware_info->wms_conf[i]->net_if;

		if(!AL_NET_IF_IS_WIRELESS(net_if))
			continue;

		ex_op = (al_802_11_operations_t*)net_if->get_extended_operations(net_if);

		if(ex_op != NULL) {
			ex_op->set_mode(net_if, AL_802_11_MODE_MASTER,0);
			ex_op->set_essid(net_if, mesh_hardware_info->wms_conf[i]->essid);
			ex_op->set_hide_ssid(net_if,mesh_hardware_info->wms_conf[i]->hide_ssid);
			ex_op->set_beacon_interval(net_if, mesh_hardware_info->wms_conf[i]->beacon_int);
			power_info.flags	= AL_802_11_TXPOW_PERC;
			power_info.power	= mesh_hardware_info->wms_conf[i]->txpower;
			ex_op->set_tx_power(net_if,&power_info);
		}

	}

	access_point_enable_wm_encryption(AL_CONTEXT 1);

	return 0;
}

#else

int	prescan_setup_wms(AL_CONTEXT_PARAM_DECL int before_dfs)
{
	return 0;	
}

int	postscan_setup_wms(AL_CONTEXT_PARAM_DECL_SINGLE)
{
	return 0;
}

#endif


