
/********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : dot1x-impl.c
 * Comments : Meshap 802.1x IMPL
 * Created  : 3/24/2005
 * Author   : Sriram Dayanandan
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * -----------------------------------------------------------------------------
 * | No  |Date      |  Comment                                        | Author |
 * -----------------------------------------------------------------------------
 * |  5  |6/16/2008 | Lock primitives changed                         | Sriram |
 * -----------------------------------------------------------------------------
 * |  4  |7/17/2007 | DOT1X 2K Buffer Implemented                     | Sriram |
 * -----------------------------------------------------------------------------
 * |  3  |9/25/2006 | dot1x_eapol_tx_ex Implemented                   | Sriram |
 * -----------------------------------------------------------------------------
 * |  2  |10/5/2005 | Misc changes                                    | Sriram |
 * -----------------------------------------------------------------------------
 * |  1  |4/6/2005  | Misc changes                                    | Sriram |
 * -----------------------------------------------------------------------------
 * |  0  |3/24/2005 | Created.                                        | Sriram |
 * -----------------------------------------------------------------------------
 ********************************************************************************/

#include <generated/autoconf.h>
#include <linux/module.h>
#include <linux/init.h>
#include <linux/version.h>
#include <linux/utsname.h>
#include <linux/random.h>
#include <linux/netdevice.h>
#include <linux/inetdevice.h>
#include <linux/skbuff.h>

#include "dot1x.h"
#include "dot1x-impl.h"
#include "dot1x-priv.h"

#include "md5.h"
#include "hmac_md5.h"
#include "dot11i-defs.h"

#include "torna_types.h"
#include "torna_mac_hdr.h"
#include "torna_wlan.h"

#include "meshap.h"
#include "meshap_core.h"
#include "meshap_tddi.h"
#include "access_point.h"


struct _encryption_context {
	MD5_CTX		md5_ctx;
};

typedef struct _encryption_context _encryption_context_t;


void* dot1x_heap_alloc(unsigned int size)
{
	return kmalloc(size,GFP_ATOMIC);
}

void dot1x_heap_free(void* memblock)
{
	kfree(memblock);
}

unsigned char* dot1x_get_2k_buffer()
{
	return al_get_gen_2KB_buffer(AL_CONTEXT_SINGLE);
}

void dot1x_release_2k_buffer(unsigned char* buffer)
{
	al_release_gen_2KB_buffer(AL_CONTEXT buffer);
}

unsigned int dot1x_create_lock()
{
	_lock_impl_t*	lock;

	lock	= (_lock_impl_t*)kmalloc(sizeof(_lock_impl_t),GFP_ATOMIC);

	spin_lock_init(&lock->lock);

	return (unsigned int)lock;
}

void dot1x_destroy_lock(unsigned int lock)
{
	_lock_impl_t*	plock;

	plock	= (_lock_impl_t*)lock;

	kfree(plock);
}


int dot1x_print(int level,const char* format,...)
{
	va_list		args;
	char		buffer[1024];

	va_start(args,format);
	vsnprintf(buffer,sizeof(buffer),format,args);
	va_end(args);
	printk(KERN_INFO"%s",buffer);

	return 0;
}

unsigned long dot1x_timestamp()
{
	return jiffies;
}

int dot1x_get_hostname(char* buffer,int buffer_length)
{
	struct new_utsname* uname;

	uname = utsname();

	strcpy(buffer,uname->nodename);
	return 0;
}

void dot1x_crypt_initialize(dot1x_t* instance)
{
	dot1x_priv_t*			priv;
	_encryption_context_t*	ec;

	priv	= (dot1x_priv_t*)instance;
	ec		= (_encryption_context_t*)kmalloc(sizeof(_encryption_context_t),GFP_ATOMIC);

	memset(ec,0,sizeof(_encryption_context_t));

	priv->encryption_context	= ec;
}

void dot1x_crypt_uninitialize(dot1x_t* instance)
{
	dot1x_priv_t*			priv;
	
	priv	= (dot1x_priv_t*)instance;

	kfree(priv->encryption_context);
}

int dot1x_crypt_hmac_md5(dot1x_t* instance,unsigned char* packet,int length,unsigned char* key,int key_len,unsigned char* mauth)
{
	unsigned char*	key_temp;
	unsigned char	mauth_temp[16];

	key_temp	= (unsigned char*)kmalloc(key_len,GFP_ATOMIC);
	memcpy(key_temp,key,key_len);

	hmac_md5(packet,length,key_temp,key_len,mauth_temp);

	if(0) {
		int		i;
		for(i = 0; i < 16; i++) 
			mauth[i] = mauth_temp[16 - i];
	} else {
		memcpy(mauth,mauth_temp,16);
	}

	kfree(key_temp);

	return 0;
}

int dot1x_crypt_md5_digest_open(dot1x_t* instance)
{
	dot1x_priv_t*			priv;
	_encryption_context_t*	ec;

	priv	= (dot1x_priv_t*)instance;
	ec		= (_encryption_context_t*)priv->encryption_context;

	memset(&ec->md5_ctx,0,sizeof(MD5_CTX));

	MD5Init(&ec->md5_ctx);

	return 0;
}

int dot1x_crypt_md5_digest(dot1x_t* instance,unsigned char* buffer,int length)
{
	dot1x_priv_t*			priv;
	_encryption_context_t*	ec;

	priv	= (dot1x_priv_t*)instance;
	ec		= (_encryption_context_t*)priv->encryption_context;

	MD5Update(&ec->md5_ctx,buffer,length);

	return 0;
}

int dot1x_crypt_md5_digest_close(dot1x_t* instance,unsigned char* hash)
{
	dot1x_priv_t*			priv;
	_encryption_context_t*	ec;

	priv	= (dot1x_priv_t*)instance;
	ec		= (_encryption_context_t*)priv->encryption_context;

	MD5Final(hash,&ec->md5_ctx);

	return 0;
}

void dot1x_get_random_bytes(unsigned char* buffer, int length)
{
	get_random_bytes(buffer,length);
}

int dot1x_get_instance_addr(dot1x_t* instance,dot1x_port_t* port,unsigned char* addr)
{
	dot11i_authenticator_t*			authenticator;
	access_point_sta_info_t			sta;
	al_net_addr_t					net_addr;

	authenticator	= (dot11i_authenticator_t*)instance->instance_data;

	if(authenticator->net_if_config_info->net_if != NULL) {
		memcpy(addr,authenticator->net_if_config_info->net_if->config.hw_addr.bytes,6);
	} else {
		memset(&net_addr,0,sizeof(al_net_addr_t));
		memcpy(net_addr.bytes,port->address,6);
		net_addr.length	= 6;

		access_point_get_sta_info(AL_CONTEXT &net_addr,&sta);

		if(sta.net_if != NULL) {
			memcpy(addr,sta.net_if->config.hw_addr.bytes,6);
		} else {
			memset(addr,0,6);
		}
	}

	return 0;
}

int dot1x_get_instance_ip_addr(dot1x_t* instance,dot1x_port_t* port,unsigned char* addr)
{
	struct net_device*		dev;
	struct in_device*		in_dev;
	
#ifndef _MESHAP_NO_MIP_
	/**
	 * In this case we use the IP address assigned to the mip0 interface
	 */
	dev				= dev_get_by_name(&init_net, "mip0");
#else

	/**
	 * In this case we use the IP address assigned to the DS net_if
	 */
	core_net_if		=	(meshap_core_net_if_t*)meshap_tddi_get_ds_net_if();
	dev				=	core_net_if->dev;
#endif

	in_dev	= (struct in_device*)dev->ip_ptr;

	if(in_dev->ifa_list) {
		memcpy(addr,&in_dev->ifa_list->ifa_address,4);
	} else {
		memset(addr,0,4);
	}

#ifndef _MESHAP_NO_MIP_
	dev_put(dev);
#endif

	return 0;
}

unsigned int dot1x_get_port_aid(dot1x_port_t* port)
{
	access_point_sta_info_t			sta;
	al_net_addr_t					net_addr;

	memset(&net_addr,0,sizeof(al_net_addr_t));

	net_addr.length	= 6;
	memcpy(net_addr.bytes,port->address,6);

	access_point_get_sta_info(AL_CONTEXT &net_addr,&sta);

	return sta.aid;
}

int dot1x_get_instance_essid(dot1x_t* instance,char* essid)
{
	dot11i_authenticator_t*		authenticator;

	authenticator	= (dot11i_authenticator_t*)instance->instance_data;

	strcpy(essid,authenticator->net_if_config_info->essid);

	return 0;
}

int dot1x_get_port_connect_info(dot1x_port_t* port,char* info)
{
	access_point_sta_info_t			sta;
	al_net_addr_t					net_addr;

	access_point_get_sta_info(AL_CONTEXT &net_addr,&sta);

	if(sta.net_if != NULL) {
		if(AL_NET_IF_IS_ETHERNET(sta.net_if)) {
			strcpy(info,"CONNECT 100 Mbps");
		} else {
			strcpy(info,"CONNECT 100 Mbps");
		}
	} else {
		strcpy(info,"CONNECT 100 Mbps");
	}

	return 0;
}

int dot1x_eapol_tx_ex(dot1x_port_t* port,unsigned char* packet_data, int packet_length, int secure)
{
	struct	sk_buff*				skb;
	struct ethhdr*					eth_hdr;
	torna_mac_hdr_t*				mac_hdr;
	unsigned char*					p;
	dot11i_authenticator_t*			authenticator;
	al_net_if_t*					net_if;
	meshap_core_net_if_t*			core_net_if;
	access_point_sta_info_t			sta;
	al_net_addr_t					addr;

	authenticator	= (dot11i_authenticator_t*)port->port_data;
	net_if			= authenticator->net_if_config_info->net_if;
	
	memset(&addr,0,sizeof(addr));
	addr.length	= 6;
	memcpy(addr.bytes,port->address,6);
	memset(&sta,0,sizeof(access_point_sta_info_t));

	access_point_get_sta_info(AL_CONTEXT &addr,&sta);

	core_net_if		= (meshap_core_net_if_t*)net_if;

	skb		= dev_alloc_skb(2400);

	if(AL_NET_IF_IS_ETHERNET(net_if)) {
		eth_hdr			= (struct ethhdr*)skb_put(skb,ETH_HLEN);
		skb->mac_header	= (u8*)eth_hdr;
		memset(eth_hdr,0,sizeof(struct ethhdr));
		memcpy(eth_hdr->h_dest,port->address,ETH_ALEN);
		memcpy(eth_hdr->h_source,net_if->config.hw_addr.bytes,ETH_ALEN);
		eth_hdr->h_proto	= htons(AL_PACKET_TYPE_EAPOL);
	} else {

		mac_hdr		= (torna_mac_hdr_t*)skb_put(skb,sizeof(torna_mac_hdr_t));
		memset(mac_hdr,0,sizeof(torna_mac_hdr_t));

		skb->mac_header	= (u8*)mac_hdr;

		mac_hdr->signature[0]	= TORNA_MAC_HDR_SIGNATURE_1;
		mac_hdr->signature[1]	= TORNA_MAC_HDR_SIGNATURE_2;
		mac_hdr->type			= TORNA_MAC_HDR_TYPE_SK_BUFF;

		if(sta.b_client)
			mac_hdr->flags		|= TORNA_MAC_HDR_FLAG_802_11_B_RATES;

		mac_hdr->frame_control	= WLAN_FC_FROMDS;

		WLAN_FC_SET_TYPE(mac_hdr->frame_control,WLAN_FC_TYPE_DATA);
		WLAN_FC_SET_STYPE(mac_hdr->frame_control,WLAN_FC_STYPE_DATA);

		memcpy(mac_hdr->addresses[0],port->address,ETH_ALEN);
		memcpy(mac_hdr->addresses[1],net_if->config.hw_addr.bytes,ETH_ALEN);
		memcpy(mac_hdr->addresses[2],net_if->config.hw_addr.bytes,ETH_ALEN);

		memcpy(mac_hdr->ethernet.h_dest,port->address,ETH_ALEN);
		memcpy(mac_hdr->ethernet.h_source,net_if->config.hw_addr.bytes,ETH_ALEN);
		mac_hdr->ethernet.h_proto	= htons(AL_PACKET_TYPE_EAPOL);	

		if(secure) {
			mac_hdr->flags		|= TORNA_MAC_HDR_FLAG_ENCRYPTION;
			mac_hdr->key_index	= sta.key_index;
		}
	}

	p	= skb_put(skb,packet_length);
	memcpy(p,packet_data,packet_length);

	skb->dev	= core_net_if->dev;
	dev_queue_xmit(skb);

	return 0;
}

int dot1x_eapol_tx(dot1x_port_t* port,unsigned char* packet_data, int packet_length)
{
	return dot1x_eapol_tx_ex(port,packet_data,packet_length,0);
}


