/********************************************************************************
* MeshDynamics
* --------------
* File     : meshap_heap_proc.c
* Comments : Meshap heap info /proc entry
* Created  : 11/14/2006
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  0  |11/14/2006| Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

#ifdef _AL_HEAP_DEBUG

#include <generated/autoconf.h>
#include <linux/version.h>
#include <linux/module.h>
#include <linux/init.h>
#include <linux/if.h>
#include <linux/netdevice.h>
#include <linux/skbuff.h>
#include <linux/cache.h>
#include <linux/pci.h>
#include <linux/wireless.h>
#include <asm/uaccess.h>
#include <net/iw_handler.h>
#include <linux/delay.h>
#include <linux/seq_file.h>

#include "meshap.h"
#include "meshap_logger.h"
#include "al.h"
#include "al_802_11.h"
#include "meshap_proc.h"
#include "meshap_core.h"

//static int _proc_read_routine2(char *page, char **start, off_t off,int count, int *eof, void *data)
static int _proc_read_routine2(struct seq_file *m, void *v)
{
   /*char						*p;
    * meshap_heap_debug_info_t*	item;
    * unsigned int				flags;
    *
    * if(off != 0) {
    * eof	= 1;
    *      return 0;
    * }
    *
    * start		= page + off;
    * p			= page;
    *
    * p			+= sprintf(p,
    *                         "--------------------------------------------------------------------------\r\n"
    *                                         "FILENAME                                  |LINE  |ALLOC SIZE|MS AGO      |\r\n"
    *                                         "--------------------------------------------------------------------------\r\n");
    *
    *
    * al_disable_interrupts(flags);
    *
    * item	= meshap_core_globals.heap_debug_list;
    *
    * while(1) {
    *      if(item->next_list != NULL)
    *              item	= item->next_list;
    *      else
    *              break;
    * }
    *
    * while(item != NULL) {
    *      p			+= sprintf(p,
    *                                 "%42s|%06d|%010d|%012ld|\r\n",
    *                                                 item->file_name,
    *                                                 item->line_number,
    *                                                 item->size,
    *                                                 ((jiffies - item->timestamp) * 1000/HZ));
    *
    *      item	= item->prev_list;
    *
    * }
    *
    * p			+= sprintf(p,
    *                             "\r\n%42s|%06d|%010d|%012ld|\r\n",
    *                                         meshap_core_globals.last_freed_item->file_name,
    *                                         meshap_core_globals.last_freed_item->line_number,
    *                                         meshap_core_globals.last_freed_item->size,
    *                                         ((jiffies - meshap_core_globals.last_freed_item->timestamp) * 1000/HZ));
    *
    * al_enable_interrupts(flags);
    *
    * return (p - page);	*/


   meshap_heap_debug_info_t *item;
   unsigned int             flags;


   seq_printf(m,
              "--------------------------------------------------------------------------\r\n"
              "FILENAME                                  |LINE  |ALLOC SIZE|MS AGO      |\r\n"
              "--------------------------------------------------------------------------\r\n");


   al_spin_lock(core_gbl_splock, core_gbl_spvar);

   item = meshap_core_globals.heap_debug_list;

   while (1)
   {
      if (item->next_list != NULL)
      {
         item = item->next_list;
      }
      else
      {
         break;
      }
   }

   while (item != NULL)
   {
      seq_printf(m,
                 "%42s|%06d|%010d|%012ld|\r\n",
                 item->file_name,
                 item->line_number,
                 item->size,
                 ((jiffies - item->timestamp) * 1000 / HZ));

      item = item->prev_list;
   }

   seq_printf(m,
              "\r\n%42s|%06d|%010d|%012ld|\r\n",
              meshap_core_globals.last_freed_item->file_name,
              meshap_core_globals.last_freed_item->line_number,
              meshap_core_globals.last_freed_item->size,
              ((jiffies - meshap_core_globals.last_freed_item->timestamp) * 1000 / HZ));

   al_spin_unlock(core_gbl_splock, core_gbl_spvar);

   return 0;
}


static int _routine2_proc_open(struct inode *inode, struct file *file)
{
   return single_open(file, _proc_read_routine2, NULL);
}


static const struct file_operations _routine2_fops =
{
   .owner   = THIS_MODULE,
   .open    = _routine2_proc_open,
   .read    = seq_read,
   .llseek  = seq_lseek,
   .release = single_release,
};

//static int _proc_read_routine(char *page, char **start, off_t off,int count, int *eof, void *data)
static int _proc_read_routine(struct seq_file *m, void *v)
{
   /*char						*p;
    * unsigned int				flags;
    *
    * if(off != 0) {
    * eof	= 1;
    *      return 0;
    * }
    *
    * start		= page + off;
    * p			= page;
    *
    *
    *
    * p			+= sprintf(p,
    *                         "--------------------------------------------------------------------------\r\n"
    *                                         "ATOTAL  |AMAX    |TALLOC  |TFREE   |TDIFF   |ALLCNT  |FRECNT  |CDIFF |MISS\r\n"
    *                                         "--------------------------------------------------------------------------\r\n");
    *
    * p			+= sprintf(p,
    *                                         "%08d|%08d|%08d|%08d|%08d|%08d|%08d|%06d|%04d\r\n",
    *                                         meshap_core_globals.active_total,
    *                                         meshap_core_globals.active_max,
    *                                         meshap_core_globals.total_alloc,
    *                                         meshap_core_globals.total_freed,
    *                                         meshap_core_globals.total_alloc - meshap_core_globals.total_freed,
    *                                         meshap_core_globals.alloc_count,
    *                                         meshap_core_globals.free_count,
    *                                         meshap_core_globals.alloc_count - meshap_core_globals.free_count,
    *                                         meshap_core_globals.free_miss_count);
    *
    * al_enable_interrupts(flags);
    *
    * return (p - page);	*/



   unsigned int flags;

   al_spin_lock(core_gbl_splock, core_gbl_spvar);


   seq_printf(m,
              "--------------------------------------------------------------------------\r\n"
              "ATOTAL  |AMAX    |TALLOC  |TFREE   |TDIFF   |ALLCNT  |FRECNT  |CDIFF |MISS\r\n"
              "--------------------------------------------------------------------------\r\n");

   seq_printf(m,
              "%08d|%08d|%08d|%08d|%08d|%08d|%08d|%06d|%04d\r\n",
              meshap_core_globals.active_total,
              meshap_core_globals.active_max,
              meshap_core_globals.total_alloc,
              meshap_core_globals.total_freed,
              meshap_core_globals.total_alloc - meshap_core_globals.total_freed,
              meshap_core_globals.alloc_count,
              meshap_core_globals.free_count,
              meshap_core_globals.alloc_count - meshap_core_globals.free_count,
              meshap_core_globals.free_miss_count);

   al_spin_unlock(core_gbl_splock, core_gbl_spvar);

   return 0;
}


static int _routine_proc_open(struct inode *inode, struct file *file)
{
   return single_open(file, _proc_read_routine, NULL);
}


static const struct file_operations _routine_fops =
{
   .owner   = THIS_MODULE,
   .open    = _routine_proc_open,
   .read    = seq_read,
   .llseek  = seq_lseek,
   .release = single_release,
};

void meshap_heap_proc_initialize()
{
   meshap_proc_add_read_entry("s-heap", &_routine_fops, NULL);
   meshap_proc_add_read_entry("i-heap", &_routine2_fops, NULL);
}


#endif /* _AL_HEAP_DEBUG */
