/********************************************************************************
* MeshDynamics
* --------------
* File     : meshap_reboot_proc.h
* Comments : Reoobt information /proc entry
* Created  : 7/25/2007
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  0  |7/25/2007 | Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

#ifndef __MESHAP_REBOOT_PROC_H__
#define __MESHAP_REBOOT_PROC_H__

int meshap_reboot_proc_initialize(void);

#endif /*__MESHAP_REBOOT_PROC_H__*/
