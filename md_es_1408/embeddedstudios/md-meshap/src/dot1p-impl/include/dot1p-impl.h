/********************************************************************************
* MeshDynamics
* --------------
* File     : dot1p-impl.h
* Comments : 802.1p Implementation specific definitions
* Created  : 11/14/2005
* Author   : Bindu Parakala
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  0  |11/14/2005| Created                                         | Bindu  |
* -----------------------------------------------------------------------------
********************************************************************************/

#ifndef __DOT1P_IMPL_H__
#define __DOT1P_IMPL_H__

#include "dot1p-impl-inc.h"

struct _dot1p_lock_impl
{
   struct semaphore semahpore;
   int              max_count;                                          /** Max Count is not used in Linux */
   int              initial_count;
};

typedef struct _dot1p_lock_impl   _dot1p_lock_impl_t;

struct dot1p_thread_param
{
   const char *thread_name;
   int        thread_id;
   void       *param;
};

typedef struct dot1p_thread_param   dot1p_thread_param_t;

typedef int (*dot1p_thread_function_t)  (dot1p_thread_param_t *param);

typedef void (*dot1p_timer_t)   (unsigned long timer_data);

#define DOT1P_PRINT_LEVEL_INFORMATION    1
#define DOT1P_PRINT_LEVEL_FLOW           2
#define DOT1P_PRINT_LEVEL_ERROR          4
#define DOT1P_PRINT_LEVEL_DEBUG          8

void *dot1p_heap_alloc(unsigned int size);
void dot1p_heap_free(void *memblock);
unsigned int dot1p_create_lock(void);
void dot1p_destroy_lock(unsigned int lock);
unsigned int dot1p_lock(unsigned int lock);
void dot1p_unlock(unsigned int lock);



//int				dot1p_print						(int level,const char* format,...);
//unsigned long	dot1p_timestamp					(void);

#endif /*__DOT1P_IMPL_H__*/
