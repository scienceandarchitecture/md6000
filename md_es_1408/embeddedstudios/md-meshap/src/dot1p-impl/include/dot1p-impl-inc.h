/********************************************************************************
* MeshDynamics
* --------------
* File     : dot1p-impl-inc.h
* Comments : Windows 802.1x Implementation
* Created  : 11/14/2005
* Author   : Bindu Parakala
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  1  |11/14/2005| Created                                        | Bindu  |
* -----------------------------------------------------------------------------
********************************************************************************/

#ifndef __DOT1P_IMPL_INC_H__
#define __DOT1P_IMPL_INC_H__

//#include <stdlib.h>
#include <linux/module.h>
#include <linux/netdevice.h>
#include <linux/skbuff.h>
#include <linux/semaphore.h>

#endif /*__DOT1P_IMPL_INC_H__*/
