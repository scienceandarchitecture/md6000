/********************************************************************************
* MeshDynamics
* --------------
* File     : dot1p-impl.c
* Comments : Windows 802.1p IMPL
* Created  : 11/14/2005
* Author   : Bindu Parakala
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  0  |11/14/2005| Created                                         | Bindu  |
* -----------------------------------------------------------------------------
********************************************************************************/

#include "dot1p-impl.h"


void *dot1p_heap_alloc(unsigned int size)
{
   void *p;

   p = kmalloc(size, GFP_ATOMIC);
   if (p == NULL)
   {
      printk(KERN_INFO "al_heap_alloc : kmalloc returned NULL\n");
   }
   return p;
}


void dot1p_heap_free(void *memblock)
{
   kfree(memblock);
}


unsigned int dot1p_create_lock()
{
   _dot1p_lock_impl_t *lock;
   int                initial_count;
   int                max_count;

   initial_count = 1;
   max_count     = 1;

   lock = (_dot1p_lock_impl_t *)kmalloc(sizeof(_dot1p_lock_impl_t), GFP_ATOMIC);

   lock->initial_count = initial_count;
   lock->max_count     = max_count;

   sema_init(&lock->semahpore, initial_count);

   return (int)lock;
}


void dot1p_destroy_lock(unsigned int lock)
{
   _dot1p_lock_impl_t *plock;

   plock = (_dot1p_lock_impl_t *)lock;

   kfree(plock);
}


unsigned int dot1p_lock(unsigned int lock)
{
/*	_dot1p_lock_impl_t*	plock;
 *
 *      plock	= (_dot1p_lock_impl_t*)lock;
 *
 *      if(in_interrupt())
 *              return 0;
 *
 *      if(plock->owner_pid == current->pid
 *      && plock->lock_count > 0) {
 ++plock->lock_count;
 *              return 0;
 *      }
 *
 *      down(&plock->semahpore);
 *
 *      plock->owner_pid	= current->pid;
 *      plock->lock_count		= 1;
 */
   return 0;
}


void dot1p_unlock(unsigned int lock)
{
/*	_dot1p_lock_impl_t*	plock;
 *
 *      plock	= (_dot1p_lock_impl_t*)lock;
 *
 *      if(in_interrupt())
 *              return;
 *
 *      if(plock->owner_pid == current->pid
 *      && plock->lock_count > 0) {
 *              --plock->lock_count;
 *              if(plock->lock_count == 0) {
 *                      up(&plock->semahpore);
 *              }
 *      }
 */
}


/*int dot1p_print(int level,const char* format,...)
 * {
 *      va_list		args;
 *      char		buffer[1024];
 *
 *      va_start(args,format);
 *      vsnprintf(buffer,sizeof(buffer),format,args);
 *      va_end(args);
 *      printk(KERN_INFO"%s",buffer);
 *
 *      return 0;
 * }*/
