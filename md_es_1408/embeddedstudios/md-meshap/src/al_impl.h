/********************************************************************************
* MeshDynamics
* --------------
* File     : al_impl.h
* Comments : Shasta AL implementation
* Created  : 9/28/2004
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* | 12  |7/9/2007  | Reset generator architecture changed            | Sriram |
* -----------------------------------------------------------------------------
* | 11  |1/26/2007 | Added ATOMIC DEC                                | Sriram |
* -----------------------------------------------------------------------------
* | 10  |12/14/2006| Reset generator changes                         | Sriram |
* -----------------------------------------------------------------------------
* |  9  |10/24/2006| Spelling mistake corrected                      | Sriram |
* -----------------------------------------------------------------------------
* |  8  |3/13/2006 | Added atomic type definitions                   | Sriram |
* -----------------------------------------------------------------------------
* |  7  |9/22/2005 | Interrupt disable/enable semantics changed      | Sriram |
* -----------------------------------------------------------------------------
* |  6  |6/17/2005 | Misc changes                                    | Sriram |
* -----------------------------------------------------------------------------
* |  5  |6/12/2005 | Status message added                            | Sriram |
* -----------------------------------------------------------------------------
* |  4  |03/09/2005| al_impl_notify_message, al_impl_board_info added| Anand  |
* -----------------------------------------------------------------------------
* |  3  |12/26/2004| Added interrupt functions                       | Sriram |
* -----------------------------------------------------------------------------
* |  1  |12/9/2004 | Swap bug fixed                                  | Sriram |
* -----------------------------------------------------------------------------
* |  0  |9/28/2004 | Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

#ifdef __KERNEL__

//RAMESH16MIG commented
//#include <asm/system.h>
#include <asm/atomic.h>
#include <linux/spinlock.h>
#include <linux/semaphore.h>
#include <linux/percpu.h>
#include <asm/percpu.h>
#include <asm-generic/percpu.h>
#include <linux/percpu-defs.h>
#include <linux/sched.h>
#include <linux/version.h>

#include <asm/atomic.h>
#include <linux/mutex.h>
#include <linux/bug.h>

#if LINUX_VERSION_CODE == KERNEL_VERSION(4,4,0)
#define x86_ONLY
#endif

#endif

#ifndef __AL_IMPL_H__
#define __AL_IMPL_H__


typedef unsigned long long   al_impl_u64_t;
typedef long long            al_impl_s64_t;

#ifdef CODEC_SYS_BIG_ENDIAN // Big Endian

#define al_impl_le32_to_cpu(n)    al_swap32(n)
#define al_impl_le16_to_cpu(n)    al_swap16(n)
#define al_impl_be32_to_cpu(n)    (n)
#define al_impl_be16_to_cpu(n)    (n)

#define al_impl_cpu_to_le32(n)    al_swap32(n)
#define al_impl_cpu_to_le16(n)    al_swap16(n)
#define al_impl_cpu_to_be16(n)    (n)
#define al_impl_cpu_to_be32(n)    (n)

#else // Little Endian
#define al_impl_le32_to_cpu(n)    (n)
#define al_impl_le16_to_cpu(n)    (n)
#define al_impl_be32_to_cpu(n)    al_swap32(n)
#define al_impl_be16_to_cpu(n)    al_swap16(n)

#define al_impl_cpu_to_le32(n)    (n)
#define al_impl_cpu_to_le16(n)    (n)
#define al_impl_cpu_to_be16(n)    al_swap16(n)
#define al_impl_cpu_to_be32(n)    al_swap32(n)
#endif // CODEC_SYS_BIG_ENDIAN

#define AL_IMPL_SNPRINTF    snprintf

int snprintf(char *buf, size_t size, const char *fmt, ...);

#define AL_IMPL_NOTIFY_MESSAGE_TYPE_SET_INTERFACE_INFO    1
#define AL_IMPL_NOTIFY_MESSAGE_TYPE_SET_STATE             2
#define AL_IMPL_NOTIFY_MESSAGE_TYPE_STATUS                3

struct al_impl_notify_message
{
   int  message_type;
   void *message_data;
};

typedef struct al_impl_notify_message   al_impl_notify_message_t;

#define AL_IMPL_BOARD_INFO_MASK_TEMP       1
#define AL_IMPL_BOARD_INFO_MASK_VOLTAGE    2


struct al_impl_board_info
{
   short valid_values_mask;
   short temperature;
   short voltage;
};

typedef struct al_impl_board_info   al_impl_board_info_t;

int al_impl_get_board_info(al_impl_board_info_t *board_info);
int al_impl_notify_message(al_impl_notify_message_t *message);
void al_impl_enable_reset_generator(unsigned char enable_or_disable, int interval_in_millis);
void al_impl_strobe_reset_generator(void);

/*
#ifdef __KERNEL__
#define al_impl_disable_interrupts(flags)    do { preempt_disable(); } while (0)
#define al_impl_enable_interrupts(flags)     do { preempt_enable(); } while (0)
#endif
*/

#ifdef __KERNEL__

/* AL SPIN LOCK API's */
#define al_spinlock_t spinlock_t
#define AL_DECLARE_PER_CPU DECLARE_PER_CPU
#define AL_DEFINE_PER_CPU DEFINE_PER_CPU
#define al_spin_lock_init spin_lock_init

#define AL_SPIN_LOCK_UNLOCKED __SPIN_LOCK_UNLOCKED
#define al_spin_lock_irqsave spin_lock_irqsave
#define al_spin_unlock_irqrestore spin_unlock_irqrestore


/* AL MUTEX API's */
#define al_mutex mutex
#define al_mutex_init mutex_init
#define al_mutex_destroy mutex_destroy

extern al_spinlock_t gbl_splock;
AL_DECLARE_PER_CPU(int, gbl_flag);

extern struct al_mutex gbl_mutex;

extern int gbl_muvar, gbl_mupid;





#if 0

/********************************************Debug_lock_start************************************************************/

#ifdef x86_ONLY
#define al_impl_disable_interrupts(flags)   do {\
												preempt_disable();\
												if (!spin_is_locked(&gbl_splock) || !__this_cpu_read(gbl_flag)) {\
													spin_lock(&gbl_splock);\
													 this_cpu_write(gbl_flag, 1);\
												} else {\
													 this_cpu_inc(gbl_flag);\
           printk(KERN_EMERG "\n=================================================== BUG START =======================================================\n");\
           printk(KERN_EMERG "*** GBL_SPIN **** Spin LLLLLLL func =%s line=%d count =%d \n",__func__, __LINE__, __this_cpu_read(gbl_flag));\
		                                       dump_stack();\
												WARN_ON(1);\
           printk(KERN_EMERG "\n=================================================== BUG END =======================================================\n");\
												} \
													preempt_enable();\
                                             } while (0)
#else
#define al_impl_disable_interrupts(flags)   do {\
												preempt_disable();\
												if (!spin_is_locked(&gbl_splock) || !__get_cpu_var(gbl_flag)) {\
													spin_lock(&gbl_splock);\
													 __get_cpu_var(gbl_flag) = 1;\
												} else {\
													 __get_cpu_var(gbl_flag)++;\
           printk(KERN_EMERG "\n=================================================== BUG START =======================================================\n");\
           printk(KERN_EMERG "*** GBL_SPIN **** Spin LLLLLLL func =%s line=%d count =%d \n",__func__, __LINE__, __get_cpu_var(gbl_flag));\
		                                        dump_stack();\
												WARN_ON(1);\
           printk(KERN_EMERG "\n=================================================== BUG END =======================================================\n");\
												} \
													preempt_enable();\
                                             } while (0)

#endif

#ifdef x86_ONLY
#define al_impl_enable_interrupts(flags)    do {\
												preempt_disable();\
												if (__this_cpu_read(gbl_flag) == 1) {\
													__this_cpu_write(gbl_flag,0);\
													spin_unlock(&gbl_splock);\
												} else {\
													this_cpu_dec(gbl_flag);\
												}\
													preempt_enable();\
                                            } while (0)

#else
#define al_impl_enable_interrupts(flags)    do {\
												preempt_disable();\
												if (__get_cpu_var(gbl_flag) == 1) {\
													__get_cpu_var(gbl_flag) = 0;\
													spin_unlock(&gbl_splock);\
												} else {\
													__get_cpu_var(gbl_flag)--;\
												}\
													preempt_enable();\
                                            } while (0)
#endif

#define _al_impl_disable_interrupts(gbl_mutex, gbl_muvar, gbl_mupid)  do {\
														preempt_disable();\
														if (!mutex_is_locked(&gbl_mutex) || gbl_mupid != current->pid) {\
															preempt_enable();\
															mutex_lock(&gbl_mutex);\
															gbl_mupid = current->pid;\
															gbl_muvar = 1;\
														} else {\
															gbl_muvar++;\
           printk(KERN_EMERG "\n=================================================== BUG START =======================================================\n");\
           printk(KERN_EMERG "*** GBL_MUTE *** MMMMM MUTE LLLLLLL func =%s line=%d count =%d \n",__func__, __LINE__, gbl_muvar);\
		                                        dump_stack();\
												WARN_ON(1);\
           printk(KERN_EMERG "\n=================================================== BUG END =======================================================\n");\
															preempt_enable();\
														}\
													} while (0)

#define _al_impl_enable_interrupts(gbl_mutex, gbl_muvar, gbl_mupid)    do {\
														preempt_disable();\
														if (gbl_muvar == 1) {\
															gbl_muvar = 0;\
															gbl_mupid = 0;\
															mutex_unlock(&gbl_mutex);\
															preempt_enable();\
														} else {\
															gbl_muvar--;\
															preempt_enable();\
														}\
													} while (0)

/********* NEW SET OF GENERIC SPIN & MUTEX LOCKS *******/
#ifdef x86_ONLY
#define al_impl_spin_lock(gen_lock, cpu_var)   do {\
                                                preempt_disable();\
                                                if (!spin_is_locked(&gen_lock) || !__this_cpu_read(cpu_var)) {\
                                                    spin_lock_irqsave(&gen_lock, flags);\
                                                     this_cpu_write(cpu_var, 1);\
                                                } else {\
                                                     this_cpu_inc(cpu_var);\
           printk(KERN_EMERG "\n=================================================== BUG START =======================================================\n");\
           printk(KERN_EMERG "*** GEN_SPIN *** Spin LLLLLLL func =%s line=%d count =%d \n",__func__, __LINE__, __this_cpu_read(cpu_var));\
		                                        dump_stack();\
												WARN_ON(1);\
           printk(KERN_EMERG "\n=================================================== BUG END =======================================================\n");\
                                                } \
                                                    preempt_enable();\
                                             } while (0)
#else
#define al_impl_spin_lock(gen_lock, cpu_var)   do {\
                                                preempt_disable();\
                                                if (!spin_is_locked(&gen_lock) || !__get_cpu_var(cpu_var)) {\
                                                    spin_lock_irqsave(&gen_lock, flags);\
                                                     __get_cpu_var(cpu_var) = 1;\
                                                } else {\
                                                     __get_cpu_var(cpu_var)++;\
           printk(KERN_EMERG "\n=================================================== BUG START =======================================================\n");\
           printk(KERN_EMERG "*** GEN_SPIN *** Spin LLLLLLL func =%s line=%d count =%d \n",__func__, __LINE__, __get_cpu_var(cpu_var));\
		                                        dump_stack();\
												WARN_ON(1);\
                                                } \
                                                    preempt_enable();\
                                             } while (0)
#endif

#ifdef x86_ONLY
#define al_impl_spin_unlock(gen_lock, cpu_var)    do {\
                                                preempt_disable();\
                                                if (__this_cpu_read(cpu_var) == 1) {\
                                                    this_cpu_write(cpu_var, 0);\
                                                    spin_unlock_irqrestore(&gen_lock, flags);\
                                                } else {\
                                                    this_cpu_dec(cpu_var);\
                                                }\
                                                    preempt_enable();\
                                            } while (0)
#else
#define al_impl_spin_unlock(gen_lock, cpu_var)    do {\
                                                preempt_disable();\
                                                if (__get_cpu_var(cpu_var) == 1) {\
                                                    __get_cpu_var(cpu_var) = 0;\
                                                    spin_unlock_irqrestore(&gen_lock, flags);\
                                                } else {\
                                                    __get_cpu_var(cpu_var)--;\
                                                }\
                                                    preempt_enable();\
                                            } while (0)

#endif

#define al_impl_mutex_lock(gen_lock, cpu_var, p_pid)  do {\
														preempt_disable();\
														if (!mutex_is_locked(&gen_lock) || p_pid != current->pid) {\
															preempt_enable();\
															mutex_lock(&gen_lock);\
															p_pid = current->pid;\
															cpu_var = 1;\
														} else {\
															cpu_var++;\
           printk(KERN_EMERG "\n=================================================== BUG START =======================================================\n");\
           printk(KERN_EMERG "*** GEN_MUTE *** MMMMM MUTE LLLLLLL func =%s line=%d count =%d \n",__func__, __LINE__, cpu_var);\
		                                        dump_stack();\
												WARN_ON(1);\
           printk(KERN_EMERG "\n=================================================== BUG END =======================================================\n");\
															preempt_enable();\
														}\
													} while (0)

#define al_impl_mutex_unlock(gen_lock, cpu_var, p_pid)    do {\
														preempt_disable();\
														if (cpu_var == 1) {\
															cpu_var = 0;\
															p_pid = 0;\
															mutex_unlock(&gen_lock);\
															preempt_enable();\
														} else {\
															cpu_var--;\
															preempt_enable();\
														}\
													} while (0)


/*******************************************Debug_lock_ends*************************************************************/
#else

#define al_impl_disable_interrupts(flags) do {\
                                            spin_lock(&gbl_splock);\
                                          }while (0)

#define al_impl_enable_interrupts(flags)  do {\
													spin_unlock(&gbl_splock);\
                                          }while(0)
#define _al_impl_disable_interrupts(gbl_mutex, gbl_muvar, gbl_mupid)  do {\
															mutex_lock(&gbl_mutex);\
													} while (0)

#define _al_impl_enable_interrupts(gbl_mutex, gbl_muvar, gbl_mupid)    do {\
															mutex_unlock(&gbl_mutex);\
													} while (0)

/**************************************** NEW SET OF GENERIC SPIN & MUTEX LOCKS *************************************************/
#define al_impl_spin_lock(gen_lock, cpu_var)   do {\
                                                    spin_lock(&gen_lock);\
                                             } while (0)

#define al_impl_spin_unlock(gen_lock, cpu_var)    do {\
                                                    spin_unlock(&gen_lock);\
                                            } while (0)

#define al_impl_spin_lock_bh(gen_lock, cpu_var)   do {\
                                                    spin_lock_bh(&gen_lock);\
                                             } while (0)

#define al_impl_spin_unlock_bh(gen_lock, cpu_var)    do {\
                                                    spin_unlock_bh(&gen_lock);\
                                            } while (0)

#define al_impl_mutex_lock(gen_lock, cpu_var, p_pid)  do {\
															mutex_lock(&gen_lock);\
													} while (0)

#define al_impl_mutex_unlock(gen_lock, cpu_var, p_pid)    do {\
															mutex_unlock(&gen_lock);\
													} while (0)

#define al_impl_spin_lock_t(gen_lock)				spin_lock(gen_lock);
#define al_impl_spin_unlock_t(gen_lock)				spin_unlock(gen_lock);
#define al_impl_spin_lock_bh_t(gen_lock)			spin_lock_bh(gen_lock);
#define al_impl_spin_unlock_bh_t(gen_lock)			spin_unlock_bh(gen_lock);
#endif


/********************************************************************************************************/
#endif

#if __KERNEL__
#define al_impl_atomic_t    atomic_t
#define AL_IMPL_ATOMIC_SET(av, v)          atomic_set(&(av), (v))
#define AL_IMPL_ATOMIC_GET(av)             atomic_read(&(av))
#define AL_IMPL_ATOMIC_INC(av)             atomic_inc(&(av))
#define AL_IMPL_ATOMIC_DEC_AND_TEST(av)    atomic_dec_and_test(&(av))
#define AL_IMPL_ATOMIC_DEC(av)             atomic_dec(&(av))
#define AL_IMPL_ATOMIC_ADD(value, av)      atomic_add(value, &(av))
#define AL_IMPL_ATOMIC_SUB(value, av)      atomic_sub(value, &(av))
#else
#define al_impl_atomic_t    int
#define AL_IMPL_ATOMIC_SET(av, v)          do { (av) = (v); } while (0)
#define AL_IMPL_ATOMIC_GET(av)             (av)
#define AL_IMPL_ATOMIC_INC(av)             do { ++(av); } while (0)
#define AL_IMPL_ATOMIC_DEC_AND_TEST(av)    ((--(av)) == 0)
#define AL_IMPL_ATOMIC_DEC(av)             do { --(av); } while (0)
#endif
#endif /*__AL_IMPL_H__*/
