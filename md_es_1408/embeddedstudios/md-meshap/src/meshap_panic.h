/********************************************************************************
* MeshDynamics
* --------------
* File     : meshap_panic.h
* Comments : Meshap Kernel Panic and Trap handler
* Created  : 4/14/2005
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  0  |4/14/2005 | Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

#ifndef __MESHAP_PANIC_H__
#define __MESHAP_PANIC_H__

void meshap_panic_initialize(void);
void meshap_panic_uninitialize(void);

#endif /*__MESHAP_PANIC_H__*/
