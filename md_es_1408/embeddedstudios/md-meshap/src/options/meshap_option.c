/********************************************************************************
* MeshDynamics
* --------------
* File     : meshap_option.c
* Comments : Option encoding and decoding module
* Created  : 8/26/2008
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  0  |8/26/2008 | Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

#include <linux/string.h>
#include "al.h"
#include "codec.h"
#include "meshap_option.h"

#define _KEY_SIZE            16
#define _MAC_ADDRESS_SIZE    6

static unsigned char _meta_key_1[] =
{
   0, 5, 2, 3, 4, 1, 5, 0, 3, 2, 4, 1, 2, 3, 0, 5
};

static unsigned char _meta_key_2[] =
{
   5, 0, 3, 2, 1, 4, 2, 3, 0, 5, 1, 4, 3, 2, 5, 0
};

/**
 * The option key is the output of the following :
 *
 *	1) Plain Text :
 *
 *     0   1   2   3   4   5   6   7   8   9   10  11  12  13  14  15
 *   +---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+
 *   | CODE  |  UNIT MAC ADDRESS     | FROM DATE     | TO DATE       |
 *   +---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+
 *
 *  2) Aes-Key : Key(_meta_key_1) and Key (_meta_key_2)
 *
 *
 *  3) AES(AES(PT,Key(_meta_key_1)),Key(_meta_key_2))
 *
 */

#ifdef _MESHAP_OPTION_ENCODE

void meshap_option_encode(AL_CONTEXT_PARAM_DECL unsigned short option, unsigned char *mac_address, unsigned char *output_buffer)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
   unsigned char  aes_key_1[_KEY_SIZE];
   unsigned char  aes_key_2[_KEY_SIZE];
   unsigned char  phase_1_op[_KEY_SIZE];
   unsigned char  plain_text[_KEY_SIZE];
   unsigned short s;
   int            i;

   /**
    * Create the plain text
    */

   memset(plain_text, 0, _KEY_SIZE);

   s = al_cpu_to_le16(option);
   memcpy(plain_text, &s, 2);
   memcpy(plain_text + 2, mac_address, _MAC_ADDRESS_SIZE);

   /**
    * Create the AES-KEYs
    */

   for (i = 0; i < _KEY_SIZE; i++)
   {
      aes_key_1[i] = mac_address[_meta_key_1[i]];
      aes_key_2[i] = mac_address[_meta_key_2[i]];
   }

   /**
    * Encrypt phase 1
    */

   _aesencrypt(plain_text, _KEY_SIZE, aes_key_1, _KEY_SIZE, phase_1_op);

   /**
    * Encrypt phase 2
    */

   _aesencrypt(phase_1_op, _KEY_SIZE, aes_key_2, _KEY_SIZE, output_buffer);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
}
#endif

#ifdef _MESHAP_OPTION_DECODE

int meshap_option_decode(AL_CONTEXT_PARAM_DECL unsigned char *input, unsigned char *mac_address, unsigned short *option_out)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
   unsigned char  aes_key_1[_KEY_SIZE];
   unsigned char  aes_key_2[_KEY_SIZE];
   unsigned char  phase_1_op[_KEY_SIZE];
   unsigned char  plain_text[_KEY_SIZE];
   int            i;
   unsigned short s;

   /**
    * Create the AES-KEYs
    */

   for (i = 0; i < _KEY_SIZE; i++)
   {
      aes_key_1[i] = mac_address[_meta_key_1[i]];
      aes_key_2[i] = mac_address[_meta_key_2[i]];
   }

   /**
    * Decrypt phase 1
    */

   _aesdecrypt(input, _KEY_SIZE, aes_key_2, _KEY_SIZE, phase_1_op);

   /**
    * Decrypt phase 2
    */

   _aesdecrypt(phase_1_op, _KEY_SIZE, aes_key_1, _KEY_SIZE, plain_text);

   /**
    * Verify the plain text
    */

   memcpy(&s, plain_text, 2);
   *option_out = al_le16_to_cpu(s);

   if (memcmp(plain_text + 2, mac_address, _MAC_ADDRESS_SIZE))
   {
   al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR," MESHAP_OPTION_DECODE_ERR_MAC_ADDRESS :: func : %s LINE : %d\n", __func__,__LINE__);
      return MESHAP_OPTION_DECODE_ERR_MAC_ADDRESS;
   }

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
   return MESHAP_OPTION_DECODE_SUCCESS;
}
#endif
