/********************************************************************************
* MeshDynamics
* --------------
* File     : meshap_option.h
* Comments : Option encoding and decoding module
* Created  : 8/26/2008
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  0  |8/26/2008 | Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

#ifndef __MESHAP_OPTION_H__
#define __MESHAP_OPTION_H__

#ifdef _MESHAP_OPTION_ENCODE

void meshap_option_encode(AL_CONTEXT_PARAM_DECL unsigned short option, unsigned char *mac_address, unsigned char *output_buffer);
#endif

#ifdef _MESHAP_OPTION_DECODE

#define MESHAP_OPTION_DECODE_SUCCESS            0
#define MESHAP_OPTION_DECODE_ERR_MAC_ADDRESS    -1

int meshap_option_decode(AL_CONTEXT_PARAM_DECL unsigned char *input, unsigned char *mac_address_in, unsigned short *option_out);
#endif

#endif /*__MESHAP_OPTION_H__*/
