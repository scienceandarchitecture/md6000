/********************************************************************************
* MeshDynamics
* --------------
* File     : meshap_sdk_proc.h
* Comments : Meshap /proc FS SDK Info Content
* Created  : 01/23/2007
* Author   : Abhijit Ayarekar
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  0  |01/23/2007| Created.                                        | Abhijit|
* -----------------------------------------------------------------------------
********************************************************************************/

#ifndef __MESHAP_SDK_PROC_H__
#define __MESHAP_SDK_PROC_H__

int meshap_sdk_proc_initialize(void);

//void	meshap_sdk_proc_add_read_entry	(const char*  name,meshap_proc_read_routine_t routine,void* data);
void meshap_sdk_proc_add_read_entry(const char *name, struct file_operations *fops, void *data);


#endif /*__MESHAP_SDK_PROC_H__*/
