/********************************************************************************
* MeshDynamics
* --------------
* File     : al_impl.c
* Comments : Shasta AL implementation
* Created  : 03/09/2005
* Author   : Anand Bhange
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  5  |7/9/2007  | Reset generator architecture changed            | Sriram |
* -----------------------------------------------------------------------------
* |  4  |12/14/2006| Reset Generator Changes                         | Sriram |
* -----------------------------------------------------------------------------
* |  3  |11/20/2006| Added Voltage                                   | Sriram |
* -----------------------------------------------------------------------------
* |  2  |10/27/2006| Spelling mistake corrected                      | Sriram |
* -----------------------------------------------------------------------------
* |  1  |6/12/2005 | Status message implemented                      | Sriram |
* -----------------------------------------------------------------------------
* |  0  |03/09/2005| Created.                                        | Anand  |
* -----------------------------------------------------------------------------
********************************************************************************/

#include <generated/autoconf.h>

#include <linux/module.h>
#include <linux/init.h>
#include <linux/version.h>
#include <linux/netdevice.h>
#include <linux/skbuff.h>
#include <linux/wireless.h>
#include <linux/random.h>

#include "meshap.h"
#include "al.h"

int al_impl_get_board_info(al_impl_board_info_t *board_info)
{
   int temp;
   int voltage;

   board_info->valid_values_mask = AL_IMPL_BOARD_INFO_MASK_TEMP | AL_IMPL_BOARD_INFO_MASK_VOLTAGE;

   temp    = meshap_get_board_temp();
   voltage = meshap_get_board_voltage();

   board_info->temperature = temp;
   board_info->voltage     = voltage;

   return 0;
}


int al_impl_notify_message(al_impl_notify_message_t *message)
{
   int sign;

   if (message->message_type == AL_IMPL_NOTIFY_MESSAGE_TYPE_SET_STATE)
   {
      sign = (int)message->message_data;
      switch (sign)
      {
      case 'G':
         meshap_set_led_on();
         break;

      case 'R':
         meshap_set_led_off();
         break;

      case 'B':
         meshap_set_led_blink_fast();
         break;

      case 'Y':
         meshap_set_led_blink();
         break;
      }
   }
   else if (message->message_type == AL_IMPL_NOTIFY_MESSAGE_TYPE_STATUS)
   {
      meshap_set_led_blink_once();
   }
   return 0;
}


void al_impl_enable_reset_generator(unsigned char enable_or_disable, int interval_in_millis)
{
   meshap_enable_reset_generator(enable_or_disable, interval_in_millis);
}


void al_impl_strobe_reset_generator()
{
   meshap_strobe_reset_generator();
}
