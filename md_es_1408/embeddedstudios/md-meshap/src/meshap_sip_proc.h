/********************************************************************************
* MeshDynamics
* --------------
* File     : mesh_sip_proc.h
* Comments : SIP information proc entry Implementation
* Created  : 5/21/2008
* Author   : Abhijit Ayarekar
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  0  |5/21/2008 | Created.                                        |Abhijit |
* -----------------------------------------------------------------------------
********************************************************************************/

#ifndef __MESHAP_SIP_PROC_H__
#define __MESHAP_SIP_PROC_H__

int meshap_sip_proc_initialize(void);

#endif /*__MESHAP_SIP_PROC_H__*/
