/********************************************************************************
* MeshDynamics
* --------------
* File     : meshap_al_test.h
* Comments : Torna MeshAP Abstraction Layer Test Suite
* Created  : 5/21/2004
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  5  |4/15/2005 | Added tests 13_1 and 14_1                       | Sriram |
* -----------------------------------------------------------------------------
* |  4  |4/2/2005  | Added test_12_1                                 | Sriram |
* -----------------------------------------------------------------------------
* |  3  |7/23/2004 | Added test_11_1                                 | Sriram |
* -----------------------------------------------------------------------------
* |  2  |6/10/2004 | Added dis_assoc test case                       | Sriram |
* -----------------------------------------------------------------------------
* |  1  |6/5/2004  | Added more tests                                | Sriram |
* -----------------------------------------------------------------------------
* |  0  |5/21/2004 | Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

#ifndef __MESHAP_AL_TEST_H__
#define __MESHAP_AL_TEST_H__

/**
 * meshap_al_test_1_1 creates a thread, that starts and waits on an
 * event for infinite duration. meshap_al_test_1_2 sets the event
 * causing the thread to quit.
 */

unsigned int meshap_al_test_1_1();
void meshap_al_test_1_2(unsigned int token);

/**
 * meshap_al_test_2_1 creates a thread, that starts and waits on an
 * event for 30 seconds. meshap_al_test_2_2 sets the event
 * causing the thread to quit. In the event of a timeout i.e
 * if meshap_al_test_2_2 is not called the thread quits.
 */

unsigned int meshap_al_test_2_1();
void meshap_al_test_2_2(unsigned int token);

/**
 * meshap_al_test_3_1 creates a thread, that starts and waits on an
 * semaphore for infinite duration. meshap_al_test_3_2 releases the
 * semaphore causing the thread to quit.
 */

unsigned int meshap_al_test_3_1();
void meshap_al_test_3_2(unsigned int token);

/**
 * meshap_al_test_4_1 displays the information about
 * sk_buff's after operations like skb_put,skb_pull,
 * etc
 */

void meshap_al_test_4_1();

/**
 * meshap_al_test_5_1 tests:
 *	a) al_get_net_if_count, al_get_net_if
 *	b) al_allocate_packet, al_release_packet
 *	c) setup of sk_buff from an al_packet
 *	d) creation of al_packket from sk_buff
 */

void meshap_al_test_5_1();

/**
 * meshap_al_test_6_1 tests:
 *	a) get_extended_operations for eth and wlan
 *	b) get_mode, set_mode
 *	c) get_essid,set_essid
 *	d) associate
 */

void meshap_al_test_6_1();

/**
 * meshap_al_test_7_1 tests:
 *	a) scan_access_points
 *	b) set_channel
 *	c) get_channel
 */

void meshap_al_test_7_1();

void meshap_al_test_8_1();

void meshap_al_test_9_1();

void meshap_al_test_10_1();

void meshap_al_test_11_1();

void meshap_al_test_12_1();

void meshap_al_test_13_1();

void meshap_al_test_14_1();

#endif /*__MESHAP_AL_TEST_H__*/
