/********************************************************************************
* MeshDynamics
* --------------
* File     : dot11i-defs.h
* Comments : WPA/802.11i definitions
* Created  : 3/1/2005
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  0  |3/1/2005 | Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

#include "al.h"
#include "al_802_11.h"
#include "access_point.h"
#include "dot1x.h"

#ifndef __DOT11I_DEFS_H__
#define __DOT11I_DEFS_H__

#define DOT11I_PRF_BUFFER_SIZE    80
#define _PACKET_DUMP(bytes, length)                         \
   {                                                        \
      int  i, j;                                            \
      char str[8];                                          \
      char buf[64];                                         \
      for (i = 0, j = 0; i < length; i++) {                 \
         if (i % 16 == 0 && i != 0) {                       \
            buf[j] = 0;                                     \
            dot1x_print(DOT1X_PRINT_LEVEL_FLOW, "%s", buf); \
            j = 0;                                          \
         }                                                  \
         snprintf(str, sizeof(str), "%02X", bytes[i]);      \
         buf[j++] = str[0];                                 \
         buf[j++] = str[1];                                 \
         buf[j++] = ' ';                                    \
      }                                                     \
      buf[j] = 0;                                           \
      dot1x_print(DOT1X_PRINT_LEVEL_FLOW, "%s", buf);       \
      j = 0;                                                \
   }

int dot11i_prf(unsigned char *key,
               int           key_length,
               const char    *label,
               unsigned char *data,
               int           data_length,
               unsigned char *prf_out,
               int           prf_out_length);

unsigned char *dot11i_addr_min(unsigned char *address_a, unsigned char *address_b);
unsigned char *dot11i_addr_max(unsigned char *address_a, unsigned char *address_b);

unsigned char *dot11i_nonce_min(unsigned char *nonce_a, unsigned char *nonce_b);
unsigned char *dot11i_nonce_max(unsigned char *nonce_a, unsigned char *nonce_b);

#define DOT11I_LEFT_BYTES(in, start, count, out)    memcpy((out), (((unsigned char *)(in)) + (start)), (count))]

#define DOT11I_PRF_128(k, kl, l, d, dl, o)          dot11i_prf((k), (kl), (l), (d), (dl), (o), 16)
#define DOT11I_PRF_192(k, kl, l, d, dl, o)          dot11i_prf((k), (kl), (l), (d), (dl), (o), 24)
#define DOT11I_PRF_256(k, kl, l, d, dl, o)          dot11i_prf((k), (kl), (l), (d), (dl), (o), 32)
#define DOT11I_PRF_384(k, kl, l, d, dl, o)          dot11i_prf((k), (kl), (l), (d), (dl), (o), 48)
#define DOT11I_PRF_512(k, kl, l, d, dl, o)          dot11i_prf((k), (kl), (l), (d), (dl), (o), 64)

struct dot11i_authenticator
{
   access_point_netif_config_info_t *net_if_config_info;
   dot1x_t                          *dot1x_instance;
   unsigned char                    rsn_ie_buffer[256];
   int                              rsn_ie_length;

   /** Authenticator Globals */
   int                              GTKAuthenticator;
   int                              GInitDone;
   int                              GNoStations;

   int                              GKeyDoneStations;
   int                              GTKReKey;

   int                              GKeyReady;
   unsigned char                    GNonce[32];
   int                              GN;
   int                              GM;
   unsigned char                    KeyCounter[32];
};

typedef struct dot11i_authenticator   dot11i_authenticator_t;

void dot11i_initialize_key_counter(dot11i_authenticator_t *authenticator);
void dot11i_increment_key_counter(dot11i_authenticator_t *authenticator);

void dot11i_setup_group_key(dot11i_authenticator_t *authenticator);
#endif /*__DOT11I_DEFS_H__*/
