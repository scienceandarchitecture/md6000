/********************************************************************************
* MeshDynamics
* --------------
* File     : dot11i-impl.h
* Comments : WPA/802.11i Implementation Definitions
* Created  : 3/2/2005
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  0  |3/2/2005  | Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

#include "dot11i-impl-inc.h"

#ifndef __DOT11I_IMPL_H__
#define __DOT11I_IMPL_H__

#ifndef DOT11I_SHA1_SIZE
#define DOT11I_SHA1_SIZE    20
#endif

DOT11I_U64 dot11i_cpu_to_be64(DOT11I_U64 in);

int dot11i_hmac_sha1(unsigned char *input,
                     int           input_length,
                     unsigned char *key,
                     int           key_len,
                     unsigned char *output);

int dot11i_rc4_encrypt(unsigned char *cipher_text,
                       unsigned char *plain_text,
                       int           plain_text_length,
                       unsigned char *key,
                       int           key_len,
                       unsigned char *key_skip,
                       int           key_skip_len);

int dot11i_aes_keywrap_encrypt(unsigned char *cipher_text,
                               unsigned char *plain_text,
                               int           plain_text_length,
                               unsigned char *key,
                               int           key_len);

int dot11i_aes_keywrap_decrypt(unsigned char *plain_text,
                               unsigned char *cipher_text,
                               int           cipher_text_length,
                               unsigned char *key,
                               int           key_len);
#endif /*__DOT11I_IMPL_H__*/
