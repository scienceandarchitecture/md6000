/********************************************************************************
* MeshDynamics
* --------------
* File     : dot11i.h
* Comments : WPA/802.11i Header
* Created  : 1/23/2005
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  1  |4/7/2005  | Misc changes                                    | Sriram |
* -----------------------------------------------------------------------------
* |  0  |1/23/2005 | Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

#include "al.h"

#ifndef __DOT11I_H__
#define __DOT11I_H__

#ifndef DOT11I_NET_IF_HASH_SIZE
#define DOT11I_NET_IF_HASH_SIZE    17
#endif

int dot11i_initialize(AL_CONTEXT_PARAM_DECL_SINGLE);
int dot11i_uninitialize(AL_CONTEXT_PARAM_DECL_SINGLE);
int dot11i_tick(AL_CONTEXT_PARAM_DECL_SINGLE);
#endif /*__DOT11I_H__*/
