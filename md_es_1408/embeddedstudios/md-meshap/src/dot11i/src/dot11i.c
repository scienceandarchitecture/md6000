/********************************************************************************
* MeshDynamics
* --------------
* File     : dot11i.c
* Comments : WPA/802.11i Implementation
* Created  : 1/23/2005
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  6  |7/25/2007 | Cleanup Parsed RSN IE                           | Sriram |
* -----------------------------------------------------------------------------
* |  5  |7/16/2007 | MAX TIMEOUTS set to 8                           | Sriram |
* -----------------------------------------------------------------------------
* |  4  |7/16/2007 | dot11i_tick modified                            | Sriram |
* -----------------------------------------------------------------------------
* |  3  |4/12/2005 | GTK generation even when encryption disabled    | Sriram |
* -----------------------------------------------------------------------------
* |  2  |4/7/2005  | Misc changes                                    | Sriram |
* -----------------------------------------------------------------------------
* |  1  |4/6/2005  | Misc changes                                    | Sriram |
* -----------------------------------------------------------------------------
* |  0  |1/23/2005 | Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

#include <linux/string.h>

#include "dot11i-defs.h"
#include "dot11i.h"
#include "dot11i-impl.h"
#include "dot1x-impl.h"
#include "dot1x-fsm.h"
#include "dot1x-fsm-other.h"
#include "dot11i-fsm-authenticator.h"
#include "dot11i-eapol.h"
#include "dot1x-backend-radius.h"
#include "dot11i-fsm-gtk.h"

struct _net_if_config_hash
{
   int                        counter;
   dot11i_authenticator_t     authenticator;
   struct _net_if_config_hash *next_hash;
   struct _net_if_config_hash *next_list;
};

typedef struct _net_if_config_hash   _net_if_config_hash_t;

static _net_if_config_hash_t *_net_if_config_hash[DOT11I_NET_IF_HASH_SIZE];
static _net_if_config_hash_t *_net_if_config_list;

static int _access_point_on_dot1x_auth_init(AL_CONTEXT_PARAM_DECL access_point_netif_config_info_t *net_if_config_info);

static int _access_point_on_dot1x_auth_start(AL_CONTEXT_PARAM_DECL al_net_if_t *net_if,
                                             access_point_netif_config_info_t  *net_if_config_info,
                                             al_net_addr_t                     *sta_addr,
                                             al_802_11_information_element_t   *rsn_ie,
                                             int                               *enable_encryption_out,
                                             int                               *key_index_out);


static int _access_point_on_dot1x_auth_delete(AL_CONTEXT_PARAM_DECL al_net_if_t *net_if,
                                              access_point_netif_config_info_t  *net_if_config_info,
                                              al_net_addr_t                     *sta_addr);

static int _access_point_on_eapol_rx(AL_CONTEXT_PARAM_DECL al_net_if_t *net_if,
                                     access_point_netif_config_info_t  *net_if_config_info,
                                     al_packet_t                       *al_packet);

static int _dot1x_port_status_change(struct dot1x_port *port, int status);

static AL_INLINE unsigned char _get_best_auth_mode(AL_CONTEXT_PARAM_DECL unsigned char sta_auth_modes, unsigned char ap_auth_modes)
{
   /**
    * Assumes ap_auth_modes is non-zero
    */

   if (sta_auth_modes == 0)
   {
      return AL_802_11_SECURITY_INFO_RSN_AUTH_NONE;
   }

   if (ap_auth_modes > sta_auth_modes)
   {
      /**
       * AP supports a higher option than the STA.
       * Choose the highest option supported by the STA
       * and then see if the AP supports it. If supported
       * return that option or else return 0 to force
       * and un-authorization. Typically this case manifests
       * as an AP supporting DOT1X and the STA only supporting
       * PSK, in which case it depends on whether the AP
       * supports PSK or not.
       */

      if (sta_auth_modes & AL_802_11_SECURITY_INFO_RSN_AUTH_1X)
      {
         sta_auth_modes = AL_802_11_SECURITY_INFO_RSN_AUTH_1X;
      }
      else
      {
         sta_auth_modes = AL_802_11_SECURITY_INFO_RSN_AUTH_PSK;
      }

      if (ap_auth_modes & sta_auth_modes)
      {
         return sta_auth_modes;
      }
      else
      {
         return 0;                /** Causes a forced un-auth */
      }
   }
   else
   {
      /**
       * STA supports a higher option than the AP.
       * Choose the highest option supported by the AP
       * and then see if the STA supports it. If supported
       * return that option or else return 0 to force
       * and un-authorization. Typically this case manifests
       * as an STA supporting DOT1X and the AP not supporting
       * DOT1X. The result depends upon whether the STS supports
       * PSK or not.
       */

      if (ap_auth_modes & AL_802_11_SECURITY_INFO_RSN_AUTH_1X)
      {
         ap_auth_modes = AL_802_11_SECURITY_INFO_RSN_AUTH_1X;
      }
      else
      {
         ap_auth_modes = AL_802_11_SECURITY_INFO_RSN_AUTH_PSK;
      }

      if (sta_auth_modes & ap_auth_modes)
      {
         return ap_auth_modes;
      }
      else
      {
         return 0;                /** Causes a forced un-auth */
      }
   }

   return 0;      /** Causes a forced un-auth */
}


static AL_INLINE unsigned char _get_best_cipher_mode(AL_CONTEXT_PARAM_DECL unsigned char sta_cipher_modes, unsigned char ap_cipher_modes)
{
   /**
    * Assumes ap_cipher_modes is non-zero
    */

   if (sta_cipher_modes == 0)
   {
      return AL_802_11_SECURITY_INFO_RSN_CIPHER_NONE;
   }

   if (ap_cipher_modes > sta_cipher_modes)
   {
      if (sta_cipher_modes & AL_802_11_SECURITY_INFO_RSN_CIPHER_CCMP)
      {
         sta_cipher_modes = AL_802_11_SECURITY_INFO_RSN_CIPHER_CCMP;
      }
      else
      {
         sta_cipher_modes = AL_802_11_SECURITY_INFO_RSN_CIPHER_TKIP;
      }
      if (ap_cipher_modes & sta_cipher_modes)
      {
         return sta_cipher_modes;
      }
      else
      {
         return 0;                /** Causes a forced un-auth */
      }
   }
   else
   {
      if (ap_cipher_modes & AL_802_11_SECURITY_INFO_RSN_CIPHER_CCMP)
      {
         ap_cipher_modes = AL_802_11_SECURITY_INFO_RSN_CIPHER_CCMP;
      }
      else
      {
         ap_cipher_modes = AL_802_11_SECURITY_INFO_RSN_CIPHER_TKIP;
      }
      if (sta_cipher_modes & ap_cipher_modes)
      {
         return ap_cipher_modes;
      }
      else
      {
         return 0;                /** Causes a forced un-auth */
      }
   }

   return 0;      /** Causes a forced un-auth */
}


static AL_INLINE int _process_rsn_ie(AL_CONTEXT_PARAM_DECL al_net_if_t *net_if,
                                     al_802_11_information_element_t   *rsn_ie,
                                     access_point_netif_config_info_t  *net_if_config_info,
                                     unsigned char                     *auth_mode_out,
                                     unsigned char                     *cipher_mode_out,
                                     al_802_11_rsn_ie_t                *parsed_rsn_ie_out)
{
   int ret;

   ret = ACCESS_POINT_ON_DOT1X_AUTH_STATUS_ACCEPTED;

   if (rsn_ie != NULL)
   {
      if (al_802_11_parse_rsn_ie(AL_CONTEXT rsn_ie, parsed_rsn_ie_out) != 0)
      {
         ret = ACCESS_POINT_ON_DOT1X_AUTH_STATUS_FORCE_UNAUTH;
      }
      if (!net_if_config_info->security_info.dot11.rsn.enabled)
      {
         if (net_if_config_info->security_info.dot11.wep.enabled)
         {
            *auth_mode_out   = AL_802_11_SECURITY_INFO_RSN_AUTH_NONE;
            *cipher_mode_out = AL_802_11_SECURITY_INFO_RSN_CIPHER_WEP;
            ret = ACCESS_POINT_ON_DOT1X_AUTH_STATUS_FORCE_AUTH;
         }
         else
         {
            *auth_mode_out   = AL_802_11_SECURITY_INFO_RSN_AUTH_NONE;
            *cipher_mode_out = AL_802_11_SECURITY_INFO_RSN_CIPHER_NONE;
            ret = ACCESS_POINT_ON_DOT1X_AUTH_STATUS_FORCE_AUTH;
         }
      }
   }
   else
   {
      if (AL_NET_IF_IS_ETHERNET(net_if))
      {
         if (!net_if_config_info->security_info.dot11.rsn.enabled)
         {
            *auth_mode_out   = AL_802_11_SECURITY_INFO_RSN_AUTH_NONE;
            *cipher_mode_out = AL_802_11_SECURITY_INFO_RSN_CIPHER_NONE;
            ret = ACCESS_POINT_ON_DOT1X_AUTH_STATUS_FORCE_AUTH;
         }
         else
         {
            if (net_if_config_info->security_info.dot11.rsn.auth_modes & AL_802_11_SECURITY_INFO_RSN_AUTH_1X)
            {
               *auth_mode_out = AL_802_11_SECURITY_INFO_RSN_AUTH_1X;
            }
            else
            {
               *auth_mode_out = AL_802_11_SECURITY_INFO_RSN_AUTH_NONE;
               ret            = ACCESS_POINT_ON_DOT1X_AUTH_STATUS_FORCE_AUTH;
            }
            *cipher_mode_out = AL_802_11_SECURITY_INFO_RSN_CIPHER_NONE;
         }
      }
      else
      {
         if (net_if_config_info->security_info.dot11.wep.enabled)
         {
            /**
             * WEP is enabled, hence we simple assume a WEP based
             * client and force an authorization
             */
            *auth_mode_out   = AL_802_11_SECURITY_INFO_RSN_AUTH_NONE;
            *cipher_mode_out = AL_802_11_SECURITY_INFO_RSN_CIPHER_WEP;
            ret = ACCESS_POINT_ON_DOT1X_AUTH_STATUS_FORCE_AUTH;
         }
         else
         {
            if (!net_if_config_info->security_info.dot11.rsn.enabled)
            {
               /**
                * WEP is disabled, and so is rsn. Here simply force authorization
                * and return
                */
               *auth_mode_out   = AL_802_11_SECURITY_INFO_RSN_AUTH_NONE;
               *cipher_mode_out = AL_802_11_SECURITY_INFO_RSN_CIPHER_NONE;
               ret = ACCESS_POINT_ON_DOT1X_AUTH_STATUS_FORCE_AUTH;
            }
            else
            {
               /**
                * RSN has been enabled but WEP has been disabled. Since the
                * client does not support RSN, we have to force unauth
                */
               *auth_mode_out   = AL_802_11_SECURITY_INFO_RSN_AUTH_NONE;
               *cipher_mode_out = AL_802_11_SECURITY_INFO_RSN_CIPHER_NONE;
               ret = ACCESS_POINT_ON_DOT1X_AUTH_STATUS_FORCE_UNAUTH;
            }
         }
      }
   }

   return ret;
}


static AL_INLINE int _refine_auth_and_cipher_modes(AL_CONTEXT_PARAM_DECL
                                                   int                              ret,
                                                   al_802_11_rsn_ie_t               *parsed_rsn_ie,
                                                   access_point_netif_config_info_t *net_if_config_info,
                                                   unsigned char                    *auth_mode_out,
                                                   unsigned char                    *cipher_mode_out)
{
   if (*auth_mode_out == 0)
   {
      /**
       * No decision regarding the authentication mechanism has been
       * made. This means that the client supports RSN and we also
       * support RSN. Now we choose the best supported mechanism by
       * both the entities and use it.
       */
      *auth_mode_out = _get_best_auth_mode(parsed_rsn_ie->auth_modes,
                                           net_if_config_info->security_info.dot11.rsn.auth_modes);
      if (*auth_mode_out == 0)
      {
         ret = ACCESS_POINT_ON_DOT1X_AUTH_STATUS_FORCE_UNAUTH;
         return ret;
      }
   }

   if (*cipher_mode_out == 0)
   {
      /**
       * No decision regarding the cipher mode has been
       * made. This means that the client supports RSN and we also
       * support RSN. Now we choose the best supported mechanism by
       * both the entities and use it.
       */
      *cipher_mode_out = _get_best_cipher_mode(parsed_rsn_ie->cipher_modes,
                                               net_if_config_info->security_info.dot11.rsn.cipher_modes);
      if (*cipher_mode_out == 0)
      {
         ret = ACCESS_POINT_ON_DOT1X_AUTH_STATUS_FORCE_UNAUTH;
         return ret;
      }
   }

   return ret;
}


static AL_INLINE int _check_force_auth_unauth(AL_CONTEXT_PARAM_DECL al_net_if_t *net_if,
                                              access_point_netif_config_info_t  *net_if_config_info,
                                              int                               ret,
                                              unsigned char                     auth_mode,
                                              unsigned char                     cipher_mode,
                                              int                               *enable_encryption_out,
                                              int                               *key_index_out)
{
   if ((auth_mode == AL_802_11_SECURITY_INFO_RSN_AUTH_NONE) &&
       (cipher_mode != AL_802_11_SECURITY_INFO_RSN_CIPHER_NONE) &&
       (cipher_mode != AL_802_11_SECURITY_INFO_RSN_CIPHER_WEP))
   {
      ret = ACCESS_POINT_ON_DOT1X_AUTH_STATUS_FORCE_UNAUTH;
      return ret;
   }

   if (AL_NET_IF_IS_WIRELESS(net_if))
   {
      if ((auth_mode != AL_802_11_SECURITY_INFO_RSN_AUTH_NONE) &&
          (cipher_mode != AL_802_11_SECURITY_INFO_RSN_CIPHER_TKIP) &&
          (cipher_mode != AL_802_11_SECURITY_INFO_RSN_CIPHER_CCMP))
      {
         ret = ACCESS_POINT_ON_DOT1X_AUTH_STATUS_FORCE_UNAUTH;
         return ret;
      }
   }

   /**
    * Deal with the NONE authentication scheme first
    */

   if (auth_mode == AL_802_11_SECURITY_INFO_RSN_AUTH_NONE)
   {
      switch (cipher_mode)
      {
      case AL_802_11_SECURITY_INFO_RSN_CIPHER_NONE:
         ret = ACCESS_POINT_ON_DOT1X_AUTH_STATUS_FORCE_AUTH;
         *enable_encryption_out = 0;
         *key_index_out         = -1;
         return ret;

      case AL_802_11_SECURITY_INFO_RSN_CIPHER_WEP:
         ret = ACCESS_POINT_ON_DOT1X_AUTH_STATUS_FORCE_AUTH;
         *enable_encryption_out = 1;
         *key_index_out         = net_if_config_info->security_info.dot11.wep.key_index;
         return ret;
      }
   }

   return ret;
}


static AL_INLINE _net_if_config_hash_t *_lookup_config(AL_CONTEXT_PARAM_DECL access_point_netif_config_info_t *net_if_config_info)
{
   _net_if_config_hash_t *nich;
   int bucket;

   bucket = ((unsigned int)net_if_config_info) % DOT11I_NET_IF_HASH_SIZE;
   nich   = _net_if_config_hash[bucket];

   while (nich != NULL)
   {
      if (net_if_config_info == nich->authenticator.net_if_config_info)
      {
         break;
      }
      nich = nich->next_hash;
   }

   return nich;
}


static int _dot1x_port_iterator_routine(dot1x_port_t *port)
{
   dot11i_fsm_authenticator_tick(port);
   dot11i_fsm_gtk_tick(port);

   return 0;
}


int dot11i_initialize(AL_CONTEXT_PARAM_DECL_SINGLE)
{
   memset(_net_if_config_hash, 0, sizeof(_net_if_config_hash_t));
   _net_if_config_list = NULL;

   access_point_set_dot1x_handler(AL_CONTEXT _access_point_on_dot1x_auth_init,
                                  _access_point_on_dot1x_auth_start,
                                  _access_point_on_dot1x_auth_delete,
                                  _access_point_on_eapol_rx);

   dot1x_print(DOT1X_PRINT_LEVEL_INFORMATION, "dot11i: Initialized\n");

   dot1x_radius_backend_initialize();

   return 0;
}


int dot11i_uninitialize(AL_CONTEXT_PARAM_DECL_SINGLE)
{
   _net_if_config_hash_t *list_node;
   _net_if_config_hash_t *temp;

   access_point_set_dot1x_handler(AL_CONTEXT NULL, NULL, NULL, NULL);

   dot1x_radius_backend_uninitialize();

   /**
    * Clean up hash table and list
    */

   list_node = _net_if_config_list;

   while (list_node != NULL)
   {
      temp = list_node->next_list;
      dot1x_destroy_instance(list_node->authenticator.dot1x_instance);
      dot1x_heap_free(list_node);
      list_node = temp;
   }

   _net_if_config_list = NULL;

   return 0;
}


int dot11i_tick(AL_CONTEXT_PARAM_DECL_SINGLE)
{
   _net_if_config_hash_t *list_node;

   list_node = _net_if_config_list;

   while (list_node != NULL)
   {
      if (list_node->counter == 2)
      {
         dot1x_tick(list_node->authenticator.dot1x_instance);
         list_node->counter = 0;
      }
      dot1x_iterate_ports(list_node->authenticator.dot1x_instance, _dot1x_port_iterator_routine);
      ++list_node->counter;
      list_node = list_node->next_list;
   }

   return 0;
}


static int _access_point_on_dot1x_auth_init(AL_CONTEXT_PARAM_DECL access_point_netif_config_info_t *net_if_config_info)
{
   _net_if_config_hash_t *nich;
   int bucket;
   dot1x_radius_backend_param_t radius_param;
   int           i;
   unsigned char *p;

   if (net_if_config_info->net_if != NULL)
   {
      dot1x_print(DOT1X_PRINT_LEVEL_INFORMATION,
                  "dot11i: Initializing interface %s, essid %s\n",
                  net_if_config_info->net_if->name,
                  net_if_config_info->essid);
   }
   else
   {
      dot1x_print(DOT1X_PRINT_LEVEL_INFORMATION, "dot11i: Initializing vlan essid %s\n", net_if_config_info->essid);
   }

   memset(&radius_param, 0, sizeof(dot1x_radius_backend_param_t));

   nich = _lookup_config(AL_CONTEXT net_if_config_info);

   if (nich != NULL)
   {
      return -1;
   }

   nich   = (_net_if_config_hash_t *)dot1x_heap_alloc(sizeof(_net_if_config_hash_t));
   bucket = ((unsigned int)net_if_config_info) % DOT11I_NET_IF_HASH_SIZE;

   memset(nich, 0, sizeof(_net_if_config_hash_t));

   nich->authenticator.net_if_config_info = net_if_config_info;

   /**
    * Add net_if_config_info to the hash
    */

   nich->next_hash             = _net_if_config_hash[bucket];
   _net_if_config_hash[bucket] = nich;

   /**
    * Add net_if_config_info to the list
    */

   nich->next_list     = _net_if_config_list;
   _net_if_config_list = nich;

   radius_param.radius_secret_len = strlen(net_if_config_info->security_info.dot1x_backend_secret);
   memcpy(radius_param.radius_secret,
          net_if_config_info->security_info.dot1x_backend_secret,
          radius_param.radius_secret_len);
   memcpy(&radius_param.radius_server_addr, net_if_config_info->security_info.dot1x_backend_addr.bytes, 4);
   radius_param.radius_server_port = net_if_config_info->security_info.dot1x_backend_port;

   nich->authenticator.dot1x_instance = dot1x_create_instance("dot11i",
                                                              "radius",
                                                              (void *)&radius_param,
                                                              (void *)&nich->authenticator,
                                                              _dot1x_port_status_change);



   /**
    * If both RSN and WEP are enabled, then the GTK is the WEP key itself.
    * If RSN is enabled and WEP is not, then we generate a GTK.
    * If both RSN and WEP are disbaled we do not do anything.
    * For VLAN configurations, we use the GTK of the net_if that is being
    * used, otherwise for proper net_if configurations we generate a
    * GTK
    */

   if ((net_if_config_info->net_if != NULL) &&
       AL_NET_IF_IS_WIRELESS(net_if_config_info->net_if))
   {
      memset(&net_if_config_info->group_key, 0, sizeof(al_802_11_security_key_info_t));

      memcpy(&net_if_config_info->group_key.destination, &broadcast_net_addr, sizeof(al_net_addr_t));

      if (net_if_config_info->security_info.dot11.rsn.enabled)
      {
         if (net_if_config_info->security_info.dot11.wep.enabled)
         {
            /**
             * WEP shall be the group key for this interface, and also
             * the default key
             */
            net_if_config_info->group_key.enabled     = 1;
            net_if_config_info->group_key.cipher_type = AL_802_11_SECURITY_INFO_RSN_CIPHER_WEP;
            net_if_config_info->group_key.strength    = net_if_config_info->security_info.dot11.wep.strength;
            net_if_config_info->group_key.key_index   = net_if_config_info->security_info.dot11.wep.key_index;

            for (i = 0, p = net_if_config_info->group_key.key; i < AL_802_11_MAX_SECURITY_KEYS; i++)
            {
               memcpy(p, net_if_config_info->security_info.dot11.wep.keys[i], net_if_config_info->security_info.dot11.wep.strength / 8);
               p += net_if_config_info->security_info.dot11.wep.strength / 8;
            }
         }
         else
         {
            /**
             * The lesser of CCMP and TKIP shall be the group key. In any case we need to
             * generate a GTK before we set the key.
             */

            dot11i_setup_group_key(&nich->authenticator);

            if (net_if_config_info->security_info.dot11.rsn.cipher_modes & AL_802_11_SECURITY_INFO_RSN_CIPHER_TKIP)
            {
               net_if_config_info->group_key.cipher_type = AL_802_11_SECURITY_INFO_RSN_CIPHER_TKIP;
            }
            else
            {
               net_if_config_info->group_key.cipher_type = AL_802_11_SECURITY_INFO_RSN_CIPHER_CCMP;
            }

            net_if_config_info->group_key.enabled   = 1;
            net_if_config_info->group_key.key_index = 1;
         }

         nich->authenticator.GTKAuthenticator = True;
         nich->authenticator.GInitDone        = True;
         nich->authenticator.GTKReKey         = False;
         nich->authenticator.GN        = 1;
         nich->authenticator.GKeyReady = True;

         nich->authenticator.rsn_ie_length = al_802_11_format_rsn_ie(AL_CONTEXT & net_if_config_info->security_info.dot11,
                                                                     NULL,
                                                                     nich->authenticator.rsn_ie_buffer,
                                                                     256);
      }
      else if (net_if_config_info->security_info.dot11.wep.enabled)
      {
         /**
          * This is a classic 802.11 BSS. Here we use only default keys
          */
         net_if_config_info->group_key.enabled     = 1;
         net_if_config_info->group_key.cipher_type = AL_802_11_SECURITY_INFO_RSN_CIPHER_WEP;
         net_if_config_info->group_key.strength    = net_if_config_info->security_info.dot11.wep.strength;
         net_if_config_info->group_key.key_index   = net_if_config_info->security_info.dot11.wep.key_index;

         for (i = 0, p = net_if_config_info->group_key.key; i < AL_802_11_MAX_SECURITY_KEYS; i++)
         {
            memcpy(p, net_if_config_info->security_info.dot11.wep.keys[i], net_if_config_info->security_info.dot11.wep.strength / 8);
            p += net_if_config_info->security_info.dot11.wep.strength / 8;
         }
      }
      else
      {
         /**
          * NO group key, as encryption has been disabled.
          * We would still generate a GTK just so that any VLAN having
          * encryption enabled will then get to use it
          */

         dot11i_setup_group_key(&nich->authenticator);

         net_if_config_info->group_key.cipher_type = AL_802_11_SECURITY_INFO_RSN_CIPHER_CCMP;
         net_if_config_info->group_key.strength    = AL_802_11_SECURITY_INFO_STRENGTH_CCMP;

         net_if_config_info->group_key.enabled   = 1;
         net_if_config_info->group_key.key_index = 1;
      }
   }

   return 0;
}


static int _access_point_on_dot1x_auth_start(AL_CONTEXT_PARAM_DECL al_net_if_t *net_if,
                                             access_point_netif_config_info_t  *net_if_config_info,
                                             al_net_addr_t                     *sta_addr,
                                             al_802_11_information_element_t   *rsn_ie,
                                             int                               *enable_encryption_out,
                                             int                               *key_index_out)
{
   _net_if_config_hash_t *nich;
   al_802_11_rsn_ie_t    parsed_rsn_ie;
   unsigned char         auth_mode;
   unsigned char         cipher_mode;
   int                        ret;
   dot1x_port_t               *port;
   dot11i_authenticator_t     *authenticator;
   dot11i_fsm_authenticator_t *fsm_auth;
   dot11i_fsm_gtk_t           *fsm_gtk;

   port = NULL;
   nich = _lookup_config(AL_CONTEXT net_if_config_info);

   if (nich == NULL)
   {
      return ACCESS_POINT_ON_DOT1X_AUTH_STATUS_FORCE_UNAUTH;
   }

   auth_mode   = 0;                     /** 0 means no decision made as oppossed to NONE */
   cipher_mode = 0;                     /** 0 means no decision made as oppossed to NONE */

   ret = _process_rsn_ie(AL_CONTEXT net_if, rsn_ie, net_if_config_info, &auth_mode, &cipher_mode, &parsed_rsn_ie);

   if (ret == ACCESS_POINT_ON_DOT1X_AUTH_STATUS_FORCE_UNAUTH)
   {
      goto _lb_ret;
   }

   if (rsn_ie != NULL)
   {
      ret = _refine_auth_and_cipher_modes(AL_CONTEXT ret, &parsed_rsn_ie, net_if_config_info, &auth_mode, &cipher_mode);
   }

   if (ret == ACCESS_POINT_ON_DOT1X_AUTH_STATUS_FORCE_UNAUTH)
   {
      goto _lb_ret;
   }

   /**
    * At this stage both cipher_mode and auth_mode are expected to
    * be non-zero, i.e a decision should have been made for both.
    * Moreover auth_mode can be none only if cipher_mode is none
    * or is WEP. Moreover if auth_mode is not none then the cipher
    * mode has to be TKIP or CCMP.
    */

   AL_ASSERT("auth_mode", auth_mode != 0);
   AL_ASSERT("cipher_mode", cipher_mode != 0);

   ret = _check_force_auth_unauth(AL_CONTEXT net_if,
                                  net_if_config_info,
                                  ret,
                                  auth_mode,
                                  cipher_mode,
                                  enable_encryption_out,
                                  key_index_out);

   if ((ret == ACCESS_POINT_ON_DOT1X_AUTH_STATUS_FORCE_AUTH) ||
       (ret == ACCESS_POINT_ON_DOT1X_AUTH_STATUS_FORCE_UNAUTH))
   {
      goto _lb_ret;
   }

   /**
    * At this stage, the auth mode should be either PSK
    * or DOT1X, and the cipher mode either TKIP or CCMP
    */

   AL_ASSERT("auth_mode",
             (auth_mode == AL_802_11_SECURITY_INFO_RSN_AUTH_PSK ||
              auth_mode == AL_802_11_SECURITY_INFO_RSN_AUTH_1X));

   if (rsn_ie != NULL)
   {
      AL_ASSERT("cipher_mode",
                (cipher_mode == AL_802_11_SECURITY_INFO_RSN_CIPHER_TKIP ||
                 cipher_mode == AL_802_11_SECURITY_INFO_RSN_CIPHER_CCMP));
   }

   /**
    * Create the port. All the standard FSM's get added here
    */

   if (net_if_config_info->net_if == NULL)
   {
      access_point_netif_config_info_t *main_net_if_config;
      _net_if_config_hash_t            *net_if_nich;

      main_net_if_config = (access_point_netif_config_info_t *)net_if->config.ext_config_info;
      net_if_nich        = _lookup_config(AL_CONTEXT main_net_if_config);

      if (net_if_nich == NULL)
      {
         ret = ACCESS_POINT_ON_DOT1X_AUTH_STATUS_FORCE_UNAUTH;
         goto _lb_ret;
      }

      authenticator = &net_if_nich->authenticator;
   }
   else
   {
      authenticator = &nich->authenticator;
   }



   port = dot1x_create_port(nich->authenticator.dot1x_instance,
                            sta_addr->bytes,
                            (void *)authenticator,
                            Both,
                            Auto,
                            True);

   AL_ASSERT("port", port != NULL);

   if (rsn_ie != NULL)
   {
      /**
       * Register the dot11i FSM
       */

      switch (parsed_rsn_ie.mode)
      {
      case AL_802_11_SECURITY_INFO_RSN_MODE_WPA:
         dot11i_fsm_authenticator_create(port, DOT11I_EAPOL_KEY_TYPE_WPA, cipher_mode, auth_mode, 8);
         dot11i_fsm_gtk_create(port, 5, DOT11I_EAPOL_KEY_TYPE_WPA);
         break;

      case AL_802_11_SECURITY_INFO_RSN_MODE_11I:
         dot11i_fsm_authenticator_create(port, DOT11I_EAPOL_KEY_TYPE_11I, cipher_mode, auth_mode, 8);
         dot11i_fsm_gtk_create(port, 5, DOT11I_EAPOL_KEY_TYPE_11I);
         break;

      default:
         ret = ACCESS_POINT_ON_DOT1X_AUTH_STATUS_FORCE_UNAUTH;
         goto _lb_ret;
      }
   }

   if (auth_mode == AL_802_11_SECURITY_INFO_RSN_AUTH_1X)
   {
      /**
       * Initialize all the FSM's. This will kick-off the dot1x process
       * After completion the dot1x will fire a port_status_change message
       */
      dot1x_port_initialize(nich->authenticator.dot1x_instance, sta_addr->bytes);
   }

   /**
    * Trigger the WPA FSMs
    */

   fsm_auth = dot11i_fsm_authenticator_get(port);

   if (fsm_auth != NULL)
   {
      fsm_auth->fsm.trigger(port,
                            DOT1X_FSM_CODE_OTHER,
                            DOT1X_FSM_OTHER_CODE_DOT11I_AUTH,
                            &fsm_auth->fsm,
                            DOT11I_FSM_AUTHENTICATOR_TRIGGER_AUTH_REQUEST);
   }

   fsm_gtk = dot11i_fsm_gtk_get(port);

   if (fsm_gtk != NULL)
   {
      fsm_gtk->fsm.trigger(port,
                           DOT1X_FSM_CODE_OTHER,
                           DOT1X_FSM_OTHER_CODE_DOT11I_GTK,
                           &fsm_gtk->fsm,
                           DOT11I_FSM_GTK_TRIGGER_AUTH);
   }

   if (rsn_ie != NULL)
   {
      al_802_11_cleanup_rsn_ie(AL_CONTEXT & parsed_rsn_ie);
   }

   return ret;

_lb_ret:

   if (port != NULL)
   {
      dot1x_destroy_port(nich->authenticator.dot1x_instance, sta_addr->bytes);
   }

   if (rsn_ie != NULL)
   {
      al_802_11_cleanup_rsn_ie(AL_CONTEXT & parsed_rsn_ie);
   }

   return ret;
}


static int _access_point_on_dot1x_auth_delete(AL_CONTEXT_PARAM_DECL al_net_if_t *net_if,
                                              access_point_netif_config_info_t  *net_if_config_info,
                                              al_net_addr_t                     *sta_addr)
{
   _net_if_config_hash_t *nich;

   nich = _lookup_config(AL_CONTEXT net_if_config_info);

   if (nich != NULL)
   {
      dot1x_destroy_port(nich->authenticator.dot1x_instance, sta_addr->bytes);
   }

   return 0;
}


static int _access_point_on_eapol_rx(AL_CONTEXT_PARAM_DECL al_net_if_t *net_if,
                                     access_point_netif_config_info_t  *net_if_config_info,
                                     al_packet_t                       *al_packet)
{
   _net_if_config_hash_t *nich;

   nich = _lookup_config(AL_CONTEXT net_if_config_info);

   if (nich == NULL)
   {
      dot1x_print(DOT1X_PRINT_LEVEL_INFORMATION, "dot11i: EAPOL RX nich was NULL\n");
      return -1;
   }

   return dot1x_process_eapol(nich->authenticator.dot1x_instance,
                              al_packet->addr_1.bytes,
                              al_packet->addr_2.bytes,
                              al_packet->type,
                              al_packet->buffer + al_packet->position,
                              al_packet->data_length);
}


static int _dot1x_port_status_change(struct dot1x_port *port, int status)
{
   dot11i_fsm_authenticator_t       *fsm_auth;
   access_point_sta_security_info_t sec_info;

   fsm_auth = dot11i_fsm_authenticator_get(port);

   switch (status)
   {
   case Unauthorized:
      memset(&sec_info, 0, sizeof(access_point_sta_security_info_t));
      sec_info.authenticated            = 0;
      sec_info.dot11.destination.length = 6;
      sec_info.reason_code = APSSI_REASON_CODE_AUTH_FAILURE;
      memcpy(sec_info.dot11.destination.bytes, port->address, 6);
      access_point_set_dot1x_auth_result(AL_CONTEXT & sec_info);
      break;

   case Authorized:
      if (fsm_auth == NULL)
      {
         /**
          * Inform AP about non WPA client
          */
         memset(&sec_info, 0, sizeof(access_point_sta_security_info_t));
         sec_info.authenticated            = 1;
         sec_info.dot11.destination.length = 6;
         sec_info.reason_code = APSSI_REASON_CODE_OK;
         memcpy(sec_info.dot11.destination.bytes, port->address, 6);
         access_point_set_dot1x_auth_result(AL_CONTEXT & sec_info);
      }
      else
      {
         /**
          * Start off the 4 way handshake
          */

         fsm_auth->fsm.trigger(port,
                               DOT1X_FSM_CODE_OTHER,
                               DOT1X_FSM_OTHER_CODE_DOT11I_AUTH,
                               &fsm_auth->fsm,
                               DOT11I_FSM_AUTHENTICATOR_TRIGGER_AAA);
      }
      break;
   }
   return 0;
}
