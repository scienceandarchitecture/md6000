/********************************************************************************
* MeshDynamics
* --------------
* File     : dot11i-eapol.c
* Comments : WPA/802.11i Key descriptor
* Created  : 3/6/2005
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  5  |7/25/2007 | Use GEN2K Buffer instead of Heap                | Sriram |
* -----------------------------------------------------------------------------
* |  4  |5/18/2007 | Fixed MIC verification bug                      | Sriram |
* -----------------------------------------------------------------------------
* |  3  |10/17/2006| Changes for WPA2                                | Sriram |
* -----------------------------------------------------------------------------
* |  2  |9/25/2006 | dot1x_eapol_tx_ex used instead of dot1x_eapol_tx| Sriram |
* -----------------------------------------------------------------------------
* |  1  |4/7/2005  | Key RSC set only for GTK                        | Sriram |
* -----------------------------------------------------------------------------
* |  0  |3/6/2005  | Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

#include <linux/string.h>

#include "dot1x-fsm.h"
#include "dot1x-impl.h"

#include "dot11i-impl.h"
#include "dot11i.h"
#include "dot11i-defs.h"
#include "dot11i-fsm-authenticator.h"
#include "dot11i-eapol.h"


#define _DOT11I_EAPOL_KEY_DESC_TYPE_POSITION      0
#define _DOT11I_EAPOL_KEY_INFO_POSITION           (_DOT11I_EAPOL_KEY_DESC_TYPE_POSITION + 1)
#define _DOT11I_EAPOL_KEY_LENGTH_POSITION         (_DOT11I_EAPOL_KEY_INFO_POSITION + 2)
#define _DOT11I_EAPOL_KEY_REPLAY_POSITION         (_DOT11I_EAPOL_KEY_LENGTH_POSITION + 2)
#define _DOT11I_EAPOL_KEY_NONCE_POSITION          (_DOT11I_EAPOL_KEY_REPLAY_POSITION + 8)
#define _DOT11I_EAPOL_KEY_IV_POSITION             (_DOT11I_EAPOL_KEY_NONCE_POSITION + 32)
#define _DOT11I_EAPOL_KEY_RSC_POSITION            (_DOT11I_EAPOL_KEY_IV_POSITION + 16)
#define _DOT11I_EAPOL_KEY_RESERVED_POSITION       (_DOT11I_EAPOL_KEY_RSC_POSITION + 8)
#define _DOT11I_EAPOL_KEY_MIC_POSITION            (_DOT11I_EAPOL_KEY_RESERVED_POSITION + 8)
#define _DOT11I_EAPOL_KEY_DATA_LENGTH_POSITION    (_DOT11I_EAPOL_KEY_MIC_POSITION + 16)
#define _DOT11I_EAPOL_KEY_DATA_POSITION           (_DOT11I_EAPOL_KEY_DATA_LENGTH_POSITION + 2)

#define _ROUNDUP(x, y)        DOT11I_ROUNDUP(x, y)
#define _ROUNDUP_PAD(x, y)    DOT11I_ROUNDUP_PAD(x, y)




static DOT1X_INLINE void _setup_eapol_fields(dot1x_eapol_frame_t *eapol_frame, unsigned char *buffer)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MESHAP : FLOW : Enter %s: %d\n", __func__, __LINE__);
   eapol_frame->version = DOT1X_2001_EAPOL_VERSION;
   eapol_frame->type    = DOT1X_EAPOL_FRAME_TYPE_KEY;
   eapol_frame->body    = NULL;

   dot1x_eapol_create_frame(eapol_frame, buffer);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MESHAP : FLOW : Exit %s: %d\n", __func__, __LINE__);
}


static DOT1X_INLINE void _set_eapol_key_fields(dot11i_authenticator_t     *authenticator,
                                               dot11i_fsm_authenticator_t *fsm_auth,
                                               dot11i_eapol_key_t         *key_desc,
                                               unsigned char              *p)
{
   unsigned short s;
   int            encr;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MESHAP : FLOW : Enter %s: %d\n", __func__, __LINE__);
   encr = KEY_INFORMATION_GET_ENC(key_desc);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MESHAP : INFO : encr: %d %s: %d\n", encr, __func__, __LINE__);
   memcpy(&p[_DOT11I_EAPOL_KEY_DESC_TYPE_POSITION], &key_desc->type, 1);

   if (fsm_auth->dot11iMode == DOT11I_EAPOL_KEY_TYPE_WPA)
   {
      KEY_INFORMATION_SET_ENC(key_desc, 0);
   }

   s = htons(key_desc->information);
   memcpy(&p[_DOT11I_EAPOL_KEY_INFO_POSITION], &s, 2);

   s = htons(key_desc->length);
   memcpy(&p[_DOT11I_EAPOL_KEY_LENGTH_POSITION], &s, 2);

   /**
    * Replay counter is big-endian
    */

#if (DOT1X_IMPL_BYTE_ORDER == DOT1X_LE)
   {
      int i;
      for (i = 0; i < 8; i++)
      {
         p[_DOT11I_EAPOL_KEY_REPLAY_POSITION + i] = key_desc->replay_ctr[7 - i];
      }
   }
#else
   memcpy(&p[_DOT11I_EAPOL_KEY_REPLAY_POSITION], key_desc->replay_ctr, 8);
#endif

   memcpy(&p[_DOT11I_EAPOL_KEY_NONCE_POSITION], key_desc->nonce, 32);

   memcpy(&p[_DOT11I_EAPOL_KEY_IV_POSITION], key_desc->iv, 16);

   /**
    * Key RSC is 802.11 based hence is little endian
    */

   memcpy(&p[_DOT11I_EAPOL_KEY_RSC_POSITION], key_desc->rsc, 8);

   memset(&p[_DOT11I_EAPOL_KEY_RESERVED_POSITION], 0, 8);

   /**
    * MIC is always set to 0 at this stage
    */

   memset(&p[_DOT11I_EAPOL_KEY_MIC_POSITION], 0, 16);

   if (key_desc->data_length > 0)
   {
      if (encr)
      {
         switch (KEY_INFORMATION_GET_VERSION(key_desc))
         {
         case KEY_INFORMATION_VERSION_TKIP:
            {
               unsigned char rc4_key_buf[32];
               unsigned char dummy[256];

               memset(dummy, 0, sizeof(dummy));

               s = htons(key_desc->data_length);
               memcpy(&p[_DOT11I_EAPOL_KEY_DATA_LENGTH_POSITION], &s, 2);

               memcpy(rc4_key_buf, key_desc->iv, 16);
               memcpy(rc4_key_buf + 16, fsm_auth->PTK + 16, 16);

               dot11i_rc4_encrypt(&p[_DOT11I_EAPOL_KEY_DATA_POSITION],
                                  key_desc->data,
                                  key_desc->data_length,
                                  rc4_key_buf,
                                  sizeof(rc4_key_buf),
                                  dummy,
                                  sizeof(dummy));
            }
            break;

         case KEY_INFORMATION_VERSION_AES:
            /** Assume that the key data is padded according to section 8.5.2 */

            dot11i_aes_keywrap_encrypt(&p[_DOT11I_EAPOL_KEY_DATA_POSITION],
                                       key_desc->data,
                                       key_desc->data_length,
                                       fsm_auth->PTK + 16,
                                       16);

            s = htons(key_desc->data_length);
            memcpy(&p[_DOT11I_EAPOL_KEY_DATA_LENGTH_POSITION], &s, 2);

            break;
         }
      }
      else
      {
         s = htons(key_desc->data_length);
         memcpy(&p[_DOT11I_EAPOL_KEY_DATA_LENGTH_POSITION], &s, 2);
         memcpy(&p[_DOT11I_EAPOL_KEY_DATA_POSITION], key_desc->data, key_desc->data_length);
      }
   }
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MESHAP : FLOW : Exit %s: %d\n", __func__, __LINE__);
}


static DOT1X_INLINE void _incr_replay_counter(dot1x_port_t               *port,
                                              dot11i_fsm_authenticator_t *fsm_auth,
                                              dot11i_authenticator_t     *authenticator)
{
   int i;

   /**
    * Replay counter is a big-endian 8 byte number
    */

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MESHAP : FLOW : Enter %s: %d\n", __func__, __LINE__);
   for (i = 7; i >= 0; i--)
   {
      if (fsm_auth->KeyReplayCounter[i] != 0xFF)
      {
         ++fsm_auth->KeyReplayCounter[i];
         break;
      }
      else
      {
         fsm_auth->KeyReplayCounter[i] = 0;
      }
   }
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MESHAP : FLOW : Exit %s: %d\n", __func__, __LINE__);
}


static DOT1X_INLINE unsigned char *_get_key_data(dot1x_port_t               *port,
                                                 dot11i_fsm_authenticator_t *fsm_auth,
                                                 dot11i_authenticator_t     *authenticator,
                                                 unsigned char              data_flags,
                                                 int                        *key_data_length_out)
{
   int           length;
   unsigned char ie_buffer[256];
   unsigned char gtk_buffer[64];
   unsigned char *rsn_ie;
   int           rsn_ie_length;
   unsigned char *gtk;
   int           gtk_length;
   access_point_netif_config_info_t *net_if_config_info;
   unsigned char *key_data_out;
   unsigned char *tmp;
   int           pad_length;

   static const unsigned char rsn_oui[3] = { 0x00, 0x0F, 0xAC };

   /**
    * Right now we only support RSN IE and GTK data.
    */

   length               = 0;
   net_if_config_info   = NULL;
   *key_data_length_out = 0;
   rsn_ie               = NULL;
   rsn_ie_length        = 0;
   gtk          = NULL;
   gtk_length   = 0;
   key_data_out = NULL;
   pad_length   = 0;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MESHAP : FLOW : Enter %s: %d\n", __func__, __LINE__);
   if (authenticator->net_if_config_info->net_if != NULL)
   {
      net_if_config_info = authenticator->net_if_config_info;
   }
   else
   {
      dot11i_authenticator_t *main_authenticator;
      main_authenticator = (dot11i_authenticator_t *)port->port_data;
      net_if_config_info = main_authenticator->net_if_config_info;
   }

   if (net_if_config_info == NULL)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESHAP : ERROR : net_if_config_info is NULL %s: %d\n", __func__, __LINE__);
      return NULL;
   }

   if (data_flags & DOT11I_EAPOL_KEY_FRAME_SEND_DATA_FLAG_RSN_IE)
   {
      if (authenticator->net_if_config_info->net_if != NULL)
      {
         length       += authenticator->rsn_ie_length;
         rsn_ie        = authenticator->rsn_ie_buffer;
         rsn_ie_length = authenticator->rsn_ie_length;
      }
      else
      {
         rsn_ie_length = al_802_11_format_rsn_ie(AL_CONTEXT & authenticator->net_if_config_info->security_info.dot11,
                                                 &net_if_config_info->security_info.dot11,
                                                 ie_buffer,
                                                 256);
         length += rsn_ie_length;
         rsn_ie  = ie_buffer;
      }
   }

   if (data_flags & DOT11I_EAPOL_KEY_FRAME_SEND_DATA_FLAG_GTK)
   {
      unsigned char *p;
      unsigned char *s;
      int           i;

      memset(gtk_buffer, 0, sizeof(gtk_buffer));

      p = gtk_buffer;

      if (fsm_auth->dot11iMode == DOT11I_EAPOL_KEY_TYPE_11I)
      {
         /** The KDE */
         *p = AL_802_11_EID_VENDOR_PRIVATE;
         p++;
         *p = 3 /** OUI */ + 1 /** Type */ + 2 /** GTK KDE MIN len*/;
         switch (net_if_config_info->group_key.cipher_type)
         {
         case AL_802_11_SECURITY_INFO_RSN_CIPHER_WEP:
            (*p) += net_if_config_info->group_key.strength / 8;
            break;

         case AL_802_11_SECURITY_INFO_RSN_CIPHER_TKIP:
            (*p) += 32;
            break;

         case AL_802_11_SECURITY_INFO_RSN_CIPHER_CCMP:
            (*p) += 16;
            break;
         }
         p++;

         memcpy(p, rsn_oui, 3);
         p += 3;
         *p = AL_802_11_I_KDE_TYPE_GTK;
         p++;

         /** The KDE Data */
         *p = net_if_config_info->group_key.key_index;
         p++;
         *p = 0;
         p++;
      }

      switch (net_if_config_info->group_key.cipher_type)
      {
      case AL_802_11_SECURITY_INFO_RSN_CIPHER_WEP:
         for (i = 0, s = net_if_config_info->group_key.key; i < net_if_config_info->group_key.key_index; i++)
         {
            s += net_if_config_info->group_key.strength / 8;
         }
         memcpy(p, s, net_if_config_info->group_key.strength / 8);
         p += net_if_config_info->group_key.strength / 8;
         break;

      case AL_802_11_SECURITY_INFO_RSN_CIPHER_TKIP:
         memcpy(p, net_if_config_info->group_key.key, 32);
         p += 32;
         break;

      case AL_802_11_SECURITY_INFO_RSN_CIPHER_CCMP:
         memcpy(p, net_if_config_info->group_key.key, 16);
         p += 16;
         break;
      }
      al_print_log(AL_CONTEXT AL_LOG_TYPE_DEBUG, "MESHAP : DEBUG : Group_key %s: %d\n", __func__, __LINE__);
      _PACKET_DUMP(net_if_config_info->group_key.key, 16);

      gtk_length = p - gtk_buffer;
      gtk        = gtk_buffer;
      length    += gtk_length;

      if (fsm_auth->cipher_mode == AL_802_11_SECURITY_INFO_RSN_CIPHER_CCMP)
      {
         length    += 8;
         pad_length = length % 8;
         if (pad_length)
         {
            pad_length  = (8 - pad_length);
            pad_length += 8;
            length     += pad_length;
         }
         else
         {
            pad_length = 8;
         }
      }
   }


   if (length > 0)
   {
      key_data_out = dot1x_get_2k_buffer();

      memset(key_data_out, 0, length);

      tmp = key_data_out;

      if (rsn_ie_length > 0)
      {
         memcpy(tmp, rsn_ie, rsn_ie_length);
         tmp += rsn_ie_length;
      }

      if (gtk_length > 0)
      {
         memcpy(tmp, gtk, gtk_length);
         tmp += gtk_length;
      }

      if (pad_length > 0)
      {
         tmp[0] = AL_802_11_EID_VENDOR_PRIVATE;
      }

      *key_data_length_out = length;
   }

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MESHAP : FLOW : Exit %s: %d\n", __func__, __LINE__);
   return key_data_out;
}


int dot11i_eapol_key_frame_create(dot11i_authenticator_t     *authenticator,
                                  dot11i_fsm_authenticator_t *fsm_auth,
                                  dot11i_eapol_key_t         *key_desc,
                                  dot1x_eapol_frame_t        *eapol_frame,
                                  unsigned char              *buffer)
{
   unsigned char digest[32];

   /** Copy IV value from key counter field */
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MESHAP : FLOW : Enter %s: %d\n", __func__, __LINE__);

   _set_eapol_key_fields(authenticator, fsm_auth, key_desc, buffer + DOT1X_EAPOL_FRAME_MIN_LENGTH);

   eapol_frame->body_length = _DOT11I_EAPOL_KEY_DATA_POSITION + key_desc->data_length;

   _setup_eapol_fields(eapol_frame, buffer);

   /**
    * Now if MIC has been requested, then we calculcate the MIC
    * The MIC calculation includes the EAPOL
    */

   if (KEY_INFORMATION_GET_MIC(key_desc))
   {
      switch (KEY_INFORMATION_GET_VERSION(key_desc))
      {
      case KEY_INFORMATION_VERSION_TKIP:
         dot1x_crypt_hmac_md5(authenticator->dot1x_instance,
                              buffer,
                              DOT1X_EAPOL_FRAME_MIN_LENGTH + _DOT11I_EAPOL_KEY_DATA_POSITION + key_desc->data_length,
                              fsm_auth->PTK,
                              16,
                              digest);
         memcpy(&buffer[DOT1X_EAPOL_FRAME_MIN_LENGTH + _DOT11I_EAPOL_KEY_MIC_POSITION],
                digest,
                16);
         break;

      case KEY_INFORMATION_VERSION_AES:
         dot11i_hmac_sha1(buffer,
                          DOT1X_EAPOL_FRAME_MIN_LENGTH + _DOT11I_EAPOL_KEY_DATA_POSITION + key_desc->data_length,
                          fsm_auth->PTK,
                          16,
                          digest);
         memcpy(&buffer[DOT1X_EAPOL_FRAME_MIN_LENGTH + _DOT11I_EAPOL_KEY_MIC_POSITION],
                digest,
                16);
         break;
      }
   }

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MESHAP : FLOW : Exit %s: %d\n", __func__, __LINE__);
   return DOT1X_EAPOL_FRAME_MIN_LENGTH + _DOT11I_EAPOL_KEY_DATA_POSITION + key_desc->data_length;
}


int dot11i_eapol_key_frame_process_1(dot11i_authenticator_t *authenticator,
                                     dot1x_eapol_frame_t    *eapol_frame,
                                     dot11i_eapol_key_t     *key_desc)
{
   unsigned char  *p;
   unsigned short s;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MESHAP : FLOW : Enter %s: %d\n", __func__, __LINE__);
   if (eapol_frame->type != DOT1X_EAPOL_FRAME_TYPE_KEY)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESHAP : ERROR : Not an EAPOL Frame key type : %d %s: %d\n", eapol_frame->type,  __func__, __LINE__);
      return -1;
   }

   memset(key_desc, 0, sizeof(dot11i_eapol_key_t));

   p = eapol_frame->body;

   if ((p[_DOT11I_EAPOL_KEY_DESC_TYPE_POSITION] != DOT11I_EAPOL_KEY_TYPE_WPA) &&
       (p[_DOT11I_EAPOL_KEY_DESC_TYPE_POSITION] != DOT11I_EAPOL_KEY_TYPE_11I))
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESHAP : ERROR : IE MISMATCH EID : %d %s: %d\n", p[_DOT11I_EAPOL_KEY_DESC_TYPE_POSITION],  __func__, __LINE__);
      return -2;
   }

   key_desc->type = p[_DOT11I_EAPOL_KEY_DESC_TYPE_POSITION];

   memcpy(&s, &p[_DOT11I_EAPOL_KEY_INFO_POSITION], 2);
   key_desc->information = ntohs(s);

   memcpy(&s, &p[_DOT11I_EAPOL_KEY_LENGTH_POSITION], 2);
   key_desc->length = ntohs(s);

   /**
    * Replay counter is big-endian
    */

#if (DOT1X_IMPL_BYTE_ORDER == DOT1X_LE)
   {
      int i;
      for (i = 0; i < 8; i++)
      {
         key_desc->replay_ctr[7 - i] = p[_DOT11I_EAPOL_KEY_REPLAY_POSITION + i];
      }
   }
#else
   memcpy(key_desc->replay_ctr, &p[_DOT11I_EAPOL_KEY_REPLAY_POSITION], 8);
#endif

   memcpy(key_desc->nonce, &p[_DOT11I_EAPOL_KEY_NONCE_POSITION], 32);

   memcpy(key_desc->iv, &p[_DOT11I_EAPOL_KEY_IV_POSITION], 16);

   /**
    * Key RSC is 802.11 based hence is little endian
    */

   memcpy(key_desc->rsc, &p[_DOT11I_EAPOL_KEY_RSC_POSITION], 8);

   memcpy(key_desc->mic, &p[_DOT11I_EAPOL_KEY_MIC_POSITION], 16);
	dot1x_print(DOT1X_PRINT_LEVEL_INFORMATION, "mic\n");
	_PACKET_DUMP(key_desc->mic, 16);

   memcpy(&s, &p[_DOT11I_EAPOL_KEY_DATA_LENGTH_POSITION], 2);
   key_desc->data_length = ntohs(s);

   key_desc->data = &p[_DOT11I_EAPOL_KEY_DATA_POSITION];

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MESHAP : FLOW : Exit %s: %d\n", __func__, __LINE__);
   return 0;
}


int dot11i_eapol_key_frame_process_2(dot11i_authenticator_t     *authenticator,
                                     dot11i_fsm_authenticator_t *fsm_auth,
                                     dot1x_eapol_frame_t        *eapol_frame,
                                     dot11i_eapol_key_t         *key_desc)
{
   unsigned char digest[160];
   unsigned char *orig_mic;
   int           ret;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MESHAP : FLOW : Enter %s: %d\n", __func__, __LINE__);
   ret = DOT11I_EAPOL_KEY_FRAME_PROCESS_2_OK;

   /**
    * Here we do MIC verification, assuming dot11i_eapol_key_frame_process_1
    * has been called.
    */

   if (KEY_INFORMATION_GET_MIC(key_desc))
   {
      unsigned char *buffer;

      buffer = dot1x_get_2k_buffer();

      dot1x_eapol_create_frame(eapol_frame, buffer);

      /**
       * Zero out the MIC in the packet
       */

      orig_mic = eapol_frame->body + _DOT11I_EAPOL_KEY_MIC_POSITION;

      memset(buffer + DOT1X_EAPOL_FRAME_MIN_LENGTH + _DOT11I_EAPOL_KEY_MIC_POSITION,
             0,
             16);

      switch (KEY_INFORMATION_GET_VERSION(key_desc))
      {
      case KEY_INFORMATION_VERSION_TKIP:
         dot1x_crypt_hmac_md5(authenticator->dot1x_instance,
                              buffer,
                              DOT1X_EAPOL_FRAME_MIN_LENGTH + eapol_frame->body_length,
                              fsm_auth->PTK,
                              16,
                              digest);
         break;

      case KEY_INFORMATION_VERSION_AES:
         dot11i_hmac_sha1(buffer,
                          DOT1X_EAPOL_FRAME_MIN_LENGTH + eapol_frame->body_length,
                          fsm_auth->PTK,
                          16,
                          digest);
         break;
      }

      if (memcmp(orig_mic, digest, 16))
      {
         ret = DOT11I_EAPOL_KEY_FRAME_PROCESS_2_MIC_ERROR;

         /**
          * Use the disabled block below for debugging purposes
          */
         al_print_log(AL_CONTEXT AL_LOG_TYPE_DEBUG, "MESHAP : DEBUG : MIC Mismatch Original MIC %s: %d\n", __func__, __LINE__);
         _PACKET_DUMP(orig_mic, 16);
         al_print_log(AL_CONTEXT AL_LOG_TYPE_DEBUG, "MESHAP : DEBUG : MIC Mismatch Calculated MIC %s: %d\n", __func__, __LINE__);
         _PACKET_DUMP(digest, 16);
      }

      dot1x_release_2k_buffer(buffer);
   }

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MESHAP : FLOW : Exit %s: %d\n", __func__, __LINE__);
   return ret;
}


void dot11i_eapol_key_frame_send(dot1x_port_t               *port,
                                 dot11i_fsm_authenticator_t *fsm_auth,
                                 dot11i_authenticator_t     *authenticator,
                                 int                        msg_code,
                                 unsigned char              Secure,
                                 unsigned char              MICAvailable,
                                 unsigned char              ACKRequired,
                                 unsigned char              Install,
                                 unsigned char              data_flags,
                                 unsigned char              KeyType,
                                 unsigned char              *Nonce,
                                 unsigned char              encrypt)
{
   unsigned char                    *buffer;
   dot11i_eapol_key_t               key_desc;
   dot1x_eapol_frame_t              eapol_desc;
   int                              length;
   unsigned char                    *key_data_buffer;
   int                              key_data_buffer_length;
   al_802_11_security_key_data_t    key_data;
   al_net_if_t                      *net_if;
   access_point_netif_config_info_t *net_if_config_info;
   al_802_11_operations_t           *operations;
   int                              ret;
   dot11i_authenticator_t           *main_authenticator;
   int                              cipher_type;

   main_authenticator = (dot11i_authenticator_t *)port->port_data;
   net_if             = main_authenticator->net_if_config_info->net_if;
   net_if_config_info = main_authenticator->net_if_config_info;
   operations         = (al_802_11_operations_t *)net_if->get_extended_operations(net_if);

   buffer = dot1x_get_2k_buffer();

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MESHAP : FLOW : Enter %s: %d\n", __func__, __LINE__);
   memset(buffer, 0, 2048);
   memset(&key_desc, 0, sizeof(key_desc));

   key_desc.type = fsm_auth->dot11iMode;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MESHAP : INFO : key_desc.type: %d cipher_mode: %d %s: %d\n", key_desc.type, fsm_auth->cipher_mode, __func__, __LINE__);

   switch (fsm_auth->cipher_mode)
   {
   case AL_802_11_SECURITY_INFO_RSN_CIPHER_TKIP:
      KEY_INFORMATION_SET_VERSION(&key_desc, KEY_INFORMATION_VERSION_TKIP);
      if (data_flags & DOT11I_EAPOL_KEY_FRAME_SEND_DATA_FLAG_GTK)
      {
         memcpy(key_desc.iv, main_authenticator->KeyCounter, 16);
         dot11i_initialize_key_counter(main_authenticator);
      }
      break;

   case AL_802_11_SECURITY_INFO_RSN_CIPHER_CCMP:
      KEY_INFORMATION_SET_VERSION(&key_desc, KEY_INFORMATION_VERSION_AES);
      break;
   }

   KEY_INFORMATION_SET_SECURE(&key_desc, Secure);
   KEY_INFORMATION_SET_MIC(&key_desc, MICAvailable);
   KEY_INFORMATION_SET_ACK(&key_desc, ACKRequired);
   KEY_INFORMATION_SET_INSTALL(&key_desc, Install);

   if (data_flags & DOT11I_EAPOL_KEY_FRAME_SEND_DATA_FLAG_GTK)
   {
      KEY_INFORMATION_SET_ENC(&key_desc, 1);
      if (fsm_auth->dot11iMode == DOT11I_EAPOL_KEY_TYPE_WPA)
      {
         KEY_INFORMATION_SET_IDX(&key_desc, net_if_config_info->group_key.key_index);
      }
   }

   if (KeyType == 'P')
   {
      KEY_INFORMATION_SET_TYPE(&key_desc, KEY_INFORMATION_TYPE_PAIRWISE);
      cipher_type = fsm_auth->cipher_mode;
   }
   else
   {
      KEY_INFORMATION_SET_TYPE(&key_desc, KEY_INFORMATION_TYPE_GROUP);
      cipher_type = net_if_config_info->group_key.cipher_type;
   }

   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MESHAP : INFO : cipher_type : %d %s: %d\n", cipher_type,  __func__, __LINE__);
   switch (cipher_type)
   {
   case AL_802_11_SECURITY_INFO_RSN_CIPHER_TKIP:
      key_desc.length = 32;
      break;

   case AL_802_11_SECURITY_INFO_RSN_CIPHER_CCMP:
      key_desc.length = 16;
      break;

   case AL_802_11_SECURITY_INFO_RSN_CIPHER_WEP:
      key_desc.length = net_if_config_info->group_key.strength / 8;
   }


   memcpy(key_desc.replay_ctr, fsm_auth->KeyReplayCounter, 8);
   _incr_replay_counter(port, fsm_auth, authenticator);

   memcpy(key_desc.nonce, Nonce, 32);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MESHAP : INFO : msg_code : %d %s: %d\n", msg_code,  __func__, __LINE__);
   switch (msg_code)
   {
   case DOT11I_EAPOL_KEY_FRAME_SEND_MSG_CODE_4_WAY_3:
   case DOT11I_EAPOL_KEY_FRAME_SEND_MSG_CODE_GROUP_1:
      if (data_flags & DOT11I_EAPOL_KEY_FRAME_SEND_DATA_FLAG_GTK)
      {
         ret = operations->get_security_key_data(net_if, net_if_config_info->group_key.key_index, &key_data, net_if->config.hw_addr.bytes);
         if (ret == 0)
         {
            key_desc.rsc[0] = (unsigned char)(key_data.key_tsc & 0xFF);
            key_desc.rsc[1] = (unsigned char)((key_data.key_tsc >> 8) & 0xFF);
            key_desc.rsc[2] = (unsigned char)((key_data.key_tsc >> 16) & 0xFF);
            key_desc.rsc[3] = (unsigned char)((key_data.key_tsc >> 24) & 0xFF);
            key_desc.rsc[4] = (unsigned char)((key_data.key_tsc >> 32) & 0xFF);
            key_desc.rsc[5] = (unsigned char)((key_data.key_tsc >> 40) & 0xFF);
         }
      }
      break;
   }

   /**
    * Now for the key's data. Typically this would be the RSN IE.
    */

   key_data_buffer = _get_key_data(port, fsm_auth, authenticator, data_flags, &key_data_buffer_length);

   key_desc.data        = key_data_buffer;
   key_desc.data_length = key_data_buffer_length;

   length = dot11i_eapol_key_frame_create(authenticator,
                                          fsm_auth,
                                          &key_desc,
                                          &eapol_desc,
                                          buffer);

   if (key_data_buffer != NULL)
   {
      dot1x_release_2k_buffer(key_data_buffer);
   }

   dot1x_eapol_tx_ex(port, buffer, length, encrypt);

   dot1x_release_2k_buffer(buffer);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MESHAP : FLOW : Exit %s: %d\n", __func__, __LINE__);
}
