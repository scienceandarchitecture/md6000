/********************************************************************************
* MeshDynamics
* --------------
* File     : dot11i-fsm-gtk.c
* Comments : 802.11i GTK FSM
* Created  : 3/31/2005
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  5  |6/16/2008 | Lock primitives changed                         | Sriram |
* -----------------------------------------------------------------------------
* |  4  |7/16/2007 | FSM destroy locking fixed                       | Sriram |
* -----------------------------------------------------------------------------
* |  3  |7/13/2007 | FSM reset if not NULL                           | Sriram |
* -----------------------------------------------------------------------------
* |  2  |4/7/2005  | Misc changes                                    | Sriram |
* -----------------------------------------------------------------------------
* |  1  |4/6/2005  | Misc changes                                    | Sriram |
* -----------------------------------------------------------------------------
* |  0  |3/31/2005 | Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

#include "dot1x-fsm.h"
#include "dot1x-impl.h"
#include "dot1x-fsm-other.h"

#include "dot11i.h"
#include "dot11i-defs.h"
#include "dot11i-impl.h"
#include "dot11i-fsm-gtk.h"
#include "dot11i-fsm-authenticator.h"
#include "dot11i-eapol.h"

/**
 * FSM functions
 */

static int _tick(dot1x_port_t *port, int fsm_code, int other_code, struct dot1x_fsm *fsm);
static int _eapol_rx_begin(dot1x_port_t *port, int fsm_code, int other_code, struct dot1x_fsm *fsm);
static int _eapol_rx(dot1x_port_t *port, int fsm_code, int other_code, struct dot1x_fsm *fsm, dot1x_eapol_frame_t *frame);
static int _eapol_rx_end(dot1x_port_t *port, int fsm_code, int other_code, struct dot1x_fsm *fsm);
static int _trigger(dot1x_port_t *port, int fsm_code, int other_code, struct dot1x_fsm *fsm, int trigger_code);
static int _initialize(dot1x_port_t *port, int fsm_code, int other_code, struct dot1x_fsm *fsm);
static int _on_destroy(dot1x_port_t *port, struct dot1x_fsm *fsm);

/**
 * Internal functions
 */

static void _on_state_entry(dot1x_port_t *port, dot11i_fsm_gtk_t *fsm_gtk, dot1x_fsm_t **trigger_fsm, int *fsm_code, int *other_code, int *trigger_code);
static void _attempt_transition(dot1x_port_t *port, dot11i_fsm_gtk_t *fsm_gtk, dot1x_fsm_t **trigger_fsm, int *fsm_code, int *other_code, int *trigger_code);

int dot11i_fsm_gtk_create(dot1x_port_t *port, int max_timeouts, int dot11imode)
{
   dot11i_fsm_gtk_t *fsm_gtk;
   int              err;

	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MESHAP : FLOW : Enter %s: %d\n", __func__, __LINE__);
   fsm_gtk = (dot11i_fsm_gtk_t *)dot1x_fsm_get(port, DOT1X_FSM_CODE_OTHER, DOT1X_FSM_OTHER_CODE_DOT11I_GTK);

   if (fsm_gtk != NULL)
   {
      fsm_gtk->state       = DOT11I_FSM_GTK_STATE_INVALID;
      fsm_gtk->HoldDestroy = 0;
      fsm_gtk->Destroyed   = 0;

      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MESHAP : INFO : dot11i-fsm-gtk: FSM reset for %s:%02x-%02x-%02x-%02x-%02x-%02x %s : %d\n",
                  port->dot1x_instance->instance_name,
                  port->address[0],
                  port->address[1],
                  port->address[2],
                  port->address[3],
                  port->address[4],
                  port->address[5], __func__, __LINE__);
      return 0;
   }

   fsm_gtk = (dot11i_fsm_gtk_t *)dot1x_heap_alloc(sizeof(dot11i_fsm_gtk_t));

   memset(fsm_gtk, 0, sizeof(dot11i_fsm_gtk_t));

   fsm_gtk->dot11iMode   = dot11imode;
   fsm_gtk->max_timeouts = max_timeouts;

   fsm_gtk->fsm.initialize     = _initialize;
   fsm_gtk->fsm.tick           = _tick;
   fsm_gtk->fsm.eapol_rx_begin = _eapol_rx_begin;
   fsm_gtk->fsm.eapol_rx       = _eapol_rx;
   fsm_gtk->fsm.eapol_rx_end   = _eapol_rx_end;
   fsm_gtk->fsm.trigger        = _trigger;
   fsm_gtk->fsm.on_destroy     = _on_destroy;
   fsm_gtk->HoldDestroy        = 0;
   fsm_gtk->Destroyed          = 0;

   fsm_gtk->lock  = dot1x_create_lock();
   fsm_gtk->state = DOT11I_FSM_GTK_STATE_INVALID;

   fsm_gtk->key_desc = (dot11i_eapol_key_t *)dot1x_heap_alloc(sizeof(dot11i_eapol_key_t));

   err = dot1x_fsm_register(port, DOT1X_FSM_CODE_OTHER, DOT1X_FSM_OTHER_CODE_DOT11I_GTK, &fsm_gtk->fsm);

   if (err)
   {
      dot1x_destroy_lock(fsm_gtk->lock);
      dot1x_heap_free(fsm_gtk->key_desc);
      dot1x_heap_free(fsm_gtk);
	   al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESHAP : ERROR : err : %d  %s: %d\n", err, __func__, __LINE__);
      return err;
   }

   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MESHAP : INFO : dot11i-fsm-gtk: FSM initialized for %s:%02x-%02x-%02x-%02x-%02x-%02x %s : %d\n",
               port->dot1x_instance->instance_name,
               port->address[0],
               port->address[1],
               port->address[2],
               port->address[3],
               port->address[4],
               port->address[5], __func__, __LINE__);

	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MESHAP : FLOW : Exit %s: %d\n", __func__, __LINE__);
   return err;
}


int dot11i_fsm_gtk_destroy(dot1x_port_t *port)
{
   dot1x_fsm_t *fsm;

	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MESHAP : FLOW : Enter %s: %d\n", __func__, __LINE__);
   fsm = dot1x_fsm_get(port, DOT1X_FSM_CODE_OTHER, DOT1X_FSM_OTHER_CODE_DOT11I_GTK);

   if (fsm != NULL)
   {
      _on_destroy(port, fsm);
   }

   dot1x_fsm_unregister(port, DOT1X_FSM_CODE_OTHER, DOT1X_FSM_OTHER_CODE_DOT11I_GTK);
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MESHAP : FLOW : Exit %s: %d\n", __func__, __LINE__);

   return 0;
}


dot11i_fsm_gtk_t *dot11i_fsm_gtk_get(dot1x_port_t *port)
{
   dot11i_fsm_gtk_t *fsm_gtk;

	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MESHAP : FLOW : Enter %s: %d\n", __func__, __LINE__);
   fsm_gtk = (dot11i_fsm_gtk_t *)dot1x_fsm_get(port,
                                               DOT1X_FSM_CODE_OTHER,
                                               DOT1X_FSM_OTHER_CODE_DOT11I_GTK);
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MESHAP : FLOW : Exit %s: %d\n", __func__, __LINE__);

   return fsm_gtk;
}


int dot11i_fsm_gtk_tick(dot1x_port_t *port)
{
   dot11i_fsm_gtk_t *fsm_gtk;
   unsigned int     lock_code;

	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MESHAP : FLOW : Enter %s: %d\n", __func__, __LINE__);
   fsm_gtk = (dot11i_fsm_gtk_t *)dot1x_fsm_get(port,
                                               DOT1X_FSM_CODE_OTHER,
                                               DOT1X_FSM_OTHER_CODE_DOT11I_GTK);

   if (fsm_gtk == NULL)
   {
	   al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESHAP : ERROR : fsm_gtk is NULL %s: %d\n", __func__, __LINE__);
      return -1;
   }

   dot1x_lock(fsm_gtk->lock, lock_code);

   fsm_gtk->trigger_fsm = NULL;

   ++fsm_gtk->tick_count;

   if (fsm_gtk->tick_count > 7)
   {
      fsm_gtk->TimeoutEvt = True;
   }

   fsm_gtk->HoldDestroy = 1;

   _attempt_transition(port,
                       fsm_gtk,
                       &fsm_gtk->trigger_fsm,
                       &fsm_gtk->trigger_fsm_code,
                       &fsm_gtk->trigger_other_code,
                       &fsm_gtk->trigger_code);

   if (fsm_gtk->trigger_fsm != NULL)
   {
      fsm_gtk->trigger_fsm->trigger(port,
                                    fsm_gtk->trigger_fsm_code,
                                    fsm_gtk->trigger_other_code,
                                    fsm_gtk->trigger_fsm,
                                    fsm_gtk->trigger_code);
   }

   fsm_gtk->HoldDestroy = 0;

   dot1x_unlock(fsm_gtk->lock, lock_code);

   if (fsm_gtk->Destroyed)
   {
      _on_destroy(port, (dot1x_fsm_t *)fsm_gtk);
   }

	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MESHAP : FLOW : Exit %s: %d\n", __func__, __LINE__);
   return 0;
}


static int _tick(dot1x_port_t *port, int fsm_code, int other_code, struct dot1x_fsm *fsm)
{
   /** Unused */
   return 0;
}


static int _eapol_rx_begin(dot1x_port_t *port, int fsm_code, int other_code, struct dot1x_fsm *fsm)
{
   dot11i_fsm_gtk_t *fsm_gtk;
   unsigned int     lock_code;

   fsm_gtk = (dot11i_fsm_gtk_t *)fsm;

	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MESHAP : FLOW : Enter %s: %d\n", __func__, __LINE__);
   dot1x_lock(fsm_gtk->lock, lock_code);

   fsm_gtk->trigger_fsm = NULL;

   dot1x_unlock(fsm_gtk->lock, lock_code);
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MESHAP : FLOW : Exit %s: %d\n", __func__, __LINE__);

   return 0;
}


static int _eapol_rx(dot1x_port_t *port, int fsm_code, int other_code, struct dot1x_fsm *fsm, dot1x_eapol_frame_t *frame)
{
   dot11i_fsm_gtk_t           *fsm_gtk;
   dot11i_authenticator_t     *authenticator;
   dot11i_fsm_authenticator_t *fsm_auth;
   int          ret;
   unsigned int lock_code;

   fsm_gtk       = (dot11i_fsm_gtk_t *)fsm;
   authenticator = (dot11i_authenticator_t *)port->dot1x_instance->instance_data;
   fsm_auth      = dot11i_fsm_authenticator_get(port);

	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MESHAP : FLOW : Enter %s: %d\n", __func__, __LINE__);
   dot1x_lock(fsm_gtk->lock, lock_code);

   if (fsm_gtk->GUpdateStationKeys == False)
   {
      dot1x_unlock(fsm_gtk->lock, lock_code);
	   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MESHAP : INFO : GUpdateStationKeys set to false %s: %d\n", __func__, __LINE__);
      return 0;
   }

   if (frame->type == DOT1X_EAPOL_FRAME_TYPE_KEY)
   {
      ret = dot11i_eapol_key_frame_process_1(authenticator, frame, fsm_gtk->key_desc);
      if (ret == 0)
      {
         fsm_gtk->EAPOLKeyReceived            = True;
         fsm_gtk->EAPOLKeyReceivedRequest     = KEY_INFORMATION_GET_REQ(fsm_gtk->key_desc);
         fsm_gtk->EAPOLKeyReceivedK           = KEY_INFORMATION_GET_TYPE(fsm_gtk->key_desc);
         fsm_gtk->EAPOLKeyReceivedMICVerified = False;

         if (KEY_INFORMATION_GET_MIC(fsm_gtk->key_desc))
         {
            ret = dot11i_eapol_key_frame_process_2(authenticator, fsm_auth, frame, fsm_gtk->key_desc);
            if (ret == DOT11I_EAPOL_KEY_FRAME_PROCESS_2_OK)
            {
               fsm_gtk->EAPOLKeyReceivedMICVerified = True;
            }
         }

         _attempt_transition(port,
                             fsm_gtk,
                             &fsm_gtk->trigger_fsm,
                             &fsm_gtk->trigger_fsm_code,
                             &fsm_gtk->trigger_other_code,
                             &fsm_gtk->trigger_code);

         fsm_gtk->EAPOLKeyReceived = False;
      }
   }

   dot1x_unlock(fsm_gtk->lock, lock_code);
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MESHAP : FLOW : Exit %s: %d \n", __func__, __LINE__);

   return 0;
}


static int _eapol_rx_end(dot1x_port_t *port, int fsm_code, int other_code, struct dot1x_fsm *fsm)
{
   dot11i_fsm_gtk_t *fsm_gtk;
   unsigned int     lock_code;

   fsm_gtk = (dot11i_fsm_gtk_t *)fsm;

	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MESHAP : FLOW : Enter %s: %d \n", __func__, __LINE__);
   dot1x_lock(fsm_gtk->lock, lock_code);

   if (fsm_gtk->trigger_fsm != NULL)
   {
      fsm_gtk->trigger_fsm->trigger(port,
                                    fsm_gtk->trigger_fsm_code,
                                    fsm_gtk->trigger_other_code,
                                    fsm_gtk->trigger_fsm,
                                    fsm_gtk->trigger_code);
   }

   dot1x_unlock(fsm_gtk->lock, lock_code);

	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MESHAP : FLOW : Exit %s: %d \n", __func__, __LINE__);
   return 0;
}

static int _trigger(dot1x_port_t *port, int fsm_code, int other_code, struct dot1x_fsm *fsm, int trigger_code)
{
   dot11i_fsm_gtk_t *fsm_gtk;
   dot1x_fsm_t      *trigger_fsm;
   int              trigger_fsm_code;
   int              new_trigger_code;
   int              trigger_other_code;
   unsigned int     lock_code;

   fsm_gtk     = (dot11i_fsm_gtk_t *)fsm;
   trigger_fsm = NULL;

	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MESHAP : FLOW : Enter %s: %d \n", __func__, __LINE__);
   dot1x_lock(fsm_gtk->lock, lock_code);

	al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MESHAP : INFO : trigger_code : %d %s: %d \n", trigger_code, __func__, __LINE__);
   switch (trigger_code)
   {
   case DOT11I_FSM_GTK_TRIGGER_AUTH:
      fsm_gtk->state = DOT11I_FSM_GTK_STATE_IDLE;
      _on_state_entry(port, fsm_gtk, &trigger_fsm, &trigger_fsm_code, &trigger_other_code, &new_trigger_code);
      break;

   case DOT11I_FSM_GTK_TRIGGER_START:
      fsm_gtk->GUpdateStationKeys = True;
      _attempt_transition(port, fsm_gtk, &trigger_fsm, &trigger_fsm_code, &trigger_other_code, &new_trigger_code);
      break;
   }

   dot1x_unlock(fsm_gtk->lock, lock_code);

   if (trigger_fsm != NULL)
   {
      trigger_fsm->trigger(port, trigger_fsm_code, trigger_other_code, trigger_fsm, new_trigger_code);
   }

	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MESHAP : FLOW : Exit %s: %d \n", __func__, __LINE__);
   return 0;
}

static int _initialize(dot1x_port_t *port, int fsm_code, int other_code, struct dot1x_fsm *fsm)
{
   dot11i_fsm_gtk_t *fsm_gtk;
   unsigned int     lock_code;

   fsm_gtk = (dot11i_fsm_gtk_t *)fsm;

	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MESHAP : FLOW : Enter %s: %d \n", __func__, __LINE__);
   dot1x_lock(fsm_gtk->lock, lock_code);

   fsm_gtk->state = DOT11I_FSM_GTK_STATE_IDLE;

   _on_state_entry(port,
                   fsm_gtk,
                   &fsm_gtk->trigger_fsm,
                   &fsm_gtk->trigger_fsm_code,
                   &fsm_gtk->trigger_other_code,
                   &fsm_gtk->trigger_code);

   dot1x_unlock(fsm_gtk->lock, lock_code);
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MESHAP : FLOW : Exit %s: %d \n", __func__, __LINE__);

   return 0;
}


static int _on_destroy(dot1x_port_t *port, struct dot1x_fsm *fsm)
{
   dot11i_fsm_gtk_t *fsm_gtk;

   fsm_gtk = (dot11i_fsm_gtk_t *)fsm;
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MESHAP : FLOW : Enter %s: %d \n", __func__, __LINE__);

   if (fsm_gtk != NULL)
   {
      if (fsm_gtk->HoldDestroy)
      {
         fsm_gtk->Destroyed = 1;
         al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MESHAP : INFO : dot11i-fsm-gtk: Holding off FSM destruction for %s:%02x-%02x-%02x-%02x-%02x-%02x %s : %d\n",
                     port->dot1x_instance->instance_name,
                     port->address[0],
                     port->address[1],
                     port->address[2],
                     port->address[3],
                     port->address[4],
                     port->address[5], __func__, __LINE__);
      }
      else
      {
         al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MESHAP : INFO : dot11i-fsm-gtk: FSM destroyed for %s:%02x-%02x-%02x-%02x-%02x-%02x %s : %d\n",
                     port->dot1x_instance->instance_name,
                     port->address[0],
                     port->address[1],
                     port->address[2],
                     port->address[3],
                     port->address[4],
                     port->address[5], __func__, __LINE__);
         dot1x_destroy_lock(fsm_gtk->lock);
         dot1x_heap_free(fsm_gtk->key_desc);
         dot1x_heap_free(fsm_gtk);
      }
   }
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MESHAP : FLOW : Exit %s: %d \n", __func__, __LINE__);

   return 0;
}


static void _on_state_entry(dot1x_port_t *port, dot11i_fsm_gtk_t *fsm_gtk, dot1x_fsm_t **trigger_fsm, int *fsm_code, int *other_code, int *trigger_code)
{
   dot11i_authenticator_t     *authenticator;
   dot11i_authenticator_t     *main_authenticator;
   dot11i_fsm_authenticator_t *fsm_auth;

	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MESHAP : FLOW : Enter %s: %d \n", __func__, __LINE__);
   authenticator = (dot11i_authenticator_t *)port->dot1x_instance->instance_data;
   fsm_auth      = dot11i_fsm_authenticator_get(port);

   if (authenticator->net_if_config_info->net_if == NULL)
   {
      main_authenticator = (dot11i_authenticator_t *)port->port_data;
   }
   else
   {
      main_authenticator = authenticator;
   }

   switch (fsm_gtk->state)
   {
   case DOT11I_FSM_GTK_STATE_IDLE:
      fsm_gtk->TimeoutCtr         = 0;
      fsm_gtk->GUpdateStationKeys = False;
      fsm_gtk->EAPOLKeyReceived   = False;
      fsm_gtk->TimeoutEvt         = False;
      break;

   case DOT11I_FSM_GTK_STATE_REKEYNEGOTIATING:

      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MESHAP : INFO : dot11i-fsm-gtk: Transmitting Group key msg 1 to %s:%02x-%02x-%02x-%02x-%02x-%02x%s : %d\n",
                  port->dot1x_instance->instance_name,
                  port->address[0],
                  port->address[1],
                  port->address[2],
                  port->address[3],
                  port->address[4],
                  port->address[5], __func__, __LINE__);

      fsm_gtk->tick_count = 0;

      dot11i_eapol_key_frame_send(port,
                                  fsm_auth,
                                  authenticator,
                                  DOT11I_EAPOL_KEY_FRAME_SEND_MSG_CODE_GROUP_1,
                                  1,
                                  1,
                                  1,
                                  0,
                                  DOT11I_EAPOL_KEY_FRAME_SEND_DATA_FLAG_GTK,
                                  'G',
                                  main_authenticator->GNonce,
                                  1);

      fsm_gtk->EAPOLKeyReceived = False;
      fsm_gtk->TimeoutEvt       = False;

      ++fsm_gtk->TimeoutCtr;
      break;

   case DOT11I_FSM_GTK_STATE_KEYERROR:
      *trigger_fsm   = &fsm_auth->fsm;
      *fsm_code      = DOT1X_FSM_CODE_OTHER;
      *other_code    = DOT1X_FSM_OTHER_CODE_DOT11I_AUTH;
      *trigger_code  = DOT11I_FSM_AUTHENTICATOR_TRIGGER_DISCONNECT;
      fsm_gtk->state = DOT11I_FSM_GTK_STATE_IDLE;
      _on_state_entry(port, fsm_gtk, trigger_fsm, fsm_code, other_code, trigger_code);
      break;

   case DOT11I_FSM_GTK_STATE_REKEYESTABLISHED:
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MESHAP : INFO : dot11i-fsm-gtk: Group key established for %s:%02x-%02x-%02x-%02x-%02x-%02x %s : %d\n",
                  port->dot1x_instance->instance_name,
                  port->address[0],
                  port->address[1],
                  port->address[2],
                  port->address[3],
                  port->address[4],
                  port->address[5], __func__, __LINE__);
      fsm_gtk->state = DOT11I_FSM_GTK_STATE_IDLE;
      _on_state_entry(port, fsm_gtk, trigger_fsm, fsm_code, other_code, trigger_code);
      break;
   }
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MESHAP : FLOW : Exit %s: %d \n", __func__, __LINE__);
}


static void _attempt_transition(dot1x_port_t *port, dot11i_fsm_gtk_t *fsm_gtk, dot1x_fsm_t **trigger_fsm, int *fsm_code, int *other_code, int *trigger_code)
{
   dot11i_authenticator_t *authenticator;

	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MESHAP : FLOW : Enter %s: %d\n", __func__, __LINE__);
   authenticator = (dot11i_authenticator_t *)port->dot1x_instance->instance_data;

	al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MESHAP : INFO : fsm_gtk->state: %d %s: %d\n", fsm_gtk->state, __func__, __LINE__);
   switch (fsm_gtk->state)
   {
   case DOT11I_FSM_GTK_STATE_IDLE:
      if (fsm_gtk->GUpdateStationKeys)
      {
         fsm_gtk->state = DOT11I_FSM_GTK_STATE_REKEYNEGOTIATING;
         _on_state_entry(port, fsm_gtk, trigger_fsm, fsm_code, other_code, trigger_code);
      }
      break;

   case DOT11I_FSM_GTK_STATE_REKEYNEGOTIATING:
      if (fsm_gtk->TimeoutEvt)
      {
         fsm_gtk->TimeoutEvt = False;
         ++fsm_gtk->TimeoutCtr;
         if (fsm_gtk->TimeoutCtr > fsm_gtk->max_timeouts)
         {
            fsm_gtk->state = DOT11I_FSM_GTK_STATE_KEYERROR;
            _on_state_entry(port, fsm_gtk, trigger_fsm, fsm_code, other_code, trigger_code);
         }
         else
         {
            fsm_gtk->state = DOT11I_FSM_GTK_STATE_REKEYNEGOTIATING;
            _on_state_entry(port, fsm_gtk, trigger_fsm, fsm_code, other_code, trigger_code);
         }
      }
      else if (fsm_gtk->EAPOLKeyReceived &&
               !fsm_gtk->EAPOLKeyReceivedRequest &&
               (fsm_gtk->EAPOLKeyReceivedK == KEY_INFORMATION_TYPE_GROUP))
      {
         fsm_gtk->state = DOT11I_FSM_GTK_STATE_REKEYESTABLISHED;
         _on_state_entry(port, fsm_gtk, trigger_fsm, fsm_code, other_code, trigger_code);
      }
      break;

   case DOT11I_FSM_GTK_STATE_KEYERROR:
      break;

   case DOT11I_FSM_GTK_STATE_REKEYESTABLISHED:
      break;
   }
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MESHAP : FLOW : Exit %s: %d \n", __func__, __LINE__);
}
