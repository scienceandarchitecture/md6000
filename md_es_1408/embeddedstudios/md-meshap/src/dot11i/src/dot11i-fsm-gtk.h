/********************************************************************************
* MeshDynamics
* --------------
* File     : dot11i-fsm-gtk.h
* Comments : 802.11i per STA group key FSM
* Created  : 3/31/2005
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  1  |7/16/2007 | FSM destroy locking fixed                       | Sriram |
* -----------------------------------------------------------------------------
* |  0  |3/31/2005 | Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

#ifndef __DOT11I_FSM_GTK_H__
#define __DOT11I_FSM_GTK_H__

#define DOT11I_FSM_GTK_STATE_INVALID             0
#define DOT11I_FSM_GTK_STATE_IDLE                1
#define DOT11I_FSM_GTK_STATE_REKEYNEGOTIATING    2
#define DOT11I_FSM_GTK_STATE_KEYERROR            3
#define DOT11I_FSM_GTK_STATE_REKEYESTABLISHED    4

struct dot11i_eapol_key;

struct dot11i_fsm_gtk
{
   dot1x_fsm_t             fsm;
   unsigned int            lock;
   int                     state;
   int                     dot11iMode;                                                                                  /** DOT11I_EAPOL_KEY_TYPE_* */
   int                     max_timeouts;
   struct dot11i_eapol_key *key_desc;
   int                     tick_count;                                                                                  /** For measuring timeouts */
   int                     HoldDestroy;
   int                     Destroyed;

   int                     GUpdateStationKeys;
   int                     TimeoutCtr;
   int                     TimeoutEvt;
   int                     EAPOLKeyReceived;
   int                     EAPOLKeyReceivedRequest;                                             /** Valid only when EAPOLKeyReceived=True */
   int                     EAPOLKeyReceivedK;                                                   /** Valid only when EAPOLKeyReceived=True */
   int                     EAPOLKeyReceivedMICVerified;                                         /** Valid only when EAPOLKeyReceived=True */


   dot1x_fsm_t             *trigger_fsm;
   int                     trigger_fsm_code;
   int                     trigger_other_code;
   int                     trigger_code;
};

typedef struct dot11i_fsm_gtk   dot11i_fsm_gtk_t;

int dot11i_fsm_gtk_create(dot1x_port_t *port,
                          int          max_timeouts,
                          int          dot11imode);

int dot11i_fsm_gtk_destroy(dot1x_port_t *port);

dot11i_fsm_gtk_t *dot11i_fsm_gtk_get(dot1x_port_t *port);

int dot11i_fsm_gtk_tick(dot1x_port_t *port);


#define DOT11I_FSM_GTK_TRIGGER_AUTH     1
#define DOT11I_FSM_GTK_TRIGGER_START    2


#endif /*__DOT11I_FSM_GTK_H__*/
