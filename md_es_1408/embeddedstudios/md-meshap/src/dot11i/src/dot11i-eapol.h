/********************************************************************************
* MeshDynamics
* --------------
* File     : dot11i-eapol.h
* Comments : WPA/802.11i EAPOL Frame
* Created  : 3/6/2005
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  0  |3/6/2005  | Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

#include "dot1x-eapol.h"

#ifndef __DOT11I_EAPOL_H__
#define __DOT11I_EAPOL_H__

#define DOT11I_ROUNDUP(x, y)        ((((x) + ((y) - 1)) / (y)) * (y))
#define DOT11I_ROUNDUP_PAD(x, y)    (DOT11I_ROUNDUP(x, y) - (x))

#define DOT11I_EAPOL_KEY_TYPE_WPA    254
#define DOT11I_EAPOL_KEY_TYPE_11I    2

/**
 *
 * The two byte information field is split into various bits shown below
 *
 * 16  15  14  13  12  11  10  9   8   7   6   5   4   3   2   1   0
 * +---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+
 * |  RESERVED |ENC|REQ|ERR| S | M | A | I | RESRVD| T |    VER    |
 * +---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+
 *
 */

#define KEY_INFORMATION_GET_VERSION(key)    ((key)->information & 0x7)
#define KEY_INFORMATION_GET_TYPE(key)       (((key)->information >> 3) & 0x1)
#define KEY_INFORMATION_GET_INSTALL(key)    (((key)->information >> 6) & 0x1)
#define KEY_INFORMATION_GET_ACK(key)        (((key)->information >> 7) & 0x1)
#define KEY_INFORMATION_GET_MIC(key)        (((key)->information >> 8) & 0x1)
#define KEY_INFORMATION_GET_SECURE(key)     (((key)->information >> 9) & 0x1)
#define KEY_INFORMATION_GET_ERR(key)        (((key)->information >> 10) & 0x1)
#define KEY_INFORMATION_GET_REQ(key)        (((key)->information >> 11) & 0x1)
#define KEY_INFORMATION_GET_ENC(key)        (((key)->information >> 12) & 0x1)
#define KEY_INFORMATION_GET_IDX(key)        (((key)->information >> 4) & 0x03)

#define KEY_INFORMATION_SET_VERSION(key, ver) \
   do {                                       \
      (key)->information &= 0xFFF8;           \
      (key)->information |= (ver) & 0x7;      \
   } while (0)

#define KEY_INFORMATION_SET_TYPE(key, type)        \
   do {                                            \
      (key)->information &= 0xFFF7;                \
      (key)->information |= (((type) & 0x1) << 3); \
   } while (0)

#define KEY_INFORMATION_SET_INSTALL(key, install)     \
   do {                                               \
      (key)->information &= 0xFFBF;                   \
      (key)->information |= (((install) & 0x1) << 6); \
   } while (0)

#define KEY_INFORMATION_SET_ACK(key, ack)         \
   do {                                           \
      (key)->information &= 0xFF7F;               \
      (key)->information |= (((ack) & 0x1) << 7); \
   } while (0)

#define KEY_INFORMATION_SET_MIC(key, mic)         \
   do {                                           \
      (key)->information &= 0xFEFF;               \
      (key)->information |= (((mic) & 0x1) << 8); \
   } while (0)

#define KEY_INFORMATION_SET_SECURE(key, secure)      \
   do {                                              \
      (key)->information &= 0xFDFF;                  \
      (key)->information |= (((secure) & 0x1) << 9); \
   } while (0)

#define KEY_INFORMATION_SET_ERR(key, err)          \
   do {                                            \
      (key)->information &= 0xFBFF;                \
      (key)->information |= (((err) & 0x1) << 10); \
   } while (0)

#define KEY_INFORMATION_SET_REQ(key, req)          \
   do {                                            \
      (key)->information &= 0xF7FF;                \
      (key)->information |= (((req) & 0x1) << 11); \
   } while (0)

#define KEY_INFORMATION_SET_ENC(key, enc)          \
   do {                                            \
      (key)->information &= 0xEFFF;                \
      (key)->information |= (((enc) & 0x1) << 12); \
   } while (0)

#define KEY_INFORMATION_SET_IDX(key, idx)          \
   do {                                            \
      (key)->information &= 0xFFCF;                \
      (key)->information |= (((idx) & 0x03) << 4); \
   } while (0)

#define KEY_INFORMATION_VERSION_TKIP     1
#define KEY_INFORMATION_VERSION_AES      2

#define KEY_INFORMATION_TYPE_GROUP       0
#define KEY_INFORMATION_TYPE_PAIRWISE    1

struct dot11i_eapol_key
{
   unsigned char  type;
   unsigned short information;
   unsigned short length;
   unsigned char  replay_ctr[8];
   unsigned char  nonce[32];
   unsigned char  iv[16];
   unsigned char  rsc[8];
   unsigned char  reserved[8];
   unsigned char  mic[16];
   unsigned short data_length;
   unsigned char  *data;
};

typedef struct dot11i_eapol_key   dot11i_eapol_key_t;

/**
 * The create function, creates a full EAPOL frame containing the WPA/11i key
 * It also calculates the MIC, encrypts the key if requested.
 */

int dot11i_eapol_key_frame_create(dot11i_authenticator_t     *authenticator,
                                  dot11i_fsm_authenticator_t *fsm_auth,
                                  dot11i_eapol_key_t         *key_desc,
                                  dot1x_eapol_frame_t        *eapol_frame,
                                  unsigned char              *buffer);

/**
 * The process_1 function processes a eapol key frame buffer, and only
 * fills up the dot11i_eapol_key_t structure, without doing any
 * MIC or decryption. This enables the caller to atleast get basic
 * fields out of the frame.
 */

int dot11i_eapol_key_frame_process_1(dot11i_authenticator_t *authenticator,
                                     dot1x_eapol_frame_t    *eapol_frame,
                                     dot11i_eapol_key_t     *key_desc);


/**
 * The process_2 function processes the dot11i_eapol_key_t structure
 * after the PTK is known. This time it verifies the MIC and also
 * decrypts the data if needed.
 */

#define DOT11I_EAPOL_KEY_FRAME_PROCESS_2_OK           0
#define DOT11I_EAPOL_KEY_FRAME_PROCESS_2_MIC_ERROR    -1
#define DOT11I_EAPOL_KEY_FRAME_PROCESS_2_ENC_ERROR    -2

int dot11i_eapol_key_frame_process_2(dot11i_authenticator_t     *authenticator,
                                     dot11i_fsm_authenticator_t *fsm_auth,
                                     dot1x_eapol_frame_t        *eapol_frame,
                                     dot11i_eapol_key_t         *key_desc);


/**
 * Transmits a EAPOL-Key frame
 */


#define DOT11I_EAPOL_KEY_FRAME_SEND_MSG_CODE_4_WAY_1     1
#define DOT11I_EAPOL_KEY_FRAME_SEND_MSG_CODE_4_WAY_3     2
#define DOT11I_EAPOL_KEY_FRAME_SEND_MSG_CODE_GROUP_1     3

#define DOT11I_EAPOL_KEY_FRAME_SEND_DATA_FLAG_RSN_IE     1
#define DOT11I_EAPOL_KEY_FRAME_SEND_DATA_FLAG_GTK        2
#define DOT11I_EAPOL_KEY_FRAME_SEND_DATA_FLAG_PMKID      4
#define DOT11I_EAPOL_KEY_FRAME_SEND_DATA_FLAG_MACADDR    8


void dot11i_eapol_key_frame_send(dot1x_port_t               *port,
                                 dot11i_fsm_authenticator_t *fsm_auth,
                                 dot11i_authenticator_t     *authenticator,
                                 int                        msg_code,
                                 unsigned char              Secure,
                                 unsigned char              MICAvailable,
                                 unsigned char              ACKRequired,
                                 unsigned char              Install,
                                 unsigned char              data_flags,
                                 unsigned char              KeyType,
                                 unsigned char              *Nonce,
                                 unsigned char              encrypt);
#endif /*__DOT11I-EAPOL_H__*/
