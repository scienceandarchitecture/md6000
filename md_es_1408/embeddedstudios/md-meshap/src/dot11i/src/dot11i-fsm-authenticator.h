/********************************************************************************
* MeshDynamics
* --------------
* File     : dot11i-fsm-authenticator.h
* Comments : WPA/802.11i Authenticator FSM
* Created  : 3/5/2005
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  2  |7/16/2007 | Destroy locking problem fixed                   | Sriram |
* -----------------------------------------------------------------------------
* |  1  |12/6/2006 | Changes for In-direct VLAN (NPS)                | Sriram |
* -----------------------------------------------------------------------------
* |  0  |3/5/2005  | Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

#ifndef __DOT11I_FSM_AUTHENTICATOR_H__
#define __DOT11I_FSM_AUTHENTICATOR_H__

#define DOT11I_FSM_AUTHENTICATOR_STATE_INVALID               0
#define DOT11I_FSM_AUTHENTICATOR_STATE_AUTHENTICATION        1
#define DOT11I_FSM_AUTHENTICATOR_STATE_AUTHENTICATION2       2
#define DOT11I_FSM_AUTHENTICATOR_STATE_INITPMK               3
#define DOT11I_FSM_AUTHENTICATOR_STATE_INITPSK               4
#define DOT11I_FSM_AUTHENTICATOR_STATE_PTKSTART              5
#define DOT11I_FSM_AUTHENTICATOR_STATE_PTKCALNEGOTIATING     6
#define DOT11I_FSM_AUTHENTICATOR_STATE_PTKCALNEGOTIATING2    7
#define DOT11I_FSM_AUTHENTICATOR_STATE_PTKINITNEGOTIATING    8
#define DOT11I_FSM_AUTHENTICATOR_STATE_PTKINITDONE           9
#define DOT11I_FSM_AUTHENTICATOR_STATE_DISCONNECT            10
#define DOT11I_FSM_AUTHENTICATOR_STATE_DISCONNECTED          11
#define DOT11I_FSM_AUTHENTICATOR_STATE_INITIALIZE            12

#define DOT11I_FSM_AUTHENTICATOR_FLAGS_PMK_VALID             1
#define DOT11I_FSM_AUTHENTICATOR_FLAGS_PTK_VALID             2
#define DOT11I_FSM_AUTHENTICATOR_FLAGS_AAAK_VALID            4

struct dot11i_eapol_key;

struct dot11i_fsm_authenticator
{
   dot1x_fsm_t             fsm;
   unsigned int            lock;
   int                     state;
   int                     dot11iMode;                                                                          /** DOT11I_EAPOL_KEY_TYPE_* */
   int                     cipher_mode;                                                                         /** Selected Cipher from list of supported ones */
   int                     auth_mode;                                                                           /** PSK or 802.1x */
   int                     max_timeouts;
   unsigned int            flags;
   int                     tick_count;                                                                                  /** For measuring timeouts */
   int                     AuthenticationRequest;
   int                     ReAuthenticationRequest;
   int                     DeAuthenticationRequest;
   int                     Disconnect;
   int                     HoldDestroy;
   int                     Destroyed;

   int                     EAPOLKeyReceived;
   int                     EAPOLKeyReceivedRequest;                                     /** Valid only when EAPOLKeyReceived=True */
   int                     EAPOLKeyReceivedK;                                           /** Valid only when EAPOLKeyReceived=True */
   dot1x_eapol_frame_t     *eapol_frame;                                                /** Valid only when EAPOLKeyReceived=True */

   int                     TimeoutEvt;
   int                     TimeoutCtr;
   unsigned char           ANonce[32];
   unsigned char           SNonce[32];
   unsigned char           PTK[64];
   unsigned char           PMK[32];
   unsigned char           KeyReplayCounter[8];
   unsigned char           AAA_Key[32];
   unsigned char           PSK[32];
   short                   VLan_Tag;                                                                            /** Vlan Tag as returned by Radius server, (0-4095, -1=no tag) */
   struct dot11i_eapol_key *key_desc;
   dot1x_fsm_t             *trigger_fsm;
   int                     trigger_fsm_code;
   int                     trigger_other_code;
   int                     trigger_code;
};

typedef struct dot11i_fsm_authenticator   dot11i_fsm_authenticator_t;

int dot11i_fsm_authenticator_create(dot1x_port_t *port,
                                    int          dot11imode,
                                    int          cipher_mode,
                                    int          auth_mode,
                                    int          max_timeouts);

int dot11i_fsm_authenticator_destroy(dot1x_port_t *port);

dot11i_fsm_authenticator_t *dot11i_fsm_authenticator_get(dot1x_port_t *port);

int dot11i_fsm_authenticator_tick(dot1x_port_t *port);

struct dot11i_fsm_authenticator_key
{
   unsigned char key[32];
};

typedef struct dot11i_fsm_authenticator_key   dot11i_fsm_authenticator_key_t;

#define DOT11I_FSM_AUTHENTICATOR_TRIGGER_AUTH_REQUEST      1
#define DOT11I_FSM_AUTHENTICATOR_TRIGGER_REAUTH_REQUEST    2
#define DOT11I_FSM_AUTHENTICATOR_TRIGGER_AAA               3
#define DOT11I_FSM_AUTHENTICATOR_TRIGGER_DEAUTH            4
#define DOT11I_FSM_AUTHENTICATOR_TRIGGER_DISCONNECT        5

#endif /*__DOT11I_FSM_AUTHENTICATOR_H__*/
