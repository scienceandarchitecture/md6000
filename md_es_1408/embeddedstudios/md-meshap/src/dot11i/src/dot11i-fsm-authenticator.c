/********************************************************************************
* MeshDynamics
* --------------
* File     : dot11i-fsm-authenticator.c
* Comments : WPA/802.11i State Machine
* Created  : 3/31/2005
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  8  |6/16/2008 | Lock primitives changed                         | Sriram |
* -----------------------------------------------------------------------------
* |  7  |8/2/2007  | 4-way MSG 4 RX Print added                      | Sriram |
* -----------------------------------------------------------------------------
* |  6  |7/16/2007 | FSM destroy locking fixed                       | Sriram |
* -----------------------------------------------------------------------------
* |  5  |7/13/2007 | FSM Reset if not NULL                           | Sriram |
* -----------------------------------------------------------------------------
* |  4  |12/6/2006 | Changes for In-direct VLAN                      | Sriram |
* -----------------------------------------------------------------------------
* |  3  |10/16/2006| Changes for WPA2                                | Sriram |
* -----------------------------------------------------------------------------
* |  2  |4/7/2005  | Misc changes                                    | Sriram |
* -----------------------------------------------------------------------------
* |  1  |4/6/2005  | Misc changes                                    | Sriram |
* -----------------------------------------------------------------------------
* |  0  |3/31/2005 | Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

#include "dot1x-fsm.h"
#include "dot1x-impl.h"
#include "dot1x-fsm-other.h"
#include "dot1x-backend-radius.h"

#include "dot11i.h"
#include "dot11i-defs.h"
#include "dot11i-impl.h"
#include "dot11i-fsm-authenticator.h"
#include "dot11i-eapol.h"
#include "dot11i-fsm-gtk.h"


/**
 * FSM functions
 */

static int _tick(dot1x_port_t *port, int fsm_code, int other_code, struct dot1x_fsm *fsm);
static int _eapol_rx_begin(dot1x_port_t *port, int fsm_code, int other_code, struct dot1x_fsm *fsm);
static int _eapol_rx(dot1x_port_t *port, int fsm_code, int other_code, struct dot1x_fsm *fsm, dot1x_eapol_frame_t *frame);
static int _eapol_rx_end(dot1x_port_t *port, int fsm_code, int other_code, struct dot1x_fsm *fsm);
static int _trigger(dot1x_port_t *port, int fsm_code, int other_code, struct dot1x_fsm *fsm, int trigger_code);
static int _initialize(dot1x_port_t *port, int fsm_code, int other_code, struct dot1x_fsm *fsm);
static int _on_destroy(dot1x_port_t *port, struct dot1x_fsm *fsm);


/**
 * Internal functions
 */

static void _on_state_entry(dot1x_port_t *port, dot11i_fsm_authenticator_t *fsm_auth, dot1x_fsm_t **trigger_fsm, int *fsm_code, int *other_code, int *trigger_code);
static void _attempt_transition(dot1x_port_t *port, dot11i_fsm_authenticator_t *fsm_auth, dot1x_fsm_t **trigger_fsm, int *fsm_code, int *other_code, int *trigger_code);

static void _initialize_anonce(dot1x_port_t               *port,
                               dot11i_fsm_authenticator_t *fsm_auth,
                               dot11i_authenticator_t     *authenticator);

static void _init_replay_counter(dot1x_port_t               *port,
                                 dot11i_fsm_authenticator_t *fsm_auth,
                                 dot11i_authenticator_t     *authenticator);

static void _calc_ptk(dot1x_port_t               *port,
                      dot11i_fsm_authenticator_t *fsm_auth,
                      dot11i_authenticator_t     *authenticator);


static int _verify_mic(dot1x_port_t               *port,
                       dot11i_fsm_authenticator_t *fsm_auth,
                       dot11i_authenticator_t     *authenticator);

static void _install_ptk(dot1x_port_t               *port,
                         dot11i_fsm_authenticator_t *fsm_auth);

static void _disconnect_port(dot1x_port_t               *port,
                             dot11i_fsm_authenticator_t *fsm_auth);


int dot11i_fsm_authenticator_create(dot1x_port_t *port,
                                    int          dot11imode,
                                    int          cipher_mode,
                                    int          auth_mode,
                                    int          max_timeouts)
{
   dot11i_fsm_authenticator_t *fsm_auth;
   int err;

   fsm_auth = (dot11i_fsm_authenticator_t *)dot1x_fsm_get(port, DOT1X_FSM_CODE_OTHER, DOT1X_FSM_OTHER_CODE_DOT11I_AUTH);

   if (fsm_auth != NULL)
   {
      fsm_auth->state       = DOT11I_FSM_AUTHENTICATOR_STATE_INVALID;
      fsm_auth->Destroyed   = 0;
      fsm_auth->HoldDestroy = 0;

      dot1x_print(DOT1X_PRINT_LEVEL_FLOW,
                  "dot11i-fsm-authenticator: FSM reset for %s:%02x-%02x-%02x-%02x-%02x-%02x\n",
                  port->dot1x_instance->instance_name,
                  port->address[0],
                  port->address[1],
                  port->address[2],
                  port->address[3],
                  port->address[4],
                  port->address[5]);

      return 0;
   }

   fsm_auth = (dot11i_fsm_authenticator_t *)dot1x_heap_alloc(sizeof(dot11i_fsm_authenticator_t));

   memset(fsm_auth, 0, sizeof(dot11i_fsm_authenticator_t));

   fsm_auth->dot11iMode   = dot11imode;
   fsm_auth->cipher_mode  = cipher_mode;
   fsm_auth->auth_mode    = auth_mode;
   fsm_auth->max_timeouts = max_timeouts;
   fsm_auth->VLan_Tag     = -1;
   fsm_auth->Destroyed    = 0;
   fsm_auth->HoldDestroy  = 0;

   fsm_auth->fsm.initialize     = _initialize;
   fsm_auth->fsm.tick           = _tick;
   fsm_auth->fsm.eapol_rx_begin = _eapol_rx_begin;
   fsm_auth->fsm.eapol_rx       = _eapol_rx;
   fsm_auth->fsm.eapol_rx_end   = _eapol_rx_end;
   fsm_auth->fsm.trigger        = _trigger;
   fsm_auth->fsm.on_destroy     = _on_destroy;

   fsm_auth->lock  = dot1x_create_lock();
   fsm_auth->state = DOT11I_FSM_AUTHENTICATOR_STATE_INVALID;

   fsm_auth->key_desc = (dot11i_eapol_key_t *)dot1x_heap_alloc(sizeof(dot11i_eapol_key_t));

   err = dot1x_fsm_register(port, DOT1X_FSM_CODE_OTHER, DOT1X_FSM_OTHER_CODE_DOT11I_AUTH, &fsm_auth->fsm);

   if (err)
   {
      dot1x_destroy_lock(fsm_auth->lock);
      dot1x_heap_free(fsm_auth->key_desc);
      dot1x_heap_free(fsm_auth);
      return err;
   }

   dot1x_print(DOT1X_PRINT_LEVEL_FLOW,
               "dot11i-fsm-authenticator: FSM initialized for %s:%02x-%02x-%02x-%02x-%02x-%02x\n",
               port->dot1x_instance->instance_name,
               port->address[0],
               port->address[1],
               port->address[2],
               port->address[3],
               port->address[4],
               port->address[5]);

   return err;
}


int dot11i_fsm_authenticator_destroy(dot1x_port_t *port)
{
   dot1x_fsm_t *fsm;

   fsm = dot1x_fsm_get(port, DOT1X_FSM_CODE_OTHER, DOT1X_FSM_OTHER_CODE_DOT11I_AUTH);

   if (fsm != NULL)
   {
      _on_destroy(port, fsm);
   }

   dot1x_fsm_unregister(port, DOT1X_FSM_CODE_OTHER, DOT1X_FSM_OTHER_CODE_DOT11I_AUTH);

   return 0;
}


dot11i_fsm_authenticator_t *dot11i_fsm_authenticator_get(dot1x_port_t *port)
{
   dot11i_fsm_authenticator_t *fsm_auth;

   fsm_auth = (dot11i_fsm_authenticator_t *)dot1x_fsm_get(port,
                                                          DOT1X_FSM_CODE_OTHER,
                                                          DOT1X_FSM_OTHER_CODE_DOT11I_AUTH);

   return fsm_auth;
}


int dot11i_fsm_authenticator_tick(dot1x_port_t *port)
{
   dot11i_fsm_authenticator_t *fsm_auth;
   unsigned int               lock_code;

   fsm_auth = (dot11i_fsm_authenticator_t *)dot1x_fsm_get(port,
                                                          DOT1X_FSM_CODE_OTHER,
                                                          DOT1X_FSM_OTHER_CODE_DOT11I_AUTH);

   if (fsm_auth == NULL)
   {
      return -1;
   }

   dot1x_lock(fsm_auth->lock, lock_code);

   fsm_auth->trigger_fsm = NULL;

   ++fsm_auth->tick_count;

   if (fsm_auth->tick_count > 7)
   {
      fsm_auth->TimeoutEvt = True;
   }

   fsm_auth->HoldDestroy = 1;

   _attempt_transition(port,
                       fsm_auth,
                       &fsm_auth->trigger_fsm,
                       &fsm_auth->trigger_fsm_code,
                       &fsm_auth->trigger_other_code,
                       &fsm_auth->trigger_code);

   if (fsm_auth->trigger_fsm != NULL)
   {
      fsm_auth->trigger_fsm->trigger(port,
                                     fsm_auth->trigger_fsm_code,
                                     fsm_auth->trigger_other_code,
                                     fsm_auth->trigger_fsm,
                                     fsm_auth->trigger_code);
   }

   fsm_auth->HoldDestroy = 0;

   dot1x_unlock(fsm_auth->lock, lock_code);

   if (fsm_auth->Destroyed)
   {
      _on_destroy(port, (dot1x_fsm_t *)fsm_auth);
   }

   return 0;
}


static void _on_state_entry(dot1x_port_t *port, dot11i_fsm_authenticator_t *fsm_auth, dot1x_fsm_t **trigger_fsm, int *fsm_code, int *other_code, int *trigger_code)
{
   dot11i_authenticator_t *authenticator;
   dot11i_fsm_gtk_t       *fsm_gtk;

   authenticator = (dot11i_authenticator_t *)port->dot1x_instance->instance_data;

   switch (fsm_auth->state)
   {
   case DOT11I_FSM_AUTHENTICATOR_STATE_AUTHENTICATION:
      ++authenticator->GNoStations;
      memset(fsm_auth->PTK, 0, sizeof(fsm_auth->PTK));
      fsm_auth->flags = 0;
      fsm_auth->state = DOT11I_FSM_AUTHENTICATOR_STATE_AUTHENTICATION2;
      fsm_auth->AuthenticationRequest = False;
      _on_state_entry(port, fsm_auth, trigger_fsm, fsm_code, other_code, trigger_code);
      break;

   case DOT11I_FSM_AUTHENTICATOR_STATE_AUTHENTICATION2:
      _initialize_anonce(port, fsm_auth, authenticator);
      fsm_auth->ReAuthenticationRequest = False;
      if (fsm_auth->auth_mode == AL_802_11_SECURITY_INFO_RSN_AUTH_PSK)
      {
         fsm_auth->state = DOT11I_FSM_AUTHENTICATOR_STATE_INITPSK;
         _on_state_entry(port, fsm_auth, trigger_fsm, fsm_code, other_code, trigger_code);
      }
      break;

   case DOT11I_FSM_AUTHENTICATOR_STATE_INITPMK:
      memcpy(fsm_auth->PMK, fsm_auth->AAA_Key, sizeof(fsm_auth->PMK));
      fsm_auth->flags |= DOT11I_FSM_AUTHENTICATOR_FLAGS_PMK_VALID;
      _init_replay_counter(port, fsm_auth, authenticator);
      dot1x_print(DOT1X_PRINT_LEVEL_FLOW,
                  "dot11i-fsm_authenticator: PMK initialized for %s:%02x-%02x-%02x-%02x-%02x-%02x via AAA\n",
                  port->dot1x_instance->instance_name,
                  port->address[0],
                  port->address[1],
                  port->address[2],
                  port->address[3],
                  port->address[4],
                  port->address[5]);
      fsm_auth->state = DOT11I_FSM_AUTHENTICATOR_STATE_PTKSTART;
      _on_state_entry(port, fsm_auth, trigger_fsm, fsm_code, other_code, trigger_code);
      break;

   case DOT11I_FSM_AUTHENTICATOR_STATE_INITPSK:
      memcpy(fsm_auth->PMK,
             authenticator->net_if_config_info->security_info.dot11.rsn.psk,
             sizeof(fsm_auth->PMK));
      fsm_auth->flags |= DOT11I_FSM_AUTHENTICATOR_FLAGS_PMK_VALID;
      _init_replay_counter(port, fsm_auth, authenticator);
      dot1x_print(DOT1X_PRINT_LEVEL_FLOW,
                  "dot11i-fsm_authenticator: PMK initialized for %s:%02x-%02x-%02x-%02x-%02x-%02x via PSK\n",
                  port->dot1x_instance->instance_name,
                  port->address[0],
                  port->address[1],
                  port->address[2],
                  port->address[3],
                  port->address[4],
                  port->address[5]);
      fsm_auth->state = DOT11I_FSM_AUTHENTICATOR_STATE_PTKSTART;
      _on_state_entry(port, fsm_auth, trigger_fsm, fsm_code, other_code, trigger_code);
      break;

   case DOT11I_FSM_AUTHENTICATOR_STATE_PTKSTART:
      dot1x_print(DOT1X_PRINT_LEVEL_FLOW,
                  "dot11i-fsm_authenticator: Transmitting 4 way msg 1 to %s:%02x-%02x-%02x-%02x-%02x-%02x\n",
                  port->dot1x_instance->instance_name,
                  port->address[0],
                  port->address[1],
                  port->address[2],
                  port->address[3],
                  port->address[4],
                  port->address[5]);
      dot11i_eapol_key_frame_send(port,
                                  fsm_auth,
                                  authenticator,
                                  DOT11I_EAPOL_KEY_FRAME_SEND_MSG_CODE_4_WAY_1,
                                  0,
                                  0,
                                  1,
                                  0,
                                  0,
                                  'P',
                                  fsm_auth->ANonce,
                                  0);
      fsm_auth->EAPOLKeyReceived = False;
      fsm_auth->tick_count       = 0;
      ++fsm_auth->TimeoutCtr;
      break;

   case DOT11I_FSM_AUTHENTICATOR_STATE_PTKCALNEGOTIATING:
      memcpy(fsm_auth->SNonce, fsm_auth->key_desc->nonce, sizeof(fsm_auth->SNonce));
      _calc_ptk(port, fsm_auth, authenticator);
      if (_verify_mic(port, fsm_auth, authenticator))
      {
         dot1x_print(DOT1X_PRINT_LEVEL_FLOW,
                     "dot11i-fsm_authenticator: Received valid 4 way msg 2 from %s:%02x-%02x-%02x-%02x-%02x-%02x\n",
                     port->dot1x_instance->instance_name,
                     port->address[0],
                     port->address[1],
                     port->address[2],
                     port->address[3],
                     port->address[4],
                     port->address[5]);
         fsm_auth->state = DOT11I_FSM_AUTHENTICATOR_STATE_PTKCALNEGOTIATING2;
         _on_state_entry(port, fsm_auth, trigger_fsm, fsm_code, other_code, trigger_code);
      }
      else
      {
         dot1x_print(DOT1X_PRINT_LEVEL_FLOW,
                     "dot11i-fsm_authenticator: Received invalid MIC 4 way msg 2 from %s:%02x-%02x-%02x-%02x-%02x-%02x\n",
                     port->dot1x_instance->instance_name,
                     port->address[0],
                     port->address[1],
                     port->address[2],
                     port->address[3],
                     port->address[4],
                     port->address[5]);
      }
      break;

   case DOT11I_FSM_AUTHENTICATOR_STATE_PTKCALNEGOTIATING2:
      fsm_auth->TimeoutCtr = 0;
      fsm_auth->TimeoutEvt = False;
      fsm_auth->state      = DOT11I_FSM_AUTHENTICATOR_STATE_PTKINITNEGOTIATING;
      _on_state_entry(port, fsm_auth, trigger_fsm, fsm_code, other_code, trigger_code);
      break;

   case DOT11I_FSM_AUTHENTICATOR_STATE_PTKINITNEGOTIATING:
      dot1x_print(DOT1X_PRINT_LEVEL_FLOW,
                  "dot11i-fsm_authenticator: Transmitting 4 way msg 3 to %s:%02x-%02x-%02x-%02x-%02x-%02x\n",
                  port->dot1x_instance->instance_name,
                  port->address[0],
                  port->address[1],
                  port->address[2],
                  port->address[3],
                  port->address[4],
                  port->address[5]);

      fsm_auth->tick_count = 0;
      fsm_auth->TimeoutEvt = False;

      if (fsm_auth->dot11iMode == DOT11I_EAPOL_KEY_TYPE_11I)
      {
         dot11i_eapol_key_frame_send(port,
                                     fsm_auth,
                                     authenticator,
                                     DOT11I_EAPOL_KEY_FRAME_SEND_MSG_CODE_4_WAY_3,
                                     1,
                                     1,
                                     1,
                                     1,
                                     DOT11I_EAPOL_KEY_FRAME_SEND_DATA_FLAG_RSN_IE | DOT11I_EAPOL_KEY_FRAME_SEND_DATA_FLAG_GTK,
                                     'P',
                                     fsm_auth->ANonce,
                                     0);
      }
      else
      {
         dot11i_eapol_key_frame_send(port,
                                     fsm_auth,
                                     authenticator,
                                     DOT11I_EAPOL_KEY_FRAME_SEND_MSG_CODE_4_WAY_3,
                                     0,
                                     1,
                                     1,
                                     1,
                                     DOT11I_EAPOL_KEY_FRAME_SEND_DATA_FLAG_RSN_IE,
                                     'P',
                                     fsm_auth->ANonce,
                                     0);
      }

      fsm_auth->EAPOLKeyReceived = False;
      ++fsm_auth->TimeoutCtr;

      if (fsm_auth->dot11iMode == DOT11I_EAPOL_KEY_TYPE_11I)
      {
         _install_ptk(port, fsm_auth);
      }
      break;

   case DOT11I_FSM_AUTHENTICATOR_STATE_PTKINITDONE:

      if (fsm_auth->dot11iMode == DOT11I_EAPOL_KEY_TYPE_WPA)
      {
         _install_ptk(port, fsm_auth);

         dot1x_print(DOT1X_PRINT_LEVEL_FLOW,
                     "dot11i-fsm_authenticator: Triggering GTK update for %s:%02x-%02x-%02x-%02x-%02x-%02x\n",
                     port->dot1x_instance->instance_name,
                     port->address[0],
                     port->address[1],
                     port->address[2],
                     port->address[3],
                     port->address[4],
                     port->address[5]);
         fsm_gtk = dot11i_fsm_gtk_get(port);
         if (fsm_gtk != NULL)
         {
            *trigger_fsm  = &fsm_gtk->fsm;
            *fsm_code     = DOT1X_FSM_CODE_OTHER;
            *other_code   = DOT1X_FSM_OTHER_CODE_DOT11I_GTK;
            *trigger_code = DOT11I_FSM_GTK_TRIGGER_START;
         }
      }
      break;

   case DOT11I_FSM_AUTHENTICATOR_STATE_DISCONNECT:
      dot1x_print(DOT1X_PRINT_LEVEL_FLOW,
                  "dot11i-fsm_authenticator: Disconnecting %s:%02x-%02x-%02x-%02x-%02x-%02x\n",
                  port->dot1x_instance->instance_name,
                  port->address[0],
                  port->address[1],
                  port->address[2],
                  port->address[3],
                  port->address[4],
                  port->address[5]);
      _disconnect_port(port, fsm_auth);
      fsm_auth->Disconnect = False;
      fsm_auth->state      = DOT11I_FSM_AUTHENTICATOR_STATE_DISCONNECTED;
      _on_state_entry(port, fsm_auth, trigger_fsm, fsm_code, other_code, trigger_code);
      break;

   case DOT11I_FSM_AUTHENTICATOR_STATE_DISCONNECTED:
      dot1x_print(DOT1X_PRINT_LEVEL_FLOW,
                  "dot11i-fsm_authenticator: %s:%02x-%02x-%02x-%02x-%02x-%02x Disconnected\n",
                  port->dot1x_instance->instance_name,
                  port->address[0],
                  port->address[1],
                  port->address[2],
                  port->address[3],
                  port->address[4],
                  port->address[5]);
      fsm_auth->DeAuthenticationRequest = False;
      fsm_auth->state = DOT11I_FSM_AUTHENTICATOR_STATE_INITIALIZE;
      _on_state_entry(port, fsm_auth, trigger_fsm, fsm_code, other_code, trigger_code);
      break;

   case DOT11I_FSM_AUTHENTICATOR_STATE_INITIALIZE:
      fsm_auth->TimeoutCtr              = 0;
      fsm_auth->AuthenticationRequest   = False;
      fsm_auth->ReAuthenticationRequest = False;
      fsm_auth->DeAuthenticationRequest = False;
      fsm_auth->Disconnect              = False;
      fsm_auth->EAPOLKeyReceived        = False;
      break;
   }
}


static void _attempt_transition(dot1x_port_t *port, dot11i_fsm_authenticator_t *fsm_auth, dot1x_fsm_t **trigger_fsm, int *fsm_code, int *other_code, int *trigger_code)
{
   dot11i_authenticator_t *authenticator;
   int changed;

   authenticator = (dot11i_authenticator_t *)port->dot1x_instance->instance_data;

   /**
    * First check for generic triggers where the current
    * state does not matter
    */

   if (fsm_auth->Disconnect)
   {
      fsm_auth->state = DOT11I_FSM_AUTHENTICATOR_STATE_DISCONNECT;
      _on_state_entry(port, fsm_auth, trigger_fsm, fsm_code, other_code, trigger_code);
      return;
   }
   else if (fsm_auth->DeAuthenticationRequest)
   {
      fsm_auth->state = DOT11I_FSM_AUTHENTICATOR_STATE_DISCONNECTED;
      _on_state_entry(port, fsm_auth, trigger_fsm, fsm_code, other_code, trigger_code);
      return;
   }
   else if (fsm_auth->ReAuthenticationRequest)
   {
      fsm_auth->state = DOT11I_FSM_AUTHENTICATOR_STATE_AUTHENTICATION2;
      _on_state_entry(port, fsm_auth, trigger_fsm, fsm_code, other_code, trigger_code);
      return;
   }
   else if (fsm_auth->AuthenticationRequest)
   {
      fsm_auth->state = DOT11I_FSM_AUTHENTICATOR_STATE_AUTHENTICATION;
      _on_state_entry(port, fsm_auth, trigger_fsm, fsm_code, other_code, trigger_code);
      return;
   }

   do
   {
      changed = 0;
      switch (fsm_auth->state)
      {
      case DOT11I_FSM_AUTHENTICATOR_STATE_AUTHENTICATION:
         break;

      case DOT11I_FSM_AUTHENTICATOR_STATE_AUTHENTICATION2:
         if (fsm_auth->flags & DOT11I_FSM_AUTHENTICATOR_FLAGS_AAAK_VALID)
         {
            changed         = 1;
            fsm_auth->state = DOT11I_FSM_AUTHENTICATOR_STATE_INITPMK;
            _on_state_entry(port, fsm_auth, trigger_fsm, fsm_code, other_code, trigger_code);
         }
         break;

      case DOT11I_FSM_AUTHENTICATOR_STATE_INITPMK:
         break;

      case DOT11I_FSM_AUTHENTICATOR_STATE_INITPSK:
         break;

      case DOT11I_FSM_AUTHENTICATOR_STATE_PTKSTART:
         if (fsm_auth->TimeoutEvt)
         {
            fsm_auth->TimeoutEvt = False;
            ++fsm_auth->TimeoutCtr;
            if (fsm_auth->TimeoutCtr > fsm_auth->max_timeouts)
            {
               dot1x_print(DOT1X_PRINT_LEVEL_FLOW,
                           "dot11i-fsm_authenticator: 4 way msg 2 timeout for %s:%02x-%02x-%02x-%02x-%02x-%02x\n",
                           port->dot1x_instance->instance_name,
                           port->address[0],
                           port->address[1],
                           port->address[2],
                           port->address[3],
                           port->address[4],
                           port->address[5]);
               changed         = 1;
               fsm_auth->state = DOT11I_FSM_AUTHENTICATOR_STATE_DISCONNECT;
               _on_state_entry(port, fsm_auth, trigger_fsm, fsm_code, other_code, trigger_code);
            }
            else
            {
               changed         = 1;
               fsm_auth->state = DOT11I_FSM_AUTHENTICATOR_STATE_PTKSTART;
               _on_state_entry(port, fsm_auth, trigger_fsm, fsm_code, other_code, trigger_code);
            }
         }
         else if (fsm_auth->EAPOLKeyReceived &&
                  !fsm_auth->EAPOLKeyReceivedRequest &&
                  (fsm_auth->EAPOLKeyReceivedK == KEY_INFORMATION_TYPE_PAIRWISE))
         {
            fsm_auth->tick_count = 0;
            fsm_auth->TimeoutEvt = False;
            changed         = 1;
            fsm_auth->state = DOT11I_FSM_AUTHENTICATOR_STATE_PTKCALNEGOTIATING;
            _on_state_entry(port, fsm_auth, trigger_fsm, fsm_code, other_code, trigger_code);
         }
         break;

      case DOT11I_FSM_AUTHENTICATOR_STATE_PTKCALNEGOTIATING:
         if (fsm_auth->TimeoutEvt)
         {
            fsm_auth->TimeoutEvt = False;
            changed         = 1;
            fsm_auth->state = DOT11I_FSM_AUTHENTICATOR_STATE_PTKSTART;
            _on_state_entry(port, fsm_auth, trigger_fsm, fsm_code, other_code, trigger_code);
         }
         break;

      case DOT11I_FSM_AUTHENTICATOR_STATE_PTKCALNEGOTIATING2:
         break;

      case DOT11I_FSM_AUTHENTICATOR_STATE_PTKINITNEGOTIATING:
         if (fsm_auth->TimeoutEvt)
         {
            fsm_auth->TimeoutEvt = False;
            ++fsm_auth->TimeoutCtr;
            if (fsm_auth->TimeoutCtr > fsm_auth->max_timeouts)
            {
               dot1x_print(DOT1X_PRINT_LEVEL_FLOW,
                           "dot11i-fsm_authenticator: 4 way msg 4 timeout for %s:%02x-%02x-%02x-%02x-%02x-%02x\n",
                           port->dot1x_instance->instance_name,
                           port->address[0],
                           port->address[1],
                           port->address[2],
                           port->address[3],
                           port->address[4],
                           port->address[5]);
               changed         = 1;
               fsm_auth->state = DOT11I_FSM_AUTHENTICATOR_STATE_DISCONNECT;
               _on_state_entry(port, fsm_auth, trigger_fsm, fsm_code, other_code, trigger_code);
            }
         }
         else if (fsm_auth->EAPOLKeyReceived &&
                  !fsm_auth->EAPOLKeyReceivedRequest &&
                  (fsm_auth->EAPOLKeyReceivedK == KEY_INFORMATION_TYPE_PAIRWISE))
         {
            if (fsm_auth->key_desc->data_length > 0)
            {
               _on_state_entry(port, fsm_auth, trigger_fsm, fsm_code, other_code, trigger_code);
            }
            else
            {
               if (_verify_mic(port, fsm_auth, authenticator))
               {
                  dot1x_print(DOT1X_PRINT_LEVEL_FLOW,
                              "dot11i-fsm_authenticator: Received valid 4 way msg 4 from %s:%02x-%02x-%02x-%02x-%02x-%02x\n",
                              port->dot1x_instance->instance_name,
                              port->address[0],
                              port->address[1],
                              port->address[2],
                              port->address[3],
                              port->address[4],
                              port->address[5]);

                  changed         = 1;
                  fsm_auth->state = DOT11I_FSM_AUTHENTICATOR_STATE_PTKINITDONE;
                  _on_state_entry(port, fsm_auth, trigger_fsm, fsm_code, other_code, trigger_code);
               }
               else
               {
                  dot1x_print(DOT1X_PRINT_LEVEL_FLOW,
                              "dot11i-fsm_authenticator: 4 way msg 4 MIC failure for %s:%02x-%02x-%02x-%02x-%02x-%02x\n",
                              port->dot1x_instance->instance_name,
                              port->address[0],
                              port->address[1],
                              port->address[2],
                              port->address[3],
                              port->address[4],
                              port->address[5]);
               }
            }
         }
         break;

      case DOT11I_FSM_AUTHENTICATOR_STATE_PTKINITDONE:
         break;

      case DOT11I_FSM_AUTHENTICATOR_STATE_DISCONNECT:
         break;

      case DOT11I_FSM_AUTHENTICATOR_STATE_DISCONNECTED:
         break;

      case DOT11I_FSM_AUTHENTICATOR_STATE_INITIALIZE:
         break;
      }
   } while (changed);
}


static void _initialize_anonce(dot1x_port_t               *port,
                               dot11i_fsm_authenticator_t *fsm_auth,
                               dot11i_authenticator_t     *authenticator)
{
   unsigned long time_stamp;
   unsigned char rand_buffer[36];
   unsigned char data[DOT1X_PORT_ADDRESS_LEN];
   unsigned char prf_out[DOT11I_PRF_BUFFER_SIZE];

   time_stamp = dot1x_timestamp();

   memcpy(rand_buffer, &time_stamp, 4);
   memcpy(rand_buffer + 4, authenticator->KeyCounter, 32);

   dot11i_increment_key_counter(authenticator);

   memcpy(data, port->address, DOT1X_PORT_ADDRESS_LEN);

   dot11i_prf(rand_buffer,
              sizeof(rand_buffer),
              "Authenticator ANonce",
              data,
              DOT1X_PORT_ADDRESS_LEN,
              prf_out,
              sizeof(fsm_auth->ANonce));

   memcpy(fsm_auth->ANonce, prf_out, sizeof(fsm_auth->ANonce));
}


static void _init_replay_counter(dot1x_port_t               *port,
                                 dot11i_fsm_authenticator_t *fsm_auth,
                                 dot11i_authenticator_t     *authenticator)
{
   memset(fsm_auth->KeyReplayCounter, 0, sizeof(fsm_auth->KeyReplayCounter));
}


static void _calc_ptk(dot1x_port_t               *port,
                      dot11i_fsm_authenticator_t *fsm_auth,
                      dot11i_authenticator_t     *authenticator)
{
   unsigned char *min_nonce;
   unsigned char *max_nonce;
   unsigned char *min_addr;
   unsigned char *max_addr;
   unsigned char aa[DOT1X_PORT_ADDRESS_LEN];
   unsigned char data[DOT1X_PORT_ADDRESS_LEN + DOT1X_PORT_ADDRESS_LEN + 32 + 32];
   unsigned char prf_out[DOT11I_PRF_BUFFER_SIZE];

   if (!(fsm_auth->flags & DOT11I_FSM_AUTHENTICATOR_FLAGS_PMK_VALID))
   {
      dot1x_print(DOT1X_PRINT_LEVEL_ERROR, "PTK calculation requested without PMK\n");
      return;
   }

   dot1x_get_instance_addr(authenticator->dot1x_instance,
                           port,
                           aa);

   min_nonce = dot11i_nonce_min(fsm_auth->ANonce, fsm_auth->SNonce);
   max_nonce = dot11i_nonce_max(fsm_auth->ANonce, fsm_auth->SNonce);

   min_addr = dot11i_addr_min(port->address, aa);
   max_addr = dot11i_addr_max(port->address, aa);

   memcpy(data, min_addr, DOT1X_PORT_ADDRESS_LEN);
   memcpy(data + DOT1X_PORT_ADDRESS_LEN, max_addr, DOT1X_PORT_ADDRESS_LEN);
   memcpy(data + DOT1X_PORT_ADDRESS_LEN + DOT1X_PORT_ADDRESS_LEN, min_nonce, 32);
   memcpy(data + DOT1X_PORT_ADDRESS_LEN + DOT1X_PORT_ADDRESS_LEN + 32, max_nonce, 32);

   if (fsm_auth->cipher_mode == AL_802_11_SECURITY_INFO_RSN_CIPHER_CCMP)
   {
      DOT11I_PRF_384(fsm_auth->PMK, 32, "Pairwise key expansion", data, sizeof(data), prf_out);
   }
   else if (fsm_auth->cipher_mode == AL_802_11_SECURITY_INFO_RSN_CIPHER_TKIP)
   {
      DOT11I_PRF_512(fsm_auth->PMK, 32, "Pairwise key expansion", data, sizeof(data), prf_out);
   }

   memcpy(fsm_auth->PTK, prf_out, sizeof(fsm_auth->PTK));

   fsm_auth->flags |= DOT11I_FSM_AUTHENTICATOR_FLAGS_PTK_VALID;
}


static int _verify_mic(dot1x_port_t               *port,
                       dot11i_fsm_authenticator_t *fsm_auth,
                       dot11i_authenticator_t     *authenticator)
{
   int ret;

   if (KEY_INFORMATION_GET_MIC(fsm_auth->key_desc))
   {
      ret = dot11i_eapol_key_frame_process_2(authenticator, fsm_auth, fsm_auth->eapol_frame, fsm_auth->key_desc);
      if (ret == DOT11I_EAPOL_KEY_FRAME_PROCESS_2_OK)
      {
         return 1;
      }
      return 0;
   }

   return 1;
}


static void _install_ptk(dot1x_port_t               *port,
                         dot11i_fsm_authenticator_t *fsm_auth)
{
   access_point_sta_security_info_t sta_sec_info;

   memset(&sta_sec_info, 0, sizeof(access_point_sta_security_info_t));

   sta_sec_info.authenticated            = 1;
   sta_sec_info.dot11.cipher_type        = fsm_auth->cipher_mode;
   sta_sec_info.dot11.destination.length = 6;
   memcpy(sta_sec_info.dot11.destination.bytes, port->address, 6);
   sta_sec_info.dot11.enabled = 1;
   sta_sec_info.vlan_tag      = fsm_auth->VLan_Tag;

   switch (sta_sec_info.dot11.cipher_type)
   {
   case AL_802_11_SECURITY_INFO_RSN_CIPHER_TKIP:
      memcpy(sta_sec_info.dot11.key, fsm_auth->PTK + 32, 32);
      break;

   case AL_802_11_SECURITY_INFO_RSN_CIPHER_CCMP:
      memcpy(sta_sec_info.dot11.key, fsm_auth->PTK + 32,
             16);
      break;
   }
   access_point_set_dot1x_auth_result(AL_CONTEXT & sta_sec_info);
}


static void _disconnect_port(dot1x_port_t               *port,
                             dot11i_fsm_authenticator_t *fsm_auth)
{
   access_point_sta_security_info_t sta_sec_info;

   memset(&sta_sec_info, 0, sizeof(access_point_sta_security_info_t));
   sta_sec_info.authenticated = 0;
   sta_sec_info.reason_code   = APSSI_REASON_CODE_AUTH_FAILURE;
   memcpy(sta_sec_info.dot11.destination.bytes, port->address, 6);
   sta_sec_info.dot11.destination.length = 6;
   access_point_set_dot1x_auth_result(AL_CONTEXT & sta_sec_info);
}


static int _initialize(dot1x_port_t *port, int fsm_code, int other_code, struct dot1x_fsm *fsm)
{
   dot11i_fsm_authenticator_t *fsm_auth;
   unsigned int               lock_code;

   fsm_auth = (dot11i_fsm_authenticator_t *)fsm;

   dot1x_lock(fsm_auth->lock, lock_code);

   fsm_auth->state = DOT11I_FSM_AUTHENTICATOR_STATE_INITIALIZE;

   _on_state_entry(port,
                   fsm_auth,
                   &fsm_auth->trigger_fsm,
                   &fsm_auth->trigger_fsm_code,
                   &fsm_auth->trigger_other_code,
                   &fsm_auth->trigger_code);

   dot1x_unlock(fsm_auth->lock, lock_code);

   return 0;
}


static int _tick(dot1x_port_t *port, int fsm_code, int other_code, struct dot1x_fsm *fsm)
{
   /** Un-used */
   return 0;
}


static int _eapol_rx_begin(dot1x_port_t *port, int fsm_code, int other_code, struct dot1x_fsm *fsm)
{
   dot11i_fsm_authenticator_t *fsm_auth;
   unsigned int               lock_code;

   fsm_auth = (dot11i_fsm_authenticator_t *)fsm;

   dot1x_lock(fsm_auth->lock, lock_code);

   fsm_auth->trigger_fsm = NULL;

   dot1x_unlock(fsm_auth->lock, lock_code);

   return 0;
}


static int _eapol_rx(dot1x_port_t *port, int fsm_code, int other_code, struct dot1x_fsm *fsm, dot1x_eapol_frame_t *frame)
{
   dot11i_fsm_authenticator_t *fsm_auth;
   dot11i_authenticator_t     *authenticator;
   int          ret;
   unsigned int lock_code;

   fsm_auth      = (dot11i_fsm_authenticator_t *)fsm;
   authenticator = (dot11i_authenticator_t *)port->dot1x_instance->instance_data;

   dot1x_lock(fsm_auth->lock, lock_code);

   if (frame->type == DOT1X_EAPOL_FRAME_TYPE_KEY)
   {
      ret = dot11i_eapol_key_frame_process_1(authenticator, frame, fsm_auth->key_desc);
      if (ret == 0)
      {
         fsm_auth->EAPOLKeyReceived        = True;
         fsm_auth->EAPOLKeyReceivedRequest = KEY_INFORMATION_GET_REQ(fsm_auth->key_desc);
         fsm_auth->EAPOLKeyReceivedK       = KEY_INFORMATION_GET_TYPE(fsm_auth->key_desc);
         fsm_auth->eapol_frame             = frame;

         _attempt_transition(port,
                             fsm_auth,
                             &fsm_auth->trigger_fsm,
                             &fsm_auth->trigger_fsm_code,
                             &fsm_auth->trigger_other_code,
                             &fsm_auth->trigger_code);

         fsm_auth->EAPOLKeyReceived = False;
      }
   }

   dot1x_unlock(fsm_auth->lock, lock_code);

   return 0;
}


static int _eapol_rx_end(dot1x_port_t *port, int fsm_code, int other_code, struct dot1x_fsm *fsm)
{
   dot11i_fsm_authenticator_t *fsm_auth;
   unsigned int               lock_code;

   fsm_auth = (dot11i_fsm_authenticator_t *)fsm;

   dot1x_lock(fsm_auth->lock, lock_code);

   if (fsm_auth->trigger_fsm != NULL)
   {
      fsm_auth->trigger_fsm->trigger(port,
                                     fsm_auth->trigger_fsm_code,
                                     fsm_auth->trigger_other_code,
                                     fsm_auth->trigger_fsm,
                                     fsm_auth->trigger_code);
   }

   dot1x_unlock(fsm_auth->lock, lock_code);

   return 0;
}


static int _trigger(dot1x_port_t *port, int fsm_code, int other_code, struct dot1x_fsm *fsm, int trigger_code)
{
   dot11i_fsm_authenticator_t *fsm_auth;
   dot11i_authenticator_t     *authenticator;
   dot1x_radius_backend_key_t rx_key;
   unsigned int               lock_code;
   dot1x_fsm_t                *trigger_fsm;
   int trigger_fsm_code;
   int new_trigger_code;
   int trigger_other_code;

   fsm_auth      = (dot11i_fsm_authenticator_t *)fsm;
   authenticator = (dot11i_authenticator_t *)port->dot1x_instance->instance_data;
   trigger_fsm   = NULL;

   dot1x_lock(fsm_auth->lock, lock_code);

   switch (trigger_code)
   {
   case DOT11I_FSM_AUTHENTICATOR_TRIGGER_AUTH_REQUEST:
      fsm_auth->AuthenticationRequest = True;
      _attempt_transition(port,
                          fsm_auth,
                          &trigger_fsm,
                          &trigger_fsm_code,
                          &trigger_other_code,
                          &new_trigger_code);
      break;

   case DOT11I_FSM_AUTHENTICATOR_TRIGGER_REAUTH_REQUEST:
      fsm_auth->ReAuthenticationRequest = True;
      _attempt_transition(port,
                          fsm_auth,
                          &trigger_fsm,
                          &trigger_fsm_code,
                          &trigger_other_code,
                          &new_trigger_code);
      break;

   case DOT11I_FSM_AUTHENTICATOR_TRIGGER_AAA:
      fsm_auth->VLan_Tag = dot1x_radius_backend_get_vlan_tag(port);
      dot1x_radius_backend_get_rx_key(port, &rx_key);
      memcpy(fsm_auth->AAA_Key, rx_key.key + 1, sizeof(fsm_auth->AAA_Key));
      fsm_auth->flags |= DOT11I_FSM_AUTHENTICATOR_FLAGS_AAAK_VALID;
      _attempt_transition(port,
                          fsm_auth,
                          &trigger_fsm,
                          &trigger_fsm_code,
                          &trigger_other_code,
                          &new_trigger_code);
      break;

   case DOT11I_FSM_AUTHENTICATOR_TRIGGER_DEAUTH:
      fsm_auth->DeAuthenticationRequest = True;
      _attempt_transition(port,
                          fsm_auth,
                          &trigger_fsm,
                          &trigger_fsm_code,
                          &trigger_other_code,
                          &new_trigger_code);
      break;

   case DOT11I_FSM_AUTHENTICATOR_TRIGGER_DISCONNECT:
      fsm_auth->Disconnect = True;
      _attempt_transition(port,
                          fsm_auth,
                          &trigger_fsm,
                          &trigger_fsm_code,
                          &trigger_other_code,
                          &new_trigger_code);
   }

   dot1x_unlock(fsm_auth->lock, lock_code);

   if (trigger_fsm != NULL)
   {
      trigger_fsm->trigger(port, trigger_fsm_code, trigger_other_code, trigger_fsm, new_trigger_code);
   }

   return 0;
}


static int _on_destroy(dot1x_port_t *port, struct dot1x_fsm *fsm)
{
   dot11i_fsm_authenticator_t *fsm_auth;

   fsm_auth = (dot11i_fsm_authenticator_t *)fsm;

   if (fsm_auth != NULL)
   {
      if (fsm_auth->HoldDestroy)
      {
         dot1x_print(DOT1X_PRINT_LEVEL_FLOW,
                     "dot11i-fsm-authenticator: Holding off FSM destruction for %s:%02x-%02x-%02x-%02x-%02x-%02x\n",
                     port->dot1x_instance->instance_name,
                     port->address[0],
                     port->address[1],
                     port->address[2],
                     port->address[3],
                     port->address[4],
                     port->address[5]);
         fsm_auth->Destroyed = 1;
      }
      else
      {
         dot1x_print(DOT1X_PRINT_LEVEL_FLOW,
                     "dot11i-fsm-authenticator: FSM destroyed for %s:%02x-%02x-%02x-%02x-%02x-%02x\n",
                     port->dot1x_instance->instance_name,
                     port->address[0],
                     port->address[1],
                     port->address[2],
                     port->address[3],
                     port->address[4],
                     port->address[5]);
         dot1x_destroy_lock(fsm_auth->lock);
         dot1x_heap_free(fsm_auth->key_desc);
         dot1x_heap_free(fsm_auth);
      }
   }

   return 0;
}
