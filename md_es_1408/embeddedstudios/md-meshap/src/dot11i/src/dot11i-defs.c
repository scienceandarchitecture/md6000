/********************************************************************************
* MeshDynamics
* --------------
* File     : dot11i-defs.c
* Comments : WPA/802.11i Definitions
* Created  : 3/1/2005
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  0  |3/1/2005  | Created.                                        | Sriram
* -----------------------------------------------------------------------------
********************************************************************************/

#include "dot1x.h"
#include "dot1x-impl.h"
#include "dot11i.h"
#include "dot11i-defs.h"
#include "dot11i-impl.h"

int dot11i_prf(unsigned char *key,
               int           key_length,
               const char    *label,
               unsigned char *data,
               int           data_length,
               unsigned char *prf_out,
               int           prf_out_length)
{
   int           i;
   unsigned char input[1024];        /* concatenated input */
   int           current_index;
   int           total_len;
   int           prefix_len;

	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MESHAP : FLOW : Enter %s: %d\n", __func__, __LINE__);
   current_index = 0;
   prefix_len    = strlen(label);
   total_len     = prefix_len + 1 + data_length;

   memset(input, 0, sizeof(input));
   memset(prf_out, 0, prf_out_length);

   memcpy(input, label, prefix_len);
   memcpy(&input[prefix_len + 1], data, data_length);
   total_len++;

   for (i = 0; i < (prf_out_length + DOT11I_SHA1_SIZE - 1) / DOT11I_SHA1_SIZE; i++)
   {
      dot11i_hmac_sha1(input, total_len, key, key_length, &prf_out[current_index]);
      current_index += DOT11I_SHA1_SIZE;
      input[total_len - 1]++;
   }

	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MESHAP : FLOW : Exit %s: %d\n", __func__, __LINE__);
   return 0;
}

unsigned char *dot11i_addr_min(unsigned char *address_a, unsigned char *address_b)
{
   int ret;

	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MESHAP : FLOW : Enter %s: %d\n", __func__, __LINE__);
   ret = memcmp(address_a, address_b, 6);

	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MESHAP : FLOW : Exit %s: %d\n", __func__, __LINE__);
   return ret < 0 ? address_a : address_b;
}

unsigned char *dot11i_addr_max(unsigned char *address_a, unsigned char *address_b)
{
   int ret;

	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MESHAP : FLOW : Enter %s: %d\n", __func__, __LINE__);
   ret = memcmp(address_a, address_b, 6);

	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MESHAP : FLOW : Exit %s: %d\n", __func__, __LINE__);
   return ret < 0 ? address_b : address_a;
}

void dot11i_increment_key_counter(dot11i_authenticator_t *authenticator)
{
   int i;

	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MESHAP : FLOW : Enter %s: %d\n", __func__, __LINE__);
   for (i = 0; i < 32; i++)
   {
      if (authenticator->KeyCounter[i] != 0xFF)
      {
         ++authenticator->KeyCounter[i];
         break;
      }
      else
      {
         authenticator->KeyCounter[i] = 0;
      }
   }

   if (i == 32)
   {
      authenticator->KeyCounter[0] = 1;
   }
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MESHAP : FLOW : Exit %s: %d\n", __func__, __LINE__);
}

unsigned char *dot11i_nonce_min(unsigned char *nonce_a, unsigned char *nonce_b)
{
   int ret;
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MESHAP : FLOW : Enter %s: %d\n", __func__, __LINE__);

   ret = memcmp(nonce_a, nonce_b, 32);

	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MESHAP : FLOW : Exit %s: %d\n", __func__, __LINE__);
   return ret < 0 ? nonce_a : nonce_b;
}

unsigned char *dot11i_nonce_max(unsigned char *nonce_a, unsigned char *nonce_b)
{
   int ret;

	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MESHAP : FLOW : Enter %s: %d\n", __func__, __LINE__);
   ret = memcmp(nonce_a, nonce_b, 32);

	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MESHAP : FLOW : Exit %s: %d\n", __func__, __LINE__);
   return ret < 0 ? nonce_b : nonce_a;
}

void dot11i_initialize_key_counter(dot11i_authenticator_t *authenticator)
{
   unsigned long time_stamp;
   unsigned char data[DOT1X_PORT_ADDRESS_LEN];
   unsigned char prf_out[DOT11I_PRF_BUFFER_SIZE];

	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MESHAP : FLOW : Enter %s: %d\n", __func__, __LINE__);
   time_stamp = dot1x_timestamp();

   dot1x_get_instance_addr(authenticator->dot1x_instance, NULL, data);

   dot11i_prf((unsigned char *)&time_stamp,
              sizeof(time_stamp),
              "Init Counter",
              data,
              DOT1X_PORT_ADDRESS_LEN,
              prf_out,
              sizeof(authenticator->KeyCounter));

   memcpy(authenticator->KeyCounter, prf_out, sizeof(authenticator->KeyCounter));
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MESHAP : FLOW : Exit %s: %d\n", __func__, __LINE__);
}

static DOT1X_INLINE void _generate_gmk(dot11i_authenticator_t *authenticator, unsigned char *GMK)
{
   unsigned long time_stamp;
   unsigned char rand_buf[4 + 32];
   unsigned char data[DOT1X_PORT_ADDRESS_LEN];
   unsigned char prf_out[DOT11I_PRF_BUFFER_SIZE];

	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MESHAP : FLOW : Enter %s: %d\n", __func__, __LINE__);
   time_stamp = dot1x_timestamp();

   memcpy(rand_buf, &time_stamp, 4);
   memcpy(rand_buf + 4, authenticator->KeyCounter, 32);

   dot11i_increment_key_counter(authenticator);

   dot11i_prf(rand_buf,
              sizeof(rand_buf),
              "Authenticator Group Master key",
              data,
              DOT1X_PORT_ADDRESS_LEN,
              prf_out,
              32);

   memcpy(GMK, prf_out, 32);
	_PACKET_DUMP(GMK, 32);
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MESHAP : FLOW : Exit %s: %d\n", __func__, __LINE__);
}

static DOT1X_INLINE void _generate_gnonce(dot11i_authenticator_t *authenticator)
{
   unsigned long time_stamp;
   unsigned char rand_buf[4 + 32];
   unsigned char data[DOT1X_PORT_ADDRESS_LEN];
   unsigned char prf_out[DOT11I_PRF_BUFFER_SIZE];

	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MESHAP : FLOW : Enter %s: %d\n", __func__, __LINE__);
   time_stamp = dot1x_timestamp();

   dot1x_get_instance_addr(authenticator->dot1x_instance, NULL, data);

   memcpy(rand_buf, &time_stamp, 4);
   memcpy(rand_buf + 4, authenticator->KeyCounter, 32);

   dot11i_increment_key_counter(authenticator);

   dot11i_prf(rand_buf,
              sizeof(rand_buf),
              "Authenticator GNonce",
              data,
              DOT1X_PORT_ADDRESS_LEN,
              prf_out,
              sizeof(authenticator->GNonce));

   memcpy(authenticator->GNonce, prf_out, sizeof(authenticator->GNonce));
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MESHAP : FLOW : Exit %s: %d\n", __func__, __LINE__);
}

void dot11i_setup_group_key(dot11i_authenticator_t *authenticator)
{
   unsigned char GTK[32];
   unsigned char GMK[32];
   unsigned char data_buf[6 + 32];
   unsigned char prf_out[DOT11I_PRF_BUFFER_SIZE];

	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MESHAP : FLOW : Enter %s: %d\n", __func__, __LINE__);
   _generate_gmk(authenticator, GMK);

   _generate_gnonce(authenticator);

   dot1x_get_instance_addr(authenticator->dot1x_instance, NULL, data_buf);
   memcpy(data_buf + 6, authenticator->KeyCounter, 32);

   dot11i_increment_key_counter(authenticator);

   dot11i_prf(GMK,
              sizeof(GMK),
              "Group key expansion",
              data_buf,
              sizeof(data_buf),
              prf_out,
              sizeof(GTK));

   memcpy(GTK, prf_out, sizeof(GTK));

   memcpy(authenticator->net_if_config_info->group_key.key,
          GTK,
          32);
	_PACKET_DUMP(GTK, 32);
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MESHAP : FLOW : Exit %s: %d\n", __func__, __LINE__);
}
