/********************************************************************************
* MeshDynamics
* --------------
* File     : meshap_dhcp.h
* Comments : DHCP Server Implementation
* Created  : 9/5/2007
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  0  |9/5/2007 | Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

#ifndef __MESHAP_DHCP_H__
#define __MESHAP_DHCP_H__

typedef void * meshap_dhcp_instance_t;

meshap_dhcp_instance_t meshap_dhcp_initialize(al_net_addr_t *self_mac_address,
                                              unsigned char net_id,
                                              unsigned char host_id_1,
                                              unsigned char host_id_2,
                                              unsigned char *subnet_mask,
                                              unsigned char *gateway_ip_addr,
                                              unsigned char *dns_addr,
                                              unsigned int  lease_time_millis);

void meshap_dhcp_uninitialize(meshap_dhcp_instance_t instance);

#define MESHAP_DHCP_RETVAL_SUCCESS     0
#define MESHAP_DHCP_RETVAL_DISCARD     -1
#define MESHAP_DHCP_RETVAL_CONTINUE    -2

int meshap_dhcp_process_packet(meshap_dhcp_instance_t instance,
                               al_net_if_t            *net_if,
                               al_packet_t            *packet_in,
                               al_packet_t            **packet_out);

#endif /*__MESHAP_DHCP_H__*/
