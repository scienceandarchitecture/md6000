/********************************************************************************
* MeshDynamics
* --------------
* File     : meshap_dhcp.c
* Comments : DHCP Server Implementation for DISJOINT ADHOC
* Created  : 9/5/2007
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  0  |9/5/2007  | Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

#include <linux/string.h>
#include <linux/sched.h>
#include "al.h"
#include "al_802_11.h"
#include "access_point.h"

#include "al_impl_context.h"
#include "al_socket.h"
#include "al_endian.h"
#include "meshap_dhcp.h"

#define _MAX_ALLOC_IP_COUNT           253
#define _IP_ADDR_SIZE                 4

#define _CLIENT_INFO_STATE_NONE       0
#define _CLIENT_INFO_STATE_OFFERED    1
#define _CLIENT_INFO_STATE_ACK        2
#define _CLIENT_INFO_STATE_TAKEN      3

#define _CLIENT_INFO_OFFER_EXPIRY     60        /* 60 seconds */

struct _client_info
{
   unsigned char allocated;
   unsigned char state;
   al_u64_t      alloc_time_stamp;
   al_net_addr_t client_addr;
};
typedef struct _client_info   _client_info_t;

struct _dhcp_priv
{
   al_net_addr_t  self_mac_address;
   unsigned char  net_id;
   unsigned char  m;
   unsigned char  n;
   unsigned char  subnet_mask[_IP_ADDR_SIZE];
   unsigned char  gateway[_IP_ADDR_SIZE];
   unsigned char  dns[_IP_ADDR_SIZE];
   unsigned char  self_ip[_IP_ADDR_SIZE];
   unsigned char  current_pos;
   _client_info_t client_info[_MAX_ALLOC_IP_COUNT];
   unsigned int   lease_time_in_millis;
};
typedef struct _dhcp_priv   _dhcp_priv_t;

/**
 * DHCP protocol fields taken from RFC 2131
 */

#define _DHCP_POSITION_OP                    (0)
#define _DHCP_POSITION_HTYPE                 (_DHCP_POSITION_OP + 1)
#define _DHCP_POSITION_HLEN                  (_DHCP_POSITION_HTYPE + 1)
#define _DHCP_POSITION_HOPS                  (_DHCP_POSITION_HLEN + 1)
#define _DHCP_POSITION_XID                   (_DHCP_POSITION_HOPS + 1)
#define _DHCP_POSITION_SECS                  (_DHCP_POSITION_XID + 4)
#define _DHCP_POSITION_FLAGS                 (_DHCP_POSITION_SECS + 2)
#define _DHCP_POSITION_CIADDR                (_DHCP_POSITION_FLAGS + 2)
#define _DHCP_POSITION_YIADDR                (_DHCP_POSITION_CIADDR + 4)
#define _DHCP_POSITION_SIADDR                (_DHCP_POSITION_YIADDR + 4)
#define _DHCP_POSITION_GIADDR                (_DHCP_POSITION_SIADDR + 4)
#define _DHCP_POSITION_CHADDR                (_DHCP_POSITION_GIADDR + 4)
#define _DHCP_POSITION_SNAME                 (_DHCP_POSITION_CHADDR + 16)
#define _DHCP_POSITION_FILE                  (_DHCP_POSITION_SNAME + 64)

#define _DHCP_MESSAGE_SIZE_BEFORE_OPTIONS    (_DHCP_POSITION_FILE + 128)
#define _DHCP_OPTIONS_COOKIE_SIZE            4

#define _DHCP_POSITION_OPTIONS_COOKIE        _DHCP_MESSAGE_SIZE_BEFORE_OPTIONS
#define _DHCP_POSITION_OPTIONS_BEGIN         (_DHCP_POSITION_OPTIONS_COOKIE + _DHCP_OPTIONS_COOKIE_SIZE)


static const unsigned char _options_cookie[_DHCP_OPTIONS_COOKIE_SIZE] = { 0x63, 0x82, 0x53, 0x63 };

#define _DHCP_OP_VALUE_REQUEST                1
#define _DHCP_OP_VALUE_REPLY                  2

#define _DHCP_OPTION_CODE_PAD                 0                 /* 0 length, No Len field */
#define _DHCP_OPTION_CODE_END                 255               /* 0 length, No Len field */
#define _DHCP_OPTION_CODE_MASK                1                 /* Len field = 4 */
#define _DHCP_OPTION_CODE_ROUTER              3                 /* Len field = 4 */
#define _DHCP_OPTION_CODE_DNS                 6                 /* Len field = 4 */
#define _DHCP_OPTION_REQUESTED_IP             50                /* Len field =  4*/
#define _DHCP_OPTION_CODE_LEASE_TIME          51                /* Len field = 4, time in seconds */
#define _DHCP_OPTION_CODE_MESSAGE_TYPE        53                /* Len = 1 */
#define _DHCP_OPTION_CODE_SERVER_ID           54                /* Len field = 4 */


#define _DHCP_OPTION_MESSAGE_TYPE_DISCOVER    1
#define _DHCP_OPTION_MESSAGE_TYPE_OFFER       2
#define _DHCP_OPTION_MESSAGE_TYPE_REQUEST     3
#define _DHCP_OPTION_MESSAGE_TYPE_DECLINE     4
#define _DHCP_OPTION_MESSAGE_TYPE_ACK         5
#define _DHCP_OPTION_MESSAGE_TYPE_NACK        6
#define _DHCP_OPTION_MESSAGE_TYPE_RELEASE     7
#define _DHCP_OPTION_MESSAGE_TYPE_INFORM      8

#define _DHCP_SERVER_UDP_PORT                 67
#define _DHCP_CLIENT_UDP_PORT                 68

/**
 * For our DHCP response packets, we use the following options:
 * MASK, ROUTER, DNS, LEASE TIME, MESSAGE TYPE, SERVER ID, END
 */

static const unsigned char _options[] =
{
   _DHCP_OPTION_CODE_MASK,         4, 255, 0, 0, 0,     /** MASK is fixed at 255.0.0.0 */
   _DHCP_OPTION_CODE_ROUTER,       4,   0, 0, 0, 0,
   _DHCP_OPTION_CODE_DNS,          4,   0, 0, 0, 0,
   _DHCP_OPTION_CODE_LEASE_TIME,   4,   0, 0, 0, 0,
   _DHCP_OPTION_CODE_MESSAGE_TYPE, 1,   0,
   _DHCP_OPTION_CODE_SERVER_ID,    4,   0, 0, 0, 0,
   _DHCP_OPTION_CODE_END
};

#define _ARP_POSITION_HTYPE            0
#define _ARP_POSITION_PTYPE            (_ARP_POSITION_HTYPE + 2)
#define _ARP_POSITION_HLEN             (_ARP_POSITION_PTYPE + 2)
#define _ARP_POSITION_PLEN             (_ARP_POSITION_HLEN + 1)
#define _ARP_POSITION_OPCODE           (_ARP_POSITION_PLEN + 1)
#define _ARP_POSITION_SRCMAC           (_ARP_POSITION_OPCODE + 2)
#define _ARP_POSITION_SRCIP            (_ARP_POSITION_SRCMAC + 6)
#define _ARP_POSITION_DSTMAC           (_ARP_POSITION_SRCIP + 4)
#define _ARP_POSITION_DSTIP            (_ARP_POSITION_DSTMAC + 6)

#define _ARP_PACKET_SIZE               (_ARP_POSITION_DSTIP + 4)

#define _ARP_HARDWARE_TYPE_ETH         1
#define _ARP_HARDWARE_TYPE_ETH_ALEN    6
#define _ARP_OPCODE_REQUEST            1
#define _ARP_OPCODE_REPLY              2


meshap_dhcp_instance_t meshap_dhcp_initialize(al_net_addr_t *self_mac_address,
                                              unsigned char net_id,
                                              unsigned char host_id_1,
                                              unsigned char host_id_2,
                                              unsigned char *subnet_mask,
                                              unsigned char *gateway_ip_addr,
                                              unsigned char *dns_addr,
                                              unsigned int  lease_time_millis)
{
   _dhcp_priv_t *priv;

   priv = (_dhcp_priv_t *)al_heap_alloc(AL_CONTEXT sizeof(_dhcp_priv_t));

   memset(priv, 0, sizeof(_dhcp_priv_t));

   memcpy(&priv->self_mac_address, self_mac_address, sizeof(al_net_addr_t));

   priv->lease_time_in_millis = lease_time_millis;
   priv->net_id = net_id;
   priv->m      = host_id_1;
   priv->n      = host_id_2;

   priv->self_ip[0] = priv->net_id;
   priv->self_ip[1] = priv->m;
   priv->self_ip[2] = priv->n;
   priv->self_ip[3] = 1;

   priv->subnet_mask[0] = 255;
   priv->subnet_mask[1] = 0;
   priv->subnet_mask[2] = 0;
   priv->subnet_mask[3] = 0;

   memcpy(priv->gateway, priv->self_ip, _IP_ADDR_SIZE);
   memcpy(priv->dns, priv->self_ip, _IP_ADDR_SIZE);

   if ((gateway_ip_addr != NULL) && (gateway_ip_addr[0] != 0))
   {
      memcpy(priv->gateway, gateway_ip_addr, _IP_ADDR_SIZE);
   }

   if ((dns_addr != NULL) && (dns_addr[0] != 0))
   {
      memcpy(priv->dns, dns_addr, _IP_ADDR_SIZE);
   }

   if ((subnet_mask != NULL) && (subnet_mask[0] != 0))
   {
      memcpy(priv->subnet_mask, subnet_mask, _IP_ADDR_SIZE);
   }

   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,
                "DHCP: Initialized : Range %d.%d.%d.(2-254) Mask %d.%d.%d.%d Gateway %d.%d.%d.%d DNS %d.%d.%d.%d",
                priv->net_id,
                priv->m,
                priv->n,
                priv->subnet_mask[0],
                priv->subnet_mask[1],
                priv->subnet_mask[2],
                priv->subnet_mask[3],
                priv->gateway[0],
                priv->gateway[1],
                priv->gateway[2],
                priv->gateway[3],
                priv->dns[0],
                priv->dns[1],
                priv->dns[2],
                priv->dns[3]);

   return (meshap_dhcp_instance_t)priv;
}


void meshap_dhcp_uninitialize(meshap_dhcp_instance_t instance)
{
   _dhcp_priv_t *priv;

   priv = (_dhcp_priv_t *)instance;

   al_heap_free(AL_CONTEXT priv);
}


static AL_INLINE int ___get_unused_index(_dhcp_priv_t *priv)
{
   int index;

   if (priv->current_pos >= _MAX_ALLOC_IP_COUNT)
   {
      priv->current_pos = 0;
   }

   for (index = priv->current_pos; index < _MAX_ALLOC_IP_COUNT; index++)
   {
      if (!priv->client_info[index].allocated)
      {
         break;
      }
   }

   return index;
}


static AL_INLINE void ___expire_indices(_dhcp_priv_t *priv)
{
   int      i;
   al_u64_t tick_count;
   int      expire;

   for (i = 0; i < _MAX_ALLOC_IP_COUNT; i++)
   {
      if (!priv->client_info[i].allocated)
      {
         continue;
      }

      expire     = 0;
      tick_count = al_get_tick_count(AL_CONTEXT_SINGLE);

      switch (priv->client_info[i].state)
      {
      case _CLIENT_INFO_STATE_OFFERED:
         if (al_timer_after_eq(tick_count, priv->client_info[i].alloc_time_stamp + (_CLIENT_INFO_OFFER_EXPIRY * HZ)))
         {
            expire = 1;
         }
         break;

      case _CLIENT_INFO_STATE_ACK:
         if (al_timer_after_eq(tick_count, priv->client_info[i].alloc_time_stamp + ((priv->lease_time_in_millis / 1000) * HZ)))
         {
            expire = 1;
         }
         break;

      case _CLIENT_INFO_STATE_TAKEN:
         expire = 0;
         break;

      default:
         expire = 1;
      }

      if (expire)
      {
         priv->client_info[i].allocated = 0;
         priv->client_info[i].state     = _CLIENT_INFO_STATE_NONE;
      }
   }
}


static AL_INLINE int _get_message_type(unsigned char *options_begin)
{
   int i;

   i = 0;

   while (1)
   {
      if (options_begin[i] == _DHCP_OPTION_CODE_END)
      {
         break;
      }

      switch (options_begin[i])
      {
      case _DHCP_OPTION_CODE_PAD:
         i++;
         break;

      case _DHCP_OPTION_CODE_MESSAGE_TYPE:
         i += 2;                /* Skip Length field */
         return options_begin[i];

      default:
         i++;
         i += options_begin[i] + 1;
      }
   }

   return -1;
}


static AL_INLINE unsigned char *__get_requested_ip(unsigned char *options_begin)
{
   int i;

   i = 0;

   while (1)
   {
      if (options_begin[i] == _DHCP_OPTION_CODE_END)
      {
         break;
      }

      switch (options_begin[i])
      {
      case _DHCP_OPTION_CODE_PAD:
         i++;
         break;

      case _DHCP_OPTION_REQUESTED_IP:
         i += 2;                /* Skip Length field */
         return &options_begin[i];

      default:
         i++;
         i += options_begin[i] + 1;
      }
   }

   return NULL;
}


static AL_INLINE int __get_unused_index(_dhcp_priv_t *priv)
{
   int index;

   /**
    * Find un-allocated IP and send DHCP Offer. Mark the IP with a short expiration time.
    * The allocation range is net_id.m.n.2 to net_id.m.n.254 inclusive.
    * index goes from 0 to 252, the allocated address is net_id.m.n.(index + 2)
    */

   index = ___get_unused_index(priv);

   if (index >= _MAX_ALLOC_IP_COUNT)
   {
      ___expire_indices(priv);
      index = ___get_unused_index(priv);
   }

   return index;
}


static AL_INLINE void __clear_previous_indices(_dhcp_priv_t *priv, al_net_addr_t *src_addr, int current_index)
{
   int index;

   for (index = 0; index < _MAX_ALLOC_IP_COUNT; index++)
   {
      if (!priv->client_info[index].allocated || (index == current_index))
      {
         continue;
      }

      if (AL_NET_ADDR_EQUAL(src_addr, &priv->client_info[index].client_addr))
      {
         priv->client_info[index].allocated = 0;
         priv->client_info[index].state     = _CLIENT_INFO_STATE_NONE;
      }
   }
}


static AL_INLINE int __search_previous_index(_dhcp_priv_t *priv, al_net_addr_t *src_addr)
{
   int index;

   for (index = 0; index < _MAX_ALLOC_IP_COUNT; index++)
   {
      if (!priv->client_info[index].allocated)
      {
         continue;
      }

      if (AL_NET_ADDR_EQUAL(src_addr, &priv->client_info[index].client_addr))
      {
         priv->client_info[index].allocated = 0;
         priv->client_info[index].state     = _CLIENT_INFO_STATE_NONE;
         break;
      }
   }

   return index;
}


static AL_INLINE int __is_requested_ip_valid(_dhcp_priv_t *priv, al_net_addr_t *src_addr, unsigned char *requested_ip)
{
   int index;

   if (requested_ip == NULL)
   {
      return 0;
   }

   /**
    * Requested IP is valid if it belongs to the same range (net_id.m.n.2 to net_id.m.n.254)
    * and EITHER it is un-allocated OR if allocated, is assigned to the  same MAC address
    */

   if ((requested_ip[0] != priv->net_id) ||
       (requested_ip[1] != priv->m) ||
       (requested_ip[2] != priv->n) ||
       (requested_ip[3] <= 1) ||
       (requested_ip[3] == 255))
   {
      return 0;
   }

   index = requested_ip[3] - 2;

   if (!priv->client_info[index].allocated)
   {
      return 1;
   }

   if (priv->client_info[index].state == _CLIENT_INFO_STATE_TAKEN)
   {
      return 0;
   }

   return AL_NET_ADDR_EQUAL(&priv->client_info[index].client_addr, src_addr);
}


static AL_INLINE int __fill_dhcp_message(_dhcp_priv_t  *priv,
                                         unsigned int  xid,
                                         int           index,
                                         unsigned char *dhcp_data_in,
                                         unsigned char *dhcp_data_out)
{
   memset(dhcp_data_out, 0, _DHCP_MESSAGE_SIZE_BEFORE_OPTIONS);

   dhcp_data_out[_DHCP_POSITION_OP]    = _DHCP_OP_VALUE_REPLY;
   dhcp_data_out[_DHCP_POSITION_HTYPE] = _ARP_HARDWARE_TYPE_ETH;
   dhcp_data_out[_DHCP_POSITION_HLEN]  = _ARP_HARDWARE_TYPE_ETH_ALEN;
   dhcp_data_out[_DHCP_POSITION_HOPS]  = 0;

   memcpy(&dhcp_data_out[_DHCP_POSITION_XID], &xid, 4);

   memcpy(&dhcp_data_out[_DHCP_POSITION_CIADDR], &dhcp_data_in[_DHCP_POSITION_CIADDR], 4);

   if (index != -1)
   {
      dhcp_data_out[_DHCP_POSITION_YIADDR]     = priv->net_id;
      dhcp_data_out[_DHCP_POSITION_YIADDR + 1] = priv->m;
      dhcp_data_out[_DHCP_POSITION_YIADDR + 2] = priv->n;
      dhcp_data_out[_DHCP_POSITION_YIADDR + 3] = index + 2;
   }

   memcpy(&dhcp_data_out[_DHCP_POSITION_GIADDR], &dhcp_data_in[_DHCP_POSITION_GIADDR], 4);
   memcpy(&dhcp_data_out[_DHCP_POSITION_CHADDR], &dhcp_data_in[_DHCP_POSITION_CHADDR], 16);

   return _DHCP_MESSAGE_SIZE_BEFORE_OPTIONS;
}


static AL_INLINE int __fill_dhcp_options(_dhcp_priv_t  *priv,
                                         unsigned char message_type,
                                         unsigned char *dhcp_data_out)
{
   unsigned char       *p;
   const unsigned char *o;
   unsigned int        temp;

   p = dhcp_data_out;

   memcpy(p, _options_cookie, _DHCP_OPTIONS_COOKIE_SIZE);
   p += _DHCP_OPTIONS_COOKIE_SIZE;

   memset(p, 0, sizeof(_options));

   /**
    * DHCP NACK messages only contain the message type option
    */

   if (message_type == _DHCP_OPTION_MESSAGE_TYPE_NACK)
   {
      *p++ = _DHCP_OPTION_CODE_MESSAGE_TYPE;
      *p++ = 1;
      *p++ = message_type;
      goto _out;
   }

   o = _options;

   while (o < _options + sizeof(_options))
   {
      switch (*o)
      {
      case _DHCP_OPTION_CODE_MASK:
         *p++ = *o++;                                                           /* Copy code */
         *p++ = *o++;                                                           /* Copy  length */
         memcpy(p, priv->subnet_mask, 4);
         o += 4;
         p += 4;                                                                /** Increment */
         break;

      case _DHCP_OPTION_CODE_ROUTER:
         *p++ = *o++;                                                           /* Copy code */
         *p++ = *o++;                                                           /* Copy  length */
         memcpy(p, priv->gateway, 4);
         o += 4;
         p += 4;                                                                /** Increment */
         break;

      case _DHCP_OPTION_CODE_DNS:
         *p++ = *o++;                                                           /* Copy code */
         *p++ = *o++;                                                           /* Copy  length */
         memcpy(p, priv->dns, 4);
         o += 4;
         p += 4;                                                                /** Increment */
         break;

      case _DHCP_OPTION_CODE_SERVER_ID:
         *p++ = *o++;                                                           /* Copy code */
         *p++ = *o++;                                                           /* Copy  length */
         memcpy(p, priv->self_ip, 4);
         o += 4;
         p += 4;                                                                /** Increment */
         break;

      case _DHCP_OPTION_CODE_LEASE_TIME:

         if (message_type != _DHCP_OPTION_MESSAGE_TYPE_ACK)
         {
            *p++ = *o++;                                                                /* Copy code */
            *p++ = *o++;                                                                /* Copy  length */

            temp = (priv->lease_time_in_millis) / 1000;
            temp = al_cpu_to_be32(temp);
            memcpy(p, &temp, 4);

            o += 4;
            p += 4;                                                                     /** Increment */
         }
         else
         {
            o += 6;
         }

         break;

      case _DHCP_OPTION_CODE_MESSAGE_TYPE:
         *p++ = *o++;                                                           /* Copy code */
         *p++ = *o++;                                                           /* Copy  length */
         *p   = message_type;

         o += 1;
         p += 1;                                                                /** Increment */
         break;

      case _DHCP_OPTION_CODE_END:
         *p++ = *o;                                                             /** Do not icrement o */
         break;
      }

      if (*o == _DHCP_OPTION_CODE_END)
      {
         break;
      }
   }

_out:

   return(p - dhcp_data_out);
}


static AL_INLINE int _do_offer(_dhcp_priv_t  *priv,
                               al_net_addr_t *src_addr,
                               unsigned int  xid,
                               unsigned int  flags,
                               unsigned char *dhcp_data,
                               al_packet_t   **packet_out)
{
   int           index;
   unsigned char *requested_ip;
   al_packet_t   *packet;
   unsigned char *buffer;
   unsigned char *p;

   requested_ip = __get_requested_ip(dhcp_data + _DHCP_POSITION_OPTIONS_BEGIN);

   if (!__is_requested_ip_valid(priv, src_addr, requested_ip))
   {
      index = __search_previous_index(priv, src_addr);

      if (index >= _MAX_ALLOC_IP_COUNT)
      {
         index = __get_unused_index(priv);
      }

      if (index >= _MAX_ALLOC_IP_COUNT)
      {
         /**
          * We have run-out of IP addresses.
          */
         return MESHAP_DHCP_RETVAL_DISCARD;
      }

      priv->current_pos = index + 1;
   }
   else
   {
      index = requested_ip[3] - 2;
   }

   __clear_previous_indices(priv, src_addr, index);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,
                "DHCP: Offering "AL_NET_ADDR_STR " IP address %d.%d.%d.%d",
                AL_NET_ADDR_TO_STR(src_addr),
                priv->net_id,
                priv->m,
                priv->n,
                index + 2);

   priv->client_info[index].allocated        = 1;
   priv->client_info[index].alloc_time_stamp = al_get_tick_count(AL_CONTEXT_SINGLE);
   memcpy(&priv->client_info[index].client_addr, src_addr, sizeof(al_net_addr_t));
   priv->client_info[index].state = _CLIENT_INFO_STATE_OFFERED;

   buffer = al_get_gen_2KB_buffer(AL_CONTEXT_SINGLE);

   p  = buffer;
   p += __fill_dhcp_message(priv, xid, index, dhcp_data, p);
   p += __fill_dhcp_options(priv, _DHCP_OPTION_MESSAGE_TYPE_OFFER, p);

   packet = al_allocate_packet(AL_CONTEXT AL_PACKET_DIRECTION_OUT);

   if (packet == NULL)
   {
      al_release_gen_2KB_buffer(AL_CONTEXT buffer);
      return MESHAP_DHCP_RETVAL_DISCARD;
   }

   udp_setup_packet(packet,
                    &priv->self_mac_address,
                    _DHCP_SERVER_UDP_PORT,
                    &broadcast_net_addr,
                    _DHCP_CLIENT_UDP_PORT,
                    buffer, (p - buffer));


   ip_setup_packet_ex(packet, priv->self_ip, NULL);

   al_release_gen_2KB_buffer(AL_CONTEXT buffer);

   *packet_out = packet;

   return MESHAP_DHCP_RETVAL_SUCCESS;
}


static AL_INLINE int _do_ack(_dhcp_priv_t  *priv,
                             al_net_addr_t *src_addr,
                             unsigned int  xid,
                             unsigned int  flags,
                             unsigned char *dhcp_data,
                             al_packet_t   **packet_out)
{
   int           index;
   unsigned char *requested_ip;
   al_packet_t   *packet;
   unsigned char *buffer;
   unsigned char *p;
   unsigned char message_type;

   /** Send DHCP ACK, and mark the IP with the correct state */

   requested_ip = __get_requested_ip(dhcp_data + _DHCP_POSITION_OPTIONS_BEGIN);

   if (!__is_requested_ip_valid(priv, src_addr, requested_ip))
   {
      if (requested_ip == NULL)
      {
         al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,
                      "DHCP: "AL_NET_ADDR_STR " sent invalid REQUEST with no requested IP option",
                      AL_NET_ADDR_TO_STR(src_addr));
      }
      else
      {
         al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,
                      "DHCP: "AL_NET_ADDR_STR " requests invalid IP address %d.%d.%d.%d",
                      AL_NET_ADDR_TO_STR(src_addr),
                      requested_ip[0],
                      requested_ip[1],
                      requested_ip[2],
                      requested_ip[3]);
      }

      message_type = _DHCP_OPTION_MESSAGE_TYPE_NACK;
      index        = -1;

      goto _xmit;
   }

   index        = requested_ip[3] - 2;
   message_type = _DHCP_OPTION_MESSAGE_TYPE_ACK;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,
                "DHCP: "AL_NET_ADDR_STR " IP address %d.%d.%d.%d acknowledged",
                AL_NET_ADDR_TO_STR(src_addr),
                priv->net_id,
                priv->m,
                priv->n,
                index + 2);

   priv->client_info[index].allocated        = 1;
   priv->client_info[index].state            = _CLIENT_INFO_STATE_ACK;
   priv->client_info[index].alloc_time_stamp = al_get_tick_count(AL_CONTEXT_SINGLE);


_xmit:

   buffer = al_get_gen_2KB_buffer(AL_CONTEXT_SINGLE);

   p  = buffer;
   p += __fill_dhcp_message(priv, xid, index, dhcp_data, p);
   p += __fill_dhcp_options(priv, message_type, p);

   packet = al_allocate_packet(AL_CONTEXT AL_PACKET_DIRECTION_OUT);

   if (packet == NULL)
   {
      al_release_gen_2KB_buffer(AL_CONTEXT buffer);
      return MESHAP_DHCP_RETVAL_DISCARD;
   }

   udp_setup_packet(packet,
                    &priv->self_mac_address,
                    _DHCP_SERVER_UDP_PORT,
                    &broadcast_net_addr,
                    _DHCP_CLIENT_UDP_PORT,
                    buffer, (p - buffer));


   ip_setup_packet_ex(packet, priv->self_ip, NULL);

   al_release_gen_2KB_buffer(AL_CONTEXT buffer);

   *packet_out = packet;

   return MESHAP_DHCP_RETVAL_SUCCESS;
}


static AL_INLINE void _do_decline(_dhcp_priv_t  *priv,
                                  al_net_addr_t *src_addr,
                                  unsigned char *dhcp_data)
{
   int           index;
   unsigned char *requested_ip;

   requested_ip = __get_requested_ip(&dhcp_data[_DHCP_POSITION_OPTIONS_BEGIN]);

   if (!__is_requested_ip_valid(priv, src_addr, requested_ip))
   {
      return;
   }

   index = requested_ip[3] - 2;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,
                "DHCP: "AL_NET_ADDR_STR " IP address %d.%d.%d.%d declined",
                AL_NET_ADDR_TO_STR(src_addr),
                priv->net_id,
                priv->m,
                priv->n,
                index + 2);

   priv->client_info[index].allocated = 1;
   priv->client_info[index].state     = _CLIENT_INFO_STATE_TAKEN;
}


static AL_INLINE void _do_release(_dhcp_priv_t  *priv,
                                  al_net_addr_t *src_addr,
                                  unsigned char *dhcp_data)
{
   int           index;
   unsigned char *requested_ip;

   requested_ip = &dhcp_data[_DHCP_POSITION_CIADDR];

   if (!__is_requested_ip_valid(priv, src_addr, requested_ip))
   {
      return;
   }

   index = requested_ip[3] - 2;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,
                "DHCP: "AL_NET_ADDR_STR " IP address %d.%d.%d.%d released",
                AL_NET_ADDR_TO_STR(src_addr),
                priv->net_id,
                priv->m,
                priv->n,
                index + 2);

   priv->client_info[index].allocated = 0;
   priv->client_info[index].state     = _CLIENT_INFO_STATE_NONE;
}


/**
 * DHCP clients may send UNICAST messages to the address identified by the SERVER ID
 * Since we send net_id.m.n.1 as the SERVER ID, we would need to respond to
 * ARP requests for net_id.m.n.1.
 */
static AL_INLINE int _do_arp(_dhcp_priv_t  *priv,
                             al_net_if_t   *net_if,
                             al_net_addr_t *src_addr,
                             al_packet_t   *packet_in,
                             al_packet_t   **packet_out)
{
   unsigned short opcode;
   unsigned char  arp_src_ip[4];
   unsigned char  arp_target_ip[4];
   unsigned char  *packet_data;
   al_packet_t    *packet;
   unsigned short temp;

   *packet_out = NULL;
   packet_data = packet_in->buffer + packet_in->position;

   memcpy(&opcode, packet_data + _ARP_POSITION_OPCODE, 2);

   opcode = al_be16_to_cpu(opcode);

   if (opcode != _ARP_OPCODE_REQUEST)
   {
      return MESHAP_DHCP_RETVAL_CONTINUE;
   }

   memcpy(arp_src_ip, packet_data + _ARP_POSITION_SRCIP, 4);
   memcpy(arp_target_ip, packet_data + _ARP_POSITION_DSTIP, 4);

   if (memcmp(priv->self_ip, arp_target_ip, 4))
   {
      return MESHAP_DHCP_RETVAL_CONTINUE;
   }

   /**
    * Create a ARP reply packet
    */

   packet = al_allocate_packet(AL_CONTEXT AL_PACKET_DIRECTION_OUT);

   if (packet == NULL)
   {
      return MESHAP_DHCP_RETVAL_CONTINUE;
   }

   packet_data = packet->buffer + packet->position - _ARP_PACKET_SIZE;

   temp = _ARP_HARDWARE_TYPE_ETH;
   temp = al_cpu_to_be16(temp);
   memcpy(packet_data + _ARP_POSITION_HTYPE, &temp, 2);

   temp = AL_PACKET_TYPE_IP;
   temp = al_cpu_to_be16(temp);
   memcpy(packet_data + _ARP_POSITION_PTYPE, &temp, 2);

   packet_data[_ARP_POSITION_HLEN] = _ARP_HARDWARE_TYPE_ETH_ALEN;
   packet_data[_ARP_POSITION_PLEN] = _IP_ADDR_SIZE;

   temp = _ARP_OPCODE_REPLY;
   temp = al_cpu_to_be16(temp);
   memcpy(packet_data + _ARP_POSITION_OPCODE, &temp, 2);

   memcpy(packet_data + _ARP_POSITION_SRCMAC, priv->self_mac_address.bytes, _ARP_HARDWARE_TYPE_ETH_ALEN);
   memcpy(packet_data + _ARP_POSITION_SRCIP, priv->self_ip, _IP_ADDR_SIZE);
   memcpy(packet_data + _ARP_POSITION_DSTMAC, src_addr->bytes, _ARP_HARDWARE_TYPE_ETH_ALEN);
   memcpy(packet_data + _ARP_POSITION_DSTIP, arp_src_ip, _IP_ADDR_SIZE);

   packet->position   -= _ARP_PACKET_SIZE;
   packet->data_length = _ARP_PACKET_SIZE;
   packet->type        = al_cpu_to_be16(AL_PACKET_TYPE_ARP);

   memcpy(&packet->addr_1, src_addr, sizeof(al_net_addr_t));
   memcpy(&packet->addr_2, &priv->self_mac_address, sizeof(al_net_addr_t));

   *packet_out = packet;

   return MESHAP_DHCP_RETVAL_SUCCESS;
}


int meshap_dhcp_process_packet(meshap_dhcp_instance_t instance,
                               al_net_if_t            *net_if,
                               al_packet_t            *packet_in,
                               al_packet_t            **packet_out)
{
   _dhcp_priv_t   *priv;
   unsigned char  *data;
   int            ret;
   unsigned int   xid;
   unsigned short flags;
   int            message_type;
   al_net_addr_t  *src_addr;

   priv        = (_dhcp_priv_t *)instance;
   src_addr    = &packet_in->addr_2;
   ret         = MESHAP_DHCP_RETVAL_SUCCESS;
   *packet_out = NULL;

   if (al_be16_to_cpu(packet_in->type) == AL_PACKET_TYPE_ARP)
   {
      ret = _do_arp(priv, net_if, src_addr, packet_in, packet_out);
      goto _xmit;
   }

   /**
    * Here we assume that the caller has already verified that the packet is
    * a IP->UDP->DHCP packet. Hence we start processing the UDP data.
    */

   data = packet_in->buffer + packet_in->position + IP_MIN_HEADER_LENGTH + UDP_HEADER_LENGTH;

   if (data[_DHCP_POSITION_OP] != _DHCP_OP_VALUE_REQUEST)
   {
	   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,
			   "DHCP: Discarding packet from "AL_NET_ADDR_STR " with op=%d",
			   AL_NET_ADDR_TO_STR(src_addr),
			   data[_DHCP_POSITION_OP]);
	  return MESHAP_DHCP_RETVAL_DISCARD;
   }

   memcpy(&xid, &data[_DHCP_POSITION_XID], 4);
   memcpy(&flags, &data[_DHCP_POSITION_FLAGS], 2);

   flags = al_be16_to_cpu(flags);

   if (memcmp(&data[_DHCP_POSITION_OPTIONS_COOKIE], _options_cookie, sizeof(_options_cookie)))
   {
	   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,
			   "DHCP: Discarding packet from "AL_NET_ADDR_STR " with option cookie mismatch",
			   AL_NET_ADDR_TO_STR(src_addr));
	  return MESHAP_DHCP_RETVAL_DISCARD;
   }

   message_type = _get_message_type(&data[_DHCP_POSITION_OPTIONS_BEGIN]);

   switch (message_type)
   {
   case _DHCP_OPTION_MESSAGE_TYPE_DISCOVER:
      ret = _do_offer(priv, src_addr, xid, flags, data, packet_out);
      break;

   case _DHCP_OPTION_MESSAGE_TYPE_REQUEST:
   case _DHCP_OPTION_MESSAGE_TYPE_INFORM:
      ret = _do_ack(priv, src_addr, xid, flags, data, packet_out);
      break;

   case _DHCP_OPTION_MESSAGE_TYPE_DECLINE:
      _do_decline(priv, src_addr, data);
      break;

   case _DHCP_OPTION_MESSAGE_TYPE_RELEASE:
      _do_release(priv, src_addr, data);
      break;

   default:
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,
                   "DHCP: Discarding packet from "AL_NET_ADDR_STR " with unknown message type %d",
                   AL_NET_ADDR_TO_STR(src_addr),
                   message_type);
   }

_xmit:

   /**
    * If *packet_out is not NULL, we need to setup the correct
    * frame control and flags depending on the type of the net_if.
    *  Moreover the *packet_out current has ADDR1 as DST, and ADDR2
    * as the SRC. In case of 802.11 interfaces, ADDR3 needs to be
    * set to the SRC address and ADDR2 is set to the BSSID
    */

   if ((ret == MESHAP_DHCP_RETVAL_SUCCESS) && (*packet_out != NULL) && AL_NET_IF_IS_WIRELESS(net_if))
   {
      (*packet_out)->frame_control = AL_802_11_FC_FROMDS;
      memcpy(&(*packet_out)->addr_3, &(*packet_out)->addr_2, sizeof(al_net_addr_t));
      memcpy(&(*packet_out)->addr_2, &net_if->config.hw_addr, sizeof(al_net_addr_t));
   }

   return ret;
}
