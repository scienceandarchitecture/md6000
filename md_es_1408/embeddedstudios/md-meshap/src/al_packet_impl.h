/********************************************************************************
* MeshDynamics
* --------------
* File     : al_packet_impl.h
* Comments : Torna Abstraction Layer Packet Implementation
* Created  : 5/26/2004
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  2  |8/16/2007 | Added al_packet_impl_get_pool_info              | Sriram |
* -----------------------------------------------------------------------------
* |  1  |7/22/2004 | Added al_packet_impl_create_skb                 | Sriram |
* -----------------------------------------------------------------------------
* |  0  |5/26/2004 | Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

#ifndef __AL_PACKET_IMPL_H__
#define __AL_PACKET_IMPL_H__


meshap_core_packet_t *al_packet_impl_create_from_skb(meshap_core_net_if_t *incoming_if, struct sk_buff *skb, char pool_type);
meshap_core_packet_t *al_packet_impl_create_outgoing_packet_from_skb(struct sk_buff *skb);
meshap_core_packet_t *al_packet_impl_create(void);
void al_packet_impl_setup_skb(meshap_core_net_if_t *outgoing_if, meshap_core_packet_t *core_packet);
struct sk_buff *al_packet_impl_create_skb(meshap_core_net_if_t *incoming_if, meshap_core_packet_t *core_packet);
void al_packet_impl_destroy(meshap_core_packet_t *core_packet);
void al_packet_impl_get_pool_info(al_packet_pool_info_t *info);
void al_packet_impl_initialize(void);
void al_packet_impl_uninitialize(void);
void _refill_procs_skb_core_pkt_pool_from_common_pool(void);
#endif /*__AL_PACKET_IMPL_H__*/
