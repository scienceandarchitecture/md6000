/********************************************************************************
* MeshDynamics
* --------------
* File     : meshap_core.c
* Comments : Torna MeshAP Core implementation
* Created  : 5/25/2004
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* | 29  |8/25/2008 | Changes for SIP                                 | Sriram |
* -----------------------------------------------------------------------------
* | 28  |5/21/2008 | Reboot locking bug fixed                        | Sriram |
* -----------------------------------------------------------------------------
* | 27  |01/07/2008| Changes for mixed mode						  |Prachiti|
* -----------------------------------------------------------------------------
* | 26  |11/20/2007 | Disable IRQ when writing reboot info           | Sriram |
* -----------------------------------------------------------------------------
* | 25  |8/2/2007  | Disable IRQ when queuing MGMT frames            | Sriram |
* -----------------------------------------------------------------------------
* | 24  |7/24/2007 | REBOOT logging changes                          | Sriram |
* -----------------------------------------------------------------------------
* | 23  |4/11/2007 | Changes for Power test Rev 1.3                  | Sriram |
* -----------------------------------------------------------------------------
* | 22  |12/20/2006| Reboot when cards do not get detected           | Sriram |
* -----------------------------------------------------------------------------
* | 21  |11/14/2006| Added heap information support                  | Sriram |
* -----------------------------------------------------------------------------
* | 20  |11/8/2006 | Changes for compression support                 | Sriram |
* -----------------------------------------------------------------------------
* | 19  |9/27/2006 | Changes for power test and NULL check           | Sriram |
* -----------------------------------------------------------------------------
* | 18  |6/13/2006 | Interrupts disable while process auth           | Sriram |
* ----------------------------------------------------------------------------
* | 17  |02/15/2006| Changes for dot11e                              | Bindu  |
* -----------------------------------------------------------------------------
* | 16  |01/17/2006| Changes for aid                                 | Sriram |
* -----------------------------------------------------------------------------
* | 15  |10/4/2005 | Misc changes                                    | Sriram |
* -----------------------------------------------------------------------------
* | 14  |9/22/2005 | Interrupt disable/enable semantics changed      | Sriram |
* -----------------------------------------------------------------------------
* | 13  |9/15/2005 | meshap_core_get_sta_info Added                  | Sriram |
* -----------------------------------------------------------------------------
* | 12  |7/29/2005 | Used dev_queue_xmit                             | Sriram |
* -----------------------------------------------------------------------------
* | 11  |5/8/2005  | Watchdog timer framework                        | Sriram |
* -----------------------------------------------------------------------------
* | 10  |4/15/2005 | Misc changes                                    | Sriram |
* -----------------------------------------------------------------------------
* |  9  |4/12/2005 | Assoc request processing fixed                  | Sriram |
* -----------------------------------------------------------------------------
* |  8  |4/11/2005 | IE iteration logic corrected                    | Sriram |
* -----------------------------------------------------------------------------
* |  7  |4/7/2005  | Misc changes                                    | Sriram |
* -----------------------------------------------------------------------------
* |  6  |4/6/2005  | Security changes                                | Sriram |
* -----------------------------------------------------------------------------
* |  5  |7/23/2004 | Misc changes                                    | Sriram |
* -----------------------------------------------------------------------------
* |  4  |7/21/2004 | Checking return type for ap_initialize          | Anand  |
* -----------------------------------------------------------------------------
* |  3  |6/11/2004 | Set the last_rx time                            | Sriram |
* -----------------------------------------------------------------------------
* |  2  |6/10/2004 | Chnages for re-assoc implementation             | Sriram |
* -----------------------------------------------------------------------------
* |  1  |6/9/2004  | Misc changes                                    | Sriram |
* -----------------------------------------------------------------------------
* |  0  |5/25/2004 | Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

#include <generated/autoconf.h>
#include <linux/module.h>
#include <linux/init.h>
#include <linux/version.h>
#include <linux/netdevice.h>
#include <linux/skbuff.h>
#include <linux/wireless.h>
#include <linux/reboot.h>
#include <linux/kthread.h>
#include <linux/sched.h>
#include <linux/spinlock.h>
#include <linux/ip.h>
#include <net/mac80211.h>

#include "meshap.h"
#include "meshap_logger.h"
#include "al.h"
#include "al_802_11.h"
#include "meshap_core.h"
#include "torna_log.h"
#include "al_conf.h"
#include "dot11e_conf.h"
#include "al_net_if_impl.h"
#include "al_packet_impl.h"
#include "access_point.h"
#include "mesh_init.h"
#include "torna_mac_hdr.h"
#include "torna_hw_id.h"
#include "dot1x.h"
#include "dot11i.h"
#include "meshap_power_test.h"
#include "meshap_power_test_impl.h"
#include "v_if_info_conf.h"
#include "mesh_defs.h" 
#include "mesh.h"

AL_DECLARE_GLOBAL_EXTERN(int mesh_state);
AL_DECLARE_GLOBAL_EXTERN(access_point_globals_t globals);
AL_DECLARE_GLOBAL_EXTERN(mesh_config_info_t *mesh_config);

int access_point_dot11i_add_sta(AL_CONTEXT_PARAM_DECL al_net_addr_t *sta_addr);
meshap_core_globals_t meshap_core_globals;
extern void dump_bytes(char *, int);
extern void dump_bytes1(char *D, int count);
int meshap_netdev_event(struct notifier_block *unused, unsigned long event, void *ptr);
al_atomic_t  process_skb_schd_out;

static meshap_core_net_if_t *_created_ifs_list;
meshap_core_net_if_t *_ifs_list;
static int _created_ifs_count;
static int _ifs_count;

al_spinlock_t core_gbl_splock;
AL_DEFINE_PER_CPU(int, core_gbl_spvar);

al_spinlock_t core_pkt_splock;
AL_DEFINE_PER_CPU(int, core_pkt_spvar);

al_spinlock_t ap_thread_core_pkt_splock;
AL_DEFINE_PER_CPU(int, ap_thread_core_pkt_spvar);
al_spinlock_t imcp_thread_core_pkt_splock;
AL_DEFINE_PER_CPU(int, imcp_thread_core_pkt_spvar);

al_spinlock_t core_net_splock;
AL_DEFINE_PER_CPU(int, core_net_spvar);

/*data_pkt_queue is used to queue the data frames */
struct sk_buff_head data_pkt_queue[NUM_SKB_QUE];
struct sk_buff_head process_skb_queue;
al_spinlock_t skb_data_que_splock[NUM_SKB_QUE];
static unsigned char Q_weightage[NUM_SKB_QUE];
al_spinlock_t skb_upstack_que_splock;
struct task_struct *meshap_data_pkt_process_task = NULL;
void meshap_core_process_data_frame_t(void *data);
/*tx_skb_queue is used to queue the data frames into tx_queue_list*/
struct sk_buff_head tx_skb_queue;
struct sk_buff_head temp_tx_skb_queue;
/*upstack_pkt_queue is used to queue the data frames which need to send to upsatck */
struct sk_buff_head upstack_pkt_queue;
struct sk_buff_head temp_upstack_pkt_queue;
int upstack_pkt_queue_init_done = 0;
struct task_struct *meshap_upstack_pkt_process_task = NULL;

struct _ie_iterator
{
   unsigned char *ie_source;
   int           ie_source_length;

   unsigned char ie_id;
   unsigned char ie_datalength;
   unsigned char *ie_data;
};

typedef struct _ie_iterator   _ie_iterator_t;

static int _meshap_dot1x_timer(void *data);

int rate_init(void);

static void _set_virtual_net_if_for_physical_net_if(meshap_core_net_if_t *virtual_net_if, meshap_core_net_if_t *phy_net_if)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP_CORE : FLOW : Enter: %s : %d\n", __func__,__LINE__);

   switch (virtual_net_if->use_type)
   {
   case AL_CONF_IF_USE_TYPE_DS:
      phy_net_if->virtual_ds_if = virtual_net_if;
      break;

   case AL_CONF_IF_USE_TYPE_WM:
   case AL_CONF_IF_USE_TYPE_AP:
      phy_net_if->virtual_wm_if = virtual_net_if;
      break;
   }

   if ((phy_net_if->virtual_ds_if != NULL) &&
       (phy_net_if->virtual_wm_if != NULL))
   {
      virtual_net_if->dev_info->initialize_mixed_mode(virtual_net_if->dev);
   }
   virtual_net_if->dev_type = MESHAP_CORE_DEV_TYPE_VIRTUAL;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP_CORE : FLOW : Exit: %s : %d\n", __func__,__LINE__); 
}


static void _init_virtual_net_ifs(void)
{
   meshap_core_net_if_t *core_net_if;
   int                  virtual_conf_token;
   int                  virtual_if_count;
   _virtual_if_info_t   virt_if_info;
   meshap_core_net_if_t *p_net_if;
   int                  i;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP_CORE : FLOW : Enter: %s : %d\n", __func__,__LINE__);

   if (meshap_core_globals.v_if_info_conf_string == NULL)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESHAP_CORE : ERROR : meshap_core_globals.v_if_info_conf_string is NULL %s : %d\n", __func__,__LINE__);
      return;
   }

   virtual_conf_token = virt_if_info_conf_parse(AL_CONTEXT meshap_core_globals.v_if_info_conf_string);
   virtual_if_count   = virt_if_info_conf_get_count(AL_CONTEXT virtual_conf_token);

   for (i = 0; i < virtual_if_count; i++)
   {
      virt_if_info_conf_get_if(AL_CONTEXT virtual_conf_token, i, &virt_if_info);

      core_net_if = meshap_core_lookup_net_if(virt_if_info.name);

      if (core_net_if == NULL)
      {
         meshap_core_create_virtual_net_if(virt_if_info.name, virt_if_info.map_rx);
      }
      core_net_if->dev_type = MESHAP_CORE_DEV_TYPE_VIRTUAL;
      p_net_if = meshap_core_lookup_net_if(virt_if_info.map_rx);

      if (p_net_if == NULL)
      {
         al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"MESHAP_CORE : INFO : %s physical rx net if not found for %s %s : %d\n",
								virt_if_info.map_rx, virt_if_info.name, __func__,__LINE__);
         continue;
      }

      _set_virtual_net_if_for_physical_net_if(core_net_if, p_net_if);

      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"MESHAP_CORE : INFO : virtual if name : %s, map_rx : %s %s : %d\n",
      				virt_if_info.name, virt_if_info.map_rx, __func__,__LINE__);
   }

   virt_if_info_conf_close(AL_CONTEXT virtual_conf_token);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s : %d\n", __func__,__LINE__); 
}


/**
 * Returns 1 if mesh can be started, i.e
 * when atleast all WMs and one DS interface
 * has been found.
 */
static int _meshap_core_can_start(void)
{
   typedef struct _list
   {
      al_conf_if_info_t if_info;
      struct _list      *next;
   } _list_t;

   int                  i;
   int                  if_count;
   int                  all_wms_found;
   int                  ds_found;
   _list_t              *used_wm_list;
   _list_t              *used_ds_list;
   _list_t              *node;
   _list_t              *temp;
   meshap_core_net_if_t *core_net_if;
   int                  al_conf_token;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP_CORE : FLOW : Enter: %s : %d\n", __func__,__LINE__);

   if ((meshap_core_globals.config_string == NULL) ||
       (meshap_core_globals.dot11e_config_string == NULL) ||
       (meshap_core_globals.acl_config_string == NULL) ||
       (meshap_core_globals.mobility_conf_string == NULL))
   {
      return 0;
   }

   _init_virtual_net_ifs();
   al_conf_token = al_conf_parse(AL_CONTEXT meshap_core_globals.config_string);
   used_wm_list  = NULL;
   used_ds_list  = NULL;
   if_count      = al_conf_get_if_count(al_conf_token);
   for (i = 0; i < if_count; i++)
   {
      node = (_list_t *)kmalloc(sizeof(_list_t), GFP_ATOMIC);
      memset(node, 0, sizeof(_list_t));
      al_conf_get_if(al_conf_token, i, &node->if_info);
      switch (node->if_info.use_type)
      {
      case AL_CONF_IF_USE_TYPE_DS:
         node->next   = used_ds_list;
         used_ds_list = node;
         break;

      case AL_CONF_IF_USE_TYPE_WM:
      case AL_CONF_IF_USE_TYPE_AP:
         node->next   = used_wm_list;
         used_wm_list = node;
         break;
      }
   }

   /**
    * Now we scan through the list of WMs and see if *all*
    * the WMs have been physically created.
    */

   ds_found = 0;

   if (used_wm_list != NULL)
   {
      all_wms_found = 1;
   }
   else
   {
      all_wms_found = 0;
   }

   for (node = used_wm_list; node != NULL; node = node->next)
   {
      core_net_if = meshap_core_lookup_net_if(node->if_info.name);
      if (core_net_if != NULL)
      {
         if (!core_net_if->used)
         {
            meshap_core_use_net_if(core_net_if, node->if_info.phy_type, node->if_info.use_type);
         }
         if ((core_net_if->dev == NULL) || (core_net_if->dev_info == NULL))
         {
            al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"MESHAP_CORE : INFO : ifname %s core_net_ptr:%p %s : %d\n",
                                    node->if_info.name, core_net_if,  __func__, __LINE__);
            all_wms_found = 0;
            break;
         }
      }
      else
      {
         all_wms_found = 0;
         break;
      }
   }


   /**
    * Now we scan through the list of DSes and see if
    * *atleast one* of them has been physically created
    */

   for (node = used_ds_list; node != NULL; node = node->next)
   {
      core_net_if = meshap_core_lookup_net_if(node->if_info.name);
      if (core_net_if != NULL)
      {
         if (!core_net_if->used)
         {
            meshap_core_use_net_if(core_net_if, node->if_info.phy_type, node->if_info.use_type);
         }
         if ((core_net_if->dev != NULL) && (core_net_if->dev_info != NULL))
         {
            ds_found = 1;
            break;
         }
      }
      else
      {
         break;
      }
   }

   al_conf_close(AL_CONTEXT al_conf_token);
   for (node = used_wm_list; node != NULL; )
   {
      temp = node->next;
      kfree(node);
      node = temp;
   }

   for (node = used_ds_list; node != NULL; )
   {
      temp = node->next;
      kfree(node);
      node = temp;
   }

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP_CORE : FLOW : Exit: %s : %d\n", __func__,__LINE__); 
   return ds_found;
}


int meshap_core_initialize()
{
   int i;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP_CORE : FLOW : Enter: %s : %d\n", __func__,__LINE__);
   al_spin_lock_init(&core_gbl_splock);
   al_spin_lock_init(&core_pkt_splock);
   al_spin_lock_init(&core_net_splock);
   al_spin_lock_init(&skb_upstack_que_splock);

   meshap_core_globals.log_mask              = 0x201;
   meshap_core_globals.on_receive            = NULL;
   meshap_core_globals.on_before_transmit    = NULL;
   meshap_core_globals.on_phy_link_notify    = NULL;
   meshap_core_globals.config_string         = NULL;
   meshap_core_globals.dot11e_config_string  = NULL;
   meshap_core_globals.acl_config_string     = NULL;
   meshap_core_globals.mobility_conf_string  = NULL;
   meshap_core_globals.v_if_info_conf_string = NULL;
   meshap_core_globals.sip_config_string     = NULL;
   meshap_core_globals.state = MESHAP_CORE_GLOBALS_STATE_STOPPED;

#ifdef _AL_HEAP_DEBUG
   meshap_core_globals.heap_debug_list = NULL;
   for (i = 0; i < MESHAP_HEAP_DEBUG_HASH_SIZE; i++)
   {
      meshap_core_globals.heap_debug_hash[i] = NULL;
   }
   meshap_core_globals.max_block_size  = 0;
   meshap_core_globals.active_total    = 0;
   meshap_core_globals.active_max      = 0;
   meshap_core_globals.total_alloc     = 0;
   meshap_core_globals.total_freed     = 0;
   meshap_core_globals.alloc_count     = 0;
   meshap_core_globals.free_count      = 0;
   meshap_core_globals.free_miss_count = 0;
   meshap_core_globals.last_freed_item = NULL;
#endif

   MESHAP_ATOMIC_SET(meshap_core_globals.reboot_locked, 0);
   MESHAP_ATOMIC_SET(meshap_core_globals.reboot_pending, 0);

   _created_ifs_list  = NULL;
   _created_ifs_count = 0;
   _ifs_list          = NULL;
   _ifs_count         = 0;

   al_initialize(AL_CONTEXT_SINGLE);
   al_packet_impl_initialize();
   skb_queue_head_init(&tx_skb_queue);
   skb_queue_head_init(&temp_tx_skb_queue);
   for (i = 0; i < NUM_SKB_QUE; i++) {
       al_spin_lock_init(&skb_data_que_splock[i]);
	   skb_queue_head_init(&data_pkt_queue[i]);
	   /*Can fine-tune based on need,
	    *as of now, weightage of Q[0-3] is 1,2,3,4 */
	   Q_weightage[i] = i + 1;
   }
   skb_queue_head_init(&process_skb_queue);
   AL_ATOMIC_SET(globals.ap_thread_disabled, 1);
   meshap_data_pkt_process_task = kthread_create(meshap_core_process_data_frame_t, NULL, "process_skb");
   kthread_bind(meshap_data_pkt_process_task,0);

   skb_queue_head_init(&upstack_pkt_queue);
   skb_queue_head_init(&temp_upstack_pkt_queue);
   meshap_upstack_pkt_process_task = kthread_create(meshap_process_upstack_pkt, NULL, "prs_upstack_pkt");
   kthread_bind(meshap_upstack_pkt_process_task,0);

   meshap_power_test_initialize();

   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"MESHAP_CORE : INFO : log_mask=%d %s : %d\n", meshap_core_globals.log_mask, __func__, __LINE__);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP_CORE : FLOW : Exit: %s : %d\n", __func__,__LINE__); 
   return 0;
}


int meshap_core_uninitialize(void)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP_CORE : FLOW : Enter: %s : %d\n", __func__,__LINE__);
   meshap_core_stop_mesh();

   meshap_core_remove_net_ifs();

   if (meshap_core_globals.config_string != NULL)
   {
      kfree(meshap_core_globals.config_string);
   }

   if (meshap_core_globals.dot11e_config_string != NULL)
   {
      kfree(meshap_core_globals.dot11e_config_string);
   }

   if (meshap_core_globals.acl_config_string != NULL)
   {
      kfree(meshap_core_globals.acl_config_string);
   }

   if (meshap_core_globals.mobility_conf_string != NULL)
   {
      kfree(meshap_core_globals.mobility_conf_string);
   }

   if (meshap_core_globals.v_if_info_conf_string != NULL)
   {
      kfree(meshap_core_globals.v_if_info_conf_string);
   }

   if (meshap_core_globals.sip_config_string != NULL)
   {
      kfree(meshap_core_globals.sip_config_string);
   }

   meshap_power_test_uninitialize();

   al_packet_impl_uninitialize();
   al_uninitialize(AL_CONTEXT_SINGLE);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP_CORE : FLOW : Exit: %s : %d\n", __func__,__LINE__); 
   return 0;
}


void meshap_core_remove_net_ifs(void)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP_CORE : FLOW : Enter: %s : %d\n", __func__,__LINE__);
   meshap_core_net_if_t *node;
   meshap_core_net_if_t *temp;

   for (node = _ifs_list; node != NULL; )
   {
      temp = node->next;
      kfree(node);
      node = temp;
   }

   _created_ifs_list  = NULL;
   _created_ifs_count = 0;
   _ifs_list          = NULL;
   _ifs_count         = 0;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP_CORE : FLOW : Exit: %s : %d\n", __func__,__LINE__); 
}


meshap_core_net_if_t *meshap_core_lookup_net_if(const char *name)
{
   meshap_core_net_if_t *node;
   unsigned long        flags;


   node = _ifs_list;

   while (node != NULL)
   {
      if (!strcmp(node->name, name))
      {
         return node;
      }
      node = node->next;
   }


   return NULL;
}


meshap_core_net_if_t *meshap_core_create_used_net_if(const char *name, unsigned char phy_type, unsigned char use_type)
{
   meshap_core_net_if_t *net_if;
   unsigned long        flags;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP_CORE : FLOW : Enter: %s : %d\n", __func__,__LINE__);

   net_if = (meshap_core_net_if_t *)kmalloc(sizeof(meshap_core_net_if_t), GFP_ATOMIC);

   memset(net_if, 0, sizeof(meshap_core_net_if_t));

   strcpy(net_if->name, name);

   net_if->dev_type = MESHAP_CORE_DEV_TYPE_PHYSICAL;

   net_if->phy_type = phy_type;
   net_if->use_type = use_type;
   net_if->used     = 1;

   al_spin_lock(core_net_splock, core_net_spvar);

   /**
    * Add it to all net_ifs list
    */

   net_if->next = _ifs_list;
   if (_ifs_list != NULL)
   {
      _ifs_list->prev = net_if;
   }
   _ifs_list = net_if;

   ++_ifs_count;

   al_spin_unlock(core_net_splock, core_net_spvar);

   TORNA_LOG(MESHAP_LOGGER_TYPE_FLOW, "meshap_core: meshap_core_create_used_net_if %s:%d:%d\n", name, phy_type, use_type);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP_CORE : FLOW : Exit: %s : %d\n", __func__,__LINE__); 
   return net_if;
}

/*return 1 if interface is vlan interface*/
int is_vlan_interface(const char *name)
{
	meshap_core_net_if_t  *core_net_if;
	core_net_if = meshap_core_lookup_net_if(name);
	if(core_net_if == NULL)
	{
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESHAP_CORE : ERROR : core_net_if is NULL for name %s -- %s : %d\n", name, __func__,__LINE__);
		return 1;
	}
	if(core_net_if->vlan_if)
		return 1;
	else
		return 0;
}

int meshap_core_create_virtual_net_if(const char *virtual_if_name, const char *phy_if_name)
{
   meshap_core_net_if_t *v_net_if;
   meshap_core_net_if_t *p_net_if;
   unsigned long        flags;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP_CORE : FLOW : Enter: %s : %d\n", __func__,__LINE__);

   p_net_if = meshap_core_lookup_net_if(phy_if_name);

   if (p_net_if == NULL)
   {
      return -1;
   }

   v_net_if = (meshap_core_net_if_t *)kmalloc(sizeof(meshap_core_net_if_t), GFP_ATOMIC);

   memset(v_net_if, 0, sizeof(meshap_core_net_if_t));

   v_net_if->dev      = p_net_if->dev;
   v_net_if->dev_info = p_net_if->dev_info;
   v_net_if->created  = 1;
   v_net_if->dev_type = MESHAP_CORE_DEV_TYPE_VIRTUAL;

   strcpy(v_net_if->name, virtual_if_name);

   al_net_if_initialize_virtual_interface(v_net_if, p_net_if);
   v_net_if->physical_if = p_net_if;

   v_net_if->dev_info->set_device_type(v_net_if->dev, MESHAP_DEV_MODE_MIXED);

   al_spin_lock(core_net_splock, core_net_spvar);

   /**
    * Add it to all net_ifs list
    */

   v_net_if->next = _ifs_list;
   if (_ifs_list != NULL)
   {
      _ifs_list->prev = v_net_if;
   }
   _ifs_list = v_net_if;

   ++_ifs_count;

   /**
    * Add it to created net_ifs list
    */

   v_net_if->next_created = _created_ifs_list;
   if (_created_ifs_list != NULL)
   {
      _created_ifs_list->prev_created = v_net_if;
   }
   _created_ifs_list = v_net_if;
   ++_created_ifs_count;

   al_spin_unlock(core_net_splock, core_net_spvar);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"MESHAP_CORE : INFO : Virtual net interafce created %s %s : %d\n", v_net_if->name, __func__, __LINE__);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP_CORE : FLOW : Exit: %s : %d\n", __func__,__LINE__); 
   return 0;
}


meshap_core_net_if_t *meshap_core_create_net_if_from_dev(struct net_device *dev, meshap_net_dev_t *dev_info)
{
   meshap_core_net_if_t *net_if;
   unsigned long        flags;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP_CORE : FLOW : Enter: %s : %d\n", __func__,__LINE__);

   net_if = (meshap_core_net_if_t *)kmalloc(sizeof(meshap_core_net_if_t), GFP_ATOMIC);

   memset(net_if, 0, sizeof(meshap_core_net_if_t));

   strcpy(net_if->name, dev->name);
   net_if->dev      = dev;
   net_if->dev_info = dev_info;
   net_if->created  = 1;

   net_if->dev_type = MESHAP_CORE_DEV_TYPE_PHYSICAL;

   /**
    * Populate the al_net_if fields
    */

   al_net_if_initialize_interface(net_if);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"MESHAP_CORE : INFO : creating core_net for %s %s : %d\n", dev->name, __func__, __LINE__);

   al_spin_lock(core_net_splock, core_net_spvar);

   /**
    * Add it to all net_ifs list
    */

   net_if->next = _ifs_list;
   if (_ifs_list != NULL)
   {
      _ifs_list->prev = net_if;
   }
   _ifs_list = net_if;

   ++_ifs_count;

   /**
    * Add it to created net_ifs list
    */

   net_if->next_created = _created_ifs_list;
   if (_created_ifs_list != NULL)
   {
      _created_ifs_list->prev_created = net_if;
   }
   _created_ifs_list = net_if;
   ++_created_ifs_count;

   al_spin_unlock(core_net_splock, core_net_spvar);

   TORNA_LOG(MESHAP_LOGGER_TYPE_FLOW, "meshap_core: meshap_core_create_net_if_from_dev %s\n", dev->name);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP_CORE : FLOW : Exit: %s : %d\n", __func__,__LINE__); 

   return net_if;
}


void meshap_core_use_net_if(meshap_core_net_if_t *net_if, unsigned char phy_type, unsigned char use_type)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP_CORE : FLOW : Enter: %s : %d\n", __func__,__LINE__);

   if (net_if->used)
   {
      return;
   }

   net_if->phy_type = phy_type;
   net_if->use_type = use_type;
   net_if->used     = 1;

   net_if->virtual_dev_mode = MESHAP_PHYSICAL_DEV_MODE_NONE;

   if (net_if->dev_type == MESHAP_CORE_DEV_TYPE_VIRTUAL)
   {
      switch (net_if->use_type)
      {
      case AL_CONF_IF_USE_TYPE_DS:
         net_if->virtual_dev_mode = MESHAP_VIRTUAL_DEV_MODE_INFRA;
         break;

      default:
         net_if->virtual_dev_mode = MESHAP_VIRTUAL_DEV_MODE_MASTER;
      }
   }

   TORNA_LOG(MESHAP_LOGGER_TYPE_FLOW, "meshap_core: meshap_core_use_net_if %s:%d:%d\n", net_if->name, phy_type, use_type);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP_CORE : FLOW : Exit: %s : %d\n", __func__,__LINE__); 
}


static inline void _send_auth_response(struct net_device *dev, struct sk_buff *skb_in)
{
   struct sk_buff  *skb_out;
   torna_mac_hdr_t *mac_hdr;
   torna_mac_hdr_t *in_mac_hdr;
   unsigned char   *data;

   static const unsigned char _auth_resp_data[] =
   {
      0x00, 0x00, 0x02, 0x00, 0x00, 0x00
   };
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP_CORE : FLOW : Enter: %s : %d\n", __func__,__LINE__);

   skb_out    = dev_alloc_skb(2400);
   in_mac_hdr = (torna_mac_hdr_t *)skb_in->mac_header;
   mac_hdr    = (torna_mac_hdr_t *)skb_put(skb_out, sizeof(torna_mac_hdr_t));

   memset(mac_hdr, 0, sizeof(torna_mac_hdr_t));

   mac_hdr->signature[0] = TORNA_MAC_HDR_SIGNATURE_1;
   mac_hdr->signature[1] = TORNA_MAC_HDR_SIGNATURE_2;
   mac_hdr->type         = TORNA_MAC_HDR_TYPE_SK_BUFF;

   memcpy(mac_hdr->addresses[0], in_mac_hdr->addresses[1], ETH_ALEN);
   memcpy(mac_hdr->addresses[1], in_mac_hdr->addresses[0], ETH_ALEN);
   memcpy(mac_hdr->addresses[2], in_mac_hdr->addresses[0], ETH_ALEN);

   AL_802_11_FC_SET_TYPE(mac_hdr->frame_control, AL_802_11_FC_TYPE_MGMT);
   AL_802_11_FC_SET_STYPE(mac_hdr->frame_control, AL_802_11_FC_STYPE_AUTH);

   data = (unsigned char *)skb_put(skb_out, sizeof(_auth_resp_data));
   memcpy(data, _auth_resp_data, sizeof(_auth_resp_data));

   skb_out->mac_header = (u8 *)mac_hdr;
   skb_out->dev        = dev;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP_CORE : FLOW : Exit: %s : %d\n", __func__,__LINE__); 
}

static int _get_next_ie(_ie_iterator_t *ie)
{
   if (ie->ie_source_length <= 2)
   {
      return 1;
   }

   ie->ie_id = *ie->ie_source;
   ie->ie_source++;

   ie->ie_datalength = *ie->ie_source;
   ie->ie_source++;

   ie->ie_data           = ie->ie_source;
   ie->ie_source        += ie->ie_datalength;
   ie->ie_source_length -= (2 + ie->ie_datalength);

   return 0;
}


static int _meshap_dot1x_timer(void *data)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"DOT1X_TIMER : FLOW : Enter: %s : %d\n", __func__,__LINE__);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"DOT1X_TIMER : INFO : Starting state = %d %s : %d\n",  meshap_core_globals.state, __func__, __LINE__);

   /* daemonize ("dot1x_timer") */
   while (1)
   {
	  thread_stat.dot1x_timer_thread_count++;
      if (meshap_core_globals.state == MESHAP_CORE_GLOBALS_STATE_STOPPED)
      {
         break;
      }
	  if(reboot_in_progress) {
        al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"DOT1X_TIMER : INFO : -----Terminating 'dot1x_timer' thread----- %s : %d\n", __func__, __LINE__);
		  threads_status &= ~THREAD_STATUS_BIT11_MASK;
		  break;
	  }

      dot11i_tick(AL_CONTEXT_SINGLE);
      set_current_state(TASK_INTERRUPTIBLE);
      schedule_timeout((500 * HZ) / 1000);
      set_current_state(TASK_RUNNING);
   }

   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"DOT1X_TIMER : INFO : Stopping %s : %d\n", __func__, __LINE__);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"DOT1X_TIMER : FLOW : Exit: %s : %d\n", __func__,__LINE__); 

   return 0;
}


void meshap_core_process_mgmt_frame(struct net_device *dev, void *dev_token, struct sk_buff *skb)
{
   meshap_core_net_if_t *core_net_if;
   meshap_core_packet_t *packet;
   int             stype;
   torna_mac_hdr_t *mac_hdr;
   al_net_addr_t   sta_addr;
   int             ret;
   unsigned short  capability;
   unsigned char   *p;
   int             ie_count;
   _ie_iterator_t  ie_iterator;
   al_802_11_information_element_t *ie;
   int i;
   al_802_11_information_element_t *ie_out;
   unsigned char              ie_out_valid;
   unsigned long              flags;
   meshap_core_net_if_t       *net_if = NULL;
   al_802_11_operations_t     *operations;
   access_point_sta_info_t    sta;
   meshap_mac80211_sta_info_t sta_info;
   mesh_conf_if_info_t *netif_conf;

   if (meshap_core_globals.state != MESHAP_CORE_GLOBALS_STATE_STARTED)
   {
		al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"MESHAP_CORE : INFO : meshap_core_process_mgmt_frame dropping management frame in stopped state\n");
      (tx_rx_pkt_stats.packet_drop[68])++;
	  dev_kfree_skb(skb);
      return;
   }
   if (dev_token == NULL)
   {
      core_net_if = meshap_core_lookup_net_if(dev->name);
   }
   else
   {
      core_net_if = (meshap_core_net_if_t *)dev_token;
   }
   if (core_net_if == NULL)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"MESHAP_CORE : INFO : icore_net_if was NULL for %s %s : %d\n", dev->name, __func__, __LINE__);
      (tx_rx_pkt_stats.packet_drop[69])++;
      dev_kfree_skb(skb);
      return;
   }
   if (core_net_if->process_management_frame == NULL)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"MESHAP_CORE : INFO : process_management_frame is NULL for %s %s : %d\n", dev->name, __func__, __LINE__);
      (tx_rx_pkt_stats.packet_drop[70])++;
      dev_kfree_skb(skb);
      return;
   }
   net_if = core_net_if;
      if (NULL == net_if)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESHAP_CORE : ERROR : net_if is NULL for dev name %s %s : %d\n", skb->dev->name, __func__,__LINE__);
      (tx_rx_pkt_stats.packet_drop[71])++;
      dev_kfree_skb(skb);
      return;
   }
   /* Get the TORNA MAC Hdr */
   mac_hdr = net_if->dev_info->meshap_get_torna_hdr_cb(skb);
   stype   = AL_802_11_FC_GET_STYPE(mac_hdr->frame_control);
   memcpy(sta_addr.bytes, mac_hdr->addresses[1], ETH_ALEN);
   sta_addr.length = ETH_ALEN;
   switch (stype)
   {
   case AL_802_11_FC_STYPE_AUTH:

      for (i = 0; i < mesh_config->if_count; i++) {
        netif_conf = &mesh_config->if_info[i];
        if (!strcmp(net_if->name, netif_conf->name) && (netif_conf->use_type == AL_CONF_IF_USE_TYPE_AP)
             && (netif_conf->service == AL_CONF_IF_SERVICE_BACKHAUL_ONLY)) {
             dev_kfree_skb(skb);
             return;
        }
      }
		al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,
							"MESHAP_CORE : INFO : case AL_802_11_FC_STYPE_AUTH, dest-addr : ["MACSTR"] and src-addr : ["MACSTR"], dev name = %s : %s %d\n",
												MAC2STR(mac_hdr->addresses[0]), MAC2STR(mac_hdr->addresses[1]), skb->dev->name, __func__, __LINE__);

      if (core_net_if->process_auth != NULL)
      {
         ret = core_net_if->process_auth(&core_net_if->al_net_if, &sta_addr);
         if (!ret)
         {
            if (core_net_if->use_type == AL_CONF_IF_USE_TYPE_WM)
            {
               operations = (al_802_11_operations_t *)core_net_if->al_net_if.get_extended_operations(&core_net_if->al_net_if);
               if (operations == NULL)
               {
				  (tx_rx_pkt_stats.packet_drop[72])++;
                  dev_kfree_skb(skb);
                  return;
               }
					al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"MESHAP_CORE : INFO : Sending auth response for dev name %s,  %s : %d\n",
										skb->dev->name,  __func__, __LINE__); 
               operations->send_auth_response(&core_net_if->al_net_if, mac_hdr->addresses[0], mac_hdr->addresses[1]);
            }
         }
         else
         {
            al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"MESHAP_CORE : INFO : AP process_auth returned %d %s : %d\n", ret, __func__, __LINE__);
         }
      }
      else
      {
         al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"MESHAP_CORE : INFO : AP process_auth was NULL %s : %d\n", __func__, __LINE__);
      }
		tx_rx_pkt_stats.mgmt_pkt_process_auth++;
      dev_kfree_skb(skb);
      break;

   case AL_802_11_FC_STYPE_ASSOC_REQ:
   case AL_802_11_FC_STYPE_REASSOC_REQ:

		al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,
							"MESHAP_CORE : INFO : case AL_802_11_FC_STYPE_ASSOC/REASSOC_REQ, dest-addr : ["MACSTR"] and src-addr : ["MACSTR"], dev name = %s : %s %d\n",
												MAC2STR(mac_hdr->addresses[0]), MAC2STR(mac_hdr->addresses[1]), skb->dev->name,  __func__, __LINE__);
      if (core_net_if->process_assoc != NULL)
      {
         /**
          * Extract STA capability
          */
         p = skb->data;
         memcpy(&capability, &p[AL_802_11_ASSOCIATION_REQUEST_CAPABILITY_POSITION], sizeof(unsigned short));
         capability = le16_to_cpu(capability);

         if (stype == AL_802_11_FC_STYPE_ASSOC_REQ)
         {
            ie_iterator.ie_source        = p + AL_802_11_ASSOCIATION_REQUEST_SSID_POSITION;
            ie_iterator.ie_source_length = skb->len - AL_802_11_ASSOCIATION_REQUEST_SSID_POSITION;
         }
         else
         {
            ie_iterator.ie_source        = p + AL_802_11_REASSOCIATION_REQUEST_SSID_POSITION;
            ie_iterator.ie_source_length = skb->len - AL_802_11_REASSOCIATION_REQUEST_SSID_POSITION;
         }

         ie_count = 0;

         while (!_get_next_ie(&ie_iterator))
         {
            ++ie_count;
         }
         ie = (al_802_11_information_element_t *)al_heap_alloc(AL_CONTEXT sizeof(al_802_11_information_element_t) * ie_count AL_HEAP_DEBUG_PARAM);
         memset(ie, 0, sizeof(al_802_11_information_element_t) * ie_count);
         if (stype == AL_802_11_FC_STYPE_ASSOC_REQ)
         {
            ie_iterator.ie_source        = p + AL_802_11_ASSOCIATION_REQUEST_SSID_POSITION;
            ie_iterator.ie_source_length = skb->len - AL_802_11_ASSOCIATION_REQUEST_SSID_POSITION;
         }
         else
         {
            ie_iterator.ie_source        = p + AL_802_11_REASSOCIATION_REQUEST_SSID_POSITION;
            ie_iterator.ie_source_length = skb->len - AL_802_11_REASSOCIATION_REQUEST_SSID_POSITION;
         }

         i = 0;

         while (!_get_next_ie(&ie_iterator))
         {
            ie[i].element_id = ie_iterator.ie_id;
            ie[i].length     = ie_iterator.ie_datalength;
            memcpy(ie[i].data, ie_iterator.ie_data, ie[i].length);
            i++;
         }
         ie_out_valid = 0;
         ie_out       = (al_802_11_information_element_t *)al_heap_alloc(AL_CONTEXT sizeof(al_802_11_information_element_t)AL_HEAP_DEBUG_PARAM);
         memset(ie_out, 0, sizeof(al_802_11_information_element_t));

         ret = core_net_if->process_assoc(&core_net_if->al_net_if,
                                          &sta_addr,
                                          capability,
                                          ie_count,
                                          ie,
                                          &ie_out_valid,
                                          ie_out);

         al_heap_free(AL_CONTEXT ie);
         if (ret > 0)
         {
            if (core_net_if->use_type == AL_CONF_IF_USE_TYPE_WM)
            {
               access_point_get_sta_info_in_intr(&sta_addr, &sta);

               memcpy(sta_info.supported_rates, sta.supported_rates, AL_802_11_SUPPORTED_RATES_LENGTH);
               sta_info.listen_interval     = sta.listen_interval;
               sta_info.aid                 = sta.aid;
               sta_info.capability          = sta.capability;
               sta_info.supported_rates_len = AL_802_11_SUPPORTED_RATES_LENGTH;
               memcpy(sta_info.sta_addr, sta.address.bytes, ETH_ALEN);
               memset(&sta_info.ht_capab, 0 , sizeof(al_802_11_ht_capabilities_t));
               memcpy(&sta_info.ht_capab, &sta.ht_capab, sizeof(al_802_11_ht_capabilities_t));

               memset(&sta_info.vht_capab, 0 , sizeof(al_802_11_vht_capabilities_t));
               memcpy(&sta_info.vht_capab, &sta.vht_capab, sizeof(al_802_11_vht_capabilities_t));

               memset(&sta_info.wmm_element, 0 , sizeof(al_802_11_wmm_information_element_t));
               memcpy(&sta_info.wmm_element, &sta.wmm_element, sizeof(al_802_11_wmm_information_element_t));

               sta_info.wmm_flag_set = sta.wmm_flag_set;
               sta_info.ht_flag_set = sta.ht_flag_set;
               sta_info.vht_flag_set = sta.vht_flag_set;

               operations = (al_802_11_operations_t *)core_net_if->al_net_if.get_extended_operations(&core_net_if->al_net_if);
               if (operations == NULL)
               {
				  (tx_rx_pkt_stats.packet_drop[73])++;
                  dev_kfree_skb(skb);
                  return;
               }
               al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"MESHAP_CORE : INFO : ifname %s -- %s : %d\n",  core_net_if->al_net_if.name, __func__, __LINE__);
               operations->send_assoc_response(&core_net_if->al_net_if,
                                               stype == AL_802_11_FC_STYPE_ASSOC_REQ ? AL_802_11_FC_STYPE_ASSOC_RESP : AL_802_11_FC_STYPE_REASSOC_RESP,
                                               mac_hdr->addresses[0],
                                               mac_hdr->addresses[1],
                                               ret,
                                               ie_out_valid,
                                               ie_out,
                                               sta_info);
	       if (sta.is_imcp)
	       {
             al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"MESHAP_CORE : INFO : setting security key in case of RELAY NODE %s : %d\n", __func__, __LINE__);
		       operations->set_security_key(&core_net_if->al_net_if, &sta.sec_key, 0);
	       }
	       else {
		       /*Adding station to dot11i Queue to initiate WEP/WPA/WPA2 security mechanisms*/
		       //access_point_dot11i_add_sta(AL_CONTEXT &sta_addr);
	       }
            }
         }
         else if (stype == AL_802_11_FC_STYPE_REASSOC_REQ)
         {
            if (core_net_if->use_type == AL_CONF_IF_USE_TYPE_WM)
            {
               operations = (al_802_11_operations_t *)core_net_if->al_net_if.get_extended_operations(&core_net_if->al_net_if);
               if (operations == NULL)
               {
				      (tx_rx_pkt_stats.packet_drop[74])++;
                  dev_kfree_skb(skb);
                  return;
               }
					al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"MESHAP_CORE : INFO : Sending deauth for dev name %s, %s : %d\n",
										skb->dev->name, __func__, __LINE__);
               operations->send_deauth(&core_net_if->al_net_if, mac_hdr->addresses[0], mac_hdr->addresses[1]);
            }
         }
         else
         {
            al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"MESHAP_CORE : INFO : AP process_assoc returned %d -- %s : %d\n", ret, __func__, __LINE__);
         }

         al_heap_free(AL_CONTEXT ie_out);
      }
      else
      {
         al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"MESHAP_CORE : INFO : AP process_assoc was NULL %s : %d\n", __func__, __LINE__);
      }
      dev_kfree_skb(skb);
      break;

   default:
      packet = al_packet_impl_create_from_skb(core_net_if, skb, MAIN_POOL); // Rew: give highest priority

      if (packet != NULL)
      {
			al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,
							"MESHAP_CORE : INFO : case default, dest-addr : ["MACSTR"] and src-addr : ["MACSTR"], dev name = %s  : %s %d\n",
												MAC2STR(mac_hdr->addresses[0]), MAC2STR(mac_hdr->addresses[1]), skb->dev->name,__func__, __LINE__);

         stype = AL_802_11_FC_GET_STYPE(packet->al_packet.frame_control);
         al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"MESHAP_CORE : INFO : default  stype = %d -- %s : %d\n", stype, __func__, __LINE__);
         i = core_net_if->process_management_frame(&core_net_if->al_net_if, stype, &packet->al_packet);
		 if(i == -1) {
			 al_release_packet(AL_CONTEXT &packet->al_packet);
		 }
      }
      else
      {
		 (tx_rx_pkt_stats.packet_drop[75])++;
         dev_kfree_skb(skb);
      }
   }
}

void process_mip_skb(struct sk_buff *skb)
{
   meshap_core_packet_t *packet;
   MESH_USAGE_PROFILING_INIT

   skb_pull(skb, ETH_HLEN);

   packet = al_packet_impl_create_outgoing_packet_from_skb(skb);

   if (packet != NULL)
   {
       MESH_USAGE_PROFILE_START(start)
	   meshap_core_globals.on_before_transmit(NULL, &packet->al_packet);
	   MESH_USAGE_PROFILE_END(profiling_info.process_mip_skb_func_tprof,index,start,end)
	   tx_rx_pkt_stats.mip_xmit_tx++;
   }
   else
   {
	   dev_kfree_skb_any(skb);
	   (tx_rx_pkt_stats.packet_drop[58])++;
   }
}

void meshap_skb_enqueue(struct sk_buff_head *list, struct sk_buff *newskb, al_spinlock_t *lock)
{
	MESH_USAGE_PROFILING_INIT
	MESH_USAGE_PROFILE_START(start)
	if(lock)
	al_spin_lock_bh_t(lock);
	MESH_USAGE_PROFILE_END(profiling_info.meshap_skb_enqueue_splock,index,start,end)
	__skb_queue_tail(list, newskb);
	MESH_USAGE_PROFILE_START(start)
	if(lock)
	al_spin_unlock_bh_t(lock);
	MESH_USAGE_PROFILE_END(profiling_info.meshap_skb_enqueue_spunlock,index,start,end)
	return;
}
EXPORT_SYMBOL(meshap_skb_enqueue);

struct sk_buff *meshap_skb_dequeue(struct sk_buff_head *list, al_spinlock_t *lock)
{
	unsigned long flags;
	struct sk_buff *result;

	MESH_USAGE_PROFILING_INIT
    MESH_USAGE_PROFILE_START(start)
    if(lock)
	al_spin_lock_bh_t(lock);
	MESH_USAGE_PROFILE_END(profiling_info.meshap_skb_deq_splock,index,start,end)
	result = __skb_dequeue(list);
    MESH_USAGE_PROFILE_START(start)
	if(lock)
	al_spin_unlock_bh_t(lock);
	MESH_USAGE_PROFILE_END(profiling_info.meshap_skb_deq_spunlock,index,start,end)
	return result;
}
EXPORT_SYMBOL(meshap_skb_dequeue);

void meshap_splice_skb_queue(struct sk_buff_head *input_queue, struct sk_buff_head *output_queue)
{
	skb_queue_splice_tail_init(input_queue, output_queue);
}

unsigned char get_pkt_priority(struct sk_buff *skb)
{
	unsigned char priority = 0;
	meshap_core_net_if_t *core_net_if = NULL;
	struct iphdr *ip_header = NULL;
	torna_mac_hdr_t *mac_hdr = NULL;
	int type = 0;


	core_net_if = meshap_core_lookup_net_if(skb->dev->name);
	if(!core_net_if) { /*Packet came from MIP interface*/
		ip_header = (struct iphdr *)skb_network_header(skb);
        type = SKB_ETHERNET(skb)->h_proto;
		if(type == 0x0800 && ip_header) {
			/*MSB 3 bits of tos represent priority*/
			priority  = ip_header->tos >> 5;
		}else{
			return 0;
		}
	}else if (core_net_if->dev_info->encapsulation == MESHAP_NET_DEV_ENCAPSULATION_802_11){ /*packet came from Wifi Interface*/
		mac_hdr = core_net_if->dev_info->meshap_get_torna_hdr_cb(skb);
		if(mac_hdr) {
			/*LSB 3 bits of QOS control represent priority*/
			priority = mac_hdr->qos_control & 0x0007;
		}else {
			return 0;
		}
	}else{ /*packet came from ETH interface*/
		ip_header = (struct iphdr *)skb_network_header(skb);
        type = SKB_ETHERNET(skb)->h_proto;
		if(type == 0x0800 && ip_header) {
			/*MSB 3 bits of tos represent priority*/
			priority  = ip_header->tos >> 5;
		}else {
			return 0;
		}
	}

	switch (priority) {
		case 0:
		case 1:
			return 0;
		case 2:
		case 3:
			return 1;
		case 4:
		case 5:
			return 2;
		case 6:
		case 7:
			return 3;
		default:
			return 0;
	}

}

/*Using TAVA/VAT method to prepare process_skb_queue
 *In TAVA/VAT method each Q assigned with static weightage
 *for ex:Q[3-0] having 4-1 weightage,and here weightage quantam
 *left for a queue is adding to next queue's static weightage
 */
int get_process_skb_queue (void)
{
	struct sk_buff *skb;
	int i, weightage, j = 0, k = 0, pkt_added = 0, carry_weightage = 0;

	for (i = NUM_SKB_QUE - 1; i >= 0; i--) {

		weightage = Q_weightage[i] + carry_weightage;
		j = weightage;

		if(!skb_queue_empty(&data_pkt_queue[i])) {
			al_spin_lock_bh_t(&skb_data_que_splock[i]);
			while (weightage-- && !skb_queue_empty(&data_pkt_queue[i])) {
				skb = meshap_skb_dequeue(&data_pkt_queue[i], NULL);
				meshap_skb_enqueue(&process_skb_queue, skb, NULL);
				q_packet_stats.process_skbQ_total_packet_added_count++;
				q_packet_stats.process_skbQ_current_packet_count++;
				pkt_added++;
			}
			al_spin_unlock_bh_t(&skb_data_que_splock[i]);

			if(weightage == -1) {
				AL_IMPL_ATOMIC_ADD(j, q_packet_stats.data_pktQ_total_packet_removed_count[i]);
				AL_IMPL_ATOMIC_SUB(j, q_packet_stats.data_pktQ_current_packet_count[i]);
				carry_weightage = 0;
			}else {
				k = j-weightage-1;
				AL_IMPL_ATOMIC_ADD(k, q_packet_stats.data_pktQ_total_packet_removed_count[i]);
				AL_IMPL_ATOMIC_SUB(k, q_packet_stats.data_pktQ_current_packet_count[i]);
				carry_weightage = j - k;
			}
		}else {
			carry_weightage = j;
		}
	}

	if(q_packet_stats.process_skbQ_current_packet_count > q_packet_stats.process_skbQ_current_packet_max_count) {
		q_packet_stats.process_skbQ_current_packet_max_count = q_packet_stats.process_skbQ_current_packet_count;
	}

	return pkt_added;
}

void meshap_core_process_data_frame(struct net_device *dev, void *dev_token, struct sk_buff *skb)
{
	unsigned long        flags;
    meshap_core_net_if_t *core_net_if;
	void *ptr;
	unsigned char i = 0;


	if ((NULL == skb) || reboot_in_progress ||(mesh_state != _MESH_STATE_RUNNING) || (AL_ATOMIC_GET(globals.ap_thread_disabled) == 1))
	{
		core_net_if = meshap_core_lookup_net_if(dev->name);
		if(skb) {
			if(core_net_if && core_net_if->dev_info->encapsulation == MESHAP_NET_DEV_ENCAPSULATION_802_11) {
				ptr = (void *)core_net_if->dev_info->meshap_get_drv_data(skb);
				kfree(ptr);
			}
			dev_kfree_skb(skb);
		}
	    (tx_rx_pkt_stats.packet_drop[44])++;
		return;
	}

	i = get_pkt_priority(skb);

	AL_IMPL_ATOMIC_INC(q_packet_stats.data_pktQ_total_packet_added_count[i]);
	AL_IMPL_ATOMIC_INC(q_packet_stats.data_pktQ_current_packet_count[i]);

	if(q_packet_stats.data_pktQ_current_packet_count[i].counter > q_packet_stats.data_pktQ_current_packet_max_count[i].counter){
		q_packet_stats.data_pktQ_current_packet_max_count[i].counter = q_packet_stats.data_pktQ_current_packet_count[i].counter;
	}

	if(skb_queue_empty(&data_pkt_queue[i]) || (AL_ATOMIC_GET(process_skb_schd_out) == 1)) {
		AL_ATOMIC_SET(process_skb_schd_out, 0);
		wake_up_process(meshap_data_pkt_process_task);
	}

	meshap_skb_enqueue(&data_pkt_queue[i], skb, &skb_data_que_splock[i]);
}

void meshap_core_process_data_frame_t(void *data)
{
   meshap_core_net_if_t *core_net_if;
   meshap_core_packet_t *packet;
   unsigned long        flags;
   int ret,pkt_poll = 0, quelen = 0;
   struct net_device *dev;
   struct sk_buff *skb = NULL;
   void *ptr = NULL;
   MESH_USAGE_PROFILING_INIT

   while (1) {
	   skb = meshap_skb_dequeue(&process_skb_queue, NULL);
	   thread_stat.process_skb_thread_count++;
	   if(skb) {
		   pkt_poll = 0;
		   MESH_USAGE_PROFILE_START(start)
		   q_packet_stats.process_skbQ_total_packet_removed_count++;
		   q_packet_stats.process_skbQ_current_packet_count--;
		   thread_stat.process_skb_thread_count++;
		   dev = skb->dev;

		   if(!strcmp(dev->name, "mip0")) {
			   process_mip_skb(skb);
			   MESH_USAGE_PROFILE_END(profiling_info.process_skb_func_tprof,index,start,end)
			   continue;
		   }

		   core_net_if = meshap_core_lookup_net_if(dev->name);

		   if (core_net_if == NULL)
		   {
	           AL_IMPL_ATOMIC_INC(q_packet_stats.upstack_pktQ_total_packet_added_count);
			   AL_IMPL_ATOMIC_INC(q_packet_stats.upstack_pktQ_current_packet_count);
			   if(q_packet_stats.upstack_pktQ_current_packet_count.counter > q_packet_stats.upstack_pktQ_current_packet_max_count.counter) {
				   q_packet_stats.upstack_pktQ_current_packet_max_count.counter = q_packet_stats.upstack_pktQ_current_packet_count.counter;
			   }
			   meshap_skb_enqueue(&upstack_pkt_queue, skb, &skb_upstack_que_splock);
			   wake_up_process(meshap_upstack_pkt_process_task);
	           (tx_rx_pkt_stats.packet_drop[46])++;
			   MESH_USAGE_PROFILE_END(profiling_info.process_skb_func_tprof,index,start,end)
			   continue;
		   }

		   if (meshap_core_globals.on_receive == NULL)
		   {
			   if(core_net_if->dev_info->encapsulation == MESHAP_NET_DEV_ENCAPSULATION_802_11) {
				   ptr = (void *)core_net_if->dev_info->meshap_get_drv_data(skb);
				   kfree(ptr);
			   }
			   AL_IMPL_ATOMIC_INC(q_packet_stats.upstack_pktQ_total_packet_added_count);
			   AL_IMPL_ATOMIC_INC(q_packet_stats.upstack_pktQ_current_packet_count);
			   if(q_packet_stats.upstack_pktQ_current_packet_count.counter > q_packet_stats.upstack_pktQ_current_packet_max_count.counter) {
				   q_packet_stats.upstack_pktQ_current_packet_max_count.counter = q_packet_stats.upstack_pktQ_current_packet_count.counter;
			   }
			   meshap_skb_enqueue(&upstack_pkt_queue, skb, &skb_upstack_que_splock);
			   wake_up_process(meshap_upstack_pkt_process_task);
	           (tx_rx_pkt_stats.packet_drop[45])++;
			   MESH_USAGE_PROFILE_END(profiling_info.process_skb_func_tprof,index,start,end)
			   continue;
		   }

		   if (core_net_if->dev != dev)
		   {
			   if(core_net_if->dev_info->encapsulation == MESHAP_NET_DEV_ENCAPSULATION_802_11) {
				   ptr = (void *)core_net_if->dev_info->meshap_get_drv_data(skb);
				   kfree(ptr);
			   }
	           AL_IMPL_ATOMIC_INC(q_packet_stats.upstack_pktQ_total_packet_added_count);
			   AL_IMPL_ATOMIC_INC(q_packet_stats.upstack_pktQ_current_packet_count);
			   meshap_skb_enqueue(&upstack_pkt_queue, skb, &skb_upstack_que_splock);
			   wake_up_process(meshap_upstack_pkt_process_task);
	           (tx_rx_pkt_stats.packet_drop[47])++;
			   MESH_USAGE_PROFILE_END(profiling_info.process_skb_func_tprof,index,start,end)
			   continue;
		   }
		   if (!core_net_if->used)
		   {
			   if(core_net_if->dev_info->encapsulation == MESHAP_NET_DEV_ENCAPSULATION_802_11) {
				   ptr = (void *)core_net_if->dev_info->meshap_get_drv_data(skb);
				   kfree(ptr);
			   }
	           AL_IMPL_ATOMIC_INC(q_packet_stats.upstack_pktQ_total_packet_added_count);
			   AL_IMPL_ATOMIC_INC(q_packet_stats.upstack_pktQ_current_packet_count);
			   if(q_packet_stats.upstack_pktQ_current_packet_count.counter > q_packet_stats.upstack_pktQ_current_packet_max_count.counter) {
				   q_packet_stats.upstack_pktQ_current_packet_max_count.counter = q_packet_stats.upstack_pktQ_current_packet_count.counter;
			   }
			   meshap_skb_enqueue(&upstack_pkt_queue, skb, &skb_upstack_que_splock);
			   wake_up_process(meshap_upstack_pkt_process_task);
	           (tx_rx_pkt_stats.packet_drop[49])++;
			   MESH_USAGE_PROFILE_END(profiling_info.process_skb_func_tprof,index,start,end)
			   continue;
		   }
		   if (meshap_core_globals.state != MESHAP_CORE_GLOBALS_STATE_STARTED)
		   {
			   if(core_net_if->dev_info->encapsulation == MESHAP_NET_DEV_ENCAPSULATION_802_11) {
				   ptr = (void *)core_net_if->dev_info->meshap_get_drv_data(skb);
				   kfree(ptr);
			   }
			   AL_IMPL_ATOMIC_INC(q_packet_stats.upstack_pktQ_total_packet_added_count);
			   AL_IMPL_ATOMIC_INC(q_packet_stats.upstack_pktQ_current_packet_count);
			   if(q_packet_stats.upstack_pktQ_current_packet_count.counter > q_packet_stats.upstack_pktQ_current_packet_max_count.counter) {
				   q_packet_stats.upstack_pktQ_current_packet_max_count.counter = q_packet_stats.upstack_pktQ_current_packet_count.counter;
			   }
			   meshap_skb_enqueue(&upstack_pkt_queue, skb, &skb_upstack_que_splock);
			   wake_up_process(meshap_upstack_pkt_process_task);
	           (tx_rx_pkt_stats.packet_drop[50])++;
			   MESH_USAGE_PROFILE_END(profiling_info.process_skb_func_tprof,index,start,end)
			   continue;
		   }

		   packet = al_packet_impl_create_from_skb(core_net_if, skb, SKB_POOL);

		   if ((packet != NULL) && (packet->al_packet.type == 0xDEDE))
		   {
			   ret = meshap_power_test_process((meshap_power_test_device_t *)core_net_if->test_device_ptr,
					   packet->al_packet.addr_1.bytes,
					   packet->al_packet.addr_2.bytes,
					   packet->al_packet.rssi,
					   packet->al_packet.bit_rate,
					   packet->al_packet.buffer + packet->al_packet.position,
					   packet->al_packet.data_length);
			   if (ret == MESHAP_POWER_TEST_PROCESS_DROP)
			   {
	               (tx_rx_pkt_stats.packet_drop[51])++;
				   al_release_packet(AL_CONTEXT & packet->al_packet);
				   MESH_USAGE_PROFILE_END(profiling_info.process_skb_func_tprof,index,start,end)
				   continue;
			   }
		   }

		   if (packet != NULL)
		   {
			   if (packet->al_packet.flags & AL_PACKET_FLAGS_VIRTUAL)
			   {
	               (tx_rx_pkt_stats.packet_drop[52])++;
				   al_release_packet(AL_CONTEXT & packet->al_packet);
			   }
			   else
			   {
				   core_net_if->al_net_if.last_rx_time = al_get_tick_count();
				   meshap_core_globals.on_receive(&core_net_if->al_net_if, &packet->al_packet);
			   }
		   }
		   else
		   {
	           (tx_rx_pkt_stats.packet_drop[53])++;
			   if(core_net_if->dev_info->encapsulation == MESHAP_NET_DEV_ENCAPSULATION_802_11) {
				   ptr = (void *)core_net_if->dev_info->meshap_get_drv_data(skb);
				   kfree(ptr);
			   }
			   dev_kfree_skb(skb);
		   }
		   MESH_USAGE_PROFILE_END(profiling_info.process_skb_func_tprof,index,start,end)
		   continue;
	   }else {
tryagain:
		   quelen = get_process_skb_queue();

		   if(!quelen) {
			   if(pkt_poll < PKT_POLL_RETRY_COUNT) {
				   pkt_poll++;
				   goto tryagain;
			   }else{
				   pkt_poll = 0;
				   set_current_state(TASK_INTERRUPTIBLE);
				   AL_ATOMIC_SET(process_skb_schd_out, 1);
				   schedule();
			   }
		   }
	   }
   }
}
EXPORT_SYMBOL(meshap_core_process_data_frame);

void meshap_core_on_link_notify(struct net_device *dev, void *dev_token, u32 state_flags)
{
   meshap_core_net_if_t *core_net_if;
   u32           old_flags;
   unsigned char mesh_mode;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP_CORE : FLOW : Enter: %s : %d\n", __func__,__LINE__);

   mesh_mode = 0;

   if (dev_token == NULL)
   {
      core_net_if = meshap_core_lookup_net_if(dev->name);
   }
   else
   {
      core_net_if = (meshap_core_net_if_t *)dev_token;
   }

   if (core_net_if == NULL)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"MESHAP_CORE : INFO : ignoring link notification for %s (0) %s : %d\n", dev->name, __func__,__LINE__);
      return;
   }

   if (core_net_if->dev != dev)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"MESHAP_CORE : INFO : ignoring link notification for %s (1) %s : %d\n", dev->name, __func__,__LINE__);
      return;
   }

   if (core_net_if->dev_info == NULL)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"MESHAP_CORE : INFO : ignoring link notification for %s (1.2) %s : %d\n", dev->name, __func__,__LINE__);
      return;
   }

   if (core_net_if->dev_info->encapsulation == MESHAP_NET_DEV_ENCAPSULATION_802_11)
   {
      mesh_mode = core_net_if->dev_info->get_device_type(dev);

      if (mesh_mode == MESHAP_DEV_MODE_MIXED)
      {
         core_net_if = core_net_if->virtual_ds_if;

         if (core_net_if == NULL)
         {
            al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"MESHAP_CORE : INFO : ignoring link notification for VIRTUAL %s (1) %s : %d\n",
                                               dev->name, __func__,__LINE__);
            return;
         }
      }
   }

   old_flags = core_net_if->link_flags;
   core_net_if->link_flags = state_flags;

   if (!core_net_if->used)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"MESHAP_CORE : INFO : ignoring link notification for %s (2) %s : %d\n", dev->name, __func__,__LINE__);
      return;
   }

   if (meshap_core_globals.on_phy_link_notify == NULL)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"MESHAP_CORE : INFO : ignoring link notification for %s (3) %s : %d\n", dev->name, __func__,__LINE__);
      return;
   }

   if (core_net_if->link_flags & MESHAP_ON_LINK_NOTIFY_STATE_ACCESS_POINT)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"MESHAP_CORE : INFO : ignoring link notification for %s in master mode"
         " (4) %s : %d\n", dev->name, __func__,__LINE__);
      core_net_if->link_flags = old_flags;
      return;
   }

   if (state_flags & MESHAP_ON_LINK_NOTIFY_STATE_LINK_OFF)
   {
      meshap_core_globals.on_phy_link_notify(&core_net_if->al_net_if, AL_NET_IF_LINK_STATE_NOT_LINKED);
   }
   else if (state_flags & MESHAP_ON_LINK_NOTIFY_STATE_LINK_ON)
   {
      meshap_core_globals.on_phy_link_notify(&core_net_if->al_net_if, AL_NET_IF_LINK_STATE_LINKED);
   }
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP_CORE : FLOW : Exit: %s : %d\n", __func__,__LINE__);
}


int rate_init()
{
   meshap_core_net_if_t *node;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP_CORE : FLOW : Enter: %s : %d\n", __func__,__LINE__);

   node = _ifs_list;

   while (node != NULL)
   {
      if (!node->dev_info->meshap_rate_init)
      {
         al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESHAP_CORE : ERROR : callback is NULL for inteface  <%s> %s : %d\n", node->dev->name, __func__,__LINE__);
         node = node->next;
		 continue;
      }
      node->dev_info->meshap_rate_init(node->dev);
      node = node->next;
   }

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP_CORE : FLOW : Exit: %s : %d\n", __func__,__LINE__);
   return 0;
}


void meshap_core_on_net_device_create(struct net_device *dev, meshap_net_dev_t *dev_info)
{
   meshap_core_net_if_t *node;
   unsigned long        flags;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP_CORE : FLOW : Enter: %s : %d\n", __func__,__LINE__);

   TORNA_LOG(MESHAP_LOGGER_TYPE_FLOW, "meshap_core: meshap_core_on_net_device_create dev=%s\n", dev->name);

   node = meshap_core_lookup_net_if(dev->name);

   if (node != NULL)
   {
      node->dev        = dev;
      node->dev_info   = dev_info;
      node->created    = 1;
      node->link_flags = MESHAP_ON_LINK_NOTIFY_STATE_LINK_OFF | MESHAP_ON_LINK_NOTIFY_STATE_DISASSOCIATED;
   }
   else
   {
      node = meshap_core_create_net_if_from_dev(dev, dev_info);
   }

   if (dev_info->set_dev_token != NULL)
   {
      dev_info->set_dev_token(dev, node);
   }

   if (dev_info->encapsulation == MESHAP_NET_DEV_ENCAPSULATION_802_11)
   {
      node->test_device_ptr = meshap_power_test_create_device(dev->name,
                                                              MESHAP_POWER_TEST_DEVICE_TYPE_802_11,
                                                              dev->dev_addr,
                                                              (void *)node);
   }
   else
   {
      node->test_device_ptr = meshap_power_test_create_device(dev->name,
                                                              MESHAP_POWER_TEST_DEVICE_TYPE_ETHERNET,
                                                              dev->dev_addr,
                                                              (void *)node);
   }

   /* Now configd deamon will start the Meshap*/
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP_CORE : FLOW : Exit: %s : %d\n", __func__,__LINE__);
}


void meshap_core_on_net_device_destroy(struct net_device *dev, void *dev_token)
{
   meshap_core_net_if_t *node;
   unsigned long        flags;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP_CORE : FLOW : Enter: %s : %d\n", __func__,__LINE__);

   TORNA_LOG(MESHAP_LOGGER_TYPE_FLOW, "meshap_core: meshap_core_on_net_device_destroy dev=%s\n", dev->name);


   if (dev_token == NULL)
   {
      node = meshap_core_lookup_net_if(dev->name);
   }
   else
   {
      node = (meshap_core_net_if_t *)dev_token;
   }

   if (node != NULL)
   {
	  al_spin_lock(core_net_splock, core_net_spvar);

      /**
       * Remove from created net_ifs list
       */

      if (node->prev_created != NULL)
      {
         node->prev_created->next_created = node->next_created;
      }
      if (node->next_created != NULL)
      {
         node->next_created->prev_created = node->prev_created;
      }
      if (node == _created_ifs_list)
      {
         _created_ifs_list = node->next_created;
      }
      --_created_ifs_count;

      if (node->used)
      {
         node->dev      = NULL;
         node->dev_info = NULL;
         node->created  = 0;
         meshap_core_stop_mesh();
      }
      else
      {
         /**
          * Remove from all net_ifs list
          */
         if (node->prev != NULL)
         {
            node->prev->next = node->next;
         }
         if (node->next != NULL)
         {
            node->next->prev = node->prev;
         }
         if (node == _ifs_list)
         {
            _ifs_list = node->next;
         }
         --_ifs_count;
         kfree(node);
      }
	  al_spin_unlock(core_net_splock, core_net_spvar);
   }
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP_CORE : FLOW : Exit: %s : %d\n", __func__,__LINE__);
}


int meshap_core_get_created_net_if_count(void)
{
   return _created_ifs_count;
}


meshap_core_net_if_t *meshap_core_get_created_net_if(int index)
{
   meshap_core_net_if_t *node;

   for (node = _created_ifs_list; node != NULL; node = node->next_created, index--)
   {
      if (!index)
      {
         return node;
      }
   }

   return NULL;
}


int meshap_core_start_mesh(void)
{
   /**
    * The fact that meshap_core_start_mesh has been called, really means that
    * configuration has been done. Now we scan the interfaces mentioned as see
    * if they have been created physically (i.e meshap_core_on_net_device_create
    * has been called for them). If the interfaces haven't been created, we simply
    * set the start pending flag and wait for them to be created.
    */

   meshap_core_net_if_t *node;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP_CORE : FLOW : Enter: %s : %d\n", __func__,__LINE__);

   node = _ifs_list;

   meshap_core_globals.state = MESHAP_CORE_GLOBALS_STATE_STARTED;
   if (!_meshap_core_can_start())
   {
      meshap_core_globals.start_pending = 1;
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESHAP_CORE : ERROR :  Queing start request until all devices initialize %s : %d\n", __func__, __LINE__);
      meshap_core_reboot_machine(MESHAP_REBOOT_MACHINE_CODE_DEVICE);
      return 0;
   }

   /**
    * Now start the mesh and access point here.
    */

   meshap_core_globals.start_pending = 0;

   if (access_point_initialize() == 0)
   {
      dot1x_initialize();
      dot11i_initialize(AL_CONTEXT_SINGLE);
      mesh_initialize(17, 1);
	  /* Initialization of rate ctrl moved to mode setting of each wireless interface */
      access_point_start(17);
	  al_create_thread_on_cpu(AL_CONTEXT _meshap_dot1x_timer, NULL,0, "_meshap_dot1x_timer");
	   threads_status |= THREAD_STATUS_BIT11_MASK;
      meshap_power_test_start();

      /* Rate Control thread is called */
      if (node->dev_info && node->dev_info->meshap_start_openrt_thread != NULL)
      {
         node->dev_info->meshap_start_openrt_thread(1000);
      }

      al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP_CORE : FLOW : Exit: %s : %d\n", __func__,__LINE__);
      return 0;
   }
   else
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESHAP_CORE : ERROR : access_point_initialize failed %s : %d\n", __func__,__LINE__);
      meshap_core_globals.state = MESHAP_CORE_GLOBALS_STATE_STOPPED;
      return -1;
   }
}


int meshap_core_stop_mesh(void)
{
   meshap_core_net_if_t *node;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP_CORE : FLOW : Enter: %s : %d\n", __func__,__LINE__);

   node = _ifs_list;

   if ((meshap_core_globals.state == MESHAP_CORE_GLOBALS_STATE_STARTED) &&
       (meshap_core_globals.start_pending != 1))
   {
      meshap_core_globals.start_pending = 0;
      meshap_core_globals.state         = MESHAP_CORE_GLOBALS_STATE_STOPPED;
      access_point_stop();
      dot11i_uninitialize(AL_CONTEXT_SINGLE);
      dot1x_uninitialize();
      mesh_uninitialize();
      access_point_uninitialize();
   }

   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"MESHAP_CORE : INFO :  Stopping kernel mode access point and mesh services %s : %d\n", __func__, __LINE__);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP_CORE : FLOW : Exit: %s : %d\n", __func__,__LINE__);

   return 0;
}


int meshap_core_get_sta_info(unsigned char *mac_addr, meshap_sta_info_t *sta_info)
{
   al_net_addr_t           addr;
   access_point_sta_info_t sta_entry;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP_CORE : FLOW : Enter: %s : %d\n", __func__,__LINE__);

   if (meshap_core_globals.state != MESHAP_CORE_GLOBALS_STATE_STARTED)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"MESHAP_CORE : INFO : meshap_core_globals.state = %d %s : %d\n",
             meshap_core_globals.state, __func__, __LINE__);
      return -1;
   }

   memset(&addr, 0, sizeof(al_net_addr_t));
   memset(sta_info, 0, sizeof(meshap_sta_info_t));

   addr.length = ETH_ALEN;
   memcpy(addr.bytes, mac_addr, ETH_ALEN);

   if (in_interrupt())
   {
      access_point_get_sta_info_ex(AL_CONTEXT & addr, &sta_entry, 1);
   }
   else
   {
      access_point_get_sta_info_ex(AL_CONTEXT & addr, &sta_entry, 0);
   }

   if (sta_entry.net_if == NULL)
   {
      return -1;
   }

   if (sta_entry.auth_state & ACCESS_POINT_STA_AUTH_STATE_ENCRYPTION)
   {
      sta_info->flags    |= MESHAP_STA_INFO_FLAG_ENCRYPTION;
      sta_info->key_index = sta_entry.key_index;
   }

   if (sta_entry.auth_state & ACCESS_POINT_STA_AUTH_STATE_COMPRESSION)
   {
      sta_info->flags |= MESHAP_STA_INFO_FLAG_COMPRESSION;
   }

   if (sta_entry.b_client)
   {
      sta_info->flags |= MESHAP_STA_INFO_FLAG_USE_802_11_B;
   }

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP_CORE : FLOW : Exit: %s : %d\n", __func__,__LINE__);
   return 0;
}


void meshap_core_exec_reboot(unsigned char reboot_code)
{
   torna_reboot_info_t info;
   unsigned long       flags;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP_CORE : FLOW : Enter: %s : %d\n", __func__,__LINE__);

   memset(&info, 0, sizeof(torna_reboot_info_t));

   info.code    = reboot_code;
   info.temp    = (unsigned char)meshap_get_board_temp();
   info.voltage = (unsigned char)meshap_get_board_voltage();

   local_irq_save(flags);

   torna_put_reboot_info(&info, NULL);

   local_irq_restore(flags);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"MESHAP_CORE : INFO : Rebooting... %s : %d\n", __func__, __LINE__);
   kernel_restart(NULL);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP_CORE : FLOW : Exit: %s : %d\n", __func__,__LINE__);
}


void meshap_core_reboot_machine(unsigned char reboot_code)
{
   if (MESHAP_ATOMIC_GET(meshap_core_globals.reboot_locked) > 0)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"MESHAP_CORE : INFO : Queing Reboot request until unclocked %s : %d\n", __func__, __LINE__);
      meshap_core_globals.reboot_code = reboot_code;
      MESHAP_ATOMIC_INC(meshap_core_globals.reboot_pending);
   }
   else
   {
      meshap_core_exec_reboot(reboot_code);
   }
}


void meshap_power_test_init_device(meshap_power_test_device_t *device, int phy_mode, int mode)
{
   meshap_core_net_if_t *core_net_if;
   int al_phy_mode;
   int al_op_mode;
   al_802_11_operations_t *operations;
   int tx_rate;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP_CORE : FLOW : Enter: %s : %d\n", __func__,__LINE__);

   core_net_if = (meshap_core_net_if_t *)device->impl_data;

   operations = (al_802_11_operations_t *)core_net_if->al_net_if.get_extended_operations(&core_net_if->al_net_if);

   if (operations == NULL)
   {
      return;
   }

   switch (phy_mode)
   {
   case MESHAP_POWER_TEST_PHY_MODE_A:
      al_phy_mode = AL_802_11_PHY_MODE_A;
      tx_rate     = 6;
      break;

   case MESHAP_POWER_TEST_PHY_MODE_B:
      al_phy_mode = AL_802_11_PHY_MODE_B;
      tx_rate     = 1;
      break;

   case MESHAP_POWER_TEST_PHY_MODE_BG:
      al_phy_mode = AL_802_11_PHY_MODE_BG;
      tx_rate     = 1;
      break;

   case MESHAP_POWER_TEST_PHY_MODE_G:
      al_phy_mode = AL_802_11_PHY_MODE_G;
      tx_rate     = 6;
      break;

   default:
      return;
   }

   switch (mode)
   {
   case MESHAP_POWER_TEST_OP_MODE_UPLINK:
      al_op_mode = AL_802_11_MODE_INFRA;
      break;

   case MESHAP_POWER_TEST_OP_MODE_DOWNLINK:
      al_op_mode = AL_802_11_MODE_MASTER;
      break;

   default:
      return;
   }

   operations->set_phy_mode(&core_net_if->al_net_if, al_phy_mode);
   operations->set_mode(&core_net_if->al_net_if, al_op_mode, 1);
   operations->set_bit_rate(&core_net_if->al_net_if, tx_rate);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP_CORE : FLOW : Exit: %s : %d\n", __func__,__LINE__);
}


int meshap_power_test_set_channel(meshap_power_test_device_t *device, int channel)
{
   al_802_11_operations_t *operations;
   meshap_core_net_if_t   *core_net_if;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP_CORE : FLOW : Enter: %s : %d\n", __func__,__LINE__);

   core_net_if = (meshap_core_net_if_t *)device->impl_data;
   operations  = (al_802_11_operations_t *)core_net_if->al_net_if.get_extended_operations(&core_net_if->al_net_if);

   if (operations == NULL)
   {
      return -1;
   }

   operations->set_channel(&core_net_if->al_net_if, channel);

   if (device->mode == MESHAP_POWER_TEST_OP_MODE_UPLINK)
   {
      al_net_addr_t addr;

      memset(&addr, 0, sizeof(al_net_addr_t));

      addr.length = ETH_ALEN;
      memcpy(addr.bytes, device->peer_mac, ETH_ALEN);

      operations->virtual_associate(&core_net_if->al_net_if, &addr, channel, 1);
   }

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP_CORE : FLOW : Exit: %s : %d\n", __func__,__LINE__);
   return 0;
}


void meshap_power_test_transmit(meshap_power_test_device_t *device,
                                unsigned char              *dst_mac,
                                unsigned char              *src_mac,
                                int                        rate_index,
                                unsigned char              *buffer,
                                int                        length)
{
   al_packet_t          *packet;
   meshap_core_net_if_t *core_net_if;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP_CORE : FLOW : Enter: %s : %d\n", __func__,__LINE__);

   core_net_if = (meshap_core_net_if_t *)device->impl_data;
   packet      = al_allocate_packet(AL_CONTEXT AL_PACKET_DIRECTION_OUT);

   packet->position -= length;
   memcpy(packet->buffer + packet->position, buffer, length);
   packet->data_length = length;
   packet->type        = 0xDEDE;
   packet->dot1q_mode  = AL_PACKET_DOT_1Q_MODE_NONE;

   if (device->type == MESHAP_POWER_TEST_DEVICE_TYPE_ETHERNET)
   {
      memcpy(packet->addr_1.bytes, dst_mac, ETH_ALEN);
      packet->addr_1.length = ETH_ALEN;
      memcpy(packet->addr_2.bytes, src_mac, ETH_ALEN);
      packet->addr_2.length = ETH_ALEN;
   }
   else
   {
      switch (device->mode)
      {
      case MESHAP_POWER_TEST_OP_MODE_UPLINK:
         memcpy(packet->addr_1.bytes, dst_mac, ETH_ALEN);
         packet->addr_1.length = ETH_ALEN;
         memcpy(packet->addr_2.bytes, src_mac, ETH_ALEN);
         packet->addr_2.length = ETH_ALEN;
         memcpy(packet->addr_3.bytes, dst_mac, ETH_ALEN);
         packet->addr_3.length = ETH_ALEN;
         packet->frame_control = AL_802_11_FC_TODS;
         break;

      case MESHAP_POWER_TEST_OP_MODE_DOWNLINK:
         memcpy(packet->addr_1.bytes, dst_mac, ETH_ALEN);
         packet->addr_1.length = ETH_ALEN;
         memcpy(packet->addr_2.bytes, src_mac, ETH_ALEN);
         packet->addr_2.length = ETH_ALEN;
         memcpy(packet->addr_3.bytes, src_mac, ETH_ALEN);
         packet->addr_3.length = ETH_ALEN;
         packet->frame_control = AL_802_11_FC_FROMDS;
         break;
      }

      packet->flags &= ~AL_PACKET_FLAGS_ENCRYPTION;
      packet->flags &= ~AL_PACKET_FLAGS_COMPRESSION;

      packet->flags   |= AL_PACKET_FLAGS_FIXED_RATE | AL_PACKET_FLAGS_EFFISTREAM;
      packet->bit_rate = rate_index;
   }


   core_net_if->al_net_if.transmit(&core_net_if->al_net_if, packet);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP_CORE : FLOW : Exit: %s : %d\n", __func__,__LINE__);
}
