/********************************************************************************
* MeshDynamics
* --------------
* File     : mesh_location_info.c
* Comments : Client Location Information Implementation
* Created  : 12/4/2007
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  0  |12/4/2007 | Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

#include <linux/string.h>
#include "al.h"
#include "al_print_log.h"
#include "al_conf.h"
#include "access_point.h"
#include "al_impl_context.h"
#include "mesh_defs.h"
#include "mesh_table.h"
#include "mesh.h"
#include "mesh_globals.h"
#include "mesh_errors.h"
#include "mesh_on_start.h"
#include "mesh_intervals.h"
#include "imcp_snip.h"
#include "mesh_imcp.h"
#include "mesh_ng.h"

#define _HASH_SIZE      29

#define _PURGE_TIME     180          /* Seconds */
#define _EXPIRY_TIME    15           /* Seconds */

#ifdef  __GNUC__
#define _MAC_HASH_FUNCTION(addr)     \
   ({                                \
      int hash;                      \
      hash = (addr)->bytes[0] * 1    \
             + (addr)->bytes[1] * 2  \
             + (addr)->bytes[2] * 3  \
             + (addr)->bytes[3] * 4  \
             + (addr)->bytes[4] * 5  \
             + (addr)->bytes[5] * 6; \
      hash % _HASH_SIZE;             \
   }                                 \
   )
#else
static int _MAC_HASH_FUNCTION(al_net_addr_t *addr)
{
   int hash; \
   hash = (addr)->bytes[0] * 1
          + (addr)->bytes[1] * 2
          + (addr)->bytes[2] * 3
          + (addr)->bytes[3] * 4
          + (addr)->bytes[4] * 5
          + (addr)->bytes[5] * 6;
   return hash % _HASH_SIZE;
}
#endif

struct _client_signal_entry
{
   al_net_addr_t               sta_address;
   al_u64_t                    timestamp;
   unsigned char               rssi;
   struct _client_signal_entry *next_hash;
   struct _client_signal_entry *prev_hash;
};
typedef struct _client_signal_entry   _client_signal_entry_t;

struct _location_info
{
   _client_signal_entry_t *hash[_HASH_SIZE];
};
typedef struct _location_info   _location_info_t;

void mesh_location_initialize(AL_CONTEXT_PARAM_DECL_SINGLE)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
   int              i;
   _location_info_t *info;
   unsigned long    flags;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,
                "MESH_LOC: Initializing client location information on downlinks...");

   _al_disable_interrupts(gbl_mutex, gbl_muvar, gbl_mupid);

   for (i = 0; i < mesh_hardware_info->wm_net_if_count; i++)
   {
      if (!AL_NET_IF_IS_WIRELESS(_WMS_NET_IF(i)))
      {
         continue;
      }

      info = (_location_info_t *)al_heap_alloc(AL_CONTEXT sizeof(_location_info_t));
      memset(info, 0, sizeof(_location_info_t));

      _WMS_CONF(i)->location_info = (void *)info;
   }

   _al_enable_interrupts(gbl_mutex, gbl_muvar, gbl_mupid);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
}


static AL_INLINE void _clear_info(AL_CONTEXT_PARAM_DECL _location_info_t *info)
{
   int i;
   _client_signal_entry_t *head;
   _client_signal_entry_t *temp;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);

   for (i = 0; i < _HASH_SIZE; i++)
   {
      head = info->hash[i];

      while (head != NULL)
      {
         temp = head->next_hash;
         al_heap_free(AL_CONTEXT head);
         head = temp;
      }
   }

   al_heap_free(AL_CONTEXT info);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
}


void mesh_location_uninitialize(AL_CONTEXT_PARAM_DECL_SINGLE)
{
   int           i;
   unsigned long flags;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,
                "MESH_LOC: Uninitializing client location information on downlinks...");

   _al_disable_interrupts(gbl_mutex, gbl_muvar, gbl_mupid);

   for (i = 0; i < mesh_hardware_info->wm_net_if_count; i++)
   {
      if (!AL_NET_IF_IS_WIRELESS(_WMS_NET_IF(i)))
      {
         continue;
      }

      _clear_info(AL_CONTEXT(_location_info_t *) _WMS_CONF(i)->location_info);
   }

   _al_enable_interrupts(gbl_mutex, gbl_muvar, gbl_mupid);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
}


static AL_INLINE void _process_info(AL_CONTEXT_PARAM_DECL al_net_if_t *net_if, _location_info_t *info, al_net_addr_t *sta_addr, int rssi)
{
   int bucket;
   _client_signal_entry_t *node;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
   bucket = _MAC_HASH_FUNCTION(sta_addr);

   node = info->hash[bucket];

   /**
    * Search for node in hash-table
    */

   while (node != NULL)
   {
      if (AL_NET_ADDR_EQUAL(sta_addr, &node->sta_address))
      {
         break;
      }
      node = node->next_hash;
   }

   if (node == NULL)
   {
      /**
       * Create a new entry and insert into hashtable
       */

      node = (_client_signal_entry_t *)al_heap_alloc(AL_CONTEXT sizeof(_client_signal_entry_t));

      node->prev_hash = NULL;
      memcpy(&node->sta_address, sta_addr, sizeof(al_net_addr_t));

      node->next_hash = info->hash[bucket];

      if (info->hash[bucket] != NULL)
      {
         info->hash[bucket]->prev_hash = node;
      }

      info->hash[bucket] = node;

      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,
                   "MESH_LOC: Added location entry for "AL_NET_ADDR_STR " RSSI %d over %s",
                   AL_NET_ADDR_TO_STR(&node->sta_address),
                   rssi,
                   net_if->name);
   }

   /**
    * Update node's RSSI and timestamp
    */

   node->rssi      = rssi;
   node->timestamp = al_get_tick_count(AL_CONTEXT_SINGLE);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
}


void mesh_location_process_info(AL_CONTEXT_PARAM_DECL al_net_if_t *al_net_if, al_net_addr_t *sta_addr, int rssi)
{
   _location_info_t *info;
   int              i;
   unsigned long    flags;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);

   for (i = 0, info = NULL; i < mesh_hardware_info->wm_net_if_count; i++)
   {
      if (!AL_NET_IF_IS_WIRELESS(_WMS_NET_IF(i)))
      {
         continue;
      }

      if (_WMS_NET_IF(i) == al_net_if)
      {
         info = (_location_info_t *)_WMS_CONF(i)->location_info;
         break;
      }
   }

   if (info == NULL)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR," info is NULL :: func : %s LINE : %d\n", __func__,__LINE__);
      return;
   }


   _al_disable_interrupts(gbl_mutex, gbl_muvar, gbl_mupid);

   _process_info(AL_CONTEXT al_net_if, info, sta_addr, rssi);

   _al_enable_interrupts(gbl_mutex, gbl_muvar, gbl_mupid);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
}


static AL_INLINE int _fill_signal_info(AL_CONTEXT_PARAM_DECL _location_info_t *info, client_signal_info_t *signal_info)
{
   unsigned long          flags;
   _client_signal_entry_t *node;
   _client_signal_entry_t *temp;
   al_u64_t               tickcount;
   al_u64_t               timeout_purge;
   al_u64_t               timeout_expiry;
   int i;
   int count;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
   _al_disable_interrupts(gbl_mutex, gbl_muvar, gbl_mupid);

   for (i = 0, count = 0; i < _HASH_SIZE; i++)
   {
      node = info->hash[i];

      while (node != NULL)
      {
         temp = node->next_hash;

         tickcount      = al_get_tick_count(AL_CONTEXT_SINGLE);
         timeout_purge  = node->timestamp + _PURGE_TIME * HZ;
         timeout_expiry = node->timestamp + _EXPIRY_TIME * HZ;

         /**
          * Skip and remove entries that are older than _EXPIRY_TIME
          */

         if (al_timer_after(tickcount, timeout_purge))
         {
            if (node->prev_hash != NULL)
            {
               node->prev_hash->next_hash = node->next_hash;
            }

            if (node->next_hash != NULL)
            {
               node->next_hash->prev_hash = node->prev_hash;
            }

            if (node == info->hash[i])
            {
               info->hash[i] = node->next_hash;
            }

            al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,
                         "MESH_LOC: Removing location entry for "AL_NET_ADDR_STR,
                         AL_NET_ADDR_TO_STR(&node->sta_address));

            al_heap_free(AL_CONTEXT node);

            goto _next;
         }
         else if (al_timer_after(tickcount, timeout_expiry))
         {
            goto _next;
         }

         /**
          * Add entries to the IMCP signal info
          */

         memcpy(&signal_info[count].sta_mac, &node->sta_address, sizeof(al_net_addr_t));
         signal_info[count].rssi = node->rssi;
         ++count;

_next:

         node = temp;
      }
   }

   _al_enable_interrupts(gbl_mutex, gbl_muvar, gbl_mupid);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
   return count;
}


static AL_INLINE void _send_info(AL_CONTEXT_PARAM_DECL al_net_if_t *net_if, _location_info_t *info)
{
   imcp_packet_info_t   *pi;
   unsigned char        *buffer;
   client_signal_info_t *signal_info;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
   pi          = imcp_snip_create_packet(AL_CONTEXT_SINGLE);
   buffer      = al_get_gen_2KB_buffer(AL_CONTEXT_SINGLE);
   signal_info = (client_signal_info_t *)buffer;

   pi->packet_type = IMCP_SNIP_PACKET_TYPE_CLIENT_SIGNAL_INFO;

   memcpy(&pi->u.client_signal_info.ds_mac, _DS_MAC, sizeof(al_net_addr_t));
   strcpy(pi->u.client_signal_info.if_name, net_if->name);

   pi->u.client_signal_info.entry_count = _fill_signal_info(AL_CONTEXT info, signal_info);
   pi->u.client_signal_info.signal_info = signal_info;
   imcp_snip_send(AL_CONTEXT pi);

   imcp_snip_release_packet(AL_CONTEXT pi);

   al_release_gen_2KB_buffer(AL_CONTEXT buffer);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
}


void mesh_location_send_info(AL_CONTEXT_PARAM_DECL_SINGLE)
{
   _location_info_t *info;
   int              i;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
   for (i = 0, info = NULL; i < mesh_hardware_info->wm_net_if_count; i++)
   {
      if (!AL_NET_IF_IS_WIRELESS(_WMS_NET_IF(i)))
      {
         continue;
      }

      _send_info(AL_CONTEXT _WMS_NET_IF(i), (_location_info_t *)_WMS_CONF(i)->location_info);
   }
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
}
