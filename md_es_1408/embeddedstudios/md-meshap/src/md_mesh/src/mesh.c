/********************************************************************************
* MeshDynamics
* --------------
* File     : mesh.c
* Comments : Mesh implementation file.
* Created  : 4/13/2004
* Author   : Anand Bhange
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* | 52  |12/14/2005| acktimeout and regdomain changes                |prachiti|
* -----------------------------------------------------------------------------
* | 51  |10/5/2005 | Oscillation related changes                     | Sriram |
* -----------------------------------------------------------------------------
* | 50  |9/22/2005 | Interrupt disable/enable semantics changed      | Sriram |
* -----------------------------------------------------------------------------
* | 49  |6/21/2005 | Misc changes                                    | Sriram |
* -----------------------------------------------------------------------------
* | 48  |6/15/2005 | Robustness changes                              | Sriram |
* -----------------------------------------------------------------------------
* | 47  |5/20/2005 | Preferred parent changes                        | Sriram |
* -----------------------------------------------------------------------------
* | 46  |5/20/2005 | DS Tx Power and TX Rate set                     | Sriram |
* -----------------------------------------------------------------------------
* | 45  |5/8/2005  | SQNR Reset disabled                             | Sriram |
* -----------------------------------------------------------------------------
* | 44  |4/15/2005 | Additional Security related changes             | Sriram |
* -----------------------------------------------------------------------------
* | 43  |04/06/2005| Security changes                                | Sriram |
* -----------------------------------------------------------------------------
* | 42  |03/09/2005| Misc changes                                    | Anand  |
* -----------------------------------------------------------------------------
* | 41  |1/12/2005 | varify_wireless_ds_parent child looping fixed   | Sriram |
* -----------------------------------------------------------------------------
* | 40  |1/6/2005  | Slight modifications to purge_known_aps         | Sriram |
* -----------------------------------------------------------------------------
* | 39  |12/22/2004| Fixed disconnect-rescan bug                     | Sriram |
* -----------------------------------------------------------------------------
* | 38  |12/13/2004| Misc changes                                    | Sriram |
* -----------------------------------------------------------------------------
* | 37  |11/01/2004| Bug Fixed for monitor radio                     | Anand  |
* -----------------------------------------------------------------------------
* | 36  |10/28/2004| Changes for monitor radio                       | Anand  |
* -----------------------------------------------------------------------------
* | 35  |7/29/2004 | Changes made by Sriram                          | Anand  |
* -----------------------------------------------------------------------------
* | 34  |7/29/2004 | changes for Rover                               | Anand  |
* -----------------------------------------------------------------------------
* | 33  |7/25/2004 | Misc changes                                    | Sriram |
* -----------------------------------------------------------------------------
* | 32  |7/25/2004 | Misc changes                                    | Anand  |
* -----------------------------------------------------------------------------
* | 31  |7/24/2004 | Preferred parent + en/dis able processing       | Anand  |
* -----------------------------------------------------------------------------
* | 30  |7/23/2004 | Misc changes                                    | Sriram |
* -----------------------------------------------------------------------------
* | 29  |7/23/2004 | preferred_parent check added which switch       | Anand  |
* -----------------------------------------------------------------------------
* | 28  |7/23/2004 | enable_disable_processing function added        | Anand  |
* -----------------------------------------------------------------------------
* | 27  |7/19/2004 | no deletion of known_ap is HB missing           | Anand  |
* -----------------------------------------------------------------------------
* | 26  |7/5/2004  | preferred_paren selection for best parent added | Anand  |
* -----------------------------------------------------------------------------
* | 25  |6/16/2004 | Fixes for pSeudo WM                             | Anand  |
* -----------------------------------------------------------------------------
* | 24  |6/16/2004 | ASSERT statements for checking NULL values      | Anand  |
* -----------------------------------------------------------------------------
* | 23  |6/15/2004 | known_ap_enable_disable func added              | Anand  |
* -----------------------------------------------------------------------------
* | 22  |6/11/2004 | purging parentap if not getting data packets    | Anand  |
* -----------------------------------------------------------------------------
* | 21  |6/11/2004 | Hardcoded value 3 replaced by hbeat_miss_count  | Anand  |
* -----------------------------------------------------------------------------
* | 20  |6/7/2004  | Filter macro used instead of al_print_log       | Anand  |
* -----------------------------------------------------------------------------
* | 19  |5/31/2004 | changes in include files for linux compilation  | Anand  |
* -----------------------------------------------------------------------------
* | 18  |5/28/2004 | Bug Fixed - Change Thr for new parent           | Anand  |
* -----------------------------------------------------------------------------
* | 17  |5/28/2004 | ap_is_child added to avoid inconsistant state   | Anand  |
* -----------------------------------------------------------------------------
* | 16  |5/28/2004 | mesh health index calculations added.           | Anand  |
* -----------------------------------------------------------------------------
* | 15  |5/27/2004 | known_pas oparations moved here                 | Anand  |
* -----------------------------------------------------------------------------
* | 14  |5/26/2004 | Dead Lock removed                               | Anand  |
* -----------------------------------------------------------------------------
* | 13  |5/26/2004 | Bug Fixed -  known_aps (deleted object)         | Anand  |
* -----------------------------------------------------------------------------
* | 12  |5/25/2004 | mesh state notify messages added                | Anand  |
* -----------------------------------------------------------------------------
* | 11  |5/25/2004 | set_tx_power instead of get_tx_power            | Anand  |
* -----------------------------------------------------------------------------
* | 10  |5/21/2004 | Log type added to al_print_log                  | Anand  |
* -----------------------------------------------------------------------------
* |  9  |5/21/2004 | Shutting WM radio's before new parent search    | Anand  |
* -----------------------------------------------------------------------------
* |  8  |5/18/2004 | Bug Fixed (Crash while sorting linked list)     | Anand  |
* -----------------------------------------------------------------------------
* |  7  |5/14/2004 | Lock added while sorting list of known_aps      | Anand  |
* -----------------------------------------------------------------------------
* |  6  |5/11/2004 | find_alternet_parent implemented for rescanning | Anand  |
* -----------------------------------------------------------------------------
* |  5  |5/7/2004  | associate with best parent moved here           | Anand  |
* -----------------------------------------------------------------------------
* |  4  |5/6/2004  | \r\n removed from log messages                  | Anand  |
* -----------------------------------------------------------------------------
* |  3  |4/30/2004 | Log messages Added                              | Anand  |
* -----------------------------------------------------------------------------
* |  2  |4/26/2004 | Testing Base Line                               | Anand  |
* -----------------------------------------------------------------------------
* |  1  |4/21/2004 | **Made (major) changes for AL_CONTEXT**         | Anand  |
* -----------------------------------------------------------------------------
* |  0  |4/13/2004 | Created                                         | Anand  |
* -----------------------------------------------------------------------------
********************************************************************************/

#include <linux/string.h>
#include "al.h"
#include "al_print_log.h"
#include "al_conf.h"
#include "access_point.h"
#include "al_impl_context.h"
#include "mesh_defs.h"
#include "mesh_table.h"
#include "mesh.h"
#include "mesh_globals.h"
#include "mesh_errors.h"
#include "mesh_on_start.h"
#include "mesh_intervals.h"

#ifdef AL_USE_ENCRYPTION
#include "al_codec.h"
#endif

void set_net_if_rate_ctrl_param(AL_CONTEXT_PARAM_DECL al_net_if_t *net_if, mesh_conf_if_info_t *if_info)
{
   al_802_11_operations_t *operations;
   al_rate_ctrl_param_t   rate_ctrl_param;
   al_rate_table_t        rate_table;
   int i;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
   operations = (al_802_11_operations_t *)net_if->get_extended_operations(net_if);

   if (operations == NULL)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR," operations is NULL :: func : %s LINE : %d\n", __func__,__LINE__);
      return;
   }

   if (operations->init_rate_ctrl(net_if) < 0) {
       al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "Rate control initilization failed for <%s>\n", net_if->name); 
       return;
   }
   al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "Rate control initilized for <%s>\n", net_if->name); 
   //operations->get_rate_table(net_if, &rate_table);

   rate_ctrl_param.max_error_percent  = if_info->max_error_percent;
   rate_ctrl_param.max_errors         = if_info->max_errors;
   rate_ctrl_param.max_retry_percent  = if_info->max_retry_percent;
   rate_ctrl_param.min_qualification  = if_info->min_qualification;
   rate_ctrl_param.min_retry_percent  = if_info->min_retry_percent;
   rate_ctrl_param.qualification      = if_info->qualification;
   rate_ctrl_param.t_el_max           = if_info->t_el_max;
   rate_ctrl_param.t_el_min           = if_info->t_el_min;
   rate_ctrl_param.initial_rate_index = if_info->initial_rate;

   operations->get_rate_table(net_if, &rate_table);

   if_info->initial_rate_mbps = rate_table.rates[0].rate_in_mbps;
   for (i = 0; i < rate_table.count; i++)
   {
      if (rate_table.rates[i].index == if_info->initial_rate)
      {
         if_info->initial_rate_mbps = rate_table.rates[i].rate_in_mbps;
      }
   }

   operations->set_rate_ctrl_parameters(net_if, &rate_ctrl_param);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
}
