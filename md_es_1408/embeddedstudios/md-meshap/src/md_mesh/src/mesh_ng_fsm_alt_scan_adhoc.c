/********************************************************************************
* MeshDynamics
* --------------
* File     : mesh_ng_fsm_alt_scan_adhoc.c
* Comments : Alternate scan state for ADHOC mode
* Created  : 8/23/2007
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  0  |8/23/2007 | Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

/**
 * This is a private source file included by mesh_ng_fsm.c, hence
 * It was created to improve readability.
 */
extern int mesh_on_start_find_ds1(AL_CONTEXT int boot_flag);

extern int mesh_on_start_initialize_ds_interface1(void);

extern int mesh_network_unstable;

static void _alternate_scan_adhoc_execute(AL_CONTEXT_PARAM_DECL_SINGLE)
{
   al_notify_message_t   message;
   access_point_config_t *config;
   al_net_if_t           *old_ds_net = NULL;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_NG : FLOW : Enter: %s : %d\n", __func__,__LINE__);

   config = &globals.config;

   message.message_type = AL_NOTIFY_MESSAGE_TYPE_SET_STATE;
   message.message_data = (void *)'Y';
   al_notify_message(AL_CONTEXT & message);

   AL_ATOMIC_SET(up_cancel_channel_change, 1);

   if (mesh_network_unstable) {
      mesh_ng_clear_mobility_window();
      goto become_lfr;
   }

   mesh_ng_evaluate_list(AL_CONTEXT_SINGLE);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MESH_NG : INFO : Disabling AP processing before alternate parent search %s : %d\n", __func__, __LINE__);
   access_point_enable_disable_processing(AL_CONTEXT 0);

   AL_ATOMIC_SET(_DS_NET_IF->buffering_state, AL_NET_IF_BUFFERING_STATE_NONE);

   if (mesh_ng_core_join_best_parent_adhoc(AL_CONTEXT_SINGLE))
   {
become_lfr:
      func_mode = _FUNC_MODE_LFR;
      mesh_mode = _MESH_AP_RELAY;

      current_parent = &dummy_lfr_parent;

      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MESH_NG : INFO : Alternate scan failed, becoming LFR,  %s : %d\n", __func__, __LINE__);

      access_point_enable_disable_processing(AL_CONTEXT 1);

      _mesh_ng_fsm_set_state(AL_CONTEXT MESH_NG_FSM_STATE_RUNNING);
      mesh_ng_fsm_update_downlink_vendor_info(AL_CONTEXT_SINGLE);
      postscan_setup_wms();

      if (mesh_config->failover_enabled)
      {
         mesh_on_start_find_ds1(0);
         config->ds_net_if_info.net_if      = mesh_hardware_info->ds_net_if;
         config->ds_net_if_info.ack_timeout = mesh_hardware_info->ds_if_ack_timeout;
         config->ds_net_if_info.txpower     = mesh_hardware_info->ds_if_tx_power;
         config->ds_net_if_info.txrate      = mesh_hardware_info->ds_if_tx_rate;
         strcpy(config->ds_net_if_info.essid, mesh_hardware_info->ds_conf->essid);

         if (!strcmp(mesh_hardware_info->ds_net_if->name, "eth1"))
         {
            /* get the ds as eth1 */
            AL_ATOMIC_SET(_DS_NET_IF->buffering_state, AL_NET_IF_BUFFERING_STATE_NONE);
         }
         else
         {
            AL_ATOMIC_SET(_DS_NET_IF->buffering_state, AL_NET_IF_BUFFERING_STATE_DROP);
         }
      }
      else
      {
         AL_ATOMIC_SET(_DS_NET_IF->buffering_state, AL_NET_IF_BUFFERING_STATE_DROP);
      }
   }
   else
   {
      if (current_parent->flags & MESH_PARENT_FLAG_LIMITED)
      {
         func_mode = _FUNC_MODE_LFN;
         mesh_mode = _MESH_AP_RELAY;
      }
      else
      {
         func_mode = _FUNC_MODE_FFN;
         mesh_mode = _MESH_AP_RELAY;
      }

      mesh_on_start_find_ds();
      config->ds_net_if_info.net_if      = mesh_hardware_info->ds_net_if;
      config->ds_net_if_info.ack_timeout = mesh_hardware_info->ds_if_ack_timeout;
      config->ds_net_if_info.txpower     = mesh_hardware_info->ds_if_tx_power;
      config->ds_net_if_info.txrate      = mesh_hardware_info->ds_if_tx_rate;
      strcpy(config->ds_net_if_info.essid, mesh_hardware_info->ds_conf->essid);

      AL_ATOMIC_SET(_DS_NET_IF->buffering_state, AL_NET_IF_BUFFERING_STATE_NONE);
      _mesh_ng_fsm_set_state(AL_CONTEXT MESH_NG_FSM_STATE_RUNNING);

      mesh_ng_fsm_update_downlink_vendor_info(AL_CONTEXT_SINGLE);
      postscan_setup_wms();
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MESH_NG : INFO : Enabling AP processing after <quick> "
                        "alternate parent search %s : %d\n", __func__, __LINE__);
      access_point_enable_disable_processing(AL_CONTEXT 1);
      mesh_imcp_send_packet_ap_sub_tree_info(AL_CONTEXT_SINGLE);

      message.message_type = AL_NOTIFY_MESSAGE_TYPE_SET_STATE;
      message.message_data = (void *)'G';
      al_notify_message(AL_CONTEXT & message);

      mesh_imcp_send_packet_heartbeat(AL_CONTEXT mesh_heart_beat_sqnr);
      mesh_imcp_send_packet_heartbeat2(AL_CONTEXT mesh_heart_beat_sqnr);
      mesh_heart_beat_sqnr++;

      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MESH_NG : INFO : Got best parent, (Parent quick shift, alternate), sub tree "
                                 "info sent. %s : %d\n", __func__, __LINE__);
   }
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_NG : FLOW : Exit: %s : %d\n", __func__,__LINE__);
}
