/********************************************************************************
* MeshDynamics
* --------------
* File     : mesh_ng_fsm_step_scan.c
* Comments : Step Scanning State
* Created  : 8/21/2007
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  0  |8/21/2007 | Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

/**
 * This is a private source file included by mesh_ng_fsm.c, hence
 * It was created to improve readability.
 */
static void AL_INLINE _begin_buffering_before_scan(AL_CONTEXT_PARAM_DECL int dwell_time_in_millis)
{
   mesh_imcp_send_packet_buffering_command(AL_CONTEXT _DS_NET_IF, IMCP_SNIP_BUFFERING_TYPE_START_REQUEST, dwell_time_in_millis);
   AL_ATOMIC_SET(_DS_NET_IF->buffering_state, AL_NET_IF_BUFFERING_STATE_DROP);
}


static void AL_INLINE _end_buffering_after_scan(AL_CONTEXT_PARAM_DECL int dwell_time_in_millis)
{
   mesh_imcp_send_packet_buffering_command(AL_CONTEXT _DS_NET_IF, IMCP_SNIP_BUFFERING_TYPE_STOP_REQUEST, dwell_time_in_millis);
   AL_ATOMIC_SET(_DS_NET_IF->buffering_state, AL_NET_IF_BUFFERING_STATE_NONE);
}


static void AL_INLINE _virtual_associate_step_scan(AL_CONTEXT_PARAM_DECL al_net_addr_t *bssid, int channel, int start)
{
   al_802_11_operations_t *ex_op;

   ex_op = (al_802_11_operations_t *)_DS_NET_IF->get_extended_operations(_DS_NET_IF);

   if (ex_op != NULL)
   {
      ex_op->virtual_associate(_DS_NET_IF, bssid, channel, start);
   }
}


static void _step_scan_execute(AL_CONTEXT_PARAM_DECL void *event_data)
{
   int dwell_time_in_millis;
   int channel;
   int ap_count;
   al_scan_access_point_info_t *access_points;
   int ret;

   if ((_DS_CONF->dca_list_count == 0) || (mesh_mode != _MESH_AP_RELAY))
   {
      goto _out;
   }

   if ((step_scan_channel_index < 0) || (step_scan_channel_index >= _DS_CONF->dca_list_count))
   {
      step_scan_channel_index = 0;
   }

   channel = _DS_CONF->dca_list[step_scan_channel_index];
   dwell_time_in_millis = (int)event_data;

   if ((func_mode != _FUNC_MODE_LFR) && (func_mode != _FUNC_MODE_FFR))
   {
      _begin_buffering_before_scan(AL_CONTEXT dwell_time_in_millis * 2);
      _virtual_associate_step_scan(AL_CONTEXT & zeroed_net_addr, channel, 2);
   }

   ret = mesh_ng_step_scan_parents(AL_CONTEXT
                                   step_scan_channel_index,
                                   dwell_time_in_millis,
                                   &access_points,
                                   &ap_count);

   if ((func_mode != _FUNC_MODE_LFR) && (func_mode != _FUNC_MODE_FFR))
   {
      _virtual_associate_step_scan(AL_CONTEXT & current_parent->bssid, current_parent->channel, 0);
      _end_buffering_after_scan(AL_CONTEXT dwell_time_in_millis * 2);
   }

   if (ret == 0)
   {
      ret = mesh_ng_step_process_parents(AL_CONTEXT ap_count, access_points);

      al_heap_free(AL_CONTEXT access_points);
   }

   ++step_scan_channel_index;

_out:

   _mesh_ng_fsm_set_state(AL_CONTEXT MESH_NG_FSM_STATE_RUNNING);
}
