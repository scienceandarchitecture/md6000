/********************************************************************************
* MeshDynamics
* --------------
* File     : mesh_ng_fsm_sample.c
* Comments : Mesh New Generation Sampler Implementation
* Created  : 4/10/2006
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  2  |6/5/2007  | mesh_imcp_send_packet_ds_excerciser changed     | Sriram |
* -----------------------------------------------------------------------------
* |  1  |5/1/2007  | Added negative RSSI filtering                   | Sriram |
* -----------------------------------------------------------------------------
* |  0  |4/10/2006 | Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

/**
 * This is a private source file included by mesh_ng_fsm.c, hence
 * It was created to improve readability.
 */
static AL_INLINE void _get_sampling_parent(AL_CONTEXT_PARAM_DECL_SINGLE)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
   do
   {
      mesh_ng_remove_sampling_head(AL_CONTEXT_SINGLE);
      if (current_sampling_parent == NULL)
      {
         break;
      }
      if (current_sampling_parent->flags & MESH_PARENT_FLAG_DISABLED ||
          current_sampling_parent->flags & MESH_PARENT_FLAG_QUESTION)
      {
         mesh_ng_add_sampling_parent(AL_CONTEXT_SINGLE);
         continue;
      }
      else
      {
         break;
      }
   } while (1);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
}


static AL_INLINE void _set_rate_ctrl_last_check(AL_CONTEXT_PARAM_DECL_SINGLE)
{
   al_802_11_operations_t *ex_op;
   al_rate_ctrl_info_t    info;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
   ex_op = (al_802_11_operations_t *)_DS_NET_IF->get_extended_operations(_DS_NET_IF);

   if (ex_op != NULL)
   {
      ex_op->get_rate_ctrl_info(_DS_NET_IF, &current_sampling_parent->bssid, &info);
      current_sampling_parent->last_rate_check = info.last_check;
   }
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
}


static AL_INLINE int _has_rate_changed(AL_CONTEXT_PARAM_DECL unsigned long *last_check, unsigned char *avg_ack_rssi)
{
   al_802_11_operations_t *ex_op;
   al_rate_ctrl_info_t    info;

   ex_op = (al_802_11_operations_t *)_DS_NET_IF->get_extended_operations(_DS_NET_IF);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
   if (ex_op != NULL)
   {
      ex_op->get_rate_ctrl_info(_DS_NET_IF, &current_sampling_parent->bssid, &info);
      *last_check = info.last_check;

      /**
       * Filter out negative RSSI values here.
       */

      if (info.avg_ack_rssi > 96)
      {
         info.avg_ack_rssi = 0;
      }

      *avg_ack_rssi = info.avg_ack_rssi;

      if (info.rate_in_mbps != current_sampling_parent->direct_bit_rate)
      {
         current_sampling_parent->direct_bit_rate = info.rate_in_mbps;
         al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR," assign the proper rate func : %s LINE : %d\n", __func__,__LINE__);
         return 1;
      }

      current_sampling_parent->direct_bit_rate = info.rate_in_mbps;

   }

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
   return 0;
}


static AL_INLINE void _set_t_next(AL_CONTEXT_PARAM_DECL_SINGLE)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
   current_sampling_parent->t_next += mesh_config->sampling_t_next_min;

   if (current_sampling_parent == current_parent)
   {
      if (current_sampling_parent->t_next > mesh_config->hbeat_interval * 1000)
      {
         current_sampling_parent->t_next = mesh_config->hbeat_interval * 1000;
      }
   }
   else
   {
      if (current_sampling_parent->t_next > mesh_config->sampling_t_next_max)
      {
         current_sampling_parent->t_next = mesh_config->sampling_t_next_max;
      }
   }
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
}


static AL_INLINE void _setup_sampling_parent(AL_CONTEXT_PARAM_DECL_SINGLE)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
   unsigned long last_check;
   int           continue_same;
   unsigned char avg_ack_rssi;
   int           ret;

   if (current_sampling_parent == NULL)
   {
      _get_sampling_parent(AL_CONTEXT_SINGLE);
   }

   if (current_sampling_parent == NULL)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR," current_sampling_parent is NULL :: func : %s LINE : %d\n", __func__,__LINE__);
      return;
   }

   if ((current_sampling_parent->sample_packets >= current_sampling_parent->max_sample_packets) &&
       (current_sampling_parent->sample_packets != 0))
   {
      continue_same = 0;

      /**
       * Enquire the driver about the current bit_rate to the sampling parent.
       * If rate has changed become agressive by reducing the t_next to minimum,
       * or else increase t_next.
       */

      ret = _has_rate_changed(AL_CONTEXT & last_check, &avg_ack_rssi);

      if (mesh_hardware_info->monitor_type == MONITOR_TYPE_ABSENT)
      {
         current_sampling_parent->signal = (current_sampling_parent->signal + avg_ack_rssi) / 2;
      }

      if (ret == 1)
      {
         current_sampling_parent->t_next = current_sampling_parent->t_next / 2;
         if (current_sampling_parent->t_next < mesh_config->sampling_t_next_min)
         {
            current_sampling_parent->t_next = mesh_config->sampling_t_next_min;
         }
      }
      else
      {
         if (last_check != current_sampling_parent->last_rate_check)
         {
            _set_t_next(AL_CONTEXT_SINGLE);
         }
         else
         {
            al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MESH_NG : Continuing to sample parent "AL_NET_ADDR_STR "", AL_NET_ADDR_TO_STR(&current_sampling_parent->ds_mac));
            continue_same = 1;
         }
      }

      if (!continue_same)
      {
         /**
          * Add the sampling parent back to the list and take the next one...
          */

         mesh_ng_add_sampling_parent(AL_CONTEXT_SINGLE);
         _get_sampling_parent(AL_CONTEXT_SINGLE);
      }
   }
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
}


static void AL_INLINE _begin_buffering(AL_CONTEXT_PARAM_DECL_SINGLE)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
   mesh_imcp_send_packet_buffering_command(AL_CONTEXT _DS_NET_IF, IMCP_SNIP_BUFFERING_TYPE_START_REQUEST, 1000);
   AL_ATOMIC_SET(_DS_NET_IF->buffering_state, AL_NET_IF_BUFFERING_STATE_REQUEUE);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
}


static void AL_INLINE _end_buffering(AL_CONTEXT_PARAM_DECL_SINGLE)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
   mesh_imcp_send_packet_buffering_command(AL_CONTEXT _DS_NET_IF, IMCP_SNIP_BUFFERING_TYPE_STOP_REQUEST, 1000);
   AL_ATOMIC_SET(_DS_NET_IF->buffering_state, AL_NET_IF_BUFFERING_STATE_NONE);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
}


static void AL_INLINE _virtual_associate(AL_CONTEXT_PARAM_DECL parent_t *parent, int start)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
   al_802_11_operations_t *ex_op;

   ex_op = (al_802_11_operations_t *)_DS_NET_IF->get_extended_operations(_DS_NET_IF);

   if (ex_op != NULL)
   {
      ex_op->virtual_associate(_DS_NET_IF, &parent->bssid, parent->channel, start);
   }
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
}


static void AL_INLINE _transmit_sampling_packet(AL_CONTEXT_PARAM_DECL_SINGLE)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
   mesh_imcp_send_packet_ds_excerciser(AL_CONTEXT NULL, NULL);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
}


static void _sample_execute(AL_CONTEXT_PARAM_DECL_SINGLE)
{
   int i;

   goto _out;

   _setup_sampling_parent(AL_CONTEXT_SINGLE);

   if (current_sampling_parent == NULL)
   {
      goto _out;
   }

   if ((current_sampling_parent->sample_packets == 0) && (current_sampling_parent->max_sample_packets == 0))
   {
      current_sampling_parent->max_sample_packets = _DS_CONF->qualification;
   }

   if (current_sampling_parent != current_parent)
   {
      /** Send IMCP Buffering begin to parent and Setup DS_NET_IF to Buffering */
      _begin_buffering(AL_CONTEXT_SINGLE);
      /** Join sampling parent */
      _virtual_associate(AL_CONTEXT current_sampling_parent, 1);
   }

   _set_rate_ctrl_last_check(AL_CONTEXT_SINGLE);

   /** Transmit atmost sampling_n_p number of packets at a time */
   for (i = 0; i < mesh_config->sampling_n_p; i++, current_sampling_parent->sample_packets++)
   {
      if (current_sampling_parent->sample_packets >= current_sampling_parent->max_sample_packets)
      {
         break;
      }

      _transmit_sampling_packet(AL_CONTEXT_SINGLE);
   }
   if (current_sampling_parent != current_parent)
   {
      /** Come back to current_parent */
      _virtual_associate(AL_CONTEXT current_parent, 0);
      /** Send IMCP Buffering end to parent and Setup DS_NET_IF to not buffer anymore */
      _end_buffering(AL_CONTEXT_SINGLE);
   }

_out:
   _mesh_ng_fsm_set_state(AL_CONTEXT MESH_NG_FSM_STATE_RUNNING);
}
