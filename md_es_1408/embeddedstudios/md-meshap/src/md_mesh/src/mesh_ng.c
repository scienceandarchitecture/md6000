/********************************************************************************
* MeshDynamics
* --------------
* File     : mesh_ng.c
* Comments : Mesh New Generation Implementation
* Created  : 4/5/2006
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  7  |01/30/2009| Changes for Uplink Scan Functionality           | Sriram |
* -----------------------------------------------------------------------------
* |  6  |6/13/2007 | MESH_PARENT_FLAG_RESET_AVG implemented          | Sriram |
* -----------------------------------------------------------------------------
* |  5  |6/12/2007 | Score calc based on signal for mobility         | Sriram |
* -----------------------------------------------------------------------------
* |  4  |5/1/2007  | DS MAC HASH cleared when clearing parents       | Sriram |
* -----------------------------------------------------------------------------
* |  3  |4/26/2007 | Re-enforcement logic added for Syncing          | Sriram |
* -----------------------------------------------------------------------------
* |  2  |11/14/2006| Changes for heap debug support                  | Sriram |
* -----------------------------------------------------------------------------
* |  1  |8/17/2006 | Changes for mobility optimization               | Sriram |
* -----------------------------------------------------------------------------
* |  0  |4/5/2006  | Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

#include <linux/string.h>
#include <linux/module.h>
#include "al.h"
#include "al_print_log.h"
#include "al_conf.h"
#include "access_point.h"
#include "al_impl_context.h"
#include "mesh_defs.h"
#include "mesh_table.h"
#include "mesh.h"
#include "mesh_globals.h"
#include "mesh_errors.h"
#include "mesh_on_start.h"
#include "mesh_intervals.h"
#include "imcp_snip.h"
#include "mesh_ng.h"
#include "mesh_ng_fsm.h"
#include "mesh_ng_scan.h"

#ifdef AL_USE_ENCRYPTION
#include "al_codec.h"
#endif

#define MESH_NG_USE_SEMAHPORE

#define _MESG_NG_PARENT_HASH_SIZE    17
#define _MESG_NG_DSMAC_HASH_SIZE     17

#define EXT_SCORE_FOR_SAME_PHY		8
#define EXT_SCORE_FOR_SUBSET_PHY	4

struct al_mutex parent_mlock;
int parent_muvar, parent_mupid;

/**
 * The hash function for PARENTS
 */

#ifdef  __GNUC__
#define _MESH_NG_PARENT_HASH_FUNCTION(addr1, addr2) \
   ({                                               \
      int hash;                                     \
      hash = (addr1)->bytes[0] * 1                  \
             + (addr1)->bytes[1] * 2                \
             + (addr1)->bytes[2] * 3                \
             + (addr1)->bytes[3] * 4                \
             + (addr1)->bytes[4] * 5                \
             + (addr1)->bytes[5] * 6                \
             + (addr2)->bytes[0] * 7                \
             + (addr2)->bytes[1] * 8                \
             + (addr2)->bytes[2] * 9                \
             + (addr2)->bytes[3] * 10               \
             + (addr2)->bytes[4] * 11               \
             + (addr2)->bytes[5] * 12;              \
      hash;                                         \
   }                                                \
   )
#define _MESH_NG_DSMAC_HASH_FUNCTION(addr) \
   ({                                      \
      int hash;                            \
      hash = (addr)->bytes[0] * 1          \
             + (addr)->bytes[1] * 2        \
             + (addr)->bytes[2] * 3        \
             + (addr)->bytes[3] * 4        \
             + (addr)->bytes[4] * 5        \
             + (addr)->bytes[5] * 6;       \
      hash;                                \
   }                                       \
   )
#else
static int _MESH_NG_PARENT_HASH_FUNCTION(al_net_addr_t *addr1, al_net_addr_t *addr2)
{
   int hash;                        \
   hash = (addr1)->bytes[0] * 1     \
          + (addr1)->bytes[1] * 2   \
          + (addr1)->bytes[2] * 3   \
          + (addr1)->bytes[3] * 4   \
          + (addr1)->bytes[4] * 5   \
          + (addr1)->bytes[5] * 6   \
          + (addr2)->bytes[0] * 7   \
          + (addr2)->bytes[1] * 8   \
          + (addr2)->bytes[2] * 9   \
          + (addr2)->bytes[3] * 10  \
          + (addr2)->bytes[4] * 11  \
          + (addr2)->bytes[5] * 12; \
   return hash;                     \
}


static int _MESH_NG_DSMAC_HASH_FUNCTION(al_net_addr_t *addr)
{
   int hash;                      \
   hash = (addr)->bytes[0] * 1    \
          + (addr)->bytes[1] * 2  \
          + (addr)->bytes[2] * 3  \
          + (addr)->bytes[3] * 4  \
          + (addr)->bytes[4] * 5  \
          + (addr)->bytes[5] * 6; \
   return hash;                   \
}
#endif

#define _MESH_NG_PARENT_PRIV_SIGNATURE    0x99887766

extern int eth1_ping_status;

AL_DECLARE_GLOBAL(parent_t * current_parent);
AL_DECLARE_GLOBAL(int mesh_state);
AL_DECLARE_GLOBAL(int mesh_mode);
AL_DECLARE_GLOBAL(int scan_state);
AL_DECLARE_GLOBAL(unsigned int mesh_test_flags);
AL_DECLARE_GLOBAL(parent_t * current_sampling_parent);
AL_DECLARE_GLOBAL(al_u64_t last_table_check);
AL_DECLARE_GLOBAL(int func_mode);
AL_DECLARE_GLOBAL(int mesh_hop_level);
AL_DECLARE_GLOBAL(parent_t dummy_lfr_parent);
AL_DECLARE_GLOBAL(al_atomic_t lock_dn_channel_change);
AL_DECLARE_GLOBAL(al_atomic_t lock_parent_change);
AL_DECLARE_GLOBAL(al_atomic_t up_cancel_channel_change);
AL_DECLARE_GLOBAL(al_atomic_t dn_channel_change_started);
AL_DECLARE_GLOBAL(al_atomic_t up_channel_change_started);

AL_DECLARE_GLOBAL(_parent_priv_t * _parent_hash[_MESG_NG_PARENT_HASH_SIZE]);
AL_DECLARE_GLOBAL(_parent_priv_t * _parent_list_head);
AL_DECLARE_GLOBAL(_parent_priv_t * _parent_ds_mac_hash[_MESG_NG_DSMAC_HASH_SIZE]);

/**
 *	1) The current_parent is not necessarily the _parent_sort_head.
 *	2) The sorted list contains all parents even if disabled/child/question
 */

AL_DECLARE_GLOBAL(_parent_priv_t * _parent_sort_head);

/**
 *	1) The sampling list contains all except the the current_sampling_parent
 *	2) The sampling list is initialized, after mesh_thread initializes
 *	3) New parents get added towards the top of the list
 *	4) Sampled parents get moved to the end of the list.
 *	5) The sampling list is sorted according to next_sample_time
 */

AL_DECLARE_GLOBAL(_parent_priv_t * _parent_sample_head);

#ifdef MESH_NG_USE_SEMAHPORE

AL_DECLARE_GLOBAL(int _parent_semaphore);

static AL_INLINE void _mesh_ng_lock_init(AL_CONTEXT_PARAM_DECL_SINGLE)
{
   _parent_semaphore = al_create_semaphore(AL_CONTEXT 1, 1);
}


static AL_INLINE void _mesh_ng_lock_destroy(AL_CONTEXT_PARAM_DECL_SINGLE)
{
   al_destroy_semaphore(AL_CONTEXT _parent_semaphore);
}

/*
#define _MESH_NG_LOCK_PARENTS(from_interrupt)      if (!(from_interrupt)) { al_wait_on_semaphore(AL_CONTEXT _parent_semaphore, AL_WAIT_TIMEOUT_INFINITE); }
#define _MESH_NG_UNLOCK_PARENTS(from_interrupt)    if (!(from_interrupt)) { al_release_semaphore(AL_CONTEXT _parent_semaphore, 1); }
#define _MESH_NG_LOCK_VAR_DECL()
*/

unsigned long flags;

#define _MESH_NG_LOCK_PARENTS(from_interrupt)      al_disable_interrupts(flags)
#define _MESH_NG_UNLOCK_PARENTS(from_interrupt)    al_enable_interrupts(flags)
#define _MESH_NG_LOCK_VAR_DECL()

#else

static AL_INLINE void _mesh_ng_lock_init(AL_CONTEXT_PARAM_DECL_SINGLE)
{
}


static AL_INLINE void _mesh_ng_lock_destroy(AL_CONTEXT_PARAM_DECL_SINGLE)
{
}


#define _MESH_NG_LOCK_PARENTS(from_interrupt)      al_disable_interrupts(flags)
#define _MESH_NG_UNLOCK_PARENTS(from_interrupt)    al_enable_interrupts(flags)
#define _MESH_NG_LOCK_VAR_DECL()                   unsigned int flags
#endif

void mesh_ng_init(AL_CONTEXT_PARAM_DECL_SINGLE)
{
   int i;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Enter %s<%d>\n",__func__,__LINE__);
   al_mutex_init(&parent_mlock);
   _mesh_ng_lock_init(AL_CONTEXT_SINGLE);

   for (i = 0; i < _MESG_NG_PARENT_HASH_SIZE; i++)
   {
      _parent_hash[i] = NULL;
   }

   for (i = 0; i < _MESG_NG_DSMAC_HASH_SIZE; i++)
   {
      _parent_ds_mac_hash[i] = NULL;
   }

   _parent_list_head       = NULL;
   _parent_sort_head       = NULL;
   current_parent          = NULL;
   _parent_sample_head     = NULL;
   current_sampling_parent = NULL;
   func_mode      = _FUNC_MODE_FFR;
   mesh_hop_level = 0;

   AL_ATOMIC_SET(lock_parent_change, 0);
   AL_ATOMIC_SET(lock_dn_channel_change, 0);
   AL_ATOMIC_SET(up_cancel_channel_change, 0);
   AL_ATOMIC_SET(dn_channel_change_started, 0);
   AL_ATOMIC_SET(up_channel_change_started, 0);

   last_table_check = al_get_tick_count(AL_CONTEXT_SINGLE);

   memset(&dummy_lfr_parent, 0, sizeof(parent_t));

   dummy_lfr_parent.ds_mac.length     = 6;
   dummy_lfr_parent.bssid.length      = 6;
   dummy_lfr_parent.root_bssid.length = 6;
   dummy_lfr_parent.flags             = MESH_PARENT_FLAG_LIMITED;
   dummy_lfr_parent.hpc = -1;

   mesh_ng_fsm_initialize(AL_CONTEXT_SINGLE);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Exit %s<%d>\n",__func__,__LINE__);
}


void mesh_ng_uninit(AL_CONTEXT_PARAM_DECL_SINGLE)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Enter %s<%d>\n",__func__,__LINE__);
   mesh_ng_clear_parents(AL_CONTEXT_SINGLE);
   mesh_ng_fsm_uninitialize(AL_CONTEXT_SINGLE);
   al_mutex_destroy(&parent_mlock);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Exit %s<%d>\n",__func__,__LINE__);
}


parent_t *mesh_ng_create_parent(AL_CONTEXT_PARAM_DECL al_net_addr_t *ds_mac, al_net_addr_t *bssid)
{
   _parent_priv_t *parent;

   parent = (_parent_priv_t *)al_heap_alloc(AL_CONTEXT sizeof(_parent_priv_t)AL_HEAP_DEBUG_PARAM);

   memset(parent, 0, sizeof(_parent_priv_t));

   parent->signature = _MESH_NG_PARENT_PRIV_SIGNATURE;

   memcpy(&parent->data.ds_mac, ds_mac, sizeof(al_net_addr_t));
   memcpy(&parent->data.bssid, bssid, sizeof(al_net_addr_t));

   return &parent->data;
}


void mesh_ng_destroy_parent(AL_CONTEXT_PARAM_DECL parent_t *parent)
{
   _parent_priv_t *priv_parent;

   if (parent->flags & MESH_PARENT_FLAG_ADDED)
   {
      return;
   }

   priv_parent = (_parent_priv_t *)parent;

   if (priv_parent->signature != _MESH_NG_PARENT_PRIV_SIGNATURE)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"MESH_AP:INFO: Enter %s<%d> parent not destroy for DS_MAC:"MACSTR" bcz "
	  "signature mismatch %d(%d)\n",
	  __func__,__LINE__, MAC2STR(parent->ds_mac.bytes), priv_parent->signature, _MESH_NG_PARENT_PRIV_SIGNATURE);
      return;
   }

   al_heap_free(AL_CONTEXT priv_parent);
}


static AL_INLINE void _add_to_dsmac_hash(AL_CONTEXT_PARAM_DECL _parent_priv_t *priv_parent)
{
   int            hash;
   _parent_priv_t *head;
   _parent_priv_t *temp;
   _parent_priv_t *prev;

   hash = _MESH_NG_DSMAC_HASH_FUNCTION(&priv_parent->data.ds_mac);
   priv_parent->ds_mac_bucket   = (hash % _MESG_NG_DSMAC_HASH_SIZE);
   priv_parent->prev_bssid      = NULL;
   priv_parent->next_bssid      = NULL;
   priv_parent->prev_dsmac_hash = NULL;
   priv_parent->next_dsmac_hash = NULL;

   /** First loop through bucket and see if DS_MAC is already present */

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Enter %s<%d>\n",__func__,__LINE__);
   head = _parent_ds_mac_hash[priv_parent->ds_mac_bucket];

   while (head != NULL)
   {
      if (AL_NET_ADDR_EQUAL(&head->data.ds_mac, &priv_parent->data.ds_mac))
      {
         break;
      }
      head = head->next_dsmac_hash;
   }

   if (head != NULL)
   {
      /** Now loop through bucket and see if BSSID is already present */
      temp = head;
      prev = NULL;
      while (temp != NULL)
      {
         if (AL_NET_ADDR_EQUAL(&temp->data.bssid, &priv_parent->data.bssid))
         {
            break;
         }
         prev = temp;
         temp = temp->next_bssid;
      }
      /** Only if BSSID is not present, add the entry. Always add to tail of list */
      if (temp == NULL)
      {
         prev->next_bssid        = priv_parent;
         priv_parent->prev_bssid = prev;
      }
   }
   else
   {
      /** Add a new hash entry and hence also a new BSSID entry */
      priv_parent->next_dsmac_hash = _parent_ds_mac_hash[priv_parent->ds_mac_bucket];
      if (_parent_ds_mac_hash[priv_parent->ds_mac_bucket] != NULL)
      {
         _parent_ds_mac_hash[priv_parent->ds_mac_bucket]->prev_dsmac_hash = priv_parent;
      }
      _parent_ds_mac_hash[priv_parent->ds_mac_bucket] = priv_parent;
   }
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Exit %s<%d>\n",__func__,__LINE__);
}


static AL_INLINE void _remove_from_dsmac_hash(AL_CONTEXT_PARAM_DECL _parent_priv_t *priv_parent)
{
   _parent_priv_t *hash;
   _parent_priv_t *bssid;
   _parent_priv_t *temp;

   /**
    * Assumes ds_mac_bucket field has a valid value.
    * Removal only removes the BSSID entry from hash. If the BSSID is the only entry
    * for the ds_mac, then the hash entry is also removed
    */
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Enter %s<%d>\n",__func__,__LINE__);

   hash = _parent_ds_mac_hash[priv_parent->ds_mac_bucket];

   while (hash != NULL)
   {
      if (AL_NET_ADDR_EQUAL(&hash->data.ds_mac, &priv_parent->data.ds_mac))
      {
         break;
      }
      hash = hash->next_dsmac_hash;
   }

   if (hash == NULL)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP:ERROR: %s<%d> HASH is NULL\n",__func__,__LINE__);
      return;
   }

   bssid = hash;

   while (bssid != NULL)
   {
      if (AL_NET_ADDR_EQUAL(&bssid->data.bssid, &priv_parent->data.bssid))
      {
         break;
      }
      bssid = bssid->next_bssid;
   }

   if (bssid == NULL)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP:ERROR: %s<%d> bssid is NULL\n",__func__,__LINE__);
      return;
   }

   if (bssid->prev_bssid != NULL)
   {
      bssid->prev_bssid->next_bssid = bssid->next_bssid;
   }
   if (bssid->next_bssid != NULL)
   {
      bssid->next_bssid->prev_bssid = bssid->prev_bssid;
   }

   if (bssid == hash)
   {
      if (hash->next_bssid == NULL)            /** The only BSSID for the DS_MAC */
      {
         if (hash->prev_dsmac_hash != NULL)
         {
            hash->prev_dsmac_hash->next_dsmac_hash = hash->next_dsmac_hash;
         }
         if (hash->next_dsmac_hash != NULL)
         {
            hash->next_dsmac_hash->prev_dsmac_hash = hash->prev_dsmac_hash;
         }
         if (hash == _parent_ds_mac_hash[priv_parent->ds_mac_bucket])
         {
            _parent_ds_mac_hash[priv_parent->ds_mac_bucket] = NULL;
         }
      }
      else
      {
         /** We set bssid->next_bssid to be in the hash */
         temp = bssid->next_bssid;
         temp->prev_dsmac_hash = hash->prev_dsmac_hash;
         temp->next_dsmac_hash = hash->next_dsmac_hash;
         if (temp->prev_dsmac_hash != NULL)
         {
            temp->prev_dsmac_hash->next_dsmac_hash = temp;
         }
         if (temp->next_dsmac_hash != NULL)
         {
            temp->next_dsmac_hash->prev_dsmac_hash = temp;
         }
         _parent_ds_mac_hash[priv_parent->ds_mac_bucket] = temp;
      }
   }
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Exit %s<%d>\n",__func__,__LINE__);
}


static void _add_sampling_parent(AL_CONTEXT_PARAM_DECL parent_t *parent)
{
   _parent_priv_t *priv_parent;
   _parent_priv_t *temp;
   _parent_priv_t *prev;

   /**
    * Assumes t_next member has been updated
    */
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Enter %s<%d>\n",__func__,__LINE__);

   priv_parent = (_parent_priv_t *)parent;

   priv_parent->data.next_sample_time   = al_get_tick_count(AL_CONTEXT_SINGLE) + ((priv_parent->data.t_next / 1000) * HZ);
   priv_parent->data.sample_packets     = 0;
   priv_parent->data.max_sample_packets = 0;


   temp = _parent_sample_head;
   priv_parent->prev_sample = NULL;
   priv_parent->next_sample = NULL;
   prev = NULL;

   while (temp != NULL)
   {
      if (al_timer_before(priv_parent->data.next_sample_time, temp->data.next_sample_time))
      {
         priv_parent->prev_sample = temp->prev_sample;
         priv_parent->next_sample = temp;
         temp->prev_sample        = priv_parent;

         if (priv_parent->prev_sample != NULL)
         {
            priv_parent->prev_sample->next_sample = priv_parent;
         }

         if (temp == _parent_sample_head)
         {
            _parent_sample_head = priv_parent;
         }

         break;
      }

      prev = temp;
      temp = temp->next_sample;
   }

   if (temp == NULL)
   {
      if (prev == NULL)
      {
         _parent_sample_head = priv_parent;
      }
      else
      {
         priv_parent->prev_sample = prev;
         prev->next_sample        = priv_parent;
      }
   }
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Exit %s<%d>\n",__func__,__LINE__);
}


void mesh_ng_add_parent(AL_CONTEXT_PARAM_DECL parent_t *parent)
{
   _parent_priv_t *priv_parent;
   int            hash_value;

   _MESH_NG_LOCK_VAR_DECL();

   priv_parent = (_parent_priv_t *)parent;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Enter %s<%d>\n",__func__,__LINE__);

   if (priv_parent->signature != _MESH_NG_PARENT_PRIV_SIGNATURE)
   {
		al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP:ERROR: %s<%d> signature mismatch: %u(%u)\n", __func__, __LINE__,
		priv_parent->signature, _MESH_NG_PARENT_PRIV_SIGNATURE);
      return;
   }

   hash_value          = _MESH_NG_PARENT_HASH_FUNCTION(&parent->ds_mac, &parent->bssid);
   priv_parent->bucket = hash_value % _MESG_NG_PARENT_HASH_SIZE;

    al_mutex_lock(parent_mlock, parent_muvar, parent_mupid);

   parent->flags |= MESH_PARENT_FLAG_ADDED;

   if (NULL != _get_parent(AL_CONTEXT & parent->ds_mac, & parent->bssid)) {
    	al_mutex_unlock(parent_mlock, parent_muvar, parent_mupid);
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP:ERROR: %s<%d> Parent ENTRY is already present for "
	  "DS_MAC:"MACSTR" parent bssid:"MACSTR"\n",
	  __func__, __LINE__, MAC2STR(parent->ds_mac.bytes), MAC2STR(parent->bssid.bytes));
      return;
   }

   /** Add to Hash table */

   priv_parent->prev_hash = NULL;
   priv_parent->next_hash = _parent_hash[priv_parent->bucket];
   if (_parent_hash[priv_parent->bucket] != NULL)
   {
      _parent_hash[priv_parent->bucket]->prev_hash = priv_parent;
   }
   _parent_hash[priv_parent->bucket] = priv_parent;

   /** Add to list */

   priv_parent->prev_list = NULL;
   priv_parent->next_list = _parent_list_head;
   if (_parent_list_head != NULL)
   {
      _parent_list_head->prev_list = priv_parent;
   }
   _parent_list_head = priv_parent;

   /**
    * Add into sampling list
    */

   priv_parent->data.t_next = mesh_config->sampling_t_next_min;

   _add_sampling_parent(AL_CONTEXT & priv_parent->data);

   /**
    * Add to DS_MAC based hash
    */

   _add_to_dsmac_hash(AL_CONTEXT priv_parent);

    al_mutex_unlock(parent_mlock, parent_muvar, parent_mupid);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Exit %s<%d>\n",__func__,__LINE__);
}

//DOUBLE_ENTRY  
_parent_priv_t *_get_parent(AL_CONTEXT_PARAM_DECL al_net_addr_t *ds_mac, al_net_addr_t *bssid)
{
   int            hash_value;
   int            bucket;
   _parent_priv_t *priv_parent;

   hash_value = _MESH_NG_PARENT_HASH_FUNCTION(ds_mac, bssid);
   bucket     = hash_value % _MESG_NG_PARENT_HASH_SIZE;

   priv_parent = _parent_hash[bucket];

   while (priv_parent != NULL)
   {
      if (AL_NET_ADDR_EQUAL(ds_mac, &priv_parent->data.ds_mac) &&
          AL_NET_ADDR_EQUAL(bssid, &priv_parent->data.bssid))
      {
         break;
      }
      priv_parent = priv_parent->next_hash;
   }

   if(!priv_parent)
   al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP:ERROR: %s<%d> Parent Not found for DS_MAC:"MACSTR"\n",
   __func__,__LINE__, MAC2STR(ds_mac->bytes));
   return priv_parent;
}


parent_t *mesh_ng_get_parent(AL_CONTEXT_PARAM_DECL al_net_addr_t *ds_mac, al_net_addr_t *bssid, int from_interrupt)
{
   _parent_priv_t *priv_parent;

   _MESH_NG_LOCK_VAR_DECL();

   al_mutex_lock(parent_mlock, parent_muvar, parent_mupid);


   priv_parent = _get_parent(AL_CONTEXT ds_mac, bssid);

    al_mutex_unlock(parent_mlock, parent_muvar, parent_mupid);

   if (priv_parent == NULL)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP:ERROR: %s<%d> parent not found for DS_MAC:"MACSTR"\n",__func__,
	  __LINE__, MAC2STR(ds_mac->bytes));
      return NULL;
   }

   return &priv_parent->data;
}


#define _COMPARE_VAL_RET(a, b)    ((a) > (b) ? 1 : -1)

static AL_INLINE int _compare_nodes(_parent_priv_t *a, _parent_priv_t *b)
{
   if (a->data.flags & MESH_PARENT_FLAG_PREFERRED)
   {
      return 1;
   }
   if (b->data.flags & MESH_PARENT_FLAG_PREFERRED)
   {
      return -1;
   }

   if (_DISJOINT_ADHOC_MODE)
   {
      if ((a->data.flags & MESH_PARENT_FLAG_LIMITED) && (b->data.flags & MESH_PARENT_FLAG_LIMITED))
      {
         goto _score_check;
      }
      if (a->data.flags & MESH_PARENT_FLAG_LIMITED)
      {
         return -1;
      }
      if (b->data.flags & MESH_PARENT_FLAG_LIMITED)
      {
         return 1;
      }
   }
   else
   {
      if ((a->data.flags & MESH_PARENT_FLAG_MOBILE) && (b->data.flags & MESH_PARENT_FLAG_MOBILE))
      {
         goto _score_check;
      }
      if (a->data.flags & MESH_PARENT_FLAG_MOBILE)
      {
         return -1;
      }
      if (b->data.flags & MESH_PARENT_FLAG_MOBILE)
      {
         return 1;
      }
   }

_score_check:

   if (!_IS_MOBILE && !_IS_UPLINK_SCAN_ENABLED)
   {
      if (a->data.score == b->data.score)
      {
         if (a == (_parent_priv_t *)current_parent)
         {
            return 1;
         }
         if (b == (_parent_priv_t *)current_parent)
         {
            return -1;
         }
         if (a->data.tree_bit_rate == b->data.tree_bit_rate)
         {
            if (a->data.hpc == b->data.hpc)
            {
               if (a->data.direct_bit_rate == b->data.direct_bit_rate)
               {
                  if (a->data.signal == b->data.signal)
                  {
                     return 0;
                  }
                  else
                  {
                     return _COMPARE_VAL_RET(a->data.signal, b->data.signal);
                  }
               }
               else
               {
                  return _COMPARE_VAL_RET(a->data.direct_bit_rate, b->data.direct_bit_rate);
               }
            }
            else
            {
               return _COMPARE_VAL_RET(b->data.hpc, a->data.hpc);
            }
         }
         else
         {
            return _COMPARE_VAL_RET(a->data.tree_bit_rate, b->data.tree_bit_rate);
         }
      }
      else
      {
         return _COMPARE_VAL_RET(a->data.score, b->data.score);
      }
   }
   else
   {
      int          diff;
      al_u64_t     tick;
      al_u64_t     timeout_a;
      al_u64_t     timeout_b;
      unsigned int thresh;

      tick      = al_get_tick_count(AL_CONTEXT_SINGLE);
      thresh    = ((mesh_config->scanner_dwell_interval * HZ)/ 1000) * mesh_config->scan_count;
      timeout_a = a->data.last_seen_time + thresh;
      timeout_b = b->data.last_seen_time + thresh;

      if (al_timer_after_eq(tick, timeout_a) && al_timer_before(tick, timeout_b))
      {
         return -1;
      }
      else if (al_timer_before(tick, timeout_a) && al_timer_after_eq(tick, timeout_b))
      {
         return 1;
      }

      diff = (int)a->data.signal - (int)b->data.signal;

      if (diff >= (int)mesh_config->evaluation_damping_factor)
      {
         return 1;
      }
      else if (diff <= ((int)mesh_config->evaluation_damping_factor * -1))
      {
         return -1;
      }
      else
      {
         /**
          * if the signal difference between the two is between
          * -mesh_config->evaluation_damping_factor to +mesh_config->evaluation_damping_factor
          * we consider them equal and proceed as below.
          * Moreover if one of them is the current parent and the other is not, the current
          * parent wins
          */

         if (a == (_parent_priv_t *)current_parent)
         {
            return 1;
         }
         if (b == (_parent_priv_t *)current_parent)
         {
            return -1;
         }

         if (a->data.direct_bit_rate == b->data.direct_bit_rate)
         {
            if (a->data.hpc == b->data.hpc)
            {
               return 0;
            }
            else
            {
               return _COMPARE_VAL_RET(b->data.hpc, a->data.hpc);
            }
         }
         else
         {
            return _COMPARE_VAL_RET(a->data.direct_bit_rate, b->data.direct_bit_rate);
         }
      }
   }
}

int extra_score_required(parent_t pdata)
{
	if(pdata.phy_sub_type == dummy_lfr_parent.phy_sub_type) {
		return EXT_SCORE_FOR_SAME_PHY;
	}else if(pdata.phy_support & dummy_lfr_parent.phy_support) {
		return EXT_SCORE_FOR_SUBSET_PHY;
	}else {
		return 0;
	}
}

static AL_INLINE void _calculate_score(_parent_priv_t *node)
{
	int score;
    score = node->data.signal;
    node->data.score = score;
    return ;
#if 0 // CREE_SIGNAL
	if (_IS_MOBILE || _IS_UPLINK_SCAN_ENABLED)
	{
		//score	= 255 - node->data.signal;
		score = node->data.signal;
	}
	else
	{
		if (node->data.tree_bit_rate < node->data.direct_bit_rate)
		{
			score = 255 - node->data.tree_bit_rate;
		}
		else
		{
			score = 255 - node->data.direct_bit_rate;
		}

		if (node->data.disconnect_count > mesh_config->disconnect_count_max)
		{
			score += mesh_config->disconnect_count_max * mesh_config->disconnect_multiplier;
		}
		else
		{
			score += node->data.disconnect_count * mesh_config->disconnect_multiplier;
		}
	}

   score += extra_score_required(node->data);

   if (score > 255)
   {
      score = 255;
   }
   if (score < 0)
   {
      score = 0;
   }

   node->data.score = score;
#endif
}

void mesh_ng_evaluate_list(AL_CONTEXT_PARAM_DECL_SINGLE)
{
   _parent_priv_t *head;
   _parent_priv_t *node;
   _parent_priv_t *temp;
   _parent_priv_t *prev;

   _MESH_NG_LOCK_VAR_DECL();

  al_mutex_lock(parent_mlock, parent_muvar, parent_mupid);

   head = NULL;
   node = _parent_list_head;

   while (node != NULL)
   {
      _calculate_score(node);

      if (head == NULL)
      {
         head            = node;
         head->next_sort = NULL;
         head->prev_sort = NULL;
      }
      else
      {
         temp = head;
         prev = NULL;
         while (temp != NULL)
         {
            if (_compare_nodes(node, temp) == 1)
            {
               node->prev_sort = temp->prev_sort;
               node->next_sort = temp;
               temp->prev_sort = node;
               if (prev == NULL)
               {
                  head = node;
               }
               else
               {
                  prev->next_sort = node;
               }
               break;
            }
            prev = temp;
            temp = temp->next_sort;
         }
         if (temp == NULL)
         {
            node->prev_sort = prev;
            prev->next_sort = node;
            node->next_sort = NULL;
         }
      }

      node = node->next_list;
   }

   _parent_sort_head = head;

   al_mutex_unlock(parent_mlock, parent_muvar, parent_mupid);
}


void mesh_ng_clear_parents(AL_CONTEXT_PARAM_DECL_SINGLE)
{
   _parent_priv_t *temp;
   int            i;

   _MESH_NG_LOCK_VAR_DECL();

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Enter %s<%d>\n",__func__,__LINE__);
   al_mutex_lock(parent_mlock, parent_muvar, parent_mupid);
   temp = _parent_list_head;

   while (temp != NULL)
   {
      _parent_list_head = temp->next_list;
      al_heap_free(AL_CONTEXT temp);
      temp = _parent_list_head;
   }

   _parent_list_head       = NULL;
   _parent_sort_head       = NULL;
   _parent_sample_head     = NULL;
   current_parent          = NULL;
   current_sampling_parent = NULL;

   for (i = 0; i < _MESG_NG_PARENT_HASH_SIZE; i++)
   {
      _parent_hash[i] = NULL;
   }

   for (i = 0; i < _MESG_NG_DSMAC_HASH_SIZE; i++)
   {
      _parent_ds_mac_hash[i] = NULL;
   }

   al_mutex_unlock(parent_mlock, parent_muvar, parent_mupid);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Exit %s<%d>\n",__func__,__LINE__);
}

int mesh_ng_enumerate_parents(AL_CONTEXT_PARAM_DECL int type, void *enum_param, int (*enum_func)(AL_CONTEXT_PARAM_DECL void *enum_param, parent_t *parent), int lock)
{
	_parent_priv_t *temp;
	int            ret;

	if (!_parent_sort_head)
		return -1;
	if (!lock) {
		al_mutex_lock(parent_mlock, parent_muvar, parent_mupid);
	}
	temp = _parent_sort_head;
	for ( ; temp != NULL; temp = temp->next_sort)
	{
		if (type == MESH_NG_ENUMERATE_PARENTS_TYPE_RELEVANT)
		{
			if (temp->data.flags & MESH_PARENT_FLAG_CHILD ||
					temp->data.flags & MESH_PARENT_FLAG_DISABLED ||
					temp->data.flags & MESH_PARENT_FLAG_QUESTION)
			{
				continue;
			}
		}

		ret = enum_func(AL_CONTEXT enum_param, &temp->data);
		if (ret != 0)
		{
			break;
		}
	}
	if (!lock) {
		al_mutex_unlock(parent_mlock, parent_muvar, parent_mupid);
	}
	return 0;
}

parent_t *mesh_ng_get_best_parent(AL_CONTEXT_PARAM_DECL_SINGLE)
{
   _parent_priv_t *temp;

   _MESH_NG_LOCK_VAR_DECL();

   al_mutex_lock(parent_mlock, parent_muvar, parent_mupid);

   temp = _parent_sort_head;

   while (temp != NULL)
   {
      if (temp->data.flags & MESH_PARENT_FLAG_CHILD ||
          temp->data.flags & MESH_PARENT_FLAG_DISABLED ||
          temp->data.flags & MESH_PARENT_FLAG_QUESTION)
      {
         temp = temp->next_sort;
      }
      else
      {
         break;
      }
   }

   al_mutex_unlock(parent_mlock, parent_muvar, parent_mupid);
   if (temp != NULL)
   {
      return &temp->data;
   }
   return NULL;
}

unsigned char get_phy_support(unsigned char phy_sub_type)
{
	unsigned char phy_support = 0;

	/*1st bit represent 802.11A  phy supports
	 *2nd bit represent 802.11B  phy supports
	 *3rd bit represent 802.11G  phy supports
	 *4th bit represent 802.11N  phy supports
	 *5th bit represent 802.11AC phy supports
	 */
	switch(phy_sub_type) {
		case AL_CONF_IF_PHY_SUB_TYPE_802_11_A:
			 phy_support = 1;
			 break;
		case AL_CONF_IF_PHY_SUB_TYPE_802_11_B:
			 phy_support = 2;
			 break;
		case AL_CONF_IF_PHY_SUB_TYPE_802_11_G:
			 phy_support = 4;
			 break;
        case AL_CONF_IF_PHY_SUB_TYPE_802_11_N_2_4GHZ:
        case AL_CONF_IF_PHY_SUB_TYPE_802_11_N_5GHZ:
			 phy_support = 8;
			 break;
		case AL_CONF_IF_PHY_SUB_TYPE_802_11_AC:
			 phy_support = 16;
			 break;
		case AL_CONF_IF_PHY_SUB_TYPE_802_11_AN:
			 phy_support = 1 | 8;
			 break;
		case AL_CONF_IF_PHY_SUB_TYPE_802_11_BG:
			 phy_support = 2 | 4;
			 break;
		case AL_CONF_IF_PHY_SUB_TYPE_802_11_BGN:
			 phy_support = 2 | 4 | 8;
			 break;
		case AL_CONF_IF_PHY_SUB_TYPE_802_11_ANAC:
			 phy_support = 1 | 8 | 16;
			 break;
		default:
			 break;
	}
	return phy_support;
}

int can_connect(parent_t pdata)
{
	/*If both parents and DS interface phy_supports are compatiable
	 *then retun 1 else return 0*/
	if(pdata.phy_support & dummy_lfr_parent.phy_support) {
		return 1;
	}else {
		return 0;
	}
}

parent_t *mesh_ng_get_best_parent_adhoc(AL_CONTEXT_PARAM_DECL_SINGLE)
{
   int            flags = 0, found_flag = 0;
   int            j;
   int            ifcnt;
   _parent_priv_t *temp;
   al_net_if_t    *net_if;
   int            ret = 0;
   al_net_addr_t  *self_root_bssid = NULL;

   _MESH_NG_LOCK_VAR_DECL();

   al_mutex_lock(parent_mlock, parent_muvar, parent_mupid);

   temp = _parent_sort_head;

   while (temp != NULL)
   {
      if (temp->data.flags & MESH_PARENT_FLAG_CHILD ||
          temp->data.flags & MESH_PARENT_FLAG_DISABLED ||
          temp->data.flags & MESH_PARENT_FLAG_QUESTION ||
          temp->data.flags & MESH_PARENT_FLAG_AD_DISABLE)
      {
         goto _next;
      }

      switch (func_mode)
      {
         case _FUNC_MODE_LFR:
            self_root_bssid = _DS_MAC;
            break;
         case _FUNC_MODE_FFN:
         case _FUNC_MODE_LFN:
            if (!current_parent) {
               self_root_bssid = _DS_MAC;
            } else {
               self_root_bssid = &current_parent->root_bssid;
            }
            break;
      }



      if (_IS_LIMITED)
      {
         if (!(temp->data.flags & MESH_PARENT_FLAG_LIMITED) && can_connect(temp->data))
         {
            /* If the Parent is not operating in Limited mode, then first get connected to that parent */
            break;
         }
         if (!AL_NET_ADDR_EQUAL(&temp->data.root_bssid, self_root_bssid))
         {
             ret = mesh_ng_mac_address_compare((&temp->data.root_bssid), self_root_bssid);
             if (ret < 0) {
                 temp->data.skip_bssid_count++;
                 if (temp->data.skip_bssid_count >= 10) {
                     goto skip_bssid_check;
                 }
                 goto _next;
             }
         }
skip_bssid_check:
         if ((func_mode == _FUNC_MODE_LFR))
         {
               if (MESH_NG_PARENT_IS_LFR(&temp->data))
			   {
				   if(can_connect(temp->data)) {
					   ret = mesh_ng_mac_address_compare(&temp->data.ds_mac, _DS_MAC);
					   if (ret <= 0) {
						   goto _next;
					   }
					   else {
						   goto out;
					   }
				   }
			   }
            else
            {
               break;
            }
		 } else {
			break;
		 }
	  } 
	  else
	  {
		if (temp->data.flags & MESH_PARENT_FLAG_LIMITED)
		{
			goto _next;
		}
		else
		{
		   break;
		}
	  }
_next:
      temp = temp->next_sort;
   }

out:
  al_mutex_unlock(parent_mlock, parent_muvar, parent_mupid);

   if (temp != NULL)
   {
      return &temp->data;
   }

   return NULL;
}


void mesh_ng_set_parent_as_child(AL_CONTEXT_PARAM_DECL al_net_addr_t *ds_mac, int child, int from_interrupt)
{
   _parent_priv_t *priv_parent;
   int            hash;
   int            bucket;

   _MESH_NG_LOCK_VAR_DECL();

   hash   = _MESH_NG_DSMAC_HASH_FUNCTION(ds_mac);
   bucket = (hash % _MESG_NG_DSMAC_HASH_SIZE);

   al_mutex_lock(parent_mlock, parent_muvar, parent_mupid);

   priv_parent = _parent_ds_mac_hash[bucket];

   while (priv_parent != NULL)
   {
      if (AL_NET_ADDR_EQUAL(ds_mac, &priv_parent->data.ds_mac))
      {
         break;
      }
      priv_parent = priv_parent->next_dsmac_hash;
   }

   while (priv_parent != NULL)
   {
      if (child)
      {
         priv_parent->data.flags |= MESH_PARENT_FLAG_CHILD;
         al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"MESH_AP:INFO: %s<%d> set child flag for parent of DS_MAC:"MACSTR"\n",
		 __func__,__LINE__, MAC2STR(priv_parent->data.ds_mac.bytes));
      }
      else
      {
         priv_parent->data.flags &= ~MESH_PARENT_FLAG_CHILD;
         al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"MESH_AP:INFO: %s<%d> reset child flag for parent of DS_MAC:"MACSTR"\n",
		 __func__,__LINE__, MAC2STR(priv_parent->data.ds_mac.bytes));
      }
      priv_parent = priv_parent->next_bssid;
   }

   al_mutex_unlock(parent_mlock, parent_muvar, parent_mupid);
}


void mesh_ng_remove_sampling_head(AL_CONTEXT_PARAM_DECL_SINGLE)
{
   _parent_priv_t *parent;
   al_u64_t       tick;

   _MESH_NG_LOCK_VAR_DECL();

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Enter %s<%d>\n",__func__,__LINE__);
   al_mutex_lock(parent_mlock, parent_muvar, parent_mupid);


   parent = _parent_sample_head;

   if (parent != NULL)
   {
      tick = al_get_tick_count(AL_CONTEXT_SINGLE);

      if (al_timer_after_eq(tick, parent->data.next_sample_time))
      {
         _parent_sample_head = _parent_sample_head->next_sample;
         if (_parent_sample_head != NULL)
         {
            _parent_sample_head->prev_sample = NULL;
         }
         parent->prev_sample = NULL;
         parent->next_sample = NULL;
      }
      else
      {
         parent = NULL;
      }
   }

    al_mutex_unlock(parent_mlock, parent_muvar, parent_mupid);

   if (parent == NULL)
   {
      current_sampling_parent = NULL;
      return;
   }

   current_sampling_parent = &parent->data;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Exit %s<%d>\n",__func__,__LINE__);
}


void mesh_ng_add_sampling_parent(AL_CONTEXT_PARAM_DECL_SINGLE)
{
   _MESH_NG_LOCK_VAR_DECL();

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Enter %s<%d>\n",__func__,__LINE__);
  al_mutex_lock(parent_mlock, parent_muvar, parent_mupid);


   if (current_sampling_parent != NULL)
   {
      _add_sampling_parent(AL_CONTEXT current_sampling_parent);
   }

  al_mutex_unlock(parent_mlock, parent_muvar, parent_mupid);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Exit %s<%d>\n",__func__,__LINE__);
}


void mesh_ng_mark_parent_for_sampling(AL_CONTEXT_PARAM_DECL parent_t *parent)
{
   _parent_priv_t *priv_parent;
   _parent_priv_t *prev;

   _MESH_NG_LOCK_VAR_DECL();

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Enter %s<%d>\n",__func__,__LINE__);
   al_mutex_lock(parent_mlock, parent_muvar, parent_mupid);

   priv_parent = _parent_sample_head;
   prev        = NULL;

   while (priv_parent != NULL)
   {
      if (priv_parent == (_parent_priv_t *)parent)
      {
         break;
      }
      prev        = priv_parent;
      priv_parent = priv_parent->next_sample;
   }

   if (priv_parent != NULL)
   {
      if ((priv_parent != _parent_sample_head) && (prev != NULL))
      {
         /** Remove from sampling list */

         if (priv_parent->next_sample != NULL)
         {
            priv_parent->next_sample->prev_sample = prev;
         }
         prev->next_sample = priv_parent->next_sample;

         /** Add back as head */
         priv_parent->prev_sample         = NULL;
         priv_parent->next_sample         = _parent_sample_head;
         _parent_sample_head->prev_sample = priv_parent;
         _parent_sample_head = priv_parent;
      }
      _parent_sample_head->data.next_sample_time = al_get_tick_count(AL_CONTEXT_SINGLE);
   }

   al_mutex_unlock(parent_mlock, parent_muvar, parent_mupid);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Exit %s<%d>\n",__func__,__LINE__);
}


static AL_INLINE void _destroy_parent(AL_CONTEXT_PARAM_DECL _parent_priv_t *parent)
{
   /** Remove from Hash table */
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Enter %s<%d>\n",__func__,__LINE__);
   if (parent->prev_hash != NULL)
   {
      parent->prev_hash->next_hash = parent->next_hash;
   }
   if (parent->next_hash != NULL)
   {
      parent->next_hash->prev_hash = parent->prev_hash;
   }
   if (parent == _parent_hash[parent->bucket])
   {
      _parent_hash[parent->bucket] = parent->next_hash;
   }

   /** Remove from List */
   if (parent->prev_list != NULL)
   {
      parent->prev_list->next_list = parent->next_list;
   }
   if (parent->next_list != NULL)
   {
      parent->next_list->prev_list = parent->prev_list;
   }
   if (parent == _parent_list_head)
   {
      _parent_list_head = parent->next_list;
   }

   /** Remove from Sort list */
   if (parent->prev_sort != NULL)
   {
      parent->prev_sort->next_sort = parent->next_sort;
   }
   if (parent->next_sort != NULL)
   {
      parent->next_sort->prev_sort = parent->prev_sort;
   }
   if (parent == _parent_sort_head)
   {
      _parent_sort_head = parent->next_sort;
   }

   /** Remove from Sampling list */
   if (parent->prev_sample != NULL)
   {
      parent->prev_sample->next_sample = parent->next_sample;
   }
   if (parent->next_sample != NULL)
   {
      parent->next_sample->prev_sample = parent->prev_sample;
   }
   if (parent == _parent_sample_head)
   {
      _parent_sample_head = parent->next_sample;
   }
   if (current_sampling_parent == &parent->data)
   {
      current_sampling_parent = NULL;
   }

   /** Remove from ds_mac hash and bssid list */

   _remove_from_dsmac_hash(AL_CONTEXT parent);

   al_heap_free(AL_CONTEXT parent);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Exit %s<%d> destroy parent DS_MAC:"MACSTR"\n",__func__,__LINE__,
   parent->data.ds_mac.bytes);
}


void mesh_ng_destroy_disabled_parents(AL_CONTEXT_PARAM_DECL_SINGLE)
{
   _parent_priv_t *temp;
   _parent_priv_t *next;

   _MESH_NG_LOCK_VAR_DECL();
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Enter %s<%d>\n",__func__,__LINE__);

   al_mutex_lock(parent_mlock, parent_muvar, parent_mupid);
   temp = _parent_list_head;

   if (temp == NULL)
   {
       al_mutex_unlock(parent_mlock, parent_muvar, parent_mupid);
      return;
   }

   for ( ; temp != NULL; )
   {
      next = temp->next_list;

      if (temp->data.flags & MESH_PARENT_FLAG_DISABLED)
      {
         al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,
                      "MESH_AP:INFO: %s<%d> Destroying Known AP "AL_NET_ADDR_STR" from list",
                      __func__, __LINE__, AL_NET_ADDR_TO_STR(&temp->data.bssid));
         _destroy_parent(AL_CONTEXT temp);
      }

      temp = next;
   }

   al_mutex_unlock(parent_mlock, parent_muvar, parent_mupid);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Exit %s<%d>\n",__func__,__LINE__);
}


int mesh_ng_enumerate_sampling_list(AL_CONTEXT_PARAM_DECL void *enum_param, int (*enum_func)(AL_CONTEXT_PARAM_DECL void *enum_param, parent_t *parent))
{
   _parent_priv_t *temp;
   int            ret;

   _MESH_NG_LOCK_VAR_DECL();

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Enter %s<%d>\n",__func__,__LINE__);
   al_mutex_lock(parent_mlock, parent_muvar, parent_mupid);


   temp = _parent_sample_head;

   if (temp == NULL)
   {
    al_mutex_unlock(parent_mlock, parent_muvar, parent_mupid);
      return -1;
   }

   for ( ; temp != NULL; temp = temp->next_sample)
   {
      ret = enum_func(AL_CONTEXT enum_param, &temp->data);

      if (ret != 0)
      {
         break;
      }
   }
    al_mutex_unlock(parent_mlock, parent_muvar, parent_mupid);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Exit %s<%d>\n",__func__,__LINE__);
   return 0;
}


int mesh_ng_enumerate_bssid_list(AL_CONTEXT_PARAM_DECL al_net_addr_t *ds_mac, void *enum_param, int (*enum_func)(AL_CONTEXT_PARAM_DECL void *enum_param, parent_t *parent))
{
   _parent_priv_t *priv_parent;
   int            hash;
   int            bucket;
   int            ret;

   _MESH_NG_LOCK_VAR_DECL();

   hash   = _MESH_NG_DSMAC_HASH_FUNCTION(ds_mac);
   bucket = (hash % _MESG_NG_DSMAC_HASH_SIZE);

   al_mutex_lock(parent_mlock, parent_muvar, parent_mupid);

   priv_parent = _parent_ds_mac_hash[bucket];

   while (priv_parent != NULL)
   {
      if (AL_NET_ADDR_EQUAL(ds_mac, &priv_parent->data.ds_mac))
      {
         break;
      }
      priv_parent = priv_parent->next_dsmac_hash;
   }

   if (priv_parent == NULL)
   {
    al_mutex_unlock(parent_mlock, parent_muvar, parent_mupid);
      return -1;
   }

   while (priv_parent != NULL)
   {
      ret = enum_func(AL_CONTEXT enum_param, &priv_parent->data);
      if (ret != 0)
      {
         break;
      }
      priv_parent = priv_parent->next_bssid;
   }

    al_mutex_unlock(parent_mlock, parent_muvar, parent_mupid);
   return 0;
}


void mesh_ng_set_parent_bssid(AL_CONTEXT_PARAM_DECL parent_t *parent, al_net_addr_t *bssid)
{
   _parent_priv_t *priv_parent;
   int            hash;

   _MESH_NG_LOCK_VAR_DECL();

   memcpy(&parent->bssid, bssid, sizeof(al_net_addr_t));

   priv_parent = (_parent_priv_t *)parent;
   hash        = _MESH_NG_PARENT_HASH_FUNCTION(&parent->ds_mac, bssid);

   al_mutex_lock(parent_mlock, parent_muvar, parent_mupid);


   /** Remove from Hash table */
   if (priv_parent->prev_hash != NULL)
   {
      priv_parent->prev_hash->next_hash = priv_parent->next_hash;
   }
   if (priv_parent->next_hash != NULL)
   {
      priv_parent->next_hash->prev_hash = priv_parent->prev_hash;
   }
   if (priv_parent == _parent_hash[priv_parent->bucket])
   {
      _parent_hash[priv_parent->bucket] = priv_parent->next_hash;
   }

   /** Add back using new hash value */
   priv_parent->bucket    = (hash % _MESG_NG_PARENT_HASH_SIZE);
   priv_parent->prev_hash = NULL;
   priv_parent->next_hash = _parent_hash[priv_parent->bucket];
   if (_parent_hash[priv_parent->bucket] != NULL)
   {
      _parent_hash[priv_parent->bucket]->prev_hash = priv_parent;
   }
   _parent_hash[priv_parent->bucket] = priv_parent;

   al_mutex_unlock(parent_mlock, parent_muvar, parent_mupid);
}
