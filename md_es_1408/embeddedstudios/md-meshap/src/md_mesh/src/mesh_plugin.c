/********************************************************************************
* MeshDynamics
* --------------
* File     : mesh_plugin.c
* Comments : External plugin interface implementation to Mesh
* Created  : 5/14/2008
* Author   : Abhijit Ayarekar
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  0  |5/14/2008 | Created.                                        | Abhijit|
* -----------------------------------------------------------------------------
********************************************************************************/

#include <linux/string.h>
#include "al.h"
#include "al_context.h"
#include "al_net_addr.h"
#include "al_802_11.h"
#include "imcp_snip.h"

#include "mesh_plugin_impl.h"
#include "mesh_plugin.h"

#define _MESH_PLUGIN_MAX_HANDLERS    1

struct _handler_data
{
   unsigned int                event_id_map;
   mesh_plugin_event_handler_t handler;
};

typedef struct _handler_data   _handler_data_t;

static _handler_data_t _handlers[_MESH_PLUGIN_MAX_HANDLERS];

int mesh_plugin_initialize()
{
   memset(_handlers, 0, sizeof(_handler_data_t) * _MESH_PLUGIN_MAX_HANDLERS);
   return 0;
}


int mesh_plugin_uninitialize()
{
   return 0;
}


int mesh_plugin_post_event(int mesh_plugin_event, void *event_param)
{
   int i;
   mesh_plugin_heartbeat_info_t hb_info;
   imcp_packet_info_t           *imcp_packet;
   int hb_info_initialized;

   hb_info_initialized = 0;
   memset(&hb_info, 0, sizeof(mesh_plugin_heartbeat_info_t));

   for (i = 0; i < _MESH_PLUGIN_MAX_HANDLERS; i++)
   {
      if ((_handlers[i].handler != NULL) && ((_handlers[i].event_id_map & mesh_plugin_event) != 0))
      {
         if (mesh_plugin_event == MESH_PLUGIN_EVENT_HEARTBEAT_RECEIVED)
         {
            if (hb_info_initialized == 0)
            {
               if (event_param == NULL)
               {
                  return 0;
               }

               imcp_packet = (imcp_packet_info_t *)event_param;
               memcpy(&hb_info.ds_mac, &imcp_packet->u.heart_beat.ds_mac, sizeof(al_net_addr_t));
               hb_info.children_count = imcp_packet->u.heart_beat.children_count;

               if (hb_info.children_count > 0)
               {
                  hb_info.immediate_sta = (al_net_addr_t *)al_heap_alloc(AL_CONTEXT sizeof(al_net_addr_t) * hb_info.children_count);
                  memcpy(hb_info.immediate_sta, imcp_packet->u.heart_beat.immidigiate_sta, sizeof(al_net_addr_t) * hb_info.children_count);
               }

               hb_info_initialized = 1;
            }

            _handlers[i].handler(mesh_plugin_event, &hb_info);

            continue;
         }

         _handlers[i].handler(mesh_plugin_event, event_param);
      }
   }

   if ((hb_info_initialized == 1) && (hb_info.immediate_sta != NULL))
   {
      al_heap_free(AL_CONTEXT hb_info.immediate_sta);
   }

   return 0;
}


int add_mesh_event_handler(unsigned int event_id_map, mesh_plugin_event_handler_t handler)
{
   int i;

   for (i = 0; i < _MESH_PLUGIN_MAX_HANDLERS; i++)
   {
      if (_handlers[i].handler == handler)
      {
         _handlers[i].event_id_map = event_id_map;
         return 0;
      }
   }

   for (i = 0; i < _MESH_PLUGIN_MAX_HANDLERS; i++)
   {
      if (_handlers[i].handler == NULL)
      {
         _handlers[i].handler      = handler;
         _handlers[i].event_id_map = event_id_map;
         return 0;
      }
   }

   return -1;
}


int remove_mesh_event_handler(unsigned int event_id_map, mesh_plugin_event_handler_t handler)
{
   int i;

   for (i = 0; i < _MESH_PLUGIN_MAX_HANDLERS; i++)
   {
      if (_handlers[i].handler == handler)
      {
         memset(&_handlers[i], 0, sizeof(_handler_data_t));
         return 0;
      }
   }

   return -1;
}
