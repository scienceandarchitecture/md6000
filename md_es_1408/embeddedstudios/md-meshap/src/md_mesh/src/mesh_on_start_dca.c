/********************************************************************************
* MeshDynamics
* --------------
* File     : mesh_on_start_dca.c
* Comments : Seperated for code readability
* Created  : 4/7/2006
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  5  |2/10/2009 | Div-Zero Bug fixed							  |Abhijit |
* -----------------------------------------------------------------------------
* |  4  |7/31/2007 | Count for downlinks not of DS PHY SUB TYPE      | Sriram |
* -----------------------------------------------------------------------------
* |  3  |11/22/2006| Fixes for ETSI Radar                            | Sriram |
* -----------------------------------------------------------------------------
* |  2  |11/14/2006| Changes for heap debug                          | Sriram |
* -----------------------------------------------------------------------------
* |  1  |10/18/2006| Changes for ETSI Radar                          | Sriram |
* -----------------------------------------------------------------------------
* |  0  |4/7/2006  | Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

#include "mesh_ng.h"
#define DWELL_TIME 500

/**
 * This is a private source file included by mesh_on_start.c, hence
 * It was created to improve readability.
 */
static AL_INLINE int random_Init(AL_CONTEXT_PARAM_DECL al_net_if_t *net_if_wm)
{
   unsigned char seed[4];
   unsigned char bt;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);

   bt = net_if_wm->config.hw_addr.bytes[2] + mesh_hardware_info->ds_net_if->config.hw_addr.bytes[2];
   memcpy(&seed[0], &bt, 1);
   bt = net_if_wm->config.hw_addr.bytes[3] + mesh_hardware_info->ds_net_if->config.hw_addr.bytes[3];
   memcpy(&seed[1], &bt, 1);
   bt = net_if_wm->config.hw_addr.bytes[4] + mesh_hardware_info->ds_net_if->config.hw_addr.bytes[4];
   memcpy(&seed[2], &bt, 1);
   bt = net_if_wm->config.hw_addr.bytes[5] + mesh_hardware_info->ds_net_if->config.hw_addr.bytes[5];
   memcpy(&seed[3], &bt, 1);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
   return 0;
}


static AL_INLINE int get_random_time(AL_CONTEXT_PARAM_DECL int min, int max)
{
   AL_PRINT_LOG_FLOW_0("MESH_AP	: get_random_time()");
   return (int)al_random(AL_CONTEXT min, max);
}


static AL_INLINE int _do_dca_lock(AL_CONTEXT_PARAM_DECL mesh_conf_if_info_t *wm_conf)
{
   int random_time;
   int reason;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
   random_Init(AL_CONTEXT wm_conf->net_if);

   random_time = get_random_time(AL_CONTEXT 1, SCAN_LOCK_OBJECTION_TIMEOUT);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MESH_AP	: Initial Random_time %d", random_time);
   al_thread_sleep(AL_CONTEXT random_time);

   /* wait for response */
   while (scan_state != _CHANNEL_SCAN_STATE_GO)
   {
      /* send imcp channel scal lock request */
      if (scan_state == _CHANNEL_SCAN_STATE_START)
      {
         scan_state = _CHANNEL_SCAN_STATE_WAIT_OBJECTION;
         mesh_imcp_send_packet_channel_scan_lock(AL_CONTEXT_SINGLE);
         al_wait_on_event(AL_CONTEXT mesh_scan_wait_objection_event, SCAN_LOCK_OBJECTION_TIMEOUT);
         if (scan_state == _CHANNEL_SCAN_STATE_WAIT_OBJECTION)
         {
            scan_state = _CHANNEL_SCAN_STATE_GO;
         }
      }
      else if (scan_state == _CHANNEL_SCAN_STATE_WAIT_UNLOCK)
      {
         reason      = al_wait_on_event(AL_CONTEXT mesh_scan_wait_unlock_event, RESCAN_LOCK_UNLOCK_WAIT_INTERVAL);
         scan_state  = _CHANNEL_SCAN_STATE_START;
         random_time = get_random_time(AL_CONTEXT 1000, RANDOM_TIME_MAX);
         al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MESH_AP	: Unlock Random_time %d", random_time);
         al_thread_sleep(AL_CONTEXT random_time);
      }
      else if (scan_state == _CHANNEL_SCAN_STATE_WAIT_RANDOM)
      {
         random_time = get_random_time(AL_CONTEXT 1000, RANDOM_TIME_MAX);
         al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MESH_AP	: Random_time %d", random_time);
         al_thread_sleep(AL_CONTEXT random_time);
         scan_state = _CHANNEL_SCAN_STATE_START;
      }

      if (mesh_state == _MESH_STATE_STOPPPING)
      {
         al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR," mesh_state is stopping ::func : %s LINE : %d\n", __func__,__LINE__);
         return RETURN_ERROR;
      }
   }

   if (mesh_state == _MESH_STATE_STOPPPING)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR," mesh_state is stopping ::func : %s LINE : %d\n", __func__,__LINE__);
      mesh_imcp_send_packet_channel_scan_unlock(AL_CONTEXT_SINGLE);
      return RETURN_ERROR;
   }

   scan_state = _CHANNEL_SCAN_STATE_SCANNING;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
   return RETURN_SUCCESS;
}

struct _channel_info
{
   al_duty_cycle_info_t *duty_cycle_info;
   int                  parent_count;
   int                  downlink_count;
   int                  radar_found;
   struct _channel_info *next_sort;                             /* Linked list for insertion sort */
   struct _channel_info *prev_sort;                             /* Linked list for insertion sort */
};

typedef struct _channel_info   _channel_info_t;
static AL_INLINE void _init_duty_cycle_info(AL_CONTEXT_PARAM_DECL mesh_conf_if_info_t *wm_conf,
                                            al_duty_cycle_info_t                      **duty_cycle_info)
{
   int i;
   al_duty_cycle_info_t *dci;
   _channel_info_t      *ci;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
   dci = (al_duty_cycle_info_t *)al_heap_alloc(AL_CONTEXT wm_conf->dca_list_count * sizeof(al_duty_cycle_info_t)AL_HEAP_DEBUG_PARAM);
   memset(dci, 0, wm_conf->dca_list_count * sizeof(al_duty_cycle_info_t));

   for (i = 0; i < wm_conf->dca_list_count; i++)
   {
      dci[i].channel      = wm_conf->dca_list[i];
      ci                  = (_channel_info_t *)al_heap_alloc(AL_CONTEXT sizeof(_channel_info_t)AL_HEAP_DEBUG_PARAM);
      ci->parent_count    = 0;
      ci->downlink_count  = 0;
      ci->radar_found     = 0;
      ci->next_sort       = NULL;
      ci->duty_cycle_info = &dci[i];
      dci[i].priv_data    = ci;
   }

   *duty_cycle_info = dci;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
}


static AL_INLINE void _uninit_duty_cycle_info(AL_CONTEXT_PARAM_DECL mesh_conf_if_info_t *wm_conf, al_duty_cycle_info_t *duty_cycle_info)
{
   int             i;
   _channel_info_t *ci;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
   for (i = 0; i < wm_conf->dca_list_count; i++)
   {
      ci = (_channel_info_t *)duty_cycle_info[i].priv_data;
      al_heap_free(AL_CONTEXT ci);
      al_heap_free(AL_CONTEXT duty_cycle_info[i].aps);
   }


   al_heap_free(AL_CONTEXT duty_cycle_info);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
}


struct _parent_enum
{
   mesh_conf_if_info_t  *wm_conf;
   al_duty_cycle_info_t *duty_cycle_info;
};

typedef struct _parent_enum   _parent_enum_t;

static int _parents_enum_func(AL_CONTEXT_PARAM_DECL void *enum_param, parent_t *parent)
{
   _parent_enum_t  *pe;
   _channel_info_t *ci;
   int             i;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);

   pe = (_parent_enum_t *)enum_param;

   for (i = 0; i < pe->wm_conf->dca_list_count; i++)
   {
      if (parent->channel == pe->duty_cycle_info[i].channel)
      {
         ci = (_channel_info_t *)pe->duty_cycle_info[i].priv_data;
         ++ci->parent_count;
      }
   }

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
   return 0;
}


static AL_INLINE void _set_parent_count_for_channels(AL_CONTEXT_PARAM_DECL mesh_conf_if_info_t *wm_conf, al_duty_cycle_info_t *duty_cycle_info)
{
   _parent_enum_t  pe;
   _channel_info_t *ci;
   int             i;
   int             j;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
   pe.duty_cycle_info = duty_cycle_info;
   pe.wm_conf         = wm_conf;

   /**
    * Iterate through parents list and update channel info
    * We do this only if the DS sub type is the same as this WM's sub_type
    */

   if (mesh_ng_is_ds_phy_sub_type_compatible(AL_CONTEXT mesh_hardware_info->ds_if_sub_type, wm_conf->phy_sub_type))
   {
      mesh_ng_enumerate_parents(AL_CONTEXT MESH_NG_ENUMERATE_PARENTS_TYPE_ALL, (void *)&pe, _parents_enum_func, 0);
   }

   /**
    * Now we iterate through other downlinks and see if they have the same channels.
    * We do this only for spectrum compatible downlinks
    */

   for (i = 0; i < mesh_hardware_info->wm_net_if_count; i++)
   {
      if (!mesh_ng_is_ds_phy_sub_type_compatible(AL_CONTEXT mesh_hardware_info->wms_conf[i]->phy_sub_type, wm_conf->phy_sub_type))
      {
         continue;
      }
      if (mesh_hardware_info->wms_conf[i]->operating_channel != 0)
      {
         for (j = 0; j < wm_conf->dca_list_count; j++)
         {
            if (mesh_hardware_info->wms_conf[i]->operating_channel == duty_cycle_info[j].channel)
            {
               ci = (_channel_info_t *)duty_cycle_info[j].priv_data;
               ++ci->downlink_count;
            }
         }
      }
   }
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
}


static AL_INLINE int _compare_channel_info(AL_CONTEXT_PARAM_DECL _channel_info_t *a, _channel_info_t *b, unsigned char phy_sub_type)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
   if (mesh_ng_is_ds_phy_sub_type_compatible(AL_CONTEXT mesh_hardware_info->ds_if_sub_type, phy_sub_type))
   {
      /**
       * Lowest Number of parents, Lowest number of downlinks, Lowest Channel saturation, Lowest power, Lowest number of APs
       */

      if (a->parent_count < b->parent_count)
      {
         return 1;
      }
      else if (a->parent_count > b->parent_count)
      {
         return -1;
      }
   }
   else
   {
      /**
       * Lowest Number of downlinks, Lowest Channel saturation, Lowest power, Lowest number of APs
       */

      if (a->downlink_count < b->downlink_count)
      {
         return 1;
      }
      else if (a->downlink_count > b->downlink_count)
      {
         return -1;
      }
   }

   if (a->downlink_count < b->downlink_count)
   {
      return 1;
   }
   else if (a->downlink_count == b->downlink_count)
   {
      if (a->duty_cycle_info->transmit_duration < b->duty_cycle_info->transmit_duration)
      {
         return 1;
      }
      else if (a->duty_cycle_info->transmit_duration == b->duty_cycle_info->transmit_duration)
      {
         if (a->duty_cycle_info->max_signal < b->duty_cycle_info->max_signal)
         {
            return 1;
         }
         else if (a->duty_cycle_info->max_signal == b->duty_cycle_info->max_signal)
         {
            if (a->duty_cycle_info->number_of_aps < b->duty_cycle_info->number_of_aps)
            {
               return 1;
            }
         }
      }
   }

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
   return -1;
}


_channel_info_t *_sort_dca_info_list(AL_CONTEXT_PARAM_DECL mesh_conf_if_info_t *wm_conf, al_duty_cycle_info_t *duty_cycle_info)
{
   _channel_info_t *head;
   _channel_info_t *ci;
   _channel_info_t *temp;               /* Iterates linked list */
   _channel_info_t *tail;               /* Preserves linked list tail for end insertion */
   int             i;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
   _set_parent_count_for_channels(AL_CONTEXT wm_conf, duty_cycle_info);

   head            = (_channel_info_t *)duty_cycle_info[0].priv_data;
   head->next_sort = NULL;
   head->prev_sort = NULL;
   tail            = head;

   for (i = 1; i < wm_conf->dca_list_count; i++)
   {
      ci   = (_channel_info_t *)duty_cycle_info[i].priv_data;
      temp = head;

      while (temp != NULL)
      {
         if (_compare_channel_info(ci, temp, wm_conf->phy_sub_type) == 1)
         {
            ci->next_sort   = temp;
            ci->prev_sort   = temp->prev_sort;
            temp->prev_sort = ci;
            if (ci->prev_sort != NULL)
            {
               ci->prev_sort->next_sort = ci;
            }
            if (temp == head)
            {
               head = ci;
            }
            break;
         }
         tail = temp;
         temp = temp->next_sort;
      }

      if (temp == NULL)
      {
         tail->next_sort = ci;
         ci->prev_sort   = tail;
         ci->next_sort   = NULL;
      }
   }

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
   return head;
}


static AL_INLINE void _print_dca_info_list(AL_CONTEXT_PARAM_DECL _channel_info_t *head)
{
   int saturation;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "=========== DCA CHANNEL INFORMATION ===========\n");
   while (head != NULL)
   {
      if ((head->duty_cycle_info->total_duration <= 0) || (head->duty_cycle_info->transmit_duration == 0))
      {
         saturation = 0;
      }
      else
      {
         saturation = (head->duty_cycle_info->transmit_duration * 10) / head->duty_cycle_info->total_duration;
      }

      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,
                   "\tChannel %d Radars %s Parents %d DL %d Saturation %d%% Max Power %ddBm APs %d\n",
                   head->duty_cycle_info->channel,
                   head->radar_found ? "Yes" : "No",
                   head->parent_count,
                   head->downlink_count,
                   saturation,
                   (int)head->duty_cycle_info->max_signal - 96,
                   head->duty_cycle_info->number_of_aps);

      head = head->next_sort;
   }
   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "===============================================\n");
}


static AL_INLINE int _dfs_scan(AL_CONTEXT_PARAM_DECL mesh_conf_if_info_t *wm_conf,
                               _channel_info_t                           *head,
                               al_802_11_operations_t                    *ex_op,
                               int                                       dfs_channel_avoid)
{
   _channel_info_t *temp;
   int             dfs_result;

   temp = head;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
   while (temp != NULL)
   {
      if (temp->duty_cycle_info->channel != dfs_channel_avoid)
      {
         ex_op->set_channel(wm_conf->net_if, temp->duty_cycle_info->channel);
         dfs_result = ex_op->dfs_scan(wm_conf->net_if);
         if (dfs_result == 1)
         {
            break;
         }
      }
      temp = temp->next_sort;
   }

   if (temp == NULL)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR," temp is NULL :: func : %s LINE : %d\n", __func__,__LINE__);
      return -1;
   }

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
   return temp->duty_cycle_info->channel;
}


static AL_INLINE int _do_dca(AL_CONTEXT_PARAM_DECL mesh_conf_if_info_t *wm_conf,
                             al_duty_cycle_info_t                      *duty_cycle_info,
                             int                                       dfs_channel_avoid)
{
   al_802_11_operations_t    *ex_op;
   al_802_11_tx_power_info_t power_info;
   _channel_info_t           *head;
   int channel, ret, flag = 0;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
   ex_op = wm_conf->net_if->get_extended_operations(wm_conf->net_if);

   if (ex_op == NULL)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR," ex_op is NULL :: func : %s LINE : %d\n", __func__,__LINE__);
      return wm_conf->config_channel;
   }

   ex_op->set_mode(wm_conf->net_if, AL_802_11_MODE_INFRA, 0);

   power_info.flags = AL_802_11_TXPOW_PERC;
   power_info.power = 100;
   ex_op->set_tx_power(wm_conf->net_if, &power_info);

   ret = ex_op->get_duty_cycle_info(wm_conf->net_if, wm_conf->dca_list_count, duty_cycle_info, DWELL_TIME, flag);
   if (ret)
       return wm_conf->config_channel;


   /** Sort the list */

   head = _sort_dca_info_list(AL_CONTEXT wm_conf, duty_cycle_info);

   /**
    * Now for ETSI rules, we need to stay on the channel for
    * atleast 60s and see if any RADAR was found.
    */

   if (_DFS_REQUIRED)
   {
      ex_op->set_mode(wm_conf->net_if, AL_802_11_MODE_MASTER, 1);
      while (1)
      {
         channel = _dfs_scan(AL_CONTEXT wm_conf, head, ex_op, dfs_channel_avoid);
         if (channel != -1)
         {
            break;
         }
         al_thread_sleep(AL_CONTEXT 60000);                     /** Sleep for 60 seconds and try again */
      }
      ex_op->set_mode(wm_conf->net_if, AL_802_11_MODE_INFRA, 0);
   }
   else
   {
      channel = head->duty_cycle_info->channel;
   }

   _print_dca_info_list(AL_CONTEXT head);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
   return channel;
}


static AL_INLINE int _dca1(AL_CONTEXT_PARAM_DECL mesh_conf_if_info_t *wm_conf, int dfs_channel_avoid)
{
   al_duty_cycle_info_t *duty_cycle_info;
   int channel;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MESH_AP	: DCA Scanning for %s....", wm_conf->net_if->name);

   _init_duty_cycle_info(AL_CONTEXT wm_conf, &duty_cycle_info);

   channel = _do_dca(AL_CONTEXT wm_conf, duty_cycle_info, dfs_channel_avoid);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MESH_AP	: WM Net IF working on Channel (%s)= %d", wm_conf->name, channel);

   _uninit_duty_cycle_info(AL_CONTEXT wm_conf, duty_cycle_info);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
   return channel;
}


int dfs_dca(AL_CONTEXT_PARAM_DECL al_net_if_t *net_if, int channel_avoid)
{
   int i;
   int channel;
   mesh_conf_if_info_t *wm_conf;

   wm_conf = NULL;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
   for (i = 0; i < mesh_hardware_info->wm_net_if_count; i++)
   {
      if (mesh_hardware_info->wms_conf[i]->net_if == net_if)
      {
         wm_conf = mesh_hardware_info->wms_conf[i];
         break;
      }
   }

   if (wm_conf->dy_channel_alloc)
   {
      channel = _dca1(AL_CONTEXT mesh_hardware_info->wms_conf[i], channel_avoid);
   }
   else
   {
      channel = -1;
   }

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
   return channel;
}
