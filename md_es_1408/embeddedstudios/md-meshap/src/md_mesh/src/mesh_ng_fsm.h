/********************************************************************************
* MeshDynamics
* --------------
* File     : mesh_ng_fsm.h
* Comments : Mesh New Generation State Machine
* Created  : 4/10/2006
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  5  |2/16/2009 | Uplink Scan Functionality Single Radio Changes  |Abhijit |
* -----------------------------------------------------------------------------
* |  4  |1/27/2009 | Changes for Uplink Scan Functionality           | Sriram |
* -----------------------------------------------------------------------------
* |  3  |6/13/2007 | Added MESH_NG_FSM_EVENT_EVALUATE_MOBILE         | Sriram |
* -----------------------------------------------------------------------------
* |  2  |4/26/2007 | Added MESH_NG_FSM_EVENT_FULL_DISCONNECT         | Sriram |
* -----------------------------------------------------------------------------
* |  1  |10/18/2006| Changes for ETSI Radar                          | Sriram |
* -----------------------------------------------------------------------------
* |  0  |4/10/2006 | Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

#ifndef __MESH_NG_FSM_H__
#define __MESH_NG_FSM_H__

struct mobility_window
{
   parent_t               *parent;
   struct mobility_window *next;
};
typedef struct mobility_window   mobility_window_t;

AL_DECLARE_GLOBAL_EXTERN(int mesh_ng_fsm_state);
AL_DECLARE_GLOBAL_EXTERN(int mobility_window_item_count);
AL_DECLARE_GLOBAL_EXTERN(mobility_window_t * mobility_window_head);
AL_DECLARE_GLOBAL_EXTERN(mobility_window_t * mobility_window_tail);
AL_DECLARE_GLOBAL_EXTERN(int step_scan_channel_index);

#define MESH_NG_FSM_STATE_STOPPED             0
#define MESH_NG_FSM_STATE_FULL_SCAN           1
#define MESH_NG_FSM_STATE_ALTERNATE_SCAN      2
#define MESH_NG_FSM_STATE_RUNNING             3
#define MESH_NG_FSM_STATE_SAMPLE              4
#define MESH_NG_FSM_STATE_HEARTBEAT           5
#define MESH_NG_FSM_STATE_EVALUATE            6
#define MESH_NG_FSM_STATE_CHECK_PARENT        7
#define MESH_NG_FSM_STATE_UPLINK_DISABLE      8
#define MESH_NG_FSM_STATE_RADAR_MASTER        9
#define MESH_NG_FSM_STATE_EVALUATE_MOBILE     10
#define MESH_NG_FSM_STATE_EVALUATE_ADHOC      11
#define MESH_NG_FSM_STATE_STEP_SCAN           12
#define MESH_NG_FSM_STATE_ALT_SCAN_ADHOC      13
#define MESH_NG_FSM_STATE_STEP_SCAN_UPLINK    14
#define MESH_NG_FSM_STATE_UPLINK_BUFFER       15

#define MESH_NG_FSM_EVENT_START               0
#define MESH_NG_FSM_EVENT_DISCONNECT          1
#define MESH_NG_FSM_EVENT_SAMPLE              2
#define MESH_NG_FSM_EVENT_HEARTBEAT           3
#define MESH_NG_FSM_EVENT_EVALUATE            4
#define MESH_NG_FSM_EVENT_CHECK_PARENT        5
#define MESH_NG_FSM_EVENT_UPLINK_DISABLE      6
#define MESH_NG_FSM_EVENT_RADAR_MASTER        7
#define MESH_NG_FSM_EVENT_RADAR_SLAVE         8
#define MESH_NG_FSM_EVENT_FULL_DISCONNECT     9
#define MESH_NG_FSM_EVENT_EVALUATE_MOBILE     10
#define MESH_NG_FSM_EVENT_EVALUATE_ADHOC      11
#define MESH_NG_FSM_EVENT_STEP_SCAN           12
#define MESH_NG_FSM_EVENT_STEP_SCAN_UPLINK    13
#define MESH_NG_FSM_EVENT_UPLINK_BUFFER       14


#define FSM_EVENT_2_STR(n) case MESH_NG_FSM_EVENT_ ## n: return #n
#define FSM_STATE_2_STR(n) case MESH_NG_FSM_STATE_ ## n: return #n
#define MESH_STATE_2_STR(n) case _MESH_STATE_ ## n: return #n

int mesh_ng_fsm_initialize(AL_CONTEXT_PARAM_DECL_SINGLE);
int mesh_ng_fsm_uninitialize(AL_CONTEXT_PARAM_DECL_SINGLE);
int mesh_ng_fsm_scanner_trigger(AL_CONTEXT_PARAM_DECL_SINGLE);
int mesh_ng_fsm_send_event(AL_CONTEXT_PARAM_DECL int event, void *event_data);

void mesh_ng_fsm_process_events(AL_CONTEXT_PARAM_DECL_SINGLE);
void mesh_ng_fsm_execute(AL_CONTEXT_PARAM_DECL_SINGLE);
void mesh_ng_fsm_update_downlink_vendor_info(AL_CONTEXT_PARAM_DECL_SINGLE);
void mesh_ng_change_current_mode_func(int crssi_limited);
extern al_spinlock_t fsm_splock;
AL_DECLARE_PER_CPU(int, fsm_spvar);

extern struct al_mutex fsm_mutex;
extern int fsm_muvar;
extern int fsm_mupid;


#endif /*__MESH_NG_FSM_H__*/
