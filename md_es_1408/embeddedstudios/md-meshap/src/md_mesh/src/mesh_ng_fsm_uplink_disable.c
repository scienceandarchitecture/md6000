/********************************************************************************
* MeshDynamics
* --------------
* File     : mesh_ng_fsm_uplink_disable.c
* Comments : Mesh new generation uplink disable state
* Created  : 5/9/2006
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  0  |5/9/2006  | Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

/**
 * This is a private source file included by mesh_ng_fsm.c, hence
 * It was created to improve readability.
 */
static void _uplink_disable_execute(AL_CONTEXT_PARAM_DECL void *event_data)
{
   al_802_11_operations_t *ex_op;
   int timeout;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
   timeout = (int)event_data;

   ex_op = (al_802_11_operations_t *)_DS_NET_IF->get_extended_operations(_DS_NET_IF);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MESH_NG: Disabling uplink transmissions for %d ms", timeout);

   AL_ATOMIC_SET(_DS_NET_IF->buffering_state, AL_NET_IF_BUFFERING_STATE_DROP);
   ex_op->enable_ds_verification_opertions(_DS_NET_IF, 0);

   al_thread_sleep(AL_CONTEXT timeout);

   ex_op->enable_ds_verification_opertions(_DS_NET_IF, 1);

   AL_ATOMIC_SET(_DS_NET_IF->buffering_state, AL_NET_IF_BUFFERING_STATE_NONE);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MESH_NG: Resuming uplink transmissions");

   _mesh_ng_fsm_set_state(AL_CONTEXT MESH_NG_FSM_STATE_RUNNING);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
}
