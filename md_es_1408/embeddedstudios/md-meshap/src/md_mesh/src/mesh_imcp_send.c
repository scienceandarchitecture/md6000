
/********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : mesh_imcp_send.c
 * Comments : Mesh IMCP Send Functions
 * Created  : 4/17/2006
 * Author   : Sriram Dayanandan
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * -----------------------------------------------------------------------------
 * | No  |Date      |  Comment                                        | Author |
 * -----------------------------------------------------------------------------
 * | 15  |1/27/2009 | Changes for Uplink Scan Functionality           | Sriram |
 * -----------------------------------------------------------------------------
 * | 14  |8/22/2008 | Changes for SIP                                 |Abhijit |
 * -----------------------------------------------------------------------------
 * | 13  |11/20/2007| Changes for GPS coordinate in HB1               | Sriram |
 * -----------------------------------------------------------------------------
 * | 12  |8/16/2007 | Set CPU usage in HI field                       | Sriram |
 * -----------------------------------------------------------------------------
 * | 11  |8/16/2007 | Added Free Mem (MB) to CTC                      | Sriram |
 * -----------------------------------------------------------------------------
 * | 10  |7/17/2007 | Memory leak protection added                    | Sriram |
 * -----------------------------------------------------------------------------
 * |  9  |7/16/2007 | HB2 list iteration logic fixed                  | Sriram |
 * -----------------------------------------------------------------------------
 * |  8  |7/16/2007 | Interrupts disabled when sending heartbeat      | Sriram |
 * -----------------------------------------------------------------------------
 * |  7  |6/5/2007  | mesh_imcp_send_packet_ds_excerciser changed     | Sriram |
 * -----------------------------------------------------------------------------
 * |  6  |6/5/2007  | HB2 bit rate queried from rate control          | Sriram |
 * -----------------------------------------------------------------------------
 * |  5  |4/11/2007 | Changes for Radio diagnostics                   | Sriram |
 * -----------------------------------------------------------------------------
 * |  4  |11/20/2006| Added Voltage to CTC                            | Sriram |
 * -----------------------------------------------------------------------------
 * |  3  |11/15/2006| Memory leak problem fixed                       | Sriram |
 * -----------------------------------------------------------------------------
 * |  2  |11/14/2006| Changes for heap debug support                  | Sriram |
 * -----------------------------------------------------------------------------
 * |  1  |5/16/2006 | send_supported_channels_info changed for scanner|Prachiti|
 * -----------------------------------------------------------------------------
 * |  0  |4/17/2006 | Created.                                        | Sriram |
 * -----------------------------------------------------------------------------
 ********************************************************************************/

/**
 * This is a private source file included by mesh_imcp.c, hence
 * It was created to improve readability.
 */

 
#include<linux/module.h>
struct _parents_enum_data {
	imcp_packet_info_t*		pi;
	int						pos;
	int						flag;
};

extern struct al_mutex parent_mlock;
extern int parent_muvar, parent_mupid;

typedef struct _parents_enum_data _parents_enum_data_t;
int is_vlan_interface(const char *name);

int _parents_enum_func(AL_CONTEXT_PARAM_DECL void* enum_param,parent_t* parent)
{
	_parents_enum_data_t*	ped;

//   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
	ped	= (_parents_enum_data_t*)enum_param;

	if(!(parent->flags & MESH_PARENT_FLAG_DISABLED)) {
		if(ped->flag == 1) {
			memcpy(&ped->pi->u.heart_beat.known_aps_info[ped->pos].ap_ds_mac,&parent->ds_mac, sizeof(al_net_addr_t));
			ped->pi->u.heart_beat.known_aps_info[ped->pos].et		= parent->direct_bit_rate;
			ped->pi->u.heart_beat.known_aps_info[ped->pos].st		= parent->signal;
		}
		++ped->pos;
	}

//   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
	return 0;
}

static AL_INLINE void _add_parents_list_to_heartbeat(AL_CONTEXT_PARAM_DECL imcp_packet_info_t* pi)
{
	_parents_enum_data_t	ped;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
	ped.pi		= pi;
	ped.pos		= 0;
	ped.flag	= 0;

	mesh_ng_enumerate_parents(AL_CONTEXT MESH_NG_ENUMERATE_PARENTS_TYPE_ALL,(void*)&ped,_parents_enum_func, 1);
    al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"MESHAP : INFO : Number of parent bening added = %d : %s : %d\n", ped.pos, __func__,__LINE__);

	pi->u.heart_beat.known_ap_count	= ped.pos;

	if(pi->u.heart_beat.known_ap_count > 0) {
		pi->u.heart_beat.known_aps_info	= (imcp_known_ap_info_t*)al_heap_alloc(AL_CONTEXT pi->u.heart_beat.known_ap_count * sizeof(imcp_known_ap_info_t) AL_HEAP_DEBUG_PARAM);
		ped.pi		= pi;
		ped.pos		= 0;
		ped.flag	= 1;
		mesh_ng_enumerate_parents(AL_CONTEXT MESH_NG_ENUMERATE_PARENTS_TYPE_ALL,(void*)&ped,_parents_enum_func, 1);
	} else {
		pi->u.heart_beat.known_aps_info	= NULL;
	}
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
}

static unsigned int _encode_coordinate(const char* coordinate)
{
	const char*		p;
	unsigned int	encoded_value;
	int				value;
	char			degrees[8];
	char			mantissa[32];
	char*			s;

	p	= coordinate;
	
	s	= degrees;
	while(*p != '.' && *p != 0) {
		*s++ = *p++;
	}
	*s = 0;

	if(*p == '.')
		p++;

	s	= mantissa;
	while(*p != 0) {
		*s++ = *p++;
	}
	*s = 0;

	encoded_value	= 0;
	value			= al_atoi(degrees);
	
	if(value < 0) {
		encoded_value	|= 0x80000000;
		value			*= -1;
	}

	encoded_value	|= (value & 0xFF);

	value			  = al_atoi(mantissa);
	value			&= 0x7FFFFF;
	value			<<= 8;	
	encoded_value	|= value;

	return encoded_value;
}

int mesh_imcp_send_packet_heartbeat(AL_CONTEXT_PARAM_DECL int sqnr)
{
	imcp_packet_info_t*	pi;
	al_net_if_stat_t*	ap_net_if_stats;
	al_board_info_t		board_info;
	unsigned long		flags;
	unsigned int		total_mem;
	unsigned int		free_mem;
	unsigned char		free_mem_MB;
	unsigned char		cpu_usage;
	char			latitude[64];
	char			longitude[64];
	char			speed[32];
	char			altitude[32];

	pi	= imcp_snip_create_packet(AL_CONTEXT_SINGLE);

        pi->flags = IMCP_DEFAULT_FLAGS;

	pi->packet_type	= IMCP_SNIP_PACKET_TYPE_HEARTBEAT;

	memcpy(&pi->u.heart_beat.ds_mac, _DS_MAC, sizeof(al_net_addr_t));
	pi->u.heart_beat.sqnr = sqnr;
	al_get_board_info(AL_CONTEXT &board_info);

	if(board_info.valid_values_mask & AL_BOARD_INFO_MASK_TEMP) {
		pi->u.heart_beat.board_temp	= board_info.temperature;
	} else {
		pi->u.heart_beat.board_temp	= 0;
	}

	al_get_board_mem_info(AL_CONTEXT &total_mem,&free_mem);

	/** 1024 * 1024 = 1048576 */

	free_mem_MB	= (unsigned char)((unsigned int)(free_mem/1048576));

	al_get_cpu_load_info(AL_CONTEXT &cpu_usage);

	al_get_gps_info(AL_CONTEXT longitude, latitude, altitude, speed);

	pi->u.heart_beat.latitude	= _encode_coordinate(latitude);
	pi->u.heart_beat.longitude	= _encode_coordinate(longitude);
	pi->u.heart_beat.altitude	= al_atoi(AL_CONTEXT altitude);
	pi->u.heart_beat.speed		= al_atoi(AL_CONTEXT speed);

	if(mesh_mode == _MESH_AP_ROOT) {
		memcpy(&pi->u.heart_beat.root_bssid, _DS_MAC,
			sizeof(al_net_addr_t));
		pi->u.heart_beat.ctc		= mesh_ng_get_ctc(AL_CONTEXT_SINGLE);
		pi->u.heart_beat.hpc		= 0;
		pi->u.heart_beat.hi		= cpu_usage;
		pi->u.heart_beat.hbi		= mesh_config->hbeat_interval;
		pi->u.heart_beat.bit_rate	= 100;
		pi->u.heart_beat.known_ap_count	= 0;
		pi->u.heart_beat.known_aps_info	= NULL;
		memcpy(&pi->u.heart_beat.parent_bssid, _DS_MAC,
			sizeof(al_net_addr_t));

	} else {

		memcpy(&pi->u.heart_beat.root_bssid,
			&current_parent->root_bssid,
			sizeof(al_net_addr_t));

		pi->u.heart_beat.ctc	= mesh_ng_get_ctc(AL_CONTEXT_SINGLE);
		pi->u.heart_beat.hi	= cpu_usage;
		pi->u.heart_beat.hbi	= mesh_config->hbeat_interval;

		if(_DISJOINT_ADHOC_MODE) {
			al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"MESHAP : INFO :%s : %d, func_mode = %d\n", __func__, __LINE__, func_mode);
			switch(func_mode) {
			case _FUNC_MODE_FFR:
			case _FUNC_MODE_LFR:
				pi->u.heart_beat.hpc = 0;
				break;
			case _FUNC_MODE_FFN:
			case _FUNC_MODE_LFN:
				pi->u.heart_beat.hpc = (current_parent->hpc + 1);
				break;
			}
		} else {
			pi->u.heart_beat.hpc = (current_parent->hpc + 1);
		}

		/**
		 * We shall always send parent->direct_bit_rate in the heartbeat.
		 */
	        al_802_11_operations_t* ex_op =NULL;

		ex_op = (al_802_11_operations_t*)mesh_hardware_info->ds_net_if->\
                get_extended_operations(mesh_hardware_info->ds_net_if);
        	if(ex_op != NULL) {
			pi->u.heart_beat.bit_rate = ex_op->get_bit_rate(mesh_hardware_info->ds_net_if);
        	} else {
			pi->u.heart_beat.bit_rate = current_parent->direct_bit_rate;
		}

		memcpy(&pi->u.heart_beat.parent_bssid, &current_parent->bssid, sizeof(al_net_addr_t));

        al_mutex_lock(parent_mlock, parent_muvar, parent_mupid);
		_add_parents_list_to_heartbeat(AL_CONTEXT pi);
		al_mutex_unlock(parent_mlock, parent_muvar, parent_mupid);

	}

	pi->u.heart_beat.crssi	= mesh_ng_get_crssi(AL_CONTEXT_SINGLE);

	IMCP_SNIP_CTC_SET_BOARD_VOLTAGE(pi->u.heart_beat.ctc,(board_info.voltage & 0xFF));

	IMCP_SNIP_CTC_SET_MEM_FREE(pi->u.heart_beat.ctc,free_mem_MB);
		
	ap_net_if_stats = access_point_get_stats (AL_CONTEXT_SINGLE); 
	
	pi->u.heart_beat.tx_packet_count	= ap_net_if_stats->tx_packets;
	pi->u.heart_beat.rx_packet_count	= ap_net_if_stats->rx_packets;
	pi->u.heart_beat.tx_byte_count		= ap_net_if_stats->tx_bytes;
	pi->u.heart_beat.rx_byte_count		= ap_net_if_stats->rx_bytes;


	mesh_table_get_immidiate_sta_entries(AL_CONTEXT &pi->u.heart_beat.immidigiate_sta, &pi->u.heart_beat.children_count);
	

	imcp_snip_send(AL_CONTEXT pi);

	if(pi->u.heart_beat.immidigiate_sta != NULL) {
		al_heap_free(AL_CONTEXT pi->u.heart_beat.immidigiate_sta);
	}

	if(pi->u.heart_beat.known_aps_info != NULL) {
		al_heap_free(AL_CONTEXT pi->u.heart_beat.known_aps_info);
	}

	imcp_snip_release_packet(AL_CONTEXT pi);

	return RETURN_SUCCESS;
}

int mesh_imcp_send_packet_sta_assoc_notification(AL_CONTEXT_PARAM_DECL al_net_addr_t* sta_addr, al_net_addr_t* ap_wm_addr)
{
	imcp_packet_info_t*	pi;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
	AL_ASSERT("MESH_AP	: mesh_imcp_send_packet_sta_assoc_notification", sta_addr != NULL);
	AL_ASSERT("MESH_AP	: mesh_imcp_send_packet_sta_assoc_notification", ap_wm_addr != NULL);

	pi	= imcp_snip_create_packet(AL_CONTEXT_SINGLE);

	pi->packet_type					= IMCP_SNIP_PACKET_TYPE_STA_ASSOC_NOTIFICATION;
	memcpy(&pi->u.sta_assoc_notification.ap_ds_mac,	_DS_MAC,		sizeof(al_net_addr_t));
	memcpy(&pi->u.sta_assoc_notification.sta_mac,		sta_addr,		sizeof(al_net_addr_t));
	memcpy(&pi->u.sta_assoc_notification.ap_wm_mac,	ap_wm_addr,		sizeof(al_net_addr_t));

	imcp_snip_send(AL_CONTEXT pi);

	imcp_snip_release_packet(AL_CONTEXT pi);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
	return RETURN_SUCCESS;
}

int mesh_imcp_send_packet_sta_disassoc_notification(AL_CONTEXT_PARAM_DECL al_net_addr_t* sta_addr, int reason)
{
	imcp_packet_info_t*	pi;

	AL_ASSERT("MESH_AP	: mesh_imcp_send_packet_sta_disassoc_notification", sta_addr != NULL);

	pi	= imcp_snip_create_packet(AL_CONTEXT_SINGLE);
    if (pi == NULL) {
        al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESHAP : ERROR : packet info is NULL : %s : %d\n", __func__,__LINE__);
        return -1;

    }

	pi->packet_type					= IMCP_SNIP_PACKET_TYPE_STA_DISASSOC_NOTIFICATION;
	memcpy(&pi->u.sta_disassoc_notification.ap_ds_mac,	_DS_MAC,		sizeof(al_net_addr_t));
	memcpy(&pi->u.sta_disassoc_notification.sta_mac,		sta_addr,		sizeof(al_net_addr_t));
	pi->u.sta_disassoc_notification.reason = reason;
	
	imcp_snip_send(AL_CONTEXT pi);

	imcp_snip_release_packet(AL_CONTEXT pi);
	
	return RETURN_SUCCESS;
}

int mesh_imcp_send_packet_channel_scan_lock(AL_CONTEXT_PARAM_DECL_SINGLE)
{
	imcp_packet_info_t*	pi;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
	pi	= imcp_snip_create_packet(AL_CONTEXT_SINGLE);

	pi->packet_type = IMCP_SNIP_PACKET_TYPE_CHANNEL_SCAN_LOCK;

	memcpy(&pi->u.channel_scan_lock.ds_mac,_DS_MAC,sizeof(al_net_addr_t));

	pi->u.channel_scan_lock.phy_id = scanning_current_phy_id;

	imcp_snip_send(AL_CONTEXT pi);

	imcp_snip_release_packet(AL_CONTEXT pi);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
	return RETURN_SUCCESS;
}

int mesh_imcp_send_packet_channel_scan_unlock(AL_CONTEXT_PARAM_DECL_SINGLE)
{
	imcp_packet_info_t*	pi;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
	pi	= imcp_snip_create_packet(AL_CONTEXT_SINGLE);

	pi->packet_type = IMCP_SNIP_PACKET_TYPE_CHANNEL_SCAN_UNLOCK;
	memcpy(&pi->u.channel_scan_unlock.ds_mac, _DS_MAC, sizeof(al_net_addr_t));
	pi->u.channel_scan_unlock.phy_id = scanning_current_phy_id;

	imcp_snip_send(AL_CONTEXT pi);

	imcp_snip_release_packet(AL_CONTEXT pi);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
	return RETURN_SUCCESS;
}

int mesh_imcp_send_packet_channel_scan_lock_objection(AL_CONTEXT_PARAM_DECL_SINGLE)
{
	imcp_packet_info_t*	pi;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
	pi	= imcp_snip_create_packet(AL_CONTEXT_SINGLE);

	pi->packet_type = IMCP_SNIP_PACKET_TYPE_CHANNEL_SCAN_LOCK_OBJECTION;
	memcpy(&pi->u.channel_scan_lock_objection.ds_mac, _DS_MAC, sizeof(al_net_addr_t));
	pi->u.channel_scan_lock_objection.phy_id = scanning_current_phy_id;

	imcp_snip_send(AL_CONTEXT pi);

	imcp_snip_release_packet(AL_CONTEXT pi);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
	return RETURN_SUCCESS;
}

int mesh_imcp_send_packet_reset(AL_CONTEXT_PARAM_DECL_SINGLE)
{
	return RETURN_SUCCESS;
}

int mesh_imcp_send_packet_hardware_info(AL_CONTEXT_PARAM_DECL_SINGLE)
{
	int								i, j;
	imcp_packet_info_t*				pi;
	al_802_11_operations_t*			ex_operations;
	unsigned char					channel;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
	pi	= imcp_snip_create_packet(AL_CONTEXT_SINGLE);

	pi->packet_type						= IMCP_SNIP_PACKET_TYPE_AP_HARDWARE_INFO;

	memcpy(&pi->u.hardware_info.ds_info.mac_addr, _DS_MAC, sizeof(al_net_addr_t));
	pi->u.hardware_info.major_version	= _TORNA_VERSION_MAJOR_;
	pi->u.hardware_info.minor_version	= _TORNA_VERSION_MINOR_;
	pi->u.hardware_info.variant_version	= _TORNA_VERSION_VARIANT_;
    pi->u.hardware_info.hardware_model_length = strlen(mesh_config->hardware_model)+1;
         
    strcpy(pi->u.hardware_info.hardware_model, mesh_config->hardware_model);
	pi->u.hardware_info.ds_info.type		= mesh_hardware_info->ds_if_type;
	pi->u.hardware_info.ds_info.sub_type	= mesh_hardware_info->ds_if_sub_type;

	channel = 0;

	ex_operations = mesh_hardware_info->ds_net_if->get_extended_operations(mesh_hardware_info->ds_net_if);
	if(ex_operations != NULL) {
		channel = ex_operations->get_channel(mesh_hardware_info->ds_net_if);
	}
	pi->u.hardware_info.ds_info.channel	= channel;


	pi->u.hardware_info.name_length		= strlen(mesh_config->name);
	memcpy(pi->u.hardware_info.name,		mesh_config->name, pi->u.hardware_info.name_length);

	pi->u.hardware_info.ds_info.name_length		=  strlen(mesh_hardware_info->ds_net_if->name);
	memcpy(pi->u.hardware_info.ds_info.name,mesh_hardware_info->ds_net_if->name, pi->u.hardware_info.ds_info.name_length);

	pi->u.hardware_info.wms_info			= (phy_hardware_info_t*)al_heap_alloc(AL_CONTEXT mesh_hardware_info->wm_net_if_count * sizeof(phy_hardware_info_t) AL_HEAP_DEBUG_PARAM);

	for(i = 0, j = 0; j < mesh_hardware_info->wm_net_if_count; j++) {
		/*Skip sending vlan interface information to NMS*/
		if(mesh_config->vlan_count > 1 && is_vlan_interface(mesh_hardware_info->wms_conf[j]->net_if->name)) {
			continue;
		}
		pi->u.hardware_info.wm_count++;
		memcpy(&pi->u.hardware_info.wms_info[i].mac_addr,	_WMS_MAC(j),	sizeof(al_net_addr_t));
		pi->u.hardware_info.wms_info[i].type			= mesh_hardware_info->wms_conf[j]->phy_type;
		pi->u.hardware_info.wms_info[i].sub_type		= mesh_hardware_info->wms_conf[j]->phy_sub_type;

		ex_operations = mesh_hardware_info->wms_conf[j]->net_if->get_extended_operations(mesh_hardware_info->wms_conf[j]->net_if);

		if(ex_operations != NULL) {
			channel = ex_operations->get_channel(mesh_hardware_info->wms_conf[j]->net_if);
		}
		pi->u.hardware_info.wms_info[i].channel	= channel;

		pi->u.hardware_info.wms_info[i].name_length	= strlen(mesh_hardware_info->wms_conf[j]->net_if->name);
		memcpy(pi->u.hardware_info.wms_info[i].name,mesh_hardware_info->wms_conf[j]->net_if->name,pi->u.hardware_info.wms_info[i].name_length);
		i++;
	}
	
	if(mesh_hardware_info->monitor_type != MONITOR_TYPE_ABSENT) {
		memcpy(&pi->u.hardware_info.scanner_info.mac_addr,_MON_MAC,	sizeof(al_net_addr_t));
		pi->u.hardware_info.scanner_info.type			= _MON_CONF->phy_type;
		pi->u.hardware_info.scanner_info.sub_type		= _MON_CONF->phy_sub_type;
		pi->u.hardware_info.scanner_info.name_length	= strlen(_MON_NET_IF->name);
		memcpy(pi->u.hardware_info.scanner_info.name,_MON_NET_IF->name,pi->u.hardware_info.scanner_info.name_length);
	}

	imcp_snip_send(AL_CONTEXT pi);

	al_heap_free(AL_CONTEXT pi->u.hardware_info.wms_info);

	imcp_snip_release_packet(AL_CONTEXT pi);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
	return RETURN_SUCCESS;

}

int mesh_imcp_send_packet_ap_sub_tree_info(AL_CONTEXT_PARAM_DECL_SINGLE)
{
	imcp_packet_info_t*	pi;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
	pi	= imcp_snip_create_packet(AL_CONTEXT_SINGLE);

	pi->packet_type						= IMCP_SNIP_PACKET_TYPE_AP_SUB_TREE_INFO;

	memcpy(&pi->u.sub_tree_info.ds_mac,	_DS_MAC, sizeof(al_net_addr_t));
	mesh_table_get_sta_entries(AL_CONTEXT pi);

	imcp_snip_send(AL_CONTEXT pi);

	al_heap_free(AL_CONTEXT pi->u.sub_tree_info.sta_entries);

	imcp_snip_release_packet(AL_CONTEXT pi);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
	return RETURN_SUCCESS;
}

int mesh_imcp_send_packet_mesh_init_status(AL_CONTEXT_PARAM_DECL char* status_message)
{
#ifndef _AL_ROVER_
	imcp_packet_info_t*	pi;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
	pi	= imcp_snip_create_packet(AL_CONTEXT_SINGLE);

	pi->packet_type						= IMCP_SNIP_PACKET_TYPE_MESH_INIT_STATUS;

	memcpy(&pi->u.mesh_init_status.ds_mac, _DS_MAC, sizeof(al_net_addr_t));
	pi->u.mesh_init_status.status				= 1;
	pi->u.mesh_init_status.status_data_length	= strlen(status_message);
	strcpy(pi->u.mesh_init_status.status_data,	status_message);
	imcp_snip_send(AL_CONTEXT pi);

	imcp_snip_release_packet(AL_CONTEXT pi);
	
#endif
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
	return RETURN_SUCCESS;
}

int mesh_imcp_send_packet_ds_excerciser(AL_CONTEXT_PARAM_DECL al_net_if_t* net_if, al_net_addr_t* dest_mac)
{
	imcp_packet_info_t*		pi;
	unsigned char			buffer[1400];

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
	pi	= imcp_snip_create_packet(AL_CONTEXT_SINGLE);

	pi->packet_type	= IMCP_SNIP_PACKET_TYPE_DS_EXCERCISER;

	pi->net_if							= net_if;
	pi->dest_addr						= dest_mac;
	pi->u.ds_excerciser.data_size		= 1400;
	pi->u.ds_excerciser.data			= buffer;

	imcp_snip_send(AL_CONTEXT pi);

	imcp_snip_release_packet(AL_CONTEXT pi);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
	return RETURN_SUCCESS;
}

int mesh_imcp_send_packet_wm_info_request(AL_CONTEXT_PARAM_DECL al_net_if_t*	net_if,
										  al_net_addr_t*						ds_mac_dest,
										  al_net_addr_t*						ds_mac_src,
										  al_net_addr_t*						wm_mac_dest,
										  unsigned char							ds_type,
										  unsigned char							ds_sub_type)
{
	imcp_packet_info_t*	pi;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
	pi	= imcp_snip_create_packet(AL_CONTEXT_SINGLE);

	pi->packet_type	= IMCP_SNIP_PACKET_TYPE_WM_INFO_REQUEST;
	pi->net_if		= net_if;
	
	memcpy(&pi->u.wm_info_request.ds_mac_dest,ds_mac_dest,sizeof(al_net_addr_t));
	memcpy(&pi->u.wm_info_request.ds_mac_src,ds_mac_src,sizeof(al_net_addr_t));
	memcpy(&pi->u.wm_info_request.wm_mac_dest,wm_mac_dest,sizeof(al_net_addr_t));
	pi->u.wm_info_request.ds_type		= ds_type;
	pi->u.wm_info_request.ds_sub_type	= ds_sub_type;

	imcp_snip_send(AL_CONTEXT pi);

	imcp_snip_release_packet(AL_CONTEXT pi);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
	return RETURN_SUCCESS;
}

int mesh_imcp_send_packet_wm_info_response(AL_CONTEXT_PARAM_DECL al_net_if_t*	net_if,
										   al_net_addr_t*						ds_mac_dest,
										   al_net_addr_t*						ds_mac_src,
										   int									wm_count,
										   unsigned char						wm_type,
										   unsigned char						wm_sub_type,
										   mesh_imcp_wm_info_list_t*			wm_list)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
	imcp_packet_info_t*			pi;
	mesh_imcp_wm_info_list_t*	temp;
	int							i;

	if(wm_list == NULL || wm_count == 0)
		return -1;

	pi	= imcp_snip_create_packet(AL_CONTEXT_SINGLE);

	pi->packet_type	= IMCP_SNIP_PACKET_TYPE_WM_INFO_RESPONSE;
	pi->net_if		= net_if;
	temp			= wm_list;

	memcpy(&pi->u.wm_info_response.ds_mac_dest,ds_mac_dest,sizeof(al_net_addr_t));
	memcpy(&pi->u.wm_info_response.ds_mac_src,ds_mac_src,sizeof(al_net_addr_t));
	pi->u.wm_info_response.wm_type		= wm_type;
	pi->u.wm_info_response.wm_sub_type	= wm_sub_type;

		
	memcpy(&pi->u.wm_info_response.wm_addr,&temp->wm_addr,sizeof(al_net_addr_t));
	pi->u.wm_info_response.wm_channel	= temp->wm_channel;
	pi->u.wm_info_response.essid_length	= strlen(temp->wm_essid);
	strcpy(pi->u.wm_info_response.essid,temp->wm_essid);

	temp	= temp->next;
	pi->u.wm_info_response.other_wm_count	= wm_count - 1;

	if(pi->u.wm_info_response.other_wm_count > 0) {
		pi->u.wm_info_response.other_wm_info = al_heap_alloc(AL_CONTEXT sizeof(*pi->u.wm_info_response.other_wm_info) * pi->u.wm_info_response.other_wm_count AL_HEAP_DEBUG_PARAM);
		memset(pi->u.wm_info_response.other_wm_info,0,sizeof(*pi->u.wm_info_response.other_wm_info) * pi->u.wm_info_response.other_wm_count);
		for(i = 0; i < pi->u.wm_info_response.other_wm_count; i++) {
			pi->u.wm_info_response.other_wm_info[i].essid_length	= strlen(temp->wm_essid);
			strcpy(pi->u.wm_info_response.other_wm_info[i].essid,temp->wm_essid);
			pi->u.wm_info_response.other_wm_info[i].wm_channel	= temp->wm_channel;
			memcpy(&pi->u.wm_info_response.other_wm_info[i].wm_addr,&temp->wm_addr,sizeof(al_net_addr_t));
		}
	}

	imcp_snip_send(AL_CONTEXT pi);

	if(pi->u.wm_info_response.other_wm_count > 0) {
		al_heap_free(AL_CONTEXT pi->u.wm_info_response.other_wm_info);
	}

	imcp_snip_release_packet(AL_CONTEXT pi);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
	return RETURN_SUCCESS;
}

int mesh_imcp_send_packet_heartbeat2(AL_CONTEXT_PARAM_DECL int sqnr)
{
	int			i;
	imcp_packet_info_t*	pi;
	al_802_11_operations_t*	ex_op =NULL;
	al_net_addr_t*		sta_list;
	access_point_sta_info_t	sta_info;
	al_rate_ctrl_info_t	info;
	unsigned long		flags;

	pi = imcp_snip_create_packet(AL_CONTEXT_SINGLE);

	pi->packet_type	= IMCP_SNIP_PACKET_TYPE_HEARTBEAT2;

	memcpy(&pi->u.heart_beat2.ds_mac, _DS_MAC, sizeof(al_net_addr_t));
	pi->u.heart_beat2.sqnr = sqnr;

	ex_op = (al_802_11_operations_t*)mesh_hardware_info->ds_net_if->\
		get_extended_operations(mesh_hardware_info->ds_net_if);
	if(ex_op != NULL) {
		pi->u.heart_beat2.up_tx_rate  = ex_op->get_bit_rate(mesh_hardware_info->ds_net_if);
	} else {
		pi->u.heart_beat2.up_tx_rate  = 0;
	}

	memcpy(&(pi->u.heart_beat2.flags),&(mesh_config->flags),4);
	pi->u.heart_beat2.last_update_number		= mesh_config->config_sqnr;
	pi->u.heart_beat2.imcp_packet_capabilities	= IMCP_SNIP_PACKET_CAPABILITIES;
	pi->u.heart_beat2.imcp_config_capabilities	= IMCP_SNIP_CONFIG_TYPE_CAPABILITIES;
	pi->u.heart_beat2.imcp_command_capabilities	= IMCP_SNIP_GENERIC_COMMAND_CAPABILITIES;


	mesh_table_get_immidiate_sta_entries(AL_CONTEXT &sta_list, &pi->u.heart_beat2.child_count);


	if(pi->u.heart_beat2.child_count > 0) {
		pi->u.heart_beat2.child_info = (imcp_child_info_t*)
			al_heap_alloc(AL_CONTEXT pi->u.heart_beat2.child_count * 
				sizeof(imcp_child_info_t) AL_HEAP_DEBUG_PARAM);

		for(i = 0; i < pi->u.heart_beat2.child_count; i++) {

			memcpy(&(pi->u.heart_beat2.child_info[i].mac_addr),&sta_list[i],sizeof(al_net_addr_t));
			
			access_point_get_sta_info(AL_CONTEXT &sta_list[i],&sta_info);

			pi->u.heart_beat2.child_info[i].signal_strength	= sta_info.rssi;

			if(sta_info.net_if != NULL) {
				ex_op = (al_802_11_operations_t*)sta_info.net_if->get_extended_operations(sta_info.net_if);
				if(ex_op != NULL) {
					ex_op->get_rate_ctrl_info(sta_info.net_if,&sta_info.address,&info);
					pi->u.heart_beat2.child_info[i].down_tx_rate	= info.rate_in_mbps;
				} else {
					pi->u.heart_beat2.child_info[i].down_tx_rate	= sta_info.transmit_bit_rate;
				}
			} else {
				pi->u.heart_beat2.child_info[i].down_tx_rate	= sta_info.transmit_bit_rate;
			}

		}
	}

	imcp_snip_send(AL_CONTEXT pi);

	if(pi->u.heart_beat2.child_count > 0) {
		al_heap_free(AL_CONTEXT pi->u.heart_beat2.child_info);
	}

	if(sta_list != NULL)
		al_heap_free(AL_CONTEXT sta_list);

	imcp_snip_release_packet(AL_CONTEXT pi);

	return RETURN_SUCCESS;
}


int _mesh_imcp_send_packet_supported_channels_info_helper(AL_CONTEXT_PARAM_DECL
	imcp_packet_info_t* pi,imcp_packet_info_t* pi_req,al_net_if_t* net_if)
{
	
	int			j,ret;
	al_channel_info_t	*al_channel_info;
	int			channel_count;
	al_802_11_operations_t*	operations;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
	operations	= net_if->get_extended_operations(net_if);
	if(operations != NULL) {
		ret = operations->get_supported_channels(net_if,&al_channel_info,&channel_count);
	
		pi->u.supported_channels_info.if_name_length = pi_req->u.supported_channels_request.if_name_length;
		strcpy(pi->u.supported_channels_info.if_name,pi_req->u.supported_channels_request.if_name);
		
		pi->u.supported_channels_info.channel_count = channel_count;

		pi->u.supported_channels_info.channels_info = (channels_info_t*)
			al_heap_alloc(AL_CONTEXT pi->u.supported_channels_info.channel_count * sizeof(channels_info_t) AL_HEAP_DEBUG_PARAM);
		
		for(j = 0;j < pi->u.supported_channels_info.channel_count;j++){
			pi->u.supported_channels_info.channels_info[j].frequency		= al_channel_info[j].frequency;
			pi->u.supported_channels_info.channels_info[j].number			= al_channel_info[j].number ;  
			pi->u.supported_channels_info.channels_info[j].flags			= al_channel_info[j].flags ;  
			pi->u.supported_channels_info.channels_info[j].max_power		= al_channel_info[j].max_power ;
			pi->u.supported_channels_info.channels_info[j].min_power		= al_channel_info[j].min_power ;
			pi->u.supported_channels_info.channels_info[j].max_reg_power	= al_channel_info[j].max_reg_power;
		}

		al_heap_free(AL_CONTEXT al_channel_info); 
        return RETURN_SUCCESS;
	}
	
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
	return RETURN_ERROR;
}

int mesh_imcp_send_packet_supported_channels_info(AL_CONTEXT_PARAM_DECL imcp_packet_info_t* pi_req)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
	int								i;
	imcp_packet_info_t*				pi;
	al_net_if_t*					net_if;
	int								ret_val;

	if(pi_req->u.supported_channels_request.if_name == NULL) {
		return RETURN_ERROR;
	}

	ret_val = RETURN_ERROR;

	pi	= imcp_snip_create_packet(AL_CONTEXT_SINGLE);

	pi->packet_type					= IMCP_SNIP_PACKET_TYPE_IF_SUPPORTED_CHANNELS_INFO;

	memcpy(&pi->u.supported_channels_info.ds_mac, _DS_MAC, sizeof(al_net_addr_t));

	if(!strcmp(mesh_hardware_info->ds_net_if->name,pi_req->u.supported_channels_request.if_name)) {
		ret_val = _mesh_imcp_send_packet_supported_channels_info_helper(pi,pi_req,mesh_hardware_info->ds_net_if); 
		goto send_packet;
	} 
	if(mesh_hardware_info->monitor_type != MONITOR_TYPE_ABSENT) { /*check for scanner*/
		if(!strcmp(_MON_NET_IF->name,pi_req->u.supported_channels_request.if_name)) {
			ret_val = _mesh_imcp_send_packet_supported_channels_info_helper(pi,pi_req,_MON_NET_IF);
			goto send_packet;
		}
	}

	for(i = 0; i < mesh_hardware_info->wm_net_if_count; i++) {
		net_if	= mesh_hardware_info->wms_conf[i]->net_if;
		if(net_if == NULL) {
			continue;
		}
		if(!strcmp(net_if->name,pi_req->u.supported_channels_request.if_name)) {
			ret_val = _mesh_imcp_send_packet_supported_channels_info_helper(pi,pi_req,net_if); 	
			break;
		}
	}	

send_packet:

	if(ret_val == RETURN_SUCCESS) {
		imcp_snip_send(AL_CONTEXT pi);
		al_heap_free(AL_CONTEXT pi->u.supported_channels_info.channels_info);
	}

	imcp_snip_release_packet(AL_CONTEXT pi);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
	return RETURN_SUCCESS;
}


int mesh_imcp_send_packet_buffering_command(AL_CONTEXT_PARAM_DECL al_net_if_t* net_if, unsigned char type,unsigned int timeout)
{
	imcp_packet_info_t*			pi;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
	pi	= imcp_snip_create_packet(AL_CONTEXT_SINGLE);

	pi->net_if						= net_if;
	pi->packet_type					= IMCP_SNIP_PACKET_TYPE_BUFFER_COMMAND;
	pi->u.buffer_command.type		= type;
	pi->u.buffer_command.timeout	= timeout;

	memcpy(&pi->u.buffer_command.ds_mac_src,_DS_MAC,sizeof(al_net_addr_t));

	switch(type) {
	case IMCP_SNIP_BUFFERING_TYPE_DISABLE_UPLINK:
		memcpy(&pi->u.buffer_command.ds_mac_dest,&broadcast_net_addr,sizeof(al_net_addr_t));
		break;
	default:
		memcpy(&pi->u.buffer_command.ds_mac_dest,&current_parent->ds_mac,sizeof(al_net_addr_t));
	}	

	imcp_snip_send(AL_CONTEXT pi);

	imcp_snip_release_packet(AL_CONTEXT pi);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
	return 0;
}

int	mesh_imcp_send_packet_downlink_saturation_info(AL_CONTEXT_PARAM_DECL al_net_if_t* net_if, int channel_count, al_duty_cycle_info_t* info)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
	imcp_packet_info_t*			pi;
	int							i,j;

	pi	= imcp_snip_create_packet(AL_CONTEXT_SINGLE);

	pi->packet_type	= IMCP_SNIP_PACKET_TYPE_DOWNLINK_SATURATION_INFO;
	
	memcpy(&pi->u.downlink_saturation.ds_mac_src,_DS_MAC,sizeof(al_net_addr_t));
	pi->u.downlink_saturation.net_if		= net_if;
	pi->u.downlink_saturation.channel_count	= channel_count;
	pi->u.downlink_saturation.channel_info	= al_heap_alloc(AL_CONTEXT sizeof(*pi->u.downlink_saturation.channel_info) * channel_count AL_HEAP_DEBUG_PARAM);
	memset(pi->u.downlink_saturation.channel_info,0,sizeof(*pi->u.downlink_saturation.channel_info) * channel_count);

	for(i = 0; i < channel_count; i++) {

		pi->u.downlink_saturation.channel_info[i].channel			= info[i].channel;
		pi->u.downlink_saturation.channel_info[i].ap_count			= info[i].number_of_aps;
		pi->u.downlink_saturation.channel_info[i].max_signal		= info[i].max_signal;
		pi->u.downlink_saturation.channel_info[i].avg_signal		= info[i].avg_signal;
		pi->u.downlink_saturation.channel_info[i].retry_duration	= info[i].retry_duration;
		pi->u.downlink_saturation.channel_info[i].transmit_duration	= info[i].transmit_duration;
		pi->u.downlink_saturation.channel_info[i].total_duration	= info[i].total_duration;
		
		pi->u.downlink_saturation.channel_info[i].ap_info			= al_heap_alloc(AL_CONTEXT sizeof(*pi->u.downlink_saturation.channel_info[i].ap_info) * info[i].number_of_aps AL_HEAP_DEBUG_PARAM);

		memset(pi->u.downlink_saturation.channel_info[i].ap_info,0,sizeof(*pi->u.downlink_saturation.channel_info[i].ap_info) * info[i].number_of_aps);

		for(j = 0; j < info[i].number_of_aps; j++) {
			memcpy(&pi->u.downlink_saturation.channel_info[i].ap_info[j].bssid,&info[i].aps[j].bssid,sizeof(al_net_addr_t));
			strcpy(pi->u.downlink_saturation.channel_info[i].ap_info[j].essid,info[i].aps[j].essid);
			pi->u.downlink_saturation.channel_info[i].ap_info[j].signal				= info[i].aps[j].signal;
			pi->u.downlink_saturation.channel_info[i].ap_info[j].retry_duration		= info[i].aps[j].retry_duration;
			pi->u.downlink_saturation.channel_info[i].ap_info[j].transmit_duration	= info[i].aps[j].transmit_duration;
		}
	}

	imcp_snip_send(AL_CONTEXT pi);

	for(i = 0; i < channel_count; i++) {
		al_heap_free(AL_CONTEXT pi->u.downlink_saturation.channel_info[i].ap_info);
	}

	al_heap_free(AL_CONTEXT pi->u.downlink_saturation.channel_info);
	
	imcp_snip_release_packet(AL_CONTEXT pi);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
	return 0;
}

int mesh_imcp_send_radio_diag_data(AL_CONTEXT_PARAM_DECL al_net_if_t* net_if, unsigned char* data, unsigned short data_length)
{
	imcp_packet_info_t*		pi;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
	pi	= imcp_snip_create_packet(AL_CONTEXT_SINGLE);

	pi->packet_type	= IMCP_SNIP_PACKET_TYPE_RADIO_DIAGNOSTIC_DATA;

	memcpy(&pi->u.radio_diagnostic_data.ds_mac,_DS_MAC,sizeof(al_net_addr_t));
	
	pi->u.radio_diagnostic_data.net_if	= net_if;
	
	pi->u.radio_diagnostic_data.data_length	= data_length;

	pi->u.radio_diagnostic_data.data	= data;

	imcp_snip_send(AL_CONTEXT pi);

	imcp_snip_release_packet(AL_CONTEXT pi);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
	return 0;
}

int mesh_imcp_send_packet_node_move_notification(AL_CONTEXT_PARAM_DECL al_net_addr_t* node_ds_mac)
{
	imcp_packet_info_t*		pi;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
	pi	= imcp_snip_create_packet(AL_CONTEXT_SINGLE);

	pi->packet_type	= IMCP_SNIP_PACKET_TYPE_NODE_MOVE_NOTIFICATION;

	memcpy(&pi->u.node_move_notification.ds_mac,_DS_MAC,sizeof(al_net_addr_t));
	memcpy(&pi->u.node_move_notification.node_ds_mac,node_ds_mac,sizeof(al_net_addr_t));

	imcp_snip_send(AL_CONTEXT pi);

	imcp_snip_release_packet(AL_CONTEXT pi);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
	return 0;
}

int	mesh_imcp_send_packet_channel_change(AL_CONTEXT_PARAM_DECL al_net_if_t* net_if, unsigned char new_channel, unsigned short millis_left)
{
	imcp_packet_info_t*		pi;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
	pi	= imcp_snip_create_packet(AL_CONTEXT_SINGLE);

	pi->packet_type	= IMCP_SNIP_PACKET_TYPE_CHANNEL_CHANGE;
	pi->net_if		= net_if;

	memcpy(&pi->u.channel_change_notification.ds_mac,_DS_MAC,sizeof(al_net_addr_t));
	pi->u.channel_change_notification.milli_seconds_left	= millis_left;
	pi->u.channel_change_notification.wm_channel			= new_channel;

	imcp_snip_send(AL_CONTEXT pi);

	imcp_snip_release_packet(AL_CONTEXT pi);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
	return 0;
}

int mesh_imcp_send_extern_packet(AL_CONTEXT_PARAM_DECL int imcp_packet_type, unsigned char* buffer, unsigned short buffer_length)
{
	imcp_packet_info_t*		pi;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
	/*
	 *	currently only sip packet is sent from external handler
	 */
	if(imcp_packet_type != IMCP_SNIP_PACKET_TYPE_SIP_PRIVATE)
		return -1;

	pi	= imcp_snip_create_packet(AL_CONTEXT_SINGLE);

	pi->packet_type	= imcp_packet_type;

	memcpy(&pi->u.sip_private.ds_mac,_DS_MAC,sizeof(al_net_addr_t));
	pi->u.sip_private.data_length	= buffer_length;
	memcpy(pi->u.sip_private.data, buffer, buffer_length);

	imcp_snip_send(AL_CONTEXT pi);

	imcp_snip_release_packet(AL_CONTEXT pi);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
	return 0;
}

int mesh_imcp_send_packet_buffering_command_ack(AL_CONTEXT_PARAM_DECL al_net_if_t* net_if, al_net_addr_t* ds_mac_dest)
{
	imcp_packet_info_t*		pi;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
	pi	= imcp_snip_create_packet(AL_CONTEXT_SINGLE);

	pi->packet_type	= IMCP_SNIP_PACKET_TYPE_BUFFER_COMMAND_ACK;
	pi->net_if		= net_if;
	pi->dest_addr	= ds_mac_dest;

	memcpy(&pi->u.buffer_command_ack.ds_mac_dest,ds_mac_dest,sizeof(al_net_addr_t));

	imcp_snip_send(AL_CONTEXT pi);

	imcp_snip_release_packet(AL_CONTEXT pi);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
	return 0;
}
