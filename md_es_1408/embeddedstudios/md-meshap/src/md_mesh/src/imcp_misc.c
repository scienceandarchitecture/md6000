
/********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : imcp_misc.c
 * Comments : Other Misc IMCP packet impl file.
 * Created  : 8/16/2004
 * Author   : Anand Bhange
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * -----------------------------------------------------------------------------
 * | No  |Date      |  Comment                                        | Author |
 * -----------------------------------------------------------------------------
 * | 20  |1/27/2009 | Changes for Uplink Scan Functionality           | Sriram |
 * -----------------------------------------------------------------------------
 * | 19  |8/22/2008 | Changes for SIP                                 |Abhijit |
 * -----------------------------------------------------------------------------
 * | 18  |12/4/2007 | Changes for Client Signal Information           | Sriram |
 * -----------------------------------------------------------------------------
 * | 17  |4/11/2007 | Changes for Radio diagnostics                   | Sriram |
 * -----------------------------------------------------------------------------
 * | 16  |11/14/2006| Changes for heap debug support                  | Sriram |
 * -----------------------------------------------------------------------------
 * | 15  |02/03/2006| generic command req/res changes                 | Abhijit|
 * -----------------------------------------------------------------------------
 * | 14  |01/01/2006| Changes for IMCP Analyser Init                  | Abhijit|
 * -----------------------------------------------------------------------------
 * | 13  |12/16/2005|supported channels changes					      |prachiti|
 * -----------------------------------------------------------------------------
 * | 12  |10/5/2005 | Typos fixed                                     | Sriram |
 * -----------------------------------------------------------------------------
 * | 11  |10/5/2005 | Misc changes                                    | Sriram |
 * -----------------------------------------------------------------------------
 * | 10  |10/4/2005 | Changes for IMCP WM Info                        | Sriram |
 * -----------------------------------------------------------------------------
 * |  9  |9/19/2005 | Added imcp_snip_ds_excerciser_get_buffer        | Sriram |
 * -----------------------------------------------------------------------------
 * |  8  |5/6/2005  | Model info added                                | Sriram |
 * -----------------------------------------------------------------------------
 * |  7  |2/14/2005 | Parameter Removed from mac checking func        | Anand  |
 * -----------------------------------------------------------------------------
 * |  6  |12/20/2004| Changes for phy_sub_type and HW-Info UDP bug    | Sriram |
 * -----------------------------------------------------------------------------
 * |  5  |12/15/2004| Fixed H/W Info packet length                    | Sriram |
 * -----------------------------------------------------------------------------
 * |  4  |11/30/2004| added op_channel,phy_type to hardware info      | Abhijit|
 * -----------------------------------------------------------------------------
 * |  3  |10/25/2004| req_res field added to mesh_imcp_key_update     | Abhijit|
 * -----------------------------------------------------------------------------
 * |  2  |10/11/2004| ap_hardware_info modified for IMCP5.0           | Abhijit|
 * -----------------------------------------------------------------------------
 * |  1  |10/11/2004| imcp_mesh_key_update function added             | Abhijit|
 * -----------------------------------------------------------------------------
 * |  0  |8/16/2004 | Created.                                        | Anand  |
 * -----------------------------------------------------------------------------
 ********************************************************************************/

static int imcp_snip_reset_get_buffer 	(AL_CONTEXT_PARAM_DECL imcp_packet_info_t* pi, unsigned char* buffer, unsigned int buf_length)
{
#define _REQ_LENGTH		10

	unsigned char*	p;
	unsigned short  cpu2le16;
	unsigned short  node_id;
	unsigned short  node_len;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
	AL_PRINT_LOG_FLOW_0( "MESH_AP	: imcp_snip_reset_get_buffer()");

	AL_ASSERT("MESH_AP	: imcp_snip_reset_get_buffer", buffer != NULL);
	AL_ASSERT("MESH_AP	: imcp_snip_reset_get_buffer", pi != NULL);

	/**
	 * Buffer length must be atleast req_length 
	 */

	if(buf_length < _REQ_LENGTH)
    {
        printk(KERN_EMERG" Func : %s Line : %d Expected_len : _REQ_LENGTH Received_len : %d\n",__func__,__LINE__,buf_length);
		return IMCP_SNIP_ERROR_INADEQUATE_BUFFER_LENGTH;
    }
	p	= buffer;

	node_id = IMCP_RESET_INFO_ID;
	node_len = _REQ_LENGTH - FIXED_ID_LEN;
	IMCP_MSG_SEND_ID_AND_LEN(p,node_id,node_len)

	/**
	 * Copy DS MAC, PHY ID
	 */
	memcpy(p,	&pi->u.reset_ap.bytes,	MAC_ADDR_LENGTH);	p += MAC_ADDR_LENGTH;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
	return _REQ_LENGTH;
#undef _REQ_LENGTH
}

static int imcp_snip_hardware_info_get_buffer 	(AL_CONTEXT_PARAM_DECL imcp_packet_info_t* pi, unsigned char* buffer, unsigned int buf_length)
{
	unsigned char*	p;
	int				i;
	unsigned char   model_len;
	unsigned int	req_length;
	unsigned short  node_id;
	unsigned short  node_len;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
	AL_PRINT_LOG_FLOW_0( "MESH_AP	: imcp_snip_hardware_info_get_buffer()");

	AL_ASSERT("MESH_AP	: imcp_snip_hardware_info_get_buffer", buffer != NULL);
	AL_ASSERT("MESH_AP	: imcp_snip_hardware_info_get_buffer", pi != NULL);

    	model_len =  strlen(pi->u.hardware_info.hardware_model);

	req_length = 16 + FIXED_ID_LEN +  pi->u.hardware_info.name_length + 
                        pi->u.hardware_info.ds_info.name_length +
                        pi->u.hardware_info.hardware_model_length;	


	for(i = 0;i < pi->u.hardware_info.wm_count;i++) {
		req_length += (10 + pi->u.hardware_info.wms_info[i].name_length);
	}

	if(mesh_hardware_info->monitor_type != MONITOR_TYPE_ABSENT) {
		req_length += (9 + pi->u.hardware_info.scanner_info.name_length);
	}

	/**
	 * Buffer length must be atleast req_length 
	 */

	if(buf_length < req_length)
    {
		 al_print_log(AL_CONTEXT AL_LOG_TYPE_DEBUG," ERROR : (%s) function : LINE (%d) :Receiving length (%d) is not Correct  \n",__func__,__LINE__,buf_length);
		return IMCP_SNIP_ERROR_INADEQUATE_BUFFER_LENGTH;
    }
	memset(buffer,0,buf_length);

	p	= buffer;

      node_id = IMCP_AP_HARDWARE_INFO_ID;
      node_len = req_length - FIXED_ID_LEN;
      IMCP_MSG_SEND_ID_AND_LEN(p,node_id,node_len)

	/**
	 * Copy Sender DS MAC, SQNR, CTC, HPC, ROOT bssid, CRSSI, HI, CH, PARENT Bssid
	 * Known AP count, 
	 */
	memcpy(p,	pi->u.hardware_info.ds_info.mac_addr.bytes,	MAC_ADDR_LENGTH);	p += MAC_ADDR_LENGTH;
	memcpy(p,	&pi->u.hardware_info.major_version,			1);					p += 1;
	memcpy(p,	&pi->u.hardware_info.minor_version,			1);					p += 1;
	memcpy(p,	&pi->u.hardware_info.variant_version,		1);					p += 1;
	memcpy(p,	&pi->u.hardware_info.name_length,			1);					p += 1;
	memcpy(p,	pi->u.hardware_info.name,					pi->u.hardware_info.name_length);			p += pi->u.hardware_info.name_length;
    memcpy(p,	&model_len,	                                1);					p += 1;
	memcpy(p,	pi->u.hardware_info.hardware_model,			model_len);
        p += model_len;
	memcpy(p,	&pi->u.hardware_info.ds_info.type,			1);					p += 1;
	memcpy(p,	&pi->u.hardware_info.ds_info.channel,		1);					p += 1;
	memcpy(p,	&pi->u.hardware_info.ds_info.name_length,	1);					p += 1;
	memcpy(p,	pi->u.hardware_info.ds_info.name,			pi->u.hardware_info.ds_info.name_length);	p += pi->u.hardware_info.ds_info.name_length;
	memcpy(p,	&pi->u.hardware_info.wm_count,				1);					p += 1;

	for(i = 0; i < pi->u.hardware_info.wm_count; i++) {
		memcpy(p,	pi->u.hardware_info.wms_info[i].mac_addr.bytes,	MAC_ADDR_LENGTH);					p += MAC_ADDR_LENGTH;
		memcpy(p,	&pi->u.hardware_info.wms_info[i].type,				1);					p += 1;
		memcpy(p,	&pi->u.hardware_info.wms_info[i].sub_type,			1);					p += 1;
		memcpy(p,	&pi->u.hardware_info.wms_info[i].channel,			1);					p += 1;
		memcpy(p,	&pi->u.hardware_info.wms_info[i].name_length,		1);					p += 1;
		memcpy(p,	pi->u.hardware_info.wms_info[i].name,pi->u.hardware_info.wms_info[i].name_length);	p += pi->u.hardware_info.wms_info[i].name_length;	
	}

	if(mesh_hardware_info->monitor_type != MONITOR_TYPE_ABSENT) {
		memcpy(p,	pi->u.hardware_info.scanner_info.mac_addr.bytes,	MAC_ADDR_LENGTH);	p += MAC_ADDR_LENGTH;
		memcpy(p,	&pi->u.hardware_info.scanner_info.type,				1);					p += 1;
		memcpy(p,	&pi->u.hardware_info.scanner_info.sub_type,			1);					p += 1;
		memcpy(p,	&pi->u.hardware_info.scanner_info.name_length,		1);					p += 1;
		memcpy(p,	pi->u.hardware_info.scanner_info.name,pi->u.hardware_info.scanner_info.name_length);	p += pi->u.hardware_info.scanner_info.name_length;
	}


   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
	return req_length;
}

static int imcp_snip_is_packet_for_ap(AL_CONTEXT_PARAM_DECL unsigned char* buffer, unsigned int buf_length)
{
#define _REQ_LENGTH		10	

//   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
	al_net_addr_t net_addr;
	unsigned char*	p;

	unsigned short node_id;
	unsigned short node_len;

	AL_PRINT_LOG_FLOW_0( "MESH_AP	: imcp_snip_reset_get_packet()");
	/**
	 * Buffer length must be atleast req_length 
	 */

	if(buf_length < _REQ_LENGTH)
	{
		printk(KERN_EMERG"Func : %s Line : %d Expected_len : _REQ_LENGTH Received_len : %d\n",__func__,__LINE__,buf_length);
		return IMCP_SNIP_ERROR_INADEQUATE_BUFFER_LENGTH;
	}

	 p	= buffer;

	IMCP_MSG_RECEIVE_ID_AND_LEN(p,node_id,node_len)
	/**
	 * Copy DS MAC, PHY ID
	 */

	//check only for Mac Addr (No need to parse other thing as configd doing that)
	memcpy(&net_addr.bytes,		p,	MAC_ADDR_LENGTH);	p += MAC_ADDR_LENGTH;
	net_addr.length = MAC_ADDR_LENGTH;
//   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
	if(AL_NET_ADDR_EQUAL(&(net_addr), _DS_MAC))
		return RETURN_SUCCESS;
	else
		return RETURN_ERROR;

	return _REQ_LENGTH;
#undef _REQ_LENGTH
}

static int imcp_snip_hardware_info_request_get_packet(AL_CONTEXT_PARAM_DECL unsigned char* buffer, unsigned int buf_length, imcp_packet_info_t* pi)
{
#define _REQ_LENGTH		6

	unsigned char*	p;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
	unsigned short node_id;
	unsigned short node_len;

	AL_PRINT_LOG_FLOW_0( "MESH_AP	: imcp_snip_hardware_info_request_get_packet()");
	AL_ASSERT("MESH_AP	: imcp_snip_hardware_info_request_get_packet", pi != NULL);
	AL_ASSERT("MESH_AP	: imcp_snip_hardware_info_request_get_packet", buffer != NULL);
	/**
	 * Buffer length must be atleast req_length 
	 */

	if(buf_length < _REQ_LENGTH)
	{
		printk(KERN_EMERG"Func : %s Line : %d Expected_len : _REQ_LENGTH Received_len : %d\n",__func__,__LINE__,buf_length);
		return IMCP_SNIP_ERROR_INADEQUATE_BUFFER_LENGTH;
	}
	p	= buffer;


	IMCP_MSG_RECEIVE_ID_AND_LEN(p,node_id,node_len)
	/**
	 * Copy DS MAC, PHY ID
	 */
	memcpy(&pi->u.hardware_info_request.ds_mac.bytes,	p,	MAC_ADDR_LENGTH);	p += MAC_ADDR_LENGTH;
	pi->u.hardware_info_request.ds_mac.length = MAC_ADDR_LENGTH;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
	return _REQ_LENGTH;
#undef _REQ_LENGTH
}


static int imcp_snip_mesh_init_status_get_buffer (AL_CONTEXT_PARAM_DECL imcp_packet_info_t* pi, unsigned char* buffer, unsigned int buf_length)
{
#define _REQ_LENGTH		8 + IMCP_ID_AND_LEN_SIZE //Size increased for ID and Length


	unsigned char*	p;
        unsigned short  node_id;
  	unsigned short  node_len;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);

	AL_PRINT_LOG_FLOW_0( "MESH_AP	: imcp_snip_mesh_init_status_get_buffer()");

	AL_ASSERT("MESH_AP	: imcp_snip_mesh_init_status_get_buffer", buffer != NULL);
	AL_ASSERT("MESH_AP	: imcp_snip_mesh_init_status_get_buffer", pi != NULL);

	/**
	 * Buffer length must be atleast req_length 
	 */

	if(buf_length < _REQ_LENGTH)
    {
		 al_print_log(AL_CONTEXT AL_LOG_TYPE_DEBUG," ERROR : (%s) function : LINE (%d) :Receiving length (%d) is not Correct  \n",__func__,__LINE__,buf_length);
		return IMCP_SNIP_ERROR_INADEQUATE_BUFFER_LENGTH;
    }
	p	= buffer;
        /**
         * Copy ID , LEN
         */
        node_id         =  IMCP_MESH_INIT_STATUS_INFO_ID;
        node_len        =  MESH_INIT_STATUS_FIXED_LEN +  pi->u.mesh_init_status.status_data_length +1 ;
        IMCP_MSG_SEND_ID_AND_LEN(p,node_id,node_len);

	/**
	 * Copy DS MAC, PHY ID
	 */
	memcpy(p,	&pi->u.mesh_init_status.ds_mac.bytes,		MAC_ADDR_LENGTH);
    	p += MAC_ADDR_LENGTH;
	memcpy(p,	&pi->u.mesh_init_status.status,				1);
	p += 1;
	memcpy(p,	&pi->u.mesh_init_status.status_data_length,	1);						
	p += 1;
	strcpy(p,	pi->u.mesh_init_status.status_data);							
	p += pi->u.mesh_init_status.status_data_length;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
	return _REQ_LENGTH + pi->u.mesh_init_status.status_data_length +1;
#undef _REQ_LENGTH
}

static int imcp_snip_mesh_imcp_key_update_get_packet(AL_CONTEXT_PARAM_DECL unsigned char* buffer, unsigned int buf_length, imcp_packet_info_t* pi)
{
#define _REQ_LENGTH		10

	unsigned char*	p;

	unsigned short node_id;
	unsigned short node_len;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
	AL_PRINT_LOG_FLOW_0( "MESH_AP	: imcp_snip_mesh_imcp_key_update_get_packet()");
	AL_ASSERT("MESH_AP	: imcp_snip_mesh_imcp_key_update_get_packet", pi != NULL);
	AL_ASSERT("MESH_AP	: imcp_snip_mesh_imcp_key_update_get_packet", buffer != NULL);

	/**
	 * Buffer length must be atleast req_length 
	 */

	if(buf_length < _REQ_LENGTH)
    {
		 al_print_log(AL_CONTEXT AL_LOG_TYPE_DEBUG," ERROR : (%s) function : LINE (%d) :Receiving length (%d) is not Correct  \n",__func__,__LINE__,buf_length);
		return IMCP_SNIP_ERROR_INADEQUATE_BUFFER_LENGTH;
    }
	p	= buffer;

        IMCP_MSG_RECEIVE_ID_AND_LEN(p,node_id,node_len)

	/**
	 * Copy DS MAC, MeshID and Key
	 */
	memcpy(&pi->u.mesh_imcp_key_update.ds_mac.bytes,	p,	MAC_ADDR_LENGTH);
       	p += MAC_ADDR_LENGTH;
 
	pi->u.mesh_imcp_key_update.ds_mac.length = MAC_ADDR_LENGTH;

	memcpy(&pi->u.mesh_imcp_key_update.response_id	,p,			1);	
	p += 1;

	memcpy(&pi->u.mesh_imcp_key_update.new_meshid_length,p,	1);
        					
	p += 1;
	memcpy(pi->u.mesh_imcp_key_update.new_meshid,p,pi->u.mesh_imcp_key_update.new_meshid_length); p += pi->u.mesh_imcp_key_update.new_meshid_length;
	pi->u.mesh_imcp_key_update.new_meshid[pi->u.mesh_imcp_key_update.new_meshid_length] = 0;

	memcpy(&pi->u.mesh_imcp_key_update.key_length ,p,	1);					p += 1;
	memcpy(pi->u.mesh_imcp_key_update.key ,p,pi->u.mesh_imcp_key_update.key_length ); p += pi->u.mesh_imcp_key_update.key_length;
	pi->u.mesh_imcp_key_update.key[pi->u.mesh_imcp_key_update.key_length] = 0;
	
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
	return _REQ_LENGTH + pi->u.mesh_imcp_key_update.new_meshid_length + pi->u.mesh_imcp_key_update.key_length;

#undef _REQ_LENGTH
}

static int imcp_snip_mesh_imcp_key_update_get_buffer 	(AL_CONTEXT_PARAM_DECL imcp_packet_info_t* pi, unsigned char* buffer, unsigned int buf_length)
{
#define _REQ_LENGTH		10 + IMCP_ID_AND_LEN_SIZE

	unsigned char*	p;
        unsigned short  node_id;
   	unsigned short  node_len;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
	AL_PRINT_LOG_FLOW_0( "MESH_AP	: imcp_snip_mesh_imcp_key_update_get_buffer()");

	AL_ASSERT("MESH_AP	: imcp_snip_mesh_imcp_key_update_get_buffer", buffer != NULL);
	AL_ASSERT("MESH_AP	: imcp_snip_mesh_imcp_key_update_get_buffer", pi != NULL);

	/**
	 * Buffer length must be atleast req_length 
	 */

	if(buf_length < _REQ_LENGTH)
    {
		 al_print_log(AL_CONTEXT AL_LOG_TYPE_DEBUG," ERROR : (%s) function : LINE (%d) :Receiving length (%d) is not Correct  \n",__func__,__LINE__,buf_length);
		return IMCP_SNIP_ERROR_INADEQUATE_BUFFER_LENGTH;
    }
	p	= buffer;
       
         /**
         * Copy ID, LEN
         */  
        node_id         =  IMCP_MESH_IMCP_KEY_UPDATE_INFO_ID;
        node_len        =  MESH_IMCP_KEY_UPDATE_FIXED_LEN + pi->u.mesh_imcp_key_update.new_meshid_length + pi->u.mesh_imcp_key_update.key_length;
        IMCP_MSG_SEND_ID_AND_LEN(p,node_id,node_len);


	/**
	 * Copy DS MAC, PHY ID
	 */
	memcpy(p,	&pi->u.mesh_imcp_key_update.ds_mac.bytes,		MAC_ADDR_LENGTH);
	p += MAC_ADDR_LENGTH;
	memcpy(p,	&pi->u.mesh_imcp_key_update.response_id ,					1);					

	p += 1;
	memcpy(p,	&pi->u.mesh_imcp_key_update.new_meshid_length  ,				1);					

	p += 1;
	memcpy(p,	&pi->u.mesh_imcp_key_update.new_meshid  ,	pi->u.mesh_imcp_key_update.new_meshid_length );

 	p += pi->u.mesh_imcp_key_update.new_meshid_length;
	pi->u.mesh_imcp_key_update.new_meshid[pi->u.mesh_imcp_key_update.new_meshid_length] = 0;
	memcpy(p,	&pi->u.mesh_imcp_key_update.key_length ,				1);						

        p += 1;
	memcpy(p,	&pi->u.mesh_imcp_key_update.key ,	pi->u.mesh_imcp_key_update.key_length);					
	
	p += pi->u.mesh_imcp_key_update.key_length;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
	return _REQ_LENGTH + pi->u.mesh_imcp_key_update.new_meshid_length + pi->u.mesh_imcp_key_update.key_length;

#undef _REQ_LENGTH

}

static int imcp_snip_ds_excerciser_get_buffer(AL_CONTEXT_PARAM_DECL imcp_packet_info_t* pi, unsigned char* buffer, unsigned int buf_length)
{
	unsigned char*	p;
	unsigned short	cpu2le;
        unsigned short  node_id;
   	unsigned short  node_len;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
	if(buf_length < pi->u.ds_excerciser.data_size + 2)
    {
		 al_print_log(AL_CONTEXT AL_LOG_TYPE_DEBUG," ERROR : (%s) function : LINE (%d) :Receiving length (%d) is not Correct  \n",__func__,__LINE__,buf_length);
		return IMCP_SNIP_ERROR_INADEQUATE_BUFFER_LENGTH;
    }
	p	= buffer;
        node_id         =  IMCP_DS_EXCERCISER_INFO_ID;
        node_len        =  HEARTBEAT2_INFO_FIXED_LEN+(pi->u.heart_beat2.child_count * 11);
        IMCP_MSG_SEND_ID_AND_LEN(p,node_id,node_len);

	cpu2le	= al_impl_cpu_to_le16(pi->u.ds_excerciser.data_size);
	memcpy(p,&cpu2le,2);	p += 2;

	memcpy(p,pi->u.ds_excerciser.data,pi->u.ds_excerciser.data_size);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
	return pi->u.ds_excerciser.data_size + 2;
}

static int imcp_snip_wm_info_request_get_buffer(AL_CONTEXT_PARAM_DECL imcp_packet_info_t* pi, unsigned char* buffer, unsigned int buf_length)
{
#define _REQ_LENGTH		20 + IMCP_ID_AND_LEN_SIZE //Size increased for ID and Length 

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
	unsigned char*	p;
        unsigned short  node_id;
	unsigned short  node_len;

	if(buf_length < _REQ_LENGTH)
    {
		 al_print_log(AL_CONTEXT AL_LOG_TYPE_DEBUG," ERROR : (%s) function : LINE (%d) :Receiving length (%d) is not Correct  \n",__func__,__LINE__,buf_length);
		return IMCP_SNIP_ERROR_INADEQUATE_BUFFER_LENGTH;
    }
	p	= buffer;
        node_id         = IMCP_WM_INFO_REQUEST_ID;
        node_len        = WM_INFO_REQUEST_FIXED_LEN;
        IMCP_MSG_SEND_ID_AND_LEN(p,node_id,node_len);


	memcpy(p,pi->u.wm_info_request.ds_mac_dest.bytes,MAC_ADDR_LENGTH);		p	+= MAC_ADDR_LENGTH;
	memcpy(p,pi->u.wm_info_request.ds_mac_src.bytes,MAC_ADDR_LENGTH);		p	+= MAC_ADDR_LENGTH;
	memcpy(p,pi->u.wm_info_request.wm_mac_dest.bytes,MAC_ADDR_LENGTH);		p	+= MAC_ADDR_LENGTH;
	memcpy(p,&pi->u.wm_info_request.ds_type,1);								p	+= 1;
	memcpy(p,&pi->u.wm_info_request.ds_sub_type,1);							p	+= 1;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
	return (p - buffer);

#undef _REQ_LENGTH
}

static int imcp_snip_wm_info_request_get_packet(AL_CONTEXT_PARAM_DECL unsigned char* buffer, unsigned int buf_length, imcp_packet_info_t* pi)
{
#define _REQ_LENGTH		20

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
	unsigned char*	p;

	unsigned short node_id;
	unsigned short node_len;

	if(buf_length < _REQ_LENGTH)
    {
		 al_print_log(AL_CONTEXT AL_LOG_TYPE_DEBUG," ERROR : (%s) function : LINE (%d) :Receiving length (%d) is not Correct  \n",__func__,__LINE__,buf_length);
		return IMCP_SNIP_ERROR_INADEQUATE_BUFFER_LENGTH;
    }
	p	= buffer;

        IMCP_MSG_RECEIVE_ID_AND_LEN(p,node_id,node_len)


	memcpy(pi->u.wm_info_request.ds_mac_dest.bytes,p,MAC_ADDR_LENGTH);		p	+= MAC_ADDR_LENGTH;
	pi->u.wm_info_request.ds_mac_dest.length	= MAC_ADDR_LENGTH;
	memcpy(pi->u.wm_info_request.ds_mac_src.bytes,p,MAC_ADDR_LENGTH);		p	+= MAC_ADDR_LENGTH;
	pi->u.wm_info_request.ds_mac_src.length	= MAC_ADDR_LENGTH;
	memcpy(pi->u.wm_info_request.wm_mac_dest.bytes,p,MAC_ADDR_LENGTH);		p	+= MAC_ADDR_LENGTH;
	pi->u.wm_info_request.wm_mac_dest.length	= MAC_ADDR_LENGTH;
	memcpy(&pi->u.wm_info_request.ds_type,p,1);								p	+= 1;
	memcpy(&pi->u.wm_info_request.ds_sub_type,p,1);							p	+= 1;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
	return _REQ_LENGTH;

#undef _REQ_LENGTH
}

static int imcp_snip_wm_info_response_get_buffer(AL_CONTEXT_PARAM_DECL imcp_packet_info_t* pi, unsigned char* buffer, unsigned int buf_length)
{

	unsigned char*	p;
	int				i;
	unsigned short	sig;
        unsigned short  node_id;
	unsigned short  node_len;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
	p	= buffer;
        node_id         =  IMCP_WM_INFO_RESPONSE_ID ;
        node_len        =  WM_INFO_RESPONSE_FIXED_LEN + pi->u.wm_info_response.essid_length + (3 +(8 * pi->u.wm_info_response.other_wm_count)) ;
        for(i = 0; i < pi->u.wm_info_response.other_wm_count; i++) {
        node_len +=  pi->u.wm_info_response.other_wm_info[i].essid_length; 
        }
	    if(buf_length < (node_len + IMCP_ID_AND_LEN_SIZE))
        {
		 al_print_log(AL_CONTEXT AL_LOG_TYPE_DEBUG," ERROR : (%s) function : LINE (%d) :Receiving length (%d) is not Correct  \n",__func__,__LINE__,buf_length);
		return IMCP_SNIP_ERROR_INADEQUATE_BUFFER_LENGTH;
        }
        IMCP_MSG_SEND_ID_AND_LEN(p,node_id,node_len);

	sig	= 0xABCD;

	memcpy(p,pi->u.wm_info_response.ds_mac_dest.bytes,MAC_ADDR_LENGTH);				p	+= MAC_ADDR_LENGTH;
	memcpy(p,pi->u.wm_info_response.ds_mac_src.bytes,MAC_ADDR_LENGTH);				p	+= MAC_ADDR_LENGTH;
	memcpy(p,&pi->u.wm_info_response.wm_type,1);									p	+= 1;
	memcpy(p,&pi->u.wm_info_response.wm_sub_type,1);								p	+= 1;
	memcpy(p,pi->u.wm_info_response.wm_addr.bytes,MAC_ADDR_LENGTH);					p	+= MAC_ADDR_LENGTH;
	memcpy(p,&pi->u.wm_info_response.wm_channel,1);									p	+= 1;
	memcpy(p,&pi->u.wm_info_response.essid_length,1);								p	+= 1;
	memcpy(p,pi->u.wm_info_response.essid,pi->u.wm_info_response.essid_length);	p	+= pi->u.wm_info_response.essid_length;

	if(pi->u.wm_info_response.other_wm_count > 0) {
		memcpy(p,&sig,2);															p	+= 2;
		memcpy(p,&pi->u.wm_info_response.other_wm_count,1);							p	+= 1;
		for(i = 0; i < pi->u.wm_info_response.other_wm_count; i++) {
			memcpy(p,pi->u.wm_info_response.other_wm_info[i].wm_addr.bytes,MAC_ADDR_LENGTH);	p	+= MAC_ADDR_LENGTH;
			memcpy(p,&pi->u.wm_info_response.other_wm_info[i].wm_channel,1);					p	+= 1;
			memcpy(p,&pi->u.wm_info_response.other_wm_info[i].essid_length,1);					p	+= 1;
			memcpy(p,pi->u.wm_info_response.other_wm_info[i].essid,pi->u.wm_info_response.other_wm_info[i].essid_length);	p	+= pi->u.wm_info_response.other_wm_info[i].essid_length;
		}
	}
    else
    {
		memset(p,0,2);															p	+= 2;
        *p=0;                                                                   p   += 1;
    }

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
	return (p - buffer);

#undef _REQ_LENGTH

}

static int imcp_snip_wm_info_response_get_packet(AL_CONTEXT_PARAM_DECL unsigned char* buffer, unsigned int buf_length, imcp_packet_info_t* pi)
{
#define _REQ_LENGTH		22

	unsigned char*	p;
	int				i;
	unsigned short	sig;

	unsigned short node_id;
	unsigned short node_len;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
	if(buf_length < _REQ_LENGTH)
    {
		 al_print_log(AL_CONTEXT AL_LOG_TYPE_DEBUG," ERROR : (%s) function : LINE (%d) :Receiving length (%d) is not Correct  \n",__func__,__LINE__,buf_length);
		return IMCP_SNIP_ERROR_INADEQUATE_BUFFER_LENGTH;
    }
	p	= buffer;

        IMCP_MSG_RECEIVE_ID_AND_LEN(p,node_id,node_len)


	memcpy(pi->u.wm_info_response.ds_mac_dest.bytes,p,MAC_ADDR_LENGTH);				p	+= MAC_ADDR_LENGTH;
	pi->u.wm_info_response.ds_mac_dest.length	= MAC_ADDR_LENGTH;
	memcpy(pi->u.wm_info_response.ds_mac_src.bytes,p,MAC_ADDR_LENGTH);				p	+= MAC_ADDR_LENGTH;
	pi->u.wm_info_response.ds_mac_src.length	= MAC_ADDR_LENGTH;
	memcpy(&pi->u.wm_info_response.wm_type,p,1);									p	+= 1;
	memcpy(&pi->u.wm_info_response.wm_sub_type,p,1);								p	+= 1;
	memcpy(pi->u.wm_info_response.wm_addr.bytes,p,MAC_ADDR_LENGTH);					p	+= MAC_ADDR_LENGTH;
	pi->u.wm_info_response.wm_addr.length	= MAC_ADDR_LENGTH;
	memcpy(&pi->u.wm_info_response.wm_channel,p,1);									p	+= 1;
	memcpy(&pi->u.wm_info_response.essid_length,p,1);								p	+= 1;
	memcpy(pi->u.wm_info_response.essid,p,pi->u.wm_info_response.essid_length);		p	+= pi->u.wm_info_response.essid_length;
	pi->u.wm_info_response.essid[pi->u.wm_info_response.essid_length] = 0;
	memcpy(&sig,p,2);																p	+= 2;

	if(sig == 0xABCD) {
		pi->u.wm_info_response.other_wm_count	= *p;								p	+= 1;
		al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,
			         "MESH_AP : Received extended WM Info response (%d) from "AL_NET_ADDR_STR,
					 pi->u.wm_info_response.other_wm_count,
					 AL_NET_ADDR_TO_STR(&pi->u.wm_info_response.ds_mac_src));

		pi->u.wm_info_response.other_wm_info	= al_heap_alloc(AL_CONTEXT sizeof(*pi->u.wm_info_response.other_wm_info) * pi->u.wm_info_response.other_wm_count AL_HEAP_DEBUG_PARAM);
		memset(pi->u.wm_info_response.other_wm_info,0,sizeof(*pi->u.wm_info_response.other_wm_info) * pi->u.wm_info_response.other_wm_count);

		for(i = 0; i < pi->u.wm_info_response.other_wm_count; i++) {
			memcpy(pi->u.wm_info_response.other_wm_info[i].wm_addr.bytes,p,MAC_ADDR_LENGTH);					p	+= MAC_ADDR_LENGTH;
			pi->u.wm_info_response.other_wm_info[i].wm_addr.length	= MAC_ADDR_LENGTH;
			memcpy(&pi->u.wm_info_response.other_wm_info[i].wm_channel,p,1);									p	+= 1;
			memcpy(&pi->u.wm_info_response.other_wm_info[i].essid_length,p,1);								p	+= 1;
			memcpy(pi->u.wm_info_response.other_wm_info[i].essid,p,pi->u.wm_info_response.other_wm_info[i].essid_length);	p	+= pi->u.wm_info_response.other_wm_info[i].essid_length;
			pi->u.wm_info_response.other_wm_info[i].essid[pi->u.wm_info_response.other_wm_info[i].essid_length] = 0;
		}
	} else {
		pi->u.wm_info_response.other_wm_count = 0;
	}

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
	return (p - buffer);

#undef _REQ_LENGTH
}

static int imcp_snip_tester_testee_mode_request_get_packet	(AL_CONTEXT_PARAM_DECL unsigned char* buffer, unsigned int buf_length, imcp_packet_info_t* pi)
{
#define _REQ_LENGTH		19

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
	unsigned char*	p;

	unsigned short node_id;
	unsigned short node_len;

	AL_PRINT_LOG_FLOW_0( "MESH_AP	: imcp_snip_tester_testee_mode_request_get_packet()");
	AL_ASSERT("MESH_AP	: imcp_snip_tester_testee_mode_request_get_packet", pi != NULL);
	AL_ASSERT("MESH_AP	: imcp_snip_tester_testee_mode_request_get_packet", buffer != NULL);

	/**
	 * Buffer length must be atleast req_length 
	 */

	if(buf_length < _REQ_LENGTH)
    {
		 al_print_log(AL_CONTEXT AL_LOG_TYPE_DEBUG," ERROR : (%s) function : LINE (%d) :Receiving length (%d) is not Correct  \n",__func__,__LINE__,buf_length);
		return IMCP_SNIP_ERROR_INADEQUATE_BUFFER_LENGTH;
    }
	p	= buffer;

        IMCP_MSG_RECEIVE_ID_AND_LEN(p,node_id,node_len)


	/**
	 * Copy DS MAC, peer mac
	 */
	memcpy(&pi->u.mesh_imcp_tester_testee_mode_req.ds_mac.bytes,	p,	MAC_ADDR_LENGTH);	p += MAC_ADDR_LENGTH;
	pi->u.mesh_imcp_tester_testee_mode_req.ds_mac.length = MAC_ADDR_LENGTH;

	memcpy(&pi->u.mesh_imcp_tester_testee_mode_req.mode			,	p,				1);		p += 1;

	memcpy(&pi->u.mesh_imcp_tester_testee_mode_req.peer_mac.bytes,	p,	MAC_ADDR_LENGTH);	p += MAC_ADDR_LENGTH;
	pi->u.mesh_imcp_tester_testee_mode_req.peer_mac.length = MAC_ADDR_LENGTH;

	pi->u.mesh_imcp_tester_testee_mode_req.interface_index			= *p;					p += 1;
	pi->u.mesh_imcp_tester_testee_mode_req.link_interface_index		= *p;					p += 1;
	pi->u.mesh_imcp_tester_testee_mode_req.channel					= *p;					p += 1;
	pi->u.mesh_imcp_tester_testee_mode_req.analysis_interval		= *p;					p += 1;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
	return _REQ_LENGTH;

#undef _REQ_LENGTH
}

static int imcp_snip_if_get_supported_channels_get_buffer(AL_CONTEXT_PARAM_DECL imcp_packet_info_t* pi, unsigned char* buffer, unsigned int buf_length)
{
	unsigned char*	p;
	int				i,cpu2le;
	unsigned int	req_length;
        unsigned short  node_id;
	unsigned short  node_len;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);

	AL_PRINT_LOG_FLOW_0( "MESH_AP	: imcp_snip_if_get_supported_channels_get_buffer()");

	AL_ASSERT("MESH_AP	: imcp_snip_if_get_supported_channels_get_buffer", buffer != NULL);
	AL_ASSERT("MESH_AP	: imcp_snip_if_get_supported_channels_get_buffer", pi != NULL);
        req_length = pi->u.supported_channels_info.if_name_length + 8 + IMCP_ID_AND_LEN_SIZE;  /* 6(MAC_ADDR) + 1(IF_NAME_LEN) + 1(CH_COUNT) + 4 (id + len)*/

	
	req_length += (12 * pi->u.supported_channels_info.channel_count);
	

	/**
	 * Buffer length must be atleast req_length 
	 */

	if(buf_length < req_length)
    {
		 al_print_log(AL_CONTEXT AL_LOG_TYPE_DEBUG," ERROR : (%s) function : LINE (%d) :Receiving length (%d) is not Correct  \n",__func__,__LINE__,buf_length);
		return IMCP_SNIP_ERROR_INADEQUATE_BUFFER_LENGTH;
    }
	memset(buffer,0,buf_length);

	p	= buffer;
        node_id         =  IMCP_IF_SUPPORTED_CHANNELS_INFO_ID;
        node_len        =  IF_SUPPORTED_CHANNELS_INFO_FIXED_LEN + pi->u.supported_channels_info.if_name_length + (12 * pi->u.supported_channels_info.channel_count);
        IMCP_MSG_SEND_ID_AND_LEN(p,node_id,node_len);


	memcpy(p,pi->u.supported_channels_info.ds_mac.bytes,	MAC_ADDR_LENGTH);
	p += MAC_ADDR_LENGTH;

	memcpy(p,&pi->u.supported_channels_info.if_name_length,1);
	p += 1;

	memcpy(p,pi->u.supported_channels_info.if_name,pi->u.supported_channels_info.if_name_length);
	p += pi->u.supported_channels_info.if_name_length;

    memcpy(p,&pi->u.supported_channels_info.channel_count,1);
	p += 1;

	for(i = 0; i < pi->u.supported_channels_info.channel_count; i++) {
		memcpy(p,&pi->u.supported_channels_info.channels_info[i].number,1);
		p += 1;
		
		/*TODO prachiti: al_cpu32_to_le*/
		cpu2le	= al_impl_cpu_to_le32(pi->u.supported_channels_info.channels_info[i].frequency);
		memcpy(p,&cpu2le,4);	p += 4;

		
		cpu2le	= al_impl_cpu_to_le32(pi->u.supported_channels_info.channels_info[i].flags);
		memcpy(p,&cpu2le,4);	p += 4;
		
		memcpy(p,&pi->u.supported_channels_info.channels_info[i].max_reg_power,1);
		p += 1;

		memcpy(p,&pi->u.supported_channels_info.channels_info[i].max_power,1);
		p += 1;
		
		memcpy(p,&pi->u.supported_channels_info.channels_info[i].min_power,1);
		p += 1;
	}

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
	return req_length;
}

static int imcp_snip_supported_channels_request_get_packet(AL_CONTEXT_PARAM_DECL unsigned char* buffer, unsigned int buf_length, imcp_packet_info_t* pi)
{
#define _REQ_LENGTH		6

//   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
	unsigned char*	p;

	unsigned short node_id;
	unsigned short node_len;

	AL_PRINT_LOG_FLOW_0( "MESH_AP	: imcp_snip_get_supported_channels_request_get_packet()");
	AL_ASSERT("MESH_AP	: imcp_snip_get_supported_channels_request_get_packet", pi != NULL);
	AL_ASSERT("MESH_AP	: imcp_snip_get_supported_channels_request_get_packet", buffer != NULL);
	/**
	 * Buffer length must be atleast req_length 
	 */

	if(buf_length < _REQ_LENGTH)
    {
		 al_print_log(AL_CONTEXT AL_LOG_TYPE_DEBUG," ERROR : (%s) function : LINE (%d) :Receiving length (%d) is not Correct  \n",__func__,__LINE__,buf_length);
		return IMCP_SNIP_ERROR_INADEQUATE_BUFFER_LENGTH;
    }
	p	= buffer;

        IMCP_MSG_RECEIVE_ID_AND_LEN(p,node_id,node_len)


	/**
	 * Copy DS MAC
	 */
	memcpy(&pi->u.supported_channels_request.ds_mac.bytes,p,MAC_ADDR_LENGTH);	p += MAC_ADDR_LENGTH;
	pi->u.supported_channels_request.ds_mac.length = MAC_ADDR_LENGTH;
	
	memcpy(&pi->u.supported_channels_request.if_name_length,p,1);
	p += 1;
	
	
	memcpy(pi->u.supported_channels_request.if_name,p,pi->u.supported_channels_request.if_name_length);
	p += pi->u.supported_channels_request.if_name_length;
	
	pi->u.supported_channels_request.if_name[pi->u.supported_channels_request.if_name_length] = 0;

 //  al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
	return _REQ_LENGTH;
#undef _REQ_LENGTH
}


static int imcp_snip_generic_command_req_res_get_packet(AL_CONTEXT_PARAM_DECL unsigned char* buffer, unsigned int buf_length, imcp_packet_info_t* pi)
{
	unsigned char*	p;
	unsigned short	le2cpu16;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
	unsigned short node_id;
	unsigned short node_len;

#define _REQ_LENGTH		9

	AL_PRINT_LOG_FLOW_0( "MESH_AP	: imcp_snip_generic_command_req_res_get_packet()");
	AL_ASSERT("MESH_AP	: imcp_snip_generic_command_req_res_get_packet", pi != NULL);
	AL_ASSERT("MESH_AP	: imcp_snip_generic_command_req_res_get_packet", buffer != NULL);
	/**
	 * Buffer length must be atleast req_length 
	 */

	if(buf_length < _REQ_LENGTH)
    {
		 al_print_log(AL_CONTEXT AL_LOG_TYPE_DEBUG," ERROR : (%s) function : LINE (%d) :Receiving length (%d) is not Correct  \n",__func__,__LINE__,buf_length);
		return IMCP_SNIP_ERROR_INADEQUATE_BUFFER_LENGTH;
    }
	p = buffer;

        IMCP_MSG_RECEIVE_ID_AND_LEN(p,node_id,node_len)


	/**
	 * Copy Sender DS MAC,request/response ,type ,response id
	 */
	memcpy(pi->u.generic_command.ds_mac.bytes,		p,		MAC_ADDR_LENGTH);	p += MAC_ADDR_LENGTH;
	pi->u.generic_command.ds_mac.length = MAC_ADDR_LENGTH;
	memcpy(&le2cpu16,		p,		2);						p += 2;
	pi->u.generic_command.type = al_impl_le16_to_cpu(le2cpu16);
	pi->u.generic_command.req_res				  = *p;							p += 1;
	
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
	return p-buffer;

#undef _REQ_LENGTH
}

static int imcp_snip_generic_command_req_res_get_buffer(AL_CONTEXT_PARAM_DECL imcp_packet_info_t* pi, unsigned char* buffer, unsigned int buf_length)
{
#define _REQ_LENGTH		9 + IMCP_ID_AND_LEN_SIZE //Size increased for ID and Length

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
	unsigned char*	p;
	unsigned short	cpu2le16;
        unsigned short  node_id;
  	unsigned short  node_len;

	if(buf_length < _REQ_LENGTH)
    {
		 al_print_log(AL_CONTEXT AL_LOG_TYPE_DEBUG," ERROR : (%s) function : LINE (%d) :Receiving length (%d) is not Correct  \n",__func__,__LINE__,buf_length);
		return IMCP_SNIP_ERROR_INADEQUATE_BUFFER_LENGTH;
    }
	p	= buffer;
        
        node_id         =  IMCP_GENERIC_COMMAND_REQ_RES_INFO_ID;
        node_len        =  GENERIC_COMMAND_REQ_RES_INFO_FIXED_LEN;
        IMCP_MSG_SEND_ID_AND_LEN(p,node_id,node_len);

	memcpy(p,pi->u.generic_command.ds_mac.bytes,MAC_ADDR_LENGTH);		
	p	+= MAC_ADDR_LENGTH;
	cpu2le16 = al_impl_cpu_to_le16(pi->u.generic_command.type);
	memcpy(p,&cpu2le16,2);				
	p	+= 2;
	*p	= pi->u.generic_command.req_res;								
	p	+= 1;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
	return (p - buffer);

#undef _REQ_LENGTH

}

static int imcp_snip_buffer_command_get_packet(AL_CONTEXT_PARAM_DECL unsigned char* buffer, unsigned int buf_length, imcp_packet_info_t* pi)
{
#define _REQ_LENGTH		17

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
	unsigned char*	p;
	unsigned int	le2cpu32;

	unsigned short node_id;
	unsigned short node_len;

	if(buf_length < _REQ_LENGTH)
    {
		 al_print_log(AL_CONTEXT AL_LOG_TYPE_DEBUG," ERROR : (%s) function : LINE (%d) :Receiving length (%d) is not Correct  \n",__func__,__LINE__,buf_length);
		return IMCP_SNIP_ERROR_INADEQUATE_BUFFER_LENGTH;
    }
	p	= buffer;

        IMCP_MSG_RECEIVE_ID_AND_LEN(p,node_id,node_len)

	pi->u.buffer_command.ds_mac_dest.length	= MAC_ADDR_LENGTH;
	pi->u.buffer_command.ds_mac_src.length	= MAC_ADDR_LENGTH;

	memcpy(pi->u.buffer_command.ds_mac_dest.bytes,p,MAC_ADDR_LENGTH);		p	+= MAC_ADDR_LENGTH;
	memcpy(pi->u.buffer_command.ds_mac_src.bytes,p,MAC_ADDR_LENGTH);		p	+= MAC_ADDR_LENGTH;
	pi->u.buffer_command.type		= *p;									p	+= 1;
	memcpy(&le2cpu32,p,4);													p	+= 4;
	pi->u.buffer_command.timeout	= al_impl_le32_to_cpu(le2cpu32);

	

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
	return (p - buffer);

#undef _REQ_LENGTH
}

static int imcp_snip_buffer_command_get_buffer(AL_CONTEXT_PARAM_DECL imcp_packet_info_t* pi, unsigned char* buffer, unsigned int buf_length)
{
#define _REQ_LENGTH		17 + + IMCP_ID_AND_LEN_SIZE //Size increased for ID and Length


   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
	unsigned char*	p;
	unsigned int	cpu2le32;
        unsigned short  node_id;
  	unsigned short  node_len;

	if(buf_length < _REQ_LENGTH)
    {
		 al_print_log(AL_CONTEXT AL_LOG_TYPE_DEBUG," ERROR : (%s) function : LINE (%d) :Receiving length (%d) is not Correct  \n",__func__,__LINE__,buf_length);
		return IMCP_SNIP_ERROR_INADEQUATE_BUFFER_LENGTH;
    }
	p	= buffer;
        node_id         =  IMCP_BUFFER_COMMAND_INFO_ID;
        node_len        =  BUFFER_COMMAND_FIXED_LEN;
        IMCP_MSG_SEND_ID_AND_LEN(p,node_id,node_len);


	memcpy(p,pi->u.buffer_command.ds_mac_dest.bytes,MAC_ADDR_LENGTH);		p	+= MAC_ADDR_LENGTH;
	memcpy(p,pi->u.buffer_command.ds_mac_src.bytes,MAC_ADDR_LENGTH);		p	+= MAC_ADDR_LENGTH;
	*p								= pi->u.buffer_command.type;			p	+= 1;
	cpu2le32						= al_impl_cpu_to_le32(pi->u.buffer_command.timeout);
	memcpy(p,&cpu2le32,4);													p	+= 4;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
	return (p - buffer);

#undef _REQ_LENGTH
}

static int imcp_snip_downlink_saturation_get_buffer(AL_CONTEXT_PARAM_DECL imcp_packet_info_t* pi, unsigned char* buffer, unsigned int buf_length)
{
#define _REQ_LENGTH		8+ IMCP_ID_AND_LEN_SIZE //Size increased for ID and Length

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
	unsigned char*	p;
	int				i,j;
	unsigned int	temp;
        unsigned short  node_id;
	unsigned short  node_len;

	if(buf_length < _REQ_LENGTH)
    {
		 al_print_log(AL_CONTEXT AL_LOG_TYPE_DEBUG," ERROR : (%s) function : LINE (%d) :Receiving length (%d) is not Correct  \n",__func__,__LINE__,buf_length);
		return IMCP_SNIP_ERROR_INADEQUATE_BUFFER_LENGTH;
    }
	p		= buffer;
        node_id         =  IMCP_DOWNLINK_SATURATION_INFO_ID;
        node_len        =  DOWNLINK_SATURATION_INFO_FIXED_LEN+ (pi->u.downlink_saturation.channel_count * (16 +16+ temp));
        IMCP_MSG_SEND_ID_AND_LEN(p,node_id,node_len);

  	
	memcpy(p,pi->u.downlink_saturation.ds_mac_src.bytes,MAC_ADDR_LENGTH);	
	p	+= MAC_ADDR_LENGTH;
	temp	= strlen(pi->u.downlink_saturation.net_if->name);
	*p		= temp;															p	+= 1;
	memcpy(p,pi->u.downlink_saturation.net_if->name,temp);					p	+= temp;
	*p		= pi->u.downlink_saturation.channel_count;						p	+= 1;
	
	for(i = 0; i < pi->u.downlink_saturation.channel_count; i++) {
		
		*p	= pi->u.downlink_saturation.channel_info[i].channel;			p	+= 1;
		*p	= pi->u.downlink_saturation.channel_info[i].ap_count;			p	+= 1;
		*p	= pi->u.downlink_saturation.channel_info[i].max_signal;			p	+= 1;
		*p	= pi->u.downlink_saturation.channel_info[i].avg_signal;			p	+= 1;

		temp	= al_cpu_to_le32(pi->u.downlink_saturation.channel_info[i].total_duration);
		memcpy(p,&temp,4);													p	+= 4;

		temp	= al_cpu_to_le32(pi->u.downlink_saturation.channel_info[i].retry_duration);
		memcpy(p,&temp,4);													p	+= 4;

		temp	= al_cpu_to_le32(pi->u.downlink_saturation.channel_info[i].transmit_duration);
		memcpy(p,&temp,4);													p	+= 4;

		for(j = 0; j < pi->u.downlink_saturation.channel_info[i].ap_count; j++) {

			memcpy(p,pi->u.downlink_saturation.channel_info[i].ap_info[j].bssid.bytes,MAC_ADDR_LENGTH); p	+= MAC_ADDR_LENGTH;

			temp	= strlen(pi->u.downlink_saturation.channel_info[i].ap_info[j].essid);
			*p		= temp;																				p	+= 1;
			memcpy(p,pi->u.downlink_saturation.channel_info[i].ap_info[j].essid,temp);					p	+= temp;

			*p		= pi->u.downlink_saturation.channel_info[i].ap_info[j].signal;						p	+= 1;

			temp	= al_cpu_to_le32(pi->u.downlink_saturation.channel_info[i].ap_info[j].retry_duration);
			memcpy(p,&temp,4);																			p	+= 4;

			temp	= al_cpu_to_le32(pi->u.downlink_saturation.channel_info[i].ap_info[j].transmit_duration);
			memcpy(p,&temp,4);																			p	+= 4;

		}

	}


   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
	return (p - buffer);

#undef _REQ_LENGTH
}

static int imcp_snip_reboot_required_get_packet(AL_CONTEXT_PARAM_DECL unsigned char* buffer, unsigned int buf_length, imcp_packet_info_t* pi)
{
#define _REQ_LENGTH             10	

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
	unsigned char*	p;
	unsigned short  temp;   	
	unsigned short  le2cpu16;
	unsigned short  node_id;
        unsigned short  node_len;

	if(buf_length < _REQ_LENGTH){
		 al_print_log(AL_CONTEXT AL_LOG_TYPE_DEBUG," ERROR : (%s) function : LINE (%d) :Receiving length (%d) is not Correct  \n",__func__,__LINE__,buf_length);
		return IMCP_SNIP_ERROR_INADEQUATE_BUFFER_LENGTH;
	}
	p	= buffer;

        IMCP_MSG_RECEIVE_ID_AND_LEN(p,node_id,node_len);


	pi->u.reboot_required.ds_mac_dest.length	= MAC_ADDR_LENGTH;
	memcpy(pi->u.reboot_required.ds_mac_dest.bytes,p,MAC_ADDR_LENGTH);		p	+= MAC_ADDR_LENGTH;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
	return (p - buffer);

#undef _REQ_LENGTH
}


static int imcp_snip_radio_diag_data_get_buffer(AL_CONTEXT_PARAM_DECL imcp_packet_info_t* pi, unsigned char* buffer, unsigned int buf_length)
{
#define _REQ_LENGTH		9 + IMCP_ID_AND_LEN_SIZE //Size increased for ID and Length 

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
	unsigned char*	p;
	unsigned short	temp;
        unsigned short  node_id;
 	unsigned short  node_len;

	if(buf_length < _REQ_LENGTH)
    {
		 al_print_log(AL_CONTEXT AL_LOG_TYPE_DEBUG," ERROR : (%s) function : LINE (%d) :Receiving length (%d) is not Correct  \n",__func__,__LINE__,buf_length);
		return IMCP_SNIP_ERROR_INADEQUATE_BUFFER_LENGTH;
    }
	p		= buffer;
        node_id         =  IMCP_RADIO_DIAGNOSTIC_DATA_ID;
        node_len        =  RADIO_DIAGNOSTIC_DATA_FIXED_LEN + pi->u.radio_diagnostic_data.data_length;
        IMCP_MSG_SEND_ID_AND_LEN(p,node_id,node_len);

	memcpy(p,pi->u.radio_diagnostic_data.ds_mac.bytes,MAC_ADDR_LENGTH);	
	p	+= MAC_ADDR_LENGTH;
	temp	= strlen(pi->u.radio_diagnostic_data.net_if->name);
	*p		= temp;														p	+= 1;
	memcpy(p,pi->u.radio_diagnostic_data.net_if->name,temp);			p	+= temp;

	temp	= al_cpu_to_le16(pi->u.radio_diagnostic_data.data_length);
	memcpy(p,&temp,2);													p	+= 2;

	if(pi->u.radio_diagnostic_data.data_length > 0) {
		memcpy(p,pi->u.radio_diagnostic_data.data,pi->u.radio_diagnostic_data.data_length);
		p	+= pi->u.radio_diagnostic_data.data_length;
	}

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
	return (p - buffer);

#undef _REQ_LENGTH
}

static int imcp_snip_radio_diag_command_get_packet(AL_CONTEXT_PARAM_DECL unsigned char* buffer, unsigned int buf_length, imcp_packet_info_t* pi)
{
#define _REQ_LENGTH		9

	unsigned char*	p;
	unsigned short	temp;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
	unsigned short node_id;
	unsigned short node_len;

	if(buf_length < _REQ_LENGTH)
    {
		 al_print_log(AL_CONTEXT AL_LOG_TYPE_DEBUG," ERROR : (%s) function : LINE (%d) :Receiving length (%d) is not Correct  \n",__func__,__LINE__,buf_length);
		return IMCP_SNIP_ERROR_INADEQUATE_BUFFER_LENGTH;
    }
	p	= buffer;

        IMCP_MSG_RECEIVE_ID_AND_LEN(p,node_id,node_len)


	pi->u.radio_diagnostic_command.ds_mac.length	= MAC_ADDR_LENGTH;
	memcpy(pi->u.radio_diagnostic_command.ds_mac.bytes,p,MAC_ADDR_LENGTH);	p += MAC_ADDR_LENGTH;

	temp	= *p;															p += 1;
	memcpy(pi->u.radio_diagnostic_command.if_name,p,temp);					p += temp;
	pi->u.radio_diagnostic_command.if_name[temp] = 0;

	memcpy(&temp,p,2);														p += 2;
	pi->u.radio_diagnostic_command.command_length	= al_le16_to_cpu(temp);

	if(pi->u.radio_diagnostic_command.command_length > 0) {
		pi->u.radio_diagnostic_command.command_data	= al_heap_alloc(AL_CONTEXT pi->u.radio_diagnostic_command.command_length AL_HEAP_DEBUG_PARAM);
		memcpy(pi->u.radio_diagnostic_command.command_data,p,pi->u.radio_diagnostic_command.command_length);
	} else {
		pi->u.radio_diagnostic_command.command_data	= NULL;
	}

	p	+= pi->u.radio_diagnostic_command.command_length;
	
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
	return (p - buffer);

#undef _REQ_LENGTH
}

static int imcp_snip_channel_change_get_buffer(AL_CONTEXT_PARAM_DECL imcp_packet_info_t* pi, unsigned char* buffer, unsigned int buf_length)
{
#define _REQ_LENGTH             9+ IMCP_ID_AND_LEN_SIZE //Size increased for ID and Length 
	unsigned char*	p;
	unsigned short	temp;
	unsigned short  node_id;	
	unsigned short  node_len;	

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
	if(buf_length < _REQ_LENGTH)
    {
		 al_print_log(AL_CONTEXT AL_LOG_TYPE_DEBUG," ERROR : (%s) function : LINE (%d) :Receiving length (%d) is not Correct  \n",__func__,__LINE__,buf_length);
		return IMCP_SNIP_ERROR_INADEQUATE_BUFFER_LENGTH;
    }
	p		= buffer;

 
	node_id         =  IMCP_CHANNEL_CHANGE_INFO_ID;
	node_len        = (CHANNEL_CHANGE_FIXED_LEN);
	IMCP_MSG_SEND_ID_AND_LEN(p,node_id,node_len)

	memcpy(p,pi->u.channel_change_notification.ds_mac.bytes,MAC_ADDR_LENGTH);	p	+= MAC_ADDR_LENGTH;
	*p = pi->u.channel_change_notification.wm_channel;							p	+= 1;

	temp	= al_cpu_to_le16(pi->u.channel_change_notification.milli_seconds_left);

	memcpy(p,&temp,2);															p	+= 2;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
	return (p - buffer);

#undef _REQ_LENGTH
}

static int imcp_snip_channel_change_get_packet(AL_CONTEXT_PARAM_DECL unsigned char* buffer, unsigned int buf_length, imcp_packet_info_t* pi)
{
#define _REQ_LENGTH             13      

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
	unsigned char*	p;
	unsigned short	temp;
       	unsigned short  le2cpu16;       
        unsigned short  node_id;
        unsigned short  node_len;

	if(buf_length < _REQ_LENGTH)
	{
		 al_print_log(AL_CONTEXT AL_LOG_TYPE_DEBUG,"ERROR : (%s) function : LINE (%d) :Receiving length (%d) is not Correct  \n",__func__,__LINE__,buf_length);	
		return IMCP_SNIP_ERROR_INADEQUATE_BUFFER_LENGTH;
	}
	p		= buffer;

        IMCP_MSG_RECEIVE_ID_AND_LEN(p,node_id,node_len);

	pi->u.channel_change_notification.ds_mac.length	= MAC_ADDR_LENGTH;

	memcpy(pi->u.channel_change_notification.ds_mac.bytes,p,MAC_ADDR_LENGTH);	p	+= MAC_ADDR_LENGTH;
	pi->u.channel_change_notification.wm_channel = *p;							p	+= 1;
	memcpy(&temp,p,2);															p	+= 2;
	pi->u.channel_change_notification.milli_seconds_left = al_cpu_to_le16(temp);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
	return (p - buffer);

#undef _REQ_LENGTH

}

static int imcp_snip_client_signal_info_get_buffer(AL_CONTEXT_PARAM_DECL imcp_packet_info_t* pi, unsigned char* buffer, unsigned int buf_length)
{
#define _REQ_LENGTH		7 + IMCP_ID_AND_LEN_SIZE //Size increased for ID and Length 

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
	unsigned char*	p;
	unsigned char	ifnl;
	unsigned short	temp;
        unsigned short  node_id;
   	unsigned short  node_len;
	int				i;

	if(buf_length < _REQ_LENGTH)
    {
		 al_print_log(AL_CONTEXT AL_LOG_TYPE_DEBUG," ERROR : (%s) function : LINE (%d) :Receiving length (%d) is not Correct  \n",__func__,__LINE__,buf_length);
		return IMCP_SNIP_ERROR_INADEQUATE_BUFFER_LENGTH;
    }
	p		= buffer;
        node_id         =  IMCP_CLIENT_SIGNAL_INFO_ID;
        node_len        =  CLIENT_SIGNAL_INFO_FIXED_LEN + pi->u.client_signal_info.if_name + (7*pi->u.client_signal_info.entry_count);
        IMCP_MSG_SEND_ID_AND_LEN(p,node_id,node_len);


	memcpy(p,pi->u.client_signal_info.ds_mac.bytes,MAC_ADDR_LENGTH);	p	+= MAC_ADDR_LENGTH;

	ifnl	= strlen(pi->u.client_signal_info.if_name);
	*p		= ifnl;														p	+= 1;

	memcpy(p,pi->u.client_signal_info.if_name, ifnl);					p	+= ifnl;

	temp	= al_cpu_to_le16(pi->u.client_signal_info.entry_count);
	memcpy(p, &temp, 2);												p	+= 2;

	for(i = 0; i < pi->u.client_signal_info.entry_count; i++) {
		memcpy(p, pi->u.client_signal_info.signal_info[i].sta_mac.bytes, MAC_ADDR_LENGTH);	p	+= MAC_ADDR_LENGTH;
		*p	= pi->u.client_signal_info.signal_info[i].rssi;									p	+= 1;
	}

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
	return (p - buffer);

#undef _REQ_LENGTH
}

static int imcp_snip_sip_private_get_buffer(AL_CONTEXT_PARAM_DECL imcp_packet_info_t* pi, unsigned char* buffer, unsigned int buf_length)
{
#define _REQ_LENGTH             8 + IMCP_ID_AND_LEN_SIZE //Size increased for ID and Length 	
	unsigned char*	p;
	unsigned short	temp;
        unsigned short  node_id; 
        unsigned short  node_len;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
	if(buf_length < _REQ_LENGTH)
    {
		 al_print_log(AL_CONTEXT AL_LOG_TYPE_DEBUG," ERROR : (%s) function : LINE (%d) :Receiving length (%d) is not Correct  \n",__func__,__LINE__,buf_length);
		return IMCP_SNIP_ERROR_INADEQUATE_BUFFER_LENGTH;
    }
	p		= buffer;

         
        node_id         = IMCP_SIP_PRIVATE_INFO_ID;
        node_len        = (SIP_PRIVATE_FIXED_LEN+(pi->u.sip_private.data_length));
        IMCP_MSG_SEND_ID_AND_LEN(p,node_id,node_len);
    


	memcpy(p, pi->u.sip_private.ds_mac.bytes, MAC_ADDR_LENGTH);			
	p	+= MAC_ADDR_LENGTH;
	temp	= al_cpu_to_le16(pi->u.sip_private.data_length);
	memcpy(p, &temp, 2);				
									
	p	+= 2;
	memcpy(p, pi->u.sip_private.data, pi->u.sip_private.data_length);			
	p	+= pi->u.sip_private.data_length;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
	return (p - buffer);

#undef _REQ_LENGTH
}

static int imcp_snip_sip_private_get_packet(AL_CONTEXT_PARAM_DECL unsigned char* buffer, unsigned int buf_length, imcp_packet_info_t* pi)
{
#define _REQ_LENGTH             12	
	unsigned char*	p;
	unsigned short	temp;		
	unsigned short  le2cpu16;	
        unsigned short  node_id;
        unsigned short  node_len;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
	if(buf_length < _REQ_LENGTH){
		 al_print_log(AL_CONTEXT AL_LOG_TYPE_DEBUG," ERROR : (%s) function : LINE (%d) :Receiving length (%d) is not Correct  \n",__func__,__LINE__,buf_length);

		return IMCP_SNIP_ERROR_INADEQUATE_BUFFER_LENGTH;
	}

	p		= buffer;

        
        IMCP_MSG_RECEIVE_ID_AND_LEN(p,node_id,node_len);
	


	pi->u.sip_private.ds_mac.length	= MAC_ADDR_LENGTH;

	memcpy(pi->u.sip_private.ds_mac.bytes, p, MAC_ADDR_LENGTH);					p	+= MAC_ADDR_LENGTH;
	memcpy(&temp, p, 2);														p	+= 2;
	pi->u.sip_private.data_length = al_cpu_to_le16(temp);
	memcpy(pi->u.sip_private.data, p, pi->u.sip_private.data_length);			p	+= pi->u.sip_private.data_length;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
	return (p - buffer);

#undef _REQ_LENGTH

}

static int imcp_snip_buffer_command_ack_get_packet(AL_CONTEXT_PARAM_DECL unsigned char* buffer, unsigned int buf_length, imcp_packet_info_t* pi)
{
#define _REQ_LENGTH		6

	unsigned char*	p;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
	unsigned short node_id;
	unsigned short node_len;

	if(buf_length < _REQ_LENGTH)
    {
		 al_print_log(AL_CONTEXT AL_LOG_TYPE_DEBUG," ERROR : (%s) function : LINE (%d) :Receiving length (%d) is not Correct  \n",__func__,__LINE__,buf_length);
		return IMCP_SNIP_ERROR_INADEQUATE_BUFFER_LENGTH;
    }
	p	= buffer;

        IMCP_MSG_RECEIVE_ID_AND_LEN(p,node_id,node_len)



	pi->u.buffer_command_ack.ds_mac_dest.length	= MAC_ADDR_LENGTH;

	memcpy(pi->u.buffer_command_ack.ds_mac_dest.bytes,p,MAC_ADDR_LENGTH);		p	+= MAC_ADDR_LENGTH;	

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
	return (p - buffer);

#undef _REQ_LENGTH
}

static int imcp_snip_buffer_command_ack_get_buffer(AL_CONTEXT_PARAM_DECL imcp_packet_info_t* pi, unsigned char* buffer, unsigned int buf_length)
{
#define _REQ_LENGTH	6 + IMCP_ID_AND_LEN_SIZE //Size increased for ID and Length 

	unsigned char*	p;
        unsigned short  node_id;
   	unsigned short  node_len;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
	if(buf_length < _REQ_LENGTH)
    {
		 al_print_log(AL_CONTEXT AL_LOG_TYPE_DEBUG," ERROR : (%s) function : LINE (%d) :Receiving length (%d) is not Correct  \n",__func__,__LINE__,buf_length);
		return IMCP_SNIP_ERROR_INADEQUATE_BUFFER_LENGTH;
    }
	p	= buffer;
        node_id         =  IMCP_BUFFER_COMMAND_ACK_ID;
        node_len        =  DS_EXCERCISER_FIXED_LEN;
        IMCP_MSG_SEND_ID_AND_LEN(p,node_id,node_len);


	memcpy(p,pi->u.buffer_command_ack.ds_mac_dest.bytes,MAC_ADDR_LENGTH);		p	+= MAC_ADDR_LENGTH;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
	return (p - buffer);

#undef _REQ_LENGTH
}

