/********************************************************************************
* MeshDynamics
* --------------
* File     : mesh_on_start_if_init.c
* Comments : Seperated for improved readability
* Created  : 4/7/2006
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  5  |2/15/2007 | Changes for set_tx_antenna                      | Sriram |
* -----------------------------------------------------------------------------
* |  4  |2/14/2007 | Changes for PS phy_subtypes                     | Sriram |
* -----------------------------------------------------------------------------
* |  3  |01/01/2007| IF-Specific TXRate and TXPower changes          |Prachiti|
* -----------------------------------------------------------------------------
* |  2  |1/9/2007  | Added Private Channel implementation            |Prachiti|
* -----------------------------------------------------------------------------
* |  1  |10/31/2006| Changes for private frequencies                 | Sriram |
* -----------------------------------------------------------------------------
* |  0  |4/7/2006  | Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

/**
 * This is a private source file included by mesh_on_start.c, hence
 * It was created to improve readability.
 */

#include <linux/rtnetlink.h>
#include <linux/wireless.h>
#include "meshap.h"
#include "al_conf.h"
#include "mesh.h"

extern unsigned char use_mac_address[6];
extern int           eth1_ping_status;
int ds_proto_type;

#define NET_IF_USED(use_type)    (use_type & 128)
#define TRUE     0
#define FALSE    1
static void _set_mesh_init_status_essid(al_net_if_t *net_if, al_802_11_operations_t *ex_op)
{
   char       essid[AL_802_11_ESSID_MAX_SIZE + 1];
   int        phy_mode;
   const char *phy_mode_freq;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Enter %s<%d>\n",__func__,__LINE__);
   phy_mode = ex_op->get_phy_mode(net_if);

   switch (phy_mode)
   {
   case AL_802_11_PHY_MODE_A:
      phy_mode_freq = "A";
      break;

   case AL_802_11_PHY_MODE_B:
      phy_mode_freq = "B";
      break;

   case AL_802_11_PHY_MODE_G:
      phy_mode_freq = "G";
      break;

   case AL_802_11_PHY_MODE_BG:
      phy_mode_freq = "BG";
      break;

   case AL_802_11_PHY_MODE_PSQ:
      phy_mode_freq = "PS5";
      break;

   case AL_802_11_PHY_MODE_PSH:
      phy_mode_freq = "PS10";
      break;

   case AL_802_11_PHY_MODE_PSF:
      phy_mode_freq = "PS20";
      break;

   default:
      phy_mode_freq = "UN";
   }

   sprintf(essid, MESH_INIT_STATUS_SSID, phy_mode_freq, use_mac_address[3], use_mac_address[4], use_mac_address[5]);

   ex_op->set_essid(net_if, essid);
   /* Do not hide SSID when in MESH_INIT_STATUS */
   ex_op->set_hide_ssid(net_if, 0);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Exit %s<%d>\n",__func__,__LINE__);
}

//PROTOCOL_COMBO
/*
Protocol Combination Matrix
---------------------------------------------------
     |                  Root
Relay|  a    b   g    n    ac   bg  bgn   an  anac
---------------------------------------------------
a    |  Y |    |    |    |    |    |    |  Y |  Y |
---------------------------------------------------
b    |    |  Y |    |    |    |  Y |  Y |    |    |
---------------------------------------------------
g    |    |    |  Y |    |    |  Y |  Y |    |    |
---------------------------------------------------
n    |    |    |    |  Y |    |    |    |    |  Y |
---------------------------------------------------
ac   |    |    |    |    |  Y |    |    |    |  Y |
---------------------------------------------------
bg   |    |  Y |  Y |    |    |  Y |  Y |    |    |
---------------------------------------------------
bgn  |    |  Y |  Y |    |    |  Y |  Y |    |    |
---------------------------------------------------
an   |  Y |    |    |    |    |    |    |  Y |  Y |
---------------------------------------------------
anac |  Y |    |    |  Y |  Y |    |    |  Y |  Y |
---------------------------------------------------
*/
int get_mode_to_proto_cap(unsigned char sub_type)
{
   static int proto_combo[] = {
         /*IGN  0*/     0,
         /*A    1*/      PROTO_TYPE_A,
         /*B    2*/      PROTO_TYPE_B,
         /*G    3*/      PROTO_TYPE_G,
         /*BG   4*/      PROTO_TYPE_BG,
         /*PSQ  5*/      0,
         /*PSH  6*/      0,
         /*PSF  7*/      0,
         /*XXX  8*/      0,
         /*XXX  9*/      0,
         /*XXX  10*/     0,
         /*AC   11*/     PROTO_TYPE_AC,
         /*BGN  12*/     PROTO_TYPE_BGN,
         /*AN   13*/     PROTO_TYPE_AN,
         /*ANAC 14*/     PROTO_TYPE_ANAC,
         /*N2.4 15*/     PROTO_TYPE_PURE_N,
         /*N5   16*/     PROTO_TYPE_PURE_N
         };

   if (sub_type >= sizeof(proto_combo))  /*review comment*/
      return 0;

   return proto_combo[sub_type];
}

int convert_mode_to_proto_cap(unsigned char sub_type)
{
   static int proto_combo[] = {
         /*IGN 0*/     0,
         /*A   1*/     PROTO_TYPE_A | PROTO_TYPE_AN | PROTO_TYPE_ANAC,
         /*B   2*/     PROTO_TYPE_B | PROTO_TYPE_BG | PROTO_TYPE_BGN,
         /*G   3*/     PROTO_TYPE_G | PROTO_TYPE_BG | PROTO_TYPE_BGN,
         /*BG  4*/     PROTO_TYPE_B | PROTO_TYPE_G | PROTO_TYPE_BG | PROTO_TYPE_BGN,
         /*PSQ 5*/     0,
         /*PSH 6*/     0,
         /*PSF 7*/     0,
         /*XXX 8*/     0,
         /*XXX 9*/     0,
         /*XXX 10*/    0,
         /*AC  11*/    PROTO_TYPE_AC | PROTO_TYPE_ANAC,
         /*BGN 12*/    PROTO_TYPE_B | PROTO_TYPE_G | PROTO_TYPE_BG | PROTO_TYPE_BGN,
         /*AN  13*/    PROTO_TYPE_A | PROTO_TYPE_AN | PROTO_TYPE_ANAC,
         /*ANAC14*/    PROTO_TYPE_A | PROTO_TYPE_PURE_N | PROTO_TYPE_AN | PROTO_TYPE_AC | PROTO_TYPE_ANAC,
         /*N2.415*/    PROTO_TYPE_PURE_N,
         /*N5  16*/    PROTO_TYPE_PURE_N | PROTO_TYPE_ANAC
         };

   if (sub_type >= sizeof(proto_combo))  /*review comment*/
      return 0;

   return proto_combo[sub_type];
}


static void _set_phy_mode_helper(al_net_if_t         *net_if,
                                 unsigned short      reg_domain,
                                 unsigned short      country_code,
                                 mesh_conf_if_info_t *if_info)
{
   al_802_11_operations_t    *operations;
   al_reg_domain_info_t      reg_domain_info;
   al_802_11_tx_power_info_t power_info;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Enter %s<%d>\n",__func__,__LINE__);
   operations = (al_802_11_operations_t *)net_if->get_extended_operations(net_if);

   if (operations == NULL)
   {
      return;
   }

//PROTOCOL_COMBO since STA valdidates, no need to add for WM, only DS is suffice
   if(if_info->use_type == AL_CONF_IF_USE_TYPE_DS) {
      ds_proto_type = get_mode_to_proto_cap(if_info->phy_sub_type);
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "PROTOCOL_COMBO:::%s-%d "
          "sub_type=%d ds_proto_type = 0x%x \n", __func__, __LINE__, if_info->phy_sub_type, ds_proto_type);
   }

   switch (if_info->phy_sub_type)
   {
   case AL_CONF_IF_PHY_SUB_TYPE_802_11_A:
      switch (if_info->bonding)
      {
      case AL_CONF_IF_BONDING_SINGLE:
         operations->set_phy_mode(net_if, AL_802_11_PHY_MODE_A);
         break;

      case AL_CONF_IF_BONDING_DOUBLE:
         operations->set_phy_mode(net_if, AL_802_11_PHY_MODE_ATHEROS_TURBO);
         break;
      }
      break;

   case AL_CONF_IF_PHY_SUB_TYPE_802_11_B:
      operations->set_phy_mode(net_if, AL_802_11_PHY_MODE_B);
      break;

   case AL_CONF_IF_PHY_SUB_TYPE_802_11_G:
      operations->set_phy_mode(net_if, AL_802_11_PHY_MODE_G);
      break;

   case AL_CONF_IF_PHY_SUB_TYPE_802_11_BG:
      operations->set_phy_mode(net_if, AL_802_11_PHY_MODE_BG);
      break;

   case AL_CONF_IF_PHY_SUB_TYPE_802_11_PSQ:
      operations->set_phy_mode(net_if, AL_802_11_PHY_MODE_PSQ);
      break;

   case AL_CONF_IF_PHY_SUB_TYPE_802_11_PSH:
      operations->set_phy_mode(net_if, AL_802_11_PHY_MODE_PSH);
      break;

   case AL_CONF_IF_PHY_SUB_TYPE_802_11_PSF:
      operations->set_phy_mode(net_if, AL_802_11_PHY_MODE_PSF);
      break;

   case AL_CONF_IF_PHY_SUB_TYPE_802_11_N_2_4GHZ:
      operations->set_phy_mode(net_if,AL_802_11_PHY_MODE_2_4GHZ);
            break;
   case AL_CONF_IF_PHY_SUB_TYPE_802_11_N_5GHZ:
      operations->set_phy_mode(net_if,AL_802_11_PHY_MODE_5_GHZ);
      break;

   case AL_CONF_IF_PHY_SUB_TYPE_802_11_AC:
      operations->set_phy_mode(net_if, AL_802_11_PHY_MODE_AC);
      break;

   case AL_CONF_IF_PHY_SUB_TYPE_802_11_BGN:
      operations->set_phy_mode(net_if, AL_802_11_PHY_MODE_BGN);
      break;

   case AL_CONF_IF_PHY_SUB_TYPE_802_11_AN:
      operations->set_phy_mode(net_if, AL_802_11_PHY_MODE_AN);
      break;

   case AL_CONF_IF_PHY_SUB_TYPE_802_11_ANAC:
      operations->set_phy_mode(net_if, AL_802_11_PHY_MODE_ANAC);
      break;
   }

   memset(&reg_domain_info, 0, sizeof(al_reg_domain_info_t));

   reg_domain_info.country_code = country_code;
   reg_domain_info.reg_domain   = reg_domain;

   if (reg_domain_info.country_code == AL_REG_DOMAIN_COUNTRY_CODE_CUSTOM)
   {
      reg_domain_info.bandwidth              = if_info->priv_channel_bandwidth;
      reg_domain_info.channel_count          = if_info->priv_channel_count;
      reg_domain_info.frequencies            = if_info->priv_frequencies;
      reg_domain_info.channel_numbers        = if_info->priv_channel_numbers;
      reg_domain_info.channel_flags          = if_info->priv_channel_flags;
      reg_domain_info.max_power              = if_info->priv_channel_max_power;
      reg_domain_info.conformance_test_limit = if_info->priv_channel_ctl;
      reg_domain_info.antenna_max            = if_info->priv_channel_ant_max;
   }

   operations->set_reg_domain_info(net_if, &reg_domain_info);

   operations->set_tx_antenna(net_if, if_info->txant);

   set_net_if_rate_ctrl_param(net_if, if_info);

   if ((if_info->use_type == AL_CONF_IF_USE_TYPE_PMON) ||
       (if_info->use_type == AL_CONF_IF_USE_TYPE_AMON))
   {
      power_info.flags = AL_802_11_TXPOW_PERC;
      power_info.power = 100;
      operations->set_tx_power(net_if, &power_info);
   }
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Exit %s<%d>\n",__func__,__LINE__);
}


static void mesh_on_start_if_init(void)
{
   int conf_if_index;
   int net_if_index;
   mesh_conf_if_info_t *conf_if;
   al_net_if_t         *net_if;
   int                 net_if_count;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Enter %s<%d>\n",__func__,__LINE__);
   net_if_count = al_get_net_if_count(AL_CONTEXT_SINGLE);

   for (conf_if_index = 0; conf_if_index < mesh_config->if_count; conf_if_index++)
   {
      conf_if         = &mesh_config->if_info[conf_if_index];
      conf_if->net_if = NULL;

      for (net_if_index = 0; net_if_index < net_if_count; net_if_index++)
      {
         net_if = al_get_net_if(AL_CONTEXT net_if_index);

         if (!strcmp(net_if->name, conf_if->name))
         {
            conf_if->net_if = net_if;
            break;
         }
      }
   }
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Exit %s<%d>\n",__func__,__LINE__);
}

void get_htcap_from_config(al_802_11_ht_capab_t ht_capab, unsigned short int *config_htcapab)
{
	unsigned short int htcapab = 0;

    al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Enter %s<%d>\n",__func__,__LINE__);
	if(ht_capab.ldpc == AL_CONF_IF_HT_PARAM_ENABLED)
		htcapab |= MESH_HT_CAP_INFO_LDPC_CODING_CAP;
	if(ht_capab.ht_bandwidth == AL_CONF_IF_HT_PARAM_40_ABOVE || ht_capab.ht_bandwidth == AL_CONF_IF_HT_PARAM_40_BELOW)
		htcapab |= MESH_HT_CAP_INFO_SUPP_CHANNEL_WIDTH_SET;

	htcapab &= ~MESH_HT_CAP_INFO_SMPS_MASK;
	switch (ht_capab.smps) {
		case AL_CONF_IF_HT_PARAM_STATIC:
			htcapab |= MESH_HT_CAP_INFO_SMPS_STATIC;
			break;
		case AL_CONF_IF_HT_PARAM_DYNAMIC:
			htcapab |= MESH_HT_CAP_INFO_SMPS_DYNAMIC;
			break;
		case AL_CONF_IF_HT_PARAM_SMPS_DISABLED:
			htcapab |= MESH_HT_CAP_INFO_SMPS_DISABLED;
			break;
	}

	if(ht_capab.gfmode == AL_CONF_IF_HT_PARAM_ENABLED)
		htcapab |= MESH_HT_CAP_INFO_GREEN_FIELD;
	if((ht_capab.gi_20 == AL_CONF_IF_HT_PARAM_AUTO) ||(ht_capab.gi_20 == AL_CONF_IF_HT_PARAM_SHORT))
		htcapab |= MESH_HT_CAP_INFO_SHORT_GI20MHZ;
	if((ht_capab.gi_40 == AL_CONF_IF_HT_PARAM_AUTO)|| (ht_capab.gi_40 == AL_CONF_IF_HT_PARAM_SHORT))
		htcapab |= MESH_HT_CAP_INFO_SHORT_GI40MHZ;
	if(ht_capab.tx_stbc == AL_CONF_IF_HT_PARAM_ENABLED)
		htcapab |= MESH_HT_CAP_INFO_TX_STBC;

	htcapab &= ~MESH_HT_CAP_INFO_RX_STBC_MASK;
	switch (ht_capab.rx_stbc) {
		case 1:
			htcapab |= MESH_HT_CAP_INFO_RX_STBC_1;
			break;
		case 2:
			htcapab |= MESH_HT_CAP_INFO_RX_STBC_12;
			break;
		case 3:
			htcapab |= MESH_HT_CAP_INFO_RX_STBC_123;
			break;
	}

	if(ht_capab.delayed_ba == AL_CONF_IF_HT_PARAM_ENABLED)
		htcapab |= MESH_HT_CAP_INFO_DELAYED_BA;
	if(ht_capab.max_amsdu_len == AL_CONF_IF_HT_PARAM_ENABLED)
		htcapab |= MESH_HT_CAP_INFO_MAX_AMSDU_SIZE;
	if(ht_capab.dsss_cck_40 == AL_CONF_IF_HT_PARAM_ENABLED)
		htcapab |= MESH_HT_CAP_INFO_DSSS_CCK40MHZ;
	if(ht_capab.intolerant == AL_CONF_IF_HT_PARAM_ENABLED)
		htcapab |= MESH_HT_CAP_INFO_40MHZ_INTOLERANT;
	if(ht_capab.lsig_txop == AL_CONF_IF_HT_PARAM_ENABLED)
		htcapab |= MESH_HT_CAP_INFO_LSIG_TXOP_PROTECT_SUPPORT;

	*config_htcapab = htcapab;
    al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Exit %s<%d>\n",__func__,__LINE__);
	return;
}

void get_vhtcap_from_config(al_802_11_vht_capab_t vht_capab, unsigned int *config_vhtcapab)
{
	unsigned int vhtcapab = 0;

    al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Enter %s<%d>\n",__func__,__LINE__);
	switch(vht_capab.max_mpdu_len)
	{
		case 3895:
         vhtcapab |= (MESH_VHT_CAP_MAX_MPDU_LENGTH_3895 & MESH_VHT_CAP_MAX_MPDU_LENGTH_MASK);
         break ;
		case 7991:
			vhtcapab |= (MESH_VHT_CAP_MAX_MPDU_LENGTH_7991 & MESH_VHT_CAP_MAX_MPDU_LENGTH_MASK);
			break ;
		case 11454:
			vhtcapab |= (MESH_VHT_CAP_MAX_MPDU_LENGTH_11454 & MESH_VHT_CAP_MAX_MPDU_LENGTH_MASK);
			break ;
		default :
			break ;
	}
	switch(vht_capab.supported_channel_width)
	{
		case 1:
			vhtcapab |= MESH_VHT_CAP_SUPP_CHAN_WIDTH_160MHZ ;
			break;
		case 2:
			vhtcapab |= MESH_VHT_CAP_SUPP_CHAN_WIDTH_160_80PLUS80MHZ ;
			break;
		default :
			break;
	}
	if(vht_capab.rx_ldpc == AL_CONF_IF_VHT_PARAM_YES){
		vhtcapab |= MESH_VHT_CAP_RXLDPC ;
	}

	if(vht_capab.gi_80 == AL_CONF_IF_VHT_PARAM_YES){
		vhtcapab |= MESH_VHT_CAP_SHORT_GI_80 ;
	}

	if(vht_capab.gi_160 == AL_CONF_IF_VHT_PARAM_YES){
		vhtcapab |= MESH_VHT_CAP_SHORT_GI_160 ;
	}

	if(vht_capab.tx_stbc == AL_CONF_IF_VHT_PARAM_YES){
		vhtcapab |= MESH_VHT_CAP_TXSTBC ;
	}

	switch(vht_capab.rx_stbc)
	{
		case 1:
			vhtcapab |= MESH_VHT_CAP_RXSTBC_1 ;
			break ;
		case 2:
			vhtcapab |= MESH_VHT_CAP_RXSTBC_2 ;
			break ;
		case 3:
			vhtcapab |= MESH_VHT_CAP_RXSTBC_3 ;
			break ;
		case 4:
			vhtcapab |= MESH_VHT_CAP_RXSTBC_4 ;
			break ;
		default :
			break ;
	}

	if(vht_capab.su_beamformer_cap == AL_CONF_IF_VHT_PARAM_YES){
		vhtcapab |= MESH_VHT_CAP_SU_BEAMFORMER_CAPABLE ;
	}

	if(vht_capab.su_beamformee_cap == AL_CONF_IF_VHT_PARAM_YES){
		vhtcapab |= MESH_VHT_CAP_SU_BEAMFORMEE_CAPABLE ;
	}

	if((vht_capab.beamformee_sts_count == 1) &&
			(vhtcapab & MESH_VHT_CAP_SU_BEAMFORMEE_CAPABLE)){
		vhtcapab |= (1 << MESH_VHT_CAP_BEAMFORMEE_STS_OFFSET) ;
	}

	if((vht_capab.sounding_dimensions == 1) &&
			(vhtcapab & MESH_VHT_CAP_SU_BEAMFORMER_CAPABLE)){
		vhtcapab |= (1 << MESH_VHT_CAP_SOUNDING_DIMENSION_OFFSET) ;
	}

	if(vht_capab.mu_beamformer_cap == AL_CONF_IF_VHT_PARAM_YES){
		vhtcapab |= MESH_VHT_CAP_MU_BEAMFORMER_CAPABLE ;
	}

	if(vht_capab.mu_beamformee_cap == AL_CONF_IF_VHT_PARAM_YES){
		vhtcapab |= MESH_VHT_CAP_MU_BEAMFORMEE_CAPABLE ;
	}

	if(vht_capab.vht_txop_ps == AL_CONF_IF_VHT_PARAM_YES){
		vhtcapab |= MESH_VHT_CAP_VHT_TXOP_PS ;
	}

	if(vht_capab.htc_vht_cap == AL_CONF_IF_VHT_PARAM_YES){
		vhtcapab |= MESH_VHT_CAP_HTC_VHT ;
	}

	if(vht_capab.rx_ant_pattern_consistency  == AL_CONF_IF_VHT_PARAM_YES){
		vhtcapab |= MESH_VHT_CAP_RX_ANTENNA_PATTERN ;
	}

	if(vht_capab.tx_ant_pattern_consistency  == AL_CONF_IF_VHT_PARAM_YES){
		vhtcapab |= MESH_VHT_CAP_TX_ANTENNA_PATTERN ;
	}

	*config_vhtcapab = vhtcapab;
    al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Exit %s<%d>\n",__func__,__LINE__);
	return;
}

int mesh_on_start_find_ds(AL_CONTEXT_PARAM_DECL_SINGLE)
{
   int flags;
   int i;
   mesh_conf_if_info_t    *netif_conf;
   al_net_addr_t          use_mac_addr;
   al_net_if_t            *net_if;
   al_802_11_operations_t *ex_op;
   unsigned short int config_htcapab = 0;
   unsigned int config_vhtcapab = 0;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Enter %s<%d>\n",__func__,__LINE__);
   memset(&use_mac_addr, 0, sizeof(al_net_addr_t));

   memcpy(use_mac_addr.bytes, use_mac_address, 6);
   use_mac_addr.length = 6;

   /**
    * Set the ROOT BSSID for the DUMMY PARENT for LFRs to our
    * own DS MAC address
    */

   memcpy(&dummy_lfr_parent.root_bssid, &use_mac_addr, sizeof(al_net_addr_t));

   for (i = 0; i < mesh_config->if_count; i++)
   {
	   netif_conf = &mesh_config->if_info[i];

	   if ((netif_conf->use_type == AL_CONF_IF_USE_TYPE_DS) && (netif_conf->net_if != NULL)
			   && AL_NET_IF_IS_WIRELESS(netif_conf->net_if))
	   {
		   /*Hold Wireless DS interface configured values for follwoing
			* dummy_lfr_parent members*/
		   dummy_lfr_parent.phy_sub_type = netif_conf->phy_sub_type;
		   dummy_lfr_parent.phy_support = get_phy_support(dummy_lfr_parent.phy_sub_type);
		   if(CHECK_FOR_AC_SUBTYPE(netif_conf->phy_sub_type)) {
			   get_vhtcap_from_config(netif_conf->vht_capab, &config_vhtcapab);
			   dummy_lfr_parent.par_ap_vht_capab.vht_capabilities_info = config_vhtcapab;
			   if(CHECK_FOR_N_SUBTYPE(netif_conf->phy_sub_type)) {
				   get_htcap_from_config(netif_conf->ht_capab, &config_htcapab);
				   dummy_lfr_parent.par_ap_ht_capab.ht_capabilities_info = config_htcapab;
			   }
		   }else if(CHECK_FOR_N_SUBTYPE(netif_conf->phy_sub_type)) {
			   get_htcap_from_config(netif_conf->ht_capab, &config_htcapab);
			   dummy_lfr_parent.par_ap_ht_capab.ht_capabilities_info = config_htcapab;
		   }else {
			   dummy_lfr_parent.par_ap_vht_capab.vht_capabilities_info = 0;
			   dummy_lfr_parent.par_ap_ht_capab.ht_capabilities_info = 0;
		   }
		   break;
	   }else {
		   continue;
	   }
   }

   for (i = 0; i < mesh_config->if_count; i++)
   {
      netif_conf = &mesh_config->if_info[i];

      if ((netif_conf->use_type != AL_CONF_IF_USE_TYPE_DS) ||
          (netif_conf->net_if == NULL))
      {
         continue;
      }

      net_if = netif_conf->net_if;

      net_if->set_hw_addr(net_if, &use_mac_addr);

      if (AL_NET_IF_IS_ETHERNET(net_if))
      {
         flags = net_if->get_flags(net_if);

         if ((flags & AL_NET_IF_FLAGS_LINKED) || (_FORCED_ROOT_MODE))
         {
            mesh_hardware_info->ds_conf           = netif_conf;
            mesh_hardware_info->ds_net_if         = net_if;
            mesh_hardware_info->ds_if_type        = netif_conf->phy_type;
            mesh_hardware_info->ds_if_sub_type    = netif_conf->phy_sub_type;
            mesh_hardware_info->ds_if_channel     = 0;
            mesh_hardware_info->ds_if_ack_timeout = netif_conf->ack_timeout;

			return RETURN_SUCCESS;
         }
      }
      else if (AL_NET_IF_IS_WIRELESS(net_if))
      {
         mesh_hardware_info->ds_conf           = netif_conf;
         mesh_hardware_info->ds_net_if         = net_if;
         mesh_hardware_info->ds_if_type        = netif_conf->phy_type;
         mesh_hardware_info->ds_if_sub_type    = netif_conf->phy_sub_type;
         mesh_hardware_info->ds_if_channel     = 0;
         mesh_hardware_info->ds_if_ack_timeout = netif_conf->ack_timeout;
         mesh_hardware_info->ds_if_tx_power    = netif_conf->txpower;
         mesh_hardware_info->ds_if_tx_rate     = netif_conf->txrate;
         netif_conf->net_if = net_if;

         ex_op = (al_802_11_operations_t *)net_if->get_extended_operations(mesh_hardware_info->ds_net_if);

         if (ex_op != NULL)
         {
            ex_op->set_mode(net_if, AL_802_11_MODE_INFRA, 0);
         }

         _set_phy_mode_helper(net_if,
                              mesh_config->reg_domain,
                              mesh_config->country_code,
                              netif_conf);

         return RETURN_SUCCESS;
      }
   }
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Exit %s<%d>\n",__func__,__LINE__);

   return RETURN_ERROR_NO_DS_FOUND;
}


int mesh_on_start_find_ds1(AL_CONTEXT int boot_flag)
{
   int flags;
   int i;
   mesh_conf_if_info_t *netif_conf;
   al_net_addr_t       use_mac_addr;
   al_net_if_t         *net_if;
   al_802_11_operations_t*     ex_op; /*IMX_RELAY*/
   unsigned short int config_htcapab = 0;
   unsigned int config_vhtcapab = 0;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Enter %s<%d>\n",__func__,__LINE__);
   memset(&use_mac_addr, 0, sizeof(al_net_addr_t));

   memcpy(use_mac_addr.bytes, use_mac_address, 6);
   use_mac_addr.length = 6;

   /**
    * Set the ROOT BSSID for the DUMMY PARENT for LFRs to our
    * own DS MAC address
    */

   memcpy(&dummy_lfr_parent.root_bssid, &use_mac_addr, sizeof(al_net_addr_t));

   for (i = 0; i < mesh_config->if_count; i++)
   {
      netif_conf = &mesh_config->if_info[i];

      if ((netif_conf->use_type != AL_CONF_IF_USE_TYPE_DS) ||
          (netif_conf->net_if == NULL))
      {
         continue;
      }


      net_if = netif_conf->net_if;
      net_if->set_hw_addr(net_if, &use_mac_addr);

      if (AL_NET_IF_IS_ETHERNET(net_if))
      {
         flags = net_if->get_flags(net_if);

         if (((flags & AL_NET_IF_FLAGS_LINKED)) || (_FORCED_ROOT_MODE))
         {
            if (_DISJOINT_ADHOC_MODE && (!strcmp(net_if->name, "eth1")))
            {
               if ((mesh_config->failover_enabled || mesh_config->power_on_default) &&
                   (((mesh_config->failover_enabled == MESH_FAILOVER_PHY_CONN) && (flags & AL_NET_IF_FLAGS_LINKED)) ||
                    ((mesh_config->failover_enabled == MESH_FAILOVER_PING_CONN) && (flags & AL_NET_IF_FLAGS_LINKED) && (eth1_ping_status == 1))))
               {
                  mesh_hardware_info->ds_conf           = netif_conf;
                  mesh_hardware_info->ds_net_if         = net_if;
                  mesh_hardware_info->ds_if_type        = netif_conf->phy_type;
                  mesh_hardware_info->ds_if_sub_type    = netif_conf->phy_sub_type;
                  mesh_hardware_info->ds_if_channel     = 0;
                  mesh_hardware_info->ds_if_ack_timeout = netif_conf->ack_timeout;

				  return RETURN_SUCCESS;
               }
            }

            if (((flags & AL_NET_IF_FLAGS_LINKED)) || (_FORCED_ROOT_MODE))
            {
               mesh_hardware_info->ds_conf           = netif_conf;
               mesh_hardware_info->ds_net_if         = net_if;
               mesh_hardware_info->ds_if_type        = netif_conf->phy_type;
               mesh_hardware_info->ds_if_sub_type    = netif_conf->phy_sub_type;
               mesh_hardware_info->ds_if_channel     = 0;
               mesh_hardware_info->ds_if_ack_timeout = netif_conf->ack_timeout;

               return RETURN_SUCCESS;
            }
         }
      }
   }

   for (i = 0; i < mesh_config->if_count; i++)
   {
      netif_conf = &mesh_config->if_info[i];

      if ((netif_conf->use_type != AL_CONF_IF_USE_TYPE_DS) ||
          (netif_conf->net_if == NULL))
      {
         continue;
      }

      net_if = netif_conf->net_if;
      net_if->set_hw_addr(net_if, &use_mac_addr);
      if (AL_NET_IF_IS_WIRELESS(net_if))
      {
         mesh_hardware_info->ds_conf           = netif_conf;
         mesh_hardware_info->ds_net_if         = net_if;
         mesh_hardware_info->ds_if_type        = netif_conf->phy_type;
         mesh_hardware_info->ds_if_sub_type    = netif_conf->phy_sub_type;
         mesh_hardware_info->ds_if_channel     = 0;
         mesh_hardware_info->ds_if_ack_timeout = netif_conf->ack_timeout;
         mesh_hardware_info->ds_if_tx_power    = netif_conf->txpower;
         mesh_hardware_info->ds_if_tx_rate     = netif_conf->txrate;
         netif_conf->net_if = net_if;

         _set_phy_mode_helper(net_if,
                              mesh_config->reg_domain,
                              mesh_config->country_code,
                              netif_conf);

        ex_op = (al_802_11_operations_t*)net_if->get_extended_operations(mesh_hardware_info->ds_net_if);
        if (ex_op != NULL) {
           ex_op->set_mode(mesh_hardware_info->ds_net_if, AL_802_11_MODE_INFRA,0);
        }
         return RETURN_SUCCESS;
      }
   }
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Exit %s<%d>\n",__func__,__LINE__);

   return RETURN_ERROR_NO_DS_FOUND;
}


static int mesh_on_start_find_wms(AL_CONTEXT_PARAM_DECL_SINGLE)
{
   int i;
   int j;
   mesh_conf_if_info_t *netif_conf;
   al_net_if_t         *net_if;

   /**
    * First iterate through all interfaces and count the number of WMs
    */
   mesh_hardware_info->wm_net_if_count = 0;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Enter %s<%d>\n",__func__,__LINE__);

   for (i = 0; i < mesh_config->if_count; i++)
   {
      netif_conf = &mesh_config->if_info[i];
      if (((netif_conf->use_type == AL_CONF_IF_USE_TYPE_WM) || (netif_conf->use_type == AL_CONF_IF_USE_TYPE_AP)) && (netif_conf->net_if != NULL))
      {
         ++mesh_hardware_info->wm_net_if_count;
      }
   }

   /**
    * Allocate the array and iterate again
    */

   mesh_hardware_info->wms_conf = (mesh_conf_if_info_t **)al_heap_alloc(AL_CONTEXT mesh_hardware_info->wm_net_if_count * sizeof(mesh_conf_if_info_t *)AL_HEAP_DEBUG_PARAM);
   memset(mesh_hardware_info->wms_conf, 0, mesh_hardware_info->wm_net_if_count * sizeof(mesh_conf_if_info_t *));

   for (i = 0, j = 0; i < mesh_config->if_count; i++)
   {
      netif_conf = &mesh_config->if_info[i];

      if (((netif_conf->use_type == AL_CONF_IF_USE_TYPE_WM) || (netif_conf->use_type == AL_CONF_IF_USE_TYPE_AP)) && (netif_conf->net_if != NULL))
      {
         net_if = netif_conf->net_if;

         if (AL_NET_IF_IS_WIRELESS(net_if))
         {
            _set_phy_mode_helper(net_if, mesh_config->reg_domain, mesh_config->country_code, netif_conf);
         }

         mesh_hardware_info->wms_conf[j]                    = netif_conf;
         mesh_hardware_info->wms_conf[j]->net_if            = net_if;
         mesh_hardware_info->wms_conf[j]->operating_channel = 0;
         j++;
      }
   }

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Exit %s<%d>\n",__func__,__LINE__);
   return RETURN_SUCCESS;
}


static int mesh_on_start_get_mon_interfaces_info(AL_CONTEXT_PARAM_DECL_SINGLE)
{
   int                 i;
   al_net_if_t         *net_if;
   mesh_conf_if_info_t *netif_conf;

   mesh_hardware_info->monitor_type   = MONITOR_TYPE_ABSENT;
   mesh_hardware_info->monitor_net_if = mesh_hardware_info->ds_net_if;
   mesh_hardware_info->monitor_conf   = NULL;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Enter %s<%d>\n",__func__,__LINE__);

   for (i = 0; i < mesh_config->if_count; i++)
   {
      netif_conf = &mesh_config->if_info[i];

      if (netif_conf->net_if == NULL)
      {
         continue;
      }

      if ((netif_conf->use_type != AL_CONF_IF_USE_TYPE_AMON) &&
          (netif_conf->use_type != AL_CONF_IF_USE_TYPE_PMON))
      {
         continue;
      }

      net_if = netif_conf->net_if;

      mesh_hardware_info->monitor_net_if = net_if;

      if (netif_conf->use_type == AL_CONF_IF_USE_TYPE_PMON)
      {
         mesh_hardware_info->monitor_type = MONITOR_TYPE_PMON;
      }
      else
      {
         mesh_hardware_info->monitor_type = MONITOR_TYPE_AMON;
      }

      _set_phy_mode_helper(net_if,
                           mesh_config->reg_domain,
                           mesh_config->country_code,
                           netif_conf);

      mesh_hardware_info->monitor_conf = netif_conf;

      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MESH_AP:INFO: %s<%d> Monitor net_if working with %d channels :\n",
	  __func__, __LINE__, netif_conf->dca_list_count);

      for (i = 0; i < netif_conf->dca_list_count; i++)
      {
         al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MESH_AP:INFO: %s<%d> \t%d\n", __func__, __LINE__,
		 netif_conf->dca_list[i]);
      }

      al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Enter %s<%d>\n",__func__,__LINE__);
      return RETURN_SUCCESS;
   }

   return RETURN_ERROR;
}


static int mesh_on_start_set_use_type(AL_CONTEXT_PARAM_DECL_SINGLE)
{
   int i = 0;
   al_802_11_operations_t *operations;
   mesh_conf_if_info_t    *netif_conf;
   al_net_if_t            *net_if;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Enter %s<%d>\n",__func__,__LINE__);

   for (i = 0; i < mesh_config->if_count; i++)
   {
      netif_conf = &mesh_config->if_info[i];
      if (netif_conf->net_if == NULL)
      {
         continue;
      }

      net_if = netif_conf->net_if;

      if (AL_NET_IF_IS_WIRELESS(net_if))
      {
         operations = (al_802_11_operations_t *)net_if->get_extended_operations(net_if);
         operations->set_use_type(net_if, netif_conf->use_type);
      }
   }
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Exit %s<%d>\n",__func__,__LINE__);
   return RETURN_SUCCESS;
}
