/********************************************************************************
* MeshDynamics
* --------------
* File     : mesh_init.c
* Comments : Mesh initialization impl
* Created  : 4/22/2004
* Author   : Anand Bhange
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* | 52  |02/12/2009| 'use_virt_if' setting is now read.              | Sriram |
* -----------------------------------------------------------------------------
* | 51  |8/27/2008 | Changes for options                             | Sriram |
* -----------------------------------------------------------------------------
* | 50  |8/22/2008 | Changes for FIPS and SIP                        | Sriram |
* -----------------------------------------------------------------------------
* | 49  |7/24/2007 | REBOOT logging changes                          | Sriram |
* -----------------------------------------------------------------------------
* | 48  |6/13/2007 | pattern_match_min added                         | Sriram |
* -----------------------------------------------------------------------------
* | 47  |3/2/2007  | Allowed WEP Key Index 0                         | Sriram |
* -----------------------------------------------------------------------------
* | 46  |2/26/2007 | Chanages for EFFISTREAM                         | Sriram |
* -----------------------------------------------------------------------------
* | 45  |2/15/2007 | Changes for set_tx_antenna                      | Sriram |
* -----------------------------------------------------------------------------
* | 44  |2/9/2007  | Changes for In-direct VLAN                      | Sriram |
* -----------------------------------------------------------------------------
* | 43	 |01/16/2007| FIPS 140-2 Compliance related Changes           |Prachiti|
* -----------------------------------------------------------------------------
* | 42  |11/14/2006| Changes for heap debug support                  | Sriram |
* -----------------------------------------------------------------------------
* | 41  |10/31/2006| Changes for private frequencies                 | Sriram |
* -----------------------------------------------------------------------------
* | 40  |10/17/2006| Changes for Mobile Infrastructure support       | Sriram |
* -----------------------------------------------------------------------------
* | 39  |3/15/2006 | Changed to UNTAGGED value                       | Sriram |
* -----------------------------------------------------------------------------
* | 38  |3/15/2006 | Added .11e enabled & category for ACL           | Bindu  |
* |     |          | Removed vlan_name                               |        |
* -----------------------------------------------------------------------------
* | 37  |3/14/2006 | Changes for ACL                                 | Bindu  |
* -----------------------------------------------------------------------------
* | 36  |3/13/2006 | ACL list implemented                            | Sriram |
* -----------------------------------------------------------------------------
* | 35  |02/15/2006| Changes for dot11e                              | Bindu  |
* -----------------------------------------------------------------------------
* | 34  |02/08/2006| Config SQNR and Hide SSID added                 | Sriram |
* -----------------------------------------------------------------------------
* | 33  |12/14/2005| acktimeout and regdomain changes                |prachiti|
* -----------------------------------------------------------------------------
* | 32  |8/20/2005 | Misc fixes                                      | Sriram |
* -----------------------------------------------------------------------------
* | 31  |6/17/2005 | Initialized test flags                          | Sriram |
* -----------------------------------------------------------------------------
* | 30  |6/9/2005  | TX Rate added to VLAN                           | Sriram |
* -----------------------------------------------------------------------------
* | 29  |5/20/2005 | FCC/ETSI + Other changes                        | Sriram |
* -----------------------------------------------------------------------------
* | 28  |5/16/2005 | Changes for MESH_INIT_STATUS setting            | Sriram |
* -----------------------------------------------------------------------------
* | 27  |5/6/2005  | Model info changes added                        | Sriram |
* -----------------------------------------------------------------------------
* | 26  |4/21/2005 | Misc changes                                    | Sriram |
* -----------------------------------------------------------------------------
* | 25  |4/17/2005 | Misc changes                                    | Sriram |
* -----------------------------------------------------------------------------
* | 24  |4/15/2005 | Config reading logic fixed                      | Sriram |
* -----------------------------------------------------------------------------
* | 23  |04/07/2005| vlan info reading added                         | Abhijit|
* -----------------------------------------------------------------------------
* | 22  |04/02/2005| Misc bug fixes while monitor radio testing      | Anand  |
* -----------------------------------------------------------------------------
* | 21  |1/8/2005  | Changes for per-interface essid/etc             | Sriram |
* -----------------------------------------------------------------------------
* | 20  |11/29/2004| meshid_length modified in read_configuration    | Abhijit|
* -----------------------------------------------------------------------------
* | 19  |11/16/2004| Enc for MAC (Rover) changed                     | Anand  |
* -----------------------------------------------------------------------------
* | 18  |11/10/2004| preamble_type,slot_time_type added              | Abhijit|
* -----------------------------------------------------------------------------
* | 17  |11/1/2004 | extern use_mac_address added                    | Abhijit|
* -----------------------------------------------------------------------------
* | 16  |10/29/2004| mesh_imcp_key length set after decode           | Anand  |
* -----------------------------------------------------------------------------
* | 15  |10/29/2004| Key Encode based on Patched Mac Addr            | Anand  |
* -----------------------------------------------------------------------------
* | 14  |10/14/2004| AL_USE_ENCRYPTION usage added                   | Abhijit|
* -----------------------------------------------------------------------------
* | 13  |10/11/2004| Changes for al_conf according to IMCP5.0        | Abhijit|
* -----------------------------------------------------------------------------
* | 12  |7/22/2004 | DCA Channel List added.                         | Anand  |
* -----------------------------------------------------------------------------
* | 11  |7/5/2004  | preferred_parent & ap name added                | Anand  |
* -----------------------------------------------------------------------------
* | 10  |6/7/2004  | Filter macro used instead of al_print_log       | Anand  |
* -----------------------------------------------------------------------------
* |  9  |5/31/2004 | al_get_conf_handle called                       | Anand  |
* -----------------------------------------------------------------------------
* |  8  |5/31/2004 | changes in include files for linux compilation  | Anand  |
* -----------------------------------------------------------------------------
* |  7  |5/21/2004 | Log type added to al_print_log                  | Anand  |
* -----------------------------------------------------------------------------
* |  6  |5/18/2004 | mesh_flags added to mesh_initialize()           | Bindu  |
* -----------------------------------------------------------------------------
* |  5  |5/14/2004 | Lock added while sorting list of known_aps      | Anand  |
* -----------------------------------------------------------------------------
* |  4  |5/5/2004  | Packet/Memory Leak Debug params added           | Anand  |
* -----------------------------------------------------------------------------
* |  3  |4/30/2004 | Log messages Added                              | Anand  |
* -----------------------------------------------------------------------------
* |  2  |4/29/2004 | Helper functions & change in read config        | Anand  |
* -----------------------------------------------------------------------------
* |  1  |4/26/2004 | Testing Base Line                               | Anand  |
* -----------------------------------------------------------------------------
* |  0  |4/22/2004 | Created                                         | Anand  |
* -----------------------------------------------------------------------------
********************************************************************************/

#include <linux/string.h>
#include "al.h"
#include "al_print_log.h"
#include "access_point.h"
#include "al_conf.h"
#include "dot11e_conf.h"
#include "acl_conf.h"
#include "al_impl_context.h"
#include "sip_conf.h"
#include "mesh_defs.h"
#include "mesh_init.h"
#include "mesh_table.h"
#include "mesh.h"
#include "mesh_globals.h"
#include "mesh_ap.h"
#include "mesh_errors.h"
#include "imcp_snip.h"
#include "mesh_ng.h"
#include "mobility_conf.h"
#include "mesh_imcp.h"
#include "fips_impl.h"
#include "mesh_location_info.h"
#include "mesh_ng_channel_change.h"
#include "mesh_plugin_impl.h"
#include "access_point_ext.h"

#ifdef AL_USE_ENCRYPTION
#include "al_codec.h"
#endif

AL_DECLARE_GLOBAL(int mesh_flags);
extern unsigned char use_mac_address[6];

#ifdef _AL_ROVER_
static unsigned char mac[6] = { 74, 78, 78, 80, 81, 82 };
#endif

static AL_INLINE void _set_security_info(AL_CONTEXT_PARAM_DECL
                                         char                      *if_name,                                                                                                    /** in */
                                         char                      *essid,                                                                                                      /** in */
                                         al_security_info_t        *al_sec_info /** in */,
                                         al_802_11_security_info_t *if_sec_info /** out */,
                                         al_ip_addr_t              *dot1x_backend_addr /** out */,
                                         unsigned short            *dot1x_backend_port /** out */,
                                         char                      *dot1x_backend_secret /** out */,
                                         int                       *group_key_renewal_period /** out */,
                                         unsigned char             *radius_based_vlan /** out */)
{
   int ret;
   int i;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Enter %s<%d>\n",__func__,__LINE__);
   memset(if_sec_info, 0, sizeof(al_802_11_security_info_t));

   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MESH_AP:INFO: %s<%d> \tInitializing security for %s ESSID %s\n",
   __func__, __LINE__, if_name, essid);

   if_sec_info->none.enabled = al_sec_info->_security_none.enabled;

   if_sec_info->wep.enabled   = al_sec_info->_security_wep.enabled;
   if_sec_info->wep.key_index = al_sec_info->_security_wep.key_index;
   if_sec_info->wep.strength  = al_sec_info->_security_wep.strength;

   switch (if_sec_info->wep.strength)
   {
   case 40:
      memcpy(if_sec_info->wep.keys[0], al_sec_info->_security_wep.keys[0].bytes, 5);
      memcpy(if_sec_info->wep.keys[1], al_sec_info->_security_wep.keys[1].bytes, 5);
      memcpy(if_sec_info->wep.keys[2], al_sec_info->_security_wep.keys[2].bytes, 5);
      memcpy(if_sec_info->wep.keys[3], al_sec_info->_security_wep.keys[3].bytes, 5);
      break;

   case 104:
      /** Copy the same key to all 4 indices */
      memcpy(if_sec_info->wep.keys[0], al_sec_info->_security_wep.keys[0].bytes, AL_802_11_MAX_KEY_LENGTH);
      memcpy(if_sec_info->wep.keys[1], al_sec_info->_security_wep.keys[0].bytes, AL_802_11_MAX_KEY_LENGTH);
      memcpy(if_sec_info->wep.keys[2], al_sec_info->_security_wep.keys[0].bytes, AL_802_11_MAX_KEY_LENGTH);
      memcpy(if_sec_info->wep.keys[3], al_sec_info->_security_wep.keys[0].bytes, AL_802_11_MAX_KEY_LENGTH);
      break;
   }

   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MESH_AP:INFO: %s<%d> \t\tWEP is %s\n",
   __func__, __LINE__, if_sec_info->wep.enabled ? "enabled" : "disabled");

   if (if_sec_info->wep.enabled)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MESH_AP:INFO: %s<%d> \t\t\tWEP strength is %d\n",
	  __func__, __LINE__, if_sec_info->wep.strength);
      switch (if_sec_info->wep.strength)
      {
      case 40:
         for (i = 0; i < AL_802_11_MAX_SECURITY_KEYS; i++)
         {
            al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,
                         "MESH_AP:INFO: %s<%d> \t\t\tKey %d is %02x:%02x:%02x:%02x:%02x\n", __func__, __LINE__, \
                         i,
                         if_sec_info->wep.keys[i][0],
                         if_sec_info->wep.keys[i][1],
                         if_sec_info->wep.keys[i][2],
                         if_sec_info->wep.keys[i][3],
                         if_sec_info->wep.keys[i][4],
                         if_sec_info->wep.keys[i][5]);
         }
         break;

      case 104:
         al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,
                      "MESH_AP:INFO: %s<%d>\t\t\tKey is %02x:%02x:%02x:%02x:%02x:%02x:%02x:%02x:%02x:%02x:%02x:%02x:%02x\n",
                      __func__, __LINE__, if_sec_info->wep.keys[0][0],
                      if_sec_info->wep.keys[0][1],
                      if_sec_info->wep.keys[0][2],
                      if_sec_info->wep.keys[0][3],
                      if_sec_info->wep.keys[0][4],
                      if_sec_info->wep.keys[0][5],
                      if_sec_info->wep.keys[0][6],
                      if_sec_info->wep.keys[0][7],
                      if_sec_info->wep.keys[0][8],
                      if_sec_info->wep.keys[0][9],
                      if_sec_info->wep.keys[0][10],
                      if_sec_info->wep.keys[0][11],
                      if_sec_info->wep.keys[0][12]);
         break;
      }

      /**
       * We now allow Key Index 0 to be used for WEP. Hence we commented
       * out the following block of code
       */

      /*if(if_sec_info->wep.key_index == 0)
       *      if_sec_info->wep.key_index	= 1;*/
   }

   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MESH_AP:INFO: %s<%d>\t\tRSN PSK is %s\n",
   __func__, __LINE__, al_sec_info->_security_rsn_psk.enabled ? "enabled" : "disabled");

   if_sec_info->rsn.enabled = if_sec_info->rsn.enabled || al_sec_info->_security_rsn_psk.enabled;

   if (al_sec_info->_security_rsn_psk.enabled)
   {
      if_sec_info->rsn.mode        = al_sec_info->_security_rsn_psk.mode;
      if_sec_info->rsn.auth_modes |= AL_802_11_SECURITY_INFO_RSN_AUTH_PSK;
      if (al_sec_info->_security_rsn_psk.cipher_ccmp)
      {
	/*Temporarily  the statement has been added*/
        al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "\n CCMP Bit is enabled <%s> %d \n",__func__,__LINE__);

         if_sec_info->rsn.cipher_modes |= AL_802_11_SECURITY_INFO_RSN_CIPHER_CCMP;
      }
      if (al_sec_info->_security_rsn_psk.cipher_tkip)
      {
	/*Temporarily  the statement has been added*/
        al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "\n TKIP Bit is enabled <%s> %d \n",__func__,__LINE__);

         if_sec_info->rsn.cipher_modes |= AL_802_11_SECURITY_INFO_RSN_CIPHER_TKIP;
      }

      /**
       * The PSK itself is stored in uuencoded/encrypted form and hence needs to be
       * decoded
       */

#ifdef AL_USE_ENCRYPTION
#ifdef _AL_ROVER_
      ret = al_decode_key(AL_CONTEXT al_sec_info->_security_rsn_psk.key_buffer, mac, if_sec_info->rsn.psk);
#else
      ret = al_decode_key(AL_CONTEXT al_sec_info->_security_rsn_psk.key_buffer, use_mac_address, if_sec_info->rsn.psk);
#endif

#else
      memcpy(if_sec_info->rsn.psk, al_sec_info->_security_rsn_psk.key_buffer, sizeof(if_sec_info->rsn.psk));
#endif
   }

   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,
                "MESH_AP:INFO: %s<%d> \t\tRSN RADIUS is %s, VLAN membership is %s\n",__func__, __LINE__,
                (al_sec_info->_security_rsn_radius.enabled & AL_CONF_RSN_RADIUS_ENABLED) ? "enabled" : "disabled",
                (al_sec_info->_security_rsn_radius.enabled & AL_CONF_RSN_RADIUS_VLAN_MEMBERSHIP) ? "enabled" : "disabled");

   if_sec_info->rsn.enabled = (if_sec_info->rsn.enabled) ||
                              (al_sec_info->_security_rsn_radius.enabled & AL_CONF_RSN_RADIUS_ENABLED);

   if (al_sec_info->_security_rsn_radius.enabled & AL_CONF_RSN_RADIUS_ENABLED)
   {
      if_sec_info->rsn.mode        = al_sec_info->_security_rsn_radius.mode;
      if_sec_info->rsn.auth_modes |= AL_802_11_SECURITY_INFO_RSN_AUTH_1X;
      if (al_sec_info->_security_rsn_radius.cipher_ccmp)
      {
	 if_sec_info->rsn.cipher_modes |= AL_802_11_SECURITY_INFO_RSN_CIPHER_CCMP;
      }
      if (al_sec_info->_security_rsn_radius.cipher_tkip)
      {
	  if_sec_info->rsn.cipher_modes |= AL_802_11_SECURITY_INFO_RSN_CIPHER_TKIP;
      }

      memcpy(dot1x_backend_addr->bytes, al_sec_info->_security_rsn_radius.radius_server, 4);
      dot1x_backend_addr->length = 4;
      *dot1x_backend_port        = al_sec_info->_security_rsn_radius.radius_port;
      *group_key_renewal_period  = al_sec_info->_security_rsn_radius.group_key_renewal;

      if (radius_based_vlan != NULL)
      {
         if (al_sec_info->_security_rsn_radius.enabled & AL_CONF_RSN_RADIUS_VLAN_MEMBERSHIP)
         {
            *radius_based_vlan = 1;
         }
         else
         {
            *radius_based_vlan = 0;
         }
      }

#ifdef AL_USE_ENCRYPTION
#ifdef _AL_ROVER_
      ret = al_decode_key(AL_CONTEXT al_sec_info->_security_rsn_radius.radius_secret_key, mac, dot1x_backend_secret);
#else
      ret = al_decode_key(AL_CONTEXT al_sec_info->_security_rsn_radius.radius_secret_key, use_mac_address, dot1x_backend_secret);
#endif

#else
      memcpy(dot1x_backend_secret, al_sec_info->_security_rsn_radius.radius_secret_key, 64);
#endif

      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,
                   "MESH_AP:INFO: %s<%d>\t\t\tServer : %d.%d.%d.%d\n", __func__, __LINE__,
                   dot1x_backend_addr->bytes[0],
                   dot1x_backend_addr->bytes[1],
                   dot1x_backend_addr->bytes[2],
                   dot1x_backend_addr->bytes[3]);

      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,
                   "MESH_AP:INFO: %s<%d>\t\t\tPort : %d\n",__func__, __LINE__,
                   *dot1x_backend_port);

      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,
                   "MESH_AP:INFO: %s<%d>\t\t\tSecret : %s\n",__func__, __LINE__,
                   dot1x_backend_secret);
   }
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Exit %s<%d>\n",__func__,__LINE__);
}


static int read_dot11e_configuration(AL_CONTEXT_PARAM_DECL_SINGLE)
{
   int handle;
   dot11e_conf_category_details_t category_info;
   al_dot11e_category_info_t      *dot11e_info;
   int i;
   int ret;

   handle = al_get_dot11e_conf_handle(AL_CONTEXT_SINGLE);

   if ((handle == 0) || (handle == -1))
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP:ERROR: %s<%d> Invalid dot11e_conf handle\n", __func__, __LINE__);
      return RETURN_ERROR;
   }

   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MESH_AP:INFO: %s<%d> ====== READING 802.11e CONFIGURATION BEGIN ======\n",
   __func__, __LINE__);

   mesh_dot11e_category_config = (mesh_dot11e_category_t *)al_heap_alloc(AL_CONTEXT sizeof(mesh_dot11e_category_t)AL_HEAP_DEBUG_PARAM);

   // Pointer to global mesh_dot11e_category_config
   dot11e_info = (al_dot11e_category_info_t *)mesh_dot11e_category_config;

   dot11e_info->count         = dot11e_conf_get_category_info_count(AL_CONTEXT handle);
   dot11e_info->category_info = (al_dot11e_category_details_t *)al_heap_alloc(AL_CONTEXT sizeof(al_dot11e_category_details_t) * dot11e_info->count AL_HEAP_DEBUG_PARAM);

   for (i = 0; i < dot11e_info->count; i++)
   {
      ret = dot11e_conf_get_category_info(AL_CONTEXT handle, i, &category_info);
      if (ret == -1)
      {
         return RETURN_ERROR;
      }

      dot11e_info->category_info[i].category        = category_info.category;
      dot11e_info->category_info[i].burst_time      = category_info.burst_time;
      dot11e_info->category_info[i].acwmin          = category_info.acwmin;
      dot11e_info->category_info[i].acwmax          = category_info.acwmax;
      dot11e_info->category_info[i].aifsn           = category_info.aifsn;
      dot11e_info->category_info[i].disable_backoff = category_info.disable_backoff;
   }

   dot11e_conf_close(AL_CONTEXT handle);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MESH_AP:INFO: %s<%d> ====== READING 802.11e CONFIGURATION END ========\n",
   __func__, __LINE__);

   return RETURN_SUCCESS;
}


int read_mesh_configuration(AL_CONTEXT_PARAM_DECL_SINGLE)
{
   int                          i, j, ret, p_index;	//VLAN
   int                          handle;
   int                          mobility_conf_handle;
   al_conf_if_info_t            *if_conf;
   al_conf_vlan_info_t          *al_conf_vlan_info;
   access_point_security_info_t *vlan_security_info;
   mobility_index_data_t        mobility_data;
   int                          mobility_index;
   int                          max_index;
   unsigned char                *server_ip_addr;
   char 			p_name[32];	//VLAN

#ifdef AL_USE_ENCRYPTION
   unsigned char mesh_imcp_key[256];
#endif

   handle = al_get_conf_handle(AL_CONTEXT_SINGLE);
   mobility_conf_handle = al_get_mobility_conf_handle(AL_CONTEXT_SINGLE);

   if ((handle == 0) || (mobility_conf_handle == 0))
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MESH_AP:INFO: %s<%d> Invalid al_conf handle\n", __func__, __LINE__);
      return RETURN_ERROR;
   }

   /**
    * Initialize
    */

   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MESH_AP:INFO: %s<%d> ====== READING CONFIGURATION BEGIN =======\n",
   __func__, __LINE__);

   if_conf           = (al_conf_if_info_t *)al_heap_alloc(AL_CONTEXT sizeof(al_conf_if_info_t)AL_HEAP_DEBUG_PARAM);
   al_conf_vlan_info = (al_conf_vlan_info_t *)al_heap_alloc(AL_CONTEXT sizeof(al_conf_vlan_info_t)AL_HEAP_DEBUG_PARAM);

   mesh_config = (mesh_config_info_t *)al_heap_alloc(AL_CONTEXT sizeof(mesh_config_info_t)AL_HEAP_DEBUG_PARAM);

   memset(mesh_config, 0, sizeof(mesh_config_info_t));

   ret = al_conf_get_name(AL_CONTEXT handle, mesh_config->name, 128);

   mesh_config->meshid_length = al_conf_get_mesh_id(AL_CONTEXT handle, NULL, 0);

   ret = al_conf_get_model(AL_CONTEXT handle, mesh_config->hardware_model, 256);

   /*
    *	Exclude \0 from meshid_length
    */
   mesh_config->meshid_length -= 1;

   ret = al_conf_get_mesh_id(AL_CONTEXT handle, mesh_config->meshid, mesh_config->meshid_length + 1);

   mesh_config->mesh_imcp_key_length = al_conf_get_mesh_imcp_key_buffer(AL_CONTEXT handle, NULL, 0);
   ret = al_conf_get_mesh_imcp_key_buffer(AL_CONTEXT handle, mesh_config->mesh_imcp_key, mesh_config->mesh_imcp_key_length);



#ifdef AL_USE_ENCRYPTION
   memcpy(mesh_imcp_key, mesh_config->mesh_imcp_key, mesh_config->mesh_imcp_key_length);

   /* TODO check if decode call is successful and then return */
#ifdef _AL_ROVER_
   ret = al_decode_key(AL_CONTEXT mesh_imcp_key, mac, mesh_config->mesh_imcp_key);
#else
   ret = al_decode_key(AL_CONTEXT mesh_imcp_key, use_mac_address, mesh_config->mesh_imcp_key);
#endif
   if (ret > 0)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MESH_AP:INFO: %s<%d> Mesh running with encryption\n",__func__,__LINE__);
      mesh_config->mesh_imcp_key_length = ret;
   }
   else
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP:ERROR: %s<%d> Error decoding key\n", __func__, __LINE__);
      al_heap_free(AL_CONTEXT if_conf);
      al_heap_free(AL_CONTEXT al_conf_vlan_info);
      return RETURN_ERROR_INCORRECT_DECODE_KEY;
   }
#endif

   mesh_config->essid_length = al_conf_get_essid(AL_CONTEXT handle, NULL, 0);
   ret = al_conf_get_essid(AL_CONTEXT handle, mesh_config->essid, mesh_config->essid_length);

   mesh_config->rts_th                   = al_conf_get_rts_th(AL_CONTEXT handle);
   mesh_config->frag_th                  = al_conf_get_frag_th(AL_CONTEXT handle);
   mesh_config->beacon_int               = al_conf_get_beacon_interval(AL_CONTEXT handle);
   mesh_config->dy_channel_alloc         = al_conf_get_dynamic_channel_allocation(AL_CONTEXT handle);
   mesh_config->stay_awake_count         = al_conf_get_stay_awake_count(AL_CONTEXT handle);
   mesh_config->bridge_ageing_time       = al_conf_get_bridge_ageing_time(AL_CONTEXT handle);
   mesh_config->fcc_certified_operation  = al_conf_get_fcc_certified(AL_CONTEXT handle);
   mesh_config->etsi_certified_operation = al_conf_get_etsi_certified(AL_CONTEXT handle);
   mesh_config->ds_tx_power              = al_conf_get_ds_tx_power(AL_CONTEXT handle);
   mesh_config->ds_tx_rate               = al_conf_get_ds_tx_rate(AL_CONTEXT handle);

   mesh_config->reg_domain = al_conf_get_regulatory_domain(AL_CONTEXT handle);

   mesh_config->country_code = al_conf_get_country_code(AL_CONTEXT handle);

   mesh_config->config_sqnr = al_conf_get_config_sqnr(AL_CONTEXT handle);

   mesh_config->use_virt_if = al_conf_get_use_virt_if(AL_CONTEXT handle);

   ret = al_conf_get_preferred_parent(AL_CONTEXT handle, &mesh_config->preferred_parent);
   ret = al_conf_get_signal_map(AL_CONTEXT handle, mesh_config->sig_map);

   mesh_config->hbeat_interval = al_conf_get_heartbeat_interval(AL_CONTEXT handle);

   if (mesh_config->hbeat_interval < 5)
   {
      mesh_config->hbeat_interval = 5;
   }

   mesh_config->hbeat_miss_count = al_conf_get_heartbeat_miss_count(AL_CONTEXT handle);
   mesh_config->hope_cost        = al_conf_get_hop_cost(AL_CONTEXT handle);
   mesh_config->max_aw_hopes     = al_conf_get_max_allowable_hops(AL_CONTEXT handle);

   mesh_config->las_interval = al_conf_get_las_interval(AL_CONTEXT handle);

   /* adding failover and power on default fields*/

   mesh_config->failover_enabled = al_conf_get_failover_enabled(AL_CONTEXT handle);

   mesh_config->power_on_default = al_conf_get_power_on_default(AL_CONTEXT handle);

	mesh_config->disable_backhaul_security = al_conf_get_disable_backhaul_security(AL_CONTEXT handle);

   server_ip_addr = al_conf_get_server_addr(AL_CONTEXT handle);
   memset(mesh_config->server_ip_addr, 0, 128);
   memcpy(mesh_config->server_ip_addr, server_ip_addr, strlen(server_ip_addr));

   mesh_config->mgmt_gw_enable = al_conf_get_mgmt_gw_enable(AL_CONTEXT handle);
   mesh_config->mobile_mode = (mesh_config->las_interval >> 7);

   max_index = mobility_conf_get_index_count(AL_CONTEXT mobility_conf_handle);

   mobility_index = mesh_config->las_interval & 0x7F;

   if (mobility_index > max_index - 1)
   {
      mobility_index = max_index - 1;
   }

   mesh_config->ch_res_th = al_conf_get_change_resistance(AL_CONTEXT handle);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MESH_AP:INFO: %s<%d> Mobility using index %d\n", __func__, __LINE__,
   mobility_index);

   /* New generation configuration parameters begin */

   /**
    * If mesh_config->mobile_mode is not set but the mobility_index is non-zero,
    * we just take the round_robin_beacon_count from the mobility_index, and take
    * all other params from the 0th index
    */

   mobility_conf_get_index_data(AL_CONTEXT mobility_conf_handle, mobility_index, &mobility_data);

   mesh_config->round_robin_beacon_count = mobility_data.round_robin_beacon_count;

   if (!mesh_config->mobile_mode && (mobility_index != 0))
   {
      mobility_conf_get_index_data(AL_CONTEXT mobility_conf_handle, 0, &mobility_data);
   }

   mesh_config->scan_count                = mobility_data.scan_count;
   mesh_config->scanner_dwell_interval    = mobility_data.scanner_dwell_interval;
   mesh_config->sampling_t_next_min       = mobility_data.sampling_t_next_min;
   mesh_config->sampling_t_next_max       = mobility_data.sampling_t_next_max;
   mesh_config->sampling_n_p              = mobility_data.sampling_n_p;
   mesh_config->disconnect_count_max      = mobility_data.disconnect_count_max;
   mesh_config->disconnect_multiplier     = mobility_data.disconnect_multiplier;
   mesh_config->evaluation_damping_factor = mobility_data.evaluation_damping_factor;

   /**
    * For mobile nodes disc_max contains the pattern_match_min parameter
    */

   mesh_config->pattern_match_min = mobility_data.disconnect_count_max;


   /* New generation configuration parameters end */

   mesh_config->if_count = al_conf_get_if_count(AL_CONTEXT handle);
//VLAN
   mesh_config->vlan_count         = al_conf_get_vlan_count(AL_CONTEXT handle);
   mesh_config->if_info                = (mesh_conf_if_info_t*)al_heap_alloc(AL_CONTEXT
		   sizeof(mesh_conf_if_info_t) * (mesh_config->if_count + mesh_config->vlan_count -1)
		   AL_HEAP_DEBUG_PARAM);
//VLAN_END

   for (i = 0; i < mesh_config->if_count; i++)
   {
      ret = al_conf_get_if(AL_CONTEXT handle, i, if_conf);
      strcpy(mesh_config->if_info[i].name, if_conf->name);

      mesh_config->if_info[i].phy_type          = if_conf->phy_type;
      mesh_config->if_info[i].phy_sub_type      = if_conf->phy_sub_type;
      mesh_config->if_info[i].use_type          = if_conf->use_type;
      mesh_config->if_info[i].config_channel    = if_conf->wm_channel;
      mesh_config->if_info[i].bonding           = if_conf->bonding;
      mesh_config->if_info[i].dy_channel_alloc  = if_conf->dca;
      mesh_config->if_info[i].dca_list_count    = if_conf->dca_list_count;
      mesh_config->if_info[i].rts_th            = if_conf->rts_th;
      mesh_config->if_info[i].frag_th           = if_conf->frag_th;
      mesh_config->if_info[i].beacon_int        = if_conf->beacon_interval;
      mesh_config->if_info[i].operating_channel = 0;

      strcpy(mesh_config->if_info[i].essid, if_conf->essid);

      for (j = 0; j < mesh_config->if_info[i].dca_list_count; j++)
      {
         mesh_config->if_info[i].dca_list[j] = if_conf->dca_list[j];
      }

      mesh_config->if_info[i].priv_channel_bandwidth = if_conf->priv_channel_bw;
      mesh_config->if_info[i].priv_channel_max_power = if_conf->priv_channel_max_power;
      mesh_config->if_info[i].priv_channel_ant_max   = if_conf->priv_channel_ant_max;
      mesh_config->if_info[i].priv_channel_ctl       = if_conf->priv_channel_ctl;
      mesh_config->if_info[i].priv_channel_count     = if_conf->priv_channel_count;

      for (j = 0; j < mesh_config->if_info[i].priv_channel_count; j++)
      {
         mesh_config->if_info[i].priv_frequencies[j]     = if_conf->priv_frequencies[j];
         mesh_config->if_info[i].priv_channel_numbers[j] = if_conf->priv_channel_numbers[j];
         mesh_config->if_info[i].priv_channel_flags[j]   = if_conf->priv_channel_flags[j];
      }

      mesh_config->if_info[i].txpower        = if_conf->txpower;
      mesh_config->if_info[i].txrate         = if_conf->txrate;
      mesh_config->if_info[i].txant          = if_conf->txant;
      mesh_config->if_info[i].service        = if_conf->service;
      mesh_config->if_info[i].preamble_type  = if_conf->preamble_type;
      mesh_config->if_info[i].slot_time_type = if_conf->slot_time_type;
      mesh_config->if_info[i].ack_timeout    = if_conf->ack_timeout;
      mesh_config->if_info[i].hide_ssid      = if_conf->hide_ssid;

      memset(mesh_config->if_info[i].dot1x_backend_secret, 0, sizeof(mesh_config->if_info[i].dot1x_backend_secret));

      mesh_config->if_info[i].dot11e_enabled  = if_conf->dot11e_enabled;
      mesh_config->if_info[i].dot11e_category = if_conf->dot11e_category;

      _set_security_info(AL_CONTEXT
                         if_conf->name,
                         if_conf->essid,
                         &if_conf->security_info,
                         &mesh_config->if_info[i].security_info,
                         &mesh_config->if_info[i].dot1x_backend_addr,
                         &mesh_config->if_info[i].dot1x_backend_port,
                         mesh_config->if_info[i].dot1x_backend_secret,
                         &mesh_config->if_info[i].group_key_renewal_period,
                         &mesh_config->if_info[i].radius_based_vlan);


#if MDE_80211N_SUPPORT 

        mesh_config->if_info[i].ht_capab.ldpc  		= if_conf->ht_capab.ldpc;
        mesh_config->if_info[i].ht_capab.ht_bandwidth  	= if_conf->ht_capab.ht_bandwidth;
	mesh_config->if_info[i].ht_capab.gfmode		= if_conf->ht_capab.gfmode;
        mesh_config->if_info[i].ht_capab.smps 		= if_conf->ht_capab.smps;
        mesh_config->if_info[i].ht_capab.gi_20  	= if_conf->ht_capab.gi_20;
        mesh_config->if_info[i].ht_capab.gi_40  	= if_conf->ht_capab.gi_40;
        mesh_config->if_info[i].ht_capab.tx_stbc  	= if_conf->ht_capab.tx_stbc;
        mesh_config->if_info[i].ht_capab.rx_stbc  	= if_conf->ht_capab.rx_stbc;
        mesh_config->if_info[i].ht_capab.delayed_ba  	= if_conf->ht_capab.delayed_ba;
        mesh_config->if_info[i].ht_capab.max_amsdu_len  = if_conf->ht_capab.max_amsdu_len;
        mesh_config->if_info[i].ht_capab.dsss_cck_40  	= if_conf->ht_capab.dsss_cck_40;
        mesh_config->if_info[i].ht_capab.intolerant  	= if_conf->ht_capab.intolerant;
        mesh_config->if_info[i].ht_capab.lsig_txop  	= if_conf->ht_capab.lsig_txop;

        mesh_config->if_info[i].frame_aggregation.ampdu_enable=if_conf->frame_aggregation.ampdu_enable;
        mesh_config->if_info[i].frame_aggregation.max_ampdu_len=if_conf->frame_aggregation.max_ampdu_len;
#endif

#if MDE_80211AC_SUPPORT   

	mesh_config->if_info[i].vht_capab.max_mpdu_len			= if_conf->vht_capab.max_mpdu_len;
	mesh_config->if_info[i].vht_capab.supported_channel_width       = if_conf->vht_capab.supported_channel_width;
	mesh_config->if_info[i].vht_capab.rx_ldpc	                = if_conf->vht_capab.rx_ldpc;
	mesh_config->if_info[i].vht_capab.gi_80		                = if_conf->vht_capab.gi_80;
	mesh_config->if_info[i].vht_capab.gi_160	                = if_conf->vht_capab.gi_160;
	mesh_config->if_info[i].vht_capab.tx_stbc	                = if_conf->vht_capab.tx_stbc;
	mesh_config->if_info[i].vht_capab.rx_stbc	                = if_conf->vht_capab.rx_stbc;
	mesh_config->if_info[i].vht_capab.su_beamformer_cap             = if_conf->vht_capab.su_beamformer_cap;
	mesh_config->if_info[i].vht_capab.su_beamformee_cap             = if_conf->vht_capab.su_beamformee_cap;
	mesh_config->if_info[i].vht_capab.beamformee_sts_count          = if_conf->vht_capab.beamformee_sts_count;
	mesh_config->if_info[i].vht_capab.sounding_dimensions           = if_conf->vht_capab.sounding_dimensions;
	mesh_config->if_info[i].vht_capab.mu_beamformer_cap             = if_conf->vht_capab.mu_beamformer_cap;
	mesh_config->if_info[i].vht_capab.mu_beamformee_cap             = if_conf->vht_capab.mu_beamformee_cap;
	mesh_config->if_info[i].vht_capab.vht_txop_ps                   = if_conf->vht_capab.vht_txop_ps;
	mesh_config->if_info[i].vht_capab.htc_vht_cap                   = if_conf->vht_capab.htc_vht_cap;
	mesh_config->if_info[i].vht_capab.rx_ant_pattern_consistency    = if_conf->vht_capab.rx_ant_pattern_consistency;
	mesh_config->if_info[i].vht_capab.tx_ant_pattern_consistency    = if_conf->vht_capab.tx_ant_pattern_consistency;
	mesh_config->if_info[i].vht_capab.vht_oper_bandwidth            = if_conf->vht_capab.vht_oper_bandwidth;
	mesh_config->if_info[i].vht_capab.seg0_center_freq              = if_conf->vht_capab.seg0_center_freq;
	mesh_config->if_info[i].vht_capab.seg1_center_freq              = if_conf->vht_capab.seg1_center_freq;

#endif

      /* Rate control algorithm parameters begin */
      mesh_config->if_info[i].t_el_max          = mobility_data.t_el_max;
      mesh_config->if_info[i].t_el_min          = mobility_data.t_el_min;
      mesh_config->if_info[i].qualification     = mobility_data.qualification;
      mesh_config->if_info[i].max_retry_percent = mobility_data.max_retry_percent;
      mesh_config->if_info[i].max_error_percent = mobility_data.max_error_percent;
      mesh_config->if_info[i].max_errors        = mobility_data.max_errors;
      mesh_config->if_info[i].min_retry_percent = mobility_data.min_retry_percent;
      mesh_config->if_info[i].min_qualification = mobility_data.min_qualification;
      mesh_config->if_info[i].initial_rate      = mobility_data.initial_rate_index;
      /* Rate control algorithm parameters end */
   }

   mesh_config->criteria_root = al_conf_get_effistream_root(AL_CONTEXT handle);

   mesh_config->vlan_count = al_conf_get_vlan_count(AL_CONTEXT handle);

   al_conf_get_dhcp_info(AL_CONTEXT handle, &mesh_config->dhcp_info);
   al_conf_get_logmon_info(AL_CONTEXT handle, &mesh_config->logmon_info);

   mesh_config->options = al_conf_get_options(AL_CONTEXT handle);

   if (mesh_config->vlan_count > 0)
   {
      mesh_config->vlan_info = (access_point_vlan_info_t *)al_heap_alloc(AL_CONTEXT
                                                                         sizeof(access_point_vlan_info_t) * mesh_config->vlan_count AL_HEAP_DEBUG_PARAM);

      memset(mesh_config->vlan_info, 0, sizeof(access_point_vlan_info_t) * mesh_config->vlan_count);

      for (i = 0; i < mesh_config->vlan_count; i++)
      {
         ret = al_conf_get_vlan(AL_CONTEXT handle, i, al_conf_vlan_info);

         strcpy(mesh_config->vlan_info[i].name, al_conf_vlan_info->name);
         memcpy(&mesh_config->vlan_info[i].ip_addr, al_conf_vlan_info->ip_address, sizeof(al_ip_addr_t));
         mesh_config->vlan_info[i].vlan_tag        = al_conf_vlan_info->tag;
         mesh_config->vlan_info[i].dot11e_enabled  = al_conf_vlan_info->dot11e_enabled;
         mesh_config->vlan_info[i].dot1p_priority  = al_conf_vlan_info->dot1p_priority;
         mesh_config->vlan_info[i].dot11e_category = al_conf_vlan_info->dot11e_category;
         strcpy(mesh_config->vlan_info[i].config.essid, al_conf_vlan_info->essid);
         mesh_config->vlan_info[i].config.rts_threshold   = al_conf_vlan_info->rts_th;
         mesh_config->vlan_info[i].config.frag_threshold  = al_conf_vlan_info->frag_th;
         mesh_config->vlan_info[i].config.beacon_interval = al_conf_vlan_info->beacon_interval;
         mesh_config->vlan_info[i].config.service         = al_conf_vlan_info->service;
         mesh_config->vlan_info[i].config.txpower         = al_conf_vlan_info->txpower;
         mesh_config->vlan_info[i].config.txrate          = al_conf_vlan_info->txrate;

         vlan_security_info = &mesh_config->vlan_info[i].config.security_info;

         memset(vlan_security_info->dot1x_backend_secret, 0, sizeof(vlan_security_info->dot1x_backend_secret));

         _set_security_info(AL_CONTEXT
                            al_conf_vlan_info->name,
                            al_conf_vlan_info->essid,
                            &al_conf_vlan_info->security_info,
                            &vlan_security_info->dot11,
                            &vlan_security_info->dot1x_backend_addr,
                            &vlan_security_info->dot1x_backend_port,
                            vlan_security_info->dot1x_backend_secret,
                            &vlan_security_info->group_key_renewal_period,
                            NULL);
      }
   }


//VLAN
   if (mesh_config->vlan_count > 1) {
	   for (j = 0; j < mesh_config->if_count; j++) {
		   /*Assuming we always create vlan on top of interface which is
		    *having use_type is AP mode and treating it as parent */
		   if(mesh_config->if_info[j].use_type == AL_CONF_IF_USE_TYPE_AP) {
			   strcpy(p_name, mesh_config->if_info[j].name);
			   break;
		   }
	   }
	   for (j = 0; j < mesh_config->if_count; j++) {
		   if(!strcmp(mesh_config->if_info[j].name, p_name))
			   break;
	   }
	   p_index = j;
	   if(!strcmp(mesh_config->if_info[p_index].name, p_name)) {
		   /*i holds the value from which potision onwards it has to add if_info entry for virtual interface*/
		   i = mesh_config->if_count;
		   /*inc if_count by vlan_count-1 because we dont want default vlan info*/
		   mesh_config->if_count   += mesh_config->vlan_count -1;
		   for(j = 1;i < mesh_config->if_count; i++, j++) {
			   /*copy parent interface if_info into virtual interface if_info*/
			   memcpy((void *)&mesh_config->if_info[i], (void *)&mesh_config->if_info[p_index],
					   sizeof(mesh_conf_if_info_t));
			   /*over write virtual interface if_info entry with vlan information in mesh.conf file*/
			   strcpy(mesh_config->if_info[i].name, mesh_config->vlan_info[j].name);
			   strcpy(mesh_config->if_info[i].essid, mesh_config->vlan_info[j].config.essid);
			   mesh_config->if_info[i].dot11e_enabled  =  mesh_config->vlan_info[j].dot11e_enabled;
			   mesh_config->if_info[i].dot11e_category = mesh_config->vlan_info[j].dot11e_category;
			   ret = al_conf_get_vlan(AL_CONTEXT handle, j, al_conf_vlan_info);
			   _set_security_info(AL_CONTEXT
					   mesh_config->vlan_info[j].name,
					   mesh_config->vlan_info[j].config.essid,
					   &al_conf_vlan_info->security_info,
					   &mesh_config->if_info[i].security_info,
					   &mesh_config->if_info[i].dot1x_backend_addr,
					   &mesh_config->if_info[i].dot1x_backend_port,
					   mesh_config->if_info[i].dot1x_backend_secret,
					   &mesh_config->if_info[i].group_key_renewal_period,
					   &mesh_config->if_info[i].radius_based_vlan);
		   }
	   }
   }
//VLAN_END

   al_conf_close(AL_CONTEXT handle);
   mobility_conf_close(AL_CONTEXT mobility_conf_handle);
   al_heap_free(AL_CONTEXT if_conf);
   al_heap_free(AL_CONTEXT al_conf_vlan_info);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MESH_AP:INFO: %s<%d> ====== READING CONFIGURATION END =========\n",
   __func__, __LINE__);

   return RETURN_SUCCESS;
}


static int read_acl_configuration(AL_CONTEXT_PARAM_DECL_SINGLE)
{
#define _MAC_ADDRESS_LENGTH    6


   int             handle;
   acl_conf_info_t info;
   int             i;
   int             ret;

   handle = al_get_acl_conf_handle(AL_CONTEXT_SINGLE);

   if ((handle == 0) || (handle == -1))
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP:ERROR: %s<%d> Invalid acl_conf handle\n", __func__, __LINE__);
      return RETURN_ERROR;
   }

   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MESH_AP:INFO: %s<%d> ====== READING ACL CONFIGURATION BEGIN =====\n",
   __func__, __LINE__);

   mesh_acl_config = (mesh_acl_config_t *)al_heap_alloc(AL_CONTEXT sizeof(mesh_acl_config_t)AL_HEAP_DEBUG_PARAM);

   mesh_acl_config->count   = acl_conf_get_info_count(AL_CONTEXT handle);
   mesh_acl_config->entries = (access_point_acl_entry_t *)al_heap_alloc(AL_CONTEXT sizeof(access_point_acl_entry_t) * mesh_acl_config->count AL_HEAP_DEBUG_PARAM);
   memset(mesh_acl_config->entries, 0, sizeof(access_point_acl_entry_t) * mesh_acl_config->count);

   for (i = 0; i < mesh_acl_config->count; i++)
   {
      ret = acl_conf_get_info(AL_CONTEXT handle, i, &info);

      if (ret == -1)
      {
         return RETURN_ERROR;
      }

      memcpy(mesh_acl_config->entries[i].sta_mac.bytes, info.sta_mac, 6);
      mesh_acl_config->entries[i].sta_mac.length = 6;
      mesh_acl_config->entries[i].vlan_tag       = info.vlan_tag;
      mesh_acl_config->entries[i].allow          = info.allow;
      if (mesh_acl_config->entries[i].vlan_tag == ACL_CONF_VLAN_UNTAGGED)
      {
         mesh_acl_config->entries[i].dot11e_enabled  = info.dot11e_enabled;
         mesh_acl_config->entries[i].dot11e_category = info.dot11e_category;
      }
      else
      {
         mesh_acl_config->entries[i].dot11e_enabled  = 0;
         mesh_acl_config->entries[i].dot11e_category = 0;
      }
   }

   acl_conf_close(AL_CONTEXT handle);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MESH_AP:INFO: %s<%d> ===== READING ACL CONFIGURATION END =======\n",
   __func__, __LINE__);

   return RETURN_SUCCESS;

#undef _MAC_ADDRESS_LENGTH
#undef _VLAN_UNTAGGED
}


static int read_sip_configuration(AL_CONTEXT_PARAM_DECL_SINGLE)
{
   sip_conf_handle_t   handle;
   sip_conf_sta_info_t info;
   int                 i;
   int                 ret;

   handle = (sip_conf_handle_t)al_get_sip_conf_handle(AL_CONTEXT_SINGLE);

   if (handle == NULL)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP:ERROR: %s<%d> Invalid sip_conf handle\n", __func__, __LINE__);
      return RETURN_ERROR;
   }

   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MESH_AP:INFO: %s<%d> ====== READING SIP CONFIGURATION BEGIN =====\n",
   __func__, __LINE__);

   memset(&mesh_config->sip_info, 0, sizeof(sip_conf_info_t));
   mesh_config->sip_info.sip_enabled = sip_conf_get_sip_enabled(AL_CONTEXT handle);
   if (mesh_config->sip_info.sip_enabled == 0)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP:ERROR: %s<%d> SIP IS DISABLED\n", __func__, __LINE__);
      goto label_exit;
   }

   mesh_config->sip_info.adhoc_only = sip_conf_get_adhoc_only(AL_CONTEXT handle);
   mesh_config->sip_info.port       = sip_conf_get_sip_port(AL_CONTEXT handle);
   mesh_config->sip_info.sta_count  = sip_conf_get_sta_count(AL_CONTEXT handle);
   sip_conf_get_server_ip(AL_CONTEXT handle, mesh_config->sip_info.server_ip);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP:ERROR: %s<%d> \tSIP Port : %d",
   __func__, __LINE__, mesh_config->sip_info.port);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP:ERROR: %s<%d> \tSIP Server IP : %d.%d.%d.%d", __func__, __LINE__,
                mesh_config->sip_info.server_ip[0],
                mesh_config->sip_info.server_ip[1],
                mesh_config->sip_info.server_ip[2],
                mesh_config->sip_info.server_ip[3]);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP:ERROR: %s<%d> \tSIP STA Count : %d\n",__func__, __LINE__,
   mesh_config->sip_info.sta_count);

   mesh_config->sip_info.sta_info = (sip_conf_sta_info_t *)al_heap_alloc(AL_CONTEXT sizeof(sip_conf_sta_info_t) * mesh_config->sip_info.sta_count AL_HEAP_DEBUG_PARAM);
   memset(mesh_config->sip_info.sta_info, 0, sizeof(sip_conf_sta_info_t) * mesh_config->sip_info.sta_count);

   for (i = 0; i < mesh_config->sip_info.sta_count; i++)
   {
      ret = sip_conf_get_sta_info(AL_CONTEXT handle, i, &info);

      if (ret == -1)
      {
         return RETURN_ERROR;
      }

      memcpy(mesh_config->sip_info.sta_info[i].mac, info.mac, 6);
      memcpy(mesh_config->sip_info.sta_info[i].extn, info.extn, SIP_CONF_MAX_STR_LEN);
   }

label_exit:
   sip_conf_close(AL_CONTEXT handle);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MESH_AP:INFO: %s<%d> ====== READING SIP CONFIGURATION END ======\n",
   __func__, __LINE__);
   return RETURN_SUCCESS;
}


static int mesh_evaluate_options(AL_CONTEXT_PARAM_DECL_SINGLE)
{
   /**
    *	Option for sip is enabled if sip is enabled from configuration update.
    *	But if option code for sip is absent then we disable sip option here.
    */
   if (access_point_ext_verify_option(ACCESS_POINT_OPTION_CODE_PBV) == 0)
   {
      mesh_config->hope_cost &= ~(0x80);
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MESH_AP:INFO: %s<%d> === SIP OPTION CLEARED ===\n", __func__, __LINE__);
   }

   return 0;
}


/*
 * mesh_ap.c file contains ap hook functions impl
 */
int mesh_initialize(AL_CONTEXT_PARAM_DECL unsigned int md_hash_sz, int flags)
{
   int ret = 0;

   mesh_flags = flags;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Enter %s<%d>\n",__func__,__LINE__);
   mesh_ap_initialize(AL_CONTEXT_SINGLE);
   if (flags)
   {
      mesh_table_initialize(AL_CONTEXT md_hash_sz);

      mesh_mode       = _MESH_AP_ROOT;
      scan_state      = _CHANNEL_SCAN_STATE_START;
      mesh_state      = _MESH_STATE_NOT_STARTED;
      mesh_test_flags = 0;

      mesh_ng_init(AL_CONTEXT_SINGLE);
      imcp_snip_pool_initialize(AL_CONTEXT_SINGLE);
      mesh_imcp_initialize(AL_CONTEXT_SINGLE);
      mesh_ng_channel_change_init();
   }
   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MESH_AP:INFO: %s<%d> mesh_initialize\n", __func__, __LINE__);

   ret = 0;

   /* Read AP configuration */
   ret = read_mesh_configuration(AL_CONTEXT_SINGLE);
   if (ret)
   {
      AL_PRINT_LOG_ERROR_0("MESH_AP	: ERROR while reading configuration");
      return ret;
   }

   ret = read_dot11e_configuration(AL_CONTEXT_SINGLE);
   if (ret)
   {
      AL_PRINT_LOG_ERROR_0("MESH_AP	: ERROR while reading 802.11e configuration");
      return ret;
   }

   ret = read_acl_configuration(AL_CONTEXT_SINGLE);
   if (ret)
   {
      AL_PRINT_LOG_ERROR_0("MESH_AP	: ERROR while reading ACL configuration");
      return ret;
   }

   ret = read_sip_configuration(AL_CONTEXT_SINGLE);
   if (ret)
   {
      AL_PRINT_LOG_ERROR_0("MESH_AP	: ERROR while reading SIP configuration");
   }

#ifdef FIPS_1402_COMPLIANT
   if (_FIPS_ENABLED)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,
                   "MESH_AP	:  ----------- FIPS 140-2 POWER ON SELF TEST BEGIN ---------- ");

      init_fips_compliant_mode();

      /*Do Power-on-self-test for AES and HMAC_SHA1, if it fails  notify and reboot immmediately */

      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "\tCHECKING RNG KNOWN ANSWER TEST...");

      ret = do_power_on_self_test_rng();

      if (ret != 0)
      {
         al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "\t\tFAILED Rebooting...");
         al_reboot_board(AL_CONTEXT AL_REBOOT_BOARD_CODE_FIPS_SELF_TEST_RNG);
         return ret;
      }
      else
      {
         al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "\t\tSUCCESS");
      }

      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "\tCHECKING AES IMPLEMENTATION...");

      ret = do_power_on_self_test_aes();

      if (ret != 0)
      {
         al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "\t\tFAILED Rebooting...");
         al_reboot_board(AL_CONTEXT AL_REBOOT_BOARD_CODE_FIPS_SELF_TEST_AES);
         return ret;
      }
      else
      {
         al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "\t\tSUCCESS");
      }

      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "\tCHECKING HMAC SHA1 IMPLEMENTATION...");
      ret = do_power_on_self_test_hmac_sha1();
      if (ret != 0)
      {
         al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "\t\tFAILED Rebooting...");
         al_reboot_board(AL_CONTEXT AL_REBOOT_BOARD_CODE_FIPS_SELF_TEST_HMAC);
         return ret;
      }
      else
      {
         al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "\t\tSUCCESS");
      }

      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,
                   "MESH_AP	:  ----------- FIPS 140-2 POWER ON SELF TEST END ---------- ");
   }
   else
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MESH_AP	: FIPS 140-2 mode is not enabled");
   }
#endif

   mesh_plugin_initialize();

   mesh_evaluate_options();
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Exit %s<%d>\n",__func__,__LINE__);

   return RETURN_SUCCESS;
}


int mesh_uninitialize(AL_CONTEXT_PARAM_DECL_SINGLE)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Enter %s<%d>\n",__func__,__LINE__);

   mesh_ap_uninitialize(AL_CONTEXT_SINGLE);

   if (mesh_flags)
   {
      mesh_table_uninitialize(AL_CONTEXT_SINGLE);
      imcp_snip_pool_uninitialize(AL_CONTEXT_SINGLE);
      mesh_ng_uninit(AL_CONTEXT_SINGLE);
      mesh_imcp_uninitialize(AL_CONTEXT_SINGLE);

      if (_PROBE_REQUEST_LOCATION_SUPPORT)
      {
         mesh_location_uninitialize(AL_CONTEXT_SINGLE);
      }
   }

   if (mesh_config->vlan_count > 0)
   {
      al_heap_free(AL_CONTEXT mesh_config->vlan_info);
   }

   al_heap_free(AL_CONTEXT mesh_config->if_info);
   al_heap_free(AL_CONTEXT mesh_config);

   mesh_config->if_info = NULL ;
   mesh_config          = NULL ;

   if (mesh_dot11e_category_config->config_info.count > 0)
   {
      al_heap_free(AL_CONTEXT mesh_dot11e_category_config->config_info.category_info);
   }

   al_heap_free(AL_CONTEXT mesh_dot11e_category_config);
   al_heap_free(AL_CONTEXT mesh_acl_config);

   mesh_plugin_uninitialize();

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Exit %s<%d>\n",__func__,__LINE__);
   return RETURN_SUCCESS;
}


int mesh_initialize_helper(AL_CONTEXT_PARAM_DECL unsigned int mesh_hash_sz, int flags)
{
   AL_PRINT_LOG_FLOW_0("MESH_AP	: mesh_initialize_helper()");
   return mesh_initialize(AL_CONTEXT mesh_hash_sz, flags);
}

