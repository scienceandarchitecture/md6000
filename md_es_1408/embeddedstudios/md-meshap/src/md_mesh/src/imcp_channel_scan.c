/********************************************************************************
* MeshDynamics
* --------------
* File     : imcp_channel_scan.c
* Comments : Channel scanning IMCP packet impl file.
* Created  : 8/16/2004
* Author   : Anand Bhange
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  0  |8/16/2004 | Created.                                        | Anand  |
* -----------------------------------------------------------------------------
********************************************************************************/
static int imcp_snip_channel_scan_lock_get_buffer(AL_CONTEXT_PARAM_DECL imcp_packet_info_t *pi, unsigned char *buffer, unsigned int buf_length)
{
#define _REQ_LENGTH    7 + IMCP_ID_AND_LEN_SIZE   //Size increased for ID and Length fields

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
	unsigned char *p;
	unsigned short  node_id;
	unsigned short  node_len;


   AL_ASSERT("MESH_AP	: imcp_snip_channel_scan_lock_get_buffer", buffer != NULL);
   AL_ASSERT("MESH_AP	: imcp_snip_channel_scan_lock_get_buffer", pi != NULL);

   /**
    * Buffer length must be atleast req_length
    */

   if (buf_length < _REQ_LENGTH)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"buff length is lessthan req length :: func : %s LINE : %d\n", __func__,__LINE__);
      return IMCP_SNIP_ERROR_INADEQUATE_BUFFER_LENGTH;
   }

   p = buffer;


	/**
	 * Copy ID ,LEN
	 */
	node_id         =  IMCP_CHANNEL_SCAN_LOCK_INFO_ID;
	node_len        =  CHANNEL_SCAN_LOCK_FIXED_LEN;
	IMCP_MSG_SEND_ID_AND_LEN(p,node_id,node_len);

	/**
	 * Copy DS MAC, PHY ID
	 */
	memcpy(p, pi->u.channel_scan_lock.ds_mac.bytes, MAC_ADDR_LENGTH);

	p += MAC_ADDR_LENGTH;
	memcpy(p, &pi->u.channel_scan_lock.phy_id, 1);

   p += 1;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
   return _REQ_LENGTH;

#undef _REQ_LENGTH
}


static int imcp_snip_channel_scan_unlock_get_buffer(AL_CONTEXT_PARAM_DECL imcp_packet_info_t *pi, unsigned char *buffer, unsigned int buf_length)
{
#define _REQ_LENGTH    7 + IMCP_ID_AND_LEN_SIZE //Size increased for ID and Length
	unsigned char *p;
	unsigned short  node_id;
	unsigned short  node_len;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
   AL_PRINT_LOG_FLOW_0("MESH_AP	: imcp_snip_channel_scan_unlock_get_buffer()");

   AL_ASSERT("MESH_AP	: imcp_snip_channel_scan_unlock_get_buffer", buffer != NULL);
   AL_ASSERT("MESH_AP	: imcp_snip_channel_scan_unlock_get_buffer", pi != NULL);

   /**
    * Buffer length must be atleast req_length
    */

   if (buf_length < _REQ_LENGTH)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"buff length is lessthan req length :: func : %s LINE : %d\n", __func__,__LINE__);
      return IMCP_SNIP_ERROR_INADEQUATE_BUFFER_LENGTH;
   }

	p = buffer;
	
        /**
         * Copy ID,LEN
         */
	node_id         =  IMCP_CHANNEL_SCAN_UNLOCK_INFO_ID;
	node_len        =  CHANNEL_SCAN_UNLOCK_FIXED_LEN;
	IMCP_MSG_SEND_ID_AND_LEN(p,node_id,node_len);


	/**
	 * Copy DS MAC, PHY ID
	 */
	memcpy(p, pi->u.channel_scan_unlock.ds_mac.bytes, MAC_ADDR_LENGTH);
	p += MAC_ADDR_LENGTH;
	memcpy(p, &pi->u.channel_scan_unlock.phy_id, 1);
	p += 1;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
   return _REQ_LENGTH;

#undef _REQ_LENGTH
}


static int imcp_snip_channel_scan_lock_objection_get_buffer(AL_CONTEXT_PARAM_DECL imcp_packet_info_t *pi, unsigned char *buffer, unsigned int buf_length)
{
#define _REQ_LENGTH    7 + IMCP_ID_AND_LEN_SIZE //Size increased for ID and Length 
   unsigned char *p;
   unsigned short  node_id;
   unsigned short  node_len;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
   AL_PRINT_LOG_FLOW_0("MESH_AP	: imcp_snip_channel_scan_lock_objection_get_buffer)");

   AL_ASSERT("MESH_AP	: imcp_snip_channel_scan_lock_objection_get_buffer", buffer != NULL);
   AL_ASSERT("MESH_AP	: imcp_snip_channel_scan_lock_objection_get_buffer", pi != NULL);

   /**
    * Buffer length must be atleast req_length
    */

   if (buf_length < _REQ_LENGTH)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"buff length is lessthan req length :: func : %s LINE : %d\n", __func__,__LINE__);
      return IMCP_SNIP_ERROR_INADEQUATE_BUFFER_LENGTH;
   }

   p = buffer;
   /**
    * Copy ID, LEN
    */

        node_id         =  IMCP_CHANNEL_SCAN_LOCK_OBJECTION_INFO_ID;
        node_len        =  CHANNEL_SCAN_LOCK_OBJECTION_FIXED_LEN;
        IMCP_MSG_SEND_ID_AND_LEN(p,node_id,node_len);


   /**
    * Copy DS MAC, PHY ID
    */
   memcpy(p, pi->u.channel_scan_lock_objection.ds_mac.bytes, MAC_ADDR_LENGTH);
   p += MAC_ADDR_LENGTH;
   memcpy(p, &pi->u.channel_scan_lock_objection.phy_id, 1);
   p += 1;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
   return _REQ_LENGTH;

#undef _REQ_LENGTH
}


static int imcp_snip_channel_scan_lock_get_packet(AL_CONTEXT_PARAM_DECL unsigned char *buffer, unsigned int buf_length, imcp_packet_info_t *pi)
{
#define _REQ_LENGTH    7
   unsigned char *p;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
   unsigned short node_id;
   unsigned short node_len;

   AL_PRINT_LOG_FLOW_0("MESH_AP	: imcp_snip_channel_scan_lock_get_packet()");
   AL_ASSERT("MESH_AP	: imcp_snip_channel_scan_lock_get_packet", pi != NULL);
   AL_ASSERT("MESH_AP	: imcp_snip_channel_scan_lock_get_packet", buffer != NULL);

   /**
    * Buffer length must be atleast req_length
    */

   if (buf_length < _REQ_LENGTH)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"buff length is lessthan req length :: func : %s LINE : %d\n", __func__,__LINE__);
      return IMCP_SNIP_ERROR_INADEQUATE_BUFFER_LENGTH;
   }

   p = buffer;

        IMCP_MSG_RECEIVE_ID_AND_LEN(p,node_id,node_len)


   /**
    * Copy DS MAC, PHY ID
    */
   memcpy(pi->u.channel_scan_lock.ds_mac.bytes, p, MAC_ADDR_LENGTH);
   p += MAC_ADDR_LENGTH;
   pi->u.channel_scan_lock.ds_mac.length = MAC_ADDR_LENGTH;
   memcpy(&pi->u.channel_scan_lock.phy_id, p, 1);
   p += 1;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
   return _REQ_LENGTH;

#undef _REQ_LENGTH
}


static int imcp_snip_channel_scan_unlock_get_packet(AL_CONTEXT_PARAM_DECL unsigned char *buffer, unsigned int buf_length, imcp_packet_info_t *pi)
{
#define _REQ_LENGTH    7
   unsigned char *p;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
   unsigned short node_id;
   unsigned short node_len;

   AL_PRINT_LOG_FLOW_0("MESH_AP	: imcp_snip_channel_scan_unlock_get_packet()");
   AL_ASSERT("MESH_AP	: imcp_snip_channel_scan_unlock_get_packet", pi != NULL);
   AL_ASSERT("MESH_AP	: imcp_snip_channel_scan_unlock_get_packet", buffer != NULL);

   /**
    * Buffer length must be atleast req_length
    */

   if (buf_length < _REQ_LENGTH)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"buff length is lessthan req length :: func : %s LINE : %d\n", __func__,__LINE__);
      return IMCP_SNIP_ERROR_INADEQUATE_BUFFER_LENGTH;
   }

   p = buffer;

        IMCP_MSG_RECEIVE_ID_AND_LEN(p,node_id,node_len)

   /**
    * Copy DS MAC, PHY ID
    */
   memcpy(pi->u.channel_scan_unlock.ds_mac.bytes, p, MAC_ADDR_LENGTH);
   p += MAC_ADDR_LENGTH;
   pi->u.channel_scan_unlock.ds_mac.length = MAC_ADDR_LENGTH;
   memcpy(&pi->u.channel_scan_unlock.phy_id, p, 1);
   p += 1;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
   return _REQ_LENGTH;

#undef _REQ_LENGTH
}


static int imcp_snip_channel_scan_lock_objection_get_packet(AL_CONTEXT_PARAM_DECL unsigned char *buffer, unsigned int buf_length, imcp_packet_info_t *pi)
{
#define _REQ_LENGTH    7
   unsigned char *p;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
   unsigned short node_id;
   unsigned short node_len

   AL_PRINT_LOG_FLOW_0("MESH_AP	: imcp_snip_channel_scan_lock_objection_get_packet()");
   AL_ASSERT("MESH_AP	: imcp_snip_channel_scan_lock_objection_get_packet", pi != NULL);
   AL_ASSERT("MESH_AP	: imcp_snip_channel_scan_lock_objection_get_packet", buffer != NULL);

   /**
    * Buffer length must be atleast req_length
    */

   if (buf_length < _REQ_LENGTH)
   {
       al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"buff length is lessthan req length :: func : %s LINE : %d\n", __func__,__LINE__);
      return IMCP_SNIP_ERROR_INADEQUATE_BUFFER_LENGTH;
   }

   p = buffer;

        IMCP_MSG_RECEIVE_ID_AND_LEN(p,node_id,node_len)


   /**
    * Copy DS MAC, PHY ID
    */
   memcpy(pi->u.channel_scan_lock_objection.ds_mac.bytes, p, MAC_ADDR_LENGTH);
   p += MAC_ADDR_LENGTH;
   pi->u.channel_scan_lock_objection.ds_mac.length = MAC_ADDR_LENGTH;
   memcpy(&pi->u.channel_scan_lock_objection.phy_id, p, 1);
   p += 1;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
   return _REQ_LENGTH;

#undef _REQ_LENGTH
}
