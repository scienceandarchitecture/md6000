/********************************************************************************
* MeshDynamics
* --------------
* File     : mesh_plugin_impl.h
* Comments : Internal plugin interface to Mesh
* Created  : 5/14/2008
* Author   : Abhijit Ayarekar
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  0  |5/14/2008 | Created.                                        | Abhijit|
* -----------------------------------------------------------------------------
********************************************************************************/
#ifndef __MESH_PLUGIN_IMPL_H__
#define __MESH_PLUGIN_IMPL_H__

int mesh_plugin_initialize(void);
int mesh_plugin_uninitialize(void);

int mesh_plugin_post_event(int mesh_plugin_event, void *event_param);

#endif /*__MESH_PLUGIN_IMPL_H__*/
