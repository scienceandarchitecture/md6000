/********************************************************************************
* MeshDynamics
* --------------
* File     : mesh_ext.c
* Comments : External interface to Mesh
* Created  : 5/20/2005
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* | 11  |8/22/2008 | Changes for SIP                                 |Abhijit |
* -----------------------------------------------------------------------------
* | 10  |7/11/2007 | mesh_ext_full_disconnect Added                  | Sriram |
* -----------------------------------------------------------------------------
* |  9  |6/14/2007 | mesh_ext_is_mobile Added                        | Sriram |
* -----------------------------------------------------------------------------
* |  8  |6/13/2007 | mesh_ext_enumerate_mobility_window Added        | Sriram |
* -----------------------------------------------------------------------------
* |  7  |6/5/2007  | Added mesh_ext_send_wm_excerciser               | Sriram |
* -----------------------------------------------------------------------------
* |  6  |5/12/2006 | Added set_reboot_flag                           | Bindu  |
* -----------------------------------------------------------------------------
* |  5  |10/5/2005 | Oscillation related changes                     | Sriram |
* -----------------------------------------------------------------------------
* |  4  |10/4/2005 | Changes for IMCP WM Info                        | Sriram |
* -----------------------------------------------------------------------------
* |  3  |9/19/2005 | Added mesh_ext_send_ds_excerciser               | Sriram |
* -----------------------------------------------------------------------------
* |  2  |6/21/2005 | Added kap and table enumeration                 | Sriram |
* -----------------------------------------------------------------------------
* |  1  |6/17/2005 | Added Test flags setting                        | Sriram |
* -----------------------------------------------------------------------------
* |  0  |5/20/2005 | Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

#include <linux/string.h>
#include "al.h"
#include "al_print_log.h"
#include "access_point.h"
#include "al_conf.h"
#include "al_impl_context.h"
#include "sip_conf.h"
#include "mesh_defs.h"
#include "mesh_init.h"
#include "mesh_table.h"
#include "mesh.h"
#include "mesh_globals.h"
#include "mesh_ap.h"
#include "mesh_errors.h"
#include "mesh_ext.h"
#include "imcp_snip.h"
#include "mesh_imcp.h"
#include "mesh_ng.h"
#include "mesh_ng_fsm.h"

static void _table_enumerate_helper(mesh_table_entry_t *entry, int level, void *callback, mesh_ext_enum_table_routine_t enum_routine);

struct _preferred_enum_data
{
   al_net_addr_t address;
   int           enable;
   int           found;
   int           mode;
};
typedef struct _preferred_enum_data   _preferred_enum_data_t;

static int _set_preferred_enum_func(AL_CONTEXT_PARAM_DECL void *enum_param, parent_t *parent)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
   _preferred_enum_data_t *enum_data;

   enum_data = (_preferred_enum_data_t *)enum_param;

   switch (enum_data->mode)
   {
   case 0:
      if (AL_NET_ADDR_EQUAL(&enum_data->address, &parent->bssid))
      {
         enum_data->found = 1;
         al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR," net addr are qual ret -1 :: func : %s LINE : %d\n", __func__,__LINE__);
         return -1;
      }
      break;

   case 1:
      if (enum_data->enable && AL_NET_ADDR_EQUAL(&enum_data->address, &parent->bssid))
      {
         parent->flags |= MESH_PARENT_FLAG_PREFERRED;
      }
      else
      {
         parent->flags &= ~MESH_PARENT_FLAG_PREFERRED;
      }
      break;
   }

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
   return 0;
}


int mesh_ext_set_preferred_parent(unsigned char enable, unsigned char *mac_address)
{
   _preferred_enum_data_t enum_data;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
   if (mesh_mode != _MESH_AP_RELAY)
   {
   al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR," mesh_mode is not AP relay :: func : %s LINE : %d\n", __func__,__LINE__);
      return -3;
   }

   if (mesh_state != _MESH_STATE_RUNNING)
   {
   al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR," mesh_state is running :: func : %s LINE : %d\n", __func__,__LINE__);
      return -1;
   }

   memset(&enum_data, 0, sizeof(_preferred_enum_data_t));

   enum_data.address.length = MAC_ADDR_SIZE;
   memcpy(enum_data.address.bytes, mac_address, MAC_ADDR_SIZE);
   enum_data.enable = enable;
   enum_data.found  = 0;
   enum_data.mode   = 0;

   if (enum_data.enable && AL_NET_ADDR_EQUAL(&enum_data.address, &zeroed_net_addr))
   {
      memcpy(&enum_data.address, &current_parent->bssid, sizeof(al_net_addr_t));
   }

   mesh_ng_enumerate_parents(AL_CONTEXT MESH_NG_ENUMERATE_PARENTS_TYPE_ALL, (void *)&enum_data, _set_preferred_enum_func, 0);

   if (enum_data.found == 0)
   {
      return -2;
   }

   enum_data.mode = 1;

   mesh_ng_enumerate_parents(AL_CONTEXT MESH_NG_ENUMERATE_PARENTS_TYPE_ALL, (void *)&enum_data, _set_preferred_enum_func, 0);

   /** Next heartbeat will cause a shift */

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
   return 0;
}


int mesh_ext_set_test_flags(unsigned int flags)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
   mesh_test_flags = flags;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MESH_AP: Accepting Test flags = 0x%08X", mesh_test_flags);
   return 0;
}


struct _enum_kap_enum_data
{
   mesh_ext_enum_kap_routine_t enum_routine;
   void                        *callback;
};
typedef struct _enum_kap_enum_data   _enum_kap_enum_data_t;

static int _enum_kap_enum_func(AL_CONTEXT_PARAM_DECL void *enum_param, parent_t *parent)
{
   _enum_kap_enum_data_t              *enum_data;
   mesh_ext_known_access_point_info_t ext_kap;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
   enum_data = (_enum_kap_enum_data_t *)enum_param;

   memset(&ext_kap, 0, sizeof(mesh_ext_known_access_point_info_t));

   memcpy(&ext_kap.bssid, &parent->bssid, sizeof(al_net_addr_t));
   ext_kap.channel          = parent->channel;
   ext_kap.crssi            = parent->crssi;
   ext_kap.direct_bit_rate  = parent->direct_bit_rate;
   ext_kap.disconnect_count = parent->disconnect_count;
   ext_kap.ds_channel       = parent->ds_channel;
   memcpy(&ext_kap.ds_mac, &parent->ds_mac, sizeof(al_net_addr_t));
   ext_kap.ds_type = parent->ds_type;
   strcpy(ext_kap.essid, parent->essid);
   ext_kap.flags        = parent->flags;
   ext_kap.hbi          = parent->hbi;
   ext_kap.hpc          = parent->hpc;
   ext_kap.last_hb_time = parent->last_hb_time;
   memcpy(&ext_kap.root_bssid, &parent->root_bssid, sizeof(al_net_addr_t));
   ext_kap.score              = parent->score;
   ext_kap.signal             = parent->signal;
   ext_kap.sqnr               = parent->sqnr;
   ext_kap.tree_bit_rate      = parent->tree_bit_rate;
   ext_kap.max_sample_packets = parent->max_sample_packets;
   ext_kap.sample_packets     = parent->sample_packets;
   ext_kap.t_next             = parent->t_next;
   ext_kap.next_sample_time   = parent->next_sample_time;
   ext_kap.pattern_count      = parent->pattern_count;
   ext_kap.last_seen_time     = parent->last_seen_time;

   if (IMCP_SNIP_CRSSI_GET_VALID(ext_kap.crssi))
   {
      ext_kap.dhcp_r_value = IMCP_SNIP_CRSSI_GET_DHCP_R_VALUE(ext_kap.crssi);
   }

   enum_data->enum_routine(enum_data->callback, &ext_kap);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
   return 0;
}


int mesh_ext_enumerate_known_aps(void *callback, mesh_ext_enum_kap_routine_t enum_routine)
{
   _enum_kap_enum_data_t enum_data;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
   enum_data.callback     = callback;
   enum_data.enum_routine = enum_routine;

   mesh_ng_enumerate_parents(AL_CONTEXT MESH_NG_ENUMERATE_PARENTS_TYPE_ALL, (void *)&enum_data, _enum_kap_enum_func, 0);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
   return 0;
}


int mesh_ext_enumerate_sampling_list(void *callback, mesh_ext_enum_kap_routine_t enum_routine)
{
   _enum_kap_enum_data_t enum_data;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
   enum_data.callback     = callback;
   enum_data.enum_routine = enum_routine;

   mesh_ng_enumerate_sampling_list(AL_CONTEXT(void *) & enum_data, _enum_kap_enum_func);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
   return 0;
}


int mesh_ext_enumerate_table(void *callback, mesh_ext_enum_table_routine_t enum_routine)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
   _table_enumerate_helper(mesh_first_child, 0, callback, enum_routine);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
   return 0;
}


static void _table_enumerate_helper(mesh_table_entry_t *entry, int level, void *callback, mesh_ext_enum_table_routine_t enum_routine)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
   mesh_ext_table_entry_t ext_entry;

   if (entry == NULL)
   {
      return;
   }

   /**
    * First process own entry
    */

   memset(&ext_entry, 0, sizeof(mesh_ext_table_entry_t));

   memcpy(&ext_entry.sta_addr, &entry->sta_addr, sizeof(al_net_addr_t));
   memcpy(&ext_entry.relay_ap_addr, &entry->relay_ap_addr, sizeof(al_net_addr_t));
   memcpy(&ext_entry.sta_ap_addr, &entry->sta_ap_addr, sizeof(al_net_addr_t));

   ext_entry.net_if  = entry->net_if;
   ext_entry.last_rx = entry->last_rx;
   ext_entry.last_tx = entry->last_tx;
   ext_entry.level   = level;

   enum_routine(callback, &ext_entry);

   /**
    * Process first child entry
    */

   if (entry->first_child != NULL)
   {
      _table_enumerate_helper(entry->first_child, level + 1, callback, enum_routine);
   }

   /**
    * Process enxt sibling entry
    */

   if (entry->next_siblings != NULL)
   {
      _table_enumerate_helper(entry->next_siblings, level, callback, enum_routine);
   }
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
}


int mesh_ext_send_ds_excerciser()
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
   mesh_imcp_send_packet_ds_excerciser(AL_CONTEXT NULL, NULL);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);

   return 0;
}


int mesh_ext_set_config_sqnr(unsigned int sqnr)
{
   mesh_config->config_sqnr = sqnr;
   return 0;
}


int mesh_ext_set_reboot_flag(unsigned int flag)
{
   if (flag == 1)
   {
      SET_MESH_FLAG(MESH_FLAG_REBOOT_REQUIRED);
   }
   else
   {
      RESET_MESH_FLAG(MESH_FLAG_REBOOT_REQUIRED);
   }

   return 0;
}


int mesh_ext_send_wm_excerciser(al_net_if_t *net_if, al_net_addr_t *mac_address)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
   mesh_imcp_send_packet_ds_excerciser(AL_CONTEXT net_if, mac_address);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);

   return 0;
}


int mesh_ext_enumerate_mobility_window(void *callback, mesh_ext_enum_mobwin_routine_t enum_routine)
{
   mobility_window_t         *item;
   mesh_ext_mobilty_window_t ext_item;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
   item = mobility_window_head;

   while (item != NULL)
   {
      memcpy(&ext_item.parent_bssid, &item->parent->bssid, sizeof(al_net_addr_t));
      memcpy(&ext_item.ds_mac, &item->parent->ds_mac, sizeof(al_net_addr_t));
      ext_item.channel        = item->parent->channel;
      ext_item.signal         = item->parent->signal;
      ext_item.pattern_count  = item->parent->pattern_count;
      ext_item.flags          = item->parent->flags;
      ext_item.window_item_id = (unsigned int)item;

      enum_routine(callback, &ext_item);

      item = item->next;
   }

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
   return 0;
}


int mesh_ext_is_mobile()
{
   return(_IS_MOBILE);
}


int mesh_ext_full_disconnect()
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
   mesh_ng_fsm_send_event(AL_CONTEXT MESH_NG_FSM_EVENT_FULL_DISCONNECT, NULL);
   al_set_event(AL_CONTEXT mesh_thread_wait_event);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);

   return 0;
}


int mesh_ext_get_func_mode()
{
   return func_mode;
}


int mesh_ext_get_step_scan_channel_index()
{
   return step_scan_channel_index;
}


int mesh_ext_get_address_info(unsigned char *mac_address, mesh_ext_table_entry_t *ext_entry)
{
   mesh_table_entry_t *entry = NULL;
   al_net_addr_t      sta_addr;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
   memset(&sta_addr, 0, sizeof(al_net_addr_t));

   sta_addr.length = MAC_ADDR_SIZE;
   memcpy(sta_addr.bytes, mac_address, MAC_ADDR_SIZE);

   entry = mesh_table_entry_find(AL_CONTEXT & sta_addr);

   if (entry == NULL)
   {
      return -1;
   }

   memset(ext_entry, 0, sizeof(mesh_ext_table_entry_t));

   memcpy(&ext_entry->sta_addr, &entry->sta_addr, sizeof(al_net_addr_t));
   memcpy(&ext_entry->relay_ap_addr, &entry->relay_ap_addr, sizeof(al_net_addr_t));
   memcpy(&ext_entry->sta_ap_addr, &entry->sta_ap_addr, sizeof(al_net_addr_t));

   ext_entry->net_if  = entry->net_if;
   ext_entry->last_rx = entry->last_rx;
   ext_entry->last_tx = entry->last_tx;

   mesh_table_entry_release(AL_CONTEXT entry);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
   return 0;
}


int mesh_ext_get_current_parent_info(mesh_ext_known_access_point_info_t *parent)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
   if (current_parent == NULL)
   {
      return -1;
   }

   memcpy(&parent->bssid, &current_parent->bssid, sizeof(al_net_addr_t));
   memcpy(&parent->ds_mac, &current_parent->ds_mac, sizeof(al_net_addr_t));
   memcpy(&parent->root_bssid, &current_parent->root_bssid, sizeof(al_net_addr_t));
   parent->channel         = current_parent->channel;
   parent->crssi           = current_parent->crssi;
   parent->direct_bit_rate = current_parent->direct_bit_rate;
   parent->flags           = current_parent->flags;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
   return 0;
}
