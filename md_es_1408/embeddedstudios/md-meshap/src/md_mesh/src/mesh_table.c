/********************************************************************************
* MeshDynamics
* --------------
* File     : mesh_table.c
* Comments : mesh table functions.
* Created  : 4/15/2004
* Author   : Anand Bhange
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* | 24  |7/22/2008 | Table integrity fixes                           | Sriram |
* -----------------------------------------------------------------------------
* | 23  |7/24/2007 | REBOOT logging changes                          | Sriram |
* -----------------------------------------------------------------------------
* | 22  |4/26/2007 | Added mesh_table_entry_find_in_list             | Sriram |
* -----------------------------------------------------------------------------
* | 21  |11/15/2006| Heap leak problem fixed                         | Sriram |
* -----------------------------------------------------------------------------
* | 20  |7/29/2005 | Misc changes                                    | Sriram |
* -----------------------------------------------------------------------------
* | 19  |7/29/2004 | Fixes from Sriram                               | Anand  |
* -----------------------------------------------------------------------------
* | 18  |7/25/2004 | Fixes for sub-tree information packet           | Sriram |
* -----------------------------------------------------------------------------
* | 17  |7/24/2004 | Fixed minor compilation warnings                | Sriram |
* -----------------------------------------------------------------------------
* | 16  |7/23/2004 | mesh_table_remove_all_entries func impl         | Sriram |
* -----------------------------------------------------------------------------
* | 15  |7/23/2004 | al_print_log commented out                      | Sriram |
* -----------------------------------------------------------------------------
* | 14  |7/19/2004 | mesh_table_process_heart_beat added             | Anand  |
* -----------------------------------------------------------------------------
* | 13  |6/15/2004 | Repeated Mesh HB processing avoided (Hash added)| Anand  |
* -----------------------------------------------------------------------------
* | 12  |6/9/2004  | Copy of net_addr corrected                      | Anand  |
* -----------------------------------------------------------------------------
* | 11  |6/9/2004  | Relay & act AP addr same as sta for immi child  | Anand  |
* -----------------------------------------------------------------------------
* | 10  |6/7/2004  | Filter macro used instead of al_print_log       | Anand  |
* -----------------------------------------------------------------------------
* |  9  |6/7/2004  | Bug Fixed - add_entry                           | Anand  |
* -----------------------------------------------------------------------------
* |  8  |6/1/2004  | PRINT_HW_ADDRESS prints Dev Name                | Anand  |
* -----------------------------------------------------------------------------
* |  7  |5/31/2004 | changes in include files for linux compilation  | Anand  |
* -----------------------------------------------------------------------------
* |  6  |5/20/2004 | Function return type changed.                   | Anand  |
* -----------------------------------------------------------------------------
* |  5  |5/6/2004  | Bug fixed while adding entry to mesh table      | Anand  |
* -----------------------------------------------------------------------------
* |  4  |5/5/2004  | Packet/Memory Leak Debug params added           | Anand  |
* -----------------------------------------------------------------------------
* |  3  |4/26/2004 | mesh_includes file removed from include         | Anand  |
* -----------------------------------------------------------------------------
* |  2  |4/26/2004 | Testing Base Line                               | Anand  |
* -----------------------------------------------------------------------------
* |  1  |4/21/2004 | **Made (major) changes for AL_CONTEXT**         | Anand  |
* -----------------------------------------------------------------------------
* |  0  |4/15/2004 | Created                                         | Anand  |
* -----------------------------------------------------------------------------
********************************************************************************/

#include <linux/string.h>
#include "al.h"
#include "al_print_log.h"
#include "access_point.h"
#include "al_impl_context.h"
#include "mesh_defs.h"
#include "mesh.h"
#include "mesh_table.h"
#include "mesh_globals.h"
#include "mesh_ap.h"
#include "mesh_errors.h"
#include "imcp_snip.h"
#include "mesh_ng.h"

AL_DECLARE_GLOBAL(mesh_table_bucket_entry_t *mesh_hash);
AL_DECLARE_GLOBAL(unsigned int mesh_hash_size);
AL_DECLARE_GLOBAL(mesh_table_entry_t * mesh_first_child);
AL_DECLARE_GLOBAL(mesh_heart_beat_processing_info_t * *mesh_hbp_hash);

al_spinlock_t mtable_splock;
AL_DEFINE_PER_CPU(int, mtable_spvar);

al_spinlock_t mtable_hbp_splock;
AL_DEFINE_PER_CPU(int, mtable_hbp_spvar);

int child_in_use;

AL_DECLARE_GLOBAL(int table_entry_semaphore);
#define MESHAP_TABLE_ENTRY_LOCK(flags)                       \
   do {                                                      \
      al_wait_on_semaphore(AL_CONTEXT table_entry_semaphore, \
                           AL_WAIT_TIMEOUT_INFINITE);        \
   } while (0)
#define MESHAP_TABLE_ENTRY_UNLOCK(flags)                         \
   do {                                                          \
      al_release_semaphore(AL_CONTEXT table_entry_semaphore, 1); \
   } while (0)

#ifdef _AL_ROVER_
void get_device_name(AL_CONTEXT_PARAM_DECL char *buf);

mesh_ap_name_info_t **mesh_name_hash;
int                 ref_count;
#endif


#ifdef  __GNUC__
#define _MESH_MAC_HASH_FUNCTION(addr, mesh_hash_sz) \
   ({                                               \
      int hash;                                     \
      hash = (addr)->bytes[0] * 1                   \
             + (addr)->bytes[1] * 2                 \
             + (addr)->bytes[2] * 3                 \
             + (addr)->bytes[3] * 4                 \
             + (addr)->bytes[4] * 5                 \
             + (addr)->bytes[5] * 6;                \
      hash % mesh_hash_sz;                          \
   }                                                \
   )
#else
static int _MESH_MAC_HASH_FUNCTION(al_net_addr_t *addr, int mesh_hash_sz)
{
   int hash; \
   hash = (addr)->bytes[0] * 1
          + (addr)->bytes[1] * 2
          + (addr)->bytes[2] * 3
          + (addr)->bytes[3] * 4
          + (addr)->bytes[4] * 5
          + (addr)->bytes[5] * 6;
   return hash % mesh_hash_sz;
}
#endif

void mesh_table_initialize(AL_CONTEXT_PARAM_DECL unsigned int md_hash_size)
{
	mesh_hash_size        = md_hash_size;
	mesh_first_child      = NULL;
	table_entry_semaphore = al_create_semaphore(AL_CONTEXT  1, 1);
	mesh_hash             = (mesh_table_bucket_entry_t *)al_heap_alloc(AL_CONTEXT sizeof(mesh_table_bucket_entry_t) * mesh_hash_size AL_HEAP_DEBUG_PARAM);
	int i;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s : %d\n", __func__,__LINE__);

	for (i = 0; i < mesh_hash_size; i++) {
		mesh_hash[i].entry = NULL;
		al_spin_lock_init(&mesh_hash[i].mtable_hsplock);
	}

	al_spin_lock_init(&mtable_splock);
	al_spin_lock_init(&mtable_hbp_splock);

	mesh_hbp_hash = (mesh_heart_beat_processing_info_t **)al_heap_alloc(AL_CONTEXT sizeof(mesh_heart_beat_processing_info_t *) * mesh_hash_size AL_HEAP_DEBUG_PARAM);
	memset(mesh_hbp_hash, 0, sizeof(mesh_heart_beat_processing_info_t *) * mesh_hash_size);

#ifdef _AL_ROVER_
	if (ref_count == 0)
	{
		mesh_name_hash = (mesh_ap_name_info_t **)al_heap_alloc(AL_CONTEXT sizeof(mesh_ap_name_info_t *) * mesh_hash_size AL_HEAP_DEBUG_PARAM);
		memset(mesh_name_hash, 0, sizeof(mesh_table_entry_t *) * mesh_hash_size);
	}
	ref_count++;
#endif
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s : %d\n", __func__,__LINE__);
}


void mesh_table_uninitialize(AL_CONTEXT_PARAM_DECL_SINGLE)
{
   mesh_first_child = NULL;
   al_heap_free(AL_CONTEXT mesh_hash);

#ifdef _AL_ROVER_
   ref_count--;
   if (ref_count == 0)
   {
      al_heap_free(AL_CONTEXT mesh_name_hash);
   }
#endif
}

int mesh_table_entry_add_lock(AL_CONTEXT_PARAM_DECL al_net_if_t *net_if,
                         al_net_addr_t                     *sta_addr,
                         al_net_addr_t                     *relay_ap_addr,
                         al_net_addr_t                     *sta_ap_addr,
                         int                               caller_id)
{
	int result;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s : %d\n", __func__,__LINE__);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,
                      "MESH_AP(%d)	: INFO : Entry for "AL_NET_ADDR_STR" over %s not added because "AL_NET_ADDR_STR " was not found!  %s : %d\n",
                      caller_id,
                      sta_addr,
                      net_if->name,
                      sta_ap_addr,
                      __func__,
                      __LINE__);


	al_spin_lock(mtable_splock, mtable_spvar);
	result  = mesh_table_entry_add(AL_CONTEXT net_if, sta_addr, relay_ap_addr, sta_ap_addr, caller_id);
	al_spin_unlock(mtable_splock, mtable_spvar);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s : %d\n", __func__,__LINE__);
	return result;
}

int mesh_table_entry_add(AL_CONTEXT_PARAM_DECL al_net_if_t *net_if,
                         al_net_addr_t                     *sta_addr,
                         al_net_addr_t                     *relay_ap_addr,
                         al_net_addr_t                     *sta_ap_addr,
                         int                               caller_id)
{
   unsigned int       hash;
   mesh_table_entry_t *entry = NULL;
   mesh_table_entry_t *new_entry = NULL;
   mesh_table_entry_t *parent_ap_entry = NULL;
   unsigned int       flags;

   parent_ap_entry = NULL;
   entry           = mesh_table_entry_find(AL_CONTEXT sta_addr);

   /* if sta already exists remove entry and add fresh entry */
   if (entry != NULL)
   {
      mesh_table_entry_remove(AL_CONTEXT sta_addr, 7);
      mesh_table_entry_release(AL_CONTEXT entry);
   }
   if (sta_ap_addr != NULL)
   {
      parent_ap_entry = mesh_table_entry_find(AL_CONTEXT sta_ap_addr);

      if (parent_ap_entry == NULL)
      {
         return MESH_TABLE_ADD_ERROR_PARENT_NODE_MISSING;
      }
   }


   new_entry = (mesh_table_entry_t *)al_heap_alloc(AL_CONTEXT sizeof(mesh_table_entry_t)AL_HEAP_DEBUG_PARAM);
   memset(new_entry, 0, sizeof(mesh_table_entry_t));

   memcpy(&new_entry->sta_addr, sta_addr, sizeof(al_net_addr_t));
   if (relay_ap_addr != NULL)
   {
      memcpy(&new_entry->relay_ap_addr, relay_ap_addr, sizeof(al_net_addr_t));
   }
   else
   {
      memcpy(&new_entry->relay_ap_addr, sta_addr, sizeof(al_net_addr_t));
   }

   if (sta_ap_addr != NULL)
   {
      memcpy(&new_entry->sta_ap_addr, sta_ap_addr, sizeof(al_net_addr_t));
   }
   else
   {
      memcpy(&new_entry->sta_ap_addr, sta_addr, sizeof(al_net_addr_t));
   }

   new_entry->net_if = net_if;
   AL_ATOMIC_SET(new_entry->ref_cnt, 1);

   new_entry->create_time = al_get_tick_count(AL_CONTEXT_SINGLE);

   hash = _MESH_MAC_HASH_FUNCTION(sta_addr, mesh_hash_size);
   al_spin_lock(mesh_hash[hash].mtable_hsplock, flags);

   /* adding node to mesh hash */
   entry = mesh_hash[hash].entry;
   if (entry != NULL)
   {
      new_entry->next_hash = entry->next_hash;
      new_entry->prev_hash = entry;
      if (entry->next_hash != NULL)
      {
         entry->next_hash->prev_hash = new_entry;
      }

      entry->next_hash = new_entry;
   }
   else
   {
      new_entry->next_hash = NULL;
      new_entry->prev_hash = NULL;
      mesh_hash[hash].entry      = new_entry;
   }

   al_spin_unlock(mesh_hash[hash].mtable_hsplock, flags);


   if ((sta_ap_addr != NULL) && (parent_ap_entry != NULL))
   {
      new_entry->parent_ap = parent_ap_entry;

      entry = new_entry->parent_ap->first_child;

      if (entry == NULL)
      {
         new_entry->parent_ap->first_child = new_entry;
      }
      else
      {
         new_entry->next_siblings = entry->next_siblings;
         new_entry->prev_siblings = entry;
         if (entry->next_siblings != NULL)
         {
            entry->next_siblings->prev_siblings = new_entry;
         }

         entry->next_siblings = new_entry;
      }
   }
   else
   {
      entry = mesh_first_child;
      if (entry == NULL)
      {
         mesh_first_child = new_entry;
      }
      else
      {
         new_entry->next_siblings = entry->next_siblings;
         new_entry->prev_siblings = entry;
         if (entry->next_siblings != NULL)
         {
            entry->next_siblings->prev_siblings = new_entry;
         }

         entry->next_siblings = new_entry;
      }
   }

   mesh_table_entry_release(AL_CONTEXT parent_ap_entry);
   return 0;
}

static int mesh_table_entry_remove1(AL_CONTEXT_PARAM_DECL mesh_table_entry_t *entry)
{
	unsigned int       hash;
	mesh_table_entry_t *child = NULL;
	mesh_table_entry_t *temp = NULL;

	if (entry == NULL)
	{
		return RETURN_ERROR;
	}

	hash = _MESH_MAC_HASH_FUNCTION(&entry->sta_addr, mesh_hash_size);

	al_spin_lock(mesh_hash[hash].mtable_hsplock, flags);
	/* removing node to mesh hash */
	if (entry->next_hash != NULL)
	{
		entry->next_hash->prev_hash = entry->prev_hash;
	}

	if (entry->prev_hash != NULL)
	{
		entry->prev_hash->next_hash = entry->next_hash;
	}
	else
	{
		mesh_hash[hash].entry = entry->next_hash;
	}
	al_spin_unlock(mesh_hash[hash].mtable_hsplock, flags);

	child = entry->first_child;
	while (child != NULL)
	{
		mesh_table_entry_remove1(AL_CONTEXT child);
		temp  = child;
		child = child->next_siblings;
		/* Atomically decrements ref_cnt by 1 and returns true if the result is 0,
		 * or false for all other cases
		 */
		if (AL_ATOMIC_DEC_AND_TEST(temp->ref_cnt)) {
			al_heap_free(AL_CONTEXT temp);
		}
	}
	return RETURN_SUCCESS;
}

int mesh_table_entry_remove_lock(AL_CONTEXT_PARAM_DECL al_net_addr_t *sta_addr, int caller_id)
{
		int result;

		al_spin_lock(mtable_splock, mtable_spvar);
        result = mesh_table_entry_remove(AL_CONTEXT sta_addr, caller_id);
		al_spin_unlock(mtable_splock, mtable_spvar);

		return result;
}


int mesh_table_entry_remove(AL_CONTEXT_PARAM_DECL al_net_addr_t *sta_addr, int caller_id)
{
	mesh_table_entry_t *entry = NULL;
	unsigned int       flags;

	entry = mesh_table_entry_find(AL_CONTEXT sta_addr);

	if (entry == NULL)
	{
		return RETURN_SUCCESS;
	}

	mesh_table_entry_remove1(AL_CONTEXT entry);

	/* removing from tree */

	if (entry->next_siblings != NULL)
	{
		entry->next_siblings->prev_siblings = entry->prev_siblings;
	}

	if (entry->prev_siblings != NULL)
	{
		entry->prev_siblings->next_siblings = entry->next_siblings;
	}
	else
	{
		if (entry->parent_ap != NULL)
		{
			entry->parent_ap->first_child = entry->next_siblings;
		}
		else if (entry == mesh_first_child)
		{
			mesh_first_child = entry->next_siblings;
		}
	}
	if (AL_ATOMIC_DEC_AND_TEST(entry->ref_cnt)) {
		al_heap_free(AL_CONTEXT entry);
		return;
    } else {		
	    mesh_table_entry_release(AL_CONTEXT entry);
	}

	return RETURN_SUCCESS;
}

mesh_table_entry_t *mesh_table_entry_find(AL_CONTEXT_PARAM_DECL al_net_addr_t *sta_addr)
{
   unsigned int       hash;
   mesh_table_entry_t *entry = NULL;
   unsigned int       flags;

   hash = _MESH_MAC_HASH_FUNCTION(sta_addr, mesh_hash_size);

   al_spin_lock(mesh_hash[hash].mtable_hsplock, flags);

   entry = mesh_hash[hash].entry;

   while (entry != NULL)
   {
      if (!memcmp(entry->sta_addr.bytes, sta_addr->bytes, MAC_ADDR_SIZE))
      {
		 al_spin_unlock(mesh_hash[hash].mtable_hsplock, flags);
		 AL_ATOMIC_INC(entry->ref_cnt);
         return entry;
      }
      entry = entry->next_hash;
   }
   al_spin_unlock(mesh_hash[hash].mtable_hsplock, flags);

   return 0;
}

void  mesh_table_entry_release(AL_CONTEXT_PARAM_DECL mesh_table_entry_t *entry)
{
   unsigned long flags;

   if (entry == NULL || AL_ATOMIC_GET(entry->ref_cnt) == 0) {
		return;
   }
   if (AL_ATOMIC_DEC_AND_TEST(entry->ref_cnt)) {
      al_heap_free(AL_CONTEXT entry);
	   return;
   }
}


int mesh_table_find_relay_ap_for_sta_ap(AL_CONTEXT_PARAM_DECL al_net_addr_t *sta_addr, al_net_if_t **relay_net_if, al_net_addr_t *relay_addr)
{
	mesh_table_entry_t *entry = NULL;

	entry = mesh_table_entry_find(AL_CONTEXT sta_addr);
	if (entry != NULL)
	{
		memcpy(relay_addr, &entry->relay_ap_addr, sizeof(al_net_addr_t));
		*relay_net_if = entry->net_if;
	    mesh_table_entry_release(AL_CONTEXT entry);
		return RETURN_SUCCESS;
	}
	else
	{
		*relay_net_if = NULL;
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : relay_net_if is NULL %s : %d\n", __func__,__LINE__);
		return RETURN_ERROR;
	}
}


void mesh_table_remove_all_entries(AL_CONTEXT_PARAM_DECL_SINGLE)
{
   mesh_table_entry_t *entry = NULL;
   al_net_addr_t      *sta;
   unsigned int       flags;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s : %d\n", __func__,__LINE__);

   al_spin_lock(mtable_splock, mtable_spvar);
   entry = mesh_first_child;
   while (entry != NULL)
   {
      sta   = &entry->sta_addr;
      entry = entry->next_siblings;
      mesh_table_entry_remove(AL_CONTEXT sta, 8);
   }
   al_spin_unlock(mtable_splock, mtable_spvar);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s : %d\n", __func__,__LINE__);
}


int mesh_hb_is_already_processed(AL_CONTEXT_PARAM_DECL al_net_addr_t *sta_addr, unsigned int sqnr)
{
   unsigned int hash;
   unsigned long flags;
   mesh_heart_beat_processing_info_t *entry;
   mesh_heart_beat_processing_info_t *new_entry;
//   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s : %d\n", __func__,__LINE__);

   hash = _MESH_MAC_HASH_FUNCTION(sta_addr, mesh_hash_size);

   al_spin_lock(mtable_hbp_splock, mtable_hbp_spvar);
   entry = mesh_hbp_hash[hash];

   //check whether entry already exists
   while (entry != NULL)
   {
      if (AL_NET_ADDR_EQUAL(&entry->ap_addr, sta_addr))
      {
         if (sqnr <= entry->processed_heartbeat_sqnr)
         {
            if (sqnr <= 10)
            {
               entry->processed_heartbeat_sqnr = sqnr;
			      al_spin_unlock(mtable_hbp_splock, mtable_hbp_spvar);
               al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : sqnr is %d -- %s : %d\n", sqnr, __func__,__LINE__);
               return RETURN_ERROR;
            }
            else
            {
			      al_spin_unlock(mtable_hbp_splock, mtable_hbp_spvar);
 //              al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s : %d\n", __func__,__LINE__);
               return RETURN_SUCCESS;
            }
         }
         else
         {
            entry->processed_heartbeat_sqnr = sqnr;
			al_spin_unlock(mtable_hbp_splock, mtable_hbp_spvar);
            return RETURN_ERROR;
         }
      }
      entry = entry->next;
   }

   /* entry not found, Add new entry */
   new_entry = (mesh_heart_beat_processing_info_t *)al_heap_alloc(AL_CONTEXT sizeof(mesh_heart_beat_processing_info_t)AL_HEAP_DEBUG_PARAM);
   memcpy(&new_entry->ap_addr, sta_addr, sizeof(al_net_addr_t));
   new_entry->processed_heartbeat_sqnr = sqnr;

   entry = mesh_hbp_hash[hash];

   /* adding node to mesh hash */
   if (entry != NULL)
   {
      new_entry->next = entry->next;
      entry->next     = new_entry;
   }
   else
   {
      new_entry->next     = NULL;
      mesh_hbp_hash[hash] = new_entry;
   }

   al_spin_unlock(mtable_hbp_splock, mtable_hbp_spvar);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s : %d\n", __func__,__LINE__);
   return RETURN_ERROR;
}


static int mesh_table_get_sta_count1(AL_CONTEXT_PARAM_DECL mesh_table_entry_t *parent)
{
   unsigned int       child_count = 0;
   mesh_table_entry_t *child = NULL;
   unsigned int       flags;

   child = parent;
   while (child != NULL)
   {
      child_count++;
      child_count += mesh_table_get_sta_count1(AL_CONTEXT child->first_child);
      child        = child->next_siblings;
   }
   return child_count;
}


int mesh_table_get_sta_count(AL_CONTEXT_PARAM_DECL int lock_taken)
{
   unsigned int count;
   unsigned long flags;

   if (!lock_taken)
       al_spin_lock(mtable_splock, mtable_spvar);
   count = mesh_table_get_sta_count1(AL_CONTEXT mesh_first_child);
   if (!lock_taken)
       al_spin_unlock(mtable_splock, mtable_spvar);

   return count;
}


static int mesh_table_get_sta_entries1(AL_CONTEXT_PARAM_DECL mesh_table_entry_t *parent, al_net_addr_t *sta_addrs, unsigned char index)
{
	mesh_table_entry_t *child = NULL;
	unsigned char      child_count = 0;
	unsigned int       flags;

	child = parent;

	while (child != NULL)
	{
		memcpy(&sta_addrs[(index + child_count) * 2 + 0], &child->sta_ap_addr, sizeof(al_net_addr_t));
		memcpy(&sta_addrs[(index + child_count) * 2 + 1], &child->sta_addr, sizeof(al_net_addr_t));

		++child_count;

		child_count += mesh_table_get_sta_entries1(AL_CONTEXT child->first_child, sta_addrs, index + child_count);

		child = child->next_siblings;
	}

	return child_count;
}


void mesh_table_get_sta_entries(AL_CONTEXT_PARAM_DECL imcp_packet_info_t* pi)
{
   unsigned long flags;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s : %d\n", __func__,__LINE__);

   al_spin_lock(mtable_splock, mtable_spvar);
	pi->u.sub_tree_info.sta_count = mesh_table_get_sta_count(AL_CONTEXT 1);
	pi->u.sub_tree_info.sta_entries = (al_net_addr_t*)al_heap_alloc(AL_CONTEXT sizeof(al_net_addr_t) * 2 * pi->u.sub_tree_info.sta_count AL_HEAP_DEBUG_PARAM);
   mesh_table_get_sta_entries1(AL_CONTEXT mesh_first_child, pi->u.sub_tree_info.sta_entries, 0);
   al_spin_unlock(mtable_splock, mtable_spvar);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s : %d\n", __func__,__LINE__);
}

void mesh_table_get_immidiate_sta_entries(AL_CONTEXT_PARAM_DECL al_net_addr_t **sta_addrs, unsigned char *children_count)
{
   unsigned int       child_count = 0;
   mesh_table_entry_t *child = NULL;
   unsigned int       flags;
   unsigned char       tmp;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s : %d\n", __func__,__LINE__);

   al_spin_lock(mtable_splock, mtable_spvar);

   child             = mesh_first_child;
   (*children_count) = 0;

   while (child != NULL)
   {
      (*children_count)++;
      child = child->next_siblings;
   }
   tmp = *children_count;
   al_spin_unlock(mtable_splock, mtable_spvar);

   if (*children_count > 0)
   {
      *sta_addrs  = (al_net_addr_t *)al_heap_alloc(AL_CONTEXT(*children_count) * sizeof(al_net_addr_t)AL_HEAP_DEBUG_PARAM);
      child_count = 0;
      al_spin_lock(mtable_splock, mtable_spvar);
      child       = mesh_first_child;
      while ((child != NULL) && (tmp > 0))
      {
         memcpy(&((*sta_addrs)[child_count++]), &child->sta_addr, sizeof(al_net_addr_t));
         child = child->next_siblings;
         tmp--;
      }
	  al_spin_unlock(mtable_splock, mtable_spvar);
   }
   else
   {
      *sta_addrs = NULL;
   }
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s : %d\n", __func__,__LINE__);
}


int mesh_table_update_sta_entries(AL_CONTEXT_PARAM_DECL al_net_if_t *net_if, al_net_addr_t *sender_address, al_net_addr_t *relay_ap_addr, al_net_addr_t *sta_addrs, unsigned char children_count)
{
   unsigned char i = 0;
   unsigned int  flags;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s : %d\n", __func__,__LINE__);

   /* remove all existing entries */
   for (i = 0; i < children_count; i++)
   {
      mesh_table_entry_remove_lock(AL_CONTEXT & sta_addrs[(i * 2) + 1], 9);
   }

   /* add sub tree entries */
   for (i = 0; i < children_count; i++)
   {
      mesh_ng_set_parent_as_child(AL_CONTEXT & sta_addrs[(i * 2) + 1], 1, 0);

      if (AL_NET_ADDR_EQUAL(&sta_addrs[(i * 2) + 0], &sta_addrs[(i * 2) + 1]))
      {
         mesh_table_entry_add_lock(AL_CONTEXT net_if, &sta_addrs[(i * 2) + 1], relay_ap_addr, sender_address, 3);
      }
      else
      {
         mesh_table_entry_add_lock(AL_CONTEXT net_if, &sta_addrs[(i * 2) + 1], relay_ap_addr, &sta_addrs[(i * 2) + 0], 3);
      }
   }

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s : %d\n", __func__,__LINE__);
   return RETURN_SUCCESS;
}

int mesh_table_process_heart_beat(AL_CONTEXT_PARAM_DECL al_net_addr_t *immigiate_ap, unsigned int child_count, al_net_addr_t *immidigiate_sta)
{
   unsigned int       i = 0;
   mesh_table_entry_t *hb_ap = NULL;
   mesh_table_entry_t *child = NULL;
   mesh_table_entry_t *next_sibling = NULL;
   al_net_addr_t      *sta;
   int                protection_count;
   unsigned int       flags;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s : %d\n", __func__,__LINE__);

   /* first find immigiate ap node */

   hb_ap = mesh_table_entry_find(AL_CONTEXT immigiate_ap);

   if (hb_ap == NULL)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESHAP_CORE : ERROR : hb_ap is NULL %s : %d\n", __func__,__LINE__);
      return RETURN_ERROR;
   }

    al_spin_lock(mtable_splock, mtable_spvar);
   /* add entries which were added in this HB period */

   for (i = 0; i < child_count; i++)
   {
      child            = hb_ap->first_child;
      protection_count = ACCESS_POINT_INTERRUPT_DISABLE_PROTECTION_COUNT;
      while (child != NULL && protection_count > 0)
      {
         if (AL_NET_ADDR_EQUAL(&immidigiate_sta[i], &child->sta_addr))
         {
            break;
         }
         child = child->next_siblings;
         --protection_count;
      }

      if (protection_count <= 0)
      {
         al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MESH_AP	: INFO : Strange condition encountered , rebooting... %s : %d\n", __func__, __LINE__);
         al_reboot_board(AL_CONTEXT AL_REBOOT_BOARD_CODE_MESH_TABLE_PROTECTION);
      }
      if (child == NULL)
      {
         mesh_table_entry_add(AL_CONTEXT hb_ap->net_if,
                              &immidigiate_sta[i],
                              &hb_ap->relay_ap_addr,
                              &hb_ap->sta_addr,
                              4);
      }
   }

   /* remove entries which were deleted in this HB period */

   child            = hb_ap->first_child;
   protection_count = ACCESS_POINT_INTERRUPT_DISABLE_PROTECTION_COUNT;

   while (child != NULL && protection_count > 0)
   {
      next_sibling = child->next_siblings;
      for (i = 0; i < child_count; i++)
      {
         if (AL_NET_ADDR_EQUAL(&immidigiate_sta[i], &child->sta_addr))
         {
            break;
         }
      }

      if (i >= child_count)
      {
         sta = &child->sta_addr;
         al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,
                      "MESH_AP	: INFO : Removing entry "AL_NET_ADDR_STR" due to HB sync %s : %d\n",
                      AL_NET_ADDR_TO_STR(sta), __func__, __LINE__);
         mesh_table_entry_remove(AL_CONTEXT sta, 10);
      }
      child = next_sibling;
      --protection_count;
   }

   if (protection_count <= 0)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MESH_AP	: Strange condition encountered in mesh_table_process_heart_beat, rebooting...");
      al_reboot_board(AL_CONTEXT AL_REBOOT_BOARD_CODE_MESH_TABLE_PROTECTION);
   }
  al_spin_unlock(mtable_splock, mtable_spvar);
  mesh_table_entry_release(AL_CONTEXT hb_ap);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s : %d\n", __func__,__LINE__);
   return RETURN_SUCCESS;
}


mesh_table_entry_t *mesh_table_entry_find_in_list(AL_CONTEXT_PARAM_DECL al_net_addr_t *sta_addr)
{
   mesh_table_entry_t *child = NULL;
   unsigned int       flags;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s : %d\n", __func__,__LINE__);

   al_spin_lock(mtable_splock, mtable_spvar);

   child = mesh_first_child;

   while (child != NULL)
   {
      if (AL_NET_ADDR_EQUAL(&child->sta_addr, sta_addr))
      {
		 al_spin_unlock(mtable_splock, mtable_spvar);
         return child;
      }
      child = child->next_siblings;
   }

   al_spin_unlock(mtable_splock, mtable_spvar);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s : %d\n", __func__,__LINE__);
   return child;
}


#ifdef _AL_ROVER_
void mesh_add_mesh_name_entry(AL_CONTEXT_PARAM_DECL_SINGLE)
{
   unsigned int        hash;
   mesh_ap_name_info_t *entry;
   mesh_ap_name_info_t *new_entry;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s : %d\n", __func__,__LINE__);

   new_entry = (mesh_ap_name_info_t *)al_heap_alloc(AL_CONTEXT sizeof(mesh_ap_name_info_t)AL_HEAP_DEBUG_PARAM);

   memcpy(&new_entry->ap_addr, _DS_MAC, sizeof(al_net_addr_t));
   get_device_name(AL_CONTEXT new_entry->name);

   hash = _MESH_MAC_HASH_FUNCTION(_DS_MAC, mesh_hash_size);

   entry = mesh_name_hash[hash];

   /* adding node to mesh hash */
   entry = mesh_name_hash[hash];
   if (entry != NULL)
   {
      new_entry->next = entry->next;
      new_entry->prev = entry;
      if (entry->next != NULL)
      {
         entry->next->prev = new_entry;
      }

      entry->next = new_entry;
   }
   else
   {
      new_entry->next      = NULL;
      new_entry->prev      = NULL;
      mesh_name_hash[hash] = new_entry;
   }

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s : %d\n", __func__,__LINE__);
}


char *get_ap_name(AL_CONTEXT_PARAM_DECL al_net_addr_t *ap_addr)
{
   unsigned int        hash;
   mesh_ap_name_info_t *entry;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s : %d\n", __func__,__LINE__);

   hash = _MESH_MAC_HASH_FUNCTION(ap_addr, mesh_hash_size);

   entry = mesh_name_hash[hash];

   //check whether entry already exists
   while (entry != NULL)
   {
      if (!memcmp(entry->ap_addr.bytes, ap_addr->bytes, MAC_ADDR_SIZE))
      {
         return (char *)(&entry->name);
      }
      entry = entry->next;
   }

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s : %d\n", __func__,__LINE__);
   return NULL;
}
#endif
