
/********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : imcp_snip.c
 * Comments : imcp snip functions impl
 * Created  : 4/13/2004
 * Author   : Anand Bhange
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * -----------------------------------------------------------------------------
 * | No  |Date      |  Comment                                        | Author |
 * -----------------------------------------------------------------------------
 * | 86  |1/27/2009 | Changes for Uplink Scan Functionality           | Sriram |
 * -----------------------------------------------------------------------------
 * | 85  |8/22/2008 | Changes for SIP                                 | Sriram |
 * -----------------------------------------------------------------------------
 * | 84  |1/22/2008 | Prints removed                                  |Abhijit |
 * -----------------------------------------------------------------------------
 * | 83  |12/4/2007 | Changes for Client Signal Information           | Sriram |
 * -----------------------------------------------------------------------------
 * | 82  |7/16/2007 | Drop packet on UDP 0xDEDE if not IMCP           | Sriram |
 * -----------------------------------------------------------------------------
 * | 81  |6/11/2007 | WM IMCP encrypt only for BCAST                  | Sriram |
 * -----------------------------------------------------------------------------
 * | 80  |6/6/2007  | Changes for DS Excerciser over WM               | Sriram |
 * -----------------------------------------------------------------------------
 * | 79  |4/11/2007 | Changes for Radio diagnostics                   | Sriram |
 * -----------------------------------------------------------------------------
 * | 78  |2/22/2007 | Added Effistream Req/Resp and Info packets      | Sriram |
 * -----------------------------------------------------------------------------
 * | 77  |1/30/2007 | Enable H/W encryption of IMCP packets to WM     | Sriram |
 * -----------------------------------------------------------------------------
 * | 76  |1/13/2007 | Set Default IMCP Rx action as SEND_WM_DS        | Sriram |
 * -----------------------------------------------------------------------------
 * | 75  |01/01/2007| Added Private Channel implementation            |Prachiti|
 * -----------------------------------------------------------------------------
 * | 74  |11/21/2006| Heartbeat encrypted                             | Sriram |
 * -----------------------------------------------------------------------------
 * | 73  |11/16/2006| Buffer pool changes                             | Sriram |
 * -----------------------------------------------------------------------------
 * | 72  |11/14/2006| Changes for heap debug support                  | Sriram |
 * -----------------------------------------------------------------------------
 * | 71  |3/29/2006 | Reboot required set for Restore defaults        | Sriram |
 * -----------------------------------------------------------------------------
 * | 70  |3/13/2006 | ACL list implemented                            | Sriram |
 * -----------------------------------------------------------------------------
 * | 69  |02/27/2006| Typo corrected in line 893                      | Sriram |
 * -----------------------------------------------------------------------------
 * | 68  |02/22/2006| dot11e added                                    | Bindu  |
 * -----------------------------------------------------------------------------
 * | 67  |02/20/2006| Config SQNR removed			                  | Abhijit|
 * -----------------------------------------------------------------------------
 * | 66  |02/08/2006| Config SQNR and Hide SSID added                 | Sriram |
 * -----------------------------------------------------------------------------
 * | 65  |02/03/2006| Generic command changes                         | Abhijit|
 * -----------------------------------------------------------------------------
 * | 64  |02/01/2006| Changes for IMCP sta info						  | Abhijit|
 * -----------------------------------------------------------------------------
 * | 63  |01/01/2006| Changes for IMCP Heartbeat2					  | Abhijit|
 * -----------------------------------------------------------------------------
 * | 62  |01/01/2006| Changes for IMCP Analyser Init                  | Abhijit|
 * -----------------------------------------------------------------------------
 * | 61  |12/16/2005|supported channels changes					      |prachiti|
 * -----------------------------------------------------------------------------
 * | 60  |11/25/2005|ACK Timeout IMCP packet added                    |Prachiti|
 * -----------------------------------------------------------------------------
 * | 59  |11/25/2005|Reg Domain IMCP packet added                     |Prachiti|
 * -----------------------------------------------------------------------------
 * | 58  |10/4/2005 | Changes for IMCP WM Info                        | Sriram |
 * -----------------------------------------------------------------------------
 * | 57  |9/20/2005 | MESH_INIT_STATUS IMCP packet dropped            | Sriram |
 * -----------------------------------------------------------------------------
 * | 56  |9/15/2005 | DS Excr,Case insensitive MESHID checking,etc    | Sriram |
 * -----------------------------------------------------------------------------
 * | 55  |9/12/2005 | IMCP packet RX and TX logic modified            | Sriram |
 * -----------------------------------------------------------------------------
 * | 54  |7/29/2005 | Misc changes                                    | Sriram |
 * -----------------------------------------------------------------------------
 * | 53  |6/9/2005  | IMCP version check added                        | Sriram |
 * -----------------------------------------------------------------------------
 * | 52  |5/17/2005 | Missing break statement problem fixed           | Anand  |
 * -----------------------------------------------------------------------------
 * | 51  |4/12/2005 | Packet allocation NULL checking added           | Sriram |
 * -----------------------------------------------------------------------------
 * | 50  |4/9/2005  | Added DS channel to vendor info                 | Sriram |
 * -----------------------------------------------------------------------------
 * | 49  |2/21/2005 | Vendor info added to beacons,HandShake Res to VI| Anand  |
 * -----------------------------------------------------------------------------
 * | 48  |2/14/2005 | VLanConfigInfo & ConfigReqRes Packets Added     | Anand  |
 * -----------------------------------------------------------------------------
 * | 47  |12/8/2004 | mesh_id Length fix                              | Sriram |
 * -----------------------------------------------------------------------------
 * | 46  |10/29/2004| Relay packet bug for decryption                 | Anand  |
 * -----------------------------------------------------------------------------
 * | 45  |10/28/2004| Changes for encryption/decryption               | Anand  |
 * -----------------------------------------------------------------------------
 * | 44  |10/28/2004| Changes for monitor radio                       | Anand  |
 * -----------------------------------------------------------------------------
 * | 43  |10/25/2004| encryption/decryption implemented               | Abhijit|
 * -----------------------------------------------------------------------------
 * | 42  |10/15/2004| mesh_id_length adjusted in imcp_snip_send       | Abhijit|
 * -----------------------------------------------------------------------------
 * | 41  |10/14/2004| AL_USE_ENCRYPTION added                         | Abhijit|
 * -----------------------------------------------------------------------------
 * | 40  |10/11/2004| Modifications for IMCP5.0                       | Abhijit|
 * -----------------------------------------------------------------------------
 * | 39  |8/26/2004 | Mesh Init Packet changes.                       | Anand  |
 * -----------------------------------------------------------------------------
 * | 38  |7/29/2004 | changes in AP Config IMCP packet                | Anand  |
 * -----------------------------------------------------------------------------
 * | 37  |7/26/2004 | changes in config imcp packet                   | Anand  |
 * -----------------------------------------------------------------------------
 * | 36  |7/25/2004 | Fixes for sub-tree information packet           | Sriram |
 * -----------------------------------------------------------------------------
 * | 35  |7/25/2004 | check for mac_addr in IP SETUP packet           | Anand  |
 * -----------------------------------------------------------------------------
 * | 34  |7/25/2004 | ap_sub_tree called                              | Anand  |
 * -----------------------------------------------------------------------------
 * | 33  |7/24/2004 | Fixed minor compilation warnings                | Sriram |
 * -----------------------------------------------------------------------------
 * | 32  |7/24/2004 | bug fixed mesh_hardware_info                    | Anand  |
 * -----------------------------------------------------------------------------
 * | 31  |7/23/2004 | bit rate added to HB                            | Anand  |
 * -----------------------------------------------------------------------------
 * | 30  |7/23/2004 | Moved ASSERT appropriately                      | Sriram |
 * -----------------------------------------------------------------------------
 * | 29  |7/21/2004 | IP_AP_START packet added.                       | Anand  |
 * -----------------------------------------------------------------------------
 * | 28  |7/19/2004 | AP_CONFIG_UPDATE packet added                   | Anand  |
 * -----------------------------------------------------------------------------
 * | 27  |6/17/2004 | Misc changes                                    | Sriram |
 * -----------------------------------------------------------------------------
 * | 26  |6/16/2004 | ASSERT statements for checking NULL values      | Anand  |
 * -----------------------------------------------------------------------------
 * | 25  |6/15/2004 | Posible Null pointer avoided (parent_ap_addr)   | Anand  |
 * -----------------------------------------------------------------------------
 * | 24  |6/14/2004 | parent_ap_addr set only if ds_net_if = 802.3    | Anand  |
 * -----------------------------------------------------------------------------
 * | 23  |6/11/2004 | hardware info request added to imcp             | Anand  |
 * -----------------------------------------------------------------------------
 * | 22  |6/9/2004  | broadcast_net_addr used instead of bc_addr      | Anand  |
 * -----------------------------------------------------------------------------
 * | 21  |6/8/2004  | Bug Fixed - Frame Control Value set corrected   | Anand  |
 * -----------------------------------------------------------------------------
 * | 20  |6/7/2004  | Filter macro used instead of al_print_log       | Anand  |
 * -----------------------------------------------------------------------------
 * | 19  |5/31/2004 | u name given to union for linux compilation     | Anand  |
 * -----------------------------------------------------------------------------
 * | 18  |5/28/2004 | release packet removed as transmit doing it     | Anand  |
 * -----------------------------------------------------------------------------
 * | 17  |5/26/2004 | Bug Fixed - for WM packets copy BC addr         | Anand  |
 * -----------------------------------------------------------------------------
 * | 16  |5/26/2004 | Bug Fixed - IMCP packet with 3rd Addr           | Anand  |
 * -----------------------------------------------------------------------------
 * | 15  |5/21/2004 | Log type added to al_print_log                  | Anand  |
 * -----------------------------------------------------------------------------
 * | 14  |5/20/2004 | receive returns correct value for further proces| Anand  |
 * -----------------------------------------------------------------------------
 * | 13  |5/20/2004 | print_log for RECEIVE moved after processing    | Anand  |
 * -----------------------------------------------------------------------------
 * | 12  |5/12/2004 | Bug Fixed Data not allocated in process HB      | Anand  |
 * -----------------------------------------------------------------------------
 * | 11  |5/11/2004 | src_ds_mac added to handshake_response          | Anand  |
 * -----------------------------------------------------------------------------
 * | 10  |5/11/2004 | net_if param added to send functions            | Anand  |
 * -----------------------------------------------------------------------------
 * |  9  |5/6/2004  | mac addr size set in get_packet functions       | Anand  |
 * -----------------------------------------------------------------------------
 * |  8  |5/5/2004  | Packet/Memory Leak Debug params added           | Anand  |
 * -----------------------------------------------------------------------------
 * |  7  |5/5/2004  | udp send functions added                        | Anand  |
 * -----------------------------------------------------------------------------
 * |  6  |4/30/2004 | Log messages Added                              | Anand  |
 * -----------------------------------------------------------------------------
 * |  5  |4/27/2004 | Ch removed fromHeartBeat/HS Response imcp Pckt  | Anand  |
 * -----------------------------------------------------------------------------
 * |  4  |4/27/2004 | Packet length corrected                         | Anand  |
 * -----------------------------------------------------------------------------
 * |  3  |4/27/2004 | HBI added in HeartBeat/HS Response imcp Packet  | Anand  |
 * -----------------------------------------------------------------------------
 * |  2  |4/26/2004 | Testing Base Line                               | Anand  |
 * -----------------------------------------------------------------------------
 * |  1  |4/21/2004 | **Made (major) changes for AL_CONTEXT**         | Anand  |
 * -----------------------------------------------------------------------------
 * |  0  |4/13/2004 | Created.                                        | Anand  |
 * -----------------------------------------------------------------------------
 ********************************************************************************/

#include <linux/string.h>
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/netdevice.h>
#include "al.h"
#include "al_print_log.h"
#include "al_802_11.h"
#include "access_point.h"
#include "sip_conf.h"
#include "mesh_defs.h"
#include "imcp_snip.h"
#include "al_impl_context.h"
#include "mesh.h"
#include "mesh_table.h"
#include "mesh_globals.h"
#include "mesh_imcp.h"
#include "mesh_errors.h"
#include "al_socket.h"
#include "mesh_ng.h"
#include "meshap.h"
#include "meshap_core.h"

#ifdef AL_USE_ENCRYPTION
	#include "al_codec.h"
#endif

#define UDP_PACKET_MAX_BUFFER_SIZE	1472
#define IMCP_UDP_PORT				(unsigned short)(0xDEDE)

#define IMCP_SNIP_VERSION_MAJOR			5
#define IMCP_SNIP_VERSION_MINOR			0

#include "imcp_heartbeat.c"
#include "imcp_sta.c"
#include "imcp_channel_scan.c"
#include "imcp_config.c"
#include "imcp_misc.c"
#include "imcp_snip_pool.c"
#define pkt_set_word(packet,position,value) do {\
  unsigned short word; \
  word = (unsigned short)value; \
  memcpy((unsigned char*)packet + position,&word,2); \
}while(0)

static int send_packet(AL_CONTEXT_PARAM_DECL al_net_if_t* net_if, al_packet_t* main_packet, bool send_pkt_mgmt_gw)
{
	unsigned char* temp;
	al_packet_t*	packet;

	AL_PRINT_LOG_FLOW_0( "MESH_AP	: send_packet()");

	AL_ASSERT("MESH_AP	: send_packet",net_if != NULL);
	AL_ASSERT("MESH_AP	: send_packet",main_packet != NULL);

	packet = al_allocate_packet(AL_CONTEXT AL_PACKET_DIRECTION_OUT AL_PACKET_DEBUG_PARAM);

	if(packet == NULL)
    {
        al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESHAP : ERROR : pkt is NULL : %s : %d\n", __func__,__LINE__);
		return -1;
    }
	AL_ASSERT("MESH_AP	: send_packet",packet != NULL);
	
	temp = packet->buffer;
	memcpy(temp, main_packet->buffer, main_packet->buffer_length);
	memcpy(packet, main_packet, sizeof(al_packet_t));
	packet->buffer = temp;
#if MULTIPLE_AP_THREAD
    packet->status = 0;
    packet->entry = NULL;
#endif

  if ( mesh_config->mgmt_gw_enable &&  send_pkt_mgmt_gw )
  {
    al_send_packet_up_stack_mgmt_gw(net_if, packet);
  }
	net_if->transmit(net_if, packet);	
	return 0;
}

static int send_ds_net_if(AL_CONTEXT_PARAM_DECL al_net_if_t* net_if, al_packet_t* main_packet, al_net_addr_t* parent_ap_addr)
{
	if(AL_NET_IF_IS_WIRELESS(net_if)) {
		memcpy(&main_packet->addr_3, &main_packet->addr_1, sizeof(al_net_addr_t));			
		memcpy(&main_packet->addr_2, &net_if->config.hw_addr, sizeof(al_net_addr_t));
		memcpy(&main_packet->addr_1, parent_ap_addr, sizeof(al_net_addr_t));
		/* set toDS to 1 as packet going up */
		main_packet->frame_control = AL_802_11_FC_TODS;
	}
	
	return send_packet(AL_CONTEXT net_if, main_packet, true);
}

static int send_wm_net_if(AL_CONTEXT_PARAM_DECL al_net_if_t* net_if, al_packet_t* main_packet)
{
	access_point_netif_config_info_t*	net_if_config_info;

	if(AL_NET_IF_IS_WIRELESS(net_if)) {
		
		memcpy(&main_packet->addr_2, &net_if->config.hw_addr, sizeof(al_net_addr_t));
		memcpy(&main_packet->addr_3, &net_if->config.hw_addr, sizeof(al_net_addr_t));			
		/* set FROMDS to 1 as packet going down */
		main_packet->frame_control = AL_802_11_FC_FROMDS;

		/**
		 * If security is enabled then we need to encrypt IMCP packets going
		 * to WMS
		 */

		net_if_config_info	= (access_point_netif_config_info_t*)net_if->config.ext_config_info;

		if(net_if_config_info->security_info.dot11.wep.enabled
		|| net_if_config_info->security_info.dot11.rsn.enabled) {
			if(AL_NET_ADDR_IS_BROADCAST(&main_packet->addr_1)) {
				main_packet->flags		|= AL_PACKET_FLAGS_ENCRYPTION;
				main_packet->key_index	= net_if_config_info->group_key.key_index;
			}
		}

	}
	return send_packet(AL_CONTEXT net_if, main_packet, false);
}



static int send_imcp_packet (AL_CONTEXT_PARAM_DECL int		imcp_type,
							 al_net_if_t*					net_if,
							 al_net_addr_t*					dest_addr,
							 al_net_addr_t*					parent_ap_addr,
							 unsigned char*					buffer,
							 int							buffer_length)
{
	int				ret;
	unsigned int	i;
	al_packet_t*	main_packet;

    //al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
	AL_PRINT_LOG_FLOW_0( "MESH_AP	: send_imcp_packet()");

	AL_ASSERT("MESH_AP	: send_imcp_packet", buffer != NULL);

	main_packet = al_allocate_packet(AL_CONTEXT AL_PACKET_DIRECTION_OUT AL_PACKET_DEBUG_PARAM);

	if(main_packet == NULL)
		return -1;

	AL_ASSERT("MESH_AP	: send_imcp_packet",main_packet != NULL);

	/* Generate UDP Header packet*/

	ret =  udp_setup_packet(main_packet,
		                     _DS_MAC,
							 IMCP_UDP_PORT,
							 &broadcast_net_addr,
							 IMCP_UDP_PORT,
							 buffer,
							 buffer_length);	

	if(ret) {
		(tx_rx_pkt_stats.packet_drop[66])++;
		al_release_packet(AL_CONTEXT main_packet);
		return ret;
	}


	/* Generate IP Header packet*/
	ret =  ip_setup_packet(main_packet);
	if(ret) {
		(tx_rx_pkt_stats.packet_drop[67])++;
		al_release_packet(AL_CONTEXT main_packet);
        al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"IP header pkt not generated ::  func : %s LINE : %d\n", __func__,__LINE__);
		return ret;
	}

	switch(imcp_type) {

	case IMCP_SNIP_PACKET_TYPE_CHANNEL_SCAN_LOCK:
	case IMCP_SNIP_PACKET_TYPE_CHANNEL_SCAN_UNLOCK:
	case IMCP_SNIP_PACKET_TYPE_CHANNEL_SCAN_LOCK_OBJECTION:
		send_ds_net_if(AL_CONTEXT mesh_hardware_info->ds_net_if, main_packet, parent_ap_addr);
		break;
	case IMCP_SNIP_PACKET_TYPE_AP_SUB_TREE_INFO:
		send_ds_net_if(AL_CONTEXT mesh_hardware_info->ds_net_if, main_packet, parent_ap_addr);
		break;
	case IMCP_SNIP_PACKET_TYPE_MESH_INIT_STATUS:
		{
			memcpy(&main_packet->addr_1, &broadcast_net_addr, sizeof(al_net_addr_t));
			for(i = 0; i < mesh_hardware_info->wm_net_if_count; i++)
				send_wm_net_if(AL_CONTEXT mesh_hardware_info->wms_conf[i]->net_if, main_packet);
		}
		break;
	case IMCP_SNIP_PACKET_TYPE_DS_EXCERCISER:
		if(net_if == NULL || dest_addr == NULL || net_if == mesh_hardware_info->ds_net_if) {
			memcpy(&main_packet->addr_1, parent_ap_addr, sizeof(al_net_addr_t));
			send_ds_net_if(AL_CONTEXT mesh_hardware_info->ds_net_if, main_packet, parent_ap_addr);
		} else {
			memcpy(&main_packet->addr_1, dest_addr, sizeof(al_net_addr_t));
			send_wm_net_if(AL_CONTEXT net_if, main_packet);
		}
		break;
	case IMCP_SNIP_PACKET_TYPE_WM_INFO_REQUEST:
	case IMCP_SNIP_PACKET_TYPE_WM_INFO_RESPONSE:
		if(net_if == mesh_hardware_info->ds_net_if) {
			send_ds_net_if(AL_CONTEXT mesh_hardware_info->ds_net_if, main_packet, parent_ap_addr);
		} else {
			memcpy(&main_packet->addr_1, &broadcast_net_addr, sizeof(al_net_addr_t));
			send_wm_net_if(AL_CONTEXT net_if, main_packet);
		}
		break;
	case IMCP_SNIP_PACKET_TYPE_BUFFER_COMMAND:
		if(net_if == mesh_hardware_info->ds_net_if) {
			send_ds_net_if(AL_CONTEXT mesh_hardware_info->ds_net_if, main_packet, parent_ap_addr);
		} else {
			memcpy(&main_packet->addr_1, &broadcast_net_addr, sizeof(al_net_addr_t));
			send_wm_net_if(AL_CONTEXT net_if, main_packet);
		}
		break;
	case IMCP_SNIP_PACKET_TYPE_BUFFER_COMMAND_ACK:
		if(net_if != mesh_hardware_info->ds_net_if) {
			memcpy(&main_packet->addr_1, dest_addr, sizeof(al_net_addr_t));
			send_wm_net_if(AL_CONTEXT net_if, main_packet);
		}
		break;
	case IMCP_SNIP_PACKET_TYPE_CHANNEL_CHANGE:
		/** Channel change notification assumed to be for downlinks only */
		send_wm_net_if(AL_CONTEXT net_if,main_packet);
		break;
	default:
		send_ds_net_if(AL_CONTEXT mesh_hardware_info->ds_net_if, main_packet, parent_ap_addr);
		memcpy(&main_packet->addr_1, &broadcast_net_addr, sizeof(al_net_addr_t));
		for(i=0; i < mesh_hardware_info->wm_net_if_count; i++)
			send_wm_net_if(AL_CONTEXT mesh_hardware_info->wms_conf[i]->net_if, main_packet);
		break;
	}

	tx_rx_pkt_stats.tx_imcp_pkt++;
	al_release_packet(AL_CONTEXT main_packet);
    //al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
	return 0;
}

int imcp_initialize(AL_CONTEXT_PARAM_DECL_SINGLE)
{
	AL_PRINT_LOG_FLOW_0( "MESH_AP	: imcp_initialize()");
	return 0;
}

#ifdef AL_USE_ENCRYPTION
static int encrypt_packet_data(AL_CONTEXT_PARAM_DECL unsigned char* buffer_ptr,int buffer_length)
{
	unsigned char*	output;
	int				ret;

    //al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
	output	= imcp_snip_create_buffer(AL_CONTEXT_SINGLE);

	ret = al_encrypt(AL_CONTEXT buffer_ptr,buffer_length,mesh_config->mesh_imcp_key,
					mesh_config->mesh_imcp_key_length,output,
					AL_ENCRYPTION_DECRYPTION_METHOD_AES_ECB);

	if(ret > 0) {
		memcpy(buffer_ptr,output,ret);
	}

	imcp_snip_release_buffer(AL_CONTEXT output);

    //al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
	return ret;
}
#endif

int imcp_snip_send (AL_CONTEXT_PARAM_DECL imcp_packet_info_t* packet_info)
{
	unsigned char*	buf;
	unsigned char*	p;
	al_net_addr_t*  parent_ap_addr;
	int		ret_type;
	int		encrypt_packet; 
	int		buf_length = 10; /* 4 for IMCP, 2 for version, 2 for flags, 2 for packet type */ 
	unsigned short  cpu2le16;
	

	AL_PRINT_LOG_FLOW_0( "MESH_AP	: imcp_snip_send()");
	AL_ASSERT("MESH_AP	: imcp_snip_send", packet_info != NULL);

	buf	= imcp_snip_create_buffer(AL_CONTEXT_SINGLE);
	p	= buf;

	memcpy(p, "IMCP", 4);	p += 4;
	*p = IMCP_SNIP_VERSION_MAJOR;	p += 1;
	*p = IMCP_SNIP_VERSION_MINOR;	p += 1;

        cpu2le16 = al_impl_cpu_to_le16(packet_info->flags);
	al_print_log(AL_CONTEXT AL_LOG_TYPE_DEBUG," send_flags : %hd func : %s LINE : %d\n",cpu2le16,__func__,__LINE__);
        memcpy(p, &cpu2le16, sizeof(unsigned short));
        p += sizeof(unsigned short);

        cpu2le16 = al_impl_cpu_to_le16(packet_info->packet_type);
	al_print_log(AL_CONTEXT AL_LOG_TYPE_DEBUG," send_packet_type : %hd func : %s LINE : %d\n",cpu2le16,__func__,__LINE__);
        memcpy(p, &cpu2le16, sizeof(unsigned short));
        p += sizeof(unsigned short);

	*p = mesh_config->meshid_length;			p += 1;
	memcpy(p,mesh_config->meshid,mesh_config->meshid_length);	p += mesh_config->meshid_length;

	buf_length = (int)(p - buf);

	if(AL_NET_IF_IS_WIRELESS(mesh_hardware_info->ds_net_if)) {
		if(current_parent != NULL)
			parent_ap_addr =  &current_parent->bssid;
		else
			parent_ap_addr	= &zeroed_net_addr;
	} else
		parent_ap_addr	= NULL;

	encrypt_packet = 0;

	switch(packet_info->packet_type) {
	case IMCP_SNIP_PACKET_TYPE_HEARTBEAT :																					//IMCP 1
		ret_type = imcp_snip_heartbeat_get_buffer(AL_CONTEXT packet_info, p, UDP_PACKET_MAX_BUFFER_SIZE-buf_length);
		if(ret_type > 0)
			buf_length += ret_type;
#ifdef AL_USE_ENCRYPTION
		encrypt_packet = 1;
#endif
		break;
	case IMCP_SNIP_PACKET_TYPE_STA_ASSOC_NOTIFICATION :																		//IMCP 2
		ret_type = imcp_snip_sta_assoc_notification_get_buffer(AL_CONTEXT packet_info, p, UDP_PACKET_MAX_BUFFER_SIZE-buf_length);
		if(ret_type > 0)
			buf_length += ret_type;
		break;
	case IMCP_SNIP_PACKET_TYPE_STA_DISASSOC_NOTIFICATION :																	//IMCP 3
		ret_type = imcp_snip_sta_disassoc_notification_get_buffer(AL_CONTEXT packet_info, p, UDP_PACKET_MAX_BUFFER_SIZE-buf_length);
		if(ret_type > 0)
			buf_length += ret_type;
		break;
	case IMCP_SNIP_PACKET_TYPE_CHANNEL_SCAN_LOCK :																			//IMCP 6
		ret_type = imcp_snip_channel_scan_lock_get_buffer(AL_CONTEXT packet_info, p, UDP_PACKET_MAX_BUFFER_SIZE-buf_length);
		if(ret_type > 0)
			buf_length += ret_type;
		break;
	case IMCP_SNIP_PACKET_TYPE_CHANNEL_SCAN_UNLOCK :																		//IMCP 7
		ret_type = imcp_snip_channel_scan_unlock_get_buffer(AL_CONTEXT packet_info, p, UDP_PACKET_MAX_BUFFER_SIZE-buf_length);
		if(ret_type > 0)
			buf_length += ret_type;
		break;
	case IMCP_SNIP_PACKET_TYPE_CHANNEL_SCAN_LOCK_OBJECTION :																//IMCP 8
		ret_type = imcp_snip_channel_scan_lock_objection_get_buffer(AL_CONTEXT packet_info, p, UDP_PACKET_MAX_BUFFER_SIZE-buf_length);
		if(ret_type > 0)
			buf_length += ret_type;
		break;
	case IMCP_SNIP_PACKET_TYPE_RESET :																						//IMCP 9
		ret_type = imcp_snip_reset_get_buffer(AL_CONTEXT packet_info, p, UDP_PACKET_MAX_BUFFER_SIZE-buf_length);
		if(ret_type > 0)
			buf_length += ret_type;
		break;
	case IMCP_SNIP_PACKET_TYPE_AP_HARDWARE_INFO :																			//IMCP 10
		ret_type = imcp_snip_hardware_info_get_buffer(AL_CONTEXT packet_info, p, UDP_PACKET_MAX_BUFFER_SIZE-buf_length);
		if(ret_type > 0)
			buf_length += ret_type;
#ifdef AL_USE_ENCRYPTION
		encrypt_packet = 1;
#endif
		break;
		/* intermidiate packets are not sent by mesh (11,12,13)*/
	case IMCP_SNIP_PACKET_TYPE_AP_SUB_TREE_INFO :																			//IMCP 14
		ret_type = imcp_snip_ap_sub_tree_info_get_buffer(AL_CONTEXT packet_info, p, UDP_PACKET_MAX_BUFFER_SIZE-buf_length);
		if(ret_type > 0)
			buf_length += ret_type;
		break;
		/* intermidiate packets are not sent by mesh (15,16)*/
	case IMCP_SNIP_PACKET_TYPE_MESH_INIT_STATUS :																			//IMCP 21
		ret_type = imcp_snip_mesh_init_status_get_buffer(AL_CONTEXT packet_info, p, UDP_PACKET_MAX_BUFFER_SIZE-buf_length);
		if(ret_type > 0)
			buf_length += ret_type;
		break;

	case IMCP_SNIP_PACKET_TYPE_MESH_IMCP_KEY_UPDATE :																		//IMCP 22
		ret_type = imcp_snip_mesh_imcp_key_update_get_buffer(AL_CONTEXT packet_info, p, UDP_PACKET_MAX_BUFFER_SIZE-buf_length);
		if(ret_type > 0)
			buf_length += ret_type;
#ifdef AL_USE_ENCRYPTION
		encrypt_packet = 1;
#endif
		break;
	case IMCP_SNIP_PACKET_TYPE_DS_EXCERCISER:
		ret_type	= imcp_snip_ds_excerciser_get_buffer(AL_CONTEXT packet_info,p,UDP_PACKET_MAX_BUFFER_SIZE-buf_length);
		if(ret_type > 0)
			buf_length += ret_type;
#ifdef AL_USE_ENCRYPTION
		encrypt_packet = 1;
#endif
		break;
	case IMCP_SNIP_PACKET_TYPE_WM_INFO_REQUEST:
		ret_type	= imcp_snip_wm_info_request_get_buffer(AL_CONTEXT packet_info,p,UDP_PACKET_MAX_BUFFER_SIZE-buf_length);
		if(ret_type > 0)
			buf_length += ret_type;
#ifdef AL_USE_ENCRYPTION
		encrypt_packet = 1;
#endif
		break;
	case IMCP_SNIP_PACKET_TYPE_WM_INFO_RESPONSE:
		ret_type	= imcp_snip_wm_info_response_get_buffer(AL_CONTEXT packet_info,p,UDP_PACKET_MAX_BUFFER_SIZE-buf_length);
		if(ret_type > 0)
			buf_length += ret_type;
#ifdef AL_USE_ENCRYPTION
		encrypt_packet = 1;
#endif
		break;
	case IMCP_SNIP_PACKET_TYPE_HEARTBEAT2 :																					//IMCP 33
		ret_type = imcp_snip_heartbeat2_get_buffer(AL_CONTEXT packet_info, p, UDP_PACKET_MAX_BUFFER_SIZE-buf_length);
		if(ret_type > 0)
			buf_length += ret_type;
#ifdef AL_USE_ENCRYPTION
		encrypt_packet = 1;
#endif
		break;
	case IMCP_SNIP_PACKET_TYPE_IF_SUPPORTED_CHANNELS_INFO :																			//IMCP 40
		ret_type = imcp_snip_if_get_supported_channels_get_buffer(AL_CONTEXT packet_info, p, UDP_PACKET_MAX_BUFFER_SIZE-buf_length);
		if(ret_type > 0)
			buf_length += ret_type;
#ifdef AL_USE_ENCRYPTION
		encrypt_packet = 1;
#endif
		break;
	case IMCP_SNIP_PACKET_TYPE_STA_INFO_RESPONSE:																			//IMCP 42
		ret_type = imcp_snip_sta_info_get_buffer(AL_CONTEXT packet_info, p, UDP_PACKET_MAX_BUFFER_SIZE-buf_length);
		if(ret_type > 0)
			buf_length += ret_type;
#ifdef AL_USE_ENCRYPTION
		encrypt_packet = 1;
#endif
		break;
	case IMCP_SNIP_PACKET_TYPE_GENERIC_COMMAND_REQ_RES :																	// IMCP 45	
		ret_type = imcp_snip_generic_command_req_res_get_buffer(AL_CONTEXT packet_info, p, UDP_PACKET_MAX_BUFFER_SIZE-buf_length);
		if(ret_type > 0)
			buf_length += ret_type;
#ifdef AL_USE_ENCRYPTION
		encrypt_packet = 1;
#endif
		break;
	case IMCP_SNIP_PACKET_TYPE_BUFFER_COMMAND:
		ret_type = imcp_snip_buffer_command_get_buffer(AL_CONTEXT packet_info,p,UDP_PACKET_MAX_BUFFER_SIZE-buf_length);
		if(ret_type > 0)
			buf_length += ret_type;
#ifdef AL_USE_ENCRYPTION
		encrypt_packet = 1;
#endif
		break;
	case IMCP_SNIP_PACKET_TYPE_BUFFER_COMMAND_ACK:
		ret_type = imcp_snip_buffer_command_ack_get_buffer(AL_CONTEXT packet_info,p,UDP_PACKET_MAX_BUFFER_SIZE-buf_length);
		if(ret_type > 0)
			buf_length += ret_type;
#ifdef AL_USE_ENCRYPTION
		encrypt_packet = 1;
#endif
		break;
	case IMCP_SNIP_PACKET_TYPE_DOWNLINK_SATURATION_INFO:
		ret_type	= imcp_snip_downlink_saturation_get_buffer(AL_CONTEXT packet_info,p,UDP_PACKET_MAX_BUFFER_SIZE-buf_length);
		if(ret_type > 0)
			buf_length += ret_type;
#ifdef AL_USE_ENCRYPTION
		encrypt_packet = 1;
#endif
		break;
	case IMCP_SNIP_PACKET_TYPE_RADIO_DIAGNOSTIC_DATA:
		ret_type	= imcp_snip_radio_diag_data_get_buffer(AL_CONTEXT packet_info,p,UDP_PACKET_MAX_BUFFER_SIZE-buf_length);
		if(ret_type > 0)
			buf_length += ret_type;
		break;
	case IMCP_SNIP_PACKET_TYPE_NODE_MOVE_NOTIFICATION:
		ret_type	= imcp_snip_node_move_notification_get_buffer(AL_CONTEXT packet_info,p,UDP_PACKET_MAX_BUFFER_SIZE-buf_length);
		if(ret_type > 0)
			buf_length += ret_type;
		break;
	case IMCP_SNIP_PACKET_TYPE_CHANNEL_CHANGE:
		ret_type	= imcp_snip_channel_change_get_buffer(AL_CONTEXT packet_info,p,UDP_PACKET_MAX_BUFFER_SIZE-buf_length);
		if(ret_type > 0)
			buf_length += ret_type;
		break;
	case IMCP_SNIP_PACKET_TYPE_CLIENT_SIGNAL_INFO:
		ret_type	= imcp_snip_client_signal_info_get_buffer(AL_CONTEXT packet_info,p,UDP_PACKET_MAX_BUFFER_SIZE-buf_length);
		if(ret_type > 0)
			buf_length += ret_type;
#ifdef AL_USE_ENCRYPTION
		encrypt_packet = 1;
#endif
		break;
	case IMCP_SNIP_PACKET_TYPE_SIP_PRIVATE:
		ret_type	= imcp_snip_sip_private_get_buffer(AL_CONTEXT packet_info,p,UDP_PACKET_MAX_BUFFER_SIZE-buf_length);
		if(ret_type > 0)
			buf_length += ret_type;
		break;
	default:
		imcp_snip_release_buffer(AL_CONTEXT buf);
		return -1;
	}

#ifdef AL_USE_ENCRYPTION

	if(encrypt_packet == 1) {
#define PACKET_TYPE_OFFSET	6
#define DATA_OFFSET		(11 + mesh_config->meshid_length)	

		p	= buf + PACKET_TYPE_OFFSET;

		cpu2le16 = al_impl_cpu_to_le16((packet_info->flags | IMCP_USE_ENCRYPTION));
                memcpy(p, &cpu2le16, sizeof(unsigned short));
		buf_length = encrypt_packet_data(AL_CONTEXT buf + DATA_OFFSET,(buf_length - DATA_OFFSET));
		if(buf_length <= 0)
        {
            al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"buffer len is not proper : %d func : %s LINE : %d\n",buf_length,  __func__,__LINE__);
			return -1;
        }
		buf_length += DATA_OFFSET;

#undef PACKET_TYPE_OFFSET 
#undef DATA_OFFSET
	}

#endif

	if(send_imcp_packet(AL_CONTEXT packet_info->packet_type,
		                packet_info->net_if,
						packet_info->dest_addr,
						parent_ap_addr,
						buf,
						buf_length)) {

		AL_PRINT_LOG_ERROR_0("MESH_AP	: SOCKET ERROR IMCP PACKET SEND ");

	}

	imcp_snip_release_buffer(AL_CONTEXT buf);
	
	return 0;
}

#ifdef AL_USE_ENCRYPTION
static int decrypt_packet_data(AL_CONTEXT_PARAM_DECL unsigned char* buffer_ptr,int buffer_length, unsigned char* output)
{
	int				ret;

	ret = al_decrypt(AL_CONTEXT buffer_ptr,buffer_length,mesh_config->mesh_imcp_key,
					mesh_config->mesh_imcp_key_length,output,
					AL_ENCRYPTION_DECRYPTION_METHOD_AES_ECB);
	
	if(ret > 0){
		return 0;
	}

	return -1;
}
#endif

int imcp_snip_receive (AL_CONTEXT_PARAM_DECL al_net_if_t* net_if, al_packet_t* packet)
{
	unsigned char*		p;
	imcp_packet_info_t*	pi;
	int					ret_type,ret,data_encrypted=0;
	unsigned char*		buf;
	unsigned int		buf_length,imcp_header_length;
	unsigned char*		output;

	unsigned short          le2cpu16;

	buf		= packet->buffer + packet->position;
	buf_length	= packet->data_length;

	output	= imcp_snip_create_buffer(AL_CONTEXT_SINGLE);

	AL_ASSERT("MESH_AP	: imcp_snip_receive", net_if != NULL);
	AL_ASSERT("MESH_AP	: imcp_snip_receive", buf != NULL);
	AL_ASSERT("MESH_AP	: imcp_snip_receive", buf_length >= 33);

	p = buf + 28;  /* IP HEADER = 20 + UDP HEADER = 8*/

	if(memcmp(p, "IMCP", 4)) {
		imcp_snip_release_buffer(AL_CONTEXT output);
		return ACCESS_POINT_PACKET_DECISION_DROP;
	}

  /* We have recived an IMCP packet, we need to send it up to the mgmt gw */
	if(mesh_config->mgmt_gw_enable)
	{
       al_send_packet_up_stack_mgmt_gw(net_if, packet);
	}

	p += 4;
	
	if((*p) != IMCP_SNIP_VERSION_MAJOR || *(p+1) != IMCP_SNIP_VERSION_MINOR) {		/* Check IMCP version */
		imcp_snip_release_buffer(AL_CONTEXT output);
		return IMCP_SNIP_ERROR_WRONG_SNIP_VERSION_OR_TYPE;
	}
	p += 2;

	pi	= imcp_snip_create_packet(AL_CONTEXT_SINGLE);


        memcpy(&le2cpu16, p, sizeof(unsigned short));
        pi->flags = al_impl_le16_to_cpu(le2cpu16);

        if( pi->flags & IMCP_USE_ENCRYPTION) {
                data_encrypted  = 1;
                pi->flags &= (unsigned short)IMCP_LEN_MASK;        
        }
        p += sizeof(unsigned short);

	memcpy(&le2cpu16, p, sizeof(unsigned short));
        pi->packet_type = al_impl_le16_to_cpu(le2cpu16);
        p += sizeof(unsigned short);

	pi->net_if		= net_if;
	ret				= *p;	p++;

	if(mesh_config->meshid_length != ret
//RAMESH16MIG 
	|| strncasecmp(p,mesh_config->meshid,mesh_config->meshid_length)) {
		imcp_snip_release_buffer(AL_CONTEXT output);
		imcp_snip_release_packet(AL_CONTEXT pi);
		return IMCP_SNIP_ERROR_WRONG_MESHID;
	}

	p += ret;

	imcp_header_length = p - (buf + 28);

	ret = IMCP_SNIP_ERROR_NOT_IMPLEMENTED;

#ifdef AL_USE_ENCRYPTION	
	if(data_encrypted == 1){
		decrypt_packet_data(AL_CONTEXT p,buf_length - imcp_header_length, output);
		p = output;
	}
#endif
	switch(pi->packet_type) {
	case IMCP_SNIP_PACKET_TYPE_HEARTBEAT ://  IMCP 1
		ret_type = imcp_snip_heartbeat_get_packet(AL_CONTEXT p, buf_length-imcp_header_length, pi);
		if(ret_type >= 0)
			ret = mesh_imcp_process_packet_heartbeat(AL_CONTEXT pi, packet->rssi);
		break;
	case IMCP_SNIP_PACKET_TYPE_STA_ASSOC_NOTIFICATION :	//  IMCP 2
		ret_type = imcp_snip_sta_assoc_notification_get_packet(AL_CONTEXT p, buf_length-imcp_header_length, pi);		
		if(ret_type >= 0)
			ret = mesh_imcp_process_packet_sta_assoc_notification(AL_CONTEXT 	 pi);
		break;
	case IMCP_SNIP_PACKET_TYPE_STA_DISASSOC_NOTIFICATION :	//  IMCP 3
		ret_type = imcp_snip_sta_disassoc_notification_get_packet(AL_CONTEXT p, buf_length-imcp_header_length, pi);
		if(ret_type >= 0)
			ret = mesh_imcp_process_packet_sta_disassoc_notification(AL_CONTEXT pi);
		break;
	case IMCP_SNIP_PACKET_TYPE_CHANNEL_SCAN_LOCK :		//  IMCP 6
		ret_type = imcp_snip_channel_scan_lock_get_packet(AL_CONTEXT p, buf_length-imcp_header_length, pi);
		if(ret_type >= 0)
			ret = mesh_imcp_process_packet_channel_scan_lock(AL_CONTEXT pi);
		break;
	case IMCP_SNIP_PACKET_TYPE_CHANNEL_SCAN_UNLOCK :	//  IMCP 7
		ret_type = imcp_snip_channel_scan_unlock_get_packet(AL_CONTEXT p, buf_length-imcp_header_length, pi);
		if(ret_type >= 0)
			ret = mesh_imcp_process_packet_channel_scan_unlock(AL_CONTEXT pi);
		break;
	case IMCP_SNIP_PACKET_TYPE_CHANNEL_SCAN_LOCK_OBJECTION :	//  IMCP 8
		ret_type = imcp_snip_channel_scan_lock_objection_get_packet(AL_CONTEXT p, buf_length-imcp_header_length, pi);
		if(ret_type >= 0)
			ret = mesh_imcp_process_packet_channel_scan_lock_objection(AL_CONTEXT pi);
		break;
	case IMCP_SNIP_PACKET_TYPE_RESET :			//  IMCP 9
		if(imcp_snip_is_packet_for_ap(AL_CONTEXT p, buf_length-imcp_header_length) == RETURN_SUCCESS) {
			ret	= ACCESS_POINT_PACKET_DECISION_SEND_UP_IMCP;
		} else {
			ret	= ACCESS_POINT_PACKET_DECISION_SEND_WM_DS;
		}
		break;
	case IMCP_SNIP_PACKET_TYPE_AP_HARDWARE_INFO :		//  IMCP 10
		ret	= ACCESS_POINT_PACKET_DECISION_SEND_WM_DS;
		break;
	case IMCP_SNIP_PACKET_TYPE_CONFIGURATION_INFO :		//  IMCP 11
		ret_type = imcp_snip_ap_config_update_get_packet(AL_CONTEXT p, buf_length-imcp_header_length, pi);
		if(ret_type >= 0){
			ret = mesh_imcp_process_packet_ap_config_update(AL_CONTEXT pi);
		}
		al_heap_free(AL_CONTEXT pi->u.ap_config_info.interface_info);
	 	break;
	case IMCP_SNIP_PACKET_TYPE_AP_CONFIGURATION_REQ_RES :	//  IMCP 12		
		ret_type = imcp_snip_config_request_response_get_packet(AL_CONTEXT p, buf_length-imcp_header_length, pi);
		if(ret_type >= 0)
			ret = mesh_imcp_process_packet_config_req_res(AL_CONTEXT pi);
		break;
	case IMCP_SNIP_PACKET_TYPE_AP_HARDWARE_INFO_REQUEST:	//  IMCP 13
		ret_type = imcp_snip_hardware_info_request_get_packet(AL_CONTEXT p, buf_length-imcp_header_length, pi);
		if(ret_type >= 0)
			ret = mesh_imcp_process_packet_hardware_info_request(AL_CONTEXT pi);
		break;
	case IMCP_SNIP_PACKET_TYPE_AP_SUB_TREE_INFO :		//  IMCP 14
		ret_type = imcp_snip_ap_sub_tree_info_get_packet(AL_CONTEXT p, buf_length-imcp_header_length, pi);
		if(ret_type >= 0)
			ret = mesh_imcp_process_packet_ap_sub_tree_info(AL_CONTEXT pi);
		break;
	case IMCP_SNIP_PACKET_TYPE_IP_CONFIG_INFO :		//  IMCP 15
		if(imcp_snip_ip_config_get_packet(AL_CONTEXT p, buf_length-imcp_header_length, pi) == RETURN_SUCCESS) {
			ret	= ACCESS_POINT_PACKET_DECISION_SEND_UP_IMCP;
		} else {
			ret	= ACCESS_POINT_PACKET_DECISION_SEND_WM_DS;
		}
		break;
	case IMCP_SNIP_PACKET_TYPE_IP_CONFIG_INFO_REQ_RES :	//  IMCP 16
		ret_type = imcp_snip_config_request_response_get_packet(AL_CONTEXT p, buf_length-imcp_header_length, pi);
		if(ret_type >= 0)
			ret = mesh_imcp_process_packet_config_req_res(AL_CONTEXT pi);
		break;
	
		/* IP AP START packet will go directly to UP Stack */											//  IMCP 17  

	case IMCP_SNIP_PACKET_TYPE_SW_UPDATE_REQUEST:		//  IMCP 18
	case IMCP_SNIP_PACKET_TYPE_SW_UPDATE_RESPONSE:	//  IMCP 19
	case IMCP_SNIP_PACKET_TYPE_SW_UPDATE_STATUS:		//  IMCP 20
		if(imcp_snip_is_packet_for_ap(AL_CONTEXT p, buf_length-imcp_header_length) == RETURN_SUCCESS) {
			ret	= ACCESS_POINT_PACKET_DECISION_SEND_UP_IMCP;
		} else {
			ret	= ACCESS_POINT_PACKET_DECISION_SEND_WM_DS;
		}
		break;

	/* Mesh Init Status Send to WMs only */

	case IMCP_SNIP_PACKET_TYPE_MESH_IMCP_KEY_UPDATE:	// IMCP 22
		ret_type = imcp_snip_mesh_imcp_key_update_get_packet(AL_CONTEXT p, buf_length-imcp_header_length, pi);
		if(ret_type >= 0){
			ret = mesh_imcp_process_packet_mesh_imcp_key_update(AL_CONTEXT pi);
		}
		break;
	case IMCP_SNIP_PACKET_TYPE_RESPONSE_PACKET:		// IMCP 23
		ret	= ACCESS_POINT_PACKET_DECISION_SEND_WM_DS;
		break;

	case IMCP_SNIP_PACKET_TYPE_VLAN_CONFIG_INFO:		// IMCP 24
		if(imcp_snip_is_packet_for_ap(AL_CONTEXT p, buf_length-imcp_header_length) == RETURN_SUCCESS) {
			ret	= ACCESS_POINT_PACKET_DECISION_SEND_UP_IMCP;
		} else {
			ret	= ACCESS_POINT_PACKET_DECISION_SEND_WM_DS;
		}
		break;

	case IMCP_SNIP_PACKET_TYPE_CONFIG_REQ_RES:		// IMCP 25
		if(imcp_snip_is_packet_for_ap(AL_CONTEXT p, buf_length-imcp_header_length) == RETURN_SUCCESS) {
			ret	= ACCESS_POINT_PACKET_DECISION_SEND_UP_IMCP;
		} else {
			ret	= ACCESS_POINT_PACKET_DECISION_SEND_WM_DS;
		}
		break;
	case IMCP_SNIP_PACKET_TYPE_MESH_INIT_STATUS :
		ret	= ACCESS_POINT_PACKET_DECISION_DROP;
		break;
	case IMCP_SNIP_PACKET_TYPE_DS_EXCERCISER:
		if(net_if != _DS_NET_IF) {
			access_point_update_sta_last_rx(AL_CONTEXT &packet->addr_2,1);
		}
		ret	= ACCESS_POINT_PACKET_DECISION_DROP;
		break;
	case IMCP_SNIP_PACKET_TYPE_CONFIGD_WATCHDOG_REQUEST:
		/**
		 * We only entertain WATCHDOG requests coming over the WM
		 * Moreover Watchdog requests are directly sent up stack
		 */
		if(net_if == _DS_NET_IF) {
			ret	= ACCESS_POINT_PACKET_DECISION_DROP;
		} else {
			ret = ACCESS_POINT_PACKET_DECISION_SEND_UP_IMCP;
		}
		break;
	case IMCP_SNIP_PACKET_TYPE_WM_INFO_REQUEST:
		ret_type = imcp_snip_wm_info_request_get_packet(AL_CONTEXT p, buf_length-imcp_header_length, pi);
		if(ret_type >= 0)
			ret = mesh_imcp_process_packet_wm_info_request(AL_CONTEXT pi);
		break;
	case IMCP_SNIP_PACKET_TYPE_WM_INFO_RESPONSE:
		ret_type = imcp_snip_wm_info_response_get_packet(AL_CONTEXT p, buf_length-imcp_header_length, pi);
		if(ret_type >= 0)
			ret = mesh_imcp_process_packet_wm_info_response(AL_CONTEXT pi);
		break;
	case IMCP_SNIP_PACKET_TYPE_SET_TESTER_TESTEE_MODE_REQUEST:											// IMCP 32
		if(imcp_snip_is_packet_for_ap(AL_CONTEXT p, buf_length-imcp_header_length) == RETURN_SUCCESS) {
			ret_type = imcp_snip_tester_testee_mode_request_get_packet(AL_CONTEXT p, buf_length-imcp_header_length, pi);
			if(ret_type >= 0){
				ret = mesh_imcp_process_packet_tester_testee_mode_request(AL_CONTEXT pi);
			}
		}
		break;
	case IMCP_SNIP_PACKET_TYPE_RESTORE_DEFAULTS_REQ:															// IMCP 34
		if(imcp_snip_is_packet_for_ap(AL_CONTEXT p, buf_length-imcp_header_length) == RETURN_SUCCESS) {
			SET_MESH_FLAG(MESH_FLAG_REBOOT_REQUIRED);
			ret	= ACCESS_POINT_PACKET_DECISION_SEND_UP_IMCP;
		} else {
			ret	= ACCESS_POINT_PACKET_DECISION_SEND_WM_DS;
		}
		break;
	case IMCP_SNIP_PACKET_TYPE_REG_DOMAIN_REQ_RES:															// IMCP 35
		if(imcp_snip_is_packet_for_ap(AL_CONTEXT p, buf_length-imcp_header_length) == RETURN_SUCCESS) {
			ret	= ACCESS_POINT_PACKET_DECISION_SEND_UP_IMCP;
		} else {
			ret	= ACCESS_POINT_PACKET_DECISION_SEND_WM_DS;
		}
		break;
	case IMCP_SNIP_PACKET_TYPE_REG_DOMAIN_INFO:														// IMCP 36
		ret_type = imcp_snip_reg_domain_cc_update_get_packet(AL_CONTEXT p, buf_length-imcp_header_length, pi);
		if(ret_type >= 0)
			ret = mesh_imcp_process_packet_reg_domain_cc_config_update(AL_CONTEXT pi);
		break;
	case IMCP_SNIP_PACKET_TYPE_ACK_TIMEOUT_REQ_RES:															// IMCP 37
		if(imcp_snip_is_packet_for_ap(AL_CONTEXT p, buf_length-imcp_header_length) == RETURN_SUCCESS) {
			ret	= ACCESS_POINT_PACKET_DECISION_SEND_UP_IMCP;
		} else {
			ret	= ACCESS_POINT_PACKET_DECISION_SEND_WM_DS;
		}
		break;
	case IMCP_SNIP_PACKET_TYPE_ACK_TIMEOUT:														// IMCP 38
		ret_type = imcp_snip_ack_timeout_update_get_packet(AL_CONTEXT p, buf_length-imcp_header_length, pi);
		if(ret_type >= 0){
			ret = mesh_imcp_process_packet_ack_timeout_config_update(AL_CONTEXT pi);
		}
		al_heap_free(AL_CONTEXT pi->u.ack_timeout.interface_info);
		break;
	case IMCP_SNIP_PACKET_TYPE_IF_SUPPORTED_CHANNELS_REQUEST:												//  IMCP 39
		ret_type = imcp_snip_supported_channels_request_get_packet(AL_CONTEXT p, buf_length-imcp_header_length, pi);
		if(ret_type >= 0)
			ret = mesh_imcp_process_packet_supported_channels_request(AL_CONTEXT pi);
		break;
	case IMCP_SNIP_PACKET_TYPE_IF_SUPPORTED_CHANNELS_INFO :													//  IMCP 40
		ret	= ACCESS_POINT_PACKET_DECISION_SEND_WM_DS;
		break;
	case IMCP_SNIP_PACKET_TYPE_STA_INFO_REQUEST :														//  IMCP 41
		ret_type = imcp_snip_sta_info_request_get_packet(AL_CONTEXT p, buf_length-imcp_header_length, pi);
		if(ret_type >= 0)
			ret = mesh_imcp_process_packet_sta_info_request(AL_CONTEXT pi);
		break;
	case IMCP_SNIP_PACKET_TYPE_GENERIC_COMMAND_REQ_RES :													// IMCP 45
		if(imcp_snip_is_packet_for_ap(AL_CONTEXT p, buf_length-imcp_header_length) == RETURN_SUCCESS) {
			ret_type = imcp_snip_generic_command_req_res_get_packet(AL_CONTEXT p, buf_length-imcp_header_length, pi);
			if(ret_type >= 0)
				ret	= mesh_imcp_process_packet_generic_command_req_res(AL_CONTEXT pi);
		} else {
			ret	= ACCESS_POINT_PACKET_DECISION_SEND_WM_DS;
		}
		break;
	case IMCP_SNIP_PACKET_TYPE_HIDE_SSID_INFO:
		ret_type = imcp_snip_hide_ssid_update_get_packet(AL_CONTEXT p, buf_length-imcp_header_length, pi);
		if(ret_type >= 0){
			ret = mesh_imcp_process_packet_hide_ssid_config_update(AL_CONTEXT pi);
		}
		al_heap_free(AL_CONTEXT pi->u.hide_ssid_info.interface_info);
		break;
	case IMCP_SNIP_PACKET_TYPE_DOT11E_CATEGORY_INFO :															//  IMCP 43
		ret_type = imcp_snip_dot11e_category_update_get_packet(AL_CONTEXT p, buf_length-imcp_header_length, pi);
		if(ret_type >= 0)
			ret = mesh_imcp_process_packet_dot11e_category_update(AL_CONTEXT pi);
		break;
    case IMCP_SNIP_PACKET_TYPE_DOT11E_CONFIG_INFO :																//  IMCP 44
		if(imcp_snip_is_packet_for_ap(AL_CONTEXT p, buf_length-imcp_header_length) == RETURN_SUCCESS) {
			ret	= ACCESS_POINT_PACKET_DECISION_SEND_UP_IMCP;
		} else {
			ret	= ACCESS_POINT_PACKET_DECISION_SEND_WM_DS;
		}
		break;
	case IMCP_SNIP_PACKET_TYPE_ACL_CONFIG_INFO:
		ret_type = imcp_snip_acl_info_get_packet(AL_CONTEXT p, buf_length-imcp_header_length, pi);
		if(ret_type >= 0)
			ret = mesh_imcp_process_packet_acl_info(AL_CONTEXT pi);
		break;
	case IMCP_SNIP_PACKET_TYPE_BUFFER_COMMAND:
		ret_type = imcp_snip_buffer_command_get_packet(AL_CONTEXT p,buf_length-imcp_header_length,pi);
		if(ret_type >= 0)
			ret	= mesh_imcp_process_packet_buffering_command(AL_CONTEXT pi);
		break;
	case IMCP_SNIP_PACKET_TYPE_BUFFER_COMMAND_ACK:
		ret_type = imcp_snip_buffer_command_ack_get_packet(AL_CONTEXT p,buf_length-imcp_header_length,pi);
		if(ret_type >= 0)
			ret	= mesh_imcp_process_packet_buffering_command_ack(AL_CONTEXT pi);
		break;
	case IMCP_SNIP_PACKET_TYPE_DOWNLINK_SATURATION_INFO:
		ret	= ACCESS_POINT_PACKET_DECISION_SEND_WM_DS;
		break;
	case IMCP_SNIP_PACKET_TYPE_REBOOT_REQUIRED:
		ret_type	= imcp_snip_reboot_required_get_packet(AL_CONTEXT p,buf_length-imcp_header_length,pi);
		if(ret_type >= 0)
			ret	= mesh_imcp_process_packet_reboot_required(AL_CONTEXT pi);
		break;
	case IMCP_SNIP_PACKET_TYPE_PRIVATE_CHANNEL_REQ_RES:															// IMCP 50
		if(imcp_snip_is_packet_for_ap(AL_CONTEXT p, buf_length-imcp_header_length) == RETURN_SUCCESS) {
			ret	= ACCESS_POINT_PACKET_DECISION_SEND_UP_IMCP;
		} else {
			ret	= ACCESS_POINT_PACKET_DECISION_SEND_WM_DS;
		}
		break;
	case IMCP_SNIP_PACKET_TYPE_PRIVATE_CHANNEL_INFO:														// IMCP 51
		if(imcp_snip_is_packet_for_ap(AL_CONTEXT p, buf_length-imcp_header_length) == RETURN_SUCCESS) {
			ret	= ACCESS_POINT_PACKET_DECISION_SEND_UP_IMCP;
		} else {
			ret	= ACCESS_POINT_PACKET_DECISION_SEND_WM_DS;
		}
		break;
	case IMCP_SNIP_PACKET_TYPE_EFFISTREAM_REQ_RES:															// IMCP 52
		if(imcp_snip_is_packet_for_ap(AL_CONTEXT p, buf_length-imcp_header_length) == RETURN_SUCCESS) {
			ret	= ACCESS_POINT_PACKET_DECISION_SEND_UP_IMCP;
		} else {
			ret	= ACCESS_POINT_PACKET_DECISION_SEND_WM_DS;
		}
		break;
	case IMCP_SNIP_PACKET_TYPE_EFFISTREAM_INFO:																// IMCP 53
		if(imcp_snip_is_packet_for_ap(AL_CONTEXT p, buf_length-imcp_header_length) == RETURN_SUCCESS) {
			ret	= ACCESS_POINT_PACKET_DECISION_SEND_UP_IMCP;
		} else {
			ret	= ACCESS_POINT_PACKET_DECISION_SEND_WM_DS;
		}
		break;
	case IMCP_SNIP_PACKET_TYPE_RADIO_DIAGNOSTIC_COMMAND:													// IMCP 54
		if(imcp_snip_is_packet_for_ap(AL_CONTEXT p, buf_length-imcp_header_length) == RETURN_SUCCESS) {
			ret_type = imcp_snip_radio_diag_command_get_packet(AL_CONTEXT p, buf_length-imcp_header_length, pi);
			if(ret_type >= 0)
				ret	= mesh_imcp_process_packet_radio_diag_command(AL_CONTEXT pi);
		} else {
			ret	= ACCESS_POINT_PACKET_DECISION_SEND_WM_DS;
		}
		break;
	case IMCP_SNIP_PACKET_TYPE_RADIO_DATA_REQ_RES:															// IMCP 56
		if(imcp_snip_is_packet_for_ap(AL_CONTEXT p, buf_length-imcp_header_length) == RETURN_SUCCESS) {
			ret	= ACCESS_POINT_PACKET_DECISION_SEND_UP_IMCP;
		} else {
			ret	= ACCESS_POINT_PACKET_DECISION_SEND_WM_DS;
		}
		break;	
	case IMCP_SNIP_PACKET_TYPE_RADIO_DATA_INFO:																// IMCP 57
		if(imcp_snip_is_packet_for_ap(AL_CONTEXT p, buf_length-imcp_header_length) == RETURN_SUCCESS) {
			ret	= ACCESS_POINT_PACKET_DECISION_SEND_UP_IMCP;
		} else {
			ret	= ACCESS_POINT_PACKET_DECISION_SEND_WM_DS;
		}
		break;
	case IMCP_SNIP_PACKET_TYPE_NODE_MOVE_NOTIFICATION:
		ret_type = imcp_snip_node_move_notification_get_packet(AL_CONTEXT p, buf_length-imcp_header_length, pi);
		if(ret_type >= 0)
			ret = mesh_imcp_process_packet_node_move_notification(AL_CONTEXT pi);
		break;
	case IMCP_SNIP_PACKET_TYPE_CHANNEL_CHANGE:
		ret_type	= imcp_snip_channel_change_get_packet(AL_CONTEXT p, buf_length-imcp_header_length, pi);
		if(ret_type >= 0)
			ret	= mesh_imcp_process_packet_channel_change(AL_CONTEXT pi, &packet->addr_2);
		break;
	case IMCP_SNIP_PACKET_TYPE_CLIENT_SIGNAL_INFO:
		ret	= ACCESS_POINT_PACKET_DECISION_SEND_WM_DS;
		break;
	case IMCP_SNIP_PACKET_TYPE_SIP_PRIVATE:
		ret_type	= imcp_snip_sip_private_get_packet(AL_CONTEXT p, buf_length-imcp_header_length, pi);
		if(ret_type >= 0)
			ret	= mesh_imcp_process_packet_sip_private(AL_CONTEXT pi);
		break;
	case IMCP_SNIP_PACKET_TYPE_SIP_CONFIG_INFO:
		if(imcp_snip_is_packet_for_ap(AL_CONTEXT p, buf_length-imcp_header_length) == RETURN_SUCCESS) {
			ret	= ACCESS_POINT_PACKET_DECISION_SEND_UP_IMCP;
		} else {
			ret	= ACCESS_POINT_PACKET_DECISION_SEND_WM_DS;
		}
		break;
	case IMCP_MAC_ADDR_INFO_REQUEST:
		if(imcp_snip_is_packet_for_ap(AL_CONTEXT p, buf_length-imcp_header_length) == RETURN_SUCCESS) {
			ret   = ACCESS_POINT_PACKET_DECISION_SEND_UP_IMCP;
		} else {
			ret   = ACCESS_POINT_PACKET_DECISION_SEND_WM_DS;
		}
      break;
	case IMCP_MGMT_GW_CONFIG_REQUEST:
		ret = ACCESS_POINT_PACKET_DECISION_SEND_UP_IMCP;
      break;
	default:
		ret	= ACCESS_POINT_PACKET_DECISION_SEND_WM_DS;
		break;
	}

	imcp_snip_release_packet(AL_CONTEXT pi);	
	imcp_snip_release_buffer(AL_CONTEXT output);
	
	return ret;
}

int	imcp_snip_get_vendor_info_buffer(AL_CONTEXT_PARAM_DECL imcp_packet_info_t* packet_in,unsigned char* vendor_info_out, int encryption)
{
	unsigned char	*p;
	unsigned int	cpu2be;
	int				length;

	p		= vendor_info_out;

	memcpy(p,	"IMCP",						4);			p += 4;
	*p			= IMCP_SNIP_VERSION_MAJOR;				p += 1;
	*p			= IMCP_SNIP_VERSION_MINOR;				p += 1;

	*p			= mesh_config->meshid_length;			p += 1;
	memcpy(p,	mesh_config->meshid,		mesh_config->meshid_length);	p += mesh_config->meshid_length;

	memcpy(p, packet_in->u.vendor_info.ds_mac.bytes, MAC_ADDR_LENGTH);				p +=MAC_ADDR_LENGTH; /*ds mac*/
	
	cpu2be	= al_cpu_to_be32(packet_in->u.vendor_info.ctc);	
	memcpy(p, 	&cpu2be,					4);			p += 4;				/*ctc*/

	*p			= packet_in->u.vendor_info.hpc;			p += 1;				/*hpc*/

	cpu2be	= al_cpu_to_be32(packet_in->u.vendor_info.crssi);	
	memcpy(p, 	&cpu2be,					4);			p += 4;				/*CRSSI*/

	*p			= packet_in->u.vendor_info.hbi;			p += 1;				/*hbi*/
	*p			= packet_in->u.vendor_info.ds_if_channel;	p += 1;			/*DS IF channel*/
	*p			= packet_in->u.vendor_info.ds_if_sub_type;	p += 1;			/*DS IF sub type*/

	memcpy(p, packet_in->u.vendor_info.root_bssid.bytes,MAC_ADDR_LENGTH); p += MAC_ADDR_LENGTH; /*root bssid*/
    *p          = packet_in->u.vendor_info.mode;         p += 1;             /*for LFRS*/
    *p          = packet_in->u.vendor_info.phy_sub_type; p += 1;             /*for LFRS*/


	length		= (p - vendor_info_out);
   if (encryption)
   {
	   length		= encrypt_packet_data(AL_CONTEXT vendor_info_out, length);
   }

	return length;

}

int imcp_snip_process_vendor_info(AL_CONTEXT_PARAM_DECL 
					unsigned char*	vendor_info_in,
					unsigned char	length_in,
					imcp_packet_info_t*	packet_out)
{
	unsigned char	output[AL_802_11_VENDOR_INFO_MAX_SIZE];
	int		ret;
	int		len;
	unsigned char	*p;
	unsigned char	buf[32];
	unsigned int	be2cpu;

	AL_ASSERT("MESH_AP	: imcp_get_known_ap_info", vendor_info_in != NULL);

	/*Decrypt Packet*/
#ifdef AL_USE_ENCRYPTION
	ret = al_decrypt(AL_CONTEXT vendor_info_in,length_in,
			 mesh_config->mesh_imcp_key,
			 mesh_config->mesh_imcp_key_length,output,
			 AL_ENCRYPTION_DECRYPTION_METHOD_AES_ECB);
#endif
	if(ret < 0) {
		return IMCP_SNIP_VENDOR_INFO_ERR_ENCR;
	}

	p = output;

	if(memcmp(p, "IMCP", 4)) {
		return IMCP_SNIP_VENDOR_INFO_ERR_IMCP;
	}

	p += 4;

	if(*p != IMCP_SNIP_VERSION_MAJOR)
		return IMCP_SNIP_VENDOR_INFO_ERR_VER;

	p += 2; /** Major & Minor version */

	len = *p;
	p++;
	/*MeshId Check*/
	memcpy(buf, p, len);
	buf[len] = 0;
	p += len;
	if(len != mesh_config->meshid_length)
		return IMCP_SNIP_VENDOR_INFO_ERR_IDL;
//RAMESH16MIG 
	if(strncasecmp(buf, mesh_config->meshid,len)) {
		return IMCP_SNIP_VENDOR_INFO_ERR_ID;
	}
	memset(packet_out,0,sizeof(imcp_packet_info_t));

	memcpy(packet_out->u.vendor_info.ds_mac.bytes,p,MAC_ADDR_LENGTH);	p += MAC_ADDR_LENGTH;
	packet_out->u.vendor_info.ds_mac.length	= MAC_ADDR_LENGTH;

	memcpy(&be2cpu,		p,		4);									p += 4;
	packet_out->u.vendor_info.ctc	= al_be32_to_cpu(be2cpu);

	packet_out->u.vendor_info.hpc 	=	*p;							p += 1;

	memcpy(&be2cpu,		p,		4);									p += 4;


	packet_out->u.vendor_info.crssi	= al_be32_to_cpu(be2cpu);

	packet_out->u.vendor_info.hbi 	=	*p;							p += 1;
	packet_out->u.vendor_info.ds_if_channel		=	*p;				p += 1;
	packet_out->u.vendor_info.ds_if_sub_type	=	*p;				p += 1;

	memcpy(packet_out->u.vendor_info.root_bssid.bytes,	p,		MAC_ADDR_LENGTH);	p += MAC_ADDR_LENGTH;
	
	packet_out->u.vendor_info.root_bssid.length			=		MAC_ADDR_LENGTH;
	packet_out->u.vendor_info.mode				= *p;			p+=1;

	packet_out->u.vendor_info.phy_sub_type		= *p;			p+=1; // downlink subtype (either a/an/anac/ac/bgn/n/so....on)

	return 0;
}

