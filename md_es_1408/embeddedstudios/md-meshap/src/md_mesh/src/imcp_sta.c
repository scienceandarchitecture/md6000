
/********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : imcp_sta.c
 * Comments : STA Assoc/Disassoc/ AP Sub Tree IMCP packet impl file.
 * Created  : 8/16/2004
 * Author   : Anand Bhange
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * -----------------------------------------------------------------------------
 * | No  |Date      |  Comment                                        | Author |
 * -----------------------------------------------------------------------------
 * |  2  |02/01/2006| imcp_snip_sta_info_request_get_packet added     | Abhijit|
 * -----------------------------------------------------------------------------
 * |  1  |11/25/2004| le2cpu funcs called for packets                 | Anand  |
 * -----------------------------------------------------------------------------
 * |  0  |8/16/2004 | Created.                                        | Anand  |
 * -----------------------------------------------------------------------------
 ********************************************************************************/

static int imcp_snip_sta_assoc_notification_get_buffer (AL_CONTEXT_PARAM_DECL imcp_packet_info_t* pi, unsigned char* buffer, unsigned int buf_length)
{
#define _REQ_LENGTH		22	

	unsigned char*	p;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);

	unsigned short  node_id;
	unsigned short  node_len;

	AL_ASSERT("MESH_AP	: imcp_snip_sta_assoc_notification_get_buffer", buffer != NULL);
	AL_ASSERT("MESH_AP	: imcp_snip_sta_assoc_notification_get_buffer", pi != NULL);

	/**
	 * Buffer length must be atleast req_length 
	 */
	AL_PRINT_LOG_FLOW_0( "MESH_AP	: imcp_snip_sta_assoc_notification_get_buffer()");

	if(buf_length < _REQ_LENGTH)
		return IMCP_SNIP_ERROR_INADEQUATE_BUFFER_LENGTH;

	p	= buffer;


	node_id = IMCP_STA_ASSOC_NOTIFICATION_INFO_ID;
	node_len = (_REQ_LENGTH) - (FIXED_ID_LEN);
	IMCP_MSG_SEND_ID_AND_LEN(p,node_id,node_len)

	/**
	 * Copy AP DS MAC, AP WM MAC, STA MAC
	 */
	memcpy(p,	pi->u.sta_assoc_notification.ap_ds_mac.bytes,		MAC_ADDR_LENGTH);	p += MAC_ADDR_LENGTH;
	memcpy(p,	pi->u.sta_assoc_notification.ap_wm_mac.bytes,		MAC_ADDR_LENGTH);	p += MAC_ADDR_LENGTH;
	memcpy(p,	pi->u.sta_assoc_notification.sta_mac.bytes,			MAC_ADDR_LENGTH);	p += MAC_ADDR_LENGTH;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
	return _REQ_LENGTH;
#undef _REQ_LENGTH
}


static int imcp_snip_sta_disassoc_notification_get_buffer (AL_CONTEXT_PARAM_DECL imcp_packet_info_t* pi, unsigned char* buffer, unsigned int buf_length)
{
#define _REQ_LENGTH		18

	unsigned char*	p;
	unsigned short  cpu2le;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
	unsigned short  node_id;
	unsigned short  node_len;

	AL_PRINT_LOG_FLOW_0( "MESH_AP	: imcp_snip_sta_disassoc_notification_get_buffer()");

	AL_ASSERT("MESH_AP	: imcp_snip_sta_disassoc_notification_get_buffer", buffer != NULL);
	AL_ASSERT("MESH_AP	: imcp_snip_sta_disassoc_notification_get_buffer", pi != NULL);

	/**
	 * Buffer length must be atleast req_length 
	 */

	if(buf_length < _REQ_LENGTH)
    {
		 al_print_log(AL_CONTEXT AL_LOG_TYPE_DEBUG," ERROR : (%s) function : LINE (%d) :Receiving length (%d) is not Correct  \n",__func__,__LINE__,buf_length);
		return IMCP_SNIP_ERROR_INADEQUATE_BUFFER_LENGTH;
    }
	p	= buffer;


        node_id = IMCP_STA_DISASSOC_NOTIFICATION_INFO_ID;
        node_len = (_REQ_LENGTH - FIXED_ID_LEN);
        IMCP_MSG_SEND_ID_AND_LEN(p,node_id,node_len)
	/**
	 * Copy AP DS MAC, STA MAC, Reason
	 */
	memcpy(p,	pi->u.sta_disassoc_notification.ap_ds_mac.bytes,	MAC_ADDR_LENGTH);	p += MAC_ADDR_LENGTH;
	memcpy(p,	pi->u.sta_disassoc_notification.sta_mac.bytes,		MAC_ADDR_LENGTH);	p += MAC_ADDR_LENGTH;
	cpu2le		= al_impl_cpu_to_le16(pi->u.sta_disassoc_notification.reason);
	memcpy(p,	&cpu2le,											2);					p += 2;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
	return _REQ_LENGTH;
#undef _REQ_LENGTH
}


static int imcp_snip_sta_assoc_notification_get_packet (AL_CONTEXT_PARAM_DECL unsigned char* buffer, unsigned int buf_length, imcp_packet_info_t* pi)
{
#define _REQ_LENGTH		22

	unsigned char*	p;

	unsigned node_id;
	unsigned node_len;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
	AL_PRINT_LOG_FLOW_0( "MESH_AP	: imcp_snip_sta_assoc_notification_get_packet()");
	AL_ASSERT("MESH_AP	: imcp_snip_sta_assoc_notification_get_packet", pi != NULL);
	AL_ASSERT("MESH_AP	: imcp_snip_sta_assoc_notification_get_packet ", buffer != NULL);
	/**
	 * Buffer length must be atleast req_length 
	 */

	if(buf_length < _REQ_LENGTH)
	{
                printk(KERN_EMERG" Func : %s Line : %d Expected_len : _REQ_LENGTH Received_len : %d\n",__func__,__LINE__,buf_length);
		return IMCP_SNIP_ERROR_INADEQUATE_BUFFER_LENGTH;
	}

	p	= buffer;

	IMCP_MSG_RECEIVE_ID_AND_LEN(p,node_id,node_len)

	/**
	 * Copy AP DS MAC, AP WM MAC, STA MAC
	 */
	memcpy(pi->u.sta_assoc_notification.ap_ds_mac.bytes,	p,	MAC_ADDR_LENGTH);	p += MAC_ADDR_LENGTH;
	pi->u.sta_assoc_notification.ap_ds_mac.length = MAC_ADDR_LENGTH;
	memcpy(pi->u.sta_assoc_notification.ap_wm_mac.bytes,	p,	MAC_ADDR_LENGTH);	p += MAC_ADDR_LENGTH;
	pi->u.sta_assoc_notification.ap_wm_mac.length = MAC_ADDR_LENGTH;
	memcpy(pi->u.sta_assoc_notification.sta_mac.bytes,	p,	MAC_ADDR_LENGTH);	p += MAC_ADDR_LENGTH;
	pi->u.sta_assoc_notification.sta_mac.length = MAC_ADDR_LENGTH;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
	return _REQ_LENGTH;
#undef _REQ_LENGTH
}


static int imcp_snip_sta_disassoc_notification_get_packet (AL_CONTEXT_PARAM_DECL unsigned char* buffer, unsigned int buf_length, imcp_packet_info_t* pi)
{
#define _REQ_LENGTH		18	

	unsigned char*	p;
	unsigned int    le2cpu;

	unsigned short  node_id;
	unsigned short  node_len;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
	AL_PRINT_LOG_FLOW_0( "MESH_AP	: imcp_snip_sta_disassoc_notification_get_packet()");
	AL_ASSERT("MESH_AP	: imcp_snip_sta_disassoc_notification_get_packet", pi != NULL);
	AL_ASSERT("MESH_AP	: imcp_snip_sta_disassoc_notification_get_packet", buffer != NULL);
	/**
	 * Buffer length must be atleast req_length 
	 */

	if(buf_length < _REQ_LENGTH)
	{
                printk(KERN_EMERG" Func : %s Line : %d Expected_len : _REQ_LENGTH Received_len : %d\n",__func__,__LINE__,buf_length);
		return IMCP_SNIP_ERROR_INADEQUATE_BUFFER_LENGTH;
	}
	p	= buffer;

	IMCP_MSG_RECEIVE_ID_AND_LEN(p,node_id,node_len)

	/**
	 * Copy AP DS MAC, STA MAC, Reason
	 */
	memcpy(pi->u.sta_disassoc_notification.ap_ds_mac.bytes,	p,	MAC_ADDR_LENGTH);		p += MAC_ADDR_LENGTH;
	pi->u.sta_disassoc_notification.ap_ds_mac.length = MAC_ADDR_LENGTH;
	memcpy(pi->u.sta_disassoc_notification.sta_mac.bytes,		p,	MAC_ADDR_LENGTH);	p += MAC_ADDR_LENGTH;
	pi->u.sta_disassoc_notification.sta_mac.length = MAC_ADDR_LENGTH;
	memcpy(&le2cpu,												p,	2);					p += 2;
	pi->u.sta_disassoc_notification.reason		= al_impl_le16_to_cpu(le2cpu);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
	return _REQ_LENGTH;
#undef _REQ_LENGTH
}


static int imcp_snip_ap_sub_tree_info_get_buffer(AL_CONTEXT_PARAM_DECL imcp_packet_info_t* pi, unsigned char* buffer, unsigned int buf_length)
{

#define REQ_LENGTH      11      

	unsigned char*	p;
	int		i;
	unsigned int	req_length;
	unsigned short  node_id;
	unsigned short  node_len;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
	AL_PRINT_LOG_FLOW_0( "MESH_AP	: imcp_snip_ap_sub_tree_info_get_buffer()");

	AL_ASSERT("MESH_AP	: imcp_snip_ap_sub_tree_info_get_buffer", buffer != NULL);
	AL_ASSERT("MESH_AP	: imcp_snip_ap_sub_tree_info_get_buffer", pi != NULL);

	req_length = (7 + FIXED_ID_LEN + (pi->u.sub_tree_info.sta_count * 12));	

	/**
	 * Buffer length must be atleast req_length 
	 */

	if(buf_length < req_length)
    {
		al_print_log(AL_CONTEXT AL_LOG_TYPE_DEBUG," ERROR : (%s) function : LINE (%d) :Receiving length (%d) is not Correct  \n",__func__,__LINE__,buf_length);
		return IMCP_SNIP_ERROR_INADEQUATE_BUFFER_LENGTH;
    }
	p	= buffer;


	node_id = IMCP_AP_SUB_TREE_INFO_ID;
	node_len = (REQ_LENGTH  - FIXED_ID_LEN) + (pi->u.sub_tree_info.sta_count * 12);
	IMCP_MSG_SEND_ID_AND_LEN(p,node_id,node_len)

	/**
	 * Copy Sender DS MAC, SQNR, CTC, HPC, ROOT bssid, CRSSI, HI, CH, PARENT Bssid
	 * Known AP count, 
	 */
	memcpy(p,	pi->u.sub_tree_info.ds_mac.bytes,				MAC_ADDR_LENGTH);	p += MAC_ADDR_LENGTH;
	memcpy(p,	&pi->u.sub_tree_info.sta_count,					1);					p += 1;

	for(i = 0; i < pi->u.sub_tree_info.sta_count; i++) {
		memcpy(p,	pi->u.sub_tree_info.sta_entries[(i * 2) + 0].bytes,	MAC_ADDR_LENGTH);		p += MAC_ADDR_LENGTH;
		memcpy(p,	pi->u.sub_tree_info.sta_entries[(i * 2) + 1].bytes,	MAC_ADDR_LENGTH);		p += MAC_ADDR_LENGTH;
	}

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
	return req_length;
}

static int imcp_snip_ap_sub_tree_info_get_packet(AL_CONTEXT_PARAM_DECL unsigned char* buffer, unsigned int buf_length, imcp_packet_info_t* pi)
{
	unsigned char*	p;
	int		i;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
	unsigned short  node_id;
	unsigned short  node_len;

#define _REQ_LENGTH		11	

	AL_PRINT_LOG_FLOW_0( "MESH_AP	: imcp_snip_ap_sub_tree_info_get_packet()");
	AL_ASSERT("MESH_AP	: imcp_snip_ap_sub_tree_info_get_packet", pi != NULL);
	AL_ASSERT("MESH_AP	: imcp_snip_ap_sub_tree_info_get_packet", buffer != NULL);
	/**
	 * Buffer length must be atleast req_length 
	 */

	if(buf_length < _REQ_LENGTH)
	{
		printk(KERN_EMERG"Func : %s Line : %d Expected_len : _REQ_LENGTH Received_len : %d\n",__func__,__LINE__,buf_length);
		return IMCP_SNIP_ERROR_INADEQUATE_BUFFER_LENGTH;
	}
	p = buffer;

	IMCP_MSG_RECEIVE_ID_AND_LEN(p,node_id,node_len)
	/**
	 * Copy Sender DS MAC, SQNR, CTC, HPC, ROOT bssid, CRSSI, HI, CH, PARENT Bssid
	 * Known AP count, 
	 */
	memcpy(pi->u.sub_tree_info.ds_mac.bytes,		p,		MAC_ADDR_LENGTH);	p += MAC_ADDR_LENGTH;
	pi->u.sub_tree_info.ds_mac.length = MAC_ADDR_LENGTH;
	memcpy(&pi->u.sub_tree_info.sta_count,			p,		1);					p += 1;

	pi->u.sub_tree_info.sta_entries = (al_net_addr_t*)al_heap_alloc(AL_CONTEXT pi->u.sub_tree_info.sta_count * 2 * sizeof(al_net_addr_t) AL_HEAP_DEBUG_PARAM);
	for(i = 0; i < pi->u.sub_tree_info.sta_count; i++) {
		memcpy(pi->u.sub_tree_info.sta_entries[(i * 2) + 0].bytes,p,	MAC_ADDR_LENGTH);	p += MAC_ADDR_LENGTH;
		pi->u.sub_tree_info.sta_entries[(i * 2) + 0].length = MAC_ADDR_LENGTH;
		memcpy(pi->u.sub_tree_info.sta_entries[(i * 2) + 1].bytes,p,	MAC_ADDR_LENGTH);	p += MAC_ADDR_LENGTH;
		pi->u.sub_tree_info.sta_entries[(i * 2) + 1].length = MAC_ADDR_LENGTH;
	}

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
	return p-buffer;
#undef _REQ_LENGTH
}

static int imcp_snip_sta_info_request_get_packet(AL_CONTEXT_PARAM_DECL unsigned char* buffer, unsigned int buf_length, imcp_packet_info_t* pi)
{
#define _REQ_LENGTH             16	
	unsigned char*	p;
	unsigned short temp ;		
	unsigned short  le2cpu16;	
	unsigned short  node_id;
    unsigned short  node_len;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);

	AL_PRINT_LOG_FLOW_0( "MESH_AP	: imcp_snip_sta_info_request_get_packet()");
	AL_ASSERT("MESH_AP	: imcp_snip_sta_info_request_get_packet", pi != NULL);
	AL_ASSERT("MESH_AP	: imcp_snip_sta_info_request_get_packet", buffer != NULL);

	/**
	 * Buffer length must be atleast req_length 
	 */

	if(buf_length < _REQ_LENGTH) {

		 al_print_log(AL_CONTEXT AL_LOG_TYPE_DEBUG," ERROR : (%s) function : LINE (%d) :Receiving length (%d) is not Correct  \n",__func__,__LINE__,buf_length); 
		printk(KERN_EMERG"MESH_AP	: IMCP_SNIP_PACKET_TYPE_STA_INFO_REQUEST IMCP_SNIP_ERROR_INADEQUATE_BUFFER_LENGTH");
		return IMCP_SNIP_ERROR_INADEQUATE_BUFFER_LENGTH;
	}

	p	= buffer;

        IMCP_MSG_RECEIVE_ID_AND_LEN(p,node_id,node_len);


	/**
	 * Copy AP DS MAC, STA MAC
	 */
	memcpy(pi->u.sta_info_request.ds_mac.bytes,	p,	MAC_ADDR_LENGTH);			p += MAC_ADDR_LENGTH;
	pi->u.sta_info_request.ds_mac.length = MAC_ADDR_LENGTH;
	memcpy(pi->u.sta_info_request.sta_mac.bytes,	p,	MAC_ADDR_LENGTH);		p += MAC_ADDR_LENGTH;
	pi->u.sta_info_request.sta_mac.length = MAC_ADDR_LENGTH;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
	return _REQ_LENGTH;
#undef _REQ_LENGTH

}

static int imcp_snip_sta_info_get_buffer(AL_CONTEXT_PARAM_DECL imcp_packet_info_t* pi, unsigned char* buffer, unsigned int buf_length)
{
	unsigned char*	p;
	unsigned int	req_length;
	unsigned int	cpu2le32;
	unsigned short  temp;		
	unsigned short  node_id;	
        unsigned short  node_len;	
 
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
	AL_PRINT_LOG_FLOW_0( "MESH_AP	: imcp_snip_if_sta_info_get_buffer()");

	AL_ASSERT("MESH_AP	: imcp_snip_if_sta_info_get_buffer", buffer != NULL);
	AL_ASSERT("MESH_AP	: imcp_snip_if_sta_info_get_buffer", pi != NULL);

	req_length = (42 + pi->u.sta_info.parent_essid_length);
	/**
	 * Buffer length must be atleast req_length 
	 */

	if(buf_length < req_length)
    {
		al_print_log(AL_CONTEXT AL_LOG_TYPE_DEBUG," ERROR : (%s) function : LINE (%d) :Receiving length (%d) is not Correct  \n",__func__,__LINE__,buf_length);
		return IMCP_SNIP_ERROR_INADEQUATE_BUFFER_LENGTH;
    }
	p	= buffer;

        node_id         = IMCP_STA_INFO_RESPONSE_ID;
        node_len        = (STA_INFO_RESPONSE_FIXED_LEN+pi->u.sta_info.parent_essid_length);
        IMCP_MSG_SEND_ID_AND_LEN(p,node_id,node_len);


	/**
	 * Copy Sender DS MAC, STA MAC, PARENT WM MAC, Parent Essid,auth_state,
	 * b_client,is_imcp,listen_interval,capability,key_index,
	 * bytes_received.,bytes-sent,active_time
	 */

	memcpy(p,pi->u.sta_info.ds_mac.bytes,MAC_ADDR_LENGTH);						
	p += MAC_ADDR_LENGTH;
	memcpy(p,pi->u.sta_info.sta_mac.bytes,MAC_ADDR_LENGTH);						
	p += MAC_ADDR_LENGTH;
	memcpy(p,pi->u.sta_info.parent_mac.bytes,MAC_ADDR_LENGTH);					
	p += MAC_ADDR_LENGTH;
	*p = pi->u.sta_info.parent_essid_length;														
	p += 1;
	memcpy(p,pi->u.sta_info.parent_essid,pi->u.sta_info.parent_essid_length);	
	p += pi->u.sta_info.parent_essid_length;
	*p = pi->u.sta_info.auth_state;																	     p += 1;
	*p = pi->u.sta_info.b_client;																  	     p += 1;
	*p = pi->u.sta_info.is_imcp;																  	     p += 1;
	memcpy(p,&(pi->u.sta_info.listen_interval),2);							
	p += 2;
	*p = pi->u.sta_info.capability;																	     p += 1;
	*p = pi->u.sta_info.key_index;																	     p += 1;

	cpu2le32	= al_impl_cpu_to_le32(pi->u.sta_info.bytes_received);
	memcpy(p,	&cpu2le32,												4);							p += 4;

	cpu2le32	= al_impl_cpu_to_le32(pi->u.sta_info.bytes_sent);
	memcpy(p,	&cpu2le32,												4);							p += 4;

	cpu2le32	= al_impl_cpu_to_le32(pi->u.sta_info.active_time);
	memcpy(p,	&cpu2le32,												4);							p += 4;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
	return req_length;
}

static int imcp_snip_node_move_notification_get_buffer(AL_CONTEXT_PARAM_DECL imcp_packet_info_t* pi, unsigned char* buffer, unsigned int buf_length)
{
#define _REQ_LENGTH             12+ IMCP_ID_AND_LEN_SIZE //Size increased for ID and Length

	unsigned char*	p;
 	unsigned short  temp;		
	unsigned short  node_id;
        unsigned short  node_len;	
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);

	/**
	 * Buffer length must be atleast req_length 
	 */

	if(buf_length < _REQ_LENGTH)
    {
		al_print_log(AL_CONTEXT AL_LOG_TYPE_DEBUG," ERROR : (%s) function : LINE (%d) :Receiving length (%d) is not Correct  \n",__func__,__LINE__,buf_length);
		return IMCP_SNIP_ERROR_INADEQUATE_BUFFER_LENGTH;
    }
	p	= buffer;

           
        node_id         = IMCP_NODE_MOVE_NOTIFICATION_INFO_ID;
        node_len        = (NODE_MOVE_NOTIFICATION_FIXED_LEN);
        IMCP_MSG_SEND_ID_AND_LEN(p,node_id,node_len);


	/**
	 * Copy AP DS MAC, NODE DS MAC
	 */

	memcpy(p, pi->u.node_move_notification.ds_mac.bytes, MAC_ADDR_LENGTH);	p += MAC_ADDR_LENGTH;
	memcpy(p, pi->u.node_move_notification.node_ds_mac.bytes, MAC_ADDR_LENGTH);	p += MAC_ADDR_LENGTH;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
	return _REQ_LENGTH;

#undef _REQ_LENGTH
}

static int imcp_snip_node_move_notification_get_packet(AL_CONTEXT_PARAM_DECL unsigned char* buffer, unsigned int buf_length, imcp_packet_info_t* pi)
{
#define _REQ_LENGTH             16	

	unsigned char*	p;
	unsigned short  temp;		
   	unsigned short  le2cpu16;       
        unsigned short  node_id;
        unsigned short  node_len;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
	/**
	 * Buffer length must be atleast req_length 
	 */

	if(buf_length < _REQ_LENGTH){
		al_print_log(AL_CONTEXT AL_LOG_TYPE_DEBUG," ERROR : (%s) function : LINE (%d) :Receiving length (%d) is not Correct  \n",__func__,__LINE__,buf_length);
		return IMCP_SNIP_ERROR_INADEQUATE_BUFFER_LENGTH;
	}

	p	= buffer;

        IMCP_MSG_RECEIVE_ID_AND_LEN(p,node_id,node_len);

	/**
	 * Copy AP DS MAC, NODE DS MAC
	 */

	memcpy(pi->u.node_move_notification.ds_mac.bytes, p, MAC_ADDR_LENGTH);	p += MAC_ADDR_LENGTH;
	memcpy(pi->u.node_move_notification.node_ds_mac.bytes, p, MAC_ADDR_LENGTH);	p += MAC_ADDR_LENGTH;

	pi->u.node_move_notification.ds_mac.length		= MAC_ADDR_LENGTH;
	pi->u.node_move_notification.node_ds_mac.length	= MAC_ADDR_LENGTH;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
	return _REQ_LENGTH;

#undef _REQ_LENGTH
}

