/********************************************************************************
* MeshDynamics
* --------------
* File     : mesh_ng_core.c
* Comments : Mesh New Generation Implementation Core
* Created  : 4/10/2006
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  7  |02/17/2009| Changes for Uplink Scan Functionality           |Sriram  |
* -----------------------------------------------------------------------------
* |  6  |8/22/2008 | Changes for FIPS and SIP                        | Abhijit|
* -----------------------------------------------------------------------------
* |  5  |6/13/2007 | mesh_ng_core_join_parent Added                  | Sriram |
* -----------------------------------------------------------------------------
* |  4  |6/13/2007 | reset_rate_ctrl called for mobile systems       | Sriram |
* -----------------------------------------------------------------------------
* |  3  |2/23/2007 | Changes for EFFISTREAM                          | Sriram |
* -----------------------------------------------------------------------------
* |  2  |01/01/2007| IF-Specific TXRate and TXPower changes          |Prachiti|
* -----------------------------------------------------------------------------
* |  1  |11/8/2006 | Changes for compression support                 | Sriram |
* -----------------------------------------------------------------------------
* |  0  |4/10/2006 | Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

#include <linux/string.h>
#include <linux/module.h>
#include "al.h"
#include "al_print_log.h"
#include "al_conf.h"
#include "access_point.h"
#include "al_impl_context.h"
#include "sip_conf.h"
#include "mesh_defs.h"
#include "mesh_table.h"
#include "mesh.h"
#include "mesh_globals.h"
#include "mesh_errors.h"
#include "mesh_on_start.h"
#include "mesh_intervals.h"
#include "imcp_snip.h"
#include "mesh_ng.h"
#include "mesh_ng_core.h"

#ifdef AL_USE_ENCRYPTION
#include "al_codec.h"
#endif

#define _MESH_NG_CORE_DESTROY_TIMEOUT    300
extern access_point_globals_t globals;
AL_DECLARE_GLOBAL(al_u64_t last_destroy = 0);

int mesh_on_start_find_ds(void);
int mesh_on_start_initialize_ds_interface1(void);

extern int eth1_ping_status;

static int _set_ds_encryption_key(AL_CONTEXT_PARAM_DECL al_802_11_operations_t *ex_op, al_802_11_md_ie_t *md_ie)
{
   unsigned char group_key[AL_802_11_MD_IE_TOTAL_GROUP_KEY_LENGTH];
   unsigned char pairwise_key[AL_802_11_MD_IE_TOTAL_PAIRWISE_KEY_LENGTH];
   int           enc_dec_method;
   int           ret, i;
   int           group_key_length;
   int           pairwise_key_length;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
   ret = 0;
   if ((md_ie->type == AL_802_11_MD_IE_TYPE_ASSOC_RESPONSE) || (md_ie->type == AL_802_11_MD_IE_TYPE_FIPS_ASSOC_RESPONSE))
   {
#ifdef AL_USE_ENCRYPTION
      enc_dec_method = (md_ie->type == AL_802_11_MD_IE_TYPE_FIPS_ASSOC_RESPONSE) ?
                       AL_ENCRYPTION_DECRYPTION_METHOD_AES_KEY_WRAP : AL_ENCRYPTION_DECRYPTION_METHOD_AES_ECB;
      group_key_length = (md_ie->type == AL_802_11_MD_IE_TYPE_FIPS_ASSOC_RESPONSE) ?
                         AL_802_11_MD_IE_FIPS_TOTAL_GROUP_KEY_LENGTH : AL_802_11_MD_IE_TOTAL_GROUP_KEY_LENGTH;
      pairwise_key_length = (md_ie->type == AL_802_11_MD_IE_TYPE_FIPS_ASSOC_RESPONSE) ?
                            AL_802_11_MD_IE_FIPS_TOTAL_PAIRWISE_KEY_LENGTH : AL_802_11_MD_IE_TOTAL_PAIRWISE_KEY_LENGTH;
#else
      md_ie->data.assoc_response.group_cipher_type = AL_802_11_SECURITY_INFO_RSN_CIPHER_NONE;
#endif
      if (md_ie->data.assoc_response.group_cipher_type != AL_802_11_SECURITY_INFO_RSN_CIPHER_NONE)
      {
#ifdef AL_USE_ENCRYPTION
         ret = al_decrypt(AL_CONTEXT md_ie->data.assoc_response.group_key,
                          group_key_length,
                          mesh_config->mesh_imcp_key,
                          mesh_config->mesh_imcp_key_length,
                          group_key,
                          enc_dec_method);

         if (ret < 0)
         {
            al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MESH_NG	: Incorrect group key, Error code = %d", ret);
            return ret;
         }
#else
         memcpy(group_key,
                md_ie->data.assoc_response.group_key,
                AL_802_11_MD_IE_MAX_GROUP_KEY_LENGTH);
#endif
      }

#ifdef AL_USE_ENCRYPTION
      ret = al_decrypt(AL_CONTEXT md_ie->data.assoc_response.pairwise_key,
                       pairwise_key_length,
                       mesh_config->mesh_imcp_key,
                       mesh_config->mesh_imcp_key_length,
                       pairwise_key,
                       enc_dec_method);

      if (ret < 0)
      {
         al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MESH_NG	: Incorrect pairwise key, Error code = %d", ret);
         return ret;
      }
#else
      memcpy(pairwise_key,
             md_ie->data.assoc_response.pairwise_key,
             AL_802_11_MD_IE_MAX_PAIRWISE_KEY_LENGTH);
#endif

      ex_op->set_ds_security_key(mesh_hardware_info->ds_net_if,
                                 group_key,
                                 md_ie->data.assoc_response.group_cipher_type,
                                 md_ie->data.assoc_response.group_key_index,
                                 pairwise_key,
                                 md_ie->data.assoc_response.md_ie_capability);


      access_point_set_ds_group_cipher_type(AL_CONTEXT md_ie->data.assoc_response.group_cipher_type);
   }

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
   return 0;
}


static int _join_parent_helper(AL_CONTEXT_PARAM_DECL parent_t *parent)
{
   al_802_11_operations_t    *ex_op, *ex_op1;
   al_802_11_md_ie_t         md_ie_in;
   al_802_11_md_ie_t         md_ie_out;
   al_802_11_tx_power_info_t power_info;
   int ret, ret1;
   int prev_ds_eth = 0;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
   access_point_config_t *config;

   config = &globals.config;
   memset(&md_ie_in, 0, sizeof(al_802_11_md_ie_t));
   memset(&md_ie_out, 0, sizeof(al_802_11_md_ie_t));

//	core_net_if = (meshap_core_net_if_t*)mesh_hardware_info->ds_net_if;

   ex_op = (al_802_11_operations_t *)mesh_hardware_info->ds_net_if->get_extended_operations(mesh_hardware_info->ds_net_if);

   if ((!strcmp(mesh_hardware_info->ds_net_if->name, "eth1")) ||
       (!strcmp(mesh_hardware_info->ds_net_if->name, "eth0")))
   {
      prev_ds_eth = 1;
      mesh_on_start_find_ds();

      config->ds_net_if_info.net_if      = mesh_hardware_info->ds_net_if;
      config->ds_net_if_info.ack_timeout = mesh_hardware_info->ds_if_ack_timeout;
      config->ds_net_if_info.txpower     = mesh_hardware_info->ds_if_tx_power;
      config->ds_net_if_info.txrate      = mesh_hardware_info->ds_if_tx_rate;
      strcpy(config->ds_net_if_info.essid, mesh_hardware_info->ds_conf->essid);
      AL_ATOMIC_SET(_DS_NET_IF->buffering_state, AL_NET_IF_BUFFERING_STATE_NONE);
      ex_op1 = (al_802_11_operations_t *)mesh_hardware_info->ds_net_if->get_extended_operations(mesh_hardware_info->ds_net_if);
      //mesh_on_start_initialize_ds_interface1();
   }

   md_ie_in.version = AL_802_11_MD_IE_VERSION;

#ifdef FIPS_1402_COMPLIANT
   md_ie_in.type = (_FIPS_ENABLED) ? AL_802_11_MD_IE_TYPE_FIPS_ASSOC_REQUEST : AL_802_11_MD_IE_TYPE_ASSOC_REQUEST;
#else
   md_ie_in.type = AL_802_11_MD_IE_TYPE_ASSOC_REQUEST;
#endif

   md_ie_in.data.assoc_request.assoc_type        = AL_802_11_MD_IE_ASSOCIATION_TYPE_FINAL;
   md_ie_in.data.assoc_request.md_ie_capability |= AL_802_11_MD_IE_COMPRESSION_SUPPORT;
   md_ie_in.data.assoc_request.md_ie_capability |= AL_802_11_MD_IE_EFFISTREAM_SUPPORT;


#if 1
   if (prev_ds_eth == 0)
   {
      //al_thread_sleep(1);
      ex_op->send_disassoc_from_station(mesh_hardware_info->ds_net_if, 0);
   }
#endif

   ex_op = (al_802_11_operations_t *)mesh_hardware_info->ds_net_if->get_extended_operations(mesh_hardware_info->ds_net_if);

   if (ex_op != NULL)
   {
      ret = ex_op->associate(mesh_hardware_info->ds_net_if,
                             &parent->bssid,
                             parent->channel,
                             parent->essid,
                             strlen(parent->essid),
                             ASSOC_TIME_OUT,
                             &md_ie_in,
                             &md_ie_out);
   }

   if (ret != 0)
   {
       al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"assoc is not happened :: func : %s LINE : %d\n", __func__,__LINE__);
      return ret;
   }

   parent->last_hb_time = al_get_tick_count(AL_CONTEXT_SINGLE);
   mesh_hardware_info->ds_if_channel = parent->channel;

   /* Use IF-specific tx_power and tx_rate instead of global values from meshap.conf */

   power_info.flags = AL_802_11_TXPOW_PERC;
   power_info.power = mesh_hardware_info->ds_if_tx_power;
   ex_op->set_tx_power(mesh_hardware_info->ds_net_if, &power_info);
   ex_op->set_bit_rate(mesh_hardware_info->ds_net_if, mesh_hardware_info->ds_if_tx_rate);
   ex_op->set_ack_timeout(mesh_hardware_info->ds_net_if, mesh_hardware_info->ds_if_ack_timeout);
   ex_op->enable_ds_verification_opertions(mesh_hardware_info->ds_net_if, 1);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MESH_NG	: Got best Parent, Associated AP =("AL_NET_ADDR_STR")", AL_NET_ADDR_TO_STR(&parent->bssid));

   current_parent = parent;

   ret = _set_ds_encryption_key(AL_CONTEXT ex_op, &md_ie_out);
   if (ret != 0)
   {
       al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"DS encryption is not set :: func : %s LINE : %d\n", __func__,__LINE__);
      return ret;
   }

   /**
    * For mobile systems reset the rate control state for the new parent
    */

   if (_IS_MOBILE || _IS_UPLINK_SCAN_ENABLED)
   {
      ex_op->reset_rate_ctrl(mesh_hardware_info->ds_net_if, &current_parent->bssid);
   }

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
   return 0;
}


static int _join_parents_enum_func(AL_CONTEXT_PARAM_DECL void *enum_param, parent_t *parent)
{
   int i;
   int ret;
   int *joined;

   joined = (int *)enum_param;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MESH_NG	: Trying to join "AL_NET_ADDR_STR"...", AL_NET_ADDR_TO_STR(&parent->bssid));
   /* Try for three times if not then try next one */

   for (i = 0; i < 3; i++)
   {
      ret = _join_parent_helper(AL_CONTEXT parent);

      if (ret == 0)
      {
         *joined = 1;
         return -1;
      }
      ++parent->disconnect_count;
   }

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
   return 0;
}


int mesh_ng_core_join_best_parent(AL_CONTEXT_PARAM_DECL_SINGLE)
{
   int joined;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
   joined = 0;
   mesh_ng_enumerate_parents(AL_CONTEXT MESH_NG_ENUMERATE_PARENTS_TYPE_RELEVANT,
                             (void *)&joined,
                             _join_parents_enum_func, 0);


   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
   return(joined == 0);
}


static int _purge_parents_enum_func(AL_CONTEXT_PARAM_DECL void *enum_param, parent_t *parent)
{
   int      *current_parent_purged;
   al_u64_t tick;
   al_u64_t difference;
   al_u64_t diff_thresh;
   al_u64_t timeout;

//    al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
   if (parent->flags & MESH_PARENT_FLAG_DISABLED)
   {
       al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"return due to D flag :: func : %s LINE : %d\n", __func__,__LINE__);
      return 0;
   }

   current_parent_purged = (int *)enum_param;

   /**
    * For a mobile scanning radio system, the critera for purging
    * is missing of beacons
    */

   tick        = al_get_tick_count(AL_CONTEXT_SINGLE);
   difference  = (al_s64_t)(tick - parent->last_hb_time);
   diff_thresh = parent->hbi * mesh_config->hbeat_miss_count * HZ;
   timeout     = parent->last_hb_time + diff_thresh;
   if (al_timer_before_eq(tick, timeout))
   {
      return 0;
   }

   if (parent == current_parent)
   {
      timeout = mesh_hardware_info->ds_net_if->last_rx_time + diff_thresh;
      if (al_timer_before_eq(tick, timeout))
      {
         return 0;
      }
   }

   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,
                "MESH_NG	: Disabling Known AP from list (Purge) AP="AL_NET_ADDR_STR" Time Diff=%Ld",
                AL_NET_ADDR_TO_STR(&parent->ds_mac),
                difference);

   parent->flags |= MESH_PARENT_FLAG_DISABLED;

   if (parent == current_parent)
   {
      *current_parent_purged = 1;
   }

 //  al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
   return 0;
}


int mesh_ng_core_purge_parents(AL_CONTEXT_PARAM_DECL_SINGLE)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
   int      current_parent_purged;
   al_u64_t tick;
   al_u64_t timeout;

   current_parent_purged = 0;
   tick = al_get_tick_count(AL_CONTEXT_SINGLE);

   if (last_destroy == 0)
   {
      last_destroy = tick;
   }

   mesh_ng_enumerate_parents(AL_CONTEXT MESH_NG_ENUMERATE_PARENTS_TYPE_ALL,
                             (void *)&current_parent_purged,
                             _purge_parents_enum_func, 0);
   timeout = last_destroy + _MESH_NG_CORE_DESTROY_TIMEOUT * HZ;

   if (((mesh_hardware_info->monitor_type == MONITOR_TYPE_PMON) ||
        (mesh_hardware_info->monitor_type == MONITOR_TYPE_AMON)) &&
       al_timer_after_eq(tick, timeout))
   {
      mesh_ng_destroy_disabled_parents(AL_CONTEXT_SINGLE);
      last_destroy = tick;
   }

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
   return current_parent_purged;
}


int mesh_ng_core_join_parent(AL_CONTEXT_PARAM_DECL parent_t *parent)
{
   return _join_parent_helper(AL_CONTEXT parent);
}


static int _join_parents_adhoc_enum_func(AL_CONTEXT_PARAM_DECL void *enum_param, parent_t *parent)
{
   int i, j;
   int ret;
   int *joined;
   int flags = 0;
   int ifcnt;
   al_net_addr_t *self_root_bssid = NULL;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);

   joined = (int *)enum_param;

   al_net_if_t *net_if;

   if (parent->flags & MESH_PARENT_FLAG_CHILD ||
       parent->flags & MESH_PARENT_FLAG_DISABLED ||
       parent->flags & MESH_PARENT_FLAG_QUESTION ||
       parent->flags & MESH_PARENT_FLAG_AD_DISABLE)
   {
      goto _out;
   }

   switch (func_mode)
   {
      case _FUNC_MODE_LFR:
         self_root_bssid = _DS_MAC;
         break;
      case _FUNC_MODE_FFN:
      case _FUNC_MODE_LFN:
         if (!current_parent) {
            self_root_bssid = _DS_MAC;
         } else {
            self_root_bssid = &current_parent->root_bssid;
         }
         break;
      default:
         goto skip_bssid_check;
   }
   if (!AL_NET_ADDR_EQUAL(&parent->root_bssid, self_root_bssid))
   {
      ret = mesh_ng_mac_address_compare(&parent->root_bssid, self_root_bssid);
      if (ret <= 0) {
         goto _out;
      }
   }

skip_bssid_check:

   if (_IS_LIMITED)
   {
      if (!((parent->flags & MESH_PARENT_FLAG_LIMITED)))
      {
         /* If the parent is a FFR/FFN, we first need to give it the highest preference */
      }
      else
      {
            if ((func_mode == _FUNC_MODE_LFR))
            {
               if (MESH_NG_PARENT_IS_LFR(parent))
               {
                  ret = mesh_ng_mac_address_compare(&parent->ds_mac, _DS_MAC);
                  if (ret <= 0)
                  {
                     goto _out;
                  }
               }
            }
       }
   } else {
      if (((parent->flags & MESH_PARENT_FLAG_LIMITED))) {
            goto _out;
      }
   }
   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MESH_NG	: Trying to join "AL_NET_ADDR_STR"...", AL_NET_ADDR_TO_STR(&parent->bssid));

   /* Try for three times if not then try next one */

   for (i = 0; i < 3; i++)
   {
      ret = _join_parent_helper(AL_CONTEXT parent);

      if (ret == 0)
      {
         *joined = 1;
         return -1;
      }

      parent->flags |= MESH_PARENT_FLAG_DISABLED;

      ++parent->disconnect_count;
   }

_out:

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
   return 0;
}

int mesh_ng_core_join_best_parent_adhoc(AL_CONTEXT_PARAM_DECL_SINGLE)
{
   int joined;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
   joined = 0;

   mesh_ng_enumerate_parents(AL_CONTEXT MESH_NG_ENUMERATE_PARENTS_TYPE_ALL,
                             (void *)&joined,
                             _join_parents_adhoc_enum_func, 0);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
   return(joined == 0);
}
