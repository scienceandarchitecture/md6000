/********************************************************************************
* MeshDynamics
* --------------
* File     : mesh_ng_fsm_evaluate_adhoc.c
* Comments : Evaluation state for DIS-JOINT AD-HOC mode
* Created  : 8/21/2007
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  1  |8/22/2008 | Changes for SIP                                 | Abhijit|
* -----------------------------------------------------------------------------
* |  0  |8/21/2007 | Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

/**
 * This is a private source file included by mesh_ng_fsm.c, hence
 * It was created to improve readability.
 */
extern int mesh_on_start_find_ds1(int boot_flag);

extern int mesh_on_start_initialize_ds_interface1(void);

static void _evaluate_adhoc_execute(AL_CONTEXT_PARAM_DECL_SINGLE)
{
   parent_t              *parent;
   access_point_config_t *config;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_NG : FLOW : Enter: %s : %d\n", __func__,__LINE__);

   config = &globals.config;

   if (mesh_mode == _MESH_AP_RELAY)
   {
      mesh_ng_evaluate_list(AL_CONTEXT_SINGLE);

      parent = mesh_ng_get_best_parent_adhoc(AL_CONTEXT_SINGLE);

      if (parent == NULL)
      {
         al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_NG : ERROR : parent is NULL %s : %d\n", __func__,__LINE__);
         goto _out;
      }

      if (parent != current_parent)
      {
         if (mesh_config->evaluation_damping_factor <= 0)
         {
            goto _change;
         }

         if (_evaluation_damping_parent == NULL)
         {
            _evaluation_damping_parent = parent;
            _evaluation_damping_count  = mesh_config->evaluation_damping_factor;
            goto _out;
         }
         else
         {
            if (_evaluation_damping_parent == parent)
            {
               if (--_evaluation_damping_count <= 0)
               {
                  goto _change;
               }
               else
               {
                  goto _out;
               }
            }
            else
            {
               _evaluation_damping_parent = parent;
               _evaluation_damping_count  = mesh_config->evaluation_damping_factor;
               goto _out;
            }
         }

_change:

         /**
          * If parent change has been locked, we skip and get out
          */

         if (AL_ATOMIC_GET(lock_parent_change))
         {
            al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,
                         "MESH_NG : INFO : %s Parent shift to "AL_NET_ADDR_STR" deffered due to change lock %s : %d\n",
                         parent->flags & MESH_PARENT_FLAG_PREFERRED ? "Preferred" : "",
                         AL_NET_ADDR_TO_STR(&parent->bssid), __func__, __LINE__);
            goto _out;
         }

         _evaluation_damping_parent = NULL;

         al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,
                      "MESH_NG : INFO : %s Parent shift to "AL_NET_ADDR_STR" CH %d required, diff = %d %s : %d\n",
                      parent->flags & MESH_PARENT_FLAG_PREFERRED ? "Preferred" : "",
                      AL_NET_ADDR_TO_STR(&parent->bssid),
                      parent->channel,
                      current_parent->score - parent->score,
                      __func__,
                      __LINE__);

         al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MESH_NG : INFO : Disabling AP processing before parent switch  %s : %d\n", __func__, __LINE__);

         access_point_enable_disable_processing(AL_CONTEXT 0);

         AL_ATOMIC_SET(_DS_NET_IF->buffering_state, AL_NET_IF_BUFFERING_STATE_NONE);

         if (mesh_ng_core_join_parent(AL_CONTEXT parent))
         {
            /**
             * Upon failure, we immedietly become a LFR
             */
            al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"MESH_NG : INFO : Failed to join parent, Beacoming LFR ... %s : %d\n", __func__, __LINE__); 
            if (mesh_config->failover_enabled)
            {
               mesh_on_start_find_ds1(0);
               if (!strcmp(mesh_hardware_info->ds_net_if->name, "eth1"))
               {
                  /* update ds interface parameters */
                  config->ds_net_if_info.net_if      = mesh_hardware_info->ds_net_if;
                  config->ds_net_if_info.ack_timeout = mesh_hardware_info->ds_if_ack_timeout;
                  config->ds_net_if_info.txpower     = mesh_hardware_info->ds_if_tx_power;
                  config->ds_net_if_info.txrate      = mesh_hardware_info->ds_if_tx_rate;

                  strcpy(config->ds_net_if_info.essid, mesh_hardware_info->ds_conf->essid);
                  AL_ATOMIC_SET(_DS_NET_IF->buffering_state, AL_NET_IF_BUFFERING_STATE_NONE);
               }
               else
               {
                  config->ds_net_if_info.net_if      = mesh_hardware_info->ds_net_if;
                  config->ds_net_if_info.ack_timeout = mesh_hardware_info->ds_if_ack_timeout;
                  config->ds_net_if_info.txpower     = mesh_hardware_info->ds_if_tx_power;
                  config->ds_net_if_info.txrate      = mesh_hardware_info->ds_if_tx_rate;
                  AL_ATOMIC_SET(_DS_NET_IF->buffering_state, AL_NET_IF_BUFFERING_STATE_DROP);
               }
            }
            else
            {
               AL_ATOMIC_SET(_DS_NET_IF->buffering_state, AL_NET_IF_BUFFERING_STATE_DROP);
            }

            func_mode      = _FUNC_MODE_LFR;
            mesh_mode      = _MESH_AP_RELAY;
            current_parent = &dummy_lfr_parent;
            if (!_IS_LIMITED)
            {
               postscan_setup_wms();
            }

            al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MESH_NG : INFO : Join failed with "AL_NET_ADDR_STR","
                 "enabling AP processing after parent switch, %s : %d\n", AL_NET_ADDR_TO_STR(&parent->bssid), __func__, __LINE__);
            access_point_enable_disable_processing(AL_CONTEXT 1);
         }
         else
         {
            if (parent->flags & MESH_PARENT_FLAG_LIMITED)
            {
               func_mode = _FUNC_MODE_LFN;
               mesh_mode = _MESH_AP_RELAY;
            }
            else
            {
               func_mode = _FUNC_MODE_FFN;
               mesh_mode = _MESH_AP_RELAY;
            }
            al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MESH_NG : INFO : Enabling AP processing after parent switch  %s : %d\n",  __func__, __LINE__);

            AL_ATOMIC_SET(_DS_NET_IF->buffering_state, AL_NET_IF_BUFFERING_STATE_NONE);
            access_point_enable_disable_processing(AL_CONTEXT 1);

            mesh_imcp_send_packet_ap_sub_tree_info(AL_CONTEXT_SINGLE);

            mesh_imcp_send_packet_heartbeat(AL_CONTEXT mesh_heart_beat_sqnr);
            mesh_imcp_send_packet_heartbeat2(AL_CONTEXT mesh_heart_beat_sqnr);
            mesh_heart_beat_sqnr++;

            mesh_plugin_post_event(MESH_PLUGIN_EVENT_PARENT_SHIFTED, NULL);
         }

         mesh_ng_fsm_update_downlink_vendor_info(AL_CONTEXT_SINGLE);
      }
      else
      {
         _evaluation_damping_parent = NULL;
         _evaluation_damping_count  = 0;
      }
   }

_out:

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_NG : FLOW : Exit: %s : %d\n", __func__,__LINE__);
   _mesh_ng_fsm_set_state(AL_CONTEXT MESH_NG_FSM_STATE_RUNNING);
}
