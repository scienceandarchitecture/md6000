/********************************************************************************
* MeshDynamics
* --------------
* File     : mesh_ng_fsm_step_scan_uplink.c
* Comments : Step Scanning State for Uplinks
* Created  : 8/21/2007
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  2  |02/24/2009| Changes for disjoint adhoc mode                 | Sriram |
* -----------------------------------------------------------------------------
* |  1  |02/12/2009| Changes for single radio                        | Sriram |
* -----------------------------------------------------------------------------
* |  0  |1/20/2008 | Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

/**
 * This is a private source file included by mesh_ng_fsm.c, hence
 * It was created to improve readability.
 */

#define _BUFFER_MAX_TIME             400
#define _CHILD_BUFFER_MAX_TIME       20
#define _DOWNLINK_BROADCAST_COUNT    3

static unsigned char _action_counter = 0;

static int AL_INLINE _begin_buffering_before_scan_uplink(AL_CONTEXT_PARAM_DECL int dwell_time_in_millis)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
   int i;
   int j;
   al_802_11_operations_t *ex_op;
   al_net_if_t            *net_if;
   unsigned char          buffer[32];
   unsigned char          data[8];
   unsigned char          *p;
   int tm;
   int length;

   if (func_mode != _FUNC_MODE_LFR)
   {
      al_reset_event(AL_CONTEXT mesh_ds_buffer_ack_event);

      mesh_imcp_send_packet_buffering_command(AL_CONTEXT _DS_NET_IF,
                                              IMCP_SNIP_BUFFERING_TYPE_START_REQUEST_WITH_ACK,
                                              dwell_time_in_millis);

      if (al_wait_on_event(AL_CONTEXT mesh_ds_buffer_ack_event, 50) != AL_WAIT_STATE_SIGNAL)
      {
         al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR," buffer overflow :: func : %s LINE : %d\n", __func__,__LINE__);
         return -1;
      }
   }

   if (mesh_config->use_virt_if)
   {
      p  = data;
      *p = 0;
      p++;                                                   /* master mode = 0, infra mode = 1 */

      tm = al_cpu_to_le32(_CHILD_BUFFER_MAX_TIME);
      memcpy(p, &tm, 4);
      p   += 4;
      *p++ = _action_counter++;

      ex_op  = (al_802_11_operations_t *)_DS_NET_IF->get_extended_operations(_DS_NET_IF);
      length = al_802_11_format_md_action(AL_CONTEXT IMCP_SNIP_ACTION_TYPE_ENABLE_BUFFERING, data, (p - data), buffer);

      for (i = 0; i < mesh_hardware_info->wm_net_if_count; i++)
      {
         net_if = mesh_hardware_info->wms_conf[i]->net_if;

         if (AL_NET_IF_IS_WIRELESS(net_if))
         {
            ex_op = (al_802_11_operations_t *)net_if->get_extended_operations(net_if);
            if (ex_op != NULL)
            {
               for (j = 0; j < _DOWNLINK_BROADCAST_COUNT; j++)
               {
                  ex_op->send_action(net_if, &broadcast_net_addr, buffer, length);
               }
            }
         }
      }
   }


   access_point_enable_thread(AL_CONTEXT 0);

   if (func_mode != _FUNC_MODE_LFR)
   {
      AL_ATOMIC_SET(_DS_NET_IF->buffering_state, AL_NET_IF_BUFFERING_STATE_REQUEUE);
   }

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
   return 0;
}


static void AL_INLINE _end_buffering_after_scan_uplink(AL_CONTEXT_PARAM_DECL int dwell_time_in_millis)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
   int i;
   al_802_11_operations_t *ex_op;
   al_net_if_t            *net_if;
   unsigned char          buffer[32];
   unsigned char          data[8];
   unsigned char          *p;
   int length;

   if (func_mode != _FUNC_MODE_LFR)
   {
      mesh_imcp_send_packet_buffering_command(AL_CONTEXT _DS_NET_IF, IMCP_SNIP_BUFFERING_TYPE_STOP_REQUEST, dwell_time_in_millis);
      AL_ATOMIC_SET(_DS_NET_IF->buffering_state, AL_NET_IF_BUFFERING_STATE_NONE);
   }

   access_point_enable_thread(AL_CONTEXT 1);

   if (mesh_config->use_virt_if)
   {
      p  = data;
      *p = 0;
      p++;                                                   /* master mode = 0, infra mode = 1 */

      memset(p, 0, 4);
      p += 4;

      ex_op  = (al_802_11_operations_t *)_DS_NET_IF->get_extended_operations(_DS_NET_IF);
      length = al_802_11_format_md_action(AL_CONTEXT IMCP_SNIP_ACTION_TYPE_ENABLE_BUFFERING, data, (p - data), buffer);

      for (i = 0; i < mesh_hardware_info->wm_net_if_count; i++)
      {
         net_if = mesh_hardware_info->wms_conf[i]->net_if;

         if (AL_NET_IF_IS_WIRELESS(net_if))
         {
            ex_op = (al_802_11_operations_t *)net_if->get_extended_operations(net_if);
            if (ex_op != NULL)
            {
               ex_op->send_action(net_if, &broadcast_net_addr, buffer, length);
            }
         }
      }
   }
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
}


static void AL_INLINE _virtual_associate_step_scan_uplink(AL_CONTEXT_PARAM_DECL al_net_addr_t *bssid, int channel, int start)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
   al_802_11_operations_t *ex_op;

   ex_op = (al_802_11_operations_t *)_DS_NET_IF->get_extended_operations(_DS_NET_IF);

   if (ex_op != NULL)
   {
      ex_op->virtual_associate(_DS_NET_IF, bssid, channel, start);
   }
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
}


static void _step_scan_uplink_execute(AL_CONTEXT_PARAM_DECL void *event_data)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
   int dwell_time_in_millis;
   int channel;
   int ap_count;
   al_scan_access_point_info_t *access_points;
   int ret;

   if ((_DS_CONF->dca_list_count == 0) || (mesh_mode != _MESH_AP_RELAY))
   {
      goto _out;
   }

   if ((step_scan_channel_index < 0) || (step_scan_channel_index >= _DS_CONF->dca_list_count))
   {
      step_scan_channel_index = 0;
   }

   channel = _DS_CONF->dca_list[step_scan_channel_index];
   dwell_time_in_millis = (int)event_data;

#if 0
	/* Since data frame buffer in power save is handled by driver, this part of code is not required. Let it commented */
   if (_begin_buffering_before_scan_uplink(AL_CONTEXT _BUFFER_MAX_TIME) != 0)
   {
      _end_buffering_after_scan_uplink(AL_CONTEXT _BUFFER_MAX_TIME);
      goto _out;
   }

   _virtual_associate_step_scan_uplink(AL_CONTEXT & zeroed_net_addr, channel, 2);
#endif

   ret = mesh_ng_step_scan_parents(AL_CONTEXT
                                   step_scan_channel_index,
                                   dwell_time_in_millis,
                                   &access_points,
                                   &ap_count);
#if 0
	/* Since data frame buffer in power save is handled by driver, this part of code is not required. Let it commented */
   _virtual_associate_step_scan_uplink(AL_CONTEXT & current_parent->bssid, current_parent->channel, 0);

   _end_buffering_after_scan_uplink(AL_CONTEXT _BUFFER_MAX_TIME);
#endif

   if (ret == 0)
   {
      ret = mesh_ng_step_process_parents(AL_CONTEXT ap_count, access_points);

      al_heap_free(AL_CONTEXT access_points);
   }

   ++step_scan_channel_index;

_out:

   _mesh_ng_fsm_set_state(AL_CONTEXT MESH_NG_FSM_STATE_RUNNING);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
}
