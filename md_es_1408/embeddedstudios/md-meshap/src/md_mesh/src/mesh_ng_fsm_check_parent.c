/********************************************************************************
* MeshDynamics
* --------------
* File     : mesh_ng_fsm_check_parent.c
* Comments : Mesh new generation check parent state
* Created  : 4/20/2006
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  0  |4/20/2006 | Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

/**
 * This is a private source file included by mesh_ng_fsm.c, hence
 * It was created to improve readability.
 */
static void _check_parent_execute(AL_CONTEXT_PARAM_DECL void *event_data)
{
   parent_t *parent;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Enter %s<%d>\n",__func__,__LINE__);
   parent = (parent_t *)event_data;

   mesh_ng_mark_parent_for_sampling(AL_CONTEXT parent);

   _mesh_ng_fsm_set_state(AL_CONTEXT MESH_NG_FSM_STATE_RUNNING);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Exit %s<%d>\n",__func__,__LINE__);
}
