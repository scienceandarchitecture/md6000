/********************************************************************************
* MeshDynamics
* --------------
* File     : mesh_ng_core.h
* Comments : Mesh New Generation Implementation Core
* Created  : 4/10/2006
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  1  |6/13/2007 | mesh_ng_core_join_parent Added                  | Sriram |
* -----------------------------------------------------------------------------
* |  0  |4/10/2006 | Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

#include "mesh_ng.h"

#ifndef __MESH_NG_CORE_H__
#define __MESH_NG_CORE_H__

int mesh_ng_core_join_best_parent(AL_CONTEXT_PARAM_DECL_SINGLE);
int mesh_ng_core_purge_parents(AL_CONTEXT_PARAM_DECL_SINGLE);
int mesh_ng_core_join_parent(AL_CONTEXT_PARAM_DECL parent_t *parent);

int mesh_ng_core_join_best_parent_adhoc(AL_CONTEXT_PARAM_DECL_SINGLE);
#endif /*__MESH_NG_CORE_H__*/
