/********************************************************************************
* MeshDynamics
* --------------
* File     : mesh_ng_scan.h
* Comments : Mesh New Generation Scan Implementation
* Created  : 4/6/2006
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  0  |4/6/2006 | Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

#ifndef __MESH_NG_SCAN_H__
#define __MESH_NG_SCAN_H__

#define MESH_NG_SCAN_MODE_DS_NET_IF           1
#define MESH_NG_SCAN_MODE_MONITOR_NET_IF      2
#define MESH_NG_SCAN_MODE_OTHER               3

#define MESH_NG_SCAN_ERROR_BAD_NET_IF         -1
#define MESH_NG_SCAN_ERROR_SCAN_PROBLEM       -2
#define MESH_NG_SCAN_ERROR_PROCESS_PROBLEM    -3

int mesh_ng_scan_parents(AL_CONTEXT_PARAM_DECL int mode);

int mesh_ng_step_scan_parents(AL_CONTEXT_PARAM_DECL
                              int                         channel_index,
                              int                         dwell_time,
                              al_scan_access_point_info_t **access_points,
                              int                         *ap_count);

int mesh_ng_step_process_parents(AL_CONTEXT_PARAM_DECL
                                 int                         ap_count,
                                 al_scan_access_point_info_t *access_points);
int mesh_ng_check_for_parent_state(unsigned char* vendor_info, unsigned char vendor_info_length);
#endif /*__MESH_NG_SCAN_H__*/
