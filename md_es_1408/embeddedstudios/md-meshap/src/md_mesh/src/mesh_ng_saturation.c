/********************************************************************************
* MeshDynamics
* --------------
* File     : mesh_ng_saturation.c
* Comments : Downlink saturation information
* Created  : 5/9/2006
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  1  |11/14/2006| Changes for heap debug support                  | Sriram |
* -----------------------------------------------------------------------------
* |  0  |5/9/2006  | Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

#include <linux/string.h>
#include "al.h"
#include "al_print_log.h"
#include "al_conf.h"
#include "access_point.h"
#include "al_impl_context.h"
#include "mesh_defs.h"
#include "mesh_table.h"
#include "mesh.h"
#include "mesh_globals.h"
#include "mesh_errors.h"
#include "mesh_on_start.h"
#include "mesh_intervals.h"
#include "imcp_snip.h"
#include "mesh_imcp.h"
#include "mesh_ng.h"

#define _WM_SATURATION_PACKET_COUNT      5
#define _WM_SATURATION_PACKET_SPACING    100
#define _WM_SATURATION_CHANNEL_DWELL     2000
#define _WM_SATURATION_EXTRA_TIME        1000

static AL_INLINE int _initialize_duty_cycle_info(AL_CONTEXT_PARAM_DECL mesh_conf_if_info_t *conf_if_info, al_duty_cycle_info_t **duty_cycle_info)
{
   int channel_count;
   al_duty_cycle_info_t *info;
   int i;

   channel_count = conf_if_info->dca_list_count;
   info          = NULL;

   if (channel_count <= 0)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_NG : ERROR : channel count %d %s : %d\n", channel_count, __func__,__LINE__);
      goto _out;
   }

   info = (al_duty_cycle_info_t *)al_heap_alloc(AL_CONTEXT sizeof(al_duty_cycle_info_t) * channel_count AL_HEAP_DEBUG_PARAM);
   memset(info, 0, sizeof(al_duty_cycle_info_t) * channel_count);

   for (i = 0; i < conf_if_info->dca_list_count; i++)
   {
      info[i].channel = conf_if_info->dca_list[i];
   }

_out:

   *duty_cycle_info = info;

   return channel_count;
}


static AL_INLINE void _uninitialize_duty_cycle_info(AL_CONTEXT_PARAM_DECL int channel_count, al_duty_cycle_info_t *duty_cycle_info)
{
   int i;

   for (i = 0; i < channel_count; i++)
   {
      al_heap_free(AL_CONTEXT duty_cycle_info[i].aps);
   }

   al_heap_free(AL_CONTEXT duty_cycle_info);
}


static AL_INLINE void _do_wm_saturation(AL_CONTEXT_PARAM_DECL mesh_conf_if_info_t *conf_if_info)
{
   int i;
   al_duty_cycle_info_t *info;
   int channel_count;
   int timeout, flag = 1;
   al_802_11_operations_t *ex_op;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MESH_NG: INFO : Determining saturation information for %s %s : %d\n",
                                       conf_if_info->net_if->name, __func__, __LINE__);

   channel_count = _initialize_duty_cycle_info(AL_CONTEXT conf_if_info, &info);

   if (channel_count <= 0)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_NG : ERROR : channel count %d %s : %d\n", channel_count, __func__,__LINE__);
      return;
   }

   timeout  = _WM_SATURATION_PACKET_COUNT * _WM_SATURATION_PACKET_SPACING;
   timeout += channel_count * _WM_SATURATION_CHANNEL_DWELL;
   timeout += _WM_SATURATION_EXTRA_TIME;

   /**
    * First send _WM_SATURATION_PACKET_COUNT buffer command packets spaced _WM_SATURATION_PACKET_SPACING ms apart
    */

   for (i = 0; i < _WM_SATURATION_PACKET_COUNT; i++)
   {
      mesh_imcp_send_packet_buffering_command(AL_CONTEXT conf_if_info->net_if, IMCP_SNIP_BUFFERING_TYPE_DISABLE_UPLINK, timeout);
      al_thread_sleep(AL_CONTEXT _WM_SATURATION_PACKET_SPACING);
   }

   AL_ATOMIC_SET(conf_if_info->net_if->buffering_state, AL_NET_IF_BUFFERING_STATE_DROP);

   /**
    * Now we are ready to compute the duty cycle information
    */

   ex_op = (al_802_11_operations_t *)conf_if_info->net_if->get_extended_operations(conf_if_info->net_if);

   //commenting for now:
   /* TODO:: from the user sapce call we need to do duty cycle with proper interface with buffered mechanism
    *  which should not affect the connectivity of the network */
   //ex_op->get_duty_cycle_info(conf_if_info->net_if, channel_count, info, _WM_SATURATION_CHANNEL_DWELL, flag);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MESH_NG : INFO : Sending saturation information for %s %s : %d\n",
                                                                 conf_if_info->net_if->name, __func__, __LINE__);

   mesh_imcp_send_packet_downlink_saturation_info(AL_CONTEXT conf_if_info->net_if, channel_count, info);

   _uninitialize_duty_cycle_info(AL_CONTEXT channel_count, info);

   ex_op->set_channel(conf_if_info->net_if, conf_if_info->operating_channel);

   AL_ATOMIC_SET(conf_if_info->net_if->buffering_state, AL_NET_IF_BUFFERING_STATE_NONE);
}


static int _saturation_thread(AL_CONTEXT_PARAM_DECL al_thread_param_t *param)
{
   int i;
   al_set_thread_name(AL_CONTEXT "saturation_thread");

   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MESH_NG : INFO : Saturation thread starting... %s : %d\n", __func__, __LINE__);

   for (i = 0; i < mesh_hardware_info->wm_net_if_count; i++)
   {
      if (AL_NET_IF_IS_WIRELESS(mesh_hardware_info->wms_conf[i]->net_if))
      {
         _do_wm_saturation(AL_CONTEXT mesh_hardware_info->wms_conf[i]);
      }
   }

   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MESH_NG: INFO : Saturation thread quitting... %s : %d\n", __func__, __LINE__);

   return 0;
}


void mesh_ng_saturation_execute(AL_CONTEXT_PARAM_DECL_SINGLE)
{
   al_create_thread_on_cpu(AL_CONTEXT _saturation_thread, NULL,0, "_saturation_thread");
}
