/********************************************************************************
* MeshDynamics
* --------------
* File     : mesh_ap.c
* Comments : Mesh AP hook function implementation.
* Created  : 4/13/2004
* Author   : Anand Bhange
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* | 73  |02/12/2009| Additional Changes for Uplink Scan Functionality| Sriram |
* -----------------------------------------------------------------------------
* | 72  |1/27/2009 | Changes for Uplink Scan Functionality           | Sriram |
* -----------------------------------------------------------------------------
* | 71  |8/22/2008 | Changes for FIPS and SIP                        |Abhijit |
* -----------------------------------------------------------------------------
* | 70  |7/24/2007 | REBOOT logging changes                          | Sriram |
* -----------------------------------------------------------------------------
* | 69  |7/20/2007 | Ethernet link thread timer changed to 15 minutes| Sriram |
* -----------------------------------------------------------------------------
* | 68  |7/9/2007  | Watchdog parameters changed                     | Sriram |
* -----------------------------------------------------------------------------
* | 67  |6/13/2007 | current_parent->signal not changed when mobile  | Sriram |
* -----------------------------------------------------------------------------
* | 66  |4/26/2007 | Disable interrupts when calling mesh table      | Sriram |
* -----------------------------------------------------------------------------
* | 65  |3/14/2007 | Disabled access_point_auth_request_handler      | Sriram |
* -----------------------------------------------------------------------------
* | 64  |10/18/2006| Changes for ETSI Radar                          | Sriram |
* -----------------------------------------------------------------------------
* | 63  |3/17/2006 | Syncing of direct children done                 | Sriram |
* -----------------------------------------------------------------------------
* | 62  |02/15/2006| changes for dot11e                              | Bindu  |
* -----------------------------------------------------------------------------
* | 61  |02/03/2006| mesh flags checked in assoc_request_handler     | Abhijit|
* -----------------------------------------------------------------------------
* | 60  |01/01/2006| Changes for heartbeat2                          | Abhijit|
* -----------------------------------------------------------------------------
* | 59  |10/5/2005 | Loop detection changes                          | Sriram |
* -----------------------------------------------------------------------------
* | 58  |10/5/2005 | Changes for enabling MIP                        | Sriram |
* -----------------------------------------------------------------------------
* | 57  |9/22/2005 | Interrupt disable/enable semantics changed      | Sriram |
* -----------------------------------------------------------------------------
* | 56  |8/20/2005 | Seek new parent during times of low bit rates   | Sriram |
* -----------------------------------------------------------------------------
* | 55  |7/29/2005 | Misc changes                                    | Sriram |
* -----------------------------------------------------------------------------
* | 54  |6/21/2005 | Misc changes                                    | Sriram |
* -----------------------------------------------------------------------------
* | 53  |6/12/2005 | Status/LED blink on every heartbeat             | Sriram |
* -----------------------------------------------------------------------------
* | 52  |5/13/2005 | Vendor info not added for Client only           | Sriram |
* -----------------------------------------------------------------------------
* | 51  |5/9/2005  | Watchdog timer additional changes               | Sriram |
* -----------------------------------------------------------------------------
* | 50  |5/8/2005  | Watchdog timer added to mesh thread             | Sriram |
* -----------------------------------------------------------------------------
* | 49  |4/22/2005 | Relay node rebooted when ethernet link          | Sriram |
* -----------------------------------------------------------------------------
* | 48  |4/11/2005 | Misc changes after merging                      | Sriram |
* -----------------------------------------------------------------------------
* | 47  |2/21/2005 | Vendor info added to beacons,HandShake Res to VI| Anand  |
* -----------------------------------------------------------------------------
* | 46  |1/10/2005 | Implemented MESH_AP_BUS_SPEED_TEST              | Sriram |
* -----------------------------------------------------------------------------
* | 45  |12/13/2004| Averaged out RSSI                               | Sriram |
* -----------------------------------------------------------------------------
* | 44  |8/26/2004 | Mesh Init Packet changes.                       | Anand  |
* -----------------------------------------------------------------------------
* | 43  |7/29/2004 | Misc Changes                                    | Anand  |
* -----------------------------------------------------------------------------
* | 42  |7/25/2004 | Misc changes                                    | Sriram |
* -----------------------------------------------------------------------------
* | 41  |7/25/2004 | bug fixed for on_phy_link_notify                | Anand  |
* -----------------------------------------------------------------------------
* | 40  |7/24/2004 | Misc changes                                    | Anand  |
* -----------------------------------------------------------------------------
* | 39  |7/23/2004 | Misc changes                                    | Sriram |
* -----------------------------------------------------------------------------
* | 38  |7/23/2004 | link_state check added in on_phy_link_notify    | Anand  |
* -----------------------------------------------------------------------------
* | 37  |7/21/2004 | IP Ap start packet send                         | Anand  |
* -----------------------------------------------------------------------------
* | 36  |7/19/2004 | Rssi added to on_data_reception                 | Anand  |
* -----------------------------------------------------------------------------
* | 35  |7/5/2004  | check to avoid ds net if & pseudoWM eth         | Anand  |
* -----------------------------------------------------------------------------
* | 34  |7/5/2004  | waiting in stop if scanning(proper exit)        | Anand  |
* -----------------------------------------------------------------------------
* | 33  |6/30/2004 | checking for sta ourging if DS packet           | Anand  |
* -----------------------------------------------------------------------------
* | 32  |6/17/2004 | Misc changes                                    | Sriram |
* -----------------------------------------------------------------------------
* | 33  |6/16/2004 | Fixes for pSeudo WM                             | Anand  |
* -----------------------------------------------------------------------------
* | 32  |6/15/2004 | known_ap enable disable on assoc/disassoc       | Anand  |
* -----------------------------------------------------------------------------
* | 31  |6/11/2004 | mesh_imcp_send_packet_hardware_info called      | Anand  |
* -----------------------------------------------------------------------------
* | 30  |6/10/2004 | Bug Fixed - Mesh Thread exits on time           | Anand  |
* -----------------------------------------------------------------------------
* | 29  |6/9/2004 | correct value of frame_control                   | Anand  |
* -----------------------------------------------------------------------------
* | 28  |6/7/2004  | net_if_out values set for on_data handler       | Anand  |
* -----------------------------------------------------------------------------
* | 27  |6/7/2004  | Filter macro used instead of al_print_log       | Anand  |
* -----------------------------------------------------------------------------
* | 26  |6/7/2004  | Disable/Enable Intr for locking used in Intr    | Anand  |
* -----------------------------------------------------------------------------
* | 25  |5/31/2004 | changes in include files for linux compilation  | Anand  |
* -----------------------------------------------------------------------------
* | 24  |5/28/2004 | varify_wireless_ds_parent called for change thr | Anand  |
* -----------------------------------------------------------------------------
* | 23  |5/28/2004 | Beacon hook added                               | Anand  |
* -----------------------------------------------------------------------------
* | 22  |5/28/2004 | ap_is_child added to avoid inconsistant state   | Anand  |
* -----------------------------------------------------------------------------
* | 21  |5/28/2004 | known_aps operations moved to mesh.c            | Anand  |
* -----------------------------------------------------------------------------
* | 20  |5/25/2004 | mesh state notify messages added                | Anand  |
* -----------------------------------------------------------------------------
* | 19  |5/25/2004 | al_on_phy_link_notify func added                | Anand  |
* -----------------------------------------------------------------------------
* | 18  |5/21/2004 | Log type added to al_print_log                  | Anand  |
* -----------------------------------------------------------------------------
* | 17  |5/21/2004 | Bug Fixed,  Mesh Thread alt parent logic.       | Anand  |
* -----------------------------------------------------------------------------
* | 16  |5/18/2004 | mesh_flag added to mesh_ap_initialise,          |		   |
* |     |	        | mesh_ap_uninitialise, ap_on_start_handler	      | Bindu  |
* -----------------------------------------------------------------------------
* | 15  |5/14/2004 | Bug Fixed in mesh thread                        | Anand  |
* -----------------------------------------------------------------------------
* | 14  |5/14/2004 | Stop state checking added while startup         | Anand  |
* -----------------------------------------------------------------------------
* | 13  |5/12/2004 | value assigned to status_code_out in assoc req  | Anand  |
* -----------------------------------------------------------------------------
* | 12  |5/12/2004 | Invalid condition for print_log corrected       | Anand  |
* -----------------------------------------------------------------------------
* | 11  |5/11/2004 | Bugs fixed, start / stop of APS, (ReInit)       | Anand  |
* -----------------------------------------------------------------------------
* | 10  |5/7/2004  | Mesh Thread Sleep removed.                      | Anand  |
* -----------------------------------------------------------------------------
* |  9  |5/6/2004  | \r\n removed from log messages                  | Anand  |
* -----------------------------------------------------------------------------
* |  8  |5/6/2004  | Addition event object added                     | Anand  |
* -----------------------------------------------------------------------------
* |  7  |5/5/2004  | Packet/Memory Leak Debug params added           | Anand  |
* -----------------------------------------------------------------------------
* |  6  |4/30/2004 | Log messages Added                              | Anand  |
* -----------------------------------------------------------------------------
* |  5  |4/30/2004 | Waiting for thread to kill in uninit            | Anand  |
* -----------------------------------------------------------------------------
* |  4  |4/29/2004 | mesh_hardware_info initialized                  | Anand  |
* -----------------------------------------------------------------------------
* |  3  |4/27/2004 | Mesh thread added.                              | Anand  |
* -----------------------------------------------------------------------------
* |  2  |4/26/2004 | Testing Base Line                               | Anand  |
* -----------------------------------------------------------------------------
* |  1  |4/21/2004 | **Made (major) changes for AL_CONTEXT**         | Anand  |
* -----------------------------------------------------------------------------
* |  0  |4/13/2004 | Created                                         | Anand  |
* -----------------------------------------------------------------------------
********************************************************************************/

#include <linux/string.h>
#include <linux/module.h>
#include "al.h"
#include "al_print_log.h"
#include "al_802_11.h"
#include "access_point.h"
#include "sip_conf.h"
#include "mesh_defs.h"
#include "imcp_snip.h"
#include "al_impl_context.h"
#include "mesh_table.h"
#include "mesh.h"
#include "mesh_globals.h"
#include "mesh_ap.h"
#include "mesh_imcp.h"
#include "mesh_errors.h"
#include "mesh_on_start.h"
#include "mesh_monitor.h"
#include "mesh_intervals.h"
#include "mesh_ng.h"
#include "mesh_ng_fsm.h"
#include "mesh_ng_channel_change.h"
#include "mesh_location_info.h"
#include "mesh_ng_scan.h"
#include "mesh_plugin.h"
#include "mesh_plugin_impl.h"

AL_DECLARE_GLOBAL(int mesh_thread_running);
AL_DECLARE_GLOBAL(int mesh_thread_exit_wait_event);
AL_DECLARE_GLOBAL(int mesh_start_stop_event);
AL_DECLARE_GLOBAL(int mesh_thread_wait_event);
AL_DECLARE_GLOBAL(int mesh_hb_wait_event);
AL_DECLARE_GLOBAL(unsigned int mesh_heart_beat_sqnr);
AL_DECLARE_GLOBAL(int mesh_round_robin_lfn_state);
AL_DECLARE_GLOBAL(int mesh_ds_buffer_ack_event);
AL_DECLARE_GLOBAL(int mesh_ds_buffer_stop_event);

extern access_point_globals_t globals;

extern int mesh_on_start_find_ds(void);

extern int mesh_on_start_find_ds1(AL_CONTEXT int boot_flag);
extern int enable_disable_processing(AL_CONTEXT_PARAM_DECL unsigned char enable);

extern unsigned char use_mac_address[6];

int mesh_on_start_initialize_ds_interface1(void);

int mesh_on_start_initialize_ds_interface(void);

int al_on_phy_link_notify(AL_CONTEXT_PARAM_DECL al_net_if_t *al_net_if, int link_state);

int switch_over_2other_ds(void);
extern int mesh_network_unstable;

int        interrupt_cnt = 0;
extern int purge_imcp_sta;

/* By default we assume that the ping connectivity is UP.
 * Later on it will be updated by the status of the ping.
 */
int eth1_ping_status = 1;

int reboot_in_progress;
int threads_status;
struct thread_status thread_stat;

struct tx_rx_packet_stats tx_rx_pkt_stats;
EXPORT_SYMBOL(tx_rx_pkt_stats);

AL_DECLARE_GLOBAL_EXTERN(int _queue_handle);
AL_DECLARE_GLOBAL_EXTERN(int mesh_thread_wait_event);
AL_DECLARE_GLOBAL_EXTERN(int mesh_hb_wait_event);
AL_DECLARE_GLOBAL_EXTERN(int _cleanup_thread_exit_event);
extern void terminate_dBmTestRx_thread(void);
extern void terminate_dBmTestTx_thread(void);

static void _mesh_thread_watchdog_routine(void)
{
   al_reboot_board(AL_CONTEXT AL_REBOOT_BOARD_CODE_MESH_THREAD_WATCHDOG);
}


static int _parent_enum_func(AL_CONTEXT_PARAM_DECL void *enum_param, parent_t *parent)
{
   parent->last_hb_time   = al_get_tick_count(AL_CONTEXT_SINGLE);
   parent->last_seen_time = al_get_tick_count(AL_CONTEXT_SINGLE);
   return 0;
}


static int _uplink_scan_thread_function(AL_CONTEXT_PARAM_DECL al_thread_param_t *param)
{
   int            number_of_scans_per_sec;
   int            max_apart;
   unsigned int   sleep_time;
   unsigned short rand_val;

   al_set_thread_name(AL_CONTEXT "uplink_scan_thread");

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Enter %s<%d> Uplink scanner thread started \n",__func__,__LINE__);

   /**
    * Scans per second = [(Duty Cycle/100) * 1000]/20 = (Duty Cycle/2)
    * Duty cycle stored in dy_channel_alloc field of uplink.
    */

   number_of_scans_per_sec = _DS_CONF->dy_channel_alloc;

   if (number_of_scans_per_sec <= 0)
   {
      goto _out;
   }

   max_apart = (500) / (number_of_scans_per_sec);
   mesh_config->scanner_dwell_interval = max_apart * 2 * _DS_CONF->dca_list_count;

   while (1)
   {
	  thread_stat.uplink_scan_thread_count++;
      if ((mesh_state == _MESH_STATE_STOPPPING) || (mesh_state == _MESH_STATE_NOT_STARTED))
      {
         break;
      }
	  if(reboot_in_progress) {
          al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"MESH_AP:INFO: %s<%d> Terminating -----\n",__func__,__LINE__);
		  threads_status &= ~THREAD_STATUS_BIT7_MASK;
		  break;
	  }

      /**
       * Generate random number between 0 - 65535 (inclusive)
       */

      al_cryptographic_random_bytes(AL_CONTEXT(unsigned char *) & rand_val, sizeof(rand_val));

      /**
       * Division by 65536
       */

      sleep_time   = (rand_val * max_apart);
      sleep_time >>= 16;
      sleep_time  += max_apart;

      al_thread_sleep(AL_CONTEXT sleep_time);

      if (mesh_state == _MESH_STATE_RUNNING)
      {
         mesh_ng_fsm_send_event(AL_CONTEXT MESH_NG_FSM_EVENT_STEP_SCAN_UPLINK, (void *)20);

         if (step_scan_channel_index >= _DS_CONF->dca_list_count)
         {
            mesh_ng_fsm_send_event(AL_CONTEXT MESH_NG_FSM_EVENT_EVALUATE_MOBILE, NULL);
         }

         al_set_event(AL_CONTEXT mesh_thread_wait_event);
      }
   }

_out:
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Exit %s<%d> Uplink scanner thread started \n",__func__,__LINE__);

   return 0;
}


static int _lfr_thread_function(AL_CONTEXT_PARAM_DECL al_thread_param_t *param)
{
   int channel_index;
   int dwell_time_in_millis;
   int ap_count;
   al_scan_access_point_info_t *access_points;
   int ret;

#define _MESH_HB_THREAD_LFR_SCAN_DWELL    250

   al_set_thread_name(AL_CONTEXT "lfr_thread");

   channel_index = -1;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Enter %s<%d>\n",__func__,__LINE__);
   while (1)
   {
	  thread_stat.lfr_thread_count++;
	  if(reboot_in_progress) {
          al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"MESH_AP:INFO: %s<%d> Terminating ----\n",__func__,__LINE__);
		  threads_status &= ~THREAD_STATUS_BIT8_MASK;
		  break;
	  }
      if (mesh_state != _MESH_STATE_RUNNING)
      {
         goto _sleep;
      }

      if (!_IS_LFR)
      {
         goto _sleep;
      }

      if ((channel_index < 0) || (channel_index >= _DS_CONF->dca_list_count))
      {
         channel_index = 0;
      }
       
      /*Below code is added to make sure in absence of DCA list
       * in the wlan1 interface we should scan for all the
       * available channels.
       * When channel index is -1 it will set channel_count = 0
       * in do_scan function.
       */
      if(_DS_CONF->dca_list_count == 0)
      {
          channel_index = -1;
      }

      /**
       * step_scan_channel_index is used by the MAIN step_scan FSM state
       * This state is used when in LFN mode as the scan takes place
       * step by step at different times. In LFR mode all scanning occurs
       * using this thread, but the proc entry reports the global
       * step_scan_channel_index. Hence here we keep it updated.
       */

      step_scan_channel_index = channel_index;

      dwell_time_in_millis = _MESH_HB_THREAD_LFR_SCAN_DWELL;

      ret = mesh_ng_step_scan_parents(AL_CONTEXT
                                      channel_index,
                                      dwell_time_in_millis,
                                      &access_points,
                                      &ap_count);

      if (ret == 0)
      {
         ret = mesh_ng_step_process_parents(AL_CONTEXT ap_count, access_points);

         al_heap_free(AL_CONTEXT access_points);

         mesh_ng_fsm_send_event(AL_CONTEXT MESH_NG_FSM_EVENT_EVALUATE_ADHOC, NULL);
         al_set_event(AL_CONTEXT mesh_thread_wait_event);
      }

      ++channel_index;

_sleep:
	  if(reboot_in_progress) {
          al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"MESH_AP:INFO: %s<%d> Terminating ----\n",__func__,__LINE__);
		  threads_status &= ~THREAD_STATUS_BIT8_MASK;
		  break;
	  }

      al_thread_sleep(AL_CONTEXT _MESH_HB_THREAD_LFR_SCAN_DWELL);
   }

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Exit %s<%d> thread Exited\n",__func__,__LINE__);

#undef _MESH_HB_THREAD_LFR_SCAN_DWELL

   return 0;
}


static int mesh_hb_thread_function(AL_CONTEXT_PARAM_DECL al_thread_param_t *param)
{
#define _MESH_HB_THREAD_FLAG_EVALUATE    0x01
#define _MESH_HB_THREAD_FLAG_SEND_HB     0x02

   int           watchdog_id;
   unsigned int  lfr_time_count;
   unsigned char flags;

   al_set_thread_name(AL_CONTEXT "mesh_hb_thread");

   watchdog_id = al_create_watchdog(AL_CONTEXT "mesh_hb_thread",
                                    mesh_config->hbeat_interval * 1000 * 15,
                                    _mesh_thread_watchdog_routine);

   lfr_time_count = 0;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Enter %s<%d>\n",__func__,__LINE__);

   while (1)
   {
      flags = 0;

	  thread_stat.mesh_hb_thread_count++;

      if (mesh_config) {
      al_wait_on_event(AL_CONTEXT mesh_hb_wait_event, mesh_config->hbeat_interval * 1000);
      }

      flags          = _MESH_HB_THREAD_FLAG_EVALUATE | _MESH_HB_THREAD_FLAG_SEND_HB;
      lfr_time_count = 0;

	  if(reboot_in_progress) {
		  al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"MESH_AP:INFO: %s<%d> Terminating---\n", __func__, __LINE__);
		  threads_status &= ~THREAD_STATUS_BIT6_MASK;
		  break;
	  }

      al_update_watchdog(AL_CONTEXT watchdog_id);

      if (mesh_config) {
      al_set_watchdog_timeout(AL_CONTEXT watchdog_id, mesh_config->hbeat_interval * 1000 * 15);
      }

      if ((mesh_state == _MESH_STATE_STOPPPING) || (mesh_state == _MESH_STATE_NOT_STARTED))
      {
         break;
      }
      if (mesh_state == _MESH_STATE_RUNNING)
      {
         if ((mesh_hardware_info->monitor_type == MONITOR_TYPE_ABSENT) &&
             (flags & _MESH_HB_THREAD_FLAG_EVALUATE) &&
             !(_IS_UPLINK_SCAN_ENABLED))
         {
            if (_DISJOINT_ADHOC_MODE)
            {
               switch (func_mode)
               {
               case _FUNC_MODE_FFR:
               case _FUNC_MODE_FFN:
                  mesh_ng_fsm_send_event(AL_CONTEXT MESH_NG_FSM_EVENT_EVALUATE, NULL);
                  break;

               case _FUNC_MODE_LFN:
                  mesh_ng_fsm_send_event(AL_CONTEXT MESH_NG_FSM_EVENT_EVALUATE_ADHOC, NULL);
                  break;
               }
            }
            else
            {
               if (mesh_hardware_info->monitor_type != MONITOR_TYPE_ABSENT)
                   mesh_ng_fsm_send_event(AL_CONTEXT MESH_NG_FSM_EVENT_EVALUATE, NULL);
            }
         }

         if (flags & _MESH_HB_THREAD_FLAG_SEND_HB)
         {
            mesh_ng_fsm_send_event(AL_CONTEXT MESH_NG_FSM_EVENT_HEARTBEAT, NULL);
         }

         if (flags)
         {
            al_set_event(AL_CONTEXT mesh_thread_wait_event);
         }
      }
   }

   al_destroy_watchdog(AL_CONTEXT watchdog_id);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MESH_AP:FLOW: Exit %s<%d> Exited\n", __func__, __LINE__);

#undef _MESH_HB_THREAD_FLAG_EVALUATE
#undef _MESH_HB_THREAD_FLAG_SEND_HB

   return 0;
}


static int mesh_thread_function(AL_CONTEXT_PARAM_DECL al_thread_param_t *param)
{
   int reason;

   al_set_thread_name(AL_CONTEXT "Mesh_Thread");

   al_reset_event(AL_CONTEXT mesh_thread_exit_wait_event);

   mesh_ng_enumerate_parents(AL_CONTEXT MESH_NG_ENUMERATE_PARENTS_TYPE_ALL, NULL, _parent_enum_func, 0);

   mesh_state = _MESH_STATE_RUNNING;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Enter %s<%d>\n",__func__,__LINE__);

   while (1)
   {
      /**
       * Process all FSM events and execute the FSM until
       * we get into the running state
       */

	  thread_stat.mesh_thread_count++;
      reason = al_wait_on_event(AL_CONTEXT mesh_thread_wait_event, AL_WAIT_TIMEOUT_INFINITE);
      al_reset_event(AL_CONTEXT mesh_thread_wait_event);

      do
      {
         if ((mesh_state == _MESH_STATE_STOPPPING) || (mesh_state == _MESH_STATE_NOT_STARTED))
         {
            break;
         }

		 if(reboot_in_progress) {
			 al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"MESH_AP:INFO: %s<%d> Terminating-----\n", __func__, __LINE__);
			 threads_status &= ~THREAD_STATUS_BIT5_MASK;
			 break;
		 }

         mesh_ng_fsm_process_events(AL_CONTEXT_SINGLE);
         mesh_ng_fsm_execute(AL_CONTEXT_SINGLE);
      } while (mesh_state != _MESH_STATE_RUNNING);

      if ((mesh_state == _MESH_STATE_STOPPPING) || (mesh_state == _MESH_STATE_NOT_STARTED))
      {
         break;
      }
   }

   al_reset_event(AL_CONTEXT mesh_thread_wait_event);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MESH_AP:FLOW: %s<%d> Exited\n", __func__, __LINE__);
   al_set_event(AL_CONTEXT mesh_thread_exit_wait_event);

   return RETURN_SUCCESS;
}


static int al_process_beacon(AL_CONTEXT_PARAM_DECL al_net_if_t *al_net_if, al_net_addr_t *bssid, unsigned char signal, unsigned char noise)
{
   return RETURN_SUCCESS;
}


static void _action_notify(AL_CONTEXT_PARAM_DECL al_net_if_t *al_net_if, al_net_addr_t *sender, unsigned char *buffer, int length)
{
   int ret;
   al_802_11_operations_t *ex_op;
   unsigned char          type;
   unsigned char          data[32];
   unsigned char          data_length;
   unsigned char          *p;
   int timeout;
   int tm;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Enter %s<%d>\n",__func__,__LINE__);
   if (mesh_state != _MESH_STATE_RUNNING)
   {
      return;
   }

   ex_op = (al_802_11_operations_t *)_DS_NET_IF->get_extended_operations(_DS_NET_IF);
   ret   = al_802_11_parse_md_action(AL_CONTEXT buffer, &type, data, &data_length);

   if (ret != 0)
   {
      return;
   }

   if (type == IMCP_SNIP_ACTION_TYPE_ENABLE_BUFFERING)
   {
      if ((al_net_if != _DS_NET_IF) || !AL_NET_IF_IS_WIRELESS(al_net_if) || (current_parent == NULL))
      {
         return;
      }

      if (memcmp(sender->bytes, current_parent->bssid.bytes, 6))
      {
         return;
      }

      p = data;

      if (*p != 0)                              /* we expect this command from parent only */
      {
         return;
      }

      p++;

      memcpy(&tm, p, 4);
      p += 4;

      timeout = al_le32_to_cpu(tm);

      if (timeout == 0)
      {
         al_set_event(AL_CONTEXT mesh_ds_buffer_stop_event);
         return;
      }

      if (current_parent->last_buffer_action_ctr != *p)
      {
         current_parent->last_buffer_action_ctr = *p;
         ret = mesh_ng_fsm_send_event(AL_CONTEXT MESH_NG_FSM_EVENT_UPLINK_BUFFER, (void *)(timeout * 3));

         if (ret == 0)
         {
            access_point_enable_thread(AL_CONTEXT 0);
            AL_ATOMIC_SET(_DS_NET_IF->buffering_state, AL_NET_IF_BUFFERING_STATE_REQUEUE);
            ex_op->enable_ds_verification_opertions(_DS_NET_IF, 0);
            al_set_event(AL_CONTEXT mesh_thread_wait_event);
         }
      }
   }
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Exit %s<%d>\n",__func__,__LINE__);
}


static void _round_robin_notify(AL_CONTEXT_PARAM_DECL al_net_if_t *al_net_if)
{
//   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Enter %s<%d>\n",__func__,__LINE__);
   if ((al_net_if == _DS_NET_IF) && (mesh_state == _MESH_STATE_RUNNING) && AL_NET_IF_IS_WIRELESS(al_net_if))
   {
      if (_IS_LFN && !_IS_MOBILE && !_IS_UPLINK_SCAN_ENABLED)
      {
         if (mesh_round_robin_lfn_state)
         {
            mesh_ng_fsm_send_event(AL_CONTEXT MESH_NG_FSM_EVENT_STEP_SCAN, (void *)15);
         }
         else
         {
            mesh_ng_fsm_send_event(AL_CONTEXT MESH_NG_FSM_EVENT_SAMPLE, NULL);
         }
         mesh_round_robin_lfn_state = !mesh_round_robin_lfn_state;
      }
      else
      {
         mesh_ng_fsm_send_event(AL_CONTEXT MESH_NG_FSM_EVENT_SAMPLE, NULL);
      }

      al_set_event(AL_CONTEXT mesh_thread_wait_event);
   }
 //  al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Exit %s<%d>\n",__func__,__LINE__);
}


static void _radar_notify(AL_CONTEXT_PARAM_DECL al_net_if_t *al_net_if, int channel)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Enter %s<%d>\n",__func__,__LINE__);
   if (al_net_if == _DS_NET_IF)
   {
      mesh_ng_fsm_send_event(AL_CONTEXT MESH_NG_FSM_EVENT_RADAR_SLAVE, (void *)al_net_if);
      al_set_event(AL_CONTEXT mesh_thread_wait_event);
   }
   else
   {
      mesh_ng_fsm_send_event(AL_CONTEXT MESH_NG_FSM_EVENT_RADAR_MASTER, (void *)al_net_if);
      al_set_event(AL_CONTEXT mesh_thread_wait_event);
   }
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Exit %s<%d>\n",__func__,__LINE__);
}


static void _enable_radar_detection(AL_CONTEXT_PARAM_DECL_SINGLE)
{
   int i;
   al_802_11_operations_t *ex_op;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Enter %s<%d>\n",__func__,__LINE__);
   if (AL_NET_IF_IS_WIRELESS(mesh_hardware_info->ds_net_if))
   {
      ex_op = mesh_hardware_info->ds_net_if->get_extended_operations(mesh_hardware_info->ds_net_if);
      ex_op->set_radar_notify_hook(mesh_hardware_info->ds_net_if, _radar_notify);
   }

   for (i = 0; i < mesh_hardware_info->wm_net_if_count; i++)
   {
      if (AL_NET_IF_IS_WIRELESS(_WMS_NET_IF(i)))
      {
         ex_op = _WMS_NET_IF(i)->get_extended_operations(_WMS_NET_IF(i));
         ex_op->set_radar_notify_hook(_WMS_NET_IF(i), _radar_notify);
      }
   }
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Exit %s<%d>\n",__func__,__LINE__);
}


static void _on_probe_request(AL_CONTEXT_PARAM_DECL al_net_if_t *al_net_if, al_net_addr_t *sta_addr, int rssi)
{
   if (!_PROBE_REQUEST_LOCATION_SUPPORT)
   {
      return;
   }

   mesh_location_process_info(AL_CONTEXT al_net_if, sta_addr, rssi);
}

static int access_point_on_start_handler2(AL_CONTEXT_PARAM_DECL access_point_config_t *config, access_point_dot11e_category_t *dot11e_config)
{
   al_802_11_operations_t *ex_op;
   al_notify_message_t    message;
   int         i;
   char        vendor_info[256];
   char        vendor_info_len;
   al_net_if_t *net_if;

     al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Enter %s<%d>\n",__func__,__LINE__);
	 AL_ASSERT("MESH_AP   : access_point_on_start_handler", config != NULL);
 
         AL_PRINT_LOG_MSG_0("MESH_AP	: Starting Mesh");
         message.message_type = AL_NOTIFY_MESSAGE_TYPE_SET_STATE;
         message.message_data = (void *)'Y';
         al_notify_message(AL_CONTEXT & message);

         mesh_round_robin_lfn_state = 0;

 
	     al_create_thread_on_cpu(AL_CONTEXT mesh_thread_function, NULL,0, "mesh_thread_function");
		 threads_status |= THREAD_STATUS_BIT5_MASK;
	     al_create_thread_on_cpu(AL_CONTEXT mesh_hb_thread_function, NULL,0, "mesh_hb_thread_function");
		 threads_status |= THREAD_STATUS_BIT6_MASK;
         /* creating thread for interrupt request (for switch over work)*/


         if ((_IS_UPLINK_SCAN_ENABLED) && (mesh_mode == _MESH_AP_RELAY))
         {
	        al_create_thread_on_cpu(AL_CONTEXT _uplink_scan_thread_function, NULL,0, "_uplink_scan_thread_function");
			threads_status |= THREAD_STATUS_BIT7_MASK;
         }
         else
         {
            if (_DISJOINT_ADHOC_MODE && (mesh_hardware_info->monitor_type == MONITOR_TYPE_ABSENT))
            {
	           al_create_thread_on_cpu(AL_CONTEXT _lfr_thread_function, NULL,0, "_lfr_thread_function");
			   threads_status |= THREAD_STATUS_BIT8_MASK;
            }
         }

         if (mesh_hardware_info->monitor_type != MONITOR_TYPE_ABSENT)
         {
            start_scan_monitor(AL_CONTEXT_SINGLE);
         }

         mesh_state = _MESH_STATE_RUNNING;

         /* Send Heartbeat and Hardware info to AccessViewer */
         mesh_imcp_send_packet_heartbeat(AL_CONTEXT mesh_heart_beat_sqnr);
         mesh_imcp_send_packet_heartbeat2(AL_CONTEXT mesh_heart_beat_sqnr);
         mesh_heart_beat_sqnr++;

         mesh_imcp_send_packet_hardware_info(AL_CONTEXT_SINGLE);

         /* send ap_start packet for IP assignment */

         al_notify_ds_net_if(AL_CONTEXT mesh_hardware_info->ds_net_if);

         AL_PRINT_LOG_MSG_0("MESH_AP	: Starting Mesh");
         message.message_type = AL_NOTIFY_MESSAGE_TYPE_SET_STATE;
         message.message_data = (void *)'G';
         al_notify_message(AL_CONTEXT & message);

         al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Exit %s<%d> Mesh State Changed New State = "
		 "_MESH_STATE_RUNNING\n",__func__,__LINE__);
}

static int access_point_on_start_handler(AL_CONTEXT_PARAM_DECL access_point_config_t *config, access_point_dot11e_category_t *dot11e_config)
{
   al_802_11_operations_t *ex_op;
   al_notify_message_t    message;
   int         i,j;
   char        vendor_info[256];
   char        vendor_info_len;
   al_net_if_t *net_if;
   mesh_conf_if_info_t *netif_conf; 

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Enter %s<%d>\n",__func__,__LINE__);
   AL_ASSERT("MESH_AP	: access_point_on_start_handler", config != NULL);

   if (mesh_flags)
   {
      if (mesh_state == _MESH_STATE_NOT_STARTED)
      {
         al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"MESH_AP:INFO:  %s<%d> Starting Mesh\n", __func__, __LINE__);
         message.message_type = AL_NOTIFY_MESSAGE_TYPE_SET_STATE;
         message.message_data = (void *)'Y';
         al_notify_message(AL_CONTEXT & message);

         mesh_round_robin_lfn_state = 0;

         if (mesh_on_start(AL_CONTEXT config, dot11e_config))
         {
            al_set_event(AL_CONTEXT mesh_start_stop_event);
            return RETURN_ERROR;
         }

         if (_DFS_REQUIRED)
         {
            _enable_radar_detection(AL_CONTEXT_SINGLE);
         }

         if (mesh_hardware_info->ds_net_if->encapsulation == AL_NET_IF_ENCAPSULATION_802_11)
         {
            ex_op = mesh_hardware_info->ds_net_if->get_extended_operations(mesh_hardware_info->ds_net_if);
            if (ex_op != NULL)
            {
               ex_op->set_beacon_hook(mesh_hardware_info->ds_net_if, al_process_beacon);
               ex_op->set_round_robin_notify_hook(_DS_NET_IF, _round_robin_notify);
               ex_op->set_action_hook(_DS_NET_IF, _action_notify);
            }
         }
         if (_PROBE_REQUEST_LOCATION_SUPPORT)
         {
            /**
             * Setup probe request hook on all downlinks
             */

            for (i = 0; i < mesh_hardware_info->wm_net_if_count; i++)
            {
               for (j = 0; j < mesh_config->if_count; j++)
               {
                  netif_conf = &mesh_config->if_info[j];
                  if ((netif_conf->use_type == AL_CONF_IF_USE_TYPE_AP) && !strcmp(globals.config.wm_net_if_info[i].net_if->name, netif_conf->name))       
                     continue;
               }  
               if (AL_NET_IF_IS_WIRELESS(_WMS_NET_IF(i)))
               {
                  ex_op = _WMS_NET_IF(i)->get_extended_operations(_WMS_NET_IF(i));
                  ex_op->set_probe_request_hook(_WMS_NET_IF(i), _on_probe_request);
               }
            }
         }
      }
      else
      {
         return RETURN_ERROR;
      }
   }
   else
   {
      mesh_on_start(AL_CONTEXT config, dot11e_config);
   }

   for (i = 0; i < mesh_hardware_info->wm_net_if_count; i++)
   {
      for (j = 0; j < mesh_config->if_count; j++)
      {
         netif_conf = &mesh_config->if_info[j];
         if ((netif_conf->use_type == AL_CONF_IF_USE_TYPE_AP) && !strcmp(globals.config.wm_net_if_info[i].net_if->name, netif_conf->name))       
            continue;
      }  
      net_if = mesh_hardware_info->wms_conf[i]->net_if;
      ex_op  = (al_802_11_operations_t *)net_if->get_extended_operations(net_if);
      if (ex_op != NULL)
      {
         if (mesh_hardware_info->wms_conf[i]->service != AL_CONF_IF_SERVICE_CLIENT_ONLY)
         {
            vendor_info_len = mesh_imcp_get_vendor_info_buffer(AL_CONTEXT vendor_info, 1);
            ex_op->set_beacon_vendor_info(net_if, vendor_info, vendor_info_len);

            al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,
                         "MESH_AP:INFO: %s<%d> Setting vendor information of length %d for %s",
                         __func__, __LINE__, vendor_info_len,
                         mesh_hardware_info->wms_conf[i]->net_if->name);
         }
         else
         {
            ex_op->set_beacon_vendor_info(net_if, NULL, 0);
         }
      }
   }
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Exit %s<%d>\n",__func__,__LINE__);

   return RETURN_SUCCESS;
}


static int access_point_on_stop_handler(AL_CONTEXT_PARAM_DECL access_point_config_t *config, access_point_dot11e_category_t *dot11e_config)
{
   int                    old_state;
   al_notify_message_t    message;
   al_802_11_operations_t *ex_op;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Enter %s<%d>\n",__func__,__LINE__);

   old_state  = mesh_state;
   mesh_state = _MESH_STATE_STOPPPING;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"MESH_AP:INFO: %s<%d> Mesh State Changed New State= "
   "_MESH_STATE_STOPPPING\n", __func__, __LINE__);
   message.message_type = AL_NOTIFY_MESSAGE_TYPE_SET_STATE;
   message.message_data = (void *)'Y';
   al_notify_message(AL_CONTEXT & message);

   if (old_state != _MESH_STATE_RUNNING)
   {
      /* if waiting for Handshake response or channel scan */
      al_set_event(AL_CONTEXT mesh_scan_wait_unlock_event);
      al_set_event(AL_CONTEXT mesh_scan_wait_objection_event);
      al_wait_on_event(AL_CONTEXT mesh_start_stop_event, AL_WAIT_TIMEOUT_INFINITE);
   }
   else
   {
      al_reset_event(AL_CONTEXT mesh_start_stop_event);
      /* stop sending heartbeats (kill HB thread) */
      al_set_event(AL_CONTEXT mesh_thread_wait_event);
      al_wait_on_event(AL_CONTEXT mesh_thread_exit_wait_event, AL_WAIT_TIMEOUT_INFINITE);
      if (mesh_hardware_info->monitor_type != MONITOR_TYPE_ABSENT)
      {
         al_set_event(AL_CONTEXT mesh_monitor_wait_event);
         al_wait_on_event(AL_CONTEXT mesh_scan_monitor_exit_wait_event, AL_WAIT_TIMEOUT_INFINITE);
      }
   }

   /* disassoc with parent */
   if (mesh_hardware_info->ds_net_if->encapsulation == AL_NET_IF_ENCAPSULATION_802_11)
   {
      ex_op = (al_802_11_operations_t *)mesh_hardware_info->ds_net_if->get_extended_operations(mesh_hardware_info->ds_net_if);
      if (ex_op)
      {
         ex_op->dis_associate(mesh_hardware_info->ds_net_if);
      }
   }


   current_parent       = NULL;
   scan_state           = _CHANNEL_SCAN_STATE_START;
   mesh_mode            = _MESH_AP_ROOT;
   mesh_heart_beat_sqnr = 0;

   if (mesh_hardware_info->wms_conf != NULL)
   {
      al_heap_free(AL_CONTEXT mesh_hardware_info->wms_conf);
   }

   al_reset_event(AL_CONTEXT mesh_start_stop_event);
   al_reset_event(AL_CONTEXT mesh_thread_wait_event);
   al_reset_event(AL_CONTEXT mesh_thread_exit_wait_event);
   al_reset_event(AL_CONTEXT mesh_monitor_wait_event);
   al_reset_event(AL_CONTEXT mesh_scan_monitor_exit_wait_event);
   al_reset_event(AL_CONTEXT mesh_scan_wait_unlock_event);
   al_reset_event(AL_CONTEXT mesh_scan_wait_objection_event);

   mesh_table_remove_all_entries(AL_CONTEXT_SINGLE);
   mesh_ng_clear_parents(AL_CONTEXT_SINGLE);

   mesh_state           = _MESH_STATE_NOT_STARTED;
   message.message_type = AL_NOTIFY_MESSAGE_TYPE_SET_STATE;
   message.message_data = (void *)'R';
   al_notify_message(AL_CONTEXT & message);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Exit %s<%d>\n",__func__,__LINE__);

   return RETURN_SUCCESS;
}

static int access_point_assoc_request_handler(AL_CONTEXT_PARAM_DECL al_net_if_t *net_if, al_net_addr_t *sta_addr, unsigned char *sta_supported_rates,
                                              unsigned char sta_supported_rates_length, unsigned short *status_code_out,
                                              unsigned char *supported_rates_out, unsigned char *supported_rates_length_out)
{
   int ret;

   AL_PRINT_LOG_FLOW_0("MESH_AP	: access_point_assoc_request_handler()");
   AL_ASSERT("MESH_AP	: access_point_assoc_request_handler", status_code_out != NULL);
   AL_ASSERT("MESH_AP	: access_point_assoc_request_handler", sta_addr != NULL);


   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Enter %s<%d>\n",__func__,__LINE__);
#ifdef __DUMP_IMCP_MESSAGE_MAC_ADDR
   _PRINT_HW_ADDRESS_DEBUG("MESH_AP	: AP STA ASSOC Request from : ", sta_addr);
#endif
   if (mesh_state != _MESH_STATE_RUNNING)
   {
      *status_code_out = AL_802_11_STATUS_SUCCESS;
      return RETURN_SUCCESS;
   }

   if (AL_NET_IF_IS_WIRELESS(_DS_NET_IF) &&
       AL_NET_ADDR_EQUAL(sta_addr, &current_parent->ds_mac))
   {
      /**
       * A loop has been detected, and hence we need to reboot to
       * break the loop
       */
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MESH_AP:INFO: %s<%d> Direct Loop detected ,disconnecting\n", __func__,
	  __LINE__);
	   mesh_network_unstable = 1;
      mesh_ng_fsm_send_event(AL_CONTEXT MESH_NG_FSM_EVENT_DISCONNECT, NULL);
      al_set_event(AL_CONTEXT mesh_thread_wait_event);
   }

   if (CHECK_MESH_FLAG(MESH_FLAG_DENY_CLIENTS))
   {
      *status_code_out = AL_802_11_STATUS_AP_UNABLE_TO_HANDLE_NEW_STA;
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP:ERROR: %s<%d> DENY CLIENTS flag set\n",__func__,__LINE__);
      return RETURN_ERROR;
   }

   /* check if sta_addr is in scanned list */
   mesh_ng_set_parent_as_child(AL_CONTEXT sta_addr, 1, 1);

#ifdef MESH_AP_BUS_SPEED_TEST
   if (!AL_NET_IF_IS_ETHERNET(_DS_NET_IF) || !AL_NET_IF_IS_ETHERNET(net_if))
#endif
   {
      ret = mesh_table_entry_add_lock(AL_CONTEXT net_if, sta_addr, NULL, NULL, 1);
      if (ret)
      {
        al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP:ERROR: %s<%d> table entry not added for:"MACSTR"\n",
		__func__,__LINE__, MAC2STR(sta_addr->bytes));
         return RETURN_ERROR;
      }

      *status_code_out = AL_802_11_STATUS_SUCCESS;

      mesh_imcp_send_packet_sta_assoc_notification(AL_CONTEXT sta_addr, &net_if->config.hw_addr);
   }

   mesh_plugin_post_event(MESH_PLUGIN_EVENT_STA_ASSOCIATED, sta_addr);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Exit %s<%d>\n",__func__,__LINE__);

   return RETURN_SUCCESS;
}


void access_point_on_deauth_handler(AL_CONTEXT_PARAM_DECL al_net_if_t *net_if, al_net_addr_t *sta_addr, unsigned short reason_code)
{
   AL_PRINT_LOG_FLOW_0("MESH_AP	: access_point_on_deauth_handler()");
   AL_ASSERT("MESH_AP	: access_point_on_deauth_handler", net_if != NULL);
   AL_ASSERT("MESH_AP	: access_point_on_deauth_handler", sta_addr != NULL);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Enter %s<%d>\n",__func__,__LINE__);
#ifdef __DUMP_IMCP_MESSAGE_MAC_ADDR
   _PRINT_HW_ADDRESS_DEBUG("MESH_AP	: AP STA DEAUTH Request from : ", sta_addr);
#endif
   if (mesh_state != _MESH_STATE_RUNNING)
   {
      return;
   }
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Exit %s<%d>\n",__func__,__LINE__);
}


void access_point_on_disassoc_handler(AL_CONTEXT_PARAM_DECL al_net_if_t *net_if, al_net_addr_t *sta_addr, unsigned short reason_code)
{
   unsigned int flags;

   AL_ASSERT("MESH_AP	: access_point_on_disassoc_handler", net_if != NULL);
   AL_ASSERT("MESH_AP	: access_point_on_disassoc_handler", sta_addr != NULL);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Enter %s<%d>\n",__func__,__LINE__);

   if (mesh_state != _MESH_STATE_RUNNING)
   {
      return;
   }

   if (net_if == mesh_hardware_info->ds_net_if)
   {
       al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"MESH_AP:INFO: %s<%d> AP STA DISASSOC Notification from:"MACSTR"\n",
	   __func__,__LINE__, MAC2STR(sta_addr->bytes));
   }
   else
   {
#ifdef __DUMP_IMCP_MESSAGE_MAC_ADDR
       al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"MESH_AP:INFO: %s<%d> AP STA DISASSOC Notification from:"MACSTR"\n",
	   __func__,__LINE__, MAC2STR(sta_addr->bytes));
#endif

      /* check if sta_addr is in scanned list */
      mesh_ng_set_parent_as_child(AL_CONTEXT sta_addr, 0, 0);

      mesh_table_entry_remove_lock(AL_CONTEXT sta_addr, 1);
      mesh_imcp_send_packet_sta_disassoc_notification(AL_CONTEXT sta_addr, reason_code);
   }
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Exit %s<%d>\n",__func__,__LINE__);
}


static int access_point_on_probe_request_handler(AL_CONTEXT_PARAM_DECL al_net_if_t *net_if, al_net_addr_t *sta_addr, unsigned char *sta_supported_rates,
                                                 unsigned char sta_supported_rates_length, unsigned char *supported_rates_out,
                                                 unsigned char *supported_rates_length_out)
{
   AL_PRINT_LOG_FLOW_0("MESH_AP	: access_point_on_probe_request_handler()");
   AL_ASSERT("MESH_AP	: access_point_on_probe_request_handler", net_if != NULL);
   AL_ASSERT("MESH_AP	: access_point_on_probe_request_handler", sta_addr != NULL);

#ifdef __DUMP_IMCP_MESSAGE_MAC_ADDR
   _PRINT_HW_ADDRESS_DEBUG("MESH_AP	: AP STA PROBE Request from : ", sta_addr);
#endif

   return RETURN_SUCCESS;
}


static int access_point_on_imcp_packet_handler(AL_CONTEXT_PARAM_DECL al_net_if_t *net_if, al_packet_t *packet)
{
   AL_ASSERT("MESH_AP	: access_point_on_imcp_packet_handler", net_if != NULL);
   AL_ASSERT("MESH_AP	: access_point_on_imcp_packet_handler", packet != NULL);

#ifdef __DUMP_IMCP_MESSAGE_MAC_ADDR
   _PRINT_HW_ADDRESS_DEBUG("MESH_AP	: AP IMCP Request from net if =: ", (&net_if->config.hw_addr));
#endif

   return imcp_snip_receive(AL_CONTEXT net_if, packet);
}


static int
access_point_on_data_reception_handler(AL_CONTEXT_PARAM_DECL al_net_if_t *net_if,
                                       unsigned int                      rssi,
                                       al_net_addr_t                     *immediate_src_addr_in,
                                       al_net_addr_t                     *src_addr_in,
                                       al_net_addr_t                     *dest_addr_in,
                                       unsigned short                    *frame_control_out,
                                       al_net_addr_t                     *immediate_dest_addr_out,
                                       al_net_if_t                       **net_if_out)
{
   mesh_table_entry_t *entry = NULL;
   unsigned int       flags;

   AL_ASSERT("MESH_AP	: access_point_on_data_reception_handler", net_if != NULL);
   AL_ASSERT("MESH_AP	: access_point_on_data_reception_handler", src_addr_in != NULL);
   AL_ASSERT("MESH_AP	: access_point_on_data_reception_handler", dest_addr_in != NULL);
   AL_ASSERT("MESH_AP	: access_point_on_data_reception_handler", immediate_dest_addr_out != NULL);
   AL_ASSERT("MESH_AP	: access_point_on_data_reception_handler", net_if_out != NULL);
   AL_ASSERT("MESH_AP	: access_point_on_data_reception_handler", frame_control_out != NULL);

   /* check if src packet came from DS */
   if (_DS_NET_IF == net_if)
   {
      entry = mesh_table_entry_find(AL_CONTEXT src_addr_in);
      if (entry != NULL)
      {
         mesh_ng_set_parent_as_child(AL_CONTEXT src_addr_in, 0, 0);
		 mesh_table_entry_release(AL_CONTEXT entry);
         mesh_table_entry_remove_lock(AL_CONTEXT src_addr_in, 2);
         mesh_imcp_send_packet_sta_disassoc_notification(AL_CONTEXT src_addr_in, AL_802_11_REASON_DISASSOC_STA_HAS_LEFT);
      }

      if (AL_NET_IF_IS_WIRELESS(net_if) && !_IS_MOBILE)
      {
         current_parent->signal = ((current_parent->signal * 3) + rssi) / 4;
      }
   }

   entry = mesh_table_entry_find(AL_CONTEXT dest_addr_in);

   if (entry == NULL)
   {
      /* entry not found */
      if (net_if == _DS_NET_IF)
      {
         /* packet came from ds so drop it*/
         return ACCESS_POINT_PACKET_DECISION_DROP;
      }
      else
      {
         if (mesh_mode == _MESH_AP_RELAY)
         {
            /* set fromDS & toDS to 1 as packet going down */
            *frame_control_out = AL_802_11_FC_FROMDS | AL_802_11_FC_TODS;
         }
         *net_if_out = _DS_NET_IF;
         return ACCESS_POINT_PACKET_DECISION_SEND_DS;
      }
   }

   /* set fromDS & toDS to 1 as packet going down */
   *frame_control_out = AL_802_11_FC_FROMDS | AL_802_11_FC_TODS;

   /* set immediagte det_addr */
   memcpy(immediate_dest_addr_out, &entry->relay_ap_addr, sizeof(al_net_addr_t));

   /* set net if where to send the packet */
   *net_if_out = entry->net_if;

    mesh_table_entry_release(AL_CONTEXT entry);
   return ACCESS_POINT_PACKET_DECISION_SEND_WM;
}


static int access_point_on_before_transmit_handler(AL_CONTEXT_PARAM_DECL al_net_addr_t *dest_addr_in, unsigned short *frame_control_out,
                                                   al_net_addr_t *immediate_dest_addr_out, al_net_if_t **net_if_out)
{
   mesh_table_entry_t *entry = NULL;

   AL_PRINT_LOG_FLOW_0("MESH_AP	: access_point_on_before_transmit_handler()");
   AL_ASSERT("MESH_AP	: access_point_on_before_transmit_handler", dest_addr_in != NULL);
   AL_ASSERT("MESH_AP	: access_point_on_before_transmit_handler", immediate_dest_addr_out != NULL);
   AL_ASSERT("MESH_AP	: access_point_on_before_transmit_handler", net_if_out != NULL);
   AL_ASSERT("MESH_AP	: access_point_on_before_transmit_handler", frame_control_out != NULL);

   entry = mesh_table_entry_find(AL_CONTEXT dest_addr_in);
   if (entry == NULL)
   {
      /* entry not found send to ds */
      if (mesh_mode == _MESH_AP_RELAY)
      {
         /* set fromDS & toDS to 1 as packet going down */
         *frame_control_out = AL_802_11_FC_FROMDS | AL_802_11_FC_TODS;
      }
      *net_if_out = _DS_NET_IF;
      //mesh_table_entry_release(entry);
      return ACCESS_POINT_PACKET_DECISION_SEND_DS;
   }

   if (mesh_mode == _MESH_AP_RELAY)
   {
      /* set fromDS & toDS to 1 as packet going down */
      *frame_control_out = AL_802_11_FC_FROMDS | AL_802_11_FC_TODS;
   }

   /* set immediagte det_addr */
   memcpy(immediate_dest_addr_out, &entry->relay_ap_addr, sizeof(al_net_addr_t));

   /* set net if where to send the packet */
   *net_if_out = entry->net_if;

   mesh_table_entry_release(AL_CONTEXT entry);
   return ACCESS_POINT_PACKET_DECISION_SEND_WM;
}


static int _ethernet_link_thread(AL_CONTEXT_PARAM_DECL al_thread_param_t *param)
{
   int flags;

   al_set_thread_name(AL_CONTEXT "eth_link");

   al_thread_sleep(AL_CONTEXT 900 * 1000);

   flags = _DS_NET_IF->get_flags(_DS_NET_IF);


   if (!(flags & AL_NET_IF_FLAGS_LINKED))
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MESH_AP: on_phy_link_notify rebooting to change DS");

      al_reboot_board(AL_CONTEXT AL_REBOOT_BOARD_CODE_ETHERNET_LINK_DOWN);
   }

   return RETURN_SUCCESS;
}


static int _wireless_ds_on_phy_link_notify(AL_CONTEXT_PARAM_DECL al_net_if_t *al_net_if, int link_state)
{
   int i;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Enter %s<%d>\n",__func__,__LINE__);
   /**
    * If the net_if is not ethernet, we ignore
    */
   if (!AL_NET_IF_IS_ETHERNET(al_net_if))
   {
      return RETURN_SUCCESS;
   }

   /**
    * If the ethernet is not linked, we ignore
    */
   if (link_state == 0)
   {
      return RETURN_SUCCESS;
   }

   /**
    * Check if this net_if's usage type is DS
    */
   for (i = 0; i < mesh_config->if_count; i++)
   {
      if (!strcmp(mesh_config->if_info[i].name, al_net_if->name) &&
          (mesh_config->if_info[i].use_type == AL_CONF_IF_USE_TYPE_DS))
      {
         /**
          * This is an Linked ethernet net_if that is declared as a DS.
          * We are a relay node. Hence we simply reboot and try to be
          * a ROOT
          */
         interrupt_cnt++;

         al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MESH_AP:INFO: %s<%d> on_phy_link_notify rebooting to change DS\n",
		 __func__, __LINE__);
      }
   }
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MESH_AP:FLOW: Exit %s<%d> on_phy_link_notify %s interface ignored\n",
   __func__, __LINE__, al_net_if->name);
   return RETURN_SUCCESS;
}


static int _ethernet_link_monitor(AL_CONTEXT_PARAM_DECL al_net_if_t *al_net_if, int link_state)
{
   al_create_thread_on_cpu(AL_CONTEXT _ethernet_link_thread, NULL,0, "_ethernet_link_thread");
   return RETURN_SUCCESS;
}


static void _set_mesh_init_status_essid_tmp(al_net_if_t *net_if, al_802_11_operations_t *ex_op)
{
   char       essid[AL_802_11_ESSID_MAX_SIZE + 1];
   int        phy_mode;
   const char *phy_mode_freq;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Enter %s<%d>\n",__func__,__LINE__);
   phy_mode = ex_op->get_phy_mode(net_if);

   switch (phy_mode)
   {
   case AL_802_11_PHY_MODE_A:
      phy_mode_freq = "A";
      break;

   case AL_802_11_PHY_MODE_B:
      phy_mode_freq = "B";
      break;

   case AL_802_11_PHY_MODE_G:
      phy_mode_freq = "G";
      break;

   case AL_802_11_PHY_MODE_BG:
      phy_mode_freq = "BG";
      break;

   case AL_802_11_PHY_MODE_PSQ:
      phy_mode_freq = "PS5";
      break;

   case AL_802_11_PHY_MODE_PSH:
      phy_mode_freq = "PS10";
      break;

   case AL_802_11_PHY_MODE_PSF:
      phy_mode_freq = "PS20";
      break;

   default:
      phy_mode_freq = "UN";
   }

   sprintf(essid, MESH_INIT_STATUS_SSID, phy_mode_freq, use_mac_address[3], use_mac_address[4], use_mac_address[5]);

   ex_op->set_essid(net_if, essid);
   /* Do not hide SSID when in MESH_INIT_STATUS */
   ex_op->set_hide_ssid(net_if, 0);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Exit %s<%d>\n",__func__,__LINE__);
}

static int check_alternate_ds_iface(AL_CONTEXT_PARAM_DECL_SINGLE)
{
	int flags;
	int i;
	mesh_conf_if_info_t    *netif_conf;
	al_net_if_t            *net_if;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Enter %s<%d>\n",__func__,__LINE__);
	for (i = 0; i < mesh_config->if_count; i++)
	{
		netif_conf = &mesh_config->if_info[i];

		if ((netif_conf->use_type != AL_CONF_IF_USE_TYPE_DS) || (netif_conf->net_if == NULL))
		{
			continue;
		}

		net_if = netif_conf->net_if;

		if (AL_NET_IF_IS_ETHERNET(net_if))
		{
			flags = net_if->get_flags(net_if);

			if ((flags & AL_NET_IF_FLAGS_LINKED) || (_FORCED_ROOT_MODE))
			{
				al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Exit %s<%d>\n",__func__,__LINE__);
				return RETURN_SUCCESS;
			}
		}
		else if (AL_NET_IF_IS_WIRELESS(net_if))
		{
			al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Exit %s<%d>\n",__func__,__LINE__);
			return RETURN_SUCCESS;
		}
	}

	al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP:ERROR: %s<%d> DS not found\n",__func__,__LINE__);
	return RETURN_ERROR_NO_DS_FOUND;
}

int al_on_phy_link_notify(AL_CONTEXT_PARAM_DECL al_net_if_t *al_net_if, int link_state)
{
   AL_ASSERT("MESH_AP	: al_on_phy_link_notify", al_net_if != NULL);
   int                    i;
   al_net_if_t            *net_if;
   al_802_11_operations_t *ex_op;
   char                   vendor_info[256];
   char                   vendor_info_len;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Enter %s<%d>\n",__func__,__LINE__);
   if (al_net_if == _DS_NET_IF)
   {
		if (((mesh_state == _MESH_STATE_RUNNING) || (mesh_state == _MESH_STATE_SEEKING_NEW_PARENT)) && (link_state == 0))
      {
         al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MESH_AP: on_phy_link_notify (%s,%d)", al_net_if->name, link_state);
         /* delete parent if 802.11 */
         if (AL_NET_IF_IS_WIRELESS(al_net_if))
         {
            /**
             * Set the DS NET_IF buffering state to Drop. This ensures even if AP code transmits,
             * the al_net_if_impl will drop it
             */
            if (_DISJOINT_ADHOC_MODE)
            {
               func_mode = _FUNC_MODE_LFN;
			      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"MESH_AP:INFO: %s<%d> Setting LFN ....\n",__func__,__LINE__);
            }
            else
            {
			   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"MESH_AP:INFO: %s<%d> Setting FFN ....\n",__func__,__LINE__);
               func_mode = _FUNC_MODE_FFN;
            }

            AL_ATOMIC_SET(_DS_NET_IF->buffering_state, AL_NET_IF_BUFFERING_STATE_DROP);
            mesh_ng_fsm_send_event(AL_CONTEXT MESH_NG_FSM_EVENT_DISCONNECT, NULL);

            al_set_event(AL_CONTEXT mesh_thread_wait_event);

            /**
             * The disconnect event will set the FSM state to ALT SCAN. ALT SCAN RE-ENABLES the
             * NET IF after disabling AP processing
             */
         }
         else
         {
            if (_DISJOINT_ADHOC_MODE)
            {
               func_mode = _FUNC_MODE_LFN;
			   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"MESH_AP:INFO: %s<%d> Setting LFN ....\n",__func__,__LINE__);
            }
            else
            {
               func_mode = _FUNC_MODE_FFN;
            }

            AL_ATOMIC_SET(_DS_NET_IF->buffering_state, AL_NET_IF_BUFFERING_STATE_DROP);

				if (mesh_config->hope_cost == 0)
				{
					if (check_alternate_ds_iface() == RETURN_ERROR_NO_DS_FOUND)
					{
						/* This will hit when all wireless interfaces are wms and ethernet DS interface go down
						 * Wait until ethernet DS interface available.
						 */
						enable_disable_processing(0);
						prescan_setup_wms(AL_CONTEXT 1);
						return RETURN_SUCCESS;
					}
				}

            purge_imcp_sta = 1;
            mesh_ng_fsm_send_event(AL_CONTEXT MESH_NG_FSM_EVENT_DISCONNECT, NULL);
            al_set_event(AL_CONTEXT mesh_thread_wait_event);
         }
      }
      else
      {
         return _wireless_ds_on_phy_link_notify(AL_CONTEXT al_net_if, link_state);
      }
   }
   else
   {
#ifdef FIPS_1402_COMPLIANT
      if (_FIPS_ENABLED && (link_state == 1))
      {
         al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MESH_AP:INFO: %s<%d> on_phy_link_notify (%s,%d)"
		 "disabling interface in FIPS140-2 mode.\n", __func__, __LINE__, al_net_if->name, link_state);
         AL_ATOMIC_SET(al_net_if->buffering_state, AL_NET_IF_BUFFERING_STATE_DROP);
      }
#endif

      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MESH_AP:INFO: %s<%d>on_phy_link_notify (%s,%d)\n",
	  __func__, __LINE__, al_net_if->name, link_state);

      /**
       * If the current DS is not wireless, we ignore
       */
      return _wireless_ds_on_phy_link_notify(AL_CONTEXT al_net_if, link_state);

   }
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Exit %s<%d>\n",__func__,__LINE__);
   return RETURN_SUCCESS;
}


int switch_over_2other_ds()
{
   int flags;

   unsigned int temp1;
   unsigned int temp2;
   char vendor_info[256];
   char vendor_info_len;
   int i = 0,j;
   mesh_conf_if_info_t *netif_conf;


   access_point_config_t *config;

   config = &globals.config;

   al_net_if_t *net_if;

   al_802_11_operations_t *ex_op;



   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Enter %s<%d>\n",__func__,__LINE__);
   while (1)
   {
      if (interrupt_cnt)
      {
         if (AL_NET_IF_IS_WIRELESS(mesh_hardware_info->ds_net_if))
         {
            ex_op = (al_802_11_operations_t *)mesh_hardware_info->ds_net_if->get_extended_operations(mesh_hardware_info->ds_net_if);
            ex_op->send_disassoc_from_station(mesh_hardware_info->ds_net_if, 1);
            ex_op->set_reboot_in_progress_state(mesh_hardware_info->ds_net_if, 1);
         }
         enable_disable_processing(0);

		 reboot_in_progress = 1;

		 /*Set events to terminate threads*/
		 if(globals.monitor_exit_handle)
			 al_set_event(AL_CONTEXT globals.monitor_exit_handle);
		 if(_queue_handle)
			 al_set_event(AL_CONTEXT _queue_handle);
		 if(mesh_thread_wait_event)
			 al_set_event(AL_CONTEXT mesh_thread_wait_event);
		 if(mesh_hb_wait_event)
			 al_set_event(AL_CONTEXT mesh_hb_wait_event);
		 if(globals.queue_event_handle)
			 al_set_event(AL_CONTEXT globals.queue_event_handle);
		 if(globals.tx_queue_event_handle)
			 al_set_event(AL_CONTEXT globals.tx_queue_event_handle);
		 if(globals.imcp_queue_event_handle)
			 al_set_event(AL_CONTEXT globals.imcp_queue_event_handle);
		 if(_cleanup_thread_exit_event)
			 al_set_event(AL_CONTEXT _cleanup_thread_exit_event);
		 terminate_dBmTestRx_thread();
		 terminate_dBmTestTx_thread();

		 /*wait before rebooting either all thread get terminate or MAX of 30 sec*/
		 while (threads_status && i < 6) {
			 al_thread_sleep(AL_CONTEXT 5000);
			 i++;
		 }

		 printk(KERN_EMERG "REBOOTING BOARD DUE TO LINK UP\n");
         al_reboot_board(AL_CONTEXT AL_REBOOT_BOARD_CODE_ETHERNET_LINK_UP);
         enable_disable_processing(AL_CONTEXT 1);
         mesh_ng_clear_parents();
         mesh_on_start_find_ds();

         /* update ds interface parameters */
         config->ds_net_if_info.net_if      = mesh_hardware_info->ds_net_if;
         config->ds_net_if_info.ack_timeout = mesh_hardware_info->ds_if_ack_timeout;
         config->ds_net_if_info.txpower     = mesh_hardware_info->ds_if_tx_power;
         config->ds_net_if_info.txrate      = mesh_hardware_info->ds_if_tx_rate;

         strcpy(config->ds_net_if_info.essid, mesh_hardware_info->ds_conf->essid);

         if (mesh_on_start_initialize_ds_interface1(AL_CONTEXT_SINGLE) == RETURN_SUCCESS)
         {
            for (i = 0; i < mesh_hardware_info->wm_net_if_count; i++)
            {
               for (j = 0; j < mesh_config->if_count; j++)
               {
                  netif_conf = &mesh_config->if_info[j];
                  if ((netif_conf->use_type == AL_CONF_IF_USE_TYPE_AP) && !strcmp(globals.config.wm_net_if_info[i].net_if->name, netif_conf->name))       
                     continue;
               }  
               net_if = mesh_hardware_info->wms_conf[i]->net_if;
               ex_op  = (al_802_11_operations_t *)net_if->get_extended_operations(net_if);
               if (ex_op != NULL)
               {
                  if (mesh_hardware_info->wms_conf[i]->service != AL_CONF_IF_SERVICE_CLIENT_ONLY)
                  {
                     vendor_info_len = mesh_imcp_get_vendor_info_buffer(AL_CONTEXT vendor_info, 1);
                     ex_op->set_beacon_vendor_info(net_if, vendor_info, vendor_info_len);

                     al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,
                                  "MESH_AP:INFO: %s<%d> Setting vendor information of length %d for %s\n",
                                  __func__, __LINE__, vendor_info_len,
                                  mesh_hardware_info->wms_conf[i]->net_if->name);
                  }
                  else
                  {
                     ex_op->set_beacon_vendor_info(net_if, NULL, 0);
                  }
               }
            }

            postscan_setup_wms();
         }
         else
         {
            if (_DISJOINT_ADHOC_MODE)
            {
               mesh_mode      = _MESH_AP_RELAY;
               func_mode      = _FUNC_MODE_LFR;
               current_parent = &dummy_lfr_parent;
               for (i = 0; i < mesh_hardware_info->wm_net_if_count; i++)
               {
                  net_if = mesh_hardware_info->wms_conf[i]->net_if;
                  ex_op  = (al_802_11_operations_t *)net_if->get_extended_operations(net_if);
                  if (ex_op != NULL)
                  {
                     if (mesh_hardware_info->wms_conf[i]->service != AL_CONF_IF_SERVICE_CLIENT_ONLY)
                     {
                        vendor_info_len = mesh_imcp_get_vendor_info_buffer(AL_CONTEXT vendor_info, 1);
                        ex_op->set_beacon_vendor_info(net_if, vendor_info, vendor_info_len);

                        al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,
                                     "MESH_AP:INFO: %s<%d> Setting vendor information of length %d for %s\n",
                                     __func__, __LINE__, vendor_info_len,
                                     mesh_hardware_info->wms_conf[i]->net_if->name);
                     }
                     else
                     {
                        ex_op->set_beacon_vendor_info(net_if, NULL, 0);
                     }
                  }
               }

            }
            else
            {
            }
         }


         enable_disable_processing(AL_CONTEXT 1);
         interrupt_cnt--;
      }
      if (!interrupt_cnt)
      {
         al_thread_sleep(AL_CONTEXT 1000);
      }
   }
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Exit %s<%d>\n",__func__,__LINE__);
}

void mesh_ap_initialize(AL_CONTEXT_PARAM_DECL_SINGLE)
{
   mesh_hardware_info = (mesh_hardware_config_t *)al_heap_alloc(AL_CONTEXT sizeof(mesh_hardware_config_t)AL_HEAP_DEBUG_PARAM);
   AL_ASSERT("MESH_AP	: mesh_ap_initialize", mesh_hardware_info != NULL);
   memset(mesh_hardware_info, 0, sizeof(mesh_hardware_config_t));
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Enter %s<%d>\n",__func__,__LINE__);

   if (mesh_flags)
   {
      mesh_thread_wait_event            = al_create_event(AL_CONTEXT    1);                             /* Manual reset event */
      mesh_hb_wait_event                = al_create_event(AL_CONTEXT    0);                             /* Auto reset event */
      mesh_monitor_wait_event           = al_create_event(AL_CONTEXT    0);
      mesh_scan_monitor_exit_wait_event = al_create_event(AL_CONTEXT    0);
      mesh_start_stop_event             = al_create_event(AL_CONTEXT 0);                        /* Auto Reset event */
      mesh_thread_exit_wait_event       = al_create_event(AL_CONTEXT 1);                        /* Manual Reset event */
      mesh_scan_wait_unlock_event       = al_create_event(AL_CONTEXT 0);                        /* Auto Reset event */
      mesh_scan_wait_objection_event    = al_create_event(AL_CONTEXT 0);                        /* Auto Reset event */
      mesh_ds_buffer_ack_event          = al_create_event(AL_CONTEXT 1);                        /* Manual Reset event */
      mesh_ds_buffer_stop_event         = al_create_event(AL_CONTEXT 1);                        /* Manual Reset event */

      scan_state = _CHANNEL_SCAN_STATE_START;
      al_set_event(AL_CONTEXT mesh_thread_exit_wait_event);
      access_point_set_assoc_request_handler(AL_CONTEXT access_point_assoc_request_handler);
      access_point_set_disassoc_handler(AL_CONTEXT access_point_on_disassoc_handler);
      access_point_set_probe_request_handler(AL_CONTEXT access_point_on_probe_request_handler);
      access_point_set_data_reception_handler(AL_CONTEXT access_point_on_data_reception_handler);
      access_point_set_before_transmit_handler(AL_CONTEXT access_point_on_before_transmit_handler);
      access_point_set_imcp_handler(AL_CONTEXT access_point_on_imcp_packet_handler);
      al_set_on_phy_link_notify_hook(AL_CONTEXT al_on_phy_link_notify);
   }

   /**
    * Register handlers with access point
    */
   access_point_set_start_handler(AL_CONTEXT access_point_on_start_handler);
   access_point_set_start_handler2(AL_CONTEXT access_point_on_start_handler2);
   access_point_set_stop_handler(AL_CONTEXT access_point_on_stop_handler);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Exit %s<%d>\n",__func__,__LINE__);
}


void mesh_ap_uninitialize(AL_CONTEXT_PARAM_DECL_SINGLE)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Enter %s<%d>\n",__func__,__LINE__);

   AL_ASSERT("MESH_AP	: mesh_ap_uninitialize", mesh_hardware_info != NULL);

   al_heap_free(AL_CONTEXT mesh_hardware_info);
   mesh_hardware_info = NULL;

   if (mesh_flags)
   {
      al_destroy_event(AL_CONTEXT mesh_start_stop_event);
      al_destroy_event(AL_CONTEXT mesh_thread_wait_event);
      al_destroy_event(AL_CONTEXT mesh_hb_wait_event);
      al_destroy_event(AL_CONTEXT mesh_thread_exit_wait_event);
      al_destroy_event(AL_CONTEXT mesh_monitor_wait_event);
      al_destroy_event(AL_CONTEXT mesh_scan_monitor_exit_wait_event);
      al_destroy_event(AL_CONTEXT mesh_scan_wait_unlock_event);
      al_destroy_event(AL_CONTEXT mesh_scan_wait_objection_event);
      al_destroy_event(AL_CONTEXT mesh_ds_buffer_ack_event);
      al_destroy_event(AL_CONTEXT mesh_ds_buffer_stop_event);

      access_point_set_auth_request_handler(AL_CONTEXT NULL);
      access_point_set_assoc_request_handler(AL_CONTEXT NULL);
      access_point_set_deauth_handler(AL_CONTEXT NULL);
      access_point_set_disassoc_handler(AL_CONTEXT NULL);
      access_point_set_probe_request_handler(AL_CONTEXT NULL);
      access_point_set_data_reception_handler(AL_CONTEXT NULL);
      access_point_set_before_transmit_handler(AL_CONTEXT NULL);
      al_set_on_phy_link_notify_hook(AL_CONTEXT NULL);
   }
   access_point_set_start_handler(AL_CONTEXT NULL);
   access_point_set_stop_handler(AL_CONTEXT NULL);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Exit %s<%d>\n",__func__,__LINE__);
}


int mesh_ap_is_table_entry_present(AL_CONTEXT_PARAM_DECL al_net_addr_t *addr)
{
   mesh_table_entry_t *entry = NULL;

   entry = mesh_table_entry_find(AL_CONTEXT addr);
   mesh_table_entry_release(AL_CONTEXT entry);

   return (entry == NULL) ? -1 : 0;
}
