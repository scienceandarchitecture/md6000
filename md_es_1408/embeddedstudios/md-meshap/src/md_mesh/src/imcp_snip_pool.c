/********************************************************************************
* MeshDynamics
* --------------
* File     : imcp_snip_pool.c
* Comments : IMCP Packet Pool implementation
* Created  : 4/13/2006
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  2  |11/16/2006| Added Buffer pool functions                     | Sriram |
* -----------------------------------------------------------------------------
* |  1  |11/14/2006| Changes for heap debug support                  | Sriram |
* -----------------------------------------------------------------------------
* |  0  |4/13/2006 | Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

#define _IMCP_POOL_MAX          5

#define _IMCP_POOL_TYPE_POOL    1
#define _IMCP_POOL_TYPE_HEAP    2

struct _imcp_pool
{
   imcp_packet_info_t packet;
   int                type;
   struct _imcp_pool  *next_pool;
};
typedef struct _imcp_pool   _imcp_pool_t;

al_spinlock_t imcp_splock;
AL_DEFINE_PER_CPU(int, imcp_spvar);

AL_DECLARE_GLOBAL(_imcp_pool_t * _imcp_pool);
AL_DECLARE_GLOBAL(_imcp_pool_t * _imcp_pool_head);

void imcp_snip_pool_initialize(AL_CONTEXT_PARAM_DECL_SINGLE)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
   int          i;
   _imcp_pool_t *temp;

   temp            = NULL;
   _imcp_pool_head = NULL;

   al_spin_lock_init(&imcp_splock);

   _imcp_pool = (_imcp_pool_t *)al_heap_alloc(AL_CONTEXT sizeof(_imcp_pool_t) * _IMCP_POOL_MAX AL_HEAP_DEBUG_PARAM);

   for (i = 0; i < _IMCP_POOL_MAX; i++)
   {
      _imcp_pool[i].next_pool = NULL;
      _imcp_pool[i].type      = _IMCP_POOL_TYPE_POOL;

      if (_imcp_pool_head == NULL)
      {
         _imcp_pool_head = &_imcp_pool[i];
      }
      else
      {
         temp->next_pool = &_imcp_pool[i];
      }

      temp = &_imcp_pool[i];
   }
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
}


void imcp_snip_pool_uninitialize(AL_CONTEXT_PARAM_DECL_SINGLE)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
   al_heap_free(AL_CONTEXT _imcp_pool);
   _imcp_pool_head = NULL;
   _imcp_pool      = NULL;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
}


imcp_packet_info_t *imcp_snip_create_packet(AL_CONTEXT_PARAM_DECL_SINGLE)
{
   _imcp_pool_t  *temp;
   unsigned long flags;

   temp = NULL;

   al_spin_lock(imcp_splock, imcp_spvar);

   if (_imcp_pool_head != NULL)
   {
      temp            = _imcp_pool_head;
      _imcp_pool_head = _imcp_pool_head->next_pool;
   }

   al_spin_unlock(imcp_splock, imcp_spvar);

   if (temp == NULL)
   {
      temp       = (_imcp_pool_t *)al_heap_alloc(AL_CONTEXT sizeof(_imcp_pool_t)AL_HEAP_DEBUG_PARAM);
      temp->type = _IMCP_POOL_TYPE_HEAP;
   }

   memset(&temp->packet, 0, sizeof(imcp_packet_info_t));

   return &temp->packet;
}


void imcp_snip_release_packet(AL_CONTEXT_PARAM_DECL imcp_packet_info_t *packet)
{
   unsigned long flags;
   _imcp_pool_t  *temp;

   temp = (_imcp_pool_t *)packet;

   if (temp->type == _IMCP_POOL_TYPE_HEAP)
   {
      al_heap_free(AL_CONTEXT temp);
      return;
   }

   al_spin_lock(imcp_splock, imcp_spvar);
   temp->next_pool = _imcp_pool_head;
   _imcp_pool_head = temp;
   al_spin_unlock(imcp_splock, imcp_spvar);
}


unsigned char *imcp_snip_create_buffer(AL_CONTEXT_PARAM_DECL_SINGLE)
{
   //al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
   return al_get_gen_2KB_buffer(AL_CONTEXT_SINGLE);
}


void imcp_snip_release_buffer(AL_CONTEXT_PARAM_DECL unsigned char *buffer)
{
   al_release_gen_2KB_buffer(AL_CONTEXT buffer);
}
