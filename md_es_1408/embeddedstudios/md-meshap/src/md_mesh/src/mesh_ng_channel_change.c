/********************************************************************************
* MeshDynamics
* --------------
* File     : mesh_ng_channel_change.c
* Comments : Channel change implementation
* Created  : 8/28/2007
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  2  |02/18/2009| Changes for uplink scan functionality           |Sriram  |
* -----------------------------------------------------------------------------
* |  1  |5/14/2008 | Changes for mobility downlink channel change    |Prachiti|
* -----------------------------------------------------------------------------
* |  0  |8/28/2007 | Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

#include <linux/string.h>
#include "al.h"
#include "al_print_log.h"
#include "al_conf.h"
#include "access_point.h"
#include "al_impl_context.h"
#include "mesh_defs.h"
#include "mesh_table.h"
#include "mesh.h"
#include "mesh_globals.h"
#include "mesh_errors.h"
#include "mesh_on_start.h"
#include "mesh_intervals.h"
#include "imcp_snip.h"
#include "mesh_imcp.h"
#include "mesh_ng.h"
#include "mesh_ng_fsm.h"

#define _TIME_INTERVAL         250
#define _MAX_CHANNELS_INDEX    16

struct _channel_index
{
   int channel;
   int occurence;
   int max_signal;
};
typedef struct _channel_index   _channel_index_map_t;

extern void enable_disable_processing(AL_CONTEXT_PARAM_DECL unsigned char enable);

AL_DECLARE_GLOBAL(_channel_index_map_t _channel_index_map[_MAX_CHANNELS_INDEX]);
AL_DECLARE_GLOBAL(_channel_index_map_t _last_channel_change);

static int _downlink_channel_change_for_mobile_thread(AL_CONTEXT_PARAM_DECL al_thread_param_t *param);

static AL_INLINE void _set_new_channel(int new_hop_level)
{
   int i;
   int temp;



   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Enter %s<%d>\n",__func__,__LINE__);
   for (i = 0; i < mesh_hardware_info->wm_net_if_count; i++)
   {
      if (!mesh_ng_is_ds_phy_sub_type_compatible(AL_CONTEXT mesh_hardware_info->ds_if_sub_type, _WMS_CONF(i)->phy_sub_type) ||
          !mesh_config->dy_channel_alloc ||
          !_WMS_CONF(i)->dy_channel_alloc ||
          !AL_NET_IF_IS_WIRELESS(_WMS_NET_IF(i)))
      {
         _WMS_CONF(i)->new_operating_channel = -1;
         continue;
      }

      temp = new_hop_level % _WMS_CONF(i)->dca_list_count;

      _WMS_CONF(i)->new_operating_channel = _WMS_CONF(i)->dca_list[temp];
   }
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Exit %s<%d>\n",__func__,__LINE__);
}


static AL_INLINE void _send_change_notification(unsigned short max_notification,
                                                unsigned short inter_packet_interval)
{
   unsigned short time_left;
   int            i;
   int            j;

   time_left = inter_packet_interval * max_notification;

   for (j = 0; j < max_notification; j++)
   {
      for (i = 0; i < mesh_hardware_info->wm_net_if_count; i++)
      {
         if (_WMS_CONF(i)->new_operating_channel == -1)
         {
            continue;
         }

         mesh_imcp_send_packet_channel_change(AL_CONTEXT _WMS_NET_IF(i),
                                              _WMS_CONF(i)->new_operating_channel,
                                              time_left);
      }

      al_thread_sleep(AL_CONTEXT inter_packet_interval);

      time_left -= inter_packet_interval;
   }
}


static AL_INLINE void _change_channels(void)
{
   int i;
   int current_channel;
   al_802_11_operations_t *ex_op;

   for (i = 0; i < mesh_hardware_info->wm_net_if_count; i++)
   {
      if (!mesh_ng_is_ds_phy_sub_type_compatible(AL_CONTEXT mesh_hardware_info->ds_if_sub_type,
                                                 _WMS_CONF(i)->phy_sub_type))
      {
         continue;
      }

      if (!mesh_config->dy_channel_alloc ||
          !_WMS_CONF(i)->dy_channel_alloc)
      {
         continue;
      }

      if (_WMS_CONF(i)->new_operating_channel == -1)
      {
         continue;
      }

      if (_WMS_CONF(i)->use_type == AL_CONF_IF_USE_TYPE_AP)
      {
         al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"MESH_AP:INFO:  %s<%d> Not changing channel for %s\n",
		 __func__, __LINE__, _WMS_CONF(i)->net_if->name);
         continue;
      }

      ex_op = _WMS_NET_IF(i)->get_extended_operations(_WMS_NET_IF(i));

      if (ex_op != NULL)
      {
         current_channel = ex_op->get_channel(_WMS_NET_IF(i));

         if ((_WMS_CONF(i)->new_operating_channel != -1) &&
             (_WMS_CONF(i)->new_operating_channel != current_channel))
         {
            al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,
                         "MESH_AP:INFO: %s<%d> setting new channel %d for downlink %s\n",
                         __func__, __LINE__, _WMS_CONF(i)->new_operating_channel,
                         _WMS_CONF(i)->name);

            ex_op->set_mode(_WMS_NET_IF(i), AL_802_11_MODE_INFRA, 0);
            ex_op->set_channel(_WMS_NET_IF(i), _WMS_CONF(i)->new_operating_channel);
            ex_op->set_mode(_WMS_NET_IF(i), AL_802_11_MODE_MASTER, 0);
            _WMS_CONF(i)->operating_channel = _WMS_CONF(i)->new_operating_channel;
         }
      }
   }
}


static int _downlink_channel_change_thread(AL_CONTEXT_PARAM_DECL al_thread_param_t *param)
{
   int new_hop_level;

   new_hop_level = (int)param->param;

   al_set_thread_name(AL_CONTEXT "dn_ch_change");

   /**
    * Lock parent change
    */

   AL_ATOMIC_INC(lock_parent_change);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,
                "MESH_AP:FLOW: %s<%d> Starting downlink channel change for hop level %d\n", __func__, __LINE__, new_hop_level);

   /**
    * Set each downlink's new channel according to the new hop level
    */

   _set_new_channel(new_hop_level);

   /**
    * Send 60 channel change notifications on each downlink, 250 ms apart
    */

   _send_change_notification(60, _TIME_INTERVAL);

   /**
    * Change the channel
    */


   _change_channels();

   /**
    * Un-lock parent change
    */

   mesh_hop_level = new_hop_level;

   AL_ATOMIC_DEC(lock_parent_change);

   AL_ATOMIC_SET(dn_channel_change_started, 0);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Exit %s<%d>\n",__func__,__LINE__);

   return 0;
}


void mesh_ng_downlink_channel_change_start(int param)
{
   if (!mesh_config->use_virt_if)
   {
      if (AL_ATOMIC_GET(lock_dn_channel_change))
      {
         al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MESH_NG: Downlink Channel change locked, skipping..");
         return;
      }

      if (AL_ATOMIC_GET(dn_channel_change_started))
      {
         return;
      }

      AL_ATOMIC_SET(dn_channel_change_started, 1);

      if (_IS_MOBILE || _IS_UPLINK_SCAN_ENABLED)
      {
         al_create_thread(AL_CONTEXT _downlink_channel_change_for_mobile_thread, (void *)param, AL_THREAD_PRIORITY_NORMAL, "_downlink_channel_change_for_mobile_thread");
      }
      else if (_DISJOINT_ADHOC_MODE)
      {
         al_create_thread(AL_CONTEXT _downlink_channel_change_thread, (void *)param, AL_THREAD_PRIORITY_NORMAL, "_downlink_channel_change_thread");
      }
   }
}


struct _up_channel_change_info
{
   int           millis_left;
   unsigned char new_channel;
};
typedef struct _up_channel_change_info   _up_channel_change_info_t;

static int _uplink_channel_change_thread(AL_CONTEXT_PARAM_DECL al_thread_param_t *param)
{
   al_802_11_operations_t    *ex_op;
   _up_channel_change_info_t *change_info;

   al_set_thread_name(AL_CONTEXT "up_ch_change");

   change_info = (_up_channel_change_info_t *)param->param;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Enter %s<%d>\n",__func__,__LINE__);

   AL_ATOMIC_INC(lock_dn_channel_change);
   AL_ATOMIC_INC(lock_parent_change);

   AL_ATOMIC_SET(up_cancel_channel_change, 0);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MESH_AP:INFO: %s<%d> Uplink channel change thread sleeping for %d ms\n",
   __func__, __LINE__, change_info->millis_left);

   al_thread_sleep(AL_CONTEXT change_info->millis_left);

   if (AL_ATOMIC_GET(up_cancel_channel_change))
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MESH_AP:INFO: %s<%d> Uplink channel change cancelled\n", __func__,
	  __LINE__);
   }
   else
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MESH_AP:INFO: %s<%d> Changing uplink channel to %d\n",
	  __func__, __LINE__, change_info->new_channel);
      ex_op = _DS_NET_IF->get_extended_operations(_DS_NET_IF);
      if (ex_op != NULL)
      {
         ex_op->set_channel(_DS_NET_IF, change_info->new_channel);
      }
      if (current_parent != NULL)
      {
         current_parent->channel = change_info->new_channel;
      }
   }

   AL_ATOMIC_DEC(lock_dn_channel_change);
   AL_ATOMIC_DEC(lock_parent_change);
   AL_ATOMIC_SET(up_channel_change_started, 0);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Exit %s<%d>\n",__func__,__LINE__);

   return 0;
}


void mesh_ng_uplink_channel_change_start(int milli_seconds_left, unsigned char new_channel)
{
   _up_channel_change_info_t *change_info;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Enter %s<%d>\n",__func__,__LINE__);

   if (AL_ATOMIC_GET(up_channel_change_started))
   {
      return;
   }

   AL_ATOMIC_SET(up_channel_change_started, 1);

   change_info = (_up_channel_change_info_t *)al_heap_alloc(AL_CONTEXT sizeof(_up_channel_change_info_t));

   change_info->new_channel = new_channel;
   change_info->millis_left = milli_seconds_left;

//RAMESH16MIG passing name to al_create_thread
   al_create_thread(AL_CONTEXT _uplink_channel_change_thread, (void *)change_info, AL_THREAD_PRIORITY_NORMAL, "_uplink_channel_change_thread");
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Exit %s<%d>\n",__func__,__LINE__);
}


/*
 *	downlink channel change for mobile node only, everything below
 */
static AL_INLINE int _check_in_dca(int current_channel, int dca_list_count)
{
   int i;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Enter %s<%d>\n",__func__,__LINE__);
   if (dca_list_count <= 0)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Exit %s<%d>\n",__func__,__LINE__);
      return -1;
   }


   for (i = 0; i < dca_list_count; i++)
   {
      if (_channel_index_map[i].channel == current_channel)
      {
         al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Exit %s<%d>\n",__func__,__LINE__);
         return i;
      }
   }

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Exit %s<%d>\n",__func__,__LINE__);
   return -1;
}


static AL_INLINE int _find_least_used_channel(int dca_list_count)
{
   int i;
   int min;
   int index;
   int min_signal;

   index      = -1;
   min_signal = -1;
   min        = -1;

   for (i = 0; i < dca_list_count; i++)
   {
      /**
       * Skip current parent's index
       */

      if (_channel_index_map[i].occurence == -1)
      {
         continue;
      }

      /**
       * First run
       */

      if (index == -1)
      {
         goto _change;
      }

      /**
       * Less number of nodes
       */

      if (_channel_index_map[i].occurence < min)
      {
         goto _change;
      }

      /**
       * Same number of nodes but better signal
       */

      if ((_channel_index_map[i].occurence == min) &&
          (_channel_index_map[i].max_signal < min_signal))
      {
         goto _change;
      }

      continue;

_change:

      min        = _channel_index_map[i].occurence;
      index      = i;
      min_signal = _channel_index_map[i].max_signal;
   }

   return index;
}


int _parents_enum_function(AL_CONTEXT_PARAM_DECL void *enum_param, parent_t *parent)
{
   int index;
   int *dca_list_count;

   dca_list_count = (int *)enum_param;

   index = _check_in_dca(parent->channel, *dca_list_count);

   if (index != -1)
   {
      _channel_index_map[index].occurence++;

      if (parent->signal > _channel_index_map[index].max_signal)
      {
         _channel_index_map[index].max_signal = parent->signal;
      }
   }

   return 0;
}


static AL_INLINE void _fill_channels_map(int dca_list_count, unsigned char *dca_list)
{
   int i;

   memset(_channel_index_map, 0, sizeof(_channel_index_map));

   /*set_default_channels*/

   for (i = 0; i < dca_list_count; i++)
   {
      if (current_parent && (current_parent->channel == dca_list[i]))          /*skip current parent's channel*/
      {
         _channel_index_map[i].channel   = -1;
         _channel_index_map[i].occurence = -1;
      }
      else
      {
         _channel_index_map[i].channel = dca_list[i];
      }
   }

   mesh_ng_enumerate_parents(AL_CONTEXT MESH_NG_ENUMERATE_PARENTS_TYPE_ALL,
                             (void *)&dca_list_count,
                             _parents_enum_function, 0);
}


static AL_INLINE void _set_new_channel_for_mobile(void)
{
   int i;
   int current_channel;
   int new_channel_index;
   al_802_11_operations_t *ex_op;
   int signal_delta;

   for (i = 0; i < mesh_hardware_info->wm_net_if_count; i++)
   {
      _WMS_CONF(i)->new_operating_channel = -1;

      if (!mesh_ng_is_ds_phy_sub_type_compatible(AL_CONTEXT mesh_hardware_info->ds_if_sub_type,
                                                 _WMS_CONF(i)->phy_sub_type) ||
          !mesh_config->dy_channel_alloc ||
          !_WMS_CONF(i)->dy_channel_alloc ||
          !AL_NET_IF_IS_WIRELESS(_WMS_NET_IF(i)))
      {
         _WMS_CONF(i)->new_operating_channel = -1;
         continue;
      }

      _fill_channels_map(_WMS_CONF(i)->dca_list_count, _WMS_CONF(i)->dca_list);

      ex_op = _WMS_NET_IF(i)->get_extended_operations(_WMS_NET_IF(i));

      current_channel = ex_op->get_channel(_WMS_NET_IF(i));

      new_channel_index = _find_least_used_channel(_WMS_CONF(i)->dca_list_count);

      if ((new_channel_index == -1) ||
          (_channel_index_map[new_channel_index].channel == current_channel))
      {
         continue;
      }

      if (_last_channel_change.channel == -1)
      {
   		 al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:INFO:  %s<%d>\n",__func__,__LINE__);
         goto _change;
      }

      if (_channel_index_map[new_channel_index].occurence < _last_channel_change.occurence)
      {
   		 al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:INFO:  %s<%d>\n",__func__,__LINE__);
         goto _change;
      }

      signal_delta = _last_channel_change.max_signal - _channel_index_map[new_channel_index].max_signal;

      if (signal_delta >= mesh_config->evaluation_damping_factor)
      {
   		 al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:INFO:  %s<%d>\n",__func__,__LINE__);
         goto _change;
      }

      continue;

_change:

      _WMS_CONF(i)->new_operating_channel = _channel_index_map[new_channel_index].channel;

      memcpy(&_last_channel_change,
             &_channel_index_map[new_channel_index],
             sizeof(_last_channel_change));
   }
}


static int _downlink_channel_change_for_mobile_thread(AL_CONTEXT_PARAM_DECL al_thread_param_t *param)
{
   unsigned short max_notification_count;

   max_notification_count = (int)param->param;

   al_set_thread_name(AL_CONTEXT "dn_ch_change_for_mobile");

   /**
    * Lock parent change
    */

   AL_ATOMIC_INC(lock_parent_change);

   /**
    * Set each downlink's new channel according to the parents list
    */

   _set_new_channel_for_mobile();

   /**
    * Send max_notification_count channel change notifications on each downlink, 250 ms apart
    */

   _send_change_notification(max_notification_count,
                             mesh_config->scanner_dwell_interval / max_notification_count);

   /**
    * Change the channel
    */

   _change_channels();

   /**
    * Un-lock parent change
    */

   AL_ATOMIC_DEC(lock_parent_change);

   AL_ATOMIC_SET(dn_channel_change_started, 0);

   return 0;
}


void mesh_ng_channel_change_init(void)
{
   _last_channel_change.channel    = -1;
   _last_channel_change.occurence  = -1;
   _last_channel_change.max_signal = -1;
}
