/********************************************************************************
* MeshDynamics
* --------------
* File     : mesh_ng_fsm_alt_scan.c
* Comments : Mesh New Generation Alternate scan state
* Created  : 4/10/2006
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  1  |2/2/2007  | Restore LED state after joining                 | Sriram |
* -----------------------------------------------------------------------------
* |  0  |4/10/2006 | Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

/**
 * This is a private source file included by mesh_ng_fsm.c, hence
 * It was created to improve readability.
 */

extern access_point_globals_t globals;
extern int mesh_on_start_find_ds(void );
void change_ds_from_wired_to_wireless(void)
{
    access_point_config_t *config;
    config = &globals.config;
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Enter %s<%d>\n",__func__,__LINE__);
    if ((!strcmp(mesh_hardware_info->ds_net_if->name, "eth1")) ||
        (!strcmp(mesh_hardware_info->ds_net_if->name, "eth0")))
    {

        func_mode = _FUNC_MODE_FFN;
        mesh_mode = _MESH_AP_RELAY;
        current_parent = &dummy_lfr_parent;

        mesh_on_start_find_ds();

        config->ds_net_if_info.net_if      = mesh_hardware_info->ds_net_if;
        config->ds_net_if_info.ack_timeout = mesh_hardware_info->ds_if_ack_timeout;
        config->ds_net_if_info.txpower     = mesh_hardware_info->ds_if_tx_power;
        config->ds_net_if_info.txrate      = mesh_hardware_info->ds_if_tx_rate;
        strcpy(config->ds_net_if_info.essid, mesh_hardware_info->ds_conf->essid);
        AL_ATOMIC_SET(_DS_NET_IF->buffering_state, AL_NET_IF_BUFFERING_STATE_NONE);
    }
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Exit %s<%d>\n",__func__,__LINE__);

}


static void _alternate_scan_execute(AL_CONTEXT_PARAM_DECL_SINGLE)
{
   al_notify_message_t message;

   message.message_type = AL_NOTIFY_MESSAGE_TYPE_SET_STATE;
   message.message_data = (void *)'Y';
   al_notify_message(AL_CONTEXT & message);

   mesh_ng_evaluate_list(AL_CONTEXT_SINGLE);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"MESH_NG:INFO: %s<%d> Disabling AP procesing before alternate parent search\n",
   __func__, __LINE__);
   access_point_enable_disable_processing(AL_CONTEXT 0);

   /**
    * RE-ENABLE the NET IF after disabling AP processing so that ASSOC REQ can be sent
    */

   AL_ATOMIC_SET(_DS_NET_IF->buffering_state, AL_NET_IF_BUFFERING_STATE_NONE);

   if (mesh_ng_core_join_best_parent(AL_CONTEXT_SINGLE))
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MESH_NG:INFO: %s<%d> Alternate scan failed, performing fallback.\n",
	  __func__, __LINE__);
	  /* Change Uplink interface from wired (ethernet) to wireless interface.
       * TODO: Uplink interface can be changed before evaluating parent list. So need to validate appropriate place for this. */
      change_ds_from_wired_to_wireless();
      _mesh_ng_fsm_set_state(AL_CONTEXT MESH_NG_FSM_STATE_FULL_SCAN);
      return;
   }

   _mesh_ng_fsm_set_state(AL_CONTEXT MESH_NG_FSM_STATE_RUNNING);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MESH_NG	: Enabling AP processing after <quick> alternate parent search");
//SPAWAR_2
   postscan_setup_wms(AL_CONTEXT_SINGLE);
   access_point_enable_disable_processing(AL_CONTEXT 1);

   /**
    * RE-ENABLE the NET IF just as a precaution
    */

   AL_ATOMIC_SET(_DS_NET_IF->buffering_state, AL_NET_IF_BUFFERING_STATE_NONE);

   mesh_imcp_send_packet_ap_sub_tree_info(AL_CONTEXT_SINGLE);

   message.message_type = AL_NOTIFY_MESSAGE_TYPE_SET_STATE;
   message.message_data = (void *)'G';
   al_notify_message(AL_CONTEXT & message);


   mesh_imcp_send_packet_heartbeat(AL_CONTEXT mesh_heart_beat_sqnr);
   mesh_imcp_send_packet_heartbeat2(AL_CONTEXT mesh_heart_beat_sqnr);
   mesh_heart_beat_sqnr++;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MESH_NG:INFO: %s<%d> Got best parent, (Parent quick shift, alternate),"
   "sub tree info sent.\n", __func__, __LINE__);
}
