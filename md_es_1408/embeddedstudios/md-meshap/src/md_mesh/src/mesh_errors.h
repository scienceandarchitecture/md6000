/********************************************************************************
* MeshDynamics
* --------------
* File     : mesh_errors.h
* Comments : Mesh error definations
* Created  : 4/22/2004
* Author   : Anand Bhange
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  1  |4/26/2004 | Testing Base Line                               | Anand  |
* -----------------------------------------------------------------------------
* |  0  |4/22/2004 | Created                                         | Anand  |
* -----------------------------------------------------------------------------
********************************************************************************/


#ifndef __MESH_ERRORS_H__
#define __MESH_ERRORS_H__

#define RETURN_SUCCESS                                      0
#define RETURN_ERROR                                        100
#define RETURN_ERROR_FILE_NOT_FOUND                         RETURN_ERROR + 1
#define RETURN_ERROR_INCORRECT_NET_IF_CONFIG_FILE_FORMAT    RETURN_ERROR + 2
#define RETURN_ERROR_NO_DS_FOUND                            RETURN_ERROR + 3
#define RETURN_ERROR_NO_WIRELESS_PARENT_FOUND               RETURN_ERROR + 4
#define RETURN_ERROR_INCORRECT_DECODE_KEY                   RETURN_ERROR + 5

#endif /*__MESH_ERRORS_H__*/
