
/********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : imcp_config.c
 * Comments : MESH AP config / IP config IMCP packet impl file.
 * Created  : 8/16/2004
 * Author   : Anand Bhange
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * -----------------------------------------------------------------------------
 * | No  |Date      |  Comment                                        | Author |
 * -----------------------------------------------------------------------------
 * | 18  |11/14/2006| Changes for heap debug support                  | Sriram |
 * -----------------------------------------------------------------------------
 * | 17  |6/12/2006 | untagged bug fix in ACL getpacket		          |Prachiti|
 * -----------------------------------------------------------------------------
 * | 16  |3/15/2006 | Added .11e enabled & category for ACL           | Bindu  |
 * |     |          | Removed vlan_name                               |        |
 * -----------------------------------------------------------------------------
 * | 15  |3/14/2006 | vlan_name reading added for ACL Packet          | Bindu  |
 * -----------------------------------------------------------------------------
 * | 14  |3/13/2006 | ACL list implemented                            | Sriram |
 * -----------------------------------------------------------------------------
 * | 13  |03/03/2006| Misc Changes of memcpy for dot11e				  | Bindu  |
 * -----------------------------------------------------------------------------
 * | 12  |02/22/2006| dot11e changes                                  | Bindu  |
 * -----------------------------------------------------------------------------
 * | 11  |02/20/2006| Config SQNR changes removed		              | Abhijit|
 * -----------------------------------------------------------------------------
 * | 10  |02/08/2006| Config SQNR and Hide SSID changes               | Sriram |
 * -----------------------------------------------------------------------------
 * |  9  |02/03/2006| Misc changes                                    | Abhijit|
 * -----------------------------------------------------------------------------
 * |  8  |12/14/2005| acktimeout and regdomain changes                |prachiti|
 * -----------------------------------------------------------------------------
 * |  7  |6/15/2005 | Misc changes                                    | Sriram |
 * -----------------------------------------------------------------------------
 * |  6  |5/20/2005 | FCC/ETSI + Other changes                        | Sriram |
 * -----------------------------------------------------------------------------
 * |  5  |1/8/2005  | Changes for per-interface essid/etc             | Sriram |
 * -----------------------------------------------------------------------------
 * |  4  |12/22/2004| Fixed typo                                      | Sriram |
 * -----------------------------------------------------------------------------
 * |  3  |12/8/2004 | Fixed minor mistakes in No 2                    | Sriram |
 * -----------------------------------------------------------------------------
 * |  2  |11/25/2004| le2cpu funcs called for packets                 | Anand  |
 * -----------------------------------------------------------------------------
 * |  1  |10/11/2004| Additions for IMCP 5.0                          | Abhijit|
 * -----------------------------------------------------------------------------
 * |  0  |8/16/2004 | Created.                                        | Anand  |
 * -----------------------------------------------------------------------------
 ********************************************************************************/

static int imcp_snip_ap_config_update_get_packet(AL_CONTEXT_PARAM_DECL unsigned char* buffer, unsigned int buf_length, imcp_packet_info_t* pi)
{
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s: %d\n",__func__,__LINE__);
	unsigned char*	p;
	int		i;
	unsigned int	cpu2le;
	int		nl;

	unsigned short  node_id;
	unsigned short  node_len;

#define _REQ_LENGTH		50	

	AL_PRINT_LOG_FLOW_0( "MESH_AP	: imcp_snip_ap_config_update_get_packet()");
	AL_ASSERT("MESH_AP	: imcp_snip_ap_config_update_get_packet", pi != NULL);
	AL_ASSERT("MESH_AP	: imcp_snip_ap_config_update_get_packet", buffer != NULL);
	/**
	 * Buffer length must be atleast req_length 
	 */

	if(buf_length < _REQ_LENGTH)
	{
		al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : Incorrect bufferlength\t  Received_len : %d fun :%s line:%d\n",buf_length,__func__,__LINE__);
		return IMCP_SNIP_ERROR_INADEQUATE_BUFFER_LENGTH;
	}
	p = buffer;

	IMCP_MSG_RECEIVE_ID_AND_LEN(p,node_id,node_len);
	/**
	 * Copy Sender DS MAC, SQNR, CTC, HPC, ROOT bssid, CRSSI, HI, CH, PARENT Bssid
	 * Known AP count, 
	 */
	memcpy(pi->u.ap_config_info.ds_mac.bytes,		p,		MAC_ADDR_LENGTH);	p += MAC_ADDR_LENGTH;
	pi->u.ap_config_info.ds_mac.length = MAC_ADDR_LENGTH;

	pi->u.ap_config_info.req_res				  = *p;							p += 1;
	pi->u.ap_config_info.res_id 				  = *p;							p += 1;

	memcpy(pi->u.ap_config_info.pref_parent.bytes,	 p,		MAC_ADDR_LENGTH);	p += MAC_ADDR_LENGTH;
	pi->u.ap_config_info.pref_parent.length = MAC_ADDR_LENGTH;

	memcpy(&pi->u.ap_config_info.signal_map,	     p,		8);					p += 8;
	pi->u.ap_config_info.hb_interval			  = *p;							p += 1;
	pi->u.ap_config_info.hb_miss_count			  = *p;							p += 1;
	pi->u.ap_config_info.hop_cost				  = *p;							p += 1;
	pi->u.ap_config_info.max_allow_hops		      = *p;							p += 1;
	pi->u.ap_config_info.la_scan_interval		  = *p;							p += 1;
	pi->u.ap_config_info.change_res_thr		      = *p;							p += 1;

	pi->u.ap_config_info.name_length		      = *p;							p += 1;
	memcpy(pi->u.ap_config_info.name,				 p,		pi->u.ap_config_info.name_length);				p += pi->u.ap_config_info.name_length;
	pi->u.ap_config_info.name[pi->u.ap_config_info.name_length] = 0;

	pi->u.ap_config_info.essid_length		      = *p;							p += 1;
	memcpy(pi->u.ap_config_info.essid,				 p,		pi->u.ap_config_info.essid_length);				p += pi->u.ap_config_info.essid_length;
	pi->u.ap_config_info.essid[pi->u.ap_config_info.essid_length] = 0;

	pi->u.ap_config_info.desc_length			  = *p;							p += 1;
	memcpy(pi->u.ap_config_info.desc,				 p,		pi->u.ap_config_info.desc_length);				p += pi->u.ap_config_info.desc_length;
	pi->u.ap_config_info.desc[pi->u.ap_config_info.desc_length] = 0;

	pi->u.ap_config_info.gps_x_length			  = *p;							p += 1;
	memcpy(pi->u.ap_config_info.gps_x,				 p,		pi->u.ap_config_info.gps_x_length);				p += pi->u.ap_config_info.gps_x_length;
	pi->u.ap_config_info.gps_x[pi->u.ap_config_info.gps_x_length] = 0;

	pi->u.ap_config_info.gps_y_length			  = *p;							p += 1;
	memcpy(pi->u.ap_config_info.gps_y,				 p,		pi->u.ap_config_info.gps_y_length);				p += pi->u.ap_config_info.gps_y_length;
	pi->u.ap_config_info.gps_y[pi->u.ap_config_info.gps_y_length] = 0;

	memcpy(&cpu2le,									 p,		4);					p += 4;	
	pi->u.ap_config_info.rts_thrd				= al_impl_cpu_to_le32(cpu2le);

	memcpy(&cpu2le,									 p,		4);					p += 4;	
	pi->u.ap_config_info.frag_thrd				= al_impl_cpu_to_le32(cpu2le);

	memcpy(&cpu2le,									 p,		4);					p += 4;
	pi->u.ap_config_info.beacon_int				= al_impl_cpu_to_le32(cpu2le);

	pi->u.ap_config_info.dca					  = *p;							p += 1;
	pi->u.ap_config_info.stay_awake_count		  = *p;							p += 1;
	pi->u.ap_config_info.bridge_ageing_time		  = *p;							p += 1;

	pi->u.ap_config_info.fcc_certified_operation  = *p;							p += 1;
	pi->u.ap_config_info.etsi_certified_operation = *p;							p += 1;

	memcpy(&cpu2le,									 p,		4);					p += 4;	
	pi->u.ap_config_info.ds_tx_rate				  = al_impl_cpu_to_le32(cpu2le);

	pi->u.ap_config_info.ds_tx_power			  = *p;							p += 1;

	pi->u.ap_config_info.interface_count 		  = *p;							p += 1;	

	pi->u.ap_config_info.interface_info			  = (phy_hardware_info_t*)al_heap_alloc(AL_CONTEXT pi->u.ap_config_info.interface_count * sizeof(phy_hardware_info_t) AL_HEAP_DEBUG_PARAM);

	for(i = 0; i< pi->u.ap_config_info.interface_count;i++) {

		memcpy(&node_id, p, sizeof(unsigned short));
		node_id = al_impl_le16_to_cpu(node_id);
		p += sizeof(unsigned short);
		memcpy(&node_len,p, sizeof(unsigned short));
		node_len = al_impl_le16_to_cpu(node_len);
		if(node_len & IMCP_EXT_MASK)
		{
			al_print_log(AL_CONTEXT AL_LOG_TYPE_DEBUG,"Extension bit is setted: %s LINE : %d\n",__func__,__LINE__);
		}
		p += sizeof(unsigned short);



		switch(node_id) {
			case IMCP_IF_802_3_INFO_ID :
				nl      = *p; p++;
				pi->u.ap_config_info.interface_info[i].name_length = nl;
				memcpy(pi->u.ap_config_info.interface_info[i].name,p,nl); p += nl;
				pi->u.ap_config_info.interface_info[i].name[nl] = 0;

				pi->u.ap_config_info.interface_info[i].type                     = *p;                   p += 1;
				pi->u.ap_config_info.interface_info[i].sub_type                 = *p;                   p += 1;
				pi->u.ap_config_info.interface_info[i].usage_type               = *p;                   p += 1;
				memcpy(&cpu2le, p, 4);                                    				p += 4;
				pi->u.ap_config_info.interface_info[i].tx_rate                  = al_impl_cpu_to_le32(cpu2le);

				pi->u.ap_config_info.interface_info[i].service_type             = *p;                   p += 1;

				break; 
			case IMCP_IF_802_11_INFO_ID: 
				nl	= *p; p++;
				pi->u.ap_config_info.interface_info[i].name_length = nl;
				memcpy(pi->u.ap_config_info.interface_info[i].name,p,nl); p += nl;
				pi->u.ap_config_info.interface_info[i].name[nl] = 0;

				pi->u.ap_config_info.interface_info[i].type				= *p;			p += 1;
				pi->u.ap_config_info.interface_info[i].sub_type			= *p;			p += 1;
				pi->u.ap_config_info.interface_info[i].usage_type 		= *p;			p += 1;
				pi->u.ap_config_info.interface_info[i].channel 			= *p;			p += 1;
				pi->u.ap_config_info.interface_info[i].bonding 			= *p;			p += 1;
				pi->u.ap_config_info.interface_info[i].dca 				= *p;			p += 1;
				pi->u.ap_config_info.interface_info[i].tx_power 		= *p;			p += 1;

				memcpy(&cpu2le,									 p,		4);					p += 4;	
				pi->u.ap_config_info.interface_info[i].tx_rate			= al_impl_cpu_to_le32(cpu2le);

				pi->u.ap_config_info.interface_info[i].service_type		= *p;			p += 1;


				pi->u.ap_config_info.interface_info[i].essid_length		= *p;			p += 1;
				memcpy(pi->u.ap_config_info.interface_info[i].essid, p, pi->u.ap_config_info.interface_info[i].essid_length); p += pi->u.ap_config_info.interface_info[i].essid_length;
				pi->u.ap_config_info.interface_info[i].essid[pi->u.ap_config_info.interface_info[i].essid_length]	= 0;

				memcpy(&cpu2le,									 p,		4);					p += 4;	
				pi->u.ap_config_info.interface_info[i].rts_thrd		= al_impl_cpu_to_le32(cpu2le);

				memcpy(&cpu2le,									 p,		4);					p += 4;	
				pi->u.ap_config_info.interface_info[i].frag_thrd	= al_impl_cpu_to_le32(cpu2le);

				memcpy(&cpu2le,									 p,		4);					p += 4;
				pi->u.ap_config_info.interface_info[i].beacon_int	= al_impl_cpu_to_le32(cpu2le);

				pi->u.ap_config_info.interface_info[i].dca_list_count	= *p;			p += 1;
				memcpy(pi->u.ap_config_info.interface_info[i].dca_list , p,	pi->u.ap_config_info.interface_info[i].dca_list_count);	p += pi->u.ap_config_info.interface_info[i].dca_list_count;

				/** Security Info */

				p++; /* Skip type */
				p++; /* Skip NONE enabled */

				p++; /* Skip type */
				if(*p) { /* WEP enabled */
					p++;
					nl = *p++; /* WEP strength */
					p++; /* Skip WEP key Index */
					p++; /* Skip key length */
					if(nl == AL_802_11_SECURITY_INFO_STRENGTH_40)
						p += AL_802_11_MAX_SECURITY_KEYS * 5;
					else
						p += 13;
				} else
					p++;

				p++; /* Skip type */
				if(*p) { /* RSN PSK enabled */
					p++;
					p++; /* RSN mode skipped */
					nl = *p++; /* key length */
					p += nl; /* key */
					p++; /* TKIP */
					p++; /* CCMP */
					p += 2; /* Renewal */
				} else
					p++;

				p++; /* Skip type */
				if(*p) { /* RSN RADIUS enabled */
					p++;
					p++; /* RSN mode skipped */
					p += 4; /* Radius server */
					p += 4; /* Port */
					nl = *p++; /* Key length */
					p += nl; /* Key */
					p++; /* TKIP */
					p++; /* CCMP */
					p += 2; /* Renewal */
				} else
					p++;
				if(node_len & IMCP_EXT_MASK)
				{
					IMCP_MSG_RECEIVE_ID_AND_LEN(p,node_id,node_len);

					switch(node_id){
						case IMCP_IF_802_11_EXT_11N_INFO_ID :

							pi->u.ap_config_info.interface_info[i].ht_capab.ldpc	= *p;	p += 1;

							pi->u.ap_config_info.interface_info[i].ht_capab.ht_bandwidth = *p;   p += 1;


							memcpy(&cpu2le, p, 4);                                          p += 4;
							pi->u.ap_config_info.interface_info[i].ht_capab.sec_offset = al_impl_cpu_to_le32(cpu2le);

							pi->u.ap_config_info.interface_info[i].ht_capab.gi_20		= *p;	 p += 1;
							pi->u.ap_config_info.interface_info[i].ht_capab.gi_40   	= *p;    p += 1;

							pi->u.ap_config_info.interface_info[i].ht_capab.tx_stbc   	= *p;  	 p += 1;

							pi->u.ap_config_info.interface_info[i].ht_capab.rx_stbc   	= *p;    p += 1;

							pi->u.ap_config_info.interface_info[i].ht_capab.gfmode		= *p;	 p += 1;

							//pi->u.ap_config_info.interface_info[i].ht_capab.smps		= *p;	 p += 1;


							memcpy(&cpu2le, p, 4);                                          	 p += 4;
							pi->u.ap_config_info.interface_info[i].ht_capab.max_amsdu_len = al_impl_cpu_to_le32(cpu2le);



							//pi->u.ap_config_info.interface_info[i].ht_capab.delayed_ba	= *p;    p += 1;


							//pi->u.ap_config_info.interface_info[i].ht_capab.dsss_cck_40      = *p;    p += 1;

							pi->u.ap_config_info.interface_info[i].ht_capab.intolerant       = *p;    p += 1;

							//pi->u.ap_config_info.interface_info[i].ht_capab.lsig_txop        = *p;    p += 1;

							pi->u.ap_config_info.interface_info[i].fram_agre.ampdu_enable	 = *p;    p += 1;

							memcpy(&cpu2le, p, 4);                                           p += 4;
							pi->u.ap_config_info.interface_info[i].fram_agre.max_ampdu_len   = al_impl_cpu_to_le32(cpu2le);

							break;
						case IMCP_IF_802_11_EXT_11N_11AC_INFO_ID :
							pi->u.ap_config_info.interface_info[i].ht_capab.ldpc    = *p;   p += 1;

							pi->u.ap_config_info.interface_info[i].ht_capab.ht_bandwidth = *p; p +=1;

							memcpy(&cpu2le, p, 4);                                          p += 4;
							pi->u.ap_config_info.interface_info[i].ht_capab.sec_offset = al_impl_cpu_to_le32(cpu2le);

							pi->u.ap_config_info.interface_info[i].ht_capab.gi_20           = *p;    p += 1;
							pi->u.ap_config_info.interface_info[i].ht_capab.gi_40           = *p;    p += 1;

							pi->u.ap_config_info.interface_info[i].ht_capab.tx_stbc         = *p;    p += 1;

							pi->u.ap_config_info.interface_info[i].ht_capab.rx_stbc         = *p;    p += 1;

							pi->u.ap_config_info.interface_info[i].ht_capab.gfmode          = *p;    p += 1;



							memcpy(&cpu2le, p, 4);                                                   p += 4;
							pi->u.ap_config_info.interface_info[i].ht_capab.max_amsdu_len = al_impl_cpu_to_le32(cpu2le);


							pi->u.ap_config_info.interface_info[i].ht_capab.intolerant       = *p;    p += 1;

							pi->u.ap_config_info.interface_info[i].fram_agre.ampdu_enable    = *p;  p += 1;

							memcpy(&cpu2le, p, 4);                                           p += 4;
							pi->u.ap_config_info.interface_info[i].fram_agre.max_ampdu_len   = al_impl_cpu_to_le32(cpu2le);

							memcpy(&cpu2le, p, 4);                                           p += 4;
							pi->u.ap_config_info.interface_info[i].vht_capab.max_mpdu_len   = al_impl_cpu_to_le32(cpu2le);


							break;
						default:
							al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR :Entered interface currently not supporting Extension : %s: %d\n",__func__,__LINE__);
							p += node_len;


						}
					}
					break;
					case IMCP_IF_802_15_4_INFO_ID:
					al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : Currently 802.15.4 interface config request/response Not Supported : %s: %d\n",__func__,__LINE__);
					break;
					default :
					al_print_log(AL_CONTEXT AL_CONTEXT AL_LOG_TYPE_DEBUG,"ERROR : (%s) function : LINE (%d) :Receiving length (%d) is not Correct  \n",__func__,__LINE__,buf_length);
					al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : Entered invalid interface  : %s: %d\n",__func__,__LINE__);
					p += node_len;
		}

		al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit : %s: %d\n",__func__,__LINE__);
		return p-buffer;

#undef _REQ_LENGTH
	}
}
static int imcp_snip_config_request_response_get_packet(AL_CONTEXT_PARAM_DECL unsigned char* buffer, unsigned int buf_length, imcp_packet_info_t* pi)
{
		al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s: %d\n",__func__,__LINE__);

#define _REQ_LENGTH             13	

		unsigned char*	p;
		unsigned short  le2cpu16;	
		unsigned short  node_id;	
		unsigned short  node_len;	

		AL_PRINT_LOG_FLOW_0( "MESH_AP	: imcp_snip_config_request_response_get_packet()");
		AL_ASSERT("MESH_AP	: imcp_snip_config_request_response_get_packet", pi != NULL);
		AL_ASSERT("MESH_AP	: imcp_snip_config_request_response_get_packet", buffer != NULL);
		/**
		 * Buffer length must be atleast req_length 
		 */

		if(buf_length < _REQ_LENGTH)
		{
			al_print_log(AL_CONTEXT AL_CONTEXT AL_LOG_TYPE_DEBUG,"ERROR : (%s) function : LINE (%d) :Receiving length (%d) is not Correct  \n",__func__,__LINE__,buf_length);

			return IMCP_SNIP_ERROR_INADEQUATE_BUFFER_LENGTH;
		}

		p = buffer;


		IMCP_MSG_RECEIVE_ID_AND_LEN(p,node_id,node_len);  


		/**
		 * Copy Sender DS MAC,request/response ,type ,response id
		 */
		memcpy(pi->u.req_res_info.ds_mac.bytes,		p,		MAC_ADDR_LENGTH);	p += MAC_ADDR_LENGTH;
		pi->u.req_res_info.ds_mac.length = MAC_ADDR_LENGTH;
		pi->u.req_res_info.req_res				  = *p;							p += 1;
		pi->u.req_res_info.type 				  = *p;							p += 1;
		pi->u.req_res_info.res_id 				  = *p;							p += 1;

		al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit : %s: %d\n",__func__,__LINE__);
		return p-buffer;

#undef _REQ_LENGTH
	}

static int imcp_snip_ip_config_get_packet(AL_CONTEXT_PARAM_DECL unsigned char* buffer, unsigned int buf_length, imcp_packet_info_t* pi)
{
		al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s: %d\n",__func__,__LINE__);
		unsigned char*	p;
		al_net_addr_t	net_addr;

		unsigned short  node_id;
		unsigned short  node_len;

#define _REQ_LENGTH		6

		AL_PRINT_LOG_FLOW_0( "MESH_AP	: imcp_snip_ip_config_get_packet()");
		AL_ASSERT("MESH_AP	: imcp_snip_ip_config_get_packet", pi != NULL);
		AL_ASSERT("MESH_AP	: imcp_snip_ip_config_get_packet", buffer != NULL);
		/**
		 * Buffer length must be atleast req_length 
		 */

		if(buf_length < _REQ_LENGTH){
			al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : IMCP_SNIP_ERROR_INADEQUATE_BUFFER_LENGTH: %s: %d\n",__func__,__LINE__);
			return IMCP_SNIP_ERROR_INADEQUATE_BUFFER_LENGTH;
		}
		p = buffer;


		IMCP_MSG_RECEIVE_ID_AND_LEN(p,node_id,node_len)


			/**
			 * Copy Sender DS MAC, SQNR, CTC, HPC, ROOT bssid, CRSSI, HI, CH, PARENT Bssid
			 * Known AP count, 
			 */
			memcpy(&net_addr.bytes,		p,	MAC_ADDR_LENGTH);	p += MAC_ADDR_LENGTH;
		net_addr.length = MAC_ADDR_LENGTH;
		if(AL_NET_ADDR_EQUAL(&(net_addr), _DS_MAC))
			return RETURN_SUCCESS;
		else
			return RETURN_ERROR;
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit : %s: %d\n",__func__,__LINE__);
#undef _REQ_LENGTH
	}

	static int imcp_snip_reg_domain_cc_update_get_packet(AL_CONTEXT_PARAM_DECL unsigned char* buffer, unsigned int buf_length, imcp_packet_info_t* pi)
	{
#define _REQ_LENGTH		12
		al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s: %d\n",__func__,__LINE__);

		unsigned short	cpu2le16;
		unsigned char*	p;

		unsigned short  node_id;
		unsigned short  node_len;

		AL_PRINT_LOG_FLOW_0( "MESH_AP	: imcp_snip_reg_domain_cc_update_get_packet()");
		AL_ASSERT("MESH_AP	: imcp_snip_reg_domain_cc_update_get_packet", pi != NULL);
		AL_ASSERT("MESH_AP	: imcp_snip_reg_domain_cc_update_get_packet", buffer != NULL);
		/**
		 * Buffer length must be atleast req_length 
		 */

		if(buf_length < _REQ_LENGTH){
			al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : IMCP_SNIP_ERROR_INADEQUATE_BUFFER_LENGTH: %s: %d\n",__func__,__LINE__);
			return IMCP_SNIP_ERROR_INADEQUATE_BUFFER_LENGTH;
		}
		p = buffer;

		IMCP_MSG_RECEIVE_ID_AND_LEN(p,node_id,node_len)


			/**
			 * Copy Regulatory Domain and Country Code 
			 * 
			 */
			memcpy(pi->u.reg_domain_cc_info.ds_mac.bytes,		p,		MAC_ADDR_LENGTH);	p += MAC_ADDR_LENGTH;
		pi->u.reg_domain_cc_info.ds_mac.length = MAC_ADDR_LENGTH;

		pi->u.reg_domain_cc_info.req_res				  = *p;							p += 1;
		pi->u.reg_domain_cc_info.res_id 				  = *p;							p += 1;

		memcpy(&cpu2le16,									 p,		2);					p += 2;
		pi->u.reg_domain_cc_info.reg_domain	= al_impl_cpu_to_le16(cpu2le16);

		memcpy(&cpu2le16,									 p,		2);					p += 2;
		pi->u.reg_domain_cc_info.country_code	= al_impl_cpu_to_le16(cpu2le16);

		al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit : %s: %d\n",__func__,__LINE__);
		return p-buffer;

#undef _REQ_LENGTH
	}

	static int imcp_snip_ack_timeout_update_get_packet(AL_CONTEXT_PARAM_DECL unsigned char* buffer, unsigned int buf_length, imcp_packet_info_t* pi)
	{
#define _REQ_LENGTH		9

		al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s: %d\n",__func__,__LINE__);
		unsigned char*	p;	
		int				i;
		int				nl;

		unsigned short			node_id;
		unsigned short			node_len;


		AL_PRINT_LOG_FLOW_0( "MESH_AP	: imcp_snip_ack_timeout_update_get_packet()");
		AL_ASSERT("MESH_AP	: imcp_snip_ack_timeout_update_get_packet", pi != NULL);
		AL_ASSERT("MESH_AP	: imcp_snip_ack_timeout_update_get_packet", buffer != NULL);
		/**
		 * Buffer length must be atleast req_length 
		 */

		if(buf_length < _REQ_LENGTH){
				al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : IMCP_SNIP_ERROR_INADEQUATE_BUFFER_LENGTH: %s: %d\n",__func__,__LINE__);
				return IMCP_SNIP_ERROR_INADEQUATE_BUFFER_LENGTH;
			}
			p = buffer;

		p = buffer;

        IMCP_MSG_RECEIVE_ID_AND_LEN(p,node_id,node_len)


		/**
		 * Copy dsmac,acktimeout count,ifnamelength,ifname,ack_timeout
		 * 
		 */
		memcpy(pi->u.ack_timeout.ds_mac.bytes,		p,		MAC_ADDR_LENGTH);	p += MAC_ADDR_LENGTH;
		pi->u.ack_timeout.ds_mac.length = MAC_ADDR_LENGTH;

		pi->u.ack_timeout.req_res				  = *p;						p += 1;
		pi->u.ack_timeout.res_id 				  = *p;						p += 1;
		pi->u.ack_timeout.interface_count 		  = *p;						p += 1;	

		pi->u.ack_timeout.interface_info		  = (phy_hardware_info_t*)al_heap_alloc(AL_CONTEXT pi->u.ack_timeout.interface_count * sizeof(phy_hardware_info_t) AL_HEAP_DEBUG_PARAM);

		for(i = 0; i< pi->u.ack_timeout.interface_count;i++) {

			nl	= *p; p++;
			pi->u.ack_timeout.interface_info[i].name_length 		= nl;
			memcpy(pi->u.ack_timeout.interface_info[i].name,p,nl); p += nl;
			pi->u.ack_timeout.interface_info[i].name[nl] = 0;

			pi->u.ack_timeout.interface_info[i].ack_timeout		= *p;			p += 1;
		}
		al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit : %s: %d\n",__func__,__LINE__);

		return p-buffer;

#undef _REQ_LENGTH
	}


	static int imcp_snip_hide_ssid_update_get_packet(AL_CONTEXT_PARAM_DECL unsigned char* buffer, unsigned int buf_length, imcp_packet_info_t* pi)
	{
#define _REQ_LENGTH		9

		al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s: %d\n",__func__,__LINE__);
		unsigned char*	p;	
		int				i;
		int				nl;

		unsigned short  node_id;
		unsigned short  node_len;

		AL_PRINT_LOG_FLOW_0( "MESH_AP	: imcp_snip_hide_ssid_update_get_packet()");
		AL_ASSERT("MESH_AP	: imcp_snip_hide_ssid_update_get_packet", pi != NULL);
		AL_ASSERT("MESH_AP	: imcp_snip_hide_ssid_update_get_packet", buffer != NULL);
		/**
		 * Buffer length must be atleast req_length 
		 */

		if(buf_length < _REQ_LENGTH)
			return IMCP_SNIP_ERROR_INADEQUATE_BUFFER_LENGTH;

		p = buffer;

		IMCP_MSG_RECEIVE_ID_AND_LEN(p,node_id,node_len)

			/**
			 * Copy dsmac,if count,ifnamelength,ifname,hide_ssid
			 * 
			 */
			memcpy(pi->u.hide_ssid_info.ds_mac.bytes,		p,		MAC_ADDR_LENGTH);	p += MAC_ADDR_LENGTH;
		pi->u.hide_ssid_info.ds_mac.length = MAC_ADDR_LENGTH;

		pi->u.hide_ssid_info.req_res				  = *p;						p += 1;
		pi->u.hide_ssid_info.res_id 				  = *p;						p += 1;
		pi->u.hide_ssid_info.interface_count 		  = *p;						p += 1;	

		pi->u.hide_ssid_info.interface_info		  = (phy_hardware_info_t*)al_heap_alloc(AL_CONTEXT pi->u.hide_ssid_info.interface_count * sizeof(phy_hardware_info_t) AL_HEAP_DEBUG_PARAM);

		for(i = 0; i< pi->u.hide_ssid_info.interface_count;i++) {

			nl	= *p; p++;
			pi->u.hide_ssid_info.interface_info[i].name_length 		= nl;
			memcpy(pi->u.hide_ssid_info.interface_info[i].name,p,nl); p += nl;
			pi->u.hide_ssid_info.interface_info[i].name[nl] = 0;

			pi->u.hide_ssid_info.interface_info[i].hide_ssid		= *p;			p += 1;
		}
		
		al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit : %s: %d\n",__func__,__LINE__);
		return p-buffer;

#undef _REQ_LENGTH
	}

	static int imcp_snip_dot11e_category_update_get_packet(AL_CONTEXT_PARAM_DECL unsigned char* buffer, unsigned int buf_length, imcp_packet_info_t* pi)
	{

#define _REQ_LENGTH		8

		al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s: %d\n",__func__,__LINE__);
		unsigned char*	p;
		int				i;
		int				category_name_length;	//(category name length)
		char			category_name[256];			//(category name)
		unsigned int	le322cpu;
		unsigned short	le162cpu;

		unsigned short  node_id;
		unsigned short  node_len;

		AL_PRINT_LOG_FLOW_0( "MESH_AP	: imcp_snip_dot11e_category_update_get_packet()");

		/**
		 * Buffer length must be atleast req_length 
		 */

		if(buf_length < _REQ_LENGTH){
			al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : IMCP_SNIP_ERROR_INADEQUATE_BUFFER_LENGTH: %s: %d\n",__func__,__LINE__);
			return IMCP_SNIP_ERROR_INADEQUATE_BUFFER_LENGTH;
		}
		p	= buffer;

		IMCP_MSG_RECEIVE_ID_AND_LEN(p,node_id,node_len)

			memset(&pi->u.dot11e_category,0,sizeof(pi->u.dot11e_category));

		memcpy(pi->u.dot11e_category.ds_mac.bytes, p, MAC_ADDR_LENGTH);	p += MAC_ADDR_LENGTH;
		pi->u.dot11e_category.ds_mac.length = MAC_ADDR_LENGTH;

		pi->u.dot11e_category.req_res		 = *p;				p += 1;
		pi->u.dot11e_category.res_id 		 = *p;				p += 1;

		if(pi->u.dot11e_category.req_res == IMCP_SNIP_CONFIG_RESPONSE) {
			al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : IMCP_SNIP_ERROR_NOT_IMPLEMENTED: %s: %d\n",__func__,__LINE__);
			return IMCP_SNIP_ERROR_NOT_IMPLEMENTED;
		}
		pi->u.dot11e_category.count		= *p;					p += 1;				

		for(i = 0; i < pi->u.dot11e_category.count;i++) {

			category_name_length	= *p;						p += 1;			
			memset(category_name,0,256);
			memcpy(category_name, p, category_name_length);		p += category_name_length;

			if(strcmp(category_name,		"ac_bk") == 0) {
				pi->u.dot11e_category.details[i].category = AL_802_11_DOT11E_CATEGORY_TYPE_AC_BK;
			} else if(strcmp(category_name, "ac_be") == 0) {
				pi->u.dot11e_category.details[i].category = AL_802_11_DOT11E_CATEGORY_TYPE_AC_BE;			
			} else if(strcmp(category_name, "ac_vi") == 0) {
				pi->u.dot11e_category.details[i].category = AL_802_11_DOT11E_CATEGORY_TYPE_AC_VI;			
			} else if(strcmp(category_name, "ac_vo") == 0) {
				pi->u.dot11e_category.details[i].category = AL_802_11_DOT11E_CATEGORY_TYPE_AC_VO;
			}			

			memcpy(&le322cpu, p, 4);							p += 4;	
			pi->u.dot11e_category.details[i].burst_time	= al_le32_to_cpu(le322cpu);

			memcpy(&le162cpu, p, 2);							p += 2;	
			pi->u.dot11e_category.details[i].acwmin		= al_le16_to_cpu(le162cpu);

			memcpy(&le162cpu, p, 2);							p += 2;	
			pi->u.dot11e_category.details[i].acwmax		= al_le16_to_cpu(le162cpu);

			pi->u.dot11e_category.details[i].aifsn				= *p;	p += 1;
			pi->u.dot11e_category.details[i].disable_backoff	= *p;	p += 1;

		}

		al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit : %s: %d\n",__func__,__LINE__);
		return p-buffer;	

#undef _REQ_LENGTH

	}

	static int imcp_snip_acl_info_get_packet(AL_CONTEXT_PARAM_DECL unsigned char* buffer, unsigned int buf_length, imcp_packet_info_t* pi)
	{

#define _REQ_LENGTH		8

		al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s: %d\n",__func__,__LINE__);
		unsigned char*	p;
		int				i;
		unsigned short	temp;
		unsigned short	le162cpu;

		unsigned short  node_id;
		unsigned short  node_len;

		/**
		 * Buffer length must be atleast req_length 
		 */

		if(buf_length < _REQ_LENGTH){
			al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : IMCP_SNIP_ERROR_INADEQUATE_BUFFER_LENGTH: %s: %d\n",__func__,__LINE__);
			return IMCP_SNIP_ERROR_INADEQUATE_BUFFER_LENGTH;
		}
		p	= buffer;

		IMCP_MSG_RECEIVE_ID_AND_LEN(p,node_id,node_len)


			memset(&pi->u.acl_info,0,sizeof(pi->u.acl_info));

		memcpy(pi->u.acl_info.ds_mac.bytes, p, MAC_ADDR_LENGTH);	p += MAC_ADDR_LENGTH;
		pi->u.acl_info.ds_mac.length = MAC_ADDR_LENGTH;

		pi->u.acl_info.req_res		 = *p;				p += 1;
		pi->u.acl_info.res_id 		 = *p;				p += 1;

		if(pi->u.acl_info.req_res == IMCP_SNIP_CONFIG_RESPONSE) {
			al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : IMCP_SNIP_ERROR_NOT_IMPLEMENTED : %s: %d\n",__func__,__LINE__);
			return IMCP_SNIP_ERROR_NOT_IMPLEMENTED;
		}
		pi->u.acl_info.count		= *p;				p += 1;

		if(pi->u.acl_info.count > 0) {
			pi->u.acl_info.entries		= (imcp_acl_entry_t*)al_heap_alloc(AL_CONTEXT sizeof(imcp_acl_entry_t) * pi->u.acl_info.count AL_HEAP_DEBUG_PARAM);
		}

		for(i = 0; i < pi->u.acl_info.count;i++) {

			memset(&pi->u.acl_info.entries[i],0,sizeof(imcp_acl_entry_t));

			memcpy(pi->u.acl_info.entries[i].sta_mac.bytes,p,MAC_ADDR_LENGTH); p	+= MAC_ADDR_LENGTH;
			pi->u.acl_info.entries[i].sta_mac.length	= MAC_ADDR_LENGTH;

			memcpy(&temp,p,2);	p += 2;

			le162cpu	= al_le16_to_cpu(temp);

			pi->u.acl_info.entries[i].vlan_tag			= IMCP_SNIP_ACL_ENTRY_GET_VLAN_TAG(le162cpu);
			pi->u.acl_info.entries[i].allow				= IMCP_SNIP_ACL_ENTRY_GET_ALLOW_BIT(le162cpu);
			pi->u.acl_info.entries[i].dot11e_enabled	= IMCP_SNIP_ACL_ENTRY_GET_DOT11E_ENABLED_BIT(le162cpu);
			pi->u.acl_info.entries[i].dot11e_category	= IMCP_SNIP_ACL_ENTRY_GET_DOT11E_CATEGORY_BIT(le162cpu);

			if(pi->u.acl_info.entries[i].vlan_tag == 0x0FFF)
				pi->u.acl_info.entries[i].vlan_tag	= 0xFFFF;

		}

		al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit : %s: %d\n",__func__,__LINE__);
		return p-buffer;	

#undef _REQ_LENGTH

	}
