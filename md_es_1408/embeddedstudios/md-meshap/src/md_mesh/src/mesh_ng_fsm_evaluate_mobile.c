/********************************************************************************
* MeshDynamics
* --------------
* File     : mesh_ng_fsm_evaluate_mobile.c
* Comments : Mobility evaluation FSM state
* Created  : 6/13/2007
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  4  |2/22/2009 | Updated current parent's last seen time         |Sriram  |
* -----------------------------------------------------------------------------
* |  3  |8/22/2008 | Changes for SIP                                 | Abhijit|
* -----------------------------------------------------------------------------
* |  2  |7/21/2008 | Changes for adhoc mode                          | Sriram |
* -----------------------------------------------------------------------------
* |  1  |5/14/2008 | Changes for mobility downlink channel change    |Prachiti|
* -----------------------------------------------------------------------------
* |  0  |6/13/2007 | Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

/**
 * This is a private source file included by mesh_ng_fsm.c, hence
 * It was created to improve readability.
 */


extern access_point_globals_t globals;


extern int mesh_on_start_find_ds1(int boot_flag);

extern int mesh_on_start_find_ds(void);
extern int mesh_on_start_initialize_ds_interface1(void);

extern int eth1_ping_status;

int mesh_clear_mobility_window;
int mesh_network_unstable;

static int _parent_reset_skip_count_enum_func (AL_CONTEXT_PARAM_DECL void *enum_param, parent_t *parent)
{
   parent->skip_bssid_count = 0;
}

static void _evaluate_mobile_execute(AL_CONTEXT_PARAM_DECL_SINGLE)
{
   parent_t              *parent=NULL, *connecting_parent=NULL;
   mobility_window_t     *window_item;
   mobility_window_t     *last_item;
   mobility_window_t     *winner = NULL;
   struct net_device     *dev;
   al_u64_t              tick_count;
   al_u64_t              timeout;
   const char            *name = "eth1";
   unsigned long         flags;
   int                   j;
   al_net_if_t           *net_if;
   access_point_config_t *config;
   int                   ifcnt;
   int                   prev_mode = func_mode;
   int                   max_window_count = 0;
   int                   parent_state_change = 0;
   static int            max_lfr_count;
   al_802_11_operations_t    *ex_op = NULL;

   config = &globals.config;

   if (mesh_clear_mobility_window) {
      printk(KERN_EMERG"%s:%d: Clearing Mobility Window\n",__func__, __LINE__);
      mesh_ng_clear_mobility_window();
      mesh_clear_mobility_window = 0;
   }

   if (mesh_mode == _MESH_AP_RELAY)
   {
      /**
       * Update current parent's last seen time so that only on_phy_link_notify
       * should account for beacon misses from it.
       */
      current_parent->last_seen_time = al_get_tick_count(AL_CONTEXT_SINGLE);
#if 0 //Mode status got changed in _current_parent_state_notify function.
	  if (_IS_LIMITED && !(current_parent->flags & MESH_PARENT_FLAG_LIMITED)) {
		  func_mode = _FUNC_MODE_FFN;
		  mesh_mode = _MESH_AP_RELAY;
		  al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "Changing it's mode since parent's mode has changed: current_parent bssid: %x:%x:%x:%x:%x:%x: mode = %d %s : %d",
			current_parent->bssid.bytes[0],
			current_parent->bssid.bytes[1],
			current_parent->bssid.bytes[2],
			current_parent->bssid.bytes[3],
			current_parent->bssid.bytes[4],
			current_parent->bssid.bytes[5],
			func_mode,
			__func__,
			__LINE__);
          parent_state_change = 1;
					
		  goto update_connected_state;
	  } 
	  else if (!_IS_LIMITED && (current_parent->flags & MESH_PARENT_FLAG_LIMITED)) {
		  func_mode = _FUNC_MODE_LFN;
		  mesh_mode = _MESH_AP_RELAY;
		  al_print_log (AL_CONTEXT AL_LOG_TYPE_INFORMATION, "Changing it's mode since parent's mode has changed: current_parent BSSID: %x:%x:%x:%x:%x:%x: mode = %d %s : %d",
			current_parent->bssid.bytes[0],
			current_parent->bssid.bytes[1],
			current_parent->bssid.bytes[2],
			current_parent->bssid.bytes[3],
			current_parent->bssid.bytes[4],
			current_parent->bssid.bytes[5],
			func_mode, __func__, __LINE__);

          parent_state_change = 1;

		  goto update_connected_state;
	  }
#endif
      mesh_ng_evaluate_list(AL_CONTEXT_SINGLE);

      if (_DISJOINT_ADHOC_MODE)
      {
         parent = mesh_ng_get_best_parent_adhoc(AL_CONTEXT_SINGLE);
      }
      else
      {
         parent = mesh_ng_get_best_parent(AL_CONTEXT_SINGLE);
      }

      if ((func_mode == _FUNC_MODE_LFR) && (parent != NULL) && (mesh_network_unstable == 0) &&
          (ds_proto_type & convert_mode_to_proto_cap(parent->phy_sub_type))/*PROTOCOL_COMBO*/)
      {
         al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MESH_NG : INFO : Node is LFR but got potential parent BSSID: %x:%x:%x:%x:%x:%x mode = %d %s : %d\n",
                 parent->bssid.bytes[0],
                 parent->bssid.bytes[1],
                 parent->bssid.bytes[2],
                 parent->bssid.bytes[3],
                 parent->bssid.bytes[4],
                 parent->bssid.bytes[5], func_mode, __func__, __LINE__);

		connecting_parent = parent;
		goto connect_parent;
      }

      if ((parent == NULL))
      {
become_lfr:
         /*set func_mode based on hope_cost*/
         if(_DISJOINT_ADHOC_MODE) {
            func_mode = _FUNC_MODE_LFR;
         }else if(!mesh_config->hope_cost){
            func_mode = _FUNC_MODE_FFN;
         }

         if (current_parent != &dummy_lfr_parent)
         {
            if (AL_NET_IF_IS_WIRELESS(mesh_hardware_info->ds_net_if))
               {
                  al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MESH_NG : INFO : Child has high caps, Send Disconnect %s : %d\n", __func__, __LINE__);
                     ex_op = (al_802_11_operations_t *)mesh_hardware_info->ds_net_if->get_extended_operations(mesh_hardware_info->ds_net_if);
                     if (ex_op)
                        ex_op->send_disassoc_from_station(mesh_hardware_info->ds_net_if, 0);
               }
               mesh_network_unstable = 1;
               mesh_clear_mobility_window = 1;
         }
         if (prev_mode != func_mode) {
            al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MESH_NG : INFO : prev_mode = %d, func_mode = %d,parent is NULL %s : %d\n",
												prev_mode, func_mode, __func__, __LINE__);
            current_parent = &dummy_lfr_parent;
            AL_ATOMIC_SET(_DS_NET_IF->buffering_state, AL_NET_IF_BUFFERING_STATE_DROP);
            _mesh_ng_fsm_set_state(AL_CONTEXT MESH_NG_FSM_STATE_RUNNING);
            mesh_ng_fsm_update_downlink_vendor_info();
         }
         goto _out;
      }

      if (mesh_network_unstable) {
         max_window_count = 10; 
      } else {
        max_window_count = mesh_config->scan_count; 
      }

      if (mobility_window_item_count < max_window_count)
      {
         window_item = (mobility_window_t *)al_heap_alloc(AL_CONTEXT sizeof(mobility_window_t)AL_HEAP_DEBUG_PARAM);
         ++mobility_window_item_count;
      }
      else
      {
         /**
          * Remove and move head
          */
         window_item          = mobility_window_head;
         mobility_window_head = mobility_window_head->next;
      }

      window_item->parent = parent;
      window_item->next   = NULL;

      /**
       * Insert into tail
       */

      if (mobility_window_head == NULL)
      {
         mobility_window_head = window_item;
      }
      else
      {
         mobility_window_tail->next = window_item;
      }

      mobility_window_tail = window_item;


      if (mobility_window_item_count != max_window_count)
      {
         al_print_log(AL_CONTEXT AL_LOG_TYPE_DEBUG, "MESH_NG : DEBUG : scan_count %d is not reached to mobility_window_item_count %d %s : %d\n", mesh_config->scan_count, mobility_window_item_count, __func__, __LINE__);
         func_mode = prev_mode;
         goto _out;
      } else {
         if (mesh_network_unstable) {
            printk(KERN_EMERG"%s:%d: Resetting Mesh Network to stable\n",__func__, __LINE__);
            mesh_network_unstable = 0;
         }
      }

      /**
       * Now we go through the window and find out the max consecutive
       * winner
       */

      last_item   = NULL;
      winner      = NULL;
      window_item = mobility_window_head;

      while (window_item != NULL)
      {
         if ((last_item != NULL) &&
				 (last_item->parent == window_item->parent) &&
				 (!(window_item->parent->flags & MESH_PARENT_FLAG_DISABLED)) &&
				 (!(window_item->parent->flags & MESH_PARENT_FLAG_CHILD)))
         {
            ++last_item->parent->pattern_count;
         }
         else
         {
            last_item = window_item;
            last_item->parent->pattern_count = 1;
         }

         if (last_item->parent->pattern_count >= mesh_config->pattern_match_min)
         {
            winner = last_item;
         }

         window_item = window_item->next;
      }

      tick_count = al_get_tick_count(AL_CONTEXT_SINGLE);
      if (winner)
         timeout    = winner->parent->last_seen_time + (((mesh_config->scanner_dwell_interval * mesh_config->scan_count *HZ) / 1000));

	  if ((winner != NULL) &&
		  (winner->parent == current_parent)) {

#if 0
          if (_IS_LIMITED && !(current_parent->flags & MESH_PARENT_FLAG_LIMITED)) {
			  func_mode = _FUNC_MODE_FFN;
			  mesh_mode = _MESH_AP_RELAY;
		  	  al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "Changing it's mode since parent's mode has changed: current_parent: %x:%x:%x:%x:%x:%x: mode = %d %s : %d",
				current_parent->bssid.bytes[0],
                current_parent->bssid.bytes[1],
                current_parent->bssid.bytes[2],
                current_parent->bssid.bytes[3],
                current_parent->bssid.bytes[4],
                current_parent->bssid.bytes[5],
				    __func__,
				    __LINE__);
						
			  goto update_connected_state;
		  } 
		  else if (!_IS_LIMITED && (current_parent->flags & MESH_PARENT_FLAG_LIMITED)) {
			  func_mode = _FUNC_MODE_LFN;
			  mesh_mode = _MESH_AP_RELAY;
		  	  al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "Changing it's mode since parent's mode has changed: current_parent: %x:%x:%x:%x:%x:%x: mode = %d %s : %d\n",
				current_parent->bssid.bytes[0],
                current_parent->bssid.bytes[1],
                current_parent->bssid.bytes[2],
                current_parent->bssid.bytes[3],
                current_parent->bssid.bytes[4],
                current_parent->bssid.bytes[5],
				    __func__,
				    __LINE__);
			  goto update_connected_state;
		  }
#endif
	  }
      if ((winner != NULL) &&
          (winner->parent->pattern_count >= mesh_config->pattern_match_min) &&
          (winner->parent != current_parent) && 
          (al_timer_before(tick_count, timeout)))
      {
         if (AL_ATOMIC_GET(lock_parent_change))
         {
            al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,
                         "MESH_NG : INFO : %s Parent shift to "AL_NET_ADDR_STR" deffered due to change lock %s : %d\n",
                         winner->parent->flags & MESH_PARENT_FLAG_PREFERRED ? "Preferred" : "",
                         AL_NET_ADDR_TO_STR(&winner->parent->bssid), __func__, __LINE__);
            func_mode = prev_mode;
            goto _out;
         }


         al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,
                      "MESH_NG : INFO : %s Parent shift to "AL_NET_ADDR_STR" CH %d required, PATTERN MATCH = %d %s : %d\n",
                      parent->flags & MESH_PARENT_FLAG_PREFERRED ? "Preferred" : "",
                      AL_NET_ADDR_TO_STR(&parent->bssid),
                      parent->channel,
                      winner->parent->pattern_count, __func__, __LINE__);


         if (_DISJOINT_ADHOC_MODE)
         {
            AL_ATOMIC_SET(_DS_NET_IF->buffering_state, AL_NET_IF_BUFFERING_STATE_NONE);
         }

         al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,
                      "MESH_NG : INFO : Joining Parent Bssid: BSSID %x:%x:%x:%x:%x:%x DS MAC : %x:%x:%x:%x:%x:%x %s :%d\n",
                      winner->parent->bssid.bytes[0],
                      winner->parent->bssid.bytes[1],
                      winner->parent->bssid.bytes[2],
                      winner->parent->bssid.bytes[3],
                      winner->parent->bssid.bytes[4],
                      winner->parent->bssid.bytes[5],
                      winner->parent->ds_mac.bytes[0],
                      winner->parent->ds_mac.bytes[1],
                      winner->parent->ds_mac.bytes[2],
                      winner->parent->ds_mac.bytes[3],
                      winner->parent->ds_mac.bytes[4],
                      winner->parent->ds_mac.bytes[5], __func__, __LINE__);

         al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,
                      "MESH_NG : INFO : Parent Bssid: %x:%x:%x:%x:%x:%x Parent DS MAC : %x:%x:%x:%x:%x:%x %s : %d\n",
                      parent->bssid.bytes[0],
                      parent->bssid.bytes[1],
                      parent->bssid.bytes[2],
                      parent->bssid.bytes[3],
                      parent->bssid.bytes[4],
                      parent->bssid.bytes[5],
                      parent->ds_mac.bytes[0],
                      parent->ds_mac.bytes[1],
                      parent->ds_mac.bytes[2],
                      parent->ds_mac.bytes[3],
                      parent->ds_mac.bytes[4],
                      parent->ds_mac.bytes[5], __func__, __LINE__);

         al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,
                      " MESH_NG : INFO Current Parent Bssid: %x:%x:%x:%x:%x:%x DS MAC : %x:%x:%x:%x:%x:%x %s : %d\n",
                      current_parent->bssid.bytes[0],
                      current_parent->bssid.bytes[1],
                      current_parent->bssid.bytes[2],
                      current_parent->bssid.bytes[3],
                      current_parent->bssid.bytes[4],
                      current_parent->bssid.bytes[5],
                      current_parent->ds_mac.bytes[0],
                      current_parent->ds_mac.bytes[1],
                      current_parent->ds_mac.bytes[2],
                      current_parent->ds_mac.bytes[3],
                      current_parent->ds_mac.bytes[4],
                      current_parent->ds_mac.bytes[5], __func__, __LINE__);

         al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MESH_NG : INFO : Disabling AP processing before parent switch %s : %d\n", __func__, __LINE__);
	

         access_point_enable_disable_processing(AL_CONTEXT 0);
         //al_thread_sleep(1);
		 connecting_parent = winner->parent;

connect_parent:
         if (mesh_ng_core_join_parent(AL_CONTEXT connecting_parent))
         {
            if (_DISJOINT_ADHOC_MODE)
            {
               /**
                * Upon failure, we immedietly become a LFR
                */
               if (mesh_config->failover_enabled)
               {
                  //mesh_ng_clear_parents();
                  mesh_on_start_find_ds1(0);
                  if (!strcmp(mesh_hardware_info->ds_net_if->name, "eth1"))
                  {
                     /* update ds interface parameters */
                     config->ds_net_if_info.net_if      = mesh_hardware_info->ds_net_if;
                     config->ds_net_if_info.ack_timeout = mesh_hardware_info->ds_if_ack_timeout;
                     config->ds_net_if_info.txpower     = mesh_hardware_info->ds_if_tx_power;
                     config->ds_net_if_info.txrate      = mesh_hardware_info->ds_if_tx_rate;

                     strcpy(config->ds_net_if_info.essid, mesh_hardware_info->ds_conf->essid);
							al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MESH_NG : INFO : ds interface parameters : "
																	"net_if = %d, ack_timeout = %d, txpower = %d, txrate = %d, %s : %d\n",
																	config->ds_net_if_info.net_if, config->ds_net_if_info.ack_timeout,
																	config->ds_net_if_info.txpower, config->ds_net_if_info.txrate,
																	__func__, __LINE__);
                     AL_ATOMIC_SET(_DS_NET_IF->buffering_state, AL_NET_IF_BUFFERING_STATE_NONE);
                     //	mesh_on_start_initialize_ds_interface1();
                  }
                  else
                  {
                     AL_ATOMIC_SET(_DS_NET_IF->buffering_state, AL_NET_IF_BUFFERING_STATE_DROP);
                  }
               }
               else
               {
                  AL_ATOMIC_SET(_DS_NET_IF->buffering_state, AL_NET_IF_BUFFERING_STATE_DROP);
               }


               func_mode      = _FUNC_MODE_LFR;
               current_parent = &dummy_lfr_parent;
               //AL_ATOMIC_SET(_DS_NET_IF->buffering_state,AL_NET_IF_BUFFERING_STATE_DROP);

//DEBUG:::MODE
               _mesh_ng_fsm_set_state(AL_CONTEXT MESH_NG_FSM_STATE_RUNNING);
               access_point_enable_disable_processing(AL_CONTEXT 1);

		 	   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "Connected to best parent %x:%x:%x:%x:%x:%x. Changing it's mode to: %d:  %s : %d",
                      current_parent->bssid.bytes[0],
                      current_parent->bssid.bytes[1],
                      current_parent->bssid.bytes[2],
                      current_parent->bssid.bytes[3],
                      current_parent->bssid.bytes[4],
                      current_parent->bssid.bytes[5],
					  func_mode, __func__, __LINE__);
            }
            else
            {
               al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MESH_NG : INFO : Switch failed, performing fallback %s : %d\n", __func__, __LINE__);
               _mesh_ng_fsm_set_state(AL_CONTEXT MESH_NG_FSM_STATE_FULL_SCAN);
            }

            mesh_ng_fsm_update_downlink_vendor_info();
//SPAWAR_2
            if (!postscan_setup_wms(AL_CONTEXT_SINGLE))
            {
                al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MESH_NG : INFO : Kernel Entry for child got deleted. Remove the child from mesh table %s: %d\n", __func__, __LINE__);
                enable_disable_processing(0);
                enable_disable_processing(1);
            }
            /* Set disabled flag for parent if association fails */
            connecting_parent->flags |= MESH_PARENT_FLAG_DISABLED;
            return;
         }

         if ((_DISJOINT_ADHOC_MODE) && (parent->flags & MESH_PARENT_FLAG_LIMITED))
         {
            func_mode = _FUNC_MODE_LFN;
         }
         else
         {
            func_mode = _FUNC_MODE_FFN;
         }
		 al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "Connected to best parent %x:%x:%x:%x:%x:%x. Changing it's mode to: %d:  %s : %d",
                      current_parent->bssid.bytes[0],
                      current_parent->bssid.bytes[1],
                      current_parent->bssid.bytes[2],
                      current_parent->bssid.bytes[3],
                      current_parent->bssid.bytes[4],
                      current_parent->bssid.bytes[5],
					  func_mode, __func__, __LINE__);
					

update_connected_state:
      mesh_on_start_find_ds();
      config->ds_net_if_info.net_if      = mesh_hardware_info->ds_net_if;
      config->ds_net_if_info.ack_timeout = mesh_hardware_info->ds_if_ack_timeout;
      config->ds_net_if_info.txpower     = mesh_hardware_info->ds_if_tx_power;
      config->ds_net_if_info.txrate      = mesh_hardware_info->ds_if_tx_rate;
      strcpy(config->ds_net_if_info.essid, mesh_hardware_info->ds_conf->essid);


//DEBUG:::MODE
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MESH_NG : INFO : Enabling AP processing after parent switch %s : %d\n", __func__, __LINE__);
      access_point_enable_disable_processing(AL_CONTEXT 1);
      if (parent_state_change == 0)
      {
         mesh_imcp_send_packet_ap_sub_tree_info(AL_CONTEXT_SINGLE);
      }
      AL_ATOMIC_SET(_DS_NET_IF->buffering_state, AL_NET_IF_BUFFERING_STATE_NONE);
         _mesh_ng_fsm_set_state(AL_CONTEXT MESH_NG_FSM_STATE_RUNNING);

         mesh_ng_fsm_update_downlink_vendor_info();
//SPAWAR_2
         if (!postscan_setup_wms(AL_CONTEXT_SINGLE))
         {
            al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MESH_NG : INFO : Kernel Entry for child got deleted. Remove the child from mesh table %s: %d\n", __func__, __LINE__);
            enable_disable_processing(0);
            enable_disable_processing(1);
         }

         mesh_imcp_send_packet_heartbeat(AL_CONTEXT mesh_heart_beat_sqnr);
         mesh_imcp_send_packet_heartbeat2(AL_CONTEXT mesh_heart_beat_sqnr);
         mesh_heart_beat_sqnr++;

         mesh_ng_enumerate_parents(AL_CONTEXT MESH_NG_ENUMERATE_PARENTS_TYPE_ALL, NULL, _parent_reset_skip_count_enum_func, 0);

         mesh_plugin_post_event(MESH_PLUGIN_EVENT_PARENT_SHIFTED, NULL);
      }
      else
      {
         //func_mode = prev_mode;
      }
      mesh_ng_downlink_channel_change_start(mesh_config->scan_count);
   }

_out:
   _mesh_ng_fsm_set_state(AL_CONTEXT MESH_NG_FSM_STATE_RUNNING);
}


mesh_ng_clear_mobility_window()
{
   mobility_window_t	*temp = mobility_window_head;
   mobility_window_t	*temp_next = NULL;

   while (temp != NULL) {
   temp_next = temp->next;
   al_heap_free(temp);
   temp = temp_next;
   }

   mobility_window_head       = NULL;
   mobility_window_tail       = NULL;
   mobility_window_item_count = 0;
}
