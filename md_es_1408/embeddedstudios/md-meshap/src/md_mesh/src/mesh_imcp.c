/********************************************************************************
* MeshDynamics
* --------------
* File     : mesh_imcp.c
* Comments : Mesh imcp processing implementation.
* Created  : 4/16/2004
* Author   : Anand Bhange
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* | 90  |02/23/2009| Added Update logic for parent mobility flag     | Sriram |
* -----------------------------------------------------------------------------
* | 89  |02/18/2009| Additional Changes for Uplink Scan Functionality| Sriram |
* -----------------------------------------------------------------------------
* | 88  |1/27/2009 | Changes for Uplink Scan Functionality           | Sriram |
* -----------------------------------------------------------------------------
* | 87  |8/22/2008 | Changes for SIP                                 |Abhijit |
* -----------------------------------------------------------------------------
* | 86  |05/09/2008| Changes for mobile downlink channel change      |Prachiti|
* -----------------------------------------------------------------------------
* | 85  |05/09/2008| Changes for Beaconing Uplink		              |Prachiti|
* -----------------------------------------------------------------------------
* | 84  |6/20/2007 | Changes for multi-cast snooping                 |Prachiti|
* -----------------------------------------------------------------------------
* | 83  |6/13/2007 | Allow 0-20 HB SQNR when detecting duplicates    | Sriram |
* -----------------------------------------------------------------------------
* | 82  |6/13/2007 | parent->signal not changed when mobile          | Sriram |
* -----------------------------------------------------------------------------
* | 81  |6/6/2007  | access_point_update_sta_last_rx changed         | Sriram |
* -----------------------------------------------------------------------------
* | 80  |4/19/2007 |  Changes for forced disassociation              | Sriram |
* -----------------------------------------------------------------------------
* | 79  |4/11/2007 | Changes for Radio diagnostics                   | Sriram |
* -----------------------------------------------------------------------------
* | 78  |01/01/2007| IF-Specific TXRate and TXPower changes          |Prachiti|
* -----------------------------------------------------------------------------
* | 77  |11/15/2006| Memory leak problem fixed                       | Sriram |
* -----------------------------------------------------------------------------
* | 76  |11/14/2006| Changes for heap debug support                  | Sriram |
* -----------------------------------------------------------------------------
* | 75  |10/18/2006| ETSI Radar changes                              | Sriram |
* -----------------------------------------------------------------------------
* | 74  |5/18/2006 | sta_info crash and leak fixed                   | Sriram |
* -----------------------------------------------------------------------------
* | 73  |3/15/2006 | Added .11e enabled & category for ACL           | Bindu  |
* |     |          | Removed vlan_name                               |        |
* -----------------------------------------------------------------------------
* | 72  |3/13/2006 | ACL list implemented                            | Sriram |
* -----------------------------------------------------------------------------
* | 71  |03/10/2006| Network change client removal undone            | Sriram |
* -----------------------------------------------------------------------------
* | 70  |03/08/2006| Loop prevention changes                         | Sriram |
* -----------------------------------------------------------------------------
* | 69  |03/03/2006| Removed comments for dot11e prints              | Bindu  |
* -----------------------------------------------------------------------------
* | 68  |02/22/2006| changes for dot11e                              | Bindu  |
* -----------------------------------------------------------------------------
* | 67  |02/20/2006| config sqnr changes removed					  | Abhijit|
* -----------------------------------------------------------------------------
* | 66  |02/03/2006| changes for generic commands and misc changes   | Abhijit|
* -----------------------------------------------------------------------------
* | 65  |02/01/2006| Changes for IMCP sta info						  | Abhijit|
* -----------------------------------------------------------------------------
* | 64  |01/01/2006| Changes for IMCP Heartbeat2					  | Abhijit|
* -----------------------------------------------------------------------------
* | 63  |01/01/2006| Changes for IMCP Analyser Init                  | Abhijit|
* -----------------------------------------------------------------------------
* | 62  |12/16/2005|supported channels changes					      |prachiti|
* -----------------------------------------------------------------------------
* | 61  |12/14/2005| acktimeout and regdomain changes                |prachiti|
* -----------------------------------------------------------------------------
* | 60  |25/11/2005|Live channel taken frm netif in hardware_info    |Prachiti|
* -----------------------------------------------------------------------------
* | 59  |10/5/2005 | Loop detection added                            | Sriram |
* -----------------------------------------------------------------------------
* | 58  |10/4/2005 | Changes for IMCP WM Info                        | Sriram |
* -----------------------------------------------------------------------------
* | 57  |9/22/2005 | Interrupt disable/enable semantics changed      | Sriram |
* -----------------------------------------------------------------------------
* | 56  |9/19/2005 | mesh_imcp_send_packet_ds_excerciser + others    | Sriram |
* -----------------------------------------------------------------------------
* | 55  |9/13/2005 | Misc changes                                    | Sriram |
* -----------------------------------------------------------------------------
* | 54  |7/29/2005 | Misc changes                                    | Sriram |
* -----------------------------------------------------------------------------
* | 53  |6/21/2005 | Fixed bug in HB processing                      | Sriram |
* -----------------------------------------------------------------------------
* | 52  |6/15/2005 | Misc changes                                    | Sriram |
* -----------------------------------------------------------------------------
* | 51  |5/20/2005 | FCC/ETSI + Other changes                        | Sriram |
* -----------------------------------------------------------------------------
* | 50  |5/17/2005 | Changes by Anand for key update                 | Sriram |
* -----------------------------------------------------------------------------
* | 49  |5/6/2005  | Model info changes added                        | Sriram |
* -----------------------------------------------------------------------------
* | 48  |4/12/2005 | Channel change packet added                     | Sriram |
* -----------------------------------------------------------------------------
* | 47  |4/9/2005  | DS channel/sub-type added to handshake          | Sriram |
* -----------------------------------------------------------------------------
* | 46  |03/09/2005| Board Temp added to heartbeat info			  | Anand  |
* -----------------------------------------------------------------------------
* | 45  |02/08/2005| Bug Fixes                                       | Anand  |
* -----------------------------------------------------------------------------
* | 44  |1/8/2005  | Changes for per-interface essid/etc             | Sriram |
* -----------------------------------------------------------------------------
* | 43  |1/6/2005  | Misc changes                                    | Sriram |
* -----------------------------------------------------------------------------
* | 42  |12/22/2004| Misc changes                                    | Sriram |
* -----------------------------------------------------------------------------
* | 41  |12/20/2004| Misc Changes + phy sub type                     | Sriram |
* -----------------------------------------------------------------------------
* | 40  |12/13/2004| Averaged out RSSI                               | Sriram |
* -----------------------------------------------------------------------------
* | 39  |12/13/2004| stay_awake_count commented temporarily          | Sriram |
* -----------------------------------------------------------------------------
* | 38  |12/8/2004 | Fixed ap config info processing                 | Sriram |
* -----------------------------------------------------------------------------
* |  37 |11/30/2004| added wm channel,type to hardware info          | Abhijit|
* -----------------------------------------------------------------------------
* |  36 |10/11/2004| changes for IMCP5.0                             | Abhijit|
* -----------------------------------------------------------------------------
* | 35  |8/26/2004 | Mesh Init Packet changes.                       | Anand  |
* -----------------------------------------------------------------------------
* | 34  |7/29/2004 | changes in AP Config IMCP packet                | Anand  |
* -----------------------------------------------------------------------------
* | 33  |7/25/2004 | Fixes for sub-tree information packet           | Sriram |
* -----------------------------------------------------------------------------
* | 32  |7/25/2004 | only enable known aps sent in HB                | Anand  |
* -----------------------------------------------------------------------------
* | 31  |7/24/2004 | misc changes                                    | Anand  |
* -----------------------------------------------------------------------------
* | 30  |7/23/2004 | bit rate added to HB                            | Anand  |
* -----------------------------------------------------------------------------
* | 29  |7/23/2004 | Misc changes                                    | Sriram |
* -----------------------------------------------------------------------------
* | 28  |7/21/2004 | mesh_imcp_send_packet_ip_ap_start added.        | Anand  |
* -----------------------------------------------------------------------------
* | 27  |7/19/2004 | ap_sub_tree_infp, ap_config_update added        | Anand  |
* -----------------------------------------------------------------------------
* | 26  |6/17/2004 | Misc changes                                    | Sriram |
* -----------------------------------------------------------------------------
* | 25  |6/16/2004 | ASSERT statements for checking NULL values      | Anand  |
* -----------------------------------------------------------------------------
* | 24  |6/15/2004 | Reset Implemented                               | Anand  |
* -----------------------------------------------------------------------------
* | 23  |6/15/2004 | Repeated Mesh HB processing avoided             | Anand  |
* -----------------------------------------------------------------------------
* | 22  |6/11/2004 | process_hardware_info_request added             | Anand  |
* -----------------------------------------------------------------------------
* | 21  |6/7/2004  | Filter macro used instead of al_print_log       | Anand  |
* -----------------------------------------------------------------------------
* | 20  |6/2/2004  | Bug Fixed - Dynamic Channel Alloc messages      | Anand  |
* ------------------------------------------------------------------------------
* | 19  |5/31/2004 | u name given to union for linux compilation     | Anand  |
* -----------------------------------------------------------------------------
* | 18  |5/28/2004 | crssi value for root is zero                    | Anand  |
* -----------------------------------------------------------------------------
* | 17  |5/28/2004 | IMCP packet Dump added.                         | Anand  |
* -----------------------------------------------------------------------------
* | 16  |5/28/2004 | mac addr copy logic changed                     | Anand  |
* -----------------------------------------------------------------------------
* | 15  |5/26/2004 | Locks added to known_aps list                   | Anand  |
* -----------------------------------------------------------------------------
* | 14  |5/25/2004 | Return types changed.                           | Anand  |
* -----------------------------------------------------------------------------
* | 13  |5/21/2004 | Log type added to al_print_log                  | Anand  |
* -----------------------------------------------------------------------------
* | 12  |5/20/2004 | changed mesh_table_find_relay_ap_for_sta_ap func| Anand  |
* -----------------------------------------------------------------------------
* | 11  |5/20/2004 | mesh state checking added (process imcp packet) | Anand  |
* -----------------------------------------------------------------------------
* | 10  |5/20/2004 | RECEIVE msg moved (prints only if processed)    | Anand  |
* -----------------------------------------------------------------------------
* |  9  |5/11/2004 | HandShake request checking on sender ds mac addr| Anand  |
* -----------------------------------------------------------------------------
* |  8  |5/6/2004  | Bug fixed, set called, event added              | Anand  |
* -----------------------------------------------------------------------------
* |  7  |5/5/2004  | Packet/Memory Leak Debug params added           | Anand  |
* -----------------------------------------------------------------------------
* |  6  |5/5/2004  | global var handshake_response_entry added       | Anand  |
* -----------------------------------------------------------------------------
* |  5  |4/30/2004 | Log messages Added                              | Anand  |
* -----------------------------------------------------------------------------
* |  4  |4/27/2004 | Ch removed fromHeartBeat/HS Response imcp Pckt  | Anand  |
* -----------------------------------------------------------------------------
* |  3  |4/27/2004 | HBI value added in Heartbeat/HS response        | Anand  |
* -----------------------------------------------------------------------------
* |  2  |4/26/2004 | Testing Base Line                               | Anand  |
* -----------------------------------------------------------------------------
* |  1  |4/21/2004 | **Made (major) changes for AL_CONTEXT**         | Anand  |
* -----------------------------------------------------------------------------
* |  0  |4/16/2004 | Created.                                        | Anand  |
* -----------------------------------------------------------------------------
********************************************************************************/

#include <linux/string.h>
#include <linux/module.h>
#include "al.h"
#include "al_print_log.h"
#include "access_point.h"
#include "al_impl_context.h"
#include "al_print_log.h"
#include "al_conf.h"
#include "sip_conf.h"
#include "mesh_defs.h"
#include "imcp_snip.h"
#include "al_impl_context.h"
#include "mesh_table.h"
#include "mesh.h"
#include "mesh_globals.h"
#include "imcp_snip.h"
#include "mesh_imcp.h"
#include "mesh_errors.h"
#include "access_point_tester_testee.h"
#include "mesh_ng.h"
#include "mesh_ng_fsm.h"
#include "mobility_conf.h"
#include "mesh_ng_saturation.h"
#include "mesh_ap.h"
#include "mesh_ng_channel_change.h"
#include "mesh_plugin.h"
#include "mesh_plugin_impl.h"

#define _MAX_WM_INFO_REQUESTS    7
extern void dump_bytes(char *D, int count);
extern int mesh_clear_mobility_window;
extern int mesh_network_unstable;

struct _wm_info_request_list
{
   al_net_addr_t                ds_mac;
   int                          count;
   struct _wm_info_request_list *next;
   struct _wm_info_request_list *prev;
};

typedef struct _wm_info_request_list   _wm_info_request_list_t;

AL_DECLARE_GLOBAL(_wm_info_request_list_t * _wm_info_list_head);

static meshap_process_imcp_sip_packet_t _sip_private_packet_handler;

extern int eth1_ping_status;

void mesh_imcp_initialize(AL_CONTEXT_PARAM_DECL_SINGLE)
{
   _wm_info_list_head          = NULL;
   _sip_private_packet_handler = NULL;
}


void mesh_imcp_uninitialize(AL_CONTEXT_PARAM_DECL_SINGLE)
{
   _wm_info_request_list_t *temp;

   temp = _wm_info_list_head;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
   while (temp != NULL)
   {
      _wm_info_list_head = temp->next;
      al_heap_free(AL_CONTEXT temp);
      temp = _wm_info_list_head;
   }
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
}


static AL_INLINE void _send_wm_info_request(AL_CONTEXT_PARAM_DECL imcp_packet_info_t *pi)
{
   _wm_info_request_list_t *temp;

   temp = _wm_info_list_head;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
   while (temp != NULL)
   {
      if (AL_NET_ADDR_EQUAL(&temp->ds_mac, &pi->u.heart_beat.ds_mac))
      {
         break;
      }
      temp = temp->next;
   }

   if (temp == NULL)
   {
      temp = (_wm_info_request_list_t *)al_heap_alloc(AL_CONTEXT sizeof(_wm_info_request_list_t)AL_HEAP_DEBUG_PARAM);
      memset(temp, 0, sizeof(_wm_info_request_list_t));

      memcpy(&temp->ds_mac, &pi->u.heart_beat.ds_mac, sizeof(al_net_addr_t));

      temp->next = _wm_info_list_head;

      if (_wm_info_list_head != NULL)
      {
         _wm_info_list_head->prev = temp;
      }

      _wm_info_list_head = temp;
   }

   if (++temp->count <= _MAX_WM_INFO_REQUESTS)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,
                   "MESH_AP: Sending WM Info request to "AL_NET_ADDR_STR,
                   AL_NET_ADDR_TO_STR(&pi->u.heart_beat.ds_mac));

      mesh_imcp_send_packet_wm_info_request(AL_CONTEXT pi->net_if,
                                            &pi->u.heart_beat.ds_mac,
                                            _DS_MAC,
                                            &zeroed_net_addr,
                                            mesh_hardware_info->ds_if_type,
                                            mesh_hardware_info->ds_if_sub_type);
   }
   else
   {
      /** Avoid overflow */
      temp->count = _MAX_WM_INFO_REQUESTS;
   }
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
}


struct _hb_enum_data
{
   int                ap_in_question;
   int                dhcp_r_value_change_required;
   imcp_packet_info_t *pi;
   int                rssi;
};

typedef struct _hb_enum_data   _hb_enum_data_t;

static int _heartbeat_parents_enum_func(AL_CONTEXT_PARAM_DECL void *enum_param, parent_t *parent)
{
   _hb_enum_data_t        *data;
   mesh_table_entry_t     *entry = NULL;
   al_802_11_operations_t *ex_op;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
   data = (_hb_enum_data_t *)enum_param;
   if (data == NULL || parent == NULL) {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESHAP : Data = %p parent = %p %s<%d>\n", data, parent, __func__, __LINE__);
      return 0;
   }
   if ((data->pi->u.heart_beat.sqnr > parent->sqnr) &&
       !(parent->flags & MESH_PARENT_FLAG_QUESTION))
   {
      parent->crssi         = data->pi->u.heart_beat.crssi;
      parent->tree_bit_rate = IMCP_SNIP_CTC_GET_TREE_LINK_RATE(data->pi->u.heart_beat.ctc);
      parent->hpc           = data->pi->u.heart_beat.hpc;
      parent->hbi           = data->pi->u.heart_beat.hbi;
      parent->last_hb_time  = al_get_tick_count(AL_CONTEXT_SINGLE);
      parent->sqnr          = data->pi->u.heart_beat.sqnr;
      //DEBUG:::MODE IMPORTANT TODO::: fix it soon
      //parent->mode			= IMCP_SNIP_CTC_GET_MODE(data->pi->u.heart_beat.crssi);

      memcpy(parent->root_bssid.bytes, data->pi->u.heart_beat.root_bssid.bytes, MAC_ADDR_SIZE);
      parent->root_bssid.length = MAC_ADDR_SIZE;

      if (parent == current_parent)
      {
         if (!_IS_MOBILE)
         {
            parent->signal = ((parent->signal * 3) + data->rssi) / 4;
         }
      }
      else
      {
         entry = mesh_table_entry_find(AL_CONTEXT & data->pi->u.heart_beat.ds_mac);
         if (entry != NULL)
         {
            parent->flags |= MESH_PARENT_FLAG_CHILD;
         }
         else
         {
            parent->flags &= ~MESH_PARENT_FLAG_CHILD;
         }
      }

      if (IMCP_SNIP_CTC_GET_MOBILE(data->pi->u.heart_beat.ctc))
      {
         parent->flags |= MESH_PARENT_FLAG_MOBILE;
      }
      else
      {
         parent->flags &= ~MESH_PARENT_FLAG_MOBILE;
      }

      parent->flags &= ~MESH_PARENT_FLAG_DISABLED;

      if (_DISJOINT_ADHOC_MODE && IMCP_SNIP_CRSSI_GET_VALID(parent->crssi))
      {
         if (IMCP_SNIP_CRSSI_GET_FUNCTIONALITY(parent->crssi))
         {
            parent->flags |= MESH_PARENT_FLAG_LIMITED;

         }
         else
         {
            parent->flags &= ~MESH_PARENT_FLAG_LIMITED;
         }
         if (mesh_config->dhcp_r_value == IMCP_SNIP_CRSSI_GET_DHCP_R_VALUE(parent->crssi))
         {
            data->dhcp_r_value_change_required = 1;
         }

         if (parent == current_parent)
         {
            /**
             * Set our functionality mode according to the current_parent's hpc and LIMITED flag
             */
            if (current_parent->flags & MESH_PARENT_FLAG_LIMITED)
            {
               if (!_IS_LIMITED)
               {
				      func_mode = _FUNC_MODE_LFN;
				      mesh_mode = _MESH_AP_RELAY;
                  printk(KERN_EMERG "Moving state to evaluate adhoc: %s:%d\n", __func__, __LINE__);
                  if (mesh_hardware_info->monitor_type == MONITOR_TYPE_ABSENT)
                      mesh_ng_fsm_send_event(AL_CONTEXT MESH_NG_FSM_EVENT_EVALUATE_ADHOC, NULL);
				      mesh_ng_fsm_update_downlink_vendor_info();
               }
            }
            else
            {
               if (_IS_LIMITED)
               {
				      func_mode = _FUNC_MODE_FFN;
				      mesh_mode = _MESH_AP_RELAY;
                  printk(KERN_EMERG "Moving state to evaluate adhoc: %s:%d\n", __func__, __LINE__);
                  if (mesh_hardware_info->monitor_type == MONITOR_TYPE_ABSENT)
                      mesh_ng_fsm_send_event(AL_CONTEXT MESH_NG_FSM_EVENT_EVALUATE_ADHOC, NULL);
				      mesh_ng_fsm_update_downlink_vendor_info();
               }
            }
         }
      }
   }
   else
   {
      parent->sqnr = data->pi->u.heart_beat.sqnr;
      if (parent != current_parent)
      {
         parent->flags       |= MESH_PARENT_FLAG_QUESTION;
         parent->flags       |= MESH_PARENT_FLAG_DISABLED;
         parent->last_hb_time = al_get_tick_count(AL_CONTEXT_SINGLE);
         data->ap_in_question = 1;
      }
   }

   /* Send disconnect to current parent if its operating in limited mode
    * and node's functional mode is non disjoint.
    */
   if (!_DISJOINT_ADHOC_MODE && IMCP_SNIP_CRSSI_GET_FUNCTIONALITY(parent->crssi))
   {
      mesh_ng_fsm_send_event(AL_CONTEXT MESH_NG_FSM_EVENT_DISCONNECT, NULL);
   }
   mesh_table_entry_release(AL_CONTEXT entry);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
   return 0;
}


int mesh_imcp_process_packet_heartbeat(AL_CONTEXT_PARAM_DECL imcp_packet_info_t *pi, int rssi)
{
   unsigned long   flags;
   int             retval;
   int             i;
   _hb_enum_data_t data;
   int             ret;

   retval = ACCESS_POINT_PACKET_DECISION_SEND_WM_DS;

   AL_ASSERT("MESH_AP	: mesh_imcp_process_packet_heartbeat", pi != NULL);

   if (mesh_state != _MESH_STATE_RUNNING)
   {
      retval = ACCESS_POINT_PACKET_DECISION_DROP;
      goto _out;
   }

   /* if self heartbeat or tree-childthen do nothing */

   if (_CMP_MAC_ADDR((&pi->u.heart_beat.ds_mac), _DS_MAC))
   {
      retval = ACCESS_POINT_PACKET_DECISION_DROP;
      goto _out;
   }

   if ((pi->net_if == _DS_NET_IF) &&
       (mesh_ap_is_table_entry_present(AL_CONTEXT & pi->u.heart_beat.ds_mac) == 0))
   {
      retval = ACCESS_POINT_PACKET_DECISION_DROP;
      goto _out;
   }

   if (pi->net_if == _DS_NET_IF)
   {
      if (mesh_hb_is_already_processed(AL_CONTEXT & pi->u.heart_beat.ds_mac, pi->u.heart_beat.sqnr) == RETURN_SUCCESS)
      {
         if (pi->u.heart_beat.sqnr > 20)
         {
            retval = ACCESS_POINT_PACKET_DECISION_DROP;
            goto _out;
         }
         /* Allow HB SQNR 0-20 for taking care of reboots */
      }
   }

   if ((mesh_mode != _MESH_AP_RELAY) || (mesh_hardware_info->monitor_type != MONITOR_TYPE_ABSENT))
   {
      goto _after_process;
   }

   data.ap_in_question = 0;
   data.pi             = pi;
   data.rssi           = rssi;
   data.dhcp_r_value_change_required = 0;

   ret = mesh_ng_enumerate_bssid_list(AL_CONTEXT & pi->u.heart_beat.ds_mac,
                                      (void *)&data,
                                      _heartbeat_parents_enum_func);
   if (ret != 0)
   {
      for (i = 0; i < pi->u.heart_beat.known_ap_count && pi->u.heart_beat.known_aps_info != NULL; i++)
      {
         if (_CMP_MAC_ADDR(&pi->u.heart_beat.known_aps_info[i].ap_ds_mac, _DS_MAC))
         {
            if (!mesh_ng_is_ds_phy_sub_type_compatible(AL_CONTEXT mesh_hardware_info->ds_if_sub_type, IMCP_SNIP_CTC_GET_DS_IF_SUB_TYPE(pi->u.heart_beat.ctc)))
            {
               break;
            }

            data.ap_in_question = 1;

            break;
         }
      }
   }

   if (!_IS_LIMITED)
   {
      if (data.ap_in_question)
      {
         _send_wm_info_request(AL_CONTEXT pi);
      }
   }


_after_process:

   if (_IS_LIMITED && !_IS_MOBILE && !_IS_UPLINK_SCAN_ENABLED && !mesh_config->use_virt_if)
   {
      /**
       * if _DISJOINT_ADHOC_MODE_INFRA_BEGIN is set, then
       * we should not change the downlink channel
       */

      if (!_DISJOINT_ADHOC_MODE_INFRA_BEGIN)
      {
         if (mesh_hop_level != current_parent->hpc + 1)
         {
            mesh_ng_downlink_channel_change_start(current_parent->hpc + 1);
         }
      }

      if (data.dhcp_r_value_change_required)
      {
         /** ADHOC_TODO: */
      }
   }

   /* if packet is from WM net if  then process immidiate stas */
   if (pi->net_if != _DS_NET_IF)
   {
      access_point_update_sta_last_rx(AL_CONTEXT & pi->u.heart_beat.ds_mac, 0);



      mesh_table_process_heart_beat(AL_CONTEXT & pi->u.heart_beat.ds_mac,
                                    pi->u.heart_beat.children_count,
                                    pi->u.heart_beat.immidigiate_sta);
   }

/*
 *      for(i = 0; i < pi->u.heart_beat.children_count && pi->u.heart_beat.immidigiate_sta != NULL; i++) {
 *
 *              sta_entry = mesh_table_entry_find(AL_CONTEXT &pi->u.heart_beat.immidigiate_sta[i]);
 *              if(sta_entry == NULL)
 *                      continue;
 *
 *              //check if sta entry exists & it is immediate child then call ap_disassoc
 *              if(AL_NET_ADDR_EQUAL(&sta_entry->sta_addr, &sta_entry->relay_ap_addr)) {
 *                      access_point_disassociate_sta(AL_CONTEXT &sta_entry->sta_addr);
 *
 *                      al_disable_interrupts(flags);
 *                      mesh_table_entry_remove_lock(AL_CONTEXT &sta_entry->sta_addr,3);
 *                      al_enable_interrupts(flags);
 *
 *                      mesh_imcp_send_packet_node_move_notification(AL_CONTEXT &pi->u.heart_beat.immidigiate_sta[i]);
 *              }
 *
 *      }
 */

_out:

   mesh_plugin_post_event(MESH_PLUGIN_EVENT_HEARTBEAT_RECEIVED, pi);

   if (pi->u.heart_beat.known_aps_info != NULL)
   {
      al_heap_free(AL_CONTEXT pi->u.heart_beat.known_aps_info);
   }

   if (pi->u.heart_beat.immidigiate_sta != NULL)
   {
      al_heap_free(AL_CONTEXT pi->u.heart_beat.immidigiate_sta);
   }

   return retval;
}


int mesh_imcp_process_packet_sta_assoc_notification(AL_CONTEXT_PARAM_DECL imcp_packet_info_t *pi)
{
   al_net_addr_t      relay_addr;
   mesh_table_entry_t *entry = NULL;
   mesh_table_entry_t *sender_entry = NULL;
   int                find_result;
   unsigned long      flags;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
   AL_ASSERT("MESH_AP	: mesh_imcp_process_packet_sta_assoc_notification", pi != NULL);

   if (mesh_state != _MESH_STATE_RUNNING)
   {
   al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR," mesh_is_running :: func : %s LINE : %d\n", __func__,__LINE__);
      return ACCESS_POINT_PACKET_DECISION_SEND_WM_DS;
   }

   /* if self assoc notification then do nothing */

   if (_CMP_MAC_ADDR((&pi->u.sta_assoc_notification.ap_ds_mac), _DS_MAC))
   {
   al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR," dropping pkt :: func : %s LINE : %d\n", __func__,__LINE__);
      return ACCESS_POINT_PACKET_DECISION_DROP;
   }

   if ((pi->net_if == _DS_NET_IF) &&
       (mesh_ap_is_table_entry_present(AL_CONTEXT & pi->u.sta_assoc_notification.ap_ds_mac) == 0))
   {
   al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR," dropping pkt :: func : %s LINE : %d\n", __func__,__LINE__);
      return ACCESS_POINT_PACKET_DECISION_DROP;
   }

   if (AL_NET_IF_IS_WIRELESS(_DS_NET_IF) &&
       AL_NET_ADDR_EQUAL(&pi->u.sta_assoc_notification.sta_mac, &current_parent->ds_mac) &&
       (pi->net_if != _DS_NET_IF))
   {
      /**
       * A loop has been detected, and hence we need to reboot to
       * break the loop
       */
      printk(KERN_INFO, "Sending DISCONNECT EVENT %s: %d  ....\n", __func__, __LINE__);
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MESH_AP : In-Direct Loop detected , disconnecting...");
      mesh_network_unstable = 1;
      mesh_ng_fsm_send_event(AL_CONTEXT MESH_NG_FSM_EVENT_DISCONNECT, NULL);
      al_set_event(AL_CONTEXT mesh_thread_wait_event);
   }

   /* find entry if already exist */
   entry = mesh_table_entry_find(AL_CONTEXT & pi->u.sta_assoc_notification.sta_mac);

   /* first check from where the packet came */
   if (pi->net_if == _DS_NET_IF)
   {
      /* if came from DS then remove entry */
      if (entry != NULL)
      {
         sender_entry = mesh_table_entry_find(AL_CONTEXT & pi->u.sta_assoc_notification.ap_ds_mac);

         if (sender_entry == NULL)
         {
            /* posible parent enable if exist in list */
            mesh_ng_set_parent_as_child(AL_CONTEXT & pi->u.sta_assoc_notification.sta_mac, 0, 0);

            /* check if sta entry exists & it is immediate child then call ap_disassoc */
            if (AL_NET_ADDR_EQUAL(&entry->sta_addr, &entry->relay_ap_addr))
            {
               access_point_disassociate_sta(AL_CONTEXT & entry->sta_addr);
            }

            mesh_table_entry_release(AL_CONTEXT entry);
            mesh_table_entry_remove_lock(AL_CONTEXT & entry->sta_addr, 3);
         }
         else
         {
            mesh_table_entry_release(AL_CONTEXT sender_entry);
            mesh_table_entry_release(AL_CONTEXT entry);
            al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR," dropping pkt :: func : %s LINE : %d\n", __func__,__LINE__);
            return ACCESS_POINT_PACKET_DECISION_DROP;
         }
      }
   }
   else
   {
      /* if came from WM then remove entry */
      al_net_if_t *relay_net_if = NULL;

      if (entry != NULL)
      {
         /* check if sta entry exists & it is immediate child then call ap_disassoc */
         if (AL_NET_ADDR_EQUAL(&entry->sta_addr, &entry->relay_ap_addr))
         {
            access_point_disassociate_sta(AL_CONTEXT & entry->sta_addr);
         }
         printk(KERN_EMERG "---> <%s><%d> mesh_table_entry_remove_lock", __func__, __LINE__);
         mesh_table_entry_release(AL_CONTEXT entry);
         mesh_table_entry_remove_lock(AL_CONTEXT & entry->sta_addr, 4);
      }
      find_result = mesh_table_find_relay_ap_for_sta_ap(AL_CONTEXT & pi->u.sta_assoc_notification.ap_ds_mac,
                                                        &relay_net_if,
                                                        &relay_addr);

      if (!find_result)
      {
         mesh_table_entry_add_lock(AL_CONTEXT pi->net_if,
                              &pi->u.sta_assoc_notification.sta_mac,
                              &relay_addr,
                              &(pi->u.sta_assoc_notification.ap_ds_mac),
                              2);
      }

   }

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
   return ACCESS_POINT_PACKET_DECISION_SEND_WM_DS;
}


int mesh_imcp_process_packet_sta_disassoc_notification(AL_CONTEXT_PARAM_DECL imcp_packet_info_t *pi)
{
   al_net_addr_t relay_addr;
   int           find_result;
   unsigned long flags;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
   AL_ASSERT("MESH_AP	: mesh_imcp_process_packet_sta_disassoc_notification", pi != NULL);

   if (mesh_state != _MESH_STATE_RUNNING)
   {
            al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR," dropping pkt :: func : %s LINE : %d\n", __func__,__LINE__);
      return ACCESS_POINT_PACKET_DECISION_SEND_WM_DS;
   }

   /* if self dis-assoc notification then do nothing */

   if (_CMP_MAC_ADDR((&pi->u.sta_disassoc_notification.ap_ds_mac), _DS_MAC))
   {
            al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR," dropping pkt :: func : %s LINE : %d\n", __func__,__LINE__);
      return ACCESS_POINT_PACKET_DECISION_DROP;
   }

   if ((pi->net_if == _DS_NET_IF) &&
       (mesh_ap_is_table_entry_present(AL_CONTEXT & pi->u.sta_disassoc_notification.ap_ds_mac) == 0))
   {
            al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR," dropping pkt :: func : %s LINE : %d\n", __func__,__LINE__);
      return ACCESS_POINT_PACKET_DECISION_DROP;
   }

   /* posible parent enable if exist in list */

   mesh_ng_set_parent_as_child(AL_CONTEXT & pi->u.sta_disassoc_notification.sta_mac, 0, 0);

   /* first check from where the packet came */
   if (pi->net_if == _DS_NET_IF)
   {
      /* if came from DS then remove entry */
            al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR," dropping pkt :: func : %s LINE : %d\n", __func__,__LINE__);
      return ACCESS_POINT_PACKET_DECISION_DROP;
   }
   else
   {
      al_net_if_t *relay_net_if = NULL;

      find_result = mesh_table_find_relay_ap_for_sta_ap(AL_CONTEXT & pi->u.sta_disassoc_notification.ap_ds_mac,
                                                        &relay_net_if,
                                                        &relay_addr);
      if ((!find_result) && (relay_net_if != pi->net_if))
      {
            al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR," dropping pkt :: func : %s LINE : %d\n", __func__,__LINE__);
         return ACCESS_POINT_PACKET_DECISION_DROP;
      }

      find_result = mesh_table_find_relay_ap_for_sta_ap(AL_CONTEXT & pi->u.sta_disassoc_notification.sta_mac,
                                                        &relay_net_if,
                                                        &relay_addr);

      if (!find_result)
      {
         if (!AL_NET_ADDR_EQUAL(&relay_addr, &pi->u.sta_disassoc_notification.sta_mac))
         {
            al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,
                         "MESH_AP	STA DISASSOC: A1:"AL_NET_ADDR_STR" A2:"AL_NET_ADDR_STR "",
                         AL_NET_ADDR_TO_STR(&pi->u.sta_disassoc_notification.sta_mac),
                         AL_NET_ADDR_TO_STR(&relay_addr));
            if (_IGMP_SUPPORT)
            {
               access_point_remove_sta_from_multicast_table(pi->net_if, &pi->u.sta_disassoc_notification.sta_mac);
            }
            mesh_table_entry_remove_lock(AL_CONTEXT & pi->u.sta_disassoc_notification.sta_mac, 5);
         }
      }
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
      return ACCESS_POINT_PACKET_DECISION_SEND_DS;
   }
}


int mesh_imcp_process_packet_channel_scan_lock(AL_CONTEXT_PARAM_DECL imcp_packet_info_t *pi)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
   AL_ASSERT("MESH_AP	: mesh_imcp_process_packet_channel_scan_lock", pi != NULL);

   if ((mesh_state == _MESH_STATE_SEEKING_NEW_PARENT) || (mesh_state == _MESH_STATE_NOT_STARTED))
   {
       al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR," dropping pkt :: func : %s LINE : %d\n", __func__,__LINE__);
      return ACCESS_POINT_PACKET_DECISION_SEND_WM_DS;
   }

   /* if self heartbeat then do nothing */
   if (_CMP_MAC_ADDR((&pi->u.channel_scan_lock.ds_mac), _DS_MAC))
   {
       al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR," dropping pkt :: func : %s LINE : %d\n", __func__,__LINE__);
      return ACCESS_POINT_PACKET_DECISION_DROP;
   }

   _PRINT_HW_ADDRESS("MESH_AP	: Channel Lock from = ", (&pi->u.channel_scan_lock.ds_mac));

   if (pi->u.channel_scan_lock.phy_id == scanning_current_phy_id)
   {
      if ((scan_state == _CHANNEL_SCAN_STATE_SCANNING) ||
          (scan_state == _CHANNEL_SCAN_STATE_WAIT_OBJECTION))
      {
         al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MESH_AP	: Sending scan lock objection and randomizing");
         mesh_imcp_send_packet_channel_scan_lock_objection(AL_CONTEXT_SINGLE);
         scan_state = _CHANNEL_SCAN_STATE_WAIT_RANDOM;
         al_set_event(AL_CONTEXT mesh_scan_wait_objection_event);
      }
   }
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
   return ACCESS_POINT_PACKET_DECISION_SEND_WM_DS;
}


int mesh_imcp_process_packet_channel_scan_unlock(AL_CONTEXT_PARAM_DECL imcp_packet_info_t *pi)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
   AL_ASSERT("MESH_AP	: mesh_imcp_process_packet_channel_scan_unlock", pi != NULL);

   /* if self heartbeat then do nothing */
   if (_CMP_MAC_ADDR((&pi->u.channel_scan_unlock.ds_mac), _DS_MAC))
   {
       al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR," dropping pkt :: func : %s LINE : %d\n", __func__,__LINE__);
      return ACCESS_POINT_PACKET_DECISION_DROP;
   }

#ifdef __DUMP_IMCP_MESSAGE_MAC_ADDR
   _PRINT_HW_ADDRESS_DEBUG("MESH_AP	: Channel UNLock from = ", (&pi->u.channel_scan_unlock.ds_mac));
#endif

   if (scan_state != _CHANNEL_SCAN_STATE_START)
   {
      al_set_event(AL_CONTEXT mesh_scan_wait_unlock_event);
   }
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
   return ACCESS_POINT_PACKET_DECISION_SEND_WM_DS;
}


int mesh_imcp_process_packet_channel_scan_lock_objection(AL_CONTEXT_PARAM_DECL imcp_packet_info_t *pi)
{
   AL_ASSERT("MESH_AP	: mesh_imcp_process_packet_channel_scan_lock_objection", pi != NULL);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
   /* if self heartbeat then do nothing */
   if (_CMP_MAC_ADDR((&pi->u.channel_scan_lock_objection.ds_mac), _DS_MAC))
   {
       al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR," dropping pkt :: func : %s LINE : %d\n", __func__,__LINE__);
      return ACCESS_POINT_PACKET_DECISION_DROP;
   }

   if (scan_state == _CHANNEL_SCAN_STATE_WAIT_OBJECTION)
   {
      scan_state = _CHANNEL_SCAN_STATE_WAIT_UNLOCK;
      _PRINT_HW_ADDRESS("MESH_AP	: Channel Lock Objection from = ", (&pi->u.channel_scan_lock_objection.ds_mac));
      al_set_event(AL_CONTEXT mesh_scan_wait_objection_event);
   }
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
   return ACCESS_POINT_PACKET_DECISION_SEND_WM_DS;
}


int mesh_imcp_process_packet_hardware_info_request(AL_CONTEXT_PARAM_DECL imcp_packet_info_t *pi)
{
   AL_ASSERT("MESH_AP	: mesh_imcp_process_packet_hardware_info_request", pi != NULL);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
   if (mesh_state != _MESH_STATE_RUNNING)
   {
       al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
      return ACCESS_POINT_PACKET_DECISION_SEND_WM_DS;
   }

   /* if self heartbeat then do nothing */
   if (!_CMP_MAC_ADDR((&pi->u.hardware_info_request.ds_mac), _DS_MAC))
   {
       al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
      return ACCESS_POINT_PACKET_DECISION_SEND_WM_DS;
   }

   if (mesh_state == _MESH_STATE_RUNNING)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MESH_AP: Sending HW Info response\n");
      mesh_imcp_send_packet_hardware_info(AL_CONTEXT_SINGLE);
   }

   al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR," dropping pkt :: func : %s LINE : %d\n", __func__,__LINE__);
   return ACCESS_POINT_PACKET_DECISION_DROP;
}


int mesh_imcp_process_packet_ap_sub_tree_info(AL_CONTEXT_PARAM_DECL imcp_packet_info_t *pi)
{
   unsigned long flags;
   unsigned char i = 0;
   al_net_addr_t relay_ap_addr;
   al_net_if_t   *relay_net_if = NULL;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
   if (mesh_state != _MESH_STATE_RUNNING)
   {
       al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
      return ACCESS_POINT_PACKET_DECISION_SEND_WM_DS;
   }

   if (_CMP_MAC_ADDR((&pi->u.sub_tree_info.ds_mac), _DS_MAC))
   {
       al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR," dropping pkt :: func : %s LINE : %d\n", __func__,__LINE__);
      return ACCESS_POINT_PACKET_DECISION_DROP;
   }

   if ((pi->net_if == _DS_NET_IF) &&
       (mesh_ap_is_table_entry_present(AL_CONTEXT & pi->u.sub_tree_info.ds_mac) == 0))
   {
       al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR," dropping pkt :: func : %s LINE : %d\n", __func__,__LINE__);
      return ACCESS_POINT_PACKET_DECISION_DROP;
   }

   /* first check if notification came from WM */
   if (pi->net_if == _DS_NET_IF)
   {
      /* remove all existing entries */
      for (i = 0; i < pi->u.sub_tree_info.sta_count; i++)
      {
         mesh_ng_set_parent_as_child(AL_CONTEXT & pi->u.sub_tree_info.sta_entries[(i * 2) + 1], 0, 0);
         mesh_table_entry_remove_lock(AL_CONTEXT & pi->u.sub_tree_info.sta_entries[(i * 2) + 1], 6);
      }
   }
   else
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MESH_AP  : Before Processing sub-tree information packet from " AL_NET_ADDR_STR, AL_NET_ADDR_TO_STR(&pi->u.sub_tree_info.ds_mac));
      if (!mesh_table_find_relay_ap_for_sta_ap(AL_CONTEXT & pi->u.sub_tree_info.ds_mac, &relay_net_if, &relay_ap_addr))
      {
         al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MESH_AP  : Processing sub-tree information packet from " AL_NET_ADDR_STR, AL_NET_ADDR_TO_STR(&pi->u.sub_tree_info.ds_mac));
         mesh_table_update_sta_entries(AL_CONTEXT pi->net_if, &pi->u.sub_tree_info.ds_mac, &relay_ap_addr, pi->u.sub_tree_info.sta_entries, pi->u.sub_tree_info.sta_count);
      }
   }

   al_heap_free(AL_CONTEXT pi->u.sub_tree_info.sta_entries);

       al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
   if (mesh_mode == _MESH_AP_ROOT)
   {
      if (pi->net_if == _DS_NET_IF)
      {
         return ACCESS_POINT_PACKET_DECISION_SEND_WM;
      }
      else
      {
         return ACCESS_POINT_PACKET_DECISION_SEND_WM_DS;
      }
   }
   else
   {
      if (pi->net_if == _DS_NET_IF)
      {
         return ACCESS_POINT_PACKET_DECISION_SEND_WM;
      }
      else
      {
         return ACCESS_POINT_PACKET_DECISION_SEND_WM_DS;
      }
   }
}


static AL_INLINE void _process_las_interval(AL_CONTEXT_PARAM_DECL_SINGLE)
{
   mobility_index_data_t mobility_data;
   int mobility_index;
   int mobility_conf_handle;
   int i;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
   mobility_conf_handle = al_get_mobility_conf_handle(AL_CONTEXT_SINGLE);

   mesh_config->mobile_mode = (mesh_config->las_interval >> 7);

   if (mesh_config->mobile_mode)
   {
      int max_index;

      mobility_index = mesh_config->las_interval & 0x7F;

      max_index = mobility_conf_get_index_count(AL_CONTEXT mobility_conf_handle);

      if (mobility_index > max_index - 1)
      {
         mobility_index = max_index - 1;
      }
   }
   else
   {
      mobility_index = 0;
   }

   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MESH_AP	: Mobility using index %d", mobility_index);

   mobility_conf_get_index_data(AL_CONTEXT mobility_conf_handle, mobility_index, &mobility_data);

   mobility_conf_close(AL_CONTEXT mobility_conf_handle);

   mesh_config->round_robin_beacon_count = mobility_data.round_robin_beacon_count;
   mesh_config->scan_count                = mobility_data.scan_count;
   mesh_config->scanner_dwell_interval    = mobility_data.scanner_dwell_interval;
   mesh_config->sampling_t_next_min       = mobility_data.sampling_t_next_min;
   mesh_config->sampling_t_next_max       = mobility_data.sampling_t_next_max;
   mesh_config->sampling_n_p              = mobility_data.sampling_n_p;
   mesh_config->disconnect_count_max      = mobility_data.disconnect_count_max;
   mesh_config->disconnect_multiplier     = mobility_data.disconnect_multiplier;
   mesh_config->evaluation_damping_factor = mobility_data.evaluation_damping_factor;

   /**
    * Setup the rate control parameters for DS and WMs
    */

   mesh_hardware_info->ds_conf->t_el_max          = mobility_data.t_el_max;
   mesh_hardware_info->ds_conf->t_el_min          = mobility_data.t_el_min;
   mesh_hardware_info->ds_conf->qualification     = mobility_data.qualification;
   mesh_hardware_info->ds_conf->max_retry_percent = mobility_data.max_retry_percent;
   mesh_hardware_info->ds_conf->max_error_percent = mobility_data.max_error_percent;
   mesh_hardware_info->ds_conf->max_errors        = mobility_data.max_errors;
   mesh_hardware_info->ds_conf->min_retry_percent = mobility_data.min_retry_percent;
   mesh_hardware_info->ds_conf->min_qualification = mobility_data.min_qualification;
   mesh_hardware_info->ds_conf->initial_rate      = mobility_data.initial_rate_index;

   set_net_if_rate_ctrl_param(AL_CONTEXT _DS_NET_IF, mesh_hardware_info->ds_conf);

   for (i = 0; i < mesh_hardware_info->wm_net_if_count; i++)
   {
      mesh_hardware_info->wms_conf[i]->t_el_max          = mobility_data.t_el_max;
      mesh_hardware_info->wms_conf[i]->t_el_min          = mobility_data.t_el_min;
      mesh_hardware_info->wms_conf[i]->qualification     = mobility_data.qualification;
      mesh_hardware_info->wms_conf[i]->max_retry_percent = mobility_data.max_retry_percent;
      mesh_hardware_info->wms_conf[i]->max_error_percent = mobility_data.max_error_percent;
      mesh_hardware_info->wms_conf[i]->max_errors        = mobility_data.max_errors;
      mesh_hardware_info->wms_conf[i]->min_retry_percent = mobility_data.min_retry_percent;
      mesh_hardware_info->wms_conf[i]->min_qualification = mobility_data.min_qualification;
      mesh_hardware_info->wms_conf[i]->initial_rate      = mobility_data.initial_rate_index;
      set_net_if_rate_ctrl_param(AL_CONTEXT mesh_hardware_info->wms_conf[i]->net_if, mesh_hardware_info->wms_conf[i]);
   }
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
}


int mesh_imcp_process_packet_ap_config_update(AL_CONTEXT_PARAM_DECL imcp_packet_info_t *pi)
{
   int i, j;
   access_point_netif_config_info_t *netif_config_info;
   int                       netif_count;
   al_net_if_t               *net_if;
   al_net_if_t               *temp_net_if;
   int                       al_netif_count;
   al_802_11_operations_t    *ex_operations;
   al_802_11_tx_power_info_t power_info;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
   if (mesh_state != _MESH_STATE_RUNNING)
   {
       al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
      return ACCESS_POINT_PACKET_DECISION_SEND_WM_DS;
   }

   if (!_CMP_MAC_ADDR((&pi->u.ap_config_info.ds_mac), _DS_MAC))
   {
       al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
      return ACCESS_POINT_PACKET_DECISION_SEND_WM_DS;
   }

   if (pi->u.ap_config_info.req_res == 0)
   {
      memcpy(&mesh_config->preferred_parent, &pi->u.ap_config_info.pref_parent, sizeof(al_net_addr_t));
      memcpy(mesh_config->sig_map, &pi->u.ap_config_info.signal_map, 8);
      mesh_config->hbeat_interval   = pi->u.ap_config_info.hb_interval;                                         /* heart beat interval */
      mesh_config->hbeat_miss_count = pi->u.ap_config_info.hb_miss_count;                                       /* heart beat miss count */
      mesh_config->hope_cost        = pi->u.ap_config_info.hop_cost;                                            /* hop count */
      mesh_config->max_aw_hopes     = pi->u.ap_config_info.max_allow_hops;                                      /* max allowable hops */
      mesh_config->las_interval     = pi->u.ap_config_info.la_scan_interval;                                    /* location awareness scan interval */
      mesh_config->ch_res_th        = pi->u.ap_config_info.change_res_thr;                                      /* change resistance tradeoff */
      memcpy(mesh_config->name, pi->u.ap_config_info.name, pi->u.ap_config_info.name_length);
      mesh_config->name[pi->u.ap_config_info.name_length] = 0;
      memcpy(mesh_config->essid, pi->u.ap_config_info.essid, pi->u.ap_config_info.essid_length);
      mesh_config->essid[pi->u.ap_config_info.essid_length] = 0;
      mesh_config->beacon_int = pi->u.ap_config_info.beacon_int;
      mesh_config->fcc_certified_operation  = pi->u.ap_config_info.fcc_certified_operation;
      mesh_config->etsi_certified_operation = pi->u.ap_config_info.etsi_certified_operation;
      mesh_config->ds_tx_rate  = pi->u.ap_config_info.ds_tx_rate;
      mesh_config->ds_tx_power = pi->u.ap_config_info.ds_tx_power;



      /** TODO:
       *	1) Find a way to set them from AP Viewer
       *	OR
       *	2) Remove them
       * Commented out for now
       */

      //mesh_config->stay_awake_count	= pi->u.ap_config_info.stay_awake_count;	/* stay awake count */
      //mesh_config->bridge_ageing_time	= pi->u.ap_config_info.bridge_ageing_time;	/* bridge ageing time */

      netif_count    = pi->u.ap_config_info.interface_count;                                                            /* interface count */
      al_netif_count = al_get_net_if_count(AL_CONTEXT_SINGLE);

      netif_config_info = (access_point_netif_config_info_t *)al_heap_alloc(AL_CONTEXT
                                                                            sizeof(access_point_netif_config_info_t) * netif_count AL_HEAP_DEBUG_PARAM);

      memset(netif_config_info, 0, sizeof(access_point_netif_config_info_t) * netif_count);

      for (i = 0; i < netif_count; i++)
      {
         net_if = NULL;

         for (j = 0; j < al_netif_count; j++)
         {
            temp_net_if = al_get_net_if(AL_CONTEXT j);
            if (!strcmp(temp_net_if->name, pi->u.ap_config_info.interface_info[i].name))
            {
               net_if = temp_net_if;
               break;
            }
         }

         netif_config_info[i].net_if          = net_if;
         netif_config_info[i].service         = pi->u.ap_config_info.interface_info[i].service_type;
         netif_config_info[i].txpower         = pi->u.ap_config_info.interface_info[i].tx_power;
         netif_config_info[i].rts_threshold   = pi->u.ap_config_info.interface_info[i].rts_thrd;
         netif_config_info[i].frag_threshold  = pi->u.ap_config_info.interface_info[i].frag_thrd;
         netif_config_info[i].beacon_interval = pi->u.ap_config_info.interface_info[i].beacon_int;
         netif_config_info[i].txrate          = pi->u.ap_config_info.interface_info[i].tx_rate;

         if ((netif_config_info[i].net_if == _DS_NET_IF) &&
             (mesh_mode == _MESH_AP_RELAY))
         {
            mesh_hardware_info->ds_if_tx_rate  = netif_config_info[i].txrate;
            mesh_hardware_info->ds_if_tx_power = netif_config_info[i].txpower;

            ex_operations = _DS_NET_IF->get_extended_operations(_DS_NET_IF);
            if (ex_operations != NULL)
            {
               power_info.flags = AL_802_11_TXPOW_PERC;
               power_info.power = netif_config_info[i].txpower;
               ex_operations->set_tx_power(_DS_NET_IF, &power_info);
               ex_operations->set_bit_rate(_DS_NET_IF, netif_config_info[i].txrate);
            }
         }

         strcpy(netif_config_info[i].essid, pi->u.ap_config_info.interface_info[i].essid);


         for (j = 0; j < mesh_config->if_count; j++)
         {
            if (!strcmp(pi->u.ap_config_info.interface_info[i].name, mesh_config->if_info[j].name))
            {
               mesh_config->if_info[j].txpower = pi->u.ap_config_info.interface_info[i].tx_power;
               strcpy(mesh_config->if_info[j].essid, pi->u.ap_config_info.interface_info[i].essid);
               mesh_config->if_info[j].rts_th     = pi->u.ap_config_info.interface_info[i].rts_thrd;
               mesh_config->if_info[j].frag_th    = pi->u.ap_config_info.interface_info[i].frag_thrd;
               mesh_config->if_info[j].beacon_int = pi->u.ap_config_info.interface_info[i].beacon_int;
               mesh_config->if_info[j].txrate     = pi->u.ap_config_info.interface_info[i].tx_rate;
            }
         }
      }


/* Now configd will send the configuration value to set in the kernel */


#if 0
      access_point_set_values(AL_CONTEXT pi->u.ap_config_info.essid,
                              pi->u.ap_config_info.beacon_int,
                              pi->u.ap_config_info.rts_thrd,
                              pi->u.ap_config_info.frag_thrd,
                              mesh_config->hope_cost,
                              netif_count,
                              netif_config_info);
#endif

      al_heap_free(AL_CONTEXT netif_config_info);

      _process_las_interval(AL_CONTEXT_SINGLE);

      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MESH_AP	: ap_config_update imcp packet processed");

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
      return ACCESS_POINT_PACKET_DECISION_SEND_UP_IMCP;
   }
   else
   {
       al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
      return ACCESS_POINT_PACKET_DECISION_SEND_WM_DS;
   }
}


int mesh_imcp_process_packet_config_req_res(AL_CONTEXT_PARAM_DECL imcp_packet_info_t *pi)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
   return ACCESS_POINT_PACKET_DECISION_SEND_UP_IMCP;
}


int mesh_imcp_process_packet_mesh_imcp_key_update(AL_CONTEXT_PARAM_DECL imcp_packet_info_t *pi)
{
   if (mesh_state != _MESH_STATE_RUNNING)
   {
       al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
      return ACCESS_POINT_PACKET_DECISION_SEND_WM_DS;
   }

   if (!_CMP_MAC_ADDR((&pi->u.mesh_imcp_key_update.ds_mac), _DS_MAC))
   {
       al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
      return ACCESS_POINT_PACKET_DECISION_SEND_WM_DS;
   }

   SET_MESH_FLAG(MESH_FLAG_REBOOT_REQUIRED);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MESH_AP: Flags set :  MESH_FLAG_REBOOT_REQUIRED");

   /*
    *	This code is commented so that mesh key update will take effect after reboot
    *	configd will update the key into meshap.conf and changes will take effect after reboot
    */

/*
 *      mesh_config->meshid_length = pi->u.mesh_imcp_key_update.new_meshid_length;
 *      memcpy(mesh_config->meshid, pi->u.mesh_imcp_key_update.new_meshid,pi->u.mesh_imcp_key_update.new_meshid_length);
 *      mesh_config->meshid[pi->u.mesh_imcp_key_update.new_meshid_length] = 0;
 *
 *      mesh_config->mesh_imcp_key_length = pi->u.mesh_imcp_key_update.key_length;
 *      memcpy(mesh_config->mesh_imcp_key, pi->u.mesh_imcp_key_update.key ,pi->u.mesh_imcp_key_update.key_length);
 *      mesh_config->mesh_imcp_key[pi->u.mesh_imcp_key_update.key_length] = 0;
 */

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
   return ACCESS_POINT_PACKET_DECISION_SEND_UP_IMCP;
}


int mesh_imcp_process_packet_wm_info_request(AL_CONTEXT_PARAM_DECL imcp_packet_info_t *pi)
{
   int i;
   al_802_11_operations_t *operations;
   int wm_count;
   mesh_imcp_wm_info_list_t *head;
   mesh_imcp_wm_info_list_t *tail;
   mesh_imcp_wm_info_list_t *node;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
   if (mesh_state != _MESH_STATE_RUNNING)
   {
       al_print_log(AL_CONTEXT AL_LOG_TYPE_DEBUG," mesh is not running :: func : %s LINE : %d\n", __func__,__LINE__);
      return ACCESS_POINT_PACKET_DECISION_SEND_WM_DS;
   }

   if (!_CMP_MAC_ADDR((&pi->u.wm_info_request.ds_mac_dest), _DS_MAC))
   {
       al_print_log(AL_CONTEXT AL_LOG_TYPE_DEBUG," mac addr are same :: func : %s LINE : %d\n", __func__,__LINE__);
      return ACCESS_POINT_PACKET_DECISION_SEND_WM_DS;
   }

   head     = NULL;
   tail     = NULL;
   wm_count = 0;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,
                "MESH_AP: Received WM Info Request from "AL_NET_ADDR_STR,
                AL_NET_ADDR_TO_STR(&pi->u.wm_info_request.ds_mac_src));

   for (i = 0; i < mesh_hardware_info->wm_net_if_count; i++)
   {
      if (AL_NET_ADDR_EQUAL(&pi->u.wm_info_request.wm_mac_dest, &zeroed_net_addr))
      {
         if ((pi->u.wm_info_request.ds_type == mesh_hardware_info->wms_conf[i]->phy_type) &&
             mesh_ng_is_ds_phy_sub_type_compatible(AL_CONTEXT pi->u.wm_info_request.ds_sub_type, mesh_hardware_info->wms_conf[i]->phy_sub_type) &&
             (mesh_hardware_info->wms_conf[i]->service != AL_CONF_IF_SERVICE_CLIENT_ONLY))
         {
            goto _add;
         }
      }
      else
      {
         if (AL_NET_ADDR_EQUAL(&pi->u.wm_info_request.wm_mac_dest, &mesh_hardware_info->wms_conf[i]->net_if->config.hw_addr))
         {
            goto _add;
         }
      }
      continue;
_add:

      node = (mesh_imcp_wm_info_list_t *)al_heap_alloc(AL_CONTEXT sizeof(mesh_imcp_wm_info_list_t)AL_HEAP_DEBUG_PARAM);
      memset(node, 0, sizeof(mesh_imcp_wm_info_list_t));

      node->net_if = mesh_hardware_info->wms_conf[i]->net_if;
      memcpy(&node->wm_addr, &mesh_hardware_info->wms_conf[i]->net_if->config.hw_addr, sizeof(al_net_addr_t));

      operations = (al_802_11_operations_t *)node->net_if->get_extended_operations(node->net_if);

      if (operations != NULL)
      {
         node->wm_channel = operations->get_channel(node->net_if);
         operations->get_essid(node->net_if, node->wm_essid, AL_802_11_MAX_ESSID_LENGTH);
      }

      if (head == NULL)
      {
         head = node;
      }
      else
      {
         tail->next = node;
      }

      tail = node;

      ++wm_count;
   }

   if (wm_count != 0)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,
                   "MESH_AP: Sending WM Info Response with %d WMs to "AL_NET_ADDR_STR,
                   wm_count,
                   AL_NET_ADDR_TO_STR(&pi->u.wm_info_request.ds_mac_src));

      mesh_imcp_send_packet_wm_info_response(AL_CONTEXT pi->net_if,
                                             &pi->u.wm_info_request.ds_mac_src,
                                             _DS_MAC,
                                             wm_count,
                                             pi->u.wm_info_request.ds_type,
                                             pi->u.wm_info_request.ds_sub_type,
                                             head);

      node = head;
      while (node != NULL)
      {
         head = node->next;
         al_heap_free(AL_CONTEXT node);
         node = head;
      }
   }
   else
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,
                   "mesh_imcp_process_packet_wm_info_request: net_if was NULL\n"
                   "Sender "AL_NET_ADDR_STR " WM MAC "AL_NET_ADDR_STR,
                   AL_NET_ADDR_TO_STR(&pi->u.wm_info_request.ds_mac_src),
                   AL_NET_ADDR_TO_STR(&pi->u.wm_info_request.wm_mac_dest));
   }

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
   return ACCESS_POINT_PACKET_DECISION_DROP;
}


struct _wm_info_enum_param
{
   imcp_packet_info_t *pi;
};

typedef struct _wm_info_enum_param   _wm_info_enum_param_t;

static AL_INLINE void _create_new_parent(AL_CONTEXT_PARAM_DECL al_net_addr_t *ds_mac,
                                         al_net_addr_t                       *wm_mac,
                                         const char                          *essid,
                                         int                                 channel)
{
   parent_t           *parent;
   mesh_table_entry_t *entry = NULL;
   _parent_priv_t     *priv;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
   //DOUBLE_ENTRY
   priv =  _get_parent(AL_CONTEXT ds_mac, wm_mac);
   if (priv) {
      //printk(KERN_EMERG "Parent ENTRY is already present ...\n");
      parent = &priv->data;
      goto update;
   }

   parent = mesh_ng_create_parent(AL_CONTEXT ds_mac, wm_mac);

update:
   parent->hbi             = mesh_config->hbeat_interval;
   parent->last_hb_time    = al_get_tick_count(AL_CONTEXT_SINGLE);
   parent->sqnr            = 0;
   parent->signal          = 0;
   parent->tree_bit_rate   = mesh_hardware_info->ds_conf->initial_rate_mbps;
   parent->direct_bit_rate = mesh_hardware_info->ds_conf->initial_rate_mbps;
   parent->flags          |= MESH_PARENT_FLAG_DISABLED;
   parent->channel         = channel;

   strcpy(parent->essid, essid);

   entry = mesh_table_entry_find(AL_CONTEXT ds_mac);

   if (entry != NULL)
   {
      if (_DISJOINT_ADHOC_MODE)
      {
         al_net_addr_t *self_root_bssid;

         switch (func_mode)
         {
         case _FUNC_MODE_FFR:
            self_root_bssid = _DS_MAC;
            break;

         case _FUNC_MODE_LFR:
            self_root_bssid = _DS_MAC;
            parent->flags  |= MESH_PARENT_FLAG_LIMITED;
            break;

         case _FUNC_MODE_FFN:
            self_root_bssid = &current_parent->root_bssid;
            break;

         case _FUNC_MODE_LFN:
            self_root_bssid = &current_parent->root_bssid;
            parent->flags  |= MESH_PARENT_FLAG_LIMITED;
            break;

         default:
            self_root_bssid = _DS_MAC;
         }

         memcpy(&parent->root_bssid, &self_root_bssid, sizeof(al_net_addr_t));
      }

      parent->flags |= MESH_PARENT_FLAG_CHILD;
   }
   else
   {
      parent->flags &= ~MESH_PARENT_FLAG_CHILD;
   }

   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,
                "MESH_AP : Adding parent "AL_NET_ADDR_STR " ESSID %s CH %d RBSSID "AL_NET_ADDR_STR " (D%c%c)",
                AL_NET_ADDR_TO_STR(&parent->bssid),
                parent->essid,
                parent->channel,
                AL_NET_ADDR_TO_STR(&parent->root_bssid),
                parent->flags & MESH_PARENT_FLAG_CHILD ? 'C' : '-',
                parent->flags & MESH_PARENT_FLAG_LIMITED ? 'L' : '-');

   mesh_table_entry_release(AL_CONTEXT entry);
   mesh_ng_add_parent(AL_CONTEXT parent);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
}


static int _wm_info_bssid_func(AL_CONTEXT_PARAM_DECL void *enum_param, parent_t *parent)
{
   _wm_info_enum_param_t *param;
   int i;
   int found;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
   param = (_wm_info_enum_param_t *)enum_param;
   found = 0;

   if (AL_NET_ADDR_EQUAL(&parent->bssid, &param->pi->u.wm_info_response.wm_addr))
   {
      found = 1;

      parent->channel = param->pi->u.wm_info_response.wm_channel;
      strcpy(parent->essid, param->pi->u.wm_info_response.essid);
   }
   else
   {
      for (i = 0; i < param->pi->u.wm_info_response.other_wm_count; i++)
      {
         if (AL_NET_ADDR_EQUAL(&parent->bssid, &param->pi->u.wm_info_response.other_wm_info[i].wm_addr))
         {
            found           = 1;
            parent->channel = param->pi->u.wm_info_response.other_wm_info[i].wm_channel;
            strcpy(parent->essid, param->pi->u.wm_info_response.other_wm_info[i].essid);
            break;
         }
      }
   }

   if (found)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,
                   "MESH_AP : Enabling questioned parent "AL_NET_ADDR_STR " with essid %s CH %d",
                   AL_NET_ADDR_TO_STR(&parent->bssid),
                   parent->essid,
                   parent->channel);
      parent->flags &= ~MESH_PARENT_FLAG_QUESTION;
      parent->flags &= ~MESH_PARENT_FLAG_DISABLED;
      /** We need to sample this parent immedietly */
      mesh_ng_fsm_send_event(AL_CONTEXT MESH_NG_FSM_EVENT_CHECK_PARENT, (void *)parent);
      al_set_event(AL_CONTEXT mesh_thread_wait_event);
   }
   else
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,
                   "MESH_AP : Permanently disabling questioned parent "AL_NET_ADDR_STR "",
                   AL_NET_ADDR_TO_STR(&parent->bssid));
      parent->flags &= ~MESH_PARENT_FLAG_QUESTION;
      parent->flags |= MESH_PARENT_FLAG_DISABLED;
   }

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
   return 0;
}


int mesh_imcp_process_packet_wm_info_response(AL_CONTEXT_PARAM_DECL imcp_packet_info_t *pi)
{
   int ret;
   _wm_info_enum_param_t param;
   int i;
   _wm_info_request_list_t *temp;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
   if (mesh_state != _MESH_STATE_RUNNING)
   {
      ret = ACCESS_POINT_PACKET_DECISION_SEND_WM_DS;
      goto _out;
   }

   if (!_CMP_MAC_ADDR((&pi->u.wm_info_response.ds_mac_dest), _DS_MAC))
   {
      ret = ACCESS_POINT_PACKET_DECISION_SEND_WM_DS;
      goto _out;
   }

   ret = ACCESS_POINT_PACKET_DECISION_DROP;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,
                "MESH_AP: Received WM Info Response from "AL_NET_ADDR_STR,
                AL_NET_ADDR_TO_STR(&pi->u.wm_info_response.ds_mac_src));

   param.pi = pi;

   /**
    * Here we first enumerate through the BSSID list of the DS MAC
    */

   i = mesh_ng_enumerate_bssid_list(AL_CONTEXT & pi->u.wm_info_response.ds_mac_src, (void *)&param, _wm_info_bssid_func);

   if (i != 0)
   {
      /** Create new entries (all disabled) */

      _create_new_parent(AL_CONTEXT & pi->u.wm_info_response.ds_mac_src,
                         &pi->u.wm_info_response.wm_addr,
                         pi->u.wm_info_response.essid,
                         pi->u.wm_info_response.wm_channel);

      for (i = 0; i < pi->u.wm_info_response.other_wm_count; i++)
      {
         _create_new_parent(AL_CONTEXT & pi->u.wm_info_response.ds_mac_src,
                            &pi->u.wm_info_response.other_wm_info[i].wm_addr,
                            pi->u.wm_info_response.other_wm_info[i].essid,
                            pi->u.wm_info_response.other_wm_info[i].wm_channel);
      }
   }

   /** Free the request list item */

   temp = _wm_info_list_head;
   while (temp != NULL)
   {
      if (AL_NET_ADDR_EQUAL(&temp->ds_mac, &pi->u.wm_info_response.ds_mac_src))
      {
         if (temp->next != NULL)
         {
            temp->next->prev = temp->prev;
         }
         if (temp->prev != NULL)
         {
            temp->prev->next = temp->next;
         }
         if (temp == _wm_info_list_head)
         {
            _wm_info_list_head = temp->next;
         }
         al_heap_free(AL_CONTEXT temp);
         break;
      }
      temp = temp->next;
   }

_out:

   if (pi->u.wm_info_response.other_wm_count > 0)
   {
      al_heap_free(AL_CONTEXT pi->u.wm_info_response.other_wm_info);
   }

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
   return ret;
}


static int _mesh_reset_thread(AL_CONTEXT_PARAM_DECL al_thread_param_t *param)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
   al_set_thread_name(AL_CONTEXT "MESH_RESET");

   access_point_stop(AL_CONTEXT_SINGLE);

   access_point_start(AL_CONTEXT 17);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
   return RETURN_SUCCESS;
}


int mesh_imcp_process_packet_tester_testee_mode_request(AL_CONTEXT_PARAM_DECL imcp_packet_info_t *pi)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
   access_point_tester_testee_globals_t data;

   if (!access_point_set_tester_testee_mode(AL_CONTEXT pi->u.mesh_imcp_tester_testee_mode_req.mode))
   {
      memcpy(&data.ds_mac_address, &pi->u.mesh_imcp_tester_testee_mode_req.ds_mac, sizeof(al_net_addr_t));
      memcpy(&data.peer_mac_address, &pi->u.mesh_imcp_tester_testee_mode_req.peer_mac, sizeof(al_net_addr_t));
      data.ap_mode              = pi->u.mesh_imcp_tester_testee_mode_req.mode;
      data.interface_index      = pi->u.mesh_imcp_tester_testee_mode_req.interface_index;
      data.link_interface_index = pi->u.mesh_imcp_tester_testee_mode_req.link_interface_index;
      data.channel              = pi->u.mesh_imcp_tester_testee_mode_req.channel;
      data.analysis_interval    = pi->u.mesh_imcp_tester_testee_mode_req.analysis_interval;

      access_point_tester_testee_initialize(AL_CONTEXT & data);

      al_create_thread(AL_CONTEXT _mesh_reset_thread, pi, AL_THREAD_PRIORITY_NORMAL, "_mesh_reset_thread");

      al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
      return RETURN_SUCCESS;
   }

   al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR," pkt drop :: func : %s LINE : %d\n", __func__,__LINE__);
   return ACCESS_POINT_PACKET_DECISION_DROP;
}


int mesh_imcp_process_packet_reg_domain_cc_config_update(AL_CONTEXT_PARAM_DECL imcp_packet_info_t *pi)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
   if (mesh_state != _MESH_STATE_RUNNING)
   {
       al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
      return ACCESS_POINT_PACKET_DECISION_SEND_WM_DS;
   }

   if (!_CMP_MAC_ADDR((&pi->u.reg_domain_cc_info.ds_mac), _DS_MAC))
   {
       al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
      return ACCESS_POINT_PACKET_DECISION_SEND_WM_DS;
   }

   if (pi->u.reg_domain_cc_info.req_res == 0)
   {
      mesh_config->reg_domain   = pi->u.reg_domain_cc_info.reg_domain;
      mesh_config->country_code = pi->u.reg_domain_cc_info.country_code;

      al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
      return ACCESS_POINT_PACKET_DECISION_SEND_UP_IMCP;
   }
   else
   {
       al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
      return ACCESS_POINT_PACKET_DECISION_SEND_WM_DS;
   }
}


int mesh_imcp_process_packet_ack_timeout_config_update(AL_CONTEXT_PARAM_DECL imcp_packet_info_t *pi)
{
   int i, j, if_count, al_netif_count;
   al_802_11_operations_t *operations;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
   al_net_if_t *net_if;


   if (mesh_state != _MESH_STATE_RUNNING)
   {
      return ACCESS_POINT_PACKET_DECISION_SEND_WM_DS;
   }

   if (!_CMP_MAC_ADDR((&pi->u.ack_timeout.ds_mac), _DS_MAC))
   {
      return ACCESS_POINT_PACKET_DECISION_SEND_WM_DS;
   }

   if (pi->u.ack_timeout.req_res == 0)
   {
      if_count       = pi->u.ack_timeout.interface_count;                                                               /* interface count */
      al_netif_count = al_get_net_if_count(AL_CONTEXT_SINGLE);

      for (i = 0; i < if_count; i++)
      {
         net_if = NULL;

/* This is handled by confid now */
#if 0
         for (j = 0; j < al_netif_count; j++)
         {
            net_if = al_get_net_if(AL_CONTEXT j);

            if (net_if != NULL)
            {
               if (!strcmp(net_if->name, pi->u.ack_timeout.interface_info[i].name))
               {
                  operations = net_if->get_extended_operations(net_if);

                  if (operations != NULL)
                  {
                     operations->set_ack_timeout(net_if, pi->u.ack_timeout.interface_info[i].ack_timeout);
                  }
               }
            }
         }
#endif
         for (j = 0; j < mesh_config->if_count; j++)
         {
            if (!strcmp(pi->u.ack_timeout.interface_info[i].name, mesh_config->if_info[j].name))
            {
               mesh_config->if_info[j].ack_timeout = pi->u.ack_timeout.interface_info[i].ack_timeout;
            }
         }
      }

      al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
      return ACCESS_POINT_PACKET_DECISION_SEND_UP_IMCP;
   }
   else
   {
       al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
      return ACCESS_POINT_PACKET_DECISION_SEND_WM_DS;
   }
}


int mesh_imcp_process_packet_supported_channels_request(AL_CONTEXT_PARAM_DECL imcp_packet_info_t *pi)
{
   AL_ASSERT("MESH_AP	: mesh_imcp_process_packet_supported_channels_request", pi != NULL);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
   if (mesh_state != _MESH_STATE_RUNNING)
   {
      return ACCESS_POINT_PACKET_DECISION_SEND_WM_DS;
   }

   /* if self packet then do nothing */
   if (!_CMP_MAC_ADDR((&pi->u.supported_channels_request.ds_mac), _DS_MAC))
   {
      return ACCESS_POINT_PACKET_DECISION_SEND_WM_DS;
   }

   if (mesh_state == _MESH_STATE_RUNNING)
   {
      mesh_imcp_send_packet_supported_channels_info(pi);
   }

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
   return ACCESS_POINT_PACKET_DECISION_DROP;
}


static int _mesh_imcp_send_packet_sta_info(AL_CONTEXT_PARAM_DECL imcp_packet_info_t *pi_req)
{
   imcp_packet_info_t      *pi;
   access_point_sta_info_t sta;
   al_u64_t                current_time;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
   access_point_get_sta_info(AL_CONTEXT & (pi_req->u.sta_info_request.sta_mac), &sta);

   if (!AL_NET_ADDR_EQUAL(&sta.address, &pi_req->u.sta_info_request.sta_mac))
   {
       al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"AL_NET_ADDR_EQUAL -:ret -1 :: func : %s LINE : %d\n", __func__,__LINE__);
      return -1;
   }

   pi = imcp_snip_create_packet(AL_CONTEXT_SINGLE);

   memset(&(pi->u.sta_info), 0, sizeof(pi->u.sta_info));

   pi->packet_type = IMCP_SNIP_PACKET_TYPE_STA_INFO_RESPONSE;

   memcpy(&pi->u.sta_info.ds_mac, _DS_MAC, sizeof(al_net_addr_t));
   memcpy(&pi->u.sta_info.sta_mac, &(pi_req->u.sta_info_request.sta_mac), sizeof(al_net_addr_t));
   memcpy(&pi->u.sta_info.parent_mac, &(sta.net_if->config.hw_addr), sizeof(al_net_addr_t));

   access_point_get_sta_essid(AL_CONTEXT & (pi_req->u.sta_info_request.sta_mac), pi->u.sta_info.parent_essid, &(pi->u.sta_info.parent_essid_length));

   pi->u.sta_info.auth_state = sta.auth_state;
   pi->u.sta_info.b_client   = sta.b_client;
   pi->u.sta_info.is_imcp    = sta.is_imcp;
   memcpy(&(pi->u.sta_info.listen_interval), &(sta.listen_interval), sizeof(unsigned short));
   pi->u.sta_info.capability = sta.capability;
   pi->u.sta_info.key_index  = sta.key_index;
   memcpy(&(pi->u.sta_info.bytes_received), &(sta.bytes_received), sizeof(unsigned int));
   memcpy(&(pi->u.sta_info.bytes_sent), &(sta.bytes_transmitted), sizeof(unsigned int));

   current_time = al_get_tick_count(AL_CONTEXT);

   pi->u.sta_info.active_time = (unsigned int)((al_s64_t)(current_time - sta.association_time));

   imcp_snip_send(AL_CONTEXT pi);

   imcp_snip_release_packet(AL_CONTEXT pi);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
   return 0;
}


int mesh_imcp_process_packet_sta_info_request(AL_CONTEXT_PARAM_DECL imcp_packet_info_t *pi)
{
   AL_ASSERT("MESH_AP	: mesh_imcp_process_packet_sta_info_request", pi != NULL);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
   /* if not self packet then do nothing */
   if (!_CMP_MAC_ADDR((&pi->u.sta_info_request.ds_mac), _DS_MAC))
   {
       al_print_log(AL_CONTEXT AL_LOG_TYPE_DEBUG,"pkt send thtough wm ds :: func : %s LINE : %d\n", __func__,__LINE__);
      return ACCESS_POINT_PACKET_DECISION_SEND_WM_DS;
   }

   if (mesh_state != _MESH_STATE_RUNNING)
   {
       al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW," pkt drop :: func : %s LINE : %d\n", __func__,__LINE__);
      return ACCESS_POINT_PACKET_DECISION_DROP;
   }

   _mesh_imcp_send_packet_sta_info(pi);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"pkt drop :: func : %s LINE : %d\n", __func__,__LINE__);
   return ACCESS_POINT_PACKET_DECISION_DROP;
}


static int mesh_imcp_send_ct_mode_response(AL_CONTEXT_PARAM_DECL imcp_packet_info_t *pi_req, unsigned char mode)
{
   imcp_packet_info_t *pi;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
   pi = imcp_snip_create_packet(AL_CONTEXT_SINGLE);

   pi->packet_type = IMCP_SNIP_PACKET_TYPE_GENERIC_COMMAND_REQ_RES;

   memcpy(&pi->u.generic_command.ds_mac, _DS_MAC, sizeof(al_net_addr_t));
   pi->u.generic_command.req_res = IMCP_SNIP_GENERIC_COMMAND_RESPONSE;
   pi->u.generic_command.type    = mode;

   imcp_snip_send(AL_CONTEXT pi);

   imcp_snip_release_packet(AL_CONTEXT pi);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
   return 0;
}


static int mesh_imcp_send_beaconing_uplink_response(AL_CONTEXT_PARAM_DECL imcp_packet_info_t *pi_req, unsigned char mode)
{
   imcp_packet_info_t *pi;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
   pi = imcp_snip_create_packet(AL_CONTEXT_SINGLE);

   pi->packet_type = IMCP_SNIP_PACKET_TYPE_GENERIC_COMMAND_REQ_RES;

   memcpy(&pi->u.generic_command.ds_mac, _DS_MAC, sizeof(al_net_addr_t));
   pi->u.generic_command.req_res = IMCP_SNIP_GENERIC_COMMAND_RESPONSE;
   pi->u.generic_command.type    = mode;

   imcp_snip_send(AL_CONTEXT pi);

   imcp_snip_release_packet(AL_CONTEXT pi);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
   return 0;
}


static AL_INLINE int mesh_imcp_process_generic_command_ct_mode_request(AL_CONTEXT_PARAM_DECL imcp_packet_info_t *pi)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
   if (pi->u.generic_command.type == IMCP_SNIP_GENERIC_COMMAND_TYPE_SET_CTM)
   {
      if (!CHECK_MESH_FLAG(MESH_FLAG_CT_MODE))
      {
         mesh_config->ctmode_backup_hbi = mesh_config->hbeat_interval;
         mesh_config->hbeat_interval    = 1;
         SET_MESH_FLAG(MESH_FLAG_CT_MODE);
         mesh_imcp_send_ct_mode_response(AL_CONTEXT pi, IMCP_SNIP_GENERIC_COMMAND_TYPE_SET_CTM);
      }
   }
   else
   {
      if (CHECK_MESH_FLAG(MESH_FLAG_CT_MODE))
      {
         mesh_config->hbeat_interval = mesh_config->ctmode_backup_hbi;
         RESET_MESH_FLAG(MESH_FLAG_CT_MODE);
         mesh_imcp_send_ct_mode_response(AL_CONTEXT pi, IMCP_SNIP_GENERIC_COMMAND_TYPE_RESET_CTM);
      }
   }

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
   return ACCESS_POINT_PACKET_DECISION_DROP;
}


static AL_INLINE int mesh_imcp_process_generic_command_reject_clients_request(AL_CONTEXT_PARAM_DECL imcp_packet_info_t *pi_req)
{
   imcp_packet_info_t *pi;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
   if (CHECK_MESH_FLAG(MESH_FLAG_DENY_CLIENTS))
   {
       al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"D flag for client is set :: func : %s LINE : %d\n", __func__,__LINE__);
      return ACCESS_POINT_PACKET_DECISION_DROP;
   }

   pi = imcp_snip_create_packet(AL_CONTEXT_SINGLE);

   mesh_table_remove_all_entries(AL_CONTEXT_SINGLE);
   access_point_remove_all_sta(AL_CONTEXT_SINGLE);
   SET_MESH_FLAG(MESH_FLAG_DENY_CLIENTS);

   pi->packet_type = IMCP_SNIP_PACKET_TYPE_GENERIC_COMMAND_REQ_RES;

   memcpy(&pi->u.generic_command.ds_mac, _DS_MAC, sizeof(al_net_addr_t));
   pi->u.generic_command.req_res = IMCP_SNIP_GENERIC_COMMAND_RESPONSE;
   pi->u.generic_command.type    = IMCP_SNIP_GENERIC_COMMAND_TYPE_REJECT_CLIENTS;

   imcp_snip_send(AL_CONTEXT pi);

   imcp_snip_release_packet(AL_CONTEXT pi);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
   return ACCESS_POINT_PACKET_DECISION_DROP;
}


static AL_INLINE int mesh_imcp_process_generic_command_trigger_dnlink_dca_request(AL_CONTEXT_PARAM_DECL imcp_packet_info_t *pi)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
   return ACCESS_POINT_PACKET_DECISION_DROP;
}


static AL_INLINE int mesh_imcp_process_generic_command_trigger_uplink_scan_request(AL_CONTEXT_PARAM_DECL imcp_packet_info_t *pi)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
   return ACCESS_POINT_PACKET_DECISION_DROP;
}


static AL_INLINE int mesh_imcp_process_generic_command_downlink_saturation(AL_CONTEXT_PARAM_DECL imcp_packet_info_t *pi)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
   mesh_ng_saturation_execute(AL_CONTEXT_SINGLE);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
   return ACCESS_POINT_PACKET_DECISION_DROP;
}


static AL_INLINE int mesh_imcp_process_generic_command_beaconing_uplink_request(AL_CONTEXT_PARAM_DECL imcp_packet_info_t *pi)
{
   al_802_11_operations_t *ex_op;
   char vendor_info[256];
   char vendor_info_len;


   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
   if (AL_NET_IF_IS_ETHERNET(mesh_hardware_info->ds_net_if))
   {
       al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"pkt drop on wired path :: func : %s LINE : %d\n", __func__,__LINE__);
      return ACCESS_POINT_PACKET_DECISION_DROP;
   }

   ex_op = mesh_hardware_info->ds_net_if->get_extended_operations(mesh_hardware_info->ds_net_if);

   if (pi->u.generic_command.type == IMCP_SNIP_GENERIC_COMMAND_TYPE_ENABLE_BEACONING_UPLINK)
   {
      vendor_info_len = mesh_imcp_get_vendor_info_buffer(AL_CONTEXT vendor_info, 1);
      ex_op->enable_beaconing_uplink(mesh_hardware_info->ds_net_if, vendor_info, vendor_info_len);
   }
   else if (pi->u.generic_command.type == IMCP_SNIP_GENERIC_COMMAND_TYPE_DISABLE_BEACONING_UPLINK)
   {
      ex_op->disable_beaconing_uplink(mesh_hardware_info->ds_net_if);
   }

   mesh_imcp_send_beaconing_uplink_response(AL_CONTEXT pi, pi->u.generic_command.type);


   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
   return ACCESS_POINT_PACKET_DECISION_DROP;
}


int mesh_imcp_process_packet_generic_command_req_res(AL_CONTEXT_PARAM_DECL imcp_packet_info_t *pi)
{
   int ret;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
   if (pi->u.generic_command.req_res == IMCP_SNIP_GENERIC_COMMAND_RESPONSE)
   {
       al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"drop req for generic cmd response :: func : %s LINE : %d\n", __func__,__LINE__);
      return ACCESS_POINT_PACKET_DECISION_DROP;
   }

   ret = ACCESS_POINT_PACKET_DECISION_SEND_UP_IMCP;

   switch (pi->u.generic_command.type)
   {
   case IMCP_SNIP_GENERIC_COMMAND_TYPE_SET_CTM:
   case IMCP_SNIP_GENERIC_COMMAND_TYPE_RESET_CTM:
      ret = mesh_imcp_process_generic_command_ct_mode_request(AL_CONTEXT pi);
      break;

   case IMCP_SNIP_GENERIC_COMMAND_TYPE_REJECT_CLIENTS:
      ret = mesh_imcp_process_generic_command_reject_clients_request(AL_CONTEXT pi);
      break;

   case IMCP_SNIP_GENERIC_COMMAND_TYPE_DNLINK_DCA:
      ret = mesh_imcp_process_generic_command_trigger_dnlink_dca_request(AL_CONTEXT pi);
      break;

   case IMCP_SNIP_GENERIC_COMMAND_TYPE_UPLINK_SCAN:
      ret = mesh_imcp_process_generic_command_trigger_uplink_scan_request(AL_CONTEXT pi);
      break;

   case IMCP_SNIP_GENERIC_COMMAND_TYPE_DNLINK_SATURATION:
      ret = mesh_imcp_process_generic_command_downlink_saturation(AL_CONTEXT pi);
      break;

   case IMCP_SNIP_GENERIC_COMMAND_TYPE_ENABLE_BEACONING_UPLINK:
   case IMCP_SNIP_GENERIC_COMMAND_TYPE_DISABLE_BEACONING_UPLINK:
      ret = mesh_imcp_process_generic_command_beaconing_uplink_request(AL_CONTEXT pi);
      break;
   }

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
   return ret;
}


int mesh_imcp_process_packet_hide_ssid_config_update(AL_CONTEXT_PARAM_DECL imcp_packet_info_t *pi)
{
   int i, j, if_count, al_netif_count;
   al_802_11_operations_t *operations;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
   al_net_if_t *net_if;


   if (mesh_state != _MESH_STATE_RUNNING)
   {
       al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"mesh is not running :: func : %s LINE : %d\n", __func__,__LINE__);
      return ACCESS_POINT_PACKET_DECISION_SEND_WM_DS;
   }

   if (!_CMP_MAC_ADDR((&pi->u.hide_ssid_info.ds_mac), _DS_MAC))
   {
       al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"ds mac are same(hide ssid of pkt) :: func : %s LINE : %d\n", __func__,__LINE__);
      return ACCESS_POINT_PACKET_DECISION_SEND_WM_DS;
   }

   if (pi->u.hide_ssid_info.req_res == IMCP_SNIP_CONFIG_REQUEST)
   {
      if_count       = pi->u.hide_ssid_info.interface_count;                                                            /* interface count */
      al_netif_count = al_get_net_if_count(AL_CONTEXT_SINGLE);

      for (i = 0; i < if_count; i++)
      {
/* Now this is handled by confiigd now */
#if 0
         net_if = NULL;

         for (j = 0; j < al_netif_count; j++)
         {
            net_if = al_get_net_if(AL_CONTEXT j);

            if (net_if != NULL)
            {
               if (!strcmp(net_if->name, pi->u.hide_ssid_info.interface_info[i].name))
               {
                  operations = net_if->get_extended_operations(net_if);

                  if (operations != NULL)
                  {
                     operations->set_hide_ssid(net_if, pi->u.hide_ssid_info.interface_info[i].hide_ssid);
                  }
               }
            }
         }
#endif
         for (j = 0; j < mesh_config->if_count; j++)
         {
            if (!strcmp(pi->u.hide_ssid_info.interface_info[i].name, mesh_config->if_info[j].name))
            {
               mesh_config->if_info[j].hide_ssid = pi->u.hide_ssid_info.interface_info[i].hide_ssid;
            }
         }
      }

      al_print_log(AL_CONTEXT AL_LOG_TYPE_DEBUG,"sending imcp pkt up stack ::  func : %s LINE : %d\n", __func__,__LINE__);

      return ACCESS_POINT_PACKET_DECISION_SEND_UP_IMCP;
   }
   else
   {
       al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
      return ACCESS_POINT_PACKET_DECISION_SEND_WM_DS;
   }
}


int mesh_imcp_process_packet_dot11e_category_update(AL_CONTEXT_PARAM_DECL imcp_packet_info_t *pi)
{
   al_dot11e_category_info_t *dot11e_config;
   int i;


   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
   if (mesh_state != _MESH_STATE_RUNNING)
   {
       al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"mesh is not running :: func : %s LINE : %d\n", __func__,__LINE__);
      return ACCESS_POINT_PACKET_DECISION_SEND_WM_DS;
   }

   if (!_CMP_MAC_ADDR((&pi->u.dot11e_category.ds_mac), _DS_MAC))
   {
       al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"ds mac are same(hide ssid of pkt) :: func : %s LINE : %d\n", __func__,__LINE__);
      return ACCESS_POINT_PACKET_DECISION_SEND_WM_DS;
   }

   dot11e_config = (al_dot11e_category_info_t *)mesh_dot11e_category_config;

   dot11e_config->count = pi->u.dot11e_category.count;

   for (i = 0; i < dot11e_config->count; i++)
   {
      dot11e_config->category_info[i].category        = pi->u.dot11e_category.details[i].category;
      dot11e_config->category_info[i].burst_time      = pi->u.dot11e_category.details[i].burst_time;
      dot11e_config->category_info[i].acwmin          = pi->u.dot11e_category.details[i].acwmin;
      dot11e_config->category_info[i].acwmax          = pi->u.dot11e_category.details[i].acwmax;
      dot11e_config->category_info[i].aifsn           = pi->u.dot11e_category.details[i].aifsn;
      dot11e_config->category_info[i].disable_backoff = pi->u.dot11e_category.details[i].disable_backoff;
   }

/*Now configd will handle the conf change */

   //access_point_set_dot11e_category_info(AL_CONTEXT dot11e_config->count, dot11e_config);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
   return ACCESS_POINT_PACKET_DECISION_SEND_UP_IMCP;
}


int mesh_imcp_process_packet_acl_info(AL_CONTEXT_PARAM_DECL imcp_packet_info_t *pi)
{
   access_point_acl_entry_t *ap_acl_entries = NULL;
   int i;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
   if (mesh_state != _MESH_STATE_RUNNING)
   {
       al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"mesh is not running :: func : %s LINE : %d\n", __func__,__LINE__);
      return ACCESS_POINT_PACKET_DECISION_SEND_WM_DS;
   }

   if (!_CMP_MAC_ADDR((&pi->u.acl_info.ds_mac), _DS_MAC))
   {
       al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"ds mac are same(hide ssid of pkt) :: func : %s LINE : %d\n", __func__,__LINE__);
      return ACCESS_POINT_PACKET_DECISION_SEND_WM_DS;
   }

   if (pi->u.acl_info.count > 0)
   {
      ap_acl_entries = (access_point_acl_entry_t *)al_heap_alloc(AL_CONTEXT sizeof(access_point_acl_entry_t) * pi->u.acl_info.count AL_HEAP_DEBUG_PARAM);
   }

   for (i = 0; i < pi->u.acl_info.count; i++)
   {
      memset(&ap_acl_entries[i], 0, sizeof(access_point_acl_entry_t));

      memcpy(ap_acl_entries[i].sta_mac.bytes, pi->u.acl_info.entries[i].sta_mac.bytes, 6);
      ap_acl_entries[i].sta_mac.length = 6;

      ap_acl_entries[i].vlan_tag        = pi->u.acl_info.entries[i].vlan_tag;
      ap_acl_entries[i].allow           = pi->u.acl_info.entries[i].allow;
      ap_acl_entries[i].dot11e_enabled  = pi->u.acl_info.entries[i].dot11e_enabled;
      ap_acl_entries[i].dot11e_category = pi->u.acl_info.entries[i].dot11e_category;
   }

   access_point_set_acl_list(AL_CONTEXT pi->u.acl_info.count, ap_acl_entries);

   if (pi->u.acl_info.count > 0)
   {
      al_heap_free(AL_CONTEXT pi->u.acl_info.entries);
      al_heap_free(AL_CONTEXT ap_acl_entries);
   }

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
   return ACCESS_POINT_PACKET_DECISION_SEND_UP_IMCP;
}


int mesh_imcp_get_vendor_info_buffer(AL_CONTEXT_PARAM_DECL unsigned char *vendor_info_out, int encryption)
{
   imcp_packet_info_t *pi;
   int                length;
   int                ifcnt;
   al_net_if_t        *net_if;
   int                j;
   int                i;
   int                flags = 0;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
   pi = imcp_snip_create_packet(AL_CONTEXT_SINGLE);

   if (current_parent == NULL)
   {
      current_parent = &dummy_lfr_parent;
   }

   memcpy(&pi->u.vendor_info.ds_mac, _DS_MAC, sizeof(al_net_addr_t));

   pi->u.vendor_info.hbi            = mesh_config->hbeat_interval;
   pi->u.vendor_info.phy_sub_type  = 0 ; // downlink subtype (either a/an/anac/ac/bgn/n/so....on) 
   for (i = 0; i < mesh_hardware_info->wm_net_if_count; i++){
       if ((mesh_hardware_info->wms_conf[i]->use_type == AL_CONF_IF_USE_TYPE_WM) && (AL_NET_IF_IS_WIRELESS(mesh_hardware_info->wms_conf[i]->net_if))){
           pi->u.vendor_info.phy_sub_type  = mesh_hardware_info->wms_conf[i]->phy_sub_type ; // downlink subtype (either a/an/anac/ac/bgn/n/so....on) 
       }
   }
   pi->u.vendor_info.ds_if_channel  = mesh_hardware_info->ds_if_channel;
   pi->u.vendor_info.ds_if_sub_type = mesh_hardware_info->ds_if_sub_type;

   if (mesh_state != _MESH_STATE_RUNNING)
   {
      if (!_DISJOINT_ADHOC_MODE)
         pi->u.vendor_info.hpc = 1;
      pi->u.vendor_info.ctc   = 0;
      pi->u.vendor_info.crssi = 0;
      memcpy(&pi->u.vendor_info.root_bssid, &zeroed_net_addr, sizeof(al_net_addr_t));
		al_print_log(AL_LOG_TYPE_INFORMATION,"Meshstate not Running  %d state  %s: %d\n",mesh_state, __func__,__LINE__);
   }
   else
   {
      if (current_parent != NULL)
      {
         pi->u.vendor_info.ctc = mesh_ng_get_ctc(AL_CONTEXT_SINGLE);
      }
      else
      {
         pi->u.vendor_info.ctc = 0;
      }

      if (mesh_mode == _MESH_AP_ROOT)
      {
         pi->u.vendor_info.hpc = 0;
         memcpy(&pi->u.vendor_info.root_bssid, _DS_MAC, sizeof(al_net_addr_t));
      }
      else
      {
         if (current_parent != NULL)
         {
            pi->u.vendor_info.hpc = current_parent->hpc + 1;
            memcpy(&pi->u.vendor_info.root_bssid, &current_parent->root_bssid, sizeof(al_net_addr_t));
         }
         else
         {
            pi->u.vendor_info.hpc = 0;
            memcpy(&pi->u.vendor_info.root_bssid, _DS_MAC, sizeof(al_net_addr_t));
         }
      }

      pi->u.vendor_info.crssi = mesh_ng_get_crssi(AL_CONTEXT_SINGLE);
   }


   ifcnt = al_get_net_if_count(AL_CONTEXT_SINGLE);
   for (j = 0; j < ifcnt; j++)
   {
      net_if = al_get_net_if(AL_CONTEXT j);
      if (!strcmp(net_if->name, "eth1"))
      {
         flags = net_if->get_flags(net_if);
      }
   }

   if ((_IS_LFR) && (mesh_config->failover_enabled || mesh_config->power_on_default) &&
       (((mesh_config->failover_enabled == MESH_FAILOVER_PHY_CONN) && (flags & AL_NET_IF_FLAGS_LINKED)) ||
        ((mesh_config->failover_enabled == MESH_FAILOVER_PING_CONN) && (flags & AL_NET_IF_FLAGS_LINKED) && (eth1_ping_status == 1))))
   {
      pi->u.vendor_info.mode = 1;
   }
   else
   {
      pi->u.vendor_info.mode = 0;
   }
   length = imcp_snip_get_vendor_info_buffer(AL_CONTEXT pi, vendor_info_out, encryption);

   imcp_snip_release_packet(AL_CONTEXT pi);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
   return length;

}


int mesh_imcp_process_packet_buffering_command(AL_CONTEXT_PARAM_DECL imcp_packet_info_t *pi)
{
   int ret;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
   if (pi->net_if == mesh_hardware_info->ds_net_if)
   {
      if ((mesh_mode != _MESH_AP_RELAY) ||
          !AL_NET_ADDR_IS_BROADCAST(&pi->u.buffer_command.ds_mac_dest) ||
          !AL_NET_ADDR_EQUAL(&pi->u.buffer_command.ds_mac_src, &current_parent->ds_mac))
      {
          al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"pkt drop :: func : %s LINE : %d\n", __func__,__LINE__);
         return ACCESS_POINT_PACKET_DECISION_DROP;
      }
   }
   else
   {
      if (!_CMP_MAC_ADDR((&pi->u.buffer_command.ds_mac_dest), _DS_MAC))
      {
          al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"pkt drop :: func : %s LINE : %d\n", __func__,__LINE__);
         return ACCESS_POINT_PACKET_DECISION_DROP;
      }
   }

   if (mesh_state != _MESH_STATE_RUNNING)
   {
          al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"pkt drop :: func : %s LINE : %d\n", __func__,__LINE__);
      return ACCESS_POINT_PACKET_DECISION_DROP;
   }

   switch (pi->u.buffer_command.type)
   {
   case IMCP_SNIP_BUFFERING_TYPE_START_REQUEST:
      access_point_set_buffering(AL_CONTEXT & pi->u.buffer_command.ds_mac_src, 1, pi->u.buffer_command.timeout);
      break;

   case IMCP_SNIP_BUFFERING_TYPE_START_REQUEST_WITH_ACK:
      access_point_set_buffering(AL_CONTEXT & pi->u.buffer_command.ds_mac_src, 1, pi->u.buffer_command.timeout);
      mesh_imcp_send_packet_buffering_command_ack(AL_CONTEXT pi->net_if, &pi->u.buffer_command.ds_mac_src);
      break;

   case IMCP_SNIP_BUFFERING_TYPE_STOP_REQUEST:
      access_point_set_buffering(AL_CONTEXT & pi->u.buffer_command.ds_mac_src, 0, 0);
      break;

   case IMCP_SNIP_BUFFERING_TYPE_DISABLE_UPLINK:
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MESH_AP: Received uplink disable request for %d ms from parent", pi->u.buffer_command.timeout);
      ret = mesh_ng_fsm_send_event(AL_CONTEXT MESH_NG_FSM_EVENT_UPLINK_DISABLE, (void *)pi->u.buffer_command.timeout);
      if (ret == 0)
      {
         al_set_event(AL_CONTEXT mesh_thread_wait_event);
      }
      break;

   case IMCP_SNIP_BUFFERING_TYPE_RADAR_DETECTED:
      if (pi->net_if == mesh_hardware_info->ds_net_if)
      {
         ret = mesh_ng_fsm_send_event(AL_CONTEXT MESH_NG_FSM_EVENT_DISCONNECT, NULL);
         al_set_event(AL_CONTEXT mesh_thread_wait_event);
      }
      else
      {
         ret = mesh_ng_fsm_send_event(AL_CONTEXT MESH_NG_FSM_EVENT_RADAR_MASTER, (void *)pi->net_if);
         al_set_event(AL_CONTEXT mesh_thread_wait_event);
      }
      break;
   }
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
   return ACCESS_POINT_PACKET_DECISION_DROP;
}


int mesh_imcp_process_packet_reboot_required(AL_CONTEXT_PARAM_DECL imcp_packet_info_t *pi)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
   if (mesh_state != _MESH_STATE_RUNNING)
   {
          al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"pkt drop :: func : %s LINE : %d\n", __func__,__LINE__);
      return ACCESS_POINT_PACKET_DECISION_DROP;
   }

   if (!_CMP_MAC_ADDR((&pi->u.reboot_required.ds_mac_dest), _DS_MAC))
   {
          al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"pkt drop :: func : %s LINE : %d\n", __func__,__LINE__);
      return ACCESS_POINT_PACKET_DECISION_SEND_WM_DS;
   }

   SET_MESH_FLAG(MESH_FLAG_REBOOT_REQUIRED);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MESH_AP: Flags set :  MESH_FLAG_REBOOT_REQUIRED");
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);

   return ACCESS_POINT_PACKET_DECISION_DROP;
}


int mesh_imcp_process_packet_radio_diag_command(AL_CONTEXT_PARAM_DECL imcp_packet_info_t *pi)
{
   int i;
   int al_netif_count;
   al_802_11_operations_t *operations;
   al_net_if_t            *net_if;
   unsigned char          *out_buffer;
   int out_data_length;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);

   if (mesh_state != _MESH_STATE_RUNNING)
   {
       al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"pkt drop :: func : %s LINE : %d\n", __func__,__LINE__);
      return ACCESS_POINT_PACKET_DECISION_DROP;
   }

   if (!_CMP_MAC_ADDR((&pi->u.radio_diagnostic_command.ds_mac), _DS_MAC))
   {
       al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"pkt drop :: func : %s LINE : %d\n", __func__,__LINE__);
      return ACCESS_POINT_PACKET_DECISION_SEND_WM_DS;
   }

   al_netif_count = al_get_net_if_count(AL_CONTEXT_SINGLE);
   operations     = NULL;
   net_if         = NULL;

   for (i = 0; i < al_netif_count; i++)
   {
      net_if = al_get_net_if(AL_CONTEXT i);

      if (net_if == NULL)
      {
         continue;
      }

      if (!strcmp(net_if->name, pi->u.radio_diagnostic_command.if_name))
      {
         operations = net_if->get_extended_operations(net_if);
         break;
      }
   }

   if ((operations == NULL) || (net_if == NULL))
   {
       al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"pkt drop :: func : %s LINE : %d\n", __func__,__LINE__);
      return ACCESS_POINT_PACKET_DECISION_DROP;
   }

   out_buffer = al_get_gen_2KB_buffer(AL_CONTEXT_SINGLE);

   out_data_length = operations->radio_diagnostic_command(net_if,
                                                          pi->u.radio_diagnostic_command.command_data,
                                                          pi->u.radio_diagnostic_command.command_length,
                                                          out_buffer,
                                                          2048);

   mesh_imcp_send_radio_diag_data(net_if, out_buffer, out_data_length);

   al_release_gen_2KB_buffer(AL_CONTEXT out_buffer);

   if (pi->u.radio_diagnostic_command.command_data != NULL)
   {
      al_heap_free(AL_CONTEXT pi->u.radio_diagnostic_command.command_data);
   }

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
   return ACCESS_POINT_PACKET_DECISION_DROP;
}


int mesh_imcp_process_packet_node_move_notification(AL_CONTEXT_PARAM_DECL imcp_packet_info_t *pi)
{
   unsigned long      flags;
   mesh_table_entry_t *entry = NULL;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
   if (mesh_state != _MESH_STATE_RUNNING)
   {
       al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"pkt drop :: func : %s LINE : %d\n", __func__,__LINE__);
      return ACCESS_POINT_PACKET_DECISION_SEND_WM_DS;
   }

   if (_CMP_MAC_ADDR((&pi->u.node_move_notification.ds_mac), _DS_MAC))
   {
       al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"pkt drop :: func : %s LINE : %d\n", __func__,__LINE__);
      return ACCESS_POINT_PACKET_DECISION_DROP;
   }

   if ((pi->net_if == _DS_NET_IF) &&
       (mesh_ap_is_table_entry_present(AL_CONTEXT & pi->u.node_move_notification.ds_mac) == 0))
   {
       al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"pkt drop :: func : %s LINE : %d\n", __func__,__LINE__);
      return ACCESS_POINT_PACKET_DECISION_DROP;
   }

   entry = mesh_table_entry_find(AL_CONTEXT & pi->u.node_move_notification.node_ds_mac);

   if (entry != NULL)
   {
      /* check if sta entry exists & it is immediate child then call ap_disassoc */

      if (AL_NET_ADDR_EQUAL(&entry->sta_addr, &entry->relay_ap_addr))
      {
         access_point_disassociate_sta(AL_CONTEXT & entry->sta_addr);
      }

	  mesh_table_entry_release(AL_CONTEXT entry);
      mesh_table_entry_remove_lock(AL_CONTEXT & pi->u.node_move_notification.node_ds_mac, 11);
   }

   if (pi->net_if == _DS_NET_IF)
   {
       al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
      return ACCESS_POINT_PACKET_DECISION_SEND_WM;
   }

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
   return ACCESS_POINT_PACKET_DECISION_SEND_WM_DS;
}


//SPAWAR_2
int mesh_imcp_process_packet_channel_change(AL_CONTEXT_PARAM_DECL imcp_packet_info_t *pi, al_net_addr_t *ap_addr)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
   if (pi->net_if == _DS_NET_IF)
   {
      if ((func_mode != _FUNC_MODE_LFR) && (AL_NET_ADDR_EQUAL(ap_addr, &current_parent->bssid)))
      {
         mesh_ng_uplink_channel_change_start(pi->u.channel_change_notification.milli_seconds_left,
                                             pi->u.channel_change_notification.wm_channel);
      }
      else
      {
         al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,
                      "MESH_AP: Ignoring parent channel change notification in LFR mode");
      }
   }

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
   return ACCESS_POINT_PACKET_DECISION_DROP;
}


int mesh_imcp_set_sip_private_packet_handler(AL_CONTEXT_PARAM_DECL meshap_process_imcp_sip_packet_t handler)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
   _sip_private_packet_handler = handler;
   return 0;
}


int mesh_imcp_process_packet_sip_private(AL_CONTEXT_PARAM_DECL imcp_packet_info_t *pi)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
   if (mesh_state != _MESH_STATE_RUNNING)
   {
       al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"pkt drop :: func : %s LINE : %d\n", __func__,__LINE__);
      return ACCESS_POINT_PACKET_DECISION_DROP;
   }

   if (_CMP_MAC_ADDR((&pi->u.sip_private.ds_mac), _DS_MAC))
   {
       al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"pkt drop :: func : %s LINE : %d\n", __func__,__LINE__);
      return ACCESS_POINT_PACKET_DECISION_DROP;
   }

/*
 *      if(!_IS_LIMITED) {
 *              al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,
 *                                       "MESH_AP: Ignoring sip private packet in NON-LIMITED mode");
 *
 *              return ACCESS_POINT_PACKET_DECISION_DROP;
 *      }
 */
   if (_sip_private_packet_handler != NULL)
   {
      _sip_private_packet_handler(&pi->u.sip_private.ds_mac, pi->u.sip_private.data, pi->u.sip_private.data_length);
   }

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
   return ACCESS_POINT_PACKET_DECISION_SEND_WM_DS;
}


int mesh_imcp_process_packet_buffering_command_ack(AL_CONTEXT_PARAM_DECL imcp_packet_info_t *pi)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
   if (mesh_state != _MESH_STATE_RUNNING)
   {
       al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"pkt drop :: func : %s LINE : %d\n", __func__,__LINE__);
      return ACCESS_POINT_PACKET_DECISION_DROP;
   }

   if (!_CMP_MAC_ADDR((&pi->u.buffer_command_ack.ds_mac_dest), _DS_MAC))
   {
       al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"pkt drop :: func : %s LINE : %d\n", __func__,__LINE__);
      return ACCESS_POINT_PACKET_DECISION_DROP;
   }

   al_set_event(AL_CONTEXT mesh_ds_buffer_ack_event);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);

   return ACCESS_POINT_PACKET_DECISION_DROP;
}


#include "mesh_imcp_send.c"
