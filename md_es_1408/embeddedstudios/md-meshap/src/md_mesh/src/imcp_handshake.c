/********************************************************************************
* MeshDynamics
* --------------
* File     : imcp_handshake.c
* Comments : Handshake IMCP packet impl file.
* Created  : 8/16/2004
* Author   : Anand Bhange
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  2  |4/9/2005  | Added DS channel and DS sub-type                | Sriram |
* -----------------------------------------------------------------------------
* |  1  |11/25/2004| le2cpu funcs called for packets                 | Anand  |
* -----------------------------------------------------------------------------
* |  0  |8/16/2004 | Created.                                        | Anand  |
* -----------------------------------------------------------------------------
********************************************************************************/
static int imcp_snip_handshake_request_get_buffer(AL_CONTEXT_PARAM_DECL imcp_packet_info_t *pi, unsigned char *buffer, unsigned int buf_length)
{
#define _REQ_LENGTH    12

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
   unsigned char *p;

   AL_PRINT_LOG_FLOW_0("MESH_AP	: imcp_snip_handshake_request_get_buffer()");

   AL_ASSERT("MESH_AP	: imcp_snip_handshake_request_get_buffer", buffer != NULL);
   AL_ASSERT("MESH_AP	: imcp_snip_handshake_request_get_buffer", pi != NULL);

   /**
    * Buffer length must be atleast req_length
    */

   if (buf_length < _REQ_LENGTH)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR," IMCP_SNIP_ERROR_INADEQUATE_BUFFER_LENGTH :: func : %s LINE : %d\n", __func__,__LINE__);
      return IMCP_SNIP_ERROR_INADEQUATE_BUFFER_LENGTH;
   }

   p = buffer;

   /**
    * Copy DS MAC, BSSID to which handshake is going on
    */
   memcpy(p, pi->u.handshake_request.ds_mac.bytes, MAC_ADDR_LENGTH);
   p += MAC_ADDR_LENGTH;
   memcpy(p, pi->u.handshake_request.bssid.bytes, MAC_ADDR_LENGTH);
   p += MAC_ADDR_LENGTH;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
   return _REQ_LENGTH;

#undef _REQ_LENGTH
}


static int imcp_snip_handshake_response_get_buffer(AL_CONTEXT_PARAM_DECL imcp_packet_info_t *pi, unsigned char *buffer, unsigned int buf_length)
{
#define _REQ_LENGTH    36

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
   unsigned char *p;
   unsigned int  cpu2le;

   AL_PRINT_LOG_FLOW_0("MESH_AP	: imcp_snip_handshake_response_get_buffer()");

   AL_ASSERT("MESH_AP	: imcp_snip_handshake_response_get_buffer", buffer != NULL);
   AL_ASSERT("MESH_AP	: imcp_snip_handshake_response_get_buffer", pi != NULL);

   /**
    * Buffer length must be atleast req_length
    */

   if (buf_length < _REQ_LENGTH)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR," IMCP_SNIP_ERROR_INADEQUATE_BUFFER_LENGTH :: func : %s LINE : %d\n", __func__,__LINE__);
      return IMCP_SNIP_ERROR_INADEQUATE_BUFFER_LENGTH;
   }

   p = buffer;

   /**
    * Copy Senders BSSID, Receiver DS MAC, Root BSSID, HPC, CTC, CH, CRSSI
    */
   memcpy(p, pi->u.handshake_response.src_ds_mac.bytes, MAC_ADDR_LENGTH);
   p += MAC_ADDR_LENGTH;
   memcpy(p, pi->u.handshake_response.bssid.bytes, MAC_ADDR_LENGTH);
   p += MAC_ADDR_LENGTH;
   memcpy(p, pi->u.handshake_response.dest_ds_mac.bytes, MAC_ADDR_LENGTH);
   p += MAC_ADDR_LENGTH;
   memcpy(p, pi->u.handshake_response.root_bssid.bytes, MAC_ADDR_LENGTH);
   p += MAC_ADDR_LENGTH;
   memcpy(p, &pi->u.handshake_response.hpc, 1);
   p     += 1;
   cpu2le = al_impl_cpu_to_le32(pi->u.handshake_response.ctc);
   memcpy(p, &cpu2le, 4);
   p     += 4;
   cpu2le = al_impl_cpu_to_le32(pi->u.handshake_response.crssi);
   memcpy(p, &cpu2le, 4);
   p += 4;
   memcpy(p, &pi->u.handshake_response.hbi, 1);
   p += 1;
   memcpy(p, &pi->u.handshake_response.ds_channel, 1);
   p += 1;
   memcpy(p, &pi->u.handshake_response.ds_type, 1);
   p += 1;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
   return _REQ_LENGTH;

#undef _REQ_LENGTH
}


static int imcp_snip_handshake_request_get_packet(AL_CONTEXT_PARAM_DECL unsigned char *buffer, unsigned int buf_length, imcp_packet_info_t *pi)
{
#define _REQ_LENGTH    12

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
   unsigned char *p;

   AL_PRINT_LOG_FLOW_0("MESH_AP	: imcp_snip_handshake_request_get_packet()");
   AL_ASSERT("MESH_AP	: imcp_snip_handshake_request_get_packet", pi != NULL);
   AL_ASSERT("MESH_AP	: imcp_snip_handshake_request_get_packet", buffer != NULL);

   /**
    * Buffer length must be atleast req_length
    */

   if (buf_length < _REQ_LENGTH)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR," IMCP_SNIP_ERROR_INADEQUATE_BUFFER_LENGTH :: func : %s LINE : %d\n", __func__,__LINE__);
      return IMCP_SNIP_ERROR_INADEQUATE_BUFFER_LENGTH;
   }

   p = buffer;

   /**
    * Copy DS MAC, BSSID to which handshake is going on
    */
   memcpy(pi->u.handshake_request.ds_mac.bytes, p, MAC_ADDR_LENGTH);
   p += MAC_ADDR_LENGTH;
   pi->u.handshake_request.ds_mac.length = MAC_ADDR_LENGTH;
   memcpy(pi->u.handshake_request.bssid.bytes, p, MAC_ADDR_LENGTH);
   p += MAC_ADDR_LENGTH;
   pi->u.handshake_request.bssid.length = MAC_ADDR_LENGTH;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
   return _REQ_LENGTH;

#undef _REQ_LENGTH
}


static int imcp_snip_handshake_response_get_packet(AL_CONTEXT_PARAM_DECL unsigned char *buffer, unsigned int buf_length, imcp_packet_info_t *pi)
{
#define _REQ_LENGTH    36

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
   unsigned char *p;
   unsigned int  le2cpu;

   AL_PRINT_LOG_FLOW_0("MESH_AP	: imcp_snip_handshake_response_get_packet()");
   AL_ASSERT("MESH_AP	: imcp_snip_handshake_response_get_packet", pi != NULL);
   AL_ASSERT("MESH_AP	: imcp_snip_handshake_response_get_packet", buffer != NULL);

   /**
    * Buffer length must be atleast req_length
    */

   if (buf_length < _REQ_LENGTH)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR," IMCP_SNIP_ERROR_INADEQUATE_BUFFER_LENGTH :: func : %s LINE : %d\n", __func__,__LINE__);
      return IMCP_SNIP_ERROR_INADEQUATE_BUFFER_LENGTH;
   }

   p = buffer;

   /**
    * Copy Senders BSSID, Receiver DS MAC, Root BSSID, HPC, CTC, CH, CRSSI
    */
   memcpy(pi->u.handshake_response.src_ds_mac.bytes, p, MAC_ADDR_LENGTH);
   p += MAC_ADDR_LENGTH;
   pi->u.handshake_response.src_ds_mac.length = MAC_ADDR_LENGTH;
   memcpy(pi->u.handshake_response.bssid.bytes, p, MAC_ADDR_LENGTH);
   p += MAC_ADDR_LENGTH;
   pi->u.handshake_response.bssid.length = MAC_ADDR_LENGTH;
   memcpy(pi->u.handshake_response.dest_ds_mac.bytes, p, MAC_ADDR_LENGTH);
   p += MAC_ADDR_LENGTH;
   pi->u.handshake_response.dest_ds_mac.length = MAC_ADDR_LENGTH;
   memcpy(pi->u.handshake_response.root_bssid.bytes, p, MAC_ADDR_LENGTH);
   p += MAC_ADDR_LENGTH;
   pi->u.handshake_response.root_bssid.length = MAC_ADDR_LENGTH;
   memcpy(&pi->u.handshake_response.hpc, p, 1);
   p += 1;
   memcpy(&le2cpu, p, 4);
   p += 4;
   pi->u.handshake_response.ctc = al_impl_le32_to_cpu(le2cpu);
   memcpy(&le2cpu, p, 4);
   p += 4;
   pi->u.handshake_response.crssi = al_impl_le32_to_cpu(le2cpu);
   memcpy(&pi->u.handshake_response.hbi, p, 1);
   p += 1;
   memcpy(&pi->u.handshake_response.ds_channel, p, 1);
   p += 1;
   memcpy(&pi->u.handshake_response.ds_type, p, 1);
   p += 1;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
   return _REQ_LENGTH;

#undef _REQ_LENGTH
}
