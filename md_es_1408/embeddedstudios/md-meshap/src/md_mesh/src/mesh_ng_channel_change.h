/********************************************************************************
* MeshDynamics
* --------------
* File     : mesh_ng_channel_change.h
* Comments : Mesh Channel Change implementation
* Created  : 8/28/2007
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  1  |5/14/2008 | Changes for mobility downlink channel change    |Prachiti|
* -----------------------------------------------------------------------------
* |  0  |8/28/2007 | Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

#ifndef __MESH_NG_CHANNEL_CHANGE_H__
#define __MESH_NG_CHANNEL_CHANGE_H__

void mesh_ng_downlink_channel_change_start(int param);
void mesh_ng_uplink_channel_change_start(int milli_seconds_left, unsigned char new_channel);
void mesh_ng_channel_change_init(void);

#endif /*__MESH_NG_CHANNEL_CHANGE_H__*/
