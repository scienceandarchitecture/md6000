/********************************************************************************
* MeshDynamics
* --------------
* File     : mesh_ng_scan.c
* Comments : Mesh New Generation Scan Implementation
* Created  : 4/6/2006
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  4  |02/23/2009| Added Update logic for parent mobility flag     | Sriram |
* -----------------------------------------------------------------------------
* |  3  |02/18/2009| Changes for Uplink Scan Functionality           | Sriram |
* -----------------------------------------------------------------------------
* |  2  |6/13/2007 | MESH_PARENT_FLAG_RESET_AVG implemented          | Sriram |
* -----------------------------------------------------------------------------
* |  1  |10/17/2006| DS scan channel list enabled                    | Sriram |
* -----------------------------------------------------------------------------
* |  0  |4/6/2006  | Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

#include <linux/string.h>
#include <linux/kernel.h>
#include "al.h"
#include "al_print_log.h"
#include "access_point.h"
#include "al_impl_context.h"
#include "al_conf.h"
#include "mesh_defs.h"
#include "mesh_table.h"
#include "mesh.h"
#include "mesh_globals.h"
#include "mesh_errors.h"
#include "imcp_snip.h"
#include "mesh_imcp.h"
#include "mesh_intervals.h"
#include "mesh_ng.h"
#include "mesh_ng_scan.h"
#include "al_802_11.h"

extern void dump_bytes(char *D, int count);
extern int mesh_clear_mobility_window;

static AL_INLINE void _set_tx_power(AL_CONTEXT_PARAM_DECL al_net_if_t *net_if, al_802_11_operations_t *ex_op)
{
   al_802_11_tx_power_info_t power_info;

   power_info.flags = AL_802_11_TXPOW_PERC;
   power_info.power = 100;
   ex_op->set_tx_power(net_if, &power_info);
}


static AL_INLINE int
_do_scan(AL_CONTEXT_PARAM_DECL int   mode,
         al_net_if_t                 *net_if,
         al_802_11_operations_t      *ex_op,
         al_scan_access_point_info_t **access_points,
         int                         *ap_count,
         int                         channel_index,
         int                         dwell_time)
{
   int           ret;
   int           duration;
   int           channel_count;
   unsigned char *channels;
   int           func_id;

   ret = -1;

   switch (mode)
   {
   case MESH_NG_SCAN_MODE_MONITOR_NET_IF:
      switch (mesh_hardware_info->monitor_type)
      {
      case MONITOR_TYPE_PMON:
         func_id = 1;
         break;

      case MONITOR_TYPE_AMON:
         func_id = 2;
         break;

      case MONITOR_TYPE_AUTO:
      default:
         func_id = 0;
         break;
      }
      if (_MON_CONF->dca_list_count > 0)
      {
         duration      = (mesh_config->scanner_dwell_interval / _MON_CONF->dca_list_count);
         channel_count = _MON_CONF->dca_list_count;
         channels      = _MON_CONF->dca_list;
      }
      else
      {
         duration      = SCAN_DURATION;
         channel_count = 0;
         channels      = NULL;
      }
      break;

   case MESH_NG_SCAN_MODE_DS_NET_IF:
      if (channel_index == -1)
      {
         if (_DS_CONF->dca_list_count > 0)
         {
            duration      = SCAN_DURATION;
            channel_count = _DS_CONF->dca_list_count;
            channels      = _DS_CONF->dca_list;
         }
         else
         {
            duration      = SCAN_DURATION;
            channel_count = 0;
            channels      = NULL;
         }
      }
      else
      {
         if ((_DS_CONF->dca_list_count > 0) && (channel_index >= 0) && (channel_index < _DS_CONF->dca_list_count))
         {
            duration      = dwell_time;
            channel_count = 1;
            channels      = &_DS_CONF->dca_list[channel_index];
         }
         else
         {
            goto _out;
         }
      }

      func_id = 0;
      break;

   default:
      duration      = SCAN_DURATION;
      channel_count = 0;
      channels      = NULL;
      func_id       = 0;
   }

   /* Bz-252: in LFR mode evaluate_mobile was called very freq, due to very lower dwell_time */
   duration = (duration > MINIMUM_DWELL_TIME) ? duration : MINIMUM_DWELL_TIME ;

   switch (func_id)
   {
   case 0:
      ret = ex_op->scan_access_points(net_if, access_points, ap_count, duration, channel_count, channels);
      break;

   case 1:
      ret = ex_op->scan_access_points_passive(net_if, access_points, ap_count, duration, channel_count, channels);
      if (ret)
      {
         mesh_hardware_info->monitor_type = MONITOR_TYPE_AUTO;
      }
      break;

   case 2:
      ret = ex_op->scan_access_points_active(net_if, access_points, ap_count, duration, channel_count, channels);
      if (ret)
      {
         mesh_hardware_info->monitor_type = MONITOR_TYPE_AUTO;
      }
      break;

   default:
      ret = ex_op->scan_access_points(net_if, access_points, ap_count, duration, channel_count, channels);
   }

_out:

   return ret;
}


static AL_INLINE int _scan_parents(AL_CONTEXT_PARAM_DECL int   mode,
                                   al_net_if_t                 *net_if,
                                   al_802_11_operations_t      *ex_op,
                                   al_scan_access_point_info_t **access_points,
                                   int                         *ap_count)
{
   ex_op->set_mode(net_if, AL_802_11_MODE_INFRA, 0);

   if (mode == MESH_NG_SCAN_MODE_DS_NET_IF)
   {
      _set_tx_power(AL_CONTEXT net_if, ex_op);
   }

   return _do_scan(AL_CONTEXT mode, net_if, ex_op, access_points, ap_count, -1, 0);
}


static AL_INLINE al_net_if_t *_get_net_if(AL_CONTEXT_PARAM_DECL int mode)
{
   al_net_if_t *scan_net_if;

   switch (mode)
   {
   case MESH_NG_SCAN_MODE_MONITOR_NET_IF:
      if (mesh_hardware_info->monitor_type == MONITOR_TYPE_ABSENT)
      {
         scan_net_if = mesh_hardware_info->ds_net_if;
      }
      else
      {
         scan_net_if = mesh_hardware_info->monitor_net_if;
      }
      break;

   case MESH_NG_SCAN_MODE_DS_NET_IF:
   default:
      scan_net_if = mesh_hardware_info->ds_net_if;
   }

   return scan_net_if;
}


static const char *_process_ap_errors[] =
{
   "IS IMCP",
   "INCORRECT KEY",
   "NOT IMCP",
   "WRONG VERSION",
   "ID LENGTH MISMATCH",
   "ID MISMATCH"
};

static AL_INLINE int
_process_ap_info(AL_CONTEXT_PARAM_DECL
                 al_scan_access_point_info_t *ap_info,
                 parent_t                    **parent_out,
                 al_u64_t                    *entry_time_out,
                 al_net_addr_t               *relay_ap_addr_out)
{
   parent_t            *parent;
   imcp_packet_info_t  *packet;
   mesh_conf_if_info_t *netif_conf;
   int                 flag;
   int                 ret;
   unsigned int       hash;

   *parent_out = NULL;
   packet      = imcp_snip_create_packet(AL_CONTEXT_SINGLE);

   ret = imcp_snip_process_vendor_info(AL_CONTEXT ap_info->vendor_info,
                                       ap_info->vendor_info_length, packet);
   if (ret != 0)
   {
      imcp_snip_release_packet(AL_CONTEXT packet);
      return ret;
   }

   parent = mesh_ng_create_parent(AL_CONTEXT & packet->u.vendor_info.ds_mac,
                                  &ap_info->bssid);
   parent->channel          = ap_info->channel;
   parent->crssi            = packet->u.vendor_info.crssi;
   parent->direct_bit_rate  = mesh_hardware_info->ds_conf->initial_rate_mbps;
   parent->disconnect_count = 0;
   parent->ds_channel       = packet->u.vendor_info.ds_if_channel;
   parent->ds_type          = packet->u.vendor_info.ds_if_sub_type;
   parent->mode             = packet->u.vendor_info.mode;
   parent->phy_sub_type     = packet->u.vendor_info.phy_sub_type;


   if (ap_info->signal > 96)
   {
      ap_info->signal = 0;
   }

   parent->signal = ap_info->signal;
   parent->hbi    = packet->u.vendor_info.hbi;
   parent->hpc    = packet->u.vendor_info.hpc;

   strcpy(parent->essid, ap_info->essid);

   if (IMCP_SNIP_CTC_GET_MOBILE(packet->u.vendor_info.ctc))
   {
      parent->flags |= MESH_PARENT_FLAG_MOBILE;
   }

   if (AL_NET_ADDR_EQUAL(&ap_info->bssid, &mesh_config->preferred_parent))
   {
      parent->flags |= MESH_PARENT_FLAG_PREFERRED;
   }

   if (!memcmp(ap_info->essid, "MESH-INIT-", 10))
   {
      parent->flags |= MESH_PARENT_FLAG_QUESTION;
      parent->flags |= MESH_PARENT_FLAG_DISABLED;
   }

   memcpy(&parent->par_ap_ht_capab,&ap_info->ap_ht_capab,sizeof(al_802_11_ht_capabilities_t));
   memcpy(&parent->par_ap_vht_capab,&ap_info->ap_vht_capab,sizeof(al_802_11_vht_capabilities_t));
   parent->phy_support = get_phy_support(parent->phy_sub_type);
   parent->tree_bit_rate = IMCP_SNIP_CTC_GET_TREE_LINK_RATE(packet->u.vendor_info.ctc);

   memcpy(&parent->root_bssid, &packet->u.vendor_info.root_bssid, sizeof(al_net_addr_t));

   if (IMCP_SNIP_CRSSI_GET_VALID(parent->crssi) &&
       IMCP_SNIP_CRSSI_GET_FUNCTIONALITY(parent->crssi))
   {
      parent->flags |= MESH_PARENT_FLAG_LIMITED;
   }

   if (_IS_MOBILE || _DISJOINT_ADHOC_MODE || _IS_UPLINK_SCAN_ENABLED)
   {
      mesh_table_entry_t *entry = NULL;
      unsigned long      flags;


      entry = mesh_table_entry_find(AL_CONTEXT & parent->ds_mac);


      if (entry != NULL)
      {
			parent->flags |= MESH_PARENT_FLAG_CHILD;

			if (entry_time_out != NULL)
			{
				*entry_time_out = entry->create_time;
			}

			if (relay_ap_addr_out != NULL)
			{
				memcpy(relay_ap_addr_out, &entry->relay_ap_addr, sizeof(al_net_addr_t));
			}
			mesh_table_entry_release(AL_CONTEXT entry);
		}

	}

	*parent_out = parent;

	imcp_snip_release_packet(AL_CONTEXT packet);

	return 0;
}


extern void mesh_ng_fsm_update_downlink_vendor_info(AL_CONTEXT_PARAM_DECL_SINGLE);

static int _parent_beacon_miss_enum_func (AL_CONTEXT_PARAM_DECL void *enum_param, parent_t *parent)
{
   int duration;
   int channel_count;
   al_u64_t time_stamp, timeout;

   channel_count = (mesh_hardware_info->monitor_type != MONITOR_TYPE_ABSENT) ? _MON_CONF->dca_list_count : _DS_CONF->dca_list_count;
   duration = mesh_config->scanner_dwell_interval / channel_count;
   duration = (duration > MINIMUM_DWELL_TIME) ? duration : MINIMUM_DWELL_TIME;

   time_stamp = al_get_tick_count(AL_CONTEXT_SINGLE);
   timeout = parent->last_beacon_time + ((SCAN_CYCLE_TO_MARK_DISABLE * 2 * duration * channel_count * HZ) / 1000);

   if (al_timer_after(time_stamp, timeout)) {
      if(parent != current_parent){ 
         parent->flags |= MESH_PARENT_FLAG_DISABLED;
         al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "Disabling parent %x:%x:%x:%x:%x:%x due to timeout, Dwell time: %d, "
             "timeout:%llx, current_time:%llx %s:%d\n", parent->bssid.bytes[0], parent->bssid.bytes[1], parent->bssid.bytes[2], 
              parent->bssid.bytes[3], parent->bssid.bytes[4], parent->bssid.bytes[5],
              mesh_config->scanner_dwell_interval, timeout, time_stamp, __func__, __LINE__);  
      }
  }
   return 0;
}

static AL_INLINE int
_process_parents(AL_CONTEXT_PARAM_DECL int mode, int ap_count,
                 al_scan_access_point_info_t *access_points)
{
   int           i, j;
   parent_t      *parent_new;
   parent_t      *parent;
   int           ret;
   int           count;
   int           if_count;
   al_net_if_t   *net_if1;
   int           ifcnt;
   al_net_if_t   *net_if;
   int           found;
   char          buf[256];
   int           print_message_flag;
   al_u64_t      parent_entry_time;
   al_net_addr_t parent_entry_relay_addr;
   al_net_addr_t *self_root_bssid;
   int           srb_flag = 0;
   int           flags;
   int           compare;

   switch (mode)
   {
   case MESH_NG_SCAN_MODE_DS_NET_IF:
      print_message_flag = 2;
      break;

   case MESH_NG_SCAN_MODE_MONITOR_NET_IF:
      print_message_flag = 0;
      break;

   default:
      print_message_flag = 1;
   }

   for (i = 0, count = 0; i < ap_count; i++)
   {
      /** Skip our own downlink address first */

      if_count = al_get_net_if_count(AL_CONTEXT_SINGLE);
      found    = 0;
      for (j = 0; j < if_count; j++)
      {
         net_if = al_get_net_if(AL_CONTEXT j);
         if (AL_NET_ADDR_EQUAL(&access_points[i].bssid,
                               &net_if->config.hw_addr))
         {
            found = 1;
            break;
         }
      }

      if (found)
      {
         continue;
      }

      /** Skip non-IMCP APs */
      if (access_points[i].vendor_info_length <= 0)
      {
         if (print_message_flag == 2)
         {
            al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,
                         "MESH_NG	: AP ("AL_NET_ADDR_STR")\t%s\tCH %d\t SGNL=%d NOT IMCP",
                         AL_NET_ADDR_TO_STR(&(access_points[i].bssid)),
                         access_points[i].essid,
                         access_points[i].channel,
                         access_points[i].signal);
         }

         if (mesh_state != _MESH_STATE_RUNNING)
         {
            sprintf(buf, "AP ("AL_NET_ADDR_STR ")\t%s\tCH %d\t SGNL=%d NOT IMCP", AL_NET_ADDR_TO_STR(&(access_points[i].bssid)), access_points[i].essid, access_points[i].channel, access_points[i].signal);
            mesh_imcp_send_packet_mesh_init_status(AL_CONTEXT buf);
         }
         continue;
      }

      ret = _process_ap_info(AL_CONTEXT & access_points[i],
                             &parent_new, &parent_entry_time,
                             &parent_entry_relay_addr);

      if (ret != 0)
      {
         if (print_message_flag == 2)
         {
            al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,
                         "MESH_NG	: AP ("AL_NET_ADDR_STR")\t%s\tCH %d\t SGNL=%d %s",
                         AL_NET_ADDR_TO_STR(&(access_points[i].bssid)),
                         access_points[i].essid,
                         access_points[i].channel,
                         access_points[i].signal,
                         _process_ap_errors[ret]);
         }

         if (mesh_state != _MESH_STATE_RUNNING)
         {
            sprintf(buf, "AP ("AL_NET_ADDR_STR ")\t%s\tCH %d\t SGNL=%d %s", AL_NET_ADDR_TO_STR(&(access_points[i].bssid)), access_points[i].essid, access_points[i].channel, access_points[i].signal, _process_ap_errors[ret]);
            mesh_imcp_send_packet_mesh_init_status(AL_CONTEXT buf);
         }
         continue;
      }

      /**
       * If we are not operating in LIMITED mode, we should
       * skip LIMITED parents
       */

      if (_DISJOINT_ADHOC_MODE)
      {
         al_u64_t     time_stamp;
         al_u64_t     timeout;
         unsigned int flags;

         /**
          * In DISJOINT ADHOC mode, if we are in LIMITED functionality,
          * we must not consider MESH_INIT nodes
          */

         /**
          * Skip ROOT BSSID checks if not a child, or if the entry has been created
          * less than 30 seconds ago.
          */

         time_stamp = al_get_tick_count(AL_CONTEXT_SINGLE);

         if (!(parent_new->flags & MESH_PARENT_FLAG_CHILD))
         {
            goto _skip_root_bssid_check;
         }

         timeout = parent_entry_time + 30 * HZ;
         if (al_timer_before(time_stamp, timeout))
         {
            goto _skip_root_bssid_check;
         }

         srb_flag = 1;
         switch (func_mode)
         {
         case _FUNC_MODE_FFR:
         case _FUNC_MODE_LFR:
            self_root_bssid = _DS_MAC;
            break;

         case _FUNC_MODE_FFN:
         case _FUNC_MODE_LFN:
            if (!current_parent) {
               self_root_bssid = _DS_MAC;
            } else {
               self_root_bssid = &current_parent->root_bssid;
            }
            break;

         default:
            mesh_ng_destroy_parent(AL_CONTEXT parent_new);
            continue;
         }
         if (!AL_NET_ADDR_EQUAL(&parent_new->root_bssid, self_root_bssid))
         {
            al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,
                         "MESH_NG	: AP ("AL_NET_ADDR_STR") is a child reporting RBSSID "AL_NET_ADDR_STR " removing entry and un-setting flag...",
                         AL_NET_ADDR_TO_STR(&(access_points[i].bssid)),
                         AL_NET_ADDR_TO_STR(&parent_new->root_bssid));

            mesh_table_entry_remove_lock(AL_CONTEXT & parent_new->ds_mac, 12);


            mesh_imcp_send_packet_node_move_notification(AL_CONTEXT & parent_new->ds_mac);

            parent_new->flags &= ~MESH_PARENT_FLAG_CHILD;
         }

         /**
          * If we are in dis-joint ad-hoc mode, running as an LFR
          * and the spotted parent is also a LFR, then we will
          * only add it if the last 32 bits of its MAC address treated
          * as a unsigned integer is greater than ours
          */
      }
      else if (parent_new->flags & MESH_PARENT_FLAG_LIMITED)
      {
          /* Skip LIMITED Parents adding into parent list when we are
           * operating as non LIMITED */
           continue;
      }
_skip_root_bssid_check:
      parent = mesh_ng_get_parent(AL_CONTEXT & parent_new->ds_mac, &parent_new->bssid, 0);

      if (parent == NULL)
      {
         al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,
                      "MESH_NG	: AP ("AL_NET_ADDR_STR")\t%s\tCH %d\t SGNL=%d IS IMCP",
                      AL_NET_ADDR_TO_STR(&(access_points[i].bssid)),
                      access_points[i].essid,
                      access_points[i].channel,
                      access_points[i].signal);

         if (mesh_state != _MESH_STATE_RUNNING)
         {
            sprintf(buf, "AP ("AL_NET_ADDR_STR ")\t%s\tCH %d\t SGNL=%d ", AL_NET_ADDR_TO_STR(&(access_points[i].bssid)), access_points[i].essid, access_points[i].channel, access_points[i].signal);
            mesh_imcp_send_packet_mesh_init_status(AL_CONTEXT buf);
         }
         parent_new->last_hb_time   = al_get_tick_count(AL_CONTEXT_SINGLE);
         parent_new->last_seen_time = al_get_tick_count(AL_CONTEXT_SINGLE);
         parent_new->last_beacon_time = al_get_tick_count(AL_CONTEXT_SINGLE);
         mesh_ng_add_parent(AL_CONTEXT parent_new);
      }
      else
      {
         int avg;

         avg = (parent->signal + parent_new->signal) / 2;


         /**
          * If MESH_PARENT_FLAG_RESET_AVG is set, we begin a new averaging window
          */

         if (parent->flags & MESH_PARENT_FLAG_RESET_AVG)
         {
            parent->signal = parent_new->signal;
            parent->flags &= ~MESH_PARENT_FLAG_RESET_AVG;
         }
         else
         {
            parent->signal = avg;
         }

         parent->channel        = parent_new->channel;
         parent->crssi          = parent_new->crssi;
         parent->ds_channel     = parent_new->ds_channel;
         parent->tree_bit_rate  = parent_new->tree_bit_rate;
         parent->last_hb_time   = al_get_tick_count(AL_CONTEXT_SINGLE);
         parent->last_beacon_time = al_get_tick_count(AL_CONTEXT_SINGLE);
         parent->last_seen_time = al_get_tick_count(AL_CONTEXT_SINGLE);
         parent->mode           = parent_new->mode;                         /* updating mode for LFRS*/
         parent->hpc            = parent_new->hpc;
         parent->phy_sub_type   = parent_new->phy_sub_type;
         strcpy(parent->essid, parent_new->essid);

         memcpy(&parent->root_bssid, &parent_new->root_bssid, sizeof(al_net_addr_t));

		 memcpy(&parent->par_ap_ht_capab,&parent_new->par_ap_ht_capab,sizeof(al_802_11_ht_capabilities_t));
		 memcpy(&parent->par_ap_vht_capab,&parent_new->par_ap_vht_capab,sizeof(al_802_11_vht_capabilities_t));
		 parent->phy_support = get_phy_support(parent->phy_sub_type);

         if (parent_new->flags & MESH_PARENT_FLAG_MOBILE)
         {
            parent->flags |= MESH_PARENT_FLAG_MOBILE;
         }
         else
         {
            parent->flags &= ~MESH_PARENT_FLAG_MOBILE;
         }

         if (parent_new->flags & MESH_PARENT_FLAG_PREFERRED)
         {
            parent->flags |= MESH_PARENT_FLAG_PREFERRED;
         }

         if (!memcmp(parent->essid, "MESH-INIT-", 10))
         {
            parent->flags |= MESH_PARENT_FLAG_QUESTION;
            parent->flags |= MESH_PARENT_FLAG_DISABLED;
         }
         else
         {
//PROTOCOL_COMBO
            int parent_proto_cap = 0;
            parent_proto_cap = convert_mode_to_proto_cap(parent_new->phy_sub_type);

            if (ds_proto_type & parent_proto_cap) {
               parent->flags &= ~MESH_PARENT_FLAG_QUESTION;
               parent->flags &= ~MESH_PARENT_FLAG_DISABLED;
            } else {
               parent->flags |= MESH_PARENT_FLAG_DISABLED;
            }
         }

         if (parent_new->flags & MESH_PARENT_FLAG_CHILD)
         {
            parent->flags |= MESH_PARENT_FLAG_CHILD;
         }
         else
         {
            parent->flags &= ~MESH_PARENT_FLAG_CHILD;
         }

         if (parent_new->flags & MESH_PARENT_FLAG_LIMITED)
         {
            if ((parent == current_parent) && !(parent->flags & MESH_PARENT_FLAG_LIMITED)) {
               mesh_clear_mobility_window = 1;
         }
            parent->flags |= MESH_PARENT_FLAG_LIMITED;
         }
         else
         {
            if ((parent == current_parent) && (parent->flags & MESH_PARENT_FLAG_LIMITED)) {
               mesh_clear_mobility_window = 1;
            }
            parent->flags &= ~MESH_PARENT_FLAG_LIMITED;
         }
         if (parent_new->flags & MESH_PARENT_FLAG_AD_DISABLE)
         {
            if (!(parent->flags & MESH_PARENT_FLAG_AD_DISABLE))
            {
               /**
                * Previously the parent was not in ADHOC disable state
                * but now the parent is: reset the first seen time
                */

               parent->first_seen_time = parent_new->first_seen_time;
            }

            parent->flags |= MESH_PARENT_FLAG_AD_DISABLE;
         }
         else
         {
            parent->flags &= ~MESH_PARENT_FLAG_AD_DISABLE;
         }

         if (parent->flags & MESH_PARENT_FLAG_AD_DISABLE)
         {
            al_u64_t time_stamp;
            al_u64_t timeout;

            time_stamp = al_get_tick_count(AL_CONTEXT_SINGLE);

            /**
             * ADHOC_TODO: experimental values need to be #define'd to make concrete
             */

            timeout = parent->first_seen_time + (100 - parent->signal) * 500;
            if (al_timer_after(time_stamp, timeout))
            {
               parent->flags &= ~MESH_PARENT_FLAG_AD_DISABLE;

               al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,
                            "MESH_NG	: ENABLING AP ("AL_NET_ADDR_STR")\t%s\tCH %d\t SGNL=%d",
                            AL_NET_ADDR_TO_STR(&(access_points[i].bssid)),
                            access_points[i].essid,
                            access_points[i].channel,
                            access_points[i].signal);
            }
         }
         if (AL_NET_ADDR_EQUAL(&parent->bssid, &current_parent->bssid) &&
             ((srb_flag == 1) && (AL_NET_ADDR_EQUAL(&parent->root_bssid, self_root_bssid))))
         {
            mesh_ng_fsm_update_downlink_vendor_info(AL_CONTEXT_SINGLE);
         }
         mesh_ng_destroy_parent(AL_CONTEXT parent_new);
      }
      count++;
   }

   mesh_ng_enumerate_parents(AL_CONTEXT MESH_NG_ENUMERATE_PARENTS_TYPE_ALL, NULL, _parent_beacon_miss_enum_func, 0);

   return count;
}


int mesh_ng_scan_parents(AL_CONTEXT_PARAM_DECL int mode)
{
   al_net_if_t *scan_net_if;
   al_scan_access_point_info_t *access_points;
   int ap_count;
   int ret;
   al_802_11_operations_t *ex_op;
   char buf[256];

   scan_net_if = _get_net_if(AL_CONTEXT mode);

   ex_op = (al_802_11_operations_t *)scan_net_if->get_extended_operations(scan_net_if);

   if (ex_op == NULL)
   {
      return MESH_NG_SCAN_ERROR_BAD_NET_IF;
   }

   if (mode != MESH_NG_SCAN_MODE_MONITOR_NET_IF)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MESH_NG	: Scanning APS ...");
   }

   if (mesh_state != _MESH_STATE_RUNNING)
   {
      mesh_imcp_send_packet_mesh_init_status(AL_CONTEXT "MESH_NG	: Scanning APs for Wireless DS ");
   }

   ret = _scan_parents(AL_CONTEXT mode, scan_net_if, ex_op, &access_points, &ap_count);

   if (ret != 0)
   {
      return MESH_NG_SCAN_ERROR_SCAN_PROBLEM;
   }

   if (mode != MESH_NG_SCAN_MODE_MONITOR_NET_IF)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MESH_NG	: Scanning Success AP Count = %d", ap_count);
   }
   if (mesh_state != _MESH_STATE_RUNNING)
   {
      sprintf(buf, "MESH_NG	: Scanning Success AP Count = %d", ap_count);
      mesh_imcp_send_packet_mesh_init_status(AL_CONTEXT buf);
   }

   ret = _process_parents(AL_CONTEXT mode, ap_count, access_points);

   al_heap_free(AL_CONTEXT access_points);

   if (ret < 0)
   {
      return MESH_NG_SCAN_ERROR_PROCESS_PROBLEM;
   }

   return ret;
}


int mesh_ng_step_scan_parents(AL_CONTEXT_PARAM_DECL
                              int                         channel_index,
                              int                         dwell_time,
                              al_scan_access_point_info_t **access_points,
                              int                         *ap_count)
{
   al_net_if_t            *scan_net_if;
   int                    ret;
   al_802_11_operations_t *ex_op;

   scan_net_if = mesh_hardware_info->ds_net_if;

   ex_op = (al_802_11_operations_t *)scan_net_if->get_extended_operations(scan_net_if);

   if (ex_op == NULL)
   {
      return MESH_NG_SCAN_ERROR_BAD_NET_IF;
   }

   ret = _do_scan(AL_CONTEXT MESH_NG_SCAN_MODE_DS_NET_IF,
                  scan_net_if,
                  ex_op,
                  access_points,
                  ap_count,
                  channel_index,
                  dwell_time);

   return ret;
}


int mesh_ng_step_process_parents(AL_CONTEXT_PARAM_DECL
                                 int                         ap_count,
                                 al_scan_access_point_info_t *access_points)
{
   int ret;

   ret = _process_parents(AL_CONTEXT MESH_NG_SCAN_MODE_OTHER, ap_count, access_points);

   if (ret < 0)
   {
      return MESH_NG_SCAN_ERROR_PROCESS_PROBLEM;
   }

   return ret;
}

#include "mesh_ng_fsm.h"
int mesh_ng_check_for_parent_state(unsigned char* vendor_info, unsigned char vendor_info_length)
{
    imcp_packet_info_t  *packet;
    int ret;
    static int crssi_value;
    packet      = imcp_snip_create_packet(AL_CONTEXT_SINGLE);
   ret = imcp_snip_process_vendor_info(AL_CONTEXT vendor_info,
                                       vendor_info_length, packet);
    if (crssi_value != IMCP_SNIP_CRSSI_GET_FUNCTIONALITY(packet->u.vendor_info.crssi))
    {
        mesh_ng_change_current_mode_func(IMCP_SNIP_CRSSI_GET_FUNCTIONALITY(packet->u.vendor_info.crssi));
        crssi_value = IMCP_SNIP_CRSSI_GET_FUNCTIONALITY(packet->u.vendor_info.crssi);
    }
    imcp_snip_release_packet(AL_CONTEXT packet);

}
EXPORT_SYMBOL(mesh_ng_check_for_parent_state);
