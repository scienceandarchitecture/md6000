/********************************************************************************
* MeshDynamics
* --------------
* File     : mesh_ng_fsm_evaluate.c
* Comments : Mesh new Generation Evaluate State
* Created  : 4/18/2006
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  2  |8/22/2008 | Changes for SIP                                 | Abhijit|
* -----------------------------------------------------------------------------
* |  1  |6/13/2007 | Eval damp removed for mobile systems            | Sriram |
* -----------------------------------------------------------------------------
* |  0  |4/18/2006 | Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

/**
 * This is a private source file included by mesh_ng_fsm.c, hence
 * It was created to improve readability.
 */
static void _evaluate_execute(AL_CONTEXT_PARAM_DECL_SINGLE)
{
   parent_t *parent;

   if (mesh_mode == _MESH_AP_RELAY)
   {
      mesh_ng_evaluate_list(AL_CONTEXT_SINGLE);

      parent = mesh_ng_get_best_parent(AL_CONTEXT_SINGLE);

      if (parent != current_parent)
      {
         if (mesh_config->evaluation_damping_factor <= 0)
         {
            goto _change;
         }

         if (!_IS_MOBILE)
         {
            if (_evaluation_damping_parent == NULL)
            {
               _evaluation_damping_parent = parent;
               _evaluation_damping_count  = mesh_config->evaluation_damping_factor;
               goto _out;
            }
            else
            {
               if (_evaluation_damping_parent == parent)
               {
                  if (--_evaluation_damping_count <= 0)
                  {
                     goto _change;
                  }
                  else
                  {
                     goto _out;
                  }
               }
               else
               {
                  _evaluation_damping_parent = parent;
                  _evaluation_damping_count  = mesh_config->evaluation_damping_factor;
                  goto _out;
               }
            }
         }

_change:

         /**
          * If parent change has been locked, we skip and get out
          */

         if (AL_ATOMIC_GET(lock_parent_change))
         {
            al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,
                         "MESH_NG	: %s Parent shift to "AL_NET_ADDR_STR" deffered due to change lock",
                         parent->flags & MESH_PARENT_FLAG_PREFERRED ? "Preferred" : "",
                         AL_NET_ADDR_TO_STR(&parent->bssid));
            goto _out;
         }

         _evaluation_damping_parent = NULL;

         al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,
                      "MESH_NG	: %s Parent shift to "AL_NET_ADDR_STR" CH %d required, diff = %d",
                      parent->flags & MESH_PARENT_FLAG_PREFERRED ? "Preferred" : "",
                      AL_NET_ADDR_TO_STR(&parent->bssid),
                      parent->channel,
                      current_parent->score - parent->score);

         al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MESH_NG	: Disabling AP processing before parent switch");
         access_point_enable_disable_processing(AL_CONTEXT 0);

         if (mesh_ng_core_join_best_parent(AL_CONTEXT_SINGLE))
         {
            al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MESH_NG	: Switch failed, performing fallback...");
            _mesh_ng_fsm_set_state(AL_CONTEXT MESH_NG_FSM_STATE_FULL_SCAN);
            return;
         }

         al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MESH_NG	: Enabling AP processing after parent switch");
         access_point_enable_disable_processing(AL_CONTEXT 1);

         mesh_imcp_send_packet_ap_sub_tree_info(AL_CONTEXT_SINGLE);

         mesh_imcp_send_packet_heartbeat(AL_CONTEXT mesh_heart_beat_sqnr);
         mesh_imcp_send_packet_heartbeat2(AL_CONTEXT mesh_heart_beat_sqnr);
         mesh_heart_beat_sqnr++;

         mesh_plugin_post_event(MESH_PLUGIN_EVENT_PARENT_SHIFTED, NULL);
      }
      else
      {
         _evaluation_damping_parent = NULL;
         _evaluation_damping_count  = 0;
      }
   }

_out:
   _mesh_ng_fsm_set_state(AL_CONTEXT MESH_NG_FSM_STATE_RUNNING);
}
