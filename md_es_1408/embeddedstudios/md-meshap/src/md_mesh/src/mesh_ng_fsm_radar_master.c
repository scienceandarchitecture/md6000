/********************************************************************************
* MeshDynamics
* --------------
* File     : mesh_ng_fsm_radar_master.c
* Comments : Radar detect master operation
* Created  : 8/7/2006
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  1  |11/22/2006| Misc changes                                    | Sriram |
* -----------------------------------------------------------------------------
* |  0  |8/7/2006  | Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

/**
 * This is a private source file included by mesh_ng_fsm.c, hence
 * It was created to improve readability.
 */
extern int reboot_in_progress;
extern int threads_status;
extern struct thread_status thread_stat;

static int _dca_thread(AL_CONTEXT_PARAM_DECL al_thread_param_t *param)
{
   al_net_if_t            *net_if;
   al_802_11_operations_t *ex_op;
   int channel;
   int i;
   al_802_11_tx_power_info_t power_info;
   mesh_conf_if_info_t       *if_info;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
   al_set_thread_name(AL_CONTEXT "radar_dca");

   SET_MESH_FLAG(MESH_FLAG_RADAR_FOUND);

   if_info = (mesh_conf_if_info_t *)param->param;
   net_if  = if_info->net_if;
   ex_op   = (al_802_11_operations_t *)net_if->get_extended_operations(net_if);
   channel = ex_op->get_channel(net_if);

   do
   {
      channel = dfs_dca(AL_CONTEXT net_if, channel);
      if (channel != -1)
      {
         break;
      }
	  if(reboot_in_progress) {
		  printk(KERN_EMERG"-----Terminating 'radar_dca' thread-----\n");
		  threads_status &= ~THREAD_STATUS_BIT10_MASK;
		  break;
	  }
	  thread_stat.radar_dca_thread_count++;
      al_thread_sleep(AL_CONTEXT 60000);                /** Sleep for 60 seconds and try again */
   } while (1);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MESH_NG: Re-enbling WM %s on channel %d\n", net_if->name, channel);
   ex_op->set_channel(net_if, channel);

   if_info->radar_found = 0;

   for (i = 0; i < mesh_config->if_count; i++)
   {
      if (!strcmp(net_if->name, mesh_config->if_info[i].name))
      {
         ex_op->set_mode(net_if, AL_802_11_MODE_MASTER, 0);
         ex_op->set_essid(net_if, mesh_config->if_info[i].essid);
         ex_op->set_hide_ssid(net_if, mesh_config->if_info[i].hide_ssid);
         ex_op->set_beacon_interval(net_if, mesh_config->if_info[i].beacon_int);
         power_info.flags = AL_802_11_TXPOW_PERC;
         power_info.power = mesh_config->if_info[i].txpower;
         ex_op->set_tx_power(net_if, &power_info);
         break;
      }
   }

   RESET_MESH_FLAG(MESH_FLAG_RADAR_FOUND);
   AL_ATOMIC_SET(net_if->buffering_state, AL_NET_IF_BUFFERING_STATE_NONE);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
   return 0;
}


static void _radar_master_execute(AL_CONTEXT_PARAM_DECL void *event_data)
{
   al_net_if_t            *net_if;
   al_802_11_operations_t *ex_op;
   int i;
   int found;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
   net_if = (al_net_if_t *)event_data;
   ex_op  = (al_802_11_operations_t *)net_if->get_extended_operations(net_if);

   for (i = 0, found = 0; i < mesh_hardware_info->wm_net_if_count; i++)
   {
      if (mesh_hardware_info->wms_conf[i]->net_if == net_if)
      {
         if (mesh_hardware_info->wms_conf[i]->radar_found)
         {
            al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MESH_NG: Radar already detected on WM %s, ignoring...\n", net_if->name);
            _mesh_ng_fsm_set_state(AL_CONTEXT MESH_NG_FSM_STATE_RUNNING);
            return;
         }
         else
         {
            mesh_hardware_info->wms_conf[i]->radar_found = 1;
            found = 1;
            break;
         }
      }
   }

   if (!found)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MESH_NG: Radar detected on unknown interface %s, ignoring...\n", net_if->name);
      _mesh_ng_fsm_set_state(AL_CONTEXT MESH_NG_FSM_STATE_RUNNING);
      return;
   }

   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MESH_NG: Radar detected on WM %s, performing DCA\n", net_if->name);

   AL_ATOMIC_SET(net_if->buffering_state, AL_NET_IF_BUFFERING_STATE_DROP);
   ex_op->set_mode(net_if, AL_802_11_MODE_INFRA, 0);

   al_create_thread(AL_CONTEXT _dca_thread, (void *)mesh_hardware_info->wms_conf[i], AL_THREAD_PRIORITY_NORMAL, "_dca_thread");
   threads_status |= THREAD_STATUS_BIT10_MASK;

   _mesh_ng_fsm_set_state(AL_CONTEXT MESH_NG_FSM_STATE_RUNNING);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
}
