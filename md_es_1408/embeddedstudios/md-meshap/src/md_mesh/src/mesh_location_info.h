/********************************************************************************
* MeshDynamics
* --------------
* File     : mesh_location_info.h
* Comments : Client Location Information Implementation
* Created  : 12/4/2007
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  0  |12/4/2007 | Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

#ifndef __MESH_LOCATION_INFO_H__
#define __MESH_LOCATION_INFO_H__

void mesh_location_initialize(AL_CONTEXT_PARAM_DECL_SINGLE);
void mesh_location_uninitialize(AL_CONTEXT_PARAM_DECL_SINGLE);
void mesh_location_process_info(AL_CONTEXT_PARAM_DECL al_net_if_t *al_net_if, al_net_addr_t *sta_addr, int rssi);

void mesh_location_send_info(AL_CONTEXT_PARAM_DECL_SINGLE);

#endif /*__MESH_LOCATION_INFO_H__*/
