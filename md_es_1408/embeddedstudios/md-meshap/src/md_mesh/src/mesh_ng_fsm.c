/********************************************************************************
* MeshDynamics
* --------------
* File     : mesh_ng_fsm.c
* Comments : Mesh New Generation State Machine
* Created  : 4/10/2006
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  8  |02/12/2009| Changes for Uplink Scan Single Radio            |Abhijit |
* -----------------------------------------------------------------------------
* |  7  |1/27/2009 | Changes for Uplink Scan Functionality           | Sriram |
* -----------------------------------------------------------------------------
* |  6  |5/14/2008 | Changes for downlink channel change             |Prachiti|
* -----------------------------------------------------------------------------
* |  4  |6/20/2007 | Added signal pattern matching                   | Sriram |
* -----------------------------------------------------------------------------
* |  3  |6/20/2007 | Added MESH_NG_FSM_EVENT_EVALUATE_MOBILE         | Sriram |
* -----------------------------------------------------------------------------
* |  2  |4/26/2007 | Added MESH_NG_FSM_EVENT_FULL_DISCONNECT         | Sriram |
* -----------------------------------------------------------------------------
* |  1  |10/18/2006| Changes for ETSI Radar                          | Sriram |
* -----------------------------------------------------------------------------
* |  0  |4/10/2006 | Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

#include <linux/string.h>
#include <linux/module.h>
#include <linux/netdevice.h>

#include <linux/if.h>
#include "al.h"
#include "al_802_11.h"
#include "al_print_log.h"
#include "al_conf.h"
#include "access_point.h"
#include "al_impl_context.h"
#include "mesh_defs.h"
#include "mesh_table.h"
#include "mesh.h"
#include "mesh_globals.h"
#include "mesh_errors.h"
#include "mesh_on_start.h"
#include "mesh_intervals.h"
#include "imcp_snip.h"
#include "mesh_imcp.h"
#include "mesh_ng.h"
#include "mesh_ng_core.h"
#include "mesh_ng_fsm.h"
#include "mesh_ng_scan.h"
#include "access_point_ext.h"
#include "mesh_location_info.h"
#include "mesh_ng_channel_change.h"
#include "mesh_plugin.h"
#include "mesh_plugin_impl.h"

#define _MESH_NG_FSM_MAX_EVENTS    20

struct _fsm_event_list
{
   int                    event;
   void                   *data;
   struct _fsm_event_list *next;
   struct _fsm_event_list *next_pool;
};

typedef struct _fsm_event_list   _fsm_event_list_t;


al_spinlock_t fsm_splock;
AL_DEFINE_PER_CPU(int, fsm_spvar);

struct al_mutex fsm_mutex;
int fsm_muvar;
int fsm_mupid;

extern access_point_globals_t globals;

AL_DECLARE_GLOBAL(int mesh_ng_fsm_state);
AL_DECLARE_GLOBAL(int mobility_window_item_count);
AL_DECLARE_GLOBAL(mobility_window_t * mobility_window_head);
AL_DECLARE_GLOBAL(mobility_window_t * mobility_window_tail);
AL_DECLARE_GLOBAL(int step_scan_channel_index);

AL_DECLARE_GLOBAL(int _fsm_full_scan_semaphore);
AL_DECLARE_GLOBAL(_fsm_event_list_t _fsm_events[_MESH_NG_FSM_MAX_EVENTS]);
AL_DECLARE_GLOBAL(_fsm_event_list_t * _fsm_events_pool);
AL_DECLARE_GLOBAL(_fsm_event_list_t * _fsm_events_head);
AL_DECLARE_GLOBAL(_fsm_event_list_t * _fsm_events_tail);
AL_DECLARE_GLOBAL(int _evaluation_damping_count);
AL_DECLARE_GLOBAL(parent_t * _evaluation_damping_parent);
AL_DECLARE_GLOBAL(al_u64_t _last_uplink_disable_event);
AL_DECLARE_GLOBAL(al_u64_t _last_uplink_buffer_event);


int purge_imcp_sta;
extern void mesh_clear_imcp_sta(void);

static const char * fsm_event_to_str(int event)
{
	switch (event)
	{
	FSM_EVENT_2_STR(START);
	FSM_EVENT_2_STR(DISCONNECT);
	FSM_EVENT_2_STR(SAMPLE);
	FSM_EVENT_2_STR(HEARTBEAT);
	FSM_EVENT_2_STR(EVALUATE);
	FSM_EVENT_2_STR(CHECK_PARENT);
	FSM_EVENT_2_STR(UPLINK_DISABLE);
	FSM_EVENT_2_STR(RADAR_MASTER);
	FSM_EVENT_2_STR(RADAR_SLAVE);
	FSM_EVENT_2_STR(FULL_DISCONNECT);
	FSM_EVENT_2_STR(EVALUATE_MOBILE);
	FSM_EVENT_2_STR(EVALUATE_ADHOC);
	FSM_EVENT_2_STR(STEP_SCAN);
	FSM_EVENT_2_STR(STEP_SCAN_UPLINK);
	FSM_EVENT_2_STR(UPLINK_BUFFER);
	}

	return "unknown";
}

static const char * fsm_state_to_str(int state)
{
	switch (state)
	{
	FSM_STATE_2_STR(STOPPED);
	FSM_STATE_2_STR(FULL_SCAN);
	FSM_STATE_2_STR(ALTERNATE_SCAN);
	FSM_STATE_2_STR(RUNNING);
	FSM_STATE_2_STR(SAMPLE);
	FSM_STATE_2_STR(HEARTBEAT);
	FSM_STATE_2_STR(EVALUATE);
	FSM_STATE_2_STR(CHECK_PARENT);
	FSM_STATE_2_STR(UPLINK_DISABLE);
	FSM_STATE_2_STR(RADAR_MASTER);
	FSM_STATE_2_STR(EVALUATE_MOBILE);
	FSM_STATE_2_STR(EVALUATE_ADHOC);
	FSM_STATE_2_STR(STEP_SCAN);
	FSM_STATE_2_STR(ALT_SCAN_ADHOC);
	FSM_STATE_2_STR(STEP_SCAN_UPLINK);
	FSM_STATE_2_STR(UPLINK_BUFFER);
	}

	return "unknown";
}

static const char * mesh_state_to_str(int state)
{
	switch (state)
	{
	MESH_STATE_2_STR(NOT_STARTED);
	MESH_STATE_2_STR(DYNAMIC_CHANNEL_SCAN);
	MESH_STATE_2_STR(SEEKING_FINAL_ASSOCIATION);
	MESH_STATE_2_STR(RUNNING);
	MESH_STATE_2_STR(SEEKING_NEW_PARENT);
	MESH_STATE_2_STR(INITIAL_WDS_SCAN);
	MESH_STATE_2_STR(SEEKING_INITIAL_ASSOCIATION);
	MESH_STATE_2_STR(STOPPPING);
	MESH_STATE_2_STR(INIT_DS);
	MESH_STATE_2_STR(INIT_WM);
	}

	return "unknown";
}

static AL_INLINE void _mesh_ng_fsm_set_state(AL_CONTEXT_PARAM_DECL int new_state)
{
	int prev_mesh_state = mesh_state;

	if (mesh_ng_fsm_state != new_state)
		al_print_log(AL_CONTEXT AL_LOG_TYPE_DEBUG, "MESH_NG: DEBUG: FSM: Changing fsm state from %s to %s ,"
							"prev_mesh_state = %d, new_state = %d, mesh_ng_fsm_state = %d, %s : %d\n",
							fsm_state_to_str(mesh_ng_fsm_state), fsm_state_to_str(new_state), 
							prev_mesh_state, new_state, mesh_ng_fsm_state,__func__, __LINE__);

   mesh_ng_fsm_state = new_state;
   switch (mesh_ng_fsm_state)
   {
   case MESH_NG_FSM_STATE_FULL_SCAN:
   case MESH_NG_FSM_STATE_ALTERNATE_SCAN:
   case MESH_NG_FSM_STATE_ALT_SCAN_ADHOC:
      mesh_state = _MESH_STATE_SEEKING_NEW_PARENT;
      break;

   case MESH_NG_FSM_STATE_RUNNING:
   case MESH_NG_FSM_STATE_SAMPLE:
   case MESH_NG_FSM_STATE_HEARTBEAT:
   case MESH_NG_FSM_STATE_EVALUATE:
   case MESH_NG_FSM_STATE_EVALUATE_MOBILE:
   case MESH_NG_FSM_STATE_EVALUATE_ADHOC:
   case MESH_NG_FSM_STATE_STEP_SCAN:
   case MESH_NG_FSM_STATE_STEP_SCAN_UPLINK:
   case MESH_NG_FSM_STATE_CHECK_PARENT:
   case MESH_NG_FSM_STATE_UPLINK_DISABLE:
   case MESH_NG_FSM_STATE_UPLINK_BUFFER:
   case MESH_NG_FSM_STATE_RADAR_MASTER:
      mesh_state = _MESH_STATE_RUNNING;
      break;
   }

	if (prev_mesh_state != mesh_state)
		al_print_log(AL_CONTEXT AL_LOG_TYPE_DEBUG, "MESH_NG: DEBUG: FSM: Changing mesh state from %s to %s"
																 "prev_mesh_state = %d, mesh_state = %d, %s : %d\n",
				mesh_state_to_str(prev_mesh_state), mesh_state_to_str(mesh_state), prev_mesh_state, mesh_state,__func__, __LINE__);
}


#include "mesh_ng_fsm_full_scan.c"
#include "mesh_ng_fsm_alt_scan.c"
#include "mesh_ng_fsm_running.c"
#include "mesh_ng_fsm_sample.c"
#include "mesh_ng_fsm_heartbeat.c"
#include "mesh_ng_fsm_evaluate.c"
#include "mesh_ng_fsm_check_parent.c"
#include "mesh_ng_fsm_uplink_disable.c"
#include "mesh_ng_fsm_radar_master.c"
#include "mesh_ng_fsm_evaluate_mobile.c"
#include "mesh_ng_fsm_evaluate_adhoc.c"
#include "mesh_ng_fsm_step_scan.c"
#include "mesh_ng_fsm_alt_scan_adhoc.c"
#include "mesh_ng_fsm_step_scan_uplink.c"
#include "mesh_ng_fsm_uplink_buffer.c"

int mesh_ng_fsm_initialize(AL_CONTEXT_PARAM_DECL_SINGLE)
{
   int               i;
   _fsm_event_list_t *temp;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_NG : FLOW : Enter: %s : %d\n", __func__,__LINE__);

   mesh_ng_fsm_state        = MESH_NG_FSM_STATE_STOPPED;
   _fsm_full_scan_semaphore = al_create_semaphore(AL_CONTEXT 1, 1);
   _fsm_events_pool         = NULL;
   _fsm_events_head         = NULL;
   _fsm_events_tail         = NULL;
   temp = NULL;
   current_sampling_parent    = NULL;
   _evaluation_damping_count  = 0;
   _evaluation_damping_parent = NULL;
   _last_uplink_disable_event = 0;
   mobility_window_head       = NULL;
   mobility_window_tail       = NULL;
   mobility_window_item_count = 0;
   step_scan_channel_index    = 0;
   _last_uplink_buffer_event  = 0;


   al_spin_lock_init(&fsm_splock);
   al_mutex_init(&fsm_mutex);


   for (i = 0; i < _MESH_NG_FSM_MAX_EVENTS; i++)
   {
      memset(&_fsm_events[i], 0, sizeof(_fsm_event_list_t));
      if (_fsm_events_pool == NULL)
      {
         _fsm_events_pool = &_fsm_events[i];
      }
      else
      {
         temp->next_pool = &_fsm_events[i];
      }
      temp = &_fsm_events[i];
   }

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_NG : FLOW : Exit: %s : %d\n", __func__,__LINE__);
   return 0;
}


int mesh_ng_fsm_uninitialize(AL_CONTEXT_PARAM_DECL_SINGLE)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_NG : FLOW : Enter: %s : %d\n", __func__,__LINE__);
   mesh_ng_fsm_state = MESH_NG_FSM_STATE_STOPPED;

   al_destroy_semaphore(AL_CONTEXT _fsm_full_scan_semaphore);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_NG : FLOW : Exit: %s : %d\n", __func__,__LINE__);
   return 0;
}


int mesh_ng_fsm_scanner_trigger(AL_CONTEXT_PARAM_DECL_SINGLE)
{
   int ret;

   ret = -1;

   al_wait_on_semaphore(AL_CONTEXT _fsm_full_scan_semaphore, AL_WAIT_TIMEOUT_INFINITE);
   if ((mesh_ng_fsm_state == MESH_NG_FSM_STATE_FULL_SCAN) ||
       (mesh_ng_fsm_state == MESH_NG_FSM_STATE_ALTERNATE_SCAN))
   {
      goto _out;
   }

   ret = mesh_ng_scan_parents(AL_CONTEXT MESH_NG_SCAN_MODE_MONITOR_NET_IF);

_out:
   al_release_semaphore(AL_CONTEXT _fsm_full_scan_semaphore, 1);

   return ret;
}


static void _process_event(AL_CONTEXT_PARAM_DECL int event, void *event_data)
{
   al_802_11_operations_t *operations;
   int is_eth1_down = 0;
	al_print_log(AL_CONTEXT AL_LOG_TYPE_DEBUG, "MESH_NG: DEBUG: FSM: In fsm state %s, Received event %si %s : %d\n",
														fsm_state_to_str(mesh_ng_fsm_state), fsm_event_to_str(event), __func__, __LINE__);

   switch (mesh_ng_fsm_state)
   {
   case MESH_NG_FSM_STATE_STOPPED:
      if (event == MESH_NG_FSM_EVENT_START)
      {
         _mesh_ng_fsm_set_state(AL_CONTEXT MESH_NG_FSM_STATE_RUNNING);
         al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MESH_NG: INFO : Starting FSM... %s : %d\n", __func__, __LINE__);
      }
      else
      {
         al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MESH_NG: INFO : Ignoring event %d in Stopped state %s : %d\n", event, __func__, __LINE__);
      }
      break;

   case MESH_NG_FSM_STATE_RUNNING:
      switch (event)
      {
      case MESH_NG_FSM_EVENT_DISCONNECT:
         if (_DISJOINT_ADHOC_MODE &&
             AL_NET_IF_IS_ETHERNET(mesh_hardware_info->ds_net_if) &&
             purge_imcp_sta)
         {
            is_eth1_down = 1;
         }

         if (_DISJOINT_ADHOC_MODE)
         {
            if (mesh_hardware_info->monitor_type != MONITOR_TYPE_ABSENT)
                _mesh_ng_fsm_set_state(AL_CONTEXT MESH_NG_FSM_STATE_ALT_SCAN_ADHOC);
            else
                _mesh_ng_fsm_set_state(AL_CONTEXT MESH_NG_FSM_STATE_FULL_SCAN);
         }
         else
         {
            if(mesh_hardware_info->monitor_type != MONITOR_TYPE_ABSENT)
                _mesh_ng_fsm_set_state(AL_CONTEXT MESH_NG_FSM_STATE_ALTERNATE_SCAN);
            else
                _mesh_ng_fsm_set_state(AL_CONTEXT MESH_NG_FSM_STATE_FULL_SCAN);
         }
         current_parent->flags |= MESH_PARENT_FLAG_DISABLED;
         ++current_parent->disconnect_count;

         if (AL_NET_IF_IS_WIRELESS(mesh_hardware_info->ds_net_if))
         {
            operations = mesh_hardware_info->ds_net_if->get_extended_operations(mesh_hardware_info->ds_net_if);
            if (operations != NULL)
            {
               operations->enable_ds_verification_opertions(mesh_hardware_info->ds_net_if, 0);
               operations->send_disassoc_from_station(mesh_hardware_info->ds_net_if, 1);
            }
         }
         else if (is_eth1_down == 1)
         {
            purge_imcp_sta = 0;
         }
			al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MESH_NG: INFO : Received disconnect event... %s : %d\n", __func__, __LINE__);
         break;

      case MESH_NG_FSM_EVENT_FULL_DISCONNECT:
         if (_DISJOINT_ADHOC_MODE)
         {
         }
         else
         {
            al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MESH_NG : INFO : Received full disconnect event... %s : %d\n", __func__, __LINE__);
            _mesh_ng_fsm_set_state(AL_CONTEXT MESH_NG_FSM_STATE_FULL_SCAN);
         }
         break;

      case MESH_NG_FSM_EVENT_SAMPLE:
         _mesh_ng_fsm_set_state(AL_CONTEXT MESH_NG_FSM_STATE_SAMPLE);
         break;

      case MESH_NG_FSM_EVENT_HEARTBEAT:
         _mesh_ng_fsm_set_state(AL_CONTEXT MESH_NG_FSM_STATE_HEARTBEAT);
         break;

      case MESH_NG_FSM_EVENT_EVALUATE:
         _mesh_ng_fsm_set_state(AL_CONTEXT MESH_NG_FSM_STATE_EVALUATE);
         break;

      case MESH_NG_FSM_EVENT_CHECK_PARENT:
         _mesh_ng_fsm_set_state(AL_CONTEXT MESH_NG_FSM_STATE_CHECK_PARENT);
         break;

      case MESH_NG_FSM_EVENT_UPLINK_DISABLE:
         _mesh_ng_fsm_set_state(AL_CONTEXT MESH_NG_FSM_STATE_UPLINK_DISABLE);
         break;

      case MESH_NG_FSM_EVENT_RADAR_MASTER:
         _mesh_ng_fsm_set_state(AL_CONTEXT MESH_NG_FSM_STATE_RADAR_MASTER);
         break;

      case MESH_NG_FSM_EVENT_RADAR_SLAVE:
         mesh_imcp_send_packet_buffering_command(AL_CONTEXT _DS_NET_IF, IMCP_SNIP_BUFFERING_TYPE_RADAR_DETECTED, 0);
         current_parent->flags |= MESH_PARENT_FLAG_DISABLED;
         ++current_parent->disconnect_count;
         al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MESH_NG: Radar detected, performing alternate scan...\n", event);
         break;

      case MESH_NG_FSM_EVENT_EVALUATE_MOBILE:
         _mesh_ng_fsm_set_state(AL_CONTEXT MESH_NG_FSM_STATE_EVALUATE_MOBILE);
         break;

      case MESH_NG_FSM_EVENT_EVALUATE_ADHOC:
         _mesh_ng_fsm_set_state(AL_CONTEXT MESH_NG_FSM_STATE_EVALUATE_ADHOC);
         break;

      case MESH_NG_FSM_EVENT_STEP_SCAN:
         _mesh_ng_fsm_set_state(AL_CONTEXT MESH_NG_FSM_STATE_STEP_SCAN);
         break;

      case MESH_NG_FSM_EVENT_STEP_SCAN_UPLINK:
         _mesh_ng_fsm_set_state(AL_CONTEXT MESH_NG_FSM_STATE_STEP_SCAN_UPLINK);
         break;

      case MESH_NG_FSM_EVENT_UPLINK_BUFFER:
         _mesh_ng_fsm_set_state(AL_CONTEXT MESH_NG_FSM_STATE_UPLINK_BUFFER);
         break;

      default:
         al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MESH_NG: INFO : Ignoring event %d in Running state %s : %d\n", event, __func__, __LINE__);
      }
      break;

   default:
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MESH_NG: INFO : Ignoring event %d in %d state %s : %d\n", event, __func__, __LINE__);
   }
}


static void _fsm_execute(AL_CONTEXT_PARAM_DECL void *event_data)
{
   switch (mesh_ng_fsm_state)
   {
   case MESH_NG_FSM_STATE_FULL_SCAN:
      _full_scan_execute(AL_CONTEXT_SINGLE);
      break;

   case MESH_NG_FSM_STATE_ALTERNATE_SCAN:
      _alternate_scan_execute(AL_CONTEXT_SINGLE);
      break;

   case MESH_NG_FSM_STATE_RUNNING:
      _running_execute(AL_CONTEXT_SINGLE);
      break;

   case MESH_NG_FSM_STATE_SAMPLE:
      _sample_execute(AL_CONTEXT_SINGLE);
      break;

   case MESH_NG_FSM_STATE_HEARTBEAT:
      _heartbeat_execute(AL_CONTEXT_SINGLE);
      break;

   case MESH_NG_FSM_STATE_EVALUATE:
      _evaluate_execute(AL_CONTEXT_SINGLE);
      break;

   case MESH_NG_FSM_STATE_CHECK_PARENT:
      _check_parent_execute(AL_CONTEXT event_data);
      break;

   case MESH_NG_FSM_STATE_UPLINK_DISABLE:
      _uplink_disable_execute(AL_CONTEXT event_data);
      break;

   case MESH_NG_FSM_STATE_RADAR_MASTER:
      _radar_master_execute(AL_CONTEXT event_data);
      break;

   case MESH_NG_FSM_STATE_EVALUATE_MOBILE:
      _evaluate_mobile_execute(AL_CONTEXT_SINGLE);
      break;

   case MESH_NG_FSM_STATE_EVALUATE_ADHOC:
      _evaluate_adhoc_execute(AL_CONTEXT_SINGLE);
      break;

   case MESH_NG_FSM_STATE_STEP_SCAN:
      _step_scan_execute(AL_CONTEXT event_data);
      break;

   case MESH_NG_FSM_STATE_ALT_SCAN_ADHOC:
      _alternate_scan_adhoc_execute(AL_CONTEXT_SINGLE);
      break;

   case MESH_NG_FSM_STATE_STEP_SCAN_UPLINK:
      _step_scan_uplink_execute(AL_CONTEXT event_data);
      break;

   case MESH_NG_FSM_STATE_UPLINK_BUFFER:
      _uplink_buffer_execute(AL_CONTEXT event_data);
      break;
   }
}


void mesh_ng_fsm_execute(AL_CONTEXT_PARAM_DECL_SINGLE)
{
   _fsm_execute(AL_CONTEXT NULL);
}


void mesh_ng_fsm_process_events(AL_CONTEXT_PARAM_DECL_SINGLE)
{
   unsigned long flags;
   int           event;
   void          *event_data;
   int           i;

   for (i = 0; i < _MESH_NG_FSM_MAX_EVENTS; i++)
   {
      al_spin_lock(fsm_splock, fsm_spvar);

      if (_fsm_events_head == NULL)
      {
		 al_spin_unlock(fsm_splock, fsm_spvar);
         break;
      }

      event      = _fsm_events_head->event;
      event_data = _fsm_events_head->data;
      _fsm_events_head->next_pool = _fsm_events_pool;
      _fsm_events_pool            = _fsm_events_head;
      _fsm_events_head            = _fsm_events_head->next;

      if (_fsm_events_head == NULL)
      {
         _fsm_events_tail = NULL;
      }

	  al_spin_unlock(fsm_splock, fsm_spvar);

      _process_event(AL_CONTEXT event, event_data);
      _fsm_execute(AL_CONTEXT event_data);
   }
}


int mesh_ng_fsm_send_event(AL_CONTEXT_PARAM_DECL int event, void *event_data)
{
   unsigned long     flags;
   _fsm_event_list_t *temp;

   if ((mesh_ng_fsm_state == MESH_NG_FSM_STATE_UPLINK_DISABLE) ||
       (mesh_ng_fsm_state == MESH_NG_FSM_STATE_UPLINK_BUFFER))
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_NG : ERROR : mesh_ng_fsm_state %d -- %s : %d\n", mesh_ng_fsm_state, __func__,__LINE__);
      return -1;
   }

   if (event == MESH_NG_FSM_EVENT_UPLINK_DISABLE)
   {
      al_u64_t tick;
      al_u64_t timeout;

      tick    = al_get_tick_count(AL_CONTEXT_SINGLE);
      timeout = _last_uplink_disable_event + (int)event_data;

      if (al_timer_before(tick, timeout))
      {
         al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MESH_NG: INFO: Uplink disable event already queued, ignoring... %s : %d\n", __func__,__LINE__);
         return -2;
      }
      else
      {
         _last_uplink_disable_event = tick;
      }
   }

   if (event == MESH_NG_FSM_EVENT_UPLINK_BUFFER)
   {
      al_u64_t tick;
      al_u64_t timeout;

      tick    = al_get_tick_count(AL_CONTEXT_SINGLE);
      timeout = _last_uplink_disable_event + (int)event_data;

      if (al_timer_before(tick, timeout))
      {
         al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MESH_NG: INFO : Uplink buffer event already queued, ignoring... %s : %d\n", __func__,__LINE__);
         return -3;
      }
      else
      {
         _last_uplink_buffer_event = tick;
      }
   }

   al_spin_lock(fsm_splock, fsm_spvar);
   temp = _fsm_events_pool;
   if (temp != NULL)
   {
      _fsm_events_pool = _fsm_events_pool->next_pool;
   }
	al_spin_unlock(fsm_splock, fsm_spvar);

   if (temp == NULL)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MESH_NG: INFO : Exceeded max simultaneous FSM events, ignoring event %d %s : %d\n", event, __func__,__LINE__);
      return -2;
   }

   temp->next      = NULL;
   temp->next_pool = NULL;
   temp->event     = event;
   temp->data      = event_data;

   al_spin_lock(fsm_splock, fsm_spvar);

   if (_fsm_events_head != NULL)
   {
      _fsm_events_tail->next = temp;
      _fsm_events_tail       = temp;
   }
   else
   {
      _fsm_events_head = temp;
      _fsm_events_tail = temp;
   }

	al_spin_unlock(fsm_splock, fsm_spvar);

   return 0;
}


void mesh_ng_fsm_update_downlink_vendor_info(AL_CONTEXT_PARAM_DECL_SINGLE)
{
   int                    i;
   al_net_if_t            *net_if;
   al_802_11_operations_t *ex_op;
   char                   vendor_info[256];
   char                   vendor_info_len;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_NG : FLOW : Enter: %s : %d\n", __func__,__LINE__);

   for (i = 0; i < mesh_hardware_info->wm_net_if_count; i++)
    {
      net_if = mesh_hardware_info->wms_conf[i]->net_if;
      if (AL_NET_IF_IS_WIRELESS(net_if) && (mesh_hardware_info->wms_conf[i]->use_type == AL_CONF_IF_USE_TYPE_WM))
      {
         ex_op  = net_if->get_extended_operations(net_if);

         if (ex_op != NULL)
         {
            if (mesh_hardware_info->wms_conf[i]->service != AL_CONF_IF_SERVICE_CLIENT_ONLY)
            {
               vendor_info_len = mesh_imcp_get_vendor_info_buffer(AL_CONTEXT vendor_info, 1);
               ex_op->set_beacon_vendor_info(net_if, vendor_info, vendor_info_len);
            }
            else
            {
               ex_op->set_beacon_vendor_info(net_if, NULL, 0);
            }
         }
      }		
   }
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_NG : FLOW : Exit: %s : %d\n", __func__,__LINE__);
}

void mesh_ng_change_current_mode_func(int crssi_limited)
{
    int flag = 0;
    access_point_config_t *config;
    if (crssi_limited)
        current_parent->flags |= MESH_PARENT_FLAG_LIMITED;
    else
        current_parent->flags &= ~MESH_PARENT_FLAG_LIMITED;

    if ((current_parent->flags & MESH_PARENT_FLAG_LIMITED) && !_IS_LIMITED)
    {
        func_mode = _FUNC_MODE_LFN;
        mesh_mode = _MESH_AP_RELAY;
        flag = 1;
    }
    else if (!(current_parent->flags & MESH_PARENT_FLAG_LIMITED) && _IS_LIMITED)
    {
        func_mode = _FUNC_MODE_FFN;
        mesh_mode = _MESH_AP_RELAY;
        flag = 1;
    }
    if (flag)
    {
        config = &globals.config;
        config->ds_net_if_info.net_if      = mesh_hardware_info->ds_net_if;
        config->ds_net_if_info.ack_timeout = mesh_hardware_info->ds_if_ack_timeout;
        config->ds_net_if_info.txpower     = mesh_hardware_info->ds_if_tx_power;
        config->ds_net_if_info.txrate      = mesh_hardware_info->ds_if_tx_rate;
        strcpy(config->ds_net_if_info.essid, mesh_hardware_info->ds_conf->essid);
        mesh_ng_fsm_update_downlink_vendor_info();
    }
}
