/********************************************************************************
* MeshDynamics
* --------------
* File     : mesh_ng_fsm_full_scan.c
* Comments : Mesh New Generation Full Scan State
* Created  : 4/10/2006
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  2  |2/2/2007  | Restore LED state after joining                 | Sriram |
* -----------------------------------------------------------------------------
* |  1  |10/18/2006| Changes for ETSI Radar                          | Sriram |
* -----------------------------------------------------------------------------
* |  0  |4/10/2006 | Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/
extern void change_ds_from_wired_to_wireless(void);
extern void mesh_ng_clear_mobility_window(void);

/**
 * This is a private source file included by mesh_ng_fsm.c, hence
 * It was created to improve readability.
 */
void enable_disable_processing(AL_CONTEXT_PARAM_DECL unsigned char enable)
{
   unsigned int       flags;

   if (enable)
   {
      access_point_enable_disable_processing(AL_CONTEXT 1);
   }
   else
   {
      access_point_enable_disable_processing(AL_CONTEXT 0);
      mesh_table_remove_all_entries(AL_CONTEXT_SINGLE);
      access_point_remove_all_sta(AL_CONTEXT_SINGLE);
   }
}


static int _parent_enum_func(AL_CONTEXT_PARAM_DECL void *enum_param, parent_t *parent)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MESH_NG :\t"AL_NET_ADDR_STR " CH=%d SG=%d SCORE=%d\n",
                AL_NET_ADDR_TO_STR(&parent->bssid),
                parent->channel,
                parent->signal,
                parent->score);

   return 0;
}


static void _full_scan_execute(AL_CONTEXT_PARAM_DECL_SINGLE)
{
   int                 count;
   al_notify_message_t message;
    unsigned int       flags;

   al_wait_on_semaphore(AL_CONTEXT _fsm_full_scan_semaphore, AL_WAIT_TIMEOUT_INFINITE);

   AL_ATOMIC_SET(_DS_NET_IF->buffering_state, AL_NET_IF_BUFFERING_STATE_DROP);

   change_ds_from_wired_to_wireless();

   if (!_DISJOINT_ADHOC_MODE) { 
      prescan_setup_wms(AL_CONTEXT 1); // Moved for testing
      enable_disable_processing(AL_CONTEXT 0); // Order wise we put the enable_disable_processing in middle.
   }
   mesh_ng_clear_parents(AL_CONTEXT_SINGLE);
   mesh_ng_clear_mobility_window();

   AL_ATOMIC_SET(_DS_NET_IF->buffering_state, AL_NET_IF_BUFFERING_STATE_NONE);


   count = mesh_ng_scan_parents(AL_CONTEXT MESH_NG_SCAN_MODE_DS_NET_IF);

   if (count <= 0)
   {
      goto _out;
   }

   mesh_ng_evaluate_list(AL_CONTEXT_SINGLE);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MESH_NG : === PARENT LIST BEGIN ====");
#if 0
   if (mesh_ng_parent_select_on_signal_strength(AL_CONTEXT_SINGLE)){
       goto _out;
   }
#endif
   mesh_ng_enumerate_parents(AL_CONTEXT MESH_NG_ENUMERATE_PARENTS_TYPE_ALL, NULL, _parent_enum_func, 0);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MESH_NG : === PARENT LIST END ====");

   /* associate with best parent */

   printk(KERN_INFO "%s:%d: Joining Best Parent \n", __FUNCTION__, __LINE__);
   if (mesh_ng_core_join_best_parent(AL_CONTEXT_SINGLE))
   {
      printk(KERN_INFO "%s:%d: Joining Best Parent \n", __FUNCTION__, __LINE__);
      goto _out;
   }

   _mesh_ng_fsm_set_state(AL_CONTEXT MESH_NG_FSM_STATE_RUNNING);

   enable_disable_processing(AL_CONTEXT 1);

   AL_ATOMIC_SET(_DS_NET_IF->buffering_state, AL_NET_IF_BUFFERING_STATE_NONE);
   if (!_IS_LIMITED)
   {
      postscan_setup_wms(AL_CONTEXT_SINGLE);
   }

	mesh_ng_fsm_update_downlink_vendor_info();

   message.message_type = AL_NOTIFY_MESSAGE_TYPE_SET_STATE;
   message.message_data = (void *)'G';
   al_notify_message(AL_CONTEXT & message);

_out:
   al_release_semaphore(AL_CONTEXT _fsm_full_scan_semaphore, 1);
}
