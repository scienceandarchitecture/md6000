/********************************************************************************
* MeshDynamics
* --------------
* File     : mesh_monitor.c
* Comments : Mesh Monitor IMPL file
* Created  : 10/7/2004
* Author   : Anand Bhange
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* | 13  |6/13/2007 | Signal Pattern matching logic implemented       | Sriram |
* -----------------------------------------------------------------------------
* | 12  |9/19/2006 | Scanner disabled in ROOT mode                   | Sriram |
* -----------------------------------------------------------------------------
* | 11  |10/5/2005 | Oscillation related changes                     | Sriram |
* -----------------------------------------------------------------------------
* | 10  |10/4/2005 | Changes for IMCP WM Info                        | Sriram |
* -----------------------------------------------------------------------------
* |  9  |9/21/2005 | Current AP signal not update in monitor mode    | Sriram |
* -----------------------------------------------------------------------------
* |  8  |9/12/2005 | Scan loop structure modified                    | Sriram |
* -----------------------------------------------------------------------------
* |  7  |6/9/2005  | IMCP version check added                        | Sriram |
* -----------------------------------------------------------------------------
* |  6  |4/17/2005 | mode parameter and auto reboot                  | Sriram |
* -----------------------------------------------------------------------------
* |  5  |04/02/2005| Misc bug fixes while monitor radio testing      | Anand  |
* -----------------------------------------------------------------------------
* |  4  |03/15/2005| Bug Fixed                                       | Anand  |
* -----------------------------------------------------------------------------
* |  3  |2/21/2005 | Vendor info added to beacons,HandShake Res to VI| Anand  |
* -----------------------------------------------------------------------------
* |  2  |12/8/2004 | Printed seperate message for IMCP timeouts      | Sriram |
* -----------------------------------------------------------------------------
* |  1  |10/28/2004| Bug fixed for monitor radio                     | Anand  |
* -----------------------------------------------------------------------------
* |  0  |10/7/2004 | Created                                         | Anand  |
* -----------------------------------------------------------------------------
********************************************************************************/

#include <linux/string.h>
#include <linux/kernel.h>
#include "al.h"
#include "al_print_log.h"
#include "access_point.h"
#include "al_impl_context.h"
#include "al_conf.h"
#include "mesh_defs.h"
#include "mesh_table.h"
#include "mesh.h"
#include "mesh_globals.h"
#include "mesh_errors.h"
#include "imcp_snip.h"
#include "mesh_imcp.h"
#include "mesh_intervals.h"
#include "mesh_monitor.h"
#include "mesh_ng.h"
#include "mesh_ng_fsm.h"
#include "mesh_ng_scan.h"

AL_DECLARE_GLOBAL(int mesh_scan_monitor_exit_wait_event);
AL_DECLARE_GLOBAL(int mesh_monitor_wait_event);

static int mesh_scan_monitor_function(AL_CONTEXT_PARAM_DECL al_thread_param_t *param)
{
   int      ret;

   al_set_thread_name(AL_CONTEXT "scan_thread");
   al_reset_event(AL_CONTEXT mesh_scan_monitor_exit_wait_event);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Enter %s<%d>\n", __func__, __LINE__);

   while (1)
   {
	   thread_stat.scan_thread_count++;
	   if(reboot_in_progress) {
		   al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP:ERROR: %s<%d>Terminating -----\n", __func__, __LINE__);
		   threads_status &= ~THREAD_STATUS_BIT9_MASK;
		   break;
	   }
      if (mesh_mode == _MESH_AP_RELAY)
      {
         ret = mesh_ng_fsm_scanner_trigger(AL_CONTEXT_SINGLE);

         al_thread_sleep(AL_CONTEXT 100);

         if (ret < 0)
         {
            al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MESH_NG: Error occurred while scanning monitor net_if.");
            /** Precautionary measure */
            al_thread_sleep(AL_CONTEXT 1000);
            continue;
         }

         if ((mesh_state == _MESH_STATE_STOPPPING) || (mesh_state == _MESH_STATE_NOT_STARTED))
         {
            break;
         }

         mesh_ng_fsm_send_event(AL_CONTEXT MESH_NG_FSM_EVENT_EVALUATE_MOBILE, NULL);
         al_set_event(AL_CONTEXT mesh_thread_wait_event);
      }
      else
      {
         al_thread_sleep(AL_CONTEXT 1000);
      }
   }

   al_reset_event(AL_CONTEXT mesh_monitor_wait_event);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MESH_NG	: Mesh Scan Monitor Thread Exited");
   al_set_event(AL_CONTEXT mesh_scan_monitor_exit_wait_event);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Exit %s<%d>\n", __func__, __LINE__);

   return RETURN_SUCCESS;
}


void start_scan_monitor(AL_CONTEXT_PARAM_DECL_SINGLE)
{
   al_create_thread_on_cpu(AL_CONTEXT mesh_scan_monitor_function, NULL,0, "mesh_scan_monitor_function");
   threads_status |= THREAD_STATUS_BIT9_MASK;
}
