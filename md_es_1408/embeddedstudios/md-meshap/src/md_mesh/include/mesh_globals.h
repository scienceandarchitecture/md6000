/********************************************************************************
* MeshDynamics
* --------------
* File     : mesh_globals.h
* Comments : mesh global variables (for Linux)
* Created  : 5/31/2004
* Author   : Anand Bhange
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* | 11  |02/12/2009| Additional Changes for Uplink Scan Functionality|Abhijit |
* -----------------------------------------------------------------------------
* | 10  |1/27/2009 | Changes for Uplink Scan Functionality           | Sriram |
* -----------------------------------------------------------------------------
* |  9  |3/13/2006 | ACL list implemented                            | Sriram |
* -----------------------------------------------------------------------------
* |  8  |02/15/2006| changes for dot11e                              | Bindu  |
* -----------------------------------------------------------------------------
* |  7  |10/5/2005 | Oscillation related changes                     | Sriram |
* -----------------------------------------------------------------------------
* |  6  |6/17/2005 | Added test_flags global variable                | Sriram |
* -----------------------------------------------------------------------------
* |  5  |4/02/2005 | mesh_scan_aps_semafore var added                | Anand  |
* -----------------------------------------------------------------------------
* |  4  |7/23/2004 | mesh_preferred_parent var added                 | Anand  |
* -----------------------------------------------------------------------------
* |  3  |7/5/2004  | mesh_start_stop_event var added                 | Anand  |
* -----------------------------------------------------------------------------
* |  2  |6/15/2004 | mesh_hbp_hash var added                         | Anand  |
* -----------------------------------------------------------------------------
* |  1  |6/1/2004  | PRINT_HW_ADDRESS prints Dev Name                | Anand  |
* -----------------------------------------------------------------------------
* |  0  |5/31/2004 | Created.                                        | Anand  |
* -----------------------------------------------------------------------------
********************************************************************************/


#ifndef __MESH_GLOBALS_H__
#define __MESH_GLOBALS_H__

#ifdef _AL_ROVER_
extern mesh_ap_name_info_t **mesh_name_hash;
#endif


AL_DECLARE_GLOBAL_EXTERN(int mesh_thread_running);
AL_DECLARE_GLOBAL_EXTERN(unsigned int mesh_heart_beat_sqnr);

AL_DECLARE_GLOBAL_EXTERN(int mesh_flags);

AL_DECLARE_GLOBAL_EXTERN(mesh_hardware_config_t * mesh_hardware_info);
AL_DECLARE_GLOBAL_EXTERN(mesh_config_info_t * mesh_config);
AL_DECLARE_GLOBAL_EXTERN(mesh_dot11e_category_t * mesh_dot11e_category_config);
AL_DECLARE_GLOBAL_EXTERN(mesh_acl_config_t * mesh_acl_config);

//AL_DECLARE_GLOBAL_EXTERN(mesh_table_entry_t * *mesh_hash);
AL_DECLARE_GLOBAL_EXTERN(mesh_table_bucket_entry_t  *mesh_hash);
AL_DECLARE_GLOBAL_EXTERN(unsigned int mesh_hash_size);
AL_DECLARE_GLOBAL_EXTERN(mesh_table_entry_t * mesh_first_child);
AL_DECLARE_GLOBAL_EXTERN(int scanning_current_phy_id);
AL_DECLARE_GLOBAL_EXTERN(mesh_heart_beat_processing_info_t * *mesh_hbp_hash);

AL_DECLARE_GLOBAL_EXTERN(int mesh_start_stop_event);
AL_DECLARE_GLOBAL_EXTERN(int mesh_thread_exit_wait_event);
AL_DECLARE_GLOBAL_EXTERN(int mesh_thread_wait_event);
AL_DECLARE_GLOBAL_EXTERN(int mesh_hb_wait_event);
AL_DECLARE_GLOBAL_EXTERN(int mesh_scan_wait_unlock_event);
AL_DECLARE_GLOBAL_EXTERN(int mesh_scan_wait_objection_event);
AL_DECLARE_GLOBAL_EXTERN(int mesh_scan_monitor_exit_wait_event);
AL_DECLARE_GLOBAL_EXTERN(int mesh_monitor_wait_event);

AL_DECLARE_GLOBAL_EXTERN(int mesh_ds_buffer_ack_event);
AL_DECLARE_GLOBAL_EXTERN(int mesh_ds_buffer_stop_event);

#endif /*__MESH_GLOBALS_H__*/
