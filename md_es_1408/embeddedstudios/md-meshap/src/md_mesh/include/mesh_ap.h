/********************************************************************************
* MeshDynamics
* --------------
* File     : mesh_ap.h
* Comments : Access Point Mesh Handler file.
* Created  : 4/20/2004
* Author   : Anand Bhange
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  2  |4/26/2004 | Testing Base Line                               | Anand  |
* -----------------------------------------------------------------------------
* |  1  |4/21/2004 | **Made (major) changes for AL_CONTEXT**         | Anand  |
* -----------------------------------------------------------------------------
* |  0  |4/20/2004 | Created.                                        | Anand  |
* -----------------------------------------------------------------------------
********************************************************************************/



#ifndef __MESH_AP_H__
#define __MESH_AP_H__


void mesh_ap_initialize(AL_CONTEXT_PARAM_DECL_SINGLE);
void mesh_ap_uninitialize(AL_CONTEXT_PARAM_DECL_SINGLE);
int mesh_ap_is_table_entry_present(AL_CONTEXT_PARAM_DECL al_net_addr_t *addr);

#endif /* __MESH_AP_H__ */
