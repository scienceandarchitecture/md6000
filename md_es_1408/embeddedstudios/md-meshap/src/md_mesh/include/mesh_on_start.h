/********************************************************************************
* MeshDynamics
* --------------
* File     : mesh_on_start.h
* Comments : Mesh on start header
* Created  : 4/23/2004
* Author   : Anand Bhange
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  5  |10/18/2006| ETSI Radar changes                              | Sriram |
* -----------------------------------------------------------------------------
* |  4  |02/15/2006| changes for dot11e                              | Bindu  |
* -----------------------------------------------------------------------------
* |  3  |12/22/2004| Added prescan_setup_wms and postscan funcs      | Sriram |
* -----------------------------------------------------------------------------
* |  2  |5/11/2004 | find_parent_for_wireless_ds added to header     | Anand  |
* -----------------------------------------------------------------------------
* |  1  |4/26/2004 | Testing Base Line                               | Anand  |
* -----------------------------------------------------------------------------
* |  0  |4/23/2004 | Created                                         | Anand  |
* -----------------------------------------------------------------------------
********************************************************************************/


#ifndef __MESH_ON_START_H__
#define __MESH_ON_START_H__

int mesh_on_start(AL_CONTEXT_PARAM_DECL access_point_config_t *config, access_point_dot11e_category_t *dot11e_config);

int find_parent_for_wireless_ds(AL_CONTEXT_PARAM_DECL_SINGLE);
int prescan_setup_wms(AL_CONTEXT_PARAM_DECL int before_dfs);

int postscan_setup_wms(AL_CONTEXT_PARAM_DECL_SINGLE);
int dfs_dca(AL_CONTEXT_PARAM_DECL al_net_if_t *net_if, int channel_avoid);
void mesh_on_start_ht_update(AL_CONTEXT_PARAM_DECL access_point_netif_config_info_t  *config, mesh_conf_if_info_t *mesh_conf);
void mesh_on_start_vht_update(AL_CONTEXT_PARAM_DECL access_point_netif_config_info_t  *config,mesh_conf_if_info_t *mesh_conf);
void mesh_on_start_vht_operation_update(AL_CONTEXT_PARAM_DECL access_point_netif_config_info_t  *config,mesh_conf_if_info_t *mesh_conf);
#endif /*__MESH_ON_START_H__*/
