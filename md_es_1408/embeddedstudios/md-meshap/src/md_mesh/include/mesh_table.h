/********************************************************************************
* MeshDynamics
* --------------
* File     : mesh_table.h
* Comments : Mesh routing table defination file.
* Created  : 4/8/2004
* Author   : Anand Bhange
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* | 10  |4/26/2007 | Added mesh_table_entry_find_in_list             | Sriram |
* -----------------------------------------------------------------------------
* |  9  |7/29/2005 | Misc changes                                    | Sriram |
* -----------------------------------------------------------------------------
* |  8  |7/29/2004 | Param added for SUb Tree Info problem           | Anand  |
* -----------------------------------------------------------------------------
* |  7  |7/25/2004 | Fixes for sub-tree info packet                  | Sriram |
* -----------------------------------------------------------------------------
* |  6  |7/19/2004 | mesh_table_process_heart_beat added             | Anand  |
* -----------------------------------------------------------------------------
* |  5  |6/15/2004 | Repeated Mesh HB processing avoided (Hash added)| Anand  |
* -----------------------------------------------------------------------------
* |  4  |6/1/2004  | PRINT_HW_ADDRESS prints Dev Name                | Anand  |
* -----------------------------------------------------------------------------
* |  3  |5/20/2004 | mesh_table_find_relay_ap_for_sta_ap prm changed | Anand  |
* -----------------------------------------------------------------------------
* |  2  |4/26/2004 | Testing Base Line                               | Anand  |
* -----------------------------------------------------------------------------
* |  1  |4/21/2004 | **Made (major) changes for AL_CONTEXT**         | Anand  |
* -----------------------------------------------------------------------------
* |  0  |4/8/2004  | Created                                         | Anand  |
* -----------------------------------------------------------------------------
********************************************************************************/

#ifndef __MESH_TABLE_H__
#define __MESH_TABLE_H__
#include "imcp_snip.h"

#ifdef _AL_ROVER_
struct mesh_ap_name_info
{
   al_net_addr_t            ap_addr;
   char                     name[50];
   struct mesh_ap_name_info *next;
   struct mesh_ap_name_info *prev;
};

typedef struct mesh_ap_name_info   mesh_ap_name_info_t;

char *get_ap_name(AL_CONTEXT_PARAM_DECL al_net_addr_t *ap_addr);

void mesh_add_mesh_name_entry(AL_CONTEXT_PARAM_DECL_SINGLE);
#endif


#define MESH_TABLE_ADD_ERROR_PARENT_NODE_MISSING    -1
#define MESH_TABLE_REMOVE_ERROR                     -2

struct mesh_heart_beat_processing_info
{
   al_net_addr_t                          ap_addr;
   unsigned int                           processed_heartbeat_sqnr;
   struct mesh_heart_beat_processing_info *next;
};

typedef struct mesh_heart_beat_processing_info   mesh_heart_beat_processing_info_t;

extern al_spinlock_t mtable_hbp_splock;
AL_DECLARE_PER_CPU(int, mtable_hbp_spvar);


struct mesh_table_entry
{
   al_atomic_t             ref_cnt;
   al_net_addr_t           sta_addr;
   al_net_if_t             *net_if;

   al_net_addr_t           relay_ap_addr;
   al_net_addr_t           sta_ap_addr;

   unsigned int            sta_hash;
   al_u64_t                create_time;
   al_u64_t                last_rx;
   al_u64_t                last_tx;


   /* members for maintenance of hash tables and link list etc */
   struct mesh_table_entry *parent_ap;
   struct mesh_table_entry *first_child;

   struct mesh_table_entry *next_siblings;
   struct mesh_table_entry *prev_siblings;

   struct mesh_table_entry *next_hash;
   struct mesh_table_entry *prev_hash;
};

typedef struct mesh_table_entry   mesh_table_entry_t;


struct mesh_table_bucket_entry
{
	mesh_table_entry_t *entry;
	al_spinlock_t mtable_hsplock;
};


typedef struct mesh_table_bucket_entry mesh_table_bucket_entry_t;

extern al_spinlock_t mtable_splock;
AL_DECLARE_PER_CPU(int, mtable_spvar);

void mesh_table_initialize(AL_CONTEXT_PARAM_DECL unsigned int md_hash_size);

void mesh_table_uninitialize(AL_CONTEXT_PARAM_DECL_SINGLE);

int mesh_table_entry_add(AL_CONTEXT_PARAM_DECL al_net_if_t *net_if,
                         al_net_addr_t                     *sta_addr,
                         al_net_addr_t                     *relay_ap_addr,
                         al_net_addr_t                     *sta_ap_addr,
                         int                               caller_id);

int mesh_table_entry_add_lock(AL_CONTEXT_PARAM_DECL al_net_if_t *net_if,
                         al_net_addr_t                     *sta_addr,
                         al_net_addr_t                     *relay_ap_addr,
                         al_net_addr_t                     *sta_ap_addr,
                         int                               caller_id);


int mesh_table_entry_remove(AL_CONTEXT_PARAM_DECL al_net_addr_t *sta_addr,
                            int                                 caller_id);

int mesh_table_entry_remove_lock(AL_CONTEXT_PARAM_DECL al_net_addr_t *sta_addr,
                            int                                 caller_id);
mesh_table_entry_t *mesh_table_entry_find(AL_CONTEXT_PARAM_DECL al_net_addr_t *sta_addr);
int mesh_table_find_relay_ap_for_sta_ap(AL_CONTEXT_PARAM_DECL al_net_addr_t *sta_addr, al_net_if_t **relay_net_if, al_net_addr_t *relay_addr);

void mesh_table_remove_all_entries(AL_CONTEXT_PARAM_DECL_SINGLE);


int mesh_hb_is_already_processed(AL_CONTEXT_PARAM_DECL al_net_addr_t *sta_addr, unsigned int sqnr);

int mesh_table_get_sta_count(AL_CONTEXT_PARAM_DECL int lock_taken);
void mesh_table_get_sta_entries(AL_CONTEXT_PARAM_DECL imcp_packet_info_t* pi);
void mesh_table_get_immidiate_sta_entries(AL_CONTEXT_PARAM_DECL al_net_addr_t **sta_addrs, unsigned char *children_count);
int mesh_table_update_sta_entries(AL_CONTEXT_PARAM_DECL al_net_if_t *net_if, al_net_addr_t *sender_address, al_net_addr_t *relay_ap_addr, al_net_addr_t *sta_addrs, unsigned char children_count);
int mesh_table_process_heart_beat(AL_CONTEXT_PARAM_DECL al_net_addr_t *immigiate_ap, unsigned int child_count, al_net_addr_t *immidigiate_sta);
mesh_table_entry_t *mesh_table_entry_find_in_list(AL_CONTEXT_PARAM_DECL al_net_addr_t *sta_addr);

void mesh_table_entry_release(AL_CONTEXT_PARAM_DECL mesh_table_entry_t *entry);
#endif /* __MESH_TABLE_H__ */
