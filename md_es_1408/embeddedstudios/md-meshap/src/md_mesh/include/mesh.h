/********************************************************************************
* MeshDynamics
* --------------
* File     : mesh.h
* Comments : Mesh Dynamics mesh main include file.
* Created  : 4/8/2004
* Author   : Anand Bhange
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  16 |1/27/2009 | Changes for Uplink Scan Functionality           | Sriram |
* -----------------------------------------------------------------------------
* |  15 |8/22/2008 | Changes for SIP                                 |Abhijit |
* -----------------------------------------------------------------------------
* |  14 |6/20/2007 | hope_cost re-used for IGMP support              |Prachiti|
* -----------------------------------------------------------------------------
* |  13 |2/6/2007  | FIPS changes                                    |Prachiti|
* -----------------------------------------------------------------------------
* |  12 |10/18/2006| Changes for ETSI Radar                          | Sriram |
* -----------------------------------------------------------------------------
* |  11 |6/21/2005 | Misc changes                                    | Sriram |
* -----------------------------------------------------------------------------
* |  10 |10/28/2004| Changes for monitor radio                       | Anand  |
* -----------------------------------------------------------------------------
* |  9  |6/15/2004 | known_ap_enable_disable func added              | Anand  |
* -----------------------------------------------------------------------------
* |  8  |6/2/2004  | Channel scan states changed                     | Anand  |
* ------------------------------------------------------------------------------
* |  7  |6/1/2004  | PRINT_HW_ADDRESS prints Dev Name                | Anand  |
* -----------------------------------------------------------------------------
* |  6  |5/28/2004 | known_aps operations moved here.                | Anand  |
* -----------------------------------------------------------------------------
* |  5  |5/25/2004 | PRINT_HW_ADDR printing in hex                   | Anand  |
* -----------------------------------------------------------------------------
* |  4  |5/21/2004 | PRINT_HW_ADDRESS to print MAC Addr added        | Anand  |
* -----------------------------------------------------------------------------
* |  3  |5/7/2004  | associate with best parent moved here           | Anand  |
* -----------------------------------------------------------------------------
* |  2  |4/26/2004 | Testing Base Line                               | Anand  |
* -----------------------------------------------------------------------------
* |  1  |4/21/2004 | **Made (major) changes for AL_CONTEXT**         | Anand  |
* -----------------------------------------------------------------------------
* |  0  |4/8/2004  | Created                                         | Anand  |
* -----------------------------------------------------------------------------
********************************************************************************/


#ifndef __MESH_H__
#define __MESH_H__

#define _FUNC_MODE_FFR             1
#define _FUNC_MODE_FFN             2
#define _FUNC_MODE_LFR             3
#define _FUNC_MODE_LFN             4
#define _FUNC_MODE_LFRS            5

#define _DS_MAC                    (&mesh_hardware_info->ds_net_if->config.hw_addr)
#define _WMS_MAC(i)    (&mesh_hardware_info->wms_conf[i]->net_if->config.hw_addr)
#define _MON_MAC                   (&mesh_hardware_info->monitor_net_if->config.hw_addr)
#define _MON_NET_IF                (mesh_hardware_info->monitor_net_if)
#define _DS_NET_IF                 (mesh_hardware_info->ds_net_if)
#define _WMS_NET_IF(i)    (mesh_hardware_info->wms_conf[i]->net_if)
#define _WMS_CONF(i)      (mesh_hardware_info->wms_conf[i])
#define _DS_CONF                   (mesh_hardware_info->ds_conf)
#define _MON_CONF                  (mesh_hardware_info->monitor_conf)

#define _IS_MOBILE                 (mesh_config->mobile_mode && mesh_hardware_info->monitor_type != MONITOR_TYPE_ABSENT)

#define _IS_LIMITED                (_DISJOINT_ADHOC_MODE && (func_mode == _FUNC_MODE_LFR || func_mode == _FUNC_MODE_LFN))
#define _IS_LFR                    (_DISJOINT_ADHOC_MODE && func_mode == _FUNC_MODE_LFR)
#define _IS_LFN                    (_DISJOINT_ADHOC_MODE && func_mode == _FUNC_MODE_LFN)

#define _IS_UPLINK_SCAN_ENABLED    ((mesh_hardware_info->monitor_type == MONITOR_TYPE_ABSENT) && (_DS_CONF->dy_channel_alloc > 0) && (_DS_CONF->dca_list_count > 0))

#ifdef _AL_ROVER_
#define _PRINT_HW_ADDRESS(msg, addr)                                                    \
   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "%s - from %s (%x:%x:%x:%x:%x:%x)", \
                msg,                                                                    \
                get_ap_name(AL_CONTEXT addr),                                           \
                addr->bytes[0],                                                         \
                addr->bytes[1],                                                         \
                addr->bytes[2],                                                         \
                addr->bytes[3],                                                         \
                addr->bytes[4],                                                         \
                addr->bytes[5])

#else
#define _PRINT_HW_ADDRESS(msg, addr)                                          \
   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "%s - %x:%x:%x:%x:%x:%x", \
                msg,                                                          \
                addr->bytes[0],                                               \
                addr->bytes[1],                                               \
                addr->bytes[2],                                               \
                addr->bytes[3],                                               \
                addr->bytes[4],                                               \
                addr->bytes[5])
#endif

#ifdef __DUMP_IMCP_PACKET_SEND
#ifdef _AL_ROVER_
#define _PRINT_HW_ADDRESS_DEBUG(msg, addr)                                        \
   al_print_log(AL_CONTEXT AL_LOG_TYPE_DEBUG, "%s - from %s (%x:%x:%x:%x:%x:%x)", \
                msg,                                                              \
                get_ap_name(AL_CONTEXT addr),                                     \
                addr->bytes[0],                                                   \
                addr->bytes[1],                                                   \
                addr->bytes[2],                                                   \
                addr->bytes[3],                                                   \
                addr->bytes[4],                                                   \
                addr->bytes[5])

#else
#define _PRINT_HW_ADDRESS_DEBUG(msg, addr)                              \
   al_print_log(AL_CONTEXT AL_LOG_TYPE_DEBUG, "%s - %x:%x:%x:%x:%x:%x", \
                msg,                                                    \
                addr->bytes[0],                                         \
                addr->bytes[1],                                         \
                addr->bytes[2],                                         \
                addr->bytes[3],                                         \
                addr->bytes[4],                                         \
                addr->bytes[5])
#endif
#endif

#ifndef BIT
#define BIT(x) (1 << (x))
#endif

#define MESH_HT_CAP_INFO_LDPC_CODING_CAP     ((u16) BIT(0))
#define MESH_HT_CAP_INFO_SUPP_CHANNEL_WIDTH_SET  ((u16) BIT(1))
#define MESH_HT_CAP_INFO_SMPS_MASK           ((u16) (BIT(2) | BIT(3)))
#define MESH_HT_CAP_INFO_SMPS_STATIC         ((u16) 0)
#define MESH_HT_CAP_INFO_SMPS_DYNAMIC        ((u16) BIT(2))
#define MESH_HT_CAP_INFO_SMPS_DISABLED       ((u16) (BIT(2) | BIT(3)))
#define MESH_HT_CAP_INFO_GREEN_FIELD         ((u16) BIT(4))
#define MESH_HT_CAP_INFO_SHORT_GI20MHZ       ((u16) BIT(5))
#define MESH_HT_CAP_INFO_SHORT_GI40MHZ       ((u16) BIT(6))
#define MESH_HT_CAP_INFO_TX_STBC         ((u16) BIT(7))
#define MESH_HT_CAP_INFO_RX_STBC_MASK        ((u16) (BIT(8) | BIT(9)))
#define MESH_HT_CAP_INFO_RX_STBC_1           ((u16) BIT(8))
#define MESH_HT_CAP_INFO_RX_STBC_12          ((u16) BIT(9))
#define MESH_HT_CAP_INFO_RX_STBC_123         ((u16) (BIT(8) | BIT(9)))
#define MESH_HT_CAP_INFO_DELAYED_BA          ((u16) BIT(10))
#define MESH_HT_CAP_INFO_MAX_AMSDU_SIZE      ((u16) BIT(11))
#define MESH_HT_CAP_INFO_DSSS_CCK40MHZ       ((u16) BIT(12))
/* B13 - Reserved (was PSMP support during P802.11n development) */
#define MESH_HT_CAP_INFO_40MHZ_INTOLERANT        ((u16) BIT(14))
#define MESH_HT_CAP_INFO_LSIG_TXOP_PROTECT_SUPPORT   ((u16) BIT(15))




#define MESH_VHT_CAP_MAX_MPDU_LENGTH_3895                 0
#define MESH_VHT_CAP_MAX_MPDU_LENGTH_7991                 1
#define MESH_VHT_CAP_MAX_MPDU_LENGTH_11454                2
#define MESH_VHT_CAP_MAX_MPDU_LENGTH_MASK                ((u32) BIT(0) | BIT(1))
#define MESH_VHT_CAP_SUPP_CHAN_WIDTH_160MHZ              ((u32) BIT(2))
#define MESH_VHT_CAP_SUPP_CHAN_WIDTH_160_80PLUS80MHZ     ((u32) BIT(3))
#define MESH_VHT_CAP_SUPP_CHAN_WIDTH_MASK                ((u32) BIT(2) | BIT(3))
#define MESH_VHT_CAP_RXLDPC                              ((u32) BIT(4))
#define MESH_VHT_CAP_SHORT_GI_80                         ((u32) BIT(5))
#define MESH_VHT_CAP_SHORT_GI_160                        ((u32) BIT(6))
#define MESH_VHT_CAP_TXSTBC                              ((u32) BIT(7))
#define MESH_VHT_CAP_RXSTBC_1                            ((u32) BIT(8))
#define MESH_VHT_CAP_RXSTBC_2                            ((u32) BIT(9))
#define MESH_VHT_CAP_RXSTBC_3                            ((u32) BIT(8) | BIT(9))
#define MESH_VHT_CAP_RXSTBC_4                            ((u32) BIT(10))
#define MESH_VHT_CAP_RXSTBC_MASK                         ((u32) BIT(8) | BIT(9) | \
                               BIT(10))
#define MESH_VHT_CAP_SU_BEAMFORMER_CAPABLE               ((u32) BIT(11))
#define MESH_VHT_CAP_SU_BEAMFORMEE_CAPABLE               ((u32) BIT(12))
#define MESH_VHT_CAP_BEAMFORMEE_STS_MAX                  ((u32) BIT(13) | \
                               BIT(14) | BIT(15))
#define MESH_VHT_CAP_BEAMFORMEE_STS_OFFSET               13
#define MESH_VHT_CAP_SOUNDING_DIMENSION_MAX              ((u32) BIT(16) | \
                               BIT(17) | BIT(18))
#define MESH_VHT_CAP_SOUNDING_DIMENSION_OFFSET           16
#define MESH_VHT_CAP_MU_BEAMFORMER_CAPABLE               ((u32) BIT(19))
#define MESH_VHT_CAP_MU_BEAMFORMEE_CAPABLE               ((u32) BIT(20))
#define MESH_VHT_CAP_VHT_TXOP_PS                         ((u32) BIT(21))
#define MESH_VHT_CAP_HTC_VHT                             ((u32) BIT(22))

#define MESH_VHT_CAP_MAX_A_MPDU_LENGTH_EXPONENT_1        ((u32) BIT(23))
#define MESH_VHT_CAP_MAX_A_MPDU_LENGTH_EXPONENT_2        ((u32) BIT(24))
#define MESH_VHT_CAP_MAX_A_MPDU_LENGTH_EXPONENT_3        ((u32) BIT(23) | BIT(24))
#define MESH_VHT_CAP_MAX_A_MPDU_LENGTH_EXPONENT_4        ((u32) BIT(25))
#define MESH_VHT_CAP_MAX_A_MPDU_LENGTH_EXPONENT_5        ((u32) BIT(23) | BIT(25))
#define MESH_VHT_CAP_MAX_A_MPDU_LENGTH_EXPONENT_6        ((u32) BIT(24) | BIT(25))
#define MESH_VHT_CAP_MAX_A_MPDU_LENGTH_EXPONENT_MAX      ((u32) BIT(23) | \
                               BIT(24) | BIT(25))
#define MESH_VHT_CAP_VHT_LINK_ADAPTATION_VHT_UNSOL_MFB   ((u32) BIT(27))
#define MESH_VHT_CAP_VHT_LINK_ADAPTATION_VHT_MRQ_MFB     ((u32) BIT(26) | BIT(27))
#define MESH_VHT_CAP_RX_ANTENNA_PATTERN                  ((u32) BIT(28))
#define MESH_VHT_CAP_TX_ANTENNA_PATTERN                  ((u32) BIT(29))

#define VHT_OPMODE_CHANNEL_WIDTH_MASK           ((u8) BIT(0) | BIT(1))
#define VHT_OPMODE_CHANNEL_RxNSS_MASK           ((u8) BIT(4) | BIT(5) | \
                             BIT(6))
#define VHT_OPMODE_NOTIF_RX_NSS_SHIFT           4

#define VHT_RX_NSS_MAX_STREAMS              8

/* VHT channel widths */
#define MESH_VHT_CHANWIDTH_USE_HT    0
#define MESH_VHT_CHANWIDTH_80MHZ 1
#define MESH_VHT_CHANWIDTH_160MHZ    2
#define MESH_VHT_CHANWIDTH_80P80MHZ  3


#define _MESH_STATE_NOT_STARTED                    0
#define _MESH_STATE_DYNAMIC_CHANNEL_SCAN           1
#define _MESH_STATE_SEEKING_FINAL_ASSOCIATION      2
#define _MESH_STATE_RUNNING                        3
#define _MESH_STATE_SEEKING_NEW_PARENT             4
#define _MESH_STATE_INITIAL_WDS_SCAN               5
#define _MESH_STATE_SEEKING_INITIAL_ASSOCIATION    6
#define _MESH_STATE_STOPPPING                      7
#define _MESH_STATE_INIT_DS                        8
#define _MESH_STATE_INIT_WM                        9

#define _MESH_AP_ROOT                              1
#define _MESH_AP_RELAY                             2

#define _CHANNEL_SCAN_STATE_START                  1
#define _CHANNEL_SCAN_STATE_WAIT_OBJECTION         2
#define _CHANNEL_SCAN_STATE_WAIT_UNLOCK            3
#define _CHANNEL_SCAN_STATE_WAIT_RANDOM            4
#define _CHANNEL_SCAN_STATE_GO                     5
#define _CHANNEL_SCAN_STATE_SCANNING               6

#define _KNOWN_AP_ENABLE                           0
#define _KNOWN_AP_DISABLE                          1

#define _DFS_REQUIRED                              (mesh_config->reg_domain & 0x0100)
#define _FIPS_ENABLED                              (mesh_config->reg_domain & 0x0200)

#define _IGMP_SUPPORT                              (mesh_config->hope_cost & 0x01)
#define _DISJOINT_ADHOC_MODE                       (mesh_config->hope_cost & 0x02)
#define _FORCED_ROOT_MODE                          (mesh_config->hope_cost & 0x04)
#define _DISJOINT_ADHOC_MODE_INFRA_BEGIN           (mesh_config->hope_cost & 0x08)              /** Experimental flags to be removed after succesful deployment */
#define _DHCP_SUPPORT                              (mesh_config->hope_cost & 0x10)
#define _PROBE_REQUEST_LOCATION_SUPPORT            (mesh_config->hope_cost & 0x20)
#define _DISJOINT_ADHOC_MODE_SECTORED              (mesh_config->hope_cost & 0x40)              /** Experimental flags to be removed after succesful deployment */
#define _SIP_SUPPORT                               (mesh_config->hope_cost & 0x80)

void set_net_if_rate_ctrl_param(AL_CONTEXT_PARAM_DECL al_net_if_t *net_if, mesh_conf_if_info_t *if_info);

#endif /* __MESH_H__ */
