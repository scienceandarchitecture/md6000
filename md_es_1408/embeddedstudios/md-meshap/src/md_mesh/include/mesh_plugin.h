/********************************************************************************
* MeshDynamics
* --------------
* File     : mesh_plugin.h
* Comments : External plugin interface to Mesh
* Created  : 5/14/2008
* Author   : Abhijit Ayarekar
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  0  |5/14/2008 | Created.                                        | Abhijit|
* -----------------------------------------------------------------------------
********************************************************************************/

#ifndef __MESH_PLUGIN_H__
#define __MESH_PLUGIN_H__

#define MESH_PLUGIN_EVENT_PARENT_SHIFTED        0x00000001
#define MESH_PLUGIN_EVENT_HEARTBEAT_RECEIVED    0x00000002
#define MESH_PLUGIN_EVENT_STA_ASSOCIATED        0x00000004

struct mesh_plugin_heartbeat_info
{
   al_net_addr_t ds_mac;                                                /* Senders DS Mac Address */
   unsigned char children_count;                                        /* Children count */
   al_net_addr_t *immediate_sta;                                        /* IMMEDIATE CHILDREN */
};

typedef struct mesh_plugin_heartbeat_info   mesh_plugin_heartbeat_info_t;

typedef int (*mesh_plugin_event_handler_t)      (unsigned int event_id, void *event_param);

int add_mesh_event_handler(unsigned int event_id_map, mesh_plugin_event_handler_t handler);
int remove_mesh_event_handler(unsigned int event_id_map, mesh_plugin_event_handler_t handler);

#endif /*__MESH_PLUGIN_H__*/
