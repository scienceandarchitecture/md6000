/********************************************************************************
* MeshDynamics
* --------------
* File     : mesh_monitor.h
* Comments : Mesh Scan Monitor header file
* Created  : 10/7/2004
* Author   : Anand Bhange
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  3  |9/26/2005 | MESH_INIT_STATUS formatting changed             | Sriram |
* -----------------------------------------------------------------------------
* |  2  |6/12/2005 | Parameter value changed                         | Sriram |
* -----------------------------------------------------------------------------
* |  1  |4/17/2005 | Added mode parameter                            | Sriram |
* -----------------------------------------------------------------------------
* |  0  |10/7/2004 | Created                                         | Anand  |
* -----------------------------------------------------------------------------
********************************************************************************/


#ifndef __MESH_MONITOR_H__
#define __MESH_MONITOR_H__

#define MESH_INIT_STATUS_SSID                     "MESH-INIT-%s-%02X-%02X-%02X"

#define MESH_MONITOR_SCAN_MODE_DS_NET_IF          1
#define MESH_MONITOR_SCAN_MODE_MONITOR_NET_IF     2

#ifndef MESH_MONITOR_SCAN_MAX_SCANS_FOR_REBOOT
#define MESH_MONITOR_SCAN_MAX_SCANS_FOR_REBOOT    200
#endif

int mesh_monitor_scan_all_imcp_aps(AL_CONTEXT_PARAM_DECL int mode);

void start_scan_monitor(AL_CONTEXT_PARAM_DECL_SINGLE);

#endif /*__MESH_MONITOR_H__*/
