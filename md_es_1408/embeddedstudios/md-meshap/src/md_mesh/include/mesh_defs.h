/********************************************************************************
* MeshDynamics
* --------------
* File     : mesh_defs.h
* Comments : Mesh global structure defination file
* Created  : 4/22/2004
* Author   : Anand Bhange
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  39 |02/12/2009| Added 'use_virt_if'                             | Sriram |
* -----------------------------------------------------------------------------
* |  38 |8/27/2008 | Changes for options                             | Sriram |
* -----------------------------------------------------------------------------
* |  37 |8/22/2008 | Changes for SIP                                 |Abhijit |
* -----------------------------------------------------------------------------
* |  36 |7/11/2007 | bridge_ageing_time changed to unsigned int      | Sriram |
* -----------------------------------------------------------------------------
* |  35 |6/20/2007 | hope_cost re-used for IGMP support              | Sriram |
* -----------------------------------------------------------------------------
* |  34 |6/13/2007 | pattern_match_min Added                         | Sriram |
* -----------------------------------------------------------------------------
* |  33 |6/5/2007  | stay_awake_count changed to unsigned int        | Sriram |
* -----------------------------------------------------------------------------
* |  32 |2/26/2007 | Changes for EFFISTREAM                          | Sriram |
* -----------------------------------------------------------------------------
* |  31 |2/23/2007 | Added TX antenna field to IF info               | Sriram |
* -----------------------------------------------------------------------------
* |  30 |2/9/2007  | Changes for In-direct VLAN                      | Sriram |
* -----------------------------------------------------------------------------
* |  29 |01/01/2007| IF-Specific TXRate and TXPower changes          |Prachiti|
* -----------------------------------------------------------------------------
* |  28 |01/01/2007| Added Private Channel implementation            |Prachiti|
* -----------------------------------------------------------------------------
* |  27 |10/31/2006| Changes for private frequencies                 | Sriram |
* -----------------------------------------------------------------------------
* |  26 |10/18/2006| ETSI Radar changes                              | Sriram |
* -----------------------------------------------------------------------------
* |  25 |3/13/2006 | ACL list implemented                            | Sriram |
* -----------------------------------------------------------------------------
* |  24 |02/15/2006| dot11e changes                                  | Bindu  |
* -----------------------------------------------------------------------------
* |  23 |02/08/2006| Config SQNR and Hide SSID added                 | Sriram |
* -----------------------------------------------------------------------------
* |  22 |02/03/2006| flags added						              |Abhijit |
* -----------------------------------------------------------------------------
* |  21 |12/14/2005| acktimeout and regdomain changes                |prachiti|
* -----------------------------------------------------------------------------
* |  20 |10/5/2005 | Oscillation related changes                     | Sriram |
* -----------------------------------------------------------------------------
* |  19 |10/4/2005 | Changes for IMCP WM Info                        | Sriram |
* -----------------------------------------------------------------------------
* |  18 |5/20/2005 | FCC/ETSI + Other changes                        | Sriram |
* -----------------------------------------------------------------------------
* |  17 |5/6/2005  | Model info added                                | Sriram |
* -----------------------------------------------------------------------------
* |  16 |4/9/2005  | Added DS channel to Known AP info               | Sriram |
* -----------------------------------------------------------------------------
* |  15 |4/9/2005  | Security info added to mesh_config              | Sriram |
* -----------------------------------------------------------------------------
* |  14 |04/07/2005| VLan Info added to mesh_config                  | Abhijit|
* -----------------------------------------------------------------------------
* |  13 |1/8/2005  | Changes for per-interface essid/etc             | Sriram |
* -----------------------------------------------------------------------------
* |  12 |12/1/2004 | Misc changes                                    | Sriram |
* -----------------------------------------------------------------------------
* |  11 |11/10/2004| preamble_type,slot_time_type added              | Abhijit|
* -----------------------------------------------------------------------------
* |  10 |10/11/2004| Additions for IMCP 5.0                          | Abhijit|
* -----------------------------------------------------------------------------
* |  9  |7/22/2004 | dca_channels & count added to mesh_config       | Anand  |
* -----------------------------------------------------------------------------
* |  8  |7/19/2004 | disabled is no HB, (no deletion of known ap     | Anand  |
* -----------------------------------------------------------------------------
* |  7  |7/5/2004 |  preferred_parent & name added to config         | Anand  |
* -----------------------------------------------------------------------------
* |  6  |6/17/2004 | Changed _CMP_MAC_ADDR                           | Sriram |
* -----------------------------------------------------------------------------
* |  5  |5/28/2004 | ap_is_child added to avoid inconsistant state   | Anand  |
* -----------------------------------------------------------------------------
* |  4  |5/28/2004 | mesh_health_index_info struct added for mhi     | Anand  |
* -----------------------------------------------------------------------------
* |  3  |5/11/2004 | ds_mac assed to known_aps structure             | Anand  |
* -----------------------------------------------------------------------------
* |  2  |4/27/2004 | HBI added in known_access_point_info            | Anand  |
* -----------------------------------------------------------------------------
* |  1  |4/26/2004 | Testing Base Line                               | Anand  |
* -----------------------------------------------------------------------------
* |  0  |4/22/2004 | Created                                         | Anand  |
* -----------------------------------------------------------------------------
********************************************************************************/


#ifndef __MESH_DEFS_H__
#define __MESH_DEFS_H__

#define MAC_ADDR_SIZE    6

#define MAC2STR(a)      (a)[0], (a)[1], (a)[2], (a)[3], (a)[4], (a)[5]
#define MACSTR    "%02x:%02x:%02x:%02x:%02x:%02x"

#define _CMP_MAC_ADDR(addr1, addr2)    AL_NET_ADDR_EQUAL(addr1, addr2)

#define MESH_FLAG_CT_MODE            0x00000001
#define MESH_FLAG_REBOOT_REQUIRED    0x00000002
#define MESH_FLAG_DENY_CLIENTS       0x00000004
#define MESH_FLAG_RADAR_FOUND        0x00000008
#define MESH_FLAG_FIPS_COMPLIANT     0x00000010

#define CHECK_MESH_FLAG(value)    (mesh_config->flags & value)
#define SET_MESH_FLAG(value)      do { mesh_config->flags |= (value); } while (0)
#define RESET_MESH_FLAG(value)    do { mesh_config->flags &= (~value); } while (0)

struct mesh_conf_if_info
{
   unsigned char             name_length;
   char                      name[32];
   unsigned char             phy_type;
   unsigned char             phy_sub_type;
   unsigned char             use_type;
   unsigned char             config_channel;
   unsigned char             bonding;
   char                      essid[AL_802_11_MAX_ESSID_LENGTH + 1];
   int                       rts_th;
   int                       frag_th;
   int                       beacon_int;
   unsigned char             dy_channel_alloc;
   unsigned char             txpower;
   unsigned char             dca_list_count;
   unsigned char             dca_list[256];
   unsigned char             service;
   int                       txrate;
   int                       txant;
   unsigned char             preamble_type;
   unsigned char             slot_time_type;
   al_802_11_security_info_t security_info;

#if MDE_80211N_SUPPORT 
   al_802_11_ht_capab_t      ht_capab;
   al_802_11_fram_agre_t     frame_aggregation;
#endif

#if MDE_80211AC_SUPPORT 
   al_802_11_vht_capab_t     vht_capab;
#endif

   al_ip_addr_t              dot1x_backend_addr;
   unsigned short            dot1x_backend_port;
   char                      dot1x_backend_secret[64];
   int                       group_key_renewal_period;
   unsigned char             radius_based_vlan;
   /* used by mesh */
   al_net_if_t               *net_if;                                   /* WMS Net IF */
   int                       operating_channel;
   int                       new_operating_channel;
   unsigned char             ack_timeout;
   unsigned char             hide_ssid;
   unsigned char             dot11e_enabled;
   unsigned char             dot11e_category;
   unsigned char             radar_found;
   unsigned char             priv_channel_bandwidth;
   unsigned char             priv_channel_max_power;
   unsigned char             priv_channel_count;
   unsigned char             priv_channel_ant_max;
   unsigned char             priv_channel_ctl;
   unsigned char             priv_channel_modulation;
   unsigned short            priv_frequencies[32];
   unsigned char             priv_channel_numbers[32];
   unsigned char             priv_channel_flags[32];
   void                      *location_info;
   /* Rate control algorithm parameters begin */
   int                       t_el_max;
   int                       t_el_min;
   int                       qualification;
   int                       max_retry_percent;
   int                       max_error_percent;
   int                       max_errors;
   int                       min_retry_percent;
   int                       min_qualification;
   int                       initial_rate;                                                      /* index */
   int                       initial_rate_mbps;
   /* Rate control algorithm parameters end */
};

typedef struct mesh_conf_if_info   mesh_conf_if_info_t;

/**
 * Format for the priv_channel_flags field to be used by F/W 2.5.30 onwards
 *
 * 07   06  05  04  03  02  01  00
 * ---+---+---+---+---+---+---+---+
 *|			RESERVED		  |DFS|
 * ---+---+---+---+---+---+---+---+
 */


#define MONITOR_TYPE_ABSENT        0
#define MONITOR_TYPE_PMON          1
#define MONITOR_TYPE_AMON          2
#define MONITOR_TYPE_AUTO          3

#define MESH_FAILOVER_PHY_CONN     1
#define MESH_FAILOVER_PING_CONN    2

struct mesh_hardware_config
{
   al_net_addr_t       *ds_mac;                                                 /* Mac addr of DS of the AP */
   unsigned char       ds_if_type;                                              /* ds phy_type from config */
   unsigned char       ds_if_sub_type;                                          /* ds phy_sub_type from config */
   unsigned char       ds_if_channel;                                           /* ds op. channel from config */
   unsigned char       ds_if_ack_timeout;                                       /* ds ack timeout from config */
   int                 ds_if_tx_rate;                                           /* ds tx rate from config */
   unsigned char       ds_if_tx_power;                                          /* ds tx power from config */
   al_net_if_t         *ds_net_if;                                              /* DS Net IF */
   mesh_conf_if_info_t *ds_conf;                                                /* DS Net IF configuration */
   unsigned char       monitor_type;                                            /* Monitor type if present */
   al_net_if_t         *monitor_net_if;                                         /* Monitor net if */
   mesh_conf_if_info_t *monitor_conf;                                           /* Monitor Net IF configuration */
   unsigned int        wm_net_if_count;                                         /* WMS Net if count */
   mesh_conf_if_info_t **wms_conf;                                              /* WMS Net if configuration */
};

typedef struct mesh_hardware_config   mesh_hardware_config_t;

/**
 * F/W 2.5.26 onwards :
 * Regulatory domain field is 16 bits and is formatted
 * as follows :
 *
 *    15   14   13   12   11   10   9    8  | 7    6    5    4    3    2     1   0
 *  +----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+
 *  |            RESERVED              |DFS |           REG DOMAIN CODE             |
 *  +----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+
 *                                          |
 */

struct mesh_config_info
{
   unsigned char                 meshid_length;
   char                          meshid[256];
   char                          hardware_model[256];
   unsigned char                 mesh_imcp_key_length;
   unsigned char                 mesh_imcp_key[256];
   al_net_addr_t                 preferred_parent;                              /* Preferred parent address for wireless ds */
   unsigned char                 sig_map[8];                                    /* curve points */
   unsigned char                 hbeat_interval;                                /* heart beat interval */
   unsigned char                 hbeat_miss_count;                              /* heart beat miss count */
   unsigned char                 hope_cost;                                     /* Re-used as flags for IGMP support/etc */
   unsigned char                 max_aw_hopes;                                  /* max allowable hops */
   unsigned char                 las_interval;                                  /* location awareness scan interval */
   unsigned char                 ch_res_th;                                     /* change resistance tradeoff */
   /* New Generation configuration parameters Begin*/
   unsigned char                 mobile_mode;                                   /* Set to 1 for mobile nodes */
   unsigned short                round_robin_beacon_count;
   unsigned short                scanner_dwell_interval;                        /* Total dwell time in milli-seconds */
   unsigned short                scan_count;                                    /* Number of times to scan before making decision */
   unsigned int                  sampling_t_next_min;                           /* Milli seconds */
   unsigned int                  sampling_t_next_max;                           /* Milli seconds */
   unsigned short                sampling_n_p;                                  /* Count of packets */
   unsigned char                 pattern_match_min;
   unsigned char                 disconnect_count_max;
   unsigned char                 disconnect_multiplier;
   unsigned int                  evaluation_damping_factor;
   /* New Generation configuration parameters End*/
   char                          name[129];                             /* Node name */
   unsigned char                 essid_length;                          /* essid length */
   unsigned char                 essid[AL_802_11_MAX_ESSID_LENGTH + 1]; /* essid */
   int                           rts_th;                                /* rts thri*/
   int                           frag_th;                               /* frag thri*/
   int                           beacon_int;                            /* beacon interval */
   unsigned char                 dy_channel_alloc;                      /* dynamic channel allocation on/off */
   unsigned int                  stay_awake_count;
   unsigned int                  bridge_ageing_time;
   unsigned char                 fcc_certified_operation;
   unsigned char                 etsi_certified_operation;
   int                           failover_enabled;
   unsigned char                 server_ip_addr[128];
   unsigned char                 mgmt_gw_addr[64];
   int                           mgmt_gw_enable;
   unsigned char                 mgmt_gw_certificates[64];

   unsigned int                  disable_backhaul_security;
   int                           power_on_default;
   unsigned char                 *server_addr;
   unsigned char                 ds_tx_power;
   int                           ds_tx_rate;
   unsigned short                reg_domain;            /*regulatory domain*/
   unsigned short                country_code;          /*country code*/
   unsigned int                  config_sqnr;           /* configuration sequence number */
   int                           if_count;              /* network interface count */
   mesh_conf_if_info_t           *if_info;              /* network interface info */

   int                           vlan_count;            /* vlan count */
   access_point_vlan_info_t      *vlan_info;            /* vlan info */

   al_conf_effistream_criteria_t *criteria_root;        /* Effistream criteria root */

   al_conf_dhcp_info_t           dhcp_info;
   al_conf_logmon_info_t         logmon_info;
   sip_conf_info_t               sip_info;
   al_conf_option_t              *options;

   unsigned char                 ctmode_backup_hbi;                             /* saved heart beat interval in ct mode */
   unsigned int                  flags;
   unsigned short                dhcp_r_value;

   unsigned char                 use_virt_if;
};

typedef struct mesh_config_info   mesh_config_info_t;

/** 802.11e Categories
 */
struct mesh_dot11e_category
{
   al_dot11e_category_info_t config_info;
};

typedef struct mesh_dot11e_category   mesh_dot11e_category_t;

struct mesh_acl_config
{
   int                      count;
   access_point_acl_entry_t *entries;
};

typedef struct mesh_acl_config   mesh_acl_config_t;


#endif /*__MESH_DEFS_H__*/
