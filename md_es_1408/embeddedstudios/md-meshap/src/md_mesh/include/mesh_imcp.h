/********************************************************************************
* MeshDynamics
* --------------
* File     : mesh_imcp.h
* Comments : Mesh IMCP processing functions
* Created  : 4/20/2004
* Author   : Anand Bhange
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* | 26  |1/27/2009 | Changes for Uplink Scan Functionality           | Sriram |
* -----------------------------------------------------------------------------
* | 25  |8/22/2008 | Changes for SIP                                 |Abhijit |
* -----------------------------------------------------------------------------
* | 24  |6/5/2007  | mesh_imcp_send_packet_ds_excerciser changed     | Sriram |
* -----------------------------------------------------------------------------
* | 23  |4/11/2007 | Changes for Radio diagnostics                   | Sriram |
* -----------------------------------------------------------------------------
* | 22  |3/13/2006 | ACL list implemented                            | Sriram |
* -----------------------------------------------------------------------------
* | 21  |02/22/2006| dot11e category added                           | Bindu  |
* -----------------------------------------------------------------------------
* | 20  |02/20/2006| Config SQNR removed	                          | Abhijit|
* -----------------------------------------------------------------------------
* | 19  |02/08/2006| Config SQNR added                               | Sriram |
* -----------------------------------------------------------------------------
* | 18  |02/03/2006| mesh_imcp_process_packet_generic_command_req_res| Abhijit|
* -----------------------------------------------------------------------------
* | 17  |02/01/2006| Added mesh_imcp_process_packet_sta_info_request | Abhijit|
* -----------------------------------------------------------------------------
* | 16  |01/01/2006| Added mesh_imcp_send_packet_heartbeat2		  | Abhijit|
* -----------------------------------------------------------------------------
* | 15  |01/01/2006| Changes for IMCP Analyser int packet            | Abhijit|
* -----------------------------------------------------------------------------
* | 14  |12/16/2005|supported channels changes					      |prachiti|
* -----------------------------------------------------------------------------
* | 13  |12/14/2005| acktimeout and regdomain changes                |prachiti|
* -----------------------------------------------------------------------------
* | 12  |10/4/2005 | Changes for IMCP WM Info                        | Sriram |
* -----------------------------------------------------------------------------
* | 11  |9/19/2005 | Added mesh_imcp_send_packet_ds_excerciser       | Sriram |
* -----------------------------------------------------------------------------
* | 10  |4/12/2005 | Channel change packet added                     | Sriram |
* -----------------------------------------------------------------------------
* |  9  |10/25/2004| process_packet_mesh_imcp_key_update added       | Abhijit|
* -----------------------------------------------------------------------------
* |  8  |8/26/2004 | Software Update Mesh Init Packets added         | Anand  |
* -----------------------------------------------------------------------------
* |  7  |7/21/2004 | mesh_imcp_send_packet_ip_ap_start added         | Anand  |
* -----------------------------------------------------------------------------
* |  6  |7/19/2004 | process_packet_ap_config_update added           | Anand  |
* -----------------------------------------------------------------------------
* |  5  |6/11/2004 | hardware_info_request func added                | Anand  |
* -----------------------------------------------------------------------------
* |  4  |5/11/2004 | al_net_if param added to send handshake response| Anand  |
* -----------------------------------------------------------------------------
* |  3  |4/27/2004 | Ch removed fromHeartBeat/HS Response imcp Pckt  | Anand  |
* -----------------------------------------------------------------------------
* |  2  |4/26/2004 | Testing Base Line                               | Anand  |
* -----------------------------------------------------------------------------
* |  1  |4/21/2004 | **Made (major) changes for AL_CONTEXT**         | Anand  |
* -----------------------------------------------------------------------------
* |  0  |4/20/2004 | Created                                         | Anand  |
* -----------------------------------------------------------------------------
********************************************************************************/


#ifndef __MESH_IMCP_H__
#define __MESH_IMCP_H__


int mesh_imcp_process_packet_heartbeat(AL_CONTEXT_PARAM_DECL imcp_packet_info_t *pi, int rssi);
int mesh_imcp_process_packet_sta_assoc_notification(AL_CONTEXT_PARAM_DECL imcp_packet_info_t *pi);
int mesh_imcp_process_packet_sta_disassoc_notification(AL_CONTEXT_PARAM_DECL imcp_packet_info_t *pi);
int mesh_imcp_process_packet_channel_scan_lock(AL_CONTEXT_PARAM_DECL imcp_packet_info_t *pi);
int mesh_imcp_process_packet_channel_scan_unlock(AL_CONTEXT_PARAM_DECL imcp_packet_info_t *pi);
int mesh_imcp_process_packet_channel_scan_lock_objection(AL_CONTEXT_PARAM_DECL imcp_packet_info_t *pi);
int mesh_imcp_process_packet_hardware_info(AL_CONTEXT_PARAM_DECL imcp_packet_info_t *pi);
int mesh_imcp_process_packet_hardware_info_request(AL_CONTEXT_PARAM_DECL imcp_packet_info_t *pi);
int mesh_imcp_process_packet_ap_sub_tree_info(AL_CONTEXT_PARAM_DECL imcp_packet_info_t *pi);
int mesh_imcp_process_packet_ap_config_update(AL_CONTEXT_PARAM_DECL imcp_packet_info_t *pi);
int mesh_imcp_process_packet_config_req_res(AL_CONTEXT_PARAM_DECL imcp_packet_info_t *pi);
int mesh_imcp_process_packet_mesh_imcp_key_update(AL_CONTEXT_PARAM_DECL imcp_packet_info_t *pi);
int mesh_imcp_process_packet_wm_info_request(AL_CONTEXT_PARAM_DECL imcp_packet_info_t *pi);
int mesh_imcp_process_packet_wm_info_response(AL_CONTEXT_PARAM_DECL imcp_packet_info_t *pi);
int mesh_imcp_process_packet_tester_testee_mode_request(AL_CONTEXT_PARAM_DECL imcp_packet_info_t *pi);
int mesh_imcp_process_packet_dot11e_config_req_res(AL_CONTEXT_PARAM_DECL imcp_packet_info_t *pi);

int mesh_imcp_send_packet_heartbeat(AL_CONTEXT_PARAM_DECL int sqnr);
int mesh_imcp_send_packet_sta_assoc_notification(AL_CONTEXT_PARAM_DECL al_net_addr_t *sta_addr, al_net_addr_t *ap_wm_addr);
int mesh_imcp_send_packet_sta_disassoc_notification(AL_CONTEXT_PARAM_DECL al_net_addr_t *sta_addr, int reason);

int mesh_imcp_send_packet_channel_scan_lock(AL_CONTEXT_PARAM_DECL_SINGLE);
int mesh_imcp_send_packet_channel_scan_unlock(AL_CONTEXT_PARAM_DECL_SINGLE);
int mesh_imcp_send_packet_channel_scan_lock_objection(AL_CONTEXT_PARAM_DECL_SINGLE);
int mesh_imcp_send_packet_reset(AL_CONTEXT_PARAM_DECL_SINGLE);
int mesh_imcp_send_packet_hardware_info(AL_CONTEXT_PARAM_DECL_SINGLE);
int mesh_imcp_send_packet_ap_sub_tree_info(AL_CONTEXT_PARAM_DECL_SINGLE);
int mesh_imcp_send_packet_mesh_init_status(AL_CONTEXT_PARAM_DECL char *status_message);
int mesh_imcp_send_packet_wm_info_request(AL_CONTEXT_PARAM_DECL al_net_if_t *net_if, al_net_addr_t *ds_mac_dest, al_net_addr_t *ds_mac_src, al_net_addr_t *wm_mac_dest, unsigned char ds_type, unsigned char ds_sub_type);
int mesh_imcp_send_radio_diag_data(AL_CONTEXT_PARAM_DECL al_net_if_t *net_if, unsigned char *data, unsigned short data_length);
int mesh_imcp_send_packet_ds_excerciser(AL_CONTEXT_PARAM_DECL al_net_if_t *net_if, al_net_addr_t *dest_mac);

struct mesh_imcp_wm_info_list
{
   al_net_if_t                   *net_if;
   al_net_addr_t                 wm_addr;
   unsigned char                 wm_channel;
   char                          wm_essid[AL_802_11_MAX_ESSID_LENGTH + 1];
   struct mesh_imcp_wm_info_list *next;
};

typedef struct mesh_imcp_wm_info_list   mesh_imcp_wm_info_list_t;

int mesh_imcp_send_packet_wm_info_response(AL_CONTEXT_PARAM_DECL al_net_if_t *net_if,
                                           al_net_addr_t                     *ds_mac_dest,
                                           al_net_addr_t                     *ds_mac_src,
                                           int                               wm_count,
                                           unsigned char                     wm_type,
                                           unsigned char                     wm_sub_type,
                                           mesh_imcp_wm_info_list_t          *wm_list);

int mesh_imcp_send_packet_heartbeat2(AL_CONTEXT_PARAM_DECL int sqnr);

int mesh_imcp_process_packet_reg_domain_cc_config_update(AL_CONTEXT_PARAM_DECL imcp_packet_info_t *pi);
int mesh_imcp_process_packet_ack_timeout_config_update(AL_CONTEXT_PARAM_DECL imcp_packet_info_t *pi);
int mesh_imcp_process_packet_supported_channels_request(AL_CONTEXT_PARAM_DECL imcp_packet_info_t *pi);
int mesh_imcp_send_packet_supported_channels_info(AL_CONTEXT_PARAM_DECL imcp_packet_info_t *pi_req);
int mesh_imcp_process_packet_sta_info_request(AL_CONTEXT_PARAM_DECL imcp_packet_info_t *pi);
int mesh_imcp_process_packet_generic_command_req_res(AL_CONTEXT_PARAM_DECL imcp_packet_info_t *pi);
int mesh_imcp_process_packet_hide_ssid_config_update(AL_CONTEXT_PARAM_DECL imcp_packet_info_t *pi);
int mesh_imcp_process_packet_dot11e_category_update(AL_CONTEXT_PARAM_DECL imcp_packet_info_t *pi);
int mesh_imcp_process_packet_acl_info(AL_CONTEXT_PARAM_DECL imcp_packet_info_t *pi);
int mesh_imcp_get_vendor_info_buffer(AL_CONTEXT_PARAM_DECL unsigned char *vendor_info_out, int encryption);

int mesh_imcp_process_packet_buffering_command(AL_CONTEXT_PARAM_DECL imcp_packet_info_t *pi);
int mesh_imcp_send_packet_buffering_command(AL_CONTEXT_PARAM_DECL al_net_if_t *net_if, unsigned char type, unsigned int timeout);

int mesh_imcp_process_packet_buffering_command_ack(AL_CONTEXT_PARAM_DECL imcp_packet_info_t *pi);
int mesh_imcp_send_packet_buffering_command_ack(AL_CONTEXT_PARAM_DECL al_net_if_t *net_if, al_net_addr_t *ds_mac_dest);

int mesh_imcp_send_packet_downlink_saturation_info(AL_CONTEXT_PARAM_DECL al_net_if_t *net_if, int channel_count, al_duty_cycle_info_t *info);
int mesh_imcp_process_packet_reboot_required(AL_CONTEXT_PARAM_DECL imcp_packet_info_t *pi);
int mesh_imcp_process_packet_radio_diag_command(AL_CONTEXT_PARAM_DECL imcp_packet_info_t *pi);

int mesh_imcp_process_packet_node_move_notification(AL_CONTEXT_PARAM_DECL imcp_packet_info_t *pi);
int mesh_imcp_send_packet_node_move_notification(AL_CONTEXT_PARAM_DECL al_net_addr_t *node_ds_mac);

//SPAWAR_2
int mesh_imcp_process_packet_channel_change(AL_CONTEXT_PARAM_DECL imcp_packet_info_t *pi, al_net_addr_t *ap_addr);
int mesh_imcp_send_packet_channel_change(AL_CONTEXT_PARAM_DECL al_net_if_t *net_if, unsigned char new_channel, unsigned short millis_left);

void mesh_imcp_initialize(AL_CONTEXT_PARAM_DECL_SINGLE);
void mesh_imcp_uninitialize(AL_CONTEXT_PARAM_DECL_SINGLE);

int mesh_imcp_send_extern_packet(AL_CONTEXT_PARAM_DECL int imcp_packet_type, unsigned char *buffer, unsigned short buffer_length);

typedef int (*meshap_process_imcp_sip_packet_t)(al_net_addr_t *sender_mac, unsigned char *packet_data, unsigned short data_length);

int mesh_imcp_set_sip_private_packet_handler(AL_CONTEXT_PARAM_DECL meshap_process_imcp_sip_packet_t handler);
int mesh_imcp_process_packet_sip_private(AL_CONTEXT_PARAM_DECL imcp_packet_info_t *pi);

#endif /* __MESH_IMCP_H__ */
