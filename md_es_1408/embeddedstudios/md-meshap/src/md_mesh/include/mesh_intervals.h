/********************************************************************************
* MeshDynamics
* --------------
* File     : mesh_intervals.h
* Comments : Mesh timing interval definations
* Created  : 4/26/2004
* Author   : Anand Bhange
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  3  |5/1/2007  | RANDOM_TIME_MAX value changed                   | Sriram |
* -----------------------------------------------------------------------------
* |  2  |6/15/2005 | RANDOM_TIME_MAX added                           | Sriram |
* -----------------------------------------------------------------------------
* |  1  |4/26/2004 | Testing Base Line                               | Anand  |
* -----------------------------------------------------------------------------
* |  0  |4/26/2004 | Created.                                        | Anand  |
* -----------------------------------------------------------------------------
********************************************************************************/


#ifndef __MESH_INTERVALS_H__
#define __MESH_INTERVALS_H__

#define MINIMUM_DWELL_TIME                  (10 * HZ / 9)
#define SCAN_CYCLE_TO_MARK_DISABLE          2
#define SCAN_DURATION                       600
#define SCAN_LOCK_OBJECTION_TIMEOUT         5000
#define ASSOC_TIME_OUT                      5000
#define RESCAN_LOCK_INTERVAL                5000
#define RESCAN_LOCK_UNLOCK_WAIT_INTERVAL    10000
#define RANDOM_TIME_MAX                     10000


#endif /*__MESH_INTERVALS_H__*/
