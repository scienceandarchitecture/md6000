/********************************************************************************
* MeshDynamics
* --------------
* File     : mesh_ext.h
* Comments : External interface to Mesh
* Created  : 5/20/2005
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* | 11  |8/22/2008 | Changes for SIP                                 |Abhijit |
* -----------------------------------------------------------------------------
* | 10  |7/11/2007 | Added mesh_ext_full_disconnect command          | Sriram |
* -----------------------------------------------------------------------------
* |  9  |6/14/2007 | mesh_ext_is_mobile Added                        | Sriram |
* -----------------------------------------------------------------------------
* |  8  |6/13/2007 | Added mesh_ext_enumerate_mobility_window        | Sriram |
* -----------------------------------------------------------------------------
* |  7  |6/5/2007  | Added mesh_ext_send_wm_excerciser               | Sriram |
* -----------------------------------------------------------------------------
* |  6  |5/12/2006 | Added set_reboot_flag                           | Bindu  |
* -----------------------------------------------------------------------------
* |  5  |10/5/2005 | Oscillation related changes                     | Sriram |
* -----------------------------------------------------------------------------
* |  4  |10/4/2005 | Changes for IMCP WM Info                        | Sriram |
* -----------------------------------------------------------------------------
* |  3  |9/19/2005 | Added mesh_ext_send_ds_excerciser               | Sriram |
* -----------------------------------------------------------------------------
* |  2  |6/18/2005 | Added kap and table enumeration                 | Sriram |
* -----------------------------------------------------------------------------
* |  1  |6/17/2005 | Added Test flags setting						  | Sriram |
* -----------------------------------------------------------------------------
* |  0  |5/20/2005 | Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

#ifndef __MESH_EXT_H__
#define __MESH_EXT_H__

#define MESH_EXT_KAP_FLAG_PREFERRED     0x00000001
#define MESH_EXT_KAP_FLAG_MOBILE        0x00000002
#define MESH_EXT_KAP_FLAG_CHILD         0x00000004
#define MESH_EXT_KAP_FLAG_DISABLED      0x00000008
#define MESH_EXT_KAP_FLAG_QUESTION      0x00000010
#define MESH_EXT_KAP_FLAG_ADDED         0x00000020
#define MESH_EXT_KAP_FLAG_LIMITED       0x00000080
#define MESH_EXT_KAP_FLAG_AD_DISABLE    0x00000100      /* Node is disabled for ad-hoc LFR resolution purposes */

struct mesh_ext_known_access_point_info
{
   unsigned int   flags;
   al_net_addr_t  ds_mac;
   al_net_addr_t  bssid;
   al_net_addr_t  root_bssid;
   unsigned char  channel;
   unsigned int   crssi;                                                        /* cumulative signal strenth*/
   unsigned char  ds_channel;
   unsigned char  ds_type;
   char           essid[AL_802_11_ESSID_MAX_SIZE + 1];
   unsigned char  hbi;                                                          /* Heart Beat Interval */
   unsigned char  hpc;                                                          /* Hope count */
   unsigned char  signal;
   unsigned char  score;
   unsigned char  tree_bit_rate;
   unsigned char  direct_bit_rate;
   int            disconnect_count;
   al_u64_t       last_hb_time;
   unsigned int   sqnr;
   unsigned int   max_sample_packets;                                           /* Maximum number of sample packets to transmit */
   unsigned int   sample_packets;                                               /* Current count of number of packets transmitted */
   int            t_next;                                                       /* Next check time parameter */
   al_u64_t       next_sample_time;                                             /* Next sample timestamp */
   al_u64_t       last_seen_time;
   int            pattern_count;
   unsigned short dhcp_r_value;
};

typedef struct mesh_ext_known_access_point_info   mesh_ext_known_access_point_info_t;

struct mesh_ext_table_entry
{
   al_net_addr_t sta_addr;
   al_net_if_t   *net_if;
   al_net_addr_t relay_ap_addr;
   al_net_addr_t sta_ap_addr;
   al_u64_t      last_rx;
   al_u64_t      last_tx;
   int           level;
};

typedef struct mesh_ext_table_entry   mesh_ext_table_entry_t;

struct mesh_ext_mobilty_window
{
   unsigned int  window_item_id;
   al_net_addr_t ds_mac;
   al_net_addr_t parent_bssid;
   unsigned char channel;
   unsigned char signal;
   int           pattern_count;
   unsigned int  flags;
};
typedef struct mesh_ext_mobilty_window   mesh_ext_mobilty_window_t;

int mesh_ext_set_preferred_parent(unsigned char enable, unsigned char *mac_address);
int mesh_ext_set_test_flags(unsigned int flags);

typedef void (*mesh_ext_enum_kap_routine_t)                     (void *callback, const mesh_ext_known_access_point_info_t *kap);
typedef void (*mesh_ext_enum_table_routine_t)           (void *callback, const mesh_ext_table_entry_t *entry);
typedef void (*mesh_ext_enum_mobwin_routine_t)          (void *callback, const mesh_ext_mobilty_window_t *entry);

int mesh_ext_enumerate_known_aps(void *callback, mesh_ext_enum_kap_routine_t enum_routine);
int mesh_ext_enumerate_sampling_list(void *callback, mesh_ext_enum_kap_routine_t enum_routine);
int mesh_ext_enumerate_table(void *callback, mesh_ext_enum_table_routine_t enum_routine);
int mesh_ext_send_ds_excerciser(void);
int mesh_ext_send_wm_excerciser(al_net_if_t *net_if, al_net_addr_t *mac_address);
int mesh_ext_set_config_sqnr(unsigned int sqnr);
int mesh_ext_set_reboot_flag(unsigned int flag);
int mesh_ext_enumerate_mobility_window(void *callback, mesh_ext_enum_mobwin_routine_t enum_routine);
int mesh_ext_is_mobile(void);
int mesh_ext_full_disconnect(void);
int mesh_ext_get_address_info(unsigned char *mac_address, mesh_ext_table_entry_t *ext_entry);

#define MESH_EXT_FUNC_MODE_FFR     1
#define MESH_EXT_FUNC_MODE_FFN     2
#define MESH_EXT_FUNC_MODE_LFR     3
#define MESH_EXT_FUNC_MODE_LFN     4
#define MESH_EXT_FUNC_MODE_LFRS    5

int mesh_ext_get_func_mode(void);
int mesh_ext_get_step_scan_channel_index(void);
int mesh_ext_get_current_parent_info(mesh_ext_known_access_point_info_t *parent);

#endif /*__MESH_EXT_H__*/
