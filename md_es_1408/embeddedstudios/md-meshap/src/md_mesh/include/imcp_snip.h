
/********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : imcp_snip.h
 * Comments : 802.11 Infrastructure Mesh Control Protocol (v4) Definitions
 * Created  : 4/12/2004
 * Author   : Anand Bhange
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * -----------------------------------------------------------------------------
 * | No  |Date      |  Comment                                        | Author |
 * -----------------------------------------------------------------------------
 * | 52  |02/12/2009|Additional Changes for Uplink Scan Functionality |Abhijit |
 * -----------------------------------------------------------------------------
 * | 51  |1/27/2009 | Changes for Uplink Scan Functionality           | Sriram |
 * -----------------------------------------------------------------------------
 * | 50  |8/22/2008 | Changes for SIP                                 |Abhijit |
 * -----------------------------------------------------------------------------
 * | 49  |05/09/2008| Changes for Beaconing Uplink		              |Prachiti|
 * -----------------------------------------------------------------------------
 * | 48  |11/20/2007| Added GPS information to HB1                    | Sriram |
 * -----------------------------------------------------------------------------
 * | 47  |8/16/2007 | Added memory usage to CTC                       | Sriram |
 * -----------------------------------------------------------------------------
 * | 46  |6/5/2007  | DS Excerciser changed                           | Sriram |
 * -----------------------------------------------------------------------------
 * | 45  |4/11/2007 | Changes for Radio diagnostics                   | Sriram |
 * -----------------------------------------------------------------------------
 * | 44  |2/22/2007 | Added Effistream Req/Resp and Info packets      | Sriram |
 * -----------------------------------------------------------------------------
 * | 43  |01/01/2007| Added Private Channel implementation            |Prachiti|
 * -----------------------------------------------------------------------------
 * | 42  |11/20/2006| Added Voltage to CTC                            | Sriram |
 * -----------------------------------------------------------------------------
 * | 41  |10/18/2006| Changes for ETSI Radar                          | Sriram |
 * -----------------------------------------------------------------------------
 * | 40  |3/15/2006 | Added .11e enabled & category for ACL           | Bindu  |
 * -----------------------------------------------------------------------------
 * | 39  |3/13/2006 | ACL list implemented                            | Sriram |
 * -----------------------------------------------------------------------------
 * | 38  |02/22/2006| dot11e packet added                             | Bindu  |
 * -----------------------------------------------------------------------------
 * | 37  |02/20/2006| Config SQNR removed			                  | Abhijit|
 * -----------------------------------------------------------------------------
 * | 36  |02/08/2006| Config SQNR and Hide SSID added                 | Sriram |
 * -----------------------------------------------------------------------------
 * | 35  |02/03/2006| Generic command packet added					  | Abhijit|
 * -----------------------------------------------------------------------------
 * | 34  |02/01/2006| sta info request/response packets added		  | Abhijit|
 * -----------------------------------------------------------------------------
 * | 33  |01/01/2006| Heartbeat2 IMCP packet added					  | Abhijit|
 * -----------------------------------------------------------------------------
 * | 32  |01/01/2006| Analyser Init IMCP packet added                 | Abhijit|
 * -----------------------------------------------------------------------------
 * | 31  |12/16/2005|supported channels changes					      |prachiti|
 * -----------------------------------------------------------------------------
 * | 30  |11/25/2005|ACK Timeout IMCP packet added                    |Prachiti|
 * -----------------------------------------------------------------------------
 * | 29  |11/25/2005|Reg Domain IMCP packet added                     |Prachiti|
 * -----------------------------------------------------------------------------
 * | 28  |10/4/2005 | WM Info IMCP packet added                       | Sriram |
 * -----------------------------------------------------------------------------
 * | 27  |9/19/2005 | DS Excerciser IMCP packet added                 | Sriram |
 * -----------------------------------------------------------------------------
 * | 26  |5/20/2005 | FCC/ETSI + Other changes                        | Sriram |
 * -----------------------------------------------------------------------------
 * | 25  |5/17/2005 | Added CONFIGD_WATCHDOG packet type              | Sriram |
 * -----------------------------------------------------------------------------
 * | 24  |5/6/2005  | Model name added to H/W info                    | Sriram |
 * -----------------------------------------------------------------------------
 * | 23  |4/12/2005 | Addec Channel change notification               | Sriram |
 * -----------------------------------------------------------------------------
 * | 22  |4/9/2005  | Added DS channel to IMCP handshake              | Sriram |
 * -----------------------------------------------------------------------------
 * | 21  |03/09/2005| Board Temp added to heartbeat info			  | Anand  |
 * -----------------------------------------------------------------------------
 * | 20  |2/21/2005 | Vendor info added to beacons,HandShake Res to VI| Anand  |
 * -----------------------------------------------------------------------------
 * | 19  |2/14/2005 | VLanConfigInfo & ConfigReqRes packets added     | Anand  |
 * -----------------------------------------------------------------------------
 * | 18  |2/14/2005 | Changes for  HandShake Res to beacon            | Anand  |
 * -----------------------------------------------------------------------------
 * | 17  |1/8/2005  | Changes for per-interface essid/etc             | Sriram |
 * -----------------------------------------------------------------------------
 * | 16  |10/25/2004| added req_res field in imcp_key_update packet   | Abhijit|
 * -----------------------------------------------------------------------------
 * | 15  |10/6/2004 | Additions/Modifications for IMCP 5.0            | Abhijit|
 * -----------------------------------------------------------------------------
 * | 14  |8/26/2004 | Software Update Mesh Init Packets added         | Anand  |
 * -----------------------------------------------------------------------------
 * | 13  |7/29/2004 | changes in AP Config IMCP packet                | Anand  |
 * -----------------------------------------------------------------------------
 * | 12  |7/26/2004 | changes in config IMCP packet                   | Anand  |
 * -----------------------------------------------------------------------------
 * | 11  |7/25/2004 | Fixes for sub-tree information packet           | Sriram |
 * -----------------------------------------------------------------------------
 * | 10  |7/23/2004 | bit rate added to HB                            | Anand  |
 * -----------------------------------------------------------------------------
 * |  9  |7/21/2004 | IMCP_SNIP_PACKET_TYPE_IP_AP_START added         | Anand  |
 * -----------------------------------------------------------------------------
 * |  8  |7/19/2004 | AP_CONFIG_UPDATE IMCP Packet added              | Anand  |
 * -----------------------------------------------------------------------------
 * |  7  |6/11/2004 | hardware_info_request added                     | Anand  |
 * -----------------------------------------------------------------------------
 * |  6  |5/31/2004 | u name given to union for linux compilation     | Anand  |
 * -----------------------------------------------------------------------------
 * |  5  |5/11/2004 | src_ds_mac added to handshake_response          | Anand  |
 * -----------------------------------------------------------------------------
 * |  4  |4/27/2004 | Ch removed fromHeartBeat/HS Response imcp Pckt  | Anand  |
 * -----------------------------------------------------------------------------
 * |  3  |4/27/2004 | HBI added in HeartBeat/HS Response imcp Packet  | Anand  |
 * -----------------------------------------------------------------------------
 * |  2  |4/26/2004 | Testing Base Line                               | Anand  |
 * -----------------------------------------------------------------------------
 * |  1  |4/21/2004 | **Made (major) changes for AL_CONTEXT**         | Anand  |
 * -----------------------------------------------------------------------------
 * |  0  |4/12/2004 | Created.                                        | Anand  |
 * -----------------------------------------------------------------------------
 ********************************************************************************/

#ifndef __IMCP_SNIP_H__
#define __IMCP_SNIP_H__

#define MAC_ADDR_LENGTH				6		// for 802.11 its 6 bytes
#define MAX_ESSID_LENGTH			48


#define IMCP_EXT_MASK                                (unsigned short)(0x8000)
#define FIXED_ID_LEN                            4        /** includes id & length field size **/
#define IMCP_DEFAULT_FLAGS                      0
#define IMCP_LEN_MASK                           (unsigned short)(0x7fff)



/**
 * IMCP SNIP Packet Identifiers
 */

#define IMCP_SNIP_PACKET_TYPE_HEARTBEAT							(unsigned short)(1)
#define IMCP_SNIP_PACKET_TYPE_STA_ASSOC_NOTIFICATION			(unsigned short)(2)
#define IMCP_SNIP_PACKET_TYPE_STA_DISASSOC_NOTIFICATION			(unsigned short)(3)
#define IMCP_SNIP_PACKET_TYPE_BUFFER_COMMAND					(unsigned short)(4)
#define IMCP_SNIP_PACKET_TYPE_BUFFER_COMMAND_ACK				(unsigned short)(5)

/** 5 is reserved for re-use later */
#define IMCP_SNIP_PACKET_TYPE_CHANNEL_SCAN_LOCK					(unsigned short)(6)
#define IMCP_SNIP_PACKET_TYPE_CHANNEL_SCAN_UNLOCK				(unsigned short)(7)
#define IMCP_SNIP_PACKET_TYPE_CHANNEL_SCAN_LOCK_OBJECTION		(unsigned short)(8)
#define IMCP_SNIP_PACKET_TYPE_RESET								(unsigned short)(9)
#define IMCP_SNIP_PACKET_TYPE_AP_HARDWARE_INFO					(unsigned short)(10)
#define IMCP_SNIP_PACKET_TYPE_CONFIGURATION_INFO				(unsigned short)(11)
#define IMCP_SNIP_PACKET_TYPE_AP_CONFIGURATION_REQ_RES			(unsigned short)(12)
#define IMCP_SNIP_PACKET_TYPE_AP_HARDWARE_INFO_REQUEST			(unsigned short)(13)
#define	IMCP_SNIP_PACKET_TYPE_AP_SUB_TREE_INFO					(unsigned short)(14)
#define	IMCP_SNIP_PACKET_TYPE_IP_CONFIG_INFO					(unsigned short)(15)
#define	IMCP_SNIP_PACKET_TYPE_IP_CONFIG_INFO_REQ_RES			(unsigned short)(16)
/** 17 is reserved for re-use later */
#define	IMCP_SNIP_PACKET_TYPE_SW_UPDATE_REQUEST					(unsigned short)(18)
#define	IMCP_SNIP_PACKET_TYPE_SW_UPDATE_RESPONSE				(unsigned short)(19)
#define	IMCP_SNIP_PACKET_TYPE_SW_UPDATE_STATUS					(unsigned short)(20)
#define	IMCP_SNIP_PACKET_TYPE_MESH_INIT_STATUS					(unsigned short)(21)
#define	IMCP_SNIP_PACKET_TYPE_MESH_IMCP_KEY_UPDATE				(unsigned short)(22)
#define	IMCP_SNIP_PACKET_TYPE_RESPONSE_PACKET					(unsigned short)(23)
#define	IMCP_SNIP_PACKET_TYPE_VLAN_CONFIG_INFO					(unsigned short)(24)
#define	IMCP_SNIP_PACKET_TYPE_CONFIG_REQ_RES					(unsigned short)(25)
#define	IMCP_SNIP_PACKET_TYPE_CHANNEL_CHANGE					(unsigned short)(26)
#define	IMCP_SNIP_PACKET_TYPE_CONFIGD_WATCHDOG_REQUEST			(unsigned short)(27)
#define	IMCP_SNIP_PACKET_TYPE_CONFIGD_WATCHDOG_RESPONSE			(unsigned short)(28)
#define	IMCP_SNIP_PACKET_TYPE_DS_EXCERCISER						(unsigned short)(29)
#define	IMCP_SNIP_PACKET_TYPE_WM_INFO_REQUEST					(unsigned short)(30)
#define	IMCP_SNIP_PACKET_TYPE_WM_INFO_RESPONSE					(unsigned short)(31)
#define IMCP_SNIP_PACKET_TYPE_SET_TESTER_TESTEE_MODE_REQUEST	(unsigned short)(32)
#define IMCP_SNIP_PACKET_TYPE_HEARTBEAT2						(unsigned short)(33)
#define	IMCP_SNIP_PACKET_TYPE_RESTORE_DEFAULTS_REQ				(unsigned short)(34)
#define	IMCP_SNIP_PACKET_TYPE_REG_DOMAIN_REQ_RES				(unsigned short)(35)
#define	IMCP_SNIP_PACKET_TYPE_REG_DOMAIN_INFO					(unsigned short)(36)
#define	IMCP_SNIP_PACKET_TYPE_ACK_TIMEOUT_REQ_RES				(unsigned short)(37)
#define	IMCP_SNIP_PACKET_TYPE_ACK_TIMEOUT						(unsigned short)(38)

#define IMCP_SNIP_PACKET_TYPE_IF_SUPPORTED_CHANNELS_REQUEST		(unsigned short)(39)
#define IMCP_SNIP_PACKET_TYPE_IF_SUPPORTED_CHANNELS_INFO		(unsigned short)(40)
#define IMCP_SNIP_PACKET_TYPE_STA_INFO_REQUEST					(unsigned short)(41)
#define IMCP_SNIP_PACKET_TYPE_STA_INFO_RESPONSE					(unsigned short)(42)
#define IMCP_SNIP_PACKET_TYPE_DOT11E_CATEGORY_INFO				(unsigned short)(43)
#define IMCP_SNIP_PACKET_TYPE_DOT11E_CONFIG_INFO				(unsigned short)(44)
#define IMCP_SNIP_PACKET_TYPE_GENERIC_COMMAND_REQ_RES			(unsigned short)(45)
#define IMCP_SNIP_PACKET_TYPE_HIDE_SSID_INFO					(unsigned short)(46)
#define IMCP_SNIP_PACKET_TYPE_ACL_CONFIG_INFO					(unsigned short)(47)

#define IMCP_SNIP_PACKET_TYPE_DOWNLINK_SATURATION_INFO			(unsigned short)(48)
#define IMCP_SNIP_PACKET_TYPE_REBOOT_REQUIRED					(unsigned short)(49)

#define IMCP_SNIP_PACKET_TYPE_PRIVATE_CHANNEL_REQ_RES			(unsigned short)(50)
#define IMCP_SNIP_PACKET_TYPE_PRIVATE_CHANNEL_INFO				(unsigned short)(51)

#define IMCP_SNIP_PACKET_TYPE_EFFISTREAM_REQ_RES				(unsigned short)(52)
#define IMCP_SNIP_PACKET_TYPE_EFFISTREAM_INFO					(unsigned short)(53)

#define	IMCP_SNIP_PACKET_TYPE_RADIO_DIAGNOSTIC_COMMAND			(unsigned short)(54)
#define	IMCP_SNIP_PACKET_TYPE_RADIO_DIAGNOSTIC_DATA				(unsigned short)(55)

#define	IMCP_SNIP_PACKET_TYPE_RADIO_DATA_REQ_RES				(unsigned short)(56)
#define	IMCP_SNIP_PACKET_TYPE_RADIO_DATA_INFO					(unsigned short)(57)

#define IMCP_SNIP_PACKET_TYPE_NODE_MOVE_NOTIFICATION			(unsigned short)(58)

#define IMCP_SNIP_PACKET_TYPE_CLIENT_SIGNAL_INFO				(unsigned short)(59)

#define IMCP_SNIP_PACKET_TYPE_SIP_PRIVATE						(unsigned short)(60)
#define	IMCP_SNIP_PACKET_TYPE_SIP_CONFIG_INFO					(unsigned short)(61)

#define IMCP_SNIP_PACKET_CAPABILITIES							(unsigned short)(61)

#define IMCP_MAC_ADDR_REQUEST_RESPONSE                   (unsigned short)(62)
#define IMCP_MAC_ADDR_INFO_REQUEST 								(unsigned short)(63)

#define IMCP_MGMT_GW_CONFIG_REQUEST								(unsigned short)(64)
#define IMCP_MGMT_GW_CONFIG_RESPONSE							(unsigned short)(65)

#define IMCP_USE_ENCRYPTION      (unsigned short)   (0x8000)       // upper bit in packet type indicates the packet is encrypted  
/*
 *	IMCP generic command request response values
 */
#define IMCP_SNIP_GENERIC_COMMAND_REQUEST					0
#define IMCP_SNIP_GENERIC_COMMAND_RESPONSE					1

/*
 *	IMCP config request response values
 */
#define IMCP_SNIP_CONFIG_REQUEST							0
#define IMCP_SNIP_CONFIG_RESPONSE							1

/*
 *	IMCP configuration request response types
 */
#define IMCP_SNIP_CONFIG_TYPE_AP_CONFIG						1
#define IMCP_SNIP_CONFIG_TYPE_IP_CONFIG						2
#define IMCP_SNIP_CONFIG_TYPE_VLAN_CONFIG					4
#define IMCP_SNIP_CONFIG_TYPE_DOT11E_CAT_CONFIG				8
#define IMCP_SNIP_CONFIG_TYPE_DOT11E_CONFIG					16	
#define IMCP_SNIP_CONFIG_TYPE_HIDE_SSID_CONFIG				32
#define IMCP_SNIP_CONFIG_TYPE_ACL_CONFIG					64
#define IMCP_SNIP_TYPE_EXTEND_TO_BYTE2						128

/* 
 * IMCP configuration request response types in byte2
 */
#define IMCP_SNIP_CONFIG_TYPE_SIP_CONFIG					1


#define IMCP_SNIP_CONFIG_TYPE_CAPABILITIES					9	 /* count of configs supported cirrently */

/*
 *	IMCP generic command types
 */
#define IMCP_SNIP_GENERIC_COMMAND_TYPE_REJECT_CLIENTS			1
#define IMCP_SNIP_GENERIC_COMMAND_TYPE_SET_CTM					2
#define IMCP_SNIP_GENERIC_COMMAND_TYPE_RESET_CTM				3
#define IMCP_SNIP_GENERIC_COMMAND_TYPE_DNLINK_DCA				4
#define IMCP_SNIP_GENERIC_COMMAND_TYPE_UPLINK_SCAN				5
#define IMCP_SNIP_GENERIC_COMMAND_TYPE_DNLINK_SATURATION		6
#define IMCP_SNIP_GENERIC_COMMAND_TYPE_ENABLE_BEACONING_UPLINK	7
#define IMCP_SNIP_GENERIC_COMMAND_TYPE_DISABLE_BEACONING_UPLINK	8

#define IMCP_SNIP_GENERIC_COMMAND_CAPABILITIES					8	/* count of commands supported currently */

/**
 * IMCPoSNIP function return values
 */
#define IMCP_SNIP_ERROR_SUCCESS								0
#define IMCP_SNIP_ERROR_NOT_IMPLEMENTED						-1
#define IMCP_SNIP_ERROR_INVALID_ARGUMENTS					-2
#define IMCP_SNIP_ERROR_INADEQUATE_BUFFER_LENGTH			-3
#define IMCP_SNIP_ERROR_PACKET_NOT_SNIP						-4
#define IMCP_SNIP_ERROR_INVALID_FUNCTION					-5
#define IMCP_SNIP_ERROR_PACKET_NOT_IMCP						-6
#define IMCP_SNIP_ERROR_WRONG_SNIP_VERSION_OR_TYPE			-7
#define IMCP_SNIP_ERROR_WRONG_MESHID						-8
#define IMCP_SNIP_ERROR_WRONG_PARAM_SIZE					-9


#define IMCP_PACKET_LENGTH_ERROR                                   -10
#define IMCP_SNIP_ERROR_WRONG_SNIP_FLAGS                           -11



/*
 * IMCP Packet type IDs
 */

#define IMCP_IF_802_11_EXT_11N_INFO_ID					(unsigned short)(111)
#define IMCP_IF_802_11_EXT_11N_11AC_INFO_ID				(unsigned short)(112)
#define IMCP_NODE_INFO_ID					  	(unsigned short)1
#define	IMCP_IF_802_3_INFO_ID 					  	(unsigned short)2
#define IMCP_IF_802_11_INFO_ID 					  	(unsigned short)3
#define IMCP_IF_802_15_4_INFO_ID 					(unsigned short)4
#define IMCP_HEARTBEAT_INFO_ID					  	(unsigned short)5
#define IMCP_STA_ASSOC_NOTIFICATION_INFO_ID			  	(unsigned short)6					    
#define IMCP_STA_DISASSOC_NOTIFICATION_INFO_ID				(unsigned short)7					    
#define IMCP_BUFFER_COMMAND_INFO_ID 					(unsigned short)8
#define IMCP_BUFFER_COMMAND_ACK_ID 					(unsigned short)9
#define IMCP_CHANNEL_SCAN_LOCK_INFO_ID					(unsigned short)10
#define IMCP_CHANNEL_SCAN_UNLOCK_INFO_ID 				(unsigned short)11
#define IMCP_CHANNEL_SCAN_LOCK_OBJECTION_INFO_ID			(unsigned short)12
#define IMCP_RESET_INFO_ID						(unsigned short)13					    
#define IMCP_AP_HARDWARE_INFO_ID					(unsigned short)14					    
#define IMCP_AP_SUB_TREE_INFO_ID					(unsigned short)16					    
#define IMCP_IP_CONFIG_INFO_REQ_RES_INFO_ID				(unsigned short)18
#define IMCP_MESH_INIT_STATUS_INFO_ID 					(unsigned short)22
#define IMCP_MESH_IMCP_KEY_UPDATE_INFO_ID 				(unsigned short)23
#define IMCP_CHANNEL_CHANGE_INFO_ID					(unsigned short)26
#define	IMCP_DS_EXCERCISER_INFO_ID 					(unsigned short)29
#define IMCP_WM_INFO_REQUEST_ID						(unsigned short)30
#define IMCP_WM_INFO_RESPONSE_ID					(unsigned short)31
#define IMCP_HEARTBEAT2_INFO_ID						(unsigned short)33
#define IMCP_IF_SUPPORTED_CHANNELS_INFO_ID 				(unsigned short)40
#define IMCP_STA_INFO_REQUEST_INFO_ID					(unsigned short)41	
#define IMCP_STA_INFO_RESPONSE_ID					(unsigned short)42
#define IMCP_GENERIC_COMMAND_REQ_RES_INFO_ID				(unsigned short)45
#define IMCP_RADIO_DIAGNOSTIC_DATA_ID 					(unsigned short)50
#define IMCP_NODE_MOVE_NOTIFICATION_INFO_ID				(unsigned short)53
#define IMCP_CLIENT_SIGNAL_INFO_ID 					(unsigned short)54
#define IMCP_SIP_PRIVATE_INFO_ID					(unsigned short)55
#define IMCP_DOWNLINK_SATURATION_INFO_ID				(unsigned short)58



#define IMCP_MSG_SEND_ID_AND_LEN(p,node_id,node_len)    {\
                                                		node_id = al_impl_cpu_to_le16(node_id);\
                                                		memcpy(p,&node_id, sizeof(unsigned short));\
								al_print_log(AL_CONTEXT AL_LOG_TYPE_DEBUG," Node_id : %hd func : %s LINE : %d\n",node_id,__func__,__LINE__);\
                                                		p += sizeof(unsigned short); \
                                                		node_len = al_impl_cpu_to_le16(node_len & IMCP_LEN_MASK);\
								al_print_log(AL_CONTEXT AL_LOG_TYPE_DEBUG," Node_len : %hd func : %s LINE : %d\n",node_len,__func__,__LINE__);\
                                                		memcpy(p,&node_len, sizeof(unsigned short));\
                                                		p += 2;\
                                                        }\

#define IMCP_MSG_RECEIVE_ID_AND_LEN(p,node_id,node_len)   {\
                                                                memcpy(&node_id, p, sizeof(unsigned short));\
								node_id = al_impl_le16_to_cpu(node_id);\
								al_print_log(AL_CONTEXT AL_LOG_TYPE_DEBUG," Receive_node_id : %hd func : %s LINE : %d\n",node_id,__func__,__LINE__);\
								p += sizeof(unsigned short);\
								memcpy(&node_len, p, sizeof(unsigned short));\
								node_len = al_impl_le16_to_cpu(node_len);\
								if(node_len & IMCP_EXT_MASK)\
								{\
									al_print_log(AL_CONTEXT AL_LOG_TYPE_DEBUG,"Extension bit is setted func : %s LINE : %d\n",__func__,__LINE__);\
								}\
								node_len = node_len & IMCP_LEN_MASK;\
                                                                al_print_log(AL_CONTEXT AL_LOG_TYPE_DEBUG," Receive_node_len : %hd func : %s LINE : %d\n",node_len,__func__,__LINE__); \
                                                                p += sizeof(unsigned short); \
							  }\




struct imcp_known_ap_info {
	unsigned char		et;					/* known ap change type Added / Deleted*/
	al_net_addr_t		ap_ds_mac;			/* MAC addr of DS inteface of the AP */
	unsigned char		st;					/* Signal strenth from parent */
};

typedef struct imcp_known_ap_info  imcp_known_ap_info_t;

struct phy_hardware_info {
	al_net_addr_t		mac_addr;			/* MAC addr of net inteface of the AP */
#if 1
	al_802_11_ht_capab_t	ht_capab;
	al_802_11_fram_agre_t	fram_agre;
	al_802_11_vht_capab_t   vht_capab;
#endif
	unsigned char		type;				/* Type of the net interface */
	unsigned char		sub_type;			/* Sub Type of the net interface */
	unsigned char		usage_type;			/* Usage Type of the net interface */
	unsigned char		channel;			/* channel no of the net interface */
	unsigned char		bonding;			/* Bonding of the net interface */
	unsigned char		essid_length;		/* Essid length */
	char			essid[MAX_ESSID_LENGTH];			/* Essid for the interface */
	unsigned int		rts_thrd;			/* RTS threshold */
	unsigned int		frag_thrd;			/* Frag threshold */
	unsigned int		beacon_int;			/* Beacon interval */
	unsigned char		dca;				/* DCA flag for net interface */
	unsigned char		tx_power;			/* tx power of the net interface */
	unsigned int		tx_rate;			/* TX Rate */
	unsigned char		dca_list_count;		/* DCA list count of the net interface */
	unsigned char		dca_list[16];		/* DCA list of the net interface */
	unsigned char		service_type;		/* service type of the net interface */
	unsigned char		name_length;		/* Name Length */
	char				name[129];			/* name of interface */
	unsigned char		ack_timeout;
	unsigned char		hide_ssid;
};

typedef struct phy_hardware_info   phy_hardware_info_t;


struct imcp_child_info {
	al_net_addr_t		mac_addr;
	unsigned int		down_tx_rate;
	unsigned char		signal_strength;
};

typedef struct imcp_child_info imcp_child_info_t;

struct channels_info {
	unsigned char	number;
	unsigned int	frequency;		/** In MHz */
	unsigned int	flags;
	char			max_reg_power;
	char			max_power;
	char			min_power;

};

typedef struct channels_info channels_info_t;

struct imcp_acl_entry {
	al_net_addr_t	sta_mac;
	unsigned short	vlan_tag;
	unsigned char	allow;
	unsigned char	dot11e_enabled;
	unsigned char	dot11e_category;
};

typedef struct imcp_acl_entry imcp_acl_entry_t;

struct client_signal_info {
	al_net_addr_t	sta_mac;
	unsigned char	rssi;
};
typedef struct client_signal_info client_signal_info_t;

/**
 * 
 * Format for the CTC field to be used by F/W 2.5.0 onwards
 *
 *  31 30 29 28 27 26 25 24 23 22 21 20 19 18 17 16 15 14 13 12 11 10 09 08 07 06 05 04 03 02 01 00
 * +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
 * |M |  DS IF SUB-TYPE    |                       |                       |   TREE LINK RATE      |
 * +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
 *
 * All blank bits are reserved
 *
 * Format for the CTC field to be used by F/W 2.5.26 onwards
 *
 *  31 30 29 28 27 26 25 24 23 22 21 20 19 18 17 16 15 14 13 12 11 10 09 08 07 06 05 04 03 02 01 00
 * +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
 * |M |  DS IF SUB-TYPE    |                       |      BRD VOLTAGE      |   TREE LINK RATE      |
 * +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
 *
 * Format for CTC field after F/W 2.5.65 onwards
 *
 *  31 30 29 28 27 26 25 24 23 22 21 20 19 18 17 16 15 14 13 12 11 10 09 08 07 06 05 04 03 02 01 00
 * +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
 * |M |  DS IF SUB-TYPE    | BRD MEMORY FREE (MB)  |      BRD VOLTAGE      |   TREE LINK RATE      |
 * +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
 *
 * --------------------------------------------------------------------------------------------------------
 * When in DISJOINT AD-HOC MODE Format for CRSSI field after F/W 2.5.66 onwards
 *
 *  31 30 29 28 27 26 25 24 23 22 21 20 19 18 17 16 15 14 13 12 11 10 09 08 07 06 05 04 03 02 01 00
 * +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
 * |V |                                            |             DHCP R VALUE                   |F |
 * +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
 * 
 * F = 1 for limited, 0 for Full
 * V = 1 signifies that the CRSSI field is formatted in the new form, otherwise CRSSI is formatted
 * in the old form and will be ignored.
 *
 * --------------------------------------------------------------------------------------------------------
 * F/W 2.5.69 onwards  HB1 to contain 12 byte (4 for latitude, 4 for longitude, 2 byte altitude, 2 byte speed)
 * GPS information
 * 
 * The coordinates are formatted as 32 bit integers as shown below:
 *
 *  |3 3 2 2 2 2 2 2|2 2 2 2 1 1 1 1|1 1 1 1 1 1 0 0|0 0 0 0 0 0 0 0|  
 * 	|1 0 9 8 7 6 5 4|3 2 1 0 9 8 7 6|5 4 3 2 1 0 9 8|7 6 5 4 3 2 1 0|                                                              
 *	+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 *	|S|     MINUTE MANTISSA STARTING AT DECIMAL     |  DEGREES      |
 *	+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 *
 *	For Latitude S = 0 for N, 1 for S
 *	For Longtude S = 0 for E, 1 for W
 *
 *	e.g.
 *
 *	in 121.56789W will be stored as
 *
 *  |3 3 2 2 2 2 2 2|2 2 2 2 1 1 1 1|1 1 1 1 1 1 0 0|0 0 0 0 0 0 0 0|  
 * 	|1 0 9 8 7 6 5 4|3 2 1 0 9 8 7 6|5 4 3 2 1 0 9 8|7 6 5 4 3 2 1 0|                                                              
 *	+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 *	|1|      Binary equivalent of 56789             | Binary eq 121 |
 *	+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 *
 *
 * Maximum minute accuracy is 0.000001 (0.0036 seconds)
 *  
 * The receiver shall first use a logic similar to sprintf(buf,"%d.%d",Degree,Minute) to get
 * the string form of the coordinate. From here floating point and other forms can be retrieved.
 *
 * The altitude is specified in meters (MAX 65536) and the speed in Km/h (MAX 65536)
 */


#define IMCP_SNIP_CTC_GET_MOBILE(ctc)				((ctc) >> 31)
#define IMCP_SNIP_CTC_GET_TREE_LINK_RATE(ctc)		((ctc) & 0xFF)
#define IMCP_SNIP_CTC_GET_DS_IF_SUB_TYPE(ctc)		(((ctc) >> 24) & 0x7F)
#define IMCP_SNIP_CTC_GET_BOARD_VOLTAGE(ctc)		(((ctc) >> 8) & 0xFF)
#define IMCP_SNIP_CTC_GET_MEM_FREE(ctc)				(((ctc) >> 16) & 0xFF)

#define IMCP_SNIP_CTC_SET_MOBILE(ctc,mobile)		do { (ctc) &= 0x7FFFFFFF; (ctc) |= ((mobile) << 31); }while(0)
#define IMCP_SNIP_CTC_SET_TREE_LINK_RATE(ctc,rate)	do { (ctc) &= 0xFFFFFF00; (ctc) |= (rate); }while(0)
#define IMCP_SNIP_CTC_SET_DS_IF_SUB_TYPE(ctc,st)	do { (ctc) &= 0x80FFFFFF; (ctc) |= ((st) << 24); }while(0)
#define IMCP_SNIP_CTC_SET_BOARD_VOLTAGE(ctc,v)		do { (ctc) &= 0xFFFF00FF; (ctc) |= (((v) & 0xFF) << 8); }while(0)
#define IMCP_SNIP_CTC_SET_MEM_FREE(ctc,m)			do { (ctc) &= 0xFF00FFFF; (ctc) |= (((m) & 0xFF) << 16); }while(0)

#define IMCP_SNIP_CRSSI_GET_FUNCTIONALITY(crssi)	((crssi) & 0x01)
#define IMCP_SNIP_CRSSI_GET_DHCP_R_VALUE(crssi)		( ((crssi) >> 1) & 0x7FFF )
#define IMCP_SNIP_CRSSI_GET_VALID(crssi)			( (crssi) >> 31 )
#define IMCP_SNIP_CRSSI_SET_STANDARD_WDS(crssi)     	((crssi) |= 1 << 16 ) /* Advertise Gen2*/

#define IMCP_SNIP_CRSSI_SET_FUNCTIONALITY(crssi,f)	\
		do { (crssi) &= 0xFFFFFFFE; (crssi) |= (f); }while(0)
#define IMCP_SNIP_CRSSI_SET_DHCP_R_VALUE(crssi,r)	\
		do { (crssi) &= 0xFFFF0001; \
			(crssi) |= ( ( (r) & 0x7FFF ) << 1 ); }while(0)
#define IMCP_SNIP_CRSSI_SET_VALID(crssi,v)		\
		do { (crssi) &= 0x7FFFFFFF; (crssi) |= ((v) << 31); }while(0)

#define IMCP_SNIP_BUFFERING_TYPE_START_REQUEST		0
#define IMCP_SNIP_BUFFERING_TYPE_STOP_REQUEST		1
#define IMCP_SNIP_BUFFERING_TYPE_DISABLE_UPLINK		2	/* Used for downlink saturation calculation */
#define IMCP_SNIP_BUFFERING_TYPE_RADAR_DETECTED		3	/* Used to inform upon radar detection */
#define IMCP_SNIP_BUFFERING_TYPE_START_REQUEST_WITH_ACK	4



#define IMCP_ID_AND_LEN_SIZE  4		/* id + length */ 

#define HEARTBEAT2_INFO_FIXED_LEN	6+4+4+1+4+4+1+1+1	/*DS_MAC+ sqnr + up_tx_rate + child_count + flags + last_update_num + imcp_packet_capabilities + imcp_packet_capabilities + imcp_packet_capabilities */
#define SIP_PRIVATE_FIXED_LEN	6+2 	/*DS_MAC + Data_length */
#define NODE_MOVE_NOTIFICATION_FIXED_LEN	6+6	/* DS_MAC + NODE_DS_MAC */
#define CHANNEL_CHANGE_FIXED_LEN	6+1+2 		/*DS_MAC + wm_channel + milli_seconds_left */
#define STA_INFO_RESPONSE_FIXED_LEN	6+6+6+1+1+1+1+2+1+1+4+4+4 	/* Sender DS MAC + STA MAC + PARENT WM MAC + Parent_Essid_length + auth_state + b_client + is_imcp + listen_interval + capability + key_index + bytes_received + bytes-sent + active_time*/

#define CHANNEL_SCAN_LOCK_FIXED_LEN	6+1	/*DS MAC + PHY ID*/
#define CHANNEL_SCAN_UNLOCK_FIXED_LEN	6+1	/*DS MAC + PHY ID*/
#define CHANNEL_SCAN_LOCK_OBJECTION_FIXED_LEN	6+1	/*DS MAC + PHY ID*/
#define MESH_INIT_STATUS_FIXED_LEN	6+1+1	/*DS MAC + status + status_data_length*/
#define MESH_IMCP_KEY_UPDATE_FIXED_LEN		6+1+1+1+1	/*DS MAC + responses_ID + new_meshid_length + new_key_length */
#define DS_EXCERCISER_FIXED_LEN		0	/* */
#define WM_INFO_REQUEST_FIXED_LEN	6+6+6+1+1	/*DEST_MAC_ADDR + SRC_DS_MAC + WM_MAC_DES + ds_type + ds_sub_type */
#define WM_INFO_RESPONSE_FIXED_LEN	6+6+1+1+6+1+1	/*DS_MAC_DEST  + DS_MAC_SRC + WM_type + wm_sub_type + wm_addr + wm_channel + essid_length */
#define IF_SUPPORTED_CHANNELS_INFO_FIXED_LEN	6+1+1	/*DS_MAC + if_name_length + channel_count */
#define GENERIC_COMMAND_REQ_RES_INFO_FIXED_LEN	6+2+1	/*DS_MAC + type + req_res */
#define BUFFER_COMMAND_FIXED_LEN	6+6+1+4	/*DS_MAC_DES + DS_MAC_SRC + type + timeout */
#define DS_EXCERCISER_FIXED_LEN		6	/*DS_MAC_DES */
#define RADIO_DIAGNOSTIC_DATA_FIXED_LEN	6+1+2	/*DS_MAC_DES + net_if_name +  data_length */
#define CLIENT_SIGNAL_INFO_FIXED_LEN	6+1+2	/*DS_MAC + if_name + entry_count */
#define DOWNLINK_SATURATION_INFO_FIXED_LEN	6+1+1	/*DS_MAC + net_if_name + channel_count  */


#define FLAGS 0


struct imcp_packet_info {

        unsigned short   packet_type;
        unsigned short   flags;

	al_net_if_t*	net_if;			/* net if from which packet came or to be sent on */
	al_net_addr_t*	dest_addr;		/* Specific destination to be sent to */

	union {
		
		struct {

			al_net_addr_t		ds_mac;			/* Senders DS Mac Address */
			unsigned int		sqnr;			/* Sequence No. */
			unsigned int		ctc;			/* Cumulative Toll Cost */
			unsigned char		hpc;			/* Hope Count */
			al_net_addr_t		root_bssid;		/* BSSID of root AP */
			unsigned int		crssi;			/* Cumulative rssi */
			unsigned char		hi;			/* health index */
			unsigned char		hbi;			/* Heart Beat Interval */
			unsigned int		bit_rate;		/* Transmit Bit Rate for Parent */
			al_net_addr_t		parent_bssid;		/* BSSID of parent AP */
			unsigned char		board_temp;		/* Board Temp */
			unsigned char		children_count;		/* Children count */
			unsigned char		known_ap_count;		/* Known AP Count */
			unsigned int		tx_packet_count;	/* No of packets transmitted */
			unsigned int		rx_packet_count;	/* No of packets received */
			unsigned int		tx_byte_count;		/* No of bytes transmitted */
			unsigned int		rx_byte_count;		/* No of bytes received */
			unsigned int		latitude;		/* Encoded latitude */
			unsigned int		longitude;		/* Encoded longitude */
			unsigned short		altitude;		/* Altitude in meters */
			unsigned short		speed;			/* Speed in Km/h */
			imcp_known_ap_info_t	*known_aps_info;	/* Known AP info */
			al_net_addr_t		*immidigiate_sta;	/* IMMEDIATE CHILDREN */
		} heart_beat;

		struct  {

			al_net_addr_t				ap_ds_mac;			/* AP DS Mac Address */
			al_net_addr_t				ap_wm_mac;			/* AP WM Mac Address */
			al_net_addr_t				sta_mac;			/* STA Mac Address */
		} sta_assoc_notification;

		struct  {
			al_net_addr_t				ap_ds_mac;			/* AP DS Mac Address */
			al_net_addr_t				sta_mac;			/* STA Mac Address */
			unsigned short				reason;				/* Reason code for Disassoc Notification */
		} sta_disassoc_notification;

		struct {
			al_net_addr_t				ds_mac;				/* Senders DS Mac Address */
			al_net_addr_t				bssid;				/* BSSID of receiver */
		} handshake_request;

		struct {
			al_net_addr_t				src_ds_mac;			/* Src DS MAc Address */
			al_net_addr_t				bssid;				/* BSSID of sender */
			al_net_addr_t				dest_ds_mac;		/* Receivers DS Mac Address */
			al_net_addr_t				root_bssid;			/* BSSID of root AP */
			unsigned char				hpc;				/* Hope Count */
			unsigned int				ctc;				/* Cumulative Toll Cost */
			unsigned int				crssi;				/* Cumulative rssi */
			unsigned char				hbi;				/* Heart Beat Interval */
			unsigned char				ds_channel;			/* DS channel number */
			unsigned char				ds_type;			/* DS phy sub type */
		} handshake_response;

		struct {
			al_net_addr_t				ds_mac;				/* Senders DS Mac Address */
			unsigned char				phy_id;				/* physical layer identifier */
		} channel_scan_lock;

		struct {
			al_net_addr_t				ds_mac;				/* Senders DS Mac Address */
			unsigned char				phy_id;				/* physical layer identifier */
		} channel_scan_lock_objection;

		struct {
			al_net_addr_t				ds_mac;				/* Senders DS Mac Address */
			unsigned char				phy_id;				/* physical layer identifier */
		} channel_scan_unlock;

		al_net_addr_t					reset_ap;			/* AP DS Mac Address to which reset */

		struct {

			phy_hardware_info_t			ds_info;				/* AP DS Mac Address */
			unsigned char				wm_count;				/* AP WM Count */
			unsigned char				major_version;			/* Torna Major Version */
			unsigned char				minor_version;			/* Torna Minor Version */
			unsigned char				variant_version;		/* Torna Variant Version */
			unsigned char				name_length;			/* Name Length */
			char						name[129];				/* Name buffer */
            unsigned char               hardware_model_length;	/* Model length */
            char						hardware_model[256];	/* Hardware model */
			phy_hardware_info_t         *wms_info;				/* AP WM mac & phy type */
			phy_hardware_info_t			scanner_info;			/* Scanner mac & phy type */
		} hardware_info;

		struct {
			al_net_addr_t				ds_mac;				/* Receivers DS Mac Address */
		} hardware_info_request;

		struct {


			al_net_addr_t				ds_mac;				/* AP DS Mac Address */
			unsigned char				sta_count;			/* AP STA Count */
			al_net_addr_t				*sta_entries;		/* STA entries */
		} sub_tree_info;

		struct {
			al_net_addr_t				ds_mac;				/* AP DS Mac Address */
			unsigned char				req_res;			/* Request or Response */
			unsigned char				res_id;				/* Response id*/
			al_net_addr_t				pref_parent;
			unsigned char				signal_map[8];		/* Signal Map values */
			unsigned char				hb_interval;
			unsigned char				hb_miss_count;
			unsigned char				hop_cost;
			unsigned char				max_allow_hops;	
			unsigned char				la_scan_interval;
			unsigned char				change_res_thr;
			unsigned char				name_length;
			char						name[255];
			unsigned char				essid_length;
			char						essid[MAX_ESSID_LENGTH];
			unsigned char				desc_length;
			char						desc[255];
			unsigned char				gps_x_length;
			char						gps_x[255];
			unsigned char				gps_y_length;
			char						gps_y[255];
			unsigned int				rts_thrd;
			unsigned int				frag_thrd;
			unsigned int				beacon_int;
			unsigned char				dca;
			unsigned char				stay_awake_count;			
			unsigned char				bridge_ageing_time;
			unsigned char				fcc_certified_operation;
			unsigned char				etsi_certified_operation;
			unsigned int				ds_tx_rate;
			unsigned char				ds_tx_power;
			unsigned char				interface_count;
			phy_hardware_info_t			*interface_info;
		} ap_config_info;

		struct {
			al_net_addr_t				ds_mac;
			unsigned int				name_length;
			unsigned char				net_if_name[32];
		} ip_ap_start;

		struct {
			al_net_addr_t				ds_mac;
			unsigned char				req_res;
			unsigned char				type;
			unsigned char				res_id;
		} req_res_info;

		struct {
			al_net_addr_t				ds_mac;
			unsigned char				status;
			unsigned char				status_data_length;
			unsigned char				status_data[255];
		} mesh_init_status;

		struct {
			al_net_addr_t				ds_mac;
			unsigned char				response_id;
			unsigned char				new_meshid_length;
			unsigned char				new_meshid[256];
			unsigned char				key_length;
			unsigned char				key[256];
		} mesh_imcp_key_update;

		struct {
			al_net_addr_t				ds_mac;
			unsigned char				wm_channel;
			unsigned short				milli_seconds_left;

		} channel_change_notification;

		struct {
			al_net_if_t*				net_if_to_send;
			al_net_addr_t*				dest_mac;
			unsigned short				data_size;
			unsigned char*				data;
		} ds_excerciser;

		struct {
			al_net_addr_t				ds_mac_dest;				/* Tarrget's DS Mac Address */
			al_net_addr_t				ds_mac_src;					/* Source's DS Mac Address */
			al_net_addr_t				wm_mac_dest;				/* Target's WM MAC (can be zeroed) */
			unsigned char				ds_type;					/* Source's DS PHY type */
			unsigned char				ds_sub_type;				/* Source's DS PHY sub-type */
		}wm_info_request;

		struct {
			al_net_addr_t				ds_mac_dest;				/* Target's DS Mac Address */
			al_net_addr_t				ds_mac_src;					/* Source's DS Mac Address */
			unsigned char				wm_type;					/* Source's WM PHY type */
			unsigned char				wm_sub_type;				/* Source's WM PHY sub-type */
			al_net_addr_t				wm_addr;					/* Source's WM MAC addr */
			unsigned char				wm_channel;					/* Source's WM channel */
			unsigned char				essid_length;				/* Source's ESSID length */
			char						essid[MAX_ESSID_LENGTH];	/* Source's ESSID */
			unsigned char				other_wm_count;				/* Info about other downlinks */
			struct {
				unsigned char			essid_length;
				char					essid[MAX_ESSID_LENGTH];
				unsigned char			wm_channel;
				al_net_addr_t			wm_addr;
			}*other_wm_info;
		}wm_info_response;

		struct {
			al_net_addr_t				ds_mac;				/* AP DS Mac Address */
			unsigned char				req_res;			/* Request or Response */
			unsigned char				res_id;				/* Response id*/
			unsigned short				reg_domain;			/* regulatory domain */
			unsigned short				country_code;		/* country code */
		}reg_domain_cc_info;
		
		struct {
			al_net_addr_t				ds_mac;				/* AP DS Mac Address */
			unsigned char				req_res;			/* Request or Response */
			unsigned char				res_id;				/* Response id*/
			unsigned char				interface_count;
			phy_hardware_info_t			*interface_info;
		}ack_timeout;

		struct {
			al_net_addr_t				ds_mac;					/* AP DS Mac Address */
			unsigned char				channel_count;			/* IF channel Count */
			unsigned char				if_name_length;			/* IF Name Length */
			char						if_name[256];			/* IF Name buffer */
			channels_info_t				*channels_info;			/* freq,channel num,flags */
		}supported_channels_info;

		struct {
			al_net_addr_t				ds_mac;					/* Receivers DS Mac Address */
			unsigned char				if_name_length;			/* IF Name Length */
			char						if_name[256];			/* IF Name buffer */
		}supported_channels_request;

		struct {
			al_net_addr_t				ds_mac;
			unsigned char				mode;
			al_net_addr_t				peer_mac;
			unsigned char				interface_index;
			unsigned char				link_interface_index;
			unsigned char				channel;
			unsigned char				analysis_interval;
		} mesh_imcp_tester_testee_mode_req;

		struct {

			al_net_addr_t				ds_mac;
			unsigned int				sqnr;
			unsigned int				up_tx_rate;
			unsigned int				flags;
			unsigned int				last_update_number;
			unsigned char				imcp_packet_capabilities;
			unsigned char				imcp_config_capabilities;
			unsigned char				imcp_command_capabilities;
			unsigned char				child_count;
			imcp_child_info_t			*child_info;
		} heart_beat2;

		struct {
			al_net_addr_t				ds_mac;
			al_net_addr_t				sta_mac;

		} sta_info_request;

		struct {
			al_net_addr_t				ds_mac;
			al_net_addr_t				sta_mac;
			al_net_addr_t				parent_mac;
			unsigned char				parent_essid_length;
			char						parent_essid[MAX_ESSID_LENGTH];
			unsigned char				auth_state;
			unsigned char				b_client;
			unsigned char				is_imcp;
			unsigned short				listen_interval;
			unsigned char				capability;
			unsigned char				key_index;
			unsigned int				bytes_received;
			unsigned int				bytes_sent;
			unsigned int				active_time;

		} sta_info;

		struct {
			al_net_addr_t				ds_mac;
			unsigned short				type;
			unsigned char				req_res;
		} generic_command;

		struct {
			al_net_addr_t				ds_mac;				/* AP DS Mac Address */
			unsigned char				req_res;			/* Request or Response */
			unsigned char				res_id;				/* Response id*/
			unsigned char				interface_count;
			phy_hardware_info_t			*interface_info;
		}hide_ssid_info;		

		struct {
			al_net_addr_t					ds_mac;				/* AP DS Mac Address			*/
			unsigned char					req_res;			/* Request or Response			*/
			unsigned char					res_id;				/* Response id					*/
			unsigned char					count;				/* No. of 802.11e Categories	*/
			al_dot11e_category_details_t	details[4];			
		}dot11e_category;

		struct {
			al_net_addr_t					ds_mac;				/* AP DS Mac Address			*/
			unsigned char					req_res;			/* Request or Response			*/
			unsigned char					res_id;				/* Response id					*/
			unsigned char					count;				/* No. of ACL entries	        */
			imcp_acl_entry_t*				entries;
		}acl_info;

		struct {
			al_net_addr_t					ds_mac;				/* AP DS Mac Address			*/
			unsigned int					ctc;				/* CTC							*/
			unsigned char					hpc;				/* Hop count					*/
			unsigned int					crssi;				/* Cummulative RSSI				*/
			unsigned char					hbi;				/* Heartbeat interval			*/
			unsigned char					ds_if_channel;		/* Uplink channel			    */
			unsigned char					ds_if_sub_type;		/* Uplink phy sub type		    */
			al_net_addr_t					root_bssid;			
			unsigned char					mode;				/* To check LFR					*/
            unsigned char                   phy_sub_type ; // downlink subtype (either a/an/anac/ac/bgn/n/so....on) 
		}vendor_info;

		struct {
			al_net_addr_t					ds_mac_dest;		/* Destination DS Mac address	*/
			al_net_addr_t					ds_mac_src;			/* Source DS Mac address	*/
			unsigned char					type;				/* IMCP_SNIP_BUFFERING_TYPE_*	*/
			unsigned int					timeout;			/* Timeout in millis			*/
		}buffer_command;

		struct {
			al_net_addr_t					ds_mac_dest;		/* Destination DS Mac address	*/
		}buffer_command_ack;

		struct {
			al_net_addr_t					ds_mac_src;			/* Source DS Mac address	*/
			al_net_if_t*					net_if;				/* net_if for which the info is being sent */
			unsigned char					channel_count;
			struct {
				unsigned char				channel;
				unsigned char				ap_count;
				unsigned char				max_signal;
				unsigned char				avg_signal;
				unsigned int				total_duration;
				unsigned int				retry_duration;
				unsigned int				transmit_duration;
				struct {
					al_net_addr_t			bssid;
					char					essid[MAX_ESSID_LENGTH];
					unsigned char			signal;
					unsigned int			retry_duration;
					unsigned int			transmit_duration;
				}*ap_info;
			}*channel_info;
		}downlink_saturation;

		struct {
			al_net_addr_t					ds_mac_dest;		/* Destination DS Mac address	*/

		}reboot_required;

		struct {
			al_net_addr_t		ds_mac;			/* AP DS Mac Address */
			char				if_name[32];	/* IF-Name */
			unsigned short		command_length;
			unsigned char*		command_data;
		}radio_diagnostic_command;

		struct {
			al_net_addr_t		ds_mac;			/* AP DS Mac Address */
			al_net_if_t*		net_if;
			unsigned short		data_length;
			unsigned char*		data;
		}radio_diagnostic_data;

		struct {
			al_net_addr_t		ds_mac;				/* DS Mac address of reporter */
			al_net_addr_t		node_ds_mac;		/* DS MAC Address of NODE that has moved */

		}node_move_notification;

		struct {
			al_net_addr_t			ds_mac;
			char					if_name[32];	/* Downlink IF-Name */
			unsigned short			entry_count;
			client_signal_info_t*	signal_info;
		}client_signal_info;

		struct {

			al_net_addr_t			ds_mac;
			unsigned char			data[1024];
			unsigned short			data_length;
		}sip_private;

	} u;
};


typedef struct imcp_packet_info imcp_packet_info_t;

/**
 * The ACL entry has 2 bytes that include VLAN tag and ALLOW/DISALLOW bit
 *
 * +---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+
 * |   C   | E |A/D|                 VLAN TAG                      |                                            
 * +---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+
 *   15  14  13  12  11  10  9   8   7   6   5   4   3   2   1   0
 *
 *	E = 802.11e Enabled Flag
 *  C = 802.11e Access Category Index
 */

#define IMCP_SNIP_ACL_ENTRY_GET_VLAN_TAG(s)				((s) & 0x0FFF)
#define IMCP_SNIP_ACL_ENTRY_GET_ALLOW_BIT(s)			(((s) >> 12) & 1)
#define IMCP_SNIP_ACL_ENTRY_GET_DOT11E_ENABLED_BIT(s)	(((s) >> 13) & 1)
#define IMCP_SNIP_ACL_ENTRY_GET_DOT11E_CATEGORY_BIT(s)	(((s) >> 14) & 3)

/**
 * IMCP SNIP functions prototypes
 */

int imcp_initialize						(AL_CONTEXT_PARAM_DECL_SINGLE);
int imcp_snip_send						(AL_CONTEXT_PARAM_DECL imcp_packet_info_t* packet);
int imcp_snip_receive					(AL_CONTEXT_PARAM_DECL al_net_if_t* net_if, al_packet_t* packet);
int	imcp_snip_get_vendor_info_buffer	(AL_CONTEXT_PARAM_DECL imcp_packet_info_t* packet_in,unsigned char* vendor_info_out, int encryption);

#define IMCP_SNIP_VENDOR_INFO_ERR_ENCR	1
#define IMCP_SNIP_VENDOR_INFO_ERR_IMCP	2
#define IMCP_SNIP_VENDOR_INFO_ERR_VER	3
#define IMCP_SNIP_VENDOR_INFO_ERR_IDL	4
#define IMCP_SNIP_VENDOR_INFO_ERR_ID	5

int	imcp_snip_process_vendor_info		(AL_CONTEXT_PARAM_DECL unsigned char* vendor_info_in, unsigned char length_in,imcp_packet_info_t* packet_out);

void				imcp_snip_pool_initialize	(AL_CONTEXT_PARAM_DECL_SINGLE);
void				imcp_snip_pool_uninitialize	(AL_CONTEXT_PARAM_DECL_SINGLE);
imcp_packet_info_t*	imcp_snip_create_packet		(AL_CONTEXT_PARAM_DECL_SINGLE);	
void				imcp_snip_release_packet	(AL_CONTEXT_PARAM_DECL imcp_packet_info_t* packet);

#define IMCP_SNIP_ACTION_TYPE_ENABLE_BUFFERING	1

#endif //__IMCP_SNIP_H__
