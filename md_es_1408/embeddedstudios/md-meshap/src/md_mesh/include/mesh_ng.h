/********************************************************************************
* MeshDynamics
* --------------
* File     : mesh_ng.h
* Comments : Mesh New Generation Implementation
* Created  : 4/5/2006
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  4  |2/16/2009 | Changes for Uplink Scan Functionality           | Sriram |
* -----------------------------------------------------------------------------
* |  3  |6/13/2007 | Added pattern_count member                      | Sriram |
* -----------------------------------------------------------------------------
* |  2  |6/13/2007 | MESH_PARENT_FLAG_RESET_AVG added                | Sriram |
* -----------------------------------------------------------------------------
* |  1  |4/26/2007 | Re-enforcement logic added for Syncing          | Sriram |
* -----------------------------------------------------------------------------
* |  0  |4/5/2006  | Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

#ifndef __MESH_NG_H__
#define __MESH_NG_H__

#define MESH_PARENT_FLAG_PREFERRED     0x00000001
#define MESH_PARENT_FLAG_MOBILE        0x00000002
#define MESH_PARENT_FLAG_CHILD         0x00000004
#define MESH_PARENT_FLAG_DISABLED      0x00000008
#define MESH_PARENT_FLAG_QUESTION      0x00000010
#define MESH_PARENT_FLAG_ADDED         0x00000020
#define MESH_PARENT_FLAG_RESET_AVG     0x00000040       /* Reset signal averaging window */
#define MESH_PARENT_FLAG_LIMITED       0x00000080       /* Node as limited functionality */
#define MESH_PARENT_FLAG_AD_DISABLE    0x00000100       /* Node is disabled for ad-hoc LFR resolution purposes */

#define MESH_NG_PARENT_IS_LFR(parent)     (((parent)->flags & MESH_PARENT_FLAG_LIMITED) && ((parent)->hpc == 0))

#define MESH_NG_PARENT_IS_LFRS(parent)    (((parent)->flags & MESH_PARENT_FLAG_LIMITED) && ((parent)->hpc == 0))

#include <linux/module.h>


struct parent
{
   unsigned int  flags;
   al_net_addr_t ds_mac;                /** ds_mac and bssid together uniqely identify a parent */
   al_net_addr_t bssid;                 /** ds_mac and bssid together uniqely identify a parent */
   al_net_addr_t root_bssid;
   unsigned char channel;
   unsigned int  crssi;
   unsigned char ds_channel;
   unsigned char ds_type;
   char          essid[AL_802_11_ESSID_MAX_SIZE + 1];

   /** Variables used to calculate the score */
   unsigned char tree_bit_rate;                                                         /* As mentioned by node */
   unsigned char direct_bit_rate;                                                       /* What we directly get */
   int           disconnect_count;                                                      /* Disconnect cost */

   unsigned char score;                                                                 /* As calculated by us */

   /** Secondary criteria */
   unsigned char signal;                                                                        /* As perceived by us */
   int           hpc;                                                                           /* Hop count as reported */

   unsigned int  sqnr;                                                                          /* Sequence number as reported */

   /** Variables used by sampling algorithm */
   unsigned int  max_sample_packets;                                                    /* Maximum number of sample packets to transmit */
   unsigned int  sample_packets;                                                        /* Current count of number of packets transmitted */
   unsigned int  t_next;                                                                /* Next check time parameter */
   al_u64_t      next_sample_time;                                                      /* Next sample timestamp */
   unsigned long last_rate_check;

   /** Static systems use last_hb_time, hbi mesh_config->hbmc as the criteria for purging */
   al_u64_t      last_hb_time;
   unsigned char hbi;                                                                           /* Heart Beat Interval */

   /** Mobile systems use last_seen_time, mesh_config->hbmc and mesh_config->scan_dwell_time as the criteria for purging */
   al_u64_t      last_seen_time;
   int           pattern_count;

   /** Disjoint adhoc sectored mode systems */
   al_u64_t      first_seen_time;                                                               /* Used for LFR arbitration on sectored systems */

   unsigned char last_buffer_action_ctr;
   unsigned char mode;                                                                          /*To decide LFRS(which node to make the winner parameter)*/
   al_802_11_ht_capabilities_t     par_ap_ht_capab;
   al_802_11_vht_capabilities_t    par_ap_vht_capab;
   unsigned char    phy_sub_type;
   unsigned char    phy_support;
   al_u64_t      skip_bssid_count;

   al_u64_t      last_beacon_time;
};

typedef struct parent   parent_t;

//DOUBLE_ENTRY
struct _parent_priv
{
   parent_t            data;
   unsigned int        signature;
   int                 bucket;
   int                 ds_mac_bucket;
   struct _parent_priv *prev_hash;
   struct _parent_priv *next_hash;
   struct _parent_priv *prev_list;
   struct _parent_priv *next_list;
   struct _parent_priv *prev_sort;
   struct _parent_priv *next_sort;
   struct _parent_priv *prev_sample;
   struct _parent_priv *next_sample;
   struct _parent_priv *prev_dsmac_hash;
   struct _parent_priv *next_dsmac_hash;
   struct _parent_priv *prev_bssid;
   struct _parent_priv *next_bssid;
};
typedef struct _parent_priv   _parent_priv_t;

AL_DECLARE_GLOBAL_EXTERN(parent_t * current_parent);
AL_DECLARE_GLOBAL_EXTERN(int mesh_state);
AL_DECLARE_GLOBAL_EXTERN(int mesh_mode);
AL_DECLARE_GLOBAL_EXTERN(int scan_state);
AL_DECLARE_GLOBAL_EXTERN(unsigned int mesh_test_flags);
AL_DECLARE_GLOBAL_EXTERN(parent_t * current_sampling_parent);
AL_DECLARE_GLOBAL_EXTERN(al_u64_t last_table_check);
AL_DECLARE_GLOBAL_EXTERN(int func_mode);
AL_DECLARE_GLOBAL_EXTERN(int mesh_hop_level);
AL_DECLARE_GLOBAL_EXTERN(parent_t dummy_lfr_parent);
AL_DECLARE_GLOBAL_EXTERN(al_atomic_t lock_parent_change);
AL_DECLARE_GLOBAL_EXTERN(al_atomic_t lock_dn_channel_change);
AL_DECLARE_GLOBAL_EXTERN(al_atomic_t dn_channel_change_started);
AL_DECLARE_GLOBAL_EXTERN(al_atomic_t up_channel_change_started);
AL_DECLARE_GLOBAL_EXTERN(al_atomic_t up_cancel_channel_change);

#define MESH_NG_TABLE_CHECK_INTERVAL    300

void mesh_ng_init(AL_CONTEXT_PARAM_DECL_SINGLE);
void mesh_ng_uninit(AL_CONTEXT_PARAM_DECL_SINGLE);
void mesh_ng_clear_parents(AL_CONTEXT_PARAM_DECL_SINGLE);
parent_t *mesh_ng_create_parent(AL_CONTEXT_PARAM_DECL al_net_addr_t *ds_mac, al_net_addr_t *bssid);
void mesh_ng_destroy_parent(AL_CONTEXT_PARAM_DECL parent_t *parent);
void mesh_ng_add_parent(AL_CONTEXT_PARAM_DECL parent_t *parent);
void mesh_ng_set_parent_bssid(AL_CONTEXT_PARAM_DECL parent_t *parent, al_net_addr_t *bssid);
parent_t *mesh_ng_get_parent(AL_CONTEXT_PARAM_DECL al_net_addr_t *ds_mac, al_net_addr_t *bssid, int from_interrupt);

void mesh_ng_evaluate_list(AL_CONTEXT_PARAM_DECL_SINGLE);
void mesh_ng_remove_sampling_head(AL_CONTEXT_PARAM_DECL_SINGLE);
void mesh_ng_add_sampling_parent(AL_CONTEXT_PARAM_DECL_SINGLE);
void mesh_ng_mark_parent_for_sampling(AL_CONTEXT_PARAM_DECL parent_t *parent);

void mesh_ng_destroy_disabled_parents(AL_CONTEXT_PARAM_DECL_SINGLE);

#define MESH_NG_ENUMERATE_PARENTS_TYPE_ALL         0
#define MESH_NG_ENUMERATE_PARENTS_TYPE_RELEVANT    1

int mesh_ng_enumerate_parents(AL_CONTEXT_PARAM_DECL int type, void *enum_param, int (*enum_func)(AL_CONTEXT_PARAM_DECL void *enum_param, parent_t *parent), int lock);

//DOUBLE_ENTRY
_parent_priv_t *_get_parent(AL_CONTEXT_PARAM_DECL al_net_addr_t *ds_mac, al_net_addr_t *bssid);
parent_t *mesh_ng_get_best_parent(AL_CONTEXT_PARAM_DECL_SINGLE);
void mesh_ng_set_parent_as_child(AL_CONTEXT_PARAM_DECL al_net_addr_t *ds_mac, int child, int from_interrupt);
int mesh_ng_enumerate_sampling_list(AL_CONTEXT_PARAM_DECL void *enum_param, int (*enum_func)(AL_CONTEXT_PARAM_DECL void *enum_param, parent_t *parent));
int mesh_ng_enumerate_bssid_list(AL_CONTEXT_PARAM_DECL al_net_addr_t *ds_mac, void *enum_param, int (*enum_func)(AL_CONTEXT_PARAM_DECL void *enum_param, parent_t *parent));

parent_t *mesh_ng_get_best_parent_adhoc(AL_CONTEXT_PARAM_DECL_SINGLE);

static AL_INLINE unsigned int mesh_ng_get_ctc(AL_CONTEXT_PARAM_DECL_SINGLE)
{
   unsigned int ctc;

   ctc = 0;

   if (mesh_mode == _MESH_AP_ROOT)
   {
      IMCP_SNIP_CTC_SET_MOBILE(ctc, 0);
      IMCP_SNIP_CTC_SET_TREE_LINK_RATE(ctc, 100);
   }
   else
   {
      IMCP_SNIP_CTC_SET_MOBILE(ctc, mesh_config->mobile_mode);

      if (_IS_LFR)
      {
         IMCP_SNIP_CTC_SET_TREE_LINK_RATE(ctc, 100);
      }
      else
      {
         if (current_parent->direct_bit_rate < current_parent->tree_bit_rate)
         {
            IMCP_SNIP_CTC_SET_TREE_LINK_RATE(ctc, current_parent->direct_bit_rate);
         }
         else
         {
            IMCP_SNIP_CTC_SET_TREE_LINK_RATE(ctc, current_parent->tree_bit_rate);
         }
      }
   }

   IMCP_SNIP_CTC_SET_DS_IF_SUB_TYPE(ctc, mesh_hardware_info->ds_if_sub_type);

   return ctc;
}


static AL_INLINE unsigned int mesh_ng_get_crssi(AL_CONTEXT_PARAM_DECL_SINGLE)
{
   unsigned int crssi;

   crssi = 0;

   if (mesh_mode == _MESH_AP_ROOT)
   {
      IMCP_SNIP_CRSSI_SET_FUNCTIONALITY(crssi, 0);
   }
   else
   {
      switch (func_mode)
      {
      case _FUNC_MODE_FFR:
      case _FUNC_MODE_FFN:
         IMCP_SNIP_CRSSI_SET_FUNCTIONALITY(crssi, 0);
         break;

      case _FUNC_MODE_LFR:
      case _FUNC_MODE_LFN:
         IMCP_SNIP_CRSSI_SET_FUNCTIONALITY(crssi, 1);
         break;
      }
   }

   IMCP_SNIP_CRSSI_SET_DHCP_R_VALUE(crssi, mesh_config->dhcp_r_value);
   IMCP_SNIP_CRSSI_SET_VALID(crssi, 1);

   /*Aishwarya: Set the CRSSI as gen2 for interop*/
   //printk(KERN_DEBUG" <%s> Before Crssi = %x\n", __func__, crssi);
   IMCP_SNIP_CRSSI_SET_STANDARD_WDS(crssi);
   //printk(KERN_DEBUG" <%s> After Crssi = %x\n", __func__, crssi);

   return crssi;
}


static AL_INLINE int mesh_ng_is_ds_phy_sub_type_compatible(AL_CONTEXT_PARAM_DECL unsigned char sub_type_1, unsigned char sub_type_2)
{
   switch (sub_type_1)
   {
   case AL_CONF_IF_PHY_SUB_TYPE_802_11_A:
      return(sub_type_2 == AL_CONF_IF_PHY_SUB_TYPE_802_11_A);

   case AL_CONF_IF_PHY_SUB_TYPE_802_11_B:
      return(sub_type_2 == AL_CONF_IF_PHY_SUB_TYPE_802_11_B || sub_type_2 == AL_CONF_IF_PHY_SUB_TYPE_802_11_BG);

   case AL_CONF_IF_PHY_SUB_TYPE_802_11_G:
      return(sub_type_2 == AL_CONF_IF_PHY_SUB_TYPE_802_11_G || sub_type_2 == AL_CONF_IF_PHY_SUB_TYPE_802_11_BG);

   case AL_CONF_IF_PHY_SUB_TYPE_802_11_BG:
      return(sub_type_2 == AL_CONF_IF_PHY_SUB_TYPE_802_11_B ||
             sub_type_2 == AL_CONF_IF_PHY_SUB_TYPE_802_11_G ||
             sub_type_2 == AL_CONF_IF_PHY_SUB_TYPE_802_11_BG);

      break;
   }

   return 0;
}


static AL_INLINE int mesh_ng_mac_address_compare(al_net_addr_t *a, al_net_addr_t *b)
{
   unsigned int n_a;
   unsigned int n_b;

   n_a = (a->bytes[2] << 24) + (a->bytes[3] << 16) + (a->bytes[4] << 8) + (a->bytes[5]);
   n_b = (b->bytes[2] << 24) + (b->bytes[3] << 16) + (b->bytes[4] << 8) + (b->bytes[5]);

   return((n_a < n_b) ? -1 : ((n_a > n_b) ? 1 : 0));
}


#endif /*__MESH_NG_H__*/
