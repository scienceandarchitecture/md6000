/********************************************************************************
* MeshDynamics
* --------------
* File     : mesh_sip_proc.c
* Comments : SIP information proc entry Implementation
* Created  : 5/21/2008
* Author   : Abhijit Ayarekar
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  0  |5/21/2008 | Created.                                        |Abhijit |
* -----------------------------------------------------------------------------
********************************************************************************/
#include <linux/proc_fs.h>
#include <linux/seq_file.h>
#include <linux/slab.h>
#include <linux/sched.h>
#include "al.h"
#include "al_802_11.h"
#include "sip_conf.h"
#include "meshap_proc.h"
#include "meshap_sip_proc.h"
#include "meshap_sip.h"

struct _proc_data
{
   char *p;
};

typedef struct _proc_data   _proc_data_t;

static struct proc_dir_entry *_meshap_sip_proc_entry = NULL;

//static int _sta_list_routine				(char *page, char **start, off_t off,int count, int *eof, void *data);
//static int _call_dlg_list_routine			(char *page, char **start, off_t off,int count, int *eof, void *data);
static void _meshap_sip_proc_add_read_entry(const char *name, struct file_operations *proc_file_fops, void *data);

/*int meshap_sip_proc_initialize(void)
 * {
 *      _meshap_sip_proc_entry	= meshap_proc_mkdir("sip");
 *      _meshap_sip_proc_add_read_entry("sta-list",_sta_list_fops,NULL);
 *      _meshap_sip_proc_add_read_entry("call-dlg-list",_call_dlg_list_fops,NULL);
 *      return 0;
 * }*/
static void _meshap_sip_proc_add_read_entry(const char *name, struct file_operations *proc_file_fops, void *data)
{
   proc_create_data(name, 0, _meshap_sip_proc_entry, proc_file_fops, data);
}


static void _sta_enum_routine(void *callback, const meshap_sip_sta_info_t *sta)
{
   //_proc_data_t*	data;
   struct seq_file *m;
   al_u64_t        tc;

   //data	= (_proc_data_t*)callback;
   m  = (struct seq_file *)callback;
   tc = al_get_tick_count(AL_CONTEXT_SINGLE);

   /*data->p	+= sprintf(data->p,
    *                 "%02x:%02x:%02x:%02x:%02x:%02x|%4s|%d.%d.%d.%d| %d  |  %x  |  %s  | %014d |\r\n",
    *                                 sta->info.mac[0], sta->info.mac[1], sta->info.mac[2],
    *                                 sta->info.mac[3], sta->info.mac[4], sta->info.mac[5],
    *                                 sta->info.extn,
    *                                 sta->ip_addr.bytes[0], sta->ip_addr.bytes[1],
    *                                 sta->ip_addr.bytes[2], sta->ip_addr.bytes[3],
    *                                 sta->rport,
    *                                 sta->flags,
    *                                 (sta->net_if != NULL) ? sta->net_if->name : "Mesh",
    *                                 (int)(tc - sta->last_check_time));*/
   seq_printf(m,
              "%02x:%02x:%02x:%02x:%02x:%02x|%4s|%d.%d.%d.%d| %d  |  %x  |  %s  | %014d |\r\n",
              sta->info.mac[0], sta->info.mac[1], sta->info.mac[2],
              sta->info.mac[3], sta->info.mac[4], sta->info.mac[5],
              sta->info.extn,
              sta->ip_addr.bytes[0], sta->ip_addr.bytes[1],
              sta->ip_addr.bytes[2], sta->ip_addr.bytes[3],
              sta->rport,
              sta->flags,
              (sta->net_if != NULL) ? sta->net_if->name : "Mesh",
              (int)(tc - sta->last_check_time));
}


//static int _sta_list_routine(char *page, char **start, off_t off,int count, int *eof, void *proc_data)
static int _sta_list_routine(struct seq_file *m, void *v)
{
   /*char         *p;
    * _proc_data_t	data;
    *
    * if(off != 0) {
    * eof	= 1;
    *      return 0;
    * }
    *
    * start		= page + off;
    * p			= page;
    * data.p		= p;
    *
    * data.p		+= sprintf(p,
    *                                      "------------------------------------------------------------------------------\r\n"
    *                                      "STATION ADDR     |EXTN| IP ADDR       | RPORT |FLAGS|NET_IF| CHECK TIME       |\r\n"
    *                                      "------------------------------------------------------------------------------\r\n");
    *
    * meshap_sip_enumerate_stas((void*)&data, _sta_enum_routine);
    *
    * p			= data.p;
    *
    * return (p - page); */

//	_proc_data_t	data;

   seq_printf(m,
              "------------------------------------------------------------------------------\r\n"
              "STATION ADDR     |EXTN| IP ADDR       | RPORT |FLAGS|NET_IF| CHECK TIME       |\r\n"
              "------------------------------------------------------------------------------\r\n");
   //meshap_sip_enumerate_stas((void*)&data, _sta_enum_routine);
   meshap_sip_enumerate_stas((void *)m, _sta_enum_routine);
}


static int _sta_list_proc_open(struct inode *inode, struct file *file)
{
   return single_open(file, _sta_list_routine, NULL);
}


static const struct file_operations _sta_list_fops =
{
   .owner   = THIS_MODULE,
   .open    = _sta_list_proc_open,
   .read    = seq_read,
   .llseek  = seq_lseek,
   .release = single_release,
};


static void _call_dlg_enum_routine(void *callback, const meshap_sip_dlg_table_entry_t *dlg)
{
   //_proc_data_t*	data;

   //data	= (_proc_data_t*)callback;
   struct seq_file *m;

   m = (struct seq_file *)callback;


   /*data->p	+= sprintf(data->p,
    *                 "%04x| %3s  | %3s  |     %x     |    %x     | %x  |\r\n",
    *                                 dlg->id,
    *                                 dlg->caller_info->info.extn,
    *                                 dlg->callee_info->info.extn,
    *                                 dlg->caller_dlg.call_state,
    *                                 dlg->callee_dlg.call_state,
    *                                 dlg->flags); */
   seq_printf(m,
              "%04x| %3s  | %3s  |     %x     |    %x     | %x  |\r\n",
              dlg->id,
              dlg->caller_info->info.extn,
              dlg->callee_info->info.extn,
              dlg->caller_dlg.call_state,
              dlg->callee_dlg.call_state,
              dlg->flags);
}


//static int _call_dlg_list_routine(char *page, char **start, off_t off,int count, int *eof, void *proc_data)
static int _call_dlg_list_routine(struct seq_file *m, void *v)
{
   /*char         *p;
    * _proc_data_t	data;
    *
    * if(off != 0) {
    * eof	= 1;
    *      return 0;
    * }
    *
    * start		= page + off;
    * p			= page;
    * data.p		= p;
    *
    * data.p		+= sprintf(p,
    *                                      "------------------------------------------------------\r\n"
    *                                      "DLG ID  |CALLER|CALLEE|CALLER STATE|CALLEE STATE|FLAGS|\r\n"
    *                                      "------------------------------------------------------\r\n");
    *
    * meshap_sip_enumerate_call_dlgs((void*)&data, _call_dlg_enum_routine);
    *
    * p			= data.p;
    *
    * return (p - page);	*/

   _proc_data_t data;

   seq_printf(m,
              "------------------------------------------------------\r\n"
              "DLG ID  |CALLER|CALLEE|CALLER STATE|CALLEE STATE|FLAGS|\r\n"
              "------------------------------------------------------\r\n");
   //meshap_sip_enumerate_call_dlgs((void*)&data, _call_dlg_enum_routine);
   meshap_sip_enumerate_call_dlgs((void *)m, _call_dlg_enum_routine);
}


static int _call_dlg_list_open(struct inode *inode, struct file *file)
{
   return single_open(file, _call_dlg_list_routine, NULL);
}


static const struct file_operations _call_dlg_list_fops =
{
   .owner   = THIS_MODULE,
   .open    = _call_dlg_list_open,
   .read    = seq_read,
   .llseek  = seq_lseek,
   .release = single_release,
};

int meshap_sip_proc_initialize(void)
{
   _meshap_sip_proc_entry = meshap_proc_mkdir("sip");
   _meshap_sip_proc_add_read_entry("sta-list", &_sta_list_fops, NULL);
   _meshap_sip_proc_add_read_entry("call-dlg-list", &_call_dlg_list_fops, NULL);
   return 0;
}
