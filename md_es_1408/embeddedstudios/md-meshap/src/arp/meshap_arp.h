/********************************************************************************
* MeshDynamics
* --------------
* File     : meshap_arp.h
* Comments : ARP Implementation
* Created  : 1/15/2008
* Author   : Abhijit Ayarekar
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  0  |1/15/2008 | Created.                                        |Abhijit |
* -----------------------------------------------------------------------------
********************************************************************************/

#ifndef __MESHAP_ARP_H__
#define __MESHAP_ARP_H__

typedef void * meshap_arp_instance_t;

meshap_arp_instance_t meshap_arp_initialize(al_net_addr_t *ds_mac_address);
void meshap_arp_uninitialize(meshap_arp_instance_t instance);

#define MESHAP_ARP_USE_ADDR_DS_NET_IF          0
#define MESHAP_ARP_USE_ADDR_INCOMING_NET_IF    1

int meshap_arp_add_entry(meshap_arp_instance_t instance,
                         unsigned char         *ip_addr,
                         int                   use_addr);

int meshap_arp_remove_entry(meshap_arp_instance_t instance,
                            unsigned char         *ip_addr);

#define MESHAP_ARP_RETVAL_SUCCESS     0
#define MESHAP_ARP_RETVAL_DISCARD     -1
#define MESHAP_ARP_RETVAL_CONTINUE    -2

int meshap_arp_process_packet(meshap_arp_instance_t instance,
                              al_net_if_t           *net_if,
                              al_packet_t           *packet_in,
                              al_packet_t           **packet_out);

#endif
