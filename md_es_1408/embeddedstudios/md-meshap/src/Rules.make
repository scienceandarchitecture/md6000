
###******************************************************************************
 # MeshDynamics 
 # -------------- 
 # File     : Rules.make
 # Comments : Global definitions for Meshap
 # Created  : 11/17/2004
 # Author   : Sriram Dayanandan
 # Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 # File Revision History 
 # -----------------------------------------------------------------------------
 # | No  |Date      |  Comment                                        | Author |
 # -----------------------------------------------------------------------------
 # |  1  |08/22/2008 | Changes for SIP.                               |Abhijit |
 # -----------------------------------------------------------------------------
 # |  0  |11/17/2004 | Created.                                       | Sriram |
 # -----------------------------------------------------------------------------
 #*******************************************************************************/

include $(TOPDIR)/arch.inc

ifndef KERNEL_PATH
	KERNEL_PATH :=/usr/src/linux
endif

ifndef CC
	CC :=/usr/local/bin/arm-linux-gcc
endif

ifndef LD
	LD := /usr/local/bin/arm-linux-ld
endif

CFLAGS=-O3 -D__KERNEL__ -DMODULE -Wall -c $(EXTRA_CFLAGS)

include $(KERNEL_PATH)/.config

INCLUDES=-I$(KERNEL_PATH)/include -I$(TOPDIR)/include -I$(TOPDIR)/aes -I$(TOPDIR)/meshap/include -I$(TOPDIR)/meshap -I$(TOPDIR)/meshap/access_point/include -I$(TOPDIR)/meshap/md_mesh/include -I$(TOPDIR)/meshap/power-test -I$(TOPDIR)/meshap/sip/include 

ifndef CONFIG_FRAME_POINTER
	CFLAGS += -fomit-frame-pointer
endif

include $(TOPDIR)/Rules.make

