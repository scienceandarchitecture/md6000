/********************************************************************************
* MeshDynamics
* --------------
* File     : acl_conf.c
* Comments : ACL Parser Implementation
* Created  : 3/13/2006
* Author   : Bindu Parakala
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  3  |8/26/2008 | Used common configuration parser routines       | Sriram |
* -----------------------------------------------------------------------------
* |  2  |11/14/2006| Changes for heap debug support                  | Sriram |
* -----------------------------------------------------------------------------
* |  1  |3/15/2006 | Added .11e enabled & category for ACL           | Bindu  |
* |     |          | Removed vlan_name                               |        |
* -----------------------------------------------------------------------------
* |  0  |3/13/2006 | Created                                         | Bindu  |
* -----------------------------------------------------------------------------
********************************************************************************/

#ifdef __KERNEL__
#include <linux/string.h>
#include <linux/kernel.h>
#else
#include <stdio.h>
#include <strings.h>
#include <string.h>
#endif
#include "al.h"
#include "al_context.h"
#include "al_impl_context.h"
#include "al_802_11.h"
#include "acl_conf.h"
#include "conf_common.h"

#define _MAC_ADDRESS_LENGTH                6

#define _MAX_TOKEN_SIZE                    256

#define _SUCCESS                           0
#define _ERR                               -1

#define _GET_TOKEN_SUCCESS                 0
#define _GET_TOKEN_EOF                     -1
#define _GET_TOKEN_UNKNOWN                 -2
#define _GET_TOKEN_ERROR                   -3

#define _ACL_CONF_TOKEN_ACL_INFO           1
#define _ACL_CONF_TOKEN_TAG                2
#define _ACL_CONF_TOKEN_ALLOW_FLAG         3
#define _ACL_CONF_TOKEN_EQUALS             4
#define _ACL_CONF_TOKEN_COMMA              5
#define _ACL_CONF_TOKEN_NUMBER             6
#define _ACL_CONF_TOKEN_ALPHA_NUMERIC      7
#define _ACL_CONF_TOKEN_STRING             8
#define _ACL_CONF_TOKEN_HASH               9
#define _ACL_CONF_TOKEN_OPEN_BRACE         10
#define _ACL_CONF_TOKEN_CLOSE_BRACE        11
#define _ACL_CONF_TOKEN_OPEN_BRACK         12
#define _ACL_CONF_TOKEN_CLOSE_BRACK        13
#define _ACL_CONF_TOKEN_COLON              14
#define _ACL_CONF_TOKEN_VLAN_UNTAGGED      15
#define _ACL_CONF_TOKEN_DOT11E_ENABLED     16
#define _ACL_CONF_TOKEN_DOT11E_CATEGORY    17

/**
 * The various states or modes of the parser.
 */

#define _PARSE_MODE_NONE             -1
#define _PARSE_MODE_ACL_INFO         1
#define _PARSE_MODE_ACL_INFO_ITEM    2


struct _token_info
{
   int        token_code;
   const char *token;
   char       token_data[_MAX_TOKEN_SIZE];
};

typedef struct _token_info   _token_info_t;

struct _tokenizer_state
{
   char       token_data[_MAX_TOKEN_SIZE];
   const char *current_pos;
   char       *error;
   int        current_line_number;
   int        current_col_number;
};

typedef struct _tokenizer_state   _tokenizer_state_t;


static const _token_info_t _tokens[] =
{
   { _ACL_CONF_TOKEN_ACL_INFO,        "acl_info",        "" },
   { _ACL_CONF_TOKEN_TAG,             "tag",             "" },
   { _ACL_CONF_TOKEN_ALLOW_FLAG,      "allow",           "" },
   { _ACL_CONF_TOKEN_DOT11E_ENABLED,  "dot11e_enabled",  "" },
   { _ACL_CONF_TOKEN_DOT11E_CATEGORY, "dot11e_category", "" },
   { _ACL_CONF_TOKEN_EQUALS,          "=",               "" },
   { _ACL_CONF_TOKEN_COMMA,           ",",               "" },
   { _ACL_CONF_TOKEN_OPEN_BRACE,      "{",               "" },
   { _ACL_CONF_TOKEN_CLOSE_BRACE,     "}",               "" },
   { _ACL_CONF_TOKEN_OPEN_BRACK,      "(",               "" },
   { _ACL_CONF_TOKEN_CLOSE_BRACK,     ")",               "" },
   { _ACL_CONF_TOKEN_COLON,           ":",               "" },
   { _ACL_CONF_TOKEN_VLAN_UNTAGGED,   "untagged",        "" }
};

#define _MAP_TOKEN_CODE_TO_PARSE_MODE(tc, pm) \
case tc:                                      \
   conf_out->parse_mode = pm; break

#define _BEGIN_MAP_BLOCK(tc)    switch (tc) {
#define _END_MAP_BLOCK()        }

#define _MAP_TOKEN_CODES(tc)                                                      \
   _BEGIN_MAP_BLOCK(tc)                                                           \
   _MAP_TOKEN_CODE_TO_PARSE_MODE(_ACL_CONF_TOKEN_ACL_INFO, _PARSE_MODE_ACL_INFO); \
   _END_MAP_BLOCK()


static int _is_token_integer(char *data, int length)
{
   char *ptr;
   int  i;
   //al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

#define _isdigit(c)    (c >= 48 && c <= 48 + 9)

   ptr = data;
   for (i = 0; i < length; i++)
   {
      char c = *ptr;
      if (!(_isdigit(c)))
      {
		 al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : _isdigit() is failed : %s<%d>\n", __func__, __LINE__);
         return _ERR;
      }
   }
   //al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);

   return _SUCCESS;

#undef _isdigit
}


static int _is_token_string(char *data, int length)
{
   char *ptr;
   int  i;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

#define _ishexalpha(c)    ((c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z'))
#define _isdigit(c)       (c >= 48 && c <= 48 + 9)

   ptr = data;
   for (i = 0; i < length; i++)
   {
      char c = *ptr;
      if (!(_isdigit(c)) && !(_ishexalpha(c)))
      {
		 al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : value is not proper : %s<%d>\n", __func__, __LINE__);
         return _ERR;
      }
   }

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return _SUCCESS;

#undef _isdigit
#undef _ishexalpha
}


static int _is_token_alpha_numeric(char *data, int length)
{
   char *ptr;
   int  i;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

#define _ishexalpha(c)    ((c >= 'A' && c <= 'F') || (c >= 'a' && c <= 'f'))
#define _isdigit(c)       (c >= 48 && c <= 48 + 9)

   ptr = data;
   for (i = 0; i < length; i++)
   {
      char c = *ptr;
      if (!(_isdigit(c)) && !(_ishexalpha(c)))
      {
		 al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : value is not proper : %s<%d>\n", __func__, __LINE__);
         return _ERR;
      }
   }
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);

   return _SUCCESS;

#undef _isdigit
#undef _ishexalpha
}


static int _get_token(_tokenizer_state_t *state, _token_info_t *token_out)
{
   const char    *p;
   const char    *current_marker;
   unsigned char leading_white_space;
   int           token_data_pos;
   int           i;

   p = state->current_pos;
   current_marker      = p;
   leading_white_space = 1;
   token_data_pos      = 0;

   while (*p != 0)
   {
      switch (*p)
      {
      case ' ':
      case '\t':
      case '\r':
      case '\n':
         if (*p == '\n')
         {
            ++state->current_line_number;
         }
         if (leading_white_space)
         {
            ++p;
            ++state->current_col_number;
            continue;
         }
         else
         {
            goto _token_found;
         }
         break;

      case '=':
      case '(':
      case ')':
      case ',':
      case ':':
         leading_white_space = 0;
         if (token_data_pos == 0)
         {
            state->token_data[token_data_pos++] = *p++;
            goto _token_found;
         }
         else
         {
            goto _token_found;
         }
         break;

      default:
         leading_white_space = 0;
         state->token_data[token_data_pos++] = *p++;
      }

      ++state->current_col_number;
   }

   return _GET_TOKEN_EOF;

_token_found:

   state->token_data[token_data_pos++] = 0;

   for (i = 0; i < sizeof(_tokens) / sizeof(*_tokens); i++)
   {
      if (!(strcmp(_tokens[i].token, state->token_data)))
      {
         memcpy(token_out, &_tokens[i], sizeof(_token_info_t));
         state->current_pos = p;
         return _GET_TOKEN_SUCCESS;
      }
   }

   memcpy((char *)token_out->token_data, state->token_data, token_data_pos - 1);
   if (_is_token_integer(token_out->token_data, token_data_pos - 1) == _SUCCESS)
   {
      token_out->token_code = _ACL_CONF_TOKEN_NUMBER;
   }
   else if (_is_token_alpha_numeric(token_out->token_data, token_data_pos - 1) == _SUCCESS)
   {
      token_out->token_code = _ACL_CONF_TOKEN_ALPHA_NUMERIC;
   }
   else if (_is_token_string(token_out->token_data, token_data_pos - 1) == _SUCCESS)
   {
      token_out->token_code = _ACL_CONF_TOKEN_STRING;
   }
   else
   {
	  al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : Token Error %s<%d>\n", __func__, __LINE__);
      return _GET_TOKEN_ERROR;
   }

   state->current_pos = p;

   return _GET_TOKEN_SUCCESS;
}


static int _parse_integer(_tokenizer_state_t *state, _token_info_t *token, int *value_out)
{
   int value;
   //al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   if (_get_token(state, token) != _GET_TOKEN_SUCCESS)
   {
	  al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : Token Error : %s<%d>\n", __func__, __LINE__);
      return _GET_TOKEN_ERROR;
   }

   if (token->token_code == _ACL_CONF_TOKEN_VLAN_UNTAGGED)
   {
      *value_out = ACL_CONF_VLAN_UNTAGGED;
   }
   else if (token->token_code == _ACL_CONF_TOKEN_NUMBER)
   {
      if (al_conf_atoi(token->token_data, &value) != 0)
      {
	     al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : al_conf_atoi() failed : %s<%d>\n", __func__, __LINE__);
         return _GET_TOKEN_ERROR;
      }
      else
      {
         *value_out = value;
      }
   }
   else
   {
	  al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : Token code invalid : %s<%d>\n", __func__, __LINE__);
      return _GET_TOKEN_ERROR;
   }

   //al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return _GET_TOKEN_SUCCESS;
}


static int _parse_mac_address(_tokenizer_state_t *state, _token_info_t *token, unsigned char *value_out)
{
   int j;
   int val;
   int ret;
   //al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

#define _PARSE_INTERMEDIATE_TOKEN(tok, tok_code)                      \
   do {                                                               \
      ret = _get_token(state, token);                                 \
      if ((ret == _GET_TOKEN_UNKNOWN) || (ret == _GET_TOKEN_ERROR)) { \
         goto label_ret;                                              \
      }                                                               \
      if (token->token_code != tok_code) {                            \
         goto label_ret;                                              \
      }                                                               \
   } while (0)

   for (j = 0; j < _MAC_ADDRESS_LENGTH; j++)
   {
      if (_get_token(state, token) != _GET_TOKEN_SUCCESS)
      {
		 al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : Token error : %s<%d>\n", __func__, __LINE__);
         return _GET_TOKEN_ERROR;
      }

      if ((token->token_code != _ACL_CONF_TOKEN_ALPHA_NUMERIC) && (token->token_code != _ACL_CONF_TOKEN_NUMBER))
      {
		 al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : Token error : %d : %s<%d>\n",token->token_code, __func__, __LINE__);
         return _GET_TOKEN_ERROR;
      }

      if (al_conf_hex_atoi(token->token_data, &val) == _ERR)
      {
		 al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : Token error : %s<%d>\n", __func__, __LINE__);
         return _GET_TOKEN_ERROR;
      }

      value_out[j] = val;

      if (j != 5)
      {
         _PARSE_INTERMEDIATE_TOKEN(':', _ACL_CONF_TOKEN_COLON);
      }
   }

   //al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return _GET_TOKEN_SUCCESS;

label_ret:
   //al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return _GET_TOKEN_ERROR;

#undef _PARSE_INTERMEDIATE_TOKEN
}


int _parse_acl(const char *buffer, char *error_out, acl_conf_t *conf_out)
{
   _tokenizer_state_t state;
   _token_info_t      token;
   int                value;
   int                i;

   int           ret;
   unsigned char sta_mac[6];
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

#define _PARSE_INTERMEDIATE_TOKEN(tok, tok_code)                      \
   do {                                                               \
      ret = _get_token(&state, &token);                               \
      if ((ret == _GET_TOKEN_UNKNOWN) || (ret == _GET_TOKEN_ERROR)) { \
         goto label_ret;                                              \
      }                                                               \
      if (token.token_code != tok_code) {                             \
         goto label_ret;                                              \
      }                                                               \
   } while (0)


   memset(&state, 0, sizeof(_tokenizer_state_t));
   memset(&token, 0, sizeof(_token_info_t));
   memset(conf_out, 0, sizeof(acl_conf_t));

   state.current_pos         = buffer;
   state.error               = error_out;
   state.current_line_number = 1;
   state.current_col_number  = 1;

   conf_out->parse_mode = _PARSE_MODE_NONE;

   while (1)
   {
      switch (conf_out->parse_mode)
      {
      case _PARSE_MODE_NONE:
         ret = _get_token(&state, &token);

         if (ret == _GET_TOKEN_EOF)
         {
            al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : Reached EOF (ret : %d) : %s<%d>\n", ret, __func__,__LINE__);
            return 0;
         }
         else if ((ret == _GET_TOKEN_ERROR) || (ret == _GET_TOKEN_UNKNOWN))
         {
            al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : Invalid or unknown token (ret : %d) : %s<%d>\n", ret, __func__,__LINE__);
            goto label_ret;
         }

         _MAP_TOKEN_CODES(token.token_code);

         break;

      case _ACL_CONF_TOKEN_ACL_INFO:
         _PARSE_INTERMEDIATE_TOKEN('(', _ACL_CONF_TOKEN_OPEN_BRACK);

         /* get total count of acl's */
         if (_parse_integer(&state, &token, &value) != _GET_TOKEN_SUCCESS)
         {
            al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : _parse_integer() failed : %s<%d>\n",__func__,__LINE__);
            goto label_ret;
         }

         memcpy(&conf_out->count, &value, sizeof(value));
         if (conf_out->count > 0)
         {
            conf_out->info = (acl_conf_info_t *)al_heap_alloc(sizeof(acl_conf_info_t) * conf_out->count AL_HEAP_DEBUG_PARAM);
            memset(conf_out->info, 0, sizeof(acl_conf_info_t) * conf_out->count);
         }

         _PARSE_INTERMEDIATE_TOKEN(')', _ACL_CONF_TOKEN_CLOSE_BRACK);
         _PARSE_INTERMEDIATE_TOKEN('{', _ACL_CONF_TOKEN_OPEN_BRACE);

         conf_out->parse_mode = _PARSE_MODE_ACL_INFO_ITEM;

         break;

      case _PARSE_MODE_ACL_INFO_ITEM:

         for (i = 0; i < conf_out->count; i++)
         {
            /** First get the STA MAC Address */

            if (_parse_mac_address(&state, &token, sta_mac) != _GET_TOKEN_SUCCESS)
            {
               al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : _parse_mac_address() failed : %s<%d>\n",__func__,__LINE__);
               goto label_ret;
            }

            memcpy(&conf_out->info[i].sta_mac, &sta_mac, _MAC_ADDRESS_LENGTH);

            _PARSE_INTERMEDIATE_TOKEN('{', _ACL_CONF_TOKEN_OPEN_BRACE);

            while (1)
            {
               ret = _get_token(&state, &token);
               if (ret == -1)
               {
                  al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : _get_token() failed : %s<%d>\n",__func__,__LINE__);
                  goto label_ret;
               }

               switch (token.token_code)
               {
               case _ACL_CONF_TOKEN_TAG:
                  _PARSE_INTERMEDIATE_TOKEN('=', _ACL_CONF_TOKEN_EQUALS);

                  if (_parse_integer(&state, &token, &value) != _GET_TOKEN_SUCCESS)
                  {
                     al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : _parse_integer() failed : %s<%d>\n",__func__,__LINE__);
                     goto label_ret;
                  }

                  conf_out->info[i].vlan_tag = value;

                  break;

               case _ACL_CONF_TOKEN_ALLOW_FLAG:
                  _PARSE_INTERMEDIATE_TOKEN('=', _ACL_CONF_TOKEN_EQUALS);

                  if (_parse_integer(&state, &token, &value) != _GET_TOKEN_SUCCESS)
                  {
                     al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : _parse_integer() failed : %s<%d>\n",__func__,__LINE__);
                     goto label_ret;
                  }

                  conf_out->info[i].allow = value;

                  break;

               case _ACL_CONF_TOKEN_DOT11E_ENABLED:
                  _PARSE_INTERMEDIATE_TOKEN('=', _ACL_CONF_TOKEN_EQUALS);

                  if (_parse_integer(&state, &token, &value) != _GET_TOKEN_SUCCESS)
                  {
                     al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : _parse_integer() failed : %s<%d>\n",__func__,__LINE__);
                     goto label_ret;
                  }

                  conf_out->info[i].dot11e_enabled = value;

                  break;

               case _ACL_CONF_TOKEN_DOT11E_CATEGORY:
                  _PARSE_INTERMEDIATE_TOKEN('=', _ACL_CONF_TOKEN_EQUALS);

                  if (_parse_integer(&state, &token, &value) != _GET_TOKEN_SUCCESS)
                  {
                     al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : _parse_integer() failed : %s<%d>\n",__func__,__LINE__);
                     goto label_ret;
                  }

                  conf_out->info[i].dot11e_category = value;

                  break;
               }                                  //switch acl_conf_info

               /** Now we expect either a COMMA or a CLOSE BRACE */
               ret = _get_token(&state, &token);
               if (ret == -1)
               {
                  al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : _get_token() failed : %s<%d>\n",__func__,__LINE__);
                  goto label_ret;
               }

               if (token.token_code == _ACL_CONF_TOKEN_COMMA)
               {
                  continue;
               }
               else if (token.token_code == _ACL_CONF_TOKEN_CLOSE_BRACE)
               {
                  break;
               }
               else
               {
                  al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : Invalid token : %d : %s<%d>\n", token.token_code, __func__,__LINE__);
                  goto label_ret;
               }
            }             //while acl_conf_info
         }                //for count


         _PARSE_INTERMEDIATE_TOKEN('}', _ACL_CONF_TOKEN_CLOSE_BRACE);
         conf_out->parse_mode = _PARSE_MODE_NONE;
      }
   }

label_ret:
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return -1;

#undef _PARSE_INTERMEDIATE_TOKEN
}


int acl_conf_parse(AL_CONTEXT_PARAM_DECL const char *acl_conf_string)
{
   acl_conf_t *data;
   int        ret;
   char       *buffer;
   char       err_out;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   data = (acl_conf_t *)al_heap_alloc(AL_CONTEXT sizeof(acl_conf_t)AL_HEAP_DEBUG_PARAM);
   memset(data, 0, sizeof(acl_conf_t));

   buffer = (char *)al_heap_alloc(AL_CONTEXT strlen(acl_conf_string) + 1  AL_HEAP_DEBUG_PARAM);
   strcpy(buffer, acl_conf_string);

   ret = _parse_acl(buffer, &err_out, data);


   if (ret != 0)
   {
      al_heap_free(AL_CONTEXT buffer);
      if (data->info != NULL)
      {
         al_heap_free(AL_CONTEXT data->info);
      }
      al_heap_free(AL_CONTEXT data);
	  al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : _parse_acl() failed : %s<%d>\n", __func__, __LINE__);
      return 0;
   }

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return (int)data;
}


void acl_conf_close(AL_CONTEXT_PARAM_DECL int acl_conf_handle)
{
   acl_conf_t *data;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   data = (acl_conf_t *)acl_conf_handle;

   if (data == NULL)
   {
	  al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : DATA is NULL : %s<%d>\n", __func__, __LINE__);
      return;
   }

   if (data->info != NULL)
   {
	  al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : Info is NULL : %s<%d>\n", __func__, __LINE__);
      al_heap_free(AL_CONTEXT data->info);
   }

   al_heap_free(AL_CONTEXT data);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
}


int acl_conf_get_info_count(AL_CONTEXT_PARAM_DECL int acl_conf_handle)
{
   acl_conf_t *data;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   data = (acl_conf_t *)acl_conf_handle;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return data->count;
}


int acl_conf_get_info(AL_CONTEXT_PARAM_DECL int acl_conf_handle, int index, acl_conf_info_t *info)
{
   acl_conf_t *data;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   data = (acl_conf_t *)acl_conf_handle;

   if (index >= data->count)
   {
	  al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : Index(val : %d) is not proper : %s<%d>\n", index,__func__, __LINE__);
      return -1;
   }

   memcpy(info, &data->info[index], sizeof(acl_conf_info_t));
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);

   return 0;
}


#ifndef _MESHAP_ACL_CONF_NO_SET_AND_PUT

int acl_conf_set_info_count(AL_CONTEXT_PARAM_DECL int acl_conf_handle, int count)
{
   acl_conf_t *data;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   data = (acl_conf_t *)acl_conf_handle;

   if (data->count > 0)
   {
      al_heap_free(AL_CONTEXT data->info);
      data->info = NULL;
   }

   data->count = count;

   if (data->count > 0)
   {
      data->info = (acl_conf_info_t *)al_heap_alloc(AL_CONTEXT sizeof(acl_conf_info_t) * data->count AL_HEAP_DEBUG_PARAM);
      memset(data->info, 0, sizeof(acl_conf_info_t) * data->count);
   }

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return 0;
}


int acl_conf_set_info(AL_CONTEXT_PARAM_DECL int acl_conf_handle, int index, acl_conf_info_t *info)
{
   acl_conf_t *data;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   data = (acl_conf_t *)acl_conf_handle;

   if (index >= data->count)
   {
	  al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : Index (val : %d) is not proper : %s<%d>\n", index,  __func__, __LINE__);
      return -1;
   }

   memcpy(&data->info[index], info, sizeof(acl_conf_info_t));
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);

   return 0;
}


int acl_conf_create_config_string_from_data(AL_CONTEXT_PARAM_DECL int acl_conf_handle, int *config_string_length, char **acl_conf_string)
{
   acl_conf_t      *data;
   acl_conf_info_t *info;
   char            *p;
   char            buffer[1024];
   char            *buffer_out;
   int             ret;
   int             i;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   data = (acl_conf_t *)acl_conf_handle;
   if (data == NULL)
   {
	  al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : DATA is NULL : %s<%d>\n", __func__, __LINE__);
      return -1;
   }

   buffer_out = (char *)al_heap_alloc(AL_CONTEXT 64 * 1024 AL_HEAP_DEBUG_PARAM);
   memset(buffer_out, 0, 10240);

   memset(buffer, 0, 1024);

   p = buffer_out;

   memset(buffer, 0, 1024);
   ret = sprintf(buffer, "acl_info (%d) {\n", data->count);
   memcpy(p, buffer, strlen(buffer));
   p += ret;
   *p = '\n';
   p += 1;


   for (i = 0; i < data->count; i++)
   {
      info = &data->info[i];

      memset(buffer, 0, 1024);
      ret = sprintf(buffer, "\t%02x:%02x:%02x:%02x:%02x:%02x {\n",
                    data->info[i].sta_mac[0], data->info[i].sta_mac[1],
                    data->info[i].sta_mac[2], data->info[i].sta_mac[3],
                    data->info[i].sta_mac[4], data->info[i].sta_mac[5]
                    );

      memcpy(p, buffer, strlen(buffer));
      p += ret;

      /** If		tag = UNTAGGED & vlan = default so dot11e_enabled & dot11e_category need to be written.
       *  else	dot11e_enabled & dot11e_category need need not be parsed.
       */

      if (data->info[i].vlan_tag == ACL_CONF_VLAN_UNTAGGED)
      {
         memset(buffer, 0, 1024);
         ret = sprintf(buffer, "\t\ttag=untagged,\n");
         memcpy(p, buffer, strlen(buffer));
         p += ret;

         memset(buffer, 0, 1024);
         ret = sprintf(buffer, "\t\tallow=%d,\n", data->info[i].allow);
         memcpy(p, buffer, strlen(buffer));
         p += ret;

         memset(buffer, 0, 1024);
         ret = sprintf(buffer, "\t\tdot11e_enabled=%d,\n", data->info[i].dot11e_enabled);
         memcpy(p, buffer, strlen(buffer));
         p += ret;

         memset(buffer, 0, 1024);
         ret = sprintf(buffer, "\t\tdot11e_category=%d\n", data->info[i].dot11e_category);
         memcpy(p, buffer, strlen(buffer));
         p += ret;
      }
      else
      {
         memset(buffer, 0, 1024);
         ret = sprintf(buffer, "\t\ttag=%d,\n", data->info[i].vlan_tag);
         memcpy(p, buffer, strlen(buffer));
         p += ret;

         memset(buffer, 0, 1024);
         ret = sprintf(buffer, "\t\tallow=%d\n", data->info[i].allow);
         memcpy(p, buffer, strlen(buffer));
         p += ret;
      }

      memset(buffer, 0, 1024);
      ret = sprintf(buffer, "\t}\n");
      memcpy(p, buffer, strlen(buffer));
      p += ret;
   }

   memset(buffer, 0, 1024);
   ret = sprintf(buffer, "}\n");
   memcpy(p, buffer, strlen(buffer));
   p += ret;
   *p = '\n';
   p++;

   *p = '\0';

   ret = p - buffer_out;
   *acl_conf_string = (char *)al_heap_alloc(AL_CONTEXT ret AL_HEAP_DEBUG_PARAM);
   memset(*acl_conf_string, 0, ret);

   strcpy(*acl_conf_string, buffer_out);
   *config_string_length = ret;

   al_heap_free(AL_CONTEXT buffer_out);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);

   return 0;
}


int acl_conf_put(AL_CONTEXT_PARAM_DECL int acl_conf_handle)
{
   char *config_string;
   int  config_string_length;
   int  ret, handle;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   if (acl_conf_handle <= 0)
   {
	  al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : Invalid acl_conf_handle(val : %d) : %s<%d>\n", acl_conf_handle, __func__, __LINE__);
      return -1;
   }

   ret = acl_conf_create_config_string_from_data(AL_CONTEXT acl_conf_handle, &config_string_length, &config_string);

   if (ret != 0)
   {
	  al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : acl_conf_create_config_string_from_data() failed : %s<%d>\n",__func__, __LINE__);
      return ret;
   }

   handle = al_open_file(AL_CONTEXT AL_FILE_ID_ACL_CONFIG, AL_OPEN_FILE_MODE_WRITE);
   al_write_file(AL_CONTEXT handle, config_string, config_string_length);
   al_close_file(AL_CONTEXT handle);

   al_heap_free(AL_CONTEXT config_string);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);

   return 0;
}
#endif /* _MESHAP_ACL_CONF_NO_SET_AND_PUT */
