/********************************************************************************
* MeshDynamics
* --------------
* File     : v_if_map_info.c
* Comments : virtual if info map file.
* Created  : 9/11/2006
* Author   : Prachiti Gaikwad
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author  |
* -----------------------------------------------------------------------------
* |  1  |8/26/2008 | Used common configuration parser routines       | Sriram |
* -----------------------------------------------------------------------------
* |  0  |9/11/2006 | Created                                         | Prachiti|
* -----------------------------------------------------------------------------
********************************************************************************/

#include <linux/string.h>
#include <linux/kernel.h>
#include "al.h"
#include "al_context.h"
#include "al_impl_context.h"
#include "v_if_info_conf.h"
#include "conf_common.h"

#define _MAX_TOKEN_SIZE                            256

#define _SUCCESS                                   0
#define _ERR                                       -1

#define _GET_TOKEN_SUCCESS                         0
#define _GET_TOKEN_EOF                             -1
#define _GET_TOKEN_UNKNOWN                         -2
#define _GET_TOKEN_ERROR                           -3

#define _V_IF_INFO_CONF_TOKEN_OPEN_BRACE           1
#define _V_IF_INFO_CONF_TOKEN_CLOSE_BRACE          2
#define _V_IF_INFO_CONF_TOKEN_OPEN_BRACKET         3
#define _V_IF_INFO_CONF_TOKEN_CLOSE_BRACKET        4
#define _V_IF_INFO_CONF_TOKEN_EQUALS               5
#define _V_IF_INFO_CONF_TOKEN_COMMA                6
#define _V_IF_INFO_CONF_TOKEN_IDENT                7
#define _V_IF_INFO_CONF_TOKEN_VIRT_IF_INFO         8
#define _V_IF_INFO_CONF_TOKEN_MAP_RX               9
#define _V_IF_INFO_CONF_TOKEN_MAP_TX               10


#define _V_IF_INFO_CONF_PARSE_MODE_NONE            -1
#define _V_IF_INFO_CONF_PARSE_MODE_IF_INFO         0
#define _V_IF_INFO_CONF_PARSE_MODE_IF_INFO_ITEM    1
#define _V_IF_INFO_CONF_PARSE_MODE_IF_INFO_TAIL    2


struct _v_if_info_conf_data
{
   char               *parser_buffer;
   char               *pos;
   char               current_token[_MAX_TOKEN_SIZE + 1];
   char               *curent_token_pos;
   int                current_token_code;
   int                line_number;
   int                column_number;
   int                parse_mode;
   int                v_if_count;
   _virtual_if_info_t *v_if_info;
};


typedef struct _v_if_info_conf_data   _v_if_info_conf_data_t;


static struct
{
   const char *token;
   int        code;
}
_token_info[] =
{
   { "virt_if_info", _V_IF_INFO_CONF_TOKEN_VIRT_IF_INFO  },
   { "map_rx",       _V_IF_INFO_CONF_TOKEN_MAP_RX        },
   { "map_tx",       _V_IF_INFO_CONF_TOKEN_MAP_TX        },
   { "{",            _V_IF_INFO_CONF_TOKEN_OPEN_BRACE    },
   { "}",            _V_IF_INFO_CONF_TOKEN_CLOSE_BRACE   },
   { "(",            _V_IF_INFO_CONF_TOKEN_OPEN_BRACKET  },
   { ")",            _V_IF_INFO_CONF_TOKEN_CLOSE_BRACKET },
   { "=",            _V_IF_INFO_CONF_TOKEN_EQUALS        },
   { ",",            _V_IF_INFO_CONF_TOKEN_COMMA         }
};

#define _MAP_TOKEN_CODE_TO_PARSE_MODE(tc, pm) \
case (tc):                                    \
   conf_data->parse_mode = (pm);              \
   break

#define _BEGIN_MAP_BLOCK(tc) \
   switch (tc) {
#define _END_MAP_BLOCK() \
   }

#define _MAP_TOKEN_CODES(tc)                                                                              \
   _BEGIN_MAP_BLOCK(tc)                                                                                   \
   _MAP_TOKEN_CODE_TO_PARSE_MODE(_V_IF_INFO_CONF_TOKEN_VIRT_IF_INFO, _V_IF_INFO_CONF_PARSE_MODE_IF_INFO); \
default:                                                                                                  \
   al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "virt_if_conf_conf.c(%d,%d): ignoring token %s\n",          \
                conf_data->line_number,                                                                   \
                conf_data->column_number,                                                                 \
                conf_data->current_token);                                                                \
   _END_MAP_BLOCK()

static int _parse(AL_CONTEXT_PARAM_DECL _v_if_info_conf_data_t *conf_data);
static int _get_token(AL_CONTEXT_PARAM_DECL _v_if_info_conf_data_t *conf_data);

static int _parse(AL_CONTEXT_PARAM_DECL _v_if_info_conf_data_t *conf_data)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s: %d\n",__func__,__LINE__);
   int ret;
   int status;
   int i;

   conf_data->parse_mode = _V_IF_INFO_CONF_PARSE_MODE_NONE;
   status = 0;


#define _PARSE_INTERMEDIATE_TOKEN(tok, tok_code)                                                                      \
   do {                                                                                                               \
      ret = _get_token(AL_CONTEXT conf_data);                                                                         \
      if (ret == -1) {                                                                                                \
         al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "al_conf.c(%d,%d): unexpected eof file while looking for '%c'\n", \
                      conf_data->line_number,                                                                         \
                      conf_data->column_number,                                                                       \
                      tok);                                                                                           \
         status = -1;                                                                                                 \
         goto label_ret;                                                                                              \
      }                                                                                                               \
      if (conf_data->current_token_code != tok_code) {                                                                \
         al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "al_conf.c(%d,%d): expecting token %c but found %s\n",            \
                      conf_data->line_number,                                                                         \
                      conf_data->column_number,                                                                       \
                      tok,                                                                                            \
                      conf_data->current_token);                                                                      \
         status = -1;                                                                                                 \
         goto label_ret;                                                                                              \
      }                                                                                                               \
   } while (0)



   while (1)
   {
      switch (conf_data->parse_mode)
      {
      case _V_IF_INFO_CONF_PARSE_MODE_NONE:
         ret = _get_token(AL_CONTEXT conf_data);
         if (ret == -1)
         {
      	    al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : Get token failed  %s : %d\n",__func__,__LINE__);
            goto label_ret;
         }
         _MAP_TOKEN_CODES(conf_data->current_token_code);
         break;

      /* virtual if info */
      case _V_IF_INFO_CONF_PARSE_MODE_IF_INFO:
         _PARSE_INTERMEDIATE_TOKEN('(', _V_IF_INFO_CONF_TOKEN_OPEN_BRACKET);
         ret = _get_token(AL_CONTEXT conf_data);
         if (ret == -1)
         {
            al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "virtual_if_info_conf.c(%d,%d): unexpected eof file while looking for virtual if count\n",
                         conf_data->line_number,
                         conf_data->column_number);
            status = -1;
            goto label_ret;
         }
         ret = al_conf_atoi(conf_data->current_token, &conf_data->v_if_count);
         if (ret == -1)
         {
            al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "virtual_if_info_conf.c(%d,%d): token %s was found instead of an integer\n",
                         conf_data->line_number,
                         conf_data->column_number,
                         conf_data->current_token);
            status = -1;
            goto label_ret;
         }
         conf_data->v_if_info = (_virtual_if_info_t *)al_heap_alloc(AL_CONTEXT sizeof(_virtual_if_info_t) * conf_data->v_if_count AL_HEAP_DEBUG_PARAM);
         memset(conf_data->v_if_info, 0, sizeof(_virtual_if_info_t) * conf_data->v_if_count);
         _PARSE_INTERMEDIATE_TOKEN(')', _V_IF_INFO_CONF_TOKEN_CLOSE_BRACKET);
         _PARSE_INTERMEDIATE_TOKEN('{', _V_IF_INFO_CONF_TOKEN_OPEN_BRACE);

         conf_data->parse_mode = _V_IF_INFO_CONF_PARSE_MODE_IF_INFO_ITEM;
         break;

      case _V_IF_INFO_CONF_PARSE_MODE_IF_INFO_ITEM:

         for (i = 0; i < conf_data->v_if_count; i++)
         {
            /* read if name */
            ret = _get_token(AL_CONTEXT conf_data);
            if (ret == -1)
            {
               al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "v_if_info.c(%d,%d): unexpected eof file while looking for virtual if name\n",
                            conf_data->line_number,
                            conf_data->column_number);
               status = -1;
               goto label_ret;
            }
            strcpy((char *)conf_data->v_if_info[i].name, conf_data->current_token);

            _PARSE_INTERMEDIATE_TOKEN('{', _V_IF_INFO_CONF_TOKEN_OPEN_BRACE);

            while (1)
            {
               ret = _get_token(AL_CONTEXT conf_data);
               if (ret == -1)
               {
                  al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "v_if_info.c(%d,%d): unexpected eof file while looking for virtual if info item\n",
                               conf_data->line_number,
                               conf_data->column_number);
                  status = -1;
                  goto label_ret;
               }
               switch (conf_data->current_token_code)
               {
               case _V_IF_INFO_CONF_TOKEN_MAP_RX:

                  _PARSE_INTERMEDIATE_TOKEN('=', _V_IF_INFO_CONF_TOKEN_EQUALS);
                  ret = _get_token(AL_CONTEXT conf_data);
                  if (ret == -1)
                  {
                     al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "virtual_if_conf.c(%d,%d): unexpected eof file while looking for map rx\n",
                                  conf_data->line_number,
                                  conf_data->column_number);
                     status = -1;
                     goto label_ret;
                  }
                  strcpy((char *)conf_data->v_if_info[i].map_rx, conf_data->current_token);
                  break;

               case _V_IF_INFO_CONF_TOKEN_MAP_TX:
                  _PARSE_INTERMEDIATE_TOKEN('=', _V_IF_INFO_CONF_TOKEN_EQUALS);
                  ret = _get_token(AL_CONTEXT conf_data);
                  if (ret == -1)
                  {
                     al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "virtual_if_conf.c(%d,%d): unexpected eof file while looking for map tx\n",
                                  conf_data->line_number,
                                  conf_data->column_number);
                     status = -1;
                     goto label_ret;
                  }
                  strcpy((char *)conf_data->v_if_info[i].map_tx, conf_data->current_token);
                  break;
               }
               ret = _get_token(AL_CONTEXT conf_data);

               if (ret == -1)
               {
                  al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "al_conf.c(%d,%d): unexpected eof file while looking for , or }\n",
                               conf_data->line_number,
                               conf_data->column_number);
                  status = -1;
                  goto label_ret;
               }
               if (conf_data->current_token_code == _V_IF_INFO_CONF_TOKEN_COMMA)
               {
                  continue;
               }
               else if (conf_data->current_token_code == _V_IF_INFO_CONF_TOKEN_CLOSE_BRACE)
               {
                  break;
               }
               else
               {
                  al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "al_conf.c(%d,%d): token %s was found instead of , or }\n",
                               conf_data->line_number,
                               conf_data->column_number,
                               conf_data->current_token);
                  status = -1;
                  goto label_ret;
               }
            }
         }
         conf_data->parse_mode = _V_IF_INFO_CONF_PARSE_MODE_IF_INFO_TAIL;
         break;

      case _V_IF_INFO_CONF_PARSE_MODE_IF_INFO_TAIL:
         _PARSE_INTERMEDIATE_TOKEN('}', _V_IF_INFO_CONF_TOKEN_CLOSE_BRACE);
         conf_data->parse_mode = _V_IF_INFO_CONF_PARSE_MODE_NONE;
         break;
      }
   }
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s: %d\n",__func__,__LINE__);

label_ret:
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s: %d\n",__func__,__LINE__);
   return status;

#undef _PARSE_INTERMEDIATE_TOKEN
}


static int _get_token(AL_CONTEXT_PARAM_DECL _v_if_info_conf_data_t *conf_data)
{
   /**
    * Here we skip white spaces, comment lines, etc
    * and return then copy the token onto current_token
    * and update current_token_code. We also update the line_number
    * and column_number
    */

   int inside_comment;
   int token_found;
   int identifier_found;
   int i;

   inside_comment = 0;
   token_found    = 0;
   conf_data->curent_token_pos  = conf_data->current_token;
   identifier_found             = 0;
   *conf_data->curent_token_pos = 0;

   while (*conf_data->pos)
   {
      if (inside_comment)
      {
         if (*conf_data->pos == '\n')
         {
            ++conf_data->line_number;
            conf_data->column_number = 0;
            inside_comment           = 0;
         }
         goto label;
      }
      switch (*conf_data->pos)
      {
      case ' ':
      case '\t':
      case '\r':
         if (identifier_found)
         {
            token_found = 1;
         }
         break;

      case '\n':
         /** Skip but increment line_number */
         ++conf_data->line_number;
         conf_data->column_number = -1;
         if (identifier_found)
         {
            token_found = 1;
            conf_data->pos++;
         }
         break;

      case '#':
         /** From here to end of line skip everything */
         inside_comment = 1;
         break;

      case '=':
      case '(':
      case '{':
      case ',':
      case ')':
      case '}':
      case ':':
      case '.':
         if (!identifier_found)
         {
            *conf_data->curent_token_pos++ = *conf_data->pos++;
         }
         token_found = 1;
         break;

      default:
         *conf_data->curent_token_pos++ = *conf_data->pos;
         identifier_found = 1;
      }
      ++conf_data->column_number;
label:
      if (token_found)
      {
         break;
      }
      conf_data->pos++;
   }

   *conf_data->curent_token_pos = 0;

   if (*conf_data->current_token == 0)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : No token found  %s : %d\n",__func__,__LINE__);
      return -1;
   }

   conf_data->current_token_code = _V_IF_INFO_CONF_TOKEN_IDENT;

   for (i = 0; i < sizeof(_token_info) / sizeof(*_token_info); i++)
   {
      if (!strcmp(conf_data->current_token, _token_info[i].token))
      {
         conf_data->current_token_code = _token_info[i].code;
         break;
      }
   }

   return 0;
}


int virt_if_info_conf_parse(AL_CONTEXT_PARAM_DECL const char *virt_if_info_conf_string)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s: %d\n",__func__,__LINE__);
   _v_if_info_conf_data_t *data;
   int ret;

   data = (_v_if_info_conf_data_t *)al_heap_alloc(AL_CONTEXT sizeof(_v_if_info_conf_data_t));

   memset(data, 0, sizeof(_v_if_info_conf_data_t));

   data->parser_buffer = (char *)al_heap_alloc(AL_CONTEXT strlen(virt_if_info_conf_string) + 1);
   strcpy(data->parser_buffer, virt_if_info_conf_string);

   data->pos         = data->parser_buffer;
   data->line_number = 1;

   ret = _parse(AL_CONTEXT data);

   if (ret != 0)
   {
      if (data->v_if_info != NULL)
      {
         al_heap_free(AL_CONTEXT data->v_if_info);
      }

      al_heap_free(AL_CONTEXT data->parser_buffer);
      al_heap_free(AL_CONTEXT data);
      
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : parsing failed  %s : %d\n",__func__,__LINE__);
      return 0;
   }

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s: %d\n",__func__,__LINE__);
   return (int)data;
}


void virt_if_info_conf_close(AL_CONTEXT_PARAM_DECL int conf_handle)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s: %d\n",__func__,__LINE__);
   _v_if_info_conf_data_t *data;

   data = (_v_if_info_conf_data_t *)conf_handle;

   if (data->parser_buffer != NULL)
   {
      al_heap_free(AL_CONTEXT data->parser_buffer);
   }


   if (data->v_if_info != NULL)
   {
      al_heap_free(AL_CONTEXT data->v_if_info);
   }

   al_heap_free(AL_CONTEXT data);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s: %d\n",__func__,__LINE__);
}


int virt_if_info_conf_get_count(AL_CONTEXT_PARAM_DECL int conf_handle)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s: %d\n",__func__,__LINE__);
   _v_if_info_conf_data_t *data;

   data = (_v_if_info_conf_data_t *)conf_handle;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s: %d\n",__func__,__LINE__);
   return data->v_if_count;
}


int virt_if_info_conf_get_if(AL_CONTEXT_PARAM_DECL int conf_handle, int v_if_index, _virtual_if_info_t *v_if_info)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s: %d\n",__func__,__LINE__);
   _v_if_info_conf_data_t *data;

   data = (_v_if_info_conf_data_t *)conf_handle;

   if (v_if_index >= data->v_if_count)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR :  Invalid virtual iface count %s : %d\n",__func__,__LINE__);
      return -1;
   }

   memcpy(v_if_info, &data->v_if_info[v_if_index], sizeof(_virtual_if_info_t));

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s: %d\n",__func__,__LINE__);
   return 0;
}


int virt_if_info_conf_open_file(AL_CONTEXT_PARAM_DECL_SINGLE)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s: %d\n",__func__,__LINE__);
   struct _list
   {
      char         line[256];
      struct _list *next;
   }
   *head, *node, *tail, *temp;

   int  handle;
   int  ret;
   char line_buffer[256];
   int  total_size;
   char *conf_string;
   char *pdst;
   char *psrc;

   head       = NULL;
   tail       = NULL;
   total_size = 0;

   handle = al_open_file(AL_CONTEXT AL_FILE_ID_VIRT_IF_INFO_CONFIG, AL_OPEN_FILE_MODE_READ);

   if (handle == (int)NULL)
   {
      al_print_log(AL_CONTEXT AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP : ERROR : virt_if_info_conf_open_file - handle == (int)NULL %s : %d\n",__func__,__LINE__);
      return -1;
   }

   while (1)
   {
      ret = al_read_file_line(AL_CONTEXT handle, line_buffer, 255);
      if (ret != 0)
      {
         break;
      }
      if (head == NULL)
      {
         head = (struct _list *)al_heap_alloc(AL_CONTEXT sizeof(struct _list)AL_HEAP_DEBUG_PARAM);
         tail = head;
         node = head;
      }
      else
      {
         node       = (struct _list *)al_heap_alloc(AL_CONTEXT sizeof(struct _list)AL_HEAP_DEBUG_PARAM);
         tail->next = node;
      }
      node->next = NULL;
      strcpy(node->line, line_buffer);
      total_size += strlen(line_buffer);
      tail        = node;
   }

   al_close_file(AL_CONTEXT handle);

   conf_string = (char *)al_heap_alloc(AL_CONTEXT(total_size + 1) AL_HEAP_DEBUG_PARAM);
   pdst        = conf_string;
   node        = head;

   while (node != NULL)
   {
      psrc = node->line;
      while (*psrc)
      {
         *pdst++ = *psrc++;
      }
      temp = node->next;
      al_heap_free(AL_CONTEXT node);
      node = temp;
   }

   *pdst = 0;

   ret = virt_if_info_conf_parse(AL_CONTEXT conf_string);

   al_heap_free(AL_CONTEXT conf_string);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s: %d\n",__func__,__LINE__);

   return ret;
}


#ifndef _MESHAP_V_IF_CONF_NO_SET_AND_PUT

int virt_if_info_conf_set_count(AL_CONTEXT_PARAM_DECL int conf_handle, int v_if_count)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s: %d\n",__func__,__LINE__);
   _v_if_info_conf_data_t *data;

   data = (_v_if_info_conf_data_t *)conf_handle;

   if (data->v_if_count > 0)
   {
      al_heap_free(AL_CONTEXT data->v_if_info);
      data->v_if_info = NULL;
   }

   data->v_if_count = v_if_count;

   if (data->v_if_count > 0)
   {
      data->v_if_info = (_virtual_if_info_t *)al_heap_alloc(AL_CONTEXT sizeof(_virtual_if_info_t) * data->v_if_count AL_HEAP_DEBUG_PARAM);
      memset(data->v_if_info, 0, sizeof(_virtual_if_info_t) * data->v_if_count);
   }
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s: %d\n",__func__,__LINE__);

   return 0;
}


int virt_if_info_conf_set_if(AL_CONTEXT_PARAM_DECL int conf_handle, int v_if_index, _virtual_if_info_t *v_if_info)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s: %d\n",__func__,__LINE__);
   _v_if_info_conf_data_t *data;

   data = (_v_if_info_conf_data_t *)conf_handle;

   if (v_if_index >= data->v_if_count)
   {	
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : Invalid virtual interface count %s : %d\n",__func__,__LINE__);
      return -1;
   }

   memcpy(&data->v_if_info[v_if_index], v_if_info, sizeof(_virtual_if_info_t));
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s: %d\n",__func__,__LINE__);
   return 0;
}
#endif /* _MESHAP_V_IF_CONF_NO_SET_AND_PUT */
