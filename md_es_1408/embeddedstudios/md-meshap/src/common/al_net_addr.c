/********************************************************************************
* MeshDynamics
* --------------
* File     : al_net_addr.c
* Comments : Mesh MAC address
* Created  : 4/5/2004
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  2  |10/5/2005 | Misc changes                                    | Sriram |
* -----------------------------------------------------------------------------
* |  1  |3/25/2005 | Added pae_group_addr                            | Sriram |
* -----------------------------------------------------------------------------
* |  0  |4/5/2004  | Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

#include "al_net_addr.h"

al_net_addr_t broadcast_net_addr =
{
   { 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0x00, 0x00 },
   6
};

al_net_addr_t zeroed_net_addr =
{
   { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 },
   6
};

al_net_addr_t pae_group_addr =
{
   { 0x01, 0x80, 0xC2, 0x00, 0x00, 0x03, 0x00, 0x00 },
   6
};
