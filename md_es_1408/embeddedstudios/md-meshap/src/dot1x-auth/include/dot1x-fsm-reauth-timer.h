/********************************************************************************
* MeshDynamics
* --------------
* File     : dot1x-fsm-reauth-timer.h
* Comments : 802.1x Re-Authentication Timer FSM
* Created  : 1/27/2005
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  0  |1/27/2005 | Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

#ifndef __DOT1X_FSM_REAUTH_TIMER_H__
#define __DOT1X_FSM_REAUTH_TIMER_H__

#define DOT1X_FSM_REAUTH_TIMER_STATE_INVALID           0
#define DOT1X_FSM_REAUTH_TIMER_STATE_INITIALIZE        1
#define DOT1X_FSM_REAUTH_TIMER_STATE_REAUTHENTICATE    2

struct dot1x_fsm_reauth_timer
{
   dot1x_fsm_t  fsm;
   unsigned int lock;
   int          state;
};

typedef struct dot1x_fsm_reauth_timer   dot1x_fsm_reauth_timer_t;

int dot1x_reauth_timer_fsm_create(dot1x_port_t *port);
int dot1x_reauth_timer_fsm_destroy(dot1x_port_t *port);

#endif /*__DOT1X_FSM_REAUTH_TIMER_H__*/
