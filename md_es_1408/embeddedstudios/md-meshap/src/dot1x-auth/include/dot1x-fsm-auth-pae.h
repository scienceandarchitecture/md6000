/********************************************************************************
* MeshDynamics
* --------------
* File     : dot1x-fsm-auth-pae.h
* Comments : 802.1x Authenticator PAE FSM
* Created  : 1/26/2005
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  1  |7/13/2007 | dot1x_auth_pae_fsm_reset Added                  | Sriram |
* -----------------------------------------------------------------------------
* |  0  |1/26/2005 | Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

#ifndef __DOT1X_FSM_AUTH_PAE_H__
#define __DOT1X_FSM_AUTH_PAE_H__

#define DOT1X_FSM_AUTH_PAE_STATE_INVALID           0
#define DOT1X_FSM_AUTH_PAE_STATE_INITIALIZE        1
#define DOT1X_FSM_AUTH_PAE_STATE_DISCONNECTED      2
#define DOT1X_FSM_AUTH_PAE_STATE_CONNECTING        3
#define DOT1X_FSM_AUTH_PAE_STATE_AUTHENTICATING    4
#define DOT1X_FSM_AUTH_PAE_STATE_AUTHENTICATED     5
#define DOT1X_FSM_AUTH_PAE_STATE_ABORTING          6
#define DOT1X_FSM_AUTH_PAE_STATE_HELD              7
#define DOT1X_FSM_AUTH_PAE_STATE_FORCE_AUTH        8
#define DOT1X_FSM_AUTH_PAE_STATE_FORCE_UNAUTH      9

struct dot1x_fsm_auth_pae
{
   dot1x_fsm_t   fsm;
   unsigned int  lock;
   int           state;
   int           eapLogoff;
   int           eapStart;
   int           portMode;
   int           reAuthCount;
   int           rxRespId;
   unsigned char *identity_data;
   int           identity_length;
   dot1x_fsm_t   *trigger_fsm;
   int           trigger_fsm_code;
   int           trigger_other_code;
   int           trigger_code;
};

typedef struct dot1x_fsm_auth_pae   dot1x_fsm_auth_pae_t;

/**
 * Trigger Codes
 */

#define DOT1X_FSM_AUTH_PAE_TRIGGER_AUTH_TIMEOUT      1
#define DOT1X_FSM_AUTH_PAE_TRIGGER_AUTH_SUCCESS      2
#define DOT1X_FSM_AUTH_PAE_TRIGGER_AUTH_FAIL         3
#define DOT1X_FSM_AUTH_PAE_TRIGGER_REAUTHENTICATE    4

int dot1x_auth_pae_fsm_create(dot1x_port_t *port);
int dot1x_auth_pae_fsm_reset(dot1x_port_t *port);
int dot1x_auth_pae_fsm_destroy(dot1x_port_t *port);

#endif /*__DOT1X-FSM-AUTH-PAE_H__*/
