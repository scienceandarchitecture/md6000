/********************************************************************************
* MeshDynamics
* --------------
* File     : dot1x-fsm-backend-auth.h
* Comments : 802.1x Backend Authentication FSM
* Created  : 1/27/2005
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  1  |7/13/2007 | dot1x_backend_auth_fsm_reset Added              | Sriram |
* -----------------------------------------------------------------------------
* |  0  |1/27/2005 | Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

#ifndef __DOT1X_FSM_BACKEND_AUTH_H__
#define __DOT1X_FSM_BACKEND_AUTH_H__

#define DOT1X_FSM_BACKEND_AUTH_STATE_INVALID       0
#define DOT1X_FSM_BACKEND_AUTH_STATE_REQUEST       1
#define DOT1X_FSM_BACKEND_AUTH_STATE_RESPONSE      2
#define DOT1X_FSM_BACKEND_AUTH_STATE_SUCCESS       3
#define DOT1X_FSM_BACKEND_AUTH_STATE_FAIL          4
#define DOT1X_FSM_BACKEND_AUTH_STATE_TIMEOUT       5
#define DOT1X_FSM_BACKEND_AUTH_STATE_IDLE          6
#define DOT1X_FSM_BACKEND_AUTH_STATE_INITIALIZE    7

struct dot1x_fsm_backend_auth
{
   dot1x_fsm_t       fsm;
   unsigned int      lock;
   int               state;
   int               reqCount;
   int               rxResp;
   int               aSuccess;
   int               aFail;
   int               aReq;
   int               idFromServer;
   dot1x_eap_frame_t *rxRespFrame;
   dot1x_eap_frame_t *aReqFrame;
   dot1x_fsm_t       *trigger_fsm;
   int               trigger_fsm_code;
   int               trigger_other_code;
   int               trigger_code;
};

typedef struct dot1x_fsm_backend_auth   dot1x_fsm_backend_auth_t;

/**
 * Trigger Codes
 */

#define DOT1X_FSM_BACKEND_AUTH_TRIGGER_AUTH_ABORT    1
#define DOT1X_FSM_BACKEND_AUTH_TRIGGER_AUTH_START    2
#define DOT1X_FSM_BACKEND_AUTH_TRIGGER_AREQ          3
#define DOT1X_FSM_BACKEND_AUTH_TRIGGER_ASUCCESS      4
#define DOT1X_FSM_BACKEND_AUTH_TRIGGER_AFAIL         5

int dot1x_backend_auth_fsm_create(dot1x_port_t *port);
int dot1x_backend_auth_fsm_reset(dot1x_port_t *port);
int dot1x_backend_auth_fsm_destroy(dot1x_port_t *port);

#endif /*__DOT1X_FSM_BACKEND_AUTH_H__*/
