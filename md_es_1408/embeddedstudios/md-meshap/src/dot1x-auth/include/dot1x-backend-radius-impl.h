/********************************************************************************
* MeshDynamics
* --------------
* File     : dot1x-backend-radius-impl.h
* Comments : 802.1x RADIUS backend impl definitions
* Created  : 2/20/2005
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  0  |2/20/2005 | Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

#ifndef __DOT1X_BACKEND_RADIUS_IMPL_H__
#define __DOT1X_BACKEND_RADIUS_IMPL_H__

int dot1x_backend_radius_init(void);
int dot1x_backend_radius_uninit(void);
int dot1x_backend_radius_create_instance(dot1x_t *instance, dot1x_radius_backend_param_t *param);
int dot1x_backend_radius_destroy_instance(dot1x_t *instance);
int dot1x_backend_radius_tx(dot1x_port_t *port, dot1x_radius_backend_param_t *param, unsigned char *packet, int len);

#endif /*__DOT1X-BACKEND-RADIUS-IMPL_H__*/
