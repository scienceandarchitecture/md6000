/********************************************************************************
* MeshDynamics
* --------------
* File     : dot1x-backend-radius.h
* Comments : 802.1x Radius Client Backend
* Created  : 1/28/2005
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  1  |12/6/2006 | Changes for In-direct VLAN (NPS)                | Sriram |
* -----------------------------------------------------------------------------
* |  0  |1/28/2005 | Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

#ifndef __DOT1X_BACKEND_RADIUS_H__
#define __DOT1X_BACKEND_RADIUS_H__

#ifndef DOT1X_BACKEND_RADIUS_TIMEOUT
#define DOT1X_BACKEND_RADIUS_TIMEOUT    15000
#endif /** DOT1X_BACKEND_RADIUS_TIMEOUT */

struct dot1x_radius_backend_param
{
   unsigned int   radius_server_addr;
   unsigned short radius_server_port;
   char           radius_secret[129];
   int            radius_secret_len;
};

typedef struct dot1x_radius_backend_param   dot1x_radius_backend_param_t;

struct dot1x_radius_backend_key
{
   unsigned char  valid;
   unsigned char  key[64];              /* key[0] is the length */
   unsigned short salt;
};

typedef struct dot1x_radius_backend_key   dot1x_radius_backend_key_t;

int dot1x_radius_backend_initialize(void);
int dot1x_radius_backend_uninitialize(void);
int dot1x_radius_backend_rx(unsigned char *packet, int length);
int dot1x_radius_backend_get_rx_key(dot1x_port_t *port, dot1x_radius_backend_key_t *key);
int dot1x_radius_backend_get_tx_key(dot1x_port_t *port, dot1x_radius_backend_key_t *key);
int dot1x_radius_backend_get_vlan_tag(dot1x_port_t *port);

#endif /*__DOT1X-BACKEND-RADIUS_H__*/
