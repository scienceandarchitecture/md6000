/********************************************************************************
* MeshDynamics
* --------------
* File     : dot1x-fsm-port-timers.h
* Comments : 802.1x Port Timers FSM
* Created  : 1/26/2005
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  1  |7/13/2007 | dot1x_port_timers_fsm_reset Added               | Sriram |
* -----------------------------------------------------------------------------
* |  0  |1/26/2005 | Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

#ifndef __DOT1X_FSM_PORT_TIMERS_H__
#define __DOT1X_FSM_PORT_TIMERS_H__

#define DOT1X_FSM_PORT_TIMERS_STATE_INVALID       0
#define DOT1X_FSM_PORT_TIMERS_STATE_ONE_SECOND    1
#define DOT1X_FSM_PORT_TIMERS_STATE_TICK          2

struct dot1x_fsm_port_timers
{
   dot1x_fsm_t  fsm;
   unsigned int lock;
   int          state;
   int          tick;
};

typedef struct dot1x_fsm_port_timers   dot1x_fsm_port_timers_t;

int dot1x_port_timers_fsm_create(dot1x_port_t *port);
int dot1x_port_timers_fsm_reset(dot1x_port_t *port);
int dot1x_port_timers_fsm_destroy(dot1x_port_t *port);

#endif /*__DOT1X-FSM-PORT-TIMERS_H__*/
