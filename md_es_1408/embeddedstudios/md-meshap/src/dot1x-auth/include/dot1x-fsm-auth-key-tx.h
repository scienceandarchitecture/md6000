/********************************************************************************
* MeshDynamics
* --------------
* File     : dot1x-fsm-auth-key-tx.h
* Comments : 802.1x Authenticator Key Transmit FSM
* Created  : 1/27/2005
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  0  |1/27/2005 | Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

#ifndef __DOT1X_FSM_AUTH_KEY_TX_H__
#define __DOT1X_FSM_AUTH_KEY_TX_H__

#define DOT1X_FSM_AUTH_KEY_TX_STATE_INVALID    0
#define DOT1X_FSM_AUTH_KEY_TX_STATE_NO_KEY     1
#define DOT1X_FSM_PORT_TIMERS_STATE_KEY_TX     2

struct dot1x_fsm_auth_tx_key
{
   dot1x_fsm_t  fsm;
   unsigned int lock;
   int          state;
   int          keyAvailable;
};

typedef struct dot1x_fsm_auth_tx_key   dot1x_fsm_auth_tx_key_t;

/**
 * Trigger Codes
 */

#define DOT1X_FSM_AUTH_KEY_TX_TRIGGER_KEY_AVAILABLE    1

int dot1x_auth_tx_key_fsm_create(dot1x_port_t *port);
int dot1x_auth_tx_key_fsm_destroy(dot1x_port_t *port);

#endif /*__DOT1X_FSM_AUTH_KEY_TX_H__*/
