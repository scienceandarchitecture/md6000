/********************************************************************************
* MeshDynamics
* --------------
* File     : dot1x-fsm.h
* Comments : 802.1x State Machine Definitions
* Created  : 1/25/2005
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  0  |1/25/2005 | Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

#include "dot1x.h"

#ifndef __DOT1X_FSM_H__
#define __DOT1X_FSM_H__

struct dot1x_fsm_globals
{
   /** TIMERS */
   int authWhile;                               /** Supplicant PAE */
   int aWhile;                                  /** Backend */
   int heldWhile;                               /** Supplicant PAE */
   int quiteWhile;                              /** Authenticator PAE */
   int reAuthWhen;                              /** Re-Authentication */
   int startWhen;                               /** Supplicant PAE */
   int txWhen;                                  /** Authenticator PAE */
   /** GLOBALS */
   int authAbort;                               /** Authenticator PAE -> Backend */
   int authFail;                                /** Backend -> Authenticator PAE */
   int authStart;                               /** Authenticator PAE -> Backend */
   int authTimeout;                             /** Backend -> Authenticator PAE */
   int authSuccess;                             /** Backend -> Authenticator PAE*/
   int currentId;                               /** Authenticator PAE & Backend */
   int portControl;                             /** Authenticator PAE */
   int portStatus;                              /** Authenticator PAE */
   int reAuthenticate;                          /** Re-Authentication -> Authenticator PAE*/
   int receiveId;
   int suppStatus;                              /** Supplicant PAE */
};

typedef struct dot1x_fsm_globals   dot1x_fsm_globals_t;

struct dot1x_fsm
{
   int (*initialize)                   (dot1x_port_t *port, int fsm_code, int other_code, struct dot1x_fsm *fsm);
   int (*tick)                                 (dot1x_port_t *port, int fsm_code, int other_code, struct dot1x_fsm *fsm);
   int (*eapol_rx_begin)               (dot1x_port_t *port, int fsm_code, int other_code, struct dot1x_fsm *fsm);
   int (*eapol_rx)                             (dot1x_port_t *port, int fsm_code, int other_code, struct dot1x_fsm *fsm, dot1x_eapol_frame_t *frame);
   int (*eapol_rx_end)                 (dot1x_port_t *port, int fsm_code, int other_code, struct dot1x_fsm *fsm);
   int (*trigger)                              (dot1x_port_t *port, int fsm_code, int other_code, struct dot1x_fsm *fsm, int trigger_code);
   int (*on_destroy)                   (dot1x_port_t *port, struct dot1x_fsm *fsm);
};

typedef struct dot1x_fsm   dot1x_fsm_t;

/**
 * Standard DOT1X State Machines
 */

#define DOT1X_FSM_CODE_PORT_TIMERS                   0
#define DOT1X_FSM_CODE_AUTHENTICATOR_PAE             1
#define DOT1X_FSM_CODE_AUTHENTICATOR_KEY_TRANSMIT    2
#define DOT1X_FSM_CODE_SUPPLICANT_KEY_TRANSMIT       3
#define DOT1X_FSM_CODE_REAUTHENTICATION_TIMER        4
#define DOT1X_FSM_CODE_BACKEND_AUTHENTICATION        5
#define DOT1X_FSM_CODE_CONTROLLED_DIRECTIONS         6
#define DOT1X_FSM_CODE_SUPPLICANT_PAE                7
#define DOT1X_FSM_CODE_KEY_RECEIVE                   8

/**
 * Number of Standard FSMs available
 */

#define DOT1X_FSM_STANDARD_COUNT    (DOT1X_FSM_CODE_KEY_RECEIVE + 1)

/**
 * For all other State Machines and extensions
 */

#define DOT1X_FSM_CODE_OTHER    9

int dot1x_fsm_register(dot1x_port_t *port, int fsm_code, int other_code, dot1x_fsm_t *fsm);
int dot1x_fsm_unregister(dot1x_port_t *port, int fsm_code, int other_code);
int dot1x_fsm_unregister_all(dot1x_port_t *port);
dot1x_fsm_t *dot1x_fsm_get(dot1x_port_t *port, int fsm_code, int other_code);
int dot1x_fsm_tick(dot1x_port_t *port);
int dot1x_fsm_eapol_rx(dot1x_port_t *port, dot1x_eapol_frame_t *frame);
int dot1x_fsm_initialize(dot1x_port_t *port);
#endif /*__DOT1X-FSM_H__*/
