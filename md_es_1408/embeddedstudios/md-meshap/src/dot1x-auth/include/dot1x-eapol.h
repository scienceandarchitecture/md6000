/********************************************************************************
* MeshDynamics
* --------------
* File     : dot1x-eapol.h
* Comments : 802.1x EAPOL Definitions
* Created  : 1/26/2005
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  0  |1/26/2005 | Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

#ifndef __DOT1X_EAPOL_H__
#define __DOT1X_EAPOL_H__

#define DOT1X_EAPOL_ETH_TYPE             0x888E

#define DOT1X_EAPOL_FRAME_TYPE_EAP       0
#define DOT1X_EAPOL_FRAME_TYPE_START     1
#define DOT1X_EAPOL_FRAME_TYPE_LOGOFF    2
#define DOT1X_EAPOL_FRAME_TYPE_KEY       3
#define DOT1X_EAPOL_FRAME_TYPE_ASF       4

#define DOT1X_EAPOL_FRAME_MIN_LENGTH     4

#define DOT1X_2001_EAPOL_VERSION         1

struct dot1x_eapol_frame
{
   unsigned char  version;
   unsigned char  type;                         /** DOT1X_EAPOL_FRAME_TYPE_* */
   unsigned short body_length;
   unsigned char  *body;
};

typedef struct dot1x_eapol_frame   dot1x_eapol_frame_t;

#define DOT1X_EAPOL_KEY_TYPE_RC4    1

struct dot1x_eapol_key_body
{
   unsigned char  type;
   unsigned short length;
   unsigned char  replay_counter[8];
   unsigned char  iv[16];
   unsigned char  index;
   unsigned char  signature[16];
   unsigned char  *key;
};

typedef struct dot1x_eapol_key_body   dot1x_eapol_key_body_t;

#define DOT1X_EAP_CODE_REQUEST          1
#define DOT1X_EAP_CODE_RESPONSE         2
#define DOT1X_EAP_CODE_SUCCESS          3
#define DOT1X_EAP_CODE_FAILURE          4

#define DOT1X_EAP_TYPE_IDENTITY         0x01
#define DOT1X_EAP_TYPE_NOTIFICATION     0x02
#define DOT1X_EAP_TYPE_NAK              0x03
#define DOT1X_EAP_TYPE_MD5_CHALLENGE    0x04
#define DOT1X_EAP_TYPE_OTP              0x05
#define DOT1X_EAP_TYPE_GTC              0x06
#define DOT1X_EAP_TYPE_EAPTLS           0x0D
#define DOT1X_EAP_TYPE_LEAP             0x11

#define DOT1X_EAP_MIN_LENGTH            4

struct dot1x_eap_frame
{
   unsigned char  code;
   unsigned char  id;
   unsigned short length;
   unsigned char  type;                         /* Only valid for REQUEST and RESPONSE */
   unsigned short data_length;
   unsigned char  *data;
};

typedef struct dot1x_eap_frame   dot1x_eap_frame_t;

int dot1x_eapol_process_frame(unsigned char *packet_data, int length, dot1x_eapol_frame_t *frame_desc);
int dot1x_eapol_process_key(dot1x_eapol_frame_t *frame_desc, dot1x_eapol_key_body_t *key_desc);
int dot1x_eapol_process_eap(dot1x_eapol_frame_t *frame_desc, dot1x_eap_frame_t *eap_desc);
int dot1x_eapol_eap_frame_process(unsigned char *packet, int packet_length, dot1x_eap_frame_t *eap_desc);

int dot1x_eapol_create_frame(dot1x_eapol_frame_t *frame_desc, unsigned char *packet);
int dot1x_eapol_key_body_create(dot1x_eapol_key_body_t *key_desc, unsigned char *packet);
int dot1x_eapol_eap_frame_create(dot1x_eap_frame_t *frame, unsigned char *packet);


#endif /*__DOT1X-EAPOL_H__*/
