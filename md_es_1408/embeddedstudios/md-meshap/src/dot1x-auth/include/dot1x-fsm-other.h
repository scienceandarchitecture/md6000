/********************************************************************************
* MeshDynamics
* --------------
* File     : dot1x-fsm-other.h
* Comments : 802.1x other FSMs
* Created  : 2/2/2005
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  0  |2/2/2005  | Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

#ifndef __DOT1X_FSM_OTHER_H__
#define __DOT1X_FSM_OTHER_H__

#define DOT1X_FSM_OTHER_CODE_RADIUS         1
#define DOT1X_FSM_OTHER_CODE_DOT11I_AUTH    2
#define DOT1X_FSM_OTHER_CODE_DOT11I_GTK     3

#endif /*__DOT1X_FSM_OTHER_H__*/
