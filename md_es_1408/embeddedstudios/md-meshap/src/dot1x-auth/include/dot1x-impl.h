/********************************************************************************
* MeshDynamics
* --------------
* File     : dot1x-impl.h
* Comments : 802.1x Implementation specific definitions
* Created  : 1/25/2005
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  3  |6/16/2008 | Lock primitives changed                         | Sriram |
* -----------------------------------------------------------------------------
* |  2  |7/17/2007 | DOT1X 2K Buffer Implemented                     | Sriram |
* -----------------------------------------------------------------------------
* |  1  |9/18/2006 | dot1x_eapol_tx_ex Added                         | Sriram |
* -----------------------------------------------------------------------------
* |  0  |1/25/2005 | Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

/**
 * The implementation is expected to provide dot1x-impl-inc.h
 * Using this file, the implementation can make available files
 * for functions like strcpy/memcpy/nthos/etc to the dot1x
 *
 * dot1x also needs the following defined
 *		DOT1X_INLINE
 *		DOT1X_IMPL_BYTE_ORDER to DOT1X_LE OR DOT1X_BE
 */

#include "dot1x-impl-inc.h"

#ifndef __DOT1X_IMPL_H__
#define __DOT1X_IMPL_H__

#define DOT1X_LE    0x1234
#define DOT1X_BE    0x4321

struct dot1x_thread_param
{
   const char *thread_name;
   int        thread_id;
   void       *param;
};

typedef struct dot1x_thread_param   dot1x_thread_param_t;

typedef int (*dot1x_thread_function_t)  (dot1x_thread_param_t *param);

typedef void (*dot1x_timer_t)   (unsigned long timer_data);

#define DOT1X_PRINT_LEVEL_INFORMATION    1
#define DOT1X_PRINT_LEVEL_FLOW           2
#define DOT1X_PRINT_LEVEL_ERROR          4
#define DOT1X_PRINT_LEVEL_DEBUG          8

void *dot1x_heap_alloc(unsigned int size);
void dot1x_heap_free(void *memblock);

unsigned int dot1x_create_lock(void);
void dot1x_destroy_lock(unsigned int lock);

#define dot1x_lock(_l, _flags)      dot1x_impl_lock(_l, _flags)
#define dot1x_unlock(_l, _flags)    dot1x_impl_unlock(_l, _flags)

unsigned char *dot1x_get_2k_buffer(void);
void dot1x_release_2k_buffer(unsigned char *buffer);

void dot1x_get_random_bytes(unsigned char *buffer, int length);

int dot1x_print(int level, const char *format, ...);
unsigned long dot1x_timestamp(void);
int dot1x_get_hostname(char *buffer, int buffer_length);

void dot1x_crypt_initialize(dot1x_t *instance);
void dot1x_crypt_uninitialize(dot1x_t *instance);
int dot1x_crypt_hmac_md5(dot1x_t *instance, unsigned char *packet, int length, unsigned char *key, int key_len, unsigned char *mauth);
int dot1x_crypt_md5_digest_open(dot1x_t *instance);
int dot1x_crypt_md5_digest(dot1x_t *instance, unsigned char *buffer, int length);
int dot1x_crypt_md5_digest_close(dot1x_t *instance, unsigned char *hash);

int dot1x_get_instance_addr(dot1x_t *instance, dot1x_port_t *port, unsigned char *addr);
int dot1x_get_instance_ip_addr(dot1x_t *instance, dot1x_port_t *port, unsigned char *addr);
unsigned int dot1x_get_port_aid(dot1x_port_t *port);

int dot1x_get_instance_essid(dot1x_t *instance, char *essid);
int dot1x_get_port_connect_info(dot1x_port_t *port, char *info);

int dot1x_eapol_tx(dot1x_port_t *port, unsigned char *packet_data, int packet_length);
int dot1x_eapol_tx_ex(dot1x_port_t *port, unsigned char *packet_data, int packet_length, int secure);
#endif /*__DOT1X-IMPL_H__*/
