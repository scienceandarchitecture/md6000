/********************************************************************************
* MeshDynamics
* --------------
* File     : dot1x-fsm-key-rx.h
* Comments : 802.1x Key Recv FSM
* Created  : 1/28/2005
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  0  |1/28/2005 | Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

#ifndef __DOT1X_FSM_KEY_RX_H__
#define __DOT1X_FSM_KEY_RX_H__

#define DOT1X_FSM_KEY_RX_STATE_INVALID    0
#define DOT1X_FSM_KEY_RX_STATE_NO_KEY     1
#define DOT1X_FSM_KEY_RX_STATE_KEY_RX     2

struct dot1x_fsm_key_rx
{
   dot1x_fsm_t  fsm;
   unsigned int lock;
   int          state;
   int          rxKey;
};

typedef struct dot1x_fsm_key_rx   dot1x_fsm_key_rx_t;

int dot1x_key_rx_fsm_create(dot1x_port_t *port);
int dot1x_key_rx_fsm_destroy(dot1x_port_t *port);

#endif /*__DOT1X-FSM-KEY-RX_H__*/
