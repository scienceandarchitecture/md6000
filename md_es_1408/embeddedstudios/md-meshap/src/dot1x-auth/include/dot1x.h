/********************************************************************************
* MeshDynamics
* --------------
* File     : dot1x.h
* Comments : 802.1x Authenticator
* Created  : 1/25/2005
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  1  |7/16/2007 | reset_port_context Added                        | Sriram |
* -----------------------------------------------------------------------------
* |  0  |1/25/2005 | Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

#include "dot1x-eapol.h"

#ifndef __DOT1X_H__
#define __DOT1X_H__

#ifndef DOT1X_BACKEND_NAME_MAX
#define DOT1X_BACKEND_NAME_MAX    64
#endif /* DOT1X_BACKEND_NAME_MAX */

#ifndef DOT1X_PORT_ADDRESS_LEN
#define DOT1X_PORT_ADDRESS_LEN    6
#endif

#ifndef DOT1X_PORT_SESSION_USER_NAME_MAX
#define DOT1X_PORT_SESSION_USER_NAME_MAX    64
#endif

enum
{
   Both = 0,
   In   = 1
};

enum
{
   ForceAuthorized   = 1,
   ForceUnauthorized = 0,
   Auto              = 2,
};

enum
{
   Unauthorized = 0,
   Authorized   = 1
};

enum
{
   False,
   True
};

struct dot1x_port;

typedef int (*dot1x_port_status_change_t)       (struct dot1x_port *port, int status);

struct dot1x
{
   char                       instance_name[256];
   char                       backend_name[256];
   void                       *instance_data;
   dot1x_port_status_change_t on_port_status_change;
};

typedef struct dot1x   dot1x_t;

struct dot1x_port
{
   dot1x_t       *dot1x_instance;
   void          *port_data;
   unsigned char address[DOT1X_PORT_ADDRESS_LEN];
   int           AdminControlledDirections;
   int           OperControlledDirections;
   int           AuthControlledPortControl;
   int           AuthControlledPortStatus;
   int           KeyTransmissionEnabled;
};

typedef struct dot1x_port   dot1x_port_t;

struct dot1x_backend
{
   int (*create_instance)              (dot1x_t *instance, void *backend_param);
   int (*create_port_context)  (dot1x_port_t *port);
   int (*reset_port_context)   (dot1x_port_t *port);
   int (*send_eap_frame)               (dot1x_port_t *port, dot1x_eap_frame_t *eap_frame);
   int (*destroy_port_context) (dot1x_port_t *port);
   int (*destroy_instance)             (dot1x_t *instance);
};

typedef struct dot1x_backend   dot1x_backend_t;

struct dot1x_port_stats
{
   /** Authenticator Statistics */
   int  eapol_rx;
   int  eapol_tx;
   int  eapol_start_rx;
   int  eapol_logoff_rx;
   int  eapol_resp_id_rx;
   int  eapol_response_rx;
   int  eapol_req_id_tx;
   int  eapol_request_tx;
   int  eapol_invalid_rx;
   int  eapol_length_error_rx;

   /** Authenticator Diagnostics */

   int  authEntersConnecting;
   int  authEapLogoffsWhileConnecting;
   int  authEntersAuthenticating;
   int  authAuthSuccessWhileAuthenticating;
   int  authAuthTimeoutsWhileAuthenticating;
   int  authAuthFailWhileAuthenticating;
   int  authAuthReauthsWhileAuthenticating;
   int  authAuthEapStartsWhileAuthenticating;
   int  authAuthEapLogoffWhileAuthenticating;
   int  authAuthReauthsWhileAuthenticated;
   int  authAuthEapStartsWhileAuthenticated;
   int  authAuthEapLogoffWhileAuthenticated;

   int  backendResponses;
   int  backendAccessChallenges;
   int  backendOtherRequestsToSupplicant;
   int  backendNonNakResponsesFromSupplicant;
   int  backendAuthSuccesses;
   int  backendAuthFails;

   /** Session Statistics */

   int  session_octets_rx;
   int  session_octets_tx;
   int  session_frames_rx;
   int  session_frames_tx;
   int  session_time;
   int  session_terminate_cause;
   char session_user_name[DOT1X_PORT_SESSION_USER_NAME_MAX + 1];
};

typedef struct dot1x_port_stats   dot1x_port_stats_t;

typedef int (*dot1x_port_iterator_routine_t)    (dot1x_port_t *port);

int dot1x_initialize(void);
int dot1x_uninitialize(void);

int dot1x_register_backend(const char *backend_name, const dot1x_backend_t *backend);
int dot1x_unregister_backend(const char *backend_name);

dot1x_t *dot1x_create_instance(const char                 *instance_name,
                               const char                 *backend_name,
                               void                       *backend_param,
                               void                       *instance_data,
                               dot1x_port_status_change_t on_status_change);

/**
 * Destroys all ports and only their standard FSMs
 * If non-standard FSMs have been registered, then
 */

void dot1x_destroy_instance(dot1x_t *instance);

int dot1x_process_eapol(dot1x_t        *instance,
                        unsigned char  *dst_mac,
                        unsigned char  *src_mac,
                        unsigned short type,
                        unsigned char  *packet_data,
                        int            packet_length);

int dot1x_tick(dot1x_t *instance);

dot1x_port_t *dot1x_create_port(dot1x_t       *instance,
                                unsigned char *address,
                                void          *port_data,
                                int           AdminControlledDirections,
                                int           AuthControlledPortControl,
                                int           KeyTransmissionEnabled);

void dot1x_port_initialize(dot1x_t       *instance,
                           unsigned char *address);

void dot1x_destroy_port(dot1x_t       *instance,
                        unsigned char *address);

void dot1x_iterate_ports(dot1x_t                       *instance,
                         dot1x_port_iterator_routine_t routine);

int dot1x_tx_canned_fail(dot1x_port_t *port);
int dot1x_tx_canned_success(dot1x_port_t *port);
#endif /*__DOT1X_H__*/
