/********************************************************************************
* MeshDynamics
* --------------
* File     : dot1x-fsm-reauth-timer.c
* Comments : 802.1x Re-Authentication Timer FSM
* Created  : 1/27/2005
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  1  |6/16/2008 | Lock primitives changed                         | Sriram |
* -----------------------------------------------------------------------------
* |  0  |1/27/2005 | Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

#include "dot1x.h"
#include "dot1x-impl.h"
#include "dot1x-priv.h"
#include "dot1x-fsm-reauth-timer.h"

/**
 * FSM functions
 */

static int _tick(dot1x_port_t *port, int fsm_code, int other_code, struct dot1x_fsm *fsm);
static int _eapol_rx(dot1x_port_t *port, int fsm_code, int other_code, struct dot1x_fsm *fsm, dot1x_eapol_frame_t *frame);
static int _trigger(dot1x_port_t *port, int fsm_code, int other_code, struct dot1x_fsm *fsm, int trigger_code);
static int _initialize(dot1x_port_t *port, int fsm_code, int other_code, struct dot1x_fsm *fsm);
static int _on_destroy(dot1x_port_t *port, struct dot1x_fsm *fsm);

/**
 * Internal functions
 */

static void _on_state_entry(dot1x_port_t *port, dot1x_fsm_reauth_timer_t *fsm_reauth);
static void _attempt_transition(dot1x_port_t *port, dot1x_fsm_reauth_timer_t *fsm_reauth);


int dot1x_reauth_timer_fsm_create(dot1x_port_t *port)
{
   dot1x_fsm_reauth_timer_t *fsm_reauth;
   int err;

   fsm_reauth = (dot1x_fsm_reauth_timer_t *)dot1x_heap_alloc(sizeof(dot1x_fsm_reauth_timer_t));

   memset(fsm_reauth, 0, sizeof(dot1x_fsm_reauth_timer_t));

   fsm_reauth->fsm.initialize = _initialize;
   fsm_reauth->fsm.eapol_rx   = _eapol_rx;
   fsm_reauth->fsm.tick       = _tick;
   fsm_reauth->fsm.trigger    = _trigger;
   fsm_reauth->fsm.on_destroy = _on_destroy;
   fsm_reauth->lock           = dot1x_create_lock();
   fsm_reauth->state          = DOT1X_FSM_REAUTH_TIMER_STATE_INVALID;

   err = dot1x_fsm_register(port, DOT1X_FSM_CODE_REAUTHENTICATION_TIMER, 0, &fsm_reauth->fsm);

   if (err)
   {
      dot1x_destroy_lock(fsm_reauth->lock);
      dot1x_heap_free(fsm_reauth);
   }

   dot1x_print(DOT1X_PRINT_LEVEL_FLOW,
               "dot1x-auth: Reauthentication Timer FSM initialized for %s:%02x-%02x-%02x-%02x-%02x-%02x\n",
               port->dot1x_instance->instance_name,
               port->address[0],
               port->address[1],
               port->address[2],
               port->address[3],
               port->address[4],
               port->address[5]);

   return err;
}


int dot1x_reauth_timer_fsm_destroy(dot1x_port_t *port)
{
   dot1x_fsm_t *fsm;

   fsm = dot1x_fsm_get(port, DOT1X_FSM_CODE_REAUTHENTICATION_TIMER, 0);

   if (fsm != NULL)
   {
      _on_destroy(port, fsm);
   }

   dot1x_fsm_unregister(port, DOT1X_FSM_CODE_REAUTHENTICATION_TIMER, 0);

   return 0;
}


static void _on_state_entry(dot1x_port_t *port, dot1x_fsm_reauth_timer_t *fsm_reauth)
{
   dot1x_port_priv_t *priv_port;

   priv_port = (dot1x_port_priv_t *)port;

   switch (fsm_reauth->state)
   {
   case DOT1X_FSM_REAUTH_TIMER_STATE_INITIALIZE:
      priv_port->fsm_globals.reAuthWhen = priv_port->reAuthPeriod;
      break;

   case DOT1X_FSM_REAUTH_TIMER_STATE_REAUTHENTICATE:
      priv_port->fsm_globals.reAuthenticate = True;
      break;
   }
}


static void _attempt_transition(dot1x_port_t *port, dot1x_fsm_reauth_timer_t *fsm_reauth)
{
   dot1x_port_priv_t *priv_port;
   int               changed;

   priv_port = (dot1x_port_priv_t *)port;

   do
   {
      changed = 0;
      switch (fsm_reauth->state)
      {
      case DOT1X_FSM_REAUTH_TIMER_STATE_INITIALIZE:
         if (priv_port->fsm_globals.reAuthWhen == 0)
         {
            fsm_reauth->state = DOT1X_FSM_REAUTH_TIMER_STATE_REAUTHENTICATE;
            _on_state_entry(port, fsm_reauth);
            changed = 1;
         }
         break;

      case DOT1X_FSM_REAUTH_TIMER_STATE_REAUTHENTICATE:
         fsm_reauth->state = DOT1X_FSM_REAUTH_TIMER_STATE_INITIALIZE;
         _on_state_entry(port, fsm_reauth);
         changed = 1;
         break;
      }
   } while (changed);
}


static int _tick(dot1x_port_t *port, int fsm_code, int other_code, struct dot1x_fsm *fsm)
{
   dot1x_fsm_reauth_timer_t *fsm_reauth;
   unsigned int             lock_code;

   fsm_reauth = (dot1x_fsm_reauth_timer_t *)fsm;

   dot1x_lock(fsm_reauth->lock, lock_code);

   _attempt_transition(port, fsm_reauth);

   dot1x_unlock(fsm_reauth->lock, lock_code);

   return 0;
}


static int _eapol_rx(dot1x_port_t *port, int fsm_code, int other_code, struct dot1x_fsm *fsm, dot1x_eapol_frame_t *frame)
{
   /** Nothing to do here */
   return 0;
}


static int _trigger(dot1x_port_t *port, int fsm_code, int other_code, struct dot1x_fsm *fsm, int trigger_code)
{
   return 0;
}


static int _initialize(dot1x_port_t *port, int fsm_code, int other_code, struct dot1x_fsm *fsm)
{
   dot1x_fsm_reauth_timer_t *fsm_reauth;
   dot1x_port_priv_t        *priv_port;
   unsigned int             lock_code;

   fsm_reauth = (dot1x_fsm_reauth_timer_t *)fsm;
   priv_port  = (dot1x_port_priv_t *)port;

   dot1x_lock(fsm_reauth->lock, lock_code);

   if (priv_port->reAuthEnabled)
   {
      fsm_reauth->state = DOT1X_FSM_REAUTH_TIMER_STATE_INITIALIZE;
      _on_state_entry(port, fsm_reauth);
      _attempt_transition(port, fsm_reauth);
   }
   else
   {
      fsm_reauth->state = DOT1X_FSM_REAUTH_TIMER_STATE_INVALID;
   }

   dot1x_unlock(fsm_reauth->lock, lock_code);

   return 0;
}


static int _on_destroy(dot1x_port_t *port, struct dot1x_fsm *fsm)
{
   dot1x_fsm_reauth_timer_t *fsm_reauth;

   fsm_reauth = (dot1x_fsm_reauth_timer_t *)fsm;

   if (fsm_reauth != NULL)
   {
      dot1x_destroy_lock(fsm_reauth->lock);
      dot1x_heap_free(fsm_reauth);
   }

   return 0;
}
