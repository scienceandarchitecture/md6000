/********************************************************************************
* MeshDynamics
* --------------
* File     : dot1x-priv-backend.c
* Comments : 802.1x private backend routines
* Created  : 1/25/2005
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  1  |6/16/2008 | Lock primitives changed                         | Sriram |
* -----------------------------------------------------------------------------
* |  0  |1/25/2005 | Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/
int dot1x_priv_backend_register(const char *backend_name, const dot1x_backend_t *backend)
{
   dot1x_backend_priv_t *temp;
   unsigned int         code;

   dot1x_lock(_backend_list_lock, code);

   temp = _backend_list_head;

   while (temp != NULL)
   {
      if (!strcmp(temp->backend_name, backend_name))
      {
         dot1x_unlock(_backend_list_lock, code);
         return -1;
      }
      temp = temp->next;
   }

   temp = (dot1x_backend_priv_t *)dot1x_heap_alloc(sizeof(dot1x_backend_priv_t));

   memset(temp, 0, sizeof(dot1x_backend_priv_t));

   temp->backend = backend;
   temp->next    = _backend_list_head;
   strcpy(temp->backend_name, backend_name);

   _backend_list_head = temp;

   dot1x_unlock(_backend_list_lock, code);

   return 0;
}


int dot1x_priv_backend_unregister(const char *backend_name)
{
   dot1x_backend_priv_t *prev;
   dot1x_backend_priv_t *temp;
   unsigned int         code;

   dot1x_lock(_backend_list_lock, code);

   temp = _backend_list_head;
   prev = NULL;

   while (temp != NULL)
   {
      if (!strcmp(temp->backend_name, backend_name))
      {
         if (prev != NULL)
         {
            prev->next = temp->next;
         }
         else
         {
            _backend_list_head = temp->next;
         }
         dot1x_heap_free(temp);
         dot1x_unlock(_backend_list_lock, code);
         return 0;
      }
      prev = temp;
      temp = temp->next;
   }

   dot1x_unlock(_backend_list_lock, code);

   return -1;
}


const dot1x_backend_t *dot1x_priv_backend_get(const char *name)
{
   dot1x_backend_priv_t  *temp;
   unsigned int          code;
   const dot1x_backend_t *backend;

   dot1x_lock(_backend_list_lock, code);

   temp = _backend_list_head;

   while (temp != NULL)
   {
      if (!strcmp(temp->backend_name, name))
      {
         backend = temp->backend;
         dot1x_unlock(_backend_list_lock, code);
         return backend;
      }
      temp = temp->next;
   }

   dot1x_unlock(_backend_list_lock, code);

   return NULL;
}
