/********************************************************************************
* MeshDynamics
* --------------
* File     : dot1x-backend-radius.c
* Comments : 802.1x Radius Client Backend
* Created  : 1/28/2005
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* | 12  |6/16/2008 | Lock primitives changed                         | Sriram |
* -----------------------------------------------------------------------------
* | 11  |7/25/2007 | Use GEN2K Buffer instead of Heap                | Sriram |
* -----------------------------------------------------------------------------
* | 10  |7/17/2007 | 2K Buffer used for RADIUS TX                    | Sriram |
* -----------------------------------------------------------------------------
* |  9  |7/16/2007 | _reset_port_context Added                       | Sriram |
* -----------------------------------------------------------------------------
* |  8  |7/16/2007 | Packet freed after TX in _send_eap_frame        | Sriram |
* -----------------------------------------------------------------------------
* |  7  |7/13/2007 | _RADIUS_MAX_ID defined                          | Sriram |
* -----------------------------------------------------------------------------
* |  6  |7/13/2007 | Bitmap position changed to unsigned int         | Sriram |
* -----------------------------------------------------------------------------
* |  5  |7/13/2007 | Radius RX/TX messages printed                   | Sriram |
* -----------------------------------------------------------------------------
* |  4  |2/8/2007  | Fixes for In-direct VLAN (NPS)                  | Sriram |
* -----------------------------------------------------------------------------
* |  3  |12/6/2006 | Changes for In-direct VLAN (NPS)                | Sriram |
* -----------------------------------------------------------------------------
* |  2  |4/7/2005  | Misc changes                                    | Sriram |
* -----------------------------------------------------------------------------
* |  1  |4/6/2005  | Misc changes                                    | Sriram |
* -----------------------------------------------------------------------------
* |  0  |1/28/2005 | Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/


#include "dot1x.h"
#include "dot1x-impl.h"
#include "dot1x-priv.h"
#include "dot1x-backend-radius.h"
#include "dot1x-backend-radius-impl.h"
#include "dot1x-fsm-auth-pae.h"
#include "dot1x-fsm-backend-auth.h"
#include "dot1x-fsm-auth-key-tx.h"

#define _RADIUS_POSITION_CODE               0
#define _RADIUS_POSITION_IDENTIFIER         1
#define _RADIUS_POSITION_LENGTH             2
#define _RADIUS_POSITION_AUTHENTICATOR      4
#define _RADIUS_POSITION_ATTRIBUTES         20

#define _RADIUS_CODE_ACCESS_REQUEST         1
#define _RADIUS_CODE_ACCESS_ACCEPT          2
#define _RADIUS_CODE_ACCESS_REJECT          3
#define _RADIUS_CODE_ACCOUNTING_REQUEST     4
#define _RADIUS_CODE_ACCOUNTING_RESPONSE    5
#define _RADIUS_CODE_ACCESS_CHALLENGE       11

#define _RADIUS_ATTR_USER_NAME              1
#define _RADIUS_ATTR_USER_PASSWD            2
#define _RADIUS_ATTR_NAS_IP_ADDRESS         4
#define _RADIUS_ATTR_NAS_PORT               5
#define _RADIUS_ATTR_SERVICE_TYPE           6
#define _RADIUS_ATTR_FRAMED_MTU             12
#define _RADIUS_ATTR_REPLY_MESSAGE          18
#define _RADIUS_ATTR_STATE                  24
#define _RADIUS_ATTR_CLASS                  25
#define _RADIUS_ATTR_VENDOR_SPECIFIC        26
#define _RADIUS_ATTR_SESSION_TIMEOUT        27
#define _RADIUS_ATTR_CALLED_STATION_ID      30
#define _RADIUS_ATTR_CALLING_STATION_ID     31
#define _RADIUS_ATTR_NAS_IDENTIFIER         32
#define _RADIUS_ATTR_EGRESS_VLAN_ID         56
#define _RADIUS_ATTR_NAS_PORT_TYPE          61
#define _RADIUS_ATTR_CONNECT_INFO           77
#define _RADIUS_ATTR_EAP_MESSAGE            79
#define _RADIUS_ATTR_MESSAGE_AUTH           80
#define _RADIUS_ATTR_NAS_PORT_ID            87

#define _RADIUS_SERVICE_TYPE_FRAMED         2
#define _RADIUS_NAS_PORT_TYPE_WIRELESS      19

#define _RADIUS_VENDOR_MICROSOFT            311
#define _RADIUS_MPPE_KEYTYPE_SEND           16                  /* MPPE-Send-Key */
#define _RADIUS_MPPE_KEYTYPE_RECV           17                  /* MPPE-Recv-Key */

#define _RADIUS_EGRESS_VLAN_TAGGED          0x31
#define _RADIUS_EGRESS_VLAN_UNTAGGED        0x32

/**
 *
 *    The Egress-VLANID attribute value is 4 octets in length
 *    transmitted from left to right:
 *
 *     0 1 2 3 4 5 6 7|8 9 0 1 2 3 4 5|6 7 8 9 0 1 2 3|4 5 6 7 8 9 0 1
 +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 |  Tag Indic.   |        Pad            |       VLANID          |
 +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 | 0x31 or 0x32  |0 0 0 0 0 0 0 0|0 0 0 0       VLANID           |
 +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 |
 |    The Tag Indication field is one octet in length and indicates
 |    whether the frames on the VLAN are tagged (0x31) or untagged
 |    (0x32).  The Pad field is 12 bits in length and MUST be 0 (zero).
 |    The VLANID is 12 bits in length and contains the [IEEE-802.1Q]
 |    VLAN VID value.
 |
 |        Refer to RFC 4675 (RADIUS Attributes for Virtual LAN and Priority Support) for details
 */


static int _create_instance(dot1x_t *instance, void *backend_param);
static int _create_port_context(dot1x_port_t *port);
static int _send_eap_frame(dot1x_port_t *port, dot1x_eap_frame_t *eap_frame);
static int _destroy_port_context(dot1x_port_t *port);
static int _destroy_instance(dot1x_t *instance);
static int _reset_port_context(dot1x_port_t *port);

static const dot1x_backend_t _backend =
{
   _create_instance,
   _create_port_context,
   _reset_port_context,
   _send_eap_frame,
   _destroy_port_context,
   _destroy_instance
};

struct _dot1x_backend_port
{
   dot1x_port_t               *port;
   unsigned long              timestamp;
   unsigned char              authenticator[16];
   int                        state_info_len;
   unsigned char              *state_info;
   int                        class_info_len;
   unsigned char              *class_info;
   int                        id;                                                                               /** -1 means invalid */
   dot1x_radius_backend_key_t tx_key;
   dot1x_radius_backend_key_t rx_key;
   short                      vlan_tag;                                                         /** -1 means no Tag */
};

typedef struct _dot1x_backend_port   _dot1x_backend_port_t;

#define _RADIUS_MAX_ID    127

static unsigned int          _bitmap_position;
static _dot1x_backend_port_t *_identifier_map[_RADIUS_MAX_ID + 1];
static unsigned int          _lock;

struct _dot1x_backend_instance
{
   dot1x_radius_backend_param_t param;
};

typedef struct _dot1x_backend_instance   _dot1x_backend_instance_t;

int dot1x_radius_backend_initialize(void)
{
   dot1x_register_backend("radius", &_backend);

   memset(_identifier_map, 0, sizeof(_identifier_map));
   _bitmap_position = 0;
   _lock            = dot1x_create_lock();

   dot1x_backend_radius_init();

   dot1x_print(DOT1X_PRINT_LEVEL_INFORMATION, "dot1x-backend_radius: Initialized\n");

   return 0;
}


int dot1x_radius_backend_uninitialize(void)
{
   dot1x_destroy_lock(_lock);
   dot1x_unregister_backend("radius");
   dot1x_backend_radius_uninit();
   dot1x_print(DOT1X_PRINT_LEVEL_INFORMATION, "dot1x-backend_radius: Un-initialized\n");
   return 0;
}


static int _create_instance(dot1x_t *instance, void *backend_param)
{
   dot1x_priv_t *priv;
   dot1x_radius_backend_param_t *radius_param;
   _dot1x_backend_instance_t    *backend_instance;

   radius_param = (dot1x_radius_backend_param_t *)backend_param;
   priv         = (dot1x_priv_t *)instance;

   backend_instance = (_dot1x_backend_instance_t *)dot1x_heap_alloc(sizeof(_dot1x_backend_instance_t));

   memset(backend_instance, 0, sizeof(_dot1x_backend_instance_t));

   memcpy(&backend_instance->param, radius_param, sizeof(dot1x_radius_backend_param_t));

   priv->backend_instance = backend_instance;

   dot1x_backend_radius_create_instance(instance, &backend_instance->param);

   return 0;
}


static int _create_port_context(dot1x_port_t *port)
{
   _dot1x_backend_port_t *backend_port;
   dot1x_port_priv_t     *priv_port;

   priv_port    = (dot1x_port_priv_t *)port;
   backend_port = (_dot1x_backend_port_t *)dot1x_heap_alloc(sizeof(_dot1x_backend_port_t));

   memset(backend_port, 0, sizeof(_dot1x_backend_port_t));

   backend_port->vlan_tag = -1;

   priv_port->backend_context = backend_port;
   backend_port->port         = port;

   dot1x_print(DOT1X_PRINT_LEVEL_INFORMATION,
               "dot1x-backend-radius: Created port context for %s:%02x-%02x-%02x-%02x-%02x-%02x\n",
               port->dot1x_instance->instance_name,
               port->address[0],
               port->address[1],
               port->address[2],
               port->address[3],
               port->address[4],
               port->address[5]);


   return 0;
}


static int _reset_port_context(dot1x_port_t *port)
{
   _dot1x_backend_port_t *backend_port;
   dot1x_port_priv_t     *priv_port;

   priv_port    = (dot1x_port_priv_t *)port;
   backend_port = (_dot1x_backend_port_t *)priv_port->backend_context;

   backend_port->vlan_tag = -1;

   if (backend_port->state_info != NULL)
   {
      backend_port->state_info_len = 0;
      dot1x_heap_free(backend_port->state_info);
      backend_port->state_info = NULL;
   }

   if (backend_port->class_info != NULL)
   {
      backend_port->class_info_len = 0;
      dot1x_heap_free(backend_port->class_info);
      backend_port->class_info = NULL;
   }

   dot1x_print(DOT1X_PRINT_LEVEL_INFORMATION,
               "dot1x-backend-radius: Reset port context for %s:%02x-%02x-%02x-%02x-%02x-%02x\n",
               port->dot1x_instance->instance_name,
               port->address[0],
               port->address[1],
               port->address[2],
               port->address[3],
               port->address[4],
               port->address[5]);

   return 0;
}


static int _destroy_port_context(dot1x_port_t *port)
{
   _dot1x_backend_port_t *backend_port;
   dot1x_port_priv_t     *priv_port;
   unsigned long         lock_code;

   priv_port    = (dot1x_port_priv_t *)port;
   backend_port = (_dot1x_backend_port_t *)priv_port->backend_context;

   dot1x_lock(_lock, lock_code);

   if ((backend_port->id != -1) &&
       (backend_port->id >= 0) && (backend_port->id <= _RADIUS_MAX_ID))
   {
      _identifier_map[backend_port->id] = NULL;
   }

   if (backend_port->state_info != NULL)
   {
      dot1x_heap_free(backend_port->state_info);
   }

   if (backend_port->class_info != NULL)
   {
      dot1x_heap_free(backend_port->class_info);
   }

   dot1x_heap_free(backend_port);

   priv_port->backend_context = NULL;

   dot1x_unlock(_lock, lock_code);

   return 0;
}


static int _destroy_instance(dot1x_t *instance)
{
   dot1x_priv_t *priv;

   priv = (dot1x_priv_t *)instance;

   dot1x_backend_radius_destroy_instance(instance);
   dot1x_heap_free(priv->backend_instance);

   return 0;
}


static DOT1X_INLINE int _reserve_attribute(unsigned char *pos, unsigned char type, int length)
{
   pos[0] = type;
   pos[1] = 2 + length;
   memset(pos + 2, 0, length);
   return length + 2;
}


static DOT1X_INLINE int _set_attribute(unsigned char *pos,
                                       unsigned char type,
                                       int           length,
                                       unsigned char *value)
{
   pos[0] = type;
   pos[1] = 2 + length;
   memcpy(pos + 2, value, length);
   return length + 2;
}


static DOT1X_INLINE int _get_eap_message_attribute(unsigned char     *pos,
                                                   int               length,
                                                   dot1x_eap_frame_t **eap_frame)
{
   unsigned char     *p;
   int               total_length;
   unsigned char     *eap_buffer;
   unsigned char     *eap_ptr;
   dot1x_eap_frame_t eap_frame_temp;
   dot1x_eap_frame_t *eap_frame_ptr;
   int               eap_count;

   /** First find out total length */

   *eap_frame = NULL;
   eap_count  = 0;

   for (p = pos, total_length = 0; p < pos + length; p += p[1])
   {
      if (*p == _RADIUS_ATTR_EAP_MESSAGE)
      {
         total_length += (p[1] - 2);
         ++eap_count;
      }
   }

   if (eap_count > 1)
   {
      eap_count = eap_count;
   }

   if (total_length > 0)
   {
      eap_buffer = dot1x_get_2k_buffer();

      memset(eap_buffer, 0, total_length);

      for (p = pos, eap_ptr = eap_buffer; p < pos + length; p += p[1])
      {
         if (*p == _RADIUS_ATTR_EAP_MESSAGE)
         {
            memcpy(eap_ptr, &p[2], p[1] - 2);
            eap_ptr += (p[1] - 2);
         }
      }

      dot1x_eapol_eap_frame_process(eap_buffer, total_length, &eap_frame_temp);

      eap_frame_ptr = (dot1x_eap_frame_t *)dot1x_heap_alloc(sizeof(dot1x_eap_frame_t));
      memset(eap_frame_ptr, 0, sizeof(dot1x_eap_frame_t));

      memcpy(eap_frame_ptr, &eap_frame_temp, sizeof(dot1x_eap_frame_t));
      eap_frame_ptr->data = (unsigned char *)dot1x_heap_alloc(eap_frame_temp.data_length);
      memcpy(eap_frame_ptr->data, eap_frame_temp.data, eap_frame_temp.data_length);

      dot1x_release_2k_buffer(eap_buffer);

      *eap_frame = eap_frame_ptr;
   }

   return 0;
}


static DOT1X_INLINE int _set_eap_message_attribute(unsigned char *pos, dot1x_eap_frame_t *eap_frame)
{
   unsigned char *eap_buf;
   int           eap_len;
   int           eap_temp;
   int           eap_pos;
   unsigned char *p;

   p       = pos;
   eap_pos = 0;
   eap_len = 4 + eap_frame->data_length + 1;

   eap_buf = dot1x_get_2k_buffer();

   dot1x_eapol_eap_frame_create(eap_frame, eap_buf);

   while (eap_len > 0)
   {
      if (eap_len > 253)
      {
         eap_temp = 253;
      }
      else
      {
         eap_temp = eap_len;
      }

      p       += _set_attribute(p, _RADIUS_ATTR_EAP_MESSAGE, eap_temp, eap_buf + eap_pos);
      eap_len -= eap_temp;
      eap_pos += eap_temp;
   }

   dot1x_release_2k_buffer(eap_buf);

   return(p - pos);
}


static DOT1X_INLINE int _set_integer(unsigned char *pos, unsigned char type, unsigned int value)
{
   value = htonl(value);
   return _set_attribute(pos, type, 4, (unsigned char *)&value);
}


static DOT1X_INLINE unsigned char *_get_attribute(unsigned char *pos, int length, unsigned char type, unsigned char *attr_length)
{
   unsigned char *p;

   for (p = pos; p < pos + length; p += p[1])
   {
      if (*p == type)
      {
         *attr_length = (p[1] - 2);
         return p;
      }
   }

   return NULL;
}


/**
 * Authenticates the message by using the authenticator
 * hash sent in the request.
 */
static DOT1X_INLINE int _check_integrity(dot1x_t                   *instance,
                                         _dot1x_backend_instance_t *backend_instance,
                                         _dot1x_backend_port_t     *backend_port,
                                         unsigned char             *packet,
                                         int                       length)
{
   unsigned char orig_hash[16];
   unsigned char hash[16];

   dot1x_crypt_md5_digest_open(instance);

   memcpy(orig_hash, &packet[_RADIUS_POSITION_AUTHENTICATOR], 16);
   memcpy(&packet[_RADIUS_POSITION_AUTHENTICATOR], backend_port->authenticator, 16);
   dot1x_crypt_md5_digest(instance, packet, length);
   dot1x_crypt_md5_digest(instance, backend_instance->param.radius_secret, backend_instance->param.radius_secret_len);

   dot1x_crypt_md5_digest_close(instance, hash);

   memcpy(&packet[_RADIUS_POSITION_AUTHENTICATOR], orig_hash, 16);

   return(memcmp(&packet[_RADIUS_POSITION_AUTHENTICATOR], hash, 16) == 0);
}


/**
 *
 */
static DOT1X_INLINE int _check_auth(dot1x_t                   *instance,
                                    _dot1x_backend_instance_t *backend_instance,
                                    _dot1x_backend_port_t     *backend_port,
                                    unsigned char             *packet,
                                    int                       length)
{
   unsigned char *mauth;
   unsigned char attr_length;
   unsigned char old_hmac_md5[16];
   unsigned char old_auth[16];
   unsigned char new_hmac_md5[16];
   int           ret;

   mauth = _get_attribute(&packet[_RADIUS_POSITION_ATTRIBUTES],
                          length - _RADIUS_POSITION_ATTRIBUTES,
                          _RADIUS_ATTR_MESSAGE_AUTH,
                          &attr_length);

   if (mauth == NULL)
   {
      /** TODO log the error and get out */
      return -1;
   }

   if (attr_length != 16)
   {
      /** TODO log the error and get out */
      return -1;
   }

   /** Save the HMAC MD5 value in the attribute */
   memcpy(old_hmac_md5, mauth + 2, 16);
   /** Zero it up*/
   memset(mauth + 2, 0, 16);
   /** Copy into the RADIUS authenticator HASH the request authenticator */
   memcpy(old_auth, &packet[_RADIUS_POSITION_AUTHENTICATOR], 16);
   memcpy(&packet[_RADIUS_POSITION_AUTHENTICATOR], backend_port->authenticator, 16);

   dot1x_crypt_hmac_md5(instance,
                        packet,
                        length,
                        backend_instance->param.radius_secret,
                        backend_instance->param.radius_secret_len,
                        new_hmac_md5);

   ret = (memcmp(old_hmac_md5, new_hmac_md5, 16) == 0);

   /** Restore modified parts of the packet */
   memcpy(mauth + 2, old_hmac_md5, 16);
   memcpy(&packet[_RADIUS_POSITION_AUTHENTICATOR], old_auth, 16);

   return ret;
}


static DOT1X_INLINE int _extract_mppe_key(unsigned char         type,
                                          unsigned char         length,
                                          unsigned short        salt,
                                          unsigned char         *string,
                                          _dot1x_backend_port_t *backend_port)
{
#define _roundup(x, y)    ((((x) + ((y) - 1)) / (y)) * (y)) /* to any y */

   int                       key_length;
   int                       rounded_length;
   unsigned char             buffer[64];
   unsigned char             hash[16];
   unsigned char             *pk;
   int                       i, j;
   dot1x_priv_t              *priv;
   _dot1x_backend_instance_t *backend_instance;

   priv             = (dot1x_priv_t *)backend_port->port->dot1x_instance;
   backend_instance = (_dot1x_backend_instance_t *)priv->backend_instance;
   key_length       = length - 1 /* type */ - 1 /* len */ - 2 /* salt */;
   rounded_length   = _roundup(key_length, 16);

   if (rounded_length > 64)
   {
      /** TODO: LOG */
      return -1;
   }

   pk = NULL;

   memset(buffer, 0, sizeof(buffer));
   memcpy(buffer, string, key_length);

   switch (type)
   {
   case _RADIUS_MPPE_KEYTYPE_SEND:
      if (backend_port->rx_key.valid)
      {
         if (backend_port->rx_key.salt == salt)
         {
            /** TODO: LOG */
            return -1;
         }
      }
      pk = backend_port->tx_key.key;
      backend_port->tx_key.valid = 1;
      break;

   case _RADIUS_MPPE_KEYTYPE_RECV:
      if (backend_port->tx_key.valid)
      {
         if (backend_port->tx_key.salt == salt)
         {
            /** TODO: LOG */
            return -1;
         }
      }
      pk = backend_port->rx_key.key;
      backend_port->rx_key.valid = 1;
      break;

   default:
      return -1;
   }

   salt = htons(salt);

   for (i = 0; i < rounded_length; i += 16)
   {
      dot1x_crypt_md5_digest_open(backend_port->port->dot1x_instance);

      dot1x_crypt_md5_digest(backend_port->port->dot1x_instance,
                             backend_instance->param.radius_secret,
                             backend_instance->param.radius_secret_len);
      if (!i)
      {
         dot1x_crypt_md5_digest(backend_port->port->dot1x_instance,
                                backend_port->authenticator,
                                16);
         dot1x_crypt_md5_digest(backend_port->port->dot1x_instance,
                                (unsigned char *)&salt,
                                2);
      }
      else
      {
         dot1x_crypt_md5_digest(backend_port->port->dot1x_instance,
                                &buffer[i - 16],
                                16);
      }

      dot1x_crypt_md5_digest_close(backend_port->port->dot1x_instance, hash);

      for (j = 0; j < 16; j++)
      {
         pk[i + j] = buffer[i + j] ^ hash[j];
      }
   }

   return 0;

#undef _roundup
}


static DOT1X_INLINE int _extract_keys(unsigned char *pos, int length, _dot1x_backend_port_t *backend_port)
{
   unsigned char  *p;
   unsigned char  key_length;                           /** type (1) + len (1) + salt (2) + key */
   unsigned long  vendor_id;
   unsigned char  *attr_data;
   unsigned short salt;

   backend_port->rx_key.valid = 0;
   backend_port->tx_key.valid = 0;

   for (p = pos; p < pos + length; p += p[1])
   {
      if ((backend_port->rx_key.valid == 1) &&
          (backend_port->tx_key.valid == 1))
      {
         break;
      }

      if (p[0] != _RADIUS_ATTR_VENDOR_SPECIFIC)
      {
         continue;
      }

      attr_data = p + 2;

      memcpy(&vendor_id, attr_data, 4);
      vendor_id = ntohl(vendor_id);

      if (vendor_id != _RADIUS_VENDOR_MICROSOFT)
      {
         continue;
      }

      attr_data += 4;

      if ((attr_data[0] != _RADIUS_MPPE_KEYTYPE_SEND) &&
          (attr_data[0] != _RADIUS_MPPE_KEYTYPE_RECV))
      {
         continue;
      }

      key_length = attr_data[1];
      memcpy(&salt, attr_data + 2, 2);
      salt = ntohs(salt);

      if (!(salt & 0x8000))
      {
         /** TODO: log */
         continue;
      }

      _extract_mppe_key(attr_data[0], key_length, salt, attr_data + 4, backend_port);
   }

   return(backend_port->rx_key.valid == 1 && backend_port->tx_key.valid == 1);
}


static DOT1X_INLINE int _get_unused_id(_dot1x_backend_port_t *port)
{
   int           pos;
   unsigned char flag;
   unsigned long lock_code;
   unsigned long timestamp;

   dot1x_lock(_lock, lock_code);

_lb_back:

   for (pos = _bitmap_position; pos <= _RADIUS_MAX_ID; pos++)
   {
      if (_identifier_map[pos] == NULL)
      {
         _bitmap_position = pos + 1;
         if (_bitmap_position > _RADIUS_MAX_ID)
         {
            _bitmap_position = 0;
         }
         _identifier_map[pos]            = port;
         _identifier_map[pos]->timestamp = dot1x_timestamp();
         port->id = pos;
         break;
      }
   }

   if (pos == _RADIUS_MAX_ID + 1)
   {
      /**
       * Couldn't find an unused ID starting at the current position
       * See if we can timeout any previous requests and try again.
       */

      flag = 0;
      for (pos = 0; pos <= _RADIUS_MAX_ID; pos++)
      {
         if (_identifier_map[pos] != NULL)
         {
            timestamp = dot1x_timestamp();
            if (timestamp - _identifier_map[pos]->timestamp > DOT1X_BACKEND_RADIUS_TIMEOUT)
            {
               _identifier_map[pos] = NULL;
               flag = 1;
            }
         }
      }

      _bitmap_position = 0;

      if (flag)
      {
         goto _lb_back;
      }

      pos = -1;
   }

   dot1x_unlock(_lock, lock_code);

   return pos;
}


static DOT1X_INLINE int _create_radius_packet(dot1x_port_priv_t     *priv_port,
                                              _dot1x_backend_port_t *backend_port,
                                              dot1x_eap_frame_t     *eap_frame,
                                              int                   id,
                                              unsigned char         *packet,
                                              unsigned short        *packet_length_out)
{
   unsigned short            length;
   unsigned char             *p;
   unsigned char             *mauth_attr;
   dot1x_fsm_auth_pae_t      *fsm_pae;
   unsigned char             hmac[16];
   char                      essid[64];
   unsigned char             addr[6];
   char                      temp[256];
   unsigned short            aid;
   _dot1x_backend_instance_t *backend_instance;
   dot1x_priv_t              *priv;

   priv             = (dot1x_priv_t *)priv_port->base.dot1x_instance;
   backend_instance = (_dot1x_backend_instance_t *)priv->backend_instance;

   dot1x_get_instance_essid(priv_port->base.dot1x_instance, essid);

   fsm_pae = (dot1x_fsm_auth_pae_t *)dot1x_fsm_get(&priv_port->base, DOT1X_FSM_CODE_AUTHENTICATOR_PAE, 0);

   memset(packet, 0, 2048);

   packet[_RADIUS_POSITION_CODE]       = _RADIUS_CODE_ACCESS_REQUEST;
   packet[_RADIUS_POSITION_IDENTIFIER] = (unsigned char)id;

   /** Skip length for now */

   dot1x_get_random_bytes(backend_port->authenticator, sizeof(backend_port->authenticator));
   memcpy(&packet[_RADIUS_POSITION_AUTHENTICATOR], backend_port->authenticator, sizeof(backend_port->authenticator));

   /**
    * Write out the attributes
    */

   p = &packet[_RADIUS_POSITION_ATTRIBUTES];

   /**
    * Reserver space for Message authenticator attribute
    */

   mauth_attr = p;
   p         += _reserve_attribute(p, _RADIUS_ATTR_MESSAGE_AUTH, 16);

   p += _set_integer(p, _RADIUS_ATTR_SERVICE_TYPE, _RADIUS_SERVICE_TYPE_FRAMED);

   /** User-Name attribute */

   if ((fsm_pae->identity_data != NULL) &&
       (fsm_pae->identity_length > 0))
   {
      p += _set_attribute(p, _RADIUS_ATTR_USER_NAME, fsm_pae->identity_length, fsm_pae->identity_data);
   }

   /** Framed-MTU attribute */

   p += _set_integer(p, _RADIUS_ATTR_FRAMED_MTU, 1492);

   /**
    * If in a response, we get state information, then send it back
    * to the server
    */

   if (backend_port->state_info_len != 0)
   {
      p += _set_attribute(p, _RADIUS_ATTR_STATE, backend_port->state_info_len, backend_port->state_info);
   }

   /**
    * If in a response, we get class information, then send it back
    * to the server
    */

   if (backend_port->class_info_len != 0)
   {
      p += _set_attribute(p, _RADIUS_ATTR_STATE, backend_port->class_info_len, backend_port->class_info);
   }

   /** Called-Station attribute */

   dot1x_get_instance_addr(priv_port->base.dot1x_instance, &priv_port->base, addr);
   sprintf(temp,
           "%02x-%02x-%02x-%02x-%02x-%02x:%s",
           addr[0], addr[1], addr[2], addr[3], addr[4], addr[5],
           essid);
   p += _set_attribute(p, _RADIUS_ATTR_CALLED_STATION_ID, strlen(temp), (unsigned char *)temp);

   /** Calling-Station-ID attribute */
   sprintf(temp,
           "%02x-%02x-%02x-%02x-%02x-%02x",
           backend_port->port->address[0],
           backend_port->port->address[1],
           backend_port->port->address[2],
           backend_port->port->address[3],
           backend_port->port->address[4],
           backend_port->port->address[5]);

   p += _set_attribute(p, _RADIUS_ATTR_CALLING_STATION_ID, strlen(temp), (unsigned char *)temp);

   /** NAS-Identifier attribute */
   dot1x_get_hostname(temp, 255);
   p += _set_attribute(p, _RADIUS_ATTR_NAS_IDENTIFIER, strlen(temp), temp);

   /** NAS-Port-Type attribute */
   p += _set_integer(p, _RADIUS_ATTR_NAS_PORT_TYPE, _RADIUS_NAS_PORT_TYPE_WIRELESS);

   /** Connect-Info attribute */
   dot1x_get_port_connect_info(backend_port->port, temp);
   p += _set_attribute(p, _RADIUS_ATTR_CONNECT_INFO, strlen(temp), (unsigned char *)temp);

   /** Insert the eap frame into the radius packet */
   p += _set_eap_message_attribute(p, eap_frame);

   /** NAS-IP-Address attribute */
   dot1x_get_instance_ip_addr(backend_port->port->dot1x_instance, backend_port->port, addr);
   p += _set_attribute(p, _RADIUS_ATTR_NAS_IP_ADDRESS, 4, addr);

   /** NAS-Port attribute */
   aid = dot1x_get_port_aid(backend_port->port);
   p  += _set_integer(p, _RADIUS_ATTR_NAS_PORT, aid);

   /** NAS-Port-ID attribute */
   sprintf(temp, "STA port # %d", aid);
   p += _set_attribute(p, _RADIUS_ATTR_NAS_PORT_ID, strlen(temp), temp);

   length             = p - packet;
   *packet_length_out = p - packet;
   length             = htons(length);

   memcpy(&packet[_RADIUS_POSITION_LENGTH], &length, 2);

   dot1x_crypt_hmac_md5(priv_port->base.dot1x_instance,
                        packet,
                        p - packet,
                        backend_instance->param.radius_secret,
                        backend_instance->param.radius_secret_len,
                        hmac);

   memcpy(mauth_attr + 2, hmac, 16);

   return 0;
}


/**
 * Pack the eap frame into a RADIUS packet and send it to the server
 */
static int _send_eap_frame(dot1x_port_t *port, dot1x_eap_frame_t *eap_frame)
{
   dot1x_port_priv_t     *priv_port;
   _dot1x_backend_port_t *backend_port;
   int                       id;
   unsigned char             *packet;
   unsigned short            packet_length;
   _dot1x_backend_instance_t *backend_instance;
   int                       ret;

   priv_port        = (dot1x_port_priv_t *)port;
   backend_port     = (_dot1x_backend_port_t *)priv_port->backend_context;
   backend_instance = (_dot1x_backend_instance_t *)((dot1x_priv_t *)(port->dot1x_instance))->backend_instance;

   id = _get_unused_id(backend_port);

   if (id == -1)
   {
      dot1x_print(DOT1X_PRINT_LEVEL_INFORMATION,
                  "dot1x-backend-radius: NO ID for EAP-RADIUS request of %s:%02x-%02x-%02x-%02x-%02x-%02x\n",
                  port->dot1x_instance->instance_name,
                  port->address[0],
                  port->address[1],
                  port->address[2],
                  port->address[3],
                  port->address[4],
                  port->address[5]);
      return -1;
   }

   packet = dot1x_get_2k_buffer();

   if (_create_radius_packet(priv_port, backend_port, eap_frame, id, packet, &packet_length) != 0)
   {
      dot1x_release_2k_buffer(packet);
      return -1;
   }

   ret = dot1x_backend_radius_tx(port, &backend_instance->param, packet, packet_length);

   dot1x_print(DOT1X_PRINT_LEVEL_INFORMATION,
               "dot1x-backend-radius: Transmitting EAP-RADIUS request ID %d Length %d Ret %d for %s:%02x-%02x-%02x-%02x-%02x-%02x\n",
               id,
               packet_length,
               ret,
               port->dot1x_instance->instance_name,
               port->address[0],
               port->address[1],
               port->address[2],
               port->address[3],
               port->address[4],
               port->address[5]);

   dot1x_release_2k_buffer(packet);

   return ret;
}


int dot1x_radius_backend_rx(unsigned char *packet, int packet_length)
{
   unsigned char             id;
   _dot1x_backend_port_t     *backend_port;
   unsigned short            length;
   dot1x_priv_t              *priv;
   _dot1x_backend_instance_t *backend_instance;
   dot1x_eap_frame_t         *eap_frame;
   unsigned long             lock_code;
   unsigned char             *attr;
   unsigned char             attr_length;
   unsigned short            vlan_tag;
   dot1x_fsm_t               *backend_auth;

   eap_frame = NULL;

   id = packet[_RADIUS_POSITION_IDENTIFIER];

   dot1x_lock(_lock, lock_code);

   backend_port        = _identifier_map[id];
   _identifier_map[id] = NULL;

   if ((backend_port == NULL) ||
       (backend_port->id != id))
   {
      /** TODO log the error and get out */
      dot1x_unlock(_lock, lock_code);
      dot1x_print(DOT1X_PRINT_LEVEL_INFORMATION,
                  "dot1x-backend-radius: Received invalid RADIUS response ID %d\n",
                  id);
      return -1;
   }

   if (packet_length < _RADIUS_POSITION_ATTRIBUTES)
   {
      dot1x_unlock(_lock, lock_code);
      dot1x_print(DOT1X_PRINT_LEVEL_INFORMATION,
                  "dot1x-backend-radius: Received RADIUS response of invalid length ID %d\n",
                  id);
      return -1;
   }

   backend_port->id = -1;

   priv = (dot1x_priv_t *)backend_port->port->dot1x_instance;

   backend_instance = (_dot1x_backend_instance_t *)priv->backend_instance;
   backend_auth     = dot1x_fsm_get(backend_port->port, DOT1X_FSM_CODE_BACKEND_AUTHENTICATION, 0);

   memcpy(&length, &packet[_RADIUS_POSITION_LENGTH], 2);
   length = ntohs(length);

   dot1x_print(DOT1X_PRINT_LEVEL_INFORMATION,
               "dot1x-backend-radius: Received RADIUS response L=%d PL=%d ID %d for %s:%02x-%02x-%02x-%02x-%02x-%02x\n",
               length,
               packet_length,
               id,
               backend_port->port->dot1x_instance->instance_name,
               backend_port->port->address[0],
               backend_port->port->address[1],
               backend_port->port->address[2],
               backend_port->port->address[3],
               backend_port->port->address[4],
               backend_port->port->address[5]);

   /**
    * First check the integrity of the message
    */

   if (!_check_integrity(backend_port->port->dot1x_instance, backend_instance, backend_port, packet, length))
   {
      /** TODO log the error and get out */
      dot1x_unlock(_lock, lock_code);
      return -1;
   }

   if (packet[_RADIUS_POSITION_CODE] != _RADIUS_CODE_ACCESS_REJECT)
   {
      if (!_check_auth(backend_port->port->dot1x_instance, backend_instance, backend_port, packet, length))
      {
         /** TODO log the error and get out */
         dot1x_unlock(_lock, lock_code);
         return -1;
      }

      attr = _get_attribute(&packet[_RADIUS_POSITION_ATTRIBUTES],
                            packet_length - _RADIUS_POSITION_ATTRIBUTES,
                            _RADIUS_ATTR_STATE,
                            &attr_length);
      if (attr != NULL)
      {
         if (backend_port->state_info != NULL)
         {
            dot1x_heap_free(backend_port->state_info);
         }
         backend_port->state_info     = (unsigned char *)dot1x_heap_alloc(attr_length);
         backend_port->state_info_len = attr_length;
         memcpy(backend_port->state_info, attr + 2, attr_length);
      }

      attr = _get_attribute(&packet[_RADIUS_POSITION_ATTRIBUTES],
                            packet_length - _RADIUS_POSITION_ATTRIBUTES,
                            _RADIUS_ATTR_CLASS,
                            &attr_length);
      if (attr != NULL)
      {
         if (backend_port->class_info != NULL)
         {
            dot1x_heap_free(backend_port->class_info);
         }
         backend_port->class_info     = (unsigned char *)dot1x_heap_alloc(attr_length);
         backend_port->class_info_len = attr_length;
         memcpy(backend_port->class_info, attr + 2, attr_length);
      }

      if (packet[_RADIUS_POSITION_CODE] == _RADIUS_CODE_ACCESS_CHALLENGE)
      {
         length = length;
      }

      _get_eap_message_attribute(&packet[_RADIUS_POSITION_ATTRIBUTES],
                                 packet_length - _RADIUS_POSITION_ATTRIBUTES,
                                 &eap_frame);
   }

   switch (packet[_RADIUS_POSITION_CODE])
   {
   case _RADIUS_CODE_ACCESS_ACCEPT:

      /**
       * In accept responses
       *	1) The RADIUS server may send a Session-Timeout attribute
       *	   which we use as the re-authentication timer value
       *
       *	2) The RADIUS server in a Vendor-specific attribute will
       *	   send the 802.11i PMK using the MPPE
       */

      attr = _get_attribute(&packet[_RADIUS_POSITION_ATTRIBUTES],
                            packet_length - _RADIUS_POSITION_ATTRIBUTES,
                            _RADIUS_ATTR_SESSION_TIMEOUT,
                            &attr_length);

      if (attr != NULL)
      {
         unsigned int timeout;
         memcpy(&timeout, attr + 2, 4);
         timeout = ntohl(timeout);
         /** TODO1 set the Reauth timer */
      }

      /**
       * Read the Egress-VLAN-ID attribute to find out the VLAN tag
       */

      attr = _get_attribute(&packet[_RADIUS_POSITION_ATTRIBUTES],
                            packet_length - _RADIUS_POSITION_ATTRIBUTES,
                            _RADIUS_ATTR_EGRESS_VLAN_ID,
                            &attr_length);

      if ((attr != NULL) && (attr_length == 4))
      {
         if (attr[2 + 0] == _RADIUS_EGRESS_VLAN_TAGGED)
         {
            memcpy(&vlan_tag, &attr[2 + 2], 2);
            vlan_tag = ntohs(vlan_tag);
            backend_port->vlan_tag = vlan_tag;

            dot1x_print(DOT1X_PRINT_LEVEL_INFORMATION,
                        "dot1x-backend-radius: Received Egress-VLANID attribute with Tag %d for for %s:%02x-%02x-%02x-%02x-%02x-%02x\n",
                        vlan_tag,
                        backend_port->port->dot1x_instance->instance_name,
                        backend_port->port->address[0],
                        backend_port->port->address[1],
                        backend_port->port->address[2],
                        backend_port->port->address[3],
                        backend_port->port->address[4],
                        backend_port->port->address[5]);
         }
         else
         {
            dot1x_print(DOT1X_PRINT_LEVEL_INFORMATION,
                        "dot1x-backend-radius: Received Egress-VLANID attribute with Untagged indicator for for %s:%02x-%02x-%02x-%02x-%02x-%02x\n",
                        backend_port->port->dot1x_instance->instance_name,
                        backend_port->port->address[0],
                        backend_port->port->address[1],
                        backend_port->port->address[2],
                        backend_port->port->address[3],
                        backend_port->port->address[4],
                        backend_port->port->address[5]);
         }
      }

      _extract_keys(&packet[_RADIUS_POSITION_ATTRIBUTES],
                    packet_length - _RADIUS_POSITION_ATTRIBUTES,
                    backend_port);

      if (backend_auth != NULL)
      {
         backend_auth->trigger(backend_port->port,
                               DOT1X_FSM_CODE_BACKEND_AUTHENTICATION,
                               0,
                               backend_auth,
                               DOT1X_FSM_BACKEND_AUTH_TRIGGER_ASUCCESS);
      }

      if ((backend_port->rx_key.valid == 1) && (backend_port->tx_key.valid == 1))
      {
         dot1x_fsm_t *fsm_auth_key_tx;

         fsm_auth_key_tx = dot1x_fsm_get(backend_port->port, DOT1X_FSM_CODE_AUTHENTICATOR_KEY_TRANSMIT, 0);

         if (fsm_auth_key_tx != NULL)
         {
            fsm_auth_key_tx->trigger(backend_port->port,
                                     DOT1X_FSM_CODE_AUTHENTICATOR_KEY_TRANSMIT,
                                     0,
                                     fsm_auth_key_tx,
                                     DOT1X_FSM_AUTH_KEY_TX_TRIGGER_KEY_AVAILABLE);
         }
      }

      if (eap_frame != NULL)
      {
         dot1x_heap_free(eap_frame->data);
         dot1x_heap_free(eap_frame);
      }

      break;

   case _RADIUS_CODE_ACCESS_REJECT:
      if (backend_auth != NULL)
      {
         backend_auth->trigger(backend_port->port,
                               DOT1X_FSM_CODE_BACKEND_AUTHENTICATION,
                               0,
                               backend_auth,
                               DOT1X_FSM_BACKEND_AUTH_TRIGGER_AFAIL);
      }
      if (eap_frame != NULL)
      {
         dot1x_heap_free(eap_frame->data);
         dot1x_heap_free(eap_frame);
      }
      break;

   case _RADIUS_CODE_ACCESS_CHALLENGE:
      if ((backend_auth != NULL) && (eap_frame != NULL))
      {
         backend_auth->trigger(backend_port->port,
                               DOT1X_FSM_CODE_BACKEND_AUTHENTICATION,
                               (int)eap_frame,
                               backend_auth,
                               DOT1X_FSM_BACKEND_AUTH_TRIGGER_AREQ);
      }
      break;

   default:
      /** TODO log */
      if (eap_frame != NULL)
      {
         dot1x_heap_free(eap_frame->data);
         dot1x_heap_free(eap_frame);
      }
      break;
   }

   dot1x_unlock(_lock, lock_code);

   return 0;
}


int dot1x_radius_backend_get_rx_key(dot1x_port_t *port, dot1x_radius_backend_key_t *key)
{
   _dot1x_backend_port_t *backend_port;
   dot1x_port_priv_t     *priv_port;
   unsigned long         lock_code;

   dot1x_lock(_lock, lock_code);

   priv_port    = (dot1x_port_priv_t *)port;
   backend_port = (_dot1x_backend_port_t *)priv_port->backend_context;

   memcpy(key, &backend_port->rx_key, sizeof(dot1x_radius_backend_key_t));

   dot1x_unlock(_lock, lock_code);

   return 0;
}


int dot1x_radius_backend_get_tx_key(dot1x_port_t *port, dot1x_radius_backend_key_t *key)
{
   _dot1x_backend_port_t *backend_port;
   dot1x_port_priv_t     *priv_port;
   unsigned long         lock_code;

   dot1x_lock(_lock, lock_code);

   priv_port    = (dot1x_port_priv_t *)port;
   backend_port = (_dot1x_backend_port_t *)priv_port->backend_context;

   memcpy(key, &backend_port->tx_key, sizeof(dot1x_radius_backend_key_t));

   dot1x_unlock(_lock, lock_code);

   return 0;
}


int dot1x_radius_backend_get_vlan_tag(dot1x_port_t *port)
{
   _dot1x_backend_port_t *backend_port;
   dot1x_port_priv_t     *priv_port;
   unsigned long         lock_code;
   int ret_val;

   dot1x_lock(_lock, lock_code);

   priv_port    = (dot1x_port_priv_t *)port;
   backend_port = (_dot1x_backend_port_t *)priv_port->backend_context;

   ret_val = backend_port->vlan_tag;

   dot1x_unlock(_lock, lock_code);

   return ret_val;
}
