/********************************************************************************
* MeshDynamics
* --------------
* File     : dot1x-fsm-backend-auth.c
* Comments : 802.1x Backend Authenticator FSM
* Created  : 1/27/2005
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  3  |6/16/2008 | Lock primitives changed                         | Sriram |
* -----------------------------------------------------------------------------
* |  2  |7/25/2007 | Use GEN2K Buffer instead of Heap                | Sriram |
* -----------------------------------------------------------------------------
* |  1  |7/13/2007 | dot1x_auth_pae_fsm_reset Added                  | Sriram |
* -----------------------------------------------------------------------------
* |  0  |1/27/2005 | Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

#include "dot1x.h"
#include "dot1x-impl.h"
#include "dot1x-priv.h"
#include "dot1x-fsm-backend-auth.h"
#include "dot1x-fsm-auth-pae.h"

/**
 * FSM functions
 */

static int _tick(dot1x_port_t *port, int fsm_code, int other_code, struct dot1x_fsm *fsm);
static int _eapol_rx_begin(dot1x_port_t *port, int fsm_code, int other_code, struct dot1x_fsm *fsm);
static int _eapol_rx(dot1x_port_t *port, int fsm_code, int other_code, struct dot1x_fsm *fsm, dot1x_eapol_frame_t *frame);
static int _eapol_rx_end(dot1x_port_t *port, int fsm_code, int other_code, struct dot1x_fsm *fsm);
static int _trigger(dot1x_port_t *port, int fsm_code, int other_code, struct dot1x_fsm *fsm, int trigger_code);
static int _initialize(dot1x_port_t *port, int fsm_code, int other_code, struct dot1x_fsm *fsm);
static int _on_destroy(dot1x_port_t *port, struct dot1x_fsm *fsm);

/**
 * Internal functions
 */

static void _on_state_entry(dot1x_port_t *port, dot1x_fsm_backend_auth_t *fsm_backend, dot1x_fsm_t **trigger_fsm, int *fsm_code, int *other_code, int *trigger_code);
static void _attempt_transition(dot1x_port_t *port, dot1x_fsm_backend_auth_t *fsm_backend, dot1x_fsm_t **trigger_fsm, int *fsm_code, int *other_code, int *trigger_code);

static int _txReq(dot1x_port_t *port, dot1x_fsm_backend_auth_t *fsm_backend);
static int _sendRespToServer(dot1x_port_t *port, dot1x_fsm_backend_auth_t *fsm_backend);
static int _txCannedSuccess(dot1x_port_t *port, dot1x_fsm_backend_auth_t *fsm_backend);
static int _txCannedFail(dot1x_port_t *port, dot1x_fsm_backend_auth_t *fsm_backend);
static int _abortAuth(dot1x_port_t *port, dot1x_fsm_backend_auth_t *fsm_backend);

static int _eapol_rx_eap(dot1x_port_t *port, dot1x_fsm_backend_auth_t *fsm_backend, dot1x_eapol_frame_t *frame);

static DOT1X_INLINE void _clear_rxResp(dot1x_fsm_backend_auth_t *fsm_backend);

int dot1x_backend_auth_fsm_create(dot1x_port_t *port)
{
   dot1x_fsm_backend_auth_t *fsm_backend;
   int err;

   fsm_backend = (dot1x_fsm_backend_auth_t *)dot1x_heap_alloc(sizeof(dot1x_fsm_backend_auth_t));

   memset(fsm_backend, 0, sizeof(dot1x_fsm_backend_auth_t));

   fsm_backend->fsm.tick           = _tick;
   fsm_backend->fsm.eapol_rx_begin = _eapol_rx_begin;
   fsm_backend->fsm.eapol_rx       = _eapol_rx;
   fsm_backend->fsm.eapol_rx_end   = _eapol_rx_end;
   fsm_backend->fsm.trigger        = _trigger;
   fsm_backend->fsm.initialize     = _initialize;
   fsm_backend->fsm.on_destroy     = _on_destroy;

   fsm_backend->lock  = dot1x_create_lock();
   fsm_backend->state = DOT1X_FSM_BACKEND_AUTH_STATE_INVALID;

   err = dot1x_fsm_register(port, DOT1X_FSM_CODE_BACKEND_AUTHENTICATION, 0, &fsm_backend->fsm);

   if (err)
   {
      dot1x_destroy_lock(fsm_backend->lock);
      dot1x_heap_free(fsm_backend);
   }

   dot1x_print(DOT1X_PRINT_LEVEL_FLOW,
               "dot1x-auth: Backend Authentication FSM initialized for %s:%02x-%02x-%02x-%02x-%02x-%02x\n",
               port->dot1x_instance->instance_name,
               port->address[0],
               port->address[1],
               port->address[2],
               port->address[3],
               port->address[4],
               port->address[5]);

   return err;
}


int dot1x_backend_auth_fsm_reset(dot1x_port_t *port)
{
   dot1x_fsm_backend_auth_t *fsm_backend;

   fsm_backend = (dot1x_fsm_backend_auth_t *)dot1x_fsm_get(port, DOT1X_FSM_CODE_BACKEND_AUTHENTICATION, 0);

   fsm_backend->state = DOT1X_FSM_BACKEND_AUTH_STATE_INVALID;

   dot1x_print(DOT1X_PRINT_LEVEL_FLOW,
               "dot1x-auth: Backend Authentication FSM reset for %s:%02x-%02x-%02x-%02x-%02x-%02x\n",
               port->dot1x_instance->instance_name,
               port->address[0],
               port->address[1],
               port->address[2],
               port->address[3],
               port->address[4],
               port->address[5]);

   return 0;
}


int dot1x_backend_auth_fsm_destroy(dot1x_port_t *port)
{
   dot1x_fsm_t *fsm;

   fsm = dot1x_fsm_get(port, DOT1X_FSM_CODE_BACKEND_AUTHENTICATION, 0);

   if (fsm != NULL)
   {
      _on_destroy(port, fsm);
   }

   dot1x_fsm_unregister(port, DOT1X_FSM_CODE_BACKEND_AUTHENTICATION, 0);

   return 0;
}


static void _on_state_entry(dot1x_port_t *port, dot1x_fsm_backend_auth_t *fsm_backend, dot1x_fsm_t **trigger_fsm, int *fsm_code, int *other_code, int *trigger_code)
{
   dot1x_port_priv_t *priv_port;

   priv_port = (dot1x_port_priv_t *)port;

   switch (fsm_backend->state)
   {
   case DOT1X_FSM_BACKEND_AUTH_STATE_REQUEST:
      fsm_backend->idFromServer        = fsm_backend->aReqFrame->id;
      priv_port->fsm_globals.currentId = fsm_backend->idFromServer;
      _txReq(port, fsm_backend);
      priv_port->fsm_globals.aWhile = priv_port->suppTimeout;
      ++fsm_backend->reqCount;
      break;

   case DOT1X_FSM_BACKEND_AUTH_STATE_RESPONSE:
      fsm_backend->aReq     = False;
      fsm_backend->aSuccess = False;
      priv_port->fsm_globals.authTimeout = False;
      fsm_backend->rxResp           = False;
      fsm_backend->aFail            = False;
      priv_port->fsm_globals.aWhile = priv_port->serverTimeout;
      fsm_backend->reqCount         = 0;
      _sendRespToServer(port, fsm_backend);
      break;

   case DOT1X_FSM_BACKEND_AUTH_STATE_SUCCESS:
      priv_port->fsm_globals.currentId = fsm_backend->idFromServer;
      _txCannedSuccess(port, fsm_backend);
      fsm_backend->state = DOT1X_FSM_BACKEND_AUTH_STATE_IDLE;

      /**
       * Setup a trigger for Authenticator PAE state machine
       */
      *trigger_fsm  = dot1x_fsm_get(port, DOT1X_FSM_CODE_AUTHENTICATOR_PAE, 0);
      *fsm_code     = DOT1X_FSM_CODE_AUTHENTICATOR_PAE;
      *other_code   = 0;
      *trigger_code = DOT1X_FSM_AUTH_PAE_TRIGGER_AUTH_SUCCESS;

      _on_state_entry(port, fsm_backend, trigger_fsm, fsm_code, other_code, trigger_code);

      break;

   case DOT1X_FSM_BACKEND_AUTH_STATE_FAIL:

      priv_port->fsm_globals.currentId = fsm_backend->idFromServer;
      _txCannedFail(port, fsm_backend);
      fsm_backend->state = DOT1X_FSM_BACKEND_AUTH_STATE_IDLE;

      /**
       * Setup a trigger for Authenticator PAE state machine
       */
      *trigger_fsm  = dot1x_fsm_get(port, DOT1X_FSM_CODE_AUTHENTICATOR_PAE, 0);
      *fsm_code     = DOT1X_FSM_CODE_AUTHENTICATOR_PAE;
      *other_code   = 0;
      *trigger_code = DOT1X_FSM_AUTH_PAE_TRIGGER_AUTH_FAIL;

      _on_state_entry(port, fsm_backend, trigger_fsm, fsm_code, other_code, trigger_code);
      break;

   case DOT1X_FSM_BACKEND_AUTH_STATE_TIMEOUT:
      if (priv_port->fsm_globals.portStatus == Unauthorized)
      {
         _txCannedFail(port, fsm_backend);
      }
      fsm_backend->state = DOT1X_FSM_BACKEND_AUTH_STATE_IDLE;

      /**
       * Setup a trigger for Authenticator PAE state machine
       */
      *trigger_fsm  = dot1x_fsm_get(port, DOT1X_FSM_CODE_AUTHENTICATOR_PAE, 0);
      *fsm_code     = DOT1X_FSM_CODE_AUTHENTICATOR_PAE;
      *other_code   = 0;
      *trigger_code = DOT1X_FSM_AUTH_PAE_TRIGGER_AUTH_TIMEOUT;

      _on_state_entry(port, fsm_backend, trigger_fsm, fsm_code, other_code, trigger_code);
      break;

   case DOT1X_FSM_BACKEND_AUTH_STATE_IDLE:
      priv_port->fsm_globals.authStart = False;
      fsm_backend->reqCount            = 0;
      break;

   case DOT1X_FSM_BACKEND_AUTH_STATE_INITIALIZE:
      _abortAuth(port, fsm_backend);
      priv_port->fsm_globals.authAbort = False;
      fsm_backend->state = DOT1X_FSM_BACKEND_AUTH_STATE_IDLE;
      _on_state_entry(port, fsm_backend, trigger_fsm, fsm_code, other_code, trigger_code);
      break;
   }
}


static void _attempt_transition(dot1x_port_t *port, dot1x_fsm_backend_auth_t *fsm_backend, dot1x_fsm_t **trigger_fsm, int *fsm_code, int *other_code, int *trigger_code)
{
   dot1x_port_priv_t *priv_port;
   int               changed;

   priv_port = (dot1x_port_priv_t *)port;

   do
   {
      changed = 0;
      switch (fsm_backend->state)
      {
      case DOT1X_FSM_BACKEND_AUTH_STATE_REQUEST:
         if ((priv_port->fsm_globals.aWhile == 0) &&
             (fsm_backend->reqCount != priv_port->maxReq))
         {
            _on_state_entry(port, fsm_backend, trigger_fsm, fsm_code, other_code, trigger_code);
         }
         else if (fsm_backend->rxResp)
         {
            fsm_backend->state = DOT1X_FSM_BACKEND_AUTH_STATE_RESPONSE;
            _on_state_entry(port, fsm_backend, trigger_fsm, fsm_code, other_code, trigger_code);
            changed = 1;
         }
         break;

      case DOT1X_FSM_BACKEND_AUTH_STATE_RESPONSE:
         if (fsm_backend->aReq)
         {
            fsm_backend->state = DOT1X_FSM_BACKEND_AUTH_STATE_REQUEST;
            _on_state_entry(port, fsm_backend, trigger_fsm, fsm_code, other_code, trigger_code);
            changed = 1;
         }
         else if (priv_port->fsm_globals.aWhile == 0)
         {
            fsm_backend->state = DOT1X_FSM_BACKEND_AUTH_STATE_TIMEOUT;
            _on_state_entry(port, fsm_backend, trigger_fsm, fsm_code, other_code, trigger_code);
            changed = 1;
         }
         else if (fsm_backend->aFail)
         {
            fsm_backend->state = DOT1X_FSM_BACKEND_AUTH_STATE_FAIL;
            _on_state_entry(port, fsm_backend, trigger_fsm, fsm_code, other_code, trigger_code);
            changed = 1;
         }
         else if (fsm_backend->aSuccess)
         {
            fsm_backend->state = DOT1X_FSM_BACKEND_AUTH_STATE_SUCCESS;
            _on_state_entry(port, fsm_backend, trigger_fsm, fsm_code, other_code, trigger_code);
            changed = 1;
         }
         break;

      case DOT1X_FSM_BACKEND_AUTH_STATE_IDLE:
         if (priv_port->fsm_globals.authStart)
         {
            fsm_backend->state = DOT1X_FSM_BACKEND_AUTH_STATE_RESPONSE;
            _on_state_entry(port, fsm_backend, trigger_fsm, fsm_code, other_code, trigger_code);
            changed = 1;
         }
         break;
      }
   } while (changed);
}


static int _txReq(dot1x_port_t *port, dot1x_fsm_backend_auth_t *fsm_backend)
{
   dot1x_eapol_frame_t eapol_frame;
   dot1x_port_priv_t   *priv_port;
   unsigned char       *buffer;
   unsigned char       *p;
   int                 ret;

   /**
    * Transmit a Request from the server to the supplicant
    */

   buffer    = dot1x_get_2k_buffer();
   priv_port = (dot1x_port_priv_t *)port;

   p = buffer + DOT1X_EAPOL_FRAME_MIN_LENGTH;

   dot1x_eapol_eap_frame_create(fsm_backend->aReqFrame, p);

   p = buffer;
   eapol_frame.version     = DOT1X_2001_EAPOL_VERSION;
   eapol_frame.type        = DOT1X_EAPOL_FRAME_TYPE_EAP;
   eapol_frame.body_length = fsm_backend->aReqFrame->length;
   eapol_frame.body        = buffer + DOT1X_EAPOL_FRAME_MIN_LENGTH;

   dot1x_eapol_create_frame(&eapol_frame, p);

   ret = dot1x_eapol_tx(port, buffer, DOT1X_EAPOL_FRAME_MIN_LENGTH + fsm_backend->aReqFrame->length);

   dot1x_release_2k_buffer(buffer);

   return ret;
}


static int _sendRespToServer(dot1x_port_t *port, dot1x_fsm_backend_auth_t *fsm_backend)
{
   /**
    * Transmit a Response from the supplicant to the server.
    * We need to convert the rxResp
    */

   dot1x_priv_t *priv;

   priv = (dot1x_priv_t *)port->dot1x_instance;

   return priv->backend->send_eap_frame(port, fsm_backend->rxRespFrame);
}


static int _txCannedSuccess(dot1x_port_t *port, dot1x_fsm_backend_auth_t *fsm_backend)
{
   return dot1x_tx_canned_success(port);
}


static int _txCannedFail(dot1x_port_t *port, dot1x_fsm_backend_auth_t *fsm_backend)
{
   return dot1x_tx_canned_fail(port);
}


static DOT1X_INLINE void _clear_rxResp(dot1x_fsm_backend_auth_t *fsm_backend)
{
   if (fsm_backend->rxRespFrame != NULL)
   {
      if (fsm_backend->rxRespFrame->data != NULL)
      {
         dot1x_heap_free(fsm_backend->rxRespFrame->data);
      }
      dot1x_heap_free(fsm_backend->rxRespFrame);
   }

   fsm_backend->rxRespFrame = NULL;
}


static DOT1X_INLINE void _clear_aReq(dot1x_fsm_backend_auth_t *fsm_backend)
{
   if (fsm_backend->aReqFrame != NULL)
   {
      if (fsm_backend->aReqFrame->data != NULL)
      {
         dot1x_heap_free(fsm_backend->aReqFrame->data);
      }
      dot1x_heap_free(fsm_backend->aReqFrame);
   }

   fsm_backend->aReqFrame = NULL;
}


static int _abortAuth(dot1x_port_t *port, dot1x_fsm_backend_auth_t *fsm_backend)
{
   _clear_rxResp(fsm_backend);
   _clear_aReq(fsm_backend);
   return 0;
}


static int _eapol_rx_eap(dot1x_port_t *port, dot1x_fsm_backend_auth_t *fsm_backend, dot1x_eapol_frame_t *frame)
{
   dot1x_eap_frame_t eap_frame;
   int               ret;
   dot1x_port_priv_t *priv_port;

   ret = dot1x_eapol_process_eap(frame, &eap_frame);

   if (ret)
   {
      goto _out;
   }

   priv_port = (dot1x_port_priv_t *)port;

   switch (eap_frame.code)
   {
   case DOT1X_EAP_CODE_RESPONSE:
      if ((eap_frame.type != DOT1X_EAP_TYPE_IDENTITY) &&
          (eap_frame.id == priv_port->fsm_globals.currentId))
      {
         fsm_backend->rxResp = True;
      }
      else
      {
         ret = -1;
      }
      /** Copy over the response frame to send it to the server */
      _clear_rxResp(fsm_backend);
      fsm_backend->rxRespFrame = (dot1x_eap_frame_t *)dot1x_heap_alloc(sizeof(dot1x_eap_frame_t));
      memset(fsm_backend->rxRespFrame, 0, sizeof(dot1x_eap_frame_t));
      memcpy(fsm_backend->rxRespFrame, &eap_frame, sizeof(dot1x_eap_frame_t));
      if (fsm_backend->rxRespFrame->data_length > 0)
      {
         fsm_backend->rxRespFrame->data = (unsigned char *)dot1x_heap_alloc(fsm_backend->rxRespFrame->data_length);
         memcpy(fsm_backend->rxRespFrame->data, eap_frame.data, eap_frame.data_length);
      }
      break;

   default:
      ret = -1;
   }

_out:
   return ret;
}


static int _tick(dot1x_port_t *port, int fsm_code, int other_code, struct dot1x_fsm *fsm)
{
   dot1x_fsm_backend_auth_t *fsm_backend;
   dot1x_fsm_t              *trigger_fsm;
   int          trigger_code;
   int          trigger_fsm_code;
   int          trigger_other_code;
   unsigned int lock_code;

   fsm_backend = (dot1x_fsm_backend_auth_t *)fsm;
   trigger_fsm = NULL;

   dot1x_lock(fsm_backend->lock, lock_code);

   _attempt_transition(port, fsm_backend, &trigger_fsm, &trigger_fsm_code, &trigger_other_code, &trigger_code);

   dot1x_unlock(fsm_backend->lock, lock_code);

   if (trigger_fsm != NULL)
   {
      trigger_fsm->trigger(port, trigger_fsm_code, trigger_other_code, trigger_fsm, trigger_code);
   }

   return 0;
}


static int _eapol_rx_begin(dot1x_port_t *port, int fsm_code, int other_code, struct dot1x_fsm *fsm)
{
   dot1x_fsm_backend_auth_t *fsm_backend;
   unsigned int             lock_code;

   fsm_backend = (dot1x_fsm_backend_auth_t *)fsm;

   dot1x_lock(fsm_backend->lock, lock_code);

   fsm_backend->trigger_fsm = NULL;

   dot1x_unlock(fsm_backend->lock, lock_code);

   return 0;
}


static int _eapol_rx(dot1x_port_t *port, int fsm_code, int other_code, struct dot1x_fsm *fsm, dot1x_eapol_frame_t *frame)
{
   dot1x_fsm_backend_auth_t *fsm_backend;
   unsigned int             lock_code;
   int ret;

   ret         = 0;
   fsm_backend = (dot1x_fsm_backend_auth_t *)fsm;

   if (fsm_backend->state == DOT1X_FSM_BACKEND_AUTH_STATE_INVALID)
   {
      return 0;
   }

   dot1x_lock(fsm_backend->lock, lock_code);

   switch (frame->type)
   {
   case DOT1X_EAPOL_FRAME_TYPE_EAP:
      ret = _eapol_rx_eap(port, fsm_backend, frame);
      break;
   }

   _attempt_transition(port,
                       fsm_backend,
                       &fsm_backend->trigger_fsm,
                       &fsm_backend->trigger_fsm_code,
                       &fsm_backend->trigger_other_code,
                       &fsm_backend->trigger_code);

   dot1x_unlock(fsm_backend->lock, lock_code);

   return ret;
}


static int _eapol_rx_end(dot1x_port_t *port, int fsm_code, int other_code, struct dot1x_fsm *fsm)
{
   dot1x_fsm_backend_auth_t *fsm_backend;
   unsigned int             lock_code;

   fsm_backend = (dot1x_fsm_backend_auth_t *)fsm;

   dot1x_lock(fsm_backend->lock, lock_code);

   if (fsm_backend->trigger_fsm != NULL)
   {
      fsm_backend->trigger_fsm->trigger(port,
                                        fsm_backend->trigger_fsm_code,
                                        fsm_backend->trigger_other_code,
                                        fsm_backend->trigger_fsm,
                                        fsm_backend->trigger_code);
   }

   dot1x_unlock(fsm_backend->lock, lock_code);

   return 0;
}


static int _trigger(dot1x_port_t *port, int fsm_code, int other_code, struct dot1x_fsm *fsm, int trigger_code)
{
   dot1x_fsm_backend_auth_t *fsm_backend;
   dot1x_fsm_t              *trigger_fsm;
   int               trigger_fsm_code;
   int               trigger_other_code;
   dot1x_port_priv_t *priv_port;
   unsigned int      lock_code;

   fsm_backend = (dot1x_fsm_backend_auth_t *)fsm;
   priv_port   = (dot1x_port_priv_t *)port;
   trigger_fsm = NULL;

   dot1x_lock(fsm_backend->lock, lock_code);

   switch (trigger_code)
   {
   case DOT1X_FSM_BACKEND_AUTH_TRIGGER_AUTH_ABORT:
      priv_port->fsm_globals.authAbort = True;
      _attempt_transition(port, fsm_backend, &trigger_fsm, &trigger_fsm_code, &trigger_other_code, &trigger_code);
      break;

   case DOT1X_FSM_BACKEND_AUTH_TRIGGER_AUTH_START:
      priv_port->fsm_globals.authStart = True;
      _attempt_transition(port, fsm_backend, &trigger_fsm, &trigger_fsm_code, &trigger_other_code, &trigger_code);
      break;

   case DOT1X_FSM_BACKEND_AUTH_TRIGGER_AREQ:

      /**
       * Here the other_code parameter represents a dot1x_eap_frame_t*
       * We cleanup the current aReqFrame and copy this new one onto it
       * before attempting a trasition
       */
      _clear_aReq(fsm_backend);
      fsm_backend->aReqFrame = (dot1x_eap_frame_t *)other_code;
      fsm_backend->aReq      = True;
      _attempt_transition(port, fsm_backend, &trigger_fsm, &trigger_fsm_code, &trigger_other_code, &trigger_code);
      break;

   case DOT1X_FSM_BACKEND_AUTH_TRIGGER_ASUCCESS:
      fsm_backend->aSuccess = True;
      _attempt_transition(port, fsm_backend, &trigger_fsm, &trigger_fsm_code, &trigger_other_code, &trigger_code);
      break;

   case DOT1X_FSM_BACKEND_AUTH_TRIGGER_AFAIL:
      fsm_backend->aFail = True;
      _attempt_transition(port, fsm_backend, &trigger_fsm, &trigger_fsm_code, &trigger_other_code, &trigger_code);
      break;
   }

   dot1x_unlock(fsm_backend->lock, lock_code);

   if (trigger_fsm != NULL)
   {
      trigger_fsm->trigger(port, trigger_fsm_code, trigger_other_code, trigger_fsm, trigger_code);
   }

   return 0;
}


static int _initialize(dot1x_port_t *port, int fsm_code, int other_code, struct dot1x_fsm *fsm)
{
   dot1x_fsm_backend_auth_t *fsm_backend;
   dot1x_fsm_t              *trigger_fsm;
   int          trigger_fsm_code;
   int          trigger_other_code;
   int          trigger_code;
   unsigned int lock_code;

   fsm_backend = (dot1x_fsm_backend_auth_t *)fsm;

   dot1x_lock(fsm_backend->lock, lock_code);

   fsm_backend->state = DOT1X_FSM_BACKEND_AUTH_STATE_INITIALIZE;
   _on_state_entry(port, fsm_backend, &trigger_fsm, &trigger_fsm_code, &trigger_other_code, &trigger_code);
   _attempt_transition(port, fsm_backend, &trigger_fsm, &trigger_fsm_code, &trigger_other_code, &trigger_code);

   dot1x_unlock(fsm_backend->lock, lock_code);

   return 0;
}


static int _on_destroy(dot1x_port_t *port, struct dot1x_fsm *fsm)
{
   dot1x_fsm_backend_auth_t *fsm_backend;

   fsm_backend = (dot1x_fsm_backend_auth_t *)fsm;

   if (fsm_backend != NULL)
   {
      _clear_rxResp(fsm_backend);
      _clear_aReq(fsm_backend);
      dot1x_destroy_lock(fsm_backend->lock);
      dot1x_heap_free(fsm_backend);
   }

   return 0;
}
