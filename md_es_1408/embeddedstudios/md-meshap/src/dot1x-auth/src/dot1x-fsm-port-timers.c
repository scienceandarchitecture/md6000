/********************************************************************************
* MeshDynamics
* --------------
* File     : dot1x-fsm-port-timers.c
* Comments : 802.1x Port Timers FSM
* Created  : 1/26/2005
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  2  |6/16/2008 | Lock primitives changed                         | Sriram |
* -----------------------------------------------------------------------------
* |  1  |7/13/2007 | dot1x_port_timers_fsm_reset Added               | Sriram |
* -----------------------------------------------------------------------------
* |  0  |1/26/2005 | Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

#include "dot1x.h"
#include "dot1x-impl.h"
#include "dot1x-priv.h"
#include "dot1x-fsm-port-timers.h"

/**
 * FSM functions
 */

static int _tick(dot1x_port_t *port, int fsm_code, int other_code, struct dot1x_fsm *fsm);
static int _eapol_rx(dot1x_port_t *port, int fsm_code, int other_code, struct dot1x_fsm *fsm, dot1x_eapol_frame_t *frame);
static int _trigger(dot1x_port_t *port, int fsm_code, int other_code, struct dot1x_fsm *fsm, int trigger_code);
static int _initialize(dot1x_port_t *port, int fsm_code, int other_code, struct dot1x_fsm *fsm);
static int _on_destroy(dot1x_port_t *port, struct dot1x_fsm *fsm);

/**
 * Internal functions
 */

static void _on_state_entry(dot1x_port_t *port, dot1x_fsm_port_timers_t *fsm_port_timers);
static void _attempt_transition(dot1x_port_t *port, dot1x_fsm_port_timers_t *fsm_port_timers);

int dot1x_port_timers_fsm_create(dot1x_port_t *port)
{
   dot1x_fsm_port_timers_t *fsm_port_timers;
   int err;

   fsm_port_timers = (dot1x_fsm_port_timers_t *)dot1x_heap_alloc(sizeof(dot1x_fsm_port_timers_t));

   memset(fsm_port_timers, 0, sizeof(dot1x_fsm_port_timers_t));

   fsm_port_timers->fsm.initialize = _initialize;
   fsm_port_timers->fsm.eapol_rx   = _eapol_rx;
   fsm_port_timers->fsm.tick       = _tick;
   fsm_port_timers->fsm.trigger    = _trigger;
   fsm_port_timers->fsm.on_destroy = _on_destroy;

   fsm_port_timers->lock  = dot1x_create_lock();
   fsm_port_timers->state = DOT1X_FSM_PORT_TIMERS_STATE_INVALID;

   err = dot1x_fsm_register(port, DOT1X_FSM_CODE_PORT_TIMERS, 0, &fsm_port_timers->fsm);

   if (err)
   {
      dot1x_destroy_lock(fsm_port_timers->lock);
      dot1x_heap_free(fsm_port_timers);
   }

   dot1x_print(DOT1X_PRINT_LEVEL_FLOW,
               "dot1x-auth: Port Timers FSM initialized for %s:%02x-%02x-%02x-%02x-%02x-%02x\n",
               port->dot1x_instance->instance_name,
               port->address[0],
               port->address[1],
               port->address[2],
               port->address[3],
               port->address[4],
               port->address[5]);

   return err;
}


int dot1x_port_timers_fsm_reset(dot1x_port_t *port)
{
   dot1x_fsm_port_timers_t *fsm_port_timers;

   fsm_port_timers = (dot1x_fsm_port_timers_t *)dot1x_fsm_get(port, DOT1X_FSM_CODE_PORT_TIMERS, 0);

   fsm_port_timers->state = DOT1X_FSM_PORT_TIMERS_STATE_INVALID;

   dot1x_print(DOT1X_PRINT_LEVEL_FLOW,
               "dot1x-auth: Port Timers FSM reset for %s:%02x-%02x-%02x-%02x-%02x-%02x\n",
               port->dot1x_instance->instance_name,
               port->address[0],
               port->address[1],
               port->address[2],
               port->address[3],
               port->address[4],
               port->address[5]);

   return 0;
}


int dot1x_port_timers_fsm_destroy(dot1x_port_t *port)
{
   dot1x_fsm_t *fsm;

   fsm = dot1x_fsm_get(port, DOT1X_FSM_CODE_PORT_TIMERS, 0);

   if (fsm != NULL)
   {
      _on_destroy(port, fsm);
   }

   dot1x_fsm_unregister(port, DOT1X_FSM_CODE_PORT_TIMERS, 0);

   return 0;
}


static void _on_state_entry(dot1x_port_t *port, dot1x_fsm_port_timers_t *fsm_port_timers)
{
   dot1x_port_priv_t *priv_port;

   priv_port = (dot1x_port_priv_t *)port;

   switch (fsm_port_timers->state)
   {
   case DOT1X_FSM_PORT_TIMERS_STATE_ONE_SECOND:
      break;

   case DOT1X_FSM_PORT_TIMERS_STATE_TICK:

      /** Decrement all timers */

      dot1x_dec(priv_port->fsm_globals.aWhile);
      dot1x_dec(priv_port->fsm_globals.authWhile);
      dot1x_dec(priv_port->fsm_globals.heldWhile);
      dot1x_dec(priv_port->fsm_globals.quiteWhile);
      dot1x_dec(priv_port->fsm_globals.reAuthWhen);
      dot1x_dec(priv_port->fsm_globals.startWhen);
      dot1x_dec(priv_port->fsm_globals.txWhen);

      fsm_port_timers->tick = False;

      /** Transition to DOT1X_FSM_PORT_TIMERS_STATE_ONE_SECOND */

      fsm_port_timers->state = DOT1X_FSM_PORT_TIMERS_STATE_ONE_SECOND;
      _on_state_entry(port, fsm_port_timers);

      break;
   }
}


static void _attempt_transition(dot1x_port_t *port, dot1x_fsm_port_timers_t *fsm_port_timers)
{
   int changed;

   do
   {
      changed = 0;
      switch (fsm_port_timers->state)
      {
      case DOT1X_FSM_PORT_TIMERS_STATE_ONE_SECOND:
         if (fsm_port_timers->tick == True)
         {
            fsm_port_timers->state = DOT1X_FSM_PORT_TIMERS_STATE_TICK;
            _on_state_entry(port, fsm_port_timers);
            changed = 1;
         }
         break;

      case DOT1X_FSM_PORT_TIMERS_STATE_TICK:
         break;
      }
   } while (changed);
}


static int _tick(dot1x_port_t *port, int fsm_code, int other_code, struct dot1x_fsm *fsm)
{
   dot1x_fsm_port_timers_t *fsm_port_timers;
   unsigned int            lock_code;

   fsm_port_timers = (dot1x_fsm_port_timers_t *)fsm;

   fsm_port_timers->tick = True;

   dot1x_lock(fsm_port_timers->lock, lock_code);

   _attempt_transition(port, fsm_port_timers);

   dot1x_unlock(fsm_port_timers->lock, lock_code);

   return 0;
}


static int _eapol_rx(dot1x_port_t *port, int fsm_code, int other_code, struct dot1x_fsm *fsm, dot1x_eapol_frame_t *frame)
{
   return 0;
}


static int _trigger(dot1x_port_t *port, int fsm_code, int other_code, struct dot1x_fsm *fsm, int trigger_code)
{
   return 0;
}


static int _initialize(dot1x_port_t *port, int fsm_code, int other_code, struct dot1x_fsm *fsm)
{
   dot1x_fsm_port_timers_t *fsm_port_timers;
   unsigned int            lock_code;

   fsm_port_timers = (dot1x_fsm_port_timers_t *)fsm;

   dot1x_lock(fsm_port_timers->lock, lock_code);

   fsm_port_timers->state = DOT1X_FSM_PORT_TIMERS_STATE_ONE_SECOND;
   _on_state_entry(port, fsm_port_timers);
   _attempt_transition(port, fsm_port_timers);

   dot1x_unlock(fsm_port_timers->lock, lock_code);

   return 0;
}


static int _on_destroy(dot1x_port_t *port, struct dot1x_fsm *fsm)
{
   dot1x_fsm_port_timers_t *fsm_port_timers;

   fsm_port_timers = (dot1x_fsm_port_timers_t *)fsm;

   if (fsm_port_timers != NULL)
   {
      dot1x_destroy_lock(fsm_port_timers->lock);
      dot1x_heap_free(fsm_port_timers);
   }

   return 0;
}
