/********************************************************************************
* MeshDynamics
* --------------
* File     : dot1x.c
* Comments : 802.1x Authenticator Implementation
* Created  : 1/25/2005
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  2  |6/16/2008 | Lock primitives changed                         | Sriram |
* -----------------------------------------------------------------------------
* |  1  |4/7/2005  | Misc changes                                    | Sriram |
* -----------------------------------------------------------------------------
* |  0  |1/25/2005 | Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

#include "dot1x.h"
#include "dot1x-impl.h"
#include "dot1x-priv.h"
#include "dot1x-eapol.h"

int dot1x_initialize()
{
   dot1x_priv_initialize();
   dot1x_print(DOT1X_PRINT_LEVEL_INFORMATION, "dot1x-auth: Initialized\n");
   return 0;
}


int dot1x_uninitialize()
{
   dot1x_priv_uninitialize();
   dot1x_print(DOT1X_PRINT_LEVEL_INFORMATION, "dot1x-auth: Un-initialized\n");
   return 0;
}


int dot1x_register_backend(const char *backend_name, const dot1x_backend_t *backend)
{
   return dot1x_priv_backend_register(backend_name, backend);
}


int dot1x_unregister_backend(const char *backend_name)
{
   return dot1x_priv_backend_unregister(backend_name);
}


dot1x_t *dot1x_create_instance(const char                 *instance_name,
                               const char                 *backend_name,
                               void                       *backend_param,
                               void                       *instance_data,
                               dot1x_port_status_change_t on_status_change)
{
   return (dot1x_t *)dot1x_priv_create_instance(instance_name,
                                                backend_name,
                                                backend_param,
                                                instance_data,
                                                on_status_change,
                                                17);
}


void dot1x_destroy_instance(dot1x_t *instance)
{
   dot1x_priv_destroy_instance((dot1x_priv_t *)instance);
}


int dot1x_process_eapol(dot1x_t        *instance,
                        unsigned char  *dst_mac,
                        unsigned char  *src_mac,
                        unsigned short type,
                        unsigned char  *packet_data,
                        int            packet_length)
{
   dot1x_eapol_frame_t frame_desc;
   dot1x_port_t        *port;
   dot1x_priv_t        *priv;
   unsigned int        lock_code;
   int                 ret;

   priv = (dot1x_priv_t *)instance;
   ret  = -1;

   dot1x_lock(priv->lock, lock_code);

   port = (dot1x_port_t *)dot1x_priv_get_port((dot1x_priv_t *)instance, src_mac);

   if (port != NULL)
   {
      dot1x_eapol_process_frame(packet_data, packet_length, &frame_desc);
      ret = dot1x_fsm_eapol_rx(port, &frame_desc);
   }

   dot1x_unlock(priv->lock, lock_code);

   return ret;
}


dot1x_port_t *dot1x_create_port(dot1x_t       *instance,
                                unsigned char *address,
                                void          *port_data,
                                int           AdminControlledDirections,
                                int           AuthControlledPortControl,
                                int           KeyTransmissionEnabled)
{
   return (dot1x_port_t *)dot1x_priv_create_port((dot1x_priv_t *)instance,
                                                 address,
                                                 port_data,
                                                 AdminControlledDirections,
                                                 AuthControlledPortControl,
                                                 KeyTransmissionEnabled,
                                                 60,                                  /* quietPeriod default */
                                                 2,                                   /* reAuthMax default */
                                                 30,                                  /* txPeriod default */
                                                 30,                                  /* suppTimeout default */
                                                 30,                                  /* serverTimeout default */
                                                 2,                                   /* maxReq default */
                                                 3600,                                /* reAuthPeriod default */
                                                 True);
}


void dot1x_port_initialize(dot1x_t *instance, unsigned char *address)
{
   dot1x_port_t *port;

   port = (dot1x_port_t *)dot1x_priv_get_port((dot1x_priv_t *)instance, address);

   if (port != NULL)
   {
      dot1x_fsm_initialize(port);
   }
}


void dot1x_destroy_port(dot1x_t       *instance,
                        unsigned char *address)

{
   dot1x_priv_destroy_port((dot1x_priv_t *)instance, address);
}


int dot1x_tick(dot1x_t *instance)
{
   return dot1x_priv_tick((dot1x_priv_t *)instance);
}


int dot1x_tx_canned_fail(dot1x_port_t *port)
{
   dot1x_eapol_frame_t eapol_frame;
   dot1x_eap_frame_t   eap_frame;
   dot1x_port_priv_t   *priv_port;

   unsigned char buffer[DOT1X_EAPOL_FRAME_MIN_LENGTH + DOT1X_EAP_MIN_LENGTH];
   unsigned char *p;

   priv_port = (dot1x_port_priv_t *)port;

   p = buffer + DOT1X_EAPOL_FRAME_MIN_LENGTH;
   eap_frame.code        = DOT1X_EAP_CODE_FAILURE;
   eap_frame.id          = priv_port->fsm_globals.currentId;
   eap_frame.length      = 0;
   eap_frame.data        = NULL;
   eap_frame.data_length = 0;

   dot1x_eapol_eap_frame_create(&eap_frame, p);

   p = buffer;
   eapol_frame.version     = DOT1X_2001_EAPOL_VERSION;
   eapol_frame.type        = DOT1X_EAPOL_FRAME_TYPE_EAP;
   eapol_frame.body_length = DOT1X_EAP_MIN_LENGTH;
   eapol_frame.body        = NULL;

   dot1x_eapol_create_frame(&eapol_frame, p);

   return dot1x_eapol_tx(port, buffer, DOT1X_EAPOL_FRAME_MIN_LENGTH + DOT1X_EAP_MIN_LENGTH);
}


int dot1x_tx_canned_success(dot1x_port_t *port)
{
   dot1x_eapol_frame_t eapol_frame;
   dot1x_eap_frame_t   eap_frame;
   dot1x_port_priv_t   *priv_port;
   unsigned char       buffer[DOT1X_EAPOL_FRAME_MIN_LENGTH + DOT1X_EAP_MIN_LENGTH];
   unsigned char       *p;

   priv_port = (dot1x_port_priv_t *)port;

   p = buffer + DOT1X_EAPOL_FRAME_MIN_LENGTH;
   eap_frame.code        = DOT1X_EAP_CODE_SUCCESS;
   eap_frame.id          = priv_port->fsm_globals.currentId;
   eap_frame.length      = 0;
   eap_frame.data        = NULL;
   eap_frame.data_length = 0;

   dot1x_eapol_eap_frame_create(&eap_frame, p);

   p = buffer;
   eapol_frame.version     = DOT1X_2001_EAPOL_VERSION;
   eapol_frame.type        = DOT1X_EAPOL_FRAME_TYPE_EAP;
   eapol_frame.body_length = DOT1X_EAP_MIN_LENGTH;
   eapol_frame.body        = NULL;

   dot1x_eapol_create_frame(&eapol_frame, p);

   return dot1x_eapol_tx(port, buffer, DOT1X_EAPOL_FRAME_MIN_LENGTH + DOT1X_EAP_MIN_LENGTH);
}


void dot1x_iterate_ports(dot1x_t                       *instance,
                         dot1x_port_iterator_routine_t routine)
{
   dot1x_port_priv_t *ports;
   dot1x_priv_t      *priv;
   unsigned int      lock_code;

   priv = (dot1x_priv_t *)instance;
   if (priv == NULL) {
	   return;
   }
   dot1x_lock(priv->lock, lock_code);

   ports = priv->port_list;

   while (ports != NULL)
   {
      routine(&ports->base);
      ports = ports->next_list;
   }

   dot1x_unlock(priv->lock, lock_code);
}
