/********************************************************************************
* MeshDynamics
* --------------
* File     : dot1x-fsm-auth-pae.c
* Comments : 802.1x Authenticator PAE FSM
* Created  : 1/26/2005
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  3  |6/16/2008 | Lock primitives changed                         | Sriram |
* -----------------------------------------------------------------------------
* |  2  |7/13/2007 | dot1x_auth_pae_fsm_reset Added                  | Sriram |
* -----------------------------------------------------------------------------
* |  1  |4/6/2005  | Misc changes                                    | Sriram |
* -----------------------------------------------------------------------------
* |  0  |1/26/2005 | Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

#include "dot1x.h"
#include "dot1x-impl.h"
#include "dot1x-priv.h"
#include "dot1x-fsm-auth-pae.h"
#include "dot1x-fsm-backend-auth.h"

/**
 * FSM functions
 */

static int _tick(dot1x_port_t *port, int fsm_code, int other_code, struct dot1x_fsm *fsm);
static int _eapol_rx_begin(dot1x_port_t *port, int fsm_code, int other_code, struct dot1x_fsm *fsm);
static int _eapol_rx(dot1x_port_t *port, int fsm_code, int other_code, struct dot1x_fsm *fsm, dot1x_eapol_frame_t *frame);
static int _eapol_rx_end(dot1x_port_t *port, int fsm_code, int other_code, struct dot1x_fsm *fsm);
static int _trigger(dot1x_port_t *port, int fsm_code, int other_code, struct dot1x_fsm *fsm, int trigger_code);
static int _initialize(dot1x_port_t *port, int fsm_code, int other_code, struct dot1x_fsm *fsm);
static int _on_destroy(dot1x_port_t *port, struct dot1x_fsm *fsm);

/**
 * Internal functions
 */

static void _on_state_entry(dot1x_port_t *port, dot1x_fsm_auth_pae_t *fsm_pae, dot1x_fsm_t **trigger_fsm, int *fsm_code, int *other_code, int *trigger_code);
static void _attempt_transition(dot1x_port_t *port, dot1x_fsm_auth_pae_t *fsm_pae, dot1x_fsm_t **trigger_fsm, int *fsm_code, int *other_code, int *trigger_code);

static int _txCannedSuccess(dot1x_port_t *port, dot1x_fsm_auth_pae_t *fsm_pae);
static int _txCannedFail(dot1x_port_t *port, dot1x_fsm_auth_pae_t *fsm_pae);
static int _txReqId(dot1x_port_t *port, dot1x_fsm_auth_pae_t *fsm_pae);

static int _eapol_rx_eap(dot1x_port_t *port, dot1x_fsm_auth_pae_t *fsm_pae, dot1x_eapol_frame_t *frame);
static int _eapol_rx_start(dot1x_port_t *port, dot1x_fsm_auth_pae_t *fsm_pae, dot1x_eapol_frame_t *frame);
static int _eapol_rx_logoff(dot1x_port_t *port, dot1x_fsm_auth_pae_t *fsm_pae, dot1x_eapol_frame_t *frame);

int dot1x_auth_pae_fsm_create(dot1x_port_t *port)
{
   dot1x_fsm_auth_pae_t *fsm_pae;
   int err;

   fsm_pae = (dot1x_fsm_auth_pae_t *)dot1x_heap_alloc(sizeof(dot1x_fsm_auth_pae_t));

   memset(fsm_pae, 0, sizeof(dot1x_fsm_auth_pae_t));

   fsm_pae->fsm.initialize     = _initialize;
   fsm_pae->fsm.tick           = _tick;
   fsm_pae->fsm.eapol_rx_begin = _eapol_rx_begin;
   fsm_pae->fsm.eapol_rx       = _eapol_rx;
   fsm_pae->fsm.eapol_rx_end   = _eapol_rx_end;
   fsm_pae->fsm.trigger        = _trigger;
   fsm_pae->fsm.on_destroy     = _on_destroy;

   fsm_pae->lock  = dot1x_create_lock();
   fsm_pae->state = DOT1X_FSM_AUTH_PAE_STATE_INVALID;

   err = dot1x_fsm_register(port, DOT1X_FSM_CODE_AUTHENTICATOR_PAE, 0, &fsm_pae->fsm);

   if (err)
   {
      dot1x_destroy_lock(fsm_pae->lock);
      dot1x_heap_free(fsm_pae);
   }

   dot1x_print(DOT1X_PRINT_LEVEL_FLOW,
               "dot1x-auth: Authenticator PAE FSM initialized for %s:%02x-%02x-%02x-%02x-%02x-%02x\n",
               port->dot1x_instance->instance_name,
               port->address[0],
               port->address[1],
               port->address[2],
               port->address[3],
               port->address[4],
               port->address[5]);

   return err;
}


int dot1x_auth_pae_fsm_reset(dot1x_port_t *port)
{
   dot1x_fsm_auth_pae_t *fsm_pae;

   fsm_pae = (dot1x_fsm_auth_pae_t *)dot1x_fsm_get(port, DOT1X_FSM_CODE_AUTHENTICATOR_PAE, 0);

   fsm_pae->state = DOT1X_FSM_AUTH_PAE_STATE_INVALID;

   dot1x_print(DOT1X_PRINT_LEVEL_FLOW,
               "dot1x-auth: Authenticator PAE FSM reset for %s:%02x-%02x-%02x-%02x-%02x-%02x\n",
               port->dot1x_instance->instance_name,
               port->address[0],
               port->address[1],
               port->address[2],
               port->address[3],
               port->address[4],
               port->address[5]);

   return 0;
}


int dot1x_auth_pae_fsm_destroy(dot1x_port_t *port)
{
   dot1x_fsm_t *fsm;

   fsm = dot1x_fsm_get(port, DOT1X_FSM_CODE_AUTHENTICATOR_PAE, 0);

   if (fsm != NULL)
   {
      _on_destroy(port, fsm);
   }

   dot1x_fsm_unregister(port, DOT1X_FSM_CODE_AUTHENTICATOR_PAE, 0);

   return 0;
}


static void _on_state_entry(dot1x_port_t *port, dot1x_fsm_auth_pae_t *fsm_pae, dot1x_fsm_t **trigger_fsm, int *fsm_code, int *other_code, int *trigger_code)
{
   dot1x_port_priv_t *priv_port;

   priv_port = (dot1x_port_priv_t *)port;

   switch (fsm_pae->state)
   {
   case DOT1X_FSM_AUTH_PAE_STATE_INITIALIZE:
      priv_port->fsm_globals.currentId = 0;
      fsm_pae->state = DOT1X_FSM_AUTH_PAE_STATE_DISCONNECTED;
      _on_state_entry(port, fsm_pae, trigger_fsm, fsm_code, other_code, trigger_code);
      break;

   case DOT1X_FSM_AUTH_PAE_STATE_DISCONNECTED:
      priv_port->fsm_globals.portStatus = Unauthorized;
      fsm_pae->eapLogoff   = False;
      fsm_pae->reAuthCount = 0;
      /*_txCannedFail(port,fsm_pae);*/
      dot1x_inc(priv_port->fsm_globals.currentId);
      fsm_pae->state = DOT1X_FSM_AUTH_PAE_STATE_CONNECTING;
      _on_state_entry(port, fsm_pae, trigger_fsm, fsm_code, other_code, trigger_code);
      break;

   case DOT1X_FSM_AUTH_PAE_STATE_CONNECTING:
      fsm_pae->eapStart = False;
      priv_port->fsm_globals.reAuthenticate = False;
      priv_port->fsm_globals.txWhen         = priv_port->txPeriod;
      fsm_pae->rxRespId = False;
      _txReqId(port, fsm_pae);
      ++fsm_pae->reAuthCount;
      DOT1X_PORT_STATISTICS_INC(priv_port, authEntersConnecting);
      break;

   case DOT1X_FSM_AUTH_PAE_STATE_AUTHENTICATING:
      priv_port->fsm_globals.authSuccess = False;
      priv_port->fsm_globals.authFail    = False;
      priv_port->fsm_globals.authTimeout = False;

      /**
       * Setup a trigger for backend authentication state machine
       */
      *trigger_fsm  = dot1x_fsm_get(port, DOT1X_FSM_CODE_BACKEND_AUTHENTICATION, 0);
      *fsm_code     = DOT1X_FSM_CODE_BACKEND_AUTHENTICATION;
      *other_code   = 0;
      *trigger_code = DOT1X_FSM_BACKEND_AUTH_TRIGGER_AUTH_START;
      break;

   case DOT1X_FSM_AUTH_PAE_STATE_AUTHENTICATED:
      priv_port->fsm_globals.portStatus = Authorized;
      fsm_pae->reAuthCount = 0;
      dot1x_inc(priv_port->fsm_globals.currentId);
      if (port->dot1x_instance->on_port_status_change != NULL)
      {
         port->dot1x_instance->on_port_status_change(port, priv_port->fsm_globals.portStatus);
      }
      break;

   case DOT1X_FSM_AUTH_PAE_STATE_ABORTING:
      dot1x_inc(priv_port->fsm_globals.currentId);
      if (!priv_port->fsm_globals.authTimeout)
      {
         /**
          * Setup a trigger for backend authentication state machine
          */
         *trigger_fsm  = dot1x_fsm_get(port, DOT1X_FSM_CODE_BACKEND_AUTHENTICATION, 0);
         *fsm_code     = DOT1X_FSM_CODE_BACKEND_AUTHENTICATION;
         *other_code   = 0;
         *trigger_code = DOT1X_FSM_BACKEND_AUTH_TRIGGER_AUTH_ABORT;
      }
      break;

   case DOT1X_FSM_AUTH_PAE_STATE_HELD:
      priv_port->fsm_globals.portStatus = Unauthorized;
      priv_port->fsm_globals.quiteWhile = priv_port->quietPeriod;
      fsm_pae->eapLogoff = False;
      dot1x_inc(priv_port->fsm_globals.currentId);
      break;

   case DOT1X_FSM_AUTH_PAE_STATE_FORCE_AUTH:
      priv_port->fsm_globals.portStatus = Authorized;
      fsm_pae->eapStart = False;
      _txCannedSuccess(port, fsm_pae);
      dot1x_inc(priv_port->fsm_globals.currentId);
      break;

   case DOT1X_FSM_AUTH_PAE_STATE_FORCE_UNAUTH:
      priv_port->fsm_globals.portStatus = Unauthorized;
      fsm_pae->eapStart = False;
      _txCannedFail(port, fsm_pae);
      dot1x_inc(priv_port->fsm_globals.currentId);
      break;
   }
}


static void _attempt_transition(dot1x_port_t *port, dot1x_fsm_auth_pae_t *fsm_pae, dot1x_fsm_t **trigger_fsm, int *fsm_code, int *other_code, int *trigger_code)
{
   dot1x_port_priv_t *priv_port;
   int               changed;

   priv_port = (dot1x_port_priv_t *)port;

   do
   {
      changed = 0;
      switch (fsm_pae->state)
      {
      case DOT1X_FSM_AUTH_PAE_STATE_INITIALIZE:
         break;

      case DOT1X_FSM_AUTH_PAE_STATE_DISCONNECTED:
         break;

      case DOT1X_FSM_AUTH_PAE_STATE_CONNECTING:
         if (((priv_port->fsm_globals.txWhen == 0) || fsm_pae->eapStart || priv_port->fsm_globals.reAuthenticate) &&
             (fsm_pae->reAuthCount <= priv_port->reAuthMax))
         {
            _on_state_entry(port, fsm_pae, trigger_fsm, fsm_code, other_code, trigger_code);
         }
         else if (fsm_pae->rxRespId && (fsm_pae->reAuthCount <= priv_port->reAuthMax))
         {
            fsm_pae->state = DOT1X_FSM_AUTH_PAE_STATE_AUTHENTICATING;
            dot1x_print(DOT1X_PRINT_LEVEL_INFORMATION,
                        "dot1x-fsm-auth-pae: Entering state AUTHENTICATING for %02x-%02x-%02x-%02x-%02x-%02x\n",
                        priv_port->base.address[0],
                        priv_port->base.address[1],
                        priv_port->base.address[2],
                        priv_port->base.address[3],
                        priv_port->base.address[4],
                        priv_port->base.address[5]);
            _on_state_entry(port, fsm_pae, trigger_fsm, fsm_code, other_code, trigger_code);
            changed = 1;
            DOT1X_PORT_STATISTICS_INC(priv_port, authEntersAuthenticating);
         }
         else if (fsm_pae->eapLogoff || (fsm_pae->reAuthCount > priv_port->reAuthMax))
         {
            fsm_pae->state = DOT1X_FSM_AUTH_PAE_STATE_DISCONNECTED;
            _on_state_entry(port, fsm_pae, trigger_fsm, fsm_code, other_code, trigger_code);
            changed = 1;
            DOT1X_PORT_STATISTICS_INC(priv_port, authEapLogoffsWhileConnecting);
         }
         break;

      case DOT1X_FSM_AUTH_PAE_STATE_AUTHENTICATING:
         if (priv_port->fsm_globals.authSuccess)
         {
            dot1x_print(DOT1X_PRINT_LEVEL_INFORMATION,
                        "dot1x-fsm-auth-pae: Entering state AUTHENTICATED for %02x-%02x-%02x-%02x-%02x-%02x\n",
                        priv_port->base.address[0],
                        priv_port->base.address[1],
                        priv_port->base.address[2],
                        priv_port->base.address[3],
                        priv_port->base.address[4],
                        priv_port->base.address[5]);
            fsm_pae->state = DOT1X_FSM_AUTH_PAE_STATE_AUTHENTICATED;
            _on_state_entry(port, fsm_pae, trigger_fsm, fsm_code, other_code, trigger_code);
            changed = 1;
            DOT1X_PORT_STATISTICS_INC(priv_port, authAuthSuccessWhileAuthenticating);
         }
         else if (priv_port->fsm_globals.authFail)
         {
            fsm_pae->state = DOT1X_FSM_AUTH_PAE_STATE_HELD;
            _on_state_entry(port, fsm_pae, trigger_fsm, fsm_code, other_code, trigger_code);
            changed = 1;
            DOT1X_PORT_STATISTICS_INC(priv_port, authAuthFailWhileAuthenticating);
         }
         else if (priv_port->fsm_globals.reAuthenticate ||
                  fsm_pae->eapStart ||
                  fsm_pae->eapLogoff ||
                  priv_port->fsm_globals.authTimeout)
         {
            fsm_pae->state = DOT1X_FSM_AUTH_PAE_STATE_ABORTING;
            _on_state_entry(port, fsm_pae, trigger_fsm, fsm_code, other_code, trigger_code);
            changed = 1;
            if (priv_port->fsm_globals.authTimeout)
            {
               DOT1X_PORT_STATISTICS_INC(priv_port, authAuthTimeoutsWhileAuthenticating);
            }
            if (priv_port->fsm_globals.reAuthenticate)
            {
               DOT1X_PORT_STATISTICS_INC(priv_port, authAuthReauthsWhileAuthenticating);
            }
            if (fsm_pae->eapStart)
            {
               DOT1X_PORT_STATISTICS_INC(priv_port, authAuthEapStartsWhileAuthenticating);
            }
            if (fsm_pae->eapLogoff)
            {
               DOT1X_PORT_STATISTICS_INC(priv_port, authAuthEapLogoffWhileAuthenticating);
            }
         }
         break;

      case DOT1X_FSM_AUTH_PAE_STATE_AUTHENTICATED:
         if (fsm_pae->eapStart ||
             priv_port->fsm_globals.reAuthenticate)
         {
            fsm_pae->state = DOT1X_FSM_AUTH_PAE_STATE_CONNECTING;
            _on_state_entry(port, fsm_pae, trigger_fsm, fsm_code, other_code, trigger_code);
            changed = 1;
            if (priv_port->fsm_globals.reAuthenticate)
            {
               DOT1X_PORT_STATISTICS_INC(priv_port, authAuthReauthsWhileAuthenticated);
            }
            if (fsm_pae->eapStart)
            {
               DOT1X_PORT_STATISTICS_INC(priv_port, authAuthEapStartsWhileAuthenticated);
            }
            if (port->dot1x_instance->on_port_status_change != NULL)
            {
               port->dot1x_instance->on_port_status_change(port, priv_port->fsm_globals.portStatus);
            }
         }
         else if (fsm_pae->eapLogoff)
         {
            fsm_pae->state = DOT1X_FSM_AUTH_PAE_STATE_DISCONNECTED;
            _on_state_entry(port, fsm_pae, trigger_fsm, fsm_code, other_code, trigger_code);
            changed = 1;
            DOT1X_PORT_STATISTICS_INC(priv_port, authAuthEapLogoffWhileAuthenticated);
            if (port->dot1x_instance->on_port_status_change != NULL)
            {
               port->dot1x_instance->on_port_status_change(port, priv_port->fsm_globals.portStatus);
            }
         }
         break;

      case DOT1X_FSM_AUTH_PAE_STATE_ABORTING:
         if (!fsm_pae->eapLogoff &&
             !priv_port->fsm_globals.authAbort)
         {
            fsm_pae->state = DOT1X_FSM_AUTH_PAE_STATE_CONNECTING;
            _on_state_entry(port, fsm_pae, trigger_fsm, fsm_code, other_code, trigger_code);
            changed = 1;
         }
         else if (fsm_pae->eapLogoff && !priv_port->fsm_globals.authAbort)
         {
            fsm_pae->state = DOT1X_FSM_AUTH_PAE_STATE_DISCONNECTED;
            _on_state_entry(port, fsm_pae, trigger_fsm, fsm_code, other_code, trigger_code);
            changed = 1;
         }
         break;

      case DOT1X_FSM_AUTH_PAE_STATE_HELD:
         if (priv_port->fsm_globals.quiteWhile == 0)
         {
            fsm_pae->state = DOT1X_FSM_AUTH_PAE_STATE_CONNECTING;
            _on_state_entry(port, fsm_pae, trigger_fsm, fsm_code, other_code, trigger_code);
            changed = 1;
         }
         break;

      case DOT1X_FSM_AUTH_PAE_STATE_FORCE_AUTH:
         if (fsm_pae->eapStart)
         {
            _on_state_entry(port, fsm_pae, trigger_fsm, fsm_code, other_code, trigger_code);
         }
         break;

      case DOT1X_FSM_AUTH_PAE_STATE_FORCE_UNAUTH:
         if (fsm_pae->eapStart)
         {
            _on_state_entry(port, fsm_pae, trigger_fsm, fsm_code, other_code, trigger_code);
         }
         break;
      }
   } while (changed);
}


static int _txCannedFail(dot1x_port_t *port, dot1x_fsm_auth_pae_t *fsm_pae)
{
   return dot1x_tx_canned_fail(port);
}


static int _txReqId(dot1x_port_t *port, dot1x_fsm_auth_pae_t *fsm_pae)
{
   dot1x_eapol_frame_t eapol_frame;
   dot1x_eap_frame_t   eap_frame;
   dot1x_port_priv_t   *priv_port;
   unsigned char       buffer[DOT1X_EAPOL_FRAME_MIN_LENGTH + DOT1X_EAP_MIN_LENGTH + 1];
   unsigned char       *p;

   priv_port = (dot1x_port_priv_t *)port;

   p = buffer + DOT1X_EAPOL_FRAME_MIN_LENGTH;
   eap_frame.code        = DOT1X_EAP_CODE_REQUEST;
   eap_frame.type        = DOT1X_EAP_TYPE_IDENTITY;
   eap_frame.id          = priv_port->fsm_globals.currentId;
   eap_frame.length      = 0;
   eap_frame.data        = NULL;
   eap_frame.data_length = 0;

   dot1x_eapol_eap_frame_create(&eap_frame, p);

   p = buffer;
   eapol_frame.version     = DOT1X_2001_EAPOL_VERSION;
   eapol_frame.type        = DOT1X_EAPOL_FRAME_TYPE_EAP;
   eapol_frame.body_length = DOT1X_EAP_MIN_LENGTH + 1;
   eapol_frame.body        = buffer + DOT1X_EAPOL_FRAME_MIN_LENGTH;

   dot1x_eapol_create_frame(&eapol_frame, p);

   return dot1x_eapol_tx(port, buffer, DOT1X_EAPOL_FRAME_MIN_LENGTH + DOT1X_EAP_MIN_LENGTH + 1);
}


static int _txCannedSuccess(dot1x_port_t *port, dot1x_fsm_auth_pae_t *fsm_pae)
{
   return dot1x_tx_canned_success(port);
}


static int _eapol_rx_eap(dot1x_port_t *port, dot1x_fsm_auth_pae_t *fsm_pae, dot1x_eapol_frame_t *frame)
{
   dot1x_eap_frame_t eap_frame;
   int               ret;
   dot1x_port_priv_t *priv_port;

   ret = dot1x_eapol_process_eap(frame, &eap_frame);

   if (ret)
   {
      goto _out;
   }

   priv_port = (dot1x_port_priv_t *)port;

   switch (eap_frame.code)
   {
   case DOT1X_EAP_CODE_RESPONSE:
      if ((eap_frame.type == DOT1X_EAP_TYPE_IDENTITY) &&
          (eap_frame.id == priv_port->fsm_globals.currentId))
      {
         fsm_pae->rxRespId = True;
         if (fsm_pae->identity_data != NULL)
         {
            dot1x_heap_free(fsm_pae->identity_data);
         }
         fsm_pae->identity_length = eap_frame.data_length;
         fsm_pae->identity_data   = (unsigned char *)dot1x_heap_alloc(fsm_pae->identity_length);
         memcpy(fsm_pae->identity_data, eap_frame.data, fsm_pae->identity_length);
      }
      else
      {
         ret = -1;
      }
      break;

   default:
      ret = -1;
   }

_out:
   return ret;
}


static int _eapol_rx_start(dot1x_port_t *port, dot1x_fsm_auth_pae_t *fsm_pae, dot1x_eapol_frame_t *frame)
{
   fsm_pae->eapStart = True;
   return 0;
}


static int _eapol_rx_logoff(dot1x_port_t *port, dot1x_fsm_auth_pae_t *fsm_pae, dot1x_eapol_frame_t *frame)
{
   fsm_pae->eapLogoff = True;
   return 0;
}


static int _tick(dot1x_port_t *port, int fsm_code, int other_code, struct dot1x_fsm *fsm)
{
   dot1x_fsm_auth_pae_t *fsm_pae;
   dot1x_fsm_t          *trigger_fsm;
   int          trigger_code;
   int          trigger_fsm_code;
   int          trigger_other_code;
   unsigned int lock_code;

   fsm_pae     = (dot1x_fsm_auth_pae_t *)fsm;
   trigger_fsm = NULL;

   dot1x_lock(fsm_pae->lock, lock_code);

   _attempt_transition(port, fsm_pae, &trigger_fsm, &trigger_fsm_code, &trigger_other_code, &trigger_code);

   dot1x_unlock(fsm_pae->lock, lock_code);

   if (trigger_fsm != NULL)
   {
      trigger_fsm->trigger(port, trigger_fsm_code, trigger_other_code, trigger_fsm, trigger_code);
   }

   return 0;
}


static int _eapol_rx_begin(dot1x_port_t *port, int fsm_code, int other_code, struct dot1x_fsm *fsm)
{
   dot1x_fsm_auth_pae_t *fsm_pae;
   unsigned int         lock_code;

   fsm_pae = (dot1x_fsm_auth_pae_t *)fsm;

   dot1x_lock(fsm_pae->lock, lock_code);

   fsm_pae->trigger_fsm = NULL;

   dot1x_unlock(fsm_pae->lock, lock_code);

   return 0;
}


static int _eapol_rx(dot1x_port_t *port, int fsm_code, int other_code, struct dot1x_fsm *fsm, dot1x_eapol_frame_t *frame)
{
   dot1x_fsm_auth_pae_t *fsm_pae;
   int          ret;
   unsigned int lock_code;

   fsm_pae = (dot1x_fsm_auth_pae_t *)fsm;

   if (fsm_pae->state == DOT1X_FSM_AUTH_PAE_STATE_INVALID)
   {
      return 0;
   }

   dot1x_lock(fsm_pae->lock, lock_code);

   switch (frame->type)
   {
   case DOT1X_EAPOL_FRAME_TYPE_EAP:
      ret = _eapol_rx_eap(port, fsm_pae, frame);
      break;

   case DOT1X_EAPOL_FRAME_TYPE_START:
      ret = _eapol_rx_start(port, fsm_pae, frame);
      break;

   case DOT1X_EAPOL_FRAME_TYPE_LOGOFF:
      ret = _eapol_rx_logoff(port, fsm_pae, frame);
      break;

   default:
      ret = -1;
   }

   _attempt_transition(port,
                       fsm_pae,
                       &fsm_pae->trigger_fsm,
                       &fsm_pae->trigger_fsm_code,
                       &fsm_pae->trigger_other_code,
                       &fsm_pae->trigger_code);

   dot1x_unlock(fsm_pae->lock, lock_code);

   return ret;
}


static int _eapol_rx_end(dot1x_port_t *port, int fsm_code, int other_code, struct dot1x_fsm *fsm)
{
   dot1x_fsm_auth_pae_t *fsm_pae;
   unsigned int         lock_code;

   fsm_pae = (dot1x_fsm_auth_pae_t *)fsm;

   dot1x_lock(fsm_pae->lock, lock_code);

   if (fsm_pae->trigger_fsm != NULL)
   {
      fsm_pae->trigger_fsm->trigger(port,
                                    fsm_pae->trigger_fsm_code,
                                    fsm_pae->trigger_other_code,
                                    fsm_pae->trigger_fsm,
                                    fsm_pae->trigger_code);
   }

   dot1x_unlock(fsm_pae->lock, lock_code);

   return 0;
}


static int _trigger(dot1x_port_t *port, int fsm_code, int other_code, struct dot1x_fsm *fsm, int trigger_code)
{
   dot1x_fsm_auth_pae_t *fsm_pae;
   dot1x_fsm_t          *trigger_fsm;
   int               new_trigger_code;
   int               trigger_fsm_code;
   int               trigger_other_code;
   dot1x_port_priv_t *priv_port;
   unsigned int      lock_code;

   fsm_pae     = (dot1x_fsm_auth_pae_t *)fsm;
   trigger_fsm = NULL;
   priv_port   = (dot1x_port_priv_t *)port;

   dot1x_lock(fsm_pae->lock, lock_code);

   switch (trigger_code)
   {
   case DOT1X_FSM_AUTH_PAE_TRIGGER_AUTH_TIMEOUT:
      priv_port->fsm_globals.authTimeout = True;
      _attempt_transition(port, fsm_pae, &trigger_fsm, &trigger_fsm_code, &trigger_other_code, &new_trigger_code);
      break;

   case DOT1X_FSM_AUTH_PAE_TRIGGER_AUTH_SUCCESS:
      priv_port->fsm_globals.authSuccess = True;
      _attempt_transition(port, fsm_pae, &trigger_fsm, &trigger_fsm_code, &trigger_other_code, &new_trigger_code);
      break;

   case DOT1X_FSM_AUTH_PAE_TRIGGER_AUTH_FAIL:
      priv_port->fsm_globals.authFail = True;
      _attempt_transition(port, fsm_pae, &trigger_fsm, &trigger_fsm_code, &trigger_other_code, &new_trigger_code);
      break;

   case DOT1X_FSM_AUTH_PAE_TRIGGER_REAUTHENTICATE:
      priv_port->fsm_globals.reAuthenticate = True;
      _attempt_transition(port, fsm_pae, &trigger_fsm, &trigger_fsm_code, &trigger_other_code, &new_trigger_code);
      break;
   }

   dot1x_unlock(fsm_pae->lock, lock_code);

   if (trigger_fsm != NULL)
   {
      trigger_fsm->trigger(port, trigger_fsm_code, trigger_other_code, trigger_fsm, new_trigger_code);
   }

   return 0;
}


static int _initialize(dot1x_port_t *port, int fsm_code, int other_code, struct dot1x_fsm *fsm)
{
   dot1x_fsm_auth_pae_t *fsm_pae;
   dot1x_fsm_t          *trigger_fsm;
   int          trigger_code;
   int          trigger_fsm_code;
   int          trigger_other_code;
   unsigned int lock_code;

   fsm_pae     = (dot1x_fsm_auth_pae_t *)fsm;
   trigger_fsm = NULL;

   dot1x_lock(fsm_pae->lock, lock_code);

   fsm_pae->state = DOT1X_FSM_AUTH_PAE_STATE_INITIALIZE;
   _on_state_entry(port, fsm_pae, &trigger_fsm, &trigger_fsm_code, &trigger_other_code, &trigger_code);
   _attempt_transition(port, fsm_pae, &trigger_fsm, &trigger_fsm_code, &trigger_other_code, &trigger_code);

   dot1x_unlock(fsm_pae->lock, lock_code);

   if (trigger_fsm != NULL)
   {
      trigger_fsm->trigger(port, trigger_fsm_code, trigger_other_code, trigger_fsm, trigger_code);
   }

   return 0;
}


static int _on_destroy(dot1x_port_t *port, struct dot1x_fsm *fsm)
{
   dot1x_fsm_auth_pae_t *fsm_pae;

   fsm_pae = (dot1x_fsm_auth_pae_t *)fsm;

   if (fsm_pae->identity_data != NULL)
   {
      dot1x_heap_free(fsm_pae->identity_data);
   }

   if (fsm_pae != NULL)
   {
      dot1x_destroy_lock(fsm_pae->lock);
      dot1x_heap_free(fsm_pae);
   }

   return 0;
}
