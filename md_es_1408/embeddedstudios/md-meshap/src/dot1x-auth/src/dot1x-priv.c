/********************************************************************************
* MeshDynamics
* --------------
* File     : dot1x-priv.c
* Comments : 802.1x authenticator private routines
* Created  : 1/25/2005
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  1  |6/16/2008 | Lock primitives changed                         | Sriram |
* -----------------------------------------------------------------------------
* |  0  |1/25/2005 | Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

#include "dot1x.h"
#include "dot1x-impl.h"
#include "dot1x-priv.h"
#include "dot1x-fsm-port-timers.h"
#include "dot1x-fsm-auth-pae.h"
#include "dot1x-fsm-auth-key-tx.h"
#include "dot1x-fsm-reauth-timer.h"
#include "dot1x-fsm-backend-auth.h"
#include "dot1x-fsm-key-rx.h"

static dot1x_backend_priv_t *_backend_list_head;
static unsigned int         _backend_list_lock;

static DOT1X_INLINE void _destroy_backend_list(void)
{
   dot1x_backend_priv_t *temp;
   dot1x_backend_priv_t *next;
   unsigned int         code;

   dot1x_lock(_backend_list_lock, code);

   temp = _backend_list_head;

   while (temp != NULL)
   {
      next = temp->next;
      dot1x_heap_free(temp);
      temp = next;
   }

   _backend_list_head = NULL;

   dot1x_unlock(_backend_list_lock, code);
}


void dot1x_priv_initialize()
{
   _backend_list_head = NULL;
   _backend_list_lock = dot1x_create_lock();
}


void dot1x_priv_uninitialize()
{
   _destroy_backend_list();
   dot1x_destroy_lock(_backend_list_lock);
}


dot1x_priv_t *dot1x_priv_create_instance(const char                 *instance_name,
                                         const char                 *backend_name,
                                         void                       *backend_param,
                                         void                       *instance_data,
                                         dot1x_port_status_change_t on_port_status_change,
                                         int                        port_hash_length)
{
   dot1x_priv_t          *instance;
   const dot1x_backend_t *backend;

   backend = dot1x_priv_backend_get(backend_name);

   if (backend == NULL)
   {
      return NULL;
   }

   instance = (dot1x_priv_t *)dot1x_heap_alloc(sizeof(dot1x_priv_t));

   memset(instance, 0, sizeof(dot1x_priv_t));

   strcpy(instance->base.backend_name, backend_name);
   strcpy(instance->base.instance_name, instance_name);

   instance->base.instance_data         = instance_data;
   instance->base.on_port_status_change = on_port_status_change;

   instance->backend          = backend;
   instance->port_hash_length = port_hash_length;
   instance->port_list        = NULL;
   instance->lock             = dot1x_create_lock();

   instance->port_hash = (dot1x_port_priv_t **)dot1x_heap_alloc(sizeof(dot1x_port_priv_t *) * port_hash_length);
   memset(instance->port_hash, 0, sizeof(dot1x_port_priv_t *) * port_hash_length);

   instance->backend->create_instance(&instance->base, backend_param);

   dot1x_crypt_initialize(&instance->base);

   dot1x_print(DOT1X_PRINT_LEVEL_INFORMATION, "dot1x-auth: Created instance %s:%s\n", instance_name, backend_name);

   return instance;
}


void dot1x_priv_destroy_instance(dot1x_priv_t *instance)
{
   unsigned int      lock_code;
   dot1x_port_priv_t *port;

   /**
    * First we destroy all the ports
    */

   dot1x_lock(instance->lock, lock_code);

   port = instance->port_list;

   while (port != NULL)
   {
      dot1x_priv_destroy_port(instance, port->base.address);
      port = port->next_list;
   }

   /**
    * Destroy the port hash
    */

   dot1x_heap_free(instance->port_hash);

   /**
    * Destroy the backend instance
    */

   instance->backend->destroy_instance(&instance->base);

   dot1x_crypt_uninitialize(&instance->base);

   dot1x_unlock(instance->lock, lock_code);

   dot1x_destroy_lock(instance->lock);

   dot1x_heap_free(instance);
}


int dot1x_priv_tick(dot1x_priv_t *instance)
{
   dot1x_port_priv_t *ports;
   unsigned int      lock_code;

   /** Tick the FSM of every port in the list */

   dot1x_lock(instance->lock, lock_code);

   ports = instance->port_list;

   while (ports != NULL)
   {
      dot1x_fsm_tick(&ports->base);
      ports = ports->next_list;
   }

   dot1x_unlock(instance->lock, lock_code);

   return 0;
}


#include "dot1x-priv-backend.c"
#include "dot1x-priv-port.c"
