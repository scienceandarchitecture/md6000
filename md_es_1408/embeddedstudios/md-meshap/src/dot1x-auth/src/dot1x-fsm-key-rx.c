/********************************************************************************
* MeshDynamics
* --------------
* File     : dot1x-fsm-key-rx.c
* Comments : 802.1x Key Recv FSM
* Created  : 1/28/2005
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  1  |6/16/2008 | Lock primitives changed                         | Sriram |
* -----------------------------------------------------------------------------
* |  0  |1/28/2005 | Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

#include "dot1x.h"
#include "dot1x-impl.h"
#include "dot1x-priv.h"
#include "dot1x-fsm-key-rx.h"

/**
 * FSM functions
 */

static int _tick(dot1x_port_t *port, int fsm_code, int other_code, struct dot1x_fsm *fsm);
static int _eapol_rx(dot1x_port_t *port, int fsm_code, int other_code, struct dot1x_fsm *fsm, dot1x_eapol_frame_t *frame);
static int _trigger(dot1x_port_t *port, int fsm_code, int other_code, struct dot1x_fsm *fsm, int trigger_code);
static int _initialize(dot1x_port_t *port, int fsm_code, int other_code, struct dot1x_fsm *fsm);
static int _on_destroy(dot1x_port_t *port, struct dot1x_fsm *fsm);

/**
 * Internal functions
 */

static void _on_state_entry(dot1x_port_t *port, dot1x_fsm_key_rx_t *fsm_key_rx);
static void _attempt_transition(dot1x_port_t *port, dot1x_fsm_key_rx_t *fsm_key_rx);
static void _processKey(dot1x_port_t *port, dot1x_fsm_key_rx_t *fsm_key_rx);

int dot1x_key_rx_fsm_create(dot1x_port_t *port)
{
   dot1x_fsm_key_rx_t *fsm_key_rx;
   int                err;

   fsm_key_rx = (dot1x_fsm_key_rx_t *)dot1x_heap_alloc(sizeof(dot1x_fsm_key_rx_t));

   memset(fsm_key_rx, 0, sizeof(dot1x_fsm_key_rx_t));

   fsm_key_rx->fsm.initialize = _initialize;
   fsm_key_rx->fsm.tick       = _tick;
   fsm_key_rx->fsm.eapol_rx   = _eapol_rx;
   fsm_key_rx->fsm.trigger    = _trigger;
   fsm_key_rx->fsm.on_destroy = _on_destroy;

   fsm_key_rx->lock  = dot1x_create_lock();
   fsm_key_rx->state = DOT1X_FSM_KEY_RX_STATE_INVALID;

   err = dot1x_fsm_register(port, DOT1X_FSM_CODE_KEY_RECEIVE, 0, &fsm_key_rx->fsm);

   if (err)
   {
      dot1x_destroy_lock(fsm_key_rx->lock);
      dot1x_heap_free(fsm_key_rx);
   }

   dot1x_print(DOT1X_PRINT_LEVEL_FLOW,
               "dot1x-auth: Key Receive FSM initialized for %s:%02x-%02x-%02x-%02x-%02x-%02x\n",
               port->dot1x_instance->instance_name,
               port->address[0],
               port->address[1],
               port->address[2],
               port->address[3],
               port->address[4],
               port->address[5]);

   return err;
}


int dot1x_key_rx_fsm_destroy(dot1x_port_t *port)
{
   dot1x_fsm_t *fsm;

   fsm = dot1x_fsm_get(port, DOT1X_FSM_CODE_KEY_RECEIVE, 0);

   if (fsm != NULL)
   {
      _on_destroy(port, fsm);
   }

   dot1x_fsm_unregister(port, DOT1X_FSM_CODE_KEY_RECEIVE, 0);

   return 0;
}


static void _on_state_entry(dot1x_port_t *port, dot1x_fsm_key_rx_t *fsm_key_rx)
{
   switch (fsm_key_rx->state)
   {
   case DOT1X_FSM_KEY_RX_STATE_NO_KEY:
      break;

   case DOT1X_FSM_KEY_RX_STATE_KEY_RX:
      _processKey(port, fsm_key_rx);
      fsm_key_rx->rxKey = False;
      break;
   }
}


static void _attempt_transition(dot1x_port_t *port, dot1x_fsm_key_rx_t *fsm_key_rx)
{
   int changed;

   do
   {
      changed = 0;
      switch (fsm_key_rx->state)
      {
      case DOT1X_FSM_KEY_RX_STATE_NO_KEY:
         if (fsm_key_rx->rxKey)
         {
            fsm_key_rx->state = DOT1X_FSM_KEY_RX_STATE_KEY_RX;
            _on_state_entry(port, fsm_key_rx);
            changed = 1;
         }
         break;

      case DOT1X_FSM_KEY_RX_STATE_KEY_RX:
         if (fsm_key_rx->rxKey)
         {
            _on_state_entry(port, fsm_key_rx);
         }
         break;
      }
   } while (changed);
}


static void _processKey(dot1x_port_t *port, dot1x_fsm_key_rx_t *fsm_key_rx)
{
}


static int _tick(dot1x_port_t *port, int fsm_code, int other_code, struct dot1x_fsm *fsm)
{
   dot1x_fsm_key_rx_t *fsm_key_rx;
   unsigned int       lock_code;

   fsm_key_rx = (dot1x_fsm_key_rx_t *)fsm;

   dot1x_lock(fsm_key_rx->lock, lock_code);

   _attempt_transition(port, fsm_key_rx);

   dot1x_unlock(fsm_key_rx->lock, lock_code);

   return 0;
}


static int _eapol_rx(dot1x_port_t *port, int fsm_code, int other_code, struct dot1x_fsm *fsm, dot1x_eapol_frame_t *frame)
{
   return 0;
}


static int _trigger(dot1x_port_t *port, int fsm_code, int other_code, struct dot1x_fsm *fsm, int trigger_code)
{
   return 0;
}


static int _initialize(dot1x_port_t *port, int fsm_code, int other_code, struct dot1x_fsm *fsm)
{
   dot1x_fsm_key_rx_t *fsm_key_rx;
   unsigned int       lock_code;

   fsm_key_rx = (dot1x_fsm_key_rx_t *)fsm;

   dot1x_lock(fsm_key_rx->lock, lock_code);

   fsm_key_rx->state = DOT1X_FSM_KEY_RX_STATE_NO_KEY;
   _on_state_entry(port, fsm_key_rx);
   _attempt_transition(port, fsm_key_rx);

   dot1x_unlock(fsm_key_rx->lock, lock_code);

   return 0;
}


static int _on_destroy(dot1x_port_t *port, struct dot1x_fsm *fsm)
{
   dot1x_fsm_key_rx_t *fsm_key_rx;

   fsm_key_rx = (dot1x_fsm_key_rx_t *)fsm;

   if (fsm_key_rx != NULL)
   {
      dot1x_destroy_lock(fsm_key_rx->lock);
      dot1x_heap_free(fsm_key_rx);
   }

   return 0;
}
