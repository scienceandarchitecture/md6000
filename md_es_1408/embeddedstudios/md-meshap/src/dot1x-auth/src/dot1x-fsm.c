/********************************************************************************
* MeshDynamics
* --------------
* File     : dot1x-fsm.c
* Comments : 802.1x State Machine
* Created  : 1/25/2005
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  1  |11/17/2008| Serious spinlock issue fixed                    | Sriram |
* -----------------------------------------------------------------------------
* |  2  |6/16/2008 | Lock primitives changed                         | Sriram |
* -----------------------------------------------------------------------------
* |  1  |4/7/2005  | Misc changes                                    | Sriram |
* -----------------------------------------------------------------------------
* |  0  |1/25/2005 | Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

#include "dot1x.h"
#include "dot1x-impl.h"
#include "dot1x-priv.h"

int dot1x_fsm_register(dot1x_port_t *port, int fsm_code, int other_code, dot1x_fsm_t *fsm)
{
   dot1x_port_priv_t *priv_port;
   dot1x_fsm_priv_t  *priv_fsm;
   unsigned int      lock_code;

   priv_port = (dot1x_port_priv_t *)port;

   if (fsm_code == DOT1X_FSM_CODE_OTHER)
   {
      dot1x_fsm_unregister(port, fsm_code, other_code);

      priv_fsm = (dot1x_fsm_priv_t *)dot1x_heap_alloc(sizeof(dot1x_fsm_priv_t));
      memset(priv_fsm, 0, sizeof(dot1x_fsm_priv_t));

      priv_fsm->fsm  = fsm;
      priv_fsm->code = other_code;

      dot1x_lock(priv_port->lock, lock_code);

      priv_fsm->next        = priv_port->other_fsms;
      priv_port->other_fsms = priv_fsm;

      dot1x_unlock(priv_port->lock, lock_code);
   }
   else if ((fsm_code >= 0) && (fsm_code < DOT1X_FSM_STANDARD_COUNT))
   {
      dot1x_lock(priv_port->lock, lock_code);

      priv_port->standard_fsms[fsm_code] = fsm;

      dot1x_unlock(priv_port->lock, lock_code);
   }
   else
   {
      return -1;
   }

   return 0;
}


int dot1x_fsm_unregister(dot1x_port_t *port, int fsm_code, int other_code)
{
   dot1x_port_priv_t *priv_port;
   dot1x_fsm_priv_t  *priv_fsm;
   dot1x_fsm_priv_t  *priv_fsm_prev;
   unsigned int      lock_code;

   priv_port = (dot1x_port_priv_t *)port;

   if (fsm_code == DOT1X_FSM_CODE_OTHER)
   {
      dot1x_lock(priv_port->lock, lock_code);

      priv_fsm      = priv_port->other_fsms;
      priv_fsm_prev = NULL;

      while (priv_fsm != NULL)
      {
         if (priv_fsm->code == other_code)
         {
            if (priv_fsm_prev != NULL)
            {
               priv_fsm_prev->next = priv_fsm->next;
            }
            else
            {
               priv_port->other_fsms = priv_fsm->next;
            }
            dot1x_heap_free(priv_fsm);
            break;
         }
         priv_fsm_prev = priv_fsm;
         priv_fsm      = priv_fsm->next;
      }

      dot1x_unlock(priv_port->lock, lock_code);
   }
   else if ((fsm_code >= 0) && (fsm_code < DOT1X_FSM_STANDARD_COUNT))
   {
      dot1x_lock(priv_port->lock, lock_code);

      priv_port->standard_fsms[fsm_code] = NULL;

      dot1x_unlock(priv_port->lock, lock_code);
   }
   else
   {
      return -1;
   }

   return 0;
}


dot1x_fsm_t *dot1x_fsm_get(dot1x_port_t *port, int fsm_code, int other_code)
{
   dot1x_port_priv_t *priv_port;
   dot1x_fsm_priv_t  *priv_fsm;
   dot1x_fsm_t       *fsm;
   unsigned int      lock_code;

   priv_port = (dot1x_port_priv_t *)port;

   if (fsm_code == DOT1X_FSM_CODE_OTHER)
   {
      dot1x_lock(priv_port->lock, lock_code);

      priv_fsm = priv_port->other_fsms;

      while (priv_fsm != NULL)
      {
         if (priv_fsm->code == other_code)
         {
            fsm = priv_fsm->fsm;
            dot1x_unlock(priv_port->lock, lock_code);
            return fsm;
         }
         priv_fsm = priv_fsm->next;
      }

      dot1x_unlock(priv_port->lock, lock_code);
   }
   else if ((fsm_code >= 0) && (fsm_code < DOT1X_FSM_STANDARD_COUNT))
   {
      dot1x_lock(priv_port->lock, lock_code);
      fsm = priv_port->standard_fsms[fsm_code];
      dot1x_unlock(priv_port->lock, lock_code);
      return fsm;
   }

   return NULL;
}


int dot1x_fsm_tick(dot1x_port_t *port)
{
   dot1x_port_priv_t *priv_port;
   dot1x_fsm_priv_t  *priv_fsm;
   unsigned int      lock_code;
   int               i;

   /**
    * Call the tick function of very FSM registered for this port
    */

   priv_port = (dot1x_port_priv_t *)port;

   dot1x_lock(priv_port->lock, lock_code);

   for (i = 0; i < DOT1X_FSM_STANDARD_COUNT; i++)
   {
      if ((priv_port->standard_fsms[i] != NULL) &&
          (priv_port->standard_fsms[i]->tick != NULL))
      {
         priv_port->standard_fsms[i]->tick(port, i, 0, priv_port->standard_fsms[i]);
      }
   }

   priv_fsm = priv_port->other_fsms;

   while (priv_fsm != NULL)
   {
      if ((priv_fsm->fsm != NULL) &&
          (priv_fsm->fsm->tick != NULL))
      {
         priv_fsm->fsm->tick(port, DOT1X_FSM_CODE_OTHER, priv_fsm->code, priv_fsm->fsm);
      }
      priv_fsm = priv_fsm->next;
   }

   dot1x_unlock(priv_port->lock, lock_code);

   return 0;
}


#define _EAPOL_RX_HELPER_BEGIN    1
#define _EAPOL_RX_HELPER          2
#define _EAPOL_RX_HELPER_END      3

static DOT1X_INLINE int _eapol_rx_helper(dot1x_port_priv_t *priv_port, dot1x_eapol_frame_t *frame, int helper_type)
{
   int              i;
   dot1x_fsm_priv_t *priv_fsm;

   for (i = 0; i < DOT1X_FSM_STANDARD_COUNT; i++)
   {
      if (priv_port->standard_fsms[i] != NULL)
      {
         switch (helper_type)
         {
         case _EAPOL_RX_HELPER_BEGIN:
            if (priv_port->standard_fsms[i]->eapol_rx_begin != NULL)
            {
               priv_port->standard_fsms[i]->eapol_rx_begin(&priv_port->base, i, 0, priv_port->standard_fsms[i]);
            }
            break;

         case _EAPOL_RX_HELPER:
            if (priv_port->standard_fsms[i]->eapol_rx != NULL)
            {
               priv_port->standard_fsms[i]->eapol_rx(&priv_port->base, i, 0, priv_port->standard_fsms[i], frame);
            }
            break;

         case _EAPOL_RX_HELPER_END:
            if (priv_port->standard_fsms[i]->eapol_rx_end != NULL)
            {
               priv_port->standard_fsms[i]->eapol_rx_end(&priv_port->base, i, 0, priv_port->standard_fsms[i]);
            }
            break;
         }
      }
   }

   priv_fsm = priv_port->other_fsms;

   while (priv_fsm != NULL)
   {
      if (priv_fsm->fsm != NULL)
      {
         switch (helper_type)
         {
         case _EAPOL_RX_HELPER_BEGIN:
            if (priv_fsm->fsm->eapol_rx_begin != NULL)
            {
               priv_fsm->fsm->eapol_rx_begin(&priv_port->base, DOT1X_FSM_CODE_OTHER, priv_fsm->code, priv_fsm->fsm);
            }
            break;

         case _EAPOL_RX_HELPER:
            if (priv_fsm->fsm->eapol_rx != NULL)
            {
               priv_fsm->fsm->eapol_rx(&priv_port->base, DOT1X_FSM_CODE_OTHER, priv_fsm->code, priv_fsm->fsm, frame);
            }
            break;

         case _EAPOL_RX_HELPER_END:
            if (priv_fsm->fsm->eapol_rx_end != NULL)
            {
               priv_fsm->fsm->eapol_rx_end(&priv_port->base, DOT1X_FSM_CODE_OTHER, priv_fsm->code, priv_fsm->fsm);
            }
            break;
         }
      }
      priv_fsm = priv_fsm->next;
   }

   return 0;
}


int dot1x_fsm_eapol_rx(dot1x_port_t *port, dot1x_eapol_frame_t *frame)
{
   dot1x_port_priv_t *priv_port;
   unsigned int      lock_code;

   /**
    * Call the eapol_rx function of very FSM registered for this port
    */

   priv_port = (dot1x_port_priv_t *)port;
   dot1x_lock(priv_port->lock, lock_code);

   _eapol_rx_helper(priv_port, frame, _EAPOL_RX_HELPER_BEGIN);
   _eapol_rx_helper(priv_port, frame, _EAPOL_RX_HELPER);
   _eapol_rx_helper(priv_port, frame, _EAPOL_RX_HELPER_END);

   dot1x_unlock(priv_port->lock, lock_code);

   return 0;
}


#undef _EAPOL_RX_HELPER_BEGIN
#undef _EAPOL_RX_HELPER
#undef _EAPOL_RX_HELPER_END

int dot1x_fsm_initialize(dot1x_port_t *port)
{
   dot1x_port_priv_t *priv_port;
   dot1x_fsm_priv_t  *priv_fsm;
   unsigned int      lock_code;
   int               i;

   /**
    * Call the initialize function of very FSM registered for this port
    */

   priv_port = (dot1x_port_priv_t *)port;
   dot1x_lock(priv_port->lock, lock_code);


   for (i = 0; i < DOT1X_FSM_STANDARD_COUNT; i++)
   {
      if ((priv_port->standard_fsms[i] != NULL) &&
          (priv_port->standard_fsms[i]->initialize != NULL))
      {
         priv_port->standard_fsms[i]->initialize(port, i, 0, priv_port->standard_fsms[i]);
      }
   }

   priv_fsm = priv_port->other_fsms;

   while (priv_fsm != NULL)
   {
      if ((priv_fsm->fsm != NULL) &&
          (priv_fsm->fsm->initialize != NULL))
      {
         priv_fsm->fsm->initialize(port, DOT1X_FSM_CODE_OTHER, priv_fsm->code, priv_fsm->fsm);
      }
      priv_fsm = priv_fsm->next;
   }

   dot1x_unlock(priv_port->lock, lock_code);

   return 0;
}


int dot1x_fsm_unregister_all(dot1x_port_t *port)
{
   dot1x_port_priv_t *priv_port;
   dot1x_fsm_priv_t  *priv_fsm;
   dot1x_fsm_priv_t  *temp;
   unsigned int      lock_code;
   int               i;

   /**
    * Call the initialize function of very FSM registered for this port
    */

   priv_port = (dot1x_port_priv_t *)port;
   dot1x_lock(priv_port->lock, lock_code);


   for (i = 0; i < DOT1X_FSM_STANDARD_COUNT; i++)
   {
      if ((priv_port->standard_fsms[i] != NULL) &&
          (priv_port->standard_fsms[i]->on_destroy != NULL))
      {
         priv_port->standard_fsms[i]->on_destroy(port, priv_port->standard_fsms[i]);
      }
   }

   priv_fsm = priv_port->other_fsms;

   while (priv_fsm != NULL)
   {
      if ((priv_fsm->fsm != NULL) &&
          (priv_fsm->fsm->on_destroy != NULL))
      {
         priv_fsm->fsm->on_destroy(port, priv_fsm->fsm);
      }
      temp     = priv_fsm;
      priv_fsm = priv_fsm->next;
      dot1x_heap_free(temp);
   }

   priv_port->other_fsms = NULL;

   dot1x_unlock(priv_port->lock, lock_code);

   return 0;
}
