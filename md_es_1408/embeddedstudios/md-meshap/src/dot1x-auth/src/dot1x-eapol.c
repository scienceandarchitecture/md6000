/********************************************************************************
* MeshDynamics
* --------------
* File     : dot1x-eapol.c
* Comments : 802.1x EAPOL Helper Functions
* Created  : 1/27/2005
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  0  |1/27/2005 | Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

#include "dot1x.h"
#include "dot1x-impl.h"
#include "dot1x-priv.h"
#include "dot1x-eapol.h"

/**
 * Field positions for a EAPOL frame
 */

#define _DOT1X_EAPOL_FRAME_VERSION_POSITION    0
#define _DOT1X_EAPOL_FRAME_TYPE_POSITION       (_DOT1X_EAPOL_FRAME_VERSION_POSITION + 1)
#define _DOT1X_EAPOL_FRAME_LENGTH_POSITION     (_DOT1X_EAPOL_FRAME_TYPE_POSITION + 1)
#define _DOT1X_EAPOL_FRAME_BODY_POSITION       (_DOT1X_EAPOL_FRAME_LENGTH_POSITION + 2)

/**
 * Field positions for a EAPOL KEY Frame's body
 */

#define _DOT1X_EAPOL_KEY_BODY_TYPE_POSITION         0
#define _DOT1X_EAPOL_KEY_BODY_LENGTH_POSITION       (_DOT1X_EAPOL_KEY_BODY_TYPE_POSITION + 1)
#define _DOT1X_EAPOL_KEY_BODY_REPCNT_POSITION       (_DOT1X_EAPOL_KEY_BODY_LENGTH_POSITION + 2)
#define _DOT1X_EAPOL_KEY_BODY_IV_POSITION           (_DOT1X_EAPOL_KEY_BODY_REPCNT_POSITION + 8)
#define _DOT1X_EAPOL_KEY_BODY_INDEX_POSITION        (_DOT1X_EAPOL_KEY_BODY_IV_POSITION + 16)
#define _DOT1X_EAPOL_KEY_BODY_SIGNATURE_POSITION    (_DOT1X_EAPOL_KEY_BODY_INDEX_POSITION + 1)
#define _DOT1X_EAPOL_KEY_BODY_KEY_POSITION          (_DOT1X_EAPOL_KEY_BODY_SIGNATURE_POSITION + 16)

/**
 * Field positions for a EAPOL EAP Frame's Body
 */

#define _DOT1X_EAP_CODE_POSITION      0
#define _DOT1X_EAP_ID_POSITION        (_DOT1X_EAP_CODE_POSITION + 1)
#define _DOT1X_EAP_LENGTH_POSITION    (_DOT1X_EAP_ID_POSITION + 1)
#define _DOT1X_EAP_DATA_POSITION      (_DOT1X_EAP_LENGTH_POSITION + 2)

int dot1x_eapol_process_frame(unsigned char *packet_data, int length, dot1x_eapol_frame_t *frame_desc)
{
   unsigned short s;

   if (length < 4)
   {
      return -1;
   }

   memset(frame_desc, 0, sizeof(dot1x_eapol_frame_t));

   frame_desc->version = packet_data[_DOT1X_EAPOL_FRAME_VERSION_POSITION];
   frame_desc->type    = packet_data[_DOT1X_EAPOL_FRAME_TYPE_POSITION];

   memcpy(&s, &packet_data[_DOT1X_EAPOL_FRAME_LENGTH_POSITION], 2);
   frame_desc->body_length = ntohs(s);

   frame_desc->body = &packet_data[_DOT1X_EAPOL_FRAME_BODY_POSITION];

   return 0;
}


int dot1x_eapol_process_key(dot1x_eapol_frame_t *frame_desc, dot1x_eapol_key_body_t *key_desc)
{
   unsigned short s;

   if (frame_desc->type != DOT1X_EAPOL_FRAME_TYPE_KEY)
   {
      return -1;
   }

   if (frame_desc->body_length < 44)
   {
      return -2;
   }

   memset(key_desc, 0, sizeof(dot1x_eapol_key_body_t));

   key_desc->type = frame_desc->body[_DOT1X_EAPOL_KEY_BODY_TYPE_POSITION];

   memcpy(&s, &frame_desc->body[_DOT1X_EAPOL_KEY_BODY_LENGTH_POSITION], 2);
   key_desc->length = ntohs(s);

#if (DOT1X_IMPL_BYTE_ORDER == DOT1X_LE)
   for (i = 0; i < sizeof(key_desc->replay_counter); i++)
   {
      key_desc->replay_counter[sizeof(key_desc->replay_counter) - 1 - i] = frame_desc->body[_DOT1X_EAPOL_KEY_BODY_REPCNT_POSITION + i];
   }
#else
   memcpy(key_desc->replay_counter, &frame_desc->body[_DOT1X_EAPOL_KEY_BODY_REPCNT_POSITION], sizeof(key_desc->replay_counter));
#endif

   /**
    * Section 7.6.4 of IEEE 802.1X-2001 doesn't specify if
    * there is any endian-ness for the IV
    */

   memcpy(key_desc->iv, &frame_desc->body[_DOT1X_EAPOL_KEY_BODY_IV_POSITION], sizeof(key_desc->iv));

   key_desc->index = frame_desc->body[_DOT1X_EAPOL_KEY_BODY_INDEX_POSITION];

   memcpy(key_desc->signature, &frame_desc->body[_DOT1X_EAPOL_KEY_BODY_SIGNATURE_POSITION], sizeof(key_desc->signature));

   key_desc->key = &frame_desc->body[_DOT1X_EAPOL_KEY_BODY_KEY_POSITION];

   return 0;
}


int dot1x_eapol_process_eap(dot1x_eapol_frame_t *frame_desc, dot1x_eap_frame_t *eap_desc)
{
   unsigned short s;

   if (frame_desc->type != DOT1X_EAPOL_FRAME_TYPE_EAP)
   {
      return -1;
   }

   if (frame_desc->body_length < 5)
   {
      return -2;
   }

   memset(eap_desc, 0, sizeof(dot1x_eap_frame_t));

   eap_desc->code = frame_desc->body[_DOT1X_EAP_CODE_POSITION];
   eap_desc->id   = frame_desc->body[_DOT1X_EAP_ID_POSITION];

   memcpy(&s, &frame_desc->body[_DOT1X_EAP_LENGTH_POSITION], 2);
   eap_desc->length = ntohs(s);

   if ((eap_desc->code == DOT1X_EAP_CODE_REQUEST) ||
       (eap_desc->code == DOT1X_EAP_CODE_RESPONSE))
   {
      eap_desc->type        = frame_desc->body[_DOT1X_EAP_DATA_POSITION];
      eap_desc->data        = &frame_desc->body[_DOT1X_EAP_DATA_POSITION + 1];
      eap_desc->data_length = eap_desc->length - DOT1X_EAP_MIN_LENGTH - 1;
   }
   else
   {
      eap_desc->data        = &frame_desc->body[_DOT1X_EAP_DATA_POSITION];
      eap_desc->data_length = eap_desc->length - DOT1X_EAP_MIN_LENGTH;
   }

   return 0;
}


int dot1x_eapol_create_frame(dot1x_eapol_frame_t *frame_desc, unsigned char *packet)
{
   unsigned short s;

   packet[_DOT1X_EAPOL_FRAME_VERSION_POSITION] = frame_desc->version;
   packet[_DOT1X_EAPOL_FRAME_TYPE_POSITION]    = frame_desc->type;
   s = htons(frame_desc->body_length);
   memcpy(&packet[_DOT1X_EAPOL_FRAME_LENGTH_POSITION], &s, 2);

   if ((frame_desc->body != NULL) && (frame_desc->body_length > 0))
   {
      memcpy(&packet[_DOT1X_EAPOL_FRAME_BODY_POSITION], frame_desc->body, frame_desc->body_length);
   }

   return _DOT1X_EAPOL_FRAME_BODY_POSITION + frame_desc->body_length;
}


int dot1x_eapol_key_body_create(dot1x_eapol_key_body_t *key_desc, unsigned char *packet)
{
   unsigned short s;

   packet[_DOT1X_EAPOL_KEY_BODY_TYPE_POSITION] = key_desc->type;

   s = htons(key_desc->length);
   memcpy(&packet[_DOT1X_EAPOL_KEY_BODY_LENGTH_POSITION], &s, 2);

#if (DOT1X_IMPL_BYTE_ORDER == DOT1X_LE)
   for (i = 0; i < sizeof(key_desc->replay_counter); i++)
   {
      packet[_DOT1X_EAPOL_KEY_BODY_REPCNT_POSITION + i] = key_desc->replay_counter[sizeof(key_desc->replay_counter) - 1 - i];
   }
#else
   memcpy(&packet[_DOT1X_EAPOL_KEY_BODY_REPCNT_POSITION], key_desc->replay_counter, sizeof(key_desc->replay_counter));
#endif

   /**
    * Section 7.6.4 of IEEE 802.1X-2001 doesn't specify if
    * there is any endian-ness for the IV
    */

   memcpy(&packet[_DOT1X_EAPOL_KEY_BODY_IV_POSITION], key_desc->iv, sizeof(key_desc->iv));

   packet[_DOT1X_EAPOL_KEY_BODY_INDEX_POSITION] = key_desc->index;

   memcpy(&packet[_DOT1X_EAPOL_KEY_BODY_SIGNATURE_POSITION], key_desc->signature, sizeof(key_desc->signature));

   memcpy(&packet[_DOT1X_EAPOL_KEY_BODY_KEY_POSITION], key_desc->key, key_desc->length);

   return _DOT1X_EAPOL_KEY_BODY_KEY_POSITION + key_desc->length;
}


int dot1x_eapol_eap_frame_create(dot1x_eap_frame_t *eap_desc, unsigned char *packet)
{
   unsigned short s;

   packet[_DOT1X_EAP_CODE_POSITION] = eap_desc->code;
   packet[_DOT1X_EAP_ID_POSITION]   = eap_desc->id;

   if ((eap_desc->code == DOT1X_EAP_CODE_REQUEST) ||
       (eap_desc->code == DOT1X_EAP_CODE_RESPONSE))
   {
      s = DOT1X_EAP_MIN_LENGTH + eap_desc->data_length + 1;
      s = htons(s);
      memcpy(&packet[_DOT1X_EAP_LENGTH_POSITION], &s, 2);
      packet[_DOT1X_EAP_DATA_POSITION] = eap_desc->type;
      if ((eap_desc->data != NULL) && (eap_desc->data_length > 0))
      {
         memcpy(&packet[_DOT1X_EAP_DATA_POSITION + 1], eap_desc->data, eap_desc->data_length);
      }
      return _DOT1X_EAP_DATA_POSITION + eap_desc->data_length + 1;
   }
   else
   {
      s = DOT1X_EAP_MIN_LENGTH + eap_desc->data_length;
      s = htons(s);
      memcpy(&packet[_DOT1X_EAP_LENGTH_POSITION], &s, 2);
      if ((eap_desc->data != NULL) && (eap_desc->data_length > 0))
      {
         memcpy(&packet[_DOT1X_EAP_DATA_POSITION + 1], eap_desc->data, eap_desc->data_length);
      }
      return _DOT1X_EAP_DATA_POSITION + eap_desc->data_length;
   }

   return 0;
}


int dot1x_eapol_eap_frame_process(unsigned char *packet, int packet_length, dot1x_eap_frame_t *eap_desc)
{
   unsigned short s;

   memset(eap_desc, 0, sizeof(dot1x_eap_frame_t));

   eap_desc->code = packet[_DOT1X_EAP_CODE_POSITION];
   eap_desc->id   = packet[_DOT1X_EAP_ID_POSITION];

   memcpy(&s, &packet[_DOT1X_EAP_LENGTH_POSITION], 2);
   eap_desc->length = ntohs(s);

   if ((eap_desc->code == DOT1X_EAP_CODE_REQUEST) ||
       (eap_desc->code == DOT1X_EAP_CODE_RESPONSE))
   {
      eap_desc->type        = packet[_DOT1X_EAP_DATA_POSITION];
      eap_desc->data        = &packet[_DOT1X_EAP_DATA_POSITION + 1];
      eap_desc->data_length = eap_desc->length - DOT1X_EAP_MIN_LENGTH - 1;
   }
   else
   {
      eap_desc->data        = &packet[_DOT1X_EAP_DATA_POSITION];
      eap_desc->data_length = eap_desc->length - DOT1X_EAP_MIN_LENGTH;
   }

   return 0;
}
