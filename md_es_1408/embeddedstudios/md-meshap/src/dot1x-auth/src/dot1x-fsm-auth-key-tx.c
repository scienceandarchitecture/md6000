/********************************************************************************
* MeshDynamics
* --------------
* File     : dot1x-fsm-auth-key-tx.c
* Comments : 802.1x Authenticator Key Transmit FSM
* Created  : 1/27/2005
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  2  |6/16/2008 | Lock primitives changed                         | Sriram |
* -----------------------------------------------------------------------------
* |  0  |1/27/2005 | Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

#include "dot1x.h"
#include "dot1x-impl.h"
#include "dot1x-priv.h"
#include "dot1x-fsm-auth-key-tx.h"
#include "dot1x-backend-radius.h"

/**
 * FSM functions
 */

static int _tick(dot1x_port_t *port, int fsm_code, int other_code, struct dot1x_fsm *fsm);
static int _eapol_rx(dot1x_port_t *port, int fsm_code, int other_code, struct dot1x_fsm *fsm, dot1x_eapol_frame_t *frame);
static int _trigger(dot1x_port_t *port, int fsm_code, int other_code, struct dot1x_fsm *fsm, int trigger_code);
static int _initialize(dot1x_port_t *port, int fsm_code, int other_code, struct dot1x_fsm *fsm);
static int _on_destroy(dot1x_port_t *port, struct dot1x_fsm *fsm);


/**
 * Internal functions
 */

static void _on_state_entry(dot1x_port_t *port, dot1x_fsm_auth_tx_key_t *fsm_auth_tx);
static void _attempt_transition(dot1x_port_t *port, dot1x_fsm_auth_tx_key_t *fsm_auth_tx);
static int _txKey(dot1x_port_t *port, dot1x_fsm_auth_tx_key_t *fsm_auth_tx);

int dot1x_auth_tx_key_fsm_create(dot1x_port_t *port)
{
   dot1x_fsm_auth_tx_key_t *fsm_auth_tx;
   int err;

   fsm_auth_tx = (dot1x_fsm_auth_tx_key_t *)dot1x_heap_alloc(sizeof(dot1x_fsm_auth_tx_key_t));

   memset(fsm_auth_tx, 0, sizeof(dot1x_fsm_auth_tx_key_t));

   fsm_auth_tx->fsm.initialize     = _initialize;
   fsm_auth_tx->fsm.tick           = _tick;
   fsm_auth_tx->fsm.eapol_rx_begin = NULL;
   fsm_auth_tx->fsm.eapol_rx       = _eapol_rx;
   fsm_auth_tx->fsm.eapol_rx_end   = NULL;
   fsm_auth_tx->fsm.trigger        = _trigger;
   fsm_auth_tx->fsm.on_destroy     = _on_destroy;

   fsm_auth_tx->lock  = dot1x_create_lock();
   fsm_auth_tx->state = DOT1X_FSM_AUTH_KEY_TX_STATE_INVALID;

   err = dot1x_fsm_register(port, DOT1X_FSM_CODE_AUTHENTICATOR_KEY_TRANSMIT, 0, &fsm_auth_tx->fsm);

   if (err)
   {
      dot1x_destroy_lock(fsm_auth_tx->lock);
      dot1x_heap_free(fsm_auth_tx);
   }

   dot1x_print(DOT1X_PRINT_LEVEL_FLOW,
               "dot1x-auth: Key Transmit FSM initialized for %s:%02x-%02x-%02x-%02x-%02x-%02x\n",
               port->dot1x_instance->instance_name,
               port->address[0],
               port->address[1],
               port->address[2],
               port->address[3],
               port->address[4],
               port->address[5]);

   return err;
}


int dot1x_auth_tx_key_fsm_destroy(dot1x_port_t *port)
{
   dot1x_fsm_t *fsm;

   fsm = dot1x_fsm_get(port, DOT1X_FSM_CODE_AUTHENTICATOR_KEY_TRANSMIT, 0);

   if (fsm != NULL)
   {
      _on_destroy(port, fsm);
   }

   dot1x_fsm_unregister(port, DOT1X_FSM_CODE_AUTHENTICATOR_KEY_TRANSMIT, 0);

   return 0;
}


static void _on_state_entry(dot1x_port_t *port, dot1x_fsm_auth_tx_key_t *fsm_auth_tx)
{
   switch (fsm_auth_tx->state)
   {
   case DOT1X_FSM_AUTH_KEY_TX_STATE_NO_KEY:
      break;

   case DOT1X_FSM_PORT_TIMERS_STATE_KEY_TX:
      _txKey(port, fsm_auth_tx);
      fsm_auth_tx->keyAvailable = False;
      break;
   }
}


static void _attempt_transition(dot1x_port_t *port, dot1x_fsm_auth_tx_key_t *fsm_auth_tx)
{
   dot1x_port_priv_t *priv_port;
   int               changed;

   priv_port = (dot1x_port_priv_t *)port;

   do
   {
      changed = 0;
      switch (fsm_auth_tx->state)
      {
      case DOT1X_FSM_AUTH_KEY_TX_STATE_NO_KEY:
         if (port->KeyTransmissionEnabled &&
             fsm_auth_tx->keyAvailable &&
             (priv_port->fsm_globals.portStatus == Authorized))
         {
            fsm_auth_tx->state = DOT1X_FSM_PORT_TIMERS_STATE_KEY_TX;
            _on_state_entry(port, fsm_auth_tx);
            changed = 1;
         }
         break;

      case DOT1X_FSM_PORT_TIMERS_STATE_KEY_TX:
         if (fsm_auth_tx->keyAvailable)
         {
            _on_state_entry(port, fsm_auth_tx);
         }
         else if (!port->KeyTransmissionEnabled || (priv_port->fsm_globals.portStatus == Unauthorized))
         {
            fsm_auth_tx->state = DOT1X_FSM_AUTH_KEY_TX_STATE_NO_KEY;
            _on_state_entry(port, fsm_auth_tx);
            changed = 1;
         }
         break;
      }
   } while (changed);
}


static int _txKey(dot1x_port_t *port, dot1x_fsm_auth_tx_key_t *fsm_auth_tx)
{
   dot1x_radius_backend_key_t rx_key;
   dot1x_radius_backend_key_t tx_key;

   dot1x_radius_backend_get_rx_key(port, &rx_key);
   dot1x_radius_backend_get_rx_key(port, &tx_key);

   return 0;
}


static int _tick(dot1x_port_t *port, int fsm_code, int other_code, struct dot1x_fsm *fsm)
{
   dot1x_fsm_auth_tx_key_t *fsm_auth_tx;
   unsigned int            lock_code;

   fsm_auth_tx = (dot1x_fsm_auth_tx_key_t *)fsm;

   dot1x_lock(fsm_auth_tx->lock, lock_code);

   _attempt_transition(port, fsm_auth_tx);

   dot1x_unlock(fsm_auth_tx->lock, lock_code);

   return 0;
}


static int _eapol_rx(dot1x_port_t *port, int fsm_code, int other_code, struct dot1x_fsm *fsm, dot1x_eapol_frame_t *frame)
{
   return 0;
}


static int _trigger(dot1x_port_t *port, int fsm_code, int other_code, struct dot1x_fsm *fsm, int trigger_code)
{
   dot1x_fsm_auth_tx_key_t *fsm_auth_tx;
   unsigned int            lock_code;

   fsm_auth_tx = (dot1x_fsm_auth_tx_key_t *)fsm;

   dot1x_lock(fsm_auth_tx->lock, lock_code);

   switch (trigger_code)
   {
   case DOT1X_FSM_AUTH_KEY_TX_TRIGGER_KEY_AVAILABLE:
      fsm_auth_tx->keyAvailable = True;
      _attempt_transition(port, fsm_auth_tx);
      break;
   }

   dot1x_unlock(fsm_auth_tx->lock, lock_code);

   return 0;
}


static int _initialize(dot1x_port_t *port, int fsm_code, int other_code, struct dot1x_fsm *fsm)
{
   dot1x_fsm_auth_tx_key_t *fsm_auth_tx;
   unsigned int            lock_code;

   fsm_auth_tx = (dot1x_fsm_auth_tx_key_t *)fsm;

   dot1x_lock(fsm_auth_tx->lock, lock_code);

   fsm_auth_tx->state = DOT1X_FSM_AUTH_KEY_TX_STATE_NO_KEY;
   _on_state_entry(port, fsm_auth_tx);
   _attempt_transition(port, fsm_auth_tx);

   dot1x_unlock(fsm_auth_tx->lock, lock_code);

   return 0;
}


static int _on_destroy(dot1x_port_t *port, struct dot1x_fsm *fsm)
{
   dot1x_fsm_auth_tx_key_t *fsm_auth_tx;

   fsm_auth_tx = (dot1x_fsm_auth_tx_key_t *)fsm;

   if (fsm_auth_tx != NULL)
   {
      dot1x_destroy_lock(fsm_auth_tx->lock);
      dot1x_heap_free(fsm_auth_tx);
   }

   return 0;
}
