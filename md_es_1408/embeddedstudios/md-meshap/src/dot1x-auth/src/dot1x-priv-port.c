/********************************************************************************
* MeshDynamics
* --------------
* File     : dot1x-priv-port.c
* Comments : 802.1x private port routines
* Created  : 1/25/2005
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  5  |6/16/2008 | Lock primitives changed                         | Sriram |
* -----------------------------------------------------------------------------
* |  4  |6/10/2008 | Port Data refreshed when not NULL               | Sriram |
* -----------------------------------------------------------------------------
* |  3  |7/13/2007 | FSM's reset when not NULL                       | Sriram |
* -----------------------------------------------------------------------------
* |  1  |4/7/2005  | Removed un-used FSMs                            | Sriram |
* -----------------------------------------------------------------------------
* |  0  |1/25/2005 | Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

#ifdef  __GNUC__
#define _DOT1X_PORT_HASH_FUNCTION(addr) \
   ({                                   \
      int hash;                         \
      hash = (addr)[0] * 1              \
             + (addr)[1] * 2            \
             + (addr)[2] * 3            \
             + (addr)[3] * 4            \
             + (addr)[4] * 5            \
             + (addr)[5] * 6;           \
      hash;                             \
   }                                    \
   )
#else
static int _DOT1X_PORT_HASH_FUNCTION(unsigned char *addr)
{
   int hash;               \
   hash = (addr)[0] * 1    \
          + (addr)[1] * 2  \
          + (addr)[2] * 3  \
          + (addr)[3] * 4  \
          + (addr)[4] * 5  \
          + (addr)[5] * 6; \
   return hash;            \
}
#endif

dot1x_port_priv_t *dot1x_priv_create_port(dot1x_priv_t  *instance,
                                          unsigned char *address,
                                          void          *port_data,
                                          int           AdminControlledDirections,
                                          int           AuthControlledPortControl,
                                          int           KeyTransmissionEnabled,
                                          int           quietPeriod,
                                          int           reAuthMax,
                                          int           txPeriod,
                                          int           suppTimeout,
                                          int           serverTimeout,
                                          int           maxReq,
                                          int           reAuthPeriod,
                                          int           reAuthEnabled)
{
   dot1x_port_priv_t *port;
   int               hash;
   int               bucket;
   unsigned int      lock_code;

   /**
    * First see if the port is already present
    */

   port = dot1x_priv_get_port(instance, address);

   if (port != NULL)
   {
      dot1x_port_timers_fsm_reset(&port->base);
      dot1x_auth_pae_fsm_reset(&port->base);
      dot1x_backend_auth_fsm_reset(&port->base);

      instance->backend->reset_port_context(&port->base);

      port->base.port_data = port_data;

      return port;
   }

   port = (dot1x_port_priv_t *)dot1x_heap_alloc(sizeof(dot1x_port_priv_t));

   memset(port, 0, sizeof(dot1x_port_priv_t));

   memcpy(port->base.address, address, DOT1X_PORT_ADDRESS_LEN);
   port->base.AdminControlledDirections = AdminControlledDirections;
   port->base.AuthControlledPortControl = AuthControlledPortControl;
   port->base.KeyTransmissionEnabled    = KeyTransmissionEnabled;
   port->base.dot1x_instance            = &instance->base;
   port->base.OperControlledDirections  = AdminControlledDirections;
   port->base.AuthControlledPortStatus  = Unauthorized;
   port->base.port_data = port_data;

   port->lock          = dot1x_create_lock();
   port->quietPeriod   = quietPeriod;
   port->reAuthMax     = reAuthMax;
   port->txPeriod      = txPeriod;
   port->suppTimeout   = suppTimeout;
   port->serverTimeout = serverTimeout;
   port->maxReq        = maxReq;
   port->reAuthPeriod  = reAuthPeriod;
   port->reAuthEnabled = reAuthEnabled;

   dot1x_lock(instance->lock, lock_code);

   /**
    * Add to instance port hash table
    */

   hash   = _DOT1X_PORT_HASH_FUNCTION(address);
   bucket = hash % instance->port_hash_length;

   port->next_hash = instance->port_hash[bucket];
   if (instance->port_hash[bucket] != NULL)
   {
      instance->port_hash[bucket]->prev_hash = port;
   }
   instance->port_hash[bucket] = port;

   /**
    * Add to port list
    */

   port->next_list = instance->port_list;
   if (instance->port_list != NULL)
   {
      instance->port_list->prev_list = port;
   }
   instance->port_list = port;

   dot1x_unlock(instance->lock, lock_code);

   /**
    * Register all standard FSMs for this port
    */

   dot1x_port_timers_fsm_create(&port->base);
   dot1x_auth_pae_fsm_create(&port->base);
   dot1x_backend_auth_fsm_create(&port->base);

   /**
    * Register the port with the backend also
    */

   instance->backend->create_port_context(&port->base);

   dot1x_print(DOT1X_PRINT_LEVEL_INFORMATION,
               "dot1x-auth: Created port %s:%02x-%02x-%02x-%02x-%02x-%02x\n",
               instance->base.instance_name,
               address[0],
               address[1],
               address[2],
               address[3],
               address[4],
               address[5]);

   return port;
}


void dot1x_priv_destroy_port(dot1x_priv_t  *instance,
                             unsigned char *address)
{
   dot1x_port_priv_t *port;
   int               hash;
   int               bucket;
   unsigned int      lock_code;


   dot1x_print(DOT1X_PRINT_LEVEL_INFORMATION,
               "dot1x-auth: Destroying port %s:%02x-%02x-%02x-%02x-%02x-%02x\n",
               instance->base.instance_name,
               address[0],
               address[1],
               address[2],
               address[3],
               address[4],
               address[5]);

   /**
    * Remove port from instance hash and list
    */

   hash   = _DOT1X_PORT_HASH_FUNCTION(address);
   bucket = hash % instance->port_hash_length;
   port   = NULL;

   dot1x_lock(instance->lock, lock_code);

   port = instance->port_hash[bucket];

   while (port != NULL)
   {
      if (!memcmp(port->base.address, address, 6))
      {
         break;
      }
      port = port->next_hash;
   }

   if (port == NULL)
   {
      dot1x_unlock(instance->lock, lock_code);
      return;
   }

   if (port->prev_hash != NULL)
   {
      port->prev_hash->next_hash = port->next_hash;
   }
   if (port->next_hash != NULL)
   {
      port->next_hash->prev_hash = port->prev_hash;
   }
   if (port == instance->port_hash[bucket])
   {
      instance->port_hash[bucket] = port->next_hash;
   }

   if (port->prev_list != NULL)
   {
      port->prev_list->next_list = port->next_list;
   }
   if (port->next_list != NULL)
   {
      port->next_list->prev_list = port->prev_list;
   }
   if (port == instance->port_list)
   {
      instance->port_list = port->next_list;
   }

   instance->backend->destroy_port_context(&port->base);

   dot1x_fsm_unregister_all(&port->base);

   dot1x_destroy_lock(port->lock);

   /**
    * Free up the memory
    */

   dot1x_heap_free(port);

   dot1x_unlock(instance->lock, lock_code);
}


dot1x_port_priv_t *dot1x_priv_get_port(dot1x_priv_t *instance, unsigned char *address)
{
   dot1x_port_priv_t *port;
   int               hash;
   int               bucket;
   unsigned int      lock_code;

   hash   = _DOT1X_PORT_HASH_FUNCTION(address);
   bucket = hash % instance->port_hash_length;
   port   = NULL;

   dot1x_lock(instance->lock, lock_code);

   port = instance->port_hash[bucket];

   while (port != NULL)
   {
      if (!memcmp(port->base.address, address, 6))
      {
         break;
      }
      port = port->next_hash;
   }

   dot1x_unlock(instance->lock, lock_code);

   return port;
}
