/********************************************************************************
* MeshDynamics
* --------------
* File     : dot1x-priv.h
* Comments : 802.1x authenticator private data
* Created  : 1/25/2005
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  0  |1/25/2005 | Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

#include "dot1x-fsm.h"

#ifndef __DOT1X_PRIV_H__
#define __DOT1X_PRIV_H__

#define dot1x_dec(x)             \
   do { if ((x) != 0) { --(x); } \
   } while (0)
#define dot1x_inc(x)                \
   do { if (++(x) > 255) { x = 0; } \
   } while (0)

struct dot1x_backend_priv
{
   char                      backend_name[DOT1X_BACKEND_NAME_MAX + 1];
   const dot1x_backend_t     *backend;
   struct dot1x_backend_priv *next;
};

typedef struct dot1x_backend_priv   dot1x_backend_priv_t;

struct dot1x_fsm_priv
{
   int                   code;
   dot1x_fsm_t           *fsm;
   struct dot1x_fsm_priv *next;
};

typedef struct dot1x_fsm_priv   dot1x_fsm_priv_t;

struct dot1x_port_priv
{
   dot1x_port_t           base;
   dot1x_fsm_globals_t    fsm_globals;
   unsigned int           lock;                                                                 /** Set upon creation */
   void                   *backend_context;                                                     /** Set upon creation */
   dot1x_fsm_t            *standard_fsms[DOT1X_FSM_STANDARD_COUNT];                             /** Added upon creation */
   dot1x_fsm_priv_t       *other_fsms;
   int                    quietPeriod, reAuthMax, txPeriod;
   int                    suppTimeout, serverTimeout, maxReq;
   int                    reAuthPeriod, reAuthEnabled;
#ifdef DOT1X_PORT_STATISTICS_ENABLED
   dot1x_port_stats_t     stats;
#endif
   struct dot1x_port_priv *prev_hash;
   struct dot1x_port_priv *next_hash;
   struct dot1x_port_priv *prev_list;
   struct dot1x_port_priv *next_list;
};

typedef struct dot1x_port_priv   dot1x_port_priv_t;

#ifdef DOT1X_PORT_STATISTICS_ENABLED
#define DOT1X_PORT_STATISTICS_SET(port, stat, val)    do { (port)->stats. ## stat = (val); } while (0)
#define DOT1X_PORT_STATISTICS_GET(port, stat)         (port)->stats. ## stat
#define DOT1X_PORT_STATISTICS_INC(port, stat)         do { ++(port)->stats. ## stat; } while (0)
#define DOT1X_PORT_STATISTICS_ADD(port, stat, val)    do { (port)->stats. ## stat += (val) } while (0)
#else
#define DOT1X_PORT_STATISTICS_SET(port, stat, val)
#define DOT1X_PORT_STATISTICS_GET(port, stat)         0
#define DOT1X_PORT_STATISTICS_INC(port, stat)
#define DOT1X_PORT_STATISTICS_ADD(port, stat, val)
#endif

struct dot1x_priv
{
   dot1x_t               base;
   const dot1x_backend_t *backend;
   void                  *backend_instance;                                     /** Set by create_instance of backend */
   unsigned int          lock;                                                  /** Set upon creation */
   dot1x_port_priv_t     **port_hash;                                           /** Initialized upon creation */
   int                   port_hash_length;
   dot1x_port_priv_t     *port_list;                                            /** Initialized upon creation */
   void                  *encryption_context;                                   /** Initialized upon creation */
};

typedef struct dot1x_priv   dot1x_priv_t;

void dot1x_priv_initialize(void);
void dot1x_priv_uninitialize(void);

int dot1x_priv_backend_register(const char *backend_name, const dot1x_backend_t *backend);
int dot1x_priv_backend_unregister(const char *backend_name);
const dot1x_backend_t *dot1x_priv_backend_get(const char *name);

dot1x_priv_t *dot1x_priv_create_instance(const char                 *instance_name,
                                         const char                 *backend_name,
                                         void                       *backend_param,
                                         void                       *instance_data,
                                         dot1x_port_status_change_t on_port_status_change,
                                         int                        port_hash_length);

void dot1x_priv_destroy_instance(dot1x_priv_t *instance);


dot1x_port_priv_t *dot1x_priv_create_port(dot1x_priv_t  *instance,
                                          unsigned char *address,
                                          void          *port_data,
                                          int           AdminControlledDirections,
                                          int           AuthControlledPortControl,
                                          int           KeyTransmissionEnabled,
                                          int           quietPeriod,
                                          int           reAuthMax,
                                          int           txPeriod,
                                          int           suppTimeout,
                                          int           serverTimeout,
                                          int           maxReq,
                                          int           reAuthPeriod,
                                          int           reAuthEnabled);


dot1x_port_priv_t *dot1x_priv_get_port(dot1x_priv_t  *instance,
                                       unsigned char *address);

void dot1x_priv_destroy_port(dot1x_priv_t  *instance,
                             unsigned char *address);

int dot1x_priv_tick(dot1x_priv_t *instance);
#endif /*__DOT1X-PRIV_H__*/
