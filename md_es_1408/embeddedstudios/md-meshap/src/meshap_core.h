/********************************************************************************
* MeshDynamics
* --------------
* File     : meshap_core.h
* Comments : Torna MeshAP Core Routines and definitions
* Created  : 5/25/2004
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* | 15  |02/12/2009| Added action hook                               |Abhijit |
* -----------------------------------------------------------------------------
* | 14  |8/25/2008 | Changes for SIP                                 | Sriram |
* -----------------------------------------------------------------------------
* | 13  |5/21/2008 | Reboot locking bug fixed                        | Sriram |
* -----------------------------------------------------------------------------
* | 12  |1/22/2008 | Changes for Mixed mode                          |Prachiti|
* -----------------------------------------------------------------------------
* | 11  |11/28/2007| Probe Request Hook added                        | Sriram |
* -----------------------------------------------------------------------------
* | 10  |7/24/2007 | REBOOT logging changes                          | Sriram |
* -----------------------------------------------------------------------------
* |  9  |11/14/2006| Added heap alloc/free count variables           | Sriram |
* -----------------------------------------------------------------------------
* |  8  |10/18/2006| Changes for ETSI Radar                          | Sriram |
* -----------------------------------------------------------------------------
* |  7  |03/13/2006| Changes for ACL	                              | Bindu  |
* -----------------------------------------------------------------------------
* |  6  |02/15/2006| changes for dot11e                              | Bindu  |
* -----------------------------------------------------------------------------
* |  5  |9/22/2005 | Interrupt disable/enable semantics changed      | Sriram |
* -----------------------------------------------------------------------------
* |  4  |9/15/2005 | meshap_core_get_sta_info Added                  | Sriram |
* -----------------------------------------------------------------------------
* |  3  |6/21/2005 | Misc changes                                    | Sriram |
* -----------------------------------------------------------------------------
* |  2  |11/15/2004| Added packet_direction member to core packet    | Sriram |
* -----------------------------------------------------------------------------
* |  1  |7/23/2004 | MESHAP_ATOMIC changes                           | Sriram |
* -----------------------------------------------------------------------------
* |  0  |5/25/2004 | Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

#ifndef __MESHAP_CORE_H__
#define __MESHAP_CORE_H__

#ifdef _MESHAP_USE_ATOMIC_
#define MESHAP_ATOMIC    atomic_t
#define MESHAP_ATOMIC_SET(variable, value)      atomic_set(&variable, value)
#define MESHAP_ATOMIC_GET(variable)             atomic_read(&variable)
#define MESHAP_ATOMIC_INC(variable)             atomic_inc(&variable)
#define MESHAP_ATOMIC_DEC_AND_TEST(variable)    atomic_dec_and_test(&variable)
#else
#define MESHAP_ATOMIC    int
#define MESHAP_ATOMIC_SET(variable, value)      do { variable = value; } while (0)
#define MESHAP_ATOMIC_GET(variable)             variable
#define MESHAP_ATOMIC_INC(variable)             do { ++variable; } while (0)
#define MESHAP_ATOMIC_DEC_AND_TEST(variable)    ((--variable) == 0)
#endif /* _MESHAP_USE_ATOMIC_ */

#define MESHAP_CORE_GLOBALS_STATE_STOPPED    0
#define MESHAP_CORE_GLOBALS_STATE_STARTED    1
/*upstack_pkt_queue is used to queue the data frames which need to send to upsatck */
extern struct sk_buff_head upstack_pkt_queue;
extern struct sk_buff_head temp_upstack_pkt_queue;
extern al_spinlock_t skb_upstack_que_splock;
extern int upstack_pkt_queue_init_done;
extern struct task_struct *meshap_upstack_pkt_process_task;
void meshap_process_upstack_pkt(void *data);

#ifdef _AL_HEAP_DEBUG

struct meshap_heap_debug_info
{
   void                          *memblock;
   const char                    *file_name;
   int                           line_number;
   unsigned int                  size;
   unsigned long                 timestamp;
   struct meshap_heap_debug_info *prev_hash;
   struct meshap_heap_debug_info *next_hash;
   struct meshap_heap_debug_info *prev_list;
   struct meshap_heap_debug_info *next_list;
};

typedef struct meshap_heap_debug_info   meshap_heap_debug_info_t;

#define MESHAP_HEAP_DEBUG_HASH_SIZE    17
#endif /* _AL_HEAP_DEBUG */

struct meshap_core_globals
{
   char                     log_buffer[1024];
   unsigned int             log_mask;
   int                      state;                                                              /* MESHAP_CORE_GLOBALS_STATE_* */
   char                     *config_string;
   int                      start_pending;
   al_on_receive_t          on_receive;
   al_on_before_transmit_t  on_before_transmit;
   al_on_phy_link_notify_t  on_phy_link_notify;
   char                     *dot11e_config_string;
   char                     *acl_config_string;
   char                     *mobility_conf_string;
   char                     *v_if_info_conf_string;
   MESHAP_ATOMIC            reboot_locked;
   MESHAP_ATOMIC            reboot_pending;
   unsigned char            reboot_code;
   char                     *sip_config_string;
#ifdef _AL_HEAP_DEBUG
   meshap_heap_debug_info_t *heap_debug_list;
   meshap_heap_debug_info_t *heap_debug_hash[MESHAP_HEAP_DEBUG_HASH_SIZE];
   unsigned int             max_block_size;                                     /* Max block size allocated */
   unsigned int             active_total;                                       /* Total active allocations */
   unsigned int             active_max;                                         /* Max total active allocations */
   unsigned int             total_alloc;
   unsigned int             total_freed;
   unsigned int             alloc_count;
   unsigned int             free_count;
   unsigned int             free_miss_count;
   meshap_heap_debug_info_t *last_freed_item;
#endif
};

typedef struct meshap_core_globals   meshap_core_globals_t;

extern meshap_core_globals_t meshap_core_globals;

extern al_spinlock_t core_gbl_splock;
AL_DECLARE_PER_CPU(int, core_gbl_spvar);

/**
 * Right now the expected number of interfaces is less than 10,
 * hence having a hashtable is really wasteful. We use a simple
 * linked list of used interfaces and one of all interfaces.
 * If the expected number of interfaces increases way beyond 10,
 * this implementation might change.
 *
 * The first member of this structure must always be al_net_if.
 * No fields must be added above the al_net_if field.
 */

struct meshap_core_net_if
{
   al_net_if_t                   al_net_if;                                                     /** the al_net_if used in MESH/AP code */
   char                          name[IFNAMSIZ];                                                /** Used for hashing */
   struct net_device             *dev;                                                          /** the corresponding linux net_device (dev==NULL means not initialized)*/
   meshap_net_dev_t              *dev_info;                                                     /** the extended operations for the net_device */
   u8                            used;                                                          /** whether this interface appears in the configuration file or not */
   u8                            created;                                                       /** whether this interface is physicall present or not */
   unsigned char                 phy_type;                                                      /** the al_conf phy_type (valid only if used==1) */
   unsigned char                 use_type;                                                      /** the al_conf use_type (valid only if used==1) */
   u32                           link_flags;

   al_process_management_frame_t process_management_frame;
   al_process_beacon_t           process_beacon;
   al_process_auth_t             process_auth;
   al_process_assoc_t            process_assoc;
   al_send_assoc_resp_t          send_assoc_response;
   al_del_station_t              del_station;
   al_send_deauth_t              send_deauth;
   al_send_auth_response_t       send_auth_response;
   al_round_robin_notify_t       process_round_robin;
   al_radar_notify_t             process_radar;
   al_on_probe_request_t         process_probe_request;
   al_action_notify_t            process_action;

   void                          *test_device_ptr;
   struct meshap_core_net_if     *prev_created;                                 /** prev node in used nodes list (valid only if created==1) */
   struct meshap_core_net_if     *next_created;                                 /** next node in used nodes list (valid only if created==1) */
   struct meshap_core_net_if     *next;                                         /** next node in all nodes list */
   struct meshap_core_net_if     *prev;                                         /** prev node in all nodes list */

   int                           dev_type;                                      /** device can be virtual or physical  **/

   /* to be used by physical device */

   struct meshap_core_net_if     *virtual_ds_if;                                /** virtual ds **/
   struct meshap_core_net_if     *virtual_wm_if;                                /** virtual wm **/

   /* to be used by virtual device */

   unsigned char                 virtual_dev_mode;
   struct meshap_core_net_if     *physical_if;
   u8                            vlan_if; /* SPAWAR */
};

typedef struct meshap_core_net_if   meshap_core_net_if_t;

extern al_spinlock_t core_net_splock;
AL_DECLARE_PER_CPU(int, core_net_spvar);


/**
 * Depending upon this mode the al_net_if functions will operate
 */

#define MESHAP_CORE_DEV_TYPE_PHYSICAL    1
#define MESHAP_CORE_DEV_TYPE_VIRTUAL     2


/**
 * Every meshap_core_packet is always associated with an SK-BUFF
 * The al_packet and the skb share the packet buffer.
 */

#define MESHAP_CORE_PACKET_TYPE_HEAP              1
#define MESHAP_CORE_PACKET_TYPE_POOL              2

#define MESHAP_CORE_PACKET_DIRECTION_NONE         0
#define MESHAP_CORE_PACKET_DIRECTION_OUTGOING     1
#define MESHAP_CORE_PACKET_DIRECTION_INCOMMING    2
#define MESHAP_CORE_PACKET_DIRECTION_NEW          3

struct meshap_core_packet
{
   al_packet_t               al_packet;
   struct sk_buff            *skb;
   meshap_core_net_if_t      *incoming_if;
   MESHAP_ATOMIC             ref_count;
   int                       type;                                                      /** MESHAP_CORE_PACKET_TYPE_* */
   int                       packet_direction;                                          /** MESHAP_CORE_PACKET_DIRECTION_* */
   struct meshap_core_packet *next_pool;
};

typedef struct meshap_core_packet   meshap_core_packet_t;

extern al_spinlock_t core_pkt_splock;
AL_DECLARE_PER_CPU(int, core_pkt_spvar);
extern al_spinlock_t ap_thread_core_pkt_splock;
AL_DECLARE_PER_CPU(int, ap_thread_core_pkt_spvar);
extern al_spinlock_t imcp_thread_core_pkt_splock;
AL_DECLARE_PER_CPU(int, imcp_thread_core_pkt_spvar);

#define _disable_interrupts(flags)    do { preempt_disable(); } while (0)
#define _enable_interrupts(flags)     do { preempt_enable(); } while (0)

int meshap_core_initialize(void);
int meshap_core_uninitialize(void);
void meshap_core_process_mgmt_frame(struct net_device *dev, void *dev_token, struct sk_buff *skb);
void meshap_core_process_data_frame(struct net_device *dev, void *dev_token, struct sk_buff *skb);
void meshap_core_on_link_notify(struct net_device *dev, void *dev_token, u32 state_flags);
void meshap_core_on_net_device_create(struct net_device *dev, meshap_net_dev_t *dev_info);
void meshap_core_on_net_device_destroy(struct net_device *dev, void *dev_token);

void meshap_core_remove_net_ifs(void);
meshap_core_net_if_t *meshap_core_lookup_net_if(const char *name);
meshap_core_net_if_t *meshap_core_create_net_if_from_dev(struct net_device *dev, meshap_net_dev_t *dev_info);
meshap_core_net_if_t *meshap_core_create_used_net_if(const char *name, unsigned char phy_type, unsigned char use_type);
void meshap_core_use_net_if(meshap_core_net_if_t *net_if, unsigned char phy_type, unsigned char use_type);
int meshap_core_get_created_net_if_count(void);
meshap_core_net_if_t *meshap_core_get_created_net_if(int index);

int meshap_core_start_mesh(void);
int meshap_core_stop_mesh(void);
int meshap_core_get_sta_info(unsigned char *mac_addr, meshap_sta_info_t *sta_info);

void meshap_write_reboot_header(void);
void meshap_core_reboot_machine(unsigned char reboot_code);
int meshap_core_create_virtual_net_if(const char *virtual_if_name, const char *phy_if_name);
void meshap_core_exec_reboot(unsigned char reboot_code);

#endif /*__MESHAP_CORE_H__*/
