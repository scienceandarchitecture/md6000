/********************************************************************************
* MeshDynamics
* --------------
* File     : meshap_ip_device.c
* Comments : Meshap Internet Protocol Virtual Interface
* Created  : 11/15/2004
* Author   : Abhijit Ayarekar
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  5  |8/20/2007 | meshap_ip_device_get_info Added                 | Sriram |
* -----------------------------------------------------------------------------
* |  4  |8/14/2007 | Changes for robust-ness                         | Sriram |
* -----------------------------------------------------------------------------
* |  3  |10/5/2005 | Misc changes                                    | Sriram |
* -----------------------------------------------------------------------------
* |  2  |4/11/2005 | Misc changes                                    | Sriram |
* -----------------------------------------------------------------------------
* |  1  |11/15/2004| Added more net_device routines and cleaned up   | Sriram |
* -----------------------------------------------------------------------------
* |  0  |11/15/2004| Created.                                        | Abhijit|
* -----------------------------------------------------------------------------
********************************************************************************/

#include <generated/autoconf.h>
#include <linux/module.h>
#include <linux/init.h>
#include <linux/version.h>
#include <linux/fs.h>
#include <linux/netdevice.h>
#include <linux/etherdevice.h>
#include <linux/rtnetlink.h>
#include <linux/skbuff.h>
#include <linux/wireless.h>
#include <asm/uaccess.h>

#include "torna_types.h"
#include "torna_ddi.h"
#include "meshap.h"
#include "meshap_logger.h"
#include "al.h"
#include "al_802_11.h"
#include "meshap_core.h"
#include "meshap_tddi.h"
#include "al_conf.h"
#include "al_packet_impl.h"
#include "meshap_ip_device.h"
#include "access_point.h"

AL_DECLARE_GLOBAL_EXTERN(int mesh_state);
AL_DECLARE_GLOBAL_EXTERN(access_point_globals_t globals);
#define _MESH_STATE_RUNNING                        3

/**
 *	Private data structure to be stored in dev->priv
 */

struct mip_priv_data
{
   struct net_device_stats   stats;
   struct meshap_core_net_if *net_if_ds;
   unsigned int              skb_alloc_count;
   unsigned int              skb_free_count;
};

typedef struct mip_priv_data   mip_priv_data_t;

static struct net_device *_dev_instance = NULL;

static int _mip_open(struct net_device *dev);
static int _mip_stop(struct net_device *dev);
static int _mip_xmit(struct sk_buff *skb, struct net_device *dev);
static void _mip_tx_timeout(struct net_device *dev);
static void _mip_set_multicast_list(struct net_device *dev);
static struct net_device_stats *_mip_net_device_get_stats(struct net_device *dev);
static int _mip_net_device_hard_header(struct sk_buff *skb, struct net_device *dev, unsigned short type, const void *daddr, const void *saddr, unsigned len);

#ifdef _MIP_TX_ALLOC_SEPERATE

static void _mip_skb_destructor(struct sk_buff *sb);
#endif

static const struct header_ops _header_ops =
{
   .create = _mip_net_device_hard_header,
};

static const struct net_device_ops _netdev_ops =
{
   .ndo_open        = _mip_open,
   .ndo_stop        = _mip_stop,
   .ndo_start_xmit  = _mip_xmit,
   .ndo_tx_timeout  = _mip_tx_timeout,
   .ndo_set_rx_mode = _mip_set_multicast_list,
   .ndo_get_stats   = _mip_net_device_get_stats,
};

int meshap_ip_device_set_net_if(meshap_core_net_if_t *core_net_if)
{
   mip_priv_data_t *pdata;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);

   if (_dev_instance == NULL)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"_dev_instance is NULL ::  func : %s LINE : %d\n", __func__,__LINE__);
      return -1;
   }

   pdata            = (mip_priv_data_t *)netdev_priv(_dev_instance);
   pdata->net_if_ds = core_net_if;

   memcpy(_dev_instance->dev_addr, core_net_if->al_net_if.config.hw_addr.bytes, ETH_ALEN);

   printk(KERN_INFO "mip: Setting MAC addr to "MACSTR, MAC2STR(_dev_instance->dev_addr));
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);

   return 0;
}


int meshap_ip_device_get_info(al_packet_pool_info_t *info)
{
   mip_priv_data_t *pdata;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
   if (_dev_instance == NULL)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"_dev_instance is NULL ::  func : %s LINE : %d\n", __func__,__LINE__);
      return -1;
   }

   pdata = (mip_priv_data_t *)netdev_priv(_dev_instance);

   info->mip_skb_alloc = pdata->skb_alloc_count;
   info->mip_skb_free  = pdata->skb_free_count;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
   return 0;
}


int meshap_ip_device_initialize()
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
   int             result;
   mip_priv_data_t *data;

   if (_dev_instance != NULL)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"_dev_instance is NULL ::  func : %s LINE : %d\n", __func__,__LINE__);
      return 0;
   }

   _dev_instance = alloc_etherdev(sizeof(mip_priv_data_t));
   data          = (mip_priv_data_t *)netdev_priv(_dev_instance);

   memset(data, 0, sizeof(mip_priv_data_t));

   ether_setup(_dev_instance);

   _dev_instance->netdev_ops      = &_netdev_ops;
   _dev_instance->header_ops      = &_header_ops;
   _dev_instance->watchdog_timeo  = 5 * HZ;
   _dev_instance->hard_header_len = ETH_HLEN;

   _dev_instance->tx_queue_len = 32;

   strcpy(_dev_instance->name, MESHAP_IP_DEVICE_NAME_TEMPLATE);

   result = register_netdev(_dev_instance);

   if (result > 0)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"result is greater than 0::  func : %s LINE : %d\n", __func__,__LINE__);
      printk(KERN_INFO "meshap_ip_device_initialize: Error %d  initializing meshap interface", result);
      return result;
   }
   else
   {
      printk(KERN_INFO "meshap_ip_device_initialize: Net device registered\n");
   }

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
   return 0;
}


int meshap_ip_device_uninitialize()
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
   printk(KERN_INFO "meshap_ip_device_uninitialize : Unregistering Net device \n");
   unregister_netdev(_dev_instance);
   kfree(_dev_instance);
   _dev_instance = NULL;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
   return 0;
}


static int _mip_open(struct net_device *dev)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
   if (dev != _dev_instance)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"_dev_instance & dev is not equal ::  func : %s LINE : %d\n", __func__,__LINE__);
      return -1;
   }

   if (dev->flags & IFF_RUNNING)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"dev flag is in running state ::  func : %s LINE : %d\n", __func__,__LINE__);
      return 0;
   }

   dev->flags      |= IFF_RUNNING;
   dev->trans_start = jiffies;

   netif_start_queue(dev);

   printk(KERN_INFO "mip: Net device opened\n");
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);

   return 0;
}


static int _mip_stop(struct net_device *dev)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
   if (dev != _dev_instance)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"_dev_instance & dev is not equal ::  func : %s LINE : %d\n", __func__,__LINE__);
      return -1;
   }

   if (!(dev->flags & IFF_RUNNING))
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"dev flag is in not running state ::  func : %s LINE : %d\n", __func__,__LINE__);
      return 0;
   }

   netif_stop_queue(dev);
   dev->flags &= ~IFF_RUNNING;

   printk(KERN_INFO "mip: Net device stopped\n");

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
   return 0;
}


static int _mip_xmit(struct sk_buff *skb, struct net_device *dev)
{
   meshap_core_packet_t *packet;
   mip_priv_data_t      *pdata;
   unsigned long        flags;

#ifdef _MIP_TX_ALLOC_SEPERATE
   struct sk_buff *skb_2;
#endif
   MESH_USAGE_PROFILING_INIT
   MESH_USAGE_PROFILE_START(start)

   if (dev != _dev_instance)
   {
       dev_kfree_skb_any(skb);
	  (tx_rx_pkt_stats.packet_drop[54])++;
      return -1;
   }

   if (meshap_core_globals.on_before_transmit == NULL)
   {
      dev_kfree_skb_any(skb);
	  (tx_rx_pkt_stats.packet_drop[55])++;
      return 0;
   }

   if ((meshap_core_globals.state != MESHAP_CORE_GLOBALS_STATE_STARTED) ||
        reboot_in_progress ||
       (mesh_state != _MESH_STATE_RUNNING) || (AL_ATOMIC_GET(globals.ap_thread_disabled) == 1))
   {
      dev_kfree_skb_any(skb);
	  (tx_rx_pkt_stats.packet_drop[56])++;
      return 0;
   }

   pdata = (mip_priv_data_t *)netdev_priv(dev);

   ++pdata->stats.tx_packets;
   pdata->stats.tx_bytes += skb->len;

#ifdef _MIP_TX_ALLOC_SEPERATE
   skb_2 = dev_alloc_skb(2400);

   if (skb_2 != NULL)
   {
      unsigned char *p;

      ++pdata->skb_alloc_count;

      skb_2->destructor = _mip_skb_destructor;

      p = skb_put(skb_2, skb->len);

      memcpy(p, skb->data, skb->len);

      skb_2->mac.raw = (u8 *)skb_2->data;

	  meshap_core_process_data_frame(dev, NULL, skb_2);
   }
	else
	{
	  (tx_rx_pkt_stats.packet_drop[76])++;
	}

   dev_kfree_skb_any(skb);

#else
	  meshap_core_process_data_frame(dev, NULL, skb);
#endif
      dev->trans_start = jiffies;
      MESH_USAGE_PROFILE_END(profiling_info.process_mip_xmit_func_tprof,index,start,end)

   return 0;
}


static void _mip_tx_timeout(struct net_device *dev)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
   if (dev != _dev_instance)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"_dev_instance & dev is not equal ::  func : %s LINE : %d\n", __func__,__LINE__);
      return;
   }

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
   netif_wake_queue(dev);
}


static void _mip_set_multicast_list(struct net_device *dev)
{
}


static struct net_device_stats *_mip_net_device_get_stats(struct net_device *dev)
{
   mip_priv_data_t *pdata;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
   if (dev != _dev_instance)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"_dev_instance is NULL ::  func : %s LINE : %d\n", __func__,__LINE__);
      return NULL;
   }

   pdata = (mip_priv_data_t *)netdev_priv(dev);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
   return &(pdata->stats);
}


static int _mip_net_device_hard_header(struct sk_buff *skb, struct net_device *dev, unsigned short type, const void *daddr, const void *saddr, unsigned len)
{
   struct ethhdr *eth;

   eth = (struct ethhdr *)skb_push(skb, ETH_HLEN);

   eth->h_proto = htons(type);
   memcpy(eth->h_source, saddr != NULL ? saddr : dev->dev_addr, ETH_ALEN);
   memcpy(eth->h_dest, daddr != NULL ? daddr : dev->dev_addr, ETH_ALEN);

   skb->mac_header = (u8 *)eth;

   return ETH_HLEN;
}


#ifdef _MIP_TX_ALLOC_SEPERATE

static void _mip_skb_destructor(struct sk_buff *sb)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
   mip_priv_data_t *pdata;

   if (_dev_instance == NULL)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"_dev_instance is NULL ::  func : %s LINE : %d\n", __func__,__LINE__);
      return;
   }

   pdata = (mip_priv_data_t *)netdev_priv(_dev_instance);

   ++pdata->skb_free_count;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
}
#endif
