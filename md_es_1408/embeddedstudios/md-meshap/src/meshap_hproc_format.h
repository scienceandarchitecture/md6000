/********************************************************************************
* MeshDynamics
* --------------
* File     : meshap_hproc_format.h
* Comments : Macros for HTML Proc Entries
* Created  : 5/4/2006
* Author   : Bindu Parakala
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  1  |5/30/2006 | Macro definitions corrected                     | Sriram |
* -----------------------------------------------------------------------------
* |  0  | 5/4/2006 | Created                                         | Bindu  |
* -----------------------------------------------------------------------------
********************************************************************************/

/*#define TABLE_START(p)									do { p	+= sprintf(p, "%s", "<table align=\"center\" border=\"1\">"); }while(0)
 #define TABLE_END(p)									do { p	+= sprintf(p, "%s", "</table>"); }while(0)
 *
 #define TABLE_ROW_HEAD_START(p)							do { p	+= sprintf(p, "%s", "<tr>"); }while(0)
 #define TABLE_ROW_HEAD_DESC_START(p)					do { p	+= sprintf(p, "%s", "<td><p align=\"center\"><font size=\"2\"><b>");}while(0)
 #define TABLE_ROW_HEAD_VALUE(p,s)						do { p	+= sprintf(p,"%s",s);}while(0)
 #define TABLE_ROW_HEAD_DESC_END(p)						do { p	+= sprintf(p, "%s", "</b></font></td>");}while(0)
 #define TABLE_ROW_HEAD_END(p)							do { p	+= sprintf(p, "%s", "</tr>");}while(0)
 *
 #define TABLE_ROW_DATA_START(p)							do { p	+= sprintf(p, "%s", "<tr>");}while(0)
 #define TABLE_ROW_DATA_DESC_START(p)					do { p	+= sprintf(p, "%s", "<td><p><font size=\"2\">");}while(0)
 #define TABLE_ROW_DATA_TYPE_INT(p, v)					do { p	+= sprintf(p,"%d",v);}while(0)
 #define TABLE_ROW_DATA_TYPE_STR(p, v)					do { p	+= sprintf(p,"%s",v);}while(0)
 #define TABLE_ROW_DATA_TYPE_L_INT(p, v)					do { p	+= sprintf(p,"%ld",v);}while(0)
 #define TABLE_ROW_DATA_TYPE_LL_INT(p, v)				do { p	+= sprintf(p,"%lld",v);}while(0)
 #define TABLE_ROW_DATA_TYPE_MAC(p,m1,m2,m3,m4,m5,m6)	do { p	+= sprintf(p,"%02x:%02x:%02x:%02x:%02x:%02x",m1,m2,m3,m4,m5,m6);}while(0)
 #define TABLE_ROW_DATA_DESC_END(p)						do { p	+= sprintf(p,"</font></td>");}while(0)
 #define TABLE_ROW_DATA_END(p)							do { p	+= sprintf(p, "%s", "</tr>");}while(0)
 *
 #define TABLE_ROW_HEAD(p, s)							do { TABLE_ROW_HEAD_DESC_START(p); TABLE_ROW_HEAD_VALUE(p,s); TABLE_ROW_HEAD_DESC_END(p); }while(0)
 #define TABLE_ROW_DATA_INT(p, v)						do { TABLE_ROW_DATA_DESC_START(p); TABLE_ROW_DATA_TYPE_INT(p,v);	 TABLE_ROW_DATA_DESC_END(p);}while(0)
 #define TABLE_ROW_DATA_STR(p, v)						do { TABLE_ROW_DATA_DESC_START(p); TABLE_ROW_DATA_TYPE_STR(p,v);	 TABLE_ROW_DATA_DESC_END(p);}while(0)
 #define TABLE_ROW_DATA_L_INT(p, v)						do { TABLE_ROW_DATA_DESC_START(p); TABLE_ROW_DATA_TYPE_L_INT(p,v);  TABLE_ROW_DATA_DESC_END(p);}while(0)
 #define TABLE_ROW_DATA_LL_INT(p, v)						do { TABLE_ROW_DATA_DESC_START(p); TABLE_ROW_DATA_TYPE_LL_INT(p,v); TABLE_ROW_DATA_DESC_END(p);}while(0)
 #define TABLE_ROW_DATA_MAC(p, v)						do { TABLE_ROW_DATA_DESC_START(p); TABLE_ROW_DATA_TYPE_MAC(p,v);	 TABLE_ROW_DATA_DESC_END(p);}while(0)
 *
 #define LINE_START(p)									do { p	+= sprintf(p, "%s", "<p><font size=\"2\">");}while(0)
 #define LINE_DATA_TEXT(p, s)							do { p	+= sprintf(p, "%s = ", s);}while(0)
 #define LINE_DATA_INT(p, v)								do { p	+= sprintf(p, "%d", v);}while(0)
 #define LINE_END(p)										do { p	+= sprintf(p, "%s", "</font></p>");}while(0)
 *
 #define LINE_INT(p, s, v)								do { LINE_START(p); LINE_DATA_TEXT(p, s);	LINE_DATA_INT(p, v);	 LINE_END(p); }while(0)
 #define LINE_INT_INT(p, s1, v1, s2, v2)					do { LINE_START(p); LINE_DATA_TEXT(p, s1); LINE_DATA_INT(p, v1); LINE_DATA_TEXT(p, s2); LINE_DATA_INT(p, v2); LINE_END(p); }while(0) */



#define TABLE_START(p)                                        do { seq_printf(p, "%s", "<table align=\"center\" border=\"1\">"); } while (0)
#define TABLE_END(p)                                          do { seq_printf(p, "%s", "</table>"); } while (0)

#define TABLE_ROW_HEAD_START(p)                               do { seq_printf(p, "%s", "<tr>"); } while (0)
#define TABLE_ROW_HEAD_DESC_START(p)                          do { seq_printf(p, "%s", "<td><p align=\"center\"><font size=\"2\"><b>"); } while (0)
#define TABLE_ROW_HEAD_VALUE(p, s)                            do { seq_printf(p, "%s", s); } while (0)
#define TABLE_ROW_HEAD_DESC_END(p)                            do { seq_printf(p, "%s", "</b></font></td>"); } while (0)
#define TABLE_ROW_HEAD_END(p)                                 do { seq_printf(p, "%s", "</tr>"); } while (0)

#define TABLE_ROW_DATA_START(p)                               do { seq_printf(p, "%s", "<tr>"); } while (0)
#define TABLE_ROW_DATA_DESC_START(p)                          do { seq_printf(p, "%s", "<td><p><font size=\"2\">"); } while (0)
#define TABLE_ROW_DATA_TYPE_INT(p, v)                         do { seq_printf(p, "%d", v); } while (0)
#define TABLE_ROW_DATA_TYPE_STR(p, v)                         do { seq_printf(p, "%s", v); } while (0)
#define TABLE_ROW_DATA_TYPE_L_INT(p, v)                       do { seq_printf(p, "%ld", v); } while (0)
#define TABLE_ROW_DATA_TYPE_LL_INT(p, v)                      do { seq_printf(p, "%lld", v); } while (0)
#define TABLE_ROW_DATA_TYPE_MAC(p, m1, m2, m3, m4, m5, m6)    do { seq_printf(p, "%02x:%02x:%02x:%02x:%02x:%02x", m1, m2, m3, m4, m5, m6); } while (0)
#define TABLE_ROW_DATA_DESC_END(p)                            do { seq_printf(p, "</font></td>"); } while (0)
#define TABLE_ROW_DATA_END(p)                                 do { seq_printf(p, "%s", "</tr>"); } while (0)

#define TABLE_ROW_HEAD(p, s)                                  do { TABLE_ROW_HEAD_DESC_START(p); TABLE_ROW_HEAD_VALUE(p, s); TABLE_ROW_HEAD_DESC_END(p); } while (0)
#define TABLE_ROW_DATA_INT(p, v)                              do { TABLE_ROW_DATA_DESC_START(p); TABLE_ROW_DATA_TYPE_INT(p, v);         TABLE_ROW_DATA_DESC_END(p); } while (0)
#define TABLE_ROW_DATA_STR(p, v)                              do { TABLE_ROW_DATA_DESC_START(p); TABLE_ROW_DATA_TYPE_STR(p, v);         TABLE_ROW_DATA_DESC_END(p); } while (0)
#define TABLE_ROW_DATA_L_INT(p, v)                            do { TABLE_ROW_DATA_DESC_START(p); TABLE_ROW_DATA_TYPE_L_INT(p, v);  TABLE_ROW_DATA_DESC_END(p); } while (0)
#define TABLE_ROW_DATA_LL_INT(p, v)                           do { TABLE_ROW_DATA_DESC_START(p); TABLE_ROW_DATA_TYPE_LL_INT(p, v); TABLE_ROW_DATA_DESC_END(p); } while (0)
#define TABLE_ROW_DATA_MAC(p, v)                              do { TABLE_ROW_DATA_DESC_START(p); TABLE_ROW_DATA_TYPE_MAC(p, v);         TABLE_ROW_DATA_DESC_END(p); } while (0)

#define LINE_START(p)                                         do { seq_printf(p, "%s", "<p><font size=\"2\">"); } while (0)
#define LINE_DATA_TEXT(p, s)                                  do { seq_printf(p, "%s = ", s); } while (0)
#define LINE_DATA_INT(p, v)                                   do { seq_printf(p, "%d", v); } while (0)
#define LINE_END(p)                                           do { seq_printf(p, "%s", "</font></p>"); } while (0)

#define LINE_INT(p, s, v)                                     do { LINE_START(p); LINE_DATA_TEXT(p, s);   LINE_DATA_INT(p, v);     LINE_END(p); } while (0)
#define LINE_INT_INT(p, s1, v1, s2, v2)                       do { LINE_START(p); LINE_DATA_TEXT(p, s1); LINE_DATA_INT(p, v1); LINE_DATA_TEXT(p, s2); LINE_DATA_INT(p, v2);   LINE_END(p); } while (0)
