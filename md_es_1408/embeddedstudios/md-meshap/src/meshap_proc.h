/********************************************************************************
* MeshDynamics
* --------------
* File     : meshap_proc.h
* Comments : Meshap /proc FS manager
* Created  : 12/26/2004
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  1  |6/18/2005 | Added meshap_proc_mkdir                         | Sriram |
* -----------------------------------------------------------------------------
* |  0  |12/26/2004| Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

#include <linux/proc_fs.h>

#ifndef __MESHAP_PROC_H__
#define __MESHAP_PROC_H__

typedef int (*meshap_proc_read_routine_t)(char *page, char **start, off_t off, int count, int *eof, void *data);


int meshap_proc_initialize(void);

//void					meshap_proc_add_read_entry	(const char*  name,meshap_proc_read_routine_t routine, void* data);
void meshap_proc_add_read_entry(const char *name, struct file_operations *proc_file_fops, void *data);
struct proc_dir_entry *meshap_proc_mkdir(const char *name);
#endif /*__MESHAP_PROC_H__*/
