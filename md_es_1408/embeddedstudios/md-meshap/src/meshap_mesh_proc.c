/********************************************************************************
* MeshDynamics
* --------------
* File     : meshap_mesh_proc.c
* Comments : Meshap MESH proc interface
* Created  : 6/18/2005
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  7  |6/17/2009 | Added ad-hoc mode entry                         | Sriram |
* -----------------------------------------------------------------------------
* |  6  |6/14/2007 | KAP listing changed for mobile mode             | Sriram |
* -----------------------------------------------------------------------------
* |  5  |6/13/2007 | Added mobility window                           | Sriram |
* -----------------------------------------------------------------------------
* |  4  |5/30/2006 | Macro usage corrected                           | Sriram |
* -----------------------------------------------------------------------------
* |  3  |02/01/2006| mesh table formatting changed					  | Abhijit|
* -----------------------------------------------------------------------------
* |  2  |10/5/2005 | More info displayed                             | Sriram |
* -----------------------------------------------------------------------------
* |  1  |10/4/2005 | Misc changes                                    | Sriram |
* -----------------------------------------------------------------------------
* |  0  |6/18/2005 | Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

#include <generated/autoconf.h>
#include <linux/version.h>
#include <linux/module.h>
#include <linux/init.h>
#include <linux/if.h>
#include <linux/netdevice.h>
#include <linux/skbuff.h>
#include <linux/cache.h>
#include <linux/pci.h>
#include <linux/wireless.h>
#include <asm/uaccess.h>
#include <net/iw_handler.h>
#include <linux/delay.h>
#include <linux/seq_file.h>
#include <linux/slab.h>
#include <linux/ieee80211.h>


#include "al.h"
#include "al_802_11.h"
#include "al_impl_context.h"
#include "al_net_addr.h"
#include "mesh_ext.h"
#include "meshap_hproc_format.h"
#include "meshap_mesh_proc.h"
#include "al_print_log.h"
#include "access_point.h"
#include "al_impl_context.h"
#include "al_print_log.h"
#include "al_conf.h"
#include "sip_conf.h"
#include "mesh_defs.h"
#include "imcp_snip.h"
#include "al_impl_context.h"
#include "mesh_table.h"
#include "mesh.h"
#include "mesh_globals.h"
#include "mesh_imcp.h"
#include "mesh_ng.h"
#include "meshap.h"
#include "meshap_core.h"

#define PROCFS_MAX_SIZE    16

#if MESH_CPU_PROFILE_ENABLED
#define THREAD_STAT_ALLIGN    40
#endif
extern meshap_core_net_if_t *_ifs_list;
extern mac80211_tx_rx_pkt_stats_t *mac_tx_rx_stats;

static struct proc_dir_entry *_meshap_mesh_proc_entry            = NULL;

static void _meshap_mesh_proc_kap_list_initialize(void);

static void _meshap_mesh_proc_table_list_initialize(void);

static void _meshap_mesh_proc_sample_list_initialize(void);

static void _enum_kap_routine(void *callback, const mesh_ext_known_access_point_info_t *kap);
static void _enum_table_routine(void *callback, const mesh_ext_table_entry_t *entry);
static void _enum_mobwin_routine(void *callback, const mesh_ext_mobilty_window_t *item);

static void _henum_table_routine(void *callback, const mesh_ext_table_entry_t *entry);

static void _henum_kap_routine(void *callback, const mesh_ext_known_access_point_info_t *kap);

static unsigned long debug_info_buf_size = 0;

struct _mesh_proc_data
{
   char            *p;
   int             flag;
   struct seq_file *m;
};

typedef struct _mesh_proc_data   _mesh_proc_data_t;

int meshap_mesh_proc_initialize(void)
{
   _meshap_mesh_proc_entry = meshap_proc_mkdir("mesh");
   _meshap_mesh_proc_kap_list_initialize();
   _meshap_mesh_proc_table_list_initialize();
   _meshap_mesh_proc_sample_list_initialize();
   return 0;
}


void meshap_mesh_proc_add_read_entry(const char *name, const struct file_operations *proc_file_fops, void *data)
{
   proc_create_data(name, 0, _meshap_mesh_proc_entry, proc_file_fops, data);
}


static int _sample_list_routine(struct seq_file *m, void *v)
{
   _mesh_proc_data_t proc_data;

   seq_printf(m,
              "--------------------------------------------------------------------------------\r\n"
              "PARENT ADDR      |CHN|SIG|RATE|TRATE|NEXTSAMPLE|DISC   |FLAGS    |SAMCNT|MAXSAM|\r\n"
              "--------------------------------------------------------------------------------\r\n");

   proc_data.m = (void *)m;
   mesh_ext_enumerate_sampling_list((void *)&proc_data, _enum_kap_routine);

   return 0;
}


static int _sample_list_proc_open(struct inode *inode, struct file *file)
{
   return single_open(file, _sample_list_routine, NULL);
}


static const struct file_operations _sample_list_fops =
{
   .owner   = THIS_MODULE,
   .open    = _sample_list_proc_open,
   .read    = seq_read,
   .llseek  = seq_lseek,
   .release = single_release,
};

static void _meshap_mesh_proc_sample_list_initialize(void)
{
   meshap_mesh_proc_add_read_entry("sample_list", &_sample_list_fops, NULL);
}


static int _kap_list_routine(struct seq_file *m, void *v)
{
   _mesh_proc_data_t proc_data;

   proc_data.flag = mesh_ext_is_mobile();

   if (proc_data.flag)
   {
      seq_printf(m,
                 "--------------------------------------------------------\r\n"
                 "PARENT ADDR      |CHN|SIG|RATE|TRATE|LAST SEEN |PATCNT |\r\n"
                 "--------------------------------------------------------\r\n");
   }
   else
   {
      seq_printf(m,
                 "--------------------------------------------------------------------------------\r\n"
                 "PARENT ADDR      |CHN|SIG|RATE|TRATE|NEXTSAMPLE|DISC   |FLAGS    |SAMCNT|MAXSAM|\r\n"
                 "--------------------------------------------------------------------------------\r\n");
   }
   proc_data.m = (void *)m;

   mesh_ext_enumerate_known_aps((void *)&proc_data, _enum_kap_routine);

   return 0;
}


static void _enum_kap_routine(void *callback, const mesh_ext_known_access_point_info_t *kap)
{
   _mesh_proc_data_t *proc_data;
   al_u64_t          tick;
   int               diff;

   proc_data = (_mesh_proc_data_t *)callback;

   tick = al_get_tick_count(AL_CONTEXT);

   if (!proc_data->flag)
   {
      diff = (int)(kap->next_sample_time - tick);
   }
   else
   {
      diff = (int)(tick - kap->last_seen_time);
   }

   if (diff < 0)
   {
      diff = 0;
   }

   if (proc_data->flag)
   {
      seq_printf(proc_data->m,
                 AL_NET_ADDR_STR "|%03d|%03d|%04d|%05d|%010d|%07d|\r\n",
                 AL_NET_ADDR_TO_STR(&kap->bssid),
                 kap->channel,
                 kap->signal,
                 kap->direct_bit_rate,
                 kap->tree_bit_rate,
                 diff,
                 kap->pattern_count);
   }
   else
   {
      seq_printf(proc_data->m,
                 AL_NET_ADDR_STR "|%03d|%03d|%04d|%05d|%010d|%07d|%c %c %c %c %c|%06d|%06d|\r\n",
                 AL_NET_ADDR_TO_STR(&kap->bssid),
                 kap->channel,
                 kap->signal,
                 kap->direct_bit_rate,
                 kap->tree_bit_rate,
                 diff,
                 kap->disconnect_count,
                 kap->flags & MESH_EXT_KAP_FLAG_PREFERRED ? 'P' : '-',
                 kap->flags & MESH_EXT_KAP_FLAG_MOBILE ? 'M' : '-',
                 kap->flags & MESH_EXT_KAP_FLAG_CHILD ? 'C' : '-',
                 kap->flags & MESH_EXT_KAP_FLAG_QUESTION ? 'Q' : '-',
                 kap->flags & MESH_EXT_KAP_FLAG_DISABLED ? 'D' : '-',
                 kap->sample_packets,
                 kap->max_sample_packets);
   }
}


static int _kap_list_proc_open(struct inode *inode, struct file *file)
{
   return single_open(file, _kap_list_routine, NULL);
}


static const struct file_operations _kap_list_fops =
{
   .owner   = THIS_MODULE,
   .open    = _kap_list_proc_open,
   .read    = seq_read,
   .llseek  = seq_lseek,
   .release = single_release,
};



static void _enum_mobwin_routine(void *callback, const mesh_ext_mobilty_window_t *item)
{
   struct seq_file *m;

   m = (struct seq_file *)callback;

   seq_printf(m,
              AL_NET_ADDR_STR "|%03d|%03d|%08X|%03d|%c %c %c %c %c %c %c|\r\n",
              AL_NET_ADDR_TO_STR(&item->parent_bssid),
              item->channel,
              item->signal,
              item->window_item_id,
              item->pattern_count,
              item->flags & MESH_EXT_KAP_FLAG_PREFERRED ? 'P' : '-',
              item->flags & MESH_EXT_KAP_FLAG_MOBILE ? 'M' : '-',
              item->flags & MESH_EXT_KAP_FLAG_CHILD ? 'C' : '-',
              item->flags & MESH_EXT_KAP_FLAG_AD_DISABLE ? 'W' : '-',
              item->flags & MESH_EXT_KAP_FLAG_QUESTION ? 'Q' : '-',
              item->flags & MESH_EXT_KAP_FLAG_DISABLED ? 'D' : '-',
              item->flags & MESH_EXT_KAP_FLAG_LIMITED ? 'L' : '-');
}


static int _mobwin_list_routine(struct seq_file *m, void *v)
{
   seq_printf(m,
              "-----------------------------------------------------\r\n"
              "PARENT ADDR      |CHN|SIG|ITEM ID |CNT|FLAGS        |\r\n"
              "-----------------------------------------------------\r\n");

   mesh_ext_enumerate_mobility_window((void *)m, _enum_mobwin_routine);


   return 0;
}


static int _mobwin_list_proc_open(struct inode *inode, struct file *file)
{
   return single_open(file, _mobwin_list_routine, NULL);
}


static const struct file_operations _mobwin_list_fops =
{
   .owner   = THIS_MODULE,
   .open    = _mobwin_list_proc_open,
   .read    = seq_read,
   .llseek  = seq_lseek,
   .release = single_release,
};


static int _table_list_routine(struct seq_file *m, void *v)
{
   seq_printf(m,
              "------------------------------------------------------------------------\r\n"
              "STATION ADDR : RELAY AP ADDR -> IMMEDIATE AP ADDR   \r\n"
              "------------------------------------------------------------------------\r\n");

   mesh_ext_enumerate_table((void *)m, _enum_table_routine);

   return 0;
}


static void _enum_table_routine(void *callback, const mesh_ext_table_entry_t *entry)
{
   int i;
   struct seq_file *m;

   m = (struct seq_file *)callback;


   for (i = 0; i < entry->level; i++)
   {
      seq_printf(m, "\t");
   }

   seq_printf(m,
              AL_NET_ADDR_STR " : "AL_NET_ADDR_STR " -> "AL_NET_ADDR_STR "\r\n",
              AL_NET_ADDR_TO_STR(&entry->sta_addr),
              AL_NET_ADDR_TO_STR(&entry->relay_ap_addr),
              AL_NET_ADDR_TO_STR(&entry->sta_ap_addr));
}


static int _table_list_proc_open(struct inode *inode, struct file *file)
{
   return single_open(file, _table_list_routine, NULL);
}


static const struct file_operations _table_list_fops =
{
   .owner   = THIS_MODULE,
   .open    = _table_list_proc_open,
   .read    = seq_read,
   .llseek  = seq_lseek,
   .release = single_release,
};

static int _htable_list_routine(struct seq_file *m, void *v)
{
   TABLE_START(m);
   TABLE_ROW_HEAD_START(m);
   TABLE_ROW_HEAD(m, "Client");
   TABLE_ROW_HEAD(m, "Relay AP");
   TABLE_ROW_HEAD(m, "Immediate AP");
   TABLE_ROW_HEAD_END(m);
   mesh_ext_enumerate_table((void *)m, _henum_table_routine);
   TABLE_END(m);

   return 0;
}


static void _henum_table_routine(void *callback, const mesh_ext_table_entry_t *entry)
{
   struct seq_file *m;

   m = (struct seq_file *)callback;

   TABLE_ROW_DATA_START(m);
   TABLE_ROW_DATA_MAC(m, AL_NET_ADDR_TO_STR(&entry->sta_addr));
   TABLE_ROW_DATA_MAC(m, AL_NET_ADDR_TO_STR(&entry->relay_ap_addr));
   TABLE_ROW_DATA_MAC(m, AL_NET_ADDR_TO_STR(&entry->sta_ap_addr));
   TABLE_ROW_DATA_END(m);
}


static int _htable_list_proc_open(struct inode *inode, struct file *file)
{
   return single_open(file, _htable_list_routine, NULL);
}


static const struct file_operations _htable_list_fops =
{
   .owner   = THIS_MODULE,
   .open    = _htable_list_proc_open,
   .read    = seq_read,
   .llseek  = seq_lseek,
   .release = single_release,
};
static void _meshap_mesh_proc_table_list_initialize(void)
{
   meshap_mesh_proc_add_read_entry("table", &_table_list_fops, NULL);
   meshap_mesh_proc_add_read_entry("htable", &_htable_list_fops, NULL);
}


static int _hkap_list_routine(struct seq_file *m, void *v)
{
   TABLE_START(m);
   TABLE_ROW_HEAD_START(m);
   TABLE_ROW_HEAD(m, "Address");
   TABLE_ROW_HEAD(m, "Channel");
   TABLE_ROW_HEAD(m, "Signal");
   TABLE_ROW_HEAD(m, "Rate");
   TABLE_ROW_HEAD(m, "Tree Rate");
   TABLE_ROW_HEAD(m, "Flags");
   TABLE_ROW_HEAD_END(m);

   mesh_ext_enumerate_known_aps((void *)m, _henum_kap_routine);

   TABLE_END(m);

   return 0;
}


static void _henum_kap_routine(void *callback, const mesh_ext_known_access_point_info_t *kap)
{
   char flags[6];
   struct seq_file *m;

   m = (struct seq_file *)callback;

   flags[0] = kap->flags & MESH_EXT_KAP_FLAG_PREFERRED      ? 'P' : '-';
   flags[1] = kap->flags & MESH_EXT_KAP_FLAG_MOBILE         ? 'M' : '-';
   flags[2] = kap->flags & MESH_EXT_KAP_FLAG_CHILD          ? 'C' : '-';
   flags[3] = kap->flags & MESH_EXT_KAP_FLAG_QUESTION       ? 'Q' : '-';
   flags[4] = kap->flags & MESH_EXT_KAP_FLAG_DISABLED       ? 'D' : '-';
   flags[4] = 0;

   TABLE_ROW_DATA_START(m);
   TABLE_ROW_DATA_MAC(m, AL_NET_ADDR_TO_STR(&kap->bssid));
   TABLE_ROW_DATA_INT(m, kap->channel);
   TABLE_ROW_DATA_INT(m, kap->signal);
   TABLE_ROW_DATA_INT(m, kap->direct_bit_rate);
   TABLE_ROW_DATA_INT(m, kap->tree_bit_rate);
   TABLE_ROW_DATA_STR(m, flags);
   TABLE_ROW_DATA_END(m);
}


static int _hkap_list_proc_open(struct inode *inode, struct file *file)
{
   return single_open(file, _hkap_list_routine, NULL);
}


static const struct file_operations _hkap_list_fops =
{
   .owner   = THIS_MODULE,
   .open    = _hkap_list_proc_open,
   .read    = seq_read,
   .llseek  = seq_lseek,
   .release = single_release,
};

static const char _func_modes[][8] =
{
   "UNK",
   "FFR",
   "FFN",
   "LFR",
   "LFN",
   "LFRS"
};

static void _adhoc_kap_routine(void *callback, const mesh_ext_known_access_point_info_t *kap)
{
   struct seq_file *m;

   m = (struct seq_file *)callback;
   const char *fnc;

   if (kap->flags & MESH_EXT_KAP_FLAG_LIMITED)
   {
      if (kap->hpc == 0)
      {
         fnc = _func_modes[MESH_EXT_FUNC_MODE_LFR];
      }
      else
      {
         fnc = _func_modes[MESH_EXT_FUNC_MODE_LFN];
      }
   }
   else
   {
      if (kap->hpc == 0)
      {
         fnc = _func_modes[MESH_EXT_FUNC_MODE_FFR];
      }
      else
      {
         fnc = _func_modes[MESH_EXT_FUNC_MODE_FFN];
      }
   }

   seq_printf(m,
              AL_NET_ADDR_STR "|%03d|%03d|%04d|%05d|%c %c %c %c %c %c|%3s|%05d|\r\n",
              AL_NET_ADDR_TO_STR(&kap->bssid),
              kap->channel,
              kap->signal,
              kap->direct_bit_rate,
              kap->tree_bit_rate,
              kap->flags & MESH_EXT_KAP_FLAG_PREFERRED ? 'P' : '-',
              kap->flags & MESH_EXT_KAP_FLAG_MOBILE ? 'M' : '-',
              kap->flags & MESH_EXT_KAP_FLAG_CHILD ? 'C' : '-',
              kap->flags & MESH_EXT_KAP_FLAG_AD_DISABLE ? 'W' : '-',
              kap->flags & MESH_EXT_KAP_FLAG_DISABLED ? 'D' : '-',
              kap->flags & MESH_EXT_KAP_FLAG_LIMITED ? 'L' : '-',
              fnc,
              kap->dhcp_r_value);
}


static int debug_log_level_show(struct file *m, void *v)
{
   seq_printf(m, "0x%x\r\n", meshap_core_globals.log_mask);

   return 0;
}

static int debug_log_level_proc_open(struct inode *inode, struct file *file)
{
   return single_open(file, debug_log_level_show, NULL);
}

int debug_log_level_write_handler(struct file *file, const char *buffer, unsigned long count, void *data)
{
   char debug_info_buf[PROCFS_MAX_SIZE];

   debug_info_buf_size = count;
   if (debug_info_buf_size > PROCFS_MAX_SIZE)
   {
      debug_info_buf_size = PROCFS_MAX_SIZE;
   }

   if (copy_from_user(debug_info_buf, buffer, debug_info_buf_size))
   {
      return -EFAULT;
   }
   else
   {
      meshap_core_globals.log_mask = simple_strtoul(debug_info_buf, debug_info_buf + debug_info_buf_size, 0);
   }

   return debug_info_buf_size;
}

struct file_operations debug_proc_fops =
{
   .owner   = THIS_MODULE,
   .open    = debug_log_level_proc_open,
   .read    = seq_read,
   .write   = debug_log_level_write_handler,
   .llseek  = seq_lseek,
   .release = single_release,
};


void meshap_mesh_proc_add_read_write_entry(const char *name, struct file_operations *fops, void *data)
{
   proc_create_data(name, 0644, _meshap_mesh_proc_entry, fops, data);
}


static int _adhoc_mode_routine(struct seq_file *m, void *v)
{
   int        func_mode;
   const char *fnc;

   func_mode = mesh_ext_get_func_mode();

   switch (func_mode)
   {
   case MESH_EXT_FUNC_MODE_FFR:
   case MESH_EXT_FUNC_MODE_FFN:
   case MESH_EXT_FUNC_MODE_LFR:
   case MESH_EXT_FUNC_MODE_LFN:
   case MESH_EXT_FUNC_MODE_LFRS:
      fnc = _func_modes[func_mode];
      break;

   default:
      fnc = "UNKNOWN";
   }

   seq_printf(m, "%s\r\n",fnc);

	return 0;
}

#if MESH_CPU_PROFILE_ENABLED
static int profiling_stats_routine(struct seq_file *m, void *v)
{
	seq_printf(m, "%-*s: %lldns, %lldns, %lldns\r\n"
			"%-*s: %lldns, %lldns, %lldns\r\n"
			"%-*s: %lldns, %lldns, %lldns\r\n"
			"%-*s: %lldns, %lldns, %lldns\r\n"
			"%-*s: %lldns, %lldns, %lldns\r\n"
			"%-*s: %lldns, %lldns, %lldns\r\n"
			"%-*s: %lldns, %lldns, %lldns\r\n"
			"%-*s: %lldns, %lldns, %lldns\r\n"
			"%-*s: %lldns, %lldns, %lldns\r\n"
			"%-*s: %lldns, %lldns, %lldns\r\n"
			"%-*s: %lldns, %lldns, %lldns\r\n"
			"%-*s: %lldns, %lldns, %lldns\r\n"
			"%-*s: %lldns, %lldns, %lldns\r\n"
			"%-*s: %lldns, %lldns, %lldns\r\n"
			"%-*s: %lldns, %lldns, %lldns\r\n"
			"%-*s: %lldns, %lldns, %lldns\r\n"
			"%-*s: %lldns, %lldns, %lldns\r\n"
			"%-*s: %lldns, %lldns, %lldns\r\n"
			"%-*s: %lldns, %lldns, %lldns\r\n"
			"%-*s: %lldns, %lldns, %lldns\r\n"
			"%-*s: %lldns, %lldns, %lldns\r\n"
			"%-*s: %lldns, %lldns, %lldns\r\n"
			"%-*s: %lldns, %lldns, %lldns\r\n"
			"%-*s: %lldns, %lldns, %lldns\r\n"
			"%-*s: %lldns, %lldns, %lldns\r\n"
			"%-*s: %lldns, %lldns, %lldns\r\n"
			"%-*s: %lldns, %lldns, %lldns\r\n"
			"%-*s: %lldns, %lldns, %lldns\r\n"
			"%-*s: %lldns, %lldns, %lldns\r\n"
			"%-*s: %lldns, %lldns, %lldns\r\n"
			"%-*s: %lldns, %lldns, %lldns\r\n"
			"%-*s: %lldns, %lldns, %lldns\r\n",
		    THREAD_STAT_ALLIGN, "meshap_skb_enqueue_splock", (long long)profiling_info.meshap_skb_enqueue_splock[0], (long long)profiling_info.meshap_skb_enqueue_splock[1], (long long)profiling_info.meshap_skb_enqueue_splock[2],
		    THREAD_STAT_ALLIGN, "meshap_skb_enqueue_spunlock", (long long)profiling_info.meshap_skb_enqueue_spunlock[0], (long long)profiling_info.meshap_skb_enqueue_spunlock[1], (long long)profiling_info.meshap_skb_enqueue_spunlock[2],
			THREAD_STAT_ALLIGN, "meshap_skb_deq_splock", (long long)profiling_info.meshap_skb_deq_splock[0], (long long)profiling_info.meshap_skb_deq_splock[1], (long long)profiling_info.meshap_skb_deq_splock[2],
			THREAD_STAT_ALLIGN, "meshap_skb_deq_spunlock", (long long)profiling_info.meshap_skb_deq_spunlock[0], (long long)profiling_info.meshap_skb_deq_spunlock[1], (long long)profiling_info.meshap_skb_deq_spunlock[2],
		    THREAD_STAT_ALLIGN, "process_skb_func_tprof", (long long)profiling_info.process_skb_func_tprof[0], (long long)profiling_info.process_skb_func_tprof[1], (long long)profiling_info.process_skb_func_tprof[2],
		    THREAD_STAT_ALLIGN, "process_wm_pkt_func_tprof", (long long)profiling_info.process_wm_pkt_func_tprof[0], (long long)profiling_info.process_wm_pkt_func_tprof[1], (long long)profiling_info.process_wm_pkt_func_tprof[2],
		    THREAD_STAT_ALLIGN, "process_ds_pkt_func_tprof", (long long)profiling_info.process_ds_pkt_func_tprof[0], (long long)profiling_info.process_ds_pkt_func_tprof[1], (long long)profiling_info.process_ds_pkt_func_tprof[2],
		    THREAD_STAT_ALLIGN, "process_mip_pkt_func_tprof", (long long)profiling_info.process_mip_pkt_func_tprof[0], (long long)profiling_info.process_mip_pkt_func_tprof[1], (long long)profiling_info.process_mip_pkt_func_tprof[2],
		    THREAD_STAT_ALLIGN, "upstack_send_pkt_func_tprof", (long long)profiling_info.upstack_send_pkt_func_tprof[0], (long long)profiling_info.upstack_send_pkt_func_tprof[1], (long long)profiling_info.upstack_send_pkt_func_tprof[2],
		    THREAD_STAT_ALLIGN, "upstack_process_pkt_func_tprof", (long long)profiling_info.upstack_process_pkt_func_tprof[0], (long long)profiling_info.upstack_process_pkt_func_tprof[1], (long long)profiling_info.upstack_process_pkt_func_tprof[2],
		    THREAD_STAT_ALLIGN, "tx_thread_tprof", (long long)profiling_info.tx_thread_tprof[0], (long long)profiling_info.tx_thread_tprof[1], (long long)profiling_info.tx_thread_tprof[2],
		    THREAD_STAT_ALLIGN, "process_mip_skb_func_tprof", (long long)profiling_info.process_mip_skb_func_tprof[0], (long long)profiling_info.process_mip_skb_func_tprof[1], (long long)profiling_info.process_mip_skb_func_tprof[2],
		    THREAD_STAT_ALLIGN, "process_mip_xmit_func_tprof", (long long)profiling_info.process_mip_xmit_func_tprof[0], (long long)profiling_info.process_mip_xmit_func_tprof[1], (long long)profiling_info.process_mip_xmit_func_tprof[2],
		    THREAD_STAT_ALLIGN, "process_eth_skb_func_tprof", (long long)profiling_info.process_eth_skb_func_tprof[0], (long long)profiling_info.process_eth_skb_func_tprof[1], (long long)profiling_info.process_eth_skb_func_tprof[2],
			THREAD_STAT_ALLIGN, "dot1pQ_imcpsplock_1", (long long)profiling_info.dot1pQ_imcpsplock_1[0], (long long)profiling_info.dot1pQ_imcpsplock_1[1], (long long)profiling_info.dot1pQ_imcpsplock_1[2],
			THREAD_STAT_ALLIGN, "dot1pQ_imcpspunlock_1", (long long)profiling_info.dot1pQ_imcpspunlock_1[0], (long long)profiling_info.dot1pQ_imcpspunlock_1[1], (long long)profiling_info.dot1pQ_imcpspunlock_1[2],
			//THREAD_STAT_ALLIGN, "dot1p_Qsplock", (long long)profiling_info.dot1p_Qsplock[0], (long long)profiling_info.dot1p_Qsplock[1], (long long)profiling_info.dot1p_Qsplock[2],
			//THREAD_STAT_ALLIGN, "dot1p_Qspunlock", (long long)profiling_info.dot1p_Qspunlock[0], (long long)profiling_info.dot1p_Qspunlock[1], (long long)profiling_info.dot1p_Qspunlock[2],
			THREAD_STAT_ALLIGN, "dot1pQ_imcpsplock_2", (long long)profiling_info.dot1pQ_imcpsplock_2[0], (long long)profiling_info.dot1pQ_imcpsplock_2[1], (long long)profiling_info.dot1pQ_imcpsplock_2[2],
			THREAD_STAT_ALLIGN, "dot1pQ_imcpspunlock_2", (long long)profiling_info.dot1pQ_imcpspunlock_2[0], (long long)profiling_info.dot1pQ_imcpspunlock_2[1], (long long)profiling_info.dot1pQ_imcpspunlock_2[2],
			THREAD_STAT_ALLIGN, "dot1p_Qsplock_1", (long long)profiling_info.dot1p_Qsplock_1[0], (long long)profiling_info.dot1p_Qsplock_1[1], (long long)profiling_info.dot1p_Qsplock_1[2],
			THREAD_STAT_ALLIGN, "dot1p_Qspunlock_1", (long long)profiling_info.dot1p_Qspunlock_1[0], (long long)profiling_info.dot1p_Qspunlock_1[1], (long long)profiling_info.dot1p_Qspunlock_1[2],
			THREAD_STAT_ALLIGN, "dot1p_Qsplock_2", (long long)profiling_info.dot1p_Qsplock_2[0], (long long)profiling_info.dot1p_Qsplock_2[1], (long long)profiling_info.dot1p_Qsplock_2[2],
			THREAD_STAT_ALLIGN, "dot1p_Qspunlock_2", (long long)profiling_info.dot1p_Qspunlock_2[0], (long long)profiling_info.dot1p_Qspunlock_2[1], (long long)profiling_info.dot1p_Qspunlock_2[2],
			THREAD_STAT_ALLIGN, "dot1p_Qsplock_3", (long long)profiling_info.dot1p_Qsplock_3[0], (long long)profiling_info.dot1p_Qsplock_3[1], (long long)profiling_info.dot1p_Qsplock_3[2],
			THREAD_STAT_ALLIGN, "dot1p_Qspunlock_3", (long long)profiling_info.dot1p_Qspunlock_3[0], (long long)profiling_info.dot1p_Qspunlock_3[1], (long long)profiling_info.dot1p_Qspunlock_3[2],
			//THREAD_STAT_ALLIGN, "dot1p_Qsplock_4", (long long)profiling_info.dot1p_Qsplock_4[0], (long long)profiling_info.dot1p_Qsplock_4[1], (long long)profiling_info.dot1p_Qsplock_4[2],
			//THREAD_STAT_ALLIGN, "dot1p_Qspunlock_4", (long long)profiling_info.dot1p_Qspunlock_4[0], (long long)profiling_info.dot1p_Qspunlock_4[1], (long long)profiling_info.dot1p_Qspunlock_4[2],
			THREAD_STAT_ALLIGN, "core_pkt_splock_1", (long long)profiling_info.core_pkt_splock_1[0], (long long)profiling_info.core_pkt_splock_1[1], (long long)profiling_info.core_pkt_splock_1[2],
			THREAD_STAT_ALLIGN, "core_pkt_spunlock_1", (long long)profiling_info.core_pkt_spunlock_1[0], (long long)profiling_info.core_pkt_spunlock_1[1], (long long)profiling_info.core_pkt_spunlock_1[2],
			THREAD_STAT_ALLIGN, "core_pkt_splock_2", (long long)profiling_info.core_pkt_splock_2[0], (long long)profiling_info.core_pkt_splock_2[1], (long long)profiling_info.core_pkt_splock_2[2],
			THREAD_STAT_ALLIGN, "core_pkt_spunlock_2", (long long)profiling_info.core_pkt_spunlock_2[0], (long long)profiling_info.core_pkt_spunlock_2[1], (long long)profiling_info.core_pkt_spunlock_2[2],
			THREAD_STAT_ALLIGN, "core_pkt_splock_3", (long long)profiling_info.core_pkt_splock_3[0], (long long)profiling_info.core_pkt_splock_3[1], (long long)profiling_info.core_pkt_splock_3[2],
			THREAD_STAT_ALLIGN, "core_pkt_spunlock_3", (long long)profiling_info.core_pkt_spunlock_3[0], (long long)profiling_info.core_pkt_spunlock_3[1], (long long)profiling_info.core_pkt_spunlock_3[2],
			THREAD_STAT_ALLIGN, "core_pkt_splock_4", (long long)profiling_info.core_pkt_splock_4[0], (long long)profiling_info.core_pkt_splock_4[1], (long long)profiling_info.core_pkt_splock_4[2],
			THREAD_STAT_ALLIGN, "core_pkt_spunlock_4", (long long)profiling_info.core_pkt_spunlock_4[0], (long long)profiling_info.core_pkt_spunlock_4[1], (long long)profiling_info.core_pkt_spunlock_4[2],
			THREAD_STAT_ALLIGN, "core_pkt_splock_5", (long long)profiling_info.core_pkt_splock_5[0], (long long)profiling_info.core_pkt_splock_5[1], (long long)profiling_info.core_pkt_splock_5[2],
			THREAD_STAT_ALLIGN, "core_pkt_spunlock_5", (long long)profiling_info.core_pkt_spunlock_5[0], (long long)profiling_info.core_pkt_spunlock_5[1], (long long)profiling_info.core_pkt_spunlock_5[2],
			THREAD_STAT_ALLIGN, "core_pkt_splock_6", (long long)profiling_info.core_pkt_splock_6[0], (long long)profiling_info.core_pkt_splock_6[1], (long long)profiling_info.core_pkt_splock_6[2],
			THREAD_STAT_ALLIGN, "core_pkt_spunlock_6", (long long)profiling_info.core_pkt_spunlock_6[0], (long long)profiling_info.core_pkt_spunlock_6[1], (long long)profiling_info.core_pkt_spunlock_6[2],
			THREAD_STAT_ALLIGN, "core_pkt_splock_3", (long long)profiling_info.core_pkt_splock_3[0], (long long)profiling_info.core_pkt_splock_3[1], (long long)profiling_info.core_pkt_splock_3[2],
			THREAD_STAT_ALLIGN, "core_pkt_spunlock_3", (long long)profiling_info.core_pkt_spunlock_3[0], (long long)profiling_info.core_pkt_spunlock_3[1], (long long)profiling_info.core_pkt_spunlock_3[2],
			THREAD_STAT_ALLIGN, "core_pkt_splock_4", (long long)profiling_info.core_pkt_splock_4[0], (long long)profiling_info.core_pkt_splock_4[1], (long long)profiling_info.core_pkt_splock_4[2],
			THREAD_STAT_ALLIGN, "core_pkt_spunlock_4", (long long)profiling_info.core_pkt_spunlock_4[0], (long long)profiling_info.core_pkt_spunlock_4[1], (long long)profiling_info.core_pkt_spunlock_4[2],
			THREAD_STAT_ALLIGN, "core_pkt_splock_5", (long long)profiling_info.core_pkt_splock_5[0], (long long)profiling_info.core_pkt_splock_5[1], (long long) profiling_info.core_pkt_splock_5[2],
			THREAD_STAT_ALLIGN, "core_pkt_spunlock_5", (long long)profiling_info.core_pkt_spunlock_5[0], (long long)profiling_info.core_pkt_spunlock_5[1], (long long)profiling_info.core_pkt_spunlock_5[2],
			THREAD_STAT_ALLIGN, "core_pkt_splock_6", (long long)profiling_info.core_pkt_splock_6[0], (long long)profiling_info.core_pkt_splock_6[1], (long long long)profiling_info.core_pkt_splock_6[2],
			THREAD_STAT_ALLIGN, "core_pkt_spunlock_6", (long long)profiling_info.core_pkt_spunlock_6[0], (long long)profiling_info.core_pkt_spunlock_6[1], (long long)profiling_info.core_pkt_spunlock_6[2]);
	return 0;
}

static int profiling_proc_open(struct inode *inode, struct file *file)
{
	   return single_open(file, profiling_stats_routine, NULL);
}
#endif

static int adhoc_mode_proc_open(struct inode *inode, struct file *file)
{
   return single_open(file, _adhoc_mode_routine, NULL);
}

static const struct file_operations _adhoc_mode_fops =
{
   .owner   = THIS_MODULE,
   .open    = adhoc_mode_proc_open,
   .read    = seq_read,
   .llseek  = seq_lseek,
   .release = single_release,
};

#if MESH_CPU_PROFILE_ENABLED
static const struct file_operations profiling_stats_fops =
{
	.owner   = THIS_MODULE,
	.open    = profiling_proc_open,
	.read    = seq_read,
	.llseek  = seq_lseek,
	.release = single_release,
};
#endif
static int _adhoc_list_routine(struct seq_file *m, void *v)
{
   int        func_mode;
   const char *fnc;
   int        step_scan_index;

   func_mode       = mesh_ext_get_func_mode();
   step_scan_index = mesh_ext_get_step_scan_channel_index();

   switch (func_mode)
   {
   case MESH_EXT_FUNC_MODE_FFR:
   case MESH_EXT_FUNC_MODE_FFN:
   case MESH_EXT_FUNC_MODE_LFN:
      fnc = _func_modes[func_mode];
      break;

   case MESH_EXT_FUNC_MODE_LFR:
      if ((!strcmp(mesh_hardware_info->ds_net_if->name, "eth1") && (mesh_config->failover_enabled)))
      {
         fnc = "LFRS";
      }
      else
      {
         fnc = _func_modes[func_mode];
      }
      break;

   default:
      fnc = "UNKNOWN";
   }

   seq_printf(m,
              "NODE OPERATING AS %s STEP SCAN INDEX %d\r\n\r\n"
              "-----------------------------------------------------------\r\n"
              "PARENT ADDR      |CHN|SIG|RATE|TRATE|FLAGS      |FNC|R-VAL|\r\n"
              "-----------------------------------------------------------\r\n",
              fnc,
              step_scan_index);

   mesh_ext_enumerate_known_aps((void *)m, _adhoc_kap_routine);
}


static int _adhoc_list_proc_open(struct inode *inode, struct file *file)
{
   return single_open(file, _adhoc_list_routine, NULL);
}


static const struct file_operations _adhoc_list_fops =
{
   .owner   = THIS_MODULE,
   .open    = _adhoc_list_proc_open,
   .read    = seq_read,
   .llseek  = seq_lseek,
   .release = single_release,
};

static int _version_routine(struct seq_file *m, void *v)
{
    seq_printf(m, "meshd_version: %d.%d."_SUB_VERSION_"_%d_"_VERSION_MONTH_"_%d_%d:%d\r\n", _TORNA_VERSION_MAJOR_, _TORNA_VERSION_MINOR_, _VERSION_DATE_, _VERSION_YEAR_, _VERSION_HOUR_, _VERSION_MINUTE_);
    return 0;
}

static int _version_proc_open(struct inode *inode, struct file *file)
{
   return single_open(file, _version_routine, NULL);
}

static const struct file_operations version_fops =
{
   .owner   = THIS_MODULE,
   .open    = _version_proc_open,
   .read    = seq_read,
   .llseek  = seq_lseek,
   .release = single_release,
};

#define THREAD_STAT_ALLIGN	40
static int _thread_stats_routine(struct seq_file *m, void *v)
{
	int i;
	char buf[THREAD_STAT_ALLIGN];

	for (i = 0; i < thread_stat.ap_thread; i++)
	{
		snprintf(buf, THREAD_STAT_ALLIGN, "Ap_thread [%d]", i + 1);
		seq_printf(m, "%-*s: %u\n", THREAD_STAT_ALLIGN, buf, thread_stat.ap_thread_count[i]);
	}

	for (i = 0; i < thread_stat.tx_thread; i++)
	{
		snprintf(buf, THREAD_STAT_ALLIGN, "Tx_thread [%d]", i + 1);
		seq_printf(m, "%-*s: %u\n", THREAD_STAT_ALLIGN, buf, thread_stat.tx_thread_count[i]);
	}

	seq_printf(m, "%-*s: %u\n"
						"%-*s: %u\n"
						"%-*s: %u\n"
						"%-*s: %u\n"
						"%-*s: %u\n"
						"%-*s: %u\n"
						"%-*s: %u\n"
						"%-*s: %u\n"
						"%-*s: %u\n"
						"%-*s: %u\n"
						"%-*s: %u\n"
						"%-*s: %u\n"
						"%-*s: %u\n"
						"%-*s: %u\n"
						"%-*s: %u\n"
						"%-*s: %u\n",
					THREAD_STAT_ALLIGN, "process_skb_thread", thread_stat.process_skb_thread_count,
					THREAD_STAT_ALLIGN, "imcp_thread", thread_stat.imcp_thread_count,
					THREAD_STAT_ALLIGN, "upstack_thread", thread_stat.upstack_thread_count,
					THREAD_STAT_ALLIGN, "Ap_sta_monitor_thread", thread_stat.ap_sta_monitor_thread_count,
					THREAD_STAT_ALLIGN, "Ap_dot11i_thread", thread_stat.ap_dot11i_thread_count,
					THREAD_STAT_ALLIGN, "Md_watch_thread", thread_stat.md_watch_thread_count,
					THREAD_STAT_ALLIGN, "Mesh_Thread", thread_stat.mesh_thread_count,
					THREAD_STAT_ALLIGN, "Mesh_hb_thread", thread_stat.mesh_hb_thread_count,
					THREAD_STAT_ALLIGN, "Uplink_scan_thread", thread_stat.uplink_scan_thread_count,
					THREAD_STAT_ALLIGN, "Lfr_thread", thread_stat.lfr_thread_count,
					THREAD_STAT_ALLIGN, "Scan_thread", thread_stat.scan_thread_count,
					THREAD_STAT_ALLIGN, "Radar_dca_thread", thread_stat.radar_dca_thread_count,
					THREAD_STAT_ALLIGN, "Dot1x_timer_thread", thread_stat.dot1x_timer_thread_count,
					THREAD_STAT_ALLIGN, "dBmTestRx_thread", thread_stat.dBmTestRx_thread_count,
					THREAD_STAT_ALLIGN, "dBmTestTx_thread", thread_stat.dBmTestTx_thread_count,
					THREAD_STAT_ALLIGN, "Sip_cleanup_thread", thread_stat.sip_cleanup_thread_count);

	return 0;
}

static int thread_stats_proc_open(struct inode *inode, struct file *file)
{
   return single_open(file, _thread_stats_routine, NULL);
}

static const struct file_operations _thread_stats_fops =
{
   .owner   = THIS_MODULE,
   .open    = thread_stats_proc_open,
   .read    = seq_read,
   .llseek  = seq_lseek,
   .release = single_release,
};

#define QUEUE_PACKET_STAT_ALLIGN	40
static int _queue_packet_stats_routine(struct seq_file *m, void *v)
{

	seq_printf(m, "%-*s: %u\n"
						"%-*s: %u\n"
						"%-*s: %u(%u)\n"
						"%-*s: %u\n"
						"%-*s: %u\n"
						"%-*s: %u(%u)\n"
						"%-*s: %u\n"
						"%-*s: %u\n"
						"%-*s: %u(%u)\n"
						"%-*s: %u\n"
						"%-*s: %u\n"
						"%-*s: %u(%u)\n"
						"%-*s: %u\n"
						"%-*s: %u\n"
						"%-*s: %u(%u)\n"
						"%-*s: 0thQ:%u\t 1stQ:%u\t 2ndQ:%u\t 3rdQ:%u\n"
						"%-*s: 0thQ:%u\t 1stQ:%u\t 2ndQ:%u\t 3rdQ:%u\n"
						"%-*s: 0thQ:%u(%u)\t 1stQ:%u(%u)\t 2ndQ:%u(%u)\t 3rdQ:%u(%u)\n"
						"%-*s: %u\n"
						"%-*s: %u\n"
						"%-*s: %u(%u)\n"
						"%-*s: %u\n"
						"%-*s: %u\n"
						"%-*s: %u(%u)\n"
						"%-*s: %u\n"
						"%-*s: %u\n"
						"%-*s: %u(%u)\n"
						"%-*s: %u\n"
						"%-*s: %u\n"
						"%-*s: %u\n"
						"%-*s: %u\n"
						"%-*s: %u\n",
					QUEUE_PACKET_STAT_ALLIGN, "dot1p_queue_total_packet_added_count", q_packet_stats.dot1p_queue_total_packet_added_count,
					QUEUE_PACKET_STAT_ALLIGN, "dot1p_queue_total_packet_removed_count", q_packet_stats.dot1p_queue_total_packet_removed_count,
					QUEUE_PACKET_STAT_ALLIGN, "dot1p_queue_current_packet_count", q_packet_stats.dot1p_queue_current_packet_count,
																				  q_packet_stats.dot1p_queue_current_packet_max_count,
					QUEUE_PACKET_STAT_ALLIGN, "dot1p_temp_queue_total_pkt_added_count", q_packet_stats.dot1p_temp_queue_total_packet_added_count,
					QUEUE_PACKET_STAT_ALLIGN, "dot1p_temp_queue_total_pkt_rmvd_count", q_packet_stats.dot1p_temp_queue_total_packet_removed_count,
					QUEUE_PACKET_STAT_ALLIGN, "dot1p_temp_queue_current_packet_count", q_packet_stats.dot1p_temp_queue_current_packet_count,
																				  q_packet_stats.dot1p_temp_queue_current_packet_max_count,
					QUEUE_PACKET_STAT_ALLIGN, "tx_pktQ_total_packet_added_count", q_packet_stats.tx_pktQ_total_packet_added_count,
					QUEUE_PACKET_STAT_ALLIGN, "tx_pktQ_total_packet_removed_count", q_packet_stats.tx_pktQ_total_packet_removed_count,
					QUEUE_PACKET_STAT_ALLIGN, "tx_pktQ_current_packet_count", q_packet_stats.tx_pktQ_current_packet_count,
																			  q_packet_stats.tx_pktQ_current_packet_max_count,
				    QUEUE_PACKET_STAT_ALLIGN, "temp_tx_pktQ_total_packet_added_count", q_packet_stats.temp_tx_pktQ_total_packet_added_count,
				    QUEUE_PACKET_STAT_ALLIGN, "temp_tx_pktQ_total_packet_removed_count", q_packet_stats.temp_tx_pktQ_total_packet_removed_count,
                    QUEUE_PACKET_STAT_ALLIGN, "temp_tx_pktQ_current_packet_count", q_packet_stats.temp_tx_pktQ_current_packet_count,
																				   q_packet_stats.temp_tx_pktQ_current_packet_max_count,
					QUEUE_PACKET_STAT_ALLIGN, "imcp_pktQ_total_packet_added_count", q_packet_stats.imcp_pktQ_total_packet_added_count,
					QUEUE_PACKET_STAT_ALLIGN, "imcp_pktQ_total_packet_removed_count", q_packet_stats.imcp_pktQ_total_packet_removed_count,
					QUEUE_PACKET_STAT_ALLIGN, "imcp_pktQ_current_packet_count", q_packet_stats.imcp_pktQ_current_packet_count,
																			    q_packet_stats.imcp_pktQ_current_packet_max_count,
					QUEUE_PACKET_STAT_ALLIGN, "data_pktQ_total_packet_added_count",
					q_packet_stats.data_pktQ_total_packet_added_count[0],q_packet_stats.data_pktQ_total_packet_added_count[1],
					q_packet_stats.data_pktQ_total_packet_added_count[2],q_packet_stats.data_pktQ_total_packet_added_count[3],
					QUEUE_PACKET_STAT_ALLIGN, "data_pktQ_total_packet_removed_count",
				   q_packet_stats.data_pktQ_total_packet_removed_count[0],q_packet_stats.data_pktQ_total_packet_removed_count[1],
				   q_packet_stats.data_pktQ_total_packet_removed_count[2],q_packet_stats.data_pktQ_total_packet_removed_count[3],
					QUEUE_PACKET_STAT_ALLIGN, "data_pktQ_current_packet_count",
					q_packet_stats.data_pktQ_current_packet_count[0],q_packet_stats.data_pktQ_current_packet_max_count[0],
					q_packet_stats.data_pktQ_current_packet_count[1],q_packet_stats.data_pktQ_current_packet_max_count[1],
					q_packet_stats.data_pktQ_current_packet_count[2],q_packet_stats.data_pktQ_current_packet_max_count[2],
					q_packet_stats.data_pktQ_current_packet_count[3],q_packet_stats.data_pktQ_current_packet_max_count[3],
					QUEUE_PACKET_STAT_ALLIGN, "process_skbQ_total_packet_added_count", q_packet_stats.process_skbQ_total_packet_added_count,
					QUEUE_PACKET_STAT_ALLIGN, "process_skbQ_total_packet_removed_count", q_packet_stats.process_skbQ_total_packet_removed_count,
					QUEUE_PACKET_STAT_ALLIGN, "process_skbQ_current_packet_count", q_packet_stats.process_skbQ_current_packet_count,
																				   q_packet_stats.process_skbQ_current_packet_max_count,
					QUEUE_PACKET_STAT_ALLIGN, "upstack_pktQ_total_packet_added_count", q_packet_stats.upstack_pktQ_total_packet_added_count,
					QUEUE_PACKET_STAT_ALLIGN, "upstack_pktQ_total_packet_removed_count", q_packet_stats.upstack_pktQ_total_packet_removed_count,
					QUEUE_PACKET_STAT_ALLIGN, "upstack_pktQ_current_packet_count", q_packet_stats.upstack_pktQ_current_packet_count,
																				   q_packet_stats.upstack_pktQ_current_packet_max_count,
                    QUEUE_PACKET_STAT_ALLIGN, "temp_upstack_pktQ_total_pkt_add_count", q_packet_stats.temp_upstack_pktQ_total_packet_added_count,
                    QUEUE_PACKET_STAT_ALLIGN, "temp_upstack_pktQ_total_pkt_rmd_count", q_packet_stats.temp_upstack_pktQ_total_packet_removed_count,
                    QUEUE_PACKET_STAT_ALLIGN, "temp_upstack_pktQ_current_packet_count", q_packet_stats.temp_upstack_pktQ_current_packet_count,
																					    q_packet_stats.temp_upstack_pktQ_current_packet_max_count,
					QUEUE_PACKET_STAT_ALLIGN, "core_packet_type_alloc_heap", q_packet_stats.core_packet_type_alloc_heap,
					QUEUE_PACKET_STAT_ALLIGN, "Core_packet_pool_current_count", q_packet_stats.core_packet_pool_current_count,
					QUEUE_PACKET_STAT_ALLIGN, "procs_skb_core_pkt_pool_current_count", q_packet_stats.procs_skb_core_pkt_pool_current_count,
					QUEUE_PACKET_STAT_ALLIGN, "ap_thread_core_pkt_pool_current_count", q_packet_stats.ap_thread_core_pkt_pool_current_count,
					QUEUE_PACKET_STAT_ALLIGN, "imcp_thread_core_pkt_pool_current_count", q_packet_stats.imcp_thread_core_pkt_pool_current_count);


	return 0;
}

static int queue_packet_stats_proc_open(struct inode *inode, struct file *file)
{
   return single_open(file, _queue_packet_stats_routine, NULL);
}

static const struct file_operations _queue_packet_stats_fops =
{
   .owner   = THIS_MODULE,
   .open    = queue_packet_stats_proc_open,
   .read    = seq_read,
   .llseek  = seq_lseek,
   .release = single_release,
};

#define TX_RX_DROP_STAT_ALLIGN	50
#define TX_RX_DROP_STAT_ALLIGN_T	3
#define CALL_TX_RX_DROP_PKT_STAT(_m, _name, _val)		(_val == 0) ? : seq_printf(_m, "%-*s : %u\n", TX_RX_DROP_STAT_ALLIGN, _name, _val)
#define CALL_TX_RX_DROP_PKT_STAT_T(_m, _name, i, _val)		(_val == 0) ? : seq_printf(_m, "%s[%-*d]_count \t\t\t: %u\n", _name, TX_RX_DROP_STAT_ALLIGN_T, i, _val)

static int _tx_rx_drop_pkt_stats_routine(struct seq_file *m, void *v)
{
	meshap_core_net_if_t *net_if;
	al_net_if_stat_t net_if_stats;
	int i;

	CALL_TX_RX_DROP_PKT_STAT(m, "access_point_process_mgmt_pkt", tx_rx_pkt_stats.access_point_process_mgmt_pkt);
	CALL_TX_RX_DROP_PKT_STAT(m, "access_point_process_data_pkt", tx_rx_pkt_stats.access_point_process_data_pkt);
	CALL_TX_RX_DROP_PKT_STAT(m, "access_point_process_pkt", tx_rx_pkt_stats.access_point_process_pkt);
	CALL_TX_RX_DROP_PKT_STAT(m, "tx_transform_pkt_on_wm", tx_rx_pkt_stats.tx_transform_pkt_on_wm);
	CALL_TX_RX_DROP_PKT_STAT(m, "tx_transform_pkt_on_ds", tx_rx_pkt_stats.tx_transform_pkt_on_ds);
	CALL_TX_RX_DROP_PKT_STAT(m, "tx_de_transform_pkt_on_ds", tx_rx_pkt_stats.tx_de_transform_pkt_on_ds);
	CALL_TX_RX_DROP_PKT_STAT(m, "rx_up_stack_pkt", tx_rx_pkt_stats.rx_up_stack_pkt);
	CALL_TX_RX_DROP_PKT_STAT(m, "tx_pkt", tx_rx_pkt_stats.tx_pkt);
	CALL_TX_RX_DROP_PKT_STAT(m, "tx_imcp_pkt", tx_rx_pkt_stats.tx_imcp_pkt);
	CALL_TX_RX_DROP_PKT_STAT(m, "test_direct_eth_pkt", tx_rx_pkt_stats.test_direct_eth_pkt);
	CALL_TX_RX_DROP_PKT_STAT(m, "test_direct_wireless_pkt", tx_rx_pkt_stats.test_direct_wireless_pkt);
	CALL_TX_RX_DROP_PKT_STAT(m, "test_eth_to_eth_pkt", tx_rx_pkt_stats.test_eth_to_eth_pkt);
	CALL_TX_RX_DROP_PKT_STAT(m, "test_eth_to_wireless_pkt", tx_rx_pkt_stats.test_eth_to_wireless_pkt);
	CALL_TX_RX_DROP_PKT_STAT(m, "test_wireless_to_eth_pkt", tx_rx_pkt_stats.test_wireless_to_eth_pkt);
	CALL_TX_RX_DROP_PKT_STAT(m, "test_wireless_to_wireless_pkt", tx_rx_pkt_stats.test_wireless_to_wireless_pkt);
	CALL_TX_RX_DROP_PKT_STAT(m, "tx_sip_pkt", tx_rx_pkt_stats.tx_sip_pkt);
	CALL_TX_RX_DROP_PKT_STAT(m, "al_test_4_1_PUT", tx_rx_pkt_stats.al_test_4_1_PUT);
	CALL_TX_RX_DROP_PKT_STAT(m, "al_test_4_1_RES", tx_rx_pkt_stats.al_test_4_1_RES);
	CALL_TX_RX_DROP_PKT_STAT(m, "al_test_4_1_RESPUT", tx_rx_pkt_stats.al_test_4_1_RESPUT);
	CALL_TX_RX_DROP_PKT_STAT(m, "mgmt_pkt_process_auth", tx_rx_pkt_stats.mgmt_pkt_process_auth);
	CALL_TX_RX_DROP_PKT_STAT(m, "mgmt_pkt_process_assoc", tx_rx_pkt_stats.mgmt_pkt_process_assoc);
	CALL_TX_RX_DROP_PKT_STAT(m, "mip_xmit_tx", tx_rx_pkt_stats.mip_xmit_tx);
	CALL_TX_RX_DROP_PKT_STAT(m, "rx_master_mode_mgmt_probe_req_frame", tx_rx_pkt_stats.rx_master_mode_mgmt_probe_req_frame);
	CALL_TX_RX_DROP_PKT_STAT(m, "rx_infra_mode_mgmt_frame", tx_rx_pkt_stats.rx_infra_mode_mgmt_frame);
	CALL_TX_RX_DROP_PKT_STAT(m, "wm_pkt_process_dhcp", tx_rx_pkt_stats.wm_pkt_process_dhcp);
	CALL_TX_RX_DROP_PKT_STAT(m, "wm_pkt_process_sip", tx_rx_pkt_stats.wm_pkt_process_sip);
	CALL_TX_RX_DROP_PKT_STAT(m, "wm_pkt_process_arp", tx_rx_pkt_stats.wm_pkt_process_arp);
	CALL_TX_RX_DROP_PKT_STAT(m, "sending_arp_pkt_up_stack", tx_rx_pkt_stats.sending_arp_pkt_up_stack);

	seq_printf(m, "\n*****Embeddedstudios and md-mac80211 packet drop stats***\n");
	for (i = 0; i < 141; i++) {
		CALL_TX_RX_DROP_PKT_STAT_T(m, "packet_drop", i, tx_rx_pkt_stats.packet_drop[i]);
	}
	if (mac_tx_rx_stats) {
		seq_printf(m, "\nmac80211 stats :\n");
		CALL_TX_RX_DROP_PKT_STAT(m, "free_tid_rx", mac_tx_rx_stats->free_tid_rx);
		CALL_TX_RX_DROP_PKT_STAT(m, "tkip_mic_test_sta_not_connected", mac_tx_rx_stats->tkip_mic_test_sta_not_connected);
		CALL_TX_RX_DROP_PKT_STAT(m, "tkip_mic_test_sta_iface_not_match", mac_tx_rx_stats->tkip_mic_test_sta_iface_not_match);
		CALL_TX_RX_DROP_PKT_STAT(m, "pkt_unknown_type", mac_tx_rx_stats->pkt_unknown_type);
		CALL_TX_RX_DROP_PKT_STAT(m, "build_beacon_success", mac_tx_rx_stats->build_beacon_success);
		CALL_TX_RX_DROP_PKT_STAT(m, "build_beacon_fail", mac_tx_rx_stats->build_beacon_fail);
		CALL_TX_RX_DROP_PKT_STAT(m, "skb_len_lt_fcs_len", mac_tx_rx_stats->skb_len_lt_fcs_len);
		CALL_TX_RX_DROP_PKT_STAT(m, "monitor_check_frame_control_drop", mac_tx_rx_stats->monitor_check_frame_control_drop);
		CALL_TX_RX_DROP_PKT_STAT(m, "monitor_should_drop_frame_drop", mac_tx_rx_stats->monitor_should_drop_frame_drop);
		CALL_TX_RX_DROP_PKT_STAT(m, "check_enough_space_for_radio_tap_hdr_drop", mac_tx_rx_stats->check_enough_space_for_radio_tap_hdr_drop);
		CALL_TX_RX_DROP_PKT_STAT(m, "monitor_dev_null_drop", mac_tx_rx_stats->monitor_dev_null_drop);
		CALL_TX_RX_DROP_PKT_STAT(m, "frame_out_of_seq_num_drop", mac_tx_rx_stats->frame_out_of_seq_num_drop);
		CALL_TX_RX_DROP_PKT_STAT(m, "frame_reorder_drop", mac_tx_rx_stats->frame_reorder_drop);
		CALL_TX_RX_DROP_PKT_STAT(m, "ps_poll_frame_queued", mac_tx_rx_stats->ps_poll_frame_queued);
		CALL_TX_RX_DROP_PKT_STAT(m, "rx_sta_process_frame", mac_tx_rx_stats->rx_sta_process_frame);
		CALL_TX_RX_DROP_PKT_STAT(m, "defragmented_frames", mac_tx_rx_stats->defragmented_frames);
		CALL_TX_RX_DROP_PKT_STAT(m, "deliver_skb_allignment_drop", mac_tx_rx_stats->deliver_skb_allignment_drop);
		CALL_TX_RX_DROP_PKT_STAT(m, "msdu_not_allowed_drop", mac_tx_rx_stats->msdu_not_allowed_drop);
		CALL_TX_RX_DROP_PKT_STAT(m, "rx_action_frame", mac_tx_rx_stats->rx_action_frame);
		CALL_TX_RX_DROP_PKT_STAT(m, "WM_mgmt_frame_processed_in_meshap_hook", mac_tx_rx_stats->WM_mgmt_frame_processed_in_meshap_hook);
		CALL_TX_RX_DROP_PKT_STAT(m, "mgmt_frame_sent_to_userspace", mac_tx_rx_stats->mgmt_frame_sent_to_userspace);
		CALL_TX_RX_DROP_PKT_STAT(m, "rx_action_return_frame", mac_tx_rx_stats->rx_action_return_frame);
		CALL_TX_RX_DROP_PKT_STAT(m, "unusable_frame_free", mac_tx_rx_stats->unusable_frame_free);
		CALL_TX_RX_DROP_PKT_STAT(m, "errored_frame_drop", mac_tx_rx_stats->errored_frame_drop);
		CALL_TX_RX_DROP_PKT_STAT(m, "non_consume_data_frame_drop", mac_tx_rx_stats->non_consume_data_frame_drop);
		CALL_TX_RX_DROP_PKT_STAT(m, "ack_skb_drop", mac_tx_rx_stats->ack_skb_drop);
		CALL_TX_RX_DROP_PKT_STAT(m, "tx_frame_not_sent_to_monitor_iface", mac_tx_rx_stats->tx_frame_not_sent_to_monitor_iface);
		CALL_TX_RX_DROP_PKT_STAT(m, "tx_status_headroom_too_small", mac_tx_rx_stats->tx_status_headroom_too_small);
		CALL_TX_RX_DROP_PKT_STAT(m, "tx_frame_to_monitor_iface", mac_tx_rx_stats->tx_frame_to_monitor_iface);
		CALL_TX_RX_DROP_PKT_STAT(m, "free_tx_skb", mac_tx_rx_stats->free_tx_skb);
		CALL_TX_RX_DROP_PKT_STAT(m, "tdls_frame_tx_fail", mac_tx_rx_stats->tdls_frame_tx_fail);
		CALL_TX_RX_DROP_PKT_STAT(m, "perged_ps_poll_frames", mac_tx_rx_stats->perged_ps_poll_frames);
		CALL_TX_RX_DROP_PKT_STAT(m, "old_ps_buffer_frame_drop", mac_tx_rx_stats->old_ps_buffer_frame_drop);
		CALL_TX_RX_DROP_PKT_STAT(m, "monitor_hw_queue_control_drop", mac_tx_rx_stats->monitor_hw_queue_control_drop);
		CALL_TX_RX_DROP_PKT_STAT(m, "tx_skb_short_len_drop", mac_tx_rx_stats->tx_skb_short_len_drop);
		CALL_TX_RX_DROP_PKT_STAT(m, "mac_tx_fail", mac_tx_rx_stats->mac_tx_fail);
		CALL_TX_RX_DROP_PKT_STAT(m, "tx_start_xmit_fail", mac_tx_rx_stats->tx_start_xmit_fail);
		CALL_TX_RX_DROP_PKT_STAT(m, "chanctx_conf_drop", mac_tx_rx_stats->chanctx_conf_drop);
		CALL_TX_RX_DROP_PKT_STAT(m, "buffered_bc_mc_frame_drop", mac_tx_rx_stats->buffered_bc_mc_frame_drop);
		CALL_TX_RX_DROP_PKT_STAT(m, "mgmt_tx_roc_drop", mac_tx_rx_stats->mgmt_tx_roc_drop);
		CALL_TX_RX_DROP_PKT_STAT(m, "tx_skb_tid", mac_tx_rx_stats->tx_skb_tid);
		CALL_TX_RX_DROP_PKT_STAT(m, "iface_work_skb", mac_tx_rx_stats->iface_work_skb);
		CALL_TX_RX_DROP_PKT_STAT(m, "free_ack_frame", mac_tx_rx_stats->free_ack_frame);
		CALL_TX_RX_DROP_PKT_STAT(m, "mesh_invalid_action", mac_tx_rx_stats->mesh_invalid_action);
		CALL_TX_RX_DROP_PKT_STAT(m, "mesh_path_move_to_que", mac_tx_rx_stats->mesh_path_move_to_que);
		CALL_TX_RX_DROP_PKT_STAT(m, "mesh_path_discardframe", mac_tx_rx_stats->mesh_path_discardframe);
		CALL_TX_RX_DROP_PKT_STAT(m, "mesh_plink_frame_tx_fail", mac_tx_rx_stats->mesh_plink_frame_tx_fail);
		CALL_TX_RX_DROP_PKT_STAT(m, "cfg_mgmt_tx_status", mac_tx_rx_stats->cfg_mgmt_tx_status);
		CALL_TX_RX_DROP_PKT_STAT(m, "unable_to_resolve_next_hop", mac_tx_rx_stats->unable_to_resolve_next_hop);
		CALL_TX_RX_DROP_PKT_STAT(m, "ctrl_frame", mac_tx_rx_stats->ctrl_frame);
		CALL_TX_RX_DROP_PKT_STAT(m, "rx_monitor_drop", mac_tx_rx_stats->rx_monitor_drop);
		CALL_TX_RX_DROP_PKT_STAT(m, "ieee80211_rx_drop", mac_tx_rx_stats->ieee80211_rx_drop);
		CALL_TX_RX_DROP_PKT_STAT(m, "tx_null_data_drop", mac_tx_rx_stats->tx_null_data_drop);
		CALL_TX_RX_DROP_PKT_STAT(m, "start_xmit_skb_shared", mac_tx_rx_stats->start_xmit_skb_shared);
		CALL_TX_RX_DROP_PKT_STAT(m, "start_xmit_clone_skb_free", mac_tx_rx_stats->start_xmit_clone_skb_free);
		CALL_TX_RX_DROP_PKT_STAT(m, "ack_skb_copy_free", mac_tx_rx_stats->ack_skb_copy_free);
		CALL_TX_RX_DROP_PKT_STAT(m, "ack_skb_free", mac_tx_rx_stats->ack_skb_free);
		CALL_TX_RX_DROP_PKT_STAT(m, "teardown_skb_free", mac_tx_rx_stats->teardown_skb_free);
		CALL_TX_RX_DROP_PKT_STAT(m, "mgmt_tx_ack_skb_free", mac_tx_rx_stats->mgmt_tx_ack_skb_free);
		CALL_TX_RX_DROP_PKT_STAT(m, "tdls_build_mgmt_pkt_fail", mac_tx_rx_stats->tdls_build_mgmt_pkt_fail);
		CALL_TX_RX_DROP_PKT_STAT(m, "tdls_skb", mac_tx_rx_stats->tdls_skb);
		CALL_TX_RX_DROP_PKT_STAT(m, "ack_skb_build_hdr", mac_tx_rx_stats->ack_skb_build_hdr);
		CALL_TX_RX_DROP_PKT_STAT(m, "ack_skb_shared_build_hdr", mac_tx_rx_stats->ack_skb_shared_build_hdr);
		CALL_TX_RX_DROP_PKT_STAT(m, "build_hdr_free", mac_tx_rx_stats->build_hdr_free);
		CALL_TX_RX_DROP_PKT_STAT(m, "xmit_fast_skb_clone_free", mac_tx_rx_stats->xmit_fast_skb_clone_free);
		CALL_TX_RX_DROP_PKT_STAT(m, "xmit_skb_resize_free", mac_tx_rx_stats->xmit_skb_resize_free);
		CALL_TX_RX_DROP_PKT_STAT(m, "xmit_fast_skb_txquued", mac_tx_rx_stats->xmit_fast_skb_txquued);
		CALL_TX_RX_DROP_PKT_STAT(m, "start_xmit_skb_len_is_small", mac_tx_rx_stats->start_xmit_skb_len_is_small);
		CALL_TX_RX_DROP_PKT_STAT(m, "non_linear_skb_frame", mac_tx_rx_stats->non_linear_skb_frame);
		CALL_TX_RX_DROP_PKT_STAT(m, "build_data_ra_sta_free", mac_tx_rx_stats->build_data_ra_sta_free);
		CALL_TX_RX_DROP_PKT_STAT(m, "build_data_select_key", mac_tx_rx_stats->build_data_select_key);
	}

	net_if = _ifs_list;
	while (net_if != NULL)
	{
		memset(&net_if_stats, 0, sizeof(al_net_if_stat_t));
		if ( (net_if->al_net_if.get_stats) && net_if->al_net_if.get_stats(&net_if->al_net_if, &net_if_stats) == 0)
		{
			seq_printf(m, "\n%s stats :\n", net_if->name);
			CALL_TX_RX_DROP_PKT_STAT(m, "rx_mgmt", net_if_stats.rx_mgmt);
			CALL_TX_RX_DROP_PKT_STAT(m, "rx_data", net_if_stats.rx_data);
		}
		net_if = net_if->next;
	}

	return 0;
}

static int tx_rx_drop_pkt_stats_proc_open(struct inode *inode, struct file *file)
{
   return single_open(file, _tx_rx_drop_pkt_stats_routine, NULL);
}

static ssize_t tx_rx_drop_pkt_stats_clear(struct file *file, const char __user *buffer, size_t count, void *data)
{
	memset(&tx_rx_pkt_stats, 0, sizeof(tx_rx_pkt_stats));

	return count;
}

static const struct file_operations _tx_rx_drop_pkt_stats_fops =
{
   .owner   = THIS_MODULE,
   .open    = tx_rx_drop_pkt_stats_proc_open,
   .read    = seq_read,
   .write	= tx_rx_drop_pkt_stats_clear,
   .llseek  = seq_lseek,
   .release = single_release,
};


#define MESH_NG_FSM_STATE_STOPPED             0
#define MESH_NG_FSM_STATE_FULL_SCAN           1
#define MESH_NG_FSM_STATE_ALTERNATE_SCAN      2
#define MESH_NG_FSM_STATE_RUNNING             3
#define MESH_NG_FSM_STATE_SAMPLE              4
#define MESH_NG_FSM_STATE_HEARTBEAT           5
#define MESH_NG_FSM_STATE_EVALUATE            6
#define MESH_NG_FSM_STATE_CHECK_PARENT        7
#define MESH_NG_FSM_STATE_UPLINK_DISABLE      8
#define MESH_NG_FSM_STATE_RADAR_MASTER        9
#define MESH_NG_FSM_STATE_EVALUATE_MOBILE     10
#define MESH_NG_FSM_STATE_EVALUATE_ADHOC      11
#define MESH_NG_FSM_STATE_STEP_SCAN           12
#define MESH_NG_FSM_STATE_ALT_SCAN_ADHOC      13
#define MESH_NG_FSM_STATE_STEP_SCAN_UPLINK    14
#define MESH_NG_FSM_STATE_UPLINK_BUFFER       15

#define DEBUG_ALLIGN		30
#define WLAN_PHY_MODE_2_STR(n)  case WLAN_PHY_MODE_ ## n: return #n
#define IW_MODE_2_STR(n)  case IW_MODE_ ## n: return #n
#define USE_TYPE_2_STR(n)  case AL_CONF_IF_USE_TYPE_ ## n: return #n
#define FSM_STATE_2_STR(n) case MESH_NG_FSM_STATE_ ## n: return #n
#define MESH_STATE_2_STR(n) case _MESH_STATE_ ## n: return #n
#define PHY_SUB_TYPE_2_STR(n)  case AL_CONF_IF_PHY_SUB_TYPE_  ## n: return #n
AL_DECLARE_GLOBAL_EXTERN(int mesh_state);

static const char *phy_mode_to_str(int state)
{
  switch (state){
  WLAN_PHY_MODE_2_STR(AUTO);
  WLAN_PHY_MODE_2_STR(802_11_A);
  WLAN_PHY_MODE_2_STR(802_11_B);
  WLAN_PHY_MODE_2_STR(802_11_G);
  WLAN_PHY_MODE_2_STR(802_11_FH);
  WLAN_PHY_MODE_2_STR(ATHEROS_TURBO);
  WLAN_PHY_MODE_2_STR(802_11_PURE_G);
  WLAN_PHY_MODE_2_STR(802_11_PSQ);
  WLAN_PHY_MODE_2_STR(802_11_PSH);
  WLAN_PHY_MODE_2_STR(802_11_PSF);
  WLAN_PHY_MODE_2_STR(802_11_AC);
  WLAN_PHY_MODE_2_STR(802_11_BGN);
  WLAN_PHY_MODE_2_STR(802_11_AN);
  WLAN_PHY_MODE_2_STR(802_11_ANAC);
  WLAN_PHY_MODE_2_STR(802_11_N_2_4_GHZ);
  WLAN_PHY_MODE_2_STR(802_11_N_5_GHZ);
  }
  return "unknown";
}

static const char *iw_mode_to_str(int state)
{
  switch (state) {
  IW_MODE_2_STR(AUTO);
  IW_MODE_2_STR(ADHOC);
  IW_MODE_2_STR(INFRA);
  IW_MODE_2_STR(MASTER);
  IW_MODE_2_STR(REPEAT);
  IW_MODE_2_STR(SECOND);
  IW_MODE_2_STR(MONITOR);
  IW_MODE_2_STR(MESH);
  }
  return "unknown";
}

static const char *use_type_to_str(int state)
{
  switch (state) {
  USE_TYPE_2_STR(DS);
  USE_TYPE_2_STR(WM);
  USE_TYPE_2_STR(PMON);
  USE_TYPE_2_STR(AMON);
  USE_TYPE_2_STR(AP);
  }
  return "unknown";
}

static const char *fsm_state_to_str(int state)
{
  switch (state) {
  FSM_STATE_2_STR(STOPPED);
  FSM_STATE_2_STR(FULL_SCAN);
  FSM_STATE_2_STR(ALTERNATE_SCAN);
  FSM_STATE_2_STR(RUNNING);
  FSM_STATE_2_STR(SAMPLE);
  FSM_STATE_2_STR(HEARTBEAT);
  FSM_STATE_2_STR(EVALUATE);
  FSM_STATE_2_STR(CHECK_PARENT);
  FSM_STATE_2_STR(UPLINK_DISABLE);
  FSM_STATE_2_STR(RADAR_MASTER);
  FSM_STATE_2_STR(EVALUATE_MOBILE);
  FSM_STATE_2_STR(EVALUATE_ADHOC);
  FSM_STATE_2_STR(STEP_SCAN);
  FSM_STATE_2_STR(ALT_SCAN_ADHOC);
  FSM_STATE_2_STR(STEP_SCAN_UPLINK);
  FSM_STATE_2_STR(UPLINK_BUFFER);
  }
  return "unknown";
}

static const char *mesh_state_to_str(int state)
{
   switch (state) {
   MESH_STATE_2_STR(NOT_STARTED);
   MESH_STATE_2_STR(DYNAMIC_CHANNEL_SCAN);
   MESH_STATE_2_STR(SEEKING_FINAL_ASSOCIATION);
   MESH_STATE_2_STR(RUNNING);
   MESH_STATE_2_STR(SEEKING_NEW_PARENT);
   MESH_STATE_2_STR(INITIAL_WDS_SCAN);
   MESH_STATE_2_STR(SEEKING_INITIAL_ASSOCIATION);
   MESH_STATE_2_STR(STOPPPING);
   MESH_STATE_2_STR(INIT_DS);
   MESH_STATE_2_STR(INIT_WM);
   }
   return "unknown";
}
static const char *phy_sub_type_to_str(int state)
{
   switch (state) {
   PHY_SUB_TYPE_2_STR(IGNORE);
   PHY_SUB_TYPE_2_STR(802_11_A);
   PHY_SUB_TYPE_2_STR(802_11_B);
   PHY_SUB_TYPE_2_STR(802_11_G);
   PHY_SUB_TYPE_2_STR(802_11_BG);
   PHY_SUB_TYPE_2_STR(802_11_PSQ);
   PHY_SUB_TYPE_2_STR(802_11_PSH);
   PHY_SUB_TYPE_2_STR(802_11_PSF);
   PHY_SUB_TYPE_2_STR(802_11_AC);
   PHY_SUB_TYPE_2_STR(802_11_BGN);
   PHY_SUB_TYPE_2_STR(802_11_AN);
   PHY_SUB_TYPE_2_STR(802_11_ANAC);
   PHY_SUB_TYPE_2_STR(802_11_N_2_4GHZ);
   PHY_SUB_TYPE_2_STR(802_11_N_5GHZ);
   }
   return "unknown";
}

static int beacon_vendor_info_parser(unsigned char *vendor_info_out, vendor_info_t  *ven_in)
{
   int ctc, crssi;
   int length = mesh_imcp_get_vendor_info_buffer(vendor_info_out, 0);
	unsigned char *p = vendor_info_out;
   memcpy(ven_in->packet, p, 4);
   ven_in->packet[4] = '\0';
   p += 4;
   ven_in->version_major = *p;
   p += 1;
   ven_in->version_minor = *p;
   p += 1;
   ven_in->meshid_length = *p;
   p += 1;
   memcpy(ven_in->meshid, p, ven_in->meshid_length);
   ven_in->meshid[ven_in->meshid_length] = '\0';
   p += ven_in->meshid_length;
   memcpy(ven_in->ds_mac.bytes, p, MAC_ADDR_LENGTH);
   p += MAC_ADDR_LENGTH;
   memcpy(&ctc, p, 4);
   ven_in->ctc = ctc;
   p += 4;
   ven_in->hpc = *p;
   p += 1;
   memcpy(&crssi, p, 4);
   ven_in->crssi = crssi;
   p += 4;
   ven_in->hbi = *p;
   p += 1;
   ven_in->ds_if_channel = *p;
   p += 1;
   ven_in->ds_if_sub_type = *p;
   p += 1;
   memcpy(ven_in->root_bssid.bytes, p, MAC_ADDR_LENGTH);
   p += MAC_ADDR_LENGTH;
   ven_in->mode = *p;
   p += 1;
   ven_in->phy_sub_type = *p;
   p += 1;
   return length;
}

static int _debug_info_routine(struct seq_file *m, void *v)
{
  debug_infra_info_t   debug_infra_info;
  meshap_core_net_if_t *net_if;
  net_if = _ifs_list;
  unsigned char vendor_info[256];
  int length;
  vendor_info_t  ven_in;
  while (net_if != NULL)
  {
     memset(&debug_infra_info, 0, sizeof(debug_infra_info_t));
     if (net_if->al_net_if.get_debug_info(&net_if->al_net_if, &debug_infra_info) == 0)
     {
        length = beacon_vendor_info_parser(vendor_info, &ven_in);
        seq_printf(m, "\n*************************************************************\n");
        seq_printf(m, "%*s", DEBUG_ALLIGN, net_if->name);
        seq_printf(m, "\n*************************************************************\n");
        seq_printf(m, "%-*s : %d\n", DEBUG_ALLIGN,   "current_essid_length", debug_infra_info.current_essid_length);
        seq_printf(m, "%-*s : %s\n", DEBUG_ALLIGN,	"current_essid", debug_infra_info.current_essid);
        seq_printf(m, "%-*s : %s\n", DEBUG_ALLIGN,	"current_phy_mode", phy_mode_to_str(debug_infra_info.current_phy_mode));
        seq_printf(m, "%-*s : %s\n", DEBUG_ALLIGN,	"iw_mode", iw_mode_to_str(debug_infra_info.iw_mode));
        seq_printf(m, "%-*s : %s\n", DEBUG_ALLIGN,	"use_type", use_type_to_str(debug_infra_info.use_type));
        seq_printf(m, "%-*s : %d\n", DEBUG_ALLIGN,	"beacon_conf_complete", debug_infra_info.beacon_conf_complete);
        seq_printf(m, "%-*s : %d\n", DEBUG_ALLIGN,	"channel_count", debug_infra_info.channel_count);
        seq_printf(m, "%-*s : %d\n", DEBUG_ALLIGN,	"beacon_interval", debug_infra_info.beacon_interval);
        seq_printf(m, "%-*s : %d\n", DEBUG_ALLIGN,	"rts_threshold", debug_infra_info.rts_threshold);
        seq_printf(m, "%-*s : %d\n", DEBUG_ALLIGN,	"frag_threshold", debug_infra_info.frag_threshold);
        seq_printf(m, "%-*s : %d\n", DEBUG_ALLIGN,	"tx_power", debug_infra_info.tx_power);
        seq_printf(m, "%-*s : %d\n", DEBUG_ALLIGN,	"sec_chan_offset", debug_infra_info.sec_chan_offset);
        seq_printf(m, "%-*s : %d\n", DEBUG_ALLIGN,	"scan_state", debug_infra_info.scan_state);
        seq_printf(m, "%-*s : %d\n", DEBUG_ALLIGN,	"country code", debug_infra_info.country_code);
        seq_printf(m, "%-*s : %s\n", DEBUG_ALLIGN,	"fsm_state", fsm_state_to_str(debug_infra_info.fsm_state));
        seq_printf(m, "%-*s : %s\n", DEBUG_ALLIGN,	"mesh_state", mesh_state_to_str(mesh_state));
        seq_printf(m, "%-*s : 0x%hX\n", DEBUG_ALLIGN,	"ht_capab_info", debug_infra_info.ht_capab_info);
        seq_printf(m, "%-*s : 0x%hX\n", DEBUG_ALLIGN,	"ht_opera", debug_infra_info.ht_opera);
        seq_printf(m, "%-*s : 0x%hX\n", DEBUG_ALLIGN,	"vht_capab_info", debug_infra_info.vht_capab_info);
        seq_printf(m, "%-*s : %lu\n", DEBUG_ALLIGN,	"scan_start_time", debug_infra_info.scan_start_time);
        seq_printf(m, "%-*s : %lu\n", DEBUG_ALLIGN,	"scan_completion_time", debug_infra_info.scan_completion_time);
        seq_printf(m, "%-*s : " AL_NET_ADDR_STR, DEBUG_ALLIGN, "current_bssid", AL_NET_ADDR_TO_STR(&debug_infra_info.current_bssid));
        seq_printf(m, "\n%-*s : %u\n", DEBUG_ALLIGN,	"connected", debug_infra_info.connected);
        seq_printf(m, "%-*s : 0x%hX\n", DEBUG_ALLIGN,	"a_mpdu_params", debug_infra_info.a_mpdu_params);
        seq_printf(m, "%-*s : %d\n", DEBUG_ALLIGN,	"W_Channel-number", debug_infra_info.current_channel.number);
        seq_printf(m, "%-*s : %u\n", DEBUG_ALLIGN,	"W_Channel-frequency(HZ)", debug_infra_info.current_channel.frequency);
        seq_printf(m, "%-*s : %u\n", DEBUG_ALLIGN,	"W_Channel-state_flags", debug_infra_info.current_channel.state_flags);
        seq_printf(m, "%-*s : %u\n", DEBUG_ALLIGN,	"W_Channel-max_power", debug_infra_info.current_channel.max_power);
        seq_printf(m, "%-*s : %u\n", DEBUG_ALLIGN,	"W_Channel-min_power", debug_infra_info.current_channel.min_power);
        seq_printf(m, "%-*s : %u\n", DEBUG_ALLIGN,	"vht_op_info_chwidth", debug_infra_info.vht_opera.vht_op_info_chwidth);
        seq_printf(m, "%-*s : %u\n", DEBUG_ALLIGN,	"vht_op_info_chan_center_freq_seg0_idx", debug_infra_info.vht_opera.vht_op_info_chan_center_freq_seg0_idx);
        seq_printf(m, "%-*s : %u\n", DEBUG_ALLIGN,	"vht_op_info_chan_center_freq_seg1_idx", debug_infra_info.vht_opera.vht_op_info_chan_center_freq_seg1_idx);
        seq_printf(m, "%-*s : %u\n", DEBUG_ALLIGN,	"vht_basic_mcs_set", debug_infra_info.vht_opera.vht_basic_mcs_set);
        if (debug_infra_info.use_type == AL_CONF_IF_USE_TYPE_WM)
        {
         seq_printf(m, "%-*s : %d\n", DEBUG_ALLIGN, "beacon_vendor_length", length);
         seq_printf(m, "%-*s :\n", DEBUG_ALLIGN, "beacon_vendor_info");
         seq_printf(m, "\t\t%-*s : %s\n", DEBUG_ALLIGN, "packet", ven_in.packet);
         seq_printf(m, "\t\t%-*s : %d\n", DEBUG_ALLIGN, "version_major", ven_in.version_major);
         seq_printf(m, "\t\t%-*s : %d\n", DEBUG_ALLIGN, "version_minor", ven_in.version_minor);
         seq_printf(m, "\t\t%-*s : %d\n", DEBUG_ALLIGN, "meshid_length", ven_in.meshid_length);
         seq_printf(m, "\t\t%-*s : %s\n", DEBUG_ALLIGN, "meshid", ven_in.meshid);
         seq_printf(m, "\t\t%-*s : %d\n", DEBUG_ALLIGN, "ctc", ven_in.ctc);
         seq_printf(m, "\t\t%-*s : %d\n", DEBUG_ALLIGN, "hpc", ven_in.hpc);
         seq_printf(m, "\t\t%-*s : %d\n", DEBUG_ALLIGN, "crssi", ven_in.crssi);
         seq_printf(m, "\t\t%-*s : %d\n", DEBUG_ALLIGN, "hbi", ven_in.hbi);
         seq_printf(m, "\t\t%-*s : %d\n", DEBUG_ALLIGN, "ds_if_channel", ven_in.ds_if_channel);
         seq_printf(m, "\t\t%-*s : %s\n", DEBUG_ALLIGN, "ds_if_sub_type", phy_sub_type_to_str(ven_in.ds_if_sub_type));
         seq_printf(m, "\t\t%-*s : " AL_NET_ADDR_STR, DEBUG_ALLIGN, "ds_mac_bytes", AL_NET_ADDR_TO_STR(&ven_in.ds_mac));
         seq_printf(m, "\n\t\t%-*s : " AL_NET_ADDR_STR, DEBUG_ALLIGN, "root_bssid_bytes", AL_NET_ADDR_TO_STR(&ven_in.root_bssid));
         seq_printf(m, "\n\t\t%-*s : %d\n", DEBUG_ALLIGN, "mode", ven_in.mode);
         seq_printf(m, "\t\t%-*s : %s\n", DEBUG_ALLIGN, "phy_sub_type", phy_sub_type_to_str(ven_in.phy_sub_type));
       }
       else if (debug_infra_info.use_type == AL_CONF_IF_USE_TYPE_DS && current_parent)
       {
         seq_printf(m, "%-*s :\n", DEBUG_ALLIGN, "parent beacon_vendor_info");
         seq_printf(m, "\t\t%-*s : %d\n", DEBUG_ALLIGN, "crssi", current_parent->crssi);
         seq_printf(m, "\t\t%-*s : %d\n", DEBUG_ALLIGN, "hbi", current_parent->hbi);
         seq_printf(m, "\t\t%-*s : %d\n", DEBUG_ALLIGN, "hpc", current_parent->hpc);
         seq_printf(m, "\t\t%-*s : %d\n", DEBUG_ALLIGN, "ds_channel", current_parent->ds_channel);
         seq_printf(m, "\t\t%-*s : %s\n", DEBUG_ALLIGN, "ds_sub_type", phy_sub_type_to_str(current_parent->ds_type));
         seq_printf(m, "\t\t%-*s : %d\n", DEBUG_ALLIGN, "mode", current_parent->mode);
         seq_printf(m, "\t\t%-*s : %s\n", DEBUG_ALLIGN, "phy_sub_type", phy_sub_type_to_str(current_parent->phy_sub_type));
         seq_printf(m, "\t\t%-*s : " AL_NET_ADDR_STR, DEBUG_ALLIGN, "root_bssid_bytes", AL_NET_ADDR_TO_STR(&current_parent->root_bssid));
        }
      }
      net_if = net_if->next;
    }
    return 0;
}

static int debug_info_stats_proc_open(struct inode *inode, struct file *file)
{
   return single_open(file, _debug_info_routine, NULL);
}

static ssize_t debug_info_stats_clear(struct file *file, const char __user *buffer, size_t count, void *data)
{
   debug_infra_info_t  debug_infra_info;
   memset(&debug_infra_info, 0, sizeof(debug_infra_info_t));
   return count;
}

static const struct file_operations _debug_info_stats_fops =
{
   .owner  = THIS_MODULE,
   .open   = debug_info_stats_proc_open,
   .read   = seq_read,
   .write  = debug_info_stats_clear,
   .llseek = seq_lseek,
   .release = single_release,
};

#define ALIGN 20
static int _tx_rx_routine(struct seq_file *m, void *v)
{
  meshap_core_net_if_t *net_if;
  al_net_if_stat_t net_if_stats;
  net_if = _ifs_list;
  while (net_if != NULL) {
    memset(&net_if_stats, 0, sizeof(al_net_if_stat_t));
     if ((net_if->al_net_if.get_stats) && net_if->al_net_if.get_stats(&net_if->al_net_if, &net_if_stats) == 0) {
         seq_printf(m, "\n*************************************************************\n");
         seq_printf(m, "%*s", DEBUG_ALLIGN, net_if->name);
         seq_printf(m, "\n*************************************************************\n");
         seq_printf(m, "\nTransmitted  and Recieved Mangement Frames\n");
         seq_printf(m, "%-*s : %-*s%-*s\n" ,ALIGN, "Frame Type", ALIGN, "Tx" , ALIGN, "Rx");
         seq_printf(m, "%-*s : %-*lu%-*lu\n", ALIGN, "assoc_req", ALIGN, net_if_stats.total_tx_mgmt[IEEE80211_STYPE_ASSOC_REQ], 
                                                                   ALIGN, net_if_stats.total_rx_mgmt[WLAN_FC_STYPE_ASSOC_REQ]);
         seq_printf(m, "%-*s : %-*lu%-*lu\n", ALIGN, "assoc_resp", ALIGN, net_if_stats.total_tx_mgmt[IEEE80211_STYPE_ASSOC_RESP],
                                                                    ALIGN, net_if_stats.total_rx_mgmt[WLAN_FC_STYPE_ASSOC_RESP]);
         seq_printf(m, "%-*s : %-*lu%-*lu\n", ALIGN, "reassoc_req", ALIGN, net_if_stats.total_tx_mgmt[IEEE80211_STYPE_REASSOC_REQ],
                                                                     ALIGN, net_if_stats.total_rx_mgmt[WLAN_FC_STYPE_REASSOC_REQ]);
         seq_printf(m, "%-*s : %-*lu%-*lu\n", ALIGN, "reassoc_resp", ALIGN, net_if_stats.total_tx_mgmt[IEEE80211_STYPE_REASSOC_RESP],
                                                                      ALIGN, net_if_stats.total_rx_mgmt[WLAN_FC_STYPE_REASSOC_RESP]);
         seq_printf(m, "%-*s : %-*lu%-*lu\n", ALIGN, "probe_req", ALIGN, net_if_stats.total_tx_mgmt[IEEE80211_STYPE_PROBE_REQ],
                                                                   ALIGN, net_if_stats.total_rx_mgmt[WLAN_FC_STYPE_PROBE_REQ]);
         seq_printf(m, "%-*s : %-*lu%-*lu\n", ALIGN, "probe_resp", ALIGN, net_if_stats.total_tx_mgmt[IEEE80211_STYPE_PROBE_RESP],
                                                                   ALIGN, net_if_stats.total_rx_mgmt[WLAN_FC_STYPE_PROBE_RESP]);
         seq_printf(m, "%-*s : %-*lu%-*lu\n", ALIGN, "beacon", ALIGN, net_if_stats.total_tx_mgmt[IEEE80211_STYPE_BEACON],
                                                               ALIGN, net_if_stats.total_rx_mgmt[WLAN_FC_STYPE_BEACON]);
         seq_printf(m, "%-*s : %-*lu%-*lu\n", ALIGN, "atim", ALIGN, net_if_stats.total_tx_mgmt[IEEE80211_STYPE_ATIM],
                                                             ALIGN, net_if_stats.total_rx_mgmt[WLAN_FC_STYPE_ATIM]);
         seq_printf(m, "%-*s : %-*lu%-*lu\n", ALIGN, "disassoc", ALIGN, net_if_stats.total_tx_mgmt[IEEE80211_STYPE_DISASSOC],
                                                                 ALIGN, net_if_stats.total_rx_mgmt[WLAN_FC_STYPE_DISASSOC]);
         seq_printf(m, "%-*s : %-*lu%-*lu\n", ALIGN, "auth", ALIGN, net_if_stats.total_tx_mgmt[IEEE80211_STYPE_AUTH],
                                                             ALIGN, net_if_stats.total_rx_mgmt[WLAN_FC_STYPE_AUTH]);
         seq_printf(m, "%-*s : %-*lu%-*lu\n", ALIGN, "deauth", ALIGN, net_if_stats.total_tx_mgmt[IEEE80211_STYPE_DEAUTH],
                                                               ALIGN, net_if_stats.total_rx_mgmt[WLAN_FC_STYPE_DEAUTH]);
         seq_printf(m, "%-*s : %-*lu%-*lu\n", ALIGN, "action", ALIGN, net_if_stats.total_tx_mgmt[IEEE80211_STYPE_ACTION],
                                                               ALIGN, net_if_stats.total_rx_mgmt[WLAN_FC_STYPE_ACTION]);
         seq_printf(m, "\nTransmitted  and Recieved Data Frames\n");
         seq_printf(m, "%-*s : %-*s%-*s\n" ,ALIGN, "Frame Type", ALIGN, "Tx" , ALIGN, "Rx");
         seq_printf(m, "%-*s : %-*lu%-*lu\n", ALIGN, "data", ALIGN, net_if_stats.total_tx_data[IEEE80211_STYPE_DATA],
                                                             ALIGN, net_if_stats.total_rx_data[WLAN_FC_STYPE_DATA]);
         seq_printf(m, "%-*s : %-*lu%-*lu\n", ALIGN, "data_cfack", ALIGN, net_if_stats.total_tx_data[IEEE80211_STYPE_DATA_CFACK],
                                                                   ALIGN, net_if_stats.total_rx_data[WLAN_FC_STYPE_DATA_CFACK]);
         seq_printf(m, "%-*s : %-*lu%-*lu\n", ALIGN, "data_cfpoll", ALIGN, net_if_stats.total_tx_data[IEEE80211_STYPE_DATA_CFPOLL],
                                                                    ALIGN, net_if_stats.total_rx_data[WLAN_FC_STYPE_DATA_CFPOLL]);
         seq_printf(m, "%-*s : %-*lu%-*lu\n", ALIGN, "data_cfackpoll", ALIGN, net_if_stats.total_tx_data[IEEE80211_STYPE_DATA_CFACKPOLL],
                                                                       ALIGN, net_if_stats.total_rx_data[WLAN_FC_STYPE_DATA_CFACKPOLL]);
         seq_printf(m, "%-*s : %-*lu%-*lu\n", ALIGN, "null_func", ALIGN, net_if_stats.total_tx_data[IEEE80211_STYPE_NULLFUNC],
                                                                  ALIGN, net_if_stats.total_rx_data[WLAN_FC_STYPE_NULLFUNC]);
         seq_printf(m, "%-*s : %-*lu%-*lu\n", ALIGN, "cf_ack", ALIGN, net_if_stats.total_tx_data[IEEE80211_STYPE_CFACK],
                                                               ALIGN, net_if_stats.total_rx_data[WLAN_FC_STYPE_CFACK]);
         seq_printf(m, "%-*s : %-*lu%-*lu\n", ALIGN, "cf_poll", ALIGN, net_if_stats.total_tx_data[IEEE80211_STYPE_CFPOLL],
                                                                ALIGN, net_if_stats.total_rx_data[WLAN_FC_STYPE_CFPOLL]);
         seq_printf(m, "%-*s : %-*lu%-*lu\n", ALIGN, "cf_ack_poll", ALIGN, net_if_stats.total_tx_data[IEEE80211_STYPE_CFACKPOLL],
                                                                    ALIGN, net_if_stats.total_rx_data[WLAN_FC_STYPE_CFACKPOLL]);
         seq_printf(m, "%-*s : %-*lu%-*lu\n", ALIGN, "qos_data", ALIGN, net_if_stats.total_tx_data[IEEE80211_STYPE_QOS_DATA],
                                                                 ALIGN, net_if_stats.total_rx_data[WLAN_FC_STYPE_QOS]);
         seq_printf(m, "%-*s : %-*lu\n", ALIGN, "qos_datacfack", ALIGN, net_if_stats.total_tx_data[IEEE80211_STYPE_QOS_DATA_CFACK]);
         seq_printf(m, "%-*s : %-*lu\n", ALIGN, "qos_datacfpoll", ALIGN, net_if_stats.total_tx_data[IEEE80211_STYPE_QOS_DATA_CFPOLL]);
         seq_printf(m, "%-*s : %-*lu\n", ALIGN, "qos_datacfackpoll", ALIGN, net_if_stats.total_tx_data[IEEE80211_STYPE_QOS_DATA_CFACKPOLL]);
         seq_printf(m, "%-*s : %-*lu\n", ALIGN, "qos_nullfunc", ALIGN, net_if_stats.total_tx_data[IEEE80211_STYPE_QOS_NULLFUNC]);
         seq_printf(m, "%-*s : %-*lu\n", ALIGN, "qos_cfack", ALIGN, net_if_stats.total_tx_data[IEEE80211_STYPE_QOS_CFACK]);
         seq_printf(m, "%-*s : %-*lu\n", ALIGN, "qos_cfpoll", ALIGN, net_if_stats.total_tx_data[IEEE80211_STYPE_QOS_CFPOLL]);
         seq_printf(m, "%-*s : %-*lu\n", ALIGN, "qos_cfackpoll", ALIGN, net_if_stats.total_tx_data[IEEE80211_STYPE_QOS_CFACKPOLL]);
      }
      net_if = net_if->next;
   }
   return 0;
}

static int tx_rx_proc_open(struct inode *inode, struct file *file)
{
   return single_open(file, _tx_rx_routine, NULL);
}

static const struct file_operations _tx_rx_fops =
{
   .owner  = THIS_MODULE,
   .open   = tx_rx_proc_open,
   .read   = seq_read,
   .llseek = seq_lseek,
   .release = single_release,
};


static void _meshap_mesh_proc_kap_list_initialize(void)
{
   meshap_mesh_proc_add_read_entry("kap", &_kap_list_fops, NULL);
   meshap_mesh_proc_add_read_entry("hkap", &_hkap_list_fops, NULL);
   meshap_mesh_proc_add_read_entry("mobwin", &_mobwin_list_fops, NULL);
   meshap_mesh_proc_add_read_entry("adhoc", &_adhoc_list_fops, NULL);
   meshap_mesh_proc_add_read_entry("adhoc_mode", &_adhoc_mode_fops, NULL);
#if MESH_CPU_PROFILE_ENABLED
   meshap_mesh_proc_add_read_entry("profiling_stats", &profiling_stats_fops, NULL);
#endif
   meshap_mesh_proc_add_read_entry("version", &version_fops, NULL);
   meshap_mesh_proc_add_read_entry("thread_stats", &_thread_stats_fops, NULL);
   meshap_mesh_proc_add_read_entry("queue_packet_stats", &_queue_packet_stats_fops, NULL);
   meshap_mesh_proc_add_read_entry("tx_rx_drop_pkt_stats", &_tx_rx_drop_pkt_stats_fops, NULL);
   meshap_mesh_proc_add_read_write_entry("debug_log_level", &debug_proc_fops, NULL);
   meshap_mesh_proc_add_read_entry("debug_info", &_debug_info_stats_fops, NULL);
   meshap_mesh_proc_add_read_entry("tx_rx_stats", &_tx_rx_fops, NULL);
}
