/********************************************************************************
* MeshDynamics
* --------------
* File     : meshap_ap_proc.c
* Comments : Meshap AP /proc interface
* Created  : 6/18/2005
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  9  |6/20/2007 |  Proc entries for multicast stats added         |Prachiti|
* -----------------------------------------------------------------------------
* |  8  |6/11/2007 | Key Index added to display                      | Sriram |
* -----------------------------------------------------------------------------
* |  7  |1/13/2007 | Proc entries for VLAN stats added               | Sriram |
* -----------------------------------------------------------------------------
* |  6  |11/8/2006 | Changes for compression support                 | Sriram |
* -----------------------------------------------------------------------------
* |  5  |5/30/2006 | Macro usage corrected                           | Sriram |
* -----------------------------------------------------------------------------
* |  4  |3/13/2006 | Added Ref count in display                      | Sriram |
* -----------------------------------------------------------------------------
* |  3  |02/01/2006| sta-list formatting changed		              | Abhijit|
* -----------------------------------------------------------------------------
* |  2  |01/04/2006| dot1p queue information displayed               | Bindu  |
* -----------------------------------------------------------------------------
* |  1  |9/15/2005 | More information displayed                      | Sriram |
* -----------------------------------------------------------------------------
* |  0  |6/18/2005 | Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

#include <generated/autoconf.h>
#include <linux/version.h>
#include <linux/module.h>
#include <linux/init.h>
#include <linux/if.h>
#include <linux/netdevice.h>
#include <linux/skbuff.h>
#include <linux/cache.h>
#include <linux/pci.h>
#include <linux/wireless.h>
#include <asm/uaccess.h>
#include <net/iw_handler.h>
#include <linux/delay.h>
#include <linux/seq_file.h>
#include "al.h"
#include "al_impl_context.h"
#include "al_802_11.h"
#include "meshap_ap_proc.h"
#include "access_point.h"
#include "access_point_ext.h"
#include "meshap_hproc_format.h"

struct _sta_list_data
{
   char *p;
};

typedef struct _sta_list_data   _sta_list_data_t;

static struct proc_dir_entry *_meshap_ap_proc_entry = NULL;

static void _meshap_ap_proc_sta_initialize(void);
static void _meshap_ap_proc_queue_stat_initialize(void);
static void _meshap_ap_proc_vlan_stat_initialize(void);
static void _meshap_ap_proc_mcast_stat_initialize(void);

static int _sta_list_routine(struct seq_file *m, void *v);
static int _hsta_list_routine(struct seq_file *m, void *v);

static int _queue_stat_routine(struct seq_file *m, void *v);
static int _hqueue_stat_routine(struct seq_file *m, void *v);

static int _downlink_stat_routine(struct seq_file *m, void *v);
static int _vlan_stat_routine(struct seq_file *m, void *v);

static void _sta_enum_routine(void *callback, const access_point_sta_info_t *sta);
static void _hsta_enum_routine(void *callback, const access_point_sta_info_t *sta);


static int _mcast_stat_routine(struct seq_file *m, void *v);

int meshap_ap_proc_initialize(void)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
   _meshap_ap_proc_entry = meshap_proc_mkdir("access-point");
   _meshap_ap_proc_sta_initialize();
   _meshap_ap_proc_queue_stat_initialize();
   _meshap_ap_proc_vlan_stat_initialize();
   _meshap_ap_proc_mcast_stat_initialize();
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
   return 0;
}


void meshap_ap_proc_add_read_entry(const char *name, struct file_operations *fops, void *data)
{
   //create_proc_read_entry(name,0,_meshap_ap_proc_entry,routine,data);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
   proc_create_data(name, 0, _meshap_ap_proc_entry, fops, data);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
}


//static int _sta_list_routine(char *page, char **start, off_t off,int count, int *eof, void *data)
static int _sta_list_routine(struct seq_file *m, void *v)
{
      seq_printf(m,
              "--------------------------------------------------------------------------------\r\n"
              "STATION ADDR     |IFNAME|VTAG|SIG|RATE|LAST PACKET RX|REFCOUNT|ACL|BUF|COMP| KI|\r\n"
              "--------------------------------------------------------------------------------\r\n");

   access_point_ext_enumerate_stas((void *)m, _sta_enum_routine);
}


static void _sta_enum_routine(void *callback, const access_point_sta_info_t *sta)
{
   struct seq_file *m;
   unsigned short  vlan_tag;

   m  = (struct seq_file *)callback;

   vlan_tag = 0;

   if ((sta->vlan_info != NULL) &&
       (sta->vlan_info->vlan_tag != AL_PACKET_VLAN_TAG_TYPE_UNTAGGED))
   {
      vlan_tag = sta->vlan_info->vlan_tag;
   }

   seq_printf(m,
              AL_NET_ADDR_STR "|%6s|%04d|%03d|%04d|%014llu|%08d|%3s|%3s|%4s|%03d|\r\n",
              AL_NET_ADDR_TO_STR(&sta->address),
              sta->net_if->name,
              vlan_tag,
              sta->rssi,
              sta->transmit_bit_rate,
              sta->last_packet_received,
              AL_ATOMIC_GET(sta->ref_count),
              (sta->auth_state & ACCESS_POINT_STA_AUTH_STATE_ACL) ? "Yes" : "No ",
              (sta->auth_state & ACCESS_POINT_STA_AUTH_STATE_BUFFERING) ? "Yes" : "No ",
              (sta->auth_state & ACCESS_POINT_STA_AUTH_STATE_COMPRESSION) ? "Yes" : "No ",
              (sta->auth_state & ACCESS_POINT_STA_AUTH_STATE_ENCRYPTION) ? sta->key_index : 0);
}


static int _sta_proc_open(struct inode *inode, struct file *file)
{
   return single_open(file, _sta_list_routine, NULL);
}


static const struct file_operations _sta_fops =
{
   .owner   = THIS_MODULE,
   .open    = _sta_proc_open,
   .read    = seq_read,
   .llseek  = seq_lseek,
   .release = single_release,
};


//static int _hsta_list_routine(char *page, char **start, off_t off,int count, int *eof, void *data)
static int _hsta_list_routine(struct seq_file *m, void *v)
{
    TABLE_START(m);
   TABLE_ROW_HEAD_START(m);
   TABLE_ROW_HEAD(m, "Address");
   TABLE_ROW_HEAD(m, "Interface");
   TABLE_ROW_HEAD(m, "Signal");
   TABLE_ROW_HEAD(m, "Rate");
   TABLE_ROW_HEAD(m, "ACL");
   TABLE_ROW_HEAD_END(m);

   access_point_ext_enumerate_stas((void *)m, _hsta_enum_routine);

   TABLE_END(m);
}


static void _hsta_enum_routine(void *callback, const access_point_sta_info_t *sta)
{
      struct seq_file *m;

   m = (struct seq_file *)callback;

   TABLE_ROW_DATA_START(m);
   TABLE_ROW_DATA_MAC(m, AL_NET_ADDR_TO_STR(&sta->address));
   TABLE_ROW_DATA_STR(m, sta->net_if->name);
   TABLE_ROW_DATA_INT(m, sta->rssi);
   TABLE_ROW_DATA_INT(m, sta->transmit_bit_rate);
   TABLE_ROW_DATA_STR(m, (sta->auth_state & ACCESS_POINT_STA_AUTH_STATE_ACL) ? "Yes" : "No");
   TABLE_ROW_DATA_END(m);
}


static int _hsta_proc_open(struct inode *inode, struct file *file)
{
   return single_open(file, _hsta_list_routine, NULL);
}


static const struct file_operations _hsta_fops =
{
   .owner   = THIS_MODULE,
   .open    = _hsta_proc_open,
   .read    = seq_read,
   .llseek  = seq_lseek,
   .release = single_release,
};

static void _meshap_ap_proc_sta_initialize(void)
{
   meshap_ap_proc_add_read_entry("sta-list", &_sta_fops, NULL);
   meshap_ap_proc_add_read_entry("hsta-list", &_hsta_fops, NULL);
}


//static int _queue_stat_routine(char *page, char **start, off_t off,int count, int *eof, void *data)
static int _queue_stat_routine(struct seq_file *m, void *v)
{
      access_point_ext_print_queue_stat(m);
}


static int _queue_stat_proc_open(struct inode *inode, struct file *file)
{
   return single_open(file, _queue_stat_routine, NULL);
}


static const struct file_operations _queue_stat_fops =
{
   .owner   = THIS_MODULE,
   .open    = _queue_stat_proc_open,
   .read    = seq_read,
   .llseek  = seq_lseek,
   .release = single_release,
};

//static int _hqueue_stat_routine(char *page, char **start, off_t off,int count, int *eof, void *data)
static int _hqueue_stat_routine(struct seq_file *m, void *v)
{
      access_point_ext_print_hqueue_stat(m);
}


static int _hqueue_stat_proc_open(struct inode *inode, struct file *file)
{
   return single_open(file, _hqueue_stat_routine, NULL);
}


static const struct file_operations _hqueue_stat_fops =
{
   .owner   = THIS_MODULE,
   .open    = _hqueue_stat_proc_open,
   .read    = seq_read,
   .llseek  = seq_lseek,
   .release = single_release,
};

static void _meshap_ap_proc_queue_stat_initialize(void)
{
   meshap_ap_proc_add_read_entry("queue-stat", &_queue_stat_fops, NULL);
   meshap_ap_proc_add_read_entry("hqueue-stat", &_hqueue_stat_fops, NULL);
}


//static int _downlink_stat_routine(char *page, char **start, off_t off,int count, int *eof, void *data)
static int _downlink_stat_routine(struct seq_file *m, void *v)
{
      access_point_ext_print_wm_info(m);
}


static int _downlink_stat_proc_open(struct inode *inode, struct file *file)
{
   return single_open(file, _downlink_stat_routine, NULL);
}


static const struct file_operations _downlink_stat_fops =
{
   .owner   = THIS_MODULE,
   .open    = _downlink_stat_proc_open,
   .read    = seq_read,
   .llseek  = seq_lseek,
   .release = single_release,
};

//static int _vlan_stat_routine(char *page, char **start, off_t off,int count, int *eof, void *data)
static int _vlan_stat_routine(struct seq_file *m, void *v)
{
      access_point_ext_print_vlan_info(m);
}


static int _vlan_stat_proc_open(struct inode *inode, struct file *file)
{
   return single_open(file, _vlan_stat_routine, NULL);
}


static const struct file_operations _vlan_stat_fops =
{
   .owner   = THIS_MODULE,
   .open    = _vlan_stat_proc_open,
   .read    = seq_read,
   .llseek  = seq_lseek,
   .release = single_release,
};

static void _meshap_ap_proc_vlan_stat_initialize(void)
{
   meshap_ap_proc_add_read_entry("downlink-stat", &_downlink_stat_fops, NULL);
   meshap_ap_proc_add_read_entry("vlan-stat", &_vlan_stat_fops, NULL);
}


//static int _mcast_stat_routine(char *page, char **start, off_t off,int count, int *eof, void *data)
static int _mcast_stat_routine(struct seq_file *m, void *v)
{
      access_point_ext_print_mcast_stas(m);
}


static int _mcast_stat_proc_open(struct inode *inode, struct file *file)
{
   return single_open(file, _mcast_stat_routine, NULL);
}


static const struct file_operations _mcast_stat_fops =
{
   .owner   = THIS_MODULE,
   .open    = _mcast_stat_proc_open,
   .read    = seq_read,
   .llseek  = seq_lseek,
   .release = single_release,
};

static void _meshap_ap_proc_mcast_stat_initialize(void)
{
   meshap_ap_proc_add_read_entry("mcast-stat", &_mcast_stat_fops, NULL);
}
