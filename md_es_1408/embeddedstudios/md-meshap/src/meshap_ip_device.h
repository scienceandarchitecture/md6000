/********************************************************************************
* MeshDynamics
* --------------
* File     : meshap_ip_device.h
* Comments : Meshap Virtual Interface for Internet Protocol
* Created  : 11/15/2004
* Author   : Abhijit Ayarekar
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  1  |8/20/2007 | meshap_ip_device_get_info Added                 | Sriram |
* -----------------------------------------------------------------------------
* |  0  |11/15/2004| Created.                                        | Abhijit |
* -----------------------------------------------------------------------------
********************************************************************************/

#ifndef __MESHAP_IP_DEVICE_H__
#define __MESHAP_IP_DEVICE_H__

#ifndef MESHAP_IP_DEVICE_NAME_TEMPLATE
#define MESHAP_IP_DEVICE_NAME_TEMPLATE    "mip%d"
#endif

int meshap_ip_device_initialize(void);
int meshap_ip_device_set_net_if(meshap_core_net_if_t *core_net_if);
int meshap_ip_device_get_info(al_packet_pool_info_t *info);
int meshap_ip_device_uninitialize(void);

#endif /*__MESHAP_IP_DEVICE_H__*/
