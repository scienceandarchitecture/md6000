/********************************************************************************
* MeshDynamics
* --------------
* File     : meshap_al_test.c
* Comments : Torna MeshAP Abstraction Layer Test Suite
* Created  : 5/21/2004
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  6  |11/14/2006| Changes for heap debug support                  | Sriram |
* -----------------------------------------------------------------------------
* |  5  |4/14/2005 | Added tests 13_1 and 14_1                       | Sriram |
* -----------------------------------------------------------------------------
* |  4  |4/6/2005  |  Added test 12_1                                | Sriram |
* -----------------------------------------------------------------------------
* |  3  |7/23/2004 | Added test 11_1                                 | Sriram |
* -----------------------------------------------------------------------------
* |  2  |6/10/2004 | Added dis_assoc test case                       | Sriram |
* -----------------------------------------------------------------------------
* |  1  |6/5/2004  | Misc changes + Added more tests                 | Sriram |
* -----------------------------------------------------------------------------
* |  0  |5/21/2004 | Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

#include <generated/autoconf.h>

#include <linux/module.h>
#include <linux/init.h>
#include <linux/version.h>
#include <linux/netdevice.h>
#include <linux/skbuff.h>
#include <linux/wireless.h>
#include <linux/string.h>
#include "al.h"
#include "al_802_11.h"
#include "meshap.h"
#include "meshap_core.h"
#include "al_packet_impl.h"
#include "torna_types.h"
#include "torna_mac_hdr.h"

#include "dot11i.h"
#include "dot11i-defs.h"

int dot11i_rc4_encrypt(unsigned char *cipher_text,
                       unsigned char *plain_text,
                       int           plain_text_length,
                       unsigned char *key,
                       int           key_len,
                       unsigned char *key_skip,
                       int           key_skip_len);

struct _test_data
{
   int event_handle;
   int semaphore_handle;
};

typedef struct _test_data   _test_data_t;

static const unsigned char _test_5_1_data[] =
{
   0x45,
   0x0,
   0x45,  0x9,
   0xB,   0x0,
   0x0,   0x0,
   0x40,
   0x11,
   0x00, 0x00,
   0x7F,  0x0,  0x0,  0x1,
   0xFF, 0xFF, 0xFF, 0xFF,

   0xDE, 0xDE,
   0xDE, 0xDE,
   0x2F,  0x0,
   0xE6, 0x9D,

   0x49, 0x4D, 0x43, 0x50,
   0x1,
   0x0,  0xE6, 0xD4, 0x59, 0xD6, 0x7B,
   0xB,   0x0,  0x0,  0x0,
   0x0,   0x0,  0x0,  0x0,
   0x0,
   0x0,  0xE6, 0xD4, 0x59, 0xD6, 0x7B,
   0x0,   0x0,  0x0,  0x0,
   0xFF,
   0xF,
   0x0,  0xE6, 0xD4, 0x59, 0xD6, 0x7B,
   0x0
};

static const al_net_addr_t _test_5_1_addresses[] =
{
   { { 0x00, 0x50, 0x56, 0xC0, 0x00, 0x08 }, 6 },
   { { 0x00, 0x50, 0x56, 0xC0, 0x00, 0x09 }, 6 },
   { { 0x00, 0x50, 0x56, 0xC0, 0x00, 0x0A }, 6 },
   { { 0x00, 0x50, 0x56, 0xC0, 0x00, 0x0B }, 6 }
};

static const char *_mode_names[] =
{
   "AL_802_11_MODE_AUTO",
   "AL_802_11_MODE_ADHOC",
   "AL_802_11_MODE_INFRA",
   "AL_802_11_MODE_MASTER",
   "AL_802_11_MODE_REPEAT",
   "AL_802_11_MODE_SECOND",
   "AL_802_11_MODE_RADIO_OFF"
};

static al_net_addr_t _test_6_1_ap_address[] =
{
   { { 0x00, 0x0C, 0x41, 0xF1, 0xCE, 0xF2 }, 6 },
   { { 0x00, 0x30, 0xBD, 0x8F, 0xF1, 0xF8 }, 6 }
};

static const unsigned char _test_8_1_data[] =
{
   /** IP HEADER 20 bytes */
   0x45, 0x00, 0x00, 0x43, 0x69, 0x1C, 0x00, 0x00, 0x80, 0x11, 0xC7, 0x84,
   0x0A, 0x00, 0x00, 0x0A, /* SRC IP ADDRESS */
   0xFF, 0xFF, 0xFF, 0xFF, /* DST IP ADDRESS */
   /** UDP HEADER 8 bytes*/
   0x0E, 0x8B,
   0xFE, 0xFE,
   0x00, 0x2F,
   0x48, 0x21,
   /** UDP DATA */
   0x49, 0x4D, 0x43, 0x50, 0x01, 0x00,
   0xE6, 0xD4, 0x59, 0xD6, 0x7B, 0x0B,
   0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
   0x00, 0x00, 0x00, 0xE6, 0xD4, 0x59,
   0xD6, 0x7B, 0x00, 0x00, 0x00, 0x00,
   0xFF, 0x0F, 0x00, 0xE6, 0xD4, 0x59,
   0xD6, 0x7B, 0x00,
};

static al_net_addr_t _test_8_1_ap_address =
{
   { 0x00, 0x06, 0x25, 0x87, 0x64, 0x67 }, 6
};

static int _test_1_thread(AL_CONTEXT_PARAM_DECL al_thread_param_t *param)
{
   _test_data_t *data;
   int          ret;

   data = (_test_data_t *)param->param;

   /**
    * In TEST_1 the thread sleeps on the event and is woken up by Test_1_2
    * after which the thread quits.
    */

   al_set_thread_name("_test_1_thread");

   al_print_log(AL_LOG_TYPE_INFORMATION, "_test_1_thread: TID=%d waiting\n", param->thread_id);

   ret = al_wait_on_event(data->event_handle, AL_WAIT_TIMEOUT_INFINITE);

   al_print_log(AL_LOG_TYPE_INFORMATION, "_test_1_thread: Woken up ret=%d and quitting\n", ret);

   al_destroy_event(data->event_handle);

   al_heap_free(data);

   return 0;
}


unsigned int meshap_al_test_1_1(void)
{
   _test_data_t *data;

   data = (_test_data_t *)al_heap_alloc(sizeof(_test_data_t)AL_HEAP_DEBUG_PARAM);
   data->event_handle = al_create_event(1);

   al_create_thread(_test_1_thread, data, AL_THREAD_PRIORITY_NORMAL, "_test_1_thread");

   return (unsigned int)data;
}


void meshap_al_test_1_2(unsigned int token)
{
   _test_data_t *data;

   data = (_test_data_t *)token;

   al_set_event(data->event_handle);
}


static int _test_2_thread(AL_CONTEXT_PARAM_DECL al_thread_param_t *param)
{
   _test_data_t *data;
   int          ret;

   data = (_test_data_t *)param->param;

   /**
    * In TEST_2 the thread sleeps on the event and is woken up by Test_2_2
    * or by the timeout, after which the thread quits.
    */

   al_set_thread_name("_test_2_thread");

   al_print_log(AL_LOG_TYPE_INFORMATION, "_test_2_thread: TID=%d waiting for INFINITE\n", param->thread_id);
   al_reset_event(AL_CONTEXT data->event_handle);
   ret = al_wait_on_event(data->event_handle, AL_WAIT_TIMEOUT_INFINITE);
   al_print_log(AL_LOG_TYPE_INFORMATION, "_test_2_thread: Woken up ret=%d and waiting again\n", ret);
   al_reset_event(AL_CONTEXT data->event_handle);
   ret = al_wait_on_event(data->event_handle, AL_WAIT_TIMEOUT_INFINITE);
   al_print_log(AL_LOG_TYPE_INFORMATION, "_test_2_thread: Woken up ret=%d and quitting\n", ret);

   al_destroy_event(data->event_handle);

   al_heap_free(data);

   return 0;
}


unsigned int meshap_al_test_2_1(void)
{
   _test_data_t *data;

   data = (_test_data_t *)al_heap_alloc(sizeof(_test_data_t)AL_HEAP_DEBUG_PARAM);
   data->event_handle = al_create_event(1);

   al_create_thread(_test_2_thread, data, AL_THREAD_PRIORITY_NORMAL, "_test_2_thread");

   return (unsigned int)data;
}


void meshap_al_test_2_2(unsigned int token)
{
   _test_data_t *data;

   data = (_test_data_t *)token;

   al_set_event(data->event_handle);
}


static int _test_3_thread(AL_CONTEXT_PARAM_DECL al_thread_param_t *param)
{
   _test_data_t *data;
   int          ret;

   data = (_test_data_t *)param->param;

   /**
    * In TEST_3 the thread sleeps on the semaphore and is woken up by Test_3_2
    * after which the thread quits.
    */

   al_set_thread_name("_test_3_thread");

   al_print_log(AL_LOG_TYPE_INFORMATION, "_test_3_thread: TID=%d waiting on semahpore\n", param->thread_id);

   ret = al_wait_on_semaphore(data->semaphore_handle, AL_WAIT_TIMEOUT_INFINITE);

   al_print_log(AL_LOG_TYPE_INFORMATION, "_test_3_thread: Woken up ret=%d and quitting\n", ret);

   al_destroy_semaphore(data->semaphore_handle);

   al_heap_free(data);

   return 0;
}


unsigned int meshap_al_test_3_1(void)
{
   _test_data_t *data;

   data = (_test_data_t *)al_heap_alloc(sizeof(_test_data_t)AL_HEAP_DEBUG_PARAM);
   data->semaphore_handle = al_create_semaphore(1, 0);

   al_create_thread(_test_3_thread, data, 0, "_test_3_thread");

   return (unsigned int)data;
}


void meshap_al_test_3_2(unsigned int token)
{
   _test_data_t *data;

   data = (_test_data_t *)token;

   al_release_semaphore(data->semaphore_handle, 1);
}


void meshap_al_test_4_1(void)
{
   struct  sk_buff *skb;
   unsigned char   *data;

   skb = dev_alloc_skb(2400);
   printk(KERN_INFO "PUT1. H=0x%8x D=0x%8x T=0x%8x L=%d TR=%d HR=%d\n", (unsigned int)skb->head, (unsigned int)skb->data, (unsigned int)skb->tail, skb->len, skb_tailroom(skb), skb_headroom(skb));
   data = skb_put(skb, 20);
   printk(KERN_INFO "PUT2. H=0x%8x D=0x%8x T=0x%8x L=%d TR=%d HR=%d\n", (unsigned int)skb->head, (unsigned int)skb->data, (unsigned int)skb->tail, skb->len, skb_tailroom(skb), skb_headroom(skb));
   data = skb_pull(skb, 20);
   printk(KERN_INFO "PUT3. H=0x%8x D=0x%8x T=0x%8x L=%d TR=%d HR=%d\n", (unsigned int)skb->head, (unsigned int)skb->data, (unsigned int)skb->tail, skb->len, skb_tailroom(skb), skb_headroom(skb));
	tx_rx_pkt_stats.al_test_4_1_PUT++;
   dev_kfree_skb(skb);

   skb = dev_alloc_skb(2400);
   printk(KERN_INFO "RES1. H=0x%8x D=0x%8x T=0x%8x L=%d TR=%d HR=%d\n", (unsigned int)skb->head, (unsigned int)skb->data, (unsigned int)skb->tail, skb->len, skb_tailroom(skb), skb_headroom(skb));
   skb_reserve(skb, 20);
   printk(KERN_INFO "RES2. H=0x%8x D=0x%8x T=0x%8x L=%d TR=%d HR=%d\n", (unsigned int)skb->head, (unsigned int)skb->data, (unsigned int)skb->tail, skb->len, skb_tailroom(skb), skb_headroom(skb));
   data = skb_push(skb, 20);
   printk(KERN_INFO "RES3. H=0x%8x D=0x%8x T=0x%8x L=%d TR=%d HR=%d\n", (unsigned int)skb->head, (unsigned int)skb->data, (unsigned int)skb->tail, skb->len, skb_tailroom(skb), skb_headroom(skb));
	tx_rx_pkt_stats.al_test_4_1_RES++;
   dev_kfree_skb(skb);

   skb = dev_alloc_skb(2400);
   printk(KERN_INFO "RESPUT1. H=0x%8x D=0x%8x T=0x%8x L=%d TR=%d HR=%d\n", (unsigned int)skb->head, (unsigned int)skb->data, (unsigned int)skb->tail, skb->len, skb_tailroom(skb), skb_headroom(skb));
   skb_reserve(skb, 2400 - sizeof(_test_5_1_data));
   printk(KERN_INFO "RESSPUT2. H=0x%8x D=0x%8x T=0x%8x L=%d TR=%d HR=%d\n", (unsigned int)skb->head, (unsigned int)skb->data, (unsigned int)skb->tail, skb->len, skb_tailroom(skb), skb_headroom(skb));
   data = skb_put(skb, sizeof(_test_5_1_data));
   printk(KERN_INFO "RESPUT3. H=0x%8x D=0x%8x T=0x%8x L=%d TR=%d HR=%d\n", (unsigned int)skb->head, (unsigned int)skb->data, (unsigned int)skb->tail, skb->len, skb_tailroom(skb), skb_headroom(skb));
	tx_rx_pkt_stats.al_test_4_1_RESPUT++;
   dev_kfree_skb(skb);
}


static inline void _test_5_1_setup_test_packet(al_packet_t *packet)
{
   packet->position    -= sizeof(_test_5_1_data);
   packet->data_length += sizeof(_test_5_1_data);
   memcpy(packet->buffer + packet->position, _test_5_1_data, sizeof(_test_5_1_data));

   packet->type = 0x0800;
   memcpy(packet->addr_1.bytes, _test_5_1_addresses[0].bytes, ETH_ALEN);
   memcpy(packet->addr_2.bytes, _test_5_1_addresses[1].bytes, ETH_ALEN);
   memcpy(packet->addr_3.bytes, _test_5_1_addresses[2].bytes, ETH_ALEN);
   memcpy(packet->addr_4.bytes, _test_5_1_addresses[3].bytes, ETH_ALEN);
   packet->frame_control = AL_802_11_FC_TODS | AL_802_11_FC_FROMDS;
}


void meshap_al_test_5_1(void)
{
   int                  if_count;
   int                  i;
   al_net_if_t          *al_net_if;
   al_net_if_t          *eth0;
   al_net_if_t          *wlan0;
   meshap_core_packet_t *core_packet;
   al_packet_t          *packet;
   torna_mac_hdr_t      *mac_hdr;
   struct sk_buff       *skb;
   unsigned char        *p;

   if_count = al_get_net_if_count();

   printk(KERN_INFO "meshap_al_test_5_1: if_count = %d\n", if_count);

   for (i = 0, eth0 = NULL, wlan0 = NULL; i < if_count; i++)
   {
      al_net_if = al_get_net_if(i);
      if (al_net_if == NULL)
      {
         printk(KERN_INFO "meshap_al_test_5_1: if_%d was NULL test failed\n", i);
         return;
      }
      printk(KERN_INFO "meshap_al_test_5_1: if_%d = %s encap %d "AL_NET_ADDR_STR "\n",
             i,
             al_net_if->name,
             al_net_if->encapsulation,
             AL_NET_ADDR_TO_STR(&al_net_if->config.hw_addr));

      if (!strcmp(al_net_if->name, "ixp0"))
      {
         eth0 = al_net_if;
      }
      if (!strcmp(al_net_if->name, "wlan0"))
      {
         wlan0 = al_net_if;
      }
   }

   if ((eth0 == NULL) || (wlan0 == NULL))
   {
      printk(KERN_INFO "meshap_al_test_5_1: eth0 == NULL || wlan0 == NULL cannot continue\n");
      return;
   }

   /**
    * Test direct ethernet packet handling
    */

   packet      = al_allocate_packet(AL_PACKET_DIRECTION_OUT);
   core_packet = (meshap_core_packet_t *)packet;

   _test_5_1_setup_test_packet(packet);

   al_packet_impl_setup_skb((meshap_core_net_if_t *)eth0, core_packet);

   if (memcmp(core_packet->skb->data, _test_5_1_addresses[0].bytes, ETH_ALEN) ||
       memcmp(core_packet->skb->data + ETH_ALEN, _test_5_1_addresses[1].bytes, ETH_ALEN))
   {
      printk(KERN_INFO "meshap_al_test_5_1: MAC hdr mismatch test failed\n");
   }

   if (memcmp(core_packet->skb->data + ETH_HLEN, _test_5_1_data, sizeof(_test_5_1_data)))
   {
      printk(KERN_INFO "meshap_al_test_5_1: Data mismatch test failed\n");
   }

   printk(KERN_INFO "meshap_al_test_5_1: ==== DIRECT ETH PACKET DUMP BEGIN====\n");
   PACKET_DUMP(KERN_INFO, core_packet->skb->data, core_packet->skb->len);
   printk(KERN_INFO "meshap_al_test_5_1: ==== DIRECT ETH PACKET DUMP END====\n\n");

	tx_rx_pkt_stats.test_direct_eth_pkt++;
   al_release_packet(packet);

   /**
    * Test direct wireless packet handling
    */

   packet      = al_allocate_packet(AL_PACKET_DIRECTION_OUT);
   core_packet = (meshap_core_packet_t *)packet;

   _test_5_1_setup_test_packet(packet);

   al_packet_impl_setup_skb((meshap_core_net_if_t *)wlan0, core_packet);

   mac_hdr = (torna_mac_hdr_t *)core_packet->skb->mac_header;

   if ((mac_hdr->signature[0] != TORNA_MAC_HDR_SIGNATURE_1) ||
       (mac_hdr->signature[1] != TORNA_MAC_HDR_SIGNATURE_2))
   {
      printk(KERN_INFO "meshap_al_test_5_1: TORNA MAC hdr not found test failed\n");
   }

   if (core_packet->skb->len != sizeof(_test_5_1_data) + sizeof(torna_mac_hdr_t))
   {
      printk(KERN_INFO "meshap_al_test_5_1: Wireless Data length mismatch (%d instead of %d) test failed\n", core_packet->skb->len, sizeof(_test_5_1_data) + sizeof(torna_mac_hdr_t));
   }

   if (memcmp(core_packet->skb->data + sizeof(torna_mac_hdr_t), _test_5_1_data, sizeof(_test_5_1_data)))
   {
      printk(KERN_INFO "meshap_al_test_5_1: Wireless Data mismatch test failed\n");
   }

   if (mac_hdr->type != TORNA_MAC_HDR_TYPE_SK_BUFF)
   {
      printk(KERN_INFO "meshap_al_test_5_1: Wireless mac_hdr->type != TORNA_MAC_HDR_TYPE_SK_BUFF test failed\n");
   }

   printk(KERN_INFO "meshap_al_test_5_1: ==== DIRECT WIRELESS PACKET DUMP BEGIN====\n");
   PACKET_DUMP(KERN_INFO, core_packet->skb->data, core_packet->skb->len);
   printk(KERN_INFO "meshap_al_test_5_1: ==== DIRECT WIRELESS PACKET DUMP END====\n\n");

	tx_rx_pkt_stats.test_direct_wireless_pkt++;
   al_release_packet(packet);

   /**
    * Test ethernet to ethernet handling
    */

   skb             = dev_alloc_skb(sizeof(_test_5_1_data) + ETH_HLEN);
   p               = skb_put(skb, ETH_HLEN);
   skb->mac_header = p;
   memcpy(SKB_ETHERNET(skb)->h_dest, _test_5_1_addresses[0].bytes, ETH_ALEN);
   memcpy(SKB_ETHERNET(skb)->h_source, _test_5_1_addresses[1].bytes, ETH_ALEN);
   SKB_ETHERNET(skb)->h_proto = htons(0x0800);
   p = skb_put(skb, sizeof(_test_5_1_data));
   memcpy(p, _test_5_1_data, sizeof(_test_5_1_data));
   skb_pull(skb, ETH_HLEN);
   core_packet = al_packet_impl_create_from_skb((meshap_core_net_if_t *)eth0, skb, MAIN_POOL);
   packet      = &core_packet->al_packet;

   al_packet_impl_setup_skb((meshap_core_net_if_t *)eth0, core_packet);

   if (memcmp(core_packet->skb->data + ETH_HLEN, _test_5_1_data, sizeof(_test_5_1_data)))
   {
      printk(KERN_INFO "meshap_al_test_5_1: ETH->ETH mismatch test failed\n");
   }

   printk(KERN_INFO "meshap_al_test_5_1: ==== ETH->ETH PACKET DUMP BEGIN====\n");
   PACKET_DUMP(KERN_INFO, core_packet->skb->data, core_packet->skb->len);
   printk(KERN_INFO "meshap_al_test_5_1: ==== ETH->ETH PACKET DUMP END====\n\n");
	tx_rx_pkt_stats.test_eth_to_eth_pkt++;
   al_release_packet(packet);


   /**
    * Test ethernet to wireless handling
    */

   skb             = dev_alloc_skb(sizeof(_test_5_1_data) + ETH_HLEN);
   p               = skb_put(skb, ETH_HLEN);
   skb->mac_header = p;
   memcpy(SKB_ETHERNET(skb)->h_dest, _test_5_1_addresses[0].bytes, ETH_ALEN);
   memcpy(SKB_ETHERNET(skb)->h_source, _test_5_1_addresses[1].bytes, ETH_ALEN);
   SKB_ETHERNET(skb)->h_proto = htons(0x0800);
   p = skb_put(skb, sizeof(_test_5_1_data));
   memcpy(p, _test_5_1_data, sizeof(_test_5_1_data));
   skb_pull(skb, ETH_HLEN);
   core_packet = al_packet_impl_create_from_skb((meshap_core_net_if_t *)eth0, skb, MAIN_POOL);
   packet      = &core_packet->al_packet;

   al_packet_impl_setup_skb((meshap_core_net_if_t *)wlan0, core_packet);

   mac_hdr = (torna_mac_hdr_t *)core_packet->skb->mac_header;

   if ((mac_hdr->signature[0] != TORNA_MAC_HDR_SIGNATURE_1) ||
       (mac_hdr->signature[1] != TORNA_MAC_HDR_SIGNATURE_2))
   {
      printk(KERN_INFO "meshap_al_test_5_1: TORNA MAC hdr not found test failed\n");
   }

   if (memcmp(core_packet->skb->data, _test_5_1_data, sizeof(_test_5_1_data)))
   {
      printk(KERN_INFO "meshap_al_test_5_1: Wireless Data mismatch test failed\n");
   }

   if (mac_hdr->type != TORNA_MAC_HDR_TYPE_HEAP)
   {
      printk(KERN_INFO "meshap_al_test_5_1: Wireless mac_hdr->type != TORNA_MAC_HDR_TYPE_HEAP test failed\n");
   }

   printk(KERN_INFO "meshap_al_test_5_1: ==== ETH->WIRELESS PACKET DUMP BEGIN====\n");
   PACKET_DUMP(KERN_INFO, core_packet->skb->data, core_packet->skb->len);
   printk(KERN_INFO "meshap_al_test_5_1: ==== ETH->WIRELESS PACKET DUMP END====\n\n");
   kfree(mac_hdr);
	tx_rx_pkt_stats.test_eth_to_wireless_pkt++;
   al_release_packet(packet);

   /**
    * Test wireless to ethernet handling
    */

   skb             = dev_alloc_skb(sizeof(_test_5_1_data) + sizeof(torna_mac_hdr_t));
   p               = skb_put(skb, sizeof(torna_mac_hdr_t));
   skb->mac_header = p;
   mac_hdr         = (torna_mac_hdr_t *)skb->mac_header;

   memcpy(SKB_ETHERNET(skb)->h_dest, _test_5_1_addresses[0].bytes, ETH_ALEN);
   memcpy(SKB_ETHERNET(skb)->h_source, _test_5_1_addresses[1].bytes, ETH_ALEN);
   SKB_ETHERNET(skb)->h_proto = htons(0x0800);

   mac_hdr->signature[0]  = TORNA_MAC_HDR_SIGNATURE_1;
   mac_hdr->signature[1]  = TORNA_MAC_HDR_SIGNATURE_2;
   mac_hdr->frame_control = AL_802_11_FC_TODS | AL_802_11_FC_FROMDS;

   memcpy(mac_hdr->addresses[0], _test_5_1_addresses[0].bytes, ETH_ALEN);
   memcpy(mac_hdr->addresses[1], _test_5_1_addresses[1].bytes, ETH_ALEN);
   memcpy(mac_hdr->addresses[2], _test_5_1_addresses[2].bytes, ETH_ALEN);
   memcpy(mac_hdr->addresses[3], _test_5_1_addresses[3].bytes, ETH_ALEN);

   mac_hdr->type = TORNA_MAC_HDR_TYPE_SK_BUFF;

   p = skb_put(skb, sizeof(_test_5_1_data));
   memcpy(p, _test_5_1_data, sizeof(_test_5_1_data));
   skb_pull(skb, sizeof(torna_mac_hdr_t));
   core_packet = al_packet_impl_create_from_skb((meshap_core_net_if_t *)wlan0, skb, MAIN_POOL);
   packet      = &core_packet->al_packet;

   al_packet_impl_setup_skb((meshap_core_net_if_t *)eth0, core_packet);

   if (memcmp(core_packet->skb->data + ETH_HLEN, _test_5_1_data, sizeof(_test_5_1_data)))
   {
      printk(KERN_INFO "meshap_al_test_5_1: WIRELESS->ETH Data mismatch test failed\n");
   }

   printk(KERN_INFO "meshap_al_test_5_1: ==== WIRELESS->ETH PACKET DUMP BEGIN====\n");
   PACKET_DUMP(KERN_INFO, core_packet->skb->data, core_packet->skb->len);
   printk(KERN_INFO "meshap_al_test_5_1: ==== WIRELESS->ETH PACKET DUMP END====\n\n");
	tx_rx_pkt_stats.test_wireless_to_eth_pkt++;
   al_release_packet(packet);


   /**
    * Test wireless to wireless handling
    */

   skb             = dev_alloc_skb(sizeof(_test_5_1_data) + sizeof(torna_mac_hdr_t));
   p               = skb_put(skb, sizeof(torna_mac_hdr_t));
   skb->mac_header = p;
   mac_hdr         = (torna_mac_hdr_t *)skb->mac_header;

   memcpy(SKB_ETHERNET(skb)->h_dest, _test_5_1_addresses[0].bytes, ETH_ALEN);
   memcpy(SKB_ETHERNET(skb)->h_source, _test_5_1_addresses[1].bytes, ETH_ALEN);
   SKB_ETHERNET(skb)->h_proto = htons(0x0800);

   mac_hdr->signature[0]  = TORNA_MAC_HDR_SIGNATURE_1;
   mac_hdr->signature[1]  = TORNA_MAC_HDR_SIGNATURE_2;
   mac_hdr->frame_control = AL_802_11_FC_TODS | AL_802_11_FC_FROMDS;

   memcpy(mac_hdr->addresses[0], _test_5_1_addresses[0].bytes, ETH_ALEN);
   memcpy(mac_hdr->addresses[1], _test_5_1_addresses[1].bytes, ETH_ALEN);
   memcpy(mac_hdr->addresses[2], _test_5_1_addresses[2].bytes, ETH_ALEN);
   memcpy(mac_hdr->addresses[3], _test_5_1_addresses[3].bytes, ETH_ALEN);

   mac_hdr->type = TORNA_MAC_HDR_TYPE_SK_BUFF;

   p = skb_put(skb, sizeof(_test_5_1_data));
   memcpy(p, _test_5_1_data, sizeof(_test_5_1_data));
   skb_pull(skb, sizeof(torna_mac_hdr_t));
   core_packet = al_packet_impl_create_from_skb((meshap_core_net_if_t *)wlan0, skb, MAIN_POOL);
   packet      = &core_packet->al_packet;

   al_packet_impl_setup_skb((meshap_core_net_if_t *)wlan0, core_packet);

   if (memcmp(core_packet->skb->data, _test_5_1_data, sizeof(_test_5_1_data)))
   {
      printk(KERN_INFO "meshap_al_test_5_1: WIRELESS->WIRELESS Data mismatch test failed\n");
   }

   printk(KERN_INFO "meshap_al_test_5_1: ==== WIRELESS->WIRELESS PACKET DUMP BEGIN====\n");
   PACKET_DUMP(KERN_INFO, core_packet->skb->data, core_packet->skb->len);
   printk(KERN_INFO "meshap_al_test_5_1: ==== WIRELESS->WIRELESS PACKET DUMP END====\n\n");
	tx_rx_pkt_stats.test_wireless_to_wireless_pkt++;
   al_release_packet(packet);
}


/**
 * meshap_al_test_6_1 tests:
 *	a) get_extended_operations for eth and wlan
 *	b) get_mode, set_mode
 *	c) get_essid,set_essid
 *	d) associate,get_bssid
 *	e) al_thread_sleep
 */
void meshap_al_test_6_1(void)
{
   int                    if_count;
   int                    i;
   al_net_if_t            *al_net_if;
   al_net_if_t            *eth0;
   al_net_if_t            *wlan0;
   al_802_11_operations_t *operations;
   int                    mode;
   al_net_addr_t          bssid;

   if_count = al_get_net_if_count();

   printk(KERN_INFO "meshap_al_test_6_1: if_count = %d\n", if_count);

   for (i = 0, eth0 = NULL, wlan0 = NULL; i < if_count; i++)
   {
      al_net_if = al_get_net_if(i);
      if (al_net_if == NULL)
      {
         printk(KERN_INFO "meshap_al_test_6_1: if_%d was NULL test failed\n", i);
         return;
      }
      printk(KERN_INFO "meshap_al_test_6_1: if_%d = %s encap %d\n", i, al_net_if->name, al_net_if->encapsulation);
      if (!strcmp(al_net_if->name, "ixp0"))
      {
         eth0 = al_net_if;
      }
      if (!strcmp(al_net_if->name, "wlan0"))
      {
         wlan0 = al_net_if;
      }
   }

   if ((eth0 == NULL) || (wlan0 == NULL))
   {
      printk(KERN_INFO "meshap_al_test_6_1: eth0 == NULL || wlan0 == NULL cannot continue\n");
      return;
   }

   operations = (al_802_11_operations_t *)eth0->get_extended_operations(eth0);
   printk(KERN_INFO "meshap_al_test_6_1: eth0->extended_ops = 0x%x\n", (unsigned int)operations);
   operations = (al_802_11_operations_t *)wlan0->get_extended_operations(wlan0);
   printk(KERN_INFO "meshap_al_test_6_1: wlan0->extended_ops = 0x%x\n", (unsigned int)operations);

   mode = operations->get_mode(wlan0);
   printk(KERN_INFO "meshap_al_test_6_1: wlan0->get_mode = %s\n", _mode_names[mode]);
   operations->set_essid(wlan0, "meshap_al_test_6_1");
   printk(KERN_INFO "meshap_al_test_6_1: essid set to meshap_al_test_6_1 sleeping for 10 seconds\n");
   al_thread_sleep(10000);
   operations->set_mode(wlan0, AL_802_11_MODE_INFRA, 0);

   printk(KERN_INFO "meshap_al_test_6_1: associatting with AP "MACSTR "\n", MAC2STR(_test_6_1_ap_address[0].bytes));
   operations->associate(wlan0, &_test_6_1_ap_address[0], 36, "linksys", 7, 2000, NULL, NULL);
   operations->get_bssid(wlan0, &bssid);
   if ((bssid.length != _test_6_1_ap_address[0].length) ||
       memcmp(bssid.bytes, _test_6_1_ap_address[0].bytes, ETH_ALEN))
   {
      printk(KERN_INFO "meshap_al_test_6_1: bssid.length != _test_6_1_ap_address[0].length || memcmp(bssid.bytes,_test_6_1_ap_address[0].bytes,ETH_ALEN) test failed\n");
   }
}


/**
 * meshap_al_test_7_1 tests:
 *	a) scan_access_points
 *	b) set_channel
 *	c) get_channel
 */
void meshap_al_test_7_1(void)
{
   int                         if_count;
   int                         i;
   al_net_if_t                 *al_net_if;
   al_net_if_t                 *eth0;
   al_net_if_t                 *wlan0;
   al_802_11_operations_t      *operations;
   al_scan_access_point_info_t *aps;
   int                         ap_count;

   if_count = al_get_net_if_count();

   printk(KERN_INFO "meshap_al_test_7_1: if_count = %d\n", if_count);

   for (i = 0, eth0 = NULL, wlan0 = NULL; i < if_count; i++)
   {
      al_net_if = al_get_net_if(i);
      if (al_net_if == NULL)
      {
         printk(KERN_INFO "meshap_al_test_7_1: if_%d was NULL test failed\n", i);
         return;
      }
      printk(KERN_INFO "meshap_al_test_7_1: if_%d = %s encap %d\n", i, al_net_if->name, al_net_if->encapsulation);
      if (!strcmp(al_net_if->name, "ixp0"))
      {
         eth0 = al_net_if;
      }
      if (!strcmp(al_net_if->name, "wlan0"))
      {
         wlan0 = al_net_if;
      }
   }

   if ((eth0 == NULL) || (wlan0 == NULL))
   {
      printk(KERN_INFO "meshap_al_test_7_1: eth0 == NULL || wlan0 == NULL cannot continue\n");
      return;
   }

   operations = (al_802_11_operations_t *)eth0->get_extended_operations(eth0);
   printk(KERN_INFO "meshap_al_test_7_1: eth0->extended_ops = 0x%x\n", (unsigned int)operations);
   operations = (al_802_11_operations_t *)wlan0->get_extended_operations(wlan0);
   printk(KERN_INFO "meshap_al_test_7_1: wlan0->extended_ops = 0x%x\n", (unsigned int)operations);

   operations->set_mode(wlan0, AL_802_11_MODE_INFRA, 0);

   printk(KERN_INFO "meshap_al_test_7_1: wlan0->scan_access_points...");
   operations->scan_access_points(wlan0, &aps, &ap_count, 5000, 0, NULL);

   for (i = 0; i < ap_count; i++)
   {
      printk(KERN_INFO "meshap_al_test_7_1: %d %s "MACSTR " channel %d signal %d\n", i + 1, aps[i].essid, MAC2STR(aps[i].bssid.bytes), aps[i].channel, aps[i].signal);
   }

   kfree(aps);

   operations->set_channel(wlan0, 40);
}


static inline void _test_8_1_setup_test_packet(al_packet_t *packet, al_net_addr_t *src_addr, int type)
{
   packet->position    -= sizeof(_test_8_1_data);
   packet->data_length += sizeof(_test_8_1_data);
   memcpy(packet->buffer + packet->position, _test_8_1_data, sizeof(_test_8_1_data));

   packet->type = htons(0x0800);

   if (type == 0)       /**ETHERNET*/
   {
      memcpy(packet->addr_1.bytes, broadcast_net_addr.bytes, ETH_ALEN);
      memcpy(packet->addr_2.bytes, src_addr->bytes, ETH_ALEN);
   }
   else
   {
      memcpy(packet->addr_1.bytes, _test_8_1_ap_address.bytes, ETH_ALEN);
      memcpy(packet->addr_2.bytes, src_addr->bytes, ETH_ALEN);
      memcpy(packet->addr_3.bytes, broadcast_net_addr.bytes, ETH_ALEN);
   }

   packet->frame_control = AL_802_11_FC_TODS;
}


void meshap_al_test_8_1(void)
{
   int                    if_count;
   int                    i;
   al_net_if_t            *al_net_if;
   al_net_if_t            *eth0;
   al_net_if_t            *wlan0;
   al_802_11_operations_t *operations;
   meshap_core_packet_t   *core_packet;
   al_packet_t            *packet;
   torna_mac_hdr_t        *mac_hdr;
   struct sk_buff         *skb;
   unsigned char          *p;
   struct ethhdr          *eth_hdr;

   if_count = al_get_net_if_count();

   printk(KERN_INFO "meshap_al_test_8_1: if_count = %d\n", if_count);

   for (i = 0, eth0 = NULL, wlan0 = NULL; i < if_count; i++)
   {
      al_net_if = al_get_net_if(i);
      if (al_net_if == NULL)
      {
         printk(KERN_INFO "meshap_al_test_8_1: if_%d was NULL test failed\n", i);
         return;
      }
      printk(KERN_INFO "meshap_al_test_8_1: if_%d = %s encap %d\n", i, al_net_if->name, al_net_if->encapsulation);
      if (!strcmp(al_net_if->name, "eth0"))
      {
         eth0 = al_net_if;
      }
      if (!strcmp(al_net_if->name, "wlan0"))
      {
         wlan0 = al_net_if;
      }
   }

   if ((eth0 == NULL) || (wlan0 == NULL))
   {
      printk(KERN_INFO "meshap_al_test_8_1: eth0 == NULL || wlan0 == NULL cannot continue\n");
      return;
   }

   operations = (al_802_11_operations_t *)eth0->get_extended_operations(wlan0);

   /*
    * operations->set_mode(wlan0,AL_802_11_MODE_INFRA);
    *
    * printk(KERN_INFO"meshap_al_test_8_1: associatting with AP "MACSTR"\n",MAC2STR(_test_8_1_ap_address.bytes));
    * operations->associate(wlan0,&_test_8_1_ap_address,6,"wlan",4,2000);
    * operations->get_bssid(wlan0,&bssid);
    * if(bssid.length != _test_8_1_ap_address.length
    || memcmp(bssid.bytes,_test_8_1_ap_address.bytes,ETH_ALEN)) {
    ||     printk(KERN_INFO"meshap_al_test_8_1: bssid.length != _test_8_1_ap_address.length || memcmp(bssid.bytes,_test_8_1_ap_address.bytes,ETH_ALEN) test failed\n");
    ||}*/

   /**
    * Test transmit of direct ethernet
    */

   packet      = al_allocate_packet(AL_PACKET_DIRECTION_OUT);
   core_packet = (meshap_core_packet_t *)packet;

   _test_8_1_setup_test_packet(packet, &eth0->config.hw_addr, 0);
   eth0->transmit(eth0, packet);

   /**
    * Test transmit of direct wireless
    */

   packet      = al_allocate_packet(AL_PACKET_DIRECTION_OUT);
   core_packet = (meshap_core_packet_t *)packet;
   _test_8_1_setup_test_packet(packet, &wlan0->config.hw_addr, 1);
   wlan0->transmit(wlan0, packet);

   /**
    * Test ETH->WIRELESS transmission
    */

   skb     = dev_alloc_skb(ETH_HLEN + sizeof(_test_8_1_data));
   eth_hdr = (struct ethhdr *)skb_put(skb, ETH_HLEN);
   memcpy(eth_hdr->h_dest, broadcast_net_addr.bytes, ETH_ALEN);
   memcpy(eth_hdr->h_source, eth0->config.hw_addr.bytes, ETH_ALEN);
   eth_hdr->h_proto = htons(0x0800);
   p = skb_put(skb, sizeof(_test_8_1_data));
   memcpy(p, _test_8_1_data, sizeof(_test_8_1_data));
   skb_pull(skb, ETH_HLEN);

   core_packet = al_packet_impl_create_from_skb((meshap_core_net_if_t *)eth0, skb, MAIN_POOL);
   core_packet->al_packet.frame_control = AL_802_11_FC_TODS;
   memcpy(core_packet->al_packet.addr_1.bytes, _test_8_1_ap_address.bytes, ETH_ALEN);
   memcpy(core_packet->al_packet.addr_2.bytes, wlan0->config.hw_addr.bytes, ETH_ALEN);
   memcpy(core_packet->al_packet.addr_3.bytes, broadcast_net_addr.bytes, ETH_ALEN);
   wlan0->transmit(wlan0, &core_packet->al_packet);

   /**
    * Test WIRELESS->ETH transmission
    */

   skb     = dev_alloc_skb(sizeof(torna_mac_hdr_t) + sizeof(_test_8_1_data));
   mac_hdr = (torna_mac_hdr_t *)skb_put(skb, sizeof(torna_mac_hdr_t));
   eth_hdr = &mac_hdr->ethernet;
   memset(mac_hdr, 0, sizeof(torna_mac_hdr_t));

   memcpy(mac_hdr->addresses[0], broadcast_net_addr.bytes, ETH_ALEN);
   memcpy(mac_hdr->addresses[1], _test_8_1_ap_address.bytes, ETH_ALEN);
   memcpy(mac_hdr->addresses[2], wlan0->config.hw_addr.bytes, ETH_ALEN);
   mac_hdr->frame_control = AL_802_11_FC_FROMDS;
   AL_802_11_FC_SET_TYPE(mac_hdr->frame_control, AL_802_11_FC_TYPE_DATA);
   AL_802_11_FC_SET_STYPE(mac_hdr->frame_control, AL_802_11_FC_STYPE_DATA);
   mac_hdr->signature[0] = TORNA_MAC_HDR_SIGNATURE_1;
   mac_hdr->signature[1] = TORNA_MAC_HDR_SIGNATURE_2;
   mac_hdr->type         = TORNA_MAC_HDR_TYPE_SK_BUFF;
   skb->mac_header       = (u8 *)mac_hdr;

   memcpy(eth_hdr->h_dest, broadcast_net_addr.bytes, ETH_ALEN);
   memcpy(eth_hdr->h_source, wlan0->config.hw_addr.bytes, ETH_ALEN);
   eth_hdr->h_proto = htons(0x0800);

   p = skb_put(skb, sizeof(_test_8_1_data));
   memcpy(p, _test_8_1_data, sizeof(_test_8_1_data));
   skb_pull(skb, sizeof(torna_mac_hdr_t));

   core_packet = al_packet_impl_create_from_skb((meshap_core_net_if_t *)wlan0, skb, MAIN_POOL);
   memcpy(core_packet->al_packet.addr_1.bytes, broadcast_net_addr.bytes, ETH_ALEN);
   memcpy(core_packet->al_packet.addr_2.bytes, eth0->config.hw_addr.bytes, ETH_ALEN);
   eth0->transmit(eth0, &core_packet->al_packet);


   /**
    * Test WIRELESS->ETH transmission
    */

   skb     = dev_alloc_skb(sizeof(torna_mac_hdr_t) + sizeof(_test_8_1_data));
   mac_hdr = (torna_mac_hdr_t *)skb_put(skb, sizeof(torna_mac_hdr_t));
   eth_hdr = &mac_hdr->ethernet;
   memset(mac_hdr, 0, sizeof(torna_mac_hdr_t));

   memcpy(mac_hdr->addresses[0], broadcast_net_addr.bytes, ETH_ALEN);
   memcpy(mac_hdr->addresses[1], _test_8_1_ap_address.bytes, ETH_ALEN);
   memcpy(mac_hdr->addresses[2], wlan0->config.hw_addr.bytes, ETH_ALEN);
   mac_hdr->frame_control = AL_802_11_FC_FROMDS;
   AL_802_11_FC_SET_TYPE(mac_hdr->frame_control, AL_802_11_FC_TYPE_DATA);
   AL_802_11_FC_SET_STYPE(mac_hdr->frame_control, AL_802_11_FC_STYPE_DATA);
   mac_hdr->signature[0] = TORNA_MAC_HDR_SIGNATURE_1;
   mac_hdr->signature[1] = TORNA_MAC_HDR_SIGNATURE_2;
   mac_hdr->type         = TORNA_MAC_HDR_TYPE_SK_BUFF;
   skb->mac_header       = (u8 *)mac_hdr;

   memcpy(eth_hdr->h_dest, broadcast_net_addr.bytes, ETH_ALEN);
   memcpy(eth_hdr->h_source, wlan0->config.hw_addr.bytes, ETH_ALEN);
   eth_hdr->h_proto = htons(0x0800);

   p = skb_put(skb, sizeof(_test_8_1_data));
   memcpy(p, _test_8_1_data, sizeof(_test_8_1_data));
   skb_pull(skb, sizeof(torna_mac_hdr_t));

   core_packet = al_packet_impl_create_from_skb((meshap_core_net_if_t *)wlan0, skb, MAIN_POOL);

   core_packet->al_packet.frame_control = AL_802_11_FC_TODS;
   AL_802_11_FC_SET_TYPE(core_packet->al_packet.frame_control, AL_802_11_FC_TYPE_DATA);
   AL_802_11_FC_SET_STYPE(core_packet->al_packet.frame_control, AL_802_11_FC_STYPE_DATA);

   memcpy(core_packet->al_packet.addr_1.bytes, _test_8_1_ap_address.bytes, ETH_ALEN);
   memcpy(core_packet->al_packet.addr_2.bytes, wlan0->config.hw_addr.bytes, ETH_ALEN);
   memcpy(core_packet->al_packet.addr_3.bytes, broadcast_net_addr.bytes, ETH_ALEN);

   al_thread_sleep(100);

   wlan0->transmit(wlan0, &core_packet->al_packet);
}


void meshap_al_test_9_1(void)
{
   int                  if_count;
   int                  i;
   al_net_if_t          *al_net_if;
   al_net_if_t          *eth0;
   al_net_if_t          *wlan0;
   meshap_core_net_if_t *core_net_if;

   if_count = al_get_net_if_count();

   printk(KERN_INFO "meshap_al_test_9_1: if_count = %d\n", if_count);

   for (i = 0, eth0 = NULL, wlan0 = NULL; i < if_count; i++)
   {
      al_net_if   = al_get_net_if(i);
      core_net_if = (meshap_core_net_if_t *)al_net_if;
      if (al_net_if == NULL)
      {
         printk(KERN_INFO "meshap_al_test_9_1: if_%d was NULL test failed\n", i);
         return;
      }
      printk(KERN_INFO "meshap_al_test_9_1: if_%d = %s encap %d if=%p dev = %p\n", i, al_net_if->name, al_net_if->encapsulation, al_net_if, core_net_if->dev);
      if (!strcmp(al_net_if->name, "eth0"))
      {
         eth0 = al_net_if;
      }
      if (!strcmp(al_net_if->name, "wlan0"))
      {
         wlan0 = al_net_if;
      }
   }
}


void meshap_al_test_10_1(void)
{
   int                    if_count;
   int                    i;
   al_net_if_t            *al_net_if;
   al_net_if_t            *wlan1;
   meshap_core_net_if_t   *core_net_if;
   al_802_11_operations_t *operations;
   al_packet_t            *packet;
   al_net_addr_t          bssid;

   if_count = al_get_net_if_count();

   printk(KERN_INFO "meshap_al_test_10_1: if_count = %d\n", if_count);

   for (i = 0, wlan1 = NULL; i < if_count; i++)
   {
      al_net_if   = al_get_net_if(i);
      core_net_if = (meshap_core_net_if_t *)al_net_if;
      if (al_net_if == NULL)
      {
         printk(KERN_INFO "meshap_al_test_10_1: if_%d was NULL test failed\n", i);
         return;
      }
      printk(KERN_INFO "meshap_al_test_10_1: if_%d = %s encap %d if=%p dev = %p\n", i, al_net_if->name, al_net_if->encapsulation, al_net_if, core_net_if->dev);
      if (!strcmp(al_net_if->name, "wlan1"))
      {
         wlan1 = al_net_if;
      }
   }

   AL_ASSERT("meshap_al_test_10_1", wlan1 != NULL);

   if (wlan1 != NULL)
   {
      operations = wlan1->get_extended_operations(wlan1);

      operations->get_bssid(wlan1, &bssid);

      packet                = al_allocate_packet(AL_PACKET_DIRECTION_OUT);
      packet->position     -= 2;
      packet->data_length  += 2;
      packet->frame_control = 0;

      memcpy(packet->addr_1.bytes, bssid.bytes, ETH_ALEN);
      memcpy(packet->addr_2.bytes, wlan1->config.hw_addr.bytes, ETH_ALEN);
      memcpy(packet->addr_3.bytes, bssid.bytes, ETH_ALEN);

      operations->send_management_frame(wlan1, AL_802_11_FC_STYPE_DEAUTH, packet);
   }
   else
   {
      printk(KERN_INFO "meshap_al_test_10_1: wlan1 was NULL test failed\n");
   }
}


void meshap_al_test_11_1(void)
{
   int         if_count;
   int         i;
   al_net_if_t *al_net_if;
   al_net_if_t *wlan1;
   al_packet_t *packet;

   if_count = al_get_net_if_count();

   printk(KERN_INFO "meshap_al_test_11_1: if_count = %d\n", if_count);

   for (i = 0, wlan1 = NULL; i < if_count; i++)
   {
      al_net_if = al_get_net_if(i);
      if (!strcmp(al_net_if->name, "eth0"))
      {
         wlan1 = al_net_if;
         break;
      }
   }

   AL_ASSERT("meshap_al_test_11_1", wlan1 != NULL);

   if (wlan1 != NULL)
   {
      packet = al_allocate_packet(AL_PACKET_DIRECTION_OUT);
      packet->data_length += sizeof(_test_8_1_data);
      packet->position    -= sizeof(_test_8_1_data);
      memcpy(packet->buffer + packet->position, _test_8_1_data, sizeof(_test_8_1_data));
      al_send_packet_up_stack(wlan1, packet);
   }
}


void meshap_al_test_12_1(void)
{
   unsigned char prf_out[128];
   unsigned char data2[64];
   unsigned char dummy[256];
   unsigned char key_buf[32];

   static unsigned char key1[] =
   {
      0x0b, 0x0b, 0x0b, 0x0b, 0x0b, 0x0b, 0x0b, 0x0b, 0x0b, 0x0b, 0x0b, 0x0b, 0x0b, 0x0b, 0x0b, 0x0b, 0x0b, 0x0b, 0x0b, 0x0b
   };
   static unsigned char data1[] = "Hi There";

   static unsigned char data[] =
   {
      0xDD, 0x26, 0x00, 0x50, 0xF2, 0x01,
      0x01, 0x00, 0xB2, 0x93, 0x0D, 0x3D,
      0x22, 0x7D, 0x3E, 0xB9, 0x30, 0xEA,
      0xA9, 0x2F, 0x54, 0x9D, 0x85, 0x68,
      0x0E, 0x5F, 0x59, 0xF7, 0x3F, 0x5C,
      0x9D, 0x41, 0xB7, 0x55, 0x80, 0x7F,
      0xAB, 0xDE, 0x06, 0x2F
   };

   static unsigned char iv[] =
   {
      0x73, 0xEE, 0x45, 0x6E, 0x4F, 0xFE, 0x83, 0x90,
      0x61, 0xBF, 0x3C, 0x14, 0xC4, 0x2A, 0xDC, 0x5C,
   };

   static unsigned char key[] =
   {
      0x79, 0x79, 0xAC, 0xFC, 0x1B, 0xAB, 0x91, 0x82,
      0xDA, 0x81, 0x3E, 0x0B, 0x9D, 0x14, 0x99, 0x96,
   };

   DOT11I_PRF_512(key1, sizeof(key1), "prefix", data1, 8, prf_out);
   AL_NET_PACKET_DUMP(prf_out, 32);
   printk(KERN_INFO "======\n");
   memset(dummy, 0, sizeof(dummy));
   memcpy(key_buf, iv, 16);
   memcpy(key_buf + 16, key, 16);
   dot11i_rc4_encrypt(data2, data, sizeof(data), key_buf, 32, dummy, sizeof(dummy));
   AL_NET_PACKET_DUMP(data2, sizeof(data));
   printk(KERN_INFO "======\n");
}


void meshap_al_test_13_1(void)
{
   unsigned char *p;

   printk(KERN_INFO "meshap_al_test_13_1: Triggering NULL pointer exception...\n");

   p  = NULL;
   *p = 0;
}


static void _meshap_al_test_14_1_timer_function(unsigned long arg)
{
   unsigned char *p;

   printk(KERN_INFO "meshap_al_test_14_1: Triggering Kernel Panic Now...\n");

   p  = NULL;
   *p = 0;
}


static struct timer_list _meshap_al_test_14_1_timer;

void meshap_al_test_14_1(void)
{
   printk(KERN_INFO "meshap_al_test_14_1: Triggering Kernel Panic in 3 seconds\n");
   _meshap_al_test_14_1_timer.expires  = jiffies + (3 * HZ);
   _meshap_al_test_14_1_timer.function = _meshap_al_test_14_1_timer_function;
   add_timer(&_meshap_al_test_14_1_timer);
}
