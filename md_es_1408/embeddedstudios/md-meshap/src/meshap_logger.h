/********************************************************************************
* MeshDynamics
* --------------
* File     : meshap_logger.h
* Comments : Torna MeshAP logger header
* Created  : 5/12/2004
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  1  |6/5/2004  | More log types added                            | Sriram |
* -----------------------------------------------------------------------------
* |  0  |5/12/2004 | Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

#include "torna_log.h"

#ifndef __MESHAP_LOGGER_H__
#define __MESHAP_LOGGER_H__

#define MESHAP_LOGGER_TYPE_FLOW               0x00000001
#define MESHAP_LOGGER_TYPE_DEBUG              0x00000002
#define MESHAP_LOGGER_TYPE_DEBUG_HW           0x00000004
#define MESHAP_LOGGER_TYPE_EXTRA              0x00000008
#define MESHAP_LOGGER_TYPE_INTERRUPT          0x00000010
#define MESHAP_LOGGER_TYPE_RX_DATA            0x00000020
#define MESHAP_LOGGER_TYPE_TX_DATA            0x00000040
#define MESHAP_LOGGER_TYPE_RX_MGMT            0x00000080
#define MESHAP_LOGGER_TYPE_TX_MGMT            0x00000100

#define MESHAP_LOGGER_TYPE_FLOW_LEVEL         KERN_INFO
#define MESHAP_LOGGER_TYPE_DEBUG_LEVEL        KERN_INFO
#define MESHAP_LOGGER_TYPE_DEBUG_HW_LEVEL     KERN_DEBUG
#define MESHAP_LOGGER_TYPE_EXTRA_LEVEL        KERN_DEBUG
#define MESHAP_LOGGER_TYPE_INTERRUPT_LEVEL    KERN_DEBUG
#define MESHAP_LOGGER_TYPE_RX_DATA_LEVEL      KERN_INFO
#define MESHAP_LOGGER_TYPE_TX_DATA_LEVEL      KERN_INFO
#define MESHAP_LOGGER_TYPE_RX_MGMT_LEVEL      KERN_INFO
#define MESHAP_LOGGER_TYPE_TX_MGMT_LEVEL      KERN_INFO
#endif /*__MESHAP_LOGGER_H__*/
