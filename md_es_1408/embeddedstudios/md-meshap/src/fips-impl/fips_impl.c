/********************************************************************************
* MeshDynamics
* --------------
* File     : fips-impl.c
* Comments : Meshap FIPS 140-2 Compliance implementation
* Created  : 01/11/2006
* Author   : Prachiti Gaikwad
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  1  |8/22/2008 | Changes for FIPS                                |Abhijit |
* -----------------------------------------------------------------------------
* |  0  |01/11/2006| Created.                                        |Prachiti|
* -----------------------------------------------------------------------------
********************************************************************************/

#ifndef __KERNEL__
#include <windows.h>
#define ULONG_MAX    0xffffffffUL
#else
#include <linux/module.h>
#include <linux/init.h>
#include <generated/autoconf.h>
#include <linux/random.h>
#endif
#include "aes.h"
#include "fips_impl.h"
#include <linux/string.h>
#include "sha1.h"
#include "dot11i-impl-inc.h"
#include "dot11i-impl.h"
#include "al.h"

#define ENC_BLOCK_SIZE    128

#define MEMSET_ARRAY(m)    memset(m, 0, sizeof(m))
#define MIN(a, b)          (((a) < (b)) ? (a) : (b))

int _get_current_system_state(unsigned char *output, int len);
int _get_current_day_and_time(unsigned char *output, int copylen);
int _generate_seed(int seed_length, unsigned char *seed);
int get_pseudo_random_number(int sd_len, unsigned char *s, unsigned char *k, unsigned char *d, unsigned char *x);

static aes_ctx       s_aes_context;
static unsigned long counter;
static unsigned char p       [BITS128];
static unsigned char seed[BITS128];
static unsigned char aeskey[BITS128];
static unsigned char seed_key[BITS256];

unsigned char known_plain_text[] =
{
   0x30, 0x53, 0x4e, 0x58, 0xaa, 0xe5, 0xca, 0xd7, 0xad, 0x1b, 0x9b, 0x83, 0xa1, 0xfd, 0x14, 0xc7,
   0x1f, 0x97, 0x0b, 0xde, 0x5e, 0xb1, 0x28, 0x25, 0x88, 0xcc, 0x48, 0x8a, 0x34, 0x07, 0x2c, 0x98,
   0x7c, 0xae, 0xf4, 0x1a, 0x36, 0xb3, 0x45, 0x05, 0x58, 0x03, 0x85, 0xd1, 0x0b, 0xc7, 0x50, 0x85,
   0x4e, 0x8a, 0xe7, 0xb9, 0xe0, 0xfc, 0x28, 0x77, 0x8f, 0x34, 0x49, 0x79, 0xf0, 0x65, 0x26, 0x12,
   0x3e, 0x6e, 0xd9, 0x73, 0x07, 0x20, 0x81, 0xfa, 0xb3, 0xe0, 0x09, 0x9d, 0x5c, 0x9c, 0x27, 0x54,
   0x39, 0x0a, 0xb0, 0xe2, 0x54, 0x07, 0x29, 0xd3, 0x2f, 0x51, 0x94, 0x35, 0xf0, 0x3b, 0x40, 0x60,
   0x44, 0x64, 0xb1, 0x27, 0xe0, 0xfc, 0xca, 0xd7, 0xe2, 0x2c, 0x1a, 0xe6, 0x57, 0x5c, 0x9c, 0x27,
   0x34, 0x8a, 0xe7, 0xb9, 0xe0, 0xfc, 0x28, 0x77, 0x8f, 0x51, 0x94, 0x35, 0xf0, 0x3b, 0x26, 0x00
};

unsigned char known_cipher_text[] =
{
   0x7f, 0x0e, 0xc2, 0x81, 0x45, 0x20, 0xf5, 0xb4, 0x87, 0x14, 0x7a, 0x61, 0xe0, 0x77, 0xf2, 0x98,
   0x59, 0xf2, 0xc4, 0xa9, 0x3c, 0xd7, 0x9c, 0xc6, 0xd8, 0x7e, 0x8d, 0xab, 0xed, 0xa6, 0x2c, 0xec,
   0xb7, 0xbe, 0x2f, 0xb8, 0x8c, 0xdb, 0xd0, 0x17, 0xa0, 0x8a, 0x7c, 0x4a, 0x07, 0x13, 0x67, 0xb9,
   0xd6, 0xb9, 0xfa, 0xaa, 0x6d, 0x02, 0xa1, 0x5c, 0x2f, 0xf1, 0x58, 0x96, 0x3e, 0x75, 0xcf, 0xeb,
   0x41, 0xdb, 0xbd, 0x1c, 0x25, 0x5f, 0xb4, 0x7c, 0xff, 0x43, 0x94, 0x91, 0x9b, 0x37, 0x1e, 0x71,
   0x5d, 0x91, 0x98, 0xc0, 0x7a, 0x9a, 0x48, 0xb1, 0x33, 0xfa, 0x74, 0x0d, 0xb8, 0x4c, 0xe2, 0x77,
   0x27, 0x06, 0xfa, 0x32, 0x14, 0x4c, 0x0f, 0xf2, 0xde, 0xac, 0x59, 0xe3, 0xac, 0x54, 0x25, 0x1d,
   0x2d, 0x17, 0xcb, 0xfa, 0x54, 0xc6, 0x3d, 0x5f, 0x9c, 0x3a, 0xc1, 0x31, 0x02, 0xe8, 0x8f, 0xf6
};

unsigned char hmac_sha1_output[] =
{
   0x13, 0x7f, 0x7c, 0x46, 0x3b, 0xf8, 0x60, 0x59, 0x08, 0x08, 0x06, 0x4b, 0xce, 0x76, 0x51, 0xfd,
   0xc4, 0x50, 0x67, 0xde, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
   0x0,   0x0,  0x0,  0x0,  0x0,  0x0,  0x0,  0x0,  0x0,  0x0,  0x0,  0x0,  0x0,  0x0,  0x0,  0x0, 0x0, 0x0, 0x0, 0x0,
   0x0,   0x0,  0x0,  0x0,  0x0,  0x0,  0x0,  0x0,  0x0,  0x0,  0x0,  0x0,  0x0,  0x0,  0x0,  0x0, 0x0, 0x0, 0x0, 0x0,
   0x0,   0x0,  0x0,  0x0,  0x0,  0x0,  0x0,  0x0,  0x0,  0x0,  0x0,  0x0,  0x0,  0x0,  0x0,  0x0, 0x0, 0x0, 0x0, 0x0,
   0x0,   0x0,  0x0,  0x0,  0x0,  0x0,  0x0,  0x0,  0x0,  0x0,  0x0,  0x0,  0x0,  0x0,  0x0,  0x0, 0x0, 0x0, 0x0, 0x0,
   0x0,   0x0,  0x0,  0x0,  0x0,  0x0,  0x0,  0x0,  0x0,  0x0,  0x0,  0x0,  0x0,  0x0,  0x0, 0x0
};

unsigned char key[] =
{
   0x99, 0x0, 0x12, 0x36, 0x17, 0x5b, 0x29, 0xed, 0x3a, 0xd3, 0xe6, 0x69, 0xdf, 0x60, 0xa0, 0xf4
};

unsigned char rng_self_test_aes_key[] =
{
   0x07, 0x2f, 0x52, 0xc4, 0xcd, 0xc7, 0x03, 0x82,
   0x5f, 0xec, 0x48, 0xdb, 0x23, 0x78, 0x61, 0x9a
};

unsigned char rng_self_test_dt[] =
{
   0xbf, 0x4d, 0xce, 0xbb, 0xd4, 0x5d, 0xf4, 0x8a,
   0xe5, 0xf1, 0x21, 0x86, 0xa9, 0xb5, 0xe4, 0x4f
};

unsigned char rng_self_test_seed[] =
{
   0xba, 0x3b, 0xf7, 0xba, 0x7e, 0x3b, 0x5c, 0xa1,
   0xc8, 0xa2, 0x91, 0xfd, 0x02, 0x49, 0xe2, 0x79
};

unsigned char rng_self_test_known_answer[] =
{
   0x55, 0x11, 0x79, 0x4D, 0xF7, 0x01, 0x97, 0xC9,
   0xBF, 0xD8, 0x30, 0x89, 0x9D, 0xCE, 0x48, 0x4F
};

int do_power_on_self_test_rng()
{
   unsigned char output[BITS128];

   memset(output, 0, BITS128);

   get_pseudo_random_number(sizeof(rng_self_test_seed),
                            rng_self_test_seed,
                            rng_self_test_aes_key,
                            rng_self_test_dt,
                            output);

   if (memcmp(output, rng_self_test_known_answer, BITS128))
   {
      return RNG_SELF_TEST_ERROR;
   }

   return 0;
}


int do_power_on_self_test_aes()
{
   int           i, n, b;
   unsigned char cipher_text       [ENC_BLOCK_SIZE];
   unsigned char output            [ENC_BLOCK_SIZE];
   unsigned char cout                      [AES_BLOCK_SIZE];
   unsigned char pout                      [AES_BLOCK_SIZE];

   MEMSET_ARRAY(output);
   MEMSET_ARRAY(cout);
   MEMSET_ARRAY(pout);
   MEMSET_ARRAY(cipher_text);

   AES_SET_KEY(key, AES_KEY_SIZE)

   n = sizeof(known_plain_text);
   b = i = 0;

   while (n > 0)
   {
      b = MIN(n, AES_BLOCK_SIZE);
      MEMSET_ARRAY(cout);
      MEMSET_ARRAY(pout);
      memcpy(pout, known_plain_text + i, b);
      AES_ENCRYPT(pout, cout)
      memcpy(cipher_text + i, cout, b);
      n = n - b;
      i = i + b;
   }

   if (memcmp(cipher_text, known_cipher_text, ENC_BLOCK_SIZE) != 0)
   {
      return AES_TEST_ERROR;
   }

   n = sizeof(known_cipher_text);
   b = i = 0;
   while (n > 0)
   {
      b = MIN(n, AES_BLOCK_SIZE);
      MEMSET_ARRAY(pout);
      MEMSET_ARRAY(cout);
      memcpy(cout, known_cipher_text + i, b);
      AES_DECRYPT(cout, pout);
      memcpy(output + i, pout, b);
      n = n - b;
      i = i + b;
   }
   if (memcmp(output, known_plain_text, ENC_BLOCK_SIZE) != 0)
   {
      return AES_TEST_ERROR;
   }
   return 0;
}


int do_power_on_self_test_hmac_sha1()
{
   unsigned char output [ENC_BLOCK_SIZE];
   int           ret;

   MEMSET_ARRAY(output);
   ret = dot11i_hmac_sha1(known_plain_text, sizeof(known_plain_text), key, sizeof(key), output);
   if (memcmp(hmac_sha1_output, output, ENC_BLOCK_SIZE) != 0)
   {
      return HMAC_TEST_ERROR;
   }

   return 0;
}


/**
 * According to ANSI X9.31 Appendix A.2.4 Using AES
 **/
int get_pseudo_random_number(int sd_len, unsigned char *s, unsigned char *k, unsigned char *d, unsigned char *x)
{
   unsigned char i[BITS128], out[BITS128];

   MEMSET_ARRAY(out);
   MEMSET_ARRAY(i);

   AES_SET_KEY(k, AES_KEY_SIZE)     /*step 1*/
   AES_ENCRYPT(d, i)

   XOR(i, s, out, sd_len)                       /*step 2*/
   AES_ENCRYPT(out, x)

   MEMSET_ARRAY(out);                           /*step 3*/
   XOR(x, i, out, sd_len)

   MEMSET_ARRAY(s);
   AES_ENCRYPT(out, s)

   return 0;
}


int init_fips_compliant_mode()
{
   MEMSET_ARRAY(p);
   MEMSET_ARRAY(seed);
   MEMSET_ARRAY(aeskey);

   get_random_bytes(seed_key, BITS256);
   memcpy(aeskey, seed_key, BITS128);
   _generate_seed(BITS128, seed);

   /*
    *    This call is to save the first generated random number
    *  for future comparisons.
    */
   get_fips_compliant_random_bytes(p, BITS128);

   return 0;
}


int get_fips_compliant_random_bytes(unsigned char *buffer, int length)
{
   unsigned char dt[BITS128], random[BITS128];
   unsigned long i, r, b;

   if (buffer == NULL)
   {
      return -1;
   }

   /*
    *  if seed and seed key match then we reboot
    */
   if (memcmp(seed, aeskey, BITS128) == 0)
   {
      return RANDOM_NUMBER_REPEATED;
   }

   MEMSET_ARRAY(dt);
   _get_current_day_and_time((unsigned char *)&dt, BITS128);

   i = 0;
   r = length;

   while (r > 0)
   {
      get_pseudo_random_number(sizeof(seed), seed, aeskey, dt, random);

      if (!memcmp(random, p, BITS128))
      {
         return RANDOM_NUMBER_REPEATED;
      }

      b = MIN(r, 8);

      memcpy(buffer + i, random, b);

      r = r - b;
      i = i + b;

      memcpy(p, random, BITS128);
   }

   return 0;
}


int _generate_seed(int seed_length, unsigned char *seed)
{
#define SYS_STATE_LEN    44

   SHA1Context tctx;

   unsigned char dt                              [BITS64];
   unsigned char sys_state               [SYS_STATE_LEN];
   unsigned char message_digest  [BITS512];
   unsigned char conv_digest             [BITS512];
   unsigned long cntr;
   int           b, r, i, len, ni = 0;

   b = i = len = 0;

   len  = r = seed_length;
   cntr = counter;

   if (cntr == ULONG_MAX)
   {
      cntr = counter = 0;
   }
   cntr += 1;

   MEMSET_ARRAY(message_digest);

   MEMSET_ARRAY(dt);
   _get_current_day_and_time((unsigned char *)&dt, BITS64);
   if (len >= sizeof(dt))
   {
      memcpy(message_digest + ni, (unsigned char *)dt, sizeof(dt));
      ni  += sizeof(dt);
      len -= sizeof(dt);
   }

   _get_current_system_state(sys_state, sizeof(sys_state));
   if (len >= sizeof(sys_state))
   {
      memcpy(message_digest + ni, (unsigned char *)sys_state, sizeof(sys_state));
      ni  += sizeof(sys_state);
      len -= sizeof(sys_state);
   }
   if (len >= sizeof(counter))
   {
      memcpy(message_digest + ni, (unsigned char *)&counter, sizeof(unsigned long));
      ni  += sizeof(unsigned long);
      len -= sizeof(unsigned long);
   }
   if (len >= BITS64)
   {
      get_random_bytes(message_digest + ni, BITS64);
      ni  += BITS64;
      len -= BITS64;
   }
   while (r > 0)
   {
      SHA1Init(&tctx);
      SHA1Update(&tctx, message_digest, sizeof(message_digest));
      SHA1Final(&tctx, conv_digest);

      memcpy(message_digest, conv_digest, 10);
      b = MIN(r, 8);
      memcpy(seed + i, message_digest, b);
      i += 8;
      r  = r - b;
   }

   counter = cntr;
   return 0;
}


int _get_current_day_and_time(unsigned char *dt, int copylen)
{
#ifdef __KERNEL__
   struct timeval tv;
   do_gettimeofday(&tv);
   memcpy(dt, &tv, sizeof(tv));

#else
   SYSTEMTIME st;

   GetSystemTime(&st);
   memcpy(dt, &st, copylen);
#endif
   return 0;
}


int _get_current_system_state(unsigned char *output, int len)
{
   int ni = 0;

#ifdef __KERNEL__
   /*get linux kernel entropy*/
   get_random_bytes(output, len);

#else
   DWORD pid, tick, tid;

   MEMORYSTATUS ms;
   ni = 0;

   pid = GetCurrentProcessId();
   if (len >= sizeof(DWORD))
   {
      memcpy(output + ni, (const void *)&pid, sizeof(DWORD));
      ni  += sizeof(pid);
      len -= sizeof(DWORD);
   }
   tid = GetCurrentThreadId();
   if (len >= sizeof(DWORD))
   {
      memcpy(output + ni, (const void *)&tid, sizeof(DWORD));
      ni  += sizeof(tid);
      len -= sizeof(DWORD);
   }
   tick = GetTickCount();
   if (len >= sizeof(DWORD))
   {
      memcpy(output + ni, (const void *)&tick, sizeof(tick));
      ni  += sizeof(tick);
      len -= sizeof(DWORD);
   }
   ms.dwLength = sizeof(MEMORYSTATUS);
   GlobalMemoryStatus(&ms);
   if (len >= sizeof(MEMORYSTATUS))
   {
      memcpy(output + ni, (const void *)&ms, sizeof(MEMORYSTATUS));
      ni  += sizeof(MEMORYSTATUS);
      len -= sizeof(MEMORYSTATUS);
   }
#endif
   return ni;
}
