/********************************************************************************
* MeshDynamics
* --------------
* File     : rc4.c
* Comments : RC4 Encryption
* Created  : 3/7/2005
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  0  |3/7/2005  | Adapted                                         | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

#include "rc4.h"

typedef struct arcfourContext_s
{
   unsigned int  x;
   unsigned int  y;
   unsigned char state[256];
} ArcfourContext;

static void arcfour_init(ArcfourContext *ctx, const unsigned char *key,
                         unsigned int key_len);
static RC4_INLINE unsigned int arcfour_byte(ArcfourContext *ctx);
static void arcfour_encrypt(ArcfourContext *ctx, unsigned char *dest,
                            const unsigned char *src, unsigned int len);

void
rc4_encrypt(char *dest, const char *src, unsigned long bufLen,
            const char *keyptr, unsigned long keyLen)
{
   ArcfourContext ctx;

   arcfour_init(&ctx, keyptr, keyLen);
   arcfour_encrypt(&ctx, dest, src, bufLen);
}


/*
 *  This version will skip over the first keyskipbuflen bytes of the
 *  RC4 output stream.
 */
void
rc4_encrypt_skip(char *dest, const char *src, unsigned long bufLen,
                 const char *keyptr, unsigned long keyLen,
                 char *keyskipbuf, unsigned long keyskipbuflen)
{
   ArcfourContext ctx;

   arcfour_init(&ctx, keyptr, keyLen);
   arcfour_encrypt(&ctx, keyskipbuf, keyskipbuf, keyskipbuflen);
   arcfour_encrypt(&ctx, dest, src, bufLen);
}


/*
 *  ARCFOUR from Internet Draft:
 *  http://www.mozilla.org/projects/security/pki/nss/draft-kaukonen-cipher-arcfour-03.txt
 */
static void
arcfour_init(ArcfourContext *ctx, const unsigned char *key,
             unsigned int key_len)
{
   unsigned char t, u;
   unsigned int  keyindex;
   unsigned int  stateindex;
   unsigned char *state;
   unsigned int  counter;

   state  = ctx->state;
   ctx->x = 0;
   ctx->y = 0;
   for (counter = 0; counter < 256; counter++)
   {
      state[counter] = (unsigned char)counter;
   }
   keyindex   = 0;
   stateindex = 0;
   for (counter = 0; counter < 256; counter++)
   {
      t                 = state[counter];
      stateindex        = (stateindex + key[keyindex] + t) & 0xff;
      u                 = state[stateindex];
      state[stateindex] = t;
      state[counter]    = u;
      if (++keyindex >= key_len)
      {
         keyindex = 0;
      }
   }
}


static RC4_INLINE unsigned int
arcfour_byte(ArcfourContext *ctx)
{
   unsigned int  x;
   unsigned int  y;
   unsigned int  sx, sy;
   unsigned char *state;

   state    = ctx->state;
   x        = (ctx->x + 1) & 0xff;
   sx       = state[x];
   y        = (sx + ctx->y) & 0xff;
   sy       = state[y];
   ctx->x   = x;
   ctx->y   = y;
   state[y] = (unsigned char)sx;
   state[x] = (unsigned char)sy;
   return state[(sx + sy) & 0xff];
}


static void
arcfour_encrypt(ArcfourContext *ctx, unsigned char *dest,
                const unsigned char *src, unsigned int len)
{
   unsigned int i;

   for (i = 0; i < len; i++)
   {
      dest[i] = src[i] ^ arcfour_byte(ctx);
   }
}
