/********************************************************************************
* MeshDynamics
* --------------
* File     : dot11i-impl.c
* Comments : Meshap 802.11i implementation
* Created  : 3/25/2005
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  2  |7/25/2007 | Use GEN2K Buffer instead of Heap                | Sriram |
* -----------------------------------------------------------------------------
* |  1  |11/14/2006| Changes for heap debug support                  | Sriram |
* -----------------------------------------------------------------------------
* |  0  |3/25/2005 | Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

#include "access_point.h"
#include "hmac_sha1.h"
#include "rc4.h"
#include "aes_wrap.h"
#include "dot11i-impl.h"

int dot11i_hmac_sha1(unsigned char *input, int input_length, unsigned char *key, int key_len, unsigned char *output)
{
   unsigned char *temp_key;

   temp_key = al_get_gen_2KB_buffer(AL_CONTEXT_SINGLE);

   memcpy(temp_key, key, key_len);

   hmac_sha1(input, input_length, temp_key, key_len, output);

   al_release_gen_2KB_buffer(AL_CONTEXT temp_key);

   return 0;
}


int dot11i_rc4_encrypt(unsigned char *cipher_text,
                       unsigned char *plain_text,
                       int           plain_text_length,
                       unsigned char *key,
                       int           key_len,
                       unsigned char *key_skip,
                       int           key_skip_len)
{
   if ((key_skip != NULL) && (key_skip_len != 0))
   {
      rc4_encrypt_skip(cipher_text,
                       plain_text,
                       plain_text_length,
                       key,
                       key_len,
                       key_skip,
                       key_skip_len);
   }
   else
   {
      rc4_encrypt(cipher_text, plain_text, plain_text_length, key, key_len);
   }

   return 0;
}


int dot11i_aes_keywrap_encrypt(unsigned char *cipher_text,
                               unsigned char *plain_text,
                               int           plain_text_length,
                               unsigned char *key,
                               int           key_len)
{
   aes_wrap(key, (plain_text_length - 8) / 8, plain_text, cipher_text);
   return 0;
}


int dot11i_aes_keywrap_decrypt(unsigned char *plain_text,
                               unsigned char *cipher_text,
                               int           cipher_text_length,
                               unsigned char *key,
                               int           key_len)
{
   return 0;
}


DOT11I_U64 dot11i_cpu_to_be64(DOT11I_U64 in)
{
   DOT11I_U64    out;
   unsigned char *pin;
   unsigned char *pout;
   int           i;

   pin  = (unsigned char *)&in;
   pout = (unsigned char *)&out;

   for (i = 0; i < 8; i++)
   {
      pout[i] = pin[7 - i];
   }

   return out;
}
