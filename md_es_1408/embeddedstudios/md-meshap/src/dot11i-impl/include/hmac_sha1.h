/********************************************************************************
* MeshDynamics
* --------------
* File     : hmac_sha1.h
* Comments : Adapted HMAC-SHA1 Source
* Created  : 3/3/2005
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  0  |3/3/2005 | Adapted                                         | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

#ifndef __HMAC_SHA1_H__
#define __HMAC_SHA1_H__

void hmac_sha1(unsigned char *text, int text_len,
               unsigned char *key, int key_len,
               unsigned char *digest);

#endif /*__HMAC_SHA1_H__*/
