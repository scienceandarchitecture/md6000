/********************************************************************************
* MeshDynamics
* --------------
* File     : rc4.h
* Comments : RC4 Encryption
* Created  : 3/7/2005
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  0  |3/7/2005  | Adapted                                         | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

#ifndef _INC_RC4
#define _INC_RC4

void rc4_encrypt(char *dest, const char *src, unsigned long bufLen,
                 const char *keyptr, unsigned long keyLen);

void rc4_encrypt_skip(char *dest, const char *src, unsigned long bufLen,
                      const char *keyptr, unsigned long keyLen,
                      char *keyskipbuf, unsigned long keyskipbuflen);

#endif /* _INC_RC4 */
