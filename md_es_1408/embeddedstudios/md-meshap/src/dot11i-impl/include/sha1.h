/********************************************************************************
* MeshDynamics
* --------------
* File     : sha1.h
* Comments : Adapted SHA1 source
* Created  : 3/3/2005
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  0  |3/3/2005  | Adapted                                         | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

/*
 *  Description:
 *      This is the header file for code which implements the Secure
 *      Hashing Algorithm 1 as defined in FIPS PUB 180-1 published
 *      April 17, 1995.
 *
 *      Many of the variable names in this code, especially the
 *      single character names, were used because those were the names
 *      used in the publication.
 *
 *      Please read the file sha1.c for more information.
 *
 */

#ifndef _SHA1_H_
#define _SHA1_H_

typedef signed long   int_least16_t;

#ifndef _SHA_enum_
#define _SHA_enum_
enum
{
   shaSuccess = 0,
   shaNull,             /* Null pointer parameter */
   shaInputTooLong,     /* input data too long */
   shaStateError        /* called Input after Result */
};
#endif
#define SHA1HashSize    20

/*
 *  This structure will hold context information for the SHA-1
 *  hashing operation
 */
typedef struct SHA1Context
{
   uint32_t      Intermediate_Hash[SHA1HashSize / 4]; /* Message Digest  */

   uint32_t      Length_Low;                          /* Message length in bits      */
   uint32_t      Length_High;                         /* Message length in bits      */

   /* Index into message block array   */
   int_least16_t Message_Block_Index;
   uint8_t       Message_Block[64]; /* 512-bit message blocks      */

   int           Computed;          /* Is the digest computed?         */
   int           Corrupted;         /* Is the message digest corrupted? */
} SHA1Context;

int SHA1Init(SHA1Context *ctx);
int SHA1Update(SHA1Context *ctx, const uint8_t *in, unsigned len);
int SHA1Final(SHA1Context *ctx, uint8_t digest[]);

#endif /* _SHA1_H_ */
