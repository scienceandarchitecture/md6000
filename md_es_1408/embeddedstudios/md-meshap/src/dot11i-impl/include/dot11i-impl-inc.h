/********************************************************************************
* MeshDynamics
* --------------
* File     : dot11i-impl-inc.h
* Comments : Meshap 802.11i implementation
* Created  : 3/25/2005
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  0  |3/25/2005 | Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

#ifndef __DOT11I_IMPL_INC_H__
#define __DOT11I_IMPL_INC_H__

#include <generated/autoconf.h>

#include <linux/module.h>
#include <linux/init.h>
#include <linux/version.h>
#include <linux/netdevice.h>
#include <linux/skbuff.h>
#include <linux/wireless.h>

#ifndef DOT1X_INLINE
#define DOT1X_INLINE    inline
#endif

typedef char                 A_CHAR;
typedef unsigned char        A_UCHAR;
typedef A_CHAR               A_INT8;
typedef A_UCHAR              A_UINT8;
typedef short                A_INT16;
typedef unsigned short       A_UINT16;
typedef int                  A_INT32;
typedef unsigned int         A_UINT32;
typedef unsigned int         A_UINT;
typedef A_UCHAR              A_BOOL;
typedef unsigned long long   A_UINT64;
#define A_BCOPY(x, y, z)    memcpy(y, x, z)
#define A_BCOMP(x, y, z)    memcmp(x, y, z)
typedef int                  A_STATUS;
#define A_EINVAL    -2
#define A_ERROR     -1
#define A_OK        0
typedef unsigned long long   DOT11I_U64;

#define malloc(s)    kmalloc(s, GFP_ATOMIC)
#define free(m)      kfree(m)

#endif /*__DOT11I_IMPL_INC_H__*/
