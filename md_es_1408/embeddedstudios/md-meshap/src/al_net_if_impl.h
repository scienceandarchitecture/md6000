/********************************************************************************
* MeshDynamics
* --------------
* File     : al_net_if_impl.h
* Comments : Torna MeshAP abstraction layer net_if implementation
* Created  : 5/25/2004
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  1  |1/22/2008 | Changes for Mixed mode                          |Prachiti|
* -----------------------------------------------------------------------------
* |  0  |5/25/2004 | Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

#ifndef __AL_NET_IF_IMPL_H__
#define __AL_NET_IF_IMPL_H__

void al_net_if_initialize_interface(meshap_core_net_if_t *core_net_if);

void al_net_if_initialize_virtual_interface(meshap_core_net_if_t *virtual_core_net_if,
                                            meshap_core_net_if_t *phy_core_net_if);

#endif /*__AL_NET_IF_IMPL_H__*/
