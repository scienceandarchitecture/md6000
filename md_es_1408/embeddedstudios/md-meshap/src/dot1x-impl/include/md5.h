/********************************************************************************
* MeshDynamics
* --------------
* File     : md5.h
* Comments : Adapted MD5 Source
* Created  : 3/3/2005
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  0  |3/3/2005  | Adapted                                         | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

/*
 *
 * This file contains MD5 library declarations.
 */

#ifndef _MD5_H_
#define _MD5_H_

#ifdef _WIN32
typedef unsigned long   A_UINT32;
#else
typedef unsigned long   A_UINT32;
#endif

#ifdef __cplusplus
extern "C" {
#endif

struct MD5Context
{
   A_UINT32      buf[4];
   A_UINT32      bits[2];
   unsigned char in[64];
};

void MD5Init(struct MD5Context *ctx);
void MD5Update(struct MD5Context *ctx, unsigned char *buf, unsigned len);

void MD5Final(unsigned char digest[16], struct MD5Context *ctx);
void MD5Transform(A_UINT32 buf[4], A_UINT32 in[16]);

typedef struct MD5Context   MD5_CTX;

void md5_calc(unsigned char *, unsigned char *, unsigned int);

#ifdef __cplusplus
}
#endif

#endif /* _MD5_H_ */
