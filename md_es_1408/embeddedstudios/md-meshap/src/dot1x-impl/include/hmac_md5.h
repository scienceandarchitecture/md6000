/********************************************************************************
* MeshDynamics
* --------------
* File     : hmac_md5.h
* Comments : Adapted HMAC-MD5  Source
* Created  : 3/3/2005
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  0  |3/3/2005  | Adapted                                         | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

#ifndef __HMAC_MD5_H__
#define __HMAC_MD5_H__

void hmac_md5(
   unsigned char *text,                 /* pointer to data stream */
   int           text_len,              /* length of data stream */
   unsigned char *key,                  /* pointer to authentication key */
   int           key_len,               /* length of authentication key */
   unsigned char *digest                /* caller digest to be filled in */
   );

#endif /*__HMAC_MD5_H__*/
