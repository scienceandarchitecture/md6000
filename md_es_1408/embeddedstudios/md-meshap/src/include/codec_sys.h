/********************************************************************************
* MeshDynamics
* --------------
* File     : codec_sys.h
* Comments : Abstraction for codecs
* Created  : 11/17/2004
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  2  |11/16/2006| Added General buffer allocation functions       | Sriram |
* -----------------------------------------------------------------------------
* |  1  |12/14/2004| Added le32 conversion macro                     | Sriram |
* -----------------------------------------------------------------------------
* |  0  |11/17/2004| Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

#ifndef __CODEC_SYS_H__
#define __CODEC_SYS_H__

void *codec_alloc(unsigned int size);
void codec_free(void *memblock);
unsigned char *codec_get_gen_buffer(void);
void codec_release_gen_buffer(unsigned char *buffer);

#ifdef CODEC_SYS_BIG_ENDIAN
#define CODEC_SYS_CONVERT_TO_LE32(l)      (((l & 0x000000FF) << 24) | ((l & 0x0000FF00) << 8) | ((l & 0x00FF0000) >> 8) | ((l & 0xFF000000) >> 24))
#else
#define CODEC_SYS_CONVERT_TO_LE32(l)      (l)
#endif

#define CODEC_SYS_CONVERT_FROM_LE32(l)    CODEC_SYS_CONVERT_TO_LE32(l)

#endif /*__CODEC_SYS_H__*/
