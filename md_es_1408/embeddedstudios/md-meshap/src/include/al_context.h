/********************************************************************************
* MeshDynamics
* --------------
* File     : al_context.h
* Comments : Abstraction Layer context file
* Created  : 4/22/2004
* Author   : Bindu Parakala
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  1  |4/22/2004 | Added AL_DECLARE_GLOBAL_EXTERN                  | Sriram |
* -----------------------------------------------------------------------------
* |  0  |4/22/2004 | Created                                         | Bindu P|
* -----------------------------------------------------------------------------
********************************************************************************/


#ifndef __AL_CONTEXT_H__
#define __AL_CONTEXT_H__

#ifndef _AL_ROVER_
#define AL_DECLARE_GLOBAL(decl)           decl
#define AL_DECLARE_GLOBAL_EXTERN(decl)    extern decl
#define AL_CONTEXT_PARAM_DECL
#define AL_CONTEXT_PARAM_DECL_SINGLE    void
#define AL_CONTEXT
#define AL_CONTEXT_SINGLE
#else
struct al_impl_context;
typedef struct al_impl_context   al_impl_context_t;
typedef al_impl_context_t        al_context_t;
#define AL_DECLARE_GLOBAL(decl)
#define AL_DECLARE_GLOBAL_EXTERN(decl)
#define AL_CONTEXT_PARAM_DECL           al_context_t * al_context,
#define AL_CONTEXT_PARAM_DECL_SINGLE    al_context_t * al_context
#define AL_CONTEXT                      al_context,
#define AL_CONTEXT_SINGLE               al_context
#endif

#endif /*__AL_CONTEXT_H__*/
