/********************************************************************************
* MeshDynamics
* --------------
* File     : al_net_if.h
* Comments : Abstraction Layer Network Interface Header
* Created  : 4/5/2004
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* | 11  |1/29/2007 | Changes for Allowed VLAN Tag                    | Sriram |
* -----------------------------------------------------------------------------
* | 10  |4/12/2005 | set_hw_addr method added                        | Sriram |
* -----------------------------------------------------------------------------
* |  9  |4/6/2005  | Added ext_config_info                           | Sriram |
* -----------------------------------------------------------------------------
* |  8  |6/15/2004 | Added macro for encapsulation check             | Sriram |
* -----------------------------------------------------------------------------
* |  7  |6/11/2004 | Added transmit_ex                               | Sriram |
* -----------------------------------------------------------------------------
* |  6  |6/10/2004 | Added last_rx and last_tx time                  | Sriram |
* -----------------------------------------------------------------------------
* |  5  |5/25/2004 | Link states added.                              | Anand  |
* -----------------------------------------------------------------------------
* |  4  |5/11/2004 | start & stop function p added to al_net_if      | Anand  |
* -----------------------------------------------------------------------------
* |  3  |4/19/2004 | Added PHY hardware types                        | Sriram |
* -----------------------------------------------------------------------------
* |  2  |4/19/2004 | Added data members to al_net_if                 | Sriram |
* -----------------------------------------------------------------------------
* |  1  |4/8/2004  | Fixed misc syntax errors                        | Bindu P|
* -----------------------------------------------------------------------------
* |  0  |4/5/2004  | Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/
#ifndef __AL_NET_IF_H__
#define __AL_NET_IF_H__

#include "al_ip.h"
#include "al_types.h"
#include "al_net_addr.h"
#include "al_packet.h"
//#include <atheros_instance.h>


#define AL_NET_IF_CONFIG_ALLOWED_VLAN_NONE    0x0000
#define AL_NET_IF_CONFIG_ALLOWED_VLAN_ALL     0xFFFF

struct al_net_if_config
{
   al_net_addr_t  hw_addr;

   /**
    * IP Specific Information.
    */
   int            ip_addr_assignment;
   al_ip_addr_t   ip_addr;
   al_ip_addr_t   mask;
   al_ip_addr_t   gateway;
   al_ip_addr_t   broadcast;
   al_ip_addr_t   primary_dns;
   al_ip_addr_t   sec_dns;
   unsigned short allowed_vlan_tag;                     /* AL_NET_IF_CONFIG_ALLOWED_VLAN_* or a specific tag */
   unsigned char  default_flag;
   void           *ext_config_info;
};

typedef struct al_net_if_config   al_net_if_config_t;

#define TX_FRAME_SIZE 0xFF
#define RX_FRAME_SIZE 15

struct al_net_if_stat
{
   unsigned long rx_packets;                  /* total packets received       */
   unsigned long tx_packets;                  /* total packets transmitted    */
   unsigned long rx_bytes;                    /* total bytes received         */
   unsigned long tx_bytes;                    /* total bytes transmitted      */
   unsigned long rx_errors;                   /* bad packets received         */
   unsigned long tx_errors;                   /* packet transmit problems     */
   unsigned long rx_dropped;                  /* no space in buffers    */
   unsigned long tx_dropped;                  /* no space available  */
   unsigned long rx_mgmt;                     /* Total manangement frames  */
   unsigned long rx_data;                     /* Total data frames  */
   unsigned long total_tx_mgmt[TX_FRAME_SIZE];
   unsigned long total_tx_data[TX_FRAME_SIZE];
   unsigned long total_rx_mgmt[RX_FRAME_SIZE];
   unsigned long total_rx_data[RX_FRAME_SIZE];
};

typedef struct al_net_if_stat   al_net_if_stat_t;

struct al_net_if_ht_vht_capab
{
       void* ht_vht_capabilities;
       int sec_chan_offset ; 
    unsigned char  a_mpdu_params; /* Maximum A-MPDU Length Exponent B0..B1
                       * Minimum MPDU Start Spacing B2..B4
                       *                * Reserved B5..B7 */
};

typedef struct al_net_if_ht_vht_capab  al_net_if_ht_vht_capab_t;

struct al_net_if_vht_operation {
    unsigned char  vht_op_info_chwidth;
    unsigned char  vht_op_info_chan_center_freq_seg0_idx;
    unsigned char  vht_op_info_chan_center_freq_seg1_idx;
    unsigned short int  vht_basic_mcs_set;
};

typedef struct al_net_if_vht_operation al_net_if_vht_operation_t;


#define AL_NET_IF_ENCAPSULATION_ETHERNET     0
#define AL_NET_IF_ENCAPSULATION_PPP          1
#define AL_NET_IF_ENCAPSULATION_802_3        2
#define AL_NET_IF_ENCAPSULATION_802_11       3

#define AL_NET_IF_FLAGS_UP                   0x00000001         /* Interface is up		     */
#define AL_NET_IF_FLAGS_LOOPBACK             0x00000002         /* is a loopback net		     */
#define AL_NET_IF_FLAGS_POINTOPOINT          0x00000004         /* interface is has p-p link	     */
#define AL_NET_IF_FLAGS_NOARP                0x00000008         /* no ARP protocol		     */
#define AL_NET_IF_FLAGS_PROMISC              0x00000010         /* receive all packets		     */
#define AL_NET_IF_FLAGS_ALLMULTI             0x00000020         /* receive all multicast packets     */
#define AL_NET_IF_FLAGS_MULTICAST            0x00000040         /* Supports multicast		     */
#define AL_NET_IF_FLAGS_ASSOCIATED           0x00000080         /* for wlan stations                     */
#define AL_NET_IF_FLAGS_ACCESS_POINT         0x00000100         /* for wlan access points                */
#define AL_NET_IF_FLAGS_TX_QUEUE_STOPPED     0x00000200         /* TX queue stopped until further notice */
#define AL_NET_IF_FLAGS_LINKED               0x00000400         /* Whether the interface is linked	 */
#define AL_NET_IF_FLAGS_BROADCAST            0x00000800         /* receive all broadcast packets	 */

#define AL_NET_IF_FILTER_MODE_UNICAST        1
#define AL_NET_IF_FILTER_MODE_MULTICAST      2
#define AL_NET_IF_FILTER_MODE_BROADCAST      4
#define AL_NET_IF_FILTER_MODE_PROMISCOUS     8

#define AL_NET_IF_PHY_HW_TYPE_802_11_B       1
#define AL_NET_IF_PHY_HW_TYPE_802_11_G_B     1
#define AL_NET_IF_PHY_HW_TYPE_802_11_A       2
#define AL_NET_IF_PHY_HW_TYPE_802_3          3

#define AL_NET_IF_PHY_HW_TYPE_802_11_N      10
#define AL_NET_IF_PHY_HW_TYPE_802_11_AC     11
#define AL_NET_IF_PHY_HW_TYPE_802_11_BGN    12
#define AL_NET_IF_PHY_HW_TYPE_802_11_AN     13
#define AL_NET_IF_PHY_HW_TYPE_802_11_ANAC   14


#define AL_NET_IF_LINK_STATE_NOT_LINKED      0
#define AL_NET_IF_LINK_STATE_LINKED          1

#define AL_NET_IF_TRANSMIT_TYPE_IMMEDIATE    1
#define AL_NET_IF_TRANSMIT_TYPE_QUEUED       2

#define AL_NET_IF_BUFFERING_STATE_NONE       0
#define AL_NET_IF_BUFFERING_STATE_REQUEUE    1
#define AL_NET_IF_BUFFERING_STATE_DROP       2

#define CURRENT_ESSID_MAX_SIZE      32
#include "torna_wlan.h"

struct _debug_infrastruct_info {
     char                      current_essid[CURRENT_ESSID_MAX_SIZE + 1];
     int                       current_essid_length;
     unsigned char             beacon_vendor_info[256];
     unsigned char             beacon_vendor_info_length;
     al_net_addr_t             current_bssid;
     unsigned char             connected;
     unsigned long             scan_start_time;
     unsigned long             scan_completion_time;
     int                       current_phy_mode;
     int                       iw_mode;
     int                       use_type;
     int                       beacon_conf_complete;
     int                       channel_count;
     int                       beacon_interval;
     int                       rts_threshold;
     int                       frag_threshold;
     int                       tx_power;
     int                       sec_chan_offset;
     int                       scan_state;
     int                       country_code;
     int                       fsm_state;
     unsigned char             a_mpdu_params;
     unsigned short            ht_capab_info;
     unsigned short            ht_opera;
     unsigned int              vht_capab_info;
     wlan_channel_t            current_channel;
     al_net_if_vht_operation_t    vht_opera ;
};

typedef struct _debug_infrastruct_info      debug_infra_info_t;

//Structure for printing the vendor info buffer
struct _vendor_info {
  unsigned char packet[5];
  int version_major;
  int version_minor;
  int meshid_length;
  unsigned char meshid[256];
  unsigned int ctc;
  unsigned int hpc;
  unsigned int crssi;
  unsigned int hbi;
  unsigned int ds_if_channel;
  unsigned int ds_if_sub_type;
  al_net_addr_t ds_mac;
  al_net_addr_t root_bssid;
  unsigned int mode;
  unsigned int phy_sub_type;
};

typedef struct _vendor_info   vendor_info_t;

struct al_net_if
{
   al_net_if_config_t config;
   char               name[16];
   int                encapsulation;
   int                arp_hw_type;
   int                mtu;
   al_atomic_t        buffering_state;
   int                phy_hw_type;
   al_u64_t           last_rx_time;
   al_u64_t           last_tx_time;
   int                (*get_name)(struct al_net_if *al_net_if, char *buffer, int length);
   int                (*get_encapsulation)(struct al_net_if *al_net_if);
   int                (*get_arp_hw_type)(struct al_net_if *al_net_if);
   int                (*get_hw_addr_size)(struct al_net_if *al_net_if);
   int                (*get_mtu)(struct al_net_if *al_net_if);
   int                (*get_config)(struct al_net_if *al_net_if, al_net_if_config_t *config);
   int                (*get_stats)(struct al_net_if *al_net_if, al_net_if_stat_t *stats);
   int                (*get_debug_info)(struct al_net_if *al_net_if, debug_infra_info_t *debug_infra_info);
   int                (*get_last_rx_time)(struct al_net_if *al_net_if, al_u64_t *last_rx_time);
   int                (*get_last_tx_time)(struct al_net_if *al_net_if, al_u64_t *last_tx_time);
   int                (*get_flags)(struct al_net_if *al_net_if);
   int                (*transmit)(struct al_net_if *al_net_if, al_packet_t *packet);
   int                (*set_filter_mode)(struct al_net_if *al_net_if, int filter_mode);
   void               * (*get_extended_operations)(struct al_net_if *al_net_if);
   int                (*start)(struct al_net_if *al_net_if);
   int                (*stop)(struct al_net_if *al_net_if);
   int                (*transmit_ex)(struct al_net_if *al_net_if, al_packet_t *packet, int transmit_type);
   int                (*set_hw_addr)(struct al_net_if *al_net_if, al_net_addr_t *hw_addr);
};

typedef struct al_net_if   al_net_if_t;


#define AL_NET_IF_IS_WIRELESS(net_if) \
   (net_if->encapsulation == AL_NET_IF_ENCAPSULATION_802_11)
#define AL_NET_IF_IS_ETHERNET(net_if)                           \
   ((net_if->encapsulation == AL_NET_IF_ENCAPSULATION_ETHERNET) \
    || (net_if->encapsulation == AL_NET_IF_ENCAPSULATION_802_3))
#endif /*__AL_NET_IF_H__*/
