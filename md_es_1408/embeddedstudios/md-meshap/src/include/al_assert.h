/********************************************************************************
* MeshDynamics
* --------------
* File     : al_assert.h
* Comments : Abstraction layer ASSERT utilities
* Created  : 6/13/2004
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  0  |6/13/2004 | Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

#ifndef __AL_ASSERT_H__
#define __AL_ASSERT_H__

#ifdef _AL_ASSERT_ENABLED

#define AL_ASSERT(desc, cond)                                                                                                            \
   if (!(cond)) {                                                                                                                        \
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, desc " : ASSERTION " # cond " failed at line %d in file %s", __LINE__, __FILE__); \
   }

#else

#define AL_ASSERT(desc, cond)
#endif         /* _AL_ASSERT_ENABLED */

#endif /*__AL_ASSERT_H__*/
