/********************************************************************************
* MeshDynamics
* --------------
* File     : al_net_addr.h
* Comments : Abstraction Layer  MAC address structure
* Created  : 4/5/2004
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  5  |3/25/2005 | AL_NET_ADDR_IS_PAE_GROUP added                  | Sriram |
* -----------------------------------------------------------------------------
* |  4  |1/7/2005  | AL_NET_ADDR_IS_BROADCAST fixed                  | Sriram |
* -----------------------------------------------------------------------------
* |  3  |6/4/2004  | Added AL_NET_ADDR_STR macros                    | Sriram |
* -----------------------------------------------------------------------------
* |  2  |5/18/2004 | changed AL_NET_ADDR_IS_BROADCAST macro          | Bindu  |
* -----------------------------------------------------------------------------
* |  1  |4/13/2004 | Added AL_NET_ADDR_EQUAL macro                   | Bindu  |
* -----------------------------------------------------------------------------
* |  0  |4/5/2004  | Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/


#ifndef __AL_NET_ADDR_H__
#define __AL_NET_ADDR_H__

struct al_net_addr
{
   unsigned char bytes[8];
   int           length;
};

typedef struct al_net_addr   al_net_addr_t;

#define AL_NET_ADDR_IS_MULTICAST(addr) \
   (                                   \
      (addr)->bytes[0] == 0x01 &&      \
      (addr)->bytes[1] == 0x00 &&      \
      (addr)->bytes[2] == 0x5E         \
   )

#define AL_NET_ADDR_IS_PAE_GROUP(addr) \
   (!memcmp(pae_group_addr.bytes, (addr)->bytes, 6))

#define AL_NET_ADDR_IS_BROADCAST(addr) \
   (!memcmp(broadcast_net_addr.bytes, (addr)->bytes, 6))

#define AL_NET_ADDR_EQUAL(addr1, addr2) \
   (((addr1)->length == (addr2)->length) && (!memcmp((addr1)->bytes, (addr2)->bytes, (addr1)->length)))

extern al_net_addr_t broadcast_net_addr;
extern al_net_addr_t zeroed_net_addr;

#define AL_NET_ADDR_STR    "%02x:%02x:%02x:%02x:%02x:%02x"
#define AL_NET_ADDR_TO_STR(a)    (a)->bytes[0], (a)->bytes[1], (a)->bytes[2], (a)->bytes[3], (a)->bytes[4], (a)->bytes[5]

#endif /*__AL_NET_ADDR_H__*/
