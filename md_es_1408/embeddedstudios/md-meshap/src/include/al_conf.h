/********************************************************************************
* MeshDynamics
* --------------
* File     : al_conf.h
* Comments : Mesh AP configuration file parser
* Created  : 4/23/2004
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  31 |8/27/2008 | Changes for options                             | Sriram |
* -----------------------------------------------------------------------------
* |  30 |11/8/2007 | Changes for Effistream queued retry             |Prachiti|
* -----------------------------------------------------------------------------
* |  29 |6/6/2007  | Changes for Effistream bitrate                  |Prachiti|
* -----------------------------------------------------------------------------
* |  28 |2/22/2007 | Changes for Effistream                          | Sriram |
* -----------------------------------------------------------------------------
* |  27 |2/15/2007 | Added txantenna                                 | Sriram |
* -----------------------------------------------------------------------------
* |  26 |2/14/2007 | Added PS PHY sub-types                          | Sriram |
* -----------------------------------------------------------------------------
* |  25 |2/9/2007  | In-direct VLAN membership                       | Sriram |
* -----------------------------------------------------------------------------
* |  24 |01/01/2007| Changes for channel specific DFS                |Prachiti|
* -----------------------------------------------------------------------------
* |  23 |10/31/2006| Changes for private frequencies                 | Sriram |
* -----------------------------------------------------------------------------
* |  22 |10/18/2006| Changes for ETSI Radar                          | Sriram |
* -----------------------------------------------------------------------------
* |  21 |02/15/2006| dot11e added				                      | Bindu  |
* -----------------------------------------------------------------------------
* |  20 |02/08/2006| Config SQNR and Hide SSID added				  | Sriram |
* -----------------------------------------------------------------------------
* |  19 |11/11/2005| ACK_Timeout added								  |Prachiti|
* -----------------------------------------------------------------------------
* |  18 |11/11/2005| Regulatory Domain and Country code added        |Prachiti|
* -----------------------------------------------------------------------------
* |  17 |6/9/2005  | TX Rate added to VLAN                           | Sriram |
* -----------------------------------------------------------------------------
* |  16 |5/19/2005 | txrate added in if_info		                  | Abhijit|
* -----------------------------------------------------------------------------
* |  15 |5/19/2005 | fcc_enabled,etsi_enabled added                  | Abhijit|
* -----------------------------------------------------------------------------
* |  14 |5/8/2005  | Changes for GPS/description                     | Sriram |
* -----------------------------------------------------------------------------
* |  13 |4/26/2005 | Model Name added                                |Prachiti|
* -----------------------------------------------------------------------------
* |  12 |4/15/2005 | radius_secret_key chaged to array               |Prachiti|
* -----------------------------------------------------------------------------
* |  11 |2/08/2005 | structure for vlan parsing added                |Prachiti|
* -----------------------------------------------------------------------------
* |  10 |2/07/2005 | structure for  security parsing added	          |Prachiti|
* -----------------------------------------------------------------------------
* |  9  |1/8/2005  | Changes for per-interface essid/etc             | Sriram |
* -----------------------------------------------------------------------------
* |  8  |11/10/2004| preamble_type,slot_time_type added              | Sriram |
* -----------------------------------------------------------------------------
* |  7  |10/15/2004| added al_conf_open                              | Abhijit|
* -----------------------------------------------------------------------------
* |  6  |10/5/2004 | Additions of al_conf_set functions              | Abhijit|
* -----------------------------------------------------------------------------
* |  5  |9/28/2004 | Changes for 3 radio                             | Sriram |
* -----------------------------------------------------------------------------
* |  4  |7/19/2004 | Implemented dca_list property                   | Sriram |
* -----------------------------------------------------------------------------
* |  3  |7/19/2004 | Implemented preferred_parent and name properties| Sriram |
* -----------------------------------------------------------------------------
* |  2  |4/24/2004 | Added if_index param to al_conf_get_if          | Sriram |
* -----------------------------------------------------------------------------
* |  1  |4/23/2004 | Changed int to unsigned char in al_conf_if_info | Sriram |
* -----------------------------------------------------------------------------
* |  0  |4/23/2004 | Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

#ifndef __AL_CONF_H__
#define __AL_CONF_H__

#define AL_CONF_IF_PHY_TYPE_ETHERNET          0
#define AL_CONF_IF_PHY_TYPE_802_11            1
#define AL_CONF_IF_PHY_TYPE_802_11_VIRTUAL    2


#if (MDE_80211N_SUPPORT || MDE_80211AC_SUPPORT) 

#define AL_CONF_IF_HT_PARAM_ENABLED			  1
#define AL_CONF_IF_HT_PARAM_DISABLED          0

#define AL_CONF_IF_HT_PARAM_40_ABOVE          0
#define AL_CONF_IF_HT_PARAM_40_BELOW          1
#define AL_CONF_IF_HT_PARAM_20                2
#define AL_CONF_IF_HT_PARAM_80                3
 
#define AL_CONF_IF_HT_PARAM_STATIC            0
#define AL_CONF_IF_HT_PARAM_DYNAMIC			  1
#define AL_CONF_IF_HT_PARAM_SMPS_DISABLED	  3

#define AL_CONF_IF_HT_PARAM_LONG              0
#define AL_CONF_IF_HT_PARAM_AUTO			  1
#define AL_CONF_IF_HT_PARAM_SHORT			  2

#define AL_CONF_IF_HT_PARAM_ALLOW             1
#define AL_CONF_IF_HT_PARAM_DENY              0

#define AL_CONF_IF_VHT_PARAM_YES              1
#define AL_CONF_IF_VHT_PARAM_NO               0


#endif

#define AL_CONF_IF_PHY_SUB_TYPE_IGNORE        0
#define AL_CONF_IF_PHY_SUB_TYPE_802_11_A      1
#define AL_CONF_IF_PHY_SUB_TYPE_802_11_B      2
#define AL_CONF_IF_PHY_SUB_TYPE_802_11_G      3
#define AL_CONF_IF_PHY_SUB_TYPE_802_11_BG     4
#define AL_CONF_IF_PHY_SUB_TYPE_802_11_PSQ    5         /* 5 MHz US FCC 4.9 GHz public safety */
#define AL_CONF_IF_PHY_SUB_TYPE_802_11_PSH    6         /* 10 MHz US FCC 4.9 GHz public safety */
#define AL_CONF_IF_PHY_SUB_TYPE_802_11_PSF    7         /* 20 MHz US FCC 4.9 GHz public safety */

#if (MDE_80211N_SUPPORT || MDE_80211AC_SUPPORT) 

#define AL_CONF_IF_PHY_SUB_TYPE_802_11_AC     11
#define AL_CONF_IF_PHY_SUB_TYPE_802_11_BGN    12
#define AL_CONF_IF_PHY_SUB_TYPE_802_11_AN     13
#define AL_CONF_IF_PHY_SUB_TYPE_802_11_ANAC   14
#define AL_CONF_IF_PHY_SUB_TYPE_802_11_N_2_4GHZ      15
#define AL_CONF_IF_PHY_SUB_TYPE_802_11_N_5GHZ   16   

#endif

#define AL_CONF_IF_USE_TYPE_DS                0
#define AL_CONF_IF_USE_TYPE_WM                1
#define AL_CONF_IF_USE_TYPE_PMON              2
#define AL_CONF_IF_USE_TYPE_AMON              3
#define AL_CONF_IF_USE_TYPE_AP                4

#define AL_CONF_IF_BONDING_IGNORE             0
#define AL_CONF_IF_BONDING_SINGLE             1
#define AL_CONF_IF_BONDING_DOUBLE             2

#define AL_CONF_IF_SERVICE_ALL                0
#define AL_CONF_IF_SERVICE_BACKHAUL_ONLY      1
#define AL_CONF_IF_SERVICE_CLIENT_ONLY        2

#define AL_CONF_IF_PREAMBLE_TYPE_LONG         0
#define AL_CONF_IF_PREAMBLE_TYPE_SHORT        1

#define AL_CONF_IF_SLOT_TIME_TYPE_LONG        0
#define AL_CONF_IF_SLOT_TIME_TYPE_SHORT       1

#define AL_802_11_MAX_SECURITY_KEYS           4
#define AL_802_11_MAX_KEY_LENGTH              13

#define AL_CONF_VLAN_INFO_UNTAGGED            -1

#define AL_CONF_DOT11E_CATEGORY_TYPE_AC_BK    0
#define AL_CONF_DOT11E_CATEGORY_TYPE_AC_BE    1
#define AL_CONF_DOT11E_CATEGORY_TYPE_AC_VI    2
#define AL_CONF_DOT11E_CATEGORY_TYPE_AC_VO    3

#define AL_CONF_DHCP_MODE_RANDOM              1
#define AL_CONF_DHCP_MODE_FIXED               2
#define TIME_MULTIPLIER                       1

//PROTOCOL_COMBO bit mapping for complete protocol matrix
#define PROTO_TYPE_A  0x0001
#define PROTO_TYPE_B  0x0002
#define PROTO_TYPE_G  0x0004
//#define PROTO_TYPE_N24GIG 0x0008
//#define PROTO_TYPE_N5GIG  0x0010  TODO::: PROTOCOL_COMBO added pure_N
#define PROTO_TYPE_PURE_N 0x0008
#define PROTO_TYPE_AC     0x0020
#define PROTO_TYPE_BG     0x0040
#define PROTO_TYPE_BGN    0x0080
#define PROTO_TYPE_AN    0x0100
#define PROTO_TYPE_ANAC  0x0200
#define PROTO_TYPE_11TH  0x0400
#define PROTO_TYPE_12TH  0x0800
#define PROTO_TYPE_13TH  0x1000
#define PROTO_TYPE_14TH  0x2000
#define PROTO_TYPE_15TH  0x4000
#define PROTO_TYPE_16TH  0x8000

extern int ds_proto_type;

#if 0
#define CHECK_FOR_N_AND_AC_SUBTYPES(sub_type)  ((sub_type == AL_CONF_IF_PHY_SUB_TYPE_802_11_N)|| \
            (sub_type == AL_CONF_IF_PHY_SUB_TYPE_802_11_BGN)|| (sub_type == AL_CONF_IF_PHY_SUB_TYPE_802_11_AN) ||\
                (sub_type == AL_CONF_IF_PHY_SUB_TYPE_802_11_AC)|| (sub_type == AL_CONF_IF_PHY_SUB_TYPE_802_11_ANAC))

#define CHECK_FOR_N_SUBTYPE(sub_type)  ((sub_type == AL_CONF_IF_PHY_SUB_TYPE_802_11_N)|| \
            (sub_type == AL_CONF_IF_PHY_SUB_TYPE_802_11_BGN)|| (sub_type == AL_CONF_IF_PHY_SUB_TYPE_802_11_AN)||\
                       (sub_type == AL_CONF_IF_PHY_SUB_TYPE_802_11_ANAC))
#endif
#if 1
#define CHECK_FOR_N_AND_AC_SUBTYPES(sub_type)  ((sub_type == AL_CONF_IF_PHY_SUB_TYPE_802_11_N_2_4GHZ)|| \
                    (sub_type == AL_CONF_IF_PHY_SUB_TYPE_802_11_BGN)|| (sub_type == AL_CONF_IF_PHY_SUB_TYPE_802_11_AN) ||\
                        (sub_type == AL_CONF_IF_PHY_SUB_TYPE_802_11_AC)|| (sub_type == AL_CONF_IF_PHY_SUB_TYPE_802_11_ANAC)\
       || (sub_type == AL_CONF_IF_PHY_SUB_TYPE_802_11_N_5GHZ))

#define CHECK_FOR_N_SUBTYPE(sub_type)  ((sub_type == AL_CONF_IF_PHY_SUB_TYPE_802_11_N_2_4GHZ)|| (sub_type == AL_CONF_IF_PHY_SUB_TYPE_802_11_N_5GHZ) \
        || (sub_type == AL_CONF_IF_PHY_SUB_TYPE_802_11_BGN)|| (sub_type == AL_CONF_IF_PHY_SUB_TYPE_802_11_AN)||\
                               (sub_type == AL_CONF_IF_PHY_SUB_TYPE_802_11_ANAC))

#endif
#define CHECK_FOR_AC_SUBTYPE(sub_type)  ((sub_type == AL_CONF_IF_PHY_SUB_TYPE_802_11_AC)||(sub_type == AL_CONF_IF_PHY_SUB_TYPE_802_11_ANAC))

struct al_conf_dhcp_info
{
   unsigned char dhcp_net_id[4];
   unsigned char dhcp_mode;
   unsigned char dhcp_mask[4];
   unsigned char dhcp_gateway[4];
   unsigned char dhcp_dns[4];
   unsigned int  dhcp_lease_time;
};
typedef struct al_conf_dhcp_info   al_conf_dhcp_info_t;

struct al_conf_gps_info
{
   unsigned char  gps_enabled;
   char           gps_input_dev[32];
   unsigned char  gps_push_dest_ip[4];
   unsigned short gps_push_port;
   unsigned short gps_push_interval;
};
typedef struct al_conf_gps_info   al_conf_gps_info_t;

struct al_conf_logmon_info
{
   unsigned char  logmon_dest_ip[4];
   unsigned short logmon_dest_port;
};
typedef struct al_conf_logmon_info   al_conf_logmon_info_t;

struct _security_none
{
   unsigned char enabled;
};

typedef struct _security_none   _security_none_t;

struct _wep_keys_
{
   unsigned char bytes[AL_802_11_MAX_KEY_LENGTH];
   unsigned char len;
};

typedef struct _wep_keys_   wep_keys_t;

struct _security_wep
{
   unsigned char enabled;
   unsigned char strength;
   unsigned char key_index;
   unsigned char key_count;
   wep_keys_t    keys[AL_802_11_MAX_SECURITY_KEYS];
};

typedef struct _security_wep   _security_wep_t;

struct _security_rsn_psk
{
   unsigned char enabled;
   unsigned char mode;
   unsigned char key_buffer[512];
   unsigned char key_buffer_len;
   unsigned char cipher_tkip;
   unsigned char cipher_ccmp;
   unsigned int  group_key_renewal;
};

typedef struct _security_rsn_psk   _security_rsn_psk_t;

#define AL_CONF_RSN_RADIUS_ENABLED            0x01
#define AL_CONF_RSN_RADIUS_VLAN_MEMBERSHIP    0x02

/**
 * Format for the enabled field in _security_rsn_radius F/W 2.5.37 onwards
 *
 * 07   06  05  04  03  02  01  00
 *+---+---+---+---+---+---+---+---+
 *|			RESERVED	  |VL |EN |
 *+---+---+---+---+---+---+---+---+
 *
 * EN = enabled
 * VL = RADIUS server decides VLAN membership
 *
 *
 */

struct _security_rsn_radius
{
   unsigned char enabled;
   unsigned char mode;
   unsigned char radius_server[4];
   int           radius_port;
   unsigned char radius_secret_key[512];
   unsigned char radius_secret_key_len;
   unsigned char cipher_tkip;
   unsigned char cipher_ccmp;
   unsigned int  group_key_renewal;
};

typedef struct _security_rsn_radius   _security_rsn_radius_t;

struct al_security_info
{
   int                    _is_present;
   _security_none_t       _security_none;
   _security_wep_t        _security_wep;
   _security_rsn_psk_t    _security_rsn_psk;
   _security_rsn_radius_t _security_rsn_radius;
};

typedef struct al_security_info   al_security_info_t;

#if MDE_80211N_SUPPORT 

struct al_fram_agre
{
	unsigned char	   ampdu_enable;
	int	   max_ampdu_len;
};
typedef struct al_fram_agre al_fram_agre_t;

struct al_ht_capab
{
	unsigned char				ldpc;
	unsigned char				ht_bandwidth;
        int                                     sec_offset;
	unsigned char				smps;
	unsigned char				gi_20;
	unsigned char				gi_40;
	unsigned char				tx_stbc;
	unsigned char				rx_stbc;
	unsigned char                           delayed_ba;
	unsigned char	   			gfmode;
	int					max_amsdu_len;
	unsigned char				dsss_cck_40;
	unsigned char				intolerant;
	unsigned char				lsig_txop;

};
typedef struct al_ht_capab   al_ht_capab_t;

#endif

#if MDE_80211AC_SUPPORT     

struct al_vht_capab
{
	int 			max_mpdu_len;
	int 			supported_channel_width;
	unsigned char 		rx_ldpc;
	unsigned char		gi_80;
	unsigned char		gi_160;
	unsigned char		tx_stbc;
	unsigned char		rx_stbc;
	unsigned char		su_beamformer_cap;
	unsigned char		su_beamformee_cap;
	int 			beamformee_sts_count;
	int			sounding_dimensions;
	unsigned char		mu_beamformer_cap;
	unsigned char		mu_beamformee_cap;
	unsigned char		vht_txop_ps;
	unsigned char		htc_vht_cap;
	unsigned char		rx_ant_pattern_consistency;
	unsigned char		tx_ant_pattern_consistency;
	int			vht_oper_bandwidth;
	int			seg0_center_freq;
	int			seg1_center_freq;
};
typedef struct al_vht_capab al_vht_capab_t;

#endif

struct al_conf_if_info
{
   char               name[32];
   unsigned char      phy_type;                                         /* AL_CONF_IF_PHY_TYPE_* */
   unsigned char      phy_sub_type;                                     /* AL_CONF_IF_PHY_SUB_TYPE_* */
   unsigned char      use_type;                                         /* AL_CONF_IF_USE_TYPE_* */
   unsigned char      wm_channel;                                       /* Valid if dca is off */
   unsigned char      bonding;                                          /* AL_CONF_IF_BONDING_* */
   char               essid[33];                                        /* Need to map it to AL_802_11_MAX_ESSID_LENGTH */
   int                rts_th;
   int                frag_th;
   int                beacon_interval;
   unsigned char      dca;                                              /* 0 for disabled, 1 for enabled */
   unsigned char      txpower;                                          /* Percentage */
   int                txrate;                                           /* MBPS */
   int                txant;                                            /* TX antenna index */
   unsigned char      service;                                          /* AL_CONF_IF_SERVICE_* */
   unsigned char      ack_timeout;                                      /* AL_CONF_IF_ACK_TIME_OUT  microsec * */
   unsigned char      preamble_type;                                    /* AL_CONF_IF_PREAMBLE_TYPE_* */
   unsigned char      slot_time_type;                                   /* AL_CONF_IF_SLOT_TIME_TYPE_* */
   int                dca_list_count;
   int                dca_list[32];                                     /* Max 32 for now */
   unsigned char      hide_ssid;
#if MDE_80211N_SUPPORT 
	al_ht_capab_t      ht_capab;
	al_fram_agre_t     frame_aggregation;
#endif

#if MDE_80211AC_SUPPORT 
	al_vht_capab_t     vht_capab;
#endif
   al_security_info_t security_info;
   unsigned char      dot11e_enabled;
   unsigned char      dot11e_category;
   unsigned char      priv_channel_bw;
   unsigned char      priv_channel_max_power;
   int                priv_channel_count;
   unsigned char      priv_channel_ant_max;
   unsigned char      priv_channel_ctl;
   unsigned short     priv_frequencies[32];
   unsigned char      priv_channel_numbers[32];
   unsigned char      priv_channel_flags[32];                   /* This contains DFS bit now. */
};

/**
 * Format for the priv_channel_flags field to be used by F/W 2.5.30 onwards
 *
 * 07   06  05  04  03  02  01  00
 * ---+---+---+---+---+---+---+---+
 *|			RESERVED		  |DFS|
 * ---+---+---+---+---+---+---+---+
 */

typedef struct al_conf_if_info   al_conf_if_info_t;

struct al_conf_default_vlan_info
{
   int           tag;
   unsigned char dot1p_priority;
   unsigned char dot11e_enabled;
   unsigned char dot11e_category;
};


typedef struct al_conf_default_vlan_info   al_conf_default_vlan_info_t;


struct al_conf_vlan_info
{
   char               name[32];
   char               essid[33];
   int                rts_th;
   int                frag_th;
   int                beacon_interval;
   unsigned char      service;
   unsigned char      txpower;
   int                txrate;
   short              tag;
   unsigned char      dot1p_priority;
   unsigned char      dot11e_enabled;
   unsigned char      dot11e_category;
   unsigned char      ip_address[4];
   al_security_info_t security_info;
};

typedef struct al_conf_vlan_info   al_conf_vlan_info_t;

#define AL_CONF_EFFISTREAM_MATCH_ID_ETH_TYPE        1
#define AL_CONF_EFFISTREAM_MATCH_ID_ETH_DST         2
#define AL_CONF_EFFISTREAM_MATCH_ID_ETH_SRC         3
#define AL_CONF_EFFISTREAM_MATCH_ID_IP_TOS          4
#define AL_CONF_EFFISTREAM_MATCH_ID_IP_DIFFSRV      5
#define AL_CONF_EFFISTREAM_MATCH_ID_IP_SRC          6
#define AL_CONF_EFFISTREAM_MATCH_ID_IP_DST          7
#define AL_CONF_EFFISTREAM_MATCH_ID_IP_PROTO        8
#define AL_CONF_EFFISTREAM_MATCH_ID_UDP_SRC_PORT    9
#define AL_CONF_EFFISTREAM_MATCH_ID_UDP_DST_PORT    10
#define AL_CONF_EFFISTREAM_MATCH_ID_UDP_LENGTH      11
#define AL_CONF_EFFISTREAM_MATCH_ID_TCP_SRC_PORT    12
#define AL_CONF_EFFISTREAM_MATCH_ID_TCP_DST_PORT    13
#define AL_CONF_EFFISTREAM_MATCH_ID_TCP_LENGTH      14
#define AL_CONF_EFFISTREAM_MATCH_ID_RTP_VERSION     15
#define AL_CONF_EFFISTREAM_MATCH_ID_RTP_PAYLOAD     16
#define AL_CONF_EFFISTREAM_MATCH_ID_RTP_LENGTH      17

struct al_conf_effistream_criteria
{
   unsigned char                      match_id;         /* AL_CONF_EFFISTREAM_MATCH_ID */
   union
   {
      struct
      {
         unsigned int min_val;
         unsigned int max_val;
      }
                    range;
      al_net_addr_t mac_address;
      unsigned char ip_address[4];
      unsigned int  value;
   }
                                      match;
   struct
   {
      unsigned char no_ack;
      unsigned char drop;
      unsigned char dot11e_category;
      unsigned char bit_rate;
      unsigned char queued_retry;
   }
                                      action;
   struct al_conf_effistream_criteria *first_child;
   struct al_conf_effistream_criteria *next_sibling;
};
typedef struct al_conf_effistream_criteria   al_conf_effistream_criteria_t;

#define AL_CONF_OPTION_CODE_SIP    8

struct al_conf_option
{
   unsigned char         key[16];                               /** Key as defined above */
   struct al_conf_option *next;                                 /** Next option */
};

typedef struct al_conf_option   al_conf_option_t;

int al_conf_open(AL_CONTEXT_PARAM_DECL_SINGLE);
int al_conf_open_file(AL_CONTEXT_PARAM_DECL_SINGLE);
int al_conf_parse(AL_CONTEXT_PARAM_DECL const char *al_conf_string);
void al_conf_close(AL_CONTEXT_PARAM_DECL unsigned int al_conf_handle);

int al_conf_get_name(AL_CONTEXT_PARAM_DECL int al_conf_handle, char *name_buffer, int buffer_length);
int al_conf_get_description(AL_CONTEXT_PARAM_DECL int al_conf_handle, char *desc_buffer, int buffer_length);
int al_conf_get_mesh_id(AL_CONTEXT_PARAM_DECL int al_conf_handle, char *mesh_id_buffer, int buffer_length);
int al_conf_get_mesh_imcp_key_buffer(AL_CONTEXT_PARAM_DECL unsigned int al_conf_handle, char *mesh_imcp_key_buffer, int buffer_length);
int al_conf_get_essid(AL_CONTEXT_PARAM_DECL int al_conf_handle, char *essid_buffer, int buffer_length);
int al_conf_get_rts_th(AL_CONTEXT_PARAM_DECL int al_conf_handle);
int al_conf_get_frag_th(AL_CONTEXT_PARAM_DECL int al_conf_handle);
int al_conf_get_beacon_interval(AL_CONTEXT_PARAM_DECL int al_conf_handle);
int al_conf_get_dynamic_channel_allocation(AL_CONTEXT_PARAM_DECL int al_conf_handle);
int al_conf_get_stay_awake_count(AL_CONTEXT_PARAM_DECL int al_conf_handle);
int al_conf_get_bridge_ageing_time(AL_CONTEXT_PARAM_DECL int al_conf_handle);
int al_conf_get_wds_encrypted(AL_CONTEXT_PARAM_DECL int al_conf_handle);
int al_conf_get_model(AL_CONTEXT_PARAM_DECL int al_conf_handle, char *model_name, int model_name_length);

int al_conf_get_gps_x_coordinate(AL_CONTEXT_PARAM_DECL int al_conf_handle, char *gps_x_coordinate, int coordinate_length);
int al_conf_get_gps_y_coordinate(AL_CONTEXT_PARAM_DECL int al_conf_handle, char *gps_y_coordinate, int coordinate_length);

int al_conf_get_regulatory_domain(AL_CONTEXT_PARAM_DECL int al_conf_handle);
int al_conf_get_country_code(AL_CONTEXT_PARAM_DECL int al_conf_handle);

int al_conf_get_fcc_certified(AL_CONTEXT_PARAM_DECL int al_conf_handle);
int al_conf_get_etsi_certified(AL_CONTEXT_PARAM_DECL int al_conf_handle);

int al_conf_get_ds_tx_rate(AL_CONTEXT_PARAM_DECL int al_conf_handle);
int al_conf_get_ds_tx_power(AL_CONTEXT_PARAM_DECL int al_conf_handle);

int al_conf_get_config_sqnr(AL_CONTEXT_PARAM_DECL int al_conf_handle);

int al_conf_get_if_count(AL_CONTEXT_PARAM_DECL unsigned int al_conf_handle);
int al_conf_get_if(AL_CONTEXT_PARAM_DECL unsigned int al_conf_handle, int if_index, al_conf_if_info_t *if_info);

int al_conf_get_vlan_count(AL_CONTEXT_PARAM_DECL int al_conf_handle);
int al_conf_get_vlan(AL_CONTEXT_PARAM_DECL int al_conf_handle, int vlan_index, al_conf_vlan_info_t *vlan_info);

int al_conf_get_preferred_parent(AL_CONTEXT_PARAM_DECL int al_conf_handle, al_net_addr_t *preferred_parent);
int al_conf_get_signal_map(AL_CONTEXT_PARAM_DECL int al_conf_handle, unsigned char *signal_map);
int al_conf_get_heartbeat_interval(AL_CONTEXT_PARAM_DECL int al_conf_handle);
int al_conf_get_heartbeat_miss_count(AL_CONTEXT_PARAM_DECL int al_conf_handle);
int al_conf_get_hop_cost(AL_CONTEXT_PARAM_DECL int al_conf_handle);
int al_conf_get_max_allowable_hops(AL_CONTEXT_PARAM_DECL int al_conf_handle);
int al_conf_get_las_interval(AL_CONTEXT_PARAM_DECL int al_conf_handle);
int al_conf_get_change_resistance(AL_CONTEXT_PARAM_DECL int al_conf_handle);
int al_conf_get_mgmt_gw_enable(AL_CONTEXT_PARAM_DECL int al_conf_handle);

al_conf_effistream_criteria_t *al_conf_get_effistream_root(AL_CONTEXT_PARAM_DECL int al_conf_handle);
void al_conf_free_effistream_tree(AL_CONTEXT_PARAM_DECL al_conf_effistream_criteria_t *tree_root);

int al_conf_get_gps_info(AL_CONTEXT_PARAM_DECL int al_conf_handle, al_conf_gps_info_t *gps_info);
int al_conf_get_dhcp_info(AL_CONTEXT_PARAM_DECL int al_conf_handle, al_conf_dhcp_info_t *dhcp_info);
int al_conf_get_logmon_info(AL_CONTEXT_PARAM_DECL int al_conf_handle, al_conf_logmon_info_t *logmon_info);

al_conf_option_t *al_conf_get_options(AL_CONTEXT_PARAM_DECL int al_conf_handle);

#ifndef _MESHAP_AL_CONF_NO_SET_AND_PUT

int al_conf_set_name(AL_CONTEXT_PARAM_DECL int al_conf_handle, char *name_buffer, int buffer_length);
int al_conf_set_description(AL_CONTEXT_PARAM_DECL int al_conf_handle, char *desc_buffer, int buffer_length);
int al_conf_set_mesh_id(AL_CONTEXT_PARAM_DECL int al_conf_handle, char *mesh_id_buffer, int buffer_length);
int al_conf_set_mesh_imcp_key_buffer(AL_CONTEXT_PARAM_DECL unsigned int al_conf_handle, char *mesh_imcp_key_buffer, int buffer_length);
int al_conf_set_essid(AL_CONTEXT_PARAM_DECL int al_conf_handle, char *essid_buffer, int buffer_length);
int al_conf_set_rts_th(AL_CONTEXT_PARAM_DECL int al_conf_handle, int rts_th);
int al_conf_set_frag_th(AL_CONTEXT_PARAM_DECL int al_conf_handle, int frag_th);
int al_conf_set_beacon_interval(AL_CONTEXT_PARAM_DECL int al_conf_handle, int beacon_interval);
int al_conf_set_dynamic_channel_allocation(AL_CONTEXT_PARAM_DECL int al_conf_handle, int dynamic_channel_allocation);
int al_conf_set_stay_awake_count(AL_CONTEXT_PARAM_DECL int al_conf_handle, int stay_awake_count);
int al_conf_set_bridge_ageing_time(AL_CONTEXT_PARAM_DECL int al_conf_handle, int bridge_ageing_time);
int al_conf_set_wds_encrypted(AL_CONTEXT_PARAM_DECL int al_conf_handle, int wds_encrypted);
int al_conf_set_model(AL_CONTEXT_PARAM_DECL int al_conf_handle, char *model_name, int model_name_length);
int al_conf_set_gps_x_coordinate(AL_CONTEXT_PARAM_DECL int al_conf_handle, char *gps_x_coordinate, int cordinate_length);
int al_conf_set_gps_y_coordinate(AL_CONTEXT_PARAM_DECL int al_conf_handle, char *gps_y_coordinate, int cordinate_length);
int al_conf_set_regulatory_domain(AL_CONTEXT_PARAM_DECL int al_conf_handle, int regulatory_domain);
int al_conf_set_country_code(AL_CONTEXT_PARAM_DECL int al_conf_handle, int country_code);
int al_conf_set_fcc_certified(AL_CONTEXT_PARAM_DECL int al_conf_handle, int fcc_certified);
int al_conf_set_etsi_certified(AL_CONTEXT_PARAM_DECL int al_conf_handle, int etsi_certified);
int al_conf_set_ds_tx_rate(AL_CONTEXT_PARAM_DECL int al_conf_handle, int ds_tx_rate);
int al_conf_set_ds_tx_power(AL_CONTEXT_PARAM_DECL int al_conf_handle, int ds_tx_rate);
int al_conf_set_config_sqnr(AL_CONTEXT_PARAM_DECL int al_conf_handle, unsigned int config_sqnr);
int al_conf_set_if_count(AL_CONTEXT_PARAM_DECL int al_conf_handle, int if_count);
int al_conf_set_if(AL_CONTEXT_PARAM_DECL unsigned int al_conf_handle, int if_index, al_conf_if_info_t *if_info);
int al_conf_set_vlan_count(AL_CONTEXT_PARAM_DECL int al_conf_handle, int vlan_count);
int al_conf_set_vlan(AL_CONTEXT_PARAM_DECL int al_conf_handle, int vlan_index, al_conf_vlan_info_t *vlan_info);
int al_conf_set_preferred_parent(AL_CONTEXT_PARAM_DECL int al_conf_handle, al_net_addr_t *preferred_parent);
int al_conf_set_signal_map(AL_CONTEXT_PARAM_DECL int al_conf_handle, unsigned char *signal_map);
int al_conf_set_heartbeat_interval(AL_CONTEXT_PARAM_DECL int al_conf_handle, int heartbeat_interval);
int al_conf_set_heartbeat_miss_count(AL_CONTEXT_PARAM_DECL int al_conf_handle, int heartbeat_miss_count);
int al_conf_set_hop_cost(AL_CONTEXT_PARAM_DECL int al_conf_handle, int hop_cost);
int al_conf_set_max_allowable_hops(AL_CONTEXT_PARAM_DECL int al_conf_handle, int max_allowable_hops);
int al_conf_set_las_interval(AL_CONTEXT_PARAM_DECL int al_conf_handle, int las_interval);
int al_conf_set_change_resistance(AL_CONTEXT_PARAM_DECL int al_conf_handle, int change_resistance);
void al_conf_set_effistream_root(AL_CONTEXT_PARAM_DECL int al_conf_handle, al_conf_effistream_criteria_t *criteria_root);
int al_conf_set_gps_info(AL_CONTEXT_PARAM_DECL int al_conf_handle, al_conf_gps_info_t *gps_info);
int al_conf_set_dhcp_info(AL_CONTEXT_PARAM_DECL int al_conf_handle, al_conf_dhcp_info_t *dhcp_info);
int al_conf_set_logmon_info(AL_CONTEXT_PARAM_DECL int al_conf_handle, al_conf_logmon_info_t *logmon_info);

int al_conf_set_options(AL_CONTEXT_PARAM_DECL int al_conf_handle, al_conf_option_t *options);

int al_conf_get_use_virt_if(AL_CONTEXT_PARAM_DECL int al_conf_handle);

int al_conf_set_use_virt_if(AL_CONTEXT_PARAM_DECL int al_conf_handle, int use_virt_if);

int al_conf_create_config_string_from_data(AL_CONTEXT_PARAM_DECL unsigned int al_conf_handle, int *config_string_length, char **al_conf_string);

int al_conf_put(AL_CONTEXT_PARAM_DECL unsigned int al_conf_handle);

int al_conf_get_power_on_default(AL_CONTEXT_PARAM_DECL int al_conf_handle);
int al_conf_get_failover_enabled(AL_CONTEXT_PARAM_DECL int al_conf_handle);
int al_conf_set_failover_enabled(AL_CONTEXT_PARAM_DECL int al_conf_handle, int on_off, int power_on_defaultint, int scan_freq_secs, char *ip);

int al_conf_set_server_addr(AL_CONTEXT_PARAM_DECL int al_conf_handle, unsigned char *server_addr);
int al_conf_set_mgmt_gw_addr(AL_CONTEXT_PARAM_DECL int al_conf_handle, unsigned char *mgmt_gw_addr);
int al_conf_set_mgmt_gw_enable(AL_CONTEXT_PARAM_DECL int al_conf_handle, int mgmt_gw_enable);
int al_conf_set_mgmt_gw_certificates(AL_CONTEXT_PARAM_DECL int al_conf_handle, unsigned char *mgmt_gw_certificates);

int al_conf_get_disable_backhaul_security(AL_CONTEXT_PARAM_DECL int al_conf_handle);
int al_conf_set_disable_backhaul_security(AL_CONTEXT_PARAM_DECL int al_conf_handle, int on_off);

int al_conf_set_sig_threshold(AL_CONTEXT_PARAM_DECL int al_conf_handle, int sig_ts);
int al_conf_get_sig_threshold(AL_CONTEXT_PARAM_DECL int al_conf_handle);
unsigned char *al_conf_get_server_addr(AL_CONTEXT_PARAM_DECL int al_conf_handle);
unsigned char *al_conf_get_mgmt_gw_addr(AL_CONTEXT_PARAM_DECL int al_conf_handle);
unsigned char *al_conf_get_mgmt_gw_certificates(AL_CONTEXT_PARAM_DECL int al_conf_handle);
unsigned char get_phy_support(unsigned char);
int convert_mode_to_proto_cap(unsigned char sub_type); //PROTOCOL_COMBO
#endif /* _MESHAP_AL_CONF_NO_SET_AND_PUT */

#endif /*__AL_CONF_H__*/
