/********************************************************************************
* MeshDynamics
* --------------
* File     : al_ip.h
* Comments : Abstraction Layer Internet Protocol header
* Created  : 4/5/2004
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  0  |4/5/2004  | Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/


#ifndef __AL_IP_H__
#define __AL_IP_H__

struct al_ip_addr
{
   unsigned char bytes[6];
   int           length;
};

typedef struct al_ip_addr   al_ip_addr_t;

#endif /*__AL_IP_H__*/
