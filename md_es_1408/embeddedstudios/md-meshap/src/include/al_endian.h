/********************************************************************************
* MeshDynamics
* --------------
* File     : al_endian.h
* Comments : Abstraction Layer CPU Endian-ness header
* Created  : 4/13/2004
* Author   : Bindu Parakala
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  2  |5/28/2004 | Removed \                                       | Sriram |
* -----------------------------------------------------------------------------
* |  1  |4/21/2004 | Fixed misc syntax errors                        | Bindu P|
* -----------------------------------------------------------------------------
* |  0  |4/13/2004 | Created.                                        | Bindu P|
* -----------------------------------------------------------------------------
********************************************************************************/


#ifndef __AL_ENDIAN_H__
#define __AL_ENDIAN_H__


#ifdef __GNUC__
#define al_swap16(s)                                  \
   ({                                                 \
      unsigned short sw;                              \
      sw = ((s & 0x00FF) << 8) | ((s & 0xFF00) >> 8); \
      sw;                                             \
   }                                                  \
   )

#define al_swap32(l)                                                                                                \
   ({                                                                                                               \
      unsigned int sw;                                                                                              \
      sw = ((l & 0x000000FF) << 24) | ((l & 0x0000FF00) << 8) | ((l & 0x00FF0000) >> 8) | ((l & 0xFF000000) >> 24); \
      sw;                                                                                                           \
   }                                                                                                                \
   )
#else
static unsigned short al_swap16(unsigned short s)
{
   unsigned short sw;

   sw = ((s & 0x00FF) << 8) | ((s & 0xFF00) >> 8);
   return sw;
}


static unsigned int al_swap32(unsigned int l)
{
   unsigned int sw;

   sw = ((l & 0x000000FF) << 24) | ((l & 0x0000FF00) << 8) | ((l & 0x00FF0000) >> 8) | ((l & 0xFF000000) >> 24);
   return sw;
}
#endif

#define al_le32_to_cpu(n)    al_impl_le32_to_cpu(n)
#define al_le16_to_cpu(n)    al_impl_le16_to_cpu(n)
#define al_be32_to_cpu(n)    al_impl_be32_to_cpu(n)
#define al_be16_to_cpu(n)    al_impl_be16_to_cpu(n)

#define al_cpu_to_le32(n)    al_impl_cpu_to_le32(n)
#define al_cpu_to_le16(n)    al_impl_cpu_to_le16(n)
#define al_cpu_to_be16(n)    al_impl_cpu_to_be16(n)
#define al_cpu_to_be32(n)    al_impl_cpu_to_be32(n)


#endif /*__AL_ENDIAN_H__*/
