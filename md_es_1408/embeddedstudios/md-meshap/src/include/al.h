/********************************************************************************
* MeshDynamics
* --------------
* File     : al.h
* Comments : Abstraction Layer header
* Created  : 4/5/2004
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* | 48  |8/22/2008 | Changes for FIPS and SIP                        |Abhijit |
* -----------------------------------------------------------------------------
* | 47  |1/22/2008 | Changes for Mixed mode                          |Prachiti|
* -----------------------------------------------------------------------------
* | 46  |8/16/2007 | al_get_packet_pool_info Added                   | Sriram |
* -----------------------------------------------------------------------------
* | 45  |8/16/2007 | al_get_cpu_load_info Added                      | Sriram |
* -----------------------------------------------------------------------------
* | 44  |7/24/2007 | REBOOT logging changes                          | Sriram |
* -----------------------------------------------------------------------------
* | 43  |7/17/2007 | al_get_board_mem_info Added                     | Sriram |
* -----------------------------------------------------------------------------
* | 42  |7/9/2007  | Added al_strobe_reset_generator                 | Sriram |
* -----------------------------------------------------------------------------
* | 41  |7/9/2007  | Added al_enable_reset_generator                 | Sriram |
* -----------------------------------------------------------------------------
* | 40  |11/16/2006| Added Gen 2K Buffer Pool                        | Sriram |
* -----------------------------------------------------------------------------
* | 39  |3/14/2006 | Changes for ACL                                 | Bindu  |
* -----------------------------------------------------------------------------
* | 38  |3/13/2006 | Added atomic definitions                        | Sriram |
* -----------------------------------------------------------------------------
* | 37  |02/15/2006| Additions for dot11e                            | Bindu  |
* -----------------------------------------------------------------------------
* | 36  |9/22/2005 | Interrupt disable/enable semantics changed      | Sriram |
* -----------------------------------------------------------------------------
* | 35  |5/9/2005  | Watchdog timer additional changes               | Sriram |
* -----------------------------------------------------------------------------
* | 34  |5/8/2005  | Watchdog timer framework added                  | Sriram |
* -----------------------------------------------------------------------------
* | 33  |4/22/2005 | Added al_restart_mesh                           | Sriram |
* -----------------------------------------------------------------------------
* | 32  |4/17/2005 | Added al_reboot_board                           | Sriram |
* -----------------------------------------------------------------------------
* | 31  |4/6/2005  | Security changes                                | Sriram |
* -----------------------------------------------------------------------------
* | 30  |03/09/2005| Board info added                                | Anand  |
* -----------------------------------------------------------------------------
* | 29  |03/05/2005| functions added for board information values    | Anand  |
* -----------------------------------------------------------------------------
* | 28  |12/26/2004| Changed interrupt functions to macros           | Sriram |
* -----------------------------------------------------------------------------
* | 27  |11/19/2004| Added random number routines                    | Sriram |
* -----------------------------------------------------------------------------
* | 26  |6/14/2004 | Included al_assert.h                            | Sriram |
* -----------------------------------------------------------------------------
* | 25  |6/9/2004  | Implemented AL_NET_PACKET_DUMP                  | Sriram |
* -----------------------------------------------------------------------------
* | 24  |6/4/2004  | AL_LOG_TYPE_INFORMATION added                   | Bindu  |
* -----------------------------------------------------------------------------
* | 23  |6/4/2004  | Added CRASH_DEBUG log type                      | Sriram |
* -----------------------------------------------------------------------------
* | 22  |5/31/2004 | Added al_get_conf_handle                        | Anand  |
* -----------------------------------------------------------------------------
* | 21  |5/31/2004 | Added al_set_thread_name                        | Sriram |
* -----------------------------------------------------------------------------
* | 20  |5/31/2004 | Defined AL_PACKET_DEBUG_PARAM                   | Sriram |
* -----------------------------------------------------------------------------
* | 19  |5/28/2004 | Added al_add_packet_reference method            | Bindu  |
* -----------------------------------------------------------------------------
* | 18  |5/25/2004 | AL_NOTIFY_MESSAGE_TYPE_SET_STATE  added         | Anand  |
* -----------------------------------------------------------------------------
* | 17  |5/25/2004 | on_phy_link_notify added.                       | Anand  |
* -----------------------------------------------------------------------------
* | 16  |5/21/2004 | Print log types defined.                        | Anand  |
* -----------------------------------------------------------------------------
* | 15  |5/7/2004  | al_notify_message added                         | Anand  |
* -----------------------------------------------------------------------------
* | 14  |5/5/2004  | Packet/Memory Leak Debug params added           | Anand  |
* -----------------------------------------------------------------------------
* | 13  |4/22/2004 | Added FILE_ID & PACKET_MAX_SIZE                 | Bindu  |
* -----------------------------------------------------------------------------
* | 12  |4/22/2004 | Removed AL receive & before_transmit Handlers   | Bindu  |
* -----------------------------------------------------------------------------
* | 11  |4/22/2004 | Added file handling functions                   | Sriram |
* -----------------------------------------------------------------------------
* | 10  |4/21/2004 | **Made (major) changes for AL_CONTEXT**         | Sriram |
* -----------------------------------------------------------------------------
* |  9  |4/21/2004 | Changed al_open_file's parameters               | Sriram |
* -----------------------------------------------------------------------------
* |  8  |4/21/2004 | Wait State codes added                          | Anand  |
* -----------------------------------------------------------------------------
* |  7  |4/21/2004 | Declared handlers as extern with name change    | Bindu  |
* -----------------------------------------------------------------------------
* |  6  |4/14/2004 | Added AL_INLINE                                 | Bindu  |
* -----------------------------------------------------------------------------
* |  5  |4/13/2004 | Added direction parameter to al_allocate_packet | Bindu  |
* -----------------------------------------------------------------------------
* |  4  |4/13/2004 | Included al_endian.h                            | Bindu  |
* -----------------------------------------------------------------------------
* |  3  |4/12/2004 | Added functions for interrupt handling          | Sriram |
* -----------------------------------------------------------------------------
* |  2  |4/12/2004 | Added thread priority constants                 | Bindu  |
* -----------------------------------------------------------------------------
* |  1  |4/6/2004  | Added al_print_log function                     | Sriram |
* -----------------------------------------------------------------------------
* |  0  |4/5/2004  | Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/
#ifndef __AL_H__
#define __AL_H__

#include "al_types.h"
#include "al_context.h"
#include "al_net_if.h"
#include "al_endian.h"
#include "al_socket.h"
#include "al_assert.h"


#define MESH_CPU_PROFILE_ENABLED	0

extern int reboot_in_progress;
extern int threads_status;
#define THREAD_STATUS_BIT0_MASK		0x00000001
#define THREAD_STATUS_BIT1_MASK		0x00000002
#define THREAD_STATUS_BIT2_MASK		0x00000004
#define THREAD_STATUS_BIT3_MASK		0x00000008
#define THREAD_STATUS_BIT4_MASK		0x00000010
#define THREAD_STATUS_BIT5_MASK		0x00000020
#define THREAD_STATUS_BIT6_MASK		0x00000040
#define THREAD_STATUS_BIT7_MASK		0x00000080
#define THREAD_STATUS_BIT8_MASK		0x00000100
#define THREAD_STATUS_BIT9_MASK		0x00000200
#define THREAD_STATUS_BIT10_MASK	0x00000400
#define THREAD_STATUS_BIT11_MASK	0x00000800
#define THREAD_STATUS_BIT12_MASK	0x00001000
#define THREAD_STATUS_BIT13_MASK	0x00002000
#define THREAD_STATUS_BIT14_MASK	0x00004000
#define THREAD_STATUS_BIT15_MASK	0x00008000
#define THREAD_STATUS_BIT16_MASK	0x00010000

#define MULTIPLE_AP_THREAD  0
#define PKT_POLL_RETRY_COUNT 20
#define NUMBER_OF_PQUEUES   1 /*Number of priority queues, should be 2 power*/
#define PQUEUES_MASK		(NUMBER_OF_PQUEUES - 1)
#define NUMBER_OF_HQUEUES   1 /*Number of hash queues to mainitain different flows, should be 2 powers*/
#define HQUEUES_MASK		(NUMBER_OF_HQUEUES - 1)
#define PACKET_UNASSIGNED   0
#define PACKET_PROCESSING   1
#define PACKET_PROCESSED    2
#define _TORNA_MESHAP_PACKET_POOL_SIZE_    3072

#define RX_PKT			1
#define AP_TRD_PKT		2
#define IMCP_TRD_PKT		3

#define SKB_ETHERNET(skb)    eth_hdr(skb)

#define AL_INLINE                          AL_IMPL_INLINE
#define AL_SNPRINTF                        AL_IMPL_SNPRINTF

#define AL_FILE_ID_MESHAP_CONFIG           1
#define AL_FILE_ID_DOT11E_CONFIG           2
#define AL_FILE_ID_ACL_CONFIG              3
#define AL_FILE_ID_VIRT_IF_INFO_CONFIG     4
#define AL_FILE_ID_SIP_CONFIG              5

#define AL_THREAD_PRIORITY_LOW             0
#define AL_THREAD_PRIORITY_BELOW_NORMAL    1
#define AL_THREAD_PRIORITY_NORMAL          2
#define AL_THREAD_PRIORITY_ABOVE_NORMAL    3
#define AL_THREAD_PRIORITY_HIGH            4
#define AL_THREAD_PRIORITY_REAL_TIME       5

#define AL_PACKET_DIRECTION_IN             0
#define AL_PACKET_DIRECTION_OUT            1
#define AL_PACKET_MAX_SIZE                 2400

#define AL_OPEN_FILE_MODE_READ             1
#define AL_OPEN_FILE_MODE_WRITE            2
#define AL_OPEN_FILE_MODE_BINARY           4

#define AL_WMM_OUI_TYPE 2
#define AL_WMM_OUI_SUBTYPE_INFORMATION_ELEMENT 0
#define AL_WMM_OUI_SUBTYPE_PARAMETER_ELEMENT 1
#define AL_WMM_OUI_SUBTYPE_TSPEC_ELEMENT 2
#define AL_WMM_VERSION 1

//  VIJAY shifted the below macros from al_802_11.h to this file 
//  since we need this macros in coupling layer this looks suitable place
#define AL_802_11_DOT11E_CATEGORY_TYPE_AC_BK    0
#define AL_802_11_DOT11E_CATEGORY_TYPE_AC_BE    1
#define AL_802_11_DOT11E_CATEGORY_TYPE_AC_VI    2
#define AL_802_11_DOT11E_CATEGORY_TYPE_AC_VO    3

  /* HT Capabilities Info field within HT Capabilities element */
#define AL_HT_CAP_INFO_LDPC_CODING_CAP     ((u16) AL_BIT(0))
#define AL_HT_CAP_INFO_SUPP_CHANNEL_WIDTH_SET  ((u16) AL_BIT(1))
#define AL_HT_CAP_INFO_SMPS_MASK           ((u16) (AL_BIT(2) | AL_BIT(3)))
#define AL_HT_CAP_INFO_SMPS_STATIC         ((u16) 0)
#define AL_HT_CAP_INFO_SMPS_DYNAMIC        ((u16) AL_BIT(2))
#define AL_HT_CAP_INFO_SMPS_DISABLED       ((u16) (AL_BIT(2) | AL_BIT(3)))
#define AL_HT_CAP_INFO_GREEN_FIELD         ((u16) AL_BIT(4))
#define AL_HT_CAP_INFO_SHORT_GI20MHZ       ((u16) AL_BIT(5))
#define AL_HT_CAP_INFO_SHORT_GI40MHZ       ((u16) AL_BIT(6))
#define AL_HT_CAP_INFO_TX_STBC         ((u16) AL_BIT(7))
#define AL_HT_CAP_INFO_RX_STBC_MASK        ((u16) (AL_BIT(8) | AL_BIT(9)))
#define AL_HT_CAP_INFO_RX_STBC_1           ((u16) AL_BIT(8))
#define AL_HT_CAP_INFO_RX_STBC_12          ((u16) AL_BIT(9))
#define AL_HT_CAP_INFO_RX_STBC_123         ((u16) (AL_BIT(8) | AL_BIT(9)))
#define AL_HT_CAP_INFO_DELAYED_BA          ((u16) AL_BIT(10))
#define AL_HT_CAP_INFO_MAX_AMSDU_SIZE      ((u16) AL_BIT(11))
#define AL_HT_CAP_INFO_DSSS_CCK40MHZ       ((u16) AL_BIT(12))
/* B13 - Reserved (was PSMP support during P802.11n development) */
#define AL_HT_CAP_INFO_40MHZ_INTOLERANT        ((u16) AL_BIT(14))
#define AL_HT_CAP_INFO_LSIG_TXOP_PROTECT_SUPPORT   ((u16) AL_BIT(15))


// HT opeartio ralated MACROS
/* First octet of HT Operation Information within HT Operation element */
#define AL_HT_INFO_HT_PARAM_SECONDARY_CHNL_OFF_MASK    ((u8) BIT(0) | BIT(1))
#define AL_HT_INFO_HT_PARAM_SECONDARY_CHNL_ABOVE       ((u8) BIT(0))
#define AL_HT_INFO_HT_PARAM_SECONDARY_CHNL_BELOW       ((u8) BIT(0) | BIT(1))
#define AL_HT_INFO_HT_PARAM_STA_CHNL_WIDTH         ((u8) BIT(2))
#define AL_HT_INFO_HT_PARAM_RIFS_MODE          ((u8) BIT(3))
/* B4..B7 - Reserved */

/* HT Protection (B8..B9 of HT Operation Information) */
#define AL_HT_PROT_NO_PROTECTION           0
#define AL_HT_PROT_NONMEMBER_PROTECTION    1
#define AL_HT_PROT_20MHZ_PROTECTION        2
#define AL_HT_PROT_NON_HT_MIXED            3
/* Bits within ieee80211_ht_operation::operation_mode (BIT(0) maps to B8 in
 * HT Operation Information) */
#define AL_HT_OPER_OP_MODE_HT_PROT_MASK ((u16) (BIT(0) | BIT(1))) /* B8..B9 */
#define AL_HT_OPER_OP_MODE_NON_GF_HT_STAS_PRESENT  ((u16) BIT(2)) /* B10 */
/* BIT(3), i.e., B11 in HT Operation Information field - Reserved */
#define AL_HT_OPER_OP_MODE_OBSS_NON_HT_STAS_PRESENT    ((u16) BIT(4)) /* B12 */
/* BIT(5)..BIT(15), i.e., B13..B23 - Reserved */


/* Last two octets of HT Operation Information (BIT(0) = B24) */
/* B24..B29 - Reserved */
#define AL_HT_OPER_PARAM_DUAL_BEACON           ((u16) BIT(6))
#define AL_HT_OPER_PARAM_DUAL_CTS_PROTECTION       ((u16) BIT(7))
#define AL_HT_OPER_PARAM_STBC_BEACON           ((u16) BIT(8))
#define AL_HT_OPER_PARAM_LSIG_TXOP_PROT_FULL_SUPP      ((u16) BIT(9))
#define AL_HT_OPER_PARAM_PCO_ACTIVE            ((u16) BIT(10))
#define AL_HT_OPER_PARAM_PCO_PHASE             ((u16) BIT(11))
/* B36..B39 - Reserved */

#define MAX_AP_THREAD	4
#define MAX_TX_THREAD	3
#define NUM_SKB_QUE		4

#if MESH_CPU_PROFILE_ENABLED
#define MESH_USAGE_PROFILING_INIT  \
static int index = 0; \
ktime_t start, end;
#else
#define MESH_USAGE_PROFILING_INIT
#endif


#if MESH_CPU_PROFILE_ENABLED
#define MESH_USAGE_PROFILE_START(start) \
start = ktime_get();
#else
#define MESH_USAGE_PROFILE_START(start)
#endif

#if MESH_CPU_PROFILE_ENABLED
#define MESH_USAGE_PROFILE_END(profile_info, index, start, end) \
	end = ktime_get(); \
	profile_info[index] = ktime_to_ns(ktime_sub(end, start)); \
	index++; \
	index = index % 3;
#else
#define MESH_USAGE_PROFILE_END(profile_info, index, start, end)
#endif

struct profiling_info {
	al_u64_t process_wm_pkt_func_tprof[3];
	al_u64_t process_ds_pkt_func_tprof[3];
	al_u64_t process_mip_pkt_func_tprof[3];
	al_u64_t dot1pQ_imcpsplock_1[3];
	al_u64_t dot1pQ_imcpspunlock_1[3];
	al_u64_t dot1p_Qsplock[3];
	al_u64_t dot1p_Qspunlock[3];
	al_u64_t dot1pQ_imcpsplock_2[3];
	al_u64_t dot1pQ_imcpspunlock_2[3];
	al_u64_t dot1p_Qsplock_1[3];
	al_u64_t dot1p_Qspunlock_1[3];
	al_u64_t dot1p_Qsplock_2[3];
	al_u64_t dot1p_Qspunlock_2[3];
	al_u64_t upstack_send_pkt_func_tprof[3];
	al_u64_t upstack_process_pkt_func_tprof[3];
	al_u64_t dot1p_Qsplock_3[3];
	al_u64_t dot1p_Qspunlock_3[3];
	al_u64_t dot1p_Qsplock_4[3];
	al_u64_t dot1p_Qspunlock_4[3];
    al_u64_t core_pkt_splock_1[3];
	al_u64_t core_pkt_spunlock_1[3];
	al_u64_t core_pkt_splock_2[3];
	al_u64_t core_pkt_spunlock_2[3];
	al_u64_t core_pkt_splock_3[3];
	al_u64_t core_pkt_spunlock_3[3];
	al_u64_t core_pkt_splock_4[3];
	al_u64_t core_pkt_spunlock_4[3];
	al_u64_t core_pkt_splock_5[3];
	al_u64_t core_pkt_spunlock_5[3];
	al_u64_t core_pkt_splock_6[3];
	al_u64_t core_pkt_spunlock_6[3];
	al_u64_t tx_thread_tprof[3];
	al_u64_t process_skb_func_tprof[3];
	al_u64_t process_mip_skb_func_tprof[3];
	al_u64_t meshap_skb_enqueue_splock[3];
	al_u64_t meshap_skb_enqueue_spunlock[3];
	al_u64_t meshap_skb_deq_splock[3];
	al_u64_t meshap_skb_deq_spunlock[3];
	al_u64_t process_mip_xmit_func_tprof[3];
	al_u64_t process_eth_skb_func_tprof[3];
};
typedef struct profiling_info profiling_info_t;
extern profiling_info_t profiling_info;

struct thread_status {
	unsigned int	ap_thread;
	unsigned int	ap_thread_count[MAX_AP_THREAD];
	unsigned int	ap_sta_monitor_thread_count;
	unsigned int	tx_thread;
	unsigned int	tx_thread_count[MAX_TX_THREAD];
	unsigned int	imcp_thread_count;
	unsigned int	process_skb_thread_count;
	unsigned int	upstack_thread_count;
	unsigned int	ap_dot11i_thread_count;
	unsigned int	md_watch_thread_count;
	unsigned int	mesh_thread_count;
	unsigned int	mesh_hb_thread_count;
	unsigned int	uplink_scan_thread_count;
	unsigned int	lfr_thread_count;
	unsigned int	scan_thread_count;
	unsigned int	radar_dca_thread_count;
	unsigned int	dot1x_timer_thread_count;
	unsigned int	dBmTestRx_thread_count;
	unsigned int	dBmTestTx_thread_count;
	unsigned int	sip_cleanup_thread_count;
};
extern struct thread_status thread_stat;

struct queue_and_packet_stats {
	unsigned int	dot1p_queue_total_packet_added_count;
	unsigned int	dot1p_queue_total_packet_removed_count;
	unsigned int	dot1p_queue_current_packet_count;
	unsigned int	dot1p_queue_current_packet_max_count;
	unsigned int	dot1p_temp_queue_total_packet_added_count;
	unsigned int	dot1p_temp_queue_total_packet_removed_count;
	unsigned int	dot1p_temp_queue_current_packet_count;
	unsigned int	dot1p_temp_queue_current_packet_max_count;
	al_atomic_t 	tx_pktQ_total_packet_added_count;
	al_atomic_t 	tx_pktQ_total_packet_removed_count;
	al_atomic_t 	tx_pktQ_current_packet_count;
	al_atomic_t 	tx_pktQ_current_packet_max_count;
    unsigned int    temp_tx_pktQ_total_packet_added_count;
    unsigned int    temp_tx_pktQ_total_packet_removed_count;
    unsigned int    temp_tx_pktQ_current_packet_count;
    unsigned int    temp_tx_pktQ_current_packet_max_count;
	unsigned int 	imcp_pktQ_total_packet_added_count;
	unsigned int 	imcp_pktQ_total_packet_removed_count;
	unsigned int 	imcp_pktQ_current_packet_count;
	unsigned int 	imcp_pktQ_current_packet_max_count;
	al_atomic_t		data_pktQ_total_packet_added_count[NUM_SKB_QUE];
	al_atomic_t		data_pktQ_total_packet_removed_count[NUM_SKB_QUE];
	al_atomic_t		data_pktQ_current_packet_count[NUM_SKB_QUE];
	al_atomic_t		data_pktQ_current_packet_max_count[NUM_SKB_QUE];
	unsigned int	process_skbQ_total_packet_added_count;
	unsigned int	process_skbQ_total_packet_removed_count;
	unsigned int	process_skbQ_current_packet_count;
	unsigned int	process_skbQ_current_packet_max_count;
	al_atomic_t 	upstack_pktQ_total_packet_added_count;
	al_atomic_t 	upstack_pktQ_total_packet_removed_count;
	al_atomic_t   	upstack_pktQ_current_packet_count;
	al_atomic_t   	upstack_pktQ_current_packet_max_count;
    unsigned int    temp_upstack_pktQ_total_packet_added_count;
    unsigned int    temp_upstack_pktQ_total_packet_removed_count;
    unsigned int    temp_upstack_pktQ_current_packet_count;
    unsigned int    temp_upstack_pktQ_current_packet_max_count;
	unsigned int    core_packet_type_alloc_heap;
	unsigned int	core_packet_pool_current_count;
	unsigned int	procs_skb_core_pkt_pool_current_count;
	unsigned int	ap_thread_core_pkt_pool_current_count;
	unsigned int	imcp_thread_core_pkt_pool_current_count;
};
extern struct queue_and_packet_stats q_packet_stats;

struct tx_rx_packet_stats {
	unsigned int	access_point_process_mgmt_pkt;
	unsigned int	access_point_process_data_pkt;
	unsigned int	access_point_process_pkt;
	unsigned int	tx_transform_pkt_on_wm;
	unsigned int	tx_transform_pkt_on_ds;
	unsigned int	tx_de_transform_pkt_on_ds;
	unsigned int	rx_up_stack_pkt;
	unsigned int	tx_pkt;
	unsigned int	tx_imcp_pkt;
	unsigned int	test_direct_eth_pkt;
	unsigned int	test_direct_wireless_pkt;
	unsigned int	test_eth_to_eth_pkt;
	unsigned int	test_eth_to_wireless_pkt;
	unsigned int	test_wireless_to_eth_pkt;
	unsigned int	test_wireless_to_wireless_pkt;
	unsigned int	tx_sip_pkt;
	unsigned int	al_test_4_1_PUT;
	unsigned int	al_test_4_1_RES;
	unsigned int	al_test_4_1_RESPUT;
	unsigned int	mgmt_pkt_process_auth;
	unsigned int	mgmt_pkt_process_assoc;
	unsigned int	mip_xmit_tx;
	unsigned int	rx_master_mode_mgmt_probe_req_frame;
	unsigned int	rx_infra_mode_mgmt_frame;
	unsigned int	wm_pkt_process_dhcp;
	unsigned int	wm_pkt_process_sip;
	unsigned int	wm_pkt_process_arp;
	unsigned int	sending_arp_pkt_up_stack;
	unsigned int	packet_drop[141];
};

extern struct tx_rx_packet_stats tx_rx_pkt_stats;

struct mac80211_tx_rx_pkt_stats {
   unsigned int   free_tid_rx;
   unsigned int   tkip_mic_test_sta_not_connected;
   unsigned int   tkip_mic_test_sta_iface_not_match;
   unsigned int   pkt_unknown_type;
   unsigned int   build_beacon_success;
   unsigned int   build_beacon_fail;
   unsigned int   skb_len_lt_fcs_len;
   unsigned int   monitor_check_frame_control_drop;
   unsigned int   monitor_should_drop_frame_drop;
   unsigned int   check_enough_space_for_radio_tap_hdr_drop;
   unsigned int   monitor_dev_null_drop;
   unsigned int   frame_out_of_seq_num_drop;
   unsigned int   frame_reorder_drop;
   unsigned int   ps_poll_frame_queued;
   unsigned int   rx_sta_process_frame;
   unsigned int   defragmented_frames;
   unsigned int   deliver_skb_allignment_drop;
   unsigned int   msdu_not_allowed_drop;
   unsigned int   rx_action_frame;
   unsigned int   WM_mgmt_frame_processed_in_meshap_hook;
   unsigned int   mgmt_frame_sent_to_userspace;
   unsigned int   rx_action_return_frame;
   unsigned int   unusable_frame_free;
   unsigned int   errored_frame_drop;
   unsigned int   non_consume_data_frame_drop;
   unsigned int   ack_skb_drop;
   unsigned int   tx_frame_not_sent_to_monitor_iface;
   unsigned int   tx_status_headroom_too_small;
   unsigned int   tx_frame_to_monitor_iface;
   unsigned int   free_tx_skb;
   unsigned int   tdls_frame_tx_fail;
   unsigned int   perged_ps_poll_frames;
   unsigned int   old_ps_buffer_frame_drop;
   unsigned int   monitor_hw_queue_control_drop;
   unsigned int   tx_skb_short_len_drop;
   unsigned int   mac_tx_fail;
   unsigned int   tx_start_xmit_fail;
   unsigned int   chanctx_conf_drop;
   unsigned int   buffered_bc_mc_frame_drop;
   unsigned int   mgmt_tx_roc_drop;
   unsigned int   tx_skb_tid;
   unsigned int   iface_work_skb;
   unsigned int   free_ack_frame;
   unsigned int   mesh_invalid_action;
   unsigned int   mesh_path_move_to_que;
   unsigned int   mesh_path_discardframe;
   unsigned int   mesh_plink_frame_tx_fail;
   unsigned int   cfg_mgmt_tx_status;
   unsigned int   unable_to_resolve_next_hop;
   unsigned int   ctrl_frame;
   unsigned int   rx_monitor_drop;
   unsigned int   ieee80211_rx_drop;
   unsigned int   tx_null_data_drop;
   unsigned int   start_xmit_skb_shared;
   unsigned int   start_xmit_clone_skb_free;
   unsigned int   ack_skb_copy_free;
   unsigned int   ack_skb_free;
   unsigned int   teardown_skb_free;
   unsigned int   mgmt_tx_ack_skb_free;
   unsigned int   tdls_build_mgmt_pkt_fail;
   unsigned int   tdls_skb;
   unsigned int   ack_skb_build_hdr;
   unsigned int   ack_skb_shared_build_hdr;
   unsigned int   build_hdr_free;
   unsigned int   xmit_fast_skb_clone_free;
   unsigned int   xmit_skb_resize_free;
   unsigned int   xmit_fast_skb_txquued;
   unsigned int   start_xmit_skb_len_is_small;;
   unsigned int   non_linear_skb_frame;
   unsigned int   build_data_ra_sta_free;
   unsigned int   build_data_select_key;
};

typedef struct mac80211_tx_rx_pkt_stats mac80211_tx_rx_pkt_stats_t;
extern mac80211_tx_rx_pkt_stats_t	mac_tx_rx_pkt_stats;

struct al_802_11_ht_capabilities {
    unsigned short int  ht_capabilities_info;
    unsigned char  a_mpdu_params; /* Maximum A-MPDU Length Exponent B0..B1
                                   * Minimum MPDU Start Spacing B2..B4
                                   * Reserved B5..B7 */
    unsigned char  supported_mcs_set[16];
    unsigned short int  ht_extended_capabilities;
    unsigned int  tx_bf_capability_info;
    unsigned char asel_capabilities;
}
__attribute__((packed));

typedef struct al_802_11_ht_capabilities al_802_11_ht_capabilities_t;

/* HT Operation element */
struct al_802_11_ht_operation {
    unsigned char primary_chan;
    /* Five octets of HT Operation Information */
    unsigned char  ht_param; /* B0..B7 */
    unsigned short int  operation_mode; /* B8..B23 */
    unsigned short int param; /* B24..B39 */
    unsigned char  basic_mcs_set[16];
}
__attribute__((packed));

typedef struct al_802_11_ht_operation  al_802_11_ht_operation_t;


#define AL_VHT_CAP_MAX_MPDU_LENGTH_7991                ((u32) BIT(0))
#define AL_VHT_CAP_MAX_MPDU_LENGTH_11454               ((u32) BIT(1))
#define AL_VHT_CAP_MAX_MPDU_LENGTH_MASK                ((u32) BIT(0) | BIT(1))
#define AL_VHT_CAP_SUPP_CHAN_WIDTH_160MHZ              ((u32) BIT(2))
#define AL_VHT_CAP_SUPP_CHAN_WIDTH_160_80PLUS80MHZ     ((u32) BIT(3))
#define AL_VHT_CAP_SUPP_CHAN_WIDTH_MASK                ((u32) BIT(2) | BIT(3))
#define AL_VHT_CAP_RXLDPC                              ((u32) BIT(4))
#define AL_VHT_CAP_SHORT_GI_80                         ((u32) BIT(5))
#define AL_VHT_CAP_SHORT_GI_160                        ((u32) BIT(6))
#define AL_VHT_CAP_TXSTBC                              ((u32) BIT(7))
#define AL_VHT_CAP_RXSTBC_1                            ((u32) BIT(8))
#define AL_VHT_CAP_RXSTBC_2                            ((u32) BIT(9))
#define AL_VHT_CAP_RXSTBC_3                            ((u32) BIT(8) | BIT(9))
#define AL_VHT_CAP_RXSTBC_4                            ((u32) BIT(10))
#define AL_VHT_CAP_RXSTBC_MASK                         ((u32) BIT(8) | BIT(9) | \
                               BIT(10))
#define AL_VHT_CAP_SU_BEAMFORMER_CAPABLE               ((u32) BIT(11))
#define AL_VHT_CAP_SU_BEAMFORMEE_CAPABLE               ((u32) BIT(12))
#define AL_VHT_CAP_BEAMFORMEE_STS_MAX                  ((u32) BIT(13) | \
                               BIT(14) | BIT(15))
#define AL_VHT_CAP_BEAMFORMEE_STS_OFFSET               13
#define AL_VHT_CAP_SOUNDING_DIMENSION_MAX              ((u32) BIT(16) | \
                               BIT(17) | BIT(18))
#define AL_VHT_CAP_SOUNDING_DIMENSION_OFFSET           16
#define AL_VHT_CAP_MU_BEAMFORMER_CAPABLE               ((u32) BIT(19))
#define AL_VHT_CAP_MU_BEAMFORMEE_CAPABLE               ((u32) BIT(20))
#define AL_VHT_CAP_VHT_TXOP_PS                         ((u32) BIT(21))
#define AL_VHT_CAP_HTC_VHT                             ((u32) BIT(22))

#define AL_VHT_CAP_MAX_A_MPDU_LENGTH_EXPONENT_1        ((u32) BIT(23))
#define AL_VHT_CAP_MAX_A_MPDU_LENGTH_EXPONENT_2        ((u32) BIT(24))
#define AL_VHT_CAP_MAX_A_MPDU_LENGTH_EXPONENT_3        ((u32) BIT(23) | BIT(24))
#define AL_VHT_CAP_MAX_A_MPDU_LENGTH_EXPONENT_4        ((u32) BIT(25))
#define AL_VHT_CAP_MAX_A_MPDU_LENGTH_EXPONENT_5        ((u32) BIT(23) | BIT(25))
#define AL_VHT_CAP_MAX_A_MPDU_LENGTH_EXPONENT_6        ((u32) BIT(24) | BIT(25))
#define AL_VHT_CAP_MAX_A_MPDU_LENGTH_EXPONENT_MAX      ((u32) BIT(23) | \
                               BIT(24) | BIT(25))
#define AL_VHT_CAP_VHT_LINK_ADAPTATION_VHT_UNSOL_MFB   ((u32) BIT(27))
#define AL_VHT_CAP_VHT_LINK_ADAPTATION_VHT_MRQ_MFB     ((u32) BIT(26) | BIT(27))
#define AL_VHT_CAP_RX_ANTENNA_PATTERN                  ((u32) BIT(28))
#define AL_VHT_CAP_TX_ANTENNA_PATTERN                  ((u32) BIT(29))

#define VHT_OPMODE_CHANNEL_WIDTH_MASK           ((u8) BIT(0) | BIT(1))
#define VHT_OPMODE_CHANNEL_RxNSS_MASK           ((u8) BIT(4) | BIT(5) | \
                             BIT(6))
#define VHT_OPMODE_NOTIF_RX_NSS_SHIFT           4

#define VHT_RX_NSS_MAX_STREAMS              8

/* VHT channel widths */
#define AL_VHT_CHANWIDTH_USE_HT    0
#define AL_VHT_CHANWIDTH_80MHZ 1
#define AL_VHT_CHANWIDTH_160MHZ    2
#define AL_VHT_CHANWIDTH_80P80MHZ  3


struct al_802_11_vht_capabilities {
    unsigned int  vht_capabilities_info;
    struct vht_supported_mcs_set {
        unsigned short int  rx_map;
        unsigned short int  rx_highest;
        unsigned short int  tx_map;
        unsigned short int  tx_highest;
    } vht_mcs_set;
}
__attribute__((packed));

typedef struct al_802_11_vht_capabilities  al_802_11_vht_capabilities_t;

struct al_802_11_vht_operation {
    unsigned char  vht_op_info_chwidth;
    unsigned char  vht_op_info_chan_center_freq_seg0_idx;
    unsigned char  vht_op_info_chan_center_freq_seg1_idx;
    unsigned short int  vht_basic_mcs_set;
}
__attribute__((packed));

typedef struct al_802_11_vht_operation al_802_11_vht_operation_t ;

struct al_802_11_wmm_information_element {
    /* Element ID: 221 (0xdd); Length: 7 */
    /* required fields for WMM version 1 */
    unsigned char oui[3]; /* 00:50:f2 */
    unsigned char oui_type; /* 2 */
    unsigned char oui_subtype; /* 0 */
    unsigned char version; /* 1 for WMM version 1.0 */
    unsigned char qos_info; /* AP/STA specific QoS info */
}
__attribute__((packed));

typedef struct al_802_11_wmm_information_element al_802_11_wmm_information_element_t ;


struct al_thread_param
{
   int  thread_id;
   void *param;
};

typedef struct al_thread_param   al_thread_param_t;

typedef int (*al_thread_function_t)     (AL_CONTEXT_PARAM_DECL al_thread_param_t *param);

struct al_thread_message
{
   int  thread_id;
   int  message_id;
   void *message_data;
};

typedef struct al_thread_message   al_thread_message_t;

typedef struct meshap_mac80211_sta_info
{
   char           supported_rates[10];
   char           supported_rates_len;
   char           sta_addr[6];
   unsigned short aid;
   unsigned short listen_interval;
   unsigned short capability;
   al_802_11_ht_capabilities_t     ht_capab;    /**  used to copy Station HT from Assoc Req to Assoc resp */
   al_802_11_vht_capabilities_t  vht_capab ;
   al_802_11_wmm_information_element_t wmm_element;
   unsigned int wmm_flag_set;
   unsigned short ht_flag_set;
   unsigned short vht_flag_set;

} meshap_mac80211_sta_info_t;

#define AL_NOTIFY_MESSAGE_TYPE_SET_INTERFACE_INFO    1
#define AL_NOTIFY_MESSAGE_TYPE_SET_STATE             2

struct al_notify_message
{
   int  message_type;
   void *message_data;
};

typedef struct al_notify_message   al_notify_message_t;

#define AL_BOARD_INFO_MASK_TEMP       1
#define AL_BOARD_INFO_MASK_VOLTAGE    2

struct al_board_info
{
   short valid_values_mask;
   short temperature;
   short voltage;
};

typedef struct al_board_info   al_board_info_t;

struct al_packet_pool_info
{
   unsigned int pool_max;
   unsigned int pool_current;
   unsigned int pool_min;
   unsigned int gen_2k_max;
   unsigned int gen_2k_current;
   unsigned int gen_2k_min;
   unsigned int gen_2k_heap_alloc;
   unsigned int gen_2k_heap_free;
   unsigned int mip_skb_alloc;
   unsigned int mip_skb_free;
};

typedef struct al_packet_pool_info   al_packet_pool_info_t;

#define AL_NET_PACKET_DUMP(bytes, length)                                                     \
   {                                                                                          \
      int  i, j;                                                                              \
      char str[8];                                                                            \
      char buf[64];                                                                           \
      for (i = 0, j = 0; i<length; i++) {                                                     \
                              if (i % 16 == 0 && i != 0) {                                    \
                                 buf[j] = 0;                                                  \
                                 al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "%s", buf); \
                                 j = 0;                                                       \
                              }                                                               \
                              AL_SNPRINTF(str, sizeof(str), "%02X", bytes[i]);                \
                              buf[j++] = str[0];                                              \
                              buf[j++] = str[1];                                              \
                              buf[j++] = ' ';                                                 \
                           }                                                                  \
                           buf[j] = 0;                                                        \
                           al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "%s", buf);       \
                           j = 0;                                                             \
                           }

#define AL_NET_PACKET_DUMP_EX(name, bytes, length)                                            \
   {                                                                                          \
      int  i, j;                                                                              \
      char str[16];                                                                           \
      char buf[128];                                                                          \
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "unsigned char %s[] = {", name);       \
      for (i = 0, j = 0; i<length; i++) {                                                     \
                              if (i % 16 == 0 && i != 0) {                                    \
                                 buf[j] = 0;                                                  \
                                 al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "%s", buf); \
                                 j = 0;                                                       \
                              }                                                               \
                              AL_SNPRINTF(str, sizeof(str), "0x%02X", bytes[i]);              \
                              buf[j++] = str[0];                                              \
                              buf[j++] = str[1];                                              \
                              buf[j++] = str[2];                                              \
                              buf[j++] = str[3];                                              \
                              buf[j++] = ',';                                                 \
                           }                                                                  \
                           buf[j] = 0;                                                        \
                           al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "%s\n};", buf);   \
                           j = 0;                                                             \
                           }



typedef int (*al_on_receive_t)                  (AL_CONTEXT_PARAM_DECL al_net_if_t *al_net_if, al_packet_t *packet);
typedef int (*al_on_before_transmit_t)  (AL_CONTEXT_PARAM_DECL al_net_if_t *al_net_if, al_packet_t *packet);
typedef int (*al_on_phy_link_notify_t)  (AL_CONTEXT_PARAM_DECL al_net_if_t *al_net_if, int link_state);

#define AL_WAIT_TIMEOUT_INFINITE    0xFFFFFFFF          /** For use with al_wait_on_event and al_wait_on_semaphore */
#define AL_WAIT_STATE_SIGNAL        0                   /** For use with al_wait_on_event and al_wait_on_semaphore */
#define AL_WAIT_STATE_TIMEOUT       1                   /** For use with al_wait_on_event and al_wait_on_semaphore */


#define AL_LOG_TYPE_ERROR           0x01
#define AL_LOG_TYPE_FLOW            0x02
#define AL_LOG_TYPE_DEBUG           0x04
#define AL_LOG_TYPE_MSG             0x08
#define AL_LOG_TYPE_MGM             0x10
#define AL_LOG_TYPE_DATA            0x20
#define AL_LOG_TYPE_RX              0x40
#define AL_LOG_TYPE_TX              0x80
#define AL_LOG_TYPE_CRASH_DEBUG     0x100
#define AL_LOG_TYPE_INFORMATION     0x200

#define AL_LOG_MGM_RX               AL_LOG_TYPE_RX | AL_LOG_TYPE_MGM
#define AL_LOG_MGM_RX_DEBUG         AL_LOG_TYPE_RX | AL_LOG_TYPE_MGM | AL_LOG_TYPE_DEBUG
#define AL_LOG_DATA_RX              AL_LOG_TYPE_RX | AL_LOG_TYPE_DATA
#define AL_LOG_DATA_RX_DEBUG        AL_LOG_TYPE_RX | AL_LOG_TYPE_DATA | AL_LOG_TYPE_DEBUG
#define AL_LOG_MGM_TX               AL_LOG_TYPE_TX | AL_LOG_TYPE_MGM
#define AL_LOG_MGM_TX_DEBUG         AL_LOG_TYPE_TX | AL_LOG_TYPE_MGM | AL_LOG_TYPE_DEBUG
#define AL_LOG_DATA_TX              AL_LOG_TYPE_TX | AL_LOG_TYPE_DATA
#define AL_LOG_DATA_TX_DEBUG        AL_LOG_TYPE_TX | AL_LOG_TYPE_DATA | AL_LOG_TYPE_DEBUG

#ifndef _AL_HEAP_DEBUG
void *al_heap_alloc(AL_CONTEXT_PARAM_DECL size_t size);

#define AL_HEAP_DEBUG_PARAM
#else
void *al_heap_alloc(AL_CONTEXT_PARAM_DECL size_t size, const char *file_name, int line);

#define AL_HEAP_DEBUG_PARAM    , __FILE__, __LINE__
#endif

void al_heap_free(AL_CONTEXT_PARAM_DECL void *memblock);
int al_create_thread(AL_CONTEXT_PARAM_DECL al_thread_function_t thread_function, void *param, int priority, char *name);
int al_create_thread_on_cpu(AL_CONTEXT_PARAM_DECL al_thread_function_t thread_function, void *param, int priority, char *name);
int al_post_thread_message(AL_CONTEXT_PARAM_DECL int thread_id, int message_id, void *message_data);
int al_get_thread_message(AL_CONTEXT_PARAM_DECL int thread_id, al_thread_message_t *message);
int al_create_semaphore(AL_CONTEXT_PARAM_DECL int max_count, int initial_count);
int al_wait_on_semaphore(AL_CONTEXT_PARAM_DECL int semaphore_id, unsigned int timeout);
int al_release_semaphore(AL_CONTEXT_PARAM_DECL int semaphore_id, int release_count);
int al_destroy_semaphore(AL_CONTEXT_PARAM_DECL int semaphore_id);
int al_create_event(AL_CONTEXT_PARAM_DECL int manual_reset);
int al_wait_on_event(AL_CONTEXT_PARAM_DECL int event_id, unsigned int timeout);
int al_set_event(AL_CONTEXT_PARAM_DECL int event_id);
int al_reset_event(AL_CONTEXT_PARAM_DECL int event_id);
int al_destroy_event(AL_CONTEXT_PARAM_DECL int event_id);

al_u64_t al_get_tick_count(AL_CONTEXT_PARAM_DECL_SINGLE);

int al_timer_after(AL_CONTEXT_PARAM_DECL al_u64_t timer1, al_u64_t timer2);

int al_timer_after_eq(AL_CONTEXT_PARAM_DECL al_u64_t timer1, al_u64_t timer2);

int al_timer_before(AL_CONTEXT_PARAM_DECL al_u64_t timer1, al_u64_t timer2);

int al_timer_before_eq(AL_CONTEXT_PARAM_DECL al_u64_t timer1, al_u64_t timer2);

void al_thread_sleep(AL_CONTEXT_PARAM_DECL int milliseconds);

int al_get_net_if_count(AL_CONTEXT_PARAM_DECL_SINGLE);
al_net_if_t *al_get_net_if(AL_CONTEXT_PARAM_DECL int index);
int al_set_on_receive_hook(AL_CONTEXT_PARAM_DECL al_on_receive_t on_recieve);
int al_set_on_before_transmit_hook(AL_CONTEXT_PARAM_DECL al_on_before_transmit_t on_before_transmit);
int al_set_on_phy_link_notify_hook(AL_CONTEXT_PARAM_DECL al_on_phy_link_notify_t on_phy_link_notify);
int al_send_packet_up_stack(AL_CONTEXT_PARAM_DECL al_net_if_t *al_net_if, al_packet_t *packet);
int al_send_packet_up_stack_mgmt_gw(AL_CONTEXT_PARAM_DECL al_net_if_t *al_net_if, al_packet_t *packet);

void al_set_thread_name(AL_CONTEXT_PARAM_DECL const char *name);

int al_get_conf_handle(AL_CONTEXT_PARAM_DECL_SINGLE);
int al_get_dot11e_conf_handle(AL_CONTEXT_PARAM_DECL_SINGLE);
int al_get_acl_conf_handle(AL_CONTEXT_PARAM_DECL_SINGLE);
int al_get_mobility_conf_handle(AL_CONTEXT_PARAM_DECL_SINGLE);

#ifndef _AL_PACKET_DEBUG
al_packet_t *al_allocate_packet(AL_CONTEXT_PARAM_DECL int direction);

#define AL_PACKET_DEBUG_PARAM
#else
al_packet_t *al_allocate_packet(AL_CONTEXT_PARAM_DECL int direction, const char *filename, int line);

#define AL_PACKET_DEBUG_PARAM    , __FILE__, __LINE__
#endif

void al_release_packet(AL_CONTEXT_PARAM_DECL al_packet_t *packet);
void al_add_packet_reference(AL_CONTEXT_PARAM_DECL al_packet_t *packet);

int al_open_file(AL_CONTEXT_PARAM_DECL int file_id, int mode);
int al_read_file_line(AL_CONTEXT_PARAM_DECL int file_handle, char *string, int n);
int al_read_file(AL_CONTEXT_PARAM_DECL int file_handle, unsigned char *buffer, int bytes);
int al_write_file(AL_CONTEXT_PARAM_DECL int file_handle, unsigned char *buffer, int bytes);
int al_puts_file(AL_CONTEXT_PARAM_DECL int file_handle, const char *string);
int al_printf_file(AL_CONTEXT_PARAM_DECL int file_handle, const char *format, ...);
int al_close_file(AL_CONTEXT_PARAM_DECL int file_handle);
int al_print_log(AL_CONTEXT_PARAM_DECL int log_type, const char *format, ...);

int al_get_board_info(AL_CONTEXT_PARAM_DECL al_board_info_t *board_info);
int al_notify_message(AL_CONTEXT_PARAM_DECL al_notify_message_t *message);

unsigned char *al_get_gen_2KB_buffer(AL_CONTEXT_PARAM_DECL_SINGLE);
void al_release_gen_2KB_buffer(AL_CONTEXT_PARAM_DECL unsigned char *buffer);


/*************** NEW SET OF GBL SPIN & MUTEX LOCKS ****************************/

/* GBL SPIN_LOCK */
#define al_disable_interrupts(flags)    al_impl_disable_interrupts(flags)
#define al_enable_interrupts(flags)     al_impl_enable_interrupts(flags)

/* GBL MUTEX_LOCK */
#define _al_disable_interrupts(gbl_mutex, gbl_muvar, gbl_mupid)    _al_impl_disable_interrupts(gbl_mutex, gbl_muvar, gbl_mupid)
#define _al_enable_interrupts(gbl_mutex, gbl_muvar, gbl_mupid)     _al_impl_enable_interrupts(gbl_mutex, gbl_muvar, gbl_mupid)

/*************** NEW SET OF GENERIC SPIN & MUTEX LOCKS ****************************/

#define al_spin_lock(gen_lock, cpu_var)    al_impl_spin_lock(gen_lock, cpu_var)
#define al_spin_unlock(gen_lock, cpu_var)     al_impl_spin_unlock(gen_lock, cpu_var)
#define al_spin_lock_bh(gen_lock, cpu_var)    al_impl_spin_lock_bh(gen_lock, cpu_var)
#define al_spin_unlock_bh(gen_lock, cpu_var)     al_impl_spin_unlock_bh(gen_lock, cpu_var)

#define al_mutex_lock(gen_lock, cpu_var, p_pid)    al_impl_mutex_lock(gen_lock, cpu_var, p_pid)
#define al_mutex_unlock(gen_lock, cpu_var, p_pid)     al_impl_mutex_unlock(gen_lock, cpu_var, p_pid)

#define al_spin_lock_t(gen_lock)             al_impl_spin_lock_t(gen_lock)
#define al_spin_unlock_t(gen_lock)           al_impl_spin_unlock_t(gen_lock)
#define al_spin_lock_bh_t(gen_lock)          al_impl_spin_lock_bh_t(gen_lock)
#define al_spin_unlock_bh_t(gen_lock)        al_impl_spin_unlock_bh_t(gen_lock)
/**********************************************************************************/

#define al_preempt_disable()    preempt_disable()
#define al_preempt_enable()     preempt_enable()

int al_notify_ds_net_if(AL_CONTEXT_PARAM_DECL al_net_if_t *net_if);
int al_random_initialize(AL_CONTEXT_PARAM_DECL unsigned char *seed);
long al_random(AL_CONTEXT_PARAM_DECL int min, int max);
void al_cryptographic_random_bytes(AL_CONTEXT_PARAM_DECL unsigned char *buffer, int length);

#define AL_REBOOT_BOARD_CODE_MESH_THREAD_WATCHDOG     128
#define AL_REBOOT_BOARD_CODE_ETHERNET_LINK_DOWN       129
#define AL_REBOOT_BOARD_CODE_ETHERNET_LINK_UP         130
#define AL_REBOOT_BOARD_CODE_FIPS_SELF_TEST_AES       131
#define AL_REBOOT_BOARD_CODE_FIPS_SELF_TEST_HMAC      132
#define AL_REBOOT_BOARD_CODE_MESH_TABLE_PROTECTION    133
#define AL_REBOOT_BOARD_CODE_FIPS_RNG                 134
#define AL_REBOOT_BOARD_CODE_AL_WATCHDOG_THREAD       135
#define AL_REBOOT_BOARD_CODE_FIPS_SELF_TEST_RNG       136

void al_reboot_board(AL_CONTEXT_PARAM_DECL unsigned char reboot_code);

void al_restart_mesh(AL_CONTEXT_PARAM_DECL_SINGLE);
void al_get_board_mem_info(AL_CONTEXT_PARAM_DECL unsigned int *max_mem, unsigned int *free_mem);
void al_get_cpu_load_info(AL_CONTEXT_PARAM_DECL unsigned char *avg_load);
void al_get_gps_info(AL_CONTEXT_PARAM_DECL char *longitude, char *latitude, char *altitude, char *speed);
int al_atoi(AL_CONTEXT_PARAM_DECL char *string);

void al_enable_reset_generator(AL_CONTEXT_PARAM_DECL int enable_or_disable, int interval_in_millis);

void al_strobe_reset_generator(AL_CONTEXT_PARAM_DECL_SINGLE);

typedef void (*al_watchdog_routine_t)   (void);

int al_create_watchdog(AL_CONTEXT_PARAM_DECL const char *thread_name, int timeout_in_millis, al_watchdog_routine_t routine);
void al_destroy_watchdog(AL_CONTEXT_PARAM_DECL int watchdog_id);
void al_update_watchdog(AL_CONTEXT_PARAM_DECL int watchdog_id);
void al_update_watchdog_ap_thread(AL_CONTEXT_PARAM_DECL int watchdog_id);
void al_set_watchdog_timeout(AL_CONTEXT_PARAM_DECL int watchdog_id, int timeout_in_millis);
void al_suspend_watchdog(AL_CONTEXT_PARAM_DECL int watchdog_id);
void al_resume_watchdog(AL_CONTEXT_PARAM_DECL int watchdog_id);

void al_get_packet_pool_info(AL_CONTEXT_PARAM_DECL al_packet_pool_info_t *info);

void al_initialize(AL_CONTEXT_PARAM_DECL_SINGLE);
void al_uninitialize(AL_CONTEXT_PARAM_DECL_SINGLE);


int al_get_sip_conf_handle(AL_CONTEXT_PARAM_DECL_SINGLE);


/*  prototype to check the phy mode type as N OR AC or both
 * @function : cheks the interface physical mode and returns the value
 * input arguments : reference to phy_mode
 * return value : 1-only N ; 2-only AC ; 3-both N & AC ; 0-none 
 * */
#define AL_PHY_SUB_TYPE_NOT_SUPPORT_N_OR_AC      0 
#define AL_PHY_SUB_TYPE_SUPPORT_ONLY_N           1 
#define AL_PHY_SUB_TYPE_SUPPORT_ONLY_AC          2 
#define AL_PHY_SUB_TYPE_SUPPORT_BOTH_N_AND_AC    3 
extern int meshap_is_N_or_AC_supported_interface(int phy_sub_type);
#define _PROCESS_SKB_POOL_MAX_SIZE				2048
#define  MAIN_POOL								1
#define  SKB_POOL								0
#endif /*__AL_H__*/
