/********************************************************************************
* MeshDynamics
* --------------
* File     : al_socket.h
* Comments : Abstraction Layer  socket abstraction header
* Created  : 4/5/2004
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  4  |3/1/2007  | Added IP_HEADER_GET_LENGTH macro                | Sriram |
* -----------------------------------------------------------------------------
* |  3  |2/23/2007 | Moved field position macros from al_socket.h    |Prachiti|
* -----------------------------------------------------------------------------
* |  2  |5/5/2004  | udp/ip set functions added. (All others removed)| Anand  |
* -----------------------------------------------------------------------------
* |  1  |4/21/2004 | **Made (major) changes for AL_CONTEXT**         | Anand  |
* -----------------------------------------------------------------------------
* |  0  |4/5/2004  | Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

#include "al_ip.h"

#ifndef __AL_SOCKET_H__
#define __AL_SOCKET_H__

#define IP_PROTO_UDP                    17                                                             /* user datagram protocol */
#define IP_PROTO_TCP                    6                                                              /* transmission control protocol */

#define UDP_WORD_POSITION_SRC_PORT      0
#define UDP_WORD_POSITION_DST_PORT      (UDP_WORD_POSITION_SRC_PORT + 2)
#define UDP_WORD_POSITION_UDP_LENGTH    (UDP_WORD_POSITION_DST_PORT + 2)
#define UDP_WORD_POSITION_UDP_CHKSUM    (UDP_WORD_POSITION_UDP_LENGTH + 2)
#define UDP_POSITION_DATA               (UDP_WORD_POSITION_UDP_CHKSUM + 2)

#define UDP_HEADER_LENGTH               (UDP_POSITION_DATA)

#define IP_DEFAULT_TTL                  64

#define IP_HEADER_SET_VERSION(version_header_len, ver)    do { version_header_len &= 0x0F; version_header_len |= ((ver) << 4); } while (0)
#define IP_HEADER_SET_LENGTH(version_header_len, len)     do { version_header_len &= 0xF0; version_header_len |= ((len) & 0x0F); } while (0)
#define IP_HEADER_GET_LENGTH(version_header_len)          (version_header_len & 0x0F)

#define IP_HEADER_INIT_FRAGMENT(flags_frag)               do { flags_frag = 0; } while (0)
#define IP_MIN_HEADER_LENGTH          20

#define IP_BYTE_POSITION_VHL          0
#define IP_BYTE_POSITION_TOS          (IP_BYTE_POSITION_VHL + 1)
#define IP_WORD_POSITION_TOTLEN       (IP_BYTE_POSITION_TOS + 1)
#define IP_WORD_POSITION_ID           (IP_WORD_POSITION_TOTLEN + 2)
#define IP_WORD_POSITION_FLAGSFRAG    (IP_WORD_POSITION_ID + 2)
#define IP_BYTE_POSITION_TTL          (IP_WORD_POSITION_FLAGSFRAG + 2)
#define IP_BYTE_POSITION_PROTOCOL     (IP_BYTE_POSITION_TTL + 1)
#define IP_WORD_POSITION_CHKSUM       (IP_BYTE_POSITION_PROTOCOL + 1)
#define IP_DWORD_POSITION_SRCADDR     (IP_WORD_POSITION_CHKSUM + 2)
#define IP_DWORD_POSITION_DSTADDR     (IP_DWORD_POSITION_SRCADDR + 4)
#define IP_POSITION_OPTIONS           (IP_DWORD_POSITION_DSTADDR + 4)


int udp_setup_packet(al_packet_t    *packet,
                     al_net_addr_t  *from_addr,
                     unsigned short from_port,
                     al_net_addr_t  *to_addr,
                     unsigned short to_port,
                     unsigned char  *buffer,
                     int            length);

int ip_setup_packet(al_packet_t *packet);

int ip_setup_packet_ex(al_packet_t *packet, unsigned char *src_ip, unsigned char *dest_ip);
#endif /*__AL_SOCKET_H__*/
