/********************************************************************************
* MeshDynamics
* --------------
* File     : al_socket.c
* Comments : Torna MeshAP abstraction layer socket
* Created  : 5/30/2004
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  5  |2/23/2007 | Removed Protocol Field Macros                   |Prachiti|
* -----------------------------------------------------------------------------
* |  4  |10/4/2005 | UDP checksum set to 0                           | Sriram |
* -----------------------------------------------------------------------------
* |  3  |12/9/2004 | IP packet setup bug fixed                       | Sriram |
* -----------------------------------------------------------------------------
* |  2  |7/29/2004 | Src IP for IMCP packets changed                 | Anand  |
* -----------------------------------------------------------------------------
* |  1  |6/7/2004  | Set the packet type                             | Sriram |
* -----------------------------------------------------------------------------
* |  0  |5/30/2004 | Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

#include <generated/autoconf.h>

#include <linux/module.h>
#include <linux/init.h>
#include <linux/version.h>
#include <linux/netdevice.h>
#include <linux/skbuff.h>
#include <linux/wireless.h>

#include <linux/string.h>
#include "al.h"
#include "al_conf.h"
#include "al_802_11.h"
#include "al_impl_context.h"
#include "al_socket.h"


static int           _seq           = 0;
static unsigned char src_ap_addr[4] = { 0x0A, 0x00, 0x00, 0x0A };
static unsigned char dest_ap_addr[4] = { 0xFF, 0xFF, 0xFF, 0xFF };

#define al_net_pkt_set_byte(packet, position, value)        \
   do {                                                     \
      unsigned char word;                                   \
      word = (unsigned char)value;                          \
      memcpy((unsigned char *)packet + position, &word, 1); \
   } while (0)

#define al_net_pkt_set_word(packet, position, value)        \
   do {                                                     \
      unsigned short word;                                  \
      word = (unsigned short)value;                         \
      memcpy((unsigned char *)packet + position, &word, 2); \
   } while (0)

static inline unsigned short net_sum_words(unsigned char *packet, int length)
{
   unsigned long  total;
   unsigned short n, *p, carries, chksum;

   total = 0;
   n     = length / 2;
   p     = (unsigned short *)packet;

   while (n--)
   {
      total += *p++;
   }

   if (length & 1)
   {
      total += *((unsigned char *)p);
   }

   while (1)
   {
      carries = (unsigned short)(total >> 16);
      if (!carries)
      {
         break;
      }
      total = (total & 0xFFFF) + carries;
   }

   chksum = (unsigned short)total;

   return chksum;
}


static inline unsigned short net_chksum(unsigned short sum, unsigned char *packet, int length)
{
   unsigned long  temp;
   unsigned short carries;

   temp = sum;

   temp += net_sum_words(packet, length);

   while (1)
   {
      carries = (unsigned short)(temp >> 16);
      if (!carries)
      {
         break;
      }
      temp = (temp & 0xFFFF) + carries;
   }

   return htons(~((unsigned short)temp));
}


int udp_setup_packet(al_packet_t    *packet,
                     al_net_addr_t  *from_addr,
                     unsigned short from_port,
                     al_net_addr_t  *to_addr,
                     unsigned short to_port,
                     unsigned char  *buffer,
                     int            buf_length)

{
   unsigned char  *data;
   unsigned short udp_length;
   unsigned short udp_length_rev;
   unsigned short chksum;

#if 0
   unsigned char zero_protocol[] = { 0, IP_PROTO_UDP };
   unsigned char udp_pseudo[12];
#endif

   udp_length = UDP_HEADER_LENGTH + buf_length;
   data       = packet->buffer + packet->buffer_length - udp_length;

   udp_length_rev = htons(udp_length);

   al_net_pkt_set_word(data, UDP_WORD_POSITION_SRC_PORT, from_port);
   al_net_pkt_set_word(data, UDP_WORD_POSITION_DST_PORT, to_port);
   al_net_pkt_set_word(data, UDP_WORD_POSITION_UDP_LENGTH, udp_length_rev);
   al_net_pkt_set_word(data, UDP_WORD_POSITION_UDP_CHKSUM, 0);

   memcpy(data + UDP_POSITION_DATA,
          buffer,
          buf_length);

#if 0

   /**
    * Before we calculate the checksum, we need to
    * calculate the word sum of the IP pseudo header.
    */

   memcpy(udp_pseudo, src_ap_addr, 4);
   memcpy(udp_pseudo + 4, dest_ap_addr, 4);
   memcpy(udp_pseudo + 8, zero_protocol, 2);
   memcpy(udp_pseudo + 10, &udp_length_rev, 2);

   chksum = net_sum_words(udp_pseudo, 12);

   /**
    * Now calculate the checksum using the word sum of the IP pseudo header
    * of the pseudo IP header.
    */

   chksum = net_chksum(chksum, data, udp_length);

   chksum = htons(chksum);
#else
   chksum = 0;
#endif

   al_net_pkt_set_word(data, UDP_WORD_POSITION_UDP_CHKSUM, chksum);

   memcpy(&packet->addr_1,
          to_addr,
          sizeof(al_net_addr_t));

   memcpy(&packet->addr_2,
          from_addr,
          sizeof(al_net_addr_t));


   packet->flags |= AL_PACKET_TYPE_IP | AL_PACKET_FLAGS_ADDRESS_1 | AL_PACKET_FLAGS_ADDRESS_2;

   packet->position   -= udp_length;
   packet->data_length = udp_length;
   return 0;
}


int ip_setup_packet_ex(al_packet_t *packet, unsigned char *src_ip, unsigned char *dest_ip)
{
   unsigned char  *buffer;
   unsigned char  *temp;
   unsigned short total_len;
   unsigned char  version_header_len;
   unsigned short flags_frag;
   unsigned short chksum;

   version_header_len = 0;

   /**
    * Adjust the position and copy the IP header. (We don't do any options yet).
    */

   IP_HEADER_SET_VERSION(version_header_len, 4);
   IP_HEADER_SET_LENGTH(version_header_len, 5);
   IP_HEADER_INIT_FRAGMENT(flags_frag);

   total_len = IP_MIN_HEADER_LENGTH + packet->data_length;

   total_len = htons(total_len);

   buffer = packet->buffer + packet->position - IP_MIN_HEADER_LENGTH;

   al_net_pkt_set_byte(buffer, IP_BYTE_POSITION_VHL, version_header_len);
   al_net_pkt_set_byte(buffer, IP_BYTE_POSITION_TOS, 0);
   al_net_pkt_set_word(buffer, IP_WORD_POSITION_TOTLEN, total_len);
   al_net_pkt_set_word(buffer, IP_WORD_POSITION_ID, _seq);
   al_net_pkt_set_word(buffer, IP_WORD_POSITION_FLAGSFRAG, flags_frag);
   al_net_pkt_set_byte(buffer, IP_BYTE_POSITION_TTL, IP_DEFAULT_TTL);
   al_net_pkt_set_byte(buffer, IP_BYTE_POSITION_PROTOCOL, IP_PROTO_UDP);
   al_net_pkt_set_word(buffer, IP_WORD_POSITION_CHKSUM, 0);

   memcpy(buffer + IP_DWORD_POSITION_SRCADDR,
          (src_ip == NULL) ? src_ap_addr : src_ip,
          4);

   memcpy(buffer + IP_DWORD_POSITION_DSTADDR,
          (dest_ip == NULL) ? dest_ap_addr : dest_ip,
          4);

   temp = buffer;

   chksum = net_chksum(0, temp, IP_MIN_HEADER_LENGTH);

   chksum = htons(chksum);

   al_net_pkt_set_word(buffer, IP_WORD_POSITION_CHKSUM, chksum);

   packet->type         = htons(AL_PACKET_TYPE_IP);
   packet->position    -= IP_MIN_HEADER_LENGTH;
   packet->data_length += IP_MIN_HEADER_LENGTH;
   ++_seq;
   return 0;
}


int ip_setup_packet(al_packet_t *packet)
{
   return ip_setup_packet_ex(packet, src_ap_addr, dest_ap_addr);
}
