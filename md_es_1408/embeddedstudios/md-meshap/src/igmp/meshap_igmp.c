/********************************************************************************
* MeshDynamics
* --------------
* File     : meshap_igmp.c
* Comments : IGMP implementation for meshap
* Created  : 4/26/2007
* Author   : Prachiti Gaikwad
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  0  |4/26/2007 | Created.                                        |Prachiti|
* -----------------------------------------------------------------------------
********************************************************************************/

#include <linux/string.h>
#include "al.h"
#include "al_802_11.h"
#include "access_point.h"

#include "al_impl_context.h"
#include "al_socket.h"
#include "al_endian.h"
#include "meshap_igmp.h"


#define _IGMP_VERSION_1_MEMBERSHIP_QUERY               0x11
#define _IGMP_VERSION_1_MEMBERSHIP_REPORT              0x12
#define _IGMP_VERSION_2_MEMBERSHIP_REPORT              0x16
#define _IGMP_VERSION_2_LEAVE_GROUP                    0x17
#define _IGMP_VERSION_3_MEMBERSHIP_REPORT              0x22

#define _IGMP_PROTO                                    2

#define _IGMP_BYTE_POSITION_TYPE                       0

#define _IGMP_BYTE_POSITION_MAX_RESPONSE_TIME          (_IGMP_BYTE_POSITION_TYPE + 1)
#define _IGMP_WORD_POSITION_CHECKSUM                   (_IGMP_BYTE_POSITION_MAX_RESPONSE_TIME + 1)
#define _IGMP_DWORD_GROUP_IP_ADDRESS                   (_IGMP_WORD_POSITION_CHECKSUM + 2)

#define _IGMP_V3_BYTE_POSITION_RESERVED1               (_IGMP_BYTE_POSITION_TYPE + 1)
#define _IGMP_V3_WORD_POSITION_CHECKSUM                (_IGMP_V3_BYTE_POSITION_RESERVED1 + 1)
#define _IGMP_V3_WORD_POSITION_RESERVED2               (_IGMP_V3_WORD_POSITION_CHECKSUM + 2)
#define _IGMP_V3_WORD_POSITION_NUM_RECORDS             (_IGMP_V3_WORD_POSITION_RESERVED2 + 2)
#define _IGMP_V3_STRUCT_POSITION_RECORDS               (_IGMP_V3_WORD_POSITION_NUM_RECORDS + 2)

#define _IGMP_V3_BYTE_POSITION_RECORD_TYPE             0
#define _IGMP_V3_BYTE_POSITION_AUX_DATA_LEN            (_IGMP_V3_BYTE_POSITION_RECORD_TYPE + 1)
#define _IGMP_V3_WORD_POSITION_NUM_SOURCES             (_IGMP_V3_BYTE_POSITION_AUX_DATA_LEN + 1)
#define _IGMP_V3_DWORD_MCAST_IP_ADDRESS                (_IGMP_V3_WORD_POSITION_NUM_SOURCES + 2)
#define _IGMP_V3_LIST_SOURCE_IP_ADDRESS                (_IGMP_V3_DWORD_MCAST_IP_ADDRESS + 4)

#define _IGMP_V3_SIZE_RECORD_TYPE                      1
#define _IGMP_V3_SIZE_AUX_DATA_LEN                     1
#define _IGMP_V3_SIZE_NUM_SOURCES                      2
#define _IGMP_V3_SIZE_MCAST_IP_ADDRESS                 4
#define _IGMP_V3_SIZE_SOURCE_IP_ADDRESS                4


#define _IGMP_V3_RECORD_TYPE_MODE_IS_INCLUDE           1
#define _IGMP_V3_RECORD_TYPE_MODE_IS_EXCLUDE           2
#define _IGMP_V3_RECORD_TYPE_CHANGE_TO_INCLUDE_MODE    3
#define _IGMP_V3_RECORD_TYPE_CHANGE_TO_EXCLUDE_MODE    4

/* copy the group address */

static AL_INLINE int _process_igmp_membership_report_packet(AL_CONTEXT_PARAM_DECL unsigned char *packet_buffer, meshap_igmp_group_info_t *group_info)
{
   memset(group_info, 0, sizeof(meshap_igmp_group_info_t));

   group_info->count = 1;

   memcpy(&group_info->group_record[0].address, &packet_buffer[_IGMP_DWORD_GROUP_IP_ADDRESS], MESHAP_IGMP_IP_ADDR_LEN);
   group_info->group_record[0].type = MESHAP_IGMP_TYPE_JOIN_GROUP;

   return 0;
}


static AL_INLINE int _process_igmp_leave_group_packet(AL_CONTEXT_PARAM_DECL unsigned char *packet_buffer, meshap_igmp_group_info_t *group_info)
{
   memset(group_info, 0, sizeof(meshap_igmp_group_info_t));

   group_info->count = 1;

   memcpy(&group_info->group_record[0].address, &packet_buffer[_IGMP_DWORD_GROUP_IP_ADDRESS], MESHAP_IGMP_IP_ADDR_LEN);
   group_info->group_record[0].type = MESHAP_IGMP_TYPE_LEAVE_GROUP;

   return 0;
}


static AL_INLINE int _process_igmp_v3_membership_report_packet(AL_CONTEXT_PARAM_DECL unsigned char *packet_buffer, meshap_igmp_group_info_t *group_info)
{
   int           i;
   short         num_of_sources;
   int           aux_data_len, type;
   unsigned char *p;

   memset(group_info, 0, sizeof(meshap_igmp_group_info_t));

   p = packet_buffer + _IGMP_V3_WORD_POSITION_NUM_RECORDS;

   memcpy(&group_info->count, p, 2);

   group_info->count = al_be16_to_cpu(group_info->count);

   p = packet_buffer + _IGMP_V3_STRUCT_POSITION_RECORDS;

   for (i = 0; i < group_info->count && i < MESHAP_IGMP_V3_MAX_GROUP_RECORDS; i++)
   {
      type = *p;
      p   += _IGMP_V3_SIZE_RECORD_TYPE;

      aux_data_len = *p;
      p           += _IGMP_V3_SIZE_AUX_DATA_LEN;
      memcpy(&num_of_sources, p, 2);
      p += _IGMP_V3_SIZE_NUM_SOURCES;
      memcpy(&group_info->group_record[i].address, p, 4);
      p += _IGMP_V3_SIZE_MCAST_IP_ADDRESS;

      p += (num_of_sources * _IGMP_V3_SIZE_SOURCE_IP_ADDRESS);                  /* ignore source address list */
      p += aux_data_len;                                                        /* ignore aux data */

      if ((type == _IGMP_V3_RECORD_TYPE_CHANGE_TO_EXCLUDE_MODE) && (num_of_sources == 0))
      {
         group_info->group_record[i].type = MESHAP_IGMP_TYPE_JOIN_GROUP;
      }
      else if ((type == _IGMP_V3_RECORD_TYPE_CHANGE_TO_INCLUDE_MODE) && (num_of_sources == 0))
      {
         group_info->group_record[i].type = MESHAP_IGMP_TYPE_LEAVE_GROUP;
      }
   }

   return 0;
}


int meshap_igmp_process_membership_packet(AL_CONTEXT_PARAM_DECL al_packet_t *packet, meshap_igmp_group_info_t *group_info)
{
   unsigned char *p;
   int           type;
   int           ip_header_length;


   ip_header_length = 0;
   type             = 0;

   p = packet->buffer + packet->position;

   if (p[IP_BYTE_POSITION_PROTOCOL] == _IGMP_PROTO)
   {
      ip_header_length  = IP_HEADER_GET_LENGTH(p[IP_BYTE_POSITION_VHL]);
      ip_header_length *= 4;                    /* IP header length is the number of 32 bit words in the IP header */

      p   += ip_header_length;
      type = p[_IGMP_BYTE_POSITION_TYPE];

      switch (type)
      {
      case _IGMP_VERSION_1_MEMBERSHIP_QUERY:
      case _IGMP_VERSION_1_MEMBERSHIP_REPORT:
      case _IGMP_VERSION_2_MEMBERSHIP_REPORT:
         return _process_igmp_membership_report_packet(p, group_info);

      case _IGMP_VERSION_2_LEAVE_GROUP:
         return _process_igmp_leave_group_packet(p, group_info);

      case _IGMP_VERSION_3_MEMBERSHIP_REPORT:
         return _process_igmp_v3_membership_report_packet(p, group_info);
      }
   }

   return -1;
}
