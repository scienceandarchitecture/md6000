/********************************************************************************
* MeshDynamics
* --------------
* File     : meshap_igmp.h
* Comments : Meshap igmp header file
* Created  : 4/26/2007
* Author   : Prachiti Gaikwad
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  0  |6/20/2007 | Created.                                        |Prachiti|
* -----------------------------------------------------------------------------
********************************************************************************/

#ifndef __MESHAP_IGMP_HEADER_H__
#define __MESHAP_IGMP_HEADER_H__

#define MESHAP_IGMP_V3_MAX_GROUP_RECORDS    10

#define MESHAP_IGMP_TYPE_JOIN_GROUP         1
#define MESHAP_IGMP_TYPE_LEAVE_GROUP        2

#define MESHAP_IGMP_ETH_ADDR_LEN            6
#define MESHAP_IGMP_IP_ADDR_LEN             4

struct meshap_igmp_group_info
{
   unsigned short count;
   struct
   {
      unsigned char address[MESHAP_IGMP_IP_ADDR_LEN];
      unsigned char type;                       /* MESHAP_IGMP_TYPE_* */
   }
                  group_record[MESHAP_IGMP_V3_MAX_GROUP_RECORDS];
};
typedef struct meshap_igmp_group_info   meshap_igmp_group_info_t;


#define MESHAP_IGMP_MCAST_MAC_FROM_IP_ADDR(_mac_addr, _ip_addr) \
   do {                                                         \
      memset((_mac_addr), 0, sizeof(al_net_addr_t));            \
      (_mac_addr)->length   = MESHAP_IGMP_ETH_ADDR_LEN;         \
      (_mac_addr)->bytes[0] = 0x01;                             \
      (_mac_addr)->bytes[1] = 0x00;                             \
      (_mac_addr)->bytes[2] = 0x5E;                             \
      (_mac_addr)->bytes[3] = ((_ip_addr)[1] & 0x7F);           \
      (_mac_addr)->bytes[4] = ((_ip_addr)[2]);                  \
      (_mac_addr)->bytes[5] = ((_ip_addr)[3]);                  \
   } while (0)

int meshap_igmp_process_membership_packet(AL_CONTEXT_PARAM_DECL al_packet_t *packet, meshap_igmp_group_info_t *group_info);

#endif /*__MESHAP_IGMP_HEADER_H__*/
