/********************************************************************************
* MeshDynamics
* --------------
* File     : meshap_panic.c
* Comments : Meshap Kernel Panic and Trap Handler
* Created  : 4/14/2005
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  2  |8/6/2007  | Disable RESET generator before EEPROM write     | Sriram |
* -----------------------------------------------------------------------------
* |  1  |7/24/2007 | REBOOT logging changes                          | Sriram |
* -----------------------------------------------------------------------------
* |  0  |4/14/2005 | Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

#include <generated/autoconf.h>
#include <linux/version.h>
#include <linux/module.h>
#include <linux/init.h>
#include <linux/delay.h>
#include <linux/kernel.h>
#include <linux/notifier.h>
#include <linux/reboot.h>
#include <linux/console.h>
#include <linux/netdevice.h>
#include <linux/skbuff.h>
#include <linux/wireless.h>


#include <linux/string.h>
#include "meshap.h"
#include "torna_hw_id.h"

//extern struct notifier_block*	die_notifier_list;

static int _meshap_die_event(struct notifier_block *notifier, unsigned long l, void *p);
static int _meshap_panic_event(struct notifier_block *notifier, unsigned long l, void *p);
static void _meshap_console_write(struct console *con, const char *s, unsigned n);

#define _MESHAP_LOG_BUFFER_SIZE    (16 * 1024)

static char _meshap_log_buffer[_MESHAP_LOG_BUFFER_SIZE];
static char _meshap_log_buffer2[_MESHAP_LOG_BUFFER_SIZE];
static int  _meshap_log_buffer_head;
static int  _meshap_log_buffer_tail;

static struct notifier_block _die_notifier =
{
   _meshap_die_event, NULL, 0xFFFF
};

static struct notifier_block _panic_notifier =
{
   _meshap_panic_event, NULL, 0xFFFF
};

static struct console _meshap_console =
{
   .name   = "meshap",
   .write  = _meshap_console_write,
   .device = NULL,
   .flags  = CON_ENABLED,
   .index  = -1
};

static torna_reboot_info_t _reboot_info;
static unsigned int        _reboot_stack_dump[64];
static int                 _in_panic;

void meshap_panic_initialize(void)
{
   _meshap_log_buffer_head = 0;
   _meshap_log_buffer_tail = 0;
   _in_panic = 0;

   printk(KERN_EMERG "RAMESH16MIG : @@@ func : %s line : %d raw_notifier_chain_register commented\n", __func__, __LINE__);
//	 raw_notifier_chain_register(&die_notifier_list, &_die_notifier);
#ifdef TMP_CNS_ONLY
	 raw_notifier_chain_register(&panic_notifier_list, &_panic_notifier);
#endif
   register_console(&_meshap_console);
}


void meshap_panic_uninitialize(void)
{
   unregister_console(&_meshap_console);
   printk(KERN_EMERG "RAMESH16MIG : @@@ func : %s line : %d raw_notifier_chain_unregister commented\n", __func__, __LINE__);
//	raw_notifier_chain_unregister(&die_notifier_list, &_die_notifier);
#ifdef TMP_CNS_ONLY
	raw_notifier_chain_unregister(&panic_notifier_list, &_panic_notifier);
#endif
}


#define _TOKEN_PC_LR_LENGTH      7
#define _TOKEN_PROCESS_LENGTH    8
#define _TOKEN_PID_LENGTH        5
#define _TOKEN_STACK_LENGTH      8
#define _TOKEN_DUMP_LENGTH       2

static const char _token_pc[]        = "pc : [<";
static const char _token_lr[]        = "lr : [<";
static const char _token_process[]   = "Process ";
static const char _token_pid[]       = "(pid:";
static const char _token_stack[]     = "Stack: (";
static const char _token_dump[]      = ": ";
static const char _token_backtrace[] = "Backtrace: ";

static int _parse_pc_lr_register(char *base)
{
   char         temp[32];
   char         *pos;
   unsigned int tempi;

   pos = base;

   pos = strstr(pos, _token_pc);

   if (pos == NULL)
   {
      printk(KERN_INFO "meshap: Panic event stack dump parse error (pc)\n");
      return -1;
   }

   pos += _TOKEN_PC_LR_LENGTH;
   memcpy(temp, pos, 8);
   temp[8] = 0;
   sscanf(temp, "%x", &tempi);
   pos += 8;
   memcpy(_reboot_info.pc_register, &tempi, 4);

   pos = strstr(pos, _token_lr);

   if (pos == NULL)
   {
      printk(KERN_INFO "meshap: Panic event stack dump parse error (lr)\n");
      return -2;
   }

   pos += _TOKEN_PC_LR_LENGTH;
   memcpy(temp, pos, 8);
   temp[8] = 0;
   sscanf(temp, "%x", &tempi);
   pos += 8;
   memcpy(_reboot_info.caller_register, &tempi, 4);

   return(pos - base);
}


static int _parse_process_name(char *base)
{
   char *pos;
   char *pos2;
   int  i;

   pos = base;

   pos = strstr(pos, _token_process);

   if (pos == NULL)
   {
      printk(KERN_INFO "meshap: Panic event stack dump parse error (process)\n");
      return -3;
   }

   pos += _TOKEN_PROCESS_LENGTH;

   pos2 = strstr(pos, _token_pid);

   if (pos2 == NULL)
   {
      printk(KERN_INFO "meshap: Panic event stack dump parse error (pid)\n");
      return -4;
   }

   for (i = 0; (i < sizeof(_reboot_info.process_name) - 1) && (pos < pos2); i++, pos++)
   {
      _reboot_info.process_name[i] = *pos;
   }

   _reboot_info.process_name[i] = 0;

   pos2 += _TOKEN_PID_LENGTH;

   return(pos2 - base);
}


static int _parse_stack_dump(char *base)
{
   char         *pos;
   char         *pos2;
   int          i;
   int          j;
   char         temp[16];
   unsigned int address;

   pos = base;

   pos = strstr(pos, _token_stack);

   if (pos == NULL)
   {
      printk(KERN_INFO "meshap: Panic event stack dump parse error (stack)\n");
      return -5;
   }

   pos += _TOKEN_STACK_LENGTH;

   pos2 = strstr(pos, _token_backtrace);

   if (pos2 == NULL)
   {
      printk(KERN_INFO "meshap: Panic event stack dump parse error (backtrace)\n");
      return -6;
   }

   i = 0;

   while (i < 64)
   {
      pos = strstr(pos, _token_dump);

      if ((pos == NULL) || (pos >= pos2))
      {
         break;
      }

      pos += _TOKEN_DUMP_LENGTH;

      for (j = 0; j < 8; j++)
      {
         memcpy(temp, pos, 8);
         temp[8] = 0;
         address = 0;
         sscanf(temp, "%x", &address);
         if ((address != 0) && (i < 64))
         {
            _reboot_stack_dump[i++] = address;
         }
         pos += 9;
      }
   }

   return i;
}


static int _meshap_panic_parse_dump(void)
{
   char         *pos;
   int          ret;
   int          length;
   unsigned int pc_reg;
   unsigned int caller_reg;

   /**
    * Linearize the log buffer into _meshap_log_buffer2
    */

   if (_meshap_log_buffer_head > _meshap_log_buffer_tail)
   {
      length = _MESHAP_LOG_BUFFER_SIZE - _meshap_log_buffer_tail;
      memcpy(_meshap_log_buffer2, &_meshap_log_buffer[_meshap_log_buffer_tail], length);
      memcpy(_meshap_log_buffer2 + length, _meshap_log_buffer, _meshap_log_buffer_tail);
      length += _meshap_log_buffer_tail;
   }
   else
   {
      length = (_meshap_log_buffer_tail - _meshap_log_buffer_head);
      memcpy(_meshap_log_buffer2, _meshap_log_buffer, length);
   }

   _meshap_log_buffer2[length] = 0;

   pos = _meshap_log_buffer2;

   ret = _parse_pc_lr_register(pos);

   if (ret < 0)
   {
      return ret;
   }

   pos += ret;

   ret = _parse_process_name(pos);

   if (ret < 0)
   {
      return ret;
   }

   pos += ret;

   ret = _parse_stack_dump(pos);

   if (ret > 0)
   {
      _reboot_info.stack_dump_count = ret;
   }
   else
   {
      _reboot_info.stack_dump_count = 0;
   }

   memcpy(&pc_reg, _reboot_info.pc_register, sizeof(_reboot_info.pc_register));
   memcpy(&caller_reg, _reboot_info.caller_register, sizeof(_reboot_info.caller_register));

   printk(KERN_INFO "meshap: PC=%08X LR=%08X PROC=%s Dump Count=%d\n",
          pc_reg,
          caller_reg,
          _reboot_info.process_name,
          ret);

   return 0;
}


static int _meshap_panic_event(struct notifier_block *notifier, unsigned long l, void *p)
{
   struct sysinfo si;
   unsigned int   used_bytes;
   unsigned int   used_meg_bytes;
   unsigned long  flags;

   si_meminfo(&si);

   used_bytes      = (si.totalram - si.freeram) * PAGE_SIZE;
   used_meg_bytes  = used_bytes / 1024;
   used_meg_bytes /= 1024;

   if (!_in_panic)
   {
#ifdef _NO_PANIC
      _in_panic = 1;

      /** Disable the reset generator before writing to EEPROM */

      meshap_enable_reset_generator(0, 6000);

      _meshap_panic_parse_dump();

      _reboot_info.code     = MESHAP_REBOOT_MACHINE_CODE_CRASH;
      _reboot_info.temp     = (unsigned char)meshap_get_board_temp();
      _reboot_info.voltage  = (unsigned char)meshap_get_board_voltage();
      _reboot_info.mem_used = used_meg_bytes;

      local_irq_save(flags);

      torna_put_reboot_info(&_reboot_info, _reboot_stack_dump);

      local_irq_restore(flags);

      /** Enable it back after writing to EEPROM (Pre-caution in case it doesn't reboot) */

      meshap_enable_reset_generator(1, 6000);

      printk(KERN_INFO "meshap: Panic event notifier\nRebooting in %d seconds...\n", _TORNA_MESHAP_REBOOT_TIME_);

      mdelay(_TORNA_MESHAP_REBOOT_TIME_ * 1000);
#endif
      kernel_restart(NULL);
   }
   else
   {
      kernel_restart(NULL);
   }

   return 0;
}


static int _meshap_die_event(struct notifier_block *notifier, unsigned long l, void *p)
{
   struct sysinfo si;
   unsigned int   used_bytes;
   unsigned int   used_meg_bytes;
   unsigned long  flags;

   si_meminfo(&si);

   used_bytes      = (si.totalram - si.freeram) * PAGE_SIZE;
   used_meg_bytes  = used_bytes / 1024;
   used_meg_bytes /= 1024;

   if (!_in_panic)
   {
      _in_panic = 1;

      /** Disable the reset generator before writing to EEPROM */

      meshap_enable_reset_generator(0, 6000);

      _meshap_panic_parse_dump();

      _reboot_info.code     = MESHAP_REBOOT_MACHINE_CODE_CRASH;
      _reboot_info.temp     = (unsigned char)meshap_get_board_temp();
      _reboot_info.voltage  = (unsigned char)meshap_get_board_voltage();
      _reboot_info.mem_used = used_meg_bytes;

      local_irq_save(flags);

      torna_put_reboot_info(&_reboot_info, _reboot_stack_dump);

      local_irq_restore(flags);

      /** Enable it back after writing to EEPROM (Pre-caution in case it doesn't reboot) */

      meshap_enable_reset_generator(1, 6000);

      printk(KERN_INFO "meshap: Die event notifier\nRebooting in %d seconds...\n", _TORNA_MESHAP_REBOOT_TIME_);

      mdelay(_TORNA_MESHAP_REBOOT_TIME_ * 1000);

      kernel_restart(NULL);
   }
   else
   {
      kernel_restart(NULL);
   }

   return 0;
}


static void _meshap_console_write(struct console *con, const char *s, unsigned n)
{
   while (n > 0)
   {
      _meshap_log_buffer[_meshap_log_buffer_tail] = *s;

      if (++_meshap_log_buffer_tail == _MESHAP_LOG_BUFFER_SIZE)
      {
         _meshap_log_buffer_tail = 0;
      }

      if (_meshap_log_buffer_tail == _meshap_log_buffer_head)
      {
         if (++_meshap_log_buffer_head == _MESHAP_LOG_BUFFER_SIZE)
         {
            _meshap_log_buffer_head = 0;
         }
      }

      --n;
      ++s;
   }
}
