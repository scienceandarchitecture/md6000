/********************************************************************************
* MeshDynamics
* --------------
* File     : meshap_version_proc.c
* Comments : Meshap /proc FS version information
* Created  : 12/26/2004
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  1  |6/18/2005 | Misc changes                                    | Sriram |
* -----------------------------------------------------------------------------
* |  0  |12/26/2004| Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

#include <generated/autoconf.h>
#include <linux/version.h>
#include <linux/module.h>
#include <linux/init.h>
#include <linux/if.h>
#include <linux/netdevice.h>
#include <linux/skbuff.h>
#include <linux/cache.h>
#include <linux/pci.h>
#include <linux/wireless.h>
#include <asm/uaccess.h>
#include <net/iw_handler.h>
#include <linux/delay.h>

#include "meshap_proc.h"


//static int _proc_read_routine(char *page, char **start, off_t off,int count, int *eof, void *data)
static int _proc_read_routine(struct seq_file *m, void *v)
{
   /*char						*p;
    *
    * if(off != 0) {
    * eof	= 1;
    *      return 0;
    * }
    *
    * start		= page + off;
    * p			= page;
    *
    * p			+= sprintf(p,"%s\n","_TORNA_VERSION_STRING");
    *
    * return (p - page);	*/


   seq_printf(m, "%s\n", "_TORNA_VERSION_STRING");

   return 0;
}


static int _read_routine_open(struct inode *inode, struct file *file)
{
   return single_open(file, _proc_read_routine, NULL);
}


static const struct file_operations _read_routine_fops =
{
   .owner   = THIS_MODULE,
   .open    = _read_routine_open,
   .read    = seq_read,
   .llseek  = seq_lseek,
   .release = single_release,
};

void meshap_version_proc_initialize(void)
{
   meshap_proc_add_read_entry("version", &_read_routine_fops, NULL);
}
