/********************************************************************************
* MeshDynamics
* --------------
* File     : al_packet_impl.c
* Comments : Torna abstraction layer packet implementation
* Created  : 5/26/2004
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* | 25  |01/07/2008| Changes for mixed mode	                      |Prachiti|
* -----------------------------------------------------------------------------
* | 24  |11/8/2007 | Changes for queued retry                        |Prachiti|
* -----------------------------------------------------------------------------
* | 23  |8/16/2007 | al_packet_impl_get_pool_info Added              | Sriram |
* -----------------------------------------------------------------------------
* | 22  |7/24/2007 | REBOOT logging changes                          | Sriram |
* -----------------------------------------------------------------------------
* | 21  |6/6/2007  | Changes for Fixed rates in RX                   | Sriram |
* -----------------------------------------------------------------------------
* | 20  |4/11/2007 | Changes for Packet specific rates               | Sriram |
* -----------------------------------------------------------------------------
* | 19  |2/23/2007 | Changes for EFFISTREAM                          |Prachiti|
* -----------------------------------------------------------------------------
* | 18  |1/10/2007 | Changes for Ignore flag                         | Sriram |
* -----------------------------------------------------------------------------
* | 17  |11/8/2006 | Changes for compression support                 | Sriram |
* -----------------------------------------------------------------------------
* | 16  |03/01/2006| WME class copied in received packets            | Sriram |
* -----------------------------------------------------------------------------
* | 15  |02/15/2006| changes for dot11e                              | Bindu  |
* -----------------------------------------------------------------------------
* | 14  |10/5/2005 | Misc changes                                    | Sriram |
* -----------------------------------------------------------------------------
* | 13  |10/5/2005 | Changes for enabling MIP                        | Sriram |
* -----------------------------------------------------------------------------
* | 12  |9/22/2005 | Interrupt disable/enable semantics changed      | Sriram |
* -----------------------------------------------------------------------------
* | 11  |6/12/2005 | Packet pool recovery reboot added               | Sriram |
* -----------------------------------------------------------------------------
* | 10  |4/15/2005 | Encryption flag set for incoming packets        | Sriram |
* -----------------------------------------------------------------------------
* |  9  |4/12/2005 | Heap fallback logic conditionalized             | Sriram |
* -----------------------------------------------------------------------------
* |  8  |4/8/2005  | Vlan changes Normalized and Corrected           | Sriram |
* -----------------------------------------------------------------------------
* |  7  |4/6/2005  | Security changes                                | Sriram |
* -----------------------------------------------------------------------------
* |  6  |2/24/2005 | vlan functionality added                        | Abhijit|
* -----------------------------------------------------------------------------
* |  5  |12/22/2004| Changes for using 802.11b rates                 | Sriram |
* -----------------------------------------------------------------------------
* |  4  |12/16/2004| Fixed wireless-wireless bug                     | Sriram |
* -----------------------------------------------------------------------------
* |  3  |9/03/2004 | create_outgoing_packet_from_skb  func added     | Anand  |
* -----------------------------------------------------------------------------
* |  2  |7/22/2004 | al_packet_impl_create_skb and misc changes      | Sriram |
* -----------------------------------------------------------------------------
* |  1  |6/10/2004 | Misc removal of commented code                  | Sriram |
* -----------------------------------------------------------------------------
* |  0  |5/26/2004 | Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

#include <generated/autoconf.h>

#include <linux/module.h>
#include <linux/init.h>
#include <linux/version.h>
#include <linux/netdevice.h>
#include <linux/skbuff.h>
#include <linux/wireless.h>
#include <linux/ieee80211.h>
#include <linux/spinlock.h>
//#include <net/mac80211.h>
#include "al.h"
#include "al_802_11.h"
#include "meshap.h"
#include "meshap_logger.h"
#include "meshap_core.h"
#include "torna_log.h"
#include "al_packet_impl.h"
#include "torna_mac_hdr.h"
#include "torna_types.h"

#ifndef _MESH_VIRTUAL_NET_IF_NAME_
#define _MESH_VIRTUAL_NET_IF_NAME_                    "mip0"
#endif

#define _AL_PACKET_IMPL_PACKET_POOL_RECOVERY_COUNT    500000


//extern meshap_core_net_if_t *meshap_get_core_net_if(struct sk_buff *skb);

extern void dump_bytes(char *, int);
access_point_sta_entry_t *access_point_get_sta(AL_CONTEXT_PARAM_DECL al_net_addr_t *addr);
access_point_sta_entry_t *access_point_get_sta_ex(AL_CONTEXT_PARAM_DECL al_net_addr_t *addr, int from_interrupt_context);
void access_point_release_sta(AL_CONTEXT_PARAM_DECL access_point_sta_entry_t *sta_entry);
void access_point_release_sta_ex(AL_CONTEXT_PARAM_DECL access_point_sta_entry_t *sta_entry, int notify_mesh, int notify_child);
void access_point_release_sta_ex2(AL_CONTEXT_PARAM_DECL access_point_sta_entry_t *sta_entry, int notify_mesh, int notify_child, int from_interrupt_context);

static meshap_core_packet_t *_pool_packet_head;
static meshap_core_packet_t *_pool_packet_tail;
static meshap_core_packet_t *_pool[_TORNA_MESHAP_PACKET_POOL_SIZE_];
static int _pool_empty_count;
static int _pool_current;
static int _pool_min;

static meshap_core_packet_t *_procs_skb_pool_pkt_head;
static meshap_core_packet_t *_procs_skb_pool_pkt_tail;
static meshap_core_packet_t *_ap_thread_pool_pkt_head;
static meshap_core_packet_t *_ap_thread_pool_pkt_tail;
static meshap_core_packet_t *_imcp_thread_pool_pkt_head;
static meshap_core_packet_t *_imcp_thread_pool_pkt_tail;



extern void dump_bytes(char *D, int count);

static inline meshap_core_packet_t *_create_core_packet(void)
{
   meshap_core_packet_t *core_packet;

   core_packet = (meshap_core_packet_t *)kmalloc(sizeof(meshap_core_packet_t), GFP_ATOMIC);

   if (core_packet == NULL)
   {
	  al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"MESH_AP:INFO: %s<%d> kmalloc failed\n",__func__,__LINE__);
      return NULL;
   }

   memset(core_packet, 0, sizeof(meshap_core_packet_t));

   core_packet->al_packet.addr_1.length = ETH_ALEN;
   core_packet->al_packet.addr_2.length = ETH_ALEN;
   core_packet->al_packet.addr_3.length = ETH_ALEN;
   core_packet->al_packet.addr_4.length = ETH_ALEN;
   core_packet->type = MESHAP_CORE_PACKET_TYPE_HEAP;

	q_packet_stats.core_packet_type_alloc_heap++;

   MESHAP_ATOMIC_SET(core_packet->ref_count, 1);

   return core_packet;
}


static inline meshap_core_packet_t *_create_core_packet_from_pool(char pool_type)
{
   meshap_core_packet_t *core_packet;
   unsigned long        flags;
   static int           out_of_pool_message_printed = 0;
   MESH_USAGE_PROFILING_INIT

   if (pool_type)
   {
	   MESH_USAGE_PROFILE_START(start)
	   al_spin_lock(core_pkt_splock, flags);
	   MESH_USAGE_PROFILE_END(profiling_info.core_pkt_splock_1,index,start,end)

	   if (_pool_packet_head != NULL)
	   {
		   core_packet = _pool_packet_head;
		   _pool_packet_head = _pool_packet_head->next_pool;

		   if (_pool_packet_head == NULL)
		   {
			   _pool_packet_tail = NULL;
		   }

		   MESHAP_ATOMIC_SET(core_packet->ref_count, 1);

		   --_pool_current;
		   q_packet_stats.core_packet_pool_current_count--;

		   if (_pool_current < _pool_min)
		   {
			   _pool_min = _pool_current;
		   }

		   out_of_pool_message_printed = 0;
		   if (_pool_empty_count > 0)
		   {
			   --_pool_empty_count;
		   }
		   MESH_USAGE_PROFILE_START(start)
		   al_spin_unlock(core_pkt_splock, flags);
		   MESH_USAGE_PROFILE_END(profiling_info.core_pkt_spunlock_1,index,start,end)
		   return core_packet;
	   }

	   MESH_USAGE_PROFILE_START(start)
	   al_spin_unlock(core_pkt_splock, flags);
	   MESH_USAGE_PROFILE_END(profiling_info.core_pkt_spunlock_1,index,start,end)
	   if (out_of_pool_message_printed == 0)
	   {
		   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"MESH_AP:INFO: %s<%d> Out of POOL packets\n",__func__,__LINE__);
		   out_of_pool_message_printed = 1;
	   }

   }
   else
   {
	   if (_procs_skb_pool_pkt_head != NULL)
	   {
		   core_packet = _procs_skb_pool_pkt_head;
		   core_packet->al_packet.pkt_status = RX_PKT;
		   _procs_skb_pool_pkt_head = _procs_skb_pool_pkt_head->next_pool;
		   MESHAP_ATOMIC_SET(core_packet->ref_count, 1);

		   q_packet_stats.procs_skb_core_pkt_pool_current_count--;
		   if (_procs_skb_pool_pkt_head == NULL)
		   {
			   _refill_procs_skb_core_pkt_pool_from_common_pool();
		   }
		   return core_packet;
	   }
   }

#ifndef _MESHAP_HEAP_FALLBACK_

   /**
	* In a live system, after we run out of pool packets,
	* creating packets out of the heap can be dangerous
	* in a broadcast flood situation, wherein we exhaust
	* the heap. It has been found out that even when packets
	* are being transferred at full speed, the pool would
	* never run out, hence we simply return NULL
	*/
   ++_pool_empty_count;
   if (_pool_empty_count >= _AL_PACKET_IMPL_PACKET_POOL_RECOVERY_COUNT)
   {
	  meshap_core_reboot_machine(MESHAP_REBOOT_MACHINE_CODE_PACKET);
   }
   return NULL;
#else
   return _create_core_packet();
#endif

}

void _refill_procs_skb_core_pkt_pool_from_common_pool(void)
{
	MESH_USAGE_PROFILING_INIT

	if (q_packet_stats.ap_thread_core_pkt_pool_current_count > q_packet_stats.imcp_thread_core_pkt_pool_current_count)
	{
		MESH_USAGE_PROFILE_START(start)
		al_spin_lock(ap_thread_core_pkt_splock, flags);
		MESH_USAGE_PROFILE_END(profiling_info.core_pkt_splock_2,index,start,end)
		_procs_skb_pool_pkt_head 	= _ap_thread_pool_pkt_head;
		_procs_skb_pool_pkt_tail 	= _ap_thread_pool_pkt_tail;
		_ap_thread_pool_pkt_head 	= NULL;
		_ap_thread_pool_pkt_tail	= NULL;
		q_packet_stats.procs_skb_core_pkt_pool_current_count =+ q_packet_stats.ap_thread_core_pkt_pool_current_count;
		q_packet_stats.ap_thread_core_pkt_pool_current_count = 0;
		MESH_USAGE_PROFILE_START(start)
		al_spin_unlock(ap_thread_core_pkt_splock, flags);
		MESH_USAGE_PROFILE_END(profiling_info.core_pkt_spunlock_2,index,start,end)
	}
	else if (q_packet_stats.imcp_thread_core_pkt_pool_current_count)
	{
		MESH_USAGE_PROFILE_START(start)
		al_spin_lock(imcp_thread_core_pkt_splock, flags);
		MESH_USAGE_PROFILE_END(profiling_info.core_pkt_splock_3,index,start,end)
		_procs_skb_pool_pkt_head 	= _imcp_thread_pool_pkt_head;
		_procs_skb_pool_pkt_tail 	= _imcp_thread_pool_pkt_tail;
		_imcp_thread_pool_pkt_head 	= NULL;
		_imcp_thread_pool_pkt_tail	= NULL;
		q_packet_stats.procs_skb_core_pkt_pool_current_count =+ q_packet_stats.imcp_thread_core_pkt_pool_current_count;
		q_packet_stats.imcp_thread_core_pkt_pool_current_count = 0;
		MESH_USAGE_PROFILE_START(start)
		al_spin_unlock(imcp_thread_core_pkt_splock, flags);
		MESH_USAGE_PROFILE_END(profiling_info.core_pkt_spunlock_3,index,start,end)
	}
	else
	{
		al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MESH_AP:INFO: "
			"ERROR: _refill_core_packet_from_lockless_pool else condition hit\n");
	}
}

void al_packet_impl_get_pool_info(al_packet_pool_info_t *info)
{
   info->pool_max     = _TORNA_MESHAP_PACKET_POOL_SIZE_;
   info->pool_min     = _pool_min;
   info->pool_current = _pool_current;
}


void al_packet_impl_initialize(void)
{
   int i;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Enter %s<%d>\n",__func__,__LINE__);
   for (i = 0, _pool_packet_head = NULL; i < _TORNA_MESHAP_PACKET_POOL_SIZE_; i++)
   {
      _pool[i]            = _create_core_packet();
      _pool[i]->type      = MESHAP_CORE_PACKET_TYPE_POOL;
      _pool[i]->next_pool = NULL;
	  if (i < _PROCESS_SKB_POOL_MAX_SIZE)
	  {
	     if(_procs_skb_pool_pkt_head == NULL)
	     {
	        _procs_skb_pool_pkt_head = _pool[i];
			_procs_skb_pool_pkt_tail = _pool[i];
			q_packet_stats.procs_skb_core_pkt_pool_current_count = _PROCESS_SKB_POOL_MAX_SIZE;
	     }
		 else
		 {
			_procs_skb_pool_pkt_tail->next_pool = _pool[i];
			_procs_skb_pool_pkt_tail			   = _pool[i];
		 }
	  }
	  else
	  {
	     if (_pool_packet_head == NULL)
	  {
            _pool_packet_head = _pool[i];
            _pool_packet_tail = _pool[i];
			q_packet_stats.core_packet_pool_current_count = (_TORNA_MESHAP_PACKET_POOL_SIZE_ - _PROCESS_SKB_POOL_MAX_SIZE);
         }
         else
         {
            _pool_packet_tail->next_pool = _pool[i];
            _pool_packet_tail            = _pool[i];
         }
	  }
      MESHAP_ATOMIC_SET(_pool[i]->ref_count, 0);
   }

   _pool_empty_count = 0;
   _pool_current     = (_TORNA_MESHAP_PACKET_POOL_SIZE_ - _PROCESS_SKB_POOL_MAX_SIZE);
   _pool_min         = (_TORNA_MESHAP_PACKET_POOL_SIZE_ - _PROCESS_SKB_POOL_MAX_SIZE);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Exit %s<%d>\n",__func__,__LINE__);
}


void al_packet_impl_uninitialize(void)
{
   int i;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Enter %s<%d>\n",__func__,__LINE__);
   for (i = 0; i < _TORNA_MESHAP_PACKET_POOL_SIZE_; i++)
   {
      if (_pool[i]->skb != NULL)
      {
         dev_kfree_skb(_pool[i]->skb);
      }
      kfree(_pool[i]);
   }
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Exit %s<%d>\n",__func__,__LINE__);
}


/**
 * Called by meshap_ip_device, when it receives a skb from upstack.
 */
meshap_core_packet_t *al_packet_impl_create_outgoing_packet_from_skb(struct sk_buff *skb)
{
   meshap_core_packet_t *core_packet;
   char pool_type = SKB_POOL;
   core_packet = _create_core_packet_from_pool(pool_type);

   if (core_packet == NULL)
   {
	  (tx_rx_pkt_stats.packet_drop[35])++;
      return NULL;
   }

   core_packet->incoming_if = NULL;
   core_packet->skb         = skb;

   core_packet->al_packet.buffer        = skb->data;
   core_packet->al_packet.buffer_length = skb->len;
   core_packet->al_packet.position      = 0;
   core_packet->al_packet.data_length   = skb->len;
#if MULTIPLE_AP_THREAD
   core_packet->al_packet.status = 0;
   core_packet->al_packet.entry = NULL;
#endif
   memcpy(core_packet->al_packet.addr_1.bytes, SKB_ETHERNET(skb)->h_dest, ETH_ALEN);
   memcpy(core_packet->al_packet.addr_2.bytes, SKB_ETHERNET(skb)->h_source, ETH_ALEN);

   core_packet->al_packet.addr_1.length = ETH_ALEN;
   core_packet->al_packet.addr_2.length = ETH_ALEN;
   core_packet->al_packet.type          = SKB_ETHERNET(skb)->h_proto;
   core_packet->al_packet.flags         = AL_PACKET_FLAGS_BUFFER | AL_PACKET_FLAGS_ADDRESS_1 | AL_PACKET_FLAGS_ADDRESS_2 | AL_PACKET_FLAGS_USER_APP_PACKET;
   core_packet->packet_direction        = MESHAP_CORE_PACKET_DIRECTION_OUTGOING;

   /**
    *	User packets for accesspoint will be untagged by default
    *	since ap is not a part of any vlan
    */
   core_packet->al_packet.dot1p_priority = 0;
   core_packet->al_packet.dot1q_tag      = AL_PACKET_VLAN_TAG_TYPE_UNTAGGED;
   core_packet->al_packet.dot1q_mode     = AL_PACKET_DOT_1Q_MODE_NONE;
   core_packet->al_packet.src_addr_sta_entry = NULL;
   core_packet->al_packet.dest_addr_sta_entry = NULL;
   return core_packet;
}


/**
 * Called by meshap_core, when it receives a skb from a network interface
 */

struct ieee80211_rx_status;
meshap_core_packet_t *al_packet_impl_create_from_skb(meshap_core_net_if_t *incoming_if, struct sk_buff *skb, char pool_type)
{
   meshap_core_packet_t *core_packet;
   torna_mac_hdr_t      *mac_hdr;
   unsigned short       vlan_info;
   unsigned short       original_type;
   meshap_core_net_if_t *net_if = NULL;
   void *ptr;

   core_packet = _create_core_packet_from_pool(pool_type);
   if (core_packet == NULL)
   {
	  (tx_rx_pkt_stats.packet_drop[36])++;
      return NULL;
   }

   core_packet->packet_direction = MESHAP_CORE_PACKET_DIRECTION_INCOMMING;
   core_packet->incoming_if      = incoming_if;
   core_packet->skb = NULL;

   core_packet->al_packet.buffer        = skb->head;
   core_packet->al_packet.buffer_length = skb->end - skb->head;
   core_packet->al_packet.position      = skb->data - skb->head;
   core_packet->al_packet.data_length   = skb->len;

#if MULTIPLE_AP_THREAD
   core_packet->al_packet.status = 0;
   core_packet->al_packet.entry = NULL;
#endif
   /* vlan packet will be untagged by default */
   core_packet->al_packet.dot1p_priority = 0;
   core_packet->al_packet.dot1q_tag      = AL_PACKET_VLAN_TAG_TYPE_UNTAGGED;
   core_packet->al_packet.dot1q_mode     = AL_PACKET_DOT_1Q_MODE_NONE;

   switch (incoming_if->dev_info->encapsulation)
   {
   case MESHAP_NET_DEV_ENCAPSULATION_ETHERNET:

      /**
       * Ethernet devices do not have a torna_mac_hdr, hence we only copy over
       * addr1,addr2 and type fields.
       */
      memcpy(core_packet->al_packet.addr_1.bytes, SKB_ETHERNET(skb)->h_dest, ETH_ALEN);
      memcpy(core_packet->al_packet.addr_2.bytes, SKB_ETHERNET(skb)->h_source, ETH_ALEN);
      core_packet->al_packet.type  = SKB_ETHERNET(skb)->h_proto;
      core_packet->al_packet.flags = AL_PACKET_FLAGS_BUFFER | AL_PACKET_FLAGS_ADDRESS_1 | AL_PACKET_FLAGS_ADDRESS_2;

      if (SKB_ETHERNET(skb)->h_proto == ntohs(AL_PACKET_TYPE_VLAN))
      {
         memcpy(&vlan_info, skb->data, AL_PACKET_VLAN_INFO_TAG_SIZE);
         memcpy(&original_type, skb->data + AL_PACKET_VLAN_INFO_TAG_SIZE, sizeof(unsigned short));

         skb_pull(skb, AL_PACKET_VLAN_INFO_DATA_SIZE);

         vlan_info = ntohs(vlan_info);
         core_packet->al_packet.type = original_type;

         core_packet->al_packet.dot1p_priority = AL_PACKET_VLAN_INFO_GET_PRIORITY(vlan_info);
         core_packet->al_packet.dot1q_tag      = AL_PACKET_VLAN_INFO_GET_TAG(vlan_info);
         core_packet->al_packet.dot1q_mode     = AL_PACKET_DOT_1Q_MODE_TAG_ADD;

         core_packet->al_packet.position    += AL_PACKET_VLAN_INFO_DATA_SIZE;
         core_packet->al_packet.data_length -= AL_PACKET_VLAN_INFO_DATA_SIZE;
      }
      break;

   case MESHAP_NET_DEV_ENCAPSULATION_802_11:

      /**
       * 802.11 devices include a torna_mac_hdr instead of the standard ethernet header.
       * We copy over all relevant 802.11 fields.
       */
	   net_if =  incoming_if;
      if (NULL == net_if)
      {
	     (tx_rx_pkt_stats.packet_drop[37])++;
         al_release_packet(core_packet);
         return NULL;
      }

      //TORNA header which we stored in skb->cb in driver code
      mac_hdr = net_if->dev_info->meshap_get_torna_hdr_cb(skb);
      if (NULL == mac_hdr)
      {
	     (tx_rx_pkt_stats.packet_drop[38])++;
         al_release_packet(core_packet);
         return NULL;
      }

      memcpy(core_packet->al_packet.addr_1.bytes, mac_hdr->addresses[0], ETH_ALEN);
      memcpy(core_packet->al_packet.addr_2.bytes, mac_hdr->addresses[1], ETH_ALEN);
      memcpy(core_packet->al_packet.addr_3.bytes, mac_hdr->addresses[2], ETH_ALEN);
      memcpy(core_packet->al_packet.addr_4.bytes, mac_hdr->addresses[3], ETH_ALEN);

      core_packet->al_packet.type          = mac_hdr->ethernet.h_proto;
      core_packet->al_packet.frame_control = mac_hdr->frame_control;
//QOS_SUPPORT
      FILL_QOS_HDR_FROM_TORNA(core_packet->al_packet, mac_hdr);
      core_packet->al_packet.rssi          = mac_hdr->rssi;
      core_packet->al_packet.bit_rate      = mac_hdr->tx_rate;

      core_packet->al_packet.flags = AL_PACKET_FLAGS_BUFFER
                                     | AL_PACKET_FLAGS_ADDRESS_1
                                     | AL_PACKET_FLAGS_ADDRESS_2
                                     | AL_PACKET_FLAGS_ADDRESS_3
                                     | AL_PACKET_FLAGS_ADDRESS_4
                                     | AL_PACKET_FLAGS_FRAME_CONTROL
                                     | AL_PACKET_FLAGS_RSSI;

      if (mac_hdr->flags & TORNA_MAC_HDR_FLAG_ENCRYPTION)
      {
         core_packet->al_packet.flags |= AL_PACKET_FLAGS_ENCRYPTION;
      }

      if (mac_hdr->flags & TORNA_MAC_HDR_FLAG_DOT11E_ENABLED)
      {
         core_packet->al_packet.flags          |= AL_PACKET_FLAGS_DOT11E_ENABLED;
         core_packet->al_packet.dot11e_category = mac_hdr->wme_class;
      }

      if (mac_hdr->flags & TORNA_MAC_HDR_FLAG_ADHOC)
      {
         core_packet->al_packet.flags |= AL_PACKET_FLAGS_ADHOC;
      }

      if (mac_hdr->flags & TORNA_MAC_HDR_FLAG_VIRTUAL)
      {
         core_packet->al_packet.flags |= AL_PACKET_FLAGS_VIRTUAL;
      }

      if (mac_hdr->flags & TORNA_MAC_HDR_FLAG_MESH_IGNORE)
      {
         core_packet->al_packet.flags |= AL_PACKET_FLAGS_MESH_IGNORE;
      }

      if (mac_hdr->flags & TORNA_MAC_HDR_FLAG_EFFISTREAM)
      {
         core_packet->al_packet.flags |= AL_PACKET_FLAGS_EFFISTREAM;
      }

      if (mac_hdr->flags & TORNA_MAC_HDR_FLAG_FIXED_RATE)
      {
         core_packet->al_packet.flags |= AL_PACKET_FLAGS_FIXED_RATE;
      }

      if (mac_hdr->flags & TORNA_MAC_HDR_FLAG_QUEUED_RETRY)
      {
         core_packet->al_packet.flags |= AL_PACKET_FLAGS_QUEUED_RETRY;
      }

      if (mac_hdr->ethernet.h_proto == ntohs(AL_PACKET_TYPE_VLAN))
      {
         vlan_info = ntohs(mac_hdr->vlan_info);

         core_packet->al_packet.type = mac_hdr->vlan_original_type;

         core_packet->al_packet.dot1p_priority = AL_PACKET_VLAN_INFO_GET_PRIORITY(vlan_info);
         core_packet->al_packet.dot1q_tag      = AL_PACKET_VLAN_INFO_GET_TAG(vlan_info);
         core_packet->al_packet.dot1q_mode     = AL_PACKET_DOT_1Q_MODE_TAG_ADD;
      }

      ptr = (void *)net_if->dev_info->meshap_get_drv_data(skb);
	  if (ptr && (AL_802_11_FC_GET_TYPE(mac_hdr->frame_control) == AL_802_11_FC_TYPE_DATA)) {
		  kfree(ptr);
	  }

      break;
   }
   core_packet->skb = skb;
   core_packet->al_packet.src_addr_sta_entry = NULL;
   core_packet->al_packet.dest_addr_sta_entry = NULL;
   return core_packet;
}


/**
 * Called by MESH/AP code when allocating a packet.
 */
meshap_core_packet_t *al_packet_impl_create(void)
{
   meshap_core_packet_t *core_packet;
   char pool_type = MAIN_POOL;
   core_packet = _create_core_packet_from_pool(pool_type);

   if (core_packet == NULL)
   {
	  al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"MESH_AP:INFO: %s<%d> Out of POOL packets\n",__func__,__LINE__);
      return NULL;
   }

   core_packet->skb = dev_alloc_skb(2464);

   if (core_packet->skb == NULL)
   {
	    al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP:ERROR: %s<%d> skb allocation failed\n",__func__,__LINE__);
	    (tx_rx_pkt_stats.packet_drop[48])++;
		al_release_packet(core_packet);
      return NULL;
   }

   core_packet->packet_direction        = MESHAP_CORE_PACKET_DIRECTION_NEW;
   core_packet->al_packet.buffer        = core_packet->skb->data;
   core_packet->al_packet.buffer_length = core_packet->skb->end - core_packet->skb->data;
   core_packet->al_packet.flags         = AL_PACKET_FLAGS_BUFFER;
   core_packet->al_packet.frame_control = 0;
   core_packet->incoming_if      = NULL;

   /**
    * MESH/AP packets will be untagged by default since mesh node is not a part of any vlan
    */

   core_packet->al_packet.dot1p_priority = 0;
   core_packet->al_packet.dot1q_tag      = AL_PACKET_VLAN_TAG_TYPE_UNTAGGED;
   core_packet->al_packet.dot1q_mode     = AL_PACKET_DOT_1Q_MODE_NONE;
   core_packet->al_packet.src_addr_sta_entry = NULL;
   core_packet->al_packet.dest_addr_sta_entry = NULL;

#if MULTIPLE_AP_THREAD
   core_packet->al_packet.status = 0;
   core_packet->al_packet.entry = NULL;
#endif
   return core_packet;
}


static inline void _add_vlan_info_to_802_3_packet(al_packet_t *al_packet, struct ethhdr *ethhdr, unsigned char *data)
{
   unsigned short vlan_info;

   vlan_info = 0;

   ethhdr->h_proto = htons(AL_PACKET_TYPE_VLAN);

   AL_PACKET_VLAN_INFO_SET_TAG(vlan_info, al_packet->dot1q_tag);
   AL_PACKET_VLAN_INFO_SET_PRIORITY(vlan_info, al_packet->dot1p_priority);

   vlan_info = htons(vlan_info);

   memcpy(data, &vlan_info, AL_PACKET_VLAN_INFO_TAG_SIZE);
   memcpy((data + AL_PACKET_VLAN_INFO_TAG_SIZE), &(al_packet->type), 2);
}


static inline unsigned short _remove_vlan_info_from_802_3_packet(meshap_core_packet_t *core_packet)
{
   return core_packet->al_packet.type;
}


static inline void _add_vlan_info_to_802_11_packet(al_packet_t *al_packet, torna_mac_hdr_t *hdr)
{
   unsigned short vlan_info;

   vlan_info = 0;

   AL_PACKET_VLAN_INFO_SET_TAG(vlan_info, al_packet->dot1q_tag);
   AL_PACKET_VLAN_INFO_SET_PRIORITY(vlan_info, al_packet->dot1p_priority);

   vlan_info = htons(vlan_info);

   hdr->vlan_info          = vlan_info;
   hdr->vlan_original_type = al_packet->type;
}


static inline unsigned short _remove_vlan_info_from_802_11_packet(meshap_core_packet_t *core_packet)
{
   return core_packet->al_packet.type;
}


static inline void _setup_skb(meshap_core_net_if_t *outgoing_if, meshap_core_packet_t *core_packet)
{
   struct ethhdr        *ethhdr;
   meshap_mac_hdr_t      *hdr;
   unsigned char        *data;
   meshap_core_net_if_t *net_if = NULL;
   struct sk_buff       *skb =core_packet->skb;

   /**
    * This core_packet was created using al_packet_impl_create and hence
    * is a packet originating directly from the MESH/AP code
    */

   switch (outgoing_if->dev_info->encapsulation)
   {
   case MESHAP_NET_DEV_ENCAPSULATION_ETHERNET:

      if (core_packet->al_packet.dot1q_mode == AL_PACKET_DOT_1Q_MODE_TAG_ADD)
      {
         /**
          *	Adjust packet position to accomodate vlan info with ethernet header
          */
         core_packet->al_packet.position    -= (ETH_HLEN + AL_PACKET_VLAN_INFO_DATA_SIZE);
         core_packet->al_packet.data_length += (ETH_HLEN + AL_PACKET_VLAN_INFO_DATA_SIZE);

         ethhdr = (struct ethhdr *)(core_packet->al_packet.buffer + core_packet->al_packet.position);
         data   = (unsigned char *)(core_packet->al_packet.buffer + core_packet->al_packet.position + ETH_HLEN);

         memcpy(ethhdr->h_dest, core_packet->al_packet.addr_1.bytes, ETH_ALEN);
         memcpy(ethhdr->h_source, core_packet->al_packet.addr_2.bytes, ETH_ALEN);

         _add_vlan_info_to_802_3_packet(&(core_packet->al_packet), ethhdr, data);
      }
      else
      {
         /**
          * Ethernet devices expect a plain simple ethernet packet.
          */
         core_packet->al_packet.position    -= ETH_HLEN;
         core_packet->al_packet.data_length += ETH_HLEN;
         ethhdr = (struct ethhdr *)(core_packet->al_packet.buffer + core_packet->al_packet.position);
         memcpy(ethhdr->h_dest, core_packet->al_packet.addr_1.bytes, ETH_ALEN);
         memcpy(ethhdr->h_source, core_packet->al_packet.addr_2.bytes, ETH_ALEN);
         ethhdr->h_proto = core_packet->al_packet.type;
      }

      core_packet->skb->mac_header = (u8 *)ethhdr;

      /**
       * Now we setup the sk_buff's data and tail according to the position and data_length
       */
      skb_reserve(core_packet->skb, core_packet->al_packet.position);
      skb_put(core_packet->skb, core_packet->al_packet.data_length);
      break;

   case MESHAP_NET_DEV_ENCAPSULATION_802_11:

      if (core_packet->al_packet.dot1q_mode == AL_PACKET_DOT_1Q_MODE_TAG_ADD)
      {
         /**
          *  Adjust packet position to accomodate vlan info with ethernet header
          */
         core_packet->al_packet.position    -= (ETH_HLEN + AL_PACKET_VLAN_INFO_DATA_SIZE);
         core_packet->al_packet.data_length += (ETH_HLEN + AL_PACKET_VLAN_INFO_DATA_SIZE);

         ethhdr = (struct ethhdr *)(core_packet->al_packet.buffer + core_packet->al_packet.position);
         data   = (unsigned char *)(core_packet->al_packet.buffer + core_packet->al_packet.position + ETH_HLEN);

         memcpy(ethhdr->h_dest, core_packet->al_packet.addr_1.bytes, ETH_ALEN);
         memcpy(ethhdr->h_source, core_packet->al_packet.addr_2.bytes, ETH_ALEN);

         _add_vlan_info_to_802_3_packet(&(core_packet->al_packet), ethhdr, data);
      }
      else
      {
         /**
          * Ethernet devices expect a plain simple ethernet packet.
          */
         core_packet->al_packet.position    -= ETH_HLEN;
         core_packet->al_packet.data_length += ETH_HLEN;
         ethhdr = (struct ethhdr *)(core_packet->al_packet.buffer + core_packet->al_packet.position);
         memcpy(ethhdr->h_dest, core_packet->al_packet.addr_1.bytes, ETH_ALEN);
         memcpy(ethhdr->h_source, core_packet->al_packet.addr_2.bytes, ETH_ALEN);
         ethhdr->h_proto = core_packet->al_packet.type;
      }

      core_packet->skb->mac_header = (u8 *)ethhdr;

      /**
       * 802.11 devices expect a torna_mac_hdr prepended to the packet data
       */
      net_if = outgoing_if;
      if (NULL == net_if)
      {
	     (tx_rx_pkt_stats.packet_drop[39])++;
         return;
      }
      hdr = net_if->dev_info->meshap_get_tx_meshap_mac_hdr_cb(core_packet->skb);
      hdr->frame_control = core_packet->al_packet.frame_control;
      hdr->qos_control      = core_packet->al_packet.qos_control;

      memcpy(hdr->addresses[0], core_packet->al_packet.addr_1.bytes, ETH_ALEN);
      memcpy(hdr->addresses[1], core_packet->al_packet.addr_2.bytes, ETH_ALEN);
      memcpy(hdr->addresses[2], core_packet->al_packet.addr_3.bytes, ETH_ALEN);
      memcpy(hdr->addresses[3], core_packet->al_packet.addr_4.bytes, ETH_ALEN);
      /**
       * Now we setup the sk_buff's data and tail according to the position and data_length
       */
      skb_reserve(core_packet->skb, core_packet->al_packet.position);
      skb_put(core_packet->skb, core_packet->al_packet.data_length);

		skb_set_mac_header(core_packet->skb, 0);
		skb_set_network_header(core_packet->skb, ETH_HLEN);
		skb_set_transport_header(core_packet->skb, ETH_HLEN + 20);

      break;
   }
}

#define _SWITCH_TYPE_NONE             -1
#define _SWITCH_TYPE_ETH_ETH          0
#define _SWITCH_TYPE_ETH_802_11       1
#define _SWITCH_TYPE_802_11_ETH       2
#define _SWITCH_TYPE_802_11_802_11    3

static inline int _get_switch_type(meshap_core_net_if_t *outgoing_if, meshap_core_net_if_t *incoming_if)
{
   switch (incoming_if->dev_info->encapsulation)
   {
   case MESHAP_NET_DEV_ENCAPSULATION_ETHERNET:
      switch (outgoing_if->dev_info->encapsulation)
      {
      case MESHAP_NET_DEV_ENCAPSULATION_ETHERNET:
         return _SWITCH_TYPE_ETH_ETH;

      case MESHAP_NET_DEV_ENCAPSULATION_802_11:
         return _SWITCH_TYPE_ETH_802_11;
      }
      break;

   case MESHAP_NET_DEV_ENCAPSULATION_802_11:
      switch (outgoing_if->dev_info->encapsulation)
      {
      case MESHAP_NET_DEV_ENCAPSULATION_ETHERNET:
         return _SWITCH_TYPE_802_11_ETH;

      case MESHAP_NET_DEV_ENCAPSULATION_802_11:
         return _SWITCH_TYPE_802_11_802_11;
      }
      break;
   }

   return _SWITCH_TYPE_NONE;
}


static inline void _setup_skb_out_going(meshap_core_net_if_t *outgoing_if, meshap_core_packet_t *core_packet)
{
   unsigned char        *data;
   meshap_mac_hdr_t      *hdr;
   struct ethhdr        *ethhdr;
   meshap_core_net_if_t *net_if = NULL;

   /**
    * This core_packet was created using al_packet_impl_create_outgoing_packet_from_skb and hence
    * is a packet that originated from meshap_ip_device.Packets originating from ap are untagged
    * hence we only setup the skbuff and dont add vlan tag
    */

   switch (outgoing_if->dev_info->encapsulation)
   {
   case MESHAP_NET_DEV_ENCAPSULATION_ETHERNET:      /**ETHERNET*/

      /**
       * The MAC header was pulled off the packet by eth_type_trans ,therefore
       * we push it back. The packet is now ready to go.
       */
      skb_push(core_packet->skb, ETH_HLEN);

      skb_set_mac_header(core_packet->skb, 0);
      skb_set_network_header(core_packet->skb, ETH_HLEN);
      skb_set_transport_header(core_packet->skb, ETH_HLEN + 20);
      break;

   case MESHAP_NET_DEV_ENCAPSULATION_802_11:      /**802.11*/

      skb_push(core_packet->skb, ETH_HLEN);

      /**
       * 802.11 devices expect a torna_mac_hdr_t prepended to the packet's data.
       * Since ethernet devices do not reserve enough head room, we simply
       * create a torna_mac_hdr on the heap and assign it to skb->mac.raw
       */
      net_if = outgoing_if;
	  if (NULL == net_if)
      {
	     (tx_rx_pkt_stats.packet_drop[41])++;
         return;
      }
      hdr = net_if->dev_info->meshap_get_tx_meshap_mac_hdr_cb(core_packet->skb);
      hdr->frame_control    = core_packet->al_packet.frame_control;
      hdr->qos_control      = core_packet->al_packet.qos_control;
      memcpy(hdr->addresses[0], core_packet->al_packet.addr_1.bytes, ETH_ALEN);
      memcpy(hdr->addresses[1], core_packet->al_packet.addr_2.bytes, ETH_ALEN);
      memcpy(hdr->addresses[2], core_packet->al_packet.addr_3.bytes, ETH_ALEN);
      memcpy(hdr->addresses[3], core_packet->al_packet.addr_4.bytes, ETH_ALEN);
      skb_set_mac_header(core_packet->skb, 0);
      skb_set_network_header(core_packet->skb, ETH_HLEN);
      skb_set_transport_header(core_packet->skb, ETH_HLEN + 20);

      break;
   }
}

static inline void _resetup_skb(meshap_core_net_if_t *outgoing_if, meshap_core_packet_t *core_packet)
{
   struct ethhdr        *ethhdr;
   meshap_mac_hdr_t      *hdr;
   unsigned char        *data;
   unsigned short       packet_type;
   meshap_core_net_if_t *net_if = NULL;
   struct sk_buff       *skb =core_packet->skb;

   net_if = outgoing_if;
   if (NULL == net_if)
   {
	  (tx_rx_pkt_stats.packet_drop[43])++;
      return;
   }


   /**
    * This core_packet was created using al_packet_impl_create_from_skb and hence
    * is a packet that originated from one of the drivers. Now we have to re-setup
    * the sk_buff.
    */

   switch (_get_switch_type(outgoing_if, core_packet->incoming_if))
   {
   case _SWITCH_TYPE_ETH_ETH:  /**ETHERNET->ETHERNET*/

      /**
       * The MAC header was pulled off the packet by eth_type_trans ,therefore
       * we push it back. The packet is now ready to go.
       */
      if (core_packet->al_packet.dot1q_mode == AL_PACKET_DOT_1Q_MODE_TAG_ADD)
      {
         /*Setup header space and copy vlan info into the header and data portion */
         skb_push(core_packet->skb, ETH_HLEN + AL_PACKET_VLAN_INFO_DATA_SIZE);

         ethhdr = (struct ethhdr *)core_packet->skb->data;
         data   = (unsigned char *)core_packet->skb->data + ETH_HLEN;

         memcpy(ethhdr->h_dest, core_packet->al_packet.addr_1.bytes, ETH_ALEN);
         memcpy(ethhdr->h_source, core_packet->al_packet.addr_2.bytes, ETH_ALEN);

         _add_vlan_info_to_802_3_packet(&(core_packet->al_packet), ethhdr, data);

         core_packet->skb->mac_header = (u8 *)ethhdr;
      }
      else if (core_packet->al_packet.dot1q_mode == AL_PACKET_DOT_1Q_MODE_TAG_REMOVE)
      {
         /* remove 4 bytes vlan info from packet and add eth header */
         packet_type = _remove_vlan_info_from_802_3_packet(core_packet);
         skb_push(core_packet->skb, ETH_HLEN);
         ethhdr = (struct ethhdr *)core_packet->skb->data;
         memcpy(ethhdr->h_dest, core_packet->al_packet.addr_1.bytes, ETH_ALEN);
         memcpy(ethhdr->h_source, core_packet->al_packet.addr_2.bytes, ETH_ALEN);
         ethhdr->h_proto = packet_type;
         core_packet->skb->mac_header = (u8 *)ethhdr;
      }
      else
      {
         skb_push(core_packet->skb, ETH_HLEN);
      }
      break;

   case _SWITCH_TYPE_ETH_802_11:  /**ETHERNET->802.11*/


      if (core_packet->al_packet.dot1q_mode == AL_PACKET_DOT_1Q_MODE_TAG_ADD)
      {
         /**
          *  Adjust packet position to accomodate vlan info with ethernet header
          */
         skb_push(core_packet->skb,ETH_HLEN + AL_PACKET_VLAN_INFO_DATA_SIZE);	//VLAN
         core_packet->al_packet.position    -= (ETH_HLEN + AL_PACKET_VLAN_INFO_DATA_SIZE);
         core_packet->al_packet.data_length += (ETH_HLEN + AL_PACKET_VLAN_INFO_DATA_SIZE);

         ethhdr = (struct ethhdr *)(core_packet->al_packet.buffer + core_packet->al_packet.position);
         data   = (unsigned char *)(core_packet->al_packet.buffer + core_packet->al_packet.position + ETH_HLEN);

         memcpy(ethhdr->h_dest, core_packet->al_packet.addr_1.bytes, ETH_ALEN);
         memcpy(ethhdr->h_source, core_packet->al_packet.addr_2.bytes, ETH_ALEN);

         _add_vlan_info_to_802_3_packet(&(core_packet->al_packet), ethhdr, data);
      }
      else if (core_packet->al_packet.dot1q_mode == AL_PACKET_DOT_1Q_MODE_TAG_REMOVE)
      {
         /* remove 4 bytes vlan info from packet and add eth header */
         packet_type = _remove_vlan_info_from_802_3_packet(core_packet);
         skb_push(core_packet->skb, ETH_HLEN);
         ethhdr = (struct ethhdr *)core_packet->skb->data;
         memcpy(ethhdr->h_dest, core_packet->al_packet.addr_1.bytes, ETH_ALEN);
         memcpy(ethhdr->h_source, core_packet->al_packet.addr_2.bytes, ETH_ALEN);
         ethhdr->h_proto = packet_type;
         core_packet->skb->mac_header     = (u8 *)ethhdr;
         core_packet->skb->network_header = (char *)core_packet->skb->data + ETH_HLEN;
      }
      else
      {
          /* 
           * For Broadcast/Multicast packets we are allocating skb in 
           * _wm_broadcast_send_helper function and we are not updating 
           * skb->data, skb->len and skb->tail. So we are updating 
           * skb->data and skb->tail pointers here
           */

          if(core_packet->skb && (0==core_packet->skb->len)) {
              skb_put(core_packet->skb, core_packet->al_packet.position + core_packet->al_packet.data_length);
              skb_pull(core_packet->skb, core_packet->al_packet.position);

              skb_push(core_packet->skb, ETH_HLEN);
              ethhdr = (struct ethhdr *)core_packet->skb->data;
              memcpy(ethhdr->h_dest, core_packet->al_packet.addr_1.bytes, ETH_ALEN);
              memcpy(ethhdr->h_source, core_packet->al_packet.addr_2.bytes, ETH_ALEN);
              ethhdr->h_proto = core_packet->al_packet.type;
              core_packet->skb->mac_header     = (u8 *)ethhdr;
              core_packet->skb->network_header = (char *)core_packet->skb->data + ETH_HLEN;

          } else {
              skb_push(core_packet->skb, ETH_HLEN);
              core_packet->skb->network_header = (char *)core_packet->skb->data + ETH_HLEN;
          }
      }

      /**
       * 802.11 devices expect a torna_mac_hdr_t prepended to the packet's data.
       * Since ethernet devices do not reserve enough head room, we simply
       * create a torna_mac_hdr on the heap and assign it to skb->mac.raw
       */
      hdr = net_if->dev_info->meshap_get_tx_meshap_mac_hdr_cb(core_packet->skb);
      hdr->frame_control    = core_packet->al_packet.frame_control;
      hdr->qos_control      = core_packet->al_packet.qos_control;
      memcpy(hdr->addresses[0], core_packet->al_packet.addr_1.bytes, ETH_ALEN);
      memcpy(hdr->addresses[1], core_packet->al_packet.addr_2.bytes, ETH_ALEN);
      memcpy(hdr->addresses[2], core_packet->al_packet.addr_3.bytes, ETH_ALEN);
      memcpy(hdr->addresses[3], core_packet->al_packet.addr_4.bytes, ETH_ALEN);
      break;

   case _SWITCH_TYPE_802_11_ETH:  /**WIRELESS->ETHERNET*/

      /**
       * 802.11 devices pre-pend a torna_mac_hdr to the packet's data.
       * Ethernet devices expect a regular ethernet packet. We push
       * packet ETH_HLEN number of bytes and setup the ether header
       */

      if (core_packet->al_packet.dot1q_mode == AL_PACKET_DOT_1Q_MODE_TAG_ADD)
      {
         /* Adjust vlan info with ethernet header */
         skb_push(core_packet->skb, ETH_HLEN + AL_PACKET_VLAN_INFO_DATA_SIZE);
         ethhdr = (struct ethhdr *)core_packet->skb->data;
         data   = (unsigned char *)core_packet->skb->data + ETH_HLEN;

         memcpy(ethhdr->h_dest, core_packet->al_packet.addr_1.bytes, ETH_ALEN);
         memcpy(ethhdr->h_source, core_packet->al_packet.addr_2.bytes, ETH_ALEN);

         _add_vlan_info_to_802_3_packet(&(core_packet->al_packet), ethhdr, data);
      }
      else if (core_packet->al_packet.dot1q_mode == AL_PACKET_DOT_1Q_MODE_TAG_REMOVE)
      {
         packet_type = _remove_vlan_info_from_802_11_packet(core_packet);

         skb_push(core_packet->skb, ETH_HLEN);
         ethhdr = (struct ethhdr *)core_packet->skb->data;
         memcpy(ethhdr->h_dest, core_packet->al_packet.addr_1.bytes, ETH_ALEN);
         memcpy(ethhdr->h_source, core_packet->al_packet.addr_2.bytes, ETH_ALEN);
         ethhdr->h_proto = packet_type;
      }
      else
      {
         skb_push(core_packet->skb, ETH_HLEN);
         ethhdr = (struct ethhdr *)core_packet->skb->data;
         memcpy(ethhdr->h_dest, core_packet->al_packet.addr_1.bytes, ETH_ALEN);
         memcpy(ethhdr->h_source, core_packet->al_packet.addr_2.bytes, ETH_ALEN);
         ethhdr->h_proto = core_packet->al_packet.type;
      }

      core_packet->skb->mac_header = (u8 *)ethhdr;
      skb_set_mac_header(core_packet->skb, 0);
      skb_set_network_header(core_packet->skb, ETH_HLEN);
      skb_set_transport_header(core_packet->skb, ETH_HLEN + 20);
      break;

   case _SWITCH_TYPE_802_11_802_11:  /**WIRELESS->WIRELESS*/

      if (core_packet->al_packet.dot1q_mode == AL_PACKET_DOT_1Q_MODE_TAG_ADD)
      {
         /* Adjust vlan info with ethernet header */
         skb_push(core_packet->skb, ETH_HLEN + AL_PACKET_VLAN_INFO_DATA_SIZE);
         ethhdr = (struct ethhdr *)core_packet->skb->data;
         data   = (unsigned char *)core_packet->skb->data + ETH_HLEN;

         memcpy(ethhdr->h_dest, core_packet->al_packet.addr_1.bytes, ETH_ALEN);
         memcpy(ethhdr->h_source, core_packet->al_packet.addr_2.bytes, ETH_ALEN);

         _add_vlan_info_to_802_3_packet(&(core_packet->al_packet), ethhdr, data);
      }
      else if (core_packet->al_packet.dot1q_mode == AL_PACKET_DOT_1Q_MODE_TAG_REMOVE)
      {
         packet_type = _remove_vlan_info_from_802_11_packet(core_packet);

         skb_push(core_packet->skb, ETH_HLEN);
         ethhdr = (struct ethhdr *)core_packet->skb->data;
         memcpy(ethhdr->h_dest, core_packet->al_packet.addr_1.bytes, ETH_ALEN);
         memcpy(ethhdr->h_source, core_packet->al_packet.addr_2.bytes, ETH_ALEN);
         ethhdr->h_proto = packet_type;
      }
      else
      {
         skb_push(core_packet->skb, ETH_HLEN);
         ethhdr = (struct ethhdr *)core_packet->skb->data;
         memcpy(ethhdr->h_dest, core_packet->al_packet.addr_1.bytes, ETH_ALEN);
         memcpy(ethhdr->h_source, core_packet->al_packet.addr_2.bytes, ETH_ALEN);
         ethhdr->h_proto = core_packet->al_packet.type;
      }
      core_packet->skb->mac_header     = (u8 *)ethhdr;
      core_packet->skb->network_header = (char *)core_packet->skb->data + ETH_HLEN;
      skb_set_mac_header(core_packet->skb, 0);
      skb_set_network_header(core_packet->skb, ETH_HLEN);
      skb_set_transport_header(core_packet->skb, ETH_HLEN + 20);


      /**
       * 802.11 devices already have the torna_mac_hdr. We simply re-initialize
       * the header here.
       */
      /* Get the torna hdr*/
      hdr = net_if->dev_info->meshap_get_tx_meshap_mac_hdr_cb(core_packet->skb);
      hdr->frame_control = core_packet->al_packet.frame_control;
      hdr->qos_control      = core_packet->al_packet.qos_control;
      memcpy(hdr->addresses[0], core_packet->al_packet.addr_1.bytes, ETH_ALEN);
      memcpy(hdr->addresses[1], core_packet->al_packet.addr_2.bytes, ETH_ALEN);
      memcpy(hdr->addresses[2], core_packet->al_packet.addr_3.bytes, ETH_ALEN);
      memcpy(hdr->addresses[3], core_packet->al_packet.addr_4.bytes, ETH_ALEN);
      break;
   }
}

void al_packet_impl_setup_skb(meshap_core_net_if_t *outgoing_if, meshap_core_packet_t *core_packet)
{
   if (core_packet->al_packet.flags & AL_PACKET_FLAGS_USER_APP_PACKET)
   {
      _setup_skb_out_going(outgoing_if, core_packet);
   }
   else if (core_packet->incoming_if == NULL)
   {
      _setup_skb(outgoing_if, core_packet);
   }
   else
   {
      _resetup_skb(outgoing_if, core_packet);
   }
}


struct sk_buff *al_packet_impl_create_skb(meshap_core_net_if_t *incoming_if, meshap_core_packet_t *core_packet)
{
   struct sk_buff *skb;
   struct ethhdr *ethhdr;
   torna_mac_hdr_t *hdr;
   unsigned char *data;
   struct net_device *lo_dev;

   switch (incoming_if->dev_info->encapsulation)
   {
   case MESHAP_NET_DEV_ENCAPSULATION_ETHERNET:
   case MESHAP_NET_DEV_ENCAPSULATION_802_11:
      skb    = dev_alloc_skb(core_packet->al_packet.data_length + sizeof(struct ethhdr));
      ethhdr = (struct ethhdr *)skb_put(skb, sizeof(struct ethhdr));
      hdr    = NULL;
      skb_pull(skb, ETH_HLEN);
      break;

   default:
      return NULL;
   }

   memcpy(ethhdr->h_dest, broadcast_net_addr.bytes, ETH_ALEN);
   memcpy(ethhdr->h_source, incoming_if->al_net_if.config.hw_addr.bytes, ETH_ALEN);
   ethhdr->h_proto = __constant_htons(ETH_P_IP);

   lo_dev         = dev_get_by_name(&init_net, "lo");
   skb->protocol  = ethhdr->h_proto;
   skb->ip_summed = CHECKSUM_NONE;

   if (lo_dev != NULL)
   {
      skb->dev = lo_dev;
      dev_put(lo_dev);
   }
   else
   {
      skb->dev = incoming_if->dev;
   }

   skb->pkt_type   = PACKET_BROADCAST;
   skb->mac_header = (unsigned char *)ethhdr;

   data = skb_put(skb, core_packet->al_packet.data_length);
   memcpy(data, core_packet->al_packet.buffer + core_packet->al_packet.position, core_packet->al_packet.data_length);

   return skb;
}

void al_packet_impl_destroy(meshap_core_packet_t *core_packet)
{
   unsigned long flags;
   MESH_USAGE_PROFILING_INIT

   if (core_packet->skb != NULL)
   {
      dev_kfree_skb_any(core_packet->skb);
   }

    if (core_packet->type == MESHAP_CORE_PACKET_TYPE_HEAP)
    {
	    q_packet_stats.core_packet_type_alloc_heap--;
	    kfree(core_packet);
    }
    else
    {
	    if (core_packet->al_packet.pkt_status == AP_TRD_PKT)
	    {
		    MESH_USAGE_PROFILE_START(start)
			    al_spin_lock(ap_thread_core_pkt_splock, flags);
		    MESH_USAGE_PROFILE_END(profiling_info.core_pkt_splock_5,index,start,end)

			    if (_ap_thread_pool_pkt_head == NULL)
			    {
				    _ap_thread_pool_pkt_head = core_packet;
				    _ap_thread_pool_pkt_tail = core_packet;
			    }
			    else
			    {
				    _ap_thread_pool_pkt_tail->next_pool = core_packet;
				    _ap_thread_pool_pkt_tail            = core_packet;
			    }
		    q_packet_stats.ap_thread_core_pkt_pool_current_count++;
		    MESH_USAGE_PROFILE_START(start)
			    al_spin_unlock(ap_thread_core_pkt_splock, flags);
		    MESH_USAGE_PROFILE_END(profiling_info.core_pkt_spunlock_5,index,start,end)
	    }
	    else if (core_packet->al_packet.pkt_status == IMCP_TRD_PKT)
	    {
		    MESH_USAGE_PROFILE_START(start)
			    al_spin_lock(imcp_thread_core_pkt_splock, flags);
		    MESH_USAGE_PROFILE_END(profiling_info.core_pkt_splock_6,index,start,end)

			    if (_imcp_thread_pool_pkt_head == NULL)
			    {
				    _imcp_thread_pool_pkt_head = core_packet;
				    _imcp_thread_pool_pkt_tail = core_packet;
			    }
			    else
			    {
				    _imcp_thread_pool_pkt_tail->next_pool = core_packet;
				    _imcp_thread_pool_pkt_tail            = core_packet;
			    }
		    q_packet_stats.imcp_thread_core_pkt_pool_current_count++;
		    MESH_USAGE_PROFILE_START(start)
			    al_spin_unlock(imcp_thread_core_pkt_splock, flags);
		    MESH_USAGE_PROFILE_END(profiling_info.core_pkt_spunlock_6,index,start,end)
	    }
	    else
	    {
		    MESH_USAGE_PROFILE_START(start)
			    al_spin_lock(core_pkt_splock, flags);
		    MESH_USAGE_PROFILE_END(profiling_info.core_pkt_splock_4,index,start,end)
			    if (_pool_packet_head == NULL)
			    {
				    _pool_packet_head = core_packet;
				    _pool_packet_tail = core_packet;
			    }
			    else
			    {
				    _pool_packet_tail->next_pool = core_packet;
				    _pool_packet_tail            = core_packet;
			    }
		    ++_pool_current;
		    q_packet_stats.core_packet_pool_current_count++;
		    MESH_USAGE_PROFILE_START(start)
			    al_spin_unlock(core_pkt_splock, flags);
		    MESH_USAGE_PROFILE_END(profiling_info.core_pkt_spunlock_4,index,start,end)
	    }
	    core_packet->next_pool        = NULL;
	    core_packet->skb              = NULL;
	    core_packet->incoming_if      = NULL;
	    core_packet->packet_direction = MESHAP_CORE_PACKET_DIRECTION_NONE;
	    core_packet->al_packet.Pindx		   = 0;
	    core_packet->al_packet.Hindx		   = 0;
	    core_packet->al_packet.pkt_status	   = 0;
	    core_packet->al_packet.dot1p_entry.net_if = NULL;
	    core_packet->al_packet.dot1p_entry.packet = NULL;
	    if(core_packet->al_packet.dest_addr_sta_entry) {
		access_point_release_sta(AL_CONTEXT core_packet->al_packet.dest_addr_sta_entry);
		core_packet->al_packet.dest_addr_sta_entry    = NULL;
	    }
	    if(core_packet->al_packet.src_addr_sta_entry) {
		access_point_release_sta(AL_CONTEXT core_packet->al_packet.src_addr_sta_entry);
		core_packet->al_packet.src_addr_sta_entry    = NULL;
	    }
	    MESHAP_ATOMIC_SET(core_packet->ref_count, 0);
#if MULTIPLE_AP_THREAD
	    core_packet->al_packet.status		   = 0;
	    core_packet->al_packet.entry = NULL;
#endif
    }
}
