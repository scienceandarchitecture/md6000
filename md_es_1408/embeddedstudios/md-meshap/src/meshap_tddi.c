/********************************************************************************
* MeshDynamics
* --------------
* File     : meshap_tddi.c
* Comments : Torna MeshAP TDDI routines
* Created  : 5/25/2004
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* | 26  |8/22/2008 | Changes for SIP                                 |Abhijit |
* -----------------------------------------------------------------------------
* | 25  |5/21/2008 | Reboot locking bug fixed                        | Sriram |
* -----------------------------------------------------------------------------
* | 24  |01/07/2008| Mixed mode changes							  |Prachiti|
* -----------------------------------------------------------------------------
* | 23  |7/24/2007 | REBOOT logging changes                          | Sriram |
* -----------------------------------------------------------------------------
* | 22  |7/11/2007 | Added Full Disconnect command                   | Sriram |
* -----------------------------------------------------------------------------
* | 21  |7/9/2007  | Reset generator architecture changed            | Sriram |
* -----------------------------------------------------------------------------
* | 20  |7/9/2007  | Added Block CPU command                         | Sriram |
* -----------------------------------------------------------------------------
* | 19  |5/17/2007 | Set VLAN command added                          | Sriram |
* -----------------------------------------------------------------------------
* | 18  |2/6/2007  | FIPS related changes                            | Sriram |
* -----------------------------------------------------------------------------
* | 17  |12/14/2006| Reset generator changes                         | Sriram |
* -----------------------------------------------------------------------------
* | 16  |5/12/2006 | Added set_reboot_flag                           | Bindu  |
* -----------------------------------------------------------------------------
* | 15  |3/13/2006 | Added ACL request/response					  | Bindu  |
* -----------------------------------------------------------------------------
* | 14  |2/22/2006 | Added 802.11e Category request/response		  | Bindu  |
* -----------------------------------------------------------------------------
* | 13  |6/17/2005 | Added mesh/ap flags setting                     | Sriram |
* -----------------------------------------------------------------------------
* | 12  |6/16/2005 | Added random request/response                   | Sriram |
* -----------------------------------------------------------------------------
* | 11  |6/15/2005 | Added kick child request/response               | Sriram |
* -----------------------------------------------------------------------------
* | 10  |5/20/2005 | Added preferred parent request/response         | Sriram |
* -----------------------------------------------------------------------------
* |  9  |4/14/2005 | Added tests 13-1 and tests 14-1                 | Sriram |
* -----------------------------------------------------------------------------
* |  8  |4/11/2005 | Double declaration removed                      | Sriram |
* -----------------------------------------------------------------------------
* |  7  |4/6/2005  | Added test 12-1                                 | Sriram |
* -----------------------------------------------------------------------------
* |  6  |3/24/2005 | Added get_ds_net_if                             | Sriram |
* -----------------------------------------------------------------------------
* |  5  |12/26/2004| Reboot implemented                              | Sriram |
* -----------------------------------------------------------------------------
* |  4  |12/15/2004| Changes for get_ds_if TDDI call                 | Sriram |
* -----------------------------------------------------------------------------
* |  3  |12/9/2004 | Printed mesh_id                                 | Sriram |
* -----------------------------------------------------------------------------
* |  2  |7/23/2004 | Added test 11-1                                 | Sriram |
* -----------------------------------------------------------------------------
* |  1  |6/10/2004 | Added test 10-1                                 | Sriram |
* -----------------------------------------------------------------------------
* |  0  |5/25/2004 | Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

#include <generated/autoconf.h>
#include <linux/module.h>
#include <linux/init.h>
#include <linux/version.h>
#include <linux/fs.h>
#include <linux/netdevice.h>
#include <linux/skbuff.h>
#include <linux/wireless.h>
#include <asm/uaccess.h>
#include <linux/reboot.h>

#include "torna_ddi.h"
#include "torna_mac_hdr.h"
#include "meshap.h"
#include "meshap_logger.h"
#include "al.h"
#include "al_impl_context.h"
#include "al_802_11.h"
#include "meshap_core.h"
#include "meshap_tddi.h"
#include "al_conf.h"
#include "dot11e_conf.h"
#include "acl_conf.h"
#include "mobility_conf.h"
#include "v_if_info_conf.h"
#include "access_point.h"
#include "mesh_ext.h"
#include "access_point_ext.h"
#include "sip_conf.h"
#include "mesh_defs.h"
#include "mesh_table.h"
#include "mesh.h"
#include "mesh_globals.h"
#include "imcp_snip.h"
#include "torna_types.h"

#ifdef _MESHAP_AL_TEST_SUITE_
#include "meshap_al_test.h"
#endif
AL_DECLARE_GLOBAL_EXTERN(mesh_config_info_t *mesh_config);
struct _file_data
{
   unsigned char *packet_buffer;
   int           packet_length;
};


int al_on_phy_link_notify(al_net_if_t *al_net_if, int link_state);
int meshap_netdev_event(struct notifier_block *unused, unsigned long event, void *ptr);

typedef struct _file_data   _file_data_t;

static int _meshap_tddi_fops_open(struct inode *inode, struct file *file);
static int _meshap_tddi_fops_release(struct inode *inode, struct file *file);
static long _meshap_tddi_fops_ioctl(struct file *file, u_int cmd, u_long arg);
static ssize_t _meshap_tddi_read(struct file *file, char *buf, size_t count, loff_t *ppos);
static ssize_t _meshap_tddi_write(struct file *file, const char *buf, size_t count, loff_t *ppos);

static inline ssize_t _meshap_tddi_start_mesh_request(_file_data_t *fd, tddi_packet_t *packet, unsigned char *data, char *buf, size_t count, loff_t *ppos);
static inline ssize_t _meshap_tddi_stop_mesh_request(_file_data_t *fd, tddi_packet_t *packet, unsigned char *data, char *buf, size_t count, loff_t *ppos);
static inline ssize_t _meshap_tddi_halt_mesh_request(_file_data_t *fd, tddi_packet_t *packet, unsigned char *data, char *buf, size_t count, loff_t *ppos);
static inline ssize_t _meshap_tddi_configure_mesh_request(_file_data_t *fd, tddi_packet_t *packet, unsigned char *data, char *buf, size_t count, loff_t *ppos);
static inline ssize_t _meshap_tddi_get_ds_if(_file_data_t *fd, tddi_packet_t *packet, unsigned char *data, char *buf, size_t count, loff_t *ppos);
static inline ssize_t _meshap_tddi_reboot(_file_data_t *fd, tddi_packet_t *packet, unsigned char *data, char *buf, size_t count, loff_t *ppos);
static inline ssize_t _meshap_tddi_preferred_parent(_file_data_t *fd, tddi_packet_t *packet, unsigned char *data, char *buf, size_t count, loff_t *ppos);
static inline ssize_t _meshap_tddi_kick_child(_file_data_t *fd, tddi_packet_t *packet, unsigned char *data, char *buf, size_t count, loff_t *ppos);
static inline ssize_t _meshap_tddi_random(_file_data_t *fd, tddi_packet_t *packet, unsigned char *data, char *buf, size_t count, loff_t *ppos);
static inline ssize_t _meshap_tddi_mesh_flags(_file_data_t *fd, tddi_packet_t *packet, unsigned char *data, char *buf, size_t count, loff_t *ppos);
static inline ssize_t _meshap_tddi_ap_flags(_file_data_t *fd, tddi_packet_t *packet, unsigned char *data, char *buf, size_t count, loff_t *ppos);
static inline ssize_t _meshap_tddi_mesh_config_sqnr(_file_data_t *fd, tddi_packet_t *packet, unsigned char *data, char *buf, size_t count, loff_t *ppos);
static inline ssize_t _meshap_tddi_configure_dot11e_request(_file_data_t *fd, tddi_packet_t *packet, unsigned char *data, char *buf, size_t count, loff_t *ppos);
static inline ssize_t _meshap_tddi_configure_acl_request(_file_data_t *fd, tddi_packet_t *packet, unsigned char *data, char *buf, size_t count, loff_t *ppos);
static inline ssize_t _meshap_tddi_configure_mobility_request(_file_data_t *fd, tddi_packet_t *packet, unsigned char *data, char *buf, size_t count, loff_t *ppos);
static inline ssize_t _meshap_tddi_lock_reboot_request(_file_data_t *fd, tddi_packet_t *packet, unsigned char *data, char *buf, size_t count, loff_t *ppos);
static inline ssize_t _meshap_tddi_set_reboot_flag(_file_data_t *fd, tddi_packet_t *packet, unsigned char *data, char *buf, size_t count, loff_t *ppos);
static inline ssize_t _meshap_tddi_enable_reset_gen(_file_data_t *fd, tddi_packet_t *packet, unsigned char *data, char *buf, size_t count, loff_t *ppos);
static inline ssize_t _meshap_tddi_set_vlan(_file_data_t *fd, tddi_packet_t *packet, unsigned char *data, char *buf, size_t count, loff_t *ppos);
static inline ssize_t _meshap_tddi_block_cpu(_file_data_t *fd, tddi_packet_t *packet, unsigned char *data, char *buf, size_t count, loff_t *ppos);
static inline ssize_t _meshap_tddi_full_disconnect(_file_data_t *fd, tddi_packet_t *packet, unsigned char *data, char *buf, size_t count, loff_t *ppos);
static inline ssize_t _meshap_tddi_dhcp_info(_file_data_t *fd, tddi_packet_t *packet, unsigned char *data, char *buf, size_t count, loff_t *ppos);
static inline ssize_t _meshap_tddi_gpio(_file_data_t *fd, tddi_packet_t *packet, unsigned char *data, char *buf, size_t count, loff_t *ppos);
static inline ssize_t _meshap_tddi_gps_info(_file_data_t *fd, tddi_packet_t *packet, unsigned char *data, char *buf, size_t count, loff_t *ppos);
static inline ssize_t _meshap_tddi_virtual_configure_request(_file_data_t *fd, tddi_packet_t *packet, unsigned char *data, char *buf, size_t count, loff_t *ppos);
static inline ssize_t _meshap_tddi_configure_sip_request(_file_data_t *fd, tddi_packet_t *packet, unsigned char *data, char *buf, size_t count, loff_t *ppos);
static inline ssize_t _meshap_tddi_eth_link_status(_file_data_t *fd, tddi_packet_t *packet, unsigned char *data, char *buf, size_t count, loff_t *ppos);
static inline ssize_t _meshap_tddi_query_board_stats(_file_data_t *fd, tddi_packet_t *packet, unsigned char *data, char *buf, size_t count, loff_t *ppos); //SPAWAR

#ifdef _MESHAP_AL_TEST_SUITE_
static inline ssize_t _meshap_al_test_1_1_request(_file_data_t *fd, tddi_packet_t *packet, unsigned char *data, char *buf, size_t count, loff_t *ppos);
static inline ssize_t _meshap_al_test_1_2_request(_file_data_t *fd, tddi_packet_t *packet, unsigned char *data, char *buf, size_t count, loff_t *ppos);
static inline ssize_t _meshap_al_test_2_1_request(_file_data_t *fd, tddi_packet_t *packet, unsigned char *data, char *buf, size_t count, loff_t *ppos);
static inline ssize_t _meshap_al_test_2_2_request(_file_data_t *fd, tddi_packet_t *packet, unsigned char *data, char *buf, size_t count, loff_t *ppos);
static inline ssize_t _meshap_al_test_3_1_request(_file_data_t *fd, tddi_packet_t *packet, unsigned char *data, char *buf, size_t count, loff_t *ppos);
static inline ssize_t _meshap_al_test_3_2_request(_file_data_t *fd, tddi_packet_t *packet, unsigned char *data, char *buf, size_t count, loff_t *ppos);
static inline ssize_t _meshap_al_test_4_1_request(_file_data_t *fd, tddi_packet_t *packet, unsigned char *data, char *buf, size_t count, loff_t *ppos);
static inline ssize_t _meshap_al_test_5_1_request(_file_data_t *fd, tddi_packet_t *packet, unsigned char *data, char *buf, size_t count, loff_t *ppos);
static inline ssize_t _meshap_al_test_6_1_request(_file_data_t *fd, tddi_packet_t *packet, unsigned char *data, char *buf, size_t count, loff_t *ppos);
static inline ssize_t _meshap_al_test_7_1_request(_file_data_t *fd, tddi_packet_t *packet, unsigned char *data, char *buf, size_t count, loff_t *ppos);
static inline ssize_t _meshap_al_test_8_1_request(_file_data_t *fd, tddi_packet_t *packet, unsigned char *data, char *buf, size_t count, loff_t *ppos);
static inline ssize_t _meshap_al_test_9_1_request(_file_data_t *fd, tddi_packet_t *packet, unsigned char *data, char *buf, size_t count, loff_t *ppos);
static inline ssize_t _meshap_al_test_10_1_request(_file_data_t *fd, tddi_packet_t *packet, unsigned char *data, char *buf, size_t count, loff_t *ppos);
static inline ssize_t _meshap_al_test_11_1_request(_file_data_t *fd, tddi_packet_t *packet, unsigned char *data, char *buf, size_t count, loff_t *ppos);
static inline ssize_t _meshap_al_test_12_1_request(_file_data_t *fd, tddi_packet_t *packet, unsigned char *data, char *buf, size_t count, loff_t *ppos);
static inline ssize_t _meshap_al_test_13_1_request(_file_data_t *fd, tddi_packet_t *packet, unsigned char *data, char *buf, size_t count, loff_t *ppos);
static inline ssize_t _meshap_al_test_14_1_request(_file_data_t *fd, tddi_packet_t *packet, unsigned char *data, char *buf, size_t count, loff_t *ppos);
#endif
//SOWNDARYAHT_VHT
static inline ssize_t _meshap_tddi_get_hw_config(_file_data_t *fd, tddi_packet_t *packet, unsigned char *data, char *buf, size_t count, loff_t *ppos);
int meshap_get_hw_support_ht_capab(meshap_core_net_if_t *core_net_if,tddi_ht_cap_t *ht_cap,tddi_vht_cap_t *vht_cap, unsigned char sub_type);

static inline ssize_t _meshap_tddi_get_wm_channel_flag(_file_data_t *fd, tddi_packet_t *packet, unsigned char *data, char *buf, size_t count, loff_t *ppos);
int meshap_get_wm_duty_cycle_flag(meshap_core_net_if_t *core_net_if, int *duty_cycle_flag, int *operating_channel);

static inline ssize_t _meshap_tddi_get_node_type(_file_data_t*   fd,tddi_packet_t* packet,unsigned char* data,char* buf,size_t count,loff_t* ppos);
static inline ssize_t _meshap_tddi_send_hwinfo_type(_file_data_t*   fd,tddi_packet_t* packet,unsigned char* data,char* buf,size_t count,loff_t* ppos);
extern int mesh_imcp_send_packet_hardware_info(AL_CONTEXT_PARAM_DECL_SINGLE);
static inline ssize_t _meshap_tddi_send_stationinfo_type(_file_data_t*   fd,tddi_packet_t* packet,unsigned char* data,char* buf,size_t count,loff_t* ppos);
extern int mesh_imcp_process_packet_sta_info_request(AL_CONTEXT_PARAM_DECL imcp_packet_info_t *pi);

int set_ap_values(int al_conf_token);

extern int eth1_ping_status;
extern int func_mode;
extern meshap_core_net_if_t *_ifs_list;
extern  int read_mesh_configuration(AL_CONTEXT_PARAM_DECL_SINGLE);


meshap_tddi_globals_t meshap_tddi_globals =
{
   major:                  -EBUSY,
   locked:                 0,
#ifdef _MESHAP_AL_TEST_SUITE_
   test_1_token:   0,
   test_2_token:   0,
   test_3_token:   0,
   test_2_count:   0
#endif
};

static struct file_operations _meshap_tddi_fops =
{
   owner:          THIS_MODULE,
   open:           _meshap_tddi_fops_open,
   release:        _meshap_tddi_fops_release,
   unlocked_ioctl:         _meshap_tddi_fops_ioctl,
   read:           _meshap_tddi_read,
   write:          _meshap_tddi_write,
};

static al_net_if_t    *_ds_net_if = NULL;
static unsigned short _regdomain  = 0;

int meshap_tddi_initialize()
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s: %d\n",__func__,__LINE__);
   meshap_tddi_globals.major = register_chrdev(0, "meshap_tddi", &_meshap_tddi_fops);

   if (meshap_tddi_globals.major == -EBUSY)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"MESH_AP : INFO : meshap_tddi: unable to find a free device # for meshap_tddi %s : %d\n", __func__,__LINE__);        
   }
   else
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"MESH_AP : INFO : meshap_tddi: Initialized Driver-Daemon Interface (%d.%d.%d) at Char Major %d : %s : %d\n",
	     TDDI_VERSION_MAJOR(TDDI_CURRENT_VERSION),
             TDDI_VERSION_MINOR(TDDI_CURRENT_VERSION),
             TDDI_VERSION_REVISION(TDDI_CURRENT_VERSION),
             meshap_tddi_globals.major,__func__,__LINE__);   
   }
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s: %d\n",__func__,__LINE__);

   return 0;
}


int meshap_tddi_uninitialize()
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s: %d\n",__func__,__LINE__);
   if (meshap_tddi_globals.major != -EBUSY)
   {
      unregister_chrdev(meshap_tddi_globals.major, "meshap_tddi");
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"MESH_AP : INFO : meshap_tddi: Uninitialized Driver-Daemon Interface at Char Major %d :%s : %d\n",meshap_tddi_globals.major,__func__,__LINE__);        
   }
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s: %d\n",__func__,__LINE__);

   return 0;
}


void meshap_tddi_set_ds_net_if(al_net_if_t *net_if)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s: %d\n",__func__,__LINE__);
   _ds_net_if = net_if;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s: %d\n",__func__,__LINE__);
}


al_net_if_t *meshap_tddi_get_ds_net_if()
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s: %d\n",__func__,__LINE__);
   return _ds_net_if;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s: %d\n",__func__,__LINE__);
}


unsigned short meshap_tddi_get_regdomain()
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s: %d\n",__func__,__LINE__);
   return _regdomain;
}


static int _meshap_tddi_fops_open(struct inode *inode, struct file *file)
{
//   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s: %d\n",__func__,__LINE__);
   _file_data_t *fd;

   if (((file->f_flags & O_ACCMODE) == O_WRONLY) ||
       ((file->f_flags & O_ACCMODE) == O_RDWR))
   {
      if (meshap_tddi_globals.locked)
      {
         TORNA_LOG(MESHAP_LOGGER_TYPE_FLOW, "meshap_tddi: _meshap_tddi_fops_open meshap_tddi_globals.locked\n");
         al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : _meshap_tddi_fops_open meshap_tddi_globals.locked %s : %d\n", __func__,__LINE__);        
		 return -EBUSY;
      }
      else
      {
         meshap_tddi_globals.locked = 1;
      }
   }

   fd = (_file_data_t *)kmalloc(sizeof(_file_data_t), GFP_KERNEL);

   memset(fd, 0, sizeof(_file_data_t));

   file->private_data = fd;

//   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s: %d\n",__func__,__LINE__);

   return 0;
}


static int _meshap_tddi_fops_release(struct inode *inode, struct file *file)
{
   //al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s: %d\n",__func__,__LINE__);
   _file_data_t *fd;

   fd = file->private_data;

   if (fd->packet_buffer != NULL)
   {
      kfree(fd->packet_buffer);
   }

   kfree(fd);

   if (((file->f_flags & O_ACCMODE) == O_WRONLY) ||
       ((file->f_flags & O_ACCMODE) == O_RDWR))
   {
      meshap_tddi_globals.locked = 0;
   }

   //al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s: %d\n",__func__,__LINE__);

   return 0;
}


static long _meshap_tddi_fops_ioctl(struct file *file, u_int cmd, u_long arg)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s: %d\n",__func__,__LINE__);
   return -EINVAL;
}


static ssize_t _meshap_tddi_read(struct file *file, char *buf, size_t count, loff_t *ppos)
{
//   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s: %d\n",__func__,__LINE__);
   _file_data_t  *fd;
   tddi_packet_t *packet;
   unsigned char *data;

   fd = file->private_data;

   if ((fd->packet_buffer == NULL) ||
       (fd->packet_length <= 0))
   {
      TORNA_LOG(MESHAP_LOGGER_TYPE_FLOW, "meshap_tddi: _meshap_tddi_read fd->packet_buffer == NULL||fd->packet_length <=0\n");
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : Invalid argument : _meshap_tddi_read fd-packet_buffer == NULL||fd->packet_length <=0> %s : %d\n", __func__,__LINE__);
      return -EINVAL;
   }

   packet = (tddi_packet_t *)fd->packet_buffer;
   data   = fd->packet_buffer + sizeof(tddi_packet_t);

   if ((packet->signature != TDDI_PACKET_SIGNATURE) ||
       (packet->type != TDDI_PACKET_TYPE_REQUEST))
   {
      TORNA_LOG(MESHAP_LOGGER_TYPE_FLOW, "meshap_tddi: _meshap_tddi_read packet-signature != TDDI_PACKET_SIGNATURE||packet->type != TDDI_PACKET_TYPE_REQUEST\n");
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : Invalid argument : _meshap_tddi_read packet-signature != TDDI_PACKET_SIGNATURE||packet->type != TDDI_PACKET_TYPE_REQUEST> %s : %d\n", __func__,__LINE__);
      return -EINVAL;
   }

   switch (packet->sub_type)
   {
   case TDDI_REQUEST_TYPE_START_MESH:
      return _meshap_tddi_start_mesh_request(fd, packet, data, buf, count, ppos);

   case TDDI_REQUEST_TYPE_STOP_MESH:
      return _meshap_tddi_stop_mesh_request(fd, packet, data, buf, count, ppos);

	case TDDI_REQUEST_TYPE_HALT_MESH:
		return _meshap_tddi_halt_mesh_request(fd, packet, data, buf, count, ppos);

   case TDDI_REQUEST_TYPE_CONFIGURE_MESH:
      return _meshap_tddi_configure_mesh_request(fd, packet, data, buf, count, ppos);
//SOWNDARYA HT_VHT INFO
   case TDDI_REQUEST_TYPE_GET_HW_CONFIG:
         return _meshap_tddi_get_hw_config(fd, packet, data, buf, count, ppos);
//HT_VHT INFO END

   case TDDI_REQUEST_TYPE_GET_WM_CHANNEL_FLAG:
         return _meshap_tddi_get_wm_channel_flag(fd, packet, data, buf, count, ppos);

#ifdef _MESHAP_AL_TEST_SUITE_
   case TDDI_REQUEST_TYPE_TEST_1_1:
      return _meshap_al_test_1_1_request(fd, packet, data, buf, count, ppos);

   case TDDI_REQUEST_TYPE_TEST_1_2:
      return _meshap_al_test_1_2_request(fd, packet, data, buf, count, ppos);

   case TDDI_REQUEST_TYPE_TEST_2_1:
      return _meshap_al_test_2_1_request(fd, packet, data, buf, count, ppos);

   case TDDI_REQUEST_TYPE_TEST_2_2:
      return _meshap_al_test_2_2_request(fd, packet, data, buf, count, ppos);

   case TDDI_REQUEST_TYPE_TEST_3_1:
      return _meshap_al_test_3_1_request(fd, packet, data, buf, count, ppos);

   case TDDI_REQUEST_TYPE_TEST_3_2:
      return _meshap_al_test_3_2_request(fd, packet, data, buf, count, ppos);

   case TDDI_REQUEST_TYPE_TEST_4_1:
      return _meshap_al_test_4_1_request(fd, packet, data, buf, count, ppos);

   case TDDI_REQUEST_TYPE_TEST_5_1:
      return _meshap_al_test_5_1_request(fd, packet, data, buf, count, ppos);

   case TDDI_REQUEST_TYPE_TEST_6_1:
      return _meshap_al_test_6_1_request(fd, packet, data, buf, count, ppos);

   case TDDI_REQUEST_TYPE_TEST_7_1:
      return _meshap_al_test_7_1_request(fd, packet, data, buf, count, ppos);

   case TDDI_REQUEST_TYPE_TEST_8_1:
      return _meshap_al_test_8_1_request(fd, packet, data, buf, count, ppos);

   case TDDI_REQUEST_TYPE_TEST_9_1:
      return _meshap_al_test_9_1_request(fd, packet, data, buf, count, ppos);

   case TDDI_REQUEST_TYPE_TEST_10_1:
      return _meshap_al_test_10_1_request(fd, packet, data, buf, count, ppos);

   case TDDI_REQUEST_TYPE_TEST_11_1:
      return _meshap_al_test_11_1_request(fd, packet, data, buf, count, ppos);

   case TDDI_REQUEST_TYPE_TEST_12_1:
      return _meshap_al_test_12_1_request(fd, packet, data, buf, count, ppos);

   case TDDI_REQUEST_TYPE_TEST_13_1:
      return _meshap_al_test_13_1_request(fd, packet, data, buf, count, ppos);

   case TDDI_REQUEST_TYPE_TEST_14_1:
      return _meshap_al_test_14_1_request(fd, packet, data, buf, count, ppos);
#endif
   case TDDI_REQUEST_TYPE_GET_DS_IF:
      return _meshap_tddi_get_ds_if(fd, packet, data, buf, count, ppos);

   case TDDI_REQUEST_TYPE_REBOOT:
      return _meshap_tddi_reboot(fd, packet, data, buf, count, ppos);

   case TDDI_REQUEST_TYPE_PREFERRED_PARENT:
      return _meshap_tddi_preferred_parent(fd, packet, data, buf, count, ppos);

   case TDDI_REQUEST_TYPE_KICK_CHILD:
      return _meshap_tddi_kick_child(fd, packet, data, buf, count, ppos);

   case TDDI_REQUEST_TYPE_RANDOM:
      return _meshap_tddi_random(fd, packet, data, buf, count, ppos);

   case TDDI_REQUEST_TYPE_MESH_FLAGS:
      return _meshap_tddi_mesh_flags(fd, packet, data, buf, count, ppos);

   case TDDI_REQUEST_TYPE_AP_FLAGS:
      return _meshap_tddi_ap_flags(fd, packet, data, buf, count, ppos);

   case TDDI_REQUEST_TYPE_MESH_CONFIG_SQNR:
      return _meshap_tddi_mesh_config_sqnr(fd, packet, data, buf, count, ppos);

   case TDDI_REQUEST_TYPE_CONFIGURE_DOT11E:
      return _meshap_tddi_configure_dot11e_request(fd, packet, data, buf, count, ppos);

   case TDDI_REQUEST_TYPE_CONFIGURE_ACL:
      return _meshap_tddi_configure_acl_request(fd, packet, data, buf, count, ppos);

   case TDDI_REQUEST_TYPE_CONFIGURE_MOBILITY:
      return _meshap_tddi_configure_mobility_request(fd, packet, data, buf, count, ppos);

   case TDDI_REQUEST_TYPE_LOCK_REBOOT:
      return _meshap_tddi_lock_reboot_request(fd, packet, data, buf, count, ppos);

   case TDDI_REQUEST_TYPE_SET_REBOOT_FLAG:
      return _meshap_tddi_set_reboot_flag(fd, packet, data, buf, count, ppos);

   case TDDI_REQUEST_TYPE_ENABLE_RESET_GEN:
      return _meshap_tddi_enable_reset_gen(fd, packet, data, buf, count, ppos);

   case TDDI_REQUEST_TYPE_SET_VLAN:
      return _meshap_tddi_set_vlan(fd, packet, data, buf, count, ppos);

   case TDDI_REQUEST_TYPE_BLOCK_CPU:
      return _meshap_tddi_block_cpu(fd, packet, data, buf, count, ppos);

   case TDDI_REQUEST_TYPE_FULL_DISCONNECT:
      return _meshap_tddi_full_disconnect(fd, packet, data, buf, count, ppos);

   case TDDI_REQUEST_TYPE_GET_DHCP_INFO:
      return _meshap_tddi_dhcp_info(fd, packet, data, buf, count, ppos);

   case TDDI_REQUEST_TYPE_GPIO_READ_WRITE:
      return _meshap_tddi_gpio(fd, packet, data, buf, count, ppos);

   case TDDI_REQUEST_TYPE_SET_GPS_INFO:
      return _meshap_tddi_gps_info(fd, packet, data, buf, count, ppos);

   case TDDI_REQUEST_TYPE_VIRTUAL_CONFIGURE:
      return _meshap_tddi_virtual_configure_request(fd, packet, data, buf, count, ppos);

   case TDDI_REQUEST_TYPE_CONFIGURE_SIP:
      return _meshap_tddi_configure_sip_request(fd, packet, data, buf, count, ppos);

   case TDDI_REQUEST_TYPE_ETH_LINK_FLAG:
      return _meshap_tddi_eth_link_status(fd, packet, data, buf, count, ppos);
//SPAWAR
   case TDDI_REQUEST_TYPE_QUERY_BOARD_STATS:
      return _meshap_tddi_query_board_stats(fd, packet, data, buf, count, ppos);
//SPAWAR_END

	case TDDI_REQUEST_TYPE_GET_NODE_TYPE:
	return _meshap_tddi_get_node_type(fd,packet,data,buf,count,ppos);

	case TDDI_REQUEST_TYPE_SEND_HW_INFO:
		return _meshap_tddi_send_hwinfo_type(fd,packet,data,buf,count,ppos);

	case TDDI_REQUEST_TYPE_SEND_STATION_INFO:
		return _meshap_tddi_send_stationinfo_type(fd,packet,data,buf,count,ppos);
   }

   TORNA_LOG(MESHAP_LOGGER_TYPE_FLOW, "meshap_tddi: _meshap_tddi_read Unknown request type %d\n", packet->sub_type);
 //  al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s: %d\n",__func__,__LINE__);

   return -EINVAL;
}


static ssize_t _meshap_tddi_write(struct file *file, const char *buf, size_t count, loff_t *ppos)
{
//   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s: %d\n",__func__,__LINE__);
   _file_data_t *fd;

   fd = file->private_data;

   if (fd->packet_buffer != NULL)
   {
      kfree(fd->packet_buffer);
   }

   fd->packet_length = 0;
   fd->packet_buffer = (unsigned char *)kmalloc(count, GFP_KERNEL);

   if (copy_from_user(fd->packet_buffer, buf, count))
   {
      kfree(fd->packet_buffer);
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : _meshap_tddi_write copy_from_user(fd-packet_buffer,buf,count) failed> %s : %d\n", __func__,__LINE__);
      return -EFAULT;
   }

   fd->packet_length = count;

   *ppos += count;

//   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s: %d\n",__func__,__LINE__);
   return count;
}


static inline ssize_t _meshap_tddi_start_mesh_request(_file_data_t *fd, tddi_packet_t *packet, unsigned char *data, char *buf, size_t count, loff_t *ppos)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s: %d\n",__func__,__LINE__);
   tddi_packet_t              response_packet;
   tddi_start_mesh_response_t response_data;

   if (count < sizeof(tddi_packet_t) + sizeof(tddi_start_mesh_response_t))
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : out of memory returning ENOMEM errorcode %s : %d\n", __func__,__LINE__);
      return -ENOMEM;
   }

   response_packet.signature = TDDI_PACKET_SIGNATURE;
   response_packet.version   = TDDI_CURRENT_VERSION;
   response_packet.type      = TDDI_PACKET_TYPE_RESPONSE;
   response_packet.sub_type  = TDDI_RESPONSE_TYPE_START_MESH;
   response_packet.data_size = sizeof(tddi_start_mesh_response_t);

   if (meshap_core_globals.state == MESHAP_CORE_GLOBALS_STATE_STOPPED)
   {
      if (meshap_core_globals.config_string != NULL)
      {
         response_data.response = TDDI_START_MESH_RESPONSE_SUCCESS;
      }
      else
      {
         response_data.response = TDDI_START_MESH_RESPONSE_NO_CONFIG;
      }
   }
   else
   {
      response_data.response = TDDI_START_MESH_RESPONSE_ALREADY_STARTED;
   }

   if (copy_to_user(buf, &response_packet, sizeof(tddi_packet_t)))
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : Bad address %s : %d\n",__func__,__LINE__);
      return -EFAULT;
   }

   if (copy_to_user(buf + sizeof(tddi_packet_t), &response_data, sizeof(tddi_start_mesh_response_t)))
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : Bad address%s : %d\n",__func__,__LINE__);
      return -EFAULT;
   }

   if (response_data.response == TDDI_START_MESH_RESPONSE_SUCCESS)
   {
      /**
       * Now we start the access point and mesh
       */
      meshap_core_start_mesh();
   }

   *ppos += sizeof(tddi_packet_t) + sizeof(tddi_start_mesh_response_t);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s: %d\n",__func__,__LINE__);
   return sizeof(tddi_packet_t) + sizeof(tddi_start_mesh_response_t);
}

static inline ssize_t _meshap_tddi_halt_mesh_request(_file_data_t *fd, tddi_packet_t *packet, unsigned char *data, char *buf, size_t count, loff_t *ppos)
{
        al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s: %d\n",__func__,__LINE__);
	tddi_packet_t             response_packet;
	tddi_stop_mesh_response_t response_data;

	if (count < sizeof(tddi_packet_t) + sizeof(tddi_stop_mesh_response_t))
	{
        	al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : Out of memory: %d\n",__LINE__);
		return -ENOMEM;
	}

	response_packet.signature = TDDI_PACKET_SIGNATURE;
	response_packet.version   = TDDI_CURRENT_VERSION;
	response_packet.type      = TDDI_PACKET_TYPE_RESPONSE;
	response_packet.sub_type  = TDDI_RESPONSE_TYPE_HALT_MESH;
	response_packet.data_size = sizeof(tddi_halt_mesh_response_t);

	access_point_enable_disable_processing(AL_CONTEXT 0);
	response_data.response = TDDI_HALT_MESH_RESPONSE_SUCCESS;

	if (copy_to_user(buf, &response_packet, sizeof(tddi_packet_t)))
	{
      		al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : Bad address,copy_to_user failed : %s : %d\n",__func__,__LINE__);
		return -EFAULT;
	}

	if (copy_to_user(buf + sizeof(tddi_packet_t), &response_data, sizeof(tddi_halt_mesh_response_t)))
	{
      		al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : Bad address,copy_to_user failed : %s : %d\n",__func__,__LINE__);
		return -EFAULT;
	}

	*ppos += sizeof(tddi_packet_t) + sizeof(tddi_halt_mesh_response_t);
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s: %d\n",__func__,__LINE__);
	return sizeof(tddi_packet_t) + sizeof(tddi_halt_mesh_response_t);
}

static inline ssize_t _meshap_tddi_stop_mesh_request(_file_data_t *fd, tddi_packet_t *packet, unsigned char *data, char *buf, size_t count, loff_t *ppos)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s: %d\n",__func__,__LINE__);
   tddi_packet_t             response_packet;
   tddi_stop_mesh_response_t response_data;

   if (count < sizeof(tddi_packet_t) + sizeof(tddi_stop_mesh_response_t))
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : Out of memory%s : %d\n",__func__,__LINE__);
      return -ENOMEM;
   }

   response_packet.signature = TDDI_PACKET_SIGNATURE;
   response_packet.version   = TDDI_CURRENT_VERSION;
   response_packet.type      = TDDI_PACKET_TYPE_RESPONSE;
   response_packet.sub_type  = TDDI_RESPONSE_TYPE_STOP_MESH;
   response_packet.data_size = sizeof(tddi_stop_mesh_response_t);

   if (meshap_core_globals.state == MESHAP_CORE_GLOBALS_STATE_STARTED)
   {
      response_data.response = TDDI_STOP_MESH_RESPONSE_SUCCESS;
   }
   else
   {
      response_data.response = TDDI_STOP_MESH_RESPONSE_NOT_STARTED;
   }

   if (copy_to_user(buf, &response_packet, sizeof(tddi_packet_t)))
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : Bad address,copy_to_user failed: %s : %d\n",__func__,__LINE__);
      return -EFAULT;
   }

   if (copy_to_user(buf + sizeof(tddi_packet_t), &response_data, sizeof(tddi_stop_mesh_response_t)))
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : Bad address ,copy_to_user failed : %s : %d\n",__func__,__LINE__);
      return -EFAULT;
   }

   if (response_data.response == TDDI_STOP_MESH_RESPONSE_SUCCESS)
   {
      /**
       * Now we stop the access point and mesh
       */
      meshap_core_stop_mesh();
   }

   *ppos += sizeof(tddi_packet_t) + sizeof(tddi_stop_mesh_response_t);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s: %d\n",__func__,__LINE__);
   return sizeof(tddi_packet_t) + sizeof(tddi_stop_mesh_response_t);
}


static inline ssize_t _meshap_tddi_configure_mesh_request(_file_data_t *fd, tddi_packet_t *packet, unsigned char *data, char *buf, size_t count, loff_t *ppos)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s: %d\n",__func__,__LINE__);
   tddi_packet_t response_packet;
   tddi_configure_mesh_response_t response_data;
   tddi_configure_mesh_request_t  *request_data;
   char                  *config_string, *tmp_conf_str = NULL;
   char                  mesh_id[129];
   int                   i;
   int                   if_count;
   al_conf_if_info_t     if_info;
   meshap_core_net_if_t  *core_net_if;
   int                   al_conf_token, vlan_count;	//VLAN
   al_conf_vlan_info_t             vlan_info; //VLAN
#if defined (IMX_ONLY) || defined (x86_ONLY)
   struct netdev_notifier_info info;
#endif
   meshap_core_net_if_t *node, *tmp_node;
   
   al_conf_token = 0;

   if (count < sizeof(tddi_packet_t) + sizeof(response_data))
   {
      TORNA_LOG(MESHAP_LOGGER_TYPE_FLOW, "meshap_tddi: _meshap_tddi_configure_mesh_request count < sizeof(tddi_packet_t) + sizeof(response_data)\n");
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : out of memory : %s : %d\n",__func__,__LINE__);
      return -ENOMEM;
   }

   response_packet.signature = TDDI_PACKET_SIGNATURE;
   response_packet.version   = TDDI_CURRENT_VERSION;
   response_packet.type      = TDDI_PACKET_TYPE_RESPONSE;
   response_packet.sub_type  = TDDI_RESPONSE_TYPE_CONFIGURE_MESH;
   response_packet.data_size = sizeof(tddi_configure_mesh_response_t);

   if (meshap_core_globals.state == MESHAP_CORE_GLOBALS_STATE_STARTED)
   {
      response_data.response = TDDI_CONFIGURE_MESH_RESPONSE_BUSY;
   }
   else
   {
      response_data.response = TDDI_CONFIGURE_MESH_RESPONSE_SUCCESS;
   }

   if (response_data.response == TDDI_CONFIGURE_MESH_RESPONSE_SUCCESS)
   {
      if (meshap_core_globals.config_string != NULL)
      {
         kfree(meshap_core_globals.config_string);
      }

      request_data = (tddi_configure_mesh_request_t *)data;

      meshap_core_globals.config_string = (char *)kmalloc(request_data->config_string_length + 1, GFP_KERNEL);
      memset(meshap_core_globals.config_string, 0, request_data->config_string_length + 1);

      config_string = data + sizeof(tddi_configure_mesh_request_t);

      memcpy(meshap_core_globals.config_string, config_string, request_data->config_string_length);

      al_conf_token = al_conf_parse(meshap_core_globals.config_string);

      if (al_conf_token == 0)
      {
         kfree(meshap_core_globals.config_string);
         meshap_core_globals.config_string = NULL;
         TORNA_LOG(MESHAP_LOGGER_TYPE_FLOW, "meshap_tddi: _meshap_tddi_configure_mesh_request meshap_tddi_globals.al_conf_token == 0\n");
       	 al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : meshap_tddi: al_conf_parse returned 0,_meshap_tddi_configure_mesh_request meshap_tddi_globals.al_conf_token == 0 : %s : %d\n",__func__,__LINE__);
         return -EINVAL;
      }
   }

   if ((response_data.response == TDDI_CONFIGURE_MESH_RESPONSE_BUSY))
   {
      request_data  = (tddi_configure_mesh_request_t *)data;
      config_string = data + sizeof(tddi_configure_mesh_request_t);

      tmp_conf_str = (char *)kmalloc(request_data->config_string_length + 1, GFP_KERNEL);

	  if (tmp_conf_str == NULL) {
		  al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR :  Memory Not allocated: %s : %d\n",__func__,__LINE__);
		  return -EINVAL;
	  }

      memset(tmp_conf_str, 0, request_data->config_string_length + 1);

      memcpy(tmp_conf_str, config_string, request_data->config_string_length);

      al_conf_token = al_conf_parse(tmp_conf_str);

      if (al_conf_token == 0)
      {
         kfree(tmp_conf_str);
         tmp_conf_str = NULL;
         TORNA_LOG(MESHAP_LOGGER_TYPE_FLOW, "meshap_tddi: _meshap_tddi_configure_mesh_request meshap_tddi_globals.al_conf_token == 0\n");
       	 al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR :  al_conf_parse returned 0,_meshap_tddi_configure_mesh_request meshap_tddi_globals.al_conf_token == 0: %s : %d\n",__func__,__LINE__);
         return -EINVAL;
      }
   }

   if (copy_to_user(buf, &response_packet, sizeof(tddi_packet_t)))
   {
      TORNA_LOG(MESHAP_LOGGER_TYPE_FLOW, "meshap_tddi: _meshap_tddi_configure_mesh_request copy_to_user(buf,&response_packet,sizeof(tddi_packet_t)) failed\n");
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR :  copy to user failed %s : %d\n",__func__,__LINE__);
      return -EFAULT;
   }

   if (copy_to_user(buf + sizeof(tddi_packet_t), &response_data, sizeof(tddi_configure_mesh_response_t)))
   {
      TORNA_LOG(MESHAP_LOGGER_TYPE_FLOW, "meshap_tddi: _meshap_tddi_configure_mesh_request copy_to_user(buf + sizeof(tddi_packet_t),&response_data,sizeof(tddi_configure_mesh_response_t)) failed\n");
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR :  copy to user failed %s : %d\n",__func__,__LINE__);
      return -EFAULT;
   }

   al_conf_get_mesh_id(al_conf_token, mesh_id, 128);

   if_count = al_conf_get_if_count(AL_CONTEXT al_conf_token);

   _regdomain = al_conf_get_regulatory_domain(AL_CONTEXT al_conf_token);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"MESH_AP : INFO : Accepted configuration with following settings: \tif_count=%d,hbi=%d,mesh_id=\%s\ Regdomain=%d \n : %s : %d\n",if_count,
               al_conf_get_heartbeat_interval(al_conf_token),
               mesh_id,
               _regdomain, __func__,__LINE__);        
   node = _ifs_list;
   while(node) {
	   for (i = 0; i < if_count; i++)
	   {
		   al_conf_get_if(al_conf_token, i, &if_info);
		   if(!strcmp(node->name, if_info.name)) {
			   break;
		   }
	   }
	   if(i >= if_count) {
           tmp_node = node->next;
#ifdef CNS_ONLY
		   meshap_netdev_event(NULL, NETDEV_UNREGISTER, node->dev);
#elif defined (IMX_ONLY) || defined (x86_ONLY)
		   info.dev = node->dev;
		   meshap_netdev_event(NULL, NETDEV_UNREGISTER, &info);
#endif
           node = tmp_node;
	   } else {
	       node = node->next;
       }
   }

   for (i = 0; i < if_count; i++)
   {
      al_conf_get_if(al_conf_token, i, &if_info);
      
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"MESH_AP : INFO :\t\t%d %s PHY=%d USE=%d CH=%d\n :%s : %d\n",i + 1,
             if_info.name,
             if_info.phy_type,
             if_info.use_type,
             if_info.wm_channel,__func__,__LINE__);        


      core_net_if = meshap_core_lookup_net_if(if_info.name);

      if (core_net_if == NULL)
      {
         meshap_core_create_used_net_if(if_info.name, if_info.phy_type, if_info.use_type);
      }
      else
      {
         if (!core_net_if->used)
         {
            meshap_core_use_net_if(core_net_if, if_info.phy_type, if_info.use_type);
         }
      }
   }

//VLAN
   vlan_count = al_conf_get_vlan_count(AL_CONTEXT al_conf_token);
   for(i=1; i < vlan_count; i++) {
	   al_conf_get_vlan(AL_CONTEXT al_conf_token,i,&vlan_info);
	   core_net_if     = meshap_core_lookup_net_if(vlan_info.name);
	   if(core_net_if && !core_net_if->used) {
		   core_net_if->used =1;
		   core_net_if->vlan_if = 1;
	   }
   }
//VLAN_END

   if ((response_data.response == TDDI_CONFIGURE_MESH_RESPONSE_BUSY))
   {
       if(mesh_config == NULL || mesh_config->if_info == NULL ){
           int ret1 ;
           ret1 =  read_mesh_configuration (AL_CONTEXT_SINGLE);
           if (ret1)
           {
     	       al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"MESH_AP : ERROR :  ERROR while reading configuration : %s : %d\n", __func__,__LINE__);        
               return ret1;
           }

       }
      set_ap_values(al_conf_token);
      if (tmp_conf_str != NULL)
      {
          kfree(tmp_conf_str);
          tmp_conf_str = NULL;
      }
   }



   al_conf_close(al_conf_token);

   *ppos += sizeof(tddi_packet_t) + sizeof(tddi_configure_mesh_response_t);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s: %d\n",__func__,__LINE__);
   return sizeof(tddi_packet_t) + sizeof(tddi_configure_mesh_response_t);
}

static inline ssize_t _meshap_tddi_get_hw_config(_file_data_t *fd, tddi_packet_t *packet, unsigned char *data, char *buf, size_t count, loff_t *ppos)
{
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s: %d\n",__func__,__LINE__);
	tddi_packet_t                   response_packet;
	tddi_get_hw_config_t            response_data;
	tddi_get_hw_config_request_t    *request_data;
	meshap_core_net_if_t            *core_net_if;


	if (count < sizeof(tddi_packet_t) + sizeof(response_data))
	{
		TORNA_LOG(MESHAP_LOGGER_TYPE_FLOW, "meshap_tddi: _meshap_tddi_configure_mesh_request count < sizeof(tddi_packet_t) + sizeof(response_data)\n");
       	 	al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : out of memory %s : %d\n",__func__,__LINE__);		
		return -ENOMEM;
	}

	response_packet.signature = TDDI_PACKET_SIGNATURE;
	response_packet.version   = TDDI_CURRENT_VERSION;
	response_packet.type      = TDDI_PACKET_TYPE_RESPONSE;
	response_packet.sub_type  = TDDI_RESPONSE_TYPE_GET_HW_CONFIG;
	response_packet.data_size = sizeof(tddi_get_hw_config_t);

	response_data.response = TDDI_CONFIGURE_MESH_RESPONSE_SUCCESS;

	if (copy_to_user(buf, &response_packet, sizeof(tddi_packet_t)))
	{
      		al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : Bad address,copy_to_user failed %s : %d\n",__func__,__LINE__);
		return -EFAULT;
	}

	if (response_data.response == TDDI_CONFIGURE_MESH_RESPONSE_SUCCESS)
	{
		request_data    = (tddi_get_hw_config_request_t *)data;

		core_net_if = meshap_core_lookup_net_if(request_data->if_name);
		if (core_net_if == NULL)
		{
      			al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : core netif is null %s : %d\n",__func__,__LINE__);
			return -1;
		}
	   memset((void *)&response_data.ht_cap, 0, sizeof(tddi_ht_cap_t));
	   memset((void *)&response_data.vht_cap, 0, sizeof(tddi_vht_cap_t));
		meshap_get_hw_support_ht_capab(core_net_if, &response_data.ht_cap,&response_data.vht_cap,request_data->sub_type);

		if (copy_to_user(buf + sizeof(tddi_packet_t), &response_data, sizeof(tddi_get_hw_config_t)))
		{
      			al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : Bad address,copy_to_user failed %s : %d\n",__func__,__LINE__);
			return -EFAULT;
		}
	}

	*ppos += sizeof(tddi_packet_t) + sizeof(tddi_get_hw_config_t);

	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s: %d\n",__func__,__LINE__);
	return sizeof(tddi_packet_t) + sizeof(tddi_get_hw_config_t);
}

int meshap_get_hw_support_ht_capab(meshap_core_net_if_t *core_net_if,tddi_ht_cap_t *ht_cap,tddi_vht_cap_t *vht_cap,unsigned char sub_type)
{
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s: %d\n",__func__,__LINE__);
	al_802_11_operations_t     *operations;
	al_net_if_t                *net_if;
	net_if = (al_net_if_t *)core_net_if;

	operations = (al_802_11_operations_t *)net_if->get_extended_operations(net_if);

	if (operations == NULL)
	{
      		al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : Null data %s : %d\n",__func__,__LINE__);
		return -1;
	}

	operations->get_hw_support_ht_capab(net_if,ht_cap,vht_cap,sub_type);

	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s: %d\n",__func__,__LINE__);
	return 0;
}

static inline ssize_t _meshap_tddi_get_wm_channel_flag(_file_data_t *fd, tddi_packet_t *packet, unsigned char *data, char *buf, size_t count, loff_t *ppos)
{
    tddi_packet_t                         response_packet;
    tddi_get_duty_cycle_flag_t            response_data;
    tddi_get_duty_cycle_flag_request_t   *request_data;
    meshap_core_net_if_t                 *core_net_if;

    if (count < sizeof(tddi_packet_t) + sizeof(response_data))
    {
        TORNA_LOG(MESHAP_LOGGER_TYPE_FLOW, "meshap_tddi: _meshap_tddi_configure_mesh_request count < sizeof(tddi_packet_t) + sizeof(response_data)\n");
        al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : out of memory %s : %d\n",__func__,__LINE__);
        return -ENOMEM;
    }

    response_packet.signature = TDDI_PACKET_SIGNATURE;
    response_packet.version   = TDDI_CURRENT_VERSION;
    response_packet.type      = TDDI_PACKET_TYPE_RESPONSE;
    response_packet.sub_type  = TDDI_REQUEST_TYPE_GET_WM_CHANNEL_FLAG;
    response_packet.data_size = sizeof(tddi_get_duty_cycle_flag_t);

    if (copy_to_user(buf, &response_packet, sizeof(tddi_packet_t)))
    {
        al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : Bad address,copy_to_user failed %s : %d\n",__func__,__LINE__);
        return -EFAULT;
    }
    request_data    = (tddi_get_duty_cycle_flag_request_t *)data;

    core_net_if = meshap_core_lookup_net_if(request_data->name);
    if (core_net_if == NULL)
    {
        al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : core netif is null %s : %d\n",__func__,__LINE__);
        return -1;
    }

    meshap_get_wm_duty_cycle_flag(core_net_if, &response_data.duty_cycle_flag, &response_data.operating_channel);

    if (copy_to_user(buf + sizeof(tddi_packet_t), &response_data, sizeof(tddi_get_duty_cycle_flag_t)))
    {
        al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : Bad address,copy_to_user failed %s : %d\n",__func__,__LINE__);
        return -EFAULT;
    }
    *ppos += sizeof(tddi_packet_t) + sizeof(tddi_get_duty_cycle_flag_t);

    return sizeof(tddi_packet_t) + sizeof(tddi_get_duty_cycle_flag_t);
}

int meshap_get_wm_duty_cycle_flag(meshap_core_net_if_t *core_net_if, int *duty_cycle_flag, int *operating_channel)
{
    al_802_11_operations_t     *operations;
    al_net_if_t                *net_if;
    net_if = (al_net_if_t *)core_net_if;

    operations = (al_802_11_operations_t *)net_if->get_extended_operations(net_if);

    if (operations == NULL)
    {
        al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : Null data %s : %d\n",__func__,__LINE__);
        return -1;
    }

    operations->get_wm_duty_cycle_flag(net_if, duty_cycle_flag, operating_channel);

    al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s: %d\n",__func__,__LINE__);
    return 0;
}

int set_ap_values(int al_conf_token)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s: %d\n",__func__,__LINE__);
   char              essid[AL_802_11_ESSID_MAX_SIZE + 1];
   unsigned short    beacon_interval;
   unsigned short    frag_th;
   unsigned short    rts_th;
   unsigned char     hop_cost;
   int               if_count;
   int               i, j;
   al_conf_if_info_t if_info;
   al_net_if_t       *net_if;
   al_net_if_t       *temp_net_if;
   int               al_netif_count;



   access_point_netif_config_info_t *netif_config_info;


   if_count = al_conf_get_if_count(AL_CONTEXT al_conf_token);

   netif_config_info = (access_point_netif_config_info_t *)al_heap_alloc(AL_CONTEXT
                                                                         sizeof(access_point_netif_config_info_t) * if_count AL_HEAP_DEBUG_PARAM);


   memset(netif_config_info, 0, sizeof(access_point_netif_config_info_t) * if_count);


   al_netif_count = al_get_net_if_count(AL_CONTEXT_SINGLE);
   al_conf_get_essid(AL_CONTEXT al_conf_token, essid, 256);
   beacon_interval = al_conf_get_beacon_interval(AL_CONTEXT al_conf_token);

   rts_th = al_conf_get_rts_th(AL_CONTEXT al_conf_token);

   frag_th = al_conf_get_frag_th(AL_CONTEXT al_conf_token);

   hop_cost = al_conf_get_hop_cost(AL_CONTEXT al_conf_token);


   for (i = 0; i < if_count; i++)
   {
      al_conf_get_if(al_conf_token, i, &if_info);


      net_if = NULL;

      for (j = 0; j < al_netif_count; j++)
      {
         temp_net_if = al_get_net_if(AL_CONTEXT j);
         if (!strcmp(temp_net_if->name, if_info.name))
         {
            net_if = temp_net_if;
            break;
         }
      }

      netif_config_info[i].net_if          = net_if;
      netif_config_info[i].service         = if_info.service;
      netif_config_info[i].txpower         = if_info.txpower;
      netif_config_info[i].rts_threshold   = if_info.rts_th;
      netif_config_info[i].frag_threshold  = if_info.frag_th;
      netif_config_info[i].beacon_interval = if_info.beacon_interval;
      netif_config_info[i].txrate          = if_info.txrate;
      netif_config_info[i].hide_ssid        = if_info.hide_ssid;
      mesh_config->if_info[i].use_type     = if_info.use_type;
      mesh_config->if_info[i].service      = if_info.service;
      strcpy(netif_config_info[i].essid, if_info.essid);
      if (mesh_config->if_info[i].use_type == AL_CONF_IF_USE_TYPE_AP && netif_config_info[i].service == AL_CONF_IF_SERVICE_BACKHAUL_ONLY) {
         access_point_remove_all_sta_table_entry(net_if->name);
      }
   }
   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"MESH_AP : INFO : HOP_COST = %x : %s : %d\n",hop_cost,__func__,__LINE__);        
   printk(KERN_EMERG "HOP_COST = %x\n", hop_cost);
   access_point_set_values(AL_CONTEXT_PARAM_DECL essid, beacon_interval, rts_th, frag_th, hop_cost, if_count, netif_config_info);

   al_heap_free(netif_config_info);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s: %d\n",__func__,__LINE__);
   return 0;
}


static inline ssize_t _meshap_tddi_get_ds_if(_file_data_t *fd, tddi_packet_t *packet, unsigned char *data, char *buf, size_t count, loff_t *ppos)
{
   //al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s: %d\n",__func__,__LINE__);
   tddi_packet_t             response_packet;
   tddi_get_ds_if_response_t response_data;
   int name_length;

   if (count < sizeof(tddi_packet_t) + sizeof(tddi_get_ds_if_response_t))
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : out of memory %s : %d\n",__func__,__LINE__);
      return -ENOMEM;
   }

   response_packet.signature = TDDI_PACKET_SIGNATURE;
   response_packet.version   = TDDI_CURRENT_VERSION;
   response_packet.type      = TDDI_PACKET_TYPE_RESPONSE;
   response_packet.sub_type  = TDDI_RESPONSE_TYPE_GET_DS_IF;

   if (_ds_net_if != NULL)
   {
      name_length = strlen(_ds_net_if->name);
   }
   else
   {
      name_length = 0;
   }

   response_packet.data_size = sizeof(tddi_get_ds_if_response_t) + name_length;

   if (meshap_core_globals.state == MESHAP_CORE_GLOBALS_STATE_STARTED)
   {
      if (_ds_net_if != NULL)
      {
         response_data.response       = TDDI_GET_DS_IF_STARTED;
         response_data.if_name_length = name_length;
      }
      else
      {
         response_data.response = TDDI_GET_DS_IF_RESPONSE_SCANNING;
      }
   }
   else
   {
      response_data.response = TDDI_GET_DS_IF_RESPONSE_NOT_STARTED;
   }

   if (copy_to_user(buf, &response_packet, sizeof(tddi_packet_t)))
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : Bad address,copy_to_user failed %s : %d\n",__func__,__LINE__);
      return -EFAULT;
   }

   if (copy_to_user(buf + sizeof(tddi_packet_t), &response_data, sizeof(tddi_get_ds_if_response_t)))
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : Bad address,copy_to_user failed %s : %d\n",__func__,__LINE__);
      return -EFAULT;
   }

   if ((name_length != 0) && (_ds_net_if != NULL))
   {
      if (copy_to_user(buf + sizeof(tddi_packet_t) + sizeof(tddi_get_ds_if_response_t),
                       _ds_net_if->name,
                       name_length))
      {
      	 al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : Bad address,copy_to_user failed: %s : %d\n",__func__,__LINE__);
         return -EFAULT;
      }
   }

   *ppos += sizeof(tddi_packet_t) + sizeof(tddi_get_ds_if_response_t) + name_length;

   //al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s: %d\n",__func__,__LINE__);
   return sizeof(tddi_packet_t) + sizeof(tddi_get_ds_if_response_t) + name_length;
}


static inline ssize_t _meshap_tddi_reboot(_file_data_t *fd, tddi_packet_t *packet, unsigned char *data, char *buf, size_t count, loff_t *ppos)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s: %d\n",__func__,__LINE__);
   tddi_packet_t          response_packet;
   tddi_reboot_response_t response_data;

   if (count < sizeof(tddi_packet_t) + sizeof(tddi_reboot_response_t))
   {
      TORNA_LOG(MESHAP_LOGGER_TYPE_FLOW, "meshap_tddi: _meshap_tddi_reboot count < sizeof(tddi_packet_t) + sizeof(tddi_reboot_response_t)\n");
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : out of memory : %s : %d\n",__func__,__LINE__);
	  return -ENOMEM;
   }

   response_packet.signature = TDDI_PACKET_SIGNATURE;
   response_packet.version   = TDDI_CURRENT_VERSION;
   response_packet.type      = TDDI_PACKET_TYPE_RESPONSE;
   response_packet.sub_type  = TDDI_RESPONSE_TYPE_REBOOTING;
   response_packet.data_size = sizeof(tddi_reboot_response_t);

   if (copy_to_user(buf, &response_packet, sizeof(tddi_packet_t)))
   {
      TORNA_LOG(MESHAP_LOGGER_TYPE_FLOW, "meshap_tddi: _meshap_tddi_reboot copy_to_user(buf,&response_packet,sizeof(tddi_packet_t)) failed\n");
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : Bad address,copy_to_user failed: %s : %d\n",__func__,__LINE__);
      return -EFAULT;
   }

   if (copy_to_user(buf + sizeof(tddi_packet_t), &response_data, sizeof(tddi_reboot_response_t)))
   {
      TORNA_LOG(MESHAP_LOGGER_TYPE_FLOW, "meshap_tddi: _meshap_tddi_reboot copy_to_user(buf + sizeof(tddi_packet_t),&response_data,sizeof(tddi_reboot_response_t)) failed\n");
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : Bad address %s : %d\n",__func__,__LINE__);
      return -EFAULT;
   }

   *ppos += sizeof(tddi_packet_t) + sizeof(tddi_reboot_response_t);

   meshap_reboot_machine(MESHAP_REBOOT_MACHINE_CODE_COMMAND_LINE);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s: %d\n",__func__,__LINE__);
   return sizeof(tddi_packet_t) + sizeof(tddi_reboot_response_t);
}


static inline ssize_t _meshap_tddi_preferred_parent(_file_data_t *fd, tddi_packet_t *packet, unsigned char *data, char *buf, size_t count, loff_t *ppos)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s: %d\n",__func__,__LINE__);
   tddi_packet_t response_packet;
   tddi_preferred_parent_response_t response_data;
   tddi_preferred_parent_request_t  *request_data;

   if (count < sizeof(tddi_packet_t) + sizeof(response_data))
   {
      TORNA_LOG(MESHAP_LOGGER_TYPE_FLOW, "meshap_tddi: _meshap_tddi_preferred_parent count < sizeof(tddi_packet_t) + sizeof(response_data)\n");
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : out of memory %s : %d\n",__func__,__LINE__);
      return -ENOMEM;
   }

   response_packet.signature = TDDI_PACKET_SIGNATURE;
   response_packet.version   = TDDI_CURRENT_VERSION;
   response_packet.type      = TDDI_PACKET_TYPE_RESPONSE;
   response_packet.sub_type  = TDDI_RESPONSE_TYPE_PREFERRED_PARENT;
   response_packet.data_size = sizeof(tddi_preferred_parent_response_t);

   if (meshap_core_globals.state != MESHAP_CORE_GLOBALS_STATE_STARTED)
   {
      response_data.response = TDDI_PREFERRED_PARENT_RESPONSE_NOT_STARTED;
   }
   else
   {
      request_data           = (tddi_preferred_parent_request_t *)data;
      response_data.response = mesh_ext_set_preferred_parent(request_data->enabled, request_data->preferred_parent);
   }

   if (copy_to_user(buf, &response_packet, sizeof(tddi_packet_t)))
   {
      TORNA_LOG(MESHAP_LOGGER_TYPE_FLOW, "meshap_tddi: _meshap_tddi_preferred_parent copy_to_user(buf,&response_packet,sizeof(tddi_packet_t)) failed\n");
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : Bad address %s : %d\n",__func__,__LINE__);
      return -EFAULT;
   }

   if (copy_to_user(buf + sizeof(tddi_packet_t), &response_data, sizeof(tddi_preferred_parent_response_t)))
   {
      TORNA_LOG(MESHAP_LOGGER_TYPE_FLOW, "meshap_tddi: _meshap_tddi_preferred_parent copy_to_user(buf + sizeof(tddi_packet_t),&response_data,sizeof(tddi_preferred_parent_response_t)) failed\n");
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : Bad address %s : %d\n",__func__,__LINE__);
      return -EFAULT;
   }

   *ppos += sizeof(tddi_packet_t) + sizeof(tddi_preferred_parent_response_t);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s: %d\n",__func__,__LINE__);
   return sizeof(tddi_packet_t) + sizeof(tddi_preferred_parent_response_t);
}


static inline ssize_t _meshap_tddi_kick_child(_file_data_t *fd, tddi_packet_t *packet, unsigned char *data, char *buf, size_t count, loff_t *ppos)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s: %d\n",__func__,__LINE__);
   tddi_packet_t              response_packet;
   tddi_kick_child_response_t response_data;
   tddi_kick_child_request_t  *request_data;

   if (count < sizeof(tddi_packet_t) + sizeof(response_data))
   {
      TORNA_LOG(MESHAP_LOGGER_TYPE_FLOW, "meshap_tddi: _meshap_tddi_kick_child count < sizeof(tddi_packet_t) + sizeof(response_data)\n");
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : out of memory %s : %d\n",__func__,__LINE__);
      return -ENOMEM;
   }

   response_packet.signature = TDDI_PACKET_SIGNATURE;
   response_packet.version   = TDDI_CURRENT_VERSION;
   response_packet.type      = TDDI_PACKET_TYPE_RESPONSE;
   response_packet.sub_type  = TDDI_RESPONSE_TYPE_KICK_CHILD;
   response_packet.data_size = sizeof(tddi_kick_child_response_t);

   if (meshap_core_globals.state != MESHAP_CORE_GLOBALS_STATE_STARTED)
   {
      response_data.response = TDDI_KICK_CHILD_RESPONSE_NOT_STARTED;
   }
   else
   {
      request_data           = (tddi_kick_child_request_t *)data;
      response_data.response = access_point_ext_disassociate_sta(request_data->address);
   }

   if (copy_to_user(buf, &response_packet, sizeof(tddi_packet_t)))
   {
      TORNA_LOG(MESHAP_LOGGER_TYPE_FLOW, "meshap_tddi: _meshap_tddi_kick_child copy_to_user(buf,&response_packet,sizeof(tddi_packet_t)) failed\n");
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : Bad address : copy to user failed %s : %d\n",__func__,__LINE__);
      return -EFAULT;
   }

   if (copy_to_user(buf + sizeof(tddi_packet_t), &response_data, sizeof(tddi_kick_child_response_t)))
   {
      TORNA_LOG(MESHAP_LOGGER_TYPE_FLOW, "meshap_tddi: _meshap_tddi_kick_child copy_to_user(buf + sizeof(tddi_packet_t),&response_data,sizeof(tddi_preferred_parent_response_t)) failed\n");
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : Bad address : copy to user failed %s : %d\n",__func__,__LINE__);
      return -EFAULT;
   }

   *ppos += sizeof(tddi_packet_t) + sizeof(tddi_kick_child_response_t);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s: %d\n",__func__,__LINE__);
   return sizeof(tddi_packet_t) + sizeof(tddi_kick_child_response_t);
}


static inline ssize_t _meshap_tddi_set_vlan(_file_data_t *fd, tddi_packet_t *packet, unsigned char *data, char *buf, size_t count, loff_t *ppos)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s: %d\n",__func__,__LINE__);
   tddi_packet_t            response_packet;
   tddi_set_vlan_response_t response_data;
   tddi_set_vlan_request_t  *request_data;

   if (count < sizeof(tddi_packet_t) + sizeof(response_data))
   {
      TORNA_LOG(MESHAP_LOGGER_TYPE_FLOW, "meshap_tddi: _meshap_tddi_set_vlan count < sizeof(tddi_packet_t) + sizeof(response_data)\n");
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : out of memory %s : %d\n",__func__,__LINE__);
      return -ENOMEM;
   }

   response_packet.signature = TDDI_PACKET_SIGNATURE;
   response_packet.version   = TDDI_CURRENT_VERSION;
   response_packet.type      = TDDI_PACKET_TYPE_RESPONSE;
   response_packet.sub_type  = TDDI_RESPONSE_TYPE_SET_VLAN;
   response_packet.data_size = sizeof(tddi_set_vlan_response_t);

   if (meshap_core_globals.state != MESHAP_CORE_GLOBALS_STATE_STARTED)
   {
      response_data.response = TDDI_SET_VLAN_RESPONSE_NOT_STARTED;
   }
   else
   {
      request_data           = (tddi_set_vlan_request_t *)data;
      response_data.response = access_point_ext_set_sta_vlan(request_data->address, request_data->vlan_tag);
   }

   if (copy_to_user(buf, &response_packet, sizeof(tddi_packet_t)))
   {
      TORNA_LOG(MESHAP_LOGGER_TYPE_FLOW, "meshap_tddi: _meshap_tddi_set_vlan copy_to_user(buf,&response_packet,sizeof(tddi_packet_t)) failed\n");
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : Bad address :  copy to user failed %s : %d\n",__func__,__LINE__);
      return -EFAULT;
   }

   if (copy_to_user(buf + sizeof(tddi_packet_t), &response_data, sizeof(tddi_set_vlan_response_t)))
   {
      TORNA_LOG(MESHAP_LOGGER_TYPE_FLOW, "meshap_tddi: _meshap_tddi_set_vlan copy_to_user(buf + sizeof(tddi_packet_t),&response_data,sizeof(tddi_set_vlan_response_t)) failed\n");
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : Bad address :  copy to user failed %s : %d\n",__func__,__LINE__);
      return -EFAULT;
   }

   *ppos += sizeof(tddi_packet_t) + sizeof(tddi_set_vlan_response_t);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s: %d\n",__func__,__LINE__);
   return sizeof(tddi_packet_t) + sizeof(tddi_set_vlan_response_t);
}


static void _meshap_panic_timer_function(unsigned long arg)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s: %d\n",__func__,__LINE__);
   unsigned char *p;

   p = NULL;

   *p = 0;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s: %d\n",__func__,__LINE__);
}


static struct timer_list _meshap_panic_timer;

/**
 * The following block of code was added for debugging on 13th AUG 2007.
 * If this code remains here after 13th Oct 2007, please remove it.
 */

#if 0

static inline void _send_auth_request(struct net_device *dev, unsigned char *dst_addr)
{mac_hdr;
   unsigned char   *data;

   static const unsigned char _auth_resp_data[] =
   {
      0x00, 0x00, 0x01, 0x00, 0x00, 0x00
   };

   skb_out = dev_alloc_skb(2400);
   mac_hdr = (torna_mac_hdr_t *)skb_put(skb_out, sizeof(torna_mac_hdr_t));

   memset(mac_hdr, 0, sizeof(torna_mac_hdr_t));

   mac_hdr->signature[0] = TORNA_MAC_HDR_SIGNATURE_1;
   mac_hdr->signature[1] = TORNA_MAC_HDR_SIGNATURE_2;
   mac_hdr->type         = TORNA_MAC_HDR_TYPE_SK_BUFF;

   memcpy(mac_hdr->addresses[0], dst_addr, ETH_ALEN);
   memcpy(mac_hdr->addresses[1], dev->dev_addr, ETH_ALEN);
   memcpy(mac_hdr->addresses[2], dst_addr, ETH_ALEN);

   AL_802_11_FC_SET_TYPE(mac_hdr->frame_control, AL_802_11_FC_TYPE_MGMT);
   AL_802_11_FC_SET_STYPE(mac_hdr->frame_control, AL_802_11_FC_STYPE_AUTH);

   data = (unsigned char *)skb_put(skb_out, sizeof(_auth_resp_data));
   memcpy(data, _auth_resp_data, sizeof(_auth_resp_data));

   skb_out->mac.raw = (u8 *)mac_hdr;
   skb_out->dev     = dev;

   dev_queue_xmit(skb_out);
}


static inline void _send_deauth(struct net_device *dev, unsigned char *dst_addr)
{
   struct sk_buff  *skb_out;
   torna_mac_hdr_t *mac_hdr;
   unsigned char   *data;
   unsigned short  reason;

   skb_out = dev_alloc_skb(2400);
   mac_hdr = (torna_mac_hdr_t *)skb_put(skb_out, sizeof(torna_mac_hdr_t));

   memset(mac_hdr, 0, sizeof(torna_mac_hdr_t));

   mac_hdr->signature[0] = TORNA_MAC_HDR_SIGNATURE_1;
   mac_hdr->signature[1] = TORNA_MAC_HDR_SIGNATURE_2;
   mac_hdr->type         = TORNA_MAC_HDR_TYPE_SK_BUFF;

   memcpy(mac_hdr->addresses[0], dst_addr, ETH_ALEN);
   memcpy(mac_hdr->addresses[1], dev->dev_addr, ETH_ALEN);
   memcpy(mac_hdr->addresses[2], dst_addr, ETH_ALEN);

   AL_802_11_FC_SET_TYPE(mac_hdr->frame_control, AL_802_11_FC_TYPE_MGMT);
   AL_802_11_FC_SET_STYPE(mac_hdr->frame_control, AL_802_11_FC_STYPE_DEAUTH);

   reason = le16_to_cpu(AL_802_11_REASON_UNSPECIFIED);
   data   = (unsigned char *)skb_put(skb_out, sizeof(reason));
   memcpy(data, &reason, sizeof(reason));

   skb_out->mac.raw = (u8 *)mac_hdr;
   skb_out->dev     = dev;

   dev_queue_xmit(skb_out);
}


static void _meshap_auth_deauth_test()
{
   int               i;
   struct net_device *wlan1;

   static unsigned char mac[] = { 0x00, 0x12, 0xCE, 0x00, 0x00, 0x0E };

   wlan1 = dev_get_by_name("wlan1");

   for (i = 0; i < 10000; i++)
   {
      _send_auth_request(wlan1, mac);
      _send_deauth(wlan1, mac);
   }

   dev_put(wlan1);
}
#endif

static inline ssize_t _meshap_tddi_block_cpu(_file_data_t *fd, tddi_packet_t *packet, unsigned char *data, char *buf, size_t count, loff_t *ppos)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s: %d\n",__func__,__LINE__);
   tddi_packet_t            response_packet;
   tddi_block_cpu_request_t *request_data;
   unsigned long            flags;

   if (count < sizeof(tddi_packet_t))
   {
      TORNA_LOG(MESHAP_LOGGER_TYPE_FLOW, "meshap_tddi: _meshap_tddi_block_cpu count < sizeof(tddi_packet_t) + sizeof(response_data)\n");
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : out of memory %s : %d\n",__func__,__LINE__);
      return -ENOMEM;
   }

   response_packet.signature = TDDI_PACKET_SIGNATURE;
   response_packet.version   = TDDI_CURRENT_VERSION;
   response_packet.type      = TDDI_PACKET_TYPE_RESPONSE;
   response_packet.sub_type  = TDDI_RESPONSE_TYPE_BLOCK_CPU;
   response_packet.data_size = 0;

   if (meshap_core_globals.state != MESHAP_CORE_GLOBALS_STATE_STARTED)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : Invalid argument found %s : %d\n",__func__,__LINE__);
      return -EINVAL;
   }
   else
   {
      request_data = (tddi_block_cpu_request_t *)data;

      switch (request_data->type)
      {
      case TDDI_BLOCK_CPU_REQUEST_TYPE_LOOP:
      	 al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"MESH_AP : INFO : meshap_tddi:meshap_tddi: Starting INFINITE loop... %s : %d\n", __func__,__LINE__);        
         while (1)
         {
         }
         break;

      case TDDI_BLOCK_CPU_REQUEST_TYPE_IRQ_LOCK:
      	 al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"MESH_AP : INFO : meshap_tddi:meshap_tddi:  Locking IRQs indefinetely %s : %d\n", __func__,__LINE__);        
         local_irq_save(flags);
         break;

      case TDDI_BLOCK_CPU_REQUEST_TYPE_CRASH:
         {
            char *p = NULL;
            *p = 0;
         }
         break;

      case TDDI_BLOCK_CPU_REQUEST_TYPE_PANIC:
         _meshap_panic_timer.expires  = jiffies + (5 * HZ);
         _meshap_panic_timer.function = _meshap_panic_timer_function;
         add_timer(&_meshap_panic_timer);
         break;

      case 5:

         /**
          * Use this case for custom debugging purposes.
          * Make sure you remove whatever code you write in this case after debugging.
          */
         break;
      }
   }

   if (copy_to_user(buf, &response_packet, sizeof(tddi_packet_t)))
   {
      TORNA_LOG(MESHAP_LOGGER_TYPE_FLOW, "meshap_tddi: _meshap_tddi_block_cpu copy_to_user(buf,&response_packet,sizeof(tddi_packet_t)) failed\n");
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : Bad address :  copy to user failed %s : %d\n",__func__,__LINE__);
      return -EFAULT;
   }

   *ppos += sizeof(tddi_packet_t);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s: %d\n",__func__,__LINE__);
   return sizeof(tddi_packet_t);
}


static inline ssize_t _meshap_tddi_random(_file_data_t *fd, tddi_packet_t *packet, unsigned char *data, char *buf, size_t count, loff_t *ppos)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s: %d\n",__func__,__LINE__);
   tddi_packet_t          response_packet;
   tddi_random_response_t response_data;
   tddi_random_request_t  *request_data;

   if (count < sizeof(tddi_packet_t) + sizeof(response_data))
   {
      TORNA_LOG(MESHAP_LOGGER_TYPE_FLOW, "meshap_tddi: _meshap_tddi_random count < sizeof(tddi_packet_t) + sizeof(response_data)\n");
       al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : out of memory %s : %d\n",__func__,__LINE__);
      return -ENOMEM;
   }

   response_packet.signature = TDDI_PACKET_SIGNATURE;
   response_packet.version   = TDDI_CURRENT_VERSION;
   response_packet.type      = TDDI_PACKET_TYPE_RESPONSE;
   response_packet.sub_type  = TDDI_RESPONSE_TYPE_RANDOM;
   response_packet.data_size = sizeof(tddi_random_response_t);

   request_data         = (tddi_random_request_t *)data;
   response_data.random = al_random(AL_CONTEXT request_data->min, request_data->max);

   if (copy_to_user(buf, &response_packet, sizeof(tddi_packet_t)))
   {
      TORNA_LOG(MESHAP_LOGGER_TYPE_FLOW, "meshap_tddi: _meshap_tddi_kick_child copy_to_user(buf,&response_packet,sizeof(tddi_packet_t)) failed\n");
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : Bad address :  copy to user failed %s : %d\n",__func__,__LINE__);
      return -EFAULT;
   }

   if (copy_to_user(buf + sizeof(tddi_packet_t), &response_data, sizeof(tddi_random_response_t)))
   {
      TORNA_LOG(MESHAP_LOGGER_TYPE_FLOW, "meshap_tddi: _meshap_tddi_kick_child copy_to_user(buf + sizeof(tddi_packet_t),&response_data,sizeof(tddi_preferred_parent_response_t)) failed\n");
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : Bad address :  copy to user failed %s : %d\n",__func__,__LINE__);
      return -EFAULT;
   }

   *ppos += sizeof(tddi_packet_t) + sizeof(tddi_random_response_t);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s: %d\n",__func__,__LINE__);
   return sizeof(tddi_packet_t) + sizeof(tddi_random_response_t);
}


static inline ssize_t _meshap_tddi_mesh_flags(_file_data_t *fd, tddi_packet_t *packet, unsigned char *data, char *buf, size_t count, loff_t *ppos)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s: %d\n",__func__,__LINE__);
   tddi_packet_t              response_packet;
   tddi_mesh_flags_response_t response_data;
   tddi_mesh_flags_request_t  *request_data;

   if (count < sizeof(tddi_packet_t) + sizeof(response_data))
   {
      TORNA_LOG(MESHAP_LOGGER_TYPE_FLOW, "meshap_tddi: _meshap_tddi_mesh_flags count < sizeof(tddi_packet_t) + sizeof(response_data)\n");
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : out of memory %s : %d\n",__func__,__LINE__);
      return -ENOMEM;
   }

   response_packet.signature = TDDI_PACKET_SIGNATURE;
   response_packet.version   = TDDI_CURRENT_VERSION;
   response_packet.type      = TDDI_PACKET_TYPE_RESPONSE;
   response_packet.sub_type  = TDDI_RESPONSE_TYPE_MESH_FLAGS;
   response_packet.data_size = sizeof(tddi_mesh_flags_response_t);

   request_data           = (tddi_mesh_flags_request_t *)data;
   response_data.response = mesh_ext_set_test_flags(request_data->flags);

   if (copy_to_user(buf, &response_packet, sizeof(tddi_packet_t)))
   {
      TORNA_LOG(MESHAP_LOGGER_TYPE_FLOW, "meshap_tddi: _meshap_tddi_mesh_flags copy_to_user(buf,&response_packet,sizeof(tddi_packet_t)) failed\n");
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : Bad address :  copy to user failed %s : %d\n",__func__,__LINE__);
      return -EFAULT;
   }

   if (copy_to_user(buf + sizeof(tddi_packet_t), &response_data, sizeof(tddi_mesh_flags_response_t)))
   {
      TORNA_LOG(MESHAP_LOGGER_TYPE_FLOW, "meshap_tddi: _meshap_tddi_mesh_flags copy_to_user(buf + sizeof(tddi_packet_t),&response_data,sizeof(tddi_mesh_flags_response_t)) failed\n");
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : Bad address :  copy to user failed %s : %d\n",__func__,__LINE__);
      return -EFAULT;
   }

   *ppos += sizeof(tddi_packet_t) + sizeof(tddi_mesh_flags_response_t);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s: %d\n",__func__,__LINE__);
   return sizeof(tddi_packet_t) + sizeof(tddi_mesh_flags_response_t);
}


static inline ssize_t _meshap_tddi_ap_flags(_file_data_t *fd, tddi_packet_t *packet, unsigned char *data, char *buf, size_t count, loff_t *ppos)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s: %d\n",__func__,__LINE__);
   tddi_packet_t            response_packet;
   tddi_ap_flags_response_t response_data;
   tddi_ap_flags_request_t  *request_data;

   if (count < sizeof(tddi_packet_t) + sizeof(response_data))
   {
      TORNA_LOG(MESHAP_LOGGER_TYPE_FLOW, "meshap_tddi: _meshap_tddi_kick_child count < sizeof(tddi_packet_t) + sizeof(response_data)\n");
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : out of memory %s : %d\n",__func__,__LINE__);
      return -ENOMEM;
   }

   response_packet.signature = TDDI_PACKET_SIGNATURE;
   response_packet.version   = TDDI_CURRENT_VERSION;
   response_packet.type      = TDDI_PACKET_TYPE_RESPONSE;
   response_packet.sub_type  = TDDI_REQUEST_TYPE_AP_FLAGS;
   response_packet.data_size = sizeof(tddi_ap_flags_response_t);

   request_data           = (tddi_ap_flags_request_t *)data;
   response_data.response = access_point_ext_set_test_flags(request_data->flags);

   if (copy_to_user(buf, &response_packet, sizeof(tddi_packet_t)))
   {
      TORNA_LOG(MESHAP_LOGGER_TYPE_FLOW, "meshap_tddi: _meshap_tddi_ap_flags copy_to_user(buf,&response_packet,sizeof(tddi_packet_t)) failed\n");
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : Bad address :  copy to user failed %s : %d\n",__func__,__LINE__);
      return -EFAULT;
   }

   if (copy_to_user(buf + sizeof(tddi_packet_t), &response_data, sizeof(tddi_ap_flags_response_t)))
   {
      TORNA_LOG(MESHAP_LOGGER_TYPE_FLOW, "meshap_tddi: _meshap_tddi_ap_flags copy_to_user(buf + sizeof(tddi_packet_t),&response_data,sizeof(tddi_mesh_flags_response_t)) failed\n");
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : Bad address :  copy to user failed %s : %d\n",__func__,__LINE__);
      return -EFAULT;
   }

   *ppos += sizeof(tddi_packet_t) + sizeof(tddi_ap_flags_response_t);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s: %d\n",__func__,__LINE__);

   return sizeof(tddi_packet_t) + sizeof(tddi_ap_flags_response_t);
}


static inline ssize_t _meshap_tddi_mesh_config_sqnr(_file_data_t *fd, tddi_packet_t *packet, unsigned char *data, char *buf, size_t count, loff_t *ppos)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s: %d\n",__func__,__LINE__);
   tddi_packet_t response_packet;
   tddi_mesh_config_sqnr_response_t response_data;
   tddi_mesh_config_sqnr_request_t  *request_data;

   if (count < sizeof(tddi_packet_t) + sizeof(response_data))
   {
      TORNA_LOG(MESHAP_LOGGER_TYPE_FLOW, "meshap_tddi: _meshap_tddi_mesh_config_sqnr count < sizeof(tddi_packet_t) + sizeof(response_data)\n");
       al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : out of memory %s : %d\n",__func__,__LINE__);
      return -ENOMEM;
   }

   response_packet.signature = TDDI_PACKET_SIGNATURE;
   response_packet.version   = TDDI_CURRENT_VERSION;
   response_packet.type      = TDDI_PACKET_TYPE_RESPONSE;
   response_packet.sub_type  = TDDI_REQUEST_TYPE_MESH_CONFIG_SQNR;
   response_packet.data_size = sizeof(tddi_mesh_config_sqnr_response_t);

   request_data           = (tddi_mesh_config_sqnr_request_t *)data;
   response_data.response = mesh_ext_set_config_sqnr(request_data->sqnr);

   if (copy_to_user(buf, &response_packet, sizeof(tddi_packet_t)))
   {
      TORNA_LOG(MESHAP_LOGGER_TYPE_FLOW, "meshap_tddi: _meshap_tddi_mesh_config_sqnr copy_to_user(buf,&response_packet,sizeof(tddi_packet_t)) failed\n");
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : Bad address :  copy to user failed %s : %d\n",__func__,__LINE__);
      return -EFAULT;
   }

   if (copy_to_user(buf + sizeof(tddi_packet_t), &response_data, sizeof(tddi_mesh_config_sqnr_response_t)))
   {
      TORNA_LOG(MESHAP_LOGGER_TYPE_FLOW, "meshap_tddi: _meshap_tddi_mesh_config_sqnr copy_to_user(buf + sizeof(tddi_packet_t),&response_data,sizeof(tddi_mesh_flags_response_t)) failed\n");
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : Bad address :  copy to user failed %s : %d\n",__func__,__LINE__);
      return -EFAULT;
   }

   *ppos += sizeof(tddi_packet_t) + sizeof(tddi_mesh_config_sqnr_response_t);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s: %d\n",__func__,__LINE__);
   return sizeof(tddi_packet_t) + sizeof(tddi_mesh_config_sqnr_response_t);
}


static inline ssize_t _meshap_tddi_configure_dot11e_request(_file_data_t *fd, tddi_packet_t *packet, unsigned char *data, char *buf, size_t count, loff_t *ppos)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s: %d\n",__func__,__LINE__);
   tddi_packet_t response_packet;
   tddi_configure_dot11e_response_t response_data;
   tddi_configure_dot11e_request_t  *request_data;
   char *dot11e_config_string;
   int  dot11e_conf_token;

   dot11e_conf_token = 0;

   if (count < sizeof(tddi_packet_t) + sizeof(response_data))
   {
      TORNA_LOG(MESHAP_LOGGER_TYPE_FLOW, "meshap_tddi: _meshap_tddi_configure_dot11e_request count < sizeof(tddi_packet_t) + sizeof(response_data)\n");
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : out of memory %s : %d\n",__func__,__LINE__);
      return -ENOMEM;
   }

   response_packet.signature = TDDI_PACKET_SIGNATURE;
   response_packet.version   = TDDI_CURRENT_VERSION;
   response_packet.type      = TDDI_PACKET_TYPE_RESPONSE;
   response_packet.sub_type  = TDDI_RESPONSE_TYPE_CONFIGURE_DOT11E;
   response_packet.data_size = sizeof(tddi_configure_dot11e_response_t);

   if (meshap_core_globals.state == MESHAP_CORE_GLOBALS_STATE_STARTED)
   {
      response_data.response = TDDI_CONFIGURE_DOT11E_RESPONSE_BUSY;
   }
   else
   {
      response_data.response = TDDI_CONFIGURE_DOT11E_RESPONSE_SUCCESS;
   }

   if ((response_data.response == TDDI_CONFIGURE_DOT11E_RESPONSE_SUCCESS) || (TDDI_CONFIGURE_DOT11E_RESPONSE_BUSY))
   {
      if (meshap_core_globals.dot11e_config_string != NULL)
      {
         kfree(meshap_core_globals.dot11e_config_string);
      }

      request_data = (tddi_configure_dot11e_request_t *)data;

      meshap_core_globals.dot11e_config_string = (char *)kmalloc(request_data->config_string_length + 1, GFP_KERNEL);
      memset(meshap_core_globals.dot11e_config_string, 0, request_data->config_string_length + 1);

      dot11e_config_string = data + sizeof(tddi_configure_dot11e_request_t);

      memcpy(meshap_core_globals.dot11e_config_string, dot11e_config_string, request_data->config_string_length);

      dot11e_conf_token = dot11e_conf_parse(AL_CONTEXT meshap_core_globals.dot11e_config_string);

      if (dot11e_conf_token == 0)
      {
         kfree(meshap_core_globals.dot11e_config_string);
         meshap_core_globals.dot11e_config_string = NULL;
         TORNA_LOG(MESHAP_LOGGER_TYPE_FLOW, "meshap_tddi: _meshap_tddi_configure_dot11e_request meshap_tddi_globals.dot11e_conf_token == 0\n");
      	 al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : dot11e conf returns 0 %s : %d\n",__func__,__LINE__);
         return -EINVAL;
      }
   }

   if (copy_to_user(buf, &response_packet, sizeof(tddi_packet_t)))
   {
      TORNA_LOG(MESHAP_LOGGER_TYPE_FLOW, "meshap_tddi: _meshap_tddi_configure_dot11e_request copy_to_user(buf,&response_packet,sizeof(tddi_packet_t)) failed\n");
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : Bad address %s : %d\n",__func__,__LINE__);
      return -EFAULT;
   }

   if (copy_to_user(buf + sizeof(tddi_packet_t), &response_data, sizeof(tddi_configure_dot11e_response_t)))
   {
      TORNA_LOG(MESHAP_LOGGER_TYPE_FLOW, "meshap_tddi: _meshap_tddi_configure_dot11e_request copy_to_user(buf + sizeof(tddi_packet_t),&response_data,sizeof(tddi_configure_dot11e_response_t)) failed\n");
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : Bad address %s : %d\n",__func__,__LINE__);
      return -EFAULT;
   }

   dot11e_conf_close(AL_CONTEXT dot11e_conf_token);

   *ppos += sizeof(tddi_packet_t) + sizeof(tddi_configure_dot11e_response_t);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s: %d\n",__func__,__LINE__);
   return sizeof(tddi_packet_t) + sizeof(tddi_configure_dot11e_response_t);
}


static inline ssize_t _meshap_tddi_configure_acl_request(_file_data_t *fd, tddi_packet_t *packet, unsigned char *data, char *buf, size_t count, loff_t *ppos)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s: %d\n",__func__,__LINE__);
   tddi_packet_t                 response_packet;
   tddi_configure_acl_response_t response_data;
   tddi_configure_acl_request_t  *request_data;
   char *acl_config_string;
   int  acl_conf_token;

   acl_conf_token = 0;

   if (count < sizeof(tddi_packet_t) + sizeof(response_data))
   {
      TORNA_LOG(MESHAP_LOGGER_TYPE_FLOW, "meshap_tddi: _meshap_tddi_configure_acl_request count < sizeof(tddi_packet_t) + sizeof(response_data)\n");
      return -ENOMEM;
   }

   response_packet.signature = TDDI_PACKET_SIGNATURE;
   response_packet.version   = TDDI_CURRENT_VERSION;
   response_packet.type      = TDDI_PACKET_TYPE_RESPONSE;
   response_packet.sub_type  = TDDI_RESPONSE_TYPE_CONFIGURE_ACL;
   response_packet.data_size = sizeof(tddi_configure_acl_response_t);

   if (meshap_core_globals.state == MESHAP_CORE_GLOBALS_STATE_STARTED)
   {
      response_data.response = TDDI_CONFIGURE_ACL_RESPONSE_BUSY;
   }
   else
   {
      response_data.response = TDDI_CONFIGURE_ACL_RESPONSE_SUCCESS;
   }

   if ((response_data.response == TDDI_CONFIGURE_ACL_RESPONSE_SUCCESS) || (TDDI_CONFIGURE_ACL_RESPONSE_BUSY))
   {
      if (meshap_core_globals.acl_config_string != NULL)
      {
         kfree(meshap_core_globals.acl_config_string);
      }

      request_data = (tddi_configure_acl_request_t *)data;

      meshap_core_globals.acl_config_string = (char *)kmalloc(request_data->config_string_length + 1, GFP_KERNEL);
      memset(meshap_core_globals.acl_config_string, 0, request_data->config_string_length + 1);

      acl_config_string = data + sizeof(tddi_configure_acl_request_t);

      memcpy(meshap_core_globals.acl_config_string, acl_config_string, request_data->config_string_length);

      acl_conf_token = acl_conf_parse(AL_CONTEXT meshap_core_globals.acl_config_string);

      if (acl_conf_token == 0)
      {
         kfree(meshap_core_globals.acl_config_string);
         meshap_core_globals.acl_config_string = NULL;
         TORNA_LOG(MESHAP_LOGGER_TYPE_FLOW, "meshap_tddi: _meshap_tddi_configure_acl_request meshap_tddi_globals.acl_conf_token == 0\n");
      	 al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : Bad address :  acl conf returns 0 %s : %d\n",__func__,__LINE__);
         return -EINVAL;
      }
   }

   if (copy_to_user(buf, &response_packet, sizeof(tddi_packet_t)))
   {
      TORNA_LOG(MESHAP_LOGGER_TYPE_FLOW, "meshap_tddi: _meshap_tddi_configure_acl_request copy_to_user(buf,&response_packet,sizeof(tddi_packet_t)) failed\n");
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : Bad address :  copy to user failed %s : %d\n",__func__,__LINE__);
      return -EFAULT;
   }

   if (copy_to_user(buf + sizeof(tddi_packet_t), &response_data, sizeof(tddi_configure_acl_response_t)))
   {
      TORNA_LOG(MESHAP_LOGGER_TYPE_FLOW, "meshap_tddi: _meshap_tddi_configure_acl_request copy_to_user(buf + sizeof(tddi_packet_t),&response_data,sizeof(tddi_configure_acl_response_t)) failed\n");
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : Bad address :  copy to user failed %s : %d\n",__func__,__LINE__);
      return -EFAULT;
   }

   acl_conf_close(AL_CONTEXT acl_conf_token);
   acl_conf_token = 0;

   *ppos += sizeof(tddi_packet_t) + sizeof(tddi_configure_acl_response_t);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s: %d\n",__func__,__LINE__);
   return sizeof(tddi_packet_t) + sizeof(tddi_configure_acl_response_t);
}


static inline ssize_t _meshap_tddi_configure_mobility_request(_file_data_t *fd, tddi_packet_t *packet, unsigned char *data, char *buf, size_t count, loff_t *ppos)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s: %d\n",__func__,__LINE__);
   tddi_packet_t response_packet;
   tddi_configure_mobility_response_t response_data;
   tddi_configure_mobility_request_t  *request_data;
   char *config_string;
   int  mobility_conf_token;

   mobility_conf_token = 0;

   if (count < sizeof(tddi_packet_t) + sizeof(response_data))
   {
      TORNA_LOG(MESHAP_LOGGER_TYPE_FLOW, "meshap_tddi: _meshap_tddi_configure_mobility_request count < sizeof(tddi_packet_t) + sizeof(response_data)\n");
       al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : out of memory %s : %d\n",__func__,__LINE__);
      return -ENOMEM;
   }

   response_packet.signature = TDDI_PACKET_SIGNATURE;
   response_packet.version   = TDDI_CURRENT_VERSION;
   response_packet.type      = TDDI_PACKET_TYPE_RESPONSE;
   response_packet.sub_type  = TDDI_RESPONSE_TYPE_CONFIGURE_MOBILITY;
   response_packet.data_size = sizeof(tddi_configure_mobility_response_t);

   if (meshap_core_globals.state == MESHAP_CORE_GLOBALS_STATE_STARTED)
   {
      response_data.response = TDDI_CONFIGURE_MOBILITY_RESPONSE_BUSY;
   }
   else
   {
      response_data.response = TDDI_CONFIGURE_MOBILITY_RESPONSE_SUCCESS;
   }

   if (response_data.response == TDDI_CONFIGURE_MOBILITY_RESPONSE_SUCCESS)
   {
      if (meshap_core_globals.mobility_conf_string != NULL)
      {
         kfree(meshap_core_globals.mobility_conf_string);
      }

      request_data = (tddi_configure_mobility_request_t *)data;

      meshap_core_globals.mobility_conf_string = (char *)kmalloc(request_data->config_string_length + 1, GFP_KERNEL);
      memset(meshap_core_globals.mobility_conf_string, 0, request_data->config_string_length + 1);

      config_string = data + sizeof(tddi_configure_mobility_request_t);

      memcpy(meshap_core_globals.mobility_conf_string, config_string, request_data->config_string_length);

      mobility_conf_token = mobility_conf_parse(AL_CONTEXT meshap_core_globals.mobility_conf_string);

      if (mobility_conf_token == 0)
      {
         kfree(meshap_core_globals.mobility_conf_string);
         meshap_core_globals.mobility_conf_string = NULL;
         TORNA_LOG(MESHAP_LOGGER_TYPE_FLOW, "meshap_tddi: _meshap_tddi_configure_mobility_request meshap_tddi_globals.mobility_conf_token == 0\n");
         al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : mobility conf returns 0 %s : %d\n",__func__,__LINE__);
         return -EINVAL;
      }
   }

   if (copy_to_user(buf, &response_packet, sizeof(tddi_packet_t)))
   {
      TORNA_LOG(MESHAP_LOGGER_TYPE_FLOW, "meshap_tddi: _meshap_tddi_configure_mobility_request copy_to_user(buf,&response_packet,sizeof(tddi_packet_t)) failed\n");
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : Bad address :  copy to user failed %s : %d\n",__func__,__LINE__);
      return -EFAULT;
   }

   if (copy_to_user(buf + sizeof(tddi_packet_t), &response_data, sizeof(tddi_configure_mobility_response_t)))
   {
      TORNA_LOG(MESHAP_LOGGER_TYPE_FLOW, "meshap_tddi: _meshap_tddi_configure_mobility_request copy_to_user(buf + sizeof(tddi_packet_t),&response_data,sizeof(tddi_configure_mobility_response_t)) failed\n");
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : Bad address :  copy to user failed %s : %d\n",__func__,__LINE__);
      return -EFAULT;
   }

   *ppos += sizeof(tddi_packet_t) + sizeof(tddi_configure_mobility_response_t);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"MESH_AP : INFO : meshap_tddi: Accepted mobility configuration with %d indices : %s : %d\n",mobility_conf_get_index_count(AL_CONTEXT mobility_conf_token), __func__,__LINE__);        

   mobility_conf_close(AL_CONTEXT mobility_conf_token);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s: %d\n",__func__,__LINE__);
   return sizeof(tddi_packet_t) + sizeof(tddi_configure_mobility_response_t);
}


static inline ssize_t _meshap_tddi_lock_reboot_request(_file_data_t *fd, tddi_packet_t *packet, unsigned char *data, char *buf, size_t count, loff_t *ppos)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s: %d\n",__func__,__LINE__);
   tddi_packet_t               response_packet;
   tddi_lock_reboot_response_t response_data;
   tddi_lock_reboot_request_t  *request_data;

   if (count < sizeof(tddi_packet_t) + sizeof(response_data))
   {
      TORNA_LOG(MESHAP_LOGGER_TYPE_FLOW, "meshap_tddi: _meshap_tddi_lock_reboot_request count < sizeof(tddi_packet_t) + sizeof(response_data)\n");
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : out of memory %s : %d\n",__func__,__LINE__);
      return -ENOMEM;
   }

   response_packet.signature = TDDI_PACKET_SIGNATURE;
   response_packet.version   = TDDI_CURRENT_VERSION;
   response_packet.type      = TDDI_PACKET_TYPE_RESPONSE;
   response_packet.sub_type  = TDDI_RESPONSE_TYPE_LOCK_REBOOT;
   response_packet.data_size = sizeof(tddi_lock_reboot_response_t);

   request_data = (tddi_lock_reboot_request_t *)data;

   if (request_data->lock_or_unlock)
   {
      MESHAP_ATOMIC_INC(meshap_core_globals.reboot_locked);
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"MESH_AP : INFO : meshap_tddi: meshap_tddi: Locking reboot (%d)...: %s : %d\n",MESHAP_ATOMIC_GET(meshap_core_globals.reboot_locked), __func__,__LINE__);        
   }
   else
   {
      if (MESHAP_ATOMIC_DEC_AND_TEST(meshap_core_globals.reboot_locked))
      {
         if (MESHAP_ATOMIC_GET(meshap_core_globals.reboot_pending) > 0)
         {
      	    al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"MESH_AP : INFO : meshap_tddi:Executing queued reboot request...: %s : %d\n", __func__,__LINE__);        
            meshap_core_exec_reboot(meshap_core_globals.reboot_code);
         }
      }
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"MESH_AP : INFO : meshap_tddi: Unlocking reboot (%d)... : %s : %d\n",MESHAP_ATOMIC_GET(meshap_core_globals.reboot_locked), __func__,__LINE__);        
   }

   response_data.lock_count = MESHAP_ATOMIC_GET(meshap_core_globals.reboot_locked);

   if (copy_to_user(buf, &response_packet, sizeof(tddi_packet_t)))
   {
      TORNA_LOG(MESHAP_LOGGER_TYPE_FLOW, "meshap_tddi: _meshap_tddi_lock_reboot_request copy_to_user(buf,&response_packet,sizeof(tddi_packet_t)) failed\n");
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : Bad address :  copy to user failed %s : %d\n",__func__,__LINE__);
      return -EFAULT;
   }

   if (copy_to_user(buf + sizeof(tddi_packet_t), &response_data, sizeof(tddi_lock_reboot_response_t)))
   {
      TORNA_LOG(MESHAP_LOGGER_TYPE_FLOW, "meshap_tddi: _meshap_tddi_lock_reboot_request copy_to_user(buf + sizeof(tddi_packet_t),&response_data,sizeof(tddi_configure_mobility_response_t)) failed\n");
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : Bad address :  copy to user failed %s : %d\n",__func__,__LINE__);
      return -EFAULT;
   }

   *ppos += sizeof(tddi_packet_t) + sizeof(tddi_lock_reboot_response_t);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s: %d\n",__func__,__LINE__);
   return sizeof(tddi_packet_t) + sizeof(tddi_configure_mobility_response_t);
}


static inline ssize_t _meshap_tddi_set_reboot_flag(_file_data_t *fd, tddi_packet_t *packet, unsigned char *data, char *buf, size_t count, loff_t *ppos)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s: %d\n",__func__,__LINE__);
   tddi_packet_t response_packet;
   tddi_set_reboot_flag_response_t response_data;
   tddi_set_reboot_flag_request_t  *request_data;

   if (count < sizeof(tddi_packet_t) + sizeof(response_data))
   {
      TORNA_LOG(MESHAP_LOGGER_TYPE_FLOW, "meshap_tddi: _meshap_tddi_set_reboot_flag count < sizeof(tddi_packet_t) + sizeof(response_data)\n");
       al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : out of memory %s : %d\n",__func__,__LINE__);
      return -ENOMEM;
   }

   response_packet.signature = TDDI_PACKET_SIGNATURE;
   response_packet.version   = TDDI_CURRENT_VERSION;
   response_packet.type      = TDDI_PACKET_TYPE_RESPONSE;
   response_packet.sub_type  = TDDI_RESPONSE_TYPE_SET_REBOOT_FLAG;
   response_packet.data_size = sizeof(tddi_set_reboot_flag_response_t);

   request_data           = (tddi_set_reboot_flag_request_t *)data;
   response_data.response = mesh_ext_set_reboot_flag(request_data->flag);

   if (copy_to_user(buf, &response_packet, sizeof(tddi_packet_t)))
   {
      TORNA_LOG(MESHAP_LOGGER_TYPE_FLOW, "meshap_tddi: _meshap_tddi_set_reboot_flag copy_to_user(buf,&response_packet,sizeof(tddi_packet_t)) failed\n");
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : Bad address :  copy to user failed %s : %d\n",__func__,__LINE__);
      return -EFAULT;
   }

   if (copy_to_user(buf + sizeof(tddi_packet_t), &response_data, sizeof(tddi_set_reboot_flag_response_t)))
   {
      TORNA_LOG(MESHAP_LOGGER_TYPE_FLOW, "meshap_tddi: _meshap_tddi_set_reboot_flag copy_to_user(buf + sizeof(tddi_packet_t),&response_data,sizeof(tddi_set_reboot_flag_response_t)) failed\n");
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : Bad address :  copy to user failed %s : %d\n",__func__,__LINE__);
      return -EFAULT;
   }

   *ppos += sizeof(tddi_packet_t) + sizeof(tddi_set_reboot_flag_response_t);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s: %d\n",__func__,__LINE__);
   return sizeof(tddi_packet_t) + sizeof(tddi_set_reboot_flag_response_t);
}


static inline ssize_t _meshap_tddi_enable_reset_gen(_file_data_t *fd, tddi_packet_t *packet, unsigned char *data, char *buf, size_t count, loff_t *ppos)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s: %d\n",__func__,__LINE__);
   tddi_packet_t response_packet;
   tddi_enable_reset_gen_response_t response_data;
   tddi_enable_reset_gen_request_t  *request_data;

   if (count < sizeof(tddi_packet_t) + sizeof(response_data))
   {
      TORNA_LOG(MESHAP_LOGGER_TYPE_FLOW, "meshap_tddi: _meshap_tddi_set_reboot_flag count < sizeof(tddi_packet_t) + sizeof(response_data)\n");
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : out of memory %s : %d\n",__func__,__LINE__);
      return -ENOMEM;
   }

   response_packet.signature = TDDI_PACKET_SIGNATURE;
   response_packet.version   = TDDI_CURRENT_VERSION;
   response_packet.type      = TDDI_PACKET_TYPE_RESPONSE;
   response_packet.sub_type  = TDDI_RESPONSE_TYPE_ENABLE_RESET_GEN;
   response_packet.data_size = sizeof(tddi_enable_reset_gen_response_t);

   request_data         = (tddi_enable_reset_gen_request_t *)data;
   response_data.ignore = 0;

   meshap_enable_reset_generator(request_data->flag, request_data->interval);

   if (copy_to_user(buf, &response_packet, sizeof(tddi_packet_t)))
   {
      TORNA_LOG(MESHAP_LOGGER_TYPE_FLOW, "meshap_tddi: _meshap_tddi_enable_reset_gen copy_to_user(buf,&response_packet,sizeof(tddi_packet_t)) failed\n");
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : Bad address :  copy to user failed %s : %d\n",__func__,__LINE__);
      return -EFAULT;
   }

   if (copy_to_user(buf + sizeof(tddi_packet_t), &response_data, sizeof(tddi_enable_reset_gen_response_t)))
   {
      TORNA_LOG(MESHAP_LOGGER_TYPE_FLOW, "meshap_tddi: _meshap_tddi_enable_reset_gen copy_to_user(buf + sizeof(tddi_packet_t),&response_data,sizeof(tddi_enable_reset_gen_response_t)) failed\n");
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : Bad address :  copy to user failed %s : %d\n",__func__,__LINE__);
      return -EFAULT;
   }

   *ppos += sizeof(tddi_packet_t) + sizeof(tddi_enable_reset_gen_response_t);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s: %d\n",__func__,__LINE__);
   return sizeof(tddi_packet_t) + sizeof(tddi_enable_reset_gen_response_t);
}


static inline ssize_t _meshap_tddi_full_disconnect(_file_data_t *fd, tddi_packet_t *packet, unsigned char *data, char *buf, size_t count, loff_t *ppos)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s: %d\n",__func__,__LINE__);
   tddi_packet_t response_packet;

   if (count < sizeof(tddi_packet_t))
   {
      TORNA_LOG(MESHAP_LOGGER_TYPE_FLOW, "meshap_tddi: _meshap_tddi_block_cpu count < sizeof(tddi_packet_t) + sizeof(response_data)\n");
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : out of memory %s : %d\n",__func__,__LINE__);
      return -ENOMEM;
   }

   response_packet.signature = TDDI_PACKET_SIGNATURE;
   response_packet.version   = TDDI_CURRENT_VERSION;
   response_packet.type      = TDDI_PACKET_TYPE_RESPONSE;
   response_packet.sub_type  = TDDI_RESPONSE_TYPE_FULL_DISCONNECT;
   response_packet.data_size = 0;

   if (meshap_core_globals.state != MESHAP_CORE_GLOBALS_STATE_STARTED)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : invalid argument %s : %d\n",__func__,__LINE__);
      return -EINVAL;
   }
   else
   {
      mesh_ext_full_disconnect();
   }

   if (copy_to_user(buf, &response_packet, sizeof(tddi_packet_t)))
   {
      TORNA_LOG(MESHAP_LOGGER_TYPE_FLOW, "meshap_tddi: _meshap_tddi_full_disconnect copy_to_user(buf,&response_packet,sizeof(tddi_packet_t)) failed\n");
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : Bad address :  copy to user failed %s : %d\n",__func__,__LINE__);
      return -EFAULT;
   }

   *ppos += sizeof(tddi_packet_t);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s: %d\n",__func__,__LINE__);

   return sizeof(tddi_packet_t);
}


static inline ssize_t _meshap_tddi_dhcp_info(_file_data_t *fd, tddi_packet_t *packet, unsigned char *data, char *buf, size_t count, loff_t *ppos)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s: %d\n",__func__,__LINE__);
   tddi_packet_t        response_packet;
   tddi_dhcp_response_t response_data;

   if (count < sizeof(tddi_packet_t) + sizeof(tddi_dhcp_response_t))
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : out of memory %s : %d\n",__func__,__LINE__);
      return -ENOMEM;
   }

   response_packet.signature = TDDI_PACKET_SIGNATURE;
   response_packet.version   = TDDI_CURRENT_VERSION;
   response_packet.type      = TDDI_PACKET_TYPE_RESPONSE;
   response_packet.sub_type  = TDDI_RESPONSE_TYPE_DHCP_INFO;

   response_packet.data_size = sizeof(tddi_dhcp_response_t);

   response_data.dhcp_enabled = 0;
   memset(response_data.self_dhcp_ip_address, 0, 4);

   if (copy_to_user(buf, &response_packet, sizeof(tddi_packet_t)))
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : Bad address :  copy to user failed %s : %d\n",__func__,__LINE__);
      return -EFAULT;
   }

   if (copy_to_user(buf + sizeof(tddi_packet_t), &response_data, sizeof(tddi_dhcp_response_t)))
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : Bad address :  copy to user failed %s : %d\n",__func__,__LINE__);
      return -EFAULT;
   }

   *ppos += sizeof(tddi_packet_t) + sizeof(tddi_dhcp_response_t);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s: %d\n",__func__,__LINE__);
   return sizeof(tddi_packet_t) + sizeof(tddi_dhcp_response_t);
}


static inline ssize_t _meshap_tddi_gpio(_file_data_t *fd, tddi_packet_t *packet, unsigned char *data, char *buf, size_t count, loff_t *ppos)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s: %d\n",__func__,__LINE__);
   tddi_packet_t        response_packet;
   tddi_gpio_response_t response_data;
   tddi_gpio_request_t  *request_data;

   if (count < sizeof(tddi_packet_t) + sizeof(response_data))
   {
      TORNA_LOG(MESHAP_LOGGER_TYPE_FLOW, "meshap_tddi: _meshap_tddi_preferred_parent count < sizeof(tddi_packet_t) + sizeof(response_data)\n");
       al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : out of memory %s : %d\n",__func__,__LINE__);
      return -ENOMEM;
   }

   response_packet.signature = TDDI_PACKET_SIGNATURE;
   response_packet.version   = TDDI_CURRENT_VERSION;
   response_packet.type      = TDDI_PACKET_TYPE_RESPONSE;
   response_packet.sub_type  = TDDI_RESPONSE_TYPE_GPIO_RESPONSE;
   response_packet.data_size = sizeof(tddi_gpio_response_t);

   request_data = (tddi_gpio_request_t *)data;

   switch (request_data->read_write)
   {
   case TDDI_GPIO_REQUEST_READ:
      response_data.response = TDDI_GPIO_RESPONSE_READ_SUCCESS;
      response_data.value    = meshap_get_gpio(request_data->gpio);
      break;

   case TDDI_GPIO_REQUEST_WRITE:
      response_data.response = TDDI_GPIO_RESPONSE_WRITE_SUCCESS;
      response_data.value    = request_data->value;
      meshap_set_gpio(request_data->gpio, request_data->value);
      break;

   default:
      response_data.response = TDDI_GPIO_RESPONSE_FAILURE;
   }

   if (copy_to_user(buf, &response_packet, sizeof(tddi_packet_t)))
   {
      TORNA_LOG(MESHAP_LOGGER_TYPE_FLOW, "meshap_tddi: _meshap_tddi_preferred_parent copy_to_user(buf,&response_packet,sizeof(tddi_packet_t)) failed\n");
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : Bad address :  copy to user failed %s : %d\n",__func__,__LINE__);
      return -EFAULT;
   }

   if (copy_to_user(buf + sizeof(tddi_packet_t), &response_data, sizeof(tddi_gpio_response_t)))
   {
      TORNA_LOG(MESHAP_LOGGER_TYPE_FLOW, "meshap_tddi: _meshap_tddi_preferred_parent copy_to_user(buf + sizeof(tddi_packet_t),&response_data,sizeof(tddi_preferred_parent_response_t)) failed\n");
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : Bad address :  copy to user failed %s : %d\n",__func__,__LINE__);
      return -EFAULT;
   }

   *ppos += sizeof(tddi_packet_t) + sizeof(tddi_gpio_response_t);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s: %d\n",__func__,__LINE__);
   return sizeof(tddi_packet_t) + sizeof(tddi_gpio_response_t);
}


static inline ssize_t _meshap_tddi_gps_info(_file_data_t *fd, tddi_packet_t *packet, unsigned char *data, char *buf, size_t count, loff_t *ppos)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s: %d\n",__func__,__LINE__);
   tddi_packet_t   response_packet;
   tddi_gps_info_t *request_data;

   if (count < sizeof(tddi_packet_t))
   {
      TORNA_LOG(MESHAP_LOGGER_TYPE_FLOW, "meshap_tddi: _meshap_tddi_gps_info count < sizeof(tddi_packet_t) + sizeof(response_data)\n");
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : out of memory %s : %d\n",__func__,__LINE__);
      return -ENOMEM;
   }

   response_packet.signature = TDDI_PACKET_SIGNATURE;
   response_packet.version   = TDDI_CURRENT_VERSION;
   response_packet.type      = TDDI_PACKET_TYPE_RESPONSE;
   response_packet.sub_type  = TDDI_RESPONSE_TYPE_GPS_RESPONSE;
   response_packet.data_size = 0;

   request_data = (tddi_gps_info_t *)data;

   meshap_set_gps_info(request_data->longitude,
                       request_data->latitude,
                       request_data->altitude,
                       request_data->speed);


   if (copy_to_user(buf, &response_packet, sizeof(tddi_packet_t)))
   {
      TORNA_LOG(MESHAP_LOGGER_TYPE_FLOW, "meshap_tddi: _meshap_tddi_gps_info copy_to_user(buf,&response_packet,sizeof(tddi_packet_t)) failed\n");
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : Bad address :  copy to user failed %s : %d\n",__func__,__LINE__);
      return -EFAULT;
   }

   *ppos += sizeof(tddi_packet_t);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s: %d\n",__func__,__LINE__);
   return sizeof(tddi_packet_t);
}


static inline ssize_t _meshap_tddi_virtual_configure_request(_file_data_t *fd, tddi_packet_t *packet, unsigned char *data, char *buf, size_t count, loff_t *ppos)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s: %d\n",__func__,__LINE__);
   tddi_packet_t response_packet;
   tddi_virtual_configure_response_t response_data;
   tddi_virtual_configure_request_t  *request_data;
   char                 *virtual_config_string;
   int                  i;
   int                  virtual_if_count;
   _virtual_if_info_t   virtual_if_info;
   meshap_core_net_if_t *core_net_if;
   int                  virtual_conf_token;

   virtual_conf_token = 0;

   if (count < sizeof(tddi_packet_t) + sizeof(response_data))
   {
      TORNA_LOG(MESHAP_LOGGER_TYPE_FLOW, "meshap_tddi: _meshap_tddi_virtual_configure_request count < sizeof(tddi_packet_t) + sizeof(response_data)\n");
       al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : out of memory %s : %d\n",__func__,__LINE__);
      return -ENOMEM;
   }

   response_packet.signature = TDDI_PACKET_SIGNATURE;
   response_packet.version   = TDDI_CURRENT_VERSION;
   response_packet.type      = TDDI_PACKET_TYPE_RESPONSE;
   response_packet.sub_type  = TDDI_RESPONSE_TYPE_VIRTUAL_CONFIGURE;
   response_packet.data_size = sizeof(tddi_virtual_configure_response_t);

   if (meshap_core_globals.state == MESHAP_CORE_GLOBALS_STATE_STARTED)
   {
      response_data.response = TDDI_VIRTUAL_CONFIGURE_RESPONSE_BUSY;
   }
   else
   {
      response_data.response = TDDI_VIRTUAL_CONFIGURE_RESPONSE_SUCCESS;
   }

   if (response_data.response == TDDI_VIRTUAL_CONFIGURE_RESPONSE_SUCCESS)
   {
      if (meshap_core_globals.v_if_info_conf_string != NULL)
      {
         kfree(meshap_core_globals.v_if_info_conf_string);
      }

      request_data = (tddi_virtual_configure_request_t *)data;

      meshap_core_globals.v_if_info_conf_string = (char *)kmalloc(request_data->virtual_config_string_length + 1, GFP_KERNEL);
      memset(meshap_core_globals.v_if_info_conf_string, 0, request_data->virtual_config_string_length + 1);

      virtual_config_string = data + sizeof(tddi_virtual_configure_request_t);

      memcpy(meshap_core_globals.v_if_info_conf_string, virtual_config_string, request_data->virtual_config_string_length);

      virtual_conf_token = virt_if_info_conf_parse(meshap_core_globals.v_if_info_conf_string);

      if (virtual_conf_token == 0)
      {
         kfree(meshap_core_globals.v_if_info_conf_string);
         meshap_core_globals.v_if_info_conf_string = NULL;
         TORNA_LOG(MESHAP_LOGGER_TYPE_FLOW, "meshap_tddi: _meshap_tddi_virtual_configure_request meshap_tddi_globals.virtual_conf_token == 0\n");
      	 al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : virtual_conf_parse returned 0 %s : %d\n",__func__,__LINE__);
      	 al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"MESH_AP : INFO : meshap_tddi: virtual_conf_parse returned 0 %s : %d\n", __func__,__LINE__);        
         return -EINVAL;
      }
   }

   if (copy_to_user(buf, &response_packet, sizeof(tddi_packet_t)))
   {
      TORNA_LOG(MESHAP_LOGGER_TYPE_FLOW, "meshap_tddi: _meshap_tddi_virtual_configure_request copy_to_user(buf,&response_packet,sizeof(tddi_packet_t)) failed\n");
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : Bad address :  copy to user failed %s : %d\n",__func__,__LINE__);
      return -EFAULT;
   }

   if (copy_to_user(buf + sizeof(tddi_packet_t), &response_data, sizeof(tddi_virtual_configure_response_t)))
   {
      TORNA_LOG(MESHAP_LOGGER_TYPE_FLOW, "meshap_tddi: _meshap_tddi_virtual_configure_request copy_to_user(buf + sizeof(tddi_packet_t),&response_data,sizeof(tddi_virtual_configure_response_t)) failed\n");
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : Bad address :  copy to user failed %s : %d\n",__func__,__LINE__);
      return -EFAULT;
   }

   virtual_if_count = virt_if_info_conf_get_count(virtual_conf_token);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"MESH_AP : INFO : meshap_tddi: Accepted virtual configuration with following settings: virtual_if_count=%d : %s : %d\n",virtual_if_count, __func__,__LINE__);        

   for (i = 0; i < virtual_if_count; i++)
   {
      virt_if_info_conf_get_if(virtual_conf_token, i, &virtual_if_info);

      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"MESH_AP : INFO : meshap_tddi: %d IF_NAME=%s PHY_RX=%s PHY_TX=%s : %s : %d\n",i + 1,
             virtual_if_info.name,
             virtual_if_info.map_rx,
             virtual_if_info.map_tx,__func__,__LINE__);


      core_net_if = meshap_core_lookup_net_if(virtual_if_info.name);

      if (core_net_if == NULL)
      {
         meshap_core_create_virtual_net_if(virtual_if_info.name, virtual_if_info.map_rx);
      }
   }

   virt_if_info_conf_close(virtual_conf_token);

   *ppos += sizeof(tddi_packet_t) + sizeof(tddi_virtual_configure_response_t);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s: %d\n",__func__,__LINE__);
   return sizeof(tddi_packet_t) + sizeof(tddi_virtual_configure_response_t);
}


static inline ssize_t _meshap_tddi_configure_sip_request(_file_data_t *fd, tddi_packet_t *packet, unsigned char *data, char *buf, size_t count, loff_t *ppos)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s: %d\n",__func__,__LINE__);
   tddi_packet_t                 response_packet;
   tddi_configure_sip_response_t response_data;
   tddi_configure_sip_request_t  *request_data;
   char              *sip_config_string;
   sip_conf_handle_t sip_conf_token;

   sip_conf_token = NULL;

   if (count < sizeof(tddi_packet_t) + sizeof(response_data))
   {
      TORNA_LOG(MESHAP_LOGGER_TYPE_FLOW, "meshap_tddi: _meshap_tddi_configure_sip_request count < sizeof(tddi_packet_t) + sizeof(response_data)\n");
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : out of memory %s : %d\n",__func__,__LINE__);
      return -ENOMEM;
   }

   response_packet.signature = TDDI_PACKET_SIGNATURE;
   response_packet.version   = TDDI_CURRENT_VERSION;
   response_packet.type      = TDDI_PACKET_TYPE_RESPONSE;
   response_packet.sub_type  = TDDI_RESPONSE_TYPE_CONFIGURE_SIP;
   response_packet.data_size = sizeof(tddi_configure_sip_response_t);

   if (meshap_core_globals.state == MESHAP_CORE_GLOBALS_STATE_STARTED)
   {
      response_data.response = TDDI_CONFIGURE_SIP_RESPONSE_BUSY;
   }
   else
   {
      response_data.response = TDDI_CONFIGURE_SIP_RESPONSE_SUCCESS;
   }

   if ((response_data.response == TDDI_CONFIGURE_SIP_RESPONSE_SUCCESS) || (TDDI_CONFIGURE_SIP_RESPONSE_BUSY))
   {
      if (meshap_core_globals.sip_config_string != NULL)
      {
         kfree(meshap_core_globals.sip_config_string);
      }

      request_data = (tddi_configure_sip_request_t *)data;

      meshap_core_globals.sip_config_string = (char *)kmalloc(request_data->config_string_length + 1, GFP_KERNEL);
      memset(meshap_core_globals.sip_config_string, 0, request_data->config_string_length + 1);

      sip_config_string = data + sizeof(tddi_configure_sip_request_t);

      memcpy(meshap_core_globals.sip_config_string, sip_config_string, request_data->config_string_length);

      sip_conf_token = sip_conf_parse(AL_CONTEXT meshap_core_globals.sip_config_string);

      if (sip_conf_token == NULL)
      {
         kfree(meshap_core_globals.sip_config_string);
         meshap_core_globals.sip_config_string = NULL;
         TORNA_LOG(MESHAP_LOGGER_TYPE_FLOW, "meshap_tddi: _meshap_tddi_configure_sip_request meshap_tddi_globals.sip_conf_token == 0\n");
      	 al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : sip_conf_token is Null %s : %d\n",__func__,__LINE__);
         return -EINVAL;
      }
   }

   if (copy_to_user(buf, &response_packet, sizeof(tddi_packet_t)))
   {
      TORNA_LOG(MESHAP_LOGGER_TYPE_FLOW, "meshap_tddi: _meshap_tddi_configure_sip_request copy_to_user(buf,&response_packet,sizeof(tddi_packet_t)) failed\n");
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : Bad address :  copy to user failed %s : %d\n",__func__,__LINE__);
      return -EFAULT;
   }

   if (copy_to_user(buf + sizeof(tddi_packet_t), &response_data, sizeof(tddi_configure_sip_response_t)))
   {
      TORNA_LOG(MESHAP_LOGGER_TYPE_FLOW, "meshap_tddi: _meshap_tddi_configure_sip_request copy_to_user(buf + sizeof(tddi_packet_t),&response_data,sizeof(tddi_configure_sip_response_t)) failed\n");
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : Bad address :  copy to user failed %s : %d\n",__func__,__LINE__);
      return -EFAULT;
   }

   sip_conf_close(AL_CONTEXT sip_conf_token);
   sip_conf_token = 0;

   *ppos += sizeof(tddi_packet_t) + sizeof(tddi_configure_sip_response_t);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s: %d\n",__func__,__LINE__);
   return sizeof(tddi_packet_t) + sizeof(tddi_configure_sip_response_t);
}

//SPAWAR
static inline ssize_t _meshap_tddi_query_board_stats(_file_data_t *fd, tddi_packet_t *packet, unsigned char *data, char *buf, size_t count, loff_t *ppos)
{
//   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s: %d\n",__func__,__LINE__);
   tddi_packet_t         response_packet;
   tddi_board_stats_t    response_data;
   tddi_board_stats_t    *request_data;

   if (count < sizeof(tddi_packet_t) + sizeof(response_data))
   {
      TORNA_LOG(MESHAP_LOGGER_TYPE_FLOW, "meshap_tddi: _meshap_tddi_query_board_stats count < sizeof(tddi_packet_t) + sizeof(response_data)\n");
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : out of memory %s : %d\n",__func__,__LINE__);
      return -ENOMEM;
   }
   response_packet.signature = TDDI_PACKET_SIGNATURE;
   response_packet.version   = TDDI_CURRENT_VERSION;
   response_packet.type      = TDDI_PACKET_TYPE_RESPONSE;
   response_packet.sub_type  = TDDI_REQUEST_TYPE_QUERY_BOARD_STATS;
   response_packet.data_size = sizeof(tddi_board_stats_t);

   request_data = (tddi_board_stats_t *)data;

   meshap_set_board_stats(request_data);
//   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s: %d\n",__func__,__LINE__);

   return sizeof(tddi_packet_t) + sizeof(tddi_board_stats_t);
}
//SPAWAR_END

static inline ssize_t _meshap_tddi_eth_link_status(_file_data_t *fd, tddi_packet_t *packet, unsigned char *data, char *buf, size_t count, loff_t *ppos)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s: %d\n",__func__,__LINE__);
   tddi_packet_t         response_packet;
   tddi_eth_link_state_t response_data;
   tddi_eth_link_state_t *request_data;
   al_net_if_t           *net_if;
   int flags = 0;

   if (count < sizeof(tddi_packet_t) + sizeof(response_data))
   {
      TORNA_LOG(MESHAP_LOGGER_TYPE_FLOW, "meshap_tddi: tddi_eth_link_state_t count < sizeof(tddi_packet_t) + sizeof(response_data)\n");
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : out of memory %s : %d\n",__func__,__LINE__);
      return -ENOMEM;
   }

   response_packet.signature = TDDI_PACKET_SIGNATURE;
   response_packet.version   = TDDI_CURRENT_VERSION;
   response_packet.type      = TDDI_PACKET_TYPE_RESPONSE;
   response_packet.sub_type  = TDDI_REQUEST_TYPE_ETH_LINK_FLAG;
   response_packet.data_size = sizeof(tddi_eth_link_state_t);

   request_data = (tddi_eth_link_state_t *)data;
   /*Update this value in some global and take decision about ethernet link state*/
   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"MESH_AP : INFO : meshap_tddi: Flags = %d : %s : %d\n",request_data->link_state, __func__,__LINE__);        

#if 1
   if (eth1_ping_status != request_data->link_state)
   {
      eth1_ping_status = request_data->link_state;

      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"MESH_AP : INFO : %s : %d\n", __func__,__LINE__);        
      net_if = meshap_core_lookup_net_if("eth1");

      if (net_if != NULL)
      {
         flags = net_if->get_flags(net_if);

         if ((flags & AL_NET_IF_FLAGS_LINKED) && (func_mode == 3) && (mesh_config->failover_enabled == 2))
         {
            if (eth1_ping_status == 1)
            {
               al_on_phy_link_notify(net_if, 1);
            }
            else
            {
               al_on_phy_link_notify(net_if, 0);
            }
         }
      }
   }
#endif
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s: %d\n",__func__,__LINE__);
   return sizeof(tddi_packet_t) + sizeof(tddi_eth_link_state_t);
}


#ifdef _MESHAP_AL_TEST_SUITE_

static inline ssize_t _meshap_al_test_1_1_request(_file_data_t *fd, tddi_packet_t *packet, unsigned char *data, char *buf, size_t count, loff_t *ppos)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s: %d\n",__func__,__LINE__);
   tddi_packet_t        response_packet;
   tddi_test_response_t response_data;

   if (count < sizeof(tddi_packet_t) + sizeof(tddi_test_response_t))
   {
      TORNA_LOG(MESHAP_LOGGER_TYPE_FLOW, "meshap_tddi: _meshap_al_test_1_1_request count < sizeof(tddi_packet_t) + sizeof(tddi_test_response_t)\n");
      return -ENOMEM;
   }

   response_packet.signature = TDDI_PACKET_SIGNATURE;
   response_packet.version   = TDDI_CURRENT_VERSION;
   response_packet.type      = TDDI_PACKET_TYPE_RESPONSE;
   response_packet.sub_type  = TDDI_RESPONSE_TYPE_TEST;
   response_packet.data_size = sizeof(tddi_test_response_t);

   if (meshap_tddi_globals.test_1_token != (unsigned int)NULL)
   {
      TORNA_LOG(MESHAP_LOGGER_TYPE_FLOW, "meshap_tddi: _meshap_al_test_1_1_request meshap_tddi_globals.test_1_token != NULL\n");
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : Invalid token %s : %d\n",__func__,__LINE__);
      return -EINVAL;
   }

   if (copy_to_user(buf, &response_packet, sizeof(tddi_packet_t)))
   {
      TORNA_LOG(MESHAP_LOGGER_TYPE_FLOW, "meshap_tddi: _meshap_al_test_1_1_request copy_to_user(buf,&response_packet,sizeof(tddi_packet_t)) failed\n");
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : Bad address :  copy to user failed %s : %d\n",__func__,__LINE__);
      return -EFAULT;
   }

   if (copy_to_user(buf + sizeof(tddi_packet_t), &response_data, sizeof(tddi_test_response_t)))
   {
      TORNA_LOG(MESHAP_LOGGER_TYPE_FLOW, "meshap_tddi: _meshap_al_test_1_1_request copy_to_user(buf + sizeof(tddi_packet_t),&response_data,sizeof(tddi_test_response_t)) failed\n");
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : Bad address :  copy to user failed %s : %d\n",__func__,__LINE__);
      return -EFAULT;
   }

   meshap_tddi_globals.test_1_token = meshap_al_test_1_1();

   *ppos += sizeof(tddi_packet_t) + sizeof(tddi_test_response_t);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s: %d\n",__func__,__LINE__);
   return sizeof(tddi_packet_t) + sizeof(tddi_test_response_t);
}


static inline ssize_t _meshap_al_test_1_2_request(_file_data_t *fd, tddi_packet_t *packet, unsigned char *data, char *buf, size_t count, loff_t *ppos)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s: %d\n",__func__,__LINE__);
   tddi_packet_t        response_packet;
   tddi_test_response_t response_data;

   if (count < sizeof(tddi_packet_t) + sizeof(tddi_test_response_t))
   {
      TORNA_LOG(MESHAP_LOGGER_TYPE_FLOW, "meshap_tddi: _meshap_al_test_1_2_request count < sizeof(tddi_packet_t) + sizeof(tddi_test_response_t)\n");
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : out of memory %s : %d\n",__func__,__LINE__);
      return -ENOMEM;
   }

   response_packet.signature = TDDI_PACKET_SIGNATURE;
   response_packet.version   = TDDI_CURRENT_VERSION;
   response_packet.type      = TDDI_PACKET_TYPE_RESPONSE;
   response_packet.sub_type  = TDDI_RESPONSE_TYPE_TEST;
   response_packet.data_size = sizeof(tddi_test_response_t);

   if (meshap_tddi_globals.test_1_token == (unsigned int)NULL)
   {
      TORNA_LOG(MESHAP_LOGGER_TYPE_FLOW, "meshap_tddi: _meshap_al_test_1_2_request meshap_tddi_globals.test_1_token == NULL\n");
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : Invalid token found %s : %d\n",__func__,__LINE__);
      return -EINVAL;
   }

   if (copy_to_user(buf, &response_packet, sizeof(tddi_packet_t)))
   {
      TORNA_LOG(MESHAP_LOGGER_TYPE_FLOW, "meshap_tddi: _meshap_al_test_1_2_request copy_to_user(buf,&response_packet,sizeof(tddi_packet_t))\n");
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : Bad address :  copy to user failed %s : %d\n",__func__,__LINE__);
      return -EFAULT;
   }

   if (copy_to_user(buf + sizeof(tddi_packet_t), &response_data, sizeof(tddi_test_response_t)))
   {
      TORNA_LOG(MESHAP_LOGGER_TYPE_FLOW, "meshap_tddi: _meshap_al_test_1_2_request copy_to_user(buf + sizeof(tddi_packet_t),&response_data,sizeof(tddi_test_response_t))\n");
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : Bad address :  copy to user failed %s : %d\n",__func__,__LINE__);
      return -EFAULT;
   }

   meshap_al_test_1_2(meshap_tddi_globals.test_1_token);

   meshap_tddi_globals.test_1_token = (unsigned int)NULL;

   *ppos += sizeof(tddi_packet_t) + sizeof(tddi_test_response_t);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s: %d\n",__func__,__LINE__);
   return sizeof(tddi_packet_t) + sizeof(tddi_test_response_t);
}


static inline ssize_t _meshap_al_test_2_1_request(_file_data_t *fd, tddi_packet_t *packet, unsigned char *data, char *buf, size_t count, loff_t *ppos)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s: %d\n",__func__,__LINE__);
   tddi_packet_t        response_packet;
   tddi_test_response_t response_data;

   if (count < sizeof(tddi_packet_t) + sizeof(tddi_test_response_t))
   {
      TORNA_LOG(MESHAP_LOGGER_TYPE_FLOW, "meshap_tddi: _meshap_al_test_2_1_request count < sizeof(tddi_packet_t) + sizeof(tddi_test_response_t)\n");
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : out of memory %s : %d\n",__func__,__LINE__);
      return -ENOMEM;
   }

   response_packet.signature = TDDI_PACKET_SIGNATURE;
   response_packet.version   = TDDI_CURRENT_VERSION;
   response_packet.type      = TDDI_PACKET_TYPE_RESPONSE;
   response_packet.sub_type  = TDDI_RESPONSE_TYPE_TEST;
   response_packet.data_size = sizeof(tddi_test_response_t);

   if (meshap_tddi_globals.test_2_token != (unsigned int)NULL)
   {
      TORNA_LOG(MESHAP_LOGGER_TYPE_FLOW, "meshap_tddi: _meshap_al_test_2_1_request meshap_tddi_globals.test_2_token != NULL\n");
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : Invalid token found %s : %d\n",__func__,__LINE__);
      return -EINVAL;
   }

   if (copy_to_user(buf, &response_packet, sizeof(tddi_packet_t)))
   {
      TORNA_LOG(MESHAP_LOGGER_TYPE_FLOW, "meshap_tddi: _meshap_al_test_2_1_request copy_to_user(buf,&response_packet,sizeof(tddi_packet_t)) failed\n");
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : Bad address :  copy to user failed %s : %d\n",__func__,__LINE__);
      return -EFAULT;
   }

   if (copy_to_user(buf + sizeof(tddi_packet_t), &response_data, sizeof(tddi_test_response_t)))
   {
      TORNA_LOG(MESHAP_LOGGER_TYPE_FLOW, "meshap_tddi: _meshap_al_test_2_1_request copy_to_user(buf + sizeof(tddi_packet_t),&response_data,sizeof(tddi_test_response_t)) failed\n");
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : Bad address :  copy to user failed %s : %d\n",__func__,__LINE__);
      return -EFAULT;
   }

   meshap_tddi_globals.test_2_token = meshap_al_test_2_1();
   meshap_tddi_globals.test_2_count = 0;

   *ppos += sizeof(tddi_packet_t) + sizeof(tddi_test_response_t);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s: %d\n",__func__,__LINE__);
   return sizeof(tddi_packet_t) + sizeof(tddi_test_response_t);
}


static inline ssize_t _meshap_al_test_2_2_request(_file_data_t *fd, tddi_packet_t *packet, unsigned char *data, char *buf, size_t count, loff_t *ppos)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s: %d\n",__func__,__LINE__);
   tddi_packet_t        response_packet;
   tddi_test_response_t response_data;

   if (count < sizeof(tddi_packet_t) + sizeof(tddi_test_response_t))
   {
      TORNA_LOG(MESHAP_LOGGER_TYPE_FLOW, "meshap_tddi: _meshap_al_test_2_2_request count < sizeof(tddi_packet_t) + sizeof(tddi_test_response_t)\n");
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : out of memory %s : %d\n",__func__,__LINE__);
      return -ENOMEM;
   }

   response_packet.signature = TDDI_PACKET_SIGNATURE;
   response_packet.version   = TDDI_CURRENT_VERSION;
   response_packet.type      = TDDI_PACKET_TYPE_RESPONSE;
   response_packet.sub_type  = TDDI_RESPONSE_TYPE_TEST;
   response_packet.data_size = sizeof(tddi_test_response_t);

   if (meshap_tddi_globals.test_2_token == (unsigned int)NULL)
   {
      TORNA_LOG(MESHAP_LOGGER_TYPE_FLOW, "meshap_tddi: _meshap_al_test_2_2_request meshap_tddi_globals.test_2_token == NULL\n");
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : Invalid token found %s : %d\n",__func__,__LINE__);
      return -EINVAL;
   }

   if (copy_to_user(buf, &response_packet, sizeof(tddi_packet_t)))
   {
      TORNA_LOG(MESHAP_LOGGER_TYPE_FLOW, "meshap_tddi: _meshap_al_test_2_2_request copy_to_user(buf,&response_packet,sizeof(tddi_packet_t))\n");
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : Bad address :  copy to user failed %s : %d\n",__func__,__LINE__);
      return -EFAULT;
   }

   if (copy_to_user(buf + sizeof(tddi_packet_t), &response_data, sizeof(tddi_test_response_t)))
   {
      TORNA_LOG(MESHAP_LOGGER_TYPE_FLOW, "meshap_tddi: _meshap_al_test_2_2_request copy_to_user(buf + sizeof(tddi_packet_t),&response_data,sizeof(tddi_test_response_t))\n");
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : Bad address :  copy to user failed %s : %d\n",__func__,__LINE__);
      return -EFAULT;
   }

   meshap_al_test_2_2(meshap_tddi_globals.test_2_token);

   if (meshap_tddi_globals.test_2_count == 1)
   {
      meshap_tddi_globals.test_2_token = (unsigned int)NULL;
   }
   else
   {
      ++meshap_tddi_globals.test_2_count;
   }

   *ppos += sizeof(tddi_packet_t) + sizeof(tddi_test_response_t);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s: %d\n",__func__,__LINE__);
   return sizeof(tddi_packet_t) + sizeof(tddi_test_response_t);
}


static inline ssize_t _meshap_al_test_3_1_request(_file_data_t *fd, tddi_packet_t *packet, unsigned char *data, char *buf, size_t count, loff_t *ppos)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s: %d\n",__func__,__LINE__);
   tddi_packet_t        response_packet;
   tddi_test_response_t response_data;

   if (count < sizeof(tddi_packet_t) + sizeof(tddi_test_response_t))
   {
      TORNA_LOG(MESHAP_LOGGER_TYPE_FLOW, "meshap_tddi: _meshap_al_test_3_1_request count < sizeof(tddi_packet_t) + sizeof(tddi_test_response_t)\n");
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : out of memory %s : %d\n",__func__,__LINE__);
      return -ENOMEM;
   }

   response_packet.signature = TDDI_PACKET_SIGNATURE;
   response_packet.version   = TDDI_CURRENT_VERSION;
   response_packet.type      = TDDI_PACKET_TYPE_RESPONSE;
   response_packet.sub_type  = TDDI_RESPONSE_TYPE_TEST;
   response_packet.data_size = sizeof(tddi_test_response_t);

   if (meshap_tddi_globals.test_3_token != (unsigned int)NULL)
   {
      TORNA_LOG(MESHAP_LOGGER_TYPE_FLOW, "meshap_tddi: _meshap_al_test_3_1_request meshap_tddi_globals.test_3_token != NULL\n");
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : Invalid token found %s : %d\n",__func__,__LINE__);
      return -EINVAL;
   }

   if (copy_to_user(buf, &response_packet, sizeof(tddi_packet_t)))
   {
      TORNA_LOG(MESHAP_LOGGER_TYPE_FLOW, "meshap_tddi: _meshap_al_test_3_1_request copy_to_user(buf,&response_packet,sizeof(tddi_packet_t)) failed\n");
      return -EFAULT;
   }

   if (copy_to_user(buf + sizeof(tddi_packet_t), &response_data, sizeof(tddi_test_response_t)))
   {
      TORNA_LOG(MESHAP_LOGGER_TYPE_FLOW, "meshap_tddi: _meshap_al_test_3_1_request copy_to_user(buf + sizeof(tddi_packet_t),&response_data,sizeof(tddi_test_response_t)) failed\n");
      return -EFAULT;
   }

   meshap_tddi_globals.test_3_token = meshap_al_test_3_1();

   *ppos += sizeof(tddi_packet_t) + sizeof(tddi_test_response_t);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s: %d\n",__func__,__LINE__);
   return sizeof(tddi_packet_t) + sizeof(tddi_test_response_t);
}


static inline ssize_t _meshap_al_test_3_2_request(_file_data_t *fd, tddi_packet_t *packet, unsigned char *data, char *buf, size_t count, loff_t *ppos)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s: %d\n",__func__,__LINE__);
   tddi_packet_t        response_packet;
   tddi_test_response_t response_data;

   if (count < sizeof(tddi_packet_t) + sizeof(tddi_test_response_t))
   {
      TORNA_LOG(MESHAP_LOGGER_TYPE_FLOW, "meshap_tddi: _meshap_al_test_3_2_request count < sizeof(tddi_packet_t) + sizeof(tddi_test_response_t)\n");
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : out of memory %s : %d\n",__func__,__LINE__);
      return -ENOMEM;
   }

   response_packet.signature = TDDI_PACKET_SIGNATURE;
   response_packet.version   = TDDI_CURRENT_VERSION;
   response_packet.type      = TDDI_PACKET_TYPE_RESPONSE;
   response_packet.sub_type  = TDDI_RESPONSE_TYPE_TEST;
   response_packet.data_size = sizeof(tddi_test_response_t);

   if (meshap_tddi_globals.test_3_token == (unsigned int)NULL)
   {
      TORNA_LOG(MESHAP_LOGGER_TYPE_FLOW, "meshap_tddi: _meshap_al_test_3_2_request meshap_tddi_globals.test_3_token == NULL\n");
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : Invalid token found %s : %d\n",__func__,__LINE__);
      return -EINVAL;
   }

   if (copy_to_user(buf, &response_packet, sizeof(tddi_packet_t)))
   {
      TORNA_LOG(MESHAP_LOGGER_TYPE_FLOW, "meshap_tddi: _meshap_al_test_3_2_request copy_to_user(buf,&response_packet,sizeof(tddi_packet_t))\n");
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : Bad address :  copy to user failed %s : %d\n",__func__,__LINE__);
      return -EFAULT;
   }

   if (copy_to_user(buf + sizeof(tddi_packet_t), &response_data, sizeof(tddi_test_response_t)))
   {
      TORNA_LOG(MESHAP_LOGGER_TYPE_FLOW, "meshap_tddi: _meshap_al_test_3_2_request copy_to_user(buf + sizeof(tddi_packet_t),&response_data,sizeof(tddi_test_response_t))\n");
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : Bad address :  copy to user failed %s : %d\n",__func__,__LINE__);
      return -EFAULT;
   }

   meshap_al_test_3_2(meshap_tddi_globals.test_3_token);

   meshap_tddi_globals.test_3_token = (unsigned int)NULL;

   *ppos += sizeof(tddi_packet_t) + sizeof(tddi_test_response_t);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s: %d\n",__func__,__LINE__);
   return sizeof(tddi_packet_t) + sizeof(tddi_test_response_t);
}


static inline ssize_t _meshap_al_test_4_1_request(_file_data_t *fd, tddi_packet_t *packet, unsigned char *data, char *buf, size_t count, loff_t *ppos)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s: %d\n",__func__,__LINE__);
   tddi_packet_t        response_packet;
   tddi_test_response_t response_data;

   if (count < sizeof(tddi_packet_t) + sizeof(tddi_test_response_t))
   {
      TORNA_LOG(MESHAP_LOGGER_TYPE_FLOW, "meshap_tddi: _meshap_al_test_4_1_request count < sizeof(tddi_packet_t) + sizeof(tddi_test_response_t)\n");
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : out of memory %s : %d\n",__func__,__LINE__);
      return -ENOMEM;
   }

   response_packet.signature = TDDI_PACKET_SIGNATURE;
   response_packet.version   = TDDI_CURRENT_VERSION;
   response_packet.type      = TDDI_PACKET_TYPE_RESPONSE;
   response_packet.sub_type  = TDDI_RESPONSE_TYPE_TEST;
   response_packet.data_size = sizeof(tddi_test_response_t);

   if (copy_to_user(buf, &response_packet, sizeof(tddi_packet_t)))
   {
      TORNA_LOG(MESHAP_LOGGER_TYPE_FLOW, "meshap_tddi: _meshap_al_test_4_1_request copy_to_user(buf,&response_packet,sizeof(tddi_packet_t)) failed\n");
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : Bad address :  copy to user failed %s : %d\n",__func__,__LINE__);
      return -EFAULT;
   }

   if (copy_to_user(buf + sizeof(tddi_packet_t), &response_data, sizeof(tddi_test_response_t)))
   {
      TORNA_LOG(MESHAP_LOGGER_TYPE_FLOW, "meshap_tddi: _meshap_al_test_4_1_request copy_to_user(buf + sizeof(tddi_packet_t),&response_data,sizeof(tddi_test_response_t)) failed\n");
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : Bad address :  copy to user failed %s : %d\n",__func__,__LINE__);
      return -EFAULT;
   }

   meshap_al_test_4_1();

   *ppos += sizeof(tddi_packet_t) + sizeof(tddi_test_response_t);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s: %d\n",__func__,__LINE__);
   return sizeof(tddi_packet_t) + sizeof(tddi_test_response_t);
}


static inline ssize_t _meshap_al_test_5_1_request(_file_data_t *fd, tddi_packet_t *packet, unsigned char *data, char *buf, size_t count, loff_t *ppos)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s: %d\n",__func__,__LINE__);
   tddi_packet_t        response_packet;
   tddi_test_response_t response_data;

   if (count < sizeof(tddi_packet_t) + sizeof(tddi_test_response_t))
   {
      TORNA_LOG(MESHAP_LOGGER_TYPE_FLOW, "meshap_tddi: _meshap_al_test_5_1_request count < sizeof(tddi_packet_t) + sizeof(tddi_test_response_t)\n");
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : out of memory %s : %d\n",__func__,__LINE__);
      return -ENOMEM;
   }

   response_packet.signature = TDDI_PACKET_SIGNATURE;
   response_packet.version   = TDDI_CURRENT_VERSION;
   response_packet.type      = TDDI_PACKET_TYPE_RESPONSE;
   response_packet.sub_type  = TDDI_RESPONSE_TYPE_TEST;
   response_packet.data_size = sizeof(tddi_test_response_t);

   if (copy_to_user(buf, &response_packet, sizeof(tddi_packet_t)))
   {
      TORNA_LOG(MESHAP_LOGGER_TYPE_FLOW, "meshap_tddi: _meshap_al_test_5_1_request copy_to_user(buf,&response_packet,sizeof(tddi_packet_t)) failed\n");
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : Bad address :  copy to user failed %s : %d\n",__func__,__LINE__);
      return -EFAULT;
   }

   if (copy_to_user(buf + sizeof(tddi_packet_t), &response_data, sizeof(tddi_test_response_t)))
   {
      TORNA_LOG(MESHAP_LOGGER_TYPE_FLOW, "meshap_tddi: _meshap_al_test_5_1_request copy_to_user(buf + sizeof(tddi_packet_t),&response_data,sizeof(tddi_test_response_t)) failed\n");
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : Bad address :  copy to user failed %s : %d\n",__func__,__LINE__);
      return -EFAULT;
   }

   meshap_al_test_5_1();

   *ppos += sizeof(tddi_packet_t) + sizeof(tddi_test_response_t);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s: %d\n",__func__,__LINE__);
   return sizeof(tddi_packet_t) + sizeof(tddi_test_response_t);
}


static inline ssize_t _meshap_al_test_6_1_request(_file_data_t *fd, tddi_packet_t *packet, unsigned char *data, char *buf, size_t count, loff_t *ppos)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s: %d\n",__func__,__LINE__);
   tddi_packet_t        response_packet;
   tddi_test_response_t response_data;

   if (count < sizeof(tddi_packet_t) + sizeof(tddi_test_response_t))
   {
      TORNA_LOG(MESHAP_LOGGER_TYPE_FLOW, "meshap_tddi: _meshap_al_test_6_1_request count < sizeof(tddi_packet_t) + sizeof(tddi_test_response_t)\n");
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : out of memory %s : %d\n",__func__,__LINE__);
      return -ENOMEM;
   }

   response_packet.signature = TDDI_PACKET_SIGNATURE;
   response_packet.version   = TDDI_CURRENT_VERSION;
   response_packet.type      = TDDI_PACKET_TYPE_RESPONSE;
   response_packet.sub_type  = TDDI_RESPONSE_TYPE_TEST;
   response_packet.data_size = sizeof(tddi_test_response_t);

   if (copy_to_user(buf, &response_packet, sizeof(tddi_packet_t)))
   {
      TORNA_LOG(MESHAP_LOGGER_TYPE_FLOW, "meshap_tddi: _meshap_al_test_6_1_request copy_to_user(buf,&response_packet,sizeof(tddi_packet_t)) failed\n");
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : Bad address :  copy to user failed %s : %d\n",__func__,__LINE__);
      return -EFAULT;
   }

   if (copy_to_user(buf + sizeof(tddi_packet_t), &response_data, sizeof(tddi_test_response_t)))
   {
      TORNA_LOG(MESHAP_LOGGER_TYPE_FLOW, "meshap_tddi: _meshap_al_test_6_1_request copy_to_user(buf + sizeof(tddi_packet_t),&response_data,sizeof(tddi_test_response_t)) failed\n");
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : Bad address :  copy to user failed %s : %d\n",__func__,__LINE__);
      return -EFAULT;
   }

   meshap_al_test_6_1();

   *ppos += sizeof(tddi_packet_t) + sizeof(tddi_test_response_t);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s: %d\n",__func__,__LINE__);
   return sizeof(tddi_packet_t) + sizeof(tddi_test_response_t);
}


static inline ssize_t _meshap_al_test_7_1_request(_file_data_t *fd, tddi_packet_t *packet, unsigned char *data, char *buf, size_t count, loff_t *ppos)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s: %d\n",__func__,__LINE__);
   tddi_packet_t        response_packet;
   tddi_test_response_t response_data;

   if (count < sizeof(tddi_packet_t) + sizeof(tddi_test_response_t))
   {
      TORNA_LOG(MESHAP_LOGGER_TYPE_FLOW, "meshap_tddi: _meshap_al_test_6_1_request count < sizeof(tddi_packet_t) + sizeof(tddi_test_response_t)\n");
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : out of memory %s : %d\n",__func__,__LINE__);
      return -ENOMEM;
   }

   response_packet.signature = TDDI_PACKET_SIGNATURE;
   response_packet.version   = TDDI_CURRENT_VERSION;
   response_packet.type      = TDDI_PACKET_TYPE_RESPONSE;
   response_packet.sub_type  = TDDI_RESPONSE_TYPE_TEST;
   response_packet.data_size = sizeof(tddi_test_response_t);

   if (copy_to_user(buf, &response_packet, sizeof(tddi_packet_t)))
   {
      TORNA_LOG(MESHAP_LOGGER_TYPE_FLOW, "meshap_tddi: _meshap_al_test_6_1_request copy_to_user(buf,&response_packet,sizeof(tddi_packet_t)) failed\n");
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : Bad address :  copy to user failed %s : %d\n",__func__,__LINE__);
      return -EFAULT;
   }

   if (copy_to_user(buf + sizeof(tddi_packet_t), &response_data, sizeof(tddi_test_response_t)))
   {
      TORNA_LOG(MESHAP_LOGGER_TYPE_FLOW, "meshap_tddi: _meshap_al_test_6_1_request copy_to_user(buf + sizeof(tddi_packet_t),&response_data,sizeof(tddi_test_response_t)) failed\n");
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : Bad address :  copy to user failed %s : %d\n",__func__,__LINE__);
      return -EFAULT;
   }

   meshap_al_test_7_1();

   *ppos += sizeof(tddi_packet_t) + sizeof(tddi_test_response_t);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s: %d\n",__func__,__LINE__);
   return sizeof(tddi_packet_t) + sizeof(tddi_test_response_t);
}


static inline ssize_t _meshap_al_test_8_1_request(_file_data_t *fd, tddi_packet_t *packet, unsigned char *data, char *buf, size_t count, loff_t *ppos)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s: %d\n",__func__,__LINE__);
   tddi_packet_t        response_packet;
   tddi_test_response_t response_data;

   if (count < sizeof(tddi_packet_t) + sizeof(tddi_test_response_t))
   {
      TORNA_LOG(MESHAP_LOGGER_TYPE_FLOW, "meshap_tddi: _meshap_al_test_6_1_request count < sizeof(tddi_packet_t) + sizeof(tddi_test_response_t)\n");
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : out of memory %s : %d\n",__func__,__LINE__);
      return -ENOMEM;
   }

   response_packet.signature = TDDI_PACKET_SIGNATURE;
   response_packet.version   = TDDI_CURRENT_VERSION;
   response_packet.type      = TDDI_PACKET_TYPE_RESPONSE;
   response_packet.sub_type  = TDDI_RESPONSE_TYPE_TEST;
   response_packet.data_size = sizeof(tddi_test_response_t);

   if (copy_to_user(buf, &response_packet, sizeof(tddi_packet_t)))
   {
      TORNA_LOG(MESHAP_LOGGER_TYPE_FLOW, "meshap_tddi: _meshap_al_test_6_1_request copy_to_user(buf,&response_packet,sizeof(tddi_packet_t)) failed\n");
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : Bad address :  copy to user failed %s : %d\n",__func__,__LINE__);
      return -EFAULT;
   }

   if (copy_to_user(buf + sizeof(tddi_packet_t), &response_data, sizeof(tddi_test_response_t)))
   {
      TORNA_LOG(MESHAP_LOGGER_TYPE_FLOW, "meshap_tddi: _meshap_al_test_6_1_request copy_to_user(buf + sizeof(tddi_packet_t),&response_data,sizeof(tddi_test_response_t)) failed\n");
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : Bad address :  copy to user failed %s : %d\n",__func__,__LINE__);
      return -EFAULT;
   }

   meshap_al_test_8_1();

   *ppos += sizeof(tddi_packet_t) + sizeof(tddi_test_response_t);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s: %d\n",__func__,__LINE__);
   return sizeof(tddi_packet_t) + sizeof(tddi_test_response_t);
}


static inline ssize_t _meshap_al_test_9_1_request(_file_data_t *fd, tddi_packet_t *packet, unsigned char *data, char *buf, size_t count, loff_t *ppos)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s: %d\n",__func__,__LINE__);
   tddi_packet_t        response_packet;
   tddi_test_response_t response_data;

   if (count < sizeof(tddi_packet_t) + sizeof(tddi_test_response_t))
   {
      TORNA_LOG(MESHAP_LOGGER_TYPE_FLOW, "meshap_tddi: _meshap_al_test_6_1_request count < sizeof(tddi_packet_t) + sizeof(tddi_test_response_t)\n");
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : out of memory %s : %d\n",__func__,__LINE__);
      return -ENOMEM;
   }

   response_packet.signature = TDDI_PACKET_SIGNATURE;
   response_packet.version   = TDDI_CURRENT_VERSION;
   response_packet.type      = TDDI_PACKET_TYPE_RESPONSE;
   response_packet.sub_type  = TDDI_RESPONSE_TYPE_TEST;
   response_packet.data_size = sizeof(tddi_test_response_t);

   if (copy_to_user(buf, &response_packet, sizeof(tddi_packet_t)))
   {
      TORNA_LOG(MESHAP_LOGGER_TYPE_FLOW, "meshap_tddi: _meshap_al_test_6_1_request copy_to_user(buf,&response_packet,sizeof(tddi_packet_t)) failed\n");
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : Bad address :  copy to user failed %s : %d\n",__func__,__LINE__);
      return -EFAULT;
   }

   if (copy_to_user(buf + sizeof(tddi_packet_t), &response_data, sizeof(tddi_test_response_t)))
   {
      TORNA_LOG(MESHAP_LOGGER_TYPE_FLOW, "meshap_tddi: _meshap_al_test_6_1_request copy_to_user(buf + sizeof(tddi_packet_t),&response_data,sizeof(tddi_test_response_t)) failed\n");
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : Bad address :  copy to user failed %s : %d\n",__func__,__LINE__);
      return -EFAULT;
   }

   meshap_al_test_9_1();

   *ppos += sizeof(tddi_packet_t) + sizeof(tddi_test_response_t);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s: %d\n",__func__,__LINE__);
   return sizeof(tddi_packet_t) + sizeof(tddi_test_response_t);
}


static inline ssize_t _meshap_al_test_10_1_request(_file_data_t *fd, tddi_packet_t *packet, unsigned char *data, char *buf, size_t count, loff_t *ppos)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s: %d\n",__func__,__LINE__);
   tddi_packet_t        response_packet;
   tddi_test_response_t response_data;

   if (count < sizeof(tddi_packet_t) + sizeof(tddi_test_response_t))
   {
      TORNA_LOG(MESHAP_LOGGER_TYPE_FLOW, "meshap_tddi: _meshap_al_test_6_1_request count < sizeof(tddi_packet_t) + sizeof(tddi_test_response_t)\n");
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : out of memory %s : %d\n",__func__,__LINE__);
      return -ENOMEM;
   }

   response_packet.signature = TDDI_PACKET_SIGNATURE;
   response_packet.version   = TDDI_CURRENT_VERSION;
   response_packet.type      = TDDI_PACKET_TYPE_RESPONSE;
   response_packet.sub_type  = TDDI_RESPONSE_TYPE_TEST;
   response_packet.data_size = sizeof(tddi_test_response_t);

   if (copy_to_user(buf, &response_packet, sizeof(tddi_packet_t)))
   {
      TORNA_LOG(MESHAP_LOGGER_TYPE_FLOW, "meshap_tddi: _meshap_al_test_6_1_request copy_to_user(buf,&response_packet,sizeof(tddi_packet_t)) failed\n");
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : Bad address :  copy to user failed %s : %d\n",__func__,__LINE__);
      return -EFAULT;
   }

   if (copy_to_user(buf + sizeof(tddi_packet_t), &response_data, sizeof(tddi_test_response_t)))
   {
      TORNA_LOG(MESHAP_LOGGER_TYPE_FLOW, "meshap_tddi: _meshap_al_test_6_1_request copy_to_user(buf + sizeof(tddi_packet_t),&response_data,sizeof(tddi_test_response_t)) failed\n");
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : Bad address :  copy to user failed %s : %d\n",__func__,__LINE__);
      return -EFAULT;
   }

   meshap_al_test_10_1();

   *ppos += sizeof(tddi_packet_t) + sizeof(tddi_test_response_t);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s: %d\n",__func__,__LINE__);
   return sizeof(tddi_packet_t) + sizeof(tddi_test_response_t);
}


static inline ssize_t _meshap_al_test_11_1_request(_file_data_t *fd, tddi_packet_t *packet, unsigned char *data, char *buf, size_t count, loff_t *ppos)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s: %d\n",__func__,__LINE__);
   tddi_packet_t        response_packet;
   tddi_test_response_t response_data;

   if (count < sizeof(tddi_packet_t) + sizeof(tddi_test_response_t))
   {
      TORNA_LOG(MESHAP_LOGGER_TYPE_FLOW, "meshap_tddi: _meshap_al_test_11_1_request count < sizeof(tddi_packet_t) + sizeof(tddi_test_response_t)\n");
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : out of memory %s : %d\n",__func__,__LINE__);
      return -ENOMEM;
   }

   response_packet.signature = TDDI_PACKET_SIGNATURE;
   response_packet.version   = TDDI_CURRENT_VERSION;
   response_packet.type      = TDDI_PACKET_TYPE_RESPONSE;
   response_packet.sub_type  = TDDI_RESPONSE_TYPE_TEST;
   response_packet.data_size = sizeof(tddi_test_response_t);

   if (copy_to_user(buf, &response_packet, sizeof(tddi_packet_t)))
   {
      TORNA_LOG(MESHAP_LOGGER_TYPE_FLOW, "meshap_tddi: _meshap_al_test_11_1_request copy_to_user(buf,&response_packet,sizeof(tddi_packet_t)) failed\n");
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : Bad address :  copy to user failed %s : %d\n",__func__,__LINE__);
      return -EFAULT;
   }

   if (copy_to_user(buf + sizeof(tddi_packet_t), &response_data, sizeof(tddi_test_response_t)))
   {
      TORNA_LOG(MESHAP_LOGGER_TYPE_FLOW, "meshap_tddi: _meshap_al_test_1_1_request copy_to_user(buf + sizeof(tddi_packet_t),&response_data,sizeof(tddi_test_response_t)) failed\n");
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : Bad address :  copy to user failed %s : %d\n",__func__,__LINE__);
      return -EFAULT;
   }

   meshap_al_test_11_1();

   *ppos += sizeof(tddi_packet_t) + sizeof(tddi_test_response_t);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s: %d\n",__func__,__LINE__);
   return sizeof(tddi_packet_t) + sizeof(tddi_test_response_t);
}


static inline ssize_t _meshap_al_test_12_1_request(_file_data_t *fd, tddi_packet_t *packet, unsigned char *data, char *buf, size_t count, loff_t *ppos)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s: %d\n",__func__,__LINE__);
   tddi_packet_t        response_packet;
   tddi_test_response_t response_data;

   if (count < sizeof(tddi_packet_t) + sizeof(tddi_test_response_t))
   {
      TORNA_LOG(MESHAP_LOGGER_TYPE_FLOW, "meshap_tddi: _meshap_al_test_12_1_request count < sizeof(tddi_packet_t) + sizeof(tddi_test_response_t)\n");
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : out of memory %s : %d\n",__func__,__LINE__);
      return -ENOMEM;
   }

   response_packet.signature = TDDI_PACKET_SIGNATURE;
   response_packet.version   = TDDI_CURRENT_VERSION;
   response_packet.type      = TDDI_PACKET_TYPE_RESPONSE;
   response_packet.sub_type  = TDDI_RESPONSE_TYPE_TEST;
   response_packet.data_size = sizeof(tddi_test_response_t);

   if (copy_to_user(buf, &response_packet, sizeof(tddi_packet_t)))
   {
      TORNA_LOG(MESHAP_LOGGER_TYPE_FLOW, "meshap_tddi: _meshap_al_test_12_1_request copy_to_user(buf,&response_packet,sizeof(tddi_packet_t)) failed\n");
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : Bad address :  copy to user failed %s : %d\n",__func__,__LINE__);
      return -EFAULT;
   }

   if (copy_to_user(buf + sizeof(tddi_packet_t), &response_data, sizeof(tddi_test_response_t)))
   {
      TORNA_LOG(MESHAP_LOGGER_TYPE_FLOW, "meshap_tddi: _meshap_al_test_12_1_request copy_to_user(buf + sizeof(tddi_packet_t),&response_data,sizeof(tddi_test_response_t)) failed\n");
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : Bad address :  copy to user failed %s : %d\n",__func__,__LINE__);
      return -EFAULT;
   }

   meshap_al_test_12_1();

   *ppos += sizeof(tddi_packet_t) + sizeof(tddi_test_response_t);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s: %d\n",__func__,__LINE__);

   return sizeof(tddi_packet_t) + sizeof(tddi_test_response_t);
}


static inline ssize_t _meshap_al_test_13_1_request(_file_data_t *fd, tddi_packet_t *packet, unsigned char *data, char *buf, size_t count, loff_t *ppos)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s: %d\n",__func__,__LINE__);
   tddi_packet_t        response_packet;
   tddi_test_response_t response_data;

   if (count < sizeof(tddi_packet_t) + sizeof(tddi_test_response_t))
   {
      TORNA_LOG(MESHAP_LOGGER_TYPE_FLOW, "meshap_tddi: _meshap_al_test_13_1_request count < sizeof(tddi_packet_t) + sizeof(tddi_test_response_t)\n");
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : out of memory %s : %d\n",__func__,__LINE__);
      return -ENOMEM;
   }

   response_packet.signature = TDDI_PACKET_SIGNATURE;
   response_packet.version   = TDDI_CURRENT_VERSION;
   response_packet.type      = TDDI_PACKET_TYPE_RESPONSE;
   response_packet.sub_type  = TDDI_RESPONSE_TYPE_TEST;
   response_packet.data_size = sizeof(tddi_test_response_t);

   if (copy_to_user(buf, &response_packet, sizeof(tddi_packet_t)))
   {
      TORNA_LOG(MESHAP_LOGGER_TYPE_FLOW, "meshap_tddi: _meshap_al_test_13_1_request copy_to_user(buf,&response_packet,sizeof(tddi_packet_t)) failed\n");
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : Bad address :  copy to user failed %s : %d\n",__func__,__LINE__);
      return -EFAULT;
   }

   if (copy_to_user(buf + sizeof(tddi_packet_t), &response_data, sizeof(tddi_test_response_t)))
   {
      TORNA_LOG(MESHAP_LOGGER_TYPE_FLOW, "meshap_tddi: _meshap_al_test_13_1_request copy_to_user(buf + sizeof(tddi_packet_t),&response_data,sizeof(tddi_test_response_t)) failed\n");
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : Bad address :  copy to user failed %s : %d\n",__func__,__LINE__);
      return -EFAULT;
   }

   meshap_al_test_13_1();

   *ppos += sizeof(tddi_packet_t) + sizeof(tddi_test_response_t);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s: %d\n",__func__,__LINE__);
   return sizeof(tddi_packet_t) + sizeof(tddi_test_response_t);
}


static inline ssize_t _meshap_al_test_14_1_request(_file_data_t *fd, tddi_packet_t *packet, unsigned char *data, char *buf, size_t count, loff_t *ppos)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s: %d\n",__func__,__LINE__);
   tddi_packet_t        response_packet;
   tddi_test_response_t response_data;

   if (count < sizeof(tddi_packet_t) + sizeof(tddi_test_response_t))
   {
      TORNA_LOG(MESHAP_LOGGER_TYPE_FLOW, "meshap_tddi: _meshap_al_test_14_1_request count < sizeof(tddi_packet_t) + sizeof(tddi_test_response_t)\n");
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : out of memory %s : %d\n",__func__,__LINE__);
      return -ENOMEM;
   }

   response_packet.signature = TDDI_PACKET_SIGNATURE;
   response_packet.version   = TDDI_CURRENT_VERSION;
   response_packet.type      = TDDI_PACKET_TYPE_RESPONSE;
   response_packet.sub_type  = TDDI_RESPONSE_TYPE_TEST;
   response_packet.data_size = sizeof(tddi_test_response_t);

   if (copy_to_user(buf, &response_packet, sizeof(tddi_packet_t)))
   {
      TORNA_LOG(MESHAP_LOGGER_TYPE_FLOW, "meshap_tddi: _meshap_al_test_14_1_request copy_to_user(buf,&response_packet,sizeof(tddi_packet_t)) failed\n");
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : Bad address :  copy to user failed %s : %d\n",__func__,__LINE__);
      return -EFAULT;
   }

   if (copy_to_user(buf + sizeof(tddi_packet_t), &response_data, sizeof(tddi_test_response_t)))
   {
      TORNA_LOG(MESHAP_LOGGER_TYPE_FLOW, "meshap_tddi: _meshap_al_test_14_1_request copy_to_user(buf + sizeof(tddi_packet_t),&response_data,sizeof(tddi_test_response_t)) failed\n");
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : Bad address :  copy to user failed %s : %d\n",__func__,__LINE__);
      return -EFAULT;
   }

   meshap_al_test_14_1();

   *ppos += sizeof(tddi_packet_t) + sizeof(tddi_test_response_t);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s: %d\n",__func__,__LINE__);
   return sizeof(tddi_packet_t) + sizeof(tddi_test_response_t);
}
#endif /* _MESHAP_AL_TEST_SUITE_ */

static inline ssize_t _meshap_tddi_send_hwinfo_type(_file_data_t*   fd,tddi_packet_t* packet,unsigned char* data,char* buf,size_t count,loff_t* ppos)
{
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s: %d\n",__func__,__LINE__);
	tddi_packet_t   response_packet;
	int             send_status =-1;
	if ( count < ( sizeof(tddi_packet_t) + sizeof(int)))
	{
		TORNA_LOG(MESHAP_LOGGER_TYPE_FLOW,"meshap_tddi: _meshap_tddi_send_hwinfo_type count < sizeof(tddi_packet_t)\n");
                al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : out of memory %s : %d\n",__func__,__LINE__);
		return -ENOMEM;
	}

	response_packet.signature       = TDDI_PACKET_SIGNATURE;
	response_packet.version         = TDDI_CURRENT_VERSION;
	response_packet.type            = TDDI_PACKET_TYPE_RESPONSE;
	response_packet.sub_type        = TDDI_RESPONSE_TYPE_SEND_HW_INFO;
	response_packet.data_size       = sizeof(int);

	/* HW INFO Response API call*/
	send_status =  mesh_imcp_send_packet_hardware_info(AL_CONTEXT_SINGLE);

	/* copy response packet */
	if (copy_to_user(buf,&response_packet,sizeof(tddi_packet_t)))
	{
		TORNA_LOG(MESHAP_LOGGER_TYPE_FLOW,"meshap_tddi: _meshap_tddi_send_hwinfo_type copy_to_user(buf,&response_packet,sizeof(tddi_packet_t)) failed\n");
     		al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : Bad address :  copy to user failed %s : %d\n",__func__,__LINE__);
		return -EFAULT;
	}

	/* copy response data */
	if (copy_to_user(buf + sizeof(tddi_packet_t),&send_status,sizeof(int)))
	{
		TORNA_LOG(MESHAP_LOGGER_TYPE_FLOW,"meshap_tddi: _meshap_tddi_send_hwinfo_type copy_to_user(buf,&send_status,sizeof(int))) failed\n");
      		al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : Bad address :  copy to user failed %s : %d\n",__func__,__LINE__);
		return -EFAULT;
	}
	*ppos   += ( sizeof(tddi_packet_t) + sizeof (int) );
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s: %d\n",__func__,__LINE__);
	return ( sizeof(tddi_packet_t) + sizeof(int));
}

static inline ssize_t _meshap_tddi_get_node_type(_file_data_t*   fd,tddi_packet_t* packet,unsigned char* data,char* buf,size_t count,loff_t* ppos)
{
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s: %d\n",__func__,__LINE__);
	tddi_packet_t   response_packet;
	int             node_type_val=-1;
	al_net_if_t		 *net_if;
	int link_status_flag;

	if ( count < (sizeof(tddi_packet_t) + sizeof(int)))
	{
		TORNA_LOG(MESHAP_LOGGER_TYPE_FLOW,"meshap_tddi: _meshap_tddi_get_node_type count < sizeof(tddi_packet_t) + sizeof(int)\n");
                al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : out of memory %s : %d\n",__func__,__LINE__);
		return -ENOMEM;
	}

	response_packet.signature       = TDDI_PACKET_SIGNATURE;
	response_packet.version         = TDDI_CURRENT_VERSION;
	response_packet.type            = TDDI_PACKET_TYPE_RESPONSE;
	response_packet.sub_type        = TDDI_RESPONSE_TYPE_GET_NODE_TYPE;
	response_packet.data_size       = sizeof(int);

	net_if = meshap_core_lookup_net_if("eth0");
	if (net_if != NULL)
	{
		link_status_flag = net_if->get_flags(net_if);
		if (link_status_flag & AL_NET_IF_FLAGS_LINKED)
		{
			node_type_val = 0;	
		}
		else
			node_type_val = 1;
	}

	/* copy response packet */
	if (copy_to_user(buf,&response_packet,sizeof(tddi_packet_t)))
	{
		TORNA_LOG(MESHAP_LOGGER_TYPE_FLOW,"meshap_tddi: _meshap_tddi_get_node_type copy_to_user(buf,&response_packet,sizeof(tddi_packet_t)) failed\n");
      		al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : Bad address :  copy to user failed %s : %d\n",__func__,__LINE__);
		return -EFAULT;
	}


	/* copy response data */
	if (copy_to_user(buf + sizeof(tddi_packet_t),&node_type_val,sizeof(int)))
	{
		TORNA_LOG(MESHAP_LOGGER_TYPE_FLOW,"meshap_tddi: _meshap_tddi_get_node_type copy_to_user(buf,&response_packet,sizeof(tddi_packet_t)) failed\n");
      		al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : Bad address :  copy to user failed %s : %d\n",__func__,__LINE__);
		return -EFAULT;
	}

	*ppos   += (sizeof(tddi_packet_t) + sizeof(int));
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s: %d\n",__func__,__LINE__);

	return ( sizeof(tddi_packet_t) + sizeof(int));
}

static inline ssize_t _meshap_tddi_send_stationinfo_type(_file_data_t*   fd,tddi_packet_t* packet,unsigned char* data,char* buf,size_t count,loff_t* ppos)
{
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s: %d\n",__func__,__LINE__);
	imcp_packet_info_t      pi;
	tddi_packet_t   response_packet;
	tddi_station_info_request_t     *sta_info_req;
	unsigned int   sta_info_pkt_type = 0;

	int             send_status =-1;
	if ( count < ( sizeof(tddi_packet_t) + sizeof(int)))
	{
		TORNA_LOG(MESHAP_LOGGER_TYPE_FLOW,"meshap_tddi: _meshap_tddi_send_stationinfo_type count < sizeof(tddi_packet_t)\n");
		al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : out of memory %s : %d\n",__func__,__LINE__);
		return -ENOMEM;
	}

	memset(&pi, 0, sizeof(imcp_packet_info_t));


	sta_info_req = (tddi_station_info_request_t *)data;

	/* DS MAC address */

	/* STATION INFO request packet type 1 */
	if ( sta_info_req->sta_info_pkt_type == STA_INFO_PKT_TYPE_1 )
	{
		pi.u.sta_info_request.sta_mac.length = 6;
		pi.u.sta_info_request.ds_mac.length  = 6;
		memcpy( pi.u.sta_info_request.ds_mac.bytes,sta_info_req->ds_mac,6);
		memcpy( pi.u.sta_info_request.sta_mac.bytes,sta_info_req->sta_mac,6);
		mesh_imcp_process_packet_sta_info_request(&pi);
	}
	response_packet.signature       = TDDI_PACKET_SIGNATURE;
	response_packet.version         = TDDI_CURRENT_VERSION;
	response_packet.type            = TDDI_PACKET_TYPE_RESPONSE;
	response_packet.sub_type        = TDDI_RESPONSE_TYPE_SEND_STATION_INFO;
	response_packet.data_size       = sizeof(int);

	/* STATION INFO Response API call*/
	send_status =  0;

	/* copy response packet */
	if (copy_to_user(buf,&response_packet,sizeof(tddi_packet_t)))
	{
		TORNA_LOG(MESHAP_LOGGER_TYPE_FLOW,"meshap_tddi: _meshap_tddi_send_stationinfo_type copy_to_user(buf,&response_packet,sizeof(tddi_packet_t)) failed\n");
      		al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : Bad address :  copy to user failed %s : %d\n",__func__,__LINE__);
		return -EFAULT;
	}

	/* copy response data */
	if (copy_to_user(buf + sizeof(tddi_packet_t),&send_status,sizeof(int)))
	{
		TORNA_LOG(MESHAP_LOGGER_TYPE_FLOW,"meshap_tddi: _meshap_tddi_send_stationinfo_type copy_to_user(buf,&send_status,sizeof(int))) failed\n");
      		al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : Bad address :  copy to user failed %s : %d\n",__func__,__LINE__);
		return -EFAULT;
	}
	*ppos   += (sizeof(tddi_packet_t) + sizeof(int));
        
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s: %d\n",__func__,__LINE__);
	return ( sizeof(tddi_packet_t) + sizeof(int));
}
