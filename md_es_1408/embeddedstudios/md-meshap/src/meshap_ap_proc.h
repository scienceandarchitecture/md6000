/********************************************************************************
* MeshDynamics
* --------------
* File     : meshap_ap_proc.h
* Comments : Meshap AP proc interface
* Created  : 6/18/2005
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  0  |6/18/2005 | Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

#include "meshap_proc.h"

#ifndef __MESHAP_AP_PROC_H__
#define __MESHAP_AP_PROC_H__

int meshap_ap_proc_initialize(void);

//void	meshap_ap_proc_add_read_entry		(const char*  name,meshap_proc_read_routine_t routine, void* data);
void meshap_ap_proc_add_read_entry(const char *name, struct file_operations *proc_file_fops, void *data);
#endif /*__MESHAP_AP_PROC_H__*/
