/********************************************************************************
* MeshDynamics
* --------------
* File     : meshap.c
* Comments : Torna MeshAP entry point implementation
* Created  : 5/12/2004
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* | 12  |8/22/2008 | Changes for SIP                                 |Abhijit |
* -----------------------------------------------------------------------------
* | 10  |7/24/2007 | REBOOT logging changes                          | Sriram |
* -----------------------------------------------------------------------------
* |  9  |01/23/2006| Added SDK /proc interface                       | Abhijit|
* -----------------------------------------------------------------------------
* |  8  |11/14/2006| Added Heap /proc interface                      | Sriram |
* -----------------------------------------------------------------------------
* |  7  |9/15/2005 | meshap_get_sta_info Added                       | Sriram |
* -----------------------------------------------------------------------------
* |  6  |6/18/2005 | Added MESH and AP /proc interfaces              | Sriram |
* -----------------------------------------------------------------------------
* |  5  |4/14/2005 | Changes for Panic/Trap handler                  | Sriram |
* -----------------------------------------------------------------------------
* |  4  |12/26/2004| Changes for /proc FS                            | Sriram |
* -----------------------------------------------------------------------------
* |  3  |12/25/2004| Added conditional compiling for MIP             | Sriram |
* -----------------------------------------------------------------------------
* |  2  |12/15/2004| Changes for get_ds_if TDDI call                 | Sriram |
* -----------------------------------------------------------------------------
* |  1  |6/5/2004  | Misc fixes                                      | Sriram |
* -----------------------------------------------------------------------------
* |  0  |5/12/2004 | Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/


#include <generated/autoconf.h>

#include <linux/module.h>
#include <linux/init.h>
#include <linux/version.h>
#include <linux/if.h>
#include <linux/netdevice.h>
#include <linux/wireless.h>
#include <linux/skbuff.h>

#include "torna_types.h"
#include "torna_wlan.h"
#include "meshap.h"
#include "meshap_logger.h"
#include "al.h"
#include "al_802_11.h"
#include "meshap_core.h"
#include "torna_mac_hdr.h"
#include "meshap_ip_device.h"
#include "meshap_tddi.h"
#include "meshap_proc.h"
#include "meshap_version_proc.h"
#include "meshap_mesh_proc.h"
#include "meshap_ap_proc.h"
#include "meshap_heap_proc.h"
#include "meshap_panic.h"
#include "meshap_sdk_proc.h"
#include "meshap_reboot_proc.h"
#include "meshap_sip_proc.h"


TORNA_DEBUG_MASK_DECLARE(_TORNA_DEBUG_MASK_VALUE_);

MODULE_AUTHOR("Sriram Dayanandan <sriram@meshdynamics.com>");
MODULE_DESCRIPTION("Torna MeshAP Entry Point version ");
//MODULE_LICENSE			("Copyright (c) 1999-2004 Advanced Cybernetics Group, Inc");
MODULE_LICENSE("GPL");

mac80211_tx_rx_pkt_stats_t *mac_tx_rx_stats = NULL;


extern void dump_bytes(char *, int);
extern int mesh_network_unstable;

extern void register_notifier(int (*call_back_handler)(struct net_device *dev, void *dev_token, int status));

void meshap_register_mac80211_tx_rx_stats(mac80211_tx_rx_pkt_stats_t *mac80211_tx_rx_stats)
{
	mac_tx_rx_stats = mac80211_tx_rx_stats;
}

void meshap_unregister_mac80211_tx_rx_stats(void)
{
	mac_tx_rx_stats = NULL;
}

void meshap_process_mgmt_frame(struct net_device *dev, void *dev_token, struct sk_buff *skb)
{
   TORNA_LOG(MESHAP_LOGGER_TYPE_RX_MGMT, "meshap_process_mgmt_frame: received data skb of length %d\n", skb->len);
   meshap_core_process_mgmt_frame(dev, dev_token, skb);
}


void meshap_process_data_frame(struct net_device *dev, void *dev_token, struct sk_buff *skb)
{
   MESH_USAGE_PROFILING_INIT

   /* On receiving an association frame, hostapd informs kernel about this association by calling netif_rx.
    * Since netif_rx has registered meshap_process_data_frame, we need not process this packet in this context
    */

   /*check wether the upstack_pkt_queue_init_done or not if not send the packet to upstack*/
   MESH_USAGE_PROFILE_START(start)
   if (!upstack_pkt_queue_init_done)
   {
	   local_bh_disable();
	   NETIF_RECEIVE_SKB(skb);
	   local_bh_enable();
           MESH_USAGE_PROFILE_END(profiling_info.process_eth_skb_func_tprof,index,start,end)
	   return;
   }

   meshap_core_net_if_t *core_net_if;
   core_net_if = meshap_core_lookup_net_if(dev->name);

   /*If core_net_if is NULL send the packet to upstack*/
   if (core_net_if == NULL)
   {
	   local_bh_disable();
	   NETIF_RECEIVE_SKB(skb);
	   local_bh_enable();
           MESH_USAGE_PROFILE_END(profiling_info.process_eth_skb_func_tprof,index,start,end)
	   return;
   }

   if (core_net_if->dev_info->encapsulation == MESHAP_NET_DEV_ENCAPSULATION_802_11)
   {
	   if(upstack_pkt_queue_init_done) {
		   AL_IMPL_ATOMIC_INC(q_packet_stats.upstack_pktQ_total_packet_added_count);
		   AL_IMPL_ATOMIC_INC(q_packet_stats.upstack_pktQ_current_packet_count);
		   if(q_packet_stats.upstack_pktQ_current_packet_count.counter > q_packet_stats.upstack_pktQ_current_packet_max_count.counter) {
			   q_packet_stats.upstack_pktQ_current_packet_max_count.counter = q_packet_stats.upstack_pktQ_current_packet_count.counter;
		   }
		   meshap_skb_enqueue(&upstack_pkt_queue, skb, &skb_upstack_que_splock);
		   wake_up_process(meshap_upstack_pkt_process_task);
	   }else {
		   local_bh_disable();
		   NETIF_RECEIVE_SKB(skb);
		   local_bh_enable();
	   }
       MESH_USAGE_PROFILE_END(profiling_info.process_eth_skb_func_tprof,index,start,end)
	   return;
   }
   meshap_core_process_data_frame(dev, dev_token, skb);
   MESH_USAGE_PROFILE_END(profiling_info.process_eth_skb_func_tprof,index,start,end)
}


void meshap_on_link_notify(struct net_device *dev, void *dev_token, u32 state_flags)
{
	TORNA_LOG(MESHAP_LOGGER_TYPE_FLOW, "meshap_on_link_notify: %s state_flags = %d\n", dev->name, state_flags);
   meshap_core_on_link_notify(dev, dev_token, state_flags);
}

void meshap_set_mesh_network_unstable()
{
   mesh_network_unstable = 1;
}

void meshap_on_net_device_create(struct net_device *dev, meshap_net_dev_t *dev_info)
{
//	MOD_INC_USE_COUNT;
   meshap_core_on_net_device_create(dev, dev_info);
}


void meshap_on_net_device_destroy(struct net_device *dev, void *dev_token)
{
//	MOD_DEC_USE_COUNT;
   meshap_core_on_net_device_destroy(dev, dev_token);
}


int meshap_get_sta_info(unsigned char *mac_addr, meshap_sta_info_t *sta_info)
{
   return meshap_core_get_sta_info(mac_addr, sta_info);
}


void meshap_reboot_machine(unsigned char reboot_code)
{
   meshap_core_reboot_machine(reboot_code);
}


int meshap_netdev_event(struct notifier_block *unused,
                               unsigned long event, void *ptr)
{
//16MIG changed as per 16.02 inorder to get eth devices to be registered
#ifdef CNS_ONLY
   struct net_device *dev = ptr;	//1408MIG cns3xxx reverted back like 1210
#elif defined (IMX_ONLY) || defined (x86_ONLY)
   struct net_device *dev = netdev_notifier_info_to_dev(ptr);
#endif

   switch (event)
   {
   case NETDEV_REGISTER:
//RAMESH16MIG dev->name should not be NULL
//		if (dev->wireless_handlers == NULL && strcmp("mip0", dev->name) && strcmp("lo", dev->name) && strlen(dev->name)) {
//RAMESH16MIG allowing only eth interfaces
		   if ((!strcmp(dev->name, "eth0")) || (!strcmp(dev->name, "eth1")) )
      {
         meshap_net_dev_t *meshap_eth_net_dev;
         meshap_eth_net_dev = (meshap_net_dev_t *)kmalloc(sizeof(meshap_net_dev_t), GFP_KERNEL);
         memset(meshap_eth_net_dev, 0, sizeof(meshap_net_dev_t));
         meshap_eth_net_dev->encapsulation = MESHAP_NET_DEV_ENCAPSULATION_ETHERNET;
         meshap_eth_net_dev->phy_hw_type   = MESHAP_NET_DEV_PHY_HW_TYPE_802_3;
         printk(KERN_EMERG "RAMESH16MIG : @@@ func : %s line : %d dev->name = %s\n", __func__, __LINE__, dev->name);
         meshap_eth_net_dev->set_hw_addr = (void *)(dev->netdev_ops->ndo_set_mac_address);
         meshap_on_net_device_create(dev, meshap_eth_net_dev);
      }
      break;

   case NETDEV_UNREGISTER:
      // Kernel Memory leak, not freeing kmalloc of meshap_net_dev_t on register
//		if (dev->wireless_handlers == NULL && strcmp("mip0", dev->name) && strcmp("lo", dev->name))
//RAMESH16MIG allowing only eth interfaces
		   if ((!strcmp(dev->name, "eth0")) || (!strcmp(dev->name, "eth1")) )
      {
         meshap_on_net_device_destroy(dev, NULL);
      }
      break;

   case NETDEV_UP:
   case NETDEV_DOWN:
   case NETDEV_CHANGE:
//		if (dev->wireless_handlers == NULL && strcmp("mip0", dev->name) && strcmp("lo", dev->name)) {
//RAMESH16MIG allowing only eth interfaces
		   if ((!strcmp(dev->name, "eth0")) || (!strcmp(dev->name, "eth1")) )
      {
	 if (netif_carrier_ok(dev) && (dev_get_flags(dev) & IFF_UP))                     // Device Up
         {
            meshap_on_link_notify(dev, NULL, MESHAP_ON_LINK_NOTIFY_STATE_LINK_ON);
         }
         else                   // Device Down
         {
            meshap_on_link_notify(dev, NULL, MESHAP_ON_LINK_NOTIFY_STATE_LINK_OFF);
         }
      }
      break;
   }

   //al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
   return 0;
}


static struct notifier_block meshap_netdev_notifier =
{
   .notifier_call = meshap_netdev_event,
};

static int __init meshap_netdev_notifier_initialize(void)
{
   register_netdevice_notifier(&meshap_netdev_notifier);
   return 0;
}


static int __init _meshap_init(void)
{
   printk(KERN_INFO "\nmeshap.o version %s loaded\n", "_TORNA_VERSION_STRING_");
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
   meshap_tddi_initialize();
   meshap_core_initialize();
#ifndef _MESHAP_NO_MIP_
   meshap_ip_device_initialize();
#endif
   meshap_proc_initialize();
   meshap_version_proc_initialize();
   meshap_sdk_proc_initialize();
   meshap_mesh_proc_initialize();
   meshap_ap_proc_initialize();
   meshap_panic_initialize();
   meshap_reboot_proc_initialize();
   meshap_sip_proc_initialize();
#ifdef _AL_HEAP_DEBUG
   meshap_heap_proc_initialize();
#endif
   meshap_netdev_notifier_initialize();
   /* TODO: Add register notifier in IMX6 ethernet driver */
#ifdef CNS_ONLY
   register_notifier(meshap_on_link_notify);
#endif
   core_dev_process_data_frame = (void *)meshap_process_data_frame;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
   return 0;
}


static void __exit _meshap_exit(void)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
   meshap_core_uninitialize();
   meshap_tddi_uninitialize();
#ifndef _MESHAP_NO_MIP_
   meshap_ip_device_uninitialize();
#endif
   meshap_panic_uninitialize();
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
   printk(KERN_INFO "\nmeshap.o version %s unloaded\n", "_TORNA_VERSION_STRING_");
}


EXPORT_SYMBOL(meshap_process_mgmt_frame);
EXPORT_SYMBOL(meshap_process_data_frame);
EXPORT_SYMBOL(meshap_on_link_notify);
EXPORT_SYMBOL(meshap_on_net_device_create);
EXPORT_SYMBOL(meshap_on_net_device_destroy);
EXPORT_SYMBOL(meshap_get_sta_info);
EXPORT_SYMBOL(meshap_reboot_machine);
EXPORT_SYMBOL(meshap_register_mac80211_tx_rx_stats);
EXPORT_SYMBOL(meshap_unregister_mac80211_tx_rx_stats);
EXPORT_SYMBOL(meshap_set_mesh_network_unstable);

module_init(_meshap_init);
module_exit(_meshap_exit);
