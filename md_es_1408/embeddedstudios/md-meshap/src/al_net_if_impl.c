/********************************************************************************
* MeshDynamics
* --------------
* File     : al_net_if_impl.c
* Comments : Torna MeshAP abstraction layer net_if implementation
* Created  : 5/25/2004
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  35 |02/10/2009| Changes for Action Frame Support				  |Abhijit |
* -----------------------------------------------------------------------------
* |  34 |02/10/2009| Changes for mixed mode						  |Abhijit |
* -----------------------------------------------------------------------------
* |  33 |05/09/2008| Changes for Beaconing Uplink		              |Prachiti|
* -----------------------------------------------------------------------------
* |  32 |01/07/2008| Changes for mixed mode						  |Prachiti|
* -----------------------------------------------------------------------------
* |  31 |8/8/2007  | Check dev_queue_xmit return value               | Sriram |
* -----------------------------------------------------------------------------
* |  30 |5/1/2007  | Added Filtering for negative RSSI values        | Sriram |
* -----------------------------------------------------------------------------
* |  29 |4/11/2007 | Changes for Radio diagnostics                   | Sriram |
* -----------------------------------------------------------------------------
* |  28 |2/15/2007 | Added set_tx_antenna                            | Sriram |
* -----------------------------------------------------------------------------
* |  27 |2/14/2007 | Changes for PS phy_subtypes                     | Sriram |
* -----------------------------------------------------------------------------
* |  26 |1/30/2007 | Changes for ALLOWED VLAN Tag                    | Sriram |
* -----------------------------------------------------------------------------
* |  25 |01/01/2007| Changes for channel specific DFS                |Prachiti|
* -----------------------------------------------------------------------------
* |  24 |11/14/2006| Changes for heap debug support                  | Sriram |
* -----------------------------------------------------------------------------
* |  23 |11/8/2006 | Changes for compression support                 | Sriram |
* -----------------------------------------------------------------------------
* |  22 |11/1/2006 | Changes for private frequencies                 | Sriram |
* -----------------------------------------------------------------------------
* |  21 |10/18/2006| ETSI Radar changes                              | Sriram |
* -----------------------------------------------------------------------------
* |  20 |03/10/2006| Get category info totally undone                | Sriram |
* -----------------------------------------------------------------------------
* |  19 |03/03/2006| Removed comments for dot11e prints              | Bindu  |
* -----------------------------------------------------------------------------
* |  18 |02/15/2006| Changes for dot11e                              | Bindu  |
* -----------------------------------------------------------------------------
* |  17 |02/08/2006| Changes for Hide SSID                           | Sriram |
* -----------------------------------------------------------------------------
* |  16 |12/14/2005|supported channels changes					      |prachiti|
* -----------------------------------------------------------------------------
* |  15 |12/14/2005| acktimeout and regdomain changes                |prachiti|
* -----------------------------------------------------------------------------
* |  14 |9/12/2005 | ext_config_info Initialized                     | Sriram |
* -----------------------------------------------------------------------------
* |  13 |8/20/2005 | get_bit_rate_info added and implemented         | Sriram |
* -----------------------------------------------------------------------------
* |  12 |5/21/2005 | set_bit_rate Implemented                        | Sriram |
* -----------------------------------------------------------------------------
* |  11 |4/12/2005 | set_hw_addr added to al_net_if                  | Sriram |
* -----------------------------------------------------------------------------
* |  10 |4/8/2005  | Misc changes after merging                      | Sriram |
* -----------------------------------------------------------------------------
* |  9  |4/6/2005  | Security changes                                | Sriram |
* -----------------------------------------------------------------------------
* |  8  |03/09/2005| Vendor Info added to Beacons                    | Anand  |
* -----------------------------------------------------------------------------
* |  7  |1/13/2005 | Implemented AL_802_11_MODE_RADIO_OFF            | Sriram |
* -----------------------------------------------------------------------------
* |  6  |1/7/2005  | Implemented get_last_beacon_time                | Sriram |
* -----------------------------------------------------------------------------
* |  5  |12/9/2004 | Common Scanning routine added                   | Sriram |
* -----------------------------------------------------------------------------
* |  4  |11/15/2004| New 802.11 operations implemented               | Sriram |
* -----------------------------------------------------------------------------
* |  3  |7/22/2004 | get_bit_rate and misc changes                   | Sriram |
* -----------------------------------------------------------------------------
* |  2  |6/11/2004 | Added transmit_ex                               | Sriram |
* -----------------------------------------------------------------------------
* |  1  |6/10/2004 | Implemented last_rx and last_tx time            | Sriram |
* -----------------------------------------------------------------------------
* |  0  |5/25/2004 | Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

#include <generated/autoconf.h>

#include <linux/module.h>
#include <linux/init.h>
#include <linux/version.h>
#include <linux/netdevice.h>
#include <linux/skbuff.h>
#include <linux/wireless.h>
#include <linux/rtnetlink.h>

#include "al.h"
#include "al_802_11.h"
#include "meshap.h"
#include "meshap_logger.h"
#include "meshap_core.h"
#include "torna_log.h"
#include "al_packet_impl.h"
#include "torna_wlan.h"
#include "torna_mac_hdr.h"
#include "al_conf.h"
#include "access_point.h"
#include "mesh_defs.h"

#include <linux/spinlock.h>
#include "access_point/include/access_point.h"
#include "access_point/src/dot1p_queue.h"

profiling_info_t profiling_info;
AL_DECLARE_GLOBAL_EXTERN(mesh_config_info_t * mesh_config);
AL_DECLARE_GLOBAL_EXTERN(struct sk_buff_head tx_skb_queue);
AL_DECLARE_GLOBAL_EXTERN(struct sk_buff_head temp_tx_skb_queue);

struct tx_core_packet {
	meshap_core_packet_t *core_packet;
	struct tx_core_packet *next;
};
typedef struct tx_core_packet tx_core_packet_t;

al_spinlock_t          tx_Qsplock = AL_SPIN_LOCK_UNLOCKED(tx_Qsplock);

AL_DECLARE_GLOBAL_EXTERN(access_point_globals_t globals);

#define _TX_THREAD_SHOULD_BREAK \
   (globals.ap_state == ACCESS_POINT_STATE_STOPPING) || (globals.ap_state == ACCESS_POINT_STATE_STOPPED)

void dump_bytes1(char *D, int count)
{
   int i, base = 0;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "-------- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- ----------------\n");
   for ( ; base + 16 < count; base += 16)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "%08X ", base);

      for (i = 0; i < 16; i++)
      {
         al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "%02X ", D[base + i]);
      }

      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "\n");
   }
   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "%08X ", base);
   for (i = base; i < count; i++)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "%02X ", D[i]);
   }
   for ( ; i < base + 16; i++)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "   ");
   }
   for ( ; i < base + 16; i++)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, " ");
   }
   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "\n");
   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "-------- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- ----------------\n");
   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "\n");
}

void dump_bytes(char *D, int count)
{
   int i, base = 0;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "-------- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- ----------------\n");
   for ( ; base + 16 < count; base += 16)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "%08X ", base);

      for (i = 0; i < 16; i++)
      {
         al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "%02X ", D[base + i]);
      }

      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "\n");
   }
   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "%08X ", base);
   for (i = base; i < count; i++)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "%02X ", D[i]);
   }
   for ( ; i < base + 16; i++)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "   ");
   }
   for ( ; i < base + 16; i++)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, " ");
   }
   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "\n");
   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "-------- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- ----------------\n");
   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "\n");
}


/**
 * al_net_if operations
 */

static int _al_net_if_impl_get_name(al_net_if_t *al_net_if, char *buffer, int length);
static int _al_net_if_impl_get_encapsulation(al_net_if_t *al_net_if);
static int _al_net_if_impl_get_arp_hw_type(al_net_if_t *al_net_if);
static int _al_net_if_impl_get_hw_addr_size(al_net_if_t *al_net_if);
static int _al_net_if_impl_get_mtu(al_net_if_t *al_net_if);
static int _al_net_if_impl_get_config(al_net_if_t *al_net_if, al_net_if_config_t *config);
static int _al_net_if_impl_get_stats(al_net_if_t *al_net_if, al_net_if_stat_t *stats);
static int _al_net_if_impl_get_debug_info(al_net_if_t *al_net_if, debug_infra_info_t *debug_infra_info);
static int _al_net_if_impl_get_last_rx_time(al_net_if_t *al_net_if, al_u64_t *last_rx_time);
static int _al_net_if_impl_get_last_tx_time(al_net_if_t *al_net_if, al_u64_t *last_tx_time);
static int _al_net_if_impl_get_flags(al_net_if_t *al_net_if);
static int _al_net_if_impl_transmit(al_net_if_t *al_net_if, al_packet_t *packet);
static int _al_net_if_impl_set_filter_mode(al_net_if_t *al_net_if, int filter_mode);
static void *_al_net_if_impl_get_extended_operations(al_net_if_t *al_net_if);
static int _al_net_if_impl_start(al_net_if_t *al_net_if);
static int _al_net_if_impl_stop(al_net_if_t *al_net_if);
static int _al_net_if_impl_transmit_ex(struct al_net_if *al_net_if, al_packet_t *packet, int transmit_type);
static int _al_net_if_impl_set_hw_addr(struct al_net_if *al_net_if, al_net_addr_t *hw_addr);

/**
 * al_802_11_operations Commands
 */

static int _al_net_if_impl_associate(al_net_if_t *al_net_if, al_net_addr_t *ap_addr, int channel, const char *essid, int length, int timeout, al_802_11_md_ie_t *md_ie_in, al_802_11_md_ie_t *md_ie_out);
static int _al_net_if_impl_dis_associate(al_net_if_t *al_net_if);
static int _al_net_if_impl_get_bssid(al_net_if_t *al_net_if, al_net_addr_t *bssid);
static int _al_net_if_impl_send_management_frame(al_net_if_t *al_net_if, int sub_type, al_packet_t *packet);
static int _al_net_if_impl_scan_access_points(al_net_if_t *al_net_if, al_scan_access_point_info_t **access_points, int *count, int scan_duration_in_millis, int channel_count, unsigned char *channels);
static int _al_net_if_impl_scan_access_points_active(al_net_if_t *al_net_if, al_scan_access_point_info_t **access_points, int *count, int scan_duration_in_millis, int channel_count, unsigned char *channels);
static int _al_net_if_impl_scan_access_points_passive(al_net_if_t *al_net_if, al_scan_access_point_info_t **access_points, int *count, int scan_duration_in_millis, int channel_count, unsigned char *channels);
static int _al_net_if_impl_set_management_frame_hook(al_net_if_t *al_net_if, al_process_management_frame_t hook);
static int _al_net_if_impl_set_beacon_hook(al_net_if_t *al_net_if, al_process_beacon_t hook);
static int _al_net_if_impl_set_error_hook(al_net_if_t *al_net_if, al_process_packet_error_t hook);
static al_u64_t _al_net_if_impl_get_last_beacon_time(al_net_if_t *al_net_if);
static void _al_net_if_impl_set_mesh_downlink_round_robin_time(al_net_if_t *al_net_if, int beacon_intervals);
static void _al_net_if_impl_add_downlink_round_robin_child(al_net_if_t *al_net_if, al_net_addr_t *addr);
static void _al_net_if_impl_remove_downlink_round_robin_child(al_net_if_t *al_net_if, al_net_addr_t *addr);
static int _al_net_if_impl_virtual_associate(al_net_if_t *al_net_if, al_net_addr_t *ap_addr, int channel, int start);
static int _al_net_if_impl_get_duty_cycle_info(al_net_if_t *al_net_if, int channel_count, al_duty_cycle_info_t *info, unsigned int dwell_time_minis, int flag);
static void _al_net_if_impl_set_rate_ctrl_parameters(al_net_if_t *al_net_if, al_rate_ctrl_param_t *rate_ctrl_param);
static void _al_net_if_impl_reset_rate_ctrl(al_net_if_t *al_net_if, al_net_addr_t *addr);
static void _al_net_if_impl_get_rate_ctrl_info(al_net_if_t *al_net_if, al_net_addr_t *addr, al_rate_ctrl_info_t *rate_ctrl_info);
static void _al_net_if_impl_set_round_robin_notify_hook(al_net_if_t *al_net_if, al_round_robin_notify_t hook);
static void _al_net_if_impl_enable_ds_verification_opertions(al_net_if_t *al_net_if, int enable);
static int _al_net_if_impl_dfs_scan(al_net_if_t *al_net_if);
static void _al_net_if_impl_set_radar_notify_hook(al_net_if_t *al_net_if, al_radar_notify_t hook);
static void _al_net_if_impl_set_probe_request_hook(al_net_if_t *al_net_if, al_on_probe_request_t on_probe_request);

/**
 * al_802_11_operations for MAC
 */

static int _al_net_if_impl_get_mode(al_net_if_t *al_net_if);
static int _al_net_if_impl_set_mode(al_net_if_t *al_net_if, int mode, unsigned char master_quiet);
static int _al_net_if_impl_get_essid(al_net_if_t *al_net_if, char *essid_buffer, int length);
static int _al_net_if_impl_set_essid(al_net_if_t *al_net_if, char *essid_buffer);
static int _al_net_if_impl_get_rts_threshold(al_net_if_t *al_net_if);
static int _al_net_if_impl_set_rts_threshold(al_net_if_t *al_net_if, int threshold);
static int _al_net_if_impl_get_frag_threshold(al_net_if_t *al_net_if);
static int _al_net_if_impl_set_frag_threshold(al_net_if_t *al_net_if, int threshold);
static int _al_net_if_impl_get_beacon_interval(al_net_if_t *al_net_if);
static int _al_net_if_impl_set_beacon_interval(al_net_if_t *al_net_if, int interval);
static int _al_net_if_impl_set_security_info(al_net_if_t *al_net_if, al_802_11_security_info_t *security_info);
static int _al_net_if_impl_set_security_key(al_net_if_t *al_net_if, al_802_11_security_key_info_t *security_key_info, int enable_compression);
static int _al_net_if_impl_release_security_key(al_net_if_t *al_net_if, int index, const u8 *mac_addr);
static int _al_net_if_impl_get_security_key_data(al_net_if_t *al_net_if, int index, al_802_11_security_key_data_t *security_key_data, const u8 *mac_addr);
static int _al_net_if_impl_set_ds_security_key(al_net_if_t *al_net_if, unsigned char *group_key, unsigned char group_key_type, unsigned char group_key_index, unsigned char *pairwise_key, int enable_compression);
static int _al_net_if_impl_get_default_capabilities(al_net_if_t *al_net_if);
static int _al_net_if_impl_get_capabilities(al_net_if_t *al_net_if);
static int _al_net_if_impl_get_slot_time_type(al_net_if_t *al_net_if);
static int _al_net_if_impl_set_slot_time_type(al_net_if_t *al_net_if, int slot_time_type);
static int _al_net_if_impl_get_erp_info(al_net_if_t *al_net_if);
static int _al_net_if_impl_set_erp_info(al_net_if_t *al_net_if, int erp_info);
static int _al_net_if_impl_set_beacon_vendor_info(al_net_if_t *al_net_if, unsigned char *vendor_info_buffer, int length);
static int _al_net_if_impl_get_ack_timeout(al_net_if_t *al_net_if);
static int _al_net_if_impl_set_ack_timeout(al_net_if_t *al_net_if, unsigned short timeout);
static int _al_net_if_impl_get_hide_ssid(al_net_if_t *al_net_if);
static int _al_net_if_impl_set_hide_ssid(al_net_if_t *al_net_if, int hide);
static int _al_net_if_impl_enable_beaconing_uplink(al_net_if_t *al_net_if, unsigned char *vendor_info_buffer, int length);
static int _al_net_if_impl_disable_beaconing_uplink(al_net_if_t *al_net_if);
static int _al_net_if_impl_get_hw_support_ht_capab(al_net_if_t *net_if, tddi_ht_cap_t *ht_cap, tddi_vht_cap_t *vht_cap,unsigned char sub_type);

static int _al_net_if_impl_set_reboot_in_progress_state(al_net_if_t *net_if, int value);
static int _al_net_if_impl_get_wm_duty_cycle_flag(al_net_if_t *net_if, int duty_cycle_flag, int operating_channel);

/**
 * al_802_11_operations for PHY
 */

static int _al_net_if_impl_get_supported_rates(al_net_if_t *al_net_if, unsigned char *supported_rates, int length);
static int _al_net_if_impl_get_bit_rate(al_net_if_t *al_net_if);
static int _al_net_if_impl_set_bit_rate(al_net_if_t *al_net_if, int bit_rate);
static void _al_net_if_impl_get_rate_table(al_net_if_t *al_net_if, al_rate_table_t *rate_table);
static int _al_net_if_impl_get_tx_power(al_net_if_t *al_net_if, al_802_11_tx_power_info_t *power_info);
static int _al_net_if_impl_set_tx_power(al_net_if_t *al_net_if, al_802_11_tx_power_info_t *power_info);
static int _al_net_if_impl_get_channel_count(al_net_if_t *al_net_if);
static int _al_net_if_impl_get_channel(al_net_if_t *al_net_if);
static int _al_net_if_impl_set_channel(al_net_if_t *al_net_if, int channel);
static int _al_net_if_impl_get_phy_mode(al_net_if_t *al_net_if);
static int _al_net_if_impl_set_phy_mode(al_net_if_t *al_net_if, int phy_mode);
static int _al_net_if_impl_get_preamble_type(al_net_if_t *al_net_if);
static int _al_net_if_impl_set_preamble_type(al_net_if_t *al_net_if, int preamble_type);
static int _al_net_if_impl_get_reg_domain_info(al_net_if_t *al_net_if, al_reg_domain_info_t *reg_domain_info);
static int _al_net_if_impl_set_reg_domain_info(al_net_if_t *al_net_if, al_reg_domain_info_t *reg_domain_info);
static int _al_net_if_impl_get_supported_channels(al_net_if_t *al_net_if, al_channel_info_t **supported_channels, int *count);
static int _al_net_if_impl_set_dot11e_category_info(al_net_if_t *al_net_if, al_dot11e_category_info_t *info);
static int _al_net_if_impl_set_tx_antenna(al_net_if_t *al_net_if, int antenna_index);

/**
 * al_802_11_operations (OTHER)
 */

static int _al_net_if_impl_set_auth_hook(al_net_if_t *al_net_if, al_process_auth_t auth_hook);
static int _al_net_if_impl_set_assoc_hook(al_net_if_t *al_net_if, al_process_assoc_t assoc_hook);
static int _al_net_if_impl_set_essid_info(al_net_if_t *al_net_if, al_802_11_security_info_t *net_if_security_info, int count, al_802_11_essid_info_t *info);

/**
 * al_802_11_operations Radio specific
 */

static int _al_net_if_impl_set_radio_data(al_net_if_t *al_net_if, unsigned char *radio_data, int data_length);
static int _al_net_if_impl_radio_diagnostic_command(al_net_if_t *al_net_if, unsigned char *diag_data_in, int data_in_length, unsigned char *diag_data_out, int data_out_buf_len);

/** Action operations */

static int _al_net_if_impl_set_action_hook(al_net_if_t *al_net_if, al_action_notify_t action_hook);
static int _al_net_if_impl_send_action(al_net_if_t *al_net_if, al_net_addr_t *addr, unsigned char *buffer, int length);
static int _al_net_if_init_channels(al_net_if_t *al_net_if);
static int _al_net_if_set_if_use_type(al_net_if_t *al_net_if, int use_type);
static int _al_net_if_set_all_conf_complete(al_net_if_t *al_net_if, int value);
static int _al_net_if_impl_send_deauth(al_net_if_t *al_net_if, u8 *address0, u8 *address1);

static int _al_net_if_impl_send_assoc_response(al_net_if_t *al_net_if, unsigned short stype, u8 *address0, u8 *address1, unsigned short aid,
                                               unsigned char ie_out_valid, al_802_11_information_element_t *ie_out,
                                               struct meshap_mac80211_sta_info sta);
static int _al_net_if_impl_send_auth_response(al_net_if_t *al_net_if, u8 *address0, u8 *address1);
static int _al_net_if_impl_del_station(al_net_if_t *al_net_if, u8 *station_addr);

static int _al_net_if_send_disassoc_from_station(al_net_if_t *al_net_if, int flag);
static int _al_net_if_impl_set_ht_vht_capab(al_net_if_t *al_net_if, al_net_if_ht_vht_capab_t* ht_vht_capab);
static int _al_net_if_impl_set_ht_operation(al_net_if_t *al_net_if, u16* ht_oper_mode);
static int _al_net_if_impl_set_vht_capab(al_net_if_t *al_net_if, al_net_if_ht_vht_capab_t* ht_vht_capab);
static int _al_net_if_impl_set_vht_operation(al_net_if_t *al_net_if, al_802_11_vht_operation_t vht_oper_mode);
static int _al_net_if_impl_init_rate_ctrl(al_net_if_t *al_net_if);

static al_802_11_operations_t _extended_operations =
{
   /**
    * Commands
    */

   associate                               :       _al_net_if_impl_associate,
   dis_associate                           :       _al_net_if_impl_dis_associate,
   get_bssid                               :       _al_net_if_impl_get_bssid,
   send_management_frame                   :       _al_net_if_impl_send_management_frame,
   scan_access_points                      :       _al_net_if_impl_scan_access_points,
   scan_access_points_active               :       _al_net_if_impl_scan_access_points_active,
   scan_access_points_passive              :       _al_net_if_impl_scan_access_points_passive,
   set_management_frame_hook               :       _al_net_if_impl_set_management_frame_hook,
   set_beacon_hook                         :       _al_net_if_impl_set_beacon_hook,
   set_error_hook                          :       _al_net_if_impl_set_error_hook,
   get_last_beacon_time                    :       _al_net_if_impl_get_last_beacon_time,
   set_mesh_downlink_round_robin_time      :       _al_net_if_impl_set_mesh_downlink_round_robin_time,
   add_downlink_round_robin_child          :       _al_net_if_impl_add_downlink_round_robin_child,
   remove_downlink_round_robin_child       :       _al_net_if_impl_remove_downlink_round_robin_child,
   virtual_associate                       :       _al_net_if_impl_virtual_associate,
   get_duty_cycle_info                     :       _al_net_if_impl_get_duty_cycle_info,
   set_rate_ctrl_parameters                :       _al_net_if_impl_set_rate_ctrl_parameters,
   reset_rate_ctrl                         :       _al_net_if_impl_reset_rate_ctrl,
   get_rate_ctrl_info                      :       _al_net_if_impl_get_rate_ctrl_info,
   set_round_robin_notify_hook             :       _al_net_if_impl_set_round_robin_notify_hook,
   enable_ds_verification_opertions        :       _al_net_if_impl_enable_ds_verification_opertions,
   dfs_scan                                :       _al_net_if_impl_dfs_scan,
   set_radar_notify_hook                   :       _al_net_if_impl_set_radar_notify_hook,
   set_probe_request_hook                  :       _al_net_if_impl_set_probe_request_hook,
   send_assoc_response                     :       _al_net_if_impl_send_assoc_response,
   del_station                             :       _al_net_if_impl_del_station,
   send_deauth                             :       _al_net_if_impl_send_deauth,
   send_auth_response                      :       _al_net_if_impl_send_auth_response,

   /**
    * MAC
    */

   get_mode                                :       _al_net_if_impl_get_mode,
   set_mode                                :       _al_net_if_impl_set_mode,
   get_essid                               :       _al_net_if_impl_get_essid,
   set_essid                               :       _al_net_if_impl_set_essid,
   get_rts_threshold                       :       _al_net_if_impl_get_rts_threshold,
   set_rts_threshold                       :       _al_net_if_impl_set_rts_threshold,
   get_frag_threshold                      :       _al_net_if_impl_get_frag_threshold,
   set_frag_threshold                      :       _al_net_if_impl_set_frag_threshold,
   get_beacon_interval                     :       _al_net_if_impl_get_beacon_interval,
   set_beacon_interval                     :       _al_net_if_impl_set_beacon_interval,
   set_security_info                       :       _al_net_if_impl_set_security_info,
   set_security_key                        :       _al_net_if_impl_set_security_key,
   release_security_key                    :       _al_net_if_impl_release_security_key,
   get_security_key_data                   :       _al_net_if_impl_get_security_key_data,
   set_ds_security_key                     :       _al_net_if_impl_set_ds_security_key,
   get_default_capabilities                :       _al_net_if_impl_get_default_capabilities,
   get_capabilities                        :       _al_net_if_impl_get_capabilities,
   get_slot_time_type                      :       _al_net_if_impl_get_slot_time_type,
   set_slot_time_type                      :       _al_net_if_impl_set_slot_time_type,
   get_erp_info                            :       _al_net_if_impl_get_erp_info,
   set_erp_info                            :       _al_net_if_impl_set_erp_info,
   set_beacon_vendor_info                  :   _al_net_if_impl_set_beacon_vendor_info,
   get_ack_timeout                         :       _al_net_if_impl_get_ack_timeout,
   set_ack_timeout                         :       _al_net_if_impl_set_ack_timeout,
   get_hide_ssid                           :       _al_net_if_impl_get_hide_ssid,
   set_hide_ssid                           :       _al_net_if_impl_set_hide_ssid,
   enable_beaconing_uplink                 :       _al_net_if_impl_enable_beaconing_uplink,
   disable_beaconing_uplink                :       _al_net_if_impl_disable_beaconing_uplink,
   get_hw_support_ht_capab                 :       _al_net_if_impl_get_hw_support_ht_capab,
   set_reboot_in_progress_state            :       _al_net_if_impl_set_reboot_in_progress_state,
   get_wm_duty_cycle_flag                  :       _al_net_if_impl_get_wm_duty_cycle_flag,



   /**
    * PHY
    */

   get_supported_rates                     :       _al_net_if_impl_get_supported_rates,
   get_bit_rate                            :       _al_net_if_impl_get_bit_rate,
   set_bit_rate                            :       _al_net_if_impl_set_bit_rate,
   get_rate_table                          :       _al_net_if_impl_get_rate_table,
   get_tx_power                            :       _al_net_if_impl_get_tx_power,
   set_tx_power                            :       _al_net_if_impl_set_tx_power,
   get_channel_count                       :       _al_net_if_impl_get_channel_count,
   get_channel                             :       _al_net_if_impl_get_channel,
   set_channel                             :       _al_net_if_impl_set_channel,
   get_phy_mode                            :       _al_net_if_impl_get_phy_mode,
   set_phy_mode                            :       _al_net_if_impl_set_phy_mode,
   get_preamble_type                       :       _al_net_if_impl_get_preamble_type,
   set_preamble_type                       :       _al_net_if_impl_set_preamble_type,
   get_reg_domain_info                     :       _al_net_if_impl_get_reg_domain_info,
   set_reg_domain_info                     :       _al_net_if_impl_set_reg_domain_info,
   get_supported_channels                  :       _al_net_if_impl_get_supported_channels,
   set_dot11e_category_info                :   _al_net_if_impl_set_dot11e_category_info,
   set_tx_antenna                          :       _al_net_if_impl_set_tx_antenna,

   /**
    * OTHER
    */

   set_auth_hook                           :       _al_net_if_impl_set_auth_hook,
   set_assoc_hook                          :       _al_net_if_impl_set_assoc_hook,
   set_essid_info                          :       _al_net_if_impl_set_essid_info,
   set_radio_data                          :       _al_net_if_impl_set_radio_data,
   radio_diagnostic_command                :       _al_net_if_impl_radio_diagnostic_command,
   set_action_hook                         :       _al_net_if_impl_set_action_hook,
   send_action                             :       _al_net_if_impl_send_action,
   init_channels                           :       _al_net_if_init_channels,
   set_use_type                            :   _al_net_if_set_if_use_type,
   set_all_conf_complete                   :   _al_net_if_set_all_conf_complete,
   send_disassoc_from_station              :   _al_net_if_send_disassoc_from_station,
   set_ht_vht_capab                        :       _al_net_if_impl_set_ht_vht_capab,
   set_ht_operation                        :       _al_net_if_impl_set_ht_operation,
   set_vht_capab                        :       _al_net_if_impl_set_vht_capab,
   set_vht_operation                        :       _al_net_if_impl_set_vht_operation,
   init_rate_ctrl                          :     _al_net_if_impl_init_rate_ctrl,    
};

void queue_packet_on_txqueue(meshap_core_packet_t *core_packet, int nu_dq_pkt)
{
	tx_core_packet_t    *tx_core_packet;
    unsigned long flags;
    struct sk_buff *skb;
    MESH_USAGE_PROFILING_INIT
#if MULTIPLE_AP_THREAD
	dot1p_queue_entry_t *head;

	head = (dot1p_queue_entry_t *)core_packet->al_packet.entry;
	while(nu_dq_pkt--) {
		head = head->next;
		skb = skb_get(core_packet->skb);
		meshap_skb_enqueue(&tx_skb_queue, skb, &tx_Qsplock);
		AL_IMPL_ATOMIC_INC(q_packet_stats.tx_pktQ_total_packet_added_count);
		AL_IMPL_ATOMIC_INC(q_packet_stats.tx_pktQ_current_packet_count);
		if(q_packet_stats.tx_pktQ_current_packet_count.counter > q_packet_stats.tx_pktQ_current_packet_max_count.counter) {
			q_packet_stats.tx_pktQ_current_packet_max_count.counter = q_packet_stats.tx_pktQ_current_packet_count.counter;
		}
		al_release_packet(core_packet);
		if(head) {
			core_packet = (meshap_core_packet_t *)head->packet;
		}
	}
	al_set_event(AL_CONTEXT globals.tx_queue_event_handle);
#else
	skb = skb_get(core_packet->skb);
	meshap_skb_enqueue(&tx_skb_queue, skb, &tx_Qsplock);
    al_set_event(AL_CONTEXT globals.tx_queue_event_handle);
    AL_IMPL_ATOMIC_INC(q_packet_stats.tx_pktQ_total_packet_added_count);
    AL_IMPL_ATOMIC_INC(q_packet_stats.tx_pktQ_current_packet_count);
	if(q_packet_stats.tx_pktQ_current_packet_count.counter > q_packet_stats.tx_pktQ_current_packet_max_count.counter) {
		q_packet_stats.tx_pktQ_current_packet_max_count.counter = q_packet_stats.tx_pktQ_current_packet_count.counter;
	}
	al_release_packet(core_packet);
#endif
}

void tx_thread(void)
{
	struct  sk_buff *skb = NULL;
	static tx_core_packet_t *temp;
	meshap_core_packet_t *core_packet;
	int ret = 0;
    unsigned long flags;
	unsigned int thread_num = thread_stat.tx_thread;
	int pkt_poll = 0,quelen = 0;
	MESH_USAGE_PROFILING_INIT
#if MESH_CPU_PROFILE_ENABLED
	ktime_t start1;
#endif

	thread_stat.tx_thread++;
	if (thread_stat.tx_thread > MAX_TX_THREAD)
		printk(KERN_EMERG"tx_thread() running more than MAX_TX_THREAD\n");

	while(1) {
		al_reset_event(AL_CONTEXT globals.tx_queue_event_handle);
		ret = al_wait_on_event(AL_CONTEXT globals.tx_queue_event_handle, AL_WAIT_TIMEOUT_INFINITE);
		if (_TX_THREAD_SHOULD_BREAK)
		{
	        (tx_rx_pkt_stats.packet_drop[32])++;
			break;
		}
next_packet:
		thread_stat.tx_thread_count[thread_num]++;

		if(reboot_in_progress) {
		    printk(KERN_EMERG"-----Terminating 'tx_thread' thread-----\n");
			threads_status &= ~THREAD_STATUS_BIT2_MASK;
	        (tx_rx_pkt_stats.packet_drop[33])++;
			break;
		}
        skb = meshap_skb_dequeue(&temp_tx_skb_queue, NULL);
	    if(skb) {
			pkt_poll = 0;
            ret = dev_queue_xmit(skb);
            if(ret) {
                (tx_rx_pkt_stats.packet_drop[59])++;
            }
            q_packet_stats.temp_tx_pktQ_total_packet_removed_count++;
            q_packet_stats.temp_tx_pktQ_current_packet_count--;
	        goto next_packet;
		}else {
tryagain:
			quelen = skb_queue_len(&tx_skb_queue);
			if(quelen) {
				al_spin_lock_bh_t(&tx_Qsplock);
				quelen = skb_queue_len(&tx_skb_queue);
				meshap_splice_skb_queue(&tx_skb_queue, &temp_tx_skb_queue);
				al_spin_unlock_bh_t(&tx_Qsplock);
				AL_IMPL_ATOMIC_ADD(quelen, q_packet_stats.tx_pktQ_total_packet_removed_count);
				AL_IMPL_ATOMIC_SUB(quelen, q_packet_stats.tx_pktQ_current_packet_count);
				q_packet_stats.temp_tx_pktQ_total_packet_added_count +=quelen;
				q_packet_stats.temp_tx_pktQ_current_packet_count +=quelen;
				if(q_packet_stats.temp_tx_pktQ_current_packet_count > q_packet_stats.temp_tx_pktQ_current_packet_max_count) {
					q_packet_stats.temp_tx_pktQ_current_packet_max_count = q_packet_stats.temp_tx_pktQ_current_packet_count;
				}
				goto next_packet;
			}else {
				if(pkt_poll < PKT_POLL_RETRY_COUNT) {
					pkt_poll++;
					goto tryagain;
				}else {
					pkt_poll = 0;
					continue;
				}
			}
		}
	}
}

static int _transmit_helper(al_net_if_t *al_net_if, al_packet_t *packet, int type, int sub_type, int transmit_type)
{
   meshap_core_net_if_t *core_net_if;
   meshap_core_packet_t *core_packet;
   int ret;
   struct  sk_buff *skb = NULL;
   meshap_core_packet_t  *temp = NULL;
   unsigned char pindx, hindx;
   dot1p_queue_t       **queue;
   unsigned long flags;
   int num_dq_pkt = 0;
   MESH_USAGE_PROFILING_INIT
   ret = 0;

   if (AL_ATOMIC_GET(al_net_if->buffering_state) == AL_NET_IF_BUFFERING_STATE_DROP)
   {
	  (tx_rx_pkt_stats.packet_drop[34])++;
      goto _return;
   }

   AL_802_11_FC_SET_TYPE(packet->frame_control, type);
   AL_802_11_FC_SET_STYPE(packet->frame_control, sub_type);

   core_net_if = (meshap_core_net_if_t *)al_net_if;
   core_packet = (meshap_core_packet_t *)packet;

   if (core_net_if->dev_type == MESHAP_CORE_DEV_TYPE_VIRTUAL)
   {
      switch (core_net_if->virtual_dev_mode)
      {
      case MESHAP_VIRTUAL_DEV_MODE_MASTER:
         core_packet->al_packet.flags |= AL_PACKET_FLAGS_VIRTUAL_MASTER;
         break;

      default:
         core_packet->al_packet.flags |= AL_PACKET_FLAGS_VIRTUAL_INFRA;
      }
   }
   al_packet_impl_setup_skb(core_net_if, core_packet);

   core_packet->skb->dev = core_net_if->dev;

#if MULTIPLE_AP_THREAD
	if(core_packet->al_packet.queued_packet)
	{
	   core_packet->al_packet.status = PACKET_PROCESSED;
	   al_net_if->last_tx_time = al_get_tick_count();
	   pindx = 0;
	   hindx = 0;
	   queue    = (dot1p_queue_t **)globals.ap_queue->queue_data[pindx];
	   MESH_USAGE_PROFILE_START(start)
	   al_spin_lock(queue[hindx]->dot1p_Qsplock, flags);
	   MESH_USAGE_PROFILE_END(profiling_info.dot1p_Qsplock_4,index,start,end)
	   if (queue[hindx]->dot1p_queue_fhead && (core_packet == queue[hindx]->dot1p_queue_fhead->packet)) {
		   num_dq_pkt++;
		   queue[hindx]->dot1p_queue_fhead = queue[hindx]->dot1p_queue_fhead->next;
next:
		   while(1) {
			   if(queue[hindx]->dot1p_queue_fhead) {
				   temp = (meshap_core_packet_t *)queue[hindx]->dot1p_queue_fhead->packet;
				   if (temp && (temp->al_packet.status == PACKET_PROCESSED)) {
					   num_dq_pkt++;
					   queue[hindx]->dot1p_queue_fhead = queue[hindx]->dot1p_queue_fhead->next;
					   continue;
				   }else {
					   break;
				   }
			   }else {
				   break;
			   }
		   }
	       MESH_USAGE_PROFILE_START(start)
		   al_spin_unlock(queue[hindx]->dot1p_Qsplock, flags);
	       MESH_USAGE_PROFILE_END(profiling_info.dot1p_Qspunlock_4,index,start,end)
		   queue_packet_on_txqueue(core_packet, num_dq_pkt);
	   }else if(queue[hindx]->dot1p_queue_fhead){
			   core_packet = (meshap_core_packet_t *)queue[hindx]->dot1p_queue_fhead->packet;
			   if (core_packet && (core_packet->al_packet.status == PACKET_PROCESSED)) {
				   num_dq_pkt++;
				   queue[hindx]->dot1p_queue_fhead = queue[hindx]->dot1p_queue_fhead->next;
				   goto next;
			   }else {
	               MESH_USAGE_PROFILE_START(start)
				   al_spin_unlock(queue[hindx]->dot1p_Qsplock, flags);
	               MESH_USAGE_PROFILE_END(profiling_info.dot1p_Qspunlock_4,index,start,end)
			   }
	   }else {
	       MESH_USAGE_PROFILE_START(start)
		   al_spin_unlock(queue[hindx]->dot1p_Qsplock, flags);
	       MESH_USAGE_PROFILE_END(profiling_info.dot1p_Qspunlock_4,index,start,end)
	   }
	   return ret;
   }else {
	   /*for these packets no need to maintain ordering ex: probe resp*/
	   queue_packet_on_txqueue(core_packet, 1);
	   al_net_if->last_tx_time = al_get_tick_count();
	   return ret;
   }
#else
	queue_packet_on_txqueue(core_packet, 1);
	al_net_if->last_tx_time = al_get_tick_count();
	return ret;
#endif
_return:
	tx_rx_pkt_stats.tx_pkt++;
   al_release_packet(packet);
   al_net_if->last_tx_time = al_get_tick_count();

   return ret;
}


#define _SCAN_HELPER_TYPE_ACTIVE     1
#define _SCAN_HELPER_TYPE_PASSIVE    2
#define _SCAN_HELPER_TYPE_DEFAULT    3

static int _scan_helper(int scan_type, al_net_if_t *al_net_if, al_scan_access_point_info_t **access_points, int *count, int scan_duration_in_millis, int channel_count, unsigned char *channels)
{
   meshap_core_net_if_t *core_net_if;
   int res;
   meshap_scan_access_point_info_t *meshap_aps;
   al_scan_access_point_info_t     *al_aps;
   int ap_count;
   int i;

   core_net_if    = (meshap_core_net_if_t *)al_net_if;
   *count         = 0;
   *access_points = NULL;

   if (core_net_if->dev_type == MESHAP_CORE_DEV_TYPE_VIRTUAL)
   {
      core_net_if->dev_info->set_virtual_mode(core_net_if->dev, core_net_if->virtual_dev_mode);
   }

   switch (scan_type)
   {
   case _SCAN_HELPER_TYPE_ACTIVE:
      res = core_net_if->dev_info->scan_access_points_active(core_net_if->dev, &meshap_aps, &ap_count, scan_duration_in_millis, channel_count, channels);
      break;

   case _SCAN_HELPER_TYPE_PASSIVE:
      res = core_net_if->dev_info->scan_access_points_passive(core_net_if->dev, &meshap_aps, &ap_count, scan_duration_in_millis, channel_count, channels);
      break;

   case _SCAN_HELPER_TYPE_DEFAULT:
   default:
      res = core_net_if->dev_info->scan_access_points(core_net_if->dev, &meshap_aps, &ap_count, scan_duration_in_millis, channel_count, channels);
      break;
   }

   if (res)
   {
		al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : scan_type %d res %d %s : %d\n", scan_type, res, __func__, __LINE__);
      return res;
   }

   al_aps = (al_scan_access_point_info_t *)kmalloc(sizeof(al_scan_access_point_info_t) * ap_count, GFP_ATOMIC);

   if (al_aps == NULL)
   {
		al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : al_ops is NULL Memory not available %s : %d\n", __func__, __LINE__);
      return -1;
   }

   for (i = 0; i < ap_count; i++)
   {
      al_aps[i].mode = meshap_aps[i].mode;
      memcpy(al_aps[i].bssid.bytes, meshap_aps[i].bssid, ETH_ALEN);
      al_aps[i].bssid.length = ETH_ALEN;
      al_aps[i].channel      = meshap_aps[i].channel;
      strcpy(al_aps[i].essid, meshap_aps[i].essid);

      /**
       * Filter our negative RSSI values
       */
      if (meshap_aps[i].signal > 96)
      {
         meshap_aps[i].signal = 0;
      }

      al_aps[i].signal          = meshap_aps[i].signal;
      al_aps[i].noise           = meshap_aps[i].noise;
      al_aps[i].beacon_interval = meshap_aps[i].beacon_interval;
      al_aps[i].capabilities    = meshap_aps[i].capabilities;
      memcpy(al_aps[i].supported_rates, meshap_aps[i].supported_rates, AL_802_11_MAX_BIT_RATES);
      memcpy(al_aps[i].vendor_info, meshap_aps[i].vendor_info, meshap_aps[i].vendor_info_length);
      al_aps[i].vendor_info_length = meshap_aps[i].vendor_info_length;
	  memcpy(&al_aps[i].ap_ht_capab, &meshap_aps[i].ap_ht_capab, sizeof(al_802_11_ht_capabilities_t));
	  memcpy(&al_aps[i].ap_vht_capab, &meshap_aps[i].ap_vht_capab, sizeof(al_802_11_vht_capabilities_t));
   }

   kfree(meshap_aps);

   *access_points = al_aps;
   *count         = ap_count;

   return 0;
}

static void _on_round_robin_notify(AL_CONTEXT_PARAM_DECL struct net_device *dev, void *dev_token)
{
   meshap_core_net_if_t *core_net_if;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   core_net_if = (meshap_core_net_if_t *)dev_token;

   if (core_net_if->virtual_ds_if != NULL)
   {
      core_net_if = core_net_if->virtual_ds_if;
   }

   if (core_net_if->process_round_robin != NULL)
   {
      core_net_if->process_round_robin(&core_net_if->al_net_if);
   }
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
}


static void _on_radar_notify(struct net_device *dev, void *dev_token, int channel)
{
   meshap_core_net_if_t *core_net_if;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   core_net_if = (meshap_core_net_if_t *)dev_token;

   if (core_net_if->virtual_wm_if != NULL)
   {
      core_net_if = core_net_if->virtual_wm_if;
   }
   if (core_net_if->process_radar != NULL)
   {
      core_net_if->process_radar(&core_net_if->al_net_if, channel);
   }
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
}


static void _on_probe_request_notify(struct net_device *dev, void *dev_token, unsigned char *sta_addr, int rssi)
{
   meshap_core_net_if_t *core_net_if;
   al_net_addr_t        address;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   core_net_if = (meshap_core_net_if_t *)dev_token;

   if (core_net_if->virtual_wm_if != NULL)
   {
      core_net_if = core_net_if->virtual_wm_if;
   }

   memset(&address, 0, sizeof(al_net_addr_t));
   address.length = ETH_ALEN;
   memcpy(address.bytes, sta_addr, ETH_ALEN);

   core_net_if->process_probe_request(&core_net_if->al_net_if, &address, rssi);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
}


static void _al_net_if_impl_set_mesh_downlink_round_robin_time(al_net_if_t *al_net_if, int beacon_intervals)
{
   meshap_core_net_if_t *core_net_if;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   core_net_if = (meshap_core_net_if_t *)al_net_if;

   core_net_if->dev_info->set_mesh_downlink_round_robin_time(core_net_if->dev, beacon_intervals);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
}


static void _al_net_if_impl_add_downlink_round_robin_child(al_net_if_t *al_net_if, al_net_addr_t *addr)
{
   meshap_core_net_if_t *core_net_if;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   core_net_if = (meshap_core_net_if_t *)al_net_if;

   core_net_if->dev_info->add_downlink_round_robin_child(core_net_if->dev, addr->bytes);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
}


static void _al_net_if_impl_remove_downlink_round_robin_child(al_net_if_t *al_net_if, al_net_addr_t *addr)
{
   meshap_core_net_if_t *core_net_if;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   core_net_if = (meshap_core_net_if_t *)al_net_if;

   core_net_if->dev_info->remove_downlink_round_robin_child(core_net_if->dev, addr->bytes);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
}


static int _al_net_if_impl_virtual_associate(al_net_if_t *al_net_if, al_net_addr_t *ap_addr, int channel, int start)
{
   meshap_core_net_if_t *core_net_if;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   core_net_if = (meshap_core_net_if_t *)al_net_if;

   if (core_net_if->dev_type == MESHAP_CORE_DEV_TYPE_VIRTUAL)
   {
      core_net_if->dev_info->set_virtual_mode(core_net_if->dev, core_net_if->virtual_dev_mode);
   }

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return core_net_if->dev_info->virtual_associate(core_net_if->dev, ap_addr->bytes, channel, start);
}


static int _al_net_if_impl_get_duty_cycle_info(al_net_if_t *al_net_if, int channel_count, al_duty_cycle_info_t *info, unsigned int dwell_time_minis, int flag)
{
   meshap_core_net_if_t     *core_net_if;
   meshap_duty_cycle_info_t *c_info;
   int i, j;
   int ret;
   int in_channel_count;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   core_net_if      = (meshap_core_net_if_t *)al_net_if;
   in_channel_count = channel_count;

   if (core_net_if->dev_type == MESHAP_CORE_DEV_TYPE_VIRTUAL)
   {
      in_channel_count = 0; /* get duty cycle info only on current channel */
   }
   c_info = (meshap_duty_cycle_info_t *)kmalloc(sizeof(meshap_duty_cycle_info_t) * channel_count, GFP_ATOMIC);
   memset(c_info, 0, sizeof(meshap_duty_cycle_info_t) * channel_count);

   for (i = 0; i < channel_count; i++)
   {
      c_info[i].channel = info[i].channel;
   }

   ret = core_net_if->dev_info->get_duty_cycle_info(core_net_if->dev, in_channel_count, c_info, dwell_time_minis, flag);
   if (ret)
   {
       i = 0;
       while (c_info[i].aps)
       {
           kfree(c_info[i].aps);
           i++;
       }
       kfree(c_info);
       return -1;
   }

   for (i = 0; i < channel_count; i++)
   {
      info[i].avg_signal        = c_info[i].avg_signal;
      info[i].max_signal        = c_info[i].max_signal;
      info[i].number_of_aps     = c_info[i].number_of_aps;
      info[i].retry_duration    = c_info[i].retry_duration;
      info[i].total_duration    = c_info[i].total_duration;
      info[i].transmit_duration = c_info[i].transmit_duration;

      info[i].aps = al_heap_alloc(AL_CONTEXT sizeof(*info[i].aps) * info[i].number_of_aps AL_HEAP_DEBUG_PARAM);

      memset(info[i].aps, 0, sizeof(*info[i].aps) * info[i].number_of_aps);

      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "avg_sigi : %d\n", info[i].avg_signal);
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "max_sig  : %d\n", info[i].max_signal);
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "retry    : %d\n", info[i].retry_duration);
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "tot_dur  : %d\n", info[i].total_duration);
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "tra_dur  : %d\n", info[i].transmit_duration);
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "channel_num : %d ap_count: %d\n\n", info[i].channel, info[i].number_of_aps);

      for (j = 0; j < info[i].number_of_aps; j++)
      {
         info[i].aps[j].bssid.length = ETH_ALEN;
         memcpy(info[i].aps[j].bssid.bytes, c_info[i].aps[j].bssid, ETH_ALEN);

         memcpy(info[i].aps[j].essid, c_info[i].aps[j].essid, AL_802_11_ESSID_MAX_SIZE);
         info[i].aps[j].retry_duration    = c_info[i].aps[j].retry_duration;
         info[i].aps[j].transmit_duration = c_info[i].aps[j].transmit_duration;
         info[i].aps[j].signal            = c_info[i].aps[j].signal;

         al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "ret         : %d\n", info[i].aps[j].retry_duration);
         al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "tra         : %d\n", info[i].aps[j].transmit_duration);
         al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "signal      : %d\n", info[i].aps[j].signal);
         al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "bssid_len   : %d\n", info[i].aps[j].bssid.length);
         al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "bssid_bytes : %x:%x:%x:%x:%x:%x\n", info[i].aps[j].bssid.bytes[0], info[i].aps[j].bssid.bytes[1], info[i].aps[j].bssid.bytes[2], info[i].aps[j].bssid.bytes[3], info[i].aps[j].bssid.bytes[4], info[i].aps[j].bssid.bytes[5]);
         al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "essid       : %s\n", info[i].aps[j].essid);
      }
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "\n--------------------------\n");
      kfree(c_info[i].aps);
   }

   kfree(c_info);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);

   return 0;
}


static void _al_net_if_impl_set_rate_ctrl_parameters(al_net_if_t *al_net_if, al_rate_ctrl_param_t *rate_ctrl_param)
{
   meshap_core_net_if_t     *core_net_if;
   meshap_rate_ctrl_param_t param;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   core_net_if = (meshap_core_net_if_t *)al_net_if;

   param.max_error_percent  = rate_ctrl_param->max_error_percent;
   param.max_errors         = rate_ctrl_param->max_errors;
   param.max_retry_percent  = rate_ctrl_param->max_retry_percent;
   param.min_qualification  = rate_ctrl_param->min_qualification;
   param.min_retry_percent  = rate_ctrl_param->min_retry_percent;
   param.qualification      = rate_ctrl_param->qualification;
   param.t_el_max           = rate_ctrl_param->t_el_max;
   param.t_el_min           = rate_ctrl_param->t_el_min;
   param.initial_rate_index = rate_ctrl_param->initial_rate_index;

   core_net_if->dev_info->set_rate_ctrl_parameters(core_net_if->dev, &param);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
}


static void _al_net_if_impl_reset_rate_ctrl(al_net_if_t *al_net_if, al_net_addr_t *addr)
{
   meshap_core_net_if_t *core_net_if;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   core_net_if = (meshap_core_net_if_t *)al_net_if;
   core_net_if->dev_info->reset_rate_ctrl(core_net_if->dev, addr->bytes);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
}


static void _al_net_if_impl_get_rate_ctrl_info(al_net_if_t *al_net_if, al_net_addr_t *addr, al_rate_ctrl_info_t *rate_ctrl_info)
{
   meshap_core_net_if_t    *core_net_if;
   meshap_rate_ctrl_info_t meshap_rate_ctrl_info;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   core_net_if = (meshap_core_net_if_t *)al_net_if;

   memset(&meshap_rate_ctrl_info, 0, sizeof(meshap_rate_ctrl_info_t));

   if (core_net_if->dev_type == MESHAP_CORE_DEV_TYPE_VIRTUAL)
   {
      core_net_if->dev_info->set_virtual_mode(core_net_if->dev, core_net_if->virtual_dev_mode);
   }

   core_net_if->dev_info->get_rate_ctrl_info(core_net_if->dev, addr->bytes, &meshap_rate_ctrl_info);

   rate_ctrl_info->confidence   = meshap_rate_ctrl_info.confidence;
   rate_ctrl_info->outlook      = meshap_rate_ctrl_info.outlook;
   rate_ctrl_info->rate_in_mbps = meshap_rate_ctrl_info.rate_in_mbps;
   rate_ctrl_info->rate_index   = meshap_rate_ctrl_info.rate_index;
   rate_ctrl_info->tx_error     = meshap_rate_ctrl_info.tx_error;
   rate_ctrl_info->tx_retry     = meshap_rate_ctrl_info.tx_retry;
   rate_ctrl_info->tx_success   = meshap_rate_ctrl_info.tx_success;
   rate_ctrl_info->t_ch         = meshap_rate_ctrl_info.t_ch;
   rate_ctrl_info->t_elapsed    = meshap_rate_ctrl_info.t_elapsed;
   rate_ctrl_info->last_check   = meshap_rate_ctrl_info.last_check;

   /**
    * Filter our negative RSSI values
    */

   if (meshap_rate_ctrl_info.avg_ack_rssi > 96)
   {
      meshap_rate_ctrl_info.avg_ack_rssi = 0;
   }

   rate_ctrl_info->avg_ack_rssi = meshap_rate_ctrl_info.avg_ack_rssi;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
}


static void _al_net_if_impl_set_round_robin_notify_hook(al_net_if_t *al_net_if, al_round_robin_notify_t hook)
{
   meshap_core_net_if_t *core_net_if;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   core_net_if = (meshap_core_net_if_t *)al_net_if;

   core_net_if->process_round_robin = hook;

   core_net_if->dev_info->set_round_robin_notify_hook(core_net_if->dev, _on_round_robin_notify);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
}


static void _al_net_if_impl_enable_ds_verification_opertions(al_net_if_t *al_net_if, int enable)
{
   meshap_core_net_if_t *core_net_if;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   core_net_if = (meshap_core_net_if_t *)al_net_if;

   if (core_net_if->dev_type == MESHAP_CORE_DEV_TYPE_VIRTUAL)
   {
      core_net_if->dev_info->set_virtual_mode(core_net_if->dev, core_net_if->virtual_dev_mode);
   }

   core_net_if->dev_info->enable_ds_verification_opertions(core_net_if->dev, enable);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
}


static int _al_net_if_impl_dfs_scan(al_net_if_t *al_net_if)
{
   meshap_core_net_if_t *core_net_if;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   core_net_if = (meshap_core_net_if_t *)al_net_if;

   if (core_net_if->dev_type == MESHAP_CORE_DEV_TYPE_VIRTUAL)
   {
      core_net_if->dev_info->set_virtual_mode(core_net_if->dev, core_net_if->virtual_dev_mode);
   }

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return core_net_if->dev_info->dfs_scan(core_net_if->dev);
}


static void _al_net_if_impl_set_radar_notify_hook(al_net_if_t *al_net_if, al_radar_notify_t hook)
{
   meshap_core_net_if_t *core_net_if;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   core_net_if = (meshap_core_net_if_t *)al_net_if;

   core_net_if->process_radar = hook;

   core_net_if->dev_info->set_radar_notify_hook(core_net_if->dev, _on_radar_notify);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
}


static void _al_net_if_impl_set_probe_request_hook(al_net_if_t *al_net_if, al_on_probe_request_t on_probe_request)
{
   meshap_core_net_if_t *core_net_if;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   core_net_if = (meshap_core_net_if_t *)al_net_if;

   core_net_if->process_probe_request = on_probe_request;

   core_net_if->dev_info->set_probe_request_notify_hook(core_net_if->dev, _on_probe_request_notify);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
}


static int _al_net_if_impl_get_name(al_net_if_t *al_net_if, char *buffer, int length)
{
   int nl;

   nl = strlen(al_net_if->name);

   if (length <= nl)
   {
      return nl;
   }

   strcpy(buffer, al_net_if->name);

   return 0;
}


static int _al_net_if_impl_get_encapsulation(al_net_if_t *al_net_if)
{
   return al_net_if->encapsulation;
}


static int _al_net_if_impl_get_arp_hw_type(al_net_if_t *al_net_if)
{
   return al_net_if->arp_hw_type;
}


static int _al_net_if_impl_get_hw_addr_size(al_net_if_t *al_net_if)
{
   return ETH_ALEN;
}


static int _al_net_if_impl_get_mtu(al_net_if_t *al_net_if)
{
   return al_net_if->mtu;
}


static int _al_net_if_impl_get_config(al_net_if_t *al_net_if, al_net_if_config_t *config)
{
   memcpy(config, &al_net_if->config, sizeof(al_net_if_config_t));
   return 0;
}


static int _al_net_if_impl_get_stats(al_net_if_t *al_net_if, al_net_if_stat_t *stats)
{
   meshap_core_net_if_t *core_net_if;

   core_net_if = (meshap_core_net_if_t *)al_net_if;
   if (!core_net_if->dev_info->get_stats)
      return -1;

   return core_net_if->dev_info->get_stats(core_net_if->dev, stats);
}

static int _al_net_if_impl_get_debug_info(al_net_if_t *al_net_if, debug_infra_info_t *debug_infra_info)
{
   meshap_core_net_if_t *core_net_if;
   core_net_if = (meshap_core_net_if_t *)al_net_if;
   if (!core_net_if->dev_info->get_debug_info)
      return -1;
   return core_net_if->dev_info->get_debug_info(core_net_if->dev, debug_infra_info);
}


static int _al_net_if_impl_get_last_rx_time(al_net_if_t *al_net_if, al_u64_t *last_rx_time)
{
   return -1;      /**TODO*/
}


static int _al_net_if_impl_get_last_tx_time(al_net_if_t *al_net_if, al_u64_t *last_tx_time)
{
   return -1;      /**TODO*/
}


static int _al_net_if_impl_get_flags(al_net_if_t *al_net_if)
{
   meshap_core_net_if_t *core_net_if;
   int flags;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   core_net_if = (meshap_core_net_if_t *)al_net_if;
   flags       = 0;

   if (core_net_if->dev->flags & IFF_UP)
   {
      flags |= AL_NET_IF_FLAGS_UP;
   }
   if (core_net_if->dev->flags & IFF_PROMISC)
   {
      flags |= AL_NET_IF_FLAGS_PROMISC;
   }
   if (core_net_if->dev->flags & IFF_ALLMULTI)
   {
      flags |= AL_NET_IF_FLAGS_ALLMULTI | AL_NET_IF_FLAGS_MULTICAST;
   }
   if (core_net_if->link_flags & MESHAP_ON_LINK_NOTIFY_STATE_LINK_ON)
   {
      flags |= AL_NET_IF_FLAGS_LINKED;
   }
   if (core_net_if->link_flags & MESHAP_ON_LINK_NOTIFY_STATE_ASSOCIATED)
   {
      flags |= AL_NET_IF_FLAGS_ASSOCIATED;
   }
   if (core_net_if->link_flags & MESHAP_ON_LINK_NOTIFY_STATE_ACCESS_POINT)
   {
      flags |= AL_NET_IF_FLAGS_ACCESS_POINT;
   }

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return flags;
}


static int _al_net_if_impl_transmit(al_net_if_t *al_net_if, al_packet_t *packet)
{
   return _transmit_helper(al_net_if, packet, AL_802_11_FC_TYPE_DATA, AL_802_11_FC_STYPE_DATA, AL_NET_IF_TRANSMIT_TYPE_QUEUED);
}


static int _al_net_if_impl_set_filter_mode(al_net_if_t *al_net_if, int filter_mode)
{
   meshap_core_net_if_t *core_net_if;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   core_net_if = (meshap_core_net_if_t *)al_net_if;
   rtnl_lock();
   if (filter_mode & AL_NET_IF_FILTER_MODE_MULTICAST)
   {
      if (!(core_net_if->dev->flags & IFF_ALLMULTI))
      {
         dev_set_allmulti(core_net_if->dev, 1);
      }
   }
   else
   {
      if (core_net_if->dev->flags & IFF_ALLMULTI)
      {
         dev_set_allmulti(core_net_if->dev, 0);
      }
   }

   if (filter_mode & AL_NET_IF_FILTER_MODE_PROMISCOUS)
   {
      if (!(core_net_if->dev->flags & IFF_PROMISC))
      {
         dev_set_promiscuity(core_net_if->dev, 1);
      }
   }
   else
   {
      if (core_net_if->dev->flags & IFF_PROMISC)
      {
         dev_set_promiscuity(core_net_if->dev, 0);
      }
   }
   rtnl_unlock();
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return 0;
}


static void *_al_net_if_impl_get_extended_operations(al_net_if_t *al_net_if)
{
   meshap_core_net_if_t *core_net_if;

   core_net_if = (meshap_core_net_if_t *)al_net_if;

   if (core_net_if->dev_info->encapsulation == MESHAP_NET_DEV_ENCAPSULATION_ETHERNET)
   {
      return NULL;
   }

   return &_extended_operations;
}


static int _al_net_if_impl_start(al_net_if_t *al_net_if)
{
   return -1;      /**TODO*/
}


static int _al_net_if_impl_stop(al_net_if_t *al_net_if)
{
   return -1;      /**TODO*/
}


static int _al_net_if_impl_associate(al_net_if_t *al_net_if, al_net_addr_t *ap_addr, int channel, const char *essid, int length, int timeout, al_802_11_md_ie_t *md_ie_in, al_802_11_md_ie_t *md_ie_out)
{
   meshap_core_net_if_t *core_net_if;
   int           res;
   unsigned char ie_in[256];
   unsigned char ie_out[256];
   al_802_11_information_element_t ie;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   core_net_if = (meshap_core_net_if_t *)al_net_if;

   al_802_11_format_md_ie(AL_CONTEXT md_ie_in, &ie);

   ie_in[0] = ie.element_id;
   ie_in[1] = ie.length;
   memcpy(&ie_in[2], ie.data, ie.length);

   memset(ie_out, 0, sizeof(ie_out));
   memset(md_ie_out, 0, sizeof(al_802_11_md_ie_t));

   if (core_net_if->dev_type == MESHAP_CORE_DEV_TYPE_VIRTUAL)
   {
      core_net_if->dev_info->set_virtual_mode(core_net_if->dev, core_net_if->virtual_dev_mode);
   }

   res = core_net_if->dev_info->associate(core_net_if->dev,
                                          ap_addr->bytes,
                                          channel,
                                          essid,
                                          length,
                                          timeout,
                                          ie_in,
                                          ie.length + 2,
                                          ie_out);

   if (ie_out[0] == AL_802_11_EID_VENDOR_PRIVATE)
   {
      ie.element_id = ie_out[0];
      ie.length     = ie_out[1];
      memcpy(ie.data, &ie_out[2], ie.length);
      al_802_11_parse_md_ie(AL_CONTEXT & ie, md_ie_out);
   }
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return res;
}


static int _al_net_if_impl_dis_associate(al_net_if_t *al_net_if)
{
   meshap_core_net_if_t *core_net_if;
   int res;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   core_net_if = (meshap_core_net_if_t *)al_net_if;

   if (core_net_if->dev_type == MESHAP_CORE_DEV_TYPE_VIRTUAL)
   {
      core_net_if->dev_info->set_virtual_mode(core_net_if->dev, core_net_if->virtual_dev_mode);
   }

   res = core_net_if->dev_info->dis_associate(core_net_if->dev);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);

   return res;
}


static int _al_net_if_impl_get_bssid(al_net_if_t *al_net_if, al_net_addr_t *bssid)
{
   meshap_core_net_if_t *core_net_if;
   int res;
//   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   core_net_if   = (meshap_core_net_if_t *)al_net_if;
   bssid->length = ETH_ALEN;

   if (core_net_if->dev_type == MESHAP_CORE_DEV_TYPE_VIRTUAL)
   {
      core_net_if->dev_info->set_virtual_mode(core_net_if->dev, core_net_if->virtual_dev_mode);
   }

   res = core_net_if->dev_info->get_bssid(core_net_if->dev, bssid->bytes);

//   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return res;
}


static int _al_net_if_impl_send_management_frame(al_net_if_t *al_net_if, int sub_type, al_packet_t *packet)
{
   return _transmit_helper(al_net_if, packet, AL_802_11_FC_TYPE_MGMT, sub_type, AL_NET_IF_TRANSMIT_TYPE_QUEUED);
}


static int _al_net_if_impl_scan_access_points(al_net_if_t *al_net_if, al_scan_access_point_info_t **access_points, int *count, int scan_duration_in_millis, int channel_count, unsigned char *channels)
{
   return _scan_helper(_SCAN_HELPER_TYPE_DEFAULT, al_net_if, access_points, count, scan_duration_in_millis, channel_count, channels);
}


static int _al_net_if_impl_scan_access_points_active(al_net_if_t *al_net_if, al_scan_access_point_info_t **access_points, int *count, int scan_duration_in_millis, int channel_count, unsigned char *channels)
{
   return _scan_helper(_SCAN_HELPER_TYPE_ACTIVE, al_net_if, access_points, count, scan_duration_in_millis, channel_count, channels);
}


static int _al_net_if_impl_scan_access_points_passive(al_net_if_t *al_net_if, al_scan_access_point_info_t **access_points, int *count, int scan_duration_in_millis, int channel_count, unsigned char *channels)
{
   return _scan_helper(_SCAN_HELPER_TYPE_PASSIVE, al_net_if, access_points, count, scan_duration_in_millis, channel_count, channels);
}


static int _al_net_if_impl_set_management_frame_hook(al_net_if_t *al_net_if, al_process_management_frame_t hook)
{
   meshap_core_net_if_t *core_net_if;

   core_net_if = (meshap_core_net_if_t *)al_net_if;

   core_net_if->process_management_frame = hook;

   return 0;
}


static int _al_net_if_impl_set_beacon_hook(al_net_if_t *al_net_if, al_process_beacon_t hook)
{
   meshap_core_net_if_t *core_net_if;

   core_net_if = (meshap_core_net_if_t *)al_net_if;

   core_net_if->process_beacon = hook;

   return 0;
}


static int _al_net_if_impl_set_error_hook(al_net_if_t *al_net_if, al_process_packet_error_t hook)
{
   return -1;      /**TODO*/
}


static int _al_net_if_impl_get_mode(al_net_if_t *al_net_if)
{
   meshap_core_net_if_t *core_net_if;
   int mode;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   core_net_if = (meshap_core_net_if_t *)al_net_if;

   if (core_net_if->dev_type == MESHAP_CORE_DEV_TYPE_VIRTUAL)
   {
      if (core_net_if->virtual_dev_mode == MESHAP_VIRTUAL_DEV_MODE_MASTER)
      {
         al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
         return AL_802_11_MODE_MASTER;
      }
      else if (core_net_if->virtual_dev_mode == MESHAP_VIRTUAL_DEV_MODE_INFRA)
      {
         al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
         return AL_802_11_MODE_INFRA;
      }
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : Invalid core_net_if->virtual_dev_mode %d -- %s : %d\n",
                                                       core_net_if->virtual_dev_mode, __func__,__LINE__);
      return -1;
   }

   mode = core_net_if->dev_info->get_mode(core_net_if->dev);

   switch (mode)
   {
   case IW_MODE_MASTER:
      al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
      return AL_802_11_MODE_MASTER;

   case IW_MODE_INFRA:
      al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
      return AL_802_11_MODE_INFRA;
   }

   al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : Invalid mode %d -- %s : %d\n", mode, __func__,__LINE__);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return -1;
}


static int _al_net_if_impl_set_mode(al_net_if_t *al_net_if, int mode, unsigned char master_quiet)
{
   meshap_core_net_if_t *core_net_if;
   int val, ret;
 //  al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   core_net_if = (meshap_core_net_if_t *)al_net_if;

   switch (mode)
   {
   case AL_802_11_MODE_MASTER:

      val = IW_MODE_MASTER;
      core_net_if->link_flags = MESHAP_ON_LINK_NOTIFY_STATE_ACCESS_POINT
                                | MESHAP_ON_LINK_NOTIFY_STATE_LINK_ON;

      if (core_net_if->dev_type == MESHAP_CORE_DEV_TYPE_VIRTUAL)
      {
         core_net_if->virtual_dev_mode = MESHAP_VIRTUAL_DEV_MODE_MASTER;
      }

      break;

   case AL_802_11_MODE_RADIO_OFF:
   case AL_802_11_MODE_INFRA:

      val = IW_MODE_INFRA;
      core_net_if->link_flags &= ~MESHAP_ON_LINK_NOTIFY_STATE_ACCESS_POINT;

      if (core_net_if->dev_type == MESHAP_CORE_DEV_TYPE_VIRTUAL)
      {
         core_net_if->virtual_dev_mode = MESHAP_VIRTUAL_DEV_MODE_INFRA;
      }

      break;

   default:
		al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : Invalid mode %d -- %s : %d\n", mode, __func__,__LINE__);
      return -1;
   }
   ret = core_net_if->dev_info->set_mode(core_net_if->dev, val, master_quiet);
   if ((core_net_if->use_type == AL_CONF_IF_USE_TYPE_WM) && (core_net_if->al_net_if.encapsulation == AL_NET_IF_ENCAPSULATION_802_11))
   {
      access_point_enable_wm_encryption(AL_CONTEXT 1);
   }

//   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return ret;
}


static int _al_net_if_impl_get_essid(al_net_if_t *al_net_if, char *essid_buffer, int length)
{
   meshap_core_net_if_t *core_net_if;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   core_net_if = (meshap_core_net_if_t *)al_net_if;

   if (core_net_if->dev_type == MESHAP_CORE_DEV_TYPE_VIRTUAL)
   {
      core_net_if->dev_info->set_virtual_mode(core_net_if->dev, core_net_if->virtual_dev_mode);
   }

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return core_net_if->dev_info->get_essid(core_net_if->dev, essid_buffer, length);
}


static int _al_net_if_impl_set_essid(al_net_if_t *al_net_if, char *essid_buffer)
{
   meshap_core_net_if_t *core_net_if;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   core_net_if = (meshap_core_net_if_t *)al_net_if;

   if (core_net_if->dev_type == MESHAP_CORE_DEV_TYPE_VIRTUAL)
   {
      core_net_if->dev_info->set_virtual_mode(core_net_if->dev, core_net_if->virtual_dev_mode);
   }

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return core_net_if->dev_info->set_essid(core_net_if->dev, essid_buffer);
}


static int _al_net_if_impl_get_rts_threshold(al_net_if_t *al_net_if)
{
   meshap_core_net_if_t *core_net_if;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   core_net_if = (meshap_core_net_if_t *)al_net_if;

   if (core_net_if->dev_type == MESHAP_CORE_DEV_TYPE_VIRTUAL)
   {
      core_net_if->dev_info->set_virtual_mode(core_net_if->dev, core_net_if->virtual_dev_mode);
   }

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return core_net_if->dev_info->get_rts_threshold(core_net_if->dev);
}


static int _al_net_if_impl_set_rts_threshold(al_net_if_t *al_net_if, int threshold)
{
   meshap_core_net_if_t *core_net_if;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   core_net_if = (meshap_core_net_if_t *)al_net_if;

   if (core_net_if->dev_type == MESHAP_CORE_DEV_TYPE_VIRTUAL)
   {
      core_net_if->dev_info->set_virtual_mode(core_net_if->dev, core_net_if->virtual_dev_mode);
   }

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return core_net_if->dev_info->set_rts_threshold(core_net_if->dev, threshold);
}


static int _al_net_if_impl_get_frag_threshold(al_net_if_t *al_net_if)
{
   meshap_core_net_if_t *core_net_if;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   core_net_if = (meshap_core_net_if_t *)al_net_if;

   if (core_net_if->dev_type == MESHAP_CORE_DEV_TYPE_VIRTUAL)
   {
      core_net_if->dev_info->set_virtual_mode(core_net_if->dev, core_net_if->virtual_dev_mode);
   }

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return core_net_if->dev_info->get_frag_threshold(core_net_if->dev);
}


static int _al_net_if_impl_set_frag_threshold(al_net_if_t *al_net_if, int threshold)
{
   meshap_core_net_if_t *core_net_if;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   core_net_if = (meshap_core_net_if_t *)al_net_if;

   if (core_net_if->dev_type == MESHAP_CORE_DEV_TYPE_VIRTUAL)
   {
      core_net_if->dev_info->set_virtual_mode(core_net_if->dev, core_net_if->virtual_dev_mode);
   }

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return core_net_if->dev_info->set_frag_threshold(core_net_if->dev, threshold);
}


static int _al_net_if_impl_get_beacon_interval(al_net_if_t *al_net_if)
{
   meshap_core_net_if_t *core_net_if;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   core_net_if = (meshap_core_net_if_t *)al_net_if;

   if (core_net_if->dev_type == MESHAP_CORE_DEV_TYPE_VIRTUAL)
   {
      core_net_if->dev_info->set_virtual_mode(core_net_if->dev, core_net_if->virtual_dev_mode);
   }

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return core_net_if->dev_info->get_beacon_interval(core_net_if->dev);
}


static int _al_net_if_impl_set_beacon_interval(al_net_if_t *al_net_if, int interval)
{
   meshap_core_net_if_t *core_net_if;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   core_net_if = (meshap_core_net_if_t *)al_net_if;

   if (core_net_if->dev_type == MESHAP_CORE_DEV_TYPE_VIRTUAL)
   {
      core_net_if->dev_info->set_virtual_mode(core_net_if->dev, core_net_if->virtual_dev_mode);
   }

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return core_net_if->dev_info->set_beacon_interval(core_net_if->dev, interval);
}


static int _al_net_if_impl_set_security_info(al_net_if_t *al_net_if, al_802_11_security_info_t *security_info)
{
   meshap_core_net_if_t *core_net_if;
   unsigned char        rsn_ie_buffer[256];
   int rsn_ie_length;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   core_net_if = (meshap_core_net_if_t *)al_net_if;

   if (core_net_if->dev_type == MESHAP_CORE_DEV_TYPE_VIRTUAL)
   {
      core_net_if->dev_info->set_virtual_mode(core_net_if->dev, core_net_if->virtual_dev_mode);
   }

   if (security_info->wep.enabled)
   {
      core_net_if->dev_info->enable_wep(core_net_if->dev);
   }
   else
   {
      core_net_if->dev_info->disable_wep(core_net_if->dev);
   }

   if (security_info->rsn.enabled)
   {
      rsn_ie_length = al_802_11_format_rsn_ie(AL_CONTEXT security_info, NULL, rsn_ie_buffer, 256);
      core_net_if->dev_info->set_rsn_ie(core_net_if->dev, rsn_ie_buffer, rsn_ie_length);
   }
   else
   {
      core_net_if->dev_info->set_rsn_ie(core_net_if->dev, NULL, 0);
   }

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return 0;
}


static int _al_net_if_impl_set_security_key(al_net_if_t *al_net_if, al_802_11_security_key_info_t *security_key_info, int enable_compression)
{
   meshap_security_key_info_t key_info;
   meshap_core_net_if_t       *core_net_if;
   meshap_security_key_data_t meshap_security_key_data;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

	if (mesh_config->disable_backhaul_security)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MESH_AP : INFO : disable_backhaul_security is enabled so "
				"not setting security key %s : %d\n", __func__,__LINE__);
		return 0;
   }

   core_net_if = (meshap_core_net_if_t *)al_net_if;

   if (core_net_if->dev_type == MESHAP_CORE_DEV_TYPE_VIRTUAL)
   {
      core_net_if->dev_info->set_virtual_mode(core_net_if->dev, core_net_if->virtual_dev_mode);
   }

   memset(&key_info, 0, sizeof(meshap_security_key_info_t));

   switch (security_key_info->cipher_type)
   {
   case AL_802_11_SECURITY_INFO_RSN_CIPHER_WEP:
      key_info.cipher_type = MESHAP_SECURITY_KEY_INFO_CIPHER_WEP;
      break;

   case AL_802_11_SECURITY_INFO_RSN_CIPHER_TKIP:
      key_info.cipher_type = MESHAP_SECURITY_KEY_INFO_CIPHER_TKIP;
      break;

   case AL_802_11_SECURITY_INFO_RSN_CIPHER_CCMP:
      key_info.cipher_type = MESHAP_SECURITY_KEY_INFO_CIPHER_CCMP;
      break;
   }

   memcpy(key_info.destination, security_key_info->destination.bytes, ETH_ALEN);
   memcpy(key_info.key, security_key_info->key, sizeof(key_info.key));

   key_info.key_index = security_key_info->key_index;
   key_info.strength  = security_key_info->strength;
   key_info.vlan_tag  = security_key_info->vlan_tag;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return core_net_if->dev_info->set_security_key(core_net_if->dev, &key_info, enable_compression);
}


static int _al_net_if_impl_release_security_key(al_net_if_t *al_net_if, int index, const u8 *mac_addr)
{
   meshap_core_net_if_t *core_net_if;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   core_net_if = (meshap_core_net_if_t *)al_net_if;

   if (core_net_if->dev_type == MESHAP_CORE_DEV_TYPE_VIRTUAL)
   {
      core_net_if->dev_info->set_virtual_mode(core_net_if->dev, core_net_if->virtual_dev_mode);
   }

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return core_net_if->dev_info->release_security_key(core_net_if->dev, index, mac_addr);
}


static int _al_net_if_impl_get_security_key_data(al_net_if_t *al_net_if, int index, al_802_11_security_key_data_t *security_key_data, const u8 *mac_addr)
{
   meshap_core_net_if_t       *core_net_if;
   meshap_security_key_data_t meshap_security_key_data;
   int ret;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   core_net_if = (meshap_core_net_if_t *)al_net_if;

   if (core_net_if->dev_type == MESHAP_CORE_DEV_TYPE_VIRTUAL)
   {
      core_net_if->dev_info->set_virtual_mode(core_net_if->dev, core_net_if->virtual_dev_mode);
   }

   ret =
      core_net_if->dev_info->get_security_key_data(core_net_if->dev, index, &meshap_security_key_data, mac_addr);

   if (ret == 0)
   {
      switch (meshap_security_key_data.cipher_type)
      {
      case MESHAP_SECURITY_KEY_INFO_CIPHER_WEP:
         security_key_data->cipher_type = AL_802_11_SECURITY_INFO_RSN_CIPHER_WEP;
         break;

      case MESHAP_SECURITY_KEY_INFO_CIPHER_TKIP:
         security_key_data->cipher_type = AL_802_11_SECURITY_INFO_RSN_CIPHER_TKIP;
         break;

      case MESHAP_SECURITY_KEY_INFO_CIPHER_CCMP:
         security_key_data->cipher_type = AL_802_11_SECURITY_INFO_RSN_CIPHER_CCMP;
         break;

      default:
         ret = -1;
      }
      memcpy(&security_key_data->key_rsc, &meshap_security_key_data.key_rsc, sizeof(security_key_data->key_rsc));
      memcpy(&security_key_data->key_tsc, &meshap_security_key_data.key_tsc, sizeof(security_key_data->key_tsc));
      memcpy(&security_key_data->wep_iv, &meshap_security_key_data.wep_iv, sizeof(security_key_data->wep_iv));
   }

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return ret;
}


static int _al_net_if_impl_set_ds_security_key(al_net_if_t *al_net_if, unsigned char *group_key, unsigned char group_key_type, unsigned char group_key_index, unsigned char *pairwise_key, int enable_compression)
{
   meshap_core_net_if_t *core_net_if;
   unsigned char        meshap_group_key_type;
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

	if (mesh_config->disable_backhaul_security)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MESH_AP : INFO : disable_backhaul_security is enabled so "
				"not setting ds security key %s : %d\n", __func__,__LINE__);
		return 0;
   }

   core_net_if = (meshap_core_net_if_t *)al_net_if;

   if (core_net_if->dev_type == MESHAP_CORE_DEV_TYPE_VIRTUAL)
   {
      core_net_if->dev_info->set_virtual_mode(core_net_if->dev, core_net_if->virtual_dev_mode);
   }

   switch (group_key_type)
   {
   case AL_802_11_SECURITY_INFO_RSN_CIPHER_NONE:
      meshap_group_key_type = MESHAP_SECURITY_KEY_INFO_CIPHER_NONE;
      break;

   case AL_802_11_SECURITY_INFO_RSN_CIPHER_WEP:
      meshap_group_key_type = MESHAP_SECURITY_KEY_INFO_CIPHER_WEP;
      break;

   case AL_802_11_SECURITY_INFO_RSN_CIPHER_WEP_104:
      meshap_group_key_type = MESHAP_SECURITY_KEY_INFO_CIPHER_WEP_104;
      break;

   case AL_802_11_SECURITY_INFO_RSN_CIPHER_TKIP:
      meshap_group_key_type = MESHAP_SECURITY_KEY_INFO_CIPHER_TKIP;
      break;

   case AL_802_11_SECURITY_INFO_RSN_CIPHER_CCMP:
      meshap_group_key_type = MESHAP_SECURITY_KEY_INFO_CIPHER_CCMP;
      break;

   default:
      meshap_group_key_type = MESHAP_SECURITY_KEY_INFO_CIPHER_NONE;
   }

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return core_net_if->dev_info->set_ds_security_key(core_net_if->dev,
                                                     group_key,
                                                     meshap_group_key_type,
                                                     group_key_index,
                                                     pairwise_key,
                                                     enable_compression);
}


static int _al_net_if_impl_get_default_capabilities(al_net_if_t *al_net_if)
{
   meshap_core_net_if_t *core_net_if;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   core_net_if = (meshap_core_net_if_t *)al_net_if;

   if (core_net_if->dev_type == MESHAP_CORE_DEV_TYPE_VIRTUAL)
   {
      core_net_if->dev_info->set_virtual_mode(core_net_if->dev, core_net_if->virtual_dev_mode);
   }

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return core_net_if->dev_info->get_default_capabilities(core_net_if->dev);
}


static int _al_net_if_impl_get_capabilities(al_net_if_t *al_net_if)
{
   meshap_core_net_if_t *core_net_if;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   core_net_if = (meshap_core_net_if_t *)al_net_if;

   if (core_net_if->dev_type == MESHAP_CORE_DEV_TYPE_VIRTUAL)
   {
      core_net_if->dev_info->set_virtual_mode(core_net_if->dev, core_net_if->virtual_dev_mode);
   }

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return core_net_if->dev_info->get_capabilities(core_net_if->dev);
}


static int _al_net_if_impl_get_slot_time_type(al_net_if_t *al_net_if)
{
   meshap_core_net_if_t *core_net_if;
   int ret;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   core_net_if = (meshap_core_net_if_t *)al_net_if;

   if (core_net_if->dev_type == MESHAP_CORE_DEV_TYPE_VIRTUAL)
   {
      core_net_if->dev_info->set_virtual_mode(core_net_if->dev, core_net_if->virtual_dev_mode);
   }

   ret = core_net_if->dev_info->get_slot_time_type(core_net_if->dev);

   switch (ret)
   {
   case WLAN_SLOT_TIME_TYPE_LONG:
      al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
      return AL_802_11_SLOT_TIME_TYPE_LONG;

   case WLAN_SLOT_TIME_TYPE_SHORT:
      al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
      return AL_802_11_SLOT_TIME_TYPE_SHORT;
   }

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return -1;
}


static int _al_net_if_impl_set_slot_time_type(al_net_if_t *al_net_if, int slot_time_type)
{
   meshap_core_net_if_t *core_net_if;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   core_net_if = (meshap_core_net_if_t *)al_net_if;

   if (core_net_if->dev_type == MESHAP_CORE_DEV_TYPE_VIRTUAL)
   {
      core_net_if->dev_info->set_virtual_mode(core_net_if->dev, core_net_if->virtual_dev_mode);
   }

   switch (slot_time_type)
   {
   case AL_802_11_SLOT_TIME_TYPE_LONG:
      al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
      return core_net_if->dev_info->set_slot_time_type(core_net_if->dev, WLAN_SLOT_TIME_TYPE_LONG);

   case AL_802_11_SLOT_TIME_TYPE_SHORT:
      al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
      return core_net_if->dev_info->set_slot_time_type(core_net_if->dev, WLAN_SLOT_TIME_TYPE_SHORT);
   }

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return -1;
}


static int _al_net_if_impl_get_erp_info(al_net_if_t *al_net_if)
{
   meshap_core_net_if_t *core_net_if;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   core_net_if = (meshap_core_net_if_t *)al_net_if;

   if (core_net_if->dev_type == MESHAP_CORE_DEV_TYPE_VIRTUAL)
   {
      core_net_if->dev_info->set_virtual_mode(core_net_if->dev, core_net_if->virtual_dev_mode);
   }

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return core_net_if->dev_info->get_erp_info(core_net_if->dev);
}


static int _al_net_if_impl_set_erp_info(al_net_if_t *al_net_if, int erp_info)
{
   meshap_core_net_if_t *core_net_if;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   core_net_if = (meshap_core_net_if_t *)al_net_if;

   if (core_net_if->dev_type == MESHAP_CORE_DEV_TYPE_VIRTUAL)
   {
      core_net_if->dev_info->set_virtual_mode(core_net_if->dev, core_net_if->virtual_dev_mode);
   }

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return core_net_if->dev_info->set_erp_info(core_net_if->dev, erp_info);
}


static int _al_net_if_impl_set_beacon_vendor_info(al_net_if_t *al_net_if, unsigned char *vendor_info_buffer, int length)
{
   meshap_core_net_if_t *core_net_if;
   core_net_if = (meshap_core_net_if_t *)al_net_if;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   if (core_net_if->use_type == AL_CONF_IF_USE_TYPE_AP)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
      return 0;
   }

   if (core_net_if->dev_type == MESHAP_CORE_DEV_TYPE_VIRTUAL)
   {
      core_net_if->dev_info->set_virtual_mode(core_net_if->dev, core_net_if->virtual_dev_mode);
   }

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return core_net_if->dev_info->set_beacon_vendor_info(core_net_if->dev, vendor_info_buffer, length);
}


static int _al_net_if_impl_get_supported_rates(al_net_if_t *al_net_if, unsigned char *supported_rates, int length)
{
   meshap_core_net_if_t *core_net_if;
   const unsigned char  *supported_rates_local;
   int supported_rates_length;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   core_net_if = (meshap_core_net_if_t *)al_net_if;

   if (core_net_if->dev_type == MESHAP_CORE_DEV_TYPE_VIRTUAL)
   {
      core_net_if->dev_info->set_virtual_mode(core_net_if->dev, core_net_if->virtual_dev_mode);
   }

   supported_rates_local = core_net_if->dev_info->get_supported_rates(core_net_if->dev, &supported_rates_length);

   memcpy(supported_rates, supported_rates_local, supported_rates_length);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return supported_rates_length;
}

static int _al_net_if_impl_get_hw_support_ht_capab(al_net_if_t *net_if, tddi_ht_cap_t *ht_cap, tddi_vht_cap_t *vht_cap, unsigned char sub_type)
{
	meshap_core_net_if_t *core_net_if;

	core_net_if = (meshap_core_net_if_t *)net_if;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

	if (core_net_if->dev_type == MESHAP_CORE_DEV_TYPE_VIRTUAL)
	{
		core_net_if->dev_info->set_virtual_mode(core_net_if->dev, core_net_if->virtual_dev_mode);
	}

	core_net_if->dev_info->get_hw_support_ht_capab(core_net_if->dev,ht_cap,vht_cap,sub_type);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
	return 0;
}

static int _al_net_if_impl_set_reboot_in_progress_state(al_net_if_t *net_if, int value)
{
	meshap_core_net_if_t *core_net_if;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);
	core_net_if = (meshap_core_net_if_t *)net_if;

	core_net_if->dev_info->set_reboot_in_progress_state(value);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
	return 0;
}

static int _al_net_if_impl_get_wm_duty_cycle_flag(al_net_if_t *net_if, int duty_cycle_flag, int operating_channel)
{
    meshap_core_net_if_t *core_net_if;

    core_net_if = (meshap_core_net_if_t *)net_if;
    al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

    core_net_if->dev_info->get_wm_duty_cycle_flag(core_net_if->dev, duty_cycle_flag, operating_channel);

    al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
    return 0;
}

static int _al_net_if_impl_get_bit_rate(al_net_if_t *al_net_if)
{
   meshap_core_net_if_t *core_net_if;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);
   core_net_if = (meshap_core_net_if_t *)al_net_if;

   if (core_net_if->dev_type == MESHAP_CORE_DEV_TYPE_VIRTUAL)
   {
      core_net_if->dev_info->set_virtual_mode(core_net_if->dev, core_net_if->virtual_dev_mode);
   }

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return core_net_if->dev_info->get_bit_rate(core_net_if->dev);
}


static int _al_net_if_impl_set_bit_rate(al_net_if_t *al_net_if, int bit_rate)
{
   meshap_core_net_if_t *core_net_if;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   core_net_if = (meshap_core_net_if_t *)al_net_if;

   if (core_net_if->dev_type == MESHAP_CORE_DEV_TYPE_VIRTUAL)
   {
      core_net_if->dev_info->set_virtual_mode(core_net_if->dev, core_net_if->virtual_dev_mode);
   }

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return core_net_if->dev_info->set_bit_rate(core_net_if->dev, bit_rate);
}


static void _al_net_if_impl_get_rate_table(al_net_if_t *al_net_if, al_rate_table_t *rate_table)
{
   meshap_core_net_if_t *core_net_if;
   meshap_rate_table_t  meshap_rate_table;
   int i;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   core_net_if = (meshap_core_net_if_t *)al_net_if;

   if (core_net_if->dev_type == MESHAP_CORE_DEV_TYPE_VIRTUAL)
   {
      core_net_if->dev_info->set_virtual_mode(core_net_if->dev, core_net_if->virtual_dev_mode);
   }

   core_net_if->dev_info->get_rate_table(core_net_if->dev, &meshap_rate_table);

   memset(rate_table, 0, sizeof(al_rate_table_t));

   rate_table->count = meshap_rate_table.count;

   for (i = 0; i < meshap_rate_table.count; i++)
   {
      rate_table->rates[i].index        = meshap_rate_table.rates[i].index;
      rate_table->rates[i].rate_in_mbps = meshap_rate_table.rates[i].rate_in_mbps;
   }
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
}


static int _al_net_if_impl_get_tx_power(al_net_if_t *al_net_if, al_802_11_tx_power_info_t *power_info)
{
   meshap_core_net_if_t   *core_net_if;
   meshap_tx_power_info_t pi;
   int res;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   core_net_if = (meshap_core_net_if_t *)al_net_if;

   if (core_net_if->dev_type == MESHAP_CORE_DEV_TYPE_VIRTUAL)
   {
      core_net_if->dev_info->set_virtual_mode(core_net_if->dev, core_net_if->virtual_dev_mode);
   }

   res = core_net_if->dev_info->get_tx_power(core_net_if->dev, &pi);

   if (res)
   {
      return res;
   }

   power_info->flags = AL_802_11_TXPOW_PERC;
   power_info->power = pi.power;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return 0;
}


static int _al_net_if_impl_set_tx_power(al_net_if_t *al_net_if, al_802_11_tx_power_info_t *power_info)
{
   meshap_core_net_if_t   *core_net_if;
   meshap_tx_power_info_t pi;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   core_net_if = (meshap_core_net_if_t *)al_net_if;
   pi.flags    = power_info->flags;
   pi.power    = power_info->power;

   if (core_net_if->dev_type == MESHAP_CORE_DEV_TYPE_VIRTUAL)
   {
      core_net_if->dev_info->set_virtual_mode(core_net_if->dev, core_net_if->virtual_dev_mode);
   }

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return core_net_if->dev_info->set_tx_power(core_net_if->dev, &pi);
}


static int _al_net_if_impl_get_channel_count(al_net_if_t *al_net_if)
{
   meshap_core_net_if_t *core_net_if;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);
   core_net_if = (meshap_core_net_if_t *)al_net_if;

   if (core_net_if->dev_type == MESHAP_CORE_DEV_TYPE_VIRTUAL)
   {
      core_net_if->dev_info->set_virtual_mode(core_net_if->dev, core_net_if->virtual_dev_mode);
   }

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return core_net_if->dev_info->get_channel_count(core_net_if->dev);
}


static int _al_net_if_impl_get_channel(al_net_if_t *al_net_if)
{
   meshap_core_net_if_t *core_net_if;

   core_net_if = (meshap_core_net_if_t *)al_net_if;

   return core_net_if->dev_info->get_channel(core_net_if->dev);
}


static int _al_net_if_impl_set_channel(al_net_if_t *al_net_if, int channel)
{
   meshap_core_net_if_t *core_net_if;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   core_net_if = (meshap_core_net_if_t *)al_net_if;

   if (core_net_if->dev_type == MESHAP_CORE_DEV_TYPE_VIRTUAL)
   {
      core_net_if->dev_info->set_virtual_mode(core_net_if->dev, core_net_if->virtual_dev_mode);
   }

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return core_net_if->dev_info->set_channel(core_net_if->dev, channel);
}


static int _al_net_if_impl_get_phy_mode(al_net_if_t *al_net_if)
{
   meshap_core_net_if_t *core_net_if;
   int ret;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   core_net_if = (meshap_core_net_if_t *)al_net_if;

   if ((core_net_if == NULL) || (core_net_if->dev_info == NULL) ||
       (core_net_if->dev == NULL))
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : Either core_net_if or core_net_if->dev_info or "
				"core_net_if->dev is NULL %s : %d\n", __func__,__LINE__);
      return -1;
   }

   ret = core_net_if->dev_info->get_phy_mode(core_net_if->dev);

   switch (ret)
   {
   case WLAN_PHY_MODE_802_11_A:
   	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
      return AL_802_11_PHY_MODE_A;

   case WLAN_PHY_MODE_802_11_B:
   	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
      return AL_802_11_PHY_MODE_B;

   case WLAN_PHY_MODE_802_11_G:
   	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
      return AL_802_11_PHY_MODE_BG;

   case WLAN_PHY_MODE_802_11_PURE_G:
   	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
      return AL_802_11_PHY_MODE_G;

   case WLAN_PHY_MODE_ATHEROS_TURBO:
   	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
      return AL_802_11_PHY_MODE_ATHEROS_TURBO;

   case WLAN_PHY_MODE_802_11_PSQ:
   	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
      return AL_802_11_PHY_MODE_PSQ;

   case WLAN_PHY_MODE_802_11_PSH:
   	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
      return AL_802_11_PHY_MODE_PSH;

   case WLAN_PHY_MODE_802_11_PSF:
   	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
      return AL_802_11_PHY_MODE_PSF;
   }

   al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : Invalid phy mode %d -- %s : %d\n", ret, __func__,__LINE__);
   return -1;
}


static int _al_net_if_impl_set_phy_mode(al_net_if_t *al_net_if, int phy_mode)
{
   meshap_core_net_if_t *core_net_if;
   int ret = -1;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   core_net_if = (meshap_core_net_if_t *)al_net_if;

   if (core_net_if->dev_type == MESHAP_CORE_DEV_TYPE_VIRTUAL)
   {
      core_net_if->dev_info->set_virtual_mode(core_net_if->dev, core_net_if->virtual_dev_mode);
   }

   core_net_if->al_net_if.phy_hw_type = phy_mode ;

   switch (phy_mode)
   {
   case AL_802_11_PHY_MODE_A:
      ret = core_net_if->dev_info->set_phy_mode(core_net_if->dev, WLAN_PHY_MODE_802_11_A);
      break;
   case AL_802_11_PHY_MODE_B:
      ret = core_net_if->dev_info->set_phy_mode(core_net_if->dev, WLAN_PHY_MODE_802_11_B);
      break;
   case AL_802_11_PHY_MODE_G:
      ret = core_net_if->dev_info->set_phy_mode(core_net_if->dev, WLAN_PHY_MODE_802_11_PURE_G);
      break;
   case AL_802_11_PHY_MODE_BG:
      ret = core_net_if->dev_info->set_phy_mode(core_net_if->dev, WLAN_PHY_MODE_802_11_G);
      break;
   case AL_802_11_PHY_MODE_ATHEROS_TURBO:
      ret = core_net_if->dev_info->set_phy_mode(core_net_if->dev, WLAN_PHY_MODE_ATHEROS_TURBO);
      break;
   case AL_802_11_PHY_MODE_PSQ:
      ret = core_net_if->dev_info->set_phy_mode(core_net_if->dev, WLAN_PHY_MODE_802_11_PSQ);
      break;
   case AL_802_11_PHY_MODE_PSH:
      ret = core_net_if->dev_info->set_phy_mode(core_net_if->dev, WLAN_PHY_MODE_802_11_PSH);
      break;
   case AL_802_11_PHY_MODE_PSF:
      ret = core_net_if->dev_info->set_phy_mode(core_net_if->dev, WLAN_PHY_MODE_802_11_PSF);
      break;
   case AL_802_11_PHY_MODE_2_4GHZ:
      ret = core_net_if->dev_info->set_phy_mode(core_net_if->dev, WLAN_PHY_MODE_802_11_N_2_4_GHZ);
      break;
    case AL_802_11_PHY_MODE_5_GHZ:
      ret = core_net_if->dev_info->set_phy_mode(core_net_if->dev, WLAN_PHY_MODE_802_11_N_5_GHZ);
      break;
   case AL_802_11_PHY_MODE_AC:
      ret = core_net_if->dev_info->set_phy_mode(core_net_if->dev, WLAN_PHY_MODE_802_11_AC);
      break;
   case AL_802_11_PHY_MODE_BGN:
      ret = core_net_if->dev_info->set_phy_mode(core_net_if->dev, WLAN_PHY_MODE_802_11_BGN);
      break;
   case AL_802_11_PHY_MODE_AN:
      ret = core_net_if->dev_info->set_phy_mode(core_net_if->dev, WLAN_PHY_MODE_802_11_AN);
      break;
   case AL_802_11_PHY_MODE_ANAC:
      ret = core_net_if->dev_info->set_phy_mode(core_net_if->dev, WLAN_PHY_MODE_802_11_ANAC);
      break;
   default:
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : ifname %s Invalid phy mode %d -- %s : %d\n",
                   core_net_if->dev->name, phy_mode, __func__,__LINE__);
      break;
   }

   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"MESH_AP : INFO : ifname %s ret %d %s : %d\n", core_net_if->dev->name, ret, __func__, __LINE__);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return ret;
}


static int _al_net_if_impl_get_preamble_type(al_net_if_t *al_net_if)
{
   meshap_core_net_if_t *core_net_if;
   int ret;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   core_net_if = (meshap_core_net_if_t *)al_net_if;

   if (core_net_if->dev_type == MESHAP_CORE_DEV_TYPE_VIRTUAL)
   {
      core_net_if->dev_info->set_virtual_mode(core_net_if->dev, core_net_if->virtual_dev_mode);
   }

   ret = core_net_if->dev_info->get_preamble_type(core_net_if->dev);

   switch (ret)
   {
   case WLAN_PREAMBLE_TYPE_LONG:
      al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
      return AL_802_11_PREAMBLE_TYPE_LONG;

   case WLAN_PREAMBLE_TYPE_SHORT:
      al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
      return AL_802_11_PREAMBLE_TYPE_SHORT;
   }

   al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : Invalid preamble_type %d -- %s : %d\n", ret, __func__,__LINE__);
   return -1;
}


static int _al_net_if_impl_set_preamble_type(al_net_if_t *al_net_if, int preamble_type)
{
   meshap_core_net_if_t *core_net_if;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   core_net_if = (meshap_core_net_if_t *)al_net_if;

   if (core_net_if->dev_type == MESHAP_CORE_DEV_TYPE_VIRTUAL)
   {
      core_net_if->dev_info->set_virtual_mode(core_net_if->dev, core_net_if->virtual_dev_mode);
   }

   switch (preamble_type)
   {
   case AL_802_11_PREAMBLE_TYPE_LONG:
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"MESH_AP : INFO : ifname %s setting preamble_type LONG %s : %d\n",
                        core_net_if->dev->name, __func__, __LINE__);
      al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
      return core_net_if->dev_info->set_preamble_type(core_net_if->dev, WLAN_PREAMBLE_TYPE_LONG);

   case AL_802_11_PREAMBLE_TYPE_SHORT:
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"MESH_AP : INFO : ifname %s setting preamble_type SHORT %d %s : %d\n",
                        core_net_if->dev->name,  __func__, __LINE__);
      al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
      return core_net_if->dev_info->set_preamble_type(core_net_if->dev, WLAN_PREAMBLE_TYPE_SHORT);
   }

   al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : ifname %s invalid preamble_type %d -- %s : %d\n",
                 core_net_if->dev->name, preamble_type, __func__,__LINE__);
   return -1;
}


static int _al_net_if_impl_set_auth_hook(al_net_if_t *al_net_if, al_process_auth_t auth_hook)
{
   meshap_core_net_if_t *core_net_if;

   core_net_if = (meshap_core_net_if_t *)al_net_if;

   core_net_if->process_auth = auth_hook;

   return 0;
}


static int _al_net_if_impl_set_assoc_hook(al_net_if_t *al_net_if, al_process_assoc_t assoc_hook)
{
   meshap_core_net_if_t *core_net_if;

   core_net_if = (meshap_core_net_if_t *)al_net_if;

   core_net_if->process_assoc = assoc_hook;

   return 0;
}


static int _al_net_if_impl_transmit_ex(struct al_net_if *al_net_if, al_packet_t *packet, int transmit_type)
{
   if(IS_NETIF_11NAC(al_net_if))
        return _transmit_helper(al_net_if, packet, AL_802_11_FC_TYPE_DATA, AL_802_11_FC_STYPE_QOS, transmit_type);

   return _transmit_helper(al_net_if, packet, AL_802_11_FC_TYPE_DATA, AL_802_11_FC_STYPE_DATA, transmit_type);
}


static int _al_net_if_impl_set_hw_addr(struct al_net_if *al_net_if, al_net_addr_t *hw_addr)
{
   meshap_core_net_if_t *core_net_if;
   int                  net_if_count;
   al_net_if_t          *net_if;
   meshap_core_net_if_t *next_core_net_if;
   int                  i;
   int                  ret;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   core_net_if = (meshap_core_net_if_t *)al_net_if;

   if (core_net_if->dev_type == MESHAP_CORE_DEV_TYPE_VIRTUAL)
   {
      net_if_count = al_get_net_if_count(AL_CONTEXT);

      /**
       * Loop through all registered interfaces and find
       * VIRTUAL net_if's that have the same ->dev.
       * For such net_if's set the MAC address
       */

      for (i = 0; i < net_if_count; i++)
      {
         net_if = al_get_net_if(AL_CONTEXT i);

         if (!strcmp(net_if->name, al_net_if->name))
         {
            continue;
         }

         next_core_net_if = (meshap_core_net_if_t *)net_if;

         if (next_core_net_if->dev != core_net_if->dev)
         {
            continue;
         }

         if (next_core_net_if->dev_type == MESHAP_CORE_DEV_TYPE_VIRTUAL)
         {
            memcpy(&net_if->config.hw_addr, hw_addr, sizeof(al_net_addr_t));
         }
      }
   }
   ret = core_net_if->dev_info->set_hw_addr(core_net_if->dev, hw_addr->bytes);
   if (ret)
   {
      clear_bit(__LINK_STATE_START, &core_net_if->dev->state);
      struct ifreq ifr;
      memcpy(ifr.ifr_hwaddr.sa_data, hw_addr->bytes, ETH_ALEN);
      ret = core_net_if->dev_info->set_hw_addr(core_net_if->dev, hw_addr->bytes);
      if (core_net_if->dev || core_net_if->name || (strlen(core_net_if->name) != 0))
      {
         ret = core_net_if->dev_info->set_hw_addr(core_net_if->dev, &ifr.ifr_hwaddr);
         al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"MESH_AP : INFO : iface %s MAC "MACSTR" %s : %d\n",
                 core_net_if->dev->name, MAC2STR(ifr.ifr_hwaddr.sa_data), __func__, __LINE__);
         set_bit(__LINK_STATE_START, &core_net_if->dev->state);
      }
   }
   memcpy(&al_net_if->config.hw_addr, hw_addr, sizeof(al_net_addr_t));
   al_net_if->config.hw_addr.length = ETH_ALEN;
   memcpy(core_net_if->dev->dev_addr, &al_net_if->config.hw_addr, sizeof(al_net_addr_t));
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return 0;
}


static al_u64_t _al_net_if_impl_get_last_beacon_time(al_net_if_t *al_net_if)
{
   meshap_core_net_if_t *core_net_if;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   core_net_if = (meshap_core_net_if_t *)al_net_if;

   if (core_net_if->dev_type == MESHAP_CORE_DEV_TYPE_VIRTUAL)
   {
      core_net_if->dev_info->set_virtual_mode(core_net_if->dev, core_net_if->virtual_dev_mode);
   }

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return core_net_if->dev_info->get_last_beacon_time(core_net_if->dev);
}


void al_net_if_initialize_interface(meshap_core_net_if_t *core_net_if)
{
   al_net_if_t *net_if;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   net_if = &core_net_if->al_net_if;

   AL_ATOMIC_SET(net_if->buffering_state, AL_NET_IF_BUFFERING_STATE_NONE);

   memcpy(net_if->config.hw_addr.bytes, core_net_if->dev->dev_addr, ETH_ALEN);
   net_if->config.hw_addr.length   = ETH_ALEN;
   net_if->config.ext_config_info  = NULL;
   net_if->config.allowed_vlan_tag = AL_NET_IF_CONFIG_ALLOWED_VLAN_ALL;

   strcpy(net_if->name, core_net_if->name);

   switch (core_net_if->dev_info->encapsulation)
   {
   case MESHAP_NET_DEV_ENCAPSULATION_ETHERNET:
   case MESHAP_NET_DEV_ENCAPSULATION_802_3:
      net_if->encapsulation = AL_NET_IF_ENCAPSULATION_802_3;
      break;

   case MESHAP_NET_DEV_ENCAPSULATION_802_11:
      net_if->encapsulation = AL_NET_IF_ENCAPSULATION_802_11;
      break;
   }

   net_if->mtu = core_net_if->dev->mtu;

   switch (core_net_if->dev_info->phy_hw_type)
   {
   case MESHAP_NET_DEV_PHY_HW_TYPE_802_3:
      net_if->phy_hw_type = AL_NET_IF_PHY_HW_TYPE_802_3;
      break;

   case MESHAP_NET_DEV_PHY_HW_TYPE_802_11_B:
      net_if->phy_hw_type = AL_NET_IF_PHY_HW_TYPE_802_11_B;
      break;

   case MESHAP_NET_DEV_PHY_HW_TYPE_802_11_A:
      net_if->phy_hw_type = AL_NET_IF_PHY_HW_TYPE_802_11_A;
      break;

   case MESHAP_NET_DEV_PHY_HW_TYPE_802_11_B_G:
      net_if->phy_hw_type = AL_NET_IF_PHY_HW_TYPE_802_11_G_B;
      break;

   case MESHAP_NET_DEV_PHY_HW_TYPE_802_11_G:
      net_if->phy_hw_type = AL_NET_IF_PHY_HW_TYPE_802_11_G_B;
      break;

   case MESHAP_NET_DEV_PHY_HW_TYPE_802_11_ATHEROS_TURBO:
      net_if->phy_hw_type = AL_NET_IF_PHY_HW_TYPE_802_11_A;
      break;


   case MESHAP_NET_DEV_PHY_HW_TYPE_802_11_N:
      net_if->phy_hw_type = AL_NET_IF_PHY_HW_TYPE_802_11_N;
      break;

   case MESHAP_NET_DEV_PHY_HW_TYPE_802_11_AC:
      net_if->phy_hw_type = AL_NET_IF_PHY_HW_TYPE_802_11_AC;
      break;

   case MESHAP_NET_DEV_PHY_HW_TYPE_802_11_BGN:
      net_if->phy_hw_type = AL_NET_IF_PHY_HW_TYPE_802_11_BGN;
      break;

   case MESHAP_NET_DEV_PHY_HW_TYPE_802_11_AN:
      net_if->phy_hw_type = AL_NET_IF_PHY_HW_TYPE_802_11_AN;
      break;

   case MESHAP_NET_DEV_PHY_HW_TYPE_802_11_ANAC:
      net_if->phy_hw_type = AL_NET_IF_PHY_HW_TYPE_802_11_ANAC;
      break;
   }


   net_if->get_name                = _al_net_if_impl_get_name;
   net_if->get_encapsulation       = _al_net_if_impl_get_encapsulation;
   net_if->get_arp_hw_type         = _al_net_if_impl_get_arp_hw_type;
   net_if->get_hw_addr_size        = _al_net_if_impl_get_hw_addr_size;
   net_if->get_mtu                 = _al_net_if_impl_get_mtu;
   net_if->get_config              = _al_net_if_impl_get_config;
   net_if->get_stats               = _al_net_if_impl_get_stats;
   net_if->get_debug_info          = _al_net_if_impl_get_debug_info;
   net_if->get_last_rx_time        = _al_net_if_impl_get_last_rx_time;
   net_if->get_last_tx_time        = _al_net_if_impl_get_last_tx_time;
   net_if->get_flags               = _al_net_if_impl_get_flags;
   net_if->transmit                = _al_net_if_impl_transmit;
   net_if->set_filter_mode         = _al_net_if_impl_set_filter_mode;
   net_if->get_extended_operations = _al_net_if_impl_get_extended_operations;
   net_if->start       = _al_net_if_impl_start;
   net_if->stop        = _al_net_if_impl_stop;
   net_if->transmit_ex = _al_net_if_impl_transmit_ex;
   net_if->set_hw_addr = _al_net_if_impl_set_hw_addr;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
}


void al_net_if_initialize_virtual_interface(meshap_core_net_if_t *virtual_core_net_if, meshap_core_net_if_t *phy_core_net_if)
{
   al_net_if_t *v_net_if;
   al_net_if_t *p_net_if;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   v_net_if = &virtual_core_net_if->al_net_if;
   p_net_if = &phy_core_net_if->al_net_if;

   AL_ATOMIC_SET(v_net_if->buffering_state, AL_NET_IF_BUFFERING_STATE_NONE);

   memcpy(v_net_if->config.hw_addr.bytes, phy_core_net_if->dev->dev_addr, ETH_ALEN);
   v_net_if->config.hw_addr.length  = ETH_ALEN;
   v_net_if->config.ext_config_info = NULL;

   strcpy(v_net_if->name, virtual_core_net_if->name);

   v_net_if->encapsulation = p_net_if->encapsulation;
   v_net_if->mtu           = virtual_core_net_if->dev->mtu;
   v_net_if->phy_hw_type   = p_net_if->phy_hw_type;


   v_net_if->get_name                = _al_net_if_impl_get_name;
   v_net_if->get_encapsulation       = _al_net_if_impl_get_encapsulation;
   v_net_if->get_arp_hw_type         = _al_net_if_impl_get_arp_hw_type;
   v_net_if->get_hw_addr_size        = _al_net_if_impl_get_hw_addr_size;
   v_net_if->get_mtu                 = _al_net_if_impl_get_mtu;
   v_net_if->get_config              = _al_net_if_impl_get_config;
   v_net_if->get_stats               = _al_net_if_impl_get_stats;
   v_net_if->get_debug_info          = _al_net_if_impl_get_debug_info;
   v_net_if->get_last_rx_time        = _al_net_if_impl_get_last_rx_time;
   v_net_if->get_last_tx_time        = _al_net_if_impl_get_last_tx_time;
   v_net_if->get_flags               = _al_net_if_impl_get_flags;
   v_net_if->transmit                = _al_net_if_impl_transmit;
   v_net_if->set_filter_mode         = _al_net_if_impl_set_filter_mode;
   v_net_if->get_extended_operations = _al_net_if_impl_get_extended_operations;
   v_net_if->start       = _al_net_if_impl_start;
   v_net_if->stop        = _al_net_if_impl_stop;
   v_net_if->transmit_ex = _al_net_if_impl_transmit_ex;
   v_net_if->set_hw_addr = _al_net_if_impl_set_hw_addr;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
}


static int _al_net_if_impl_set_essid_info(al_net_if_t *al_net_if, al_802_11_security_info_t *net_if_security_info, int count, al_802_11_essid_info_t *info)
{
   meshap_core_net_if_t *core_net_if;
   meshap_essid_info_t  *essid_info;
   int i;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   core_net_if = (meshap_core_net_if_t *)al_net_if;

   if (core_net_if->dev_type == MESHAP_CORE_DEV_TYPE_VIRTUAL)
   {
      core_net_if->dev_info->set_virtual_mode(core_net_if->dev, core_net_if->virtual_dev_mode);
   }

   essid_info = (meshap_essid_info_t *)kmalloc(sizeof(meshap_essid_info_t) * count, GFP_ATOMIC);

   if (essid_info == NULL)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : essid_info NULL Memory not available %s : %d\n", __func__,__LINE__);
      return -1;
   }

   memset(essid_info, 0, sizeof(meshap_essid_info_t) * count);

   for (i = 0; i < count; i++)
   {
      strcpy(essid_info[i].essid, info[i].essid);
      essid_info[i].vlan_tag = info[i].vlan_tag;

      if (info[i].sec_info.wep.enabled == 1)
      {
         essid_info[i].sec_info.flags |= MESHAP_ESSID_SECURITY_INFO_WEP_ENABLED;
      }

      if (info[i].sec_info.rsn.enabled == 1)
      {
         essid_info[i].sec_info.rsn_ie_length = al_802_11_format_rsn_ie(AL_CONTEXT & (info[i].sec_info),
                                                                        net_if_security_info,
                                                                        essid_info[i].sec_info.rsn_ie_buffer,
                                                                        256);
         essid_info[i].sec_info.flags |= MESHAP_ESSID_SECURITY_INFO_RSN_ENABLED;
      }
   }

   core_net_if->dev_info->set_essid_info(core_net_if->dev, count, essid_info);

   kfree(essid_info);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return 0;
}


static int _al_net_if_impl_get_ack_timeout(al_net_if_t *al_net_if)
{
   meshap_core_net_if_t *core_net_if;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   core_net_if = (meshap_core_net_if_t *)al_net_if;

   if (core_net_if->dev_type == MESHAP_CORE_DEV_TYPE_VIRTUAL)
   {
      core_net_if->dev_info->set_virtual_mode(core_net_if->dev, core_net_if->virtual_dev_mode);
   }

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return core_net_if->dev_info->get_ack_timeout(core_net_if->dev);
}


static int _al_net_if_impl_set_ack_timeout(al_net_if_t *al_net_if, unsigned short timeout)
{
   meshap_core_net_if_t *core_net_if;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   core_net_if = (meshap_core_net_if_t *)al_net_if;

   if (core_net_if->dev_type == MESHAP_CORE_DEV_TYPE_VIRTUAL)
   {
      core_net_if->dev_info->set_virtual_mode(core_net_if->dev, core_net_if->virtual_dev_mode);
   }

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return core_net_if->dev_info->set_ack_timeout(core_net_if->dev, timeout);
}


static int _al_net_if_impl_get_reg_domain_info(al_net_if_t *al_net_if, al_reg_domain_info_t *reg_domain_info)
{
   meshap_core_net_if_t *core_net_if;
   int ret;
   meshap_reg_domain_info_t info;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   core_net_if = (meshap_core_net_if_t *)al_net_if;

   if (core_net_if->dev_type == MESHAP_CORE_DEV_TYPE_VIRTUAL)
   {
      core_net_if->dev_info->set_virtual_mode(core_net_if->dev, core_net_if->virtual_dev_mode);
   }

   ret = core_net_if->dev_info->get_reg_domain_info(core_net_if->dev, &info);

   if (ret == 0)
   {
      reg_domain_info->country_code = info.country_code;
      reg_domain_info->reg_domain   = info.reg_domain;
   	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   }

   al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : Invalid reg domain info %d -- %s : %d\n", ret, __func__,__LINE__);
   return ret;
}


static int _al_net_if_impl_set_reg_domain_info(al_net_if_t *al_net_if, al_reg_domain_info_t *reg_domain_info)
{
   meshap_core_net_if_t *core_net_if;
   int ret;
   meshap_reg_domain_info_t info;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   core_net_if = (meshap_core_net_if_t *)al_net_if;

   if (core_net_if->dev_type == MESHAP_CORE_DEV_TYPE_VIRTUAL)
   {
      core_net_if->dev_info->set_virtual_mode(core_net_if->dev, core_net_if->virtual_dev_mode);
   }

   info.country_code           = reg_domain_info->country_code;
   info.reg_domain             = reg_domain_info->reg_domain;
   info.bandwidth              = reg_domain_info->bandwidth;
   info.channel_count          = reg_domain_info->channel_count;
   info.frequencies            = reg_domain_info->frequencies;
   info.channel_numbers        = reg_domain_info->channel_numbers;
   info.channel_flags          = reg_domain_info->channel_flags;
   info.max_power              = reg_domain_info->max_power;
   info.antenna_max            = reg_domain_info->antenna_max;
   info.conformance_test_limit = reg_domain_info->conformance_test_limit;

   ret = core_net_if->dev_info->set_reg_domain_info(core_net_if->dev, &info);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return ret;
}


static int _al_net_if_impl_get_supported_channels(al_net_if_t *al_net_if, al_channel_info_t **supported_channels, int *count)
{
   meshap_core_net_if_t  *core_net_if;
   meshap_channel_info_t *meshap_channels;
   al_channel_info_t     *al_channels;
   int channel_count;
   int i, ret;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   core_net_if = (meshap_core_net_if_t *)al_net_if;

   if (core_net_if->dev_type == MESHAP_CORE_DEV_TYPE_VIRTUAL)
   {
      core_net_if->dev_info->set_virtual_mode(core_net_if->dev, core_net_if->virtual_dev_mode);
   }

   *count = 0;
   *supported_channels = NULL;

   ret = core_net_if->dev_info->get_supported_channels(core_net_if->dev, &meshap_channels, &channel_count);


   if (ret)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : Failed to get supported channels %s : %d\n", __func__,__LINE__);
      return ret;
   }

   al_channels = (al_channel_info_t *)kmalloc(sizeof(al_channel_info_t) * channel_count, GFP_ATOMIC);

   if (al_channels == NULL)
   {
   	al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : al_channels NULL Memory not available %s : %d\n", __func__,__LINE__);
      return -1;
   }

   for (i = 0; i < channel_count; i++)
   {
      al_channels[i].flags         = meshap_channels[i].flags;
      al_channels[i].frequency     = meshap_channels[i].frequency;
      al_channels[i].number        = meshap_channels[i].number;
      al_channels[i].max_power     = meshap_channels[i].max_power;
      al_channels[i].min_power     = meshap_channels[i].min_power;
      al_channels[i].max_reg_power = meshap_channels[i].max_reg_power;
   }

   kfree(meshap_channels);

   *supported_channels = al_channels;
   *count = channel_count;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return 0;
}


static int _al_net_if_impl_get_hide_ssid(al_net_if_t *al_net_if)
{
   meshap_core_net_if_t *core_net_if;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   core_net_if = (meshap_core_net_if_t *)al_net_if;

   if (core_net_if->dev_type == MESHAP_CORE_DEV_TYPE_VIRTUAL)
   {
      core_net_if->dev_info->set_virtual_mode(core_net_if->dev, core_net_if->virtual_dev_mode);
   }

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return core_net_if->dev_info->get_hide_ssid(core_net_if->dev);
}


static int _al_net_if_impl_set_hide_ssid(al_net_if_t *al_net_if, int hide)
{
   meshap_core_net_if_t *core_net_if;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   core_net_if = (meshap_core_net_if_t *)al_net_if;

   if (core_net_if->dev_type == MESHAP_CORE_DEV_TYPE_VIRTUAL)
   {
      core_net_if->dev_info->set_virtual_mode(core_net_if->dev, core_net_if->virtual_dev_mode);
   }

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return core_net_if->dev_info->set_hide_ssid(core_net_if->dev, hide);
}


static int _al_net_if_impl_set_dot11e_category_info(al_net_if_t *al_net_if, al_dot11e_category_info_t *dot11e_info)
{
   int ret;
   int i;
   meshap_dot11e_category_info_t info;
   meshap_core_net_if_t          *core_net_if;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   core_net_if = (meshap_core_net_if_t *)al_net_if;

   info.count = dot11e_info->count;

   info.category_info = (meshap_dot11e_category_details_t *)kmalloc(sizeof(meshap_dot11e_category_details_t) * info.count, GFP_ATOMIC);
   memset(info.category_info, 0, sizeof(meshap_dot11e_category_details_t) * info.count);

   for (i = 0; i < info.count; i++)
   {
      info.category_info[i].category        = dot11e_info->category_info[i].category;
      info.category_info[i].burst_time      = dot11e_info->category_info[i].burst_time;
      info.category_info[i].acwmin          = dot11e_info->category_info[i].acwmin;
      info.category_info[i].acwmax          = dot11e_info->category_info[i].acwmax;
      info.category_info[i].aifsn           = dot11e_info->category_info[i].aifsn;
      info.category_info[i].disable_backoff = dot11e_info->category_info[i].disable_backoff;
   }

   if (core_net_if->dev_type == MESHAP_CORE_DEV_TYPE_VIRTUAL)
   {
      core_net_if->dev_info->set_virtual_mode(core_net_if->dev, core_net_if->virtual_dev_mode);
   }

   ret = core_net_if->dev_info->set_dot11e_category_info(core_net_if->dev, &info);

   kfree(info.category_info);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return ret;
}


static int _al_net_if_impl_set_tx_antenna(al_net_if_t *al_net_if, int antenna_index)
{
   meshap_core_net_if_t *core_net_if;

   core_net_if = (meshap_core_net_if_t *)al_net_if;

   return core_net_if->dev_info->set_tx_antenna(core_net_if->dev, antenna_index);
}


static int _al_net_if_impl_set_radio_data(al_net_if_t *al_net_if, unsigned char *radio_data, int data_length)
{
   meshap_core_net_if_t *core_net_if;

   core_net_if = (meshap_core_net_if_t *)al_net_if;

   return core_net_if->dev_info->set_radio_data(core_net_if->dev, radio_data, data_length);
}


static int _al_net_if_impl_radio_diagnostic_command(al_net_if_t *al_net_if, unsigned char *diag_data_in, int data_in_length, unsigned char *diag_data_out, int data_out_buf_len)
{
   meshap_core_net_if_t *core_net_if;

   core_net_if = (meshap_core_net_if_t *)al_net_if;

   return core_net_if->dev_info->radio_diagnostic_command(core_net_if->dev,
                                                          diag_data_in,
                                                          data_in_length,
                                                          diag_data_out,
                                                          data_out_buf_len);
}


static int _al_net_if_impl_enable_beaconing_uplink(al_net_if_t *al_net_if, unsigned char *vendor_info_buffer, int length)
{
   meshap_core_net_if_t *core_net_if;

   core_net_if = (meshap_core_net_if_t *)al_net_if;

   return core_net_if->dev_info->enable_beaconing_uplink(core_net_if->dev, vendor_info_buffer, length);
}


static int _al_net_if_impl_disable_beaconing_uplink(al_net_if_t *al_net_if)
{
   meshap_core_net_if_t *core_net_if;

   core_net_if = (meshap_core_net_if_t *)al_net_if;

   return core_net_if->dev_info->disable_beaconing_uplink(core_net_if->dev);
}


static void _on_action_notify(struct net_device *dev, void *dev_token, unsigned char *sender, unsigned char *data, int length, int iw_mode)
{
   meshap_core_net_if_t *core_net_if;
   al_net_addr_t        address;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   core_net_if = (meshap_core_net_if_t *)dev_token;

   if ((iw_mode == IW_MODE_MASTER) && (core_net_if->virtual_wm_if != NULL))
   {
      core_net_if = core_net_if->virtual_wm_if;
   }
   else if ((iw_mode == IW_MODE_INFRA) && (core_net_if->virtual_ds_if != NULL))
   {
      core_net_if = core_net_if->virtual_ds_if;
   }

   if (core_net_if == NULL)
   {
      return;
   }

   if (core_net_if->process_action == NULL)
   {
      return;
   }

   memset(&address, 0, sizeof(al_net_addr_t));
   address.length = ETH_ALEN;
   memcpy(address.bytes, sender, ETH_ALEN);

   core_net_if->process_action(&core_net_if->al_net_if, &address, data, length);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
}


static int _al_net_if_impl_set_action_hook(al_net_if_t *al_net_if, al_action_notify_t action_hook)
{
   meshap_core_net_if_t *core_net_if;

   core_net_if = (meshap_core_net_if_t *)al_net_if;

   core_net_if->process_action = action_hook;

   core_net_if->dev_info->set_action_hook(core_net_if->dev, _on_action_notify);

   return 0;
}


static int _al_net_if_set_if_use_type(al_net_if_t *al_net_if, int use_type)
{
   meshap_core_net_if_t *core_net_if;

   core_net_if = (meshap_core_net_if_t *)al_net_if;
   core_net_if->dev_info->set_use_type(core_net_if->dev, use_type);
   return 0;
}


static int _al_net_if_set_all_conf_complete(al_net_if_t *al_net_if, int value)
{
   meshap_core_net_if_t *core_net_if;

   core_net_if = (meshap_core_net_if_t *)al_net_if;
   core_net_if->dev_info->set_all_conf_complete(core_net_if->dev, value);
   return 0;
}


static int _al_net_if_impl_send_deauth(al_net_if_t *al_net_if, u8 *address0, u8 *address1)
{
   meshap_core_net_if_t *core_net_if;

   core_net_if = (meshap_core_net_if_t *)al_net_if;
   if (core_net_if->use_type == AL_CONF_IF_USE_TYPE_AP) {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MESHAP : INFO : Not sending deauth as AP interface %s <%d>\n", __func__, __LINE__);
      return 0;
   }
   core_net_if->dev_info->send_deauth(core_net_if->dev, address0, address1);
   return 0;
}


static int _al_net_if_send_disassoc_from_station(al_net_if_t *al_net_if, int flag)
{
   meshap_core_net_if_t *core_net_if;

   core_net_if = (meshap_core_net_if_t *)al_net_if;
   if (core_net_if->use_type == AL_CONF_IF_USE_TYPE_AP) {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MESHAP : INFO : Not sending disassoc as AP interface %s <%d>\n", __func__, __LINE__);
      return 0;
   }
   return core_net_if->dev_info->send_disassoc_from_station(core_net_if->dev, flag);
}


static int _al_net_if_impl_send_auth_response(al_net_if_t *al_net_if, u8 *address0, u8 *address1)
{
   meshap_core_net_if_t *core_net_if;

   core_net_if = (meshap_core_net_if_t *)al_net_if;
   core_net_if->dev_info->send_auth_response(core_net_if->dev, address0, address1);
   return 0;
}


static int _al_net_if_impl_send_assoc_response(al_net_if_t *al_net_if, unsigned short stype, u8 *address0, u8 *address1, unsigned short aid,
                                               unsigned char ie_out_valid, al_802_11_information_element_t *ie_out, meshap_mac80211_sta_info_t sta)
{
   meshap_core_net_if_t *core_net_if;

   core_net_if = (meshap_core_net_if_t *)al_net_if;
   printk("@@@ %s line:%d ifname:%s\n", __func__, __LINE__, core_net_if->al_net_if.name);
   core_net_if->dev_info->send_assoc_response(core_net_if->dev, stype, address0, address1, aid, ie_out_valid, ie_out, sta);
   return 0;
}


static int _al_net_if_impl_del_station(al_net_if_t *al_net_if, u8 *station_addr)
{
   meshap_core_net_if_t *core_net_if;

   core_net_if = (meshap_core_net_if_t *)al_net_if;
   core_net_if->dev_info->del_station(core_net_if->dev, station_addr);
   return 0;
}


static int _al_net_if_init_channels(al_net_if_t *al_net_if)
{
   meshap_core_net_if_t *core_net_if;

   core_net_if = (meshap_core_net_if_t *)al_net_if;
   core_net_if->dev_info->init_channels(core_net_if->dev);
   return 0;
}


static int _al_net_if_impl_send_action(al_net_if_t *al_net_if, al_net_addr_t *addr, unsigned char *buffer, int length)
{
   meshap_core_net_if_t *core_net_if;

   core_net_if = (meshap_core_net_if_t *)al_net_if;

   if (core_net_if->dev_type == MESHAP_CORE_DEV_TYPE_VIRTUAL)
   {
      core_net_if->dev_info->set_virtual_mode(core_net_if->dev, core_net_if->virtual_dev_mode);
   }

   return core_net_if->dev_info->send_action(core_net_if->dev, addr->bytes, buffer, length);
}

/*  Abstraction Layer Function to handle HT & VHT capablities */ 
static int _al_net_if_impl_set_ht_vht_capab(al_net_if_t *al_net_if, al_net_if_ht_vht_capab_t* ht_vht_capab)
{
    meshap_core_net_if_t *core_net_if;
    int phy_mode = 0;
    al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

    core_net_if = (meshap_core_net_if_t *)al_net_if;

    if (core_net_if->dev_type == MESHAP_CORE_DEV_TYPE_VIRTUAL)
    {
        core_net_if->dev_info->set_virtual_mode(core_net_if->dev, core_net_if->virtual_dev_mode);
    }
    phy_mode = meshap_is_N_or_AC_supported_interface(core_net_if->al_net_if.phy_hw_type);

    if((phy_mode == AL_PHY_SUB_TYPE_SUPPORT_ONLY_N) || (phy_mode  == AL_PHY_SUB_TYPE_SUPPORT_BOTH_N_AND_AC )){
   	  al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
        return core_net_if->dev_info->set_ht_capab(core_net_if->dev, ht_vht_capab );
    }

    al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
    return 0 ;

}

/* HT - Operation Update function 
 * This function will take input from MDE the ht_oper_mode 
 * arguments : ht_oper_mode
 * return value : 
 */

static int _al_net_if_impl_set_ht_operation(al_net_if_t *al_net_if, u16* ht_oper_mode)
{
    meshap_core_net_if_t *core_net_if;
    int phy_mode = 0;
    al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

    core_net_if = (meshap_core_net_if_t *)al_net_if;

    if (core_net_if->dev_type == MESHAP_CORE_DEV_TYPE_VIRTUAL)
    {
        core_net_if->dev_info->set_virtual_mode(core_net_if->dev, core_net_if->virtual_dev_mode);
    }
    phy_mode = meshap_is_N_or_AC_supported_interface(core_net_if->al_net_if.phy_hw_type);
    if((phy_mode == AL_PHY_SUB_TYPE_SUPPORT_ONLY_N) || (phy_mode  == AL_PHY_SUB_TYPE_SUPPORT_BOTH_N_AND_AC )){
        al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
        return core_net_if->dev_info->set_ht_opera(core_net_if->dev, *ht_oper_mode );
    }

    al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
    return 0 ;
}

/* HT - Operation Update function 
 * This function will take input from MDE the ht_oper_mode 
 * arguments : ht_oper_mode
 * return value : 
 */
static int _al_net_if_impl_set_vht_capab(al_net_if_t *al_net_if, al_net_if_ht_vht_capab_t* ht_vht_capab)
{
   meshap_core_net_if_t *core_net_if;
   int phy_mode = 0;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   core_net_if = (meshap_core_net_if_t *)al_net_if;

   if (core_net_if->dev_type == MESHAP_CORE_DEV_TYPE_VIRTUAL)
   {
      core_net_if->dev_info->set_virtual_mode(core_net_if->dev, core_net_if->virtual_dev_mode);
   }
   phy_mode = meshap_is_N_or_AC_supported_interface(core_net_if->al_net_if.phy_hw_type);

    if((phy_mode == AL_PHY_SUB_TYPE_SUPPORT_ONLY_AC) || (phy_mode  == AL_PHY_SUB_TYPE_SUPPORT_BOTH_N_AND_AC )){
        al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
        return core_net_if->dev_info->set_vht_capab(core_net_if->dev,ht_vht_capab);
    }

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return 0 ;
}

/* HT - Operation Update function 
 * This function will take input from MDE the ht_oper_mode 
 * arguments : ht_oper_mode
 * return value : 
 */
static int _al_net_if_impl_set_vht_operation(al_net_if_t *al_net_if, al_802_11_vht_operation_t vht_oper)
{
    meshap_core_net_if_t *core_net_if;
    int phy_mode = 0;
    al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

    core_net_if = (meshap_core_net_if_t *)al_net_if;

    if (core_net_if->dev_type == MESHAP_CORE_DEV_TYPE_VIRTUAL)
    {
        core_net_if->dev_info->set_virtual_mode(core_net_if->dev, core_net_if->virtual_dev_mode);
    }
    phy_mode = meshap_is_N_or_AC_supported_interface(core_net_if->al_net_if.phy_hw_type);

    if((phy_mode == AL_PHY_SUB_TYPE_SUPPORT_ONLY_AC) || (phy_mode  == AL_PHY_SUB_TYPE_SUPPORT_BOTH_N_AND_AC )){
        al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
        return core_net_if->dev_info->set_vht_operation(core_net_if->dev, vht_oper);
    }

    al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
    return 0 ;
}

static int _al_net_if_impl_init_rate_ctrl(al_net_if_t *al_net_if)
{
   meshap_core_net_if_t *core_net_if;

   core_net_if = (meshap_core_net_if_t *)al_net_if;
   return core_net_if->dev_info->meshap_rate_init(core_net_if->dev);
}


