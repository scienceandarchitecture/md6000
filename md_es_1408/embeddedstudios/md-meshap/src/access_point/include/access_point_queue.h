/********************************************************************************
* MeshDynamics
* --------------
* File     : access_point.h
* Comments : Access Point Queue Interface based defination.
* Created  : 2/4/2005
* Author   : Anand Bhange
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  2  |5/31/2006 | queue_print_hstats added                        | Sriram |
* -----------------------------------------------------------------------------
* |  1  |01/04/2006| queue_print_stats added                         | Bindu  |
* -----------------------------------------------------------------------------
* |  0  |02/04/2005| Created                                         | Anand  |
* -----------------------------------------------------------------------------
********************************************************************************/



#ifndef __ACCESS_POINT_QUEUE_FACTORY_H__
#define __ACCESS_POINT_QUEUE_FACTORY_H__


/*## QUEUE TYPE DEFINATIONS START##*/
#define QUEUE_FIFO     1
#define QUEUE_DOT1P    2
/*## QUEUE TYPE DEFINATIONS END##*/

struct access_point_queue
{
   int         (*queue_uninitialize)                   (AL_CONTEXT_PARAM_DECL struct access_point_queue *ap_queue);
   int         (*queue_add_packet)                             (AL_CONTEXT_PARAM_DECL struct access_point_queue *ap_queue, al_net_if_t *al_net_if, al_packet_t *packet);
   al_packet_t * (*queue_remove_packet)                  (AL_CONTEXT_PARAM_DECL struct access_point_queue *ap_queue, al_net_if_t **net_if);
   int         (*queue_empty_check)                    (AL_CONTEXT_PARAM_DECL struct access_point_queue *ap_queue);
   //int				(*queue_print_stats)			(AL_CONTEXT_PARAM_DECL struct access_point_queue* ap_queue,char* print_p);
   int         (*queue_print_stats)                    (AL_CONTEXT_PARAM_DECL struct access_point_queue *ap_queue, struct seq_file *m);
   //int					(*queue_print_hstats)			(AL_CONTEXT_PARAM_DECL struct access_point_queue* ap_queue,char* print_p);
   int         (*queue_print_hstats)                   (AL_CONTEXT_PARAM_DECL struct access_point_queue *ap_queue, struct seq_file *m);
   int         (*queue_requeue_packet)                 (AL_CONTEXT_PARAM_DECL struct access_point_queue *ap_queue, al_net_if_t *al_net_if, al_packet_t *packet);
   void        *queue_data;
};

typedef struct access_point_queue   access_point_queue_t;


access_point_queue_t *access_point_queue_initialize(AL_CONTEXT_PARAM_DECL char queue_type);

#endif /*__ACCESS_POINT_QUEUE_FACTORY_H__*/
