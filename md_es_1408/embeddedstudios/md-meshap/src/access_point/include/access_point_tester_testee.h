/********************************************************************************
* MeshDynamics
* --------------
* File     : access_point_tester_testee.h
* Comments : AccessPoint tester main header
* Created  : 9/25/2005
* Author   : Abhijit Ayarekar
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  0  |9/25/2005 | Created.                                        | Abhijit|
* -----------------------------------------------------------------------------
********************************************************************************/


struct tester_testee_if_info
{
   al_net_addr_t wm_mac_address;
   char          name[32];
   char          essid[32];
   char          single_link_essid[32];
   int           single_link_mode;
   al_net_addr_t link_mac_address;
   int           mode;
   int           rts_threshold;
   int           frag_threshold;
   int           beacon_interval;
   int           tx_power;
   int           tx_rate;
   int           channel;
   int           long_slot_time_count;
   int           long_preamble_count;
   unsigned int  rx_count;
   unsigned int  tx_count;
   unsigned int  peer_rssi;
   unsigned int  rssi;
   int           peer_bit_rate;
   int           bit_rate;
   al_net_if_t   *net_if;
   unsigned char channel_switch_initiated;
};
typedef struct tester_testee_if_info   tester_testee_if_info_t;

struct access_point_tester_testee_globals
{
   al_net_addr_t           ds_mac_address;
   int                     ap_mode;
   al_net_addr_t           peer_mac_address;
   unsigned int            interface_index;
   unsigned int            link_interface_index;
   unsigned int            channel;
   unsigned char           analysis_interval;
   int                     if_count;
   tester_testee_if_info_t *if_info;
};

typedef struct access_point_tester_testee_globals   access_point_tester_testee_globals_t;

int access_point_tester_testee_initialize(AL_CONTEXT_PARAM_DECL access_point_tester_testee_globals_t *data);
int access_point_tester_testee_start_thread(AL_CONTEXT_PARAM_DECL al_thread_param_t *param);
