/********************************************************************************
* MeshDynamics
* --------------
* File     : access_point_fifo_queue.h
* Comments : First In First Out Queue Defination
* Created  : 2/9/2005
* Author   : Anand Bhange
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  2  |3/15/2007 | Pool size taken from Makefile                   | Sriram |
* -----------------------------------------------------------------------------
* |  1  |01/04/2006| fifo_queue_print_stats added                    | Bindu  |
* -----------------------------------------------------------------------------
* |  0  |2/09/2005 | Created                                         | Anand  |
* -----------------------------------------------------------------------------
********************************************************************************/


#ifndef __FIFO_QUEUE_H__
#define __FIFO_QUEUE_H__

#define _ACCESS_POINT_POOL_ITEM_TYPE_POOL    0
#define _ACCESS_POINT_POOL_ITEM_TYPE_HEAP    1
#define _ACCESS_POINT_PACKET_POOL_SIZE       ACCESS_POINT_PACKET_POOL_SIZE


struct fifo_queue_entry
{
   al_u64_t                timestamp;
   al_net_if_t             *net_if;
   al_packet_t             *packet;
   struct fifo_queue_entry *next;
};

typedef struct fifo_queue_entry   fifo_queue_entry_t;


struct fifo_queue_node_pool_item
{
   fifo_queue_entry_t               queue_node;
   int                              pool_item_type;
   struct fifo_queue_node_pool_item *next_pool;
};

typedef struct fifo_queue_node_pool_item   fifo_queue_node_pool_item_t;


struct fifo_queue
{
   int                         queue_semaphore_handle;
   fifo_queue_entry_t          *queue_head;
   fifo_queue_entry_t          *queue_tail;
   fifo_queue_node_pool_item_t packet_pool[_ACCESS_POINT_PACKET_POOL_SIZE];
   fifo_queue_node_pool_item_t *available_head;
};

typedef struct fifo_queue   fifo_queue_t;


fifo_queue_t *fifo_queue_initialize(AL_CONTEXT_PARAM_DECL_SINGLE);
int fifo_queue_uninitialize(AL_CONTEXT_PARAM_DECL access_point_queue_t *ap_queue);
int fifo_queue_add_packet(AL_CONTEXT_PARAM_DECL access_point_queue_t *ap_queue, al_net_if_t *al_net_if, al_packet_t *packet);
al_packet_t *fifo_queue_remove_packet(AL_CONTEXT_PARAM_DECL access_point_queue_t *ap_queue, al_net_if_t **net_if);
int fifo_queue_empty_check(AL_CONTEXT_PARAM_DECL access_point_queue_t *ap_queue);

int fifo_queue_print_hstats(AL_CONTEXT_PARAM_DECL struct access_point_queue *ap_queue, struct seq_file *m);

//int			    fifo_queue_print_hstats			(AL_CONTEXT_PARAM_DECL struct access_point_queue* ap_queue,char* print_p);
int fifo_queue_print_stats(AL_CONTEXT_PARAM_DECL struct access_point_queue *ap_queue, struct seq_file *m);

//int					fifo_queue_print_stats			(AL_CONTEXT_PARAM_DECL struct access_point_queue* ap_queue,char* print_p);
//int					fifo_queue_print_stats			(AL_CONTEXT_PARAM_DECL struct access_point_queue* ap_queue,struct seq_file* m);
int fifo_queue_requeue_packet(AL_CONTEXT_PARAM_DECL struct access_point_queue *ap_queue, al_net_if_t *al_net_if, al_packet_t *packet);

//int					fifo_queue_print_hstats			(AL_CONTEXT_PARAM_DECL struct access_point_queue* ap_queue,char* print_p);
//int					fifo_queue_print_hstats			(AL_CONTEXT_PARAM_DECL struct access_point_queue* ap_queue,struct seq_file* m);

#endif /*__FIFO_QUEUE_H__*/
