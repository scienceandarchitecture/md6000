/********************************************************************************
* MeshDynamics
* --------------
* File     : access_point_fifo_queue.c
* Comments : First In First Out Queue Impl file
* Created  : 2/9/2005
* Author   : Anand Bhange
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  6  |5/31/2006 | fifo_queue_print_hstats added                   | Sriram |
* -----------------------------------------------------------------------------
* |  5  |01/04/2006| fifo_queue_print_stats added                    | Bindu  |
* -----------------------------------------------------------------------------
* |  4  |10/5/2005 | Changes for enabling MIP                        | Sriram |
* -----------------------------------------------------------------------------
* |  3  |9/22/2005 | Interrupt disable/enable semantics changed      | Sriram |
* -----------------------------------------------------------------------------
* |  1  |2/21/2005 | Bug Fixed                                       | Anand  |
* -----------------------------------------------------------------------------
* |  0  |2/09/2005 | Created                                         | Anand  |
* -----------------------------------------------------------------------------
********************************************************************************/


#include <linux/string.h>
#include <linux/seq_file.h>
#include <linux/preempt.h>
#include "al.h"
#include "al_impl_context.h"
#include "al_print_log.h"
#include "access_point_queue.h"
#include "access_point_fifo_queue.h"

//int fifo_queue_print_hstats(AL_CONTEXT_PARAM_DECL struct access_point_queue* ap_queue,struct seq_file* m);
//int fifo_queue_print_hstats(AL_CONTEXT_PARAM_DECL struct access_point_queue* ap_queue,struct seq_file* m)
//int fifo_queue_print_stats(AL_CONTEXT_PARAM_DECL struct access_point_queue* ap_queue,struct seq_file* m);



static fifo_queue_entry_t *_allocate_queue_node(AL_CONTEXT_PARAM_DECL fifo_queue_t *queue)
{
   fifo_queue_node_pool_item_t *node;


   if (queue->available_head != NULL)
   {
      node = queue->available_head;
      queue->available_head = queue->available_head->next_pool;
   }
   else
   {
      node = (fifo_queue_node_pool_item_t *)al_heap_alloc(AL_CONTEXT sizeof(fifo_queue_node_pool_item_t)AL_HEAP_DEBUG_PARAM);
      memset(node, 0, sizeof(fifo_queue_node_pool_item_t));
      node->pool_item_type = _ACCESS_POINT_POOL_ITEM_TYPE_HEAP;
   }

   return &node->queue_node;
}


static void _free_queue_node(AL_CONTEXT_PARAM_DECL fifo_queue_t *queue, fifo_queue_entry_t *queue_node)
{
   fifo_queue_node_pool_item_t *node;


   AL_ASSERT("_free_queue_node", queue_node != NULL);

   node = (fifo_queue_node_pool_item_t *)queue_node;

   if (node->pool_item_type == _ACCESS_POINT_POOL_ITEM_TYPE_HEAP)
   {
      al_heap_free(AL_CONTEXT queue_node);
   }
   else
   {
      node->next_pool       = queue->available_head;
      queue->available_head = node;
   }
}


fifo_queue_t *fifo_queue_initialize(AL_CONTEXT_PARAM_DECL_SINGLE)
{
   int i;

   fifo_queue_node_pool_item_t *node;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Enter %s<%d>\n",__func__,__LINE__);
   fifo_queue_t *queue = (fifo_queue_t *)al_heap_alloc(AL_CONTEXT sizeof(fifo_queue_t)AL_HEAP_DEBUG_PARAM);

   AL_PRINT_LOG_FLOW_0("AP		: fifo_queue_initialize()");

   /*
    * depending on whether queue is of type heap or pool initialization to be done
    */
   queue->queue_head     = NULL;
   queue->queue_tail     = NULL;
   queue->available_head = NULL;
   node = NULL;

   for (i = 0; i < _ACCESS_POINT_PACKET_POOL_SIZE; i++)
   {
      memset(&queue->packet_pool[i], 0, sizeof(fifo_queue_node_pool_item_t));
      queue->packet_pool[i].pool_item_type = _ACCESS_POINT_POOL_ITEM_TYPE_POOL;
      if (queue->available_head == NULL)
      {
         queue->available_head = &queue->packet_pool[i];
      }
      else
      {
         node->next_pool = &queue->packet_pool[i];
      }
      node = &queue->packet_pool[i];
   }

   queue->queue_semaphore_handle = al_create_semaphore(AL_CONTEXT 1, 1);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Exit %s<%d>\n",__func__,__LINE__);

   return queue;
}


int fifo_queue_uninitialize(AL_CONTEXT_PARAM_DECL access_point_queue_t *ap_queue)
{
   fifo_queue_node_pool_item_t *node;
   fifo_queue_node_pool_item_t *temp;
   fifo_queue_t                *queue = (fifo_queue_t *)ap_queue->queue_data;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Enter %s<%d>\n",__func__,__LINE__);

   for (node = (fifo_queue_node_pool_item_t *)queue->queue_head; node != NULL; )
   {
      al_release_packet(AL_CONTEXT node->queue_node.packet);
      if (node->pool_item_type == _ACCESS_POINT_POOL_ITEM_TYPE_HEAP)
      {
         temp = (fifo_queue_node_pool_item_t *)node->queue_node.next;
         al_heap_free(AL_CONTEXT node);
         node = temp;
      }
      else
      {
         node = (fifo_queue_node_pool_item_t *)node->queue_node.next;
      }
   }

   al_destroy_semaphore(AL_CONTEXT queue->queue_semaphore_handle);
   queue->queue_head = NULL;
   queue->queue_tail = NULL;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Exit %s<%d>\n",__func__,__LINE__);

   return 0;
}


int fifo_queue_add_packet(AL_CONTEXT_PARAM_DECL access_point_queue_t *ap_queue, al_net_if_t *al_net_if, al_packet_t *packet)
{
   fifo_queue_entry_t *node;
   unsigned long      flags;
   fifo_queue_t       *queue = (fifo_queue_t *)ap_queue->queue_data;

   AL_ASSERT("access_point_queue_add_packet", packet != NULL);

   _al_disable_interrupts(gbl_mutex, gbl_muvar, gbl_mupid);

#ifdef _AL_ROVER_
   al_wait_on_semaphore(AL_CONTEXT queue->queue_semaphore_handle, AL_WAIT_TIMEOUT_INFINITE);
#endif

   if ((queue->queue_head == NULL) && (queue->queue_tail == NULL))
   {
      node         = _allocate_queue_node(AL_CONTEXT queue);
      node->net_if = al_net_if;
      node->packet = packet;
      /*node->timestamp				= al_get_tick_count(AL_CONTEXT_SINGLE);*/
      node->next = NULL;

      queue->queue_head = queue->queue_tail = node;
   }
   else
   {
      node = _allocate_queue_node(AL_CONTEXT queue);

      node->net_if = al_net_if;
      node->packet = packet;
      /*node->timestamp					= al_get_tick_count(AL_CONTEXT_SINGLE);*/
      node->next = NULL;

      queue->queue_tail->next = node;
      queue->queue_tail       = queue->queue_tail->next;
   }

#ifdef _AL_ROVER_
   al_release_semaphore(AL_CONTEXT queue->queue_semaphore_handle, 1);
#endif
   _al_enable_interrupts(gbl_mutex, gbl_muvar, gbl_mupid);

   return 0;
}


al_packet_t *fifo_queue_remove_packet(AL_CONTEXT_PARAM_DECL access_point_queue_t *ap_queue, al_net_if_t **net_if)
{
   fifo_queue_entry_t *node;
   al_packet_t        *packet;
   unsigned long      flags;
   fifo_queue_t       *queue = (fifo_queue_t *)ap_queue->queue_data;


   _al_disable_interrupts(gbl_mutex, gbl_muvar, gbl_mupid);

   node   = queue->queue_head;
   packet = NULL;

   if (node != NULL)
   {
      packet            = node->packet;
      *net_if           = node->net_if;
      queue->queue_head = queue->queue_head->next;
      if (queue->queue_head == NULL)
      {
         queue->queue_tail = NULL;
      }
      _free_queue_node(AL_CONTEXT queue, node);
   }

   _al_enable_interrupts(gbl_mutex, gbl_muvar, gbl_mupid);

   return packet;
}


int fifo_queue_empty_check(AL_CONTEXT_PARAM_DECL access_point_queue_t *ap_queue)
{
   fifo_queue_t *queue = (fifo_queue_t *)ap_queue->queue_data;

   return(queue->queue_head != NULL);
}


int fifo_queue_print_stats(AL_CONTEXT_PARAM_DECL struct access_point_queue *ap_queue, struct seq_file *m)
{
   int a = 0;

   //*print_p = 0;
   return a;
}


int fifo_queue_requeue_packet(AL_CONTEXT_PARAM_DECL struct access_point_queue *ap_queue, al_net_if_t *al_net_if, al_packet_t *packet)
{
   packet->flags |= AL_PACKET_FLAGS_REQUEUED;

   return fifo_queue_add_packet(AL_CONTEXT ap_queue, al_net_if, packet);
}


int fifo_queue_print_hstats(AL_CONTEXT_PARAM_DECL struct access_point_queue *ap_queue, struct seq_file *m)
{
   int a = 0;

   //*print_p = 0;
   return a;
}
