/********************************************************************************
* MeshDynamics
* --------------
* File     : access_point_sta.c
* Comments : Access Point Station
* Created  : 4/8/2004
* Author   : Bindu Parakala
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* | 31  |7/23/2007 | DEAUTH sent instead of DISASSOC                 | Sriram |
* -----------------------------------------------------------------------------
* | 30  |7/17/2007 | STA state printed before removal                | Sriram |
* -----------------------------------------------------------------------------
* | 29  |6/20/2007 | Changes for IGMP Snooping                       | Sriram |
* -----------------------------------------------------------------------------
* | 28  |5/17/2007 | dot1x_delete only when fully authenticated      | Sriram |
* -----------------------------------------------------------------------------
* | 27  |3/21/2007 | Fix for Ethernet WM VLAN child count            | Sriram |
* -----------------------------------------------------------------------------
* | 26  |2/6/2007  | Fixes to VLAN BCAST optimization                | Sriram |
* -----------------------------------------------------------------------------
* | 25  |1/11/2007 | Changes for VLAN BCAST optimization             | Sriram |
* -----------------------------------------------------------------------------
* | 24  |3/14/2006 | ATOMIC type changes                             | Sriram |
* -----------------------------------------------------------------------------
* | 23  |01/17/2006| Changes for aid                                 | Sriram |
* -----------------------------------------------------------------------------
* | 22  |9/22/2005 | Interrupt disable/enable semantics changed      | Sriram |
* -----------------------------------------------------------------------------
* | 21  |6/15/2005 | Interrupt disable protection added              | Sriram |
* -----------------------------------------------------------------------------
* | 20  |6/9/2005  | Misc changes                                    | Sriram |
* -----------------------------------------------------------------------------
* | 19  |4/14/2005 | Misc changes                                    | Sriram |
* -----------------------------------------------------------------------------
* | 18  |4/6/2005  | Changes for 802.1x/802.11i                      | Sriram |
* -----------------------------------------------------------------------------
* | 17  |1/6/2005  | Misc changes                                    | Sriram |
* -----------------------------------------------------------------------------
* | 16  |12/24/2004| Compilation warnings removed                    | Sriram |
* -----------------------------------------------------------------------------
* | 15  |12/24/2004| ex versions for add_sta/get_sta/release_sta     | Sriram |
* -----------------------------------------------------------------------------
* | 14  |12/22/2004| Peamble type set when .11b client leaves        | Sriram |
* -----------------------------------------------------------------------------
* | 13  |12/8/2004 | 802.11 b/g STA changes                          | Sriram |
* -----------------------------------------------------------------------------
* | 12  |11/16/2004| Compilation errors removed                      | Anand  |
* -----------------------------------------------------------------------------
* | 11  |11/10/2004| b client changes	 added to sta_release_ex      |prachiti|
* -----------------------------------------------------------------------------
* | 12  |7/24/2004 | notify_child param added to sta_release_ex      | Anand  |
* -----------------------------------------------------------------------------
* | 11  |7/19/2004 | access_point_release_sta_ex & misc fixes        | Sriram |
* -----------------------------------------------------------------------------
* | 10  |6/21/2004 | Added sta_queue_semaphore                       | Bindu  |
* -----------------------------------------------------------------------------
* |  9  |6/21/2004 | Added  _remove_sta                              | Bindu  |
* -----------------------------------------------------------------------------
* |  8  |6/14/2004 | Added AL_ASSERT macro                           | Bindu  |
* -----------------------------------------------------------------------------
* |  7  |6/4/2004  | Misc fixes                                      | Bindu  |
* -----------------------------------------------------------------------------
* |  6  |6/2/2004  | Implemented locking                             | Sriram |
* -----------------------------------------------------------------------------
* |  5  |5/31/2004 | included access_point_sta.h                     | Bindu  |
* -----------------------------------------------------------------------------
* |  4  |5/11/2004 | misc bugs fixed                                 | Anand  |
* -----------------------------------------------------------------------------
* |  3  |5/6/2004  | Bug fixed while adding sta to hash              | Anand  |
* -----------------------------------------------------------------------------
* |  2  |5/5/2004  | Packet/Memory Leak Debug params added           | Anand  |
* -----------------------------------------------------------------------------
* |  1  |4/22/2004 | **Made (major) changes for AL_CONTEXT**         | Bindu  |
* -----------------------------------------------------------------------------
* |  0  |4/8/2004	| Created                                         | Bindu  |
* -----------------------------------------------------------------------------
********************************************************************************/

#include <linux/string.h>
#include <linux/module.h>
#include <linux/init.h>
#include <linux/version.h>
#include "al.h"
#include "access_point.h"
#include "al_impl_context.h"
#include "access_point_sta.h"
#include "al_print_log.h"
#include "access_point_thread.h"
#include "access_point_globals.h"
#include "access_point_mgt.h"
#include "access_point_vlan.h"
#include "mesh_defs.h"

#define _IGMP_SUPPORT    (globals.config.igmp_support)

AL_DECLARE_GLOBAL(struct ap_sta_bucket_entry *_sta_hash);
AL_DECLARE_GLOBAL(access_point_sta_entry_t * _sta_list_head);
AL_DECLARE_GLOBAL(access_point_sta_entry_t * _sta_list_tail);
AL_DECLARE_GLOBAL(int _sta_hash_length);
AL_DECLARE_GLOBAL_EXTERN(mesh_config_info_t * mesh_config);

al_spinlock_t sta_splock;
AL_DEFINE_PER_CPU(int, sta_spvar);

struct al_mutex sta_mutex;
int sta_muvar;
int sta_mupid;


static AL_INLINE void _remove_sta(AL_CONTEXT_PARAM_DECL access_point_sta_entry_t *node)
{
   int bucket;
   unsigned long flags;

   AL_ASSERT("_remove_sta", node != NULL);

   bucket = node->hash_value % _sta_hash_length;

   /*
    * Remove from hash
    */


   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Enter %s<%d>\n",__func__,__LINE__);
   al_spin_lock(_sta_hash[bucket].sta_hsplock, flags);

   if (node->next_hash != NULL)
   {
      node->next_hash->prev_hash = node->prev_hash;
   }
   if (node->prev_hash != NULL)
   {
      node->prev_hash->next_hash = node->next_hash;
   }
   if (node == _sta_hash[bucket].sta_entry)
   {
      _sta_hash[bucket].sta_entry = node->next_hash;
   }
   al_spin_unlock(_sta_hash[bucket].sta_hsplock, flags);
   /*
    * Remove from list
    */

   al_spin_lock(sta_splock, sta_spvar);

   if (node->next_list != NULL)
   {
      node->next_list->prev_list = node->prev_list;
   }
   if (node->prev_list != NULL)
   {
      node->prev_list->next_list = node->next_list;
   }
   if (node == _sta_list_head)
   {
      _sta_list_head = node->next_list;
   }
   if (node == _sta_list_tail)
   {
      _sta_list_tail = node->prev_list;
   }

   al_spin_unlock(sta_splock, sta_spvar);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,
                "MESH_AP:INFO %s<%d> Removing Entry "AL_NET_ADDR_STR " (state %d)\n",__func__, __LINE__,
                AL_NET_ADDR_TO_STR(&node->sta.address), node->sta.state);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Exit %s<%d>\n",__func__,__LINE__);
}


int access_point_sta_initialize(AL_CONTEXT_PARAM_DECL int sta_hash_length)
{
	int i;

	_sta_hash_length = sta_hash_length;
	//_sta_hash        = (access_point_sta_entry_t **)al_heap_alloc(AL_CONTEXT sizeof(access_point_sta_entry_t *) * sta_hash_length AL_HEAP_DEBUG_PARAM);
	_sta_hash        = (struct ap_sta_bucket_entry *)al_heap_alloc(AL_CONTEXT sizeof(struct ap_sta_bucket_entry ) * sta_hash_length AL_HEAP_DEBUG_PARAM);

    al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Enter %s<%d>\n",__func__,__LINE__);
	for (i = 0; i < sta_hash_length; i++)
	{
		_sta_hash[i].sta_entry = NULL;
	    al_spin_lock_init(&_sta_hash[i].sta_hsplock);
	}

	al_spin_lock_init(&sta_splock);
	al_mutex_init(&sta_mutex);

	sta_queue_semaphore = al_create_semaphore(AL_CONTEXT  1, 1);
    al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Exit %s<%d>\n",__func__,__LINE__);

	return 0;
}


int access_point_sta_uninitialize(AL_CONTEXT_PARAM_DECL_SINGLE)
{
   access_point_sta_entry_t *node;
   access_point_sta_entry_t *temp;
   unsigned long flags;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Enter %s<%d>\n",__func__,__LINE__);
   al_spin_lock(sta_splock, sta_spvar);

   node = _sta_list_head;
   while (node != NULL)
   {
      temp = node->next_list;
      access_point_remove_sta(AL_CONTEXT & node->sta.address);
      node = temp;
   }
   al_spin_unlock(sta_splock, sta_spvar);

   al_heap_free(AL_CONTEXT _sta_hash);
   al_destroy_semaphore(AL_CONTEXT sta_queue_semaphore);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Exit %s<%d>\n",__func__,__LINE__);

   return 0;
}


access_point_sta_entry_t *access_point_add_sta_ex(AL_CONTEXT_PARAM_DECL al_net_if_t *net_if, al_net_addr_t *addr, int from_interrupt_context)
{
	int                      hash_value;
	int                      bucket;
	unsigned long            flags;
	access_point_sta_entry_t *node, *bucket_node;;

	AL_ASSERT("access_point_add_sta", net_if != NULL);
	AL_ASSERT("access_point_add_sta", addr != NULL);

	flags      = 0;
	hash_value = _ACCESS_POINT_STA_HASH_FUNCTION(addr);
	bucket     = hash_value % _sta_hash_length;

    al_spin_lock(_sta_hash[bucket].sta_hsplock, flags);
	node = _sta_hash[bucket].sta_entry;

	while (node != NULL)
	{
		if (AL_NET_ADDR_EQUAL(&node->sta.address, addr))
		{
			break;
		}
		node = node->next_hash;
	}

	if (node != NULL)
	{
            if (AL_ATOMIC_GET(node->sta.ref_count)) {
                node->sta.wmm_flag_set = 0;
                node->sta.ht_flag_set = 0;
                node->sta.vht_flag_set = 0;
                al_spin_unlock(_sta_hash[bucket].sta_hsplock, flags);
                return node;
            }
            else {
                node = NULL;
            }
        }


	/**
	 * Create a new entry
	 */

	node = (access_point_sta_entry_t *)al_heap_alloc(AL_CONTEXT sizeof(access_point_sta_entry_t)AL_HEAP_DEBUG_PARAM);
	memset(node, 0, sizeof(access_point_sta_entry_t));
	memcpy(&(node->sta.address), addr, sizeof(al_net_addr_t));
	node->sta.net_if = net_if;
	AL_ATOMIC_SET(node->sta.ref_count, 1);
	node->hash_value    = hash_value;
	node->sta.vlan_info = &default_vlan_info;



	/**
	 * Add to hash table
	 */


	if (_sta_hash[bucket].sta_entry == NULL)
	{
		node->next_hash   = NULL;
		node->prev_hash   = NULL;
		_sta_hash[bucket].sta_entry= node;

	}
	else
	{
		node->next_hash = _sta_hash[bucket].sta_entry;
		_sta_hash[bucket].sta_entry->prev_hash = node;
		_sta_hash[bucket].sta_entry            = node;
	}
	al_spin_unlock(_sta_hash[bucket].sta_hsplock, flags);
	/**
	 * Add to list
	 */

	al_spin_lock(sta_splock, sta_spvar);

	if (_sta_list_head == NULL)
	{
		_sta_list_head = node;
		_sta_list_tail = node;
	}
	else
	{
		_sta_list_tail->next_list = node;
		node->prev_list           = _sta_list_tail;
		_sta_list_tail            = node;
	}

	node->sta.last_packet_received = node->sta.last_packet_transmitted =
	al_get_tick_count(AL_CONTEXT_SINGLE);

	al_spin_unlock(sta_splock, sta_spvar);
	return node;
}


access_point_sta_entry_t *access_point_add_sta(AL_CONTEXT_PARAM_DECL al_net_if_t *
                                               net_if, al_net_addr_t *addr)
{
   return access_point_add_sta_ex(AL_CONTEXT net_if, addr, 0);
}


access_point_sta_entry_t *access_point_get_sta_ex(AL_CONTEXT_PARAM_DECL
                                                  al_net_addr_t *addr, int from_interrupt_context)
{
	int hash_value;
	int bucket;
	access_point_sta_entry_t *node;
	unsigned long            flags;

	AL_ASSERT("access_point_get_sta", addr != NULL);

	flags      = 0;
	hash_value = _ACCESS_POINT_STA_HASH_FUNCTION(addr);
	bucket     = hash_value % _sta_hash_length;

	al_spin_lock(_sta_hash[bucket].sta_hsplock, flags);
	node = _sta_hash[bucket].sta_entry;
	/* Check if node exists in hash table */

	while (node != NULL)
	{
		if (AL_NET_ADDR_EQUAL(&node->sta.address, addr))
		{
			if (!AL_ATOMIC_GET(node->sta.ref_count)) {
				al_spin_unlock(_sta_hash[bucket].sta_hsplock, flags);
				return NULL;
			} else {
				AL_ATOMIC_INC(node->sta.ref_count);
				break;
			}
		}
		node = node->next_hash;
	}
	al_spin_unlock(_sta_hash[bucket].sta_hsplock, flags);
	if (node != NULL)
	{
		return node;
	}

	return NULL;
}


access_point_sta_entry_t *access_point_get_sta(AL_CONTEXT_PARAM_DECL al_net_addr_t *addr)
{
   return access_point_get_sta_ex(AL_CONTEXT addr, 0);
}


void access_point_remove_sta(AL_CONTEXT_PARAM_DECL al_net_addr_t *addr)
{
	int hash_value;
	int bucket;
	access_point_sta_entry_t *node, *tmp;
	unsigned long            flags;
	int protection_count;

	AL_ASSERT("access_point_remove_sta", addr != NULL);

	hash_value = _ACCESS_POINT_STA_HASH_FUNCTION(addr);
	bucket     = hash_value % _sta_hash_length;


    al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Enter %s<%d>\n",__func__,__LINE__);
	al_spin_lock(_sta_hash[bucket].sta_hsplock, flags);
	node             = _sta_hash[bucket].sta_entry;


	protection_count = ACCESS_POINT_INTERRUPT_DISABLE_PROTECTION_COUNT;

	/**
	 * Remove sta from hash table
	 */

	while (node != NULL && protection_count > 0)
	{
		if (AL_NET_ADDR_EQUAL(&node->sta.address, addr))
		{
			break;
		}
		node = node->next_hash;
		--protection_count;
	}
	al_spin_unlock(_sta_hash[bucket].sta_hsplock, flags);

	if ((node == NULL) ||
			(protection_count == 0))
	{
		return;
	}

	access_point_release_sta(AL_CONTEXT node);
    al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Exit %s<%d>\n",__func__,__LINE__);
}


access_point_sta_entry_t *access_point_get_first_sta(AL_CONTEXT_PARAM_DECL_SINGLE)
{
   unsigned long flags;
   access_point_sta_entry_t *node;

   al_spin_lock(sta_splock, sta_spvar);

   node = _sta_list_head;

   while (node) {
        if (!AL_ATOMIC_GET(node->sta.ref_count)) {
            node = node->next_list;
            continue;
        }else {
            AL_ATOMIC_INC(node->sta.ref_count);
            break;
 		}
	}
   al_spin_unlock(sta_splock, sta_spvar);

   return node;
}


void access_point_release_sta(AL_CONTEXT_PARAM_DECL access_point_sta_entry_t *sta_entry)
{
   access_point_release_sta_ex(AL_CONTEXT sta_entry, 0, 0);
}


void access_point_release_sta_ex(AL_CONTEXT_PARAM_DECL
                                 access_point_sta_entry_t *sta_entry,
                                 int notify_mesh, int notify_child)
{
   access_point_release_sta_ex2(AL_CONTEXT sta_entry, notify_mesh, notify_child, 0);
}


void access_point_release_sta_ex2(AL_CONTEXT_PARAM_DECL
                                  access_point_sta_entry_t *sta_entry,
                                  int notify_mesh, int notify_child,
                                  int from_interrupt_context)
{
	unsigned long                    flags;
	access_point_sta_info_t          *sta_info;
	access_point_netif_config_info_t *al_netif_config_info;
	al_802_11_operations_t           *extended_operations;
    mesh_conf_if_info_t *netif_conf;
	int phy_mode,j,i;

	flags = 0;

	al_netif_config_info = (access_point_netif_config_info_t *)
		sta_entry->sta.net_if->config.ext_config_info;

   if (!AL_ATOMIC_GET(sta_entry->sta.ref_count)) {
		return;
	}

	if (AL_ATOMIC_DEC_AND_TEST(sta_entry->sta.ref_count))
	{
		_remove_sta(AL_CONTEXT sta_entry);
		if (notify_child)
		{
         extended_operations = sta_entry->sta.net_if->get_extended_operations(sta_entry->sta.net_if);
         if (extended_operations == NULL)
         {
            al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : Not able to get extended operations, "
                      "unable to send deauth %s : %d\n", __func__,__LINE__);
            return;
         }
         extended_operations->send_deauth(sta_entry->sta.net_if, sta_entry->sta.net_if->config.hw_addr.bytes,
                                    &sta_entry->sta.address);
		}
		if (notify_mesh)
		{
			if (on_disassoc_handler != NULL)
			{
				on_disassoc_handler(
						AL_CONTEXT sta_entry->sta.net_if,
						&sta_entry->sta.address,
						AL_802_11_REASON_DISASSOC_DUE_TO_INACTIVITY);
			}
		}

		sta_info = &sta_entry->sta;
        netif_conf = NULL;
        for (j = 0; j < mesh_config->if_count; j++)
        {
           if (!strcmp(globals.config.wm_net_if_info[i].net_if->name, sta_info->net_if->name))
               netif_conf = &mesh_config->if_info[j];
        }

		if (AL_NET_IF_IS_WIRELESS(sta_info->net_if))
		{
         if (!notify_child)
         {
            /* When notify child is set, send_deauth will delete station entry from mac80211
             * No need to call explicitly one more time here
             */
            extended_operations = sta_info->net_if->get_extended_operations(sta_info->net_if);
            /*Delete the station from mac80211*/
            if ((extended_operations != NULL) && (from_interrupt_context == 0))
            {
               if (netif_conf) {
                  if (netif_conf->use_type != AL_CONF_IF_USE_TYPE_AP)
                     extended_operations->del_station(sta_entry->sta.net_if, (u8 *) &sta_entry->sta.address.bytes);
               } else {
				  al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MESH_AP:INFO: %s<%d> station connected to interface name %s will be released "
                                               "by HOSTAPD",__func__,__LINE__,sta_info->net_if->name);
                     }
            }
         }
			if (sta_info->auth_state &
					ACCESS_POINT_STA_AUTH_STATE_ENCRYPTION &&
					(sta_info->key_index >= 4))
			{
				al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MESH_AP:INFO: %s<%d> Releasing key index %d for STA "
				"" AL_NET_ADDR_STR, __func__, __LINE__, sta_info->key_index, AL_NET_ADDR_TO_STR(&sta_info->address));
			}

			if (sta_info->is_imcp)
			{
				extended_operations->remove_downlink_round_robin_child(sta_info->net_if, &sta_info->address);
				if (AL_ATOMIC_GET(al_netif_config_info->imcp_sta_count))
				{
					AL_ATOMIC_DEC(al_netif_config_info->imcp_sta_count);
				}
				else
				{
					al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,
							"MESH_AP:INFO: %s<%d> imcp_sta_count for %s was strangely %d",
							__func__, __LINE__, sta_entry->sta.net_if->name,
							AL_ATOMIC_GET(al_netif_config_info->imcp_sta_count));
				}
			}
			else
			{
				if (sta_info->vlan_info == &default_vlan_info)
				{
					if (AL_ATOMIC_GET(al_netif_config_info->sta_count))
					{
						AL_ATOMIC_DEC(al_netif_config_info->sta_count);
					}
					else
					{
						al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,
								"MESH_AP:INFO: %s<%d> sta_count for %s was strangely %d",
								__func__, __LINE__, sta_entry->sta.net_if->name,
								AL_ATOMIC_GET(al_netif_config_info->sta_count));
					}
				}
				else if (sta_info->vlan_info != NULL)
				{
					access_point_decr_net_if_vlan_count(AL_CONTEXT sta_info->vlan_info, sta_info->net_if);
				}
			}
		}
		else
		{
			if (sta_info->vlan_info == &default_vlan_info)
			{
				if (AL_ATOMIC_GET(al_netif_config_info->sta_count))
				{
					AL_ATOMIC_DEC(al_netif_config_info->sta_count);
				}
				else
				{
					al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,
							"MESH_AP:INFO: %s<%d> sta_count for %s was strangely %d",
							__func__, __LINE__, sta_entry->sta.net_if->name,
							AL_ATOMIC_GET(al_netif_config_info->sta_count));
				}
			}
			else if (sta_info->vlan_info != NULL)
			{
				access_point_decr_net_if_vlan_count(AL_CONTEXT sta_info->vlan_info, sta_info->net_if);
			}
		}

		if (sta_info->auth_state & ACCESS_POINT_STA_AUTH_STATE_802_1X_INITIATED)
		{
			on_dot1x_auth_delete(AL_CONTEXT sta_info->net_if, al_netif_config_info, &sta_info->address);
		}

		if (sta_info->state == ACCESS_POINT_STA_STATE_ASSOCIATED)
		{
			access_point_sta_release_aid(AL_CONTEXT sta_info->net_if, sta_info->aid);
		}

		if (sta_info->no_ht_gf_set) {
			sta_info->no_ht_gf_set = 0;
			al_netif_config_info->num_sta_ht_no_gf--;
		}

		if (sta_info->no_ht_set) {
			sta_info->no_ht_set = 0;
			al_netif_config_info->num_sta_no_ht--;
		}

		if (sta_info->ht_20mhz_set) {
			sta_info->ht_20mhz_set = 0;
			al_netif_config_info->num_sta_ht_20mhz--;
		}

		if(al_ht_operation_update(al_netif_config_info)){
			extended_operations->set_ht_operation(al_netif_config_info->net_if ,&al_netif_config_info->ht_oper_mode );
			if (sta_info->wmm_flag_set)
				sta_info->wmm_flag_set = 0;
			if (sta_info->ht_flag_set)
				sta_info->ht_flag_set = 0;
			if (sta_info->vht_flag_set)
				sta_info->vht_flag_set = 0;

		}
		//al_ht_operation_update(&al_netif_config_info);
		/**
		 *	Remove disassociating sta from multicast sta and indr_sta lists if present
		 */

		if (_IGMP_SUPPORT)
		{
			access_point_remove_sta_from_multicast_table(AL_CONTEXT sta_info->net_if, &sta_info->address);
		}

        al_heap_free(AL_CONTEXT sta_entry);
	}
}


unsigned short access_point_sta_get_aid(AL_CONTEXT_PARAM_DECL al_net_if_t *net_if)
{
   access_point_netif_config_info_t *netif_config_info;
   unsigned long  flags;
   unsigned short aid;
   int            i;
   int            j;

   netif_config_info = (access_point_netif_config_info_t *)net_if->config.ext_config_info;
   aid = 0;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Enter %s<%d>\n",__func__,__LINE__);
   al_spin_lock(sta_splock, sta_spvar);


   for (i = 0; i < sizeof(netif_config_info->aid_bitmap); i++)
   {
      if (netif_config_info->aid_bitmap[i] != 0xFF)
      {
         for (j = 0; j < 8; j++)
         {
            if (!(netif_config_info->aid_bitmap[i] & (1 << j)))
            {
               aid = (i * 8) + j + 1;
               netif_config_info->aid_bitmap[i] |= (1 << j);
               break;
            }
         }
      }
      if (aid != 0)
      {
         break;
      }
   }

   al_spin_unlock(sta_splock, sta_spvar);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Exit %s<%d>\n",__func__,__LINE__);
   return aid;
}


void access_point_sta_release_aid(AL_CONTEXT_PARAM_DECL al_net_if_t *net_if, unsigned short aid)
{
   access_point_netif_config_info_t *netif_config_info;
   unsigned long flags;
   int           i;
   int           j;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Enter %s<%d>\n",__func__,__LINE__);
   netif_config_info = (access_point_netif_config_info_t *)net_if->config.ext_config_info;

   al_spin_lock(sta_splock, sta_spvar);

   i = (aid - 1) / 8;
   j = (aid - 1) % 8;

   netif_config_info->aid_bitmap[i] &= ~(1 << j);

   al_spin_unlock(sta_splock, sta_spvar);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Exit %s<%d>\n",__func__,__LINE__);
}
