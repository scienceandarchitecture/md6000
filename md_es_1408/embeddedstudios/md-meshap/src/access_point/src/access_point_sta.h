/********************************************************************************
* MeshDynamics
* --------------
* File     : access_point_sta.h
* Comments : Access Point Station Header File
* Created  : 4/8/2004
* Author   : Bindu Parakala
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* | 10  |3/14/2006 | ATOMIC type changes                             | Sriram |
* -----------------------------------------------------------------------------
* |  9  |01/17/2006| Changes for aid                                 | Sriram |
* -----------------------------------------------------------------------------
* |  8  |9/22/2005 | Interrupt disable/enable semantics changed      | Sriram |
* -----------------------------------------------------------------------------
* |  7  |12/22/2004| ex versions for add_sta/get_sta/release_sta     | Sriram |
* -----------------------------------------------------------------------------
* |  6  |7/24/2004 | notify_child param added to sta_release_ex      | Anand  |
* -----------------------------------------------------------------------------
* |  5  |7/19/2004 | Added access_point_release_sta_ex function      | Sriram |
* -----------------------------------------------------------------------------
* |  4  |6/21/2004 | Added access_point_release_sta()                | Bindu  |
* -----------------------------------------------------------------------------
* |  3  |6/21/2004 | Added ref_count to sta entry struct             | Bindu  |
* -----------------------------------------------------------------------------
* |  2  |6/21/2004 | Added ACCESS_POINT_STA_LOCK/UNLOCK macro        | Bindu  |
* -----------------------------------------------------------------------------
* |  1  |4/22/2004 | **Made (major) changes for AL_CONTEXT**         | Bindu  |
* -----------------------------------------------------------------------------
* |  0  |4/8/2004  | Created                                         | Bindu  |
* -----------------------------------------------------------------------------
********************************************************************************/


#ifndef __ACCESS_POINT_STA_H__
#define __ACCESS_POINT_STA_H__


/**
 * The hash function for STAs
 */

#ifdef  __GNUC__
#define _ACCESS_POINT_STA_HASH_FUNCTION(addr) \
   ({                                         \
      int hash;                               \
      hash = (addr)->bytes[0] * 1             \
             + (addr)->bytes[1] * 2           \
             + (addr)->bytes[2] * 3           \
             + (addr)->bytes[3] * 4           \
             + (addr)->bytes[4] * 5           \
             + (addr)->bytes[5] * 6;          \
      hash;                                   \
   }                                          \
   )
#else
static int _ACCESS_POINT_STA_HASH_FUNCTION(al_net_addr_t *addr)
{
   int hash;                      \
   hash = (addr)->bytes[0] * 1    \
          + (addr)->bytes[1] * 2  \
          + (addr)->bytes[2] * 3  \
          + (addr)->bytes[3] * 4  \
          + (addr)->bytes[4] * 5  \
          + (addr)->bytes[5] * 6; \
   return hash;                   \
}
#endif

#define ACCESS_POINT_STA_LOCK(flags)  \
	do {                              \
		al_disable_interrupts(flags); \
	} while (0)
#define ACCESS_POINT_STA_UNLOCK(flags) \
	do {                               \
		al_enable_interrupts(flags);   \
	} while (0)


extern al_spinlock_t sta_splock;
AL_DECLARE_PER_CPU(int, sta_spvar);

extern struct al_mutex sta_mutex;
extern int sta_muvar;
extern int sta_mupid;

struct access_point_sta_entry
{
   access_point_sta_info_t       sta;
   int                           hash_value;
   struct access_point_sta_entry *next_hash;
   struct access_point_sta_entry *prev_hash;
   struct access_point_sta_entry *next_list;
   struct access_point_sta_entry *prev_list;
};

struct ap_sta_bucket_entry
{
    struct access_point_sta_entry *sta_entry;
    al_spinlock_t sta_hsplock;
};

typedef struct access_point_sta_entry   access_point_sta_entry_t;

int access_point_sta_initialize(AL_CONTEXT_PARAM_DECL int sta_hash_length);

int access_point_sta_uninitialize(AL_CONTEXT_PARAM_DECL_SINGLE);

access_point_sta_entry_t *access_point_add_sta(AL_CONTEXT_PARAM_DECL
                                               al_net_if_t *net_if, al_net_addr_t *addr);

access_point_sta_entry_t *access_point_add_sta_ex(AL_CONTEXT_PARAM_DECL
                                                  al_net_if_t *net_if, al_net_addr_t *addr,
                                                  int from_interrupt_context);

access_point_sta_entry_t *access_point_get_sta(AL_CONTEXT_PARAM_DECL al_net_addr_t *addr);
access_point_sta_entry_t *access_point_get_sta_ex(AL_CONTEXT_PARAM_DECL al_net_addr_t *addr, int from_interrupt_context);
void access_point_remove_sta(AL_CONTEXT_PARAM_DECL al_net_addr_t *addr);

access_point_sta_entry_t *access_point_get_first_sta(AL_CONTEXT_PARAM_DECL_SINGLE);
void access_point_release_sta(AL_CONTEXT_PARAM_DECL access_point_sta_entry_t *sta_entry);
void access_point_release_sta_ex(AL_CONTEXT_PARAM_DECL access_point_sta_entry_t *sta_entry, int notify_mesh, int notify_child);
void access_point_release_sta_ex2(AL_CONTEXT_PARAM_DECL access_point_sta_entry_t *sta_entry, int notify_mesh, int notify_child, int from_interrupt_context);

unsigned short access_point_sta_get_aid(AL_CONTEXT_PARAM_DECL al_net_if_t *net_if);
void access_point_sta_release_aid(AL_CONTEXT_PARAM_DECL al_net_if_t *net_if, unsigned short aid);

#endif /*__ACCESS_POINT_STA_H__*/
