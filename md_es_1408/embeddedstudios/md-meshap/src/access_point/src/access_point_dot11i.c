/********************************************************************************
* MeshDynamics
* --------------
* File     : access_point_dot11i.c
* Comments : 802.11i support code
* Created  : 3/29/2005
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  5  |6/5/2007  | Changes for In-direct VLAN                      | Sriram |
* -----------------------------------------------------------------------------
* |  4  |11/14/2006| Changes for heap debug support                  | Sriram |
* -----------------------------------------------------------------------------
* |  3  |11/8/2006 | Changes for compression support                 | Sriram |
* -----------------------------------------------------------------------------
* |  2  |9/22/2005 | Interrupt disable/enable semantics changed      | Sriram |
* -----------------------------------------------------------------------------
* |  1  |4/13/2005 | Corrections to VLAN based security              | Sriram |
* -----------------------------------------------------------------------------
* |  0  |3/29/2005 | Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/
#include <linux/string.h>
#include <linux/kernel.h>
#include "al.h"
#include "access_point.h"
#include "al_impl_context.h"
#include "al_print_log.h"
#include "access_point_globals.h"
#include "access_point_sta.h"
#include "access_point_vlan.h"

struct _access_point_dot11i_queue
{
   al_net_addr_t                     sta_addr;
   struct _access_point_dot11i_queue *next;
};

typedef struct _access_point_dot11i_queue   _access_point_dot11i_queue_t;

al_spinlock_t dot11i_splock;
AL_DEFINE_PER_CPU(int, dot11i_spvar);

AL_DECLARE_GLOBAL(_access_point_dot11i_queue_t * _queue_head);
AL_DECLARE_GLOBAL(_access_point_dot11i_queue_t * _queue_tail);
AL_DECLARE_GLOBAL(int _queue_handle);
AL_DECLARE_GLOBAL(int _stop_handle);
AL_DECLARE_GLOBAL(int _quit);
AL_DECLARE_GLOBAL(int _semaphore_handle);

static int _access_point_dot11i_thread(AL_CONTEXT_PARAM_DECL al_thread_param_t *param);
static AL_INLINE void _access_point_dot11i_process_queue(AL_CONTEXT_PARAM_DECL _access_point_dot11i_queue_t *node);

int access_point_dot11i_initialize(AL_CONTEXT_PARAM_DECL_SINGLE)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
   _queue_handle     = al_create_event(AL_CONTEXT 1);
   _stop_handle      = al_create_event(AL_CONTEXT 1);
   _semaphore_handle = al_create_semaphore(AL_CONTEXT 1, 1);
   _quit             = 0;
   _queue_head       = NULL;
   _queue_tail       = NULL;

   al_spin_lock_init(&dot11i_splock);

   al_create_thread_on_cpu(AL_CONTEXT _access_point_dot11i_thread, NULL,0, "_access_point_dot11i_thread");
   threads_status |= THREAD_STATUS_BIT3_MASK;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
   return 0;
}


int access_point_dot11i_uninitialize(AL_CONTEXT_PARAM_DECL_SINGLE)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
   _access_point_dot11i_queue_t *node;
   _access_point_dot11i_queue_t *temp;
   unsigned long                flags;

   _quit = 1;

   al_reset_event(AL_CONTEXT _stop_handle);
   al_set_event(AL_CONTEXT _queue_handle);
   al_wait_on_event(AL_CONTEXT _stop_handle, AL_WAIT_TIMEOUT_INFINITE);

   al_destroy_event(AL_CONTEXT _queue_handle);
   al_destroy_event(AL_CONTEXT _stop_handle);

#ifdef _AL_ROVER_
   al_wait_on_semaphore(AL_CONTEXT _semaphore_handle, AL_WAIT_TIMEOUT_INFINITE);
#endif

  al_spin_lock(dot11i_splock, dot11i_spvar);

   node = _queue_head;

   while (node != NULL)
   {
      temp = node->next;
      al_heap_free(AL_CONTEXT node);
      node = temp;
   }

   _queue_head = NULL;
   _queue_tail = NULL;

  al_spin_unlock(dot11i_splock, dot11i_spvar);

#ifdef _AL_ROVER_
   al_release_semaphore(AL_CONTEXT _semaphore_handle, 1);
#endif

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
   return 0;
}


static int _access_point_dot11i_thread(AL_CONTEXT_PARAM_DECL al_thread_param_t *param)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
   _access_point_dot11i_queue_t *node;
   int           ret;
   unsigned long flags;

   al_set_thread_name(AL_CONTEXT "ap_dot11i");

   while (1)
   {
_sleep:
      al_reset_event(AL_CONTEXT _queue_handle);
	  thread_stat.ap_dot11i_thread_count++;

      ret = al_wait_on_event(AL_CONTEXT _queue_handle, AL_WAIT_TIMEOUT_INFINITE);

      if (_quit)
      {
         break;
      }

_next_node:

	  if(reboot_in_progress) {
		  printk(KERN_EMERG"-----Terminating 'ap_dot11i' thread-----\n");
		  threads_status &= ~THREAD_STATUS_BIT3_MASK;
		  break;
	  }
#ifdef _AL_ROVER_
      al_wait_on_semaphore(AL_CONTEXT _semaphore_handle, AL_WAIT_TIMEOUT_INFINITE);
#endif
	  al_spin_lock(dot11i_splock, dot11i_spvar);

      node = _queue_head;
      if (node != NULL)
      {
         _queue_head = node->next;
      }

	  al_spin_unlock(dot11i_splock, dot11i_spvar);
#ifdef _AL_ROVER_
      al_release_semaphore(AL_CONTEXT _semaphore_handle, 1);
#endif

      if (node == NULL)
      {
         goto _sleep;
      }

      _access_point_dot11i_process_queue(AL_CONTEXT node);

      al_heap_free(AL_CONTEXT node);

      goto _next_node;
   }

   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "AP :   dot11i Thread Exited -----");
   al_set_event(AL_CONTEXT _stop_handle);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
   return 0;
}


int access_point_dot11i_add_sta(AL_CONTEXT_PARAM_DECL al_net_addr_t *sta_addr)
{
   unsigned long                flags;
   _access_point_dot11i_queue_t *node;

   node = (_access_point_dot11i_queue_t *)al_heap_alloc(AL_CONTEXT sizeof(_access_point_dot11i_queue_t)AL_HEAP_DEBUG_PARAM);
   memset(node, 0, sizeof(_access_point_dot11i_queue_t));

   memcpy(&node->sta_addr, sta_addr, sizeof(al_net_addr_t));

#ifdef _AL_ROVER_
   al_wait_on_semaphore(AL_CONTEXT _semaphore_handle, AL_WAIT_TIMEOUT_INFINITE);
#endif
   al_spin_lock(dot11i_splock, dot11i_spvar);

   if (_queue_head == NULL)
   {
      _queue_head = node;
      _queue_tail = node;
   }
   else
   {
      _queue_tail->next = node;
      _queue_tail       = node;
   }

   al_spin_unlock(dot11i_splock, dot11i_spvar);
#ifdef _AL_ROVER_
   al_release_semaphore(AL_CONTEXT _semaphore_handle, 1);
#endif

   al_set_event(AL_CONTEXT _queue_handle);

   return 0;
}


static AL_INLINE void _access_point_dot11i_process_queue(AL_CONTEXT_PARAM_DECL _access_point_dot11i_queue_t *node)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
   access_point_sta_entry_t         *sta_entry;
   access_point_netif_config_info_t *config_info;
   int ret;
   int key_index;
   int enable_encryption;

   sta_entry = access_point_get_sta(&node->sta_addr);

   if (sta_entry == NULL)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESHAP : ERROR : AP dot11i : STA entry not found for %s : %d\n", __func__, __LINE__);
      return;
   }

   if ((strcmp(sta_entry->sta.net_if->name, "wlan2") == 0) || (sta_entry->sta.vlan_info != &default_vlan_info))
   {
//SPAWAR
      access_point_release_sta(AL_CONTEXT sta_entry);
//SPAWAR_END
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESHAP : ERROR : not processing the sta through wlan2 or it is not a default vlan %s : %d\n ", __func__, __LINE__);
      return;
   }

   if (AL_NET_IF_IS_WIRELESS(sta_entry->sta.net_if))
   {
      if ((sta_entry->sta.vlan_info == NULL) ||
          (sta_entry->sta.vlan_info == &default_vlan_info) ||
          (sta_entry->sta.auth_state & ACCESS_POINT_STA_AUTH_STATE_INDIRECT_VLAN))
      {
         config_info = (access_point_netif_config_info_t *)sta_entry->sta.net_if->config.ext_config_info;
      }
      else
      {
         config_info = &sta_entry->sta.vlan_info->config;
      }
   }
   else
   {
      config_info = (access_point_netif_config_info_t *)sta_entry->sta.net_if->config.ext_config_info;
   }

   if (config_info->security_info.dot11.rsn.enabled)
   {
      sta_entry->sta.auth_state |= ACCESS_POINT_STA_AUTH_STATE_802_1X_INITIATED;
   }

   if (config_info->security_info.dot11.wep.enabled ||
       config_info->security_info.dot11.rsn.enabled)
   {
      if (sta_entry->sta.auth_state & ACCESS_POINT_STA_AUTH_STATE_RSN)
      {
         ret = on_dot1x_auth_start_handler(AL_CONTEXT sta_entry->sta.net_if,
                                           config_info,
                                           &node->sta_addr,
                                           &sta_entry->sta.rsn_ie,
                                           &enable_encryption,
                                           &key_index);
      }
      else
      {
         ret = on_dot1x_auth_start_handler(AL_CONTEXT sta_entry->sta.net_if,
                                           config_info,
                                           &node->sta_addr,
                                           NULL,
                                           &enable_encryption,
                                           &key_index);
      }

      switch (ret)
      {
      case ACCESS_POINT_ON_DOT1X_AUTH_STATUS_ACCEPTED:
         al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,
                      "AP dot11i: Authentication process started for STA "AL_NET_ADDR_STR " over %s",
                      AL_NET_ADDR_TO_STR(&node->sta_addr),
                      sta_entry->sta.net_if->name);
         break;

      case ACCESS_POINT_ON_DOT1X_AUTH_STATUS_FORCE_UNAUTH:
         al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,
                      "AP dot11i: Forcing out un-authorized STA "AL_NET_ADDR_STR " over %s",
                      AL_NET_ADDR_TO_STR(&node->sta_addr),
                      sta_entry->sta.net_if->name);
         if (AL_NET_IF_IS_WIRELESS(sta_entry->sta.net_if))
         {
            access_point_release_sta(AL_CONTEXT sta_entry);
            access_point_release_sta_ex(AL_CONTEXT sta_entry, 1, 1);
            al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESHAP : ERROR : If station net_if is on WIRELESS release the sta "AL_NET_ADDR_STR " over %s : %s : %d\n",AL_NET_ADDR_TO_STR(&node->sta_addr), sta_entry->sta.net_if->name, __func__,__LINE__);
            return;
         }
         break;

      case ACCESS_POINT_ON_DOT1X_AUTH_STATUS_FORCE_AUTH:
         sta_entry->sta.auth_state |= ACCESS_POINT_STA_AUTH_STATE_802_1X_AUTHENTICATED;
         if (enable_encryption)
         {
            sta_entry->sta.key_index   = key_index;
            sta_entry->sta.auth_state |= ACCESS_POINT_STA_AUTH_STATE_ENCRYPTION;
            al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,
                         "AP  : Enabling encryption with key index %d for STA "AL_NET_ADDR_STR,
                         sta_entry->sta.key_index,
                         AL_NET_ADDR_TO_STR(&node->sta_addr));
         }
         else
         {
            sta_entry->sta.auth_state &= ~ACCESS_POINT_STA_AUTH_STATE_ENCRYPTION;
         }
         al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,
                      "AP dot11i: Forcing authorization for STA "AL_NET_ADDR_STR " over %s",
                      AL_NET_ADDR_TO_STR(&node->sta_addr),
                      sta_entry->sta.net_if->name);
         break;
      }
   }
   else
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,
                   "AP dot11i: Implicitly Forcing authorization for STA "AL_NET_ADDR_STR " over %s",
                   AL_NET_ADDR_TO_STR(&node->sta_addr),
                   sta_entry->sta.net_if->name);
   }

   access_point_release_sta(AL_CONTEXT sta_entry);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
}
