/********************************************************************************
* MeshDynamics
* --------------
* File     : access_point_mgt.h
* Comments : Proccesing Management Packets
* Created  : 4/12/2004
* Author   : Bindu Parakala
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  4  |6/15/2004 | assoc_req / re_assoc_req,  ret = int            | Bindu  |
* -----------------------------------------------------------------------------
* |  3  |6/10/2004 | Misc changes + new functions added              | Sriram |
* -----------------------------------------------------------------------------
* |  2  |5/13/2004 | re-assoc & assoc methods added                  | Bindu  |
* -----------------------------------------------------------------------------
* |  1  |4/22/2004 | **Made (major) changes for AL_CONTEXT**         | Bindu  |
* -----------------------------------------------------------------------------
* |  0  |4/12/2004 | Created                                         | Bindu  |
* -----------------------------------------------------------------------------
********************************************************************************/


#include "al.h"
#include "al_packet.h"
#include "al_net_if.h"

#ifndef __ACCESS_POINT_MGT_H__
#define __ACCESS_POINT_MGT_H__

int process_al_802_11_fc_stype_re_assoc_req(AL_CONTEXT_PARAM_DECL al_packet_t *packet, unsigned char *supported_rates, unsigned char *supported_rates_length);
int process_al_802_11_fc_stype_assoc_req_helper(AL_CONTEXT_PARAM_DECL al_packet_t *packet, unsigned char *supported_rates, unsigned char *supported_rates_length);

//void	process_al_802_11_fc_stype_assoc_req_packet	(AL_CONTEXT_PARAM_DECL al_net_if_t* net_if, al_packet_t* packet, int sub_type);
void process_al_802_11_fc_stype_probe_req_packet(AL_CONTEXT_PARAM_DECL al_net_if_t *net_if, al_packet_t *packet);
void process_al_802_11_fc_stype_disassoc_packet(AL_CONTEXT_PARAM_DECL al_net_if_t *net_if, al_packet_t *packet);
void process_al_802_11_fc_stype_auth_packet(AL_CONTEXT_PARAM_DECL al_net_if_t *net_if, al_packet_t *packet);
void process_al_802_11_fc_stype_deauth_packet(AL_CONTEXT_PARAM_DECL al_net_if_t *net_if, al_packet_t *packet);

void send_deauth_notification(AL_CONTEXT_PARAM_DECL al_net_if_t *net_if,
                              al_net_addr_t                     *dest,
                              unsigned short                    reason_code);

void send_disassoc_notification(AL_CONTEXT_PARAM_DECL al_net_if_t *net_if,
                                al_net_addr_t                     *dest,
                                unsigned short                    reason_code);

void send_assoc_reassoc_response(AL_CONTEXT_PARAM_DECL al_net_if_t *net_if,
                                 al_net_addr_t                     *dest,
                                 unsigned short                    type,
                                 unsigned short                    status_code,
                                 unsigned short                    aid,
                                 unsigned char                     *supported_rates,
                                 unsigned char                     supported_rates_length);
#endif /*__ACCESS_POINT_MGT_H__*/
