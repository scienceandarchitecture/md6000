/********************************************************************************
* MeshDynamics
* --------------
* File     : access_point.cpp
* Comments : Access Point
* Created  : 4/7/2004
* Author   : Bindu	 Parakala
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |125  |1/27/2009 | Changes for Uplink Scan Functionality           | Sriram |
* -----------------------------------------------------------------------------
* |124  |8/27/2008 | Changes for Options                             | Sriram |
* -----------------------------------------------------------------------------
* |123  |8/22/2008 | Changes or SIP                                  |Abhijit |
* -----------------------------------------------------------------------------
* |123  |3/5/2008  | DS NET IF ext_config_info set                   | Sriram |
* -----------------------------------------------------------------------------
* |122  |11/20/2007| AID re-used for already associated STAs         | Sriram |
* -----------------------------------------------------------------------------
* |121  |8/14/2007 | TX POWER turn off bug fixed                     | Sriram |
* -----------------------------------------------------------------------------
* |120  |8/14/2007 | access_point_mcast_purge_indirect_entries called| Sriram |
* -----------------------------------------------------------------------------
* |119  |8/2/2007  | Added dynamic TX POWER turn off                 | Sriram |
* -----------------------------------------------------------------------------
* |118  |7/23/2007 | DISASSOC changed to DEAUTH                      | Sriram |
* -----------------------------------------------------------------------------
* |117  |7/17/2007 | authentication_time Added to STA INFO           | Sriram |
* -----------------------------------------------------------------------------
* |116  |7/3/2007  | Put stack vars on heap for assoc                | Sriram |
* -----------------------------------------------------------------------------
* |115  |6/20/2007 | Changes for IGMP Snooping                       |Prachiti|
* -----------------------------------------------------------------------------
* |114  |6/6/2007  | Changes for EFFISTREAM Bitrate                  |Prachiti|
* -----------------------------------------------------------------------------
* |113  |6/6/2007  | access_point_update_sta_last_rx changed         | Sriram |
* -----------------------------------------------------------------------------
* |112  |6/5/2007  | (More) Changes for In-direct VLAN               | Sriram |
* -----------------------------------------------------------------------------
* |111  |5/17/2007 | dot1x_delete only when fully authenticated      | Sriram |
* -----------------------------------------------------------------------------
* |110  |5/17/2007 | access_point_set_sta_vlan added                 | Sriram |
* -----------------------------------------------------------------------------
* |109  |5/16/2007 | net_if name added to al_print_log statement     | Sriram |
* -----------------------------------------------------------------------------
* |108  |4/19/2007 |  Changes for forced disassociation              | Sriram |
* -----------------------------------------------------------------------------
* |107  |2/23/2007 | Changes for EFFISTREAM                          |Prachiti|
* -----------------------------------------------------------------------------
* |106  |2/15/2007 | Disable WM if TXPOWER is set to 0               | Sriram |
* -----------------------------------------------------------------------------
* |105  |2/7/2007  | Removed ACK timeout setting in set_values       | Sriram |
* -----------------------------------------------------------------------------
* |104  |1/30/2007 | Changes for Ethernet VLAN restriction           | Sriram |
* -----------------------------------------------------------------------------
* |103  |1/10/2007 | Changes for VLAN BCAST optimization             | Sriram |
* -----------------------------------------------------------------------------
* |102  |12/6/2006 | Changes for In-direct VLAN                      | Sriram |
* -----------------------------------------------------------------------------
* |101  |11/8/2006 | Changes for compression support                 | Sriram |
* -----------------------------------------------------------------------------
* |100  |10/18/2006| Changes for ETSI Radar                          | Sriram |
* -----------------------------------------------------------------------------
* | 99  |6/14/2006 | Changes for disallow_other_sta	              |Prachiti|
* -----------------------------------------------------------------------------
* | 98  |3/29/2006 | Init function re-arranged                       | Sriram |
* -----------------------------------------------------------------------------
* | 97  |3/15/2006 | Bug in get_sta_essid fixed                      | Sriram |
* -----------------------------------------------------------------------------
* | 96  |3/13/2006 | ACL list implemented                            | Sriram |
* -----------------------------------------------------------------------------
* | 95  |03/09/2006| 802.11e Category Info set for DS net_if         | Sriram |
* -----------------------------------------------------------------------------
* | 94  |03/08/2006| ESSID ignored for RELAY nodes                   | Sriram |
* -----------------------------------------------------------------------------
* | 93  |03/03/2006| Misc Changes of memcpy for dot11e               | Bindu  |
* -----------------------------------------------------------------------------
* | 92  |02/15/2006| Changes for dot11e                              | Bindu  |
* -----------------------------------------------------------------------------
* | 91  |02/08/2006| Changes for Hide SSID                           | Sriram |
* -----------------------------------------------------------------------------
* | 90  |02/03/2006| Misc changes                                    | Abhijit|
* -----------------------------------------------------------------------------
* | 89  |02/01/2006| access_point_get_sta_essid added                | Abhijt |
* -----------------------------------------------------------------------------
* | 88  |1/25/2006 | Removed dot1p_queue variable                    | Bindu  |
* -----------------------------------------------------------------------------
* | 87  |01/17/2006| Changes for aid                                 | Sriram |
* -----------------------------------------------------------------------------
* | 86  |01/04/2006| Added dot1p_queue implementation                | Bindu  |
* -----------------------------------------------------------------------------
* | 85  |01/01/2006| Analyser changes made.                          | Abhijit|
* -----------------------------------------------------------------------------
* | 84  |12/14/2005| acktimeout and regdomain changes                |prachiti|
* -----------------------------------------------------------------------------
* | 83  |10/5/2005 | DS Excerciser re-enabled                        | Sriram |
* -----------------------------------------------------------------------------
* | 82  |10/5/2005 | Changes for enabling MIP                        | Sriram |
* -----------------------------------------------------------------------------
* | 81  |9/20/2005 | Interrupt disable/enable semantics changed      | Sriram |
* -----------------------------------------------------------------------------
* | 80  |9/19/2005 | DS Excerciser logic and other changes           | Sriram |
* -----------------------------------------------------------------------------
* | 79  |9/12/2005 | access_point_enable_wm_encryption Added         | Sriram |
* -----------------------------------------------------------------------------
* | 78  |9/12/2005 | Interface essid checking logic fixed            | Sriram |
* -----------------------------------------------------------------------------
* | 77  |6/15/2005 | disassociate_sta_ex Implemented                 | Sriram |
* -----------------------------------------------------------------------------
* | 76  |6/9/2005  | Dynamic ESSID change fixed                      | Sriram |
* -----------------------------------------------------------------------------
* | 75  |5/20/2005 | Fixed TX Rate changes                           | Sriram |
* -----------------------------------------------------------------------------
* | 74  |5/16/2005 | IMCP assoc not allowed when no DS link          | Sriram |
* -----------------------------------------------------------------------------
* | 73  |5/9/2005  | Service type checking corrected                 | Sriram |
* -----------------------------------------------------------------------------
* | 72  |4/15/2005 | ds_group_cipher_type + other changes            | Sriram |
* -----------------------------------------------------------------------------
* | 71  |4/12/2005 | Packet allocation NULL checking added           | Sriram |
* -----------------------------------------------------------------------------
* | 70  |4/11/2005 | Changes after merging                           | Sriram |
* -----------------------------------------------------------------------------
* | 69  |4/6/2005  | Changes for 802.1x/802.11i                      | Sriram |
* -----------------------------------------------------------------------------
* | 68  |3/24/2005 | Added access_point_get_sta_info                 | Sriram |
* -----------------------------------------------------------------------------
* | 67  |2/24/2005 | VLAN implementation added                       | Abhijit|
* -----------------------------------------------------------------------------
* | 66  |2/09/2005 | Interface based packet Queue                    | Anand  |
* -----------------------------------------------------------------------------
* | 65  |1/8/2005  | Changes for per-interface essid/etc             | Sriram |
* -----------------------------------------------------------------------------
* | 64  |1/7/2005  | AL_NET_ADDR_IS_BROADCAST fix                    | Sriram |
* -----------------------------------------------------------------------------
* | 63  |1/6/2005  | Misc changes                                    | Sriram |
* -----------------------------------------------------------------------------
* | 62  |12/24/2004| ex versions for add_sta/get_sta/release_sta     | Sriram |
* -----------------------------------------------------------------------------
* | 61  |12/22/2004| Changes for 802.11b client in 802.11g mode      | Sriram |
* -----------------------------------------------------------------------------
* | 60  |12/20/2004| Merged changes by Anand for phy_sub_type        | Sriram |
* -----------------------------------------------------------------------------
* | 59  |12/15/2004| set_phy_mode calls removed                      | Sriram |
* -----------------------------------------------------------------------------
* | 58  |12/14/2004| Avoided blind usage of bridge ageing time       | Sriram |
* -----------------------------------------------------------------------------
* | 57  |12/8/2004 | Modifications for access_point_set_values       | Sriram |
* -----------------------------------------------------------------------------
* | 56  |12/8/2004 | Allow patching for non wireless interfaces also | Sriram |
* -----------------------------------------------------------------------------
* | 55  |11/16/2004| compilation errors removed                      | Anand  |
* -----------------------------------------------------------------------------
* | 54  |15/11/2004| capability parameter added to al_process_assoc  |Prachiti|
* -----------------------------------------------------------------------------
* | 53  |11/10/2004| access_point_get_netif_config_info() added      |prachiti|
* -----------------------------------------------------------------------------
* | 52  |11/10/2004| b client checking added                         |prachiti|
* -----------------------------------------------------------------------------
* | 51  |11/1/2004 | use_mac_address added                           | Abhijit|
* -----------------------------------------------------------------------------
* | 50  |10/29/2004| SetMode called before setEssid                  | Anand  |
* -----------------------------------------------------------------------------
* | 49  |9/03/2004 | Changes for mip interface                       | Anand  |
* -----------------------------------------------------------------------------
* | 48  |8/26/2004 | Driver hooks before startup- Mesh Init Packets  | Anand  |
* -----------------------------------------------------------------------------
* | 47  |7/29/2004 | added fn set_values() for MESH to set - essid,  |        |
* |	 |			| beacon_interval, frag_threshold & rts_threshold | Bindu  |
* -----------------------------------------------------------------------------
* | 46  |7/25/2004 | Changed monitor thread wait time                | Sriram |
* -----------------------------------------------------------------------------
* | 45  |7/25/2004 | AP_STATE changed on enable_disable              | Anand  |
* -----------------------------------------------------------------------------
* | 44  |7/24/2004 | Conditional compilation of license check        | Sriram |
* -----------------------------------------------------------------------------
* | 43  |7/24/2004 | Fixed bug in remove_all_sta                     | Sriram |
* -----------------------------------------------------------------------------
* | 42  |7/24/2004 | remove_all_sta, enable/disable processing added | Anand  |
* -----------------------------------------------------------------------------
* | 41  |7/22/2004 | misc changes                                    | Bindu  |
* -----------------------------------------------------------------------------
* | 40  |7/21/2004 | License check added                             | Bindu  |
* -----------------------------------------------------------------------------
* | 39  |7/19/2004 | if_transmit_list removal and misc changes       | Sriram |
* -----------------------------------------------------------------------------
* | 38  |7/19/2004 | if_transmit_list added to set transmit rate     | Bindu  |
* -----------------------------------------------------------------------------
* | 37  |7/5/2004  | bug fixed for STOPPING monitor thread if started| Anand  |
* -----------------------------------------------------------------------------
* | 36  |7/2/2004  | Misc changes for release sta                    | Anand  |
* -----------------------------------------------------------------------------
* | 35  |6/30/2004 | Misc changes - monitor thread                   | Anand  |
* -----------------------------------------------------------------------------
* | 34  |6/29/2004 | Monitor Thread exit added in stop               | Anand  |
* -----------------------------------------------------------------------------
* | 33  |6/21/2004 | added calls to access_point_release_sta()       | Bindu  |
* -----------------------------------------------------------------------------
* | 32  |6/21/2004 | access_point_sta_monitor_thread() added         | Bindu  |
* -----------------------------------------------------------------------------
* | 31  |6/16/2004 | Fixes for pSeudo WM                             | Anand  |
* -----------------------------------------------------------------------------
* | 30  |6/14/2004 | Added AL_ASSERT macro                           | Bindu  |
* -----------------------------------------------------------------------------
* | 29  |6/11/2004 | Added message for STA removal                   | Sriram |
* -----------------------------------------------------------------------------
* | 28  |6/10/2004 | Misc changes to al_print_log text               | Sriram |
* -----------------------------------------------------------------------------
* | 27  |6/9/2004  | Implemented access_point_disassociate_sta       | Sriram |
* -----------------------------------------------------------------------------
* | 26  |6/9/2004  | Updated last rx time on re-association          | Sriram |
* -----------------------------------------------------------------------------
* | 25  |6/7/2004  | Misc Fixes                                      | Sriram |
* -----------------------------------------------------------------------------
* | 24  |6/4/2004  | Misc fixes for Torna                            | Sriram |
* -----------------------------------------------------------------------------
* | 23  |6/4/2004  | Print log macros used                           | Bindu  |
* -----------------------------------------------------------------------------
* | 22  |6/4/2004  | Misc fixes for Torna                            | Sriram |
* -----------------------------------------------------------------------------
* | 21  |6/2/2004  | Returned 0 when already associated              | Sriram |
* -----------------------------------------------------------------------------
* | 20  |6/2/2004  | added auth & assoc function impls               | Bindu  |
* -----------------------------------------------------------------------------
* | 19  |6/1/2004  | state changed in ap_stop()                      | Bindu  |
* -----------------------------------------------------------------------------
* | 18  |5/31/2004 | set_thread_name called                          | Bindu  |
* -----------------------------------------------------------------------------
* | 17  |5/31/2004 | include access_point_sta.h                      | Bindu  |
* -----------------------------------------------------------------------------
* | 16  |5/31/2004 | included access_point_thread.h etc              | Bindu  |
* -----------------------------------------------------------------------------
* | 15  |5/31/2004 | included access_point_globals.h                 | Bindu  |
* -----------------------------------------------------------------------------
* | 14  |5/28/2004 | removed busy flag setting for packet            | Bindu  |
* -----------------------------------------------------------------------------
* | 13  |5/25/2004 | disassoc only if sta is associated (in AP stop) | Anand  |
* -----------------------------------------------------------------------------
* | 12  |5/21/2004 | Log type added to al_print_log                  | Anand  |
* -----------------------------------------------------------------------------
* | 11  |5/20/2004 | added al_print_log sts                          | Bindu  |
* -----------------------------------------------------------------------------
* | 10  |5/12/2004 | Added set_mode for DS Interface                 | Bindu  |
* -----------------------------------------------------------------------------
* |  9  |5/12/2004 | globals.config.ds_net_if value set              | Bindu  |
* -----------------------------------------------------------------------------
* |  8  |5/11/2004 | AP_start_thread fn added & state checking done  | Anand  |
* -----------------------------------------------------------------------------
* |  7  |5/7/2004	| Added stop_event_handle var functionality       | Bindu  |
* -----------------------------------------------------------------------------
* |  6  |5/5/2004  | Packet Debug params added                       | Anand  |
* -----------------------------------------------------------------------------
* |  5  |5/5/2004  | Process Mgm Frame hook added.                   | Anand  |
* -----------------------------------------------------------------------------
* |  4  |4/30/2004 | Added al_print_log sts                          | Bindu  |
* -----------------------------------------------------------------------------
* |  3  |4/29/2004 | Helper functions added to access from c++       | Anand  |
* -----------------------------------------------------------------------------
* |  2  |4/22/2004 | Made changes for packet buffer position setting | Bindu  |
* -----------------------------------------------------------------------------
* |  1  |4/22/2004 | **Made (major) changes for AL_CONTEXT**         | Bindu  |
* -----------------------------------------------------------------------------
* |  0  |4/7/2004	| Created                                         | Bindu  |
* -----------------------------------------------------------------------------
********************************************************************************/

#include <linux/string.h>
#include <linux/kernel.h>
#include "al.h"
#include "access_point.h"
#include "access_point_queue.h"
#include "al_impl_context.h"
#include "access_point_thread.h"
#include "access_point_sta.h"
#include "access_point_globals.h"
#include "al_print_log.h"
#include "access_point_mgt.h"
#include "al_conf.h"
#include "access_point_vlan.h"
#include "access_point_dot11i.h"
#include "mesh_ext.h"
#include "access_point_tester_testee.h"
#include "access_point_acl.h"
#include "access_point_multicast.h"
#include "meshap_dhcp.h"
#include "meshap_arp.h"
#include "meshap_sip.h"
#include "meshap_option.h"
#include <linux/netdevice.h>
#include <linux/rtnetlink.h>
#include <linux/jiffies.h>
#include <linux/semaphore.h>
#include <linux/wireless.h>
#include "meshap.h"
#include "mesh_defs.h"
#include "meshap_core.h"
#include "../../md_mesh/include/mesh_table.h"

#include <linux/cpumask.h>

#ifdef AL_USE_ENCRYPTION
#include "al_codec.h"
#endif

al_spinlock_t gbl_splock;
AL_DEFINE_PER_CPU(int, gbl_flag);
AL_DECLARE_GLOBAL_EXTERN(mesh_config_info_t * mesh_config);

struct al_mutex gbl_mutex;
int gbl_muvar, gbl_mupid;
#define PHY_IF_COUNT 6	//VLAN

unsigned char _SIGNATURE_[] =
{
   'T',  'O',  'R',  'N',  'A',  'S',  'T', 'A', 'R', 'T',
   0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
   0xff, 0xff, 0xff, 0xff, 0xff, 0xff,	/*PHY and VLAN count and APP count (3 Unused bytes)*/
   0xff, 0xff, 0xff, 0xff, 0xff, 0xff,	/*6 RADIO MAC's start*/
   0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
   0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
   0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
   0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
   0xff, 0xff, 0xff, 0xff, 0xff, 0xff,	/*RADIO MAC End*/
   0xff, 0xff, 0xff, 0xff, 0xff, 0xff,	/*1 Virtual Intefce MAC start*/
   0xff, 0xff, 0xff, 0xff, 0xff, 0xff,	/*5 VLAN MAC's start*/
   0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
   0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
   0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
   0xff, 0xff, 0xff, 0xff, 0xff, 0xff,	/*VLAN MAC End*/
	0xff, 0xff, 0xff, 0xff, 0xff, 0xff, /*4 APP MAC's start*/
	0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
	0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
	0xff, 0xff, 0xff, 0xff, 0xff, 0xff, /*APP MAC End*/
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, /*Ethernet(eth1) mac address*/
   'T',  'O',  'R',  'N',  'A',  'E',  'N', 'D'
};

unsigned char *_BEGIN_SIGNATURE = _SIGNATURE_;
unsigned char *_MAC_ADDRESS     = &_SIGNATURE_[10];
unsigned char *_RMAC_ADDRESS     = &_SIGNATURE_[22];
unsigned char *_VIRTUALMAC_ADDRESS     = &_SIGNATURE_[58];
unsigned char *_VLANMAC_ADDRESS     = &_SIGNATURE_[64];
unsigned char *_APPMAC_ADDRESS     = &_SIGNATURE_[94];
unsigned char *_ETHMAC_ADDRESS     = &_SIGNATURE_[118];
unsigned char *_END_SIGNATURE   = &_SIGNATURE_[124];

unsigned char use_mac_address[6];

int switch_over_2other_ds(void);

//CREE_DEMO vendor_info #include "mesh_ng_fsm.h"
extern void mesh_ng_fsm_update_downlink_vendor_info(AL_CONTEXT_PARAM_DECL_SINGLE);


static AL_INLINE void _imcp_sta_security_helper(AL_CONTEXT_PARAM_DECL al_802_11_md_ie_t *md_ie,
                                                al_802_11_md_ie_t                       *md_ie_out,
                                                access_point_netif_config_info_t        *al_netif_config_info,
                                                access_point_sta_entry_t                *sta,
                                                al_net_if_t                             *al_net_if,
                                                al_802_11_operations_t                  *extended_operations,
                                                unsigned char                           *imcp_key_ie_valid_out,
                                                al_802_11_information_element_t         *imcp_key_ie_out,
                                                unsigned char                           compr_enable)
{
   unsigned char random_key[AL_802_11_MD_IE_MAX_PAIRWISE_KEY_LENGTH];

#ifdef AL_USE_ENCRYPTION
   int enc_method, i;

   enc_method = (md_ie->type == AL_802_11_MD_IE_TYPE_FIPS_ASSOC_REQUEST) ?
                AL_ENCRYPTION_DECRYPTION_METHOD_AES_KEY_WRAP : AL_ENCRYPTION_DECRYPTION_METHOD_AES_ECB;
#endif
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Enter %s<%d>\n",__func__,__LINE__);
   if (md_ie->data.assoc_request.assoc_type == AL_802_11_MD_IE_ASSOCIATION_TYPE_FINAL)
   {
      memset(md_ie_out, 0, sizeof(md_ie_out));

      md_ie_out->version = md_ie->version;                      /** Support whatever version supported by the child node */
      md_ie_out->type    = (md_ie->type == AL_802_11_MD_IE_TYPE_FIPS_ASSOC_REQUEST) ?
                           AL_802_11_MD_IE_TYPE_FIPS_ASSOC_RESPONSE : AL_802_11_MD_IE_TYPE_ASSOC_RESPONSE;

      if (al_netif_config_info->security_info.dot11.wep.enabled ||
          al_netif_config_info->security_info.dot11.rsn.enabled)
      {
         if ((al_netif_config_info->group_key.cipher_type == AL_802_11_SECURITY_INFO_RSN_CIPHER_WEP) &&
             (al_netif_config_info->group_key.strength == AL_802_11_SECURITY_INFO_STRENGTH_104))
         {
            md_ie_out->data.assoc_response.group_cipher_type = AL_802_11_SECURITY_INFO_RSN_CIPHER_WEP_104;
         }
         else
         {
            md_ie_out->data.assoc_response.group_cipher_type = al_netif_config_info->group_key.cipher_type;
         }

         md_ie_out->data.assoc_response.group_key_index = al_netif_config_info->group_key.key_index;

#ifdef AL_USE_ENCRYPTION
         al_encrypt(AL_CONTEXT al_netif_config_info->group_key.key,
                    AL_802_11_MAX_KEY_LENGTH * AL_802_11_MAX_SECURITY_KEYS,
                    globals.config.mesh_imcp_key,
                    globals.config.mesh_imcp_key_length,
                    md_ie_out->data.assoc_response.group_key,
                    enc_method);
#else
         memcpy(md_ie_out->data.assoc_response.group_key,
                al_netif_config_info->group_key.key,
                AL_802_11_MD_IE_MAX_GROUP_KEY_LENGTH);
#endif
      }
      else
      {
         md_ie_out->data.assoc_response.group_cipher_type = al_netif_config_info->group_key.cipher_type;
         md_ie_out->data.assoc_response.group_key_index   = al_netif_config_info->group_key.key_index;

#ifdef AL_USE_ENCRYPTION
         al_encrypt(AL_CONTEXT al_netif_config_info->group_key.key,
                    AL_802_11_MAX_KEY_LENGTH * AL_802_11_MAX_SECURITY_KEYS,
                    globals.config.mesh_imcp_key,
                    globals.config.mesh_imcp_key_length,
                    md_ie_out->data.assoc_response.group_key,
                    enc_method);
#else
         memcpy(md_ie_out->data.assoc_response.group_key,
                al_netif_config_info->group_key.key,
                AL_802_11_MD_IE_MAX_GROUP_KEY_LENGTH);
#endif
      }

      al_cryptographic_random_bytes(AL_CONTEXT random_key, AL_802_11_MD_IE_MAX_PAIRWISE_KEY_LENGTH);

#ifdef AL_USE_ENCRYPTION
      al_encrypt(AL_CONTEXT random_key,
                 AL_802_11_MD_IE_MAX_PAIRWISE_KEY_LENGTH,
                 globals.config.mesh_imcp_key,
                 globals.config.mesh_imcp_key_length,
                 md_ie_out->data.assoc_response.pairwise_key,
                 enc_method);

#else
      memcpy(md_ie_out->data.assoc_response.pairwise_key, random_key, AL_802_11_MD_IE_MAX_PAIRWISE_KEY_LENGTH);
#endif

      al_802_11_format_md_ie(AL_CONTEXT md_ie_out, imcp_key_ie_out);
      *imcp_key_ie_valid_out = 1;

      memset(&sta->sta.sec_key, 0, sizeof(sta->sta.sec_key));
      sta->sta.sec_key.cipher_type = AL_802_11_SECURITY_INFO_RSN_CIPHER_CCMP;
      memcpy(&sta->sta.sec_key.destination, &sta->sta.address, sizeof(al_net_addr_t));
      sta->sta.sec_key.enabled = 1;
      memcpy(sta->sta.sec_key.key, random_key, AL_802_11_MD_IE_MAX_PAIRWISE_KEY_LENGTH);
      sta->sta.sec_key.key_index = 0;

      sta->sta.key_index   = 0;
      sta->sta.auth_state |= ACCESS_POINT_STA_AUTH_STATE_ENCRYPTION;
   }
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Exit %s<%d>\n",__func__,__LINE__);
}


static int al_process_auth(AL_CONTEXT_PARAM_DECL al_net_if_t *al_net_if, al_net_addr_t *sta_addr)
{
   access_point_sta_entry_t         *sta_entry;
   access_point_netif_config_info_t *config_info_old;
   al_802_11_operations_t           *ex_op_old;

   AL_ASSERT("al_process_auth", al_net_if != NULL);
   AL_ASSERT("al_process_auth", sta_addr != NULL);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Enter %s<%d>\n",__func__,__LINE__);

   sta_entry = access_point_add_sta_ex(AL_CONTEXT al_net_if, sta_addr, 0);

   if (sta_entry->sta.state == ACCESS_POINT_STA_STATE_ASSOCIATED)
   {
      if (al_net_if != sta_entry->sta.net_if)
      {
         al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,
                      "AP  : "AL_NET_ADDR_STR " re-associating to %s from %s",
                      AL_NET_ADDR_TO_STR(sta_addr),
                      al_net_if->name,
                      sta_entry->sta.net_if->name);
      }

      ex_op_old       = sta_entry->sta.net_if->get_extended_operations(sta_entry->sta.net_if);
      config_info_old = (access_point_netif_config_info_t *)sta_entry->sta.net_if->config.ext_config_info;

      if (sta_entry->sta.is_imcp)
      {
         ex_op_old->remove_downlink_round_robin_child(sta_entry->sta.net_if, sta_addr);
         if (AL_ATOMIC_GET(config_info_old->imcp_sta_count))
         {
            AL_ATOMIC_DEC(config_info_old->imcp_sta_count);
         }
         else
         {
            al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,
                         "AP: imcp_sta_count for %s was strangely %d",
                         sta_entry->sta.net_if,
                         AL_ATOMIC_GET(config_info_old->imcp_sta_count));
         }
      }
      else
      {
         if (sta_entry->sta.vlan_info == &default_vlan_info)
         {
            if (AL_ATOMIC_GET(config_info_old->sta_count))
            {
               AL_ATOMIC_DEC(config_info_old->sta_count);
            }
            else
            {
               al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,
                            "AP: sta_count for %s was strangely %d",
                            sta_entry->sta.net_if,
                            AL_ATOMIC_GET(config_info_old->sta_count));
            }
         }
         else if (sta_entry->sta.vlan_info != NULL)
         {
            access_point_decr_net_if_vlan_count(AL_CONTEXT sta_entry->sta.vlan_info, sta_entry->sta.net_if);
         }
      }

      if (sta_entry->sta.auth_state & ACCESS_POINT_STA_AUTH_STATE_ENCRYPTION &&
          (sta_entry->sta.key_index >= 4))
      {
         printk(KERN_EMERG "Got station security key index >=4\n");
         //ex_op_old->release_security_key(sta_entry->sta.net_if,sta_entry->sta.key_index,sta_addr->bytes);
         sta_entry->sta.auth_state &= ~ACCESS_POINT_STA_AUTH_STATE_ENCRYPTION;
      }

      sta_entry->sta.auth_state &= ~ACCESS_POINT_STA_AUTH_STATE_COMPRESSION;

      if (config_info_old->security_info.dot11.rsn.enabled &&
          sta_entry->sta.auth_state & ACCESS_POINT_STA_AUTH_STATE_802_1X_AUTHENTICATED)
      {
         on_dot1x_auth_delete(AL_CONTEXT sta_entry->sta.net_if, config_info_old, &sta_entry->sta.address);
         sta_entry->sta.auth_state &= ~ACCESS_POINT_STA_AUTH_STATE_802_1X_INITIATED;
         sta_entry->sta.auth_state &= ~ACCESS_POINT_STA_AUTH_STATE_802_1X_AUTHENTICATED;
      }

      if (sta_entry->sta.net_if != al_net_if)
      {
         access_point_sta_release_aid(sta_entry->sta.net_if, sta_entry->sta.aid);
         sta_entry->sta.aid = 0;
      }

      sta_entry->sta.auth_state &= ~ACCESS_POINT_STA_AUTH_STATE_RSN;
      sta_entry->sta.is_imcp     = 0;
      sta_entry->sta.net_if      = al_net_if;
   }

   sta_entry->sta.state = ACCESS_POINT_STA_STATE_AUTHENTICATED;
   sta_entry->sta.last_packet_received = al_get_tick_count(AL_CONTEXT_SINGLE);
   sta_entry->sta.authentication_time  = al_get_tick_count(AL_CONTEXT_SINGLE);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Exit %s<%d>\n",__func__,__LINE__);

   return 0;
}


static void al_update_sta_ht(access_point_netif_config_info_t *netif, access_point_sta_entry_t  *sta)
{
    u16 ht_capab;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Enter %s<%d>\n",__func__,__LINE__);

    ht_capab = (sta->sta.ht_capab.ht_capabilities_info);

    if ((ht_capab & AL_HT_CAP_INFO_GREEN_FIELD) == 0) {
        if (!sta->sta.no_ht_gf_set) {
            sta->sta.no_ht_gf_set = 1;
            netif->num_sta_ht_no_gf++;
        }
    }
    if ((ht_capab & AL_HT_CAP_INFO_SUPP_CHANNEL_WIDTH_SET) == 0) {
        if (!sta->sta.ht_20mhz_set) {
            sta->sta.ht_20mhz_set = 1;
            netif->num_sta_ht_20mhz++;
        }
    }
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Exit %s<%d>\n",__func__,__LINE__);
}

static void al_update_sta_no_ht(access_point_netif_config_info_t *netif, access_point_sta_entry_t  *sta)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Enter %s<%d>\n",__func__,__LINE__);
    if (!sta->sta.no_ht_set) {
        sta->sta.no_ht_set = 1;
        netif->num_sta_no_ht++;
    }
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Exit %s<%d>\n",__func__,__LINE__);
}


/*
   op_mode
   Set to 0 (HT pure) under the followign conditions
   - all STAs in the BSS are 20/40 MHz HT in 20/40 MHz BSS or
   - all STAs in the BSS are 20 MHz HT in 20 MHz BSS
   Set to 1 (HT non-member protection) if there may be non-HT STAs
   in both the primary and the secondary channel
   Set to 2 if only HT STAs are associated in BSS,
   however and at least one 20 MHz HT STA is associated
   Set to 3 (HT mixed mode) when one or more non-HT STAs are associated
   */
int al_ht_operation_update(access_point_netif_config_info_t *netif)
{
    u16 cur_oper_mode, new_oper_mode;
    int oper_mode_changes = 0;


    al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Enter %s<%d>\n",__func__,__LINE__);
    if (!(netif->ht_oper_mode & AL_HT_OPER_OP_MODE_NON_GF_HT_STAS_PRESENT)
            && netif->num_sta_ht_no_gf) {
        netif->ht_oper_mode |= AL_HT_OPER_OP_MODE_NON_GF_HT_STAS_PRESENT;
        oper_mode_changes++;
    } else if ((netif->ht_oper_mode &
                AL_HT_OPER_OP_MODE_NON_GF_HT_STAS_PRESENT) &&
            netif->num_sta_ht_no_gf == 0) {
        netif->ht_oper_mode &= ~AL_HT_OPER_OP_MODE_NON_GF_HT_STAS_PRESENT;
        oper_mode_changes++;
    }

    if (!(netif->ht_oper_mode & AL_HT_OPER_OP_MODE_OBSS_NON_HT_STAS_PRESENT) &&
            (netif->num_sta_no_ht || netif->olbc_ht)) {
        netif->ht_oper_mode |= AL_HT_OPER_OP_MODE_OBSS_NON_HT_STAS_PRESENT;
        oper_mode_changes++;
    } else if ((netif->ht_oper_mode &
                AL_HT_OPER_OP_MODE_OBSS_NON_HT_STAS_PRESENT) &&
            (netif->num_sta_no_ht == 0 && !netif->olbc_ht)) {
        netif->ht_oper_mode &= ~AL_HT_OPER_OP_MODE_OBSS_NON_HT_STAS_PRESENT;
        oper_mode_changes++;
    }

    if (netif->num_sta_no_ht)
        new_oper_mode = AL_HT_PROT_NON_HT_MIXED;
    else if (netif->sec_chan_offset && netif->num_sta_ht_20mhz)
        new_oper_mode = AL_HT_PROT_20MHZ_PROTECTION;
    else if (netif->olbc_ht)
        new_oper_mode = AL_HT_PROT_NONMEMBER_PROTECTION;
    else
        new_oper_mode = AL_HT_PROT_NO_PROTECTION;

    cur_oper_mode = netif->ht_oper_mode & AL_HT_OPER_OP_MODE_HT_PROT_MASK;
    if (cur_oper_mode != new_oper_mode) {
        netif->ht_oper_mode &= ~AL_HT_OPER_OP_MODE_HT_PROT_MASK;
        netif->ht_oper_mode |= new_oper_mode;
        oper_mode_changes++;
    }
    al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Exit %s<%d>\n",__func__,__LINE__);


    return oper_mode_changes;
}



static int al_process_assoc(AL_CONTEXT_PARAM_DECL al_net_if_t *al_net_if,
                            al_net_addr_t                     *sta_addr,
                            unsigned short                    capability,
                            int                               ie_count,
                            al_802_11_information_element_t   *ie,
                            unsigned char                     *imcp_key_ie_valid_out,
                            al_802_11_information_element_t   *imcp_key_ie_out)
{
   static unsigned char supported_rates[8] =
   {
      0x82, 0x84, 0x8b, 0x96
   };

   al_802_11_ht_capabilities_t *ht_capab_data ;
   al_802_11_vht_capabilities_t *vht_capab_data ;
   al_802_11_wmm_information_element_t *wmm_parameter ;
   access_point_sta_entry_t *sta;
   unsigned short           status_code_out;
   int                              i;
   int                              rsn_parsed;
   int                              md_ie_found;
   al_802_11_rsn_ie_t               *rsn_ie;
   al_802_11_md_ie_t                *md_ie;
   al_802_11_md_ie_t                *md_ie_out;
   char                             net_if_essid[AL_802_11_ESSID_MAX_SIZE + 1];
   int                              net_if_essid_length;
   int                              net_if_config_essid_length;
   unsigned char                    supported_rates_out[8];
   unsigned char                    supported_rates_length_out;
   access_point_netif_config_info_t *al_netif_config_info;
   al_802_11_operations_t           *extended_operations;
   int                              phy_mode;
   int                              ret;
   access_point_vlan_info_t         *vlan_info;
   al_802_11_information_element_t  *ie_essid;
   int                              ret_val;
   int                              ht_ie_found = 0;
   int                              vht_ie_found = 0;
   int                              wmm_ie_found = 0;
   int                              phy_sub_type;

   AL_ASSERT("al_process_assoc", al_net_if != NULL);
   AL_ASSERT("al_process_assoc", sta_addr != NULL);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Enter %s<%d>\n",__func__,__LINE__);

   ie_essid             = NULL;
   extended_operations  = al_net_if->get_extended_operations(al_net_if);
   al_netif_config_info = (access_point_netif_config_info_t *)al_net_if->config.ext_config_info;
   sta = access_point_get_sta_ex(AL_CONTEXT sta_addr, 0);

   if (sta == NULL)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:ERROR: %s<%d> associating sta over %s was NULL\n"
	  ,__func__,__LINE__, al_net_if->name);
	  (tx_rx_pkt_stats.packet_drop[78])++;
      return -1;
   }

   rsn_parsed  = 0;
   md_ie_found = 0;
   ht_ie_found = 0;
   vht_ie_found = 0;
   ret_val     = -1;

   rsn_ie    = (al_802_11_rsn_ie_t *)al_heap_alloc(AL_CONTEXT sizeof(al_802_11_rsn_ie_t)AL_HEAP_DEBUG_PARAM);
   md_ie     = (al_802_11_md_ie_t *)al_heap_alloc(AL_CONTEXT sizeof(al_802_11_md_ie_t)AL_HEAP_DEBUG_PARAM);
   md_ie_out = (al_802_11_md_ie_t *)al_heap_alloc(AL_CONTEXT sizeof(al_802_11_md_ie_t)AL_HEAP_DEBUG_PARAM);

   phy_sub_type = meshap_is_N_or_AC_supported_interface(al_net_if->phy_hw_type);

   for (i = 0; i < ie_count; i++)
   {
      if (((ie[i].element_id == AL_802_11_EID_RSN) ||
           (ie[i].element_id == AL_802_11_EID_VENDOR_PRIVATE)) &&
          !rsn_parsed)
      {
         ret = al_802_11_parse_rsn_ie(AL_CONTEXT & ie[i], rsn_ie);
         if (ret == 0)
         {
            rsn_parsed                 = 1;
            sta->sta.auth_state       |= ACCESS_POINT_STA_AUTH_STATE_RSN;
            sta->sta.rsn_ie.element_id = ie[i].element_id;
            sta->sta.rsn_ie.length     = ie[i].length;
            memcpy(sta->sta.rsn_ie.data, ie[i].data, ie[i].length);
            al_802_11_cleanup_rsn_ie(AL_CONTEXT rsn_ie);
         }
      }

      if ((ie[i].element_id == AL_802_11_EID_VENDOR_PRIVATE) && !md_ie_found)
      {
         ret = al_802_11_parse_md_ie(AL_CONTEXT & ie[i], md_ie);
         if ((ret == 0) && ((md_ie->type == AL_802_11_MD_IE_TYPE_ASSOC_REQUEST) || (md_ie->type == AL_802_11_MD_IE_TYPE_FIPS_ASSOC_REQUEST)))
         {
            md_ie_found      = 1;
            sta->sta.is_imcp = 1;
         }
      }

      if ((ie[i].element_id == AL_802_11_EID_SSID) && (ie_essid == NULL))
      {
         ie_essid = &ie[i];
      }

      if (ie[i].element_id == AL_802_11_EID_HT_CAPAB)
      {
              ht_ie_found = 1 ;
              ht_capab_data = (al_802_11_ht_capabilities_t *)ie[i].data;
              sta->sta.ht_capab.ht_capabilities_info = le16_to_cpu(ht_capab_data->ht_capabilities_info);
              sta->sta.ht_capab.a_mpdu_params = ht_capab_data->a_mpdu_params;
              memcpy(sta->sta.ht_capab.supported_mcs_set,ht_capab_data->supported_mcs_set,16);
              sta->sta.ht_capab.ht_extended_capabilities = le16_to_cpu(ht_capab_data->ht_extended_capabilities);
              sta->sta.ht_capab.tx_bf_capability_info = le32_to_cpu(ht_capab_data->tx_bf_capability_info);
              sta->sta.ht_capab.asel_capabilities = ht_capab_data->asel_capabilities;
      }

      if (ie[i].element_id == AL_802_11_EID_VHT_CAPAB)
      {
              vht_ie_found = 1 ;
              vht_capab_data = (al_802_11_vht_capabilities_t *)ie[i].data;
              sta->sta.vht_capab.vht_capabilities_info = le32_to_cpu(vht_capab_data->vht_capabilities_info);
              sta->sta.vht_capab.vht_mcs_set.rx_map = vht_capab_data->vht_mcs_set.rx_map;
              sta->sta.vht_capab.vht_mcs_set.rx_map = vht_capab_data->vht_mcs_set.rx_map;
              sta->sta.vht_capab.vht_mcs_set.rx_highest = vht_capab_data->vht_mcs_set.rx_highest;
              sta->sta.vht_capab.vht_mcs_set.tx_map = vht_capab_data->vht_mcs_set.tx_map;
              sta->sta.vht_capab.vht_mcs_set.tx_highest = vht_capab_data->vht_mcs_set.tx_highest;
      }

      if (ie[i].element_id == AL_802_11_EID_VENDOR_SPECIFIC)
      {
          wmm_parameter = (al_802_11_wmm_information_element_t *)ie[i].data;
          if (ie[i].length < sizeof(al_802_11_wmm_information_element_t)) {
              al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP:ERROR: %s<%d> WMM/WME IE length is less\n",__func__,__LINE__);
			  (tx_rx_pkt_stats.packet_drop[77])++;
              return 0;
      }


          if ( (ie[i].length ==  sizeof(al_802_11_wmm_information_element_t) )&&
                  (wmm_parameter->oui_type ==  AL_WMM_OUI_TYPE) &&
                  (wmm_parameter->oui_subtype == AL_WMM_OUI_SUBTYPE_INFORMATION_ELEMENT) && 
                  (wmm_parameter->version == AL_WMM_VERSION)) {
              memcpy(&sta->sta.wmm_element,wmm_parameter,sizeof(al_802_11_wmm_information_element_t));
              wmm_ie_found = 1 ;
                  al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"MESH_AP:INFO: %s<%d> Validating WMM IE: OUI %02x:%02x:%02x  "
                  "OUI type %d  OUI sub-type %d  version %d  QoS info 0x%x",__func__, __LINE__,
                  wmm_parameter->oui[0], wmm_parameter->oui[1], wmm_parameter->oui[2], wmm_parameter->oui_type,
                  wmm_parameter->oui_subtype, wmm_parameter->version, wmm_parameter->qos_info);

          }else{
              al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"MESH_AP:INFO: %s<%d> Unsupported WMM IE Subtype/Version "
			  "STA_MAC: "MACSTR"\n", __func__, __LINE__, MAC2STR(sta_addr->bytes));
   }
      }

   }

   if (ht_ie_found || vht_ie_found || wmm_ie_found){
       sta->sta.wmm_flag_set = 1 ;
   }else {
        al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"MESH_AP:INFO: %s<%d> WMM Flag Not Set, Station is Not a  HT capable"
		"STA_MAC: "MACSTR"\n",__func__, __LINE__, MAC2STR(sta_addr->bytes));
   }
   if(ht_ie_found) {
       sta->sta.ht_flag_set = 1;
       al_update_sta_ht(al_netif_config_info , sta );
       if(al_ht_operation_update(al_netif_config_info)){
       extended_operations->set_ht_operation(al_netif_config_info->net_if ,&al_netif_config_info->ht_oper_mode );
       }
   }else {
       al_update_sta_no_ht(al_netif_config_info , sta );
       if(al_ht_operation_update(al_netif_config_info)){
       extended_operations->set_ht_operation(al_netif_config_info->net_if ,&al_netif_config_info->ht_oper_mode );
   }
   }
   if (vht_ie_found)
       sta->sta.vht_flag_set = 1;

/*  if interface is pure N mode then station should support HT or else dont allow for association ;
 *  same for pure AC station needs to support VHT for else dont allow for association */
  if(((al_net_if->phy_hw_type == AL_CONF_IF_PHY_SUB_TYPE_802_11_N_2_4GHZ) || (al_net_if->phy_hw_type == AL_CONF_IF_PHY_SUB_TYPE_802_11_N_5GHZ)) && (!ht_ie_found) ){
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP:ERROR: %s<%d> Associating sta "AL_NET_ADDR_STR " "
	  "over %s found doesnot support HT,so not able to associate to the require HT interface\n", __func__, __LINE__,
	  AL_NET_ADDR_TO_STR(sta_addr), al_net_if->name);
	  access_point_release_sta_ex2(AL_CONTEXT sta, 0, 0, 0);
      goto _lb_ret;

  }

   if((al_net_if->phy_hw_type == AL_CONF_IF_PHY_SUB_TYPE_802_11_AC) && (!ht_ie_found) && (!vht_ie_found) ){
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP:ERROR: %s<%d> Associating sta "AL_NET_ADDR_STR " "
	  "over %s found doesnot support VHT,so not able to associate to the require VHT interface\n", __func__, __LINE__,
	  AL_NET_ADDR_TO_STR(sta_addr), al_net_if->name);
       access_point_release_sta_ex2(AL_CONTEXT sta, 0, 0, 0);
       goto _lb_ret;
   }
   /**
    * Check if the STA is found in the ACL or was previously assigned an in-direct VLAN.
    * If found, we can skip the VLAN assignment part and strictly assign to
    * the specified VLAN
    */

   {
      int allow;
      access_point_vlan_info_t *vlan_info;
      int dot11e_enabled;
      int dot11e_catgory;

      if (access_point_acl_lookup_entry(AL_CONTEXT sta_addr, &vlan_info, &allow, &dot11e_enabled, &dot11e_catgory))
      {
         if (!allow)
         {
            al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,
                         "MESH_AP:ERROR: %s<%d> Associating sta "AL_NET_ADDR_STR " over %s found disallowed in ACL\n",
                         __func__, __LINE__, AL_NET_ADDR_TO_STR(sta_addr),al_net_if->name);
            access_point_release_sta_ex2(AL_CONTEXT sta, 0, 0, 0);
            goto _lb_ret;
         }

         if (!sta->sta.is_imcp)
         {
            al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,
                         "MESH_AP:INFO: %s<%d> Associating sta "AL_NET_ADDR_STR " over %s found allowed in ACL on vlan %s\n",
                         __func__, __LINE__, AL_NET_ADDR_TO_STR(sta_addr), al_net_if->name, vlan_info->name);
            sta->sta.vlan_info   = vlan_info;
            sta->sta.auth_state |= ACCESS_POINT_STA_AUTH_STATE_ACL;
            if (vlan_info == &default_vlan_info)
            {
               sta->sta.dot11e_enabled  = dot11e_enabled;
               sta->sta.dot11e_category = dot11e_catgory;
            }
            goto _after_essid_check;
         }
      }
      else
      {
         if ((sta->sta.is_imcp == 0) && access_point_acl_get_disallow_other_stas(AL_CONTEXT_SINGLE))
         {
            al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,
                         "MESH_AP:ERROR: %s<%d> Associating sta "AL_NET_ADDR_STR " over %s found excluded in ACL\n",
                         __func__, __LINE__, AL_NET_ADDR_TO_STR(sta_addr), al_net_if->name);
            access_point_release_sta_ex2(AL_CONTEXT sta, 0, 0, 0);
            goto _lb_ret;
         }
      }
   }

   if (ie_essid != NULL)
   {
      memset(net_if_essid, 0, AL_802_11_ESSID_MAX_SIZE + 1);
      extended_operations->get_essid(al_net_if, net_if_essid, AL_802_11_ESSID_MAX_SIZE);
      net_if_essid_length = strlen(net_if_essid);

      if (sta->sta.is_imcp)
      {
         /**
          * For our own relay nodes, we ignore the ESSID with which they associate
          * and always add them to the default vlan
          */
         ie_essid->length = net_if_essid_length;
         memcpy(ie_essid->data, net_if_essid, ie_essid->length);
      }

      if (ie_essid->length <= 0)
      {
         al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP:ERROR: %s<%d> Invalid ESSID IE length %d for "
		 ""AL_NET_ADDR_STR " associating sta over %s\n", __func__, __LINE__, ie_essid->length, AL_NET_ADDR_TO_STR(sta_addr),
		 al_net_if->name);
         access_point_release_sta_ex2(AL_CONTEXT sta, 0, 0, 0);
         goto _lb_ret;
      }

      if (!memcmp(net_if_essid, ie_essid->data, ie_essid->length) &&
          (net_if_essid_length == ie_essid->length))
      {
//VLAN	      
/* if ssid of net_iface and ssid in assoc req packet matches then check vlan_info exist for this ssid */
	      if(vlan_info = access_point_vlan_get(AL_CONTEXT ie_essid->data)) {
		      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MESH_AP:INFO: %s<%d> found Vlan information for:%s ssid\n"
			  ,__func__,__LINE__,net_if_essid);
	      }
	      else {
		      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MESH_AP:INFO: %s<%d> not found Vlan information for:%s ssid\n"
			  ,__func__,__LINE__,net_if_essid);
		      vlan_info = &default_vlan_info;
	      }
//VLAN_END
      }
      else
      {
         if (al_netif_config_info == NULL)
         {
            al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP:ERROR: %s<%d> invalid essid %s for associating sta over "
			"%s before init\n", __func__, __LINE__, ie_essid->data, al_net_if->name);
            access_point_release_sta_ex2(AL_CONTEXT sta, 0, 0, 0);
            goto _lb_ret;
         }

         net_if_config_essid_length = strlen(al_netif_config_info->essid);

         if (!memcmp(al_netif_config_info->essid, ie_essid->data, ie_essid->length) &&
             (net_if_config_essid_length == ie_essid->length))
         {
            /* we assume default vlan */
            vlan_info = &default_vlan_info;
         }
         else
         {
            /**
             *	Check sta essid with our vlan configuration essid return if vlan not found
             */
            vlan_info = access_point_vlan_get(AL_CONTEXT ie_essid->data);

            if (vlan_info == NULL)
            {
               al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP:ERROR: %s<%d> invalid essid %s for associating sta over"
			   " %s\n", __func__, __LINE__, ie_essid->data, al_net_if->name);
               access_point_release_sta_ex2(AL_CONTEXT sta, 0, 0, 0);
               goto _lb_ret;
            }
            else
            {
               al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MESH_AP:INFO: %s<%d> Setting vlan tag %d for associating sta"
			   " over %s\n", __func__, __LINE__, vlan_info->vlan_tag, ie_essid->data, al_net_if->name);
            }
         }
      }

      sta->sta.vlan_info = vlan_info;
   }
   else
   {
      /** No ESSID element was found in association request. We reject the client **/
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,
                   "MESH_AP:ERROR: %s<%d> No ESSID element found for associating sta "AL_NET_ADDR_STR " over %s\n",
                   __func__, __LINE__, AL_NET_ADDR_TO_STR(sta_addr), al_net_if->name);
      access_point_release_sta_ex2(AL_CONTEXT sta, 0, 0, 0);
      goto _lb_ret;
   }

_after_essid_check:

   sta->sta.capability = capability;

   /**
    * The following case should never happen in theory, as al_process_auth,
    * checks this condition and resets the state to AUTHENTICATED.
    * Hence we disable the block
    */


   switch (al_netif_config_info->service)
   {
   case AL_CONF_IF_SERVICE_BACKHAUL_ONLY:
      if (!sta->sta.is_imcp)
      {
	 access_point_release_sta_ex(AL_CONTEXT sta, 0, 0);
         access_point_release_sta_ex2(AL_CONTEXT sta, 1, 1, 0);
         goto _lb_ret;
      }
      break;

   case AL_CONF_IF_SERVICE_CLIENT_ONLY:
      if (sta->sta.is_imcp)
      {
         access_point_release_sta_ex2(AL_CONTEXT sta, 0, 0, 0);
         goto _lb_ret;
      }
      break;
   }

   if ((globals.ap_state == ACCESS_POINT_STATE_STARTING) && sta->sta.is_imcp)
   {
      access_point_release_sta_ex2(AL_CONTEXT sta, 0, 0, 0);
      goto _lb_ret;
   }

   if (sta->sta.state == ACCESS_POINT_STA_STATE_AUTHENTICATED)
   {
      if (on_assoc_request_handler != NULL)
      {
         on_assoc_request_handler(AL_CONTEXT al_net_if,
                                  sta_addr,
                                  supported_rates,
                                  4,
                                  &status_code_out,
                                  supported_rates_out,
                                  &supported_rates_length_out);
      }
      else
      {
         status_code_out = AL_802_11_STATUS_SUCCESS;
      }

      if (status_code_out == AL_802_11_STATUS_SUCCESS)
      {
         phy_mode                      = extended_operations->get_phy_mode(al_net_if);
         sta->sta.state                = ACCESS_POINT_STA_STATE_ASSOCIATED;
         sta->sta.association_time     = al_get_tick_count(AL_CONTEXT_SINGLE);
         sta->sta.last_packet_received = al_get_tick_count(AL_CONTEXT_SINGLE);

         if (sta->sta.aid == 0)
         {
            sta->sta.aid = access_point_sta_get_aid(AL_CONTEXT al_net_if);
         }

         ret = sta->sta.aid;

         if (!(capability & AL_802_11_CAPABILITY_SHORT_SLOT_TIME) &&
             ((phy_mode == AL_802_11_PHY_MODE_G) || (phy_mode == AL_802_11_PHY_MODE_BG)))
         {
            /* found b client */

            sta->sta.b_client = 1;

            if (++al_netif_config_info->long_slot_time_count == 1)
            {
               extended_operations->set_slot_time_type(al_net_if, AL_802_11_SLOT_TIME_TYPE_LONG);
               if (!(capability & AL_802_11_CAPABILITY_SHORT_PREAMBLE))
               {
                  if (++al_netif_config_info->long_preamble_count == 1)
                  {
                     extended_operations->set_preamble_type(al_net_if, AL_802_11_PREAMBLE_TYPE_LONG);
                     extended_operations->set_erp_info(al_net_if,
                                                       AL_802_11_ERP_INFO_NON_ERP_PRESENT
                                                       | AL_802_11_ERP_INFO_USE_PROTECTION
                                                       | AL_802_11_ERP_INFO_BARKER_PREAMBLE_MODE);
                  }
               }
               else
               {
                  extended_operations->set_erp_info(al_net_if,
                                                    AL_802_11_ERP_INFO_NON_ERP_PRESENT
                                                    | AL_802_11_ERP_INFO_USE_PROTECTION);
               }
            }
            al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,
                         "MESH_AP:INFO: %s<%d> 802.11b %s STA "AL_NET_ADDR_STR " associated over %s with essid : %s\n",
                         rsn_parsed ? "RSN" : md_ie_found ? "IMCP" : "", __func__, __LINE__, AL_NET_ADDR_TO_STR(sta_addr),
                         al_net_if->name, ie_essid->data);
         }
         else
         {
            al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,
                         "MESH_AP:INFO: %s<%d> %s STA "AL_NET_ADDR_STR " associated over %s with essid : %s\n",
                         rsn_parsed ? "RSN" : md_ie_found ? "IMCP" : "", __func__, __LINE__, AL_NET_ADDR_TO_STR(sta_addr),
                         al_net_if->name, ie_essid->data);
         }

         if (!sta->sta.is_imcp)
         {
            if ((ap_enable_disable_processing != 0) && (globals.ap_state == ACCESS_POINT_STATE_STARTED))
            {
		/*Commented this for add a station to dot11i Queue After association*/
        	// access_point_dot11i_add_sta(AL_CONTEXT sta_addr);
               if (sta->sta.vlan_info == &default_vlan_info)
               {
                  AL_ATOMIC_INC(al_netif_config_info->sta_count);
               }
               else if (sta->sta.vlan_info != NULL)
               {
                  access_point_incr_net_if_vlan_count(AL_CONTEXT sta->sta.vlan_info, al_net_if);
               }
            }
         }
         else
         {
            unsigned char compr_enable;
            unsigned char qos_enable;

            compr_enable = 0;
            qos_enable   = 0;

            if (md_ie->version >= AL_802_11_MD_IE_VERSION_COMPRESSION)
            {
               compr_enable = (md_ie->data.assoc_request.md_ie_capability & AL_802_11_MD_IE_COMPRESSION_SUPPORT);
            }

            md_ie_out->data.assoc_response.md_ie_capability |= compr_enable;

            if (md_ie->version >= AL_802_11_MD_IE_VERSION_EFFISTREAM)
            {
               qos_enable = (md_ie->data.assoc_request.md_ie_capability & AL_802_11_MD_IE_EFFISTREAM_SUPPORT);
            }

            md_ie_out->data.assoc_response.md_ie_capability |= qos_enable;

            _imcp_sta_security_helper(AL_CONTEXT md_ie,
                                      md_ie_out,
                                      al_netif_config_info,
                                      sta,
                                      al_net_if,
                                      extended_operations,
                                      imcp_key_ie_valid_out,
                                      imcp_key_ie_out,
                                      compr_enable);

            extended_operations->add_downlink_round_robin_child(al_net_if, &sta->sta.address);
            AL_ATOMIC_INC(al_netif_config_info->imcp_sta_count);

            if (compr_enable)
            {
               sta->sta.auth_state |= ACCESS_POINT_STA_AUTH_STATE_COMPRESSION;
            }
            if (qos_enable)
            {
               sta->sta.auth_state |= ACCESS_POINT_STA_AUTH_STATE_EFFISTREAM;
            }
         }


         access_point_release_sta_ex2(AL_CONTEXT sta, 0, 0, 0);

         ret_val = ret;
         goto _lb_ret;
      }
      else
      {
         al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESH_AP:ERROR: %s<%d> association was denied by mesh for STA"
		 ""AL_NET_ADDR_STR " STATE = %d\n", __func__, __LINE__, AL_NET_ADDR_TO_STR(sta_addr), sta->sta.state);
         access_point_release_sta_ex2(AL_CONTEXT sta, 0, 0, 0);
         goto _lb_ret;
      }
   }

   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MESH_AP:INFO: %s<%d> %s STA "AL_NET_ADDR_STR " was not "
   "authenticated STATE = %d\n", __func__, __LINE__, al_net_if->name, AL_NET_ADDR_TO_STR(sta_addr), sta->sta.state);
   access_point_release_sta_ex2(AL_CONTEXT sta, 0, 0, 0);

_lb_ret:

   al_heap_free(AL_CONTEXT rsn_ie);
   al_heap_free(AL_CONTEXT md_ie);
   al_heap_free(AL_CONTEXT md_ie_out);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Exit %s<%d>\n",__func__,__LINE__);
   return ret_val;
}


static int al_process_management_frame(AL_CONTEXT_PARAM_DECL al_net_if_t *al_net_if, int sub_type, al_packet_t *packet)
{
   AL_PRINT_LOG_FLOW_0("AP		: al_process_management_frame()");
   AL_PRINT_LOG_MGM_RX_0("AP		:  Mgm Packet Received. Adding to AP packet queue.");

   AL_ASSERT("al_process_management_frame", al_net_if != NULL);
   AL_ASSERT("al_process_management_frame", packet != NULL);

   if (globals.ap_queue != NULL)
   {
      if (globals.ap_queue->queue_add_packet(AL_CONTEXT globals.ap_queue, al_net_if, packet) == -1)
      {
		 (tx_rx_pkt_stats.packet_drop[79])++;
         al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP:ERROR: %s<%d> Faild to add packet into queue\n",__func__,__LINE__);
         return -1;
      }
      al_set_event(AL_CONTEXT globals.queue_event_handle);
   }

   return 0;
}


static int al_on_receive(AL_CONTEXT_PARAM_DECL al_net_if_t *al_net_if, al_packet_t *packet)
{

   AL_ASSERT("al_on_receive", al_net_if != NULL);
   AL_ASSERT("al_on_receive", packet != NULL);

   if (globals.ap_queue != NULL)
   {
      if (globals.ap_queue->queue_add_packet(AL_CONTEXT globals.ap_queue, al_net_if, packet) == -1)
      {
		 (tx_rx_pkt_stats.packet_drop[0])++;
         return -1;
      }

      al_set_event(AL_CONTEXT globals.queue_event_handle);
   }

   return 0;
}


static int al_on_before_transmit(AL_CONTEXT_PARAM_DECL al_net_if_t *al_net_if, al_packet_t *packet)
{
   if (globals.ap_queue != NULL)
   {
      if (globals.ap_queue->queue_add_packet(AL_CONTEXT globals.ap_queue, al_net_if, packet) == -1)
      {
		 (tx_rx_pkt_stats.packet_drop[1])++;
         return -1;
      }

      al_set_event(AL_CONTEXT globals.queue_event_handle);
   }

   return 0;
}


int access_point_initialize(AL_CONTEXT_PARAM_DECL_SINGLE)
{
   int           net_if_count;
   int           i;
   meshap_core_net_if_t *core_net_if;
   al_net_if_t   *net_if;
   unsigned char mac_addr[6];
   unsigned char mac_addr2[6];
   int           j;
   int           mac_found;
   int phyRadcount, vlanCount,virtualCount, ethcount;
   int           thread_id;
   unsigned int  ncpu  = num_online_cpus();

   AL_PRINT_LOG_FLOW_0("AP		: access_point_initialize()");

   mac_found = 0;

   /**
    * Check for a valid License
    */
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Enter %s<%d>\n",__func__,__LINE__);
#ifdef x86_ONLY
   for (i = 0; i < sizeof(mac_addr); i++)
   {
   	mac_addr2[i] = _MAC_ADDRESS[i] - _BEGIN_SIGNATURE[i];
   }

   mac_found = 1;
   memcpy(use_mac_address, mac_addr2,sizeof(mac_addr2));
#endif

   net_if_count = al_get_net_if_count(AL_CONTEXT_SINGLE);

#ifndef x86_ONLY
   for (i = 0; i < net_if_count; i++)
   {
      net_if = al_get_net_if(AL_CONTEXT i);
      AL_ASSERT("access_point_initialize", net_if != NULL);
      /*if(AL_NET_IF_IS_WIRELESS(net_if))*/ {
        memcpy(mac_addr, net_if->config.hw_addr.bytes, net_if->config.hw_addr.length);
        for (j = 0; j < sizeof(mac_addr); j++)
        {
            mac_addr[j] = mac_addr[j] + _BEGIN_SIGNATURE[j];
        }

         if (!memcmp(mac_addr, _MAC_ADDRESS, sizeof(mac_addr)))
         {
#ifdef ACCESS_POINT_LICENSE_CHECK_DEBUG
            al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MESH_AP:INFO: %s<%d> START "
			"SIGNATURE = %2x,%2x,%2x,%2x,%2x,%2x,%2x,%2x,%2x,%2x\n", __func__, __LINE__,_BEGIN_SIGNATURE[0], _BEGIN_SIGNATURE[1],
                         _BEGIN_SIGNATURE[2], _BEGIN_SIGNATURE[3], _BEGIN_SIGNATURE[4], _BEGIN_SIGNATURE[5],
                         _BEGIN_SIGNATURE[6], _BEGIN_SIGNATURE[7], _BEGIN_SIGNATURE[8], _BEGIN_SIGNATURE[9]);


            al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MESH_AP:INFO: %s<%d> MAC ADDR = %2x,%2x,%2x,%2x,%2x,%2x",
			__func__, __LINE__,  _MAC_ADDRESS[0], _MAC_ADDRESS[1],_MAC_ADDRESS[2], _MAC_ADDRESS[3],
			 _MAC_ADDRESS[4], _MAC_ADDRESS[5]);


            al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MESH_AP:INFO: %s<%d> END "
			"SIGNATURE = %2x,%2x,%2x,%2x,%2x,%2x,%2x,%2x\n", __func__, __LINE__, _END_SIGNATURE[0], _END_SIGNATURE[1],
                         _END_SIGNATURE[2], _END_SIGNATURE[3], _END_SIGNATURE[4],
                         _END_SIGNATURE[5], _END_SIGNATURE[6], _END_SIGNATURE[7]);
#endif      /* ACCESS_POINT_LICENSE_CHECK_DEBUG */
            mac_found = 1;
            memcpy(use_mac_address, net_if->config.hw_addr.bytes, net_if->config.hw_addr.length);
				al_print_log(AL_CONTEXT AL_LOG_TYPE_DEBUG, "MESH_AP:DEBUG: %s<%d> use_mac_address:\t%x:%x:%x:%x:%x:%x\n"
			,__func__, __LINE__, use_mac_address[0], use_mac_address[1], use_mac_address[2],
			use_mac_address[3], use_mac_address[4], use_mac_address[5]);
				break;
         }
      }
   }
#endif
   /* PATCH_BEGIN */
   if (mac_found)
   {
      unsigned char tmp_mac[MAC_ADDR_SIZE];
      struct ifreq  ifr;
      int           ret;
      int           i;

      al_net_addr_t use_mac_addr;
      memset(&use_mac_addr, 0, sizeof(al_net_addr_t));
      use_mac_addr.length = 6;

      memcpy(tmp_mac, use_mac_address, sizeof(use_mac_address));

	  /**************** RETRIEVE RADIO AND VLAN MAC ******************/
	  phyRadcount = _SIGNATURE_[16];
	  virtualCount = _SIGNATURE_[17];
	  vlanCount = _SIGNATURE_[18];
      ethcount = _SIGNATURE_[20];

	  int tmpPhyRadioCount = 0;
	  int tmpVlanCount = 0;
	  int tmpVirtualCount = 0;
      int tmpethcount = 0;

	  /*Retrieve Physical Interface MAC's*/
	  al_print_log(AL_CONTEXT AL_LOG_TYPE_DEBUG, "MESH_AP:DEBUG: %s<%d> Radio Inteface Count:%d\t Vlan Interface Count:%d "
	  "net_if_count:%d, ethcount = %d\n", __func__, __LINE__, phyRadcount, vlanCount, net_if_count, ethcount);
     for (i = net_if_count-1; i >=0; i--)
	  {
			core_net_if = meshap_core_get_created_net_if(i);
			if (!core_net_if)
				continue;

			net_if = &core_net_if->al_net_if;
         AL_ASSERT("access_point_initialize", net_if != NULL);
            if (net_if->encapsulation == AL_NET_IF_ENCAPSULATION_802_3 && !strcmp(net_if->name, "eth1") && ethcount)
            {
                memcpy(tmp_mac, &_ETHMAC_ADDRESS[(tmpethcount * MAC_ADDR_SIZE)], MAC_ADDR_SIZE);		// Start of ETH1 MAC
                memcpy(use_mac_addr.bytes, tmp_mac, MAC_ADDR_SIZE);
                net_if->set_hw_addr(net_if, &use_mac_addr);
            }
			if (net_if->encapsulation == AL_NET_IF_ENCAPSULATION_802_11)
			{
				if (core_net_if->vlan_if == 0) //vlan_if will be set for VLAN interfaces
				{
					if(core_net_if->phy_type != AL_CONF_IF_PHY_TYPE_802_11_VIRTUAL)
					{
					memcpy(tmp_mac, &_RMAC_ADDRESS[(tmpPhyRadioCount * MAC_ADDR_SIZE)], MAC_ADDR_SIZE);		// Start of Virtual MAC
					memcpy(use_mac_addr.bytes, tmp_mac, MAC_ADDR_SIZE);
					net_if->set_hw_addr(net_if, &use_mac_addr);
					al_print_log(AL_CONTEXT AL_LOG_TYPE_DEBUG, "MESH_AP:DEBUG: %s<%d> Interface %s: RADIO MAC %d\t"MACSTR"\n",
					__func__, __LINE__, net_if->name, i, MAC2STR(tmp_mac));
					tmpPhyRadioCount++;
					}
					else
					{
					memcpy(tmp_mac, &_VIRTUALMAC_ADDRESS[(tmpVirtualCount * MAC_ADDR_SIZE)], MAC_ADDR_SIZE);		// Start of Virtual MAC
					memcpy(use_mac_addr.bytes, tmp_mac, MAC_ADDR_SIZE);
					net_if->set_hw_addr(net_if, &use_mac_addr);
					al_print_log(AL_CONTEXT AL_LOG_TYPE_DEBUG, "MESH_AP:DEBUG: %s<%d> Interface %s: RADIO MAC %d\t"MACSTR"\n",
					__func__, __LINE__, net_if->name, i, MAC2STR(tmp_mac));
					tmpVirtualCount++;
						
					}
				}
				else
				{
					memcpy(tmp_mac, &_VLANMAC_ADDRESS[(tmpVlanCount * MAC_ADDR_SIZE)], MAC_ADDR_SIZE);		// Start of VLAN MAC
					memcpy(use_mac_addr.bytes, tmp_mac, MAC_ADDR_SIZE);
					net_if->set_hw_addr(net_if, &use_mac_addr);
					al_print_log(AL_CONTEXT AL_LOG_TYPE_DEBUG, "MESH_AP:DEBUG: %s<%d> Interface %s: VLAN MAC %d\t"MACSTR"\n",
					__func__, __LINE__, net_if->name, i, MAC2STR(tmp_mac));
					tmpVlanCount++;
				}
				
			}
	  }
   }
#ifndef ACCESS_POINT_NO_LICENSE_CHECK
   if (mac_found)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "AP	:   Valid License Found !!!");
   }
   else
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "AP	:   This is not a Valid License  !!!");
      return -1;
   }
#endif /* ACCESS_POINT_NO_LICENSE_CHECK */

   memset(&globals, 0, sizeof(access_point_globals_t));

   al_set_on_receive_hook(AL_CONTEXT al_on_receive);
   al_set_on_before_transmit_hook(AL_CONTEXT al_on_before_transmit);

   globals.queue_event_handle     = al_create_event(AL_CONTEXT 1);
   globals.stop_event_handle      = al_create_event(AL_CONTEXT 1);
   globals.monitor_exit_handle    = al_create_event(AL_CONTEXT 1);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MESH_AP:INFO: %s<%d> ncpu:%d\n", __func__, __LINE__, ncpu);
   globals.tx_queue_event_handle     = al_create_event(AL_CONTEXT 1);
   globals.imcp_queue_event_handle   = al_create_event(AL_CONTEXT 1);
   if(ncpu > 1) {
	   /*For IMX(quad-core) processor assign i = 1 to avoid binding access_point_thread
	    *to CPU0 compare i with ncpu-2 to reserv second last CPU for tx_tread and last
		*CPU for interrupts routing.
		*/
	   for(i = 1; i < (ncpu -2) ; i++){
		   thread_id = al_create_thread_on_cpu(AL_CONTEXT access_point_thread,NULL,i, "access_point_thread");
	   }
	   if (ncpu == 2) {
		   thread_id = al_create_thread_on_cpu(AL_CONTEXT access_point_thread,NULL,i, "access_point_thread");
		   thread_id = al_create_thread_on_cpu(AL_CONTEXT tx_thread,NULL,i, "tx_thread");
	   }else {
		   thread_id = al_create_thread_on_cpu(AL_CONTEXT tx_thread,NULL,i, "tx_thread");
	   }
   }else{
	   /*create one tx and one ap thread and bind to CPU0*/
	   thread_id = al_create_thread_on_cpu(AL_CONTEXT access_point_thread,NULL,0, "access_point_thread");
	   thread_id = al_create_thread_on_cpu(AL_CONTEXT tx_thread,NULL,0, "tx_thread");
   }
   thread_id = al_create_thread_on_cpu(AL_CONTEXT imcp_thread,NULL,0, "imcp_thread");
   threads_status |= THREAD_STATUS_BIT0_MASK;
   threads_status |= THREAD_STATUS_BIT2_MASK;
   threads_status |= THREAD_STATUS_BIT16_MASK;
   globals.ap_state             = ACCESS_POINT_STATE_INITIALIZED;
   globals.ap_queue             = NULL;
   ap_enable_disable_processing = 0;
   globals.ap_mode = ACCESS_POINT_MODE_AP;


   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Exit %s<%d>\n",__func__,__LINE__);
   return 0;
}


static int access_point_sta_monitor_thread(AL_CONTEXT_PARAM_DECL al_thread_param_t *param)
{
   int reason;
   access_point_sta_entry_t *next;
   access_point_sta_entry_t *node;
   al_u64_t                 last_packet_time;
   al_u64_t                 tickcount;
   al_u64_t                 timeout;
   unsigned long            flags;
   int sleep_time;
   int i;
   access_point_netif_config_info_t *net_if_config_info;

   AL_PRINT_LOG_FLOW_0("AP		: access_point_sta_monitor_thread()");

   al_set_thread_name(AL_CONTEXT "ap_sta_monitor");

   while (1)
   {
	  thread_stat.ap_sta_monitor_thread_count++;
      sleep_time = globals.config.bridge_ageing_time < ACCESS_POINT_MIN_BRIDGE_AGEING_TIME ? ACCESS_POINT_MIN_BRIDGE_AGEING_TIME : globals.config.bridge_ageing_time;
      reason     = al_wait_on_event(AL_CONTEXT globals.monitor_exit_handle, ((sleep_time * 1000) / 2));

	  if(reboot_in_progress) {
		  printk(KERN_EMERG"-----Terminating 'ap_sta_monitor' thread-----\n");
		  threads_status &= ~THREAD_STATUS_BIT1_MASK;
		  break;
	  }
      if ((ap_enable_disable_processing == 0) && (globals.ap_state == ACCESS_POINT_STATE_STARTING))
      {
         continue;
      }

      if ((reason == AL_WAIT_STATE_SIGNAL) || (globals.ap_state != ACCESS_POINT_STATE_STARTED))
      {
         break;
      }

      node = access_point_get_first_sta(AL_CONTEXT_SINGLE);
      al_spin_lock(sta_splock, sta_spvar);
      for ( ; node != NULL; )
      {
          next = node->next_list;
          if (next != NULL)
          {
              AL_ATOMIC_INC(next->sta.ref_count);
          }
          node = next;
      }
      al_spin_unlock(sta_splock, sta_spvar);

      node = access_point_get_first_sta(AL_CONTEXT_SINGLE);
      if (node)
          access_point_release_sta(AL_CONTEXT node);
      for ( ; node != NULL; )
      {
         next = node->next_list;
         if (al_timer_after(node->sta.last_packet_received,
                            node->sta.last_packet_transmitted))
         {
            last_packet_time = node->sta.last_packet_received;
         }
         else
         {
            last_packet_time =
               node->sta.last_packet_transmitted;
         }

         tickcount = al_get_tick_count(AL_CONTEXT_SINGLE);
         timeout   = last_packet_time + sleep_time * HZ;
         al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"MESH_AP:INFO: tickcount %llu timeout %llu last_packet_time %llu  %s<%d>\n", tickcount, timeout, last_packet_time, __func__, __LINE__); 
         if (al_timer_after(tickcount, timeout))
         {
            al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MESH_AP:INFO: %s<%d> STA PURGED "AL_NET_ADDR_STR, __func__,
			__LINE__, AL_NET_ADDR_TO_STR(&node->sta.address));
            access_point_release_sta(AL_CONTEXT node);
            access_point_release_sta_ex(AL_CONTEXT node, 1, 1);
         }
         else
         {
            access_point_release_sta(AL_CONTEXT node);
         }
         node = next;
      }
      if (globals.config.igmp_support)
      {
         for (i = 0; i < globals.config.wm_net_if_count; i++)
         {
            net_if_config_info = (access_point_netif_config_info_t *)globals.config.wm_net_if_info[i].net_if->config.ext_config_info;
            access_point_mcast_purge_indirect_entries(AL_CONTEXT net_if_config_info);
         }
      }
   }

   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MESH_AP:INFO: %s<%d> Monitor Thread Exited -----\n", __func__, __LINE__);
   al_set_event(AL_CONTEXT globals.stop_event_handle);

   return 0;
}


static void _set_ethernet_allowed_vlan_info(AL_CONTEXT_PARAM_DECL access_point_netif_config_info_t *net_if_config_info)
{
   access_point_vlan_info_t *vlan_info;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Enter %s<%d>\n",__func__,__LINE__);
   if (!strcmp(net_if_config_info->essid, ACCESS_POINT_ALLOW_NO_VLAN_ESSID))
   {
      net_if_config_info->net_if->config.allowed_vlan_tag = AL_NET_IF_CONFIG_ALLOWED_VLAN_NONE;
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MESH_AP:INFO: %s<%d> Allowing no VLANs over %s\n", __func__, __LINE__,
	  net_if_config_info->net_if->name);
   }
   else
   {
      vlan_info = access_point_vlan_get(AL_CONTEXT net_if_config_info->essid);
      if (vlan_info != NULL)
      {
         net_if_config_info->net_if->config.allowed_vlan_tag = vlan_info->vlan_tag;
         al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MESH_AP:INFO: %s<%d> Allowing only VLAN TAG %d over %s\n",
		 __func__, __LINE__, vlan_info->vlan_tag, net_if_config_info->net_if->name);
      }
      else
      {
         net_if_config_info->net_if->config.allowed_vlan_tag = AL_NET_IF_CONFIG_ALLOWED_VLAN_ALL;
         al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MESH_AP:INFO: %s<%d> Allowing ALL VLANS over %s\n", __func__,
		 __LINE__, net_if_config_info->net_if->name);
      }
   }
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Exit %s<%d>\n",__func__,__LINE__);
}


static void _initialise_mbps_to_rate_index_table(AL_CONTEXT_PARAM_DECL
                                                 access_point_netif_config_info_t *net_if_config_info,
                                                 al_rate_table_t                  *rate_table)
{
   int i;
   int j;
   int max_rate_index;

   max_rate_index = rate_table->rates[rate_table->count - 1].index;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Enter %s<%d>\n",__func__,__LINE__);
   memset(&net_if_config_info->mbps_to_rate_index, max_rate_index, ACCESS_POINT_MBPS_TO_RATE_INDEX_COUNT);

   for (i = 0, j = 0; i < rate_table->count; i++)
   {
      for ( ; j <= rate_table->rates[i].rate_in_mbps && j < ACCESS_POINT_MBPS_TO_RATE_INDEX_COUNT; j++)
      {
         net_if_config_info->mbps_to_rate_index[j] = rate_table->rates[i].index;
      }
   }
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Exit %s<%d>\n",__func__,__LINE__);
}


static int access_point_start_thread(AL_CONTEXT_PARAM_DECL al_thread_param_t *param)
{
   int i, j;
   int ret_flag;

#ifndef _AL_ROVER_
   int         net_if_count;
   al_net_if_t *net_if;
#endif

   al_802_11_operations_t    *operations;
   al_802_11_tx_power_info_t power_info;
   al_802_11_essid_info_t    *essid_info;
   int essid_info_count;
   access_point_vlan_entry_t *vlan_entry;
   al_rate_table_t           rate_table;
   al_net_if_ht_vht_capab_t ht_vht_capab ;
   al_802_11_vht_operation_t    vht_oper;
   mesh_conf_if_info_t *netif_conf;
   meshap_core_net_if_t *core_net_if;


   AL_PRINT_LOG_FLOW_0("AP		: access_point_start_thread()");
   al_set_thread_name(AL_CONTEXT "ap_start");
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Enter %s<%d>\n",__func__,__LINE__);

#ifndef _AL_ROVER_
   net_if_count = al_get_net_if_count(AL_CONTEXT_SINGLE);
   for (i = 0; i < net_if_count; i++)
   {
      net_if = al_get_net_if(AL_CONTEXT i);
      AL_ASSERT("access_point_initialize", net_if != NULL);
      if (AL_NET_IF_IS_WIRELESS(net_if))
      {
         operations = net_if->get_extended_operations(net_if);
         operations->set_management_frame_hook(net_if, al_process_management_frame);
         operations->set_auth_hook(net_if, al_process_auth);
         operations->set_assoc_hook(net_if, al_process_assoc);
         operations->init_channels(net_if);
      }
   }
#endif

   ret_flag = on_start_handler(AL_CONTEXT & globals.config, &globals.dot11e_config);
   if (ret_flag != 0)
   {
      return -1;
   }
   if (globals.config.ds_net_if_info.net_if->encapsulation == AL_NET_IF_ENCAPSULATION_802_11)      //|| (!strcmp(globals.config.ds_net_if_info.net_if->name,"eth1"))) {
   {
      globals.config.ds_net_if_info.net_if->config.allowed_vlan_tag = AL_NET_IF_CONFIG_ALLOWED_VLAN_ALL;
      operations = globals.config.ds_net_if_info.net_if->get_extended_operations(globals.config.ds_net_if_info.net_if);
      AL_ASSERT("access_point_start_thread", operations != NULL);
      operations->set_mode(globals.config.ds_net_if_info.net_if, AL_802_11_MODE_INFRA, 0);
      operations->set_dot11e_category_info(globals.config.ds_net_if_info.net_if, &globals.dot11e_config.config_info);
      operations->enable_ds_verification_opertions(globals.config.ds_net_if_info.net_if, 1);
      globals.config.ds_net_if_info.channel = operations->get_channel(globals.config.ds_net_if_info.net_if);
      

         memset(&ht_vht_capab, 0 , sizeof(al_net_if_ht_vht_capab_t));
         ht_vht_capab.ht_vht_capabilities = (void*)&(globals.config.ds_net_if_info.ht_capab);
         ht_vht_capab.sec_chan_offset = globals.config.ds_net_if_info.sec_chan_offset;
#if 1 // VIJAY_FRAME
      ht_vht_capab.a_mpdu_params = globals.config.ds_net_if_info.a_mpdu_params;
#endif
         operations->set_ht_vht_capab(globals.config.ds_net_if_info.net_if, &ht_vht_capab);
         memset(&ht_vht_capab, 0 , sizeof(al_net_if_ht_vht_capab_t));
         ht_vht_capab.ht_vht_capabilities = (void*)&(globals.config.ds_net_if_info.vht_capab);
         ht_vht_capab.sec_chan_offset = globals.config.ds_net_if_info.sec_chan_offset;
#if 1 // VIJAY_FRAME
      ht_vht_capab.a_mpdu_params = globals.config.ds_net_if_info.a_mpdu_params;
#endif
         operations->set_vht_capab(globals.config.ds_net_if_info.net_if, &ht_vht_capab);

      

      /**
       * Set the DS NET IF's ext_config_info
       */

      globals.config.ds_net_if_info.net_if->config.ext_config_info = (void *)&globals.config.ds_net_if_info;
      memset(&rate_table, 0, sizeof(al_rate_table_t));
      operations->get_rate_table(globals.config.ds_net_if_info.net_if, &rate_table);
      _initialise_mbps_to_rate_index_table(&globals.config.ds_net_if_info, &rate_table);
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MESH_AP:INFO: %s<%d> Access Point Relay Node DS=%s %d WM(s)\n",
	  __func__, __LINE__, globals.config.ds_net_if_info.net_if->name, globals.config.wm_net_if_count);
   }
   else
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MESH_AP:INFO: %s<%d> Access Point Root Node DS=%s %d WM(s)\n",
	   __func__, __LINE__, globals.config.ds_net_if_info.net_if->name, globals.config.wm_net_if_count);
      globals.config.ds_net_if_info.net_if->set_filter_mode(globals.config.ds_net_if_info.net_if,
                                                            AL_NET_IF_FILTER_MODE_UNICAST
                                                            | AL_NET_IF_FILTER_MODE_MULTICAST
                                                            | AL_NET_IF_FILTER_MODE_BROADCAST
                                                            | AL_NET_IF_FILTER_MODE_PROMISCOUS);

      _set_ethernet_allowed_vlan_info(AL_CONTEXT & globals.config.ds_net_if_info);
   }

   power_info.flags = AL_802_11_TXPOW_PERC;

   access_point_vlan_create_essid_info(AL_CONTEXT & essid_info_count, &essid_info);

   for (i = 0; i < globals.config.wm_net_if_count; i++)
   {
      globals.config.wm_net_if_info[i].net_if->config.ext_config_info = (void *)&globals.config.wm_net_if_info[i];

      memset(globals.config.wm_net_if_info[i].aid_bitmap, 0, sizeof(globals.config.wm_net_if_info[i].aid_bitmap));

      AL_ATOMIC_SET(globals.config.wm_net_if_info[i].sta_count, 0);
      AL_ATOMIC_SET(globals.config.wm_net_if_info[i].imcp_sta_count, 0);
      memset(globals.config.wm_net_if_info[i].vlan_child_count, 0, sizeof(globals.config.wm_net_if_info[i].vlan_child_count));

      globals.config.wm_net_if_info[i].tagged_bcast_count             = 0;
      globals.config.wm_net_if_info[i].untagged_bcast_count           = 0;
      globals.config.wm_net_if_info[i].untagged_bcast_reordered_count = 0;

      access_point_multicast_initialize(&globals.config.wm_net_if_info[i]);

      if (AL_NET_IF_IS_ETHERNET(globals.config.wm_net_if_info[i].net_if))
      {
         globals.config.wm_net_if_info[i].net_if->set_filter_mode(globals.config.wm_net_if_info[i].net_if, AL_NET_IF_FILTER_MODE_PROMISCOUS);
         _set_ethernet_allowed_vlan_info(AL_CONTEXT & globals.config.wm_net_if_info[i]);

#ifdef FIPS_1402_COMPLIANT
         if (AP_IS_FIPS_ENABLED)
         {
            if (globals.config.wm_net_if_info[i].service == AL_CONF_IF_SERVICE_ALL)
            {
               al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MESH_AP:INFO: %s<%d> Disabling %s in FIPS140-2 mode.",
			   __func__, __LINE__, globals.config.wm_net_if_info[i].net_if->name);
               AL_ATOMIC_SET(globals.config.wm_net_if_info[i].net_if->buffering_state, AL_NET_IF_BUFFERING_STATE_DROP);
            }
         }
#endif
      }
      else if (AL_NET_IF_IS_WIRELESS(globals.config.wm_net_if_info[i].net_if))
      {
          al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MESH_AP:INFO: %s<%d> Access Point Root Node WM(s)\n",
		 __func__, __LINE__);
       
         globals.config.wm_net_if_info[i].net_if->config.allowed_vlan_tag = AL_NET_IF_CONFIG_ALLOWED_VLAN_ALL;
         operations = globals.config.wm_net_if_info[i].net_if->get_extended_operations(globals.config.wm_net_if_info[i].net_if);
         AL_ASSERT("access_point_start_thread", operations != NULL);

         operations->set_management_frame_hook(globals.config.wm_net_if_info[i].net_if, al_process_management_frame);
         operations->set_auth_hook(globals.config.wm_net_if_info[i].net_if, al_process_auth);
         operations->set_assoc_hook(globals.config.wm_net_if_info[i].net_if, al_process_assoc);
         for (j = 0; j < mesh_config->if_count; j++)
         {
            netif_conf = &mesh_config->if_info[j];
            if ((netif_conf->use_type == AL_CONF_IF_USE_TYPE_AP) && !strcmp(globals.config.wm_net_if_info[i].net_if->name, netif_conf->name)) 
               continue;
         }  
         operations->set_mode(globals.config.wm_net_if_info[i].net_if, AL_802_11_MODE_MASTER, 0);


         memset(&ht_vht_capab, 0 , sizeof(al_net_if_ht_vht_capab_t));

         ht_vht_capab.ht_vht_capabilities = (void*)&(globals.config.wm_net_if_info[i].ht_capab);
         ht_vht_capab.sec_chan_offset = globals.config.wm_net_if_info[i].sec_chan_offset;
#if 1 // VIJAY_FRAME
         ht_vht_capab.a_mpdu_params = globals.config.wm_net_if_info[i].a_mpdu_params;
#endif
         operations->set_ht_vht_capab(globals.config.wm_net_if_info[i].net_if, &ht_vht_capab);
         operations->set_ht_operation(globals.config.wm_net_if_info[i].net_if, &(globals.config.wm_net_if_info[i].ht_oper_mode));

         memset(&ht_vht_capab, 0 , sizeof(al_net_if_ht_vht_capab_t));
         ht_vht_capab.ht_vht_capabilities = (void*)&(globals.config.wm_net_if_info[i].vht_capab);
         ht_vht_capab.sec_chan_offset = globals.config.wm_net_if_info[i].sec_chan_offset;
#if 1 // VIJAY_FRAME
         ht_vht_capab.a_mpdu_params = globals.config.wm_net_if_info[i].a_mpdu_params;
#endif
         operations->set_vht_capab(globals.config.wm_net_if_info[i].net_if, &ht_vht_capab);

         vht_oper.vht_op_info_chwidth = globals.config.wm_net_if_info[i].vht_oper_mode.vht_op_info_chwidth;
         vht_oper.vht_op_info_chan_center_freq_seg0_idx = globals.config.wm_net_if_info[i].vht_oper_mode.vht_op_info_chan_center_freq_seg0_idx;
         vht_oper.vht_op_info_chan_center_freq_seg1_idx = globals.config.wm_net_if_info[i].vht_oper_mode.vht_op_info_chan_center_freq_seg1_idx;
         vht_oper.vht_basic_mcs_set = globals.config.wm_net_if_info[i].vht_oper_mode.vht_basic_mcs_set;
         operations->set_vht_operation(globals.config.wm_net_if_info[i].net_if, vht_oper);

         operations->set_essid(globals.config.wm_net_if_info[i].net_if, globals.config.wm_net_if_info[i].essid);
         operations->set_rts_threshold(globals.config.wm_net_if_info[i].net_if, globals.config.wm_net_if_info[i].rts_threshold);
         operations->set_frag_threshold(globals.config.wm_net_if_info[i].net_if, globals.config.wm_net_if_info[i].frag_threshold);
         operations->set_beacon_interval(globals.config.wm_net_if_info[i].net_if, globals.config.wm_net_if_info[i].beacon_interval);
         core_net_if = (meshap_core_net_if_t *)globals.config.wm_net_if_info[i].net_if;
         if (core_net_if && (core_net_if->dev_type == MESHAP_CORE_DEV_TYPE_VIRTUAL) &&
             AL_NET_IF_IS_WIRELESS(globals.config.ds_net_if_info.net_if))
         {
             operations->set_channel(globals.config.wm_net_if_info[i].net_if, globals.config.ds_net_if_info.channel);
         }
         else
         {
             operations->set_channel(globals.config.wm_net_if_info[i].net_if, globals.config.wm_net_if_info[i].channel);
         }
         operations->set_ack_timeout(globals.config.wm_net_if_info[i].net_if, globals.config.wm_net_if_info[i].ack_timeout);
         operations->set_hide_ssid(globals.config.wm_net_if_info[i].net_if, globals.config.wm_net_if_info[i].hide_ssid);

         operations->set_preamble_type(globals.config.wm_net_if_info[i].net_if, globals.config.wm_net_if_info[i].preamble_type);
         operations->set_slot_time_type(globals.config.wm_net_if_info[i].net_if, globals.config.wm_net_if_info[i].slot_time_type);
#if 0
         if (globals.config.wm_net_if_info[i].group_key.enabled)
         {
            for (j = 0; j < mesh_config->if_count; j++)
            {
                netif_conf = &mesh_config->if_info[j];
                if ((netif_conf->use_type == AL_CONF_IF_USE_TYPE_WM) && !strcmp(globals.config.wm_net_if_info[i].net_if->name, netif_conf->name))
                    operations->set_security_key(globals.config.wm_net_if_info[i].net_if,
                                                 &globals.config.wm_net_if_info[i].group_key,
                                                 0);
            }
         }
#endif

         operations->set_security_info(globals.config.wm_net_if_info[i].net_if,
                                       &globals.config.wm_net_if_info[i].security_info.dot11);

         operations->set_essid_info(globals.config.wm_net_if_info[i].net_if,
                                    &globals.config.wm_net_if_info[i].security_info.dot11,
                                    essid_info_count,
                                    essid_info);

         operations->set_bit_rate(globals.config.wm_net_if_info[i].net_if, globals.config.wm_net_if_info[i].txrate);

         operations->set_dot11e_category_info(globals.config.wm_net_if_info[i].net_if, &globals.dot11e_config.config_info);

         operations->set_mesh_downlink_round_robin_time(globals.config.wm_net_if_info[i].net_if, globals.config.round_robin_beacon_count);

         memset(&rate_table, 0, sizeof(al_rate_table_t));
         operations->get_rate_table(globals.config.wm_net_if_info[i].net_if, &rate_table);
         _initialise_mbps_to_rate_index_table(&globals.config.wm_net_if_info[i], &rate_table);

         if ((globals.config.wm_net_if_info[i].channel == -1) ||
             (globals.config.wm_net_if_info[i].txpower == 0))
         {
            operations->set_mode(globals.config.wm_net_if_info[i].net_if, AL_802_11_MODE_INFRA, 0);
            AL_ATOMIC_SET(globals.config.wm_net_if_info[i].net_if->buffering_state, AL_NET_IF_BUFFERING_STATE_DROP);
         }
         else
         {
            power_info.power = globals.config.wm_net_if_info[i].txpower;
            operations->set_tx_power(globals.config.wm_net_if_info[i].net_if, &power_info);
         }
         //CREE_DEMO adding vendor_info
         mesh_ng_fsm_update_downlink_vendor_info();
         operations->set_all_conf_complete(globals.config.wm_net_if_info[i].net_if, 1);
         if (globals.config.wm_net_if_info[i].group_key.enabled)
         {
            for (j = 0; j < mesh_config->if_count; j++)
            {
               netif_conf = &mesh_config->if_info[j];
               if ((netif_conf->use_type == AL_CONF_IF_USE_TYPE_WM) && !strcmp(globals.config.wm_net_if_info[i].net_if->name, netif_conf->name))
                   operations->set_security_key(globals.config.wm_net_if_info[i].net_if,
                                                &globals.config.wm_net_if_info[i].group_key,
                                                0);
            }
         }
      }
   }

   access_point_vlan_destroy_essid_info(AL_CONTEXT essid_info);


   for (i = 0, vlan_entry = vlan_list_head; i < vlan_count && vlan_entry != NULL; i++, vlan_entry = vlan_entry->next_list)
   {
      on_dot1x_auth_init_handler(AL_CONTEXT & vlan_entry->vlan.config);
   }

   globals.arp_instance = meshap_arp_initialize(&globals.config.ds_net_if_info.net_if->config.hw_addr);

   if (globals.config.dhcp_support)
   {
      globals.dhcp_instance = meshap_dhcp_initialize(&globals.config.ds_net_if_info.net_if->config.hw_addr,
                                                     globals.config.dhcp_info.net_id,
                                                     globals.config.dhcp_info.host_id_1,
                                                     globals.config.dhcp_info.host_id_2,
                                                     globals.config.dhcp_info.subnet_mask,
                                                     globals.config.dhcp_info.gateway_ip,
                                                     globals.config.dhcp_info.dns_ip,
                                                     globals.config.dhcp_info.lease_timeout);
      meshap_arp_add_entry((meshap_arp_instance_t)globals.arp_instance, globals.config.dhcp_info.gateway_ip, MESHAP_ARP_USE_ADDR_DS_NET_IF);
   }

   if (globals.config.sip_info.sip_enabled)
   {
      if (access_point_verify_option(AL_CONTEXT AL_CONF_OPTION_CODE_SIP) == 0)
      {
         globals.sip_instance = meshap_sip_initialize(globals.config.ds_net_if_info.net_if,
                                                      &globals.config.sip_info);
      }
      else
      {
         globals.config.sip_info.sip_enabled = 0;
      }
   }
   access_point_dot11i_initialize(AL_CONTEXT_SINGLE);

   globals.ap_state             = ACCESS_POINT_STATE_STARTED;
   ap_enable_disable_processing = 1;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MESH_AP:INFO: %s<%d> -----------Access Point started-----------\n",
   __func__, __LINE__);
   al_create_thread_on_cpu(AL_CONTEXT access_point_sta_monitor_thread, NULL,0, "access_point_sta_monitor_thread");
   threads_status |= THREAD_STATUS_BIT1_MASK;
   al_create_thread_on_cpu(AL_CONTEXT switch_over_2other_ds, NULL,0, "switch_over_2other_ds");

#if 1 
   /* To start mesh after ap_thread has completed initialization of wlan interfaes! */
   ret_flag = on_start_handler2(AL_CONTEXT & globals.config, &globals.dot11e_config);
   if (ret_flag != 0)
   {
      return -1;
   }
#endif

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Exit %s<%d>\n",__func__,__LINE__);
   return 0;
}


int access_point_start(AL_CONTEXT_PARAM_DECL int sta_table_hash_length)
{
   unsigned long flags;

   AL_PRINT_LOG_FLOW_0("AP		: access_point_start()");


   al_spin_lock_init(&gbl_splock);
   al_mutex_init(&gbl_mutex);
   al_mutex_init(&mcast_mutex);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Enter %s<%d>\n",__func__,__LINE__);
   globals.ap_queue = access_point_queue_initialize(AL_CONTEXT QUEUE_DOT1P);

   access_point_sta_initialize(AL_CONTEXT sta_table_hash_length);

   access_point_vlan_initialize(AL_CONTEXT_SINGLE);

   access_point_acl_initialize(AL_CONTEXT_SINGLE);

   globals.ap_state = ACCESS_POINT_STATE_STARTING;

   if ((globals.ap_mode == ACCESS_POINT_MODE_TESTER) || (globals.ap_mode == ACCESS_POINT_MODE_TESTEE))
   {
      al_create_thread_on_cpu(AL_CONTEXT access_point_tester_testee_start_thread, NULL,0, "access_point_tester_testee_start_thread");
   }
   else
   {
      al_create_thread_on_cpu(AL_CONTEXT access_point_start_thread, NULL,0, "access_point_start_thread");
   }

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Exit %s<%d>\n",__func__,__LINE__);
   return 0;
}


int access_point_stop(AL_CONTEXT_PARAM_DECL_SINGLE)
{
   int i,j;
   al_802_11_operations_t *extended_operations;
   int ap_state = globals.ap_state;
   mesh_conf_if_info_t *netif_conf;
   
   AL_PRINT_LOG_FLOW_0("AP		: access_point_stop()");
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Enter %s<%d>\n",__func__,__LINE__);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "AP : Stopping mesh...");
   on_stop_handler(AL_CONTEXT & globals.config, &globals.dot11e_config);
   globals.ap_state = ACCESS_POINT_STATE_STOPPING;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "AP : Stopping ap_thread...");
   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "AP : STATE CHANGE ACCESS_POINT_STATE_STOPPED ");
   al_set_event(AL_CONTEXT globals.queue_event_handle);
   al_set_event(AL_CONTEXT globals.tx_queue_event_handle);
   al_set_event(AL_CONTEXT globals.imcp_queue_event_handle);
   //CREE_DEMO
   printk(KERN_EMERG "RESTART_MESHD::: Waiting on event %s-%d \n",__func__,__LINE__);
   al_wait_on_event(AL_CONTEXT globals.stop_event_handle, AL_WAIT_TIMEOUT_INFINITE);

   if (ap_state == ACCESS_POINT_STATE_STARTED)
   {
      /** Wait for monitor thread */
      al_reset_event(AL_CONTEXT globals.stop_event_handle);
      al_set_event(AL_CONTEXT globals.monitor_exit_handle);
      printk(KERN_EMERG "RESTART_MESHD::: Waiting on event %s-%d \n",__func__,__LINE__);
      al_wait_on_event(AL_CONTEXT globals.stop_event_handle, AL_WAIT_TIMEOUT_INFINITE);

      /** Wait for excerciser thread */
      if (AL_NET_IF_IS_WIRELESS(globals.config.ds_net_if_info.net_if))
      {
         al_reset_event(AL_CONTEXT globals.stop_event_handle);
         printk(KERN_EMERG "RESTART_MESHD::: Waiting on event %s-%d \n",__func__,__LINE__);
         al_wait_on_event(AL_CONTEXT globals.stop_event_handle, AL_WAIT_TIMEOUT_INFINITE);
      }
   }

   if (ap_state == ACCESS_POINT_STATE_STARTED)
   {
      access_point_dot11i_uninitialize(AL_CONTEXT_SINGLE);
   }

   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MESH_AP:INFO: %s<%d> Cleaning up\n", __func__, __LINE__);

   access_point_sta_uninitialize(AL_CONTEXT_SINGLE);
#if 0
   globals.ap_queue->queue_uninitialize(AL_CONTEXT globals.ap_queue);
#endif
   access_point_vlan_uninitialize(AL_CONTEXT_SINGLE);
   access_point_acl_uninitialize(AL_CONTEXT);

   for (i = 0; i < globals.config.wm_net_if_count; i++)
   {
      if (AL_NET_IF_IS_WIRELESS(globals.config.wm_net_if_info[i].net_if))
      {
         access_point_netif_config_info_t *net_if_config_info;
         net_if_config_info = (access_point_netif_config_info_t *)globals.config.wm_net_if_info[i].net_if->config.ext_config_info;
         access_point_multicast_uninitialize(AL_CONTEXT net_if_config_info);
         for (j = 0; j < mesh_config->if_count; j++)
         {
            netif_conf = &mesh_config->if_info[j];
            if ((netif_conf->use_type == AL_CONF_IF_USE_TYPE_AP) && !strcmp(globals.config.wm_net_if_info[i].net_if->name, netif_conf->name))       
               continue;
         }  
         extended_operations = globals.config.wm_net_if_info[i].net_if->get_extended_operations(globals.config.wm_net_if_info[i].net_if);
         extended_operations->set_mode(globals.config.wm_net_if_info[i].net_if, AL_802_11_MODE_RADIO_OFF, 0);
      }
   }

   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MESH_AP:INFO: %s<%d> Access Point stopped\n", __func__, __LINE__);
   globals.ap_state = ACCESS_POINT_STATE_STOPPED;

   al_heap_free(AL_CONTEXT globals.ap_queue);

   globals.ap_queue = NULL;

   al_mutex_destroy(&gbl_mutex);
   al_mutex_destroy(&mcast_mutex);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Exit %s<%d>\n",__func__,__LINE__);

   return 0;
}


int access_point_uninitialize(AL_CONTEXT_PARAM_DECL_SINGLE)
{
   AL_PRINT_LOG_FLOW_0("AP		: access_point_uninitialize()");

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Enter %s<%d>\n",__func__,__LINE__);
   al_set_on_before_transmit_hook(AL_CONTEXT NULL);
   al_set_on_receive_hook(AL_CONTEXT NULL);

   globals.ap_state = ACCESS_POINT_STATE_UNITIALIZED;
   al_set_event(AL_CONTEXT globals.queue_event_handle);
   al_set_event(AL_CONTEXT globals.tx_queue_event_handle);
   al_set_event(AL_CONTEXT globals.imcp_queue_event_handle);
   al_destroy_event(AL_CONTEXT globals.tx_queue_event_handle);
   al_destroy_event(AL_CONTEXT globals.imcp_queue_event_handle);

   al_destroy_event(AL_CONTEXT globals.queue_event_handle);
   al_destroy_event(AL_CONTEXT globals.stop_event_handle);
   al_destroy_event(AL_CONTEXT globals.monitor_exit_handle);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Exit %s<%d>\n",__func__,__LINE__);

   return 0;
}


void access_point_get_sta_info_ex(AL_CONTEXT_PARAM_DECL al_net_addr_t *address,
                                  access_point_sta_info_t *sta_info, int from_interrupt_context)
{
   access_point_sta_entry_t *node;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Enter %s<%d>\n",__func__,__LINE__);
   memset(sta_info, 0, sizeof(access_point_sta_info_t));

   node = access_point_get_sta_ex(AL_CONTEXT address,
                                  from_interrupt_context);

   if (node != NULL)
   {
      memcpy(sta_info, &node->sta, sizeof(access_point_sta_info_t));
      access_point_release_sta_ex2(AL_CONTEXT node, 0, 0, from_interrupt_context);
   }
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Exit %s<%d>\n",__func__,__LINE__);
}


void access_point_get_sta_info(AL_CONTEXT_PARAM_DECL al_net_addr_t *address,
                               access_point_sta_info_t             *sta_info)
{
   access_point_get_sta_info_ex(AL_CONTEXT address, sta_info, 0);
}


void access_point_get_sta_info_in_intr(AL_CONTEXT_PARAM_DECL al_net_addr_t *address,
                                       access_point_sta_info_t             *sta_info)
{
   access_point_get_sta_info_ex(AL_CONTEXT address, sta_info, 1);
}


int access_point_set_start_handler(AL_CONTEXT_PARAM_DECL access_point_on_start_t handler)
{
   on_start_handler = handler;
   return 0;
}

int access_point_set_start_handler2(AL_CONTEXT_PARAM_DECL access_point_on_start_t handler)
{
   on_start_handler2 = handler;
   return 0;
}

int access_point_set_stop_handler(AL_CONTEXT_PARAM_DECL access_point_on_stop_t handler)
{
   on_stop_handler = handler;
   return 0;
}


int access_point_set_auth_request_handler(AL_CONTEXT_PARAM_DECL access_point_on_auth_request_t handler)
{
   on_auth_request_handler = handler;
   return 0;
}


int access_point_set_assoc_request_handler(AL_CONTEXT_PARAM_DECL access_point_on_assoc_request_t handler)
{
   on_assoc_request_handler = handler;
   return 0;
}


int access_point_set_deauth_handler(AL_CONTEXT_PARAM_DECL access_point_on_deauth_t handler)
{
   on_deauth_handler = handler;
   return 0;
}


int access_point_set_disassoc_handler(AL_CONTEXT_PARAM_DECL access_point_on_disassoc_t handler)
{
   on_disassoc_handler = handler;
   return 0;
}


int access_point_set_probe_request_handler(AL_CONTEXT_PARAM_DECL access_point_on_probe_request_t handler)
{
   on_probe_request_handler = handler;
   return 0;
}


int access_point_set_data_reception_handler(AL_CONTEXT_PARAM_DECL access_point_on_data_reception_t handler)
{
   on_data_reception_handler = handler;
   return 0;
}


int access_point_set_before_transmit_handler(AL_CONTEXT_PARAM_DECL access_point_on_before_transmit_t handler)
{
   on_before_transmit_handler = handler;
   return 0;
}


int access_point_set_imcp_handler(AL_CONTEXT_PARAM_DECL access_point_on_imcp_packet_t handler)
{
   on_imcp_packet_handler = handler;
   return 0;
}


int access_point_initialize_helper(AL_CONTEXT_PARAM_DECL_SINGLE)
{
   return access_point_initialize(AL_CONTEXT_SINGLE);
}


int access_point_start_helper(AL_CONTEXT_PARAM_DECL int sta_table_hash_length)
{
   return access_point_start(AL_CONTEXT sta_table_hash_length);
}


int access_point_stop_helper(AL_CONTEXT_PARAM_DECL_SINGLE)
{
   return access_point_stop(AL_CONTEXT_SINGLE);
}


int access_point_uninitialize_helper(AL_CONTEXT_PARAM_DECL_SINGLE)
{
   return access_point_uninitialize(AL_CONTEXT_SINGLE);
}


int access_point_disassociate_sta_ex(AL_CONTEXT_PARAM_DECL al_net_addr_t *address, int from_mesh, al_net_if_t *net_if)
{
   al_802_11_operations_t *operations;
   access_point_sta_entry_t *entry;
   int ret;
   int i,j;
   mesh_conf_if_info_t *netif_conf;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Enter %s<%d>\n",__func__,__LINE__);
   AL_ASSERT("access_point_disassociate_sta_ex", address != NULL);

   entry = access_point_get_sta(AL_CONTEXT address);
   ret   = ((entry == NULL) ? -1 : 0);

   if (entry != NULL)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,
                   "MESH_AP:INFO: %s<%d> %s Forcing STA " AL_NET_ADDR_STR " from list\n",__func__, __LINE__,
                   from_mesh ? "(Mesh Sync)" : "",
                   AL_NET_ADDR_TO_STR(address));
      access_point_release_sta(AL_CONTEXT entry);
      access_point_release_sta_ex(AL_CONTEXT entry, !from_mesh, 1);
   }
   else
   {
      /**
       * if the entry is not found we force a disassociation on the provided net_if.
       * If no net_if is provided we force the disassociation on all WMs except AP
       */
       for (j = 0; j < mesh_config->if_count; j++)
       {
           netif_conf = &mesh_config->if_info[j];
           if ((netif_conf->use_type == AL_CONF_IF_USE_TYPE_AP) && !strcmp(globals.config.wm_net_if_info[i].net_if->name, netif_conf->name))       
               continue;
       }  
      if (net_if != NULL)
      {
         if (AL_ATOMIC_GET(net_if->buffering_state) == AL_NET_IF_BUFFERING_STATE_NONE)
         {
             operations = net_if->get_extended_operations(net_if);
             if (operations == NULL)
             {
                al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : Not able to get extended operations "
                      "%s : %d\n", __func__,__LINE__);
                return -1;
             }
             operations->send_deauth(net_if, net_if->config.hw_addr.bytes, address);
         }
      }
      else
      {
         for (i = 0; i < globals.config.wm_net_if_count; i++)
         {
            net_if = globals.config.wm_net_if_info[i].net_if;
            if (AL_ATOMIC_GET(net_if->buffering_state) == AL_NET_IF_BUFFERING_STATE_NONE)
            {
               operations = net_if->get_extended_operations(net_if);
               if (operations == NULL)
               {
                  al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : Not able to get extended operations "
                      "%s : %d\n", __func__,__LINE__);
                  return -1;
               }
               operations->send_deauth(net_if, net_if->config.hw_addr.bytes, address);
            }
         }
      }
   }
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Exit %s<%d>\n",__func__,__LINE__);

   return ret;
}


int access_point_disassociate_sta(AL_CONTEXT_PARAM_DECL al_net_addr_t *address)
{
   return access_point_disassociate_sta_ex(AL_CONTEXT address, 1, NULL);
}


int access_point_remove_all_sta(AL_CONTEXT_PARAM_DECL_SINGLE)
{
   unsigned long            flags;
   access_point_sta_entry_t *next;
   access_point_sta_entry_t *node;
   int j;
   mesh_conf_if_info_t *netif_conf;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Enter %s<%d>\n",__func__,__LINE__);
   node = access_point_get_first_sta(AL_CONTEXT_SINGLE);
   al_spin_lock(sta_splock, sta_spvar);
   for ( ; node != NULL; )
   {
      next = node->next_list;
      if (next != NULL)
      {
         AL_ATOMIC_INC(next->sta.ref_count);
      }
      node = next;
   }
   al_spin_unlock(sta_splock, sta_spvar);


   node = access_point_get_first_sta(AL_CONTEXT_SINGLE);
   if (node)
       access_point_release_sta(AL_CONTEXT node);
   for ( ; node != NULL; )
   {
      next = node->next_list;
      access_point_release_sta(AL_CONTEXT node);
      access_point_release_sta_ex(AL_CONTEXT node, 0, 1);
      node = next;
   }
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Exit %s<%d>\n",__func__,__LINE__);

   return 0;
}

int access_point_remove_all_sta_table_entry(char *name)
{
   access_point_sta_entry_t *next;
   access_point_sta_entry_t *node;
   access_point_sta_entry_t *first_node;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Enter %s<%d>\n",__func__,__LINE__);

   node = access_point_get_first_sta(AL_CONTEXT_SINGLE);
   first_node = node;
   al_spin_lock(sta_splock, sta_spvar);
   for ( ; node != NULL; )
   {
      next = node->next_list;
      if (next != NULL)
      {
         AL_ATOMIC_INC(next->sta.ref_count);
      }
      node = next;
   }
   al_spin_unlock(sta_splock, sta_spvar);
   node = first_node;
   for ( ; node != NULL; )
   {
      next = node->next_list;
      if (!strcmp(node->sta.net_if->name, name)) {
        mesh_table_entry_remove(AL_CONTEXT &node->sta.address, 8);
        access_point_release_sta(AL_CONTEXT node);
        access_point_release_sta_ex(AL_CONTEXT node, 0, 0);
      } else {
        access_point_release_sta(AL_CONTEXT node);
      }
     node = next;
   }

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Exit %s<%d>\n",__func__,__LINE__);
   return 0;
}


int access_point_enable_disable_processing(AL_CONTEXT_PARAM_DECL unsigned char enable)
{
   ap_enable_disable_processing = enable;
   if (enable)
   {
      globals.ap_state = ACCESS_POINT_STATE_STARTED;
   }
   else
   {
      globals.ap_state = ACCESS_POINT_STATE_STARTING;
   }
   return 0;
}


int access_point_set_values(AL_CONTEXT_PARAM_DECL char       *essid,
                            unsigned short                   beacon_interval,
                            unsigned short                   rts_threshold,
                            unsigned short                   frag_threshold,
                            unsigned char                    igmp_support,
                            int                              netif_count,
                            access_point_netif_config_info_t *netif_config_info)
{
   al_802_11_operations_t *operations;
   int i, j, ap_intf_found;
   al_802_11_tx_power_info_t        power_info;
   access_point_netif_config_info_t *al_netif_config_info;
   mesh_conf_if_info_t *netif_conf;


   strcpy(globals.config.essid, essid);                         /** Un-used */
   globals.config.beacon_interval = beacon_interval;            /** Un-used */
   globals.config.rts_threshold   = rts_threshold;              /** Un-used */
   globals.config.frag_threshold  = frag_threshold;             /** Un-used */


   globals.config.igmp_support = igmp_support;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Enter %s<%d>\n",__func__,__LINE__);


   for (i = 0; i < globals.config.wm_net_if_count; i++)
   {
      for (j = 0; j < netif_count; j++)
      {
         if (globals.config.wm_net_if_info[i].net_if == netif_config_info[j].net_if)
         {
            al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "interface %s %s<%d>\n",netif_config_info[j].net_if->name,__func__,__LINE__);
            globals.config.wm_net_if_info[i].service = netif_config_info[j].service;
            globals.config.wm_net_if_info[i].txpower = netif_config_info[j].txpower;
            al_netif_config_info = (access_point_netif_config_info_t *)globals.config.wm_net_if_info[i].net_if->config.ext_config_info;
            power_info.flags = AL_802_11_TXPOW_PERC;
            power_info.power = netif_config_info[j].txpower;

            operations = globals.config.wm_net_if_info[i].net_if->get_extended_operations(globals.config.wm_net_if_info[i].net_if);

            if (operations != NULL)
            {
               operations->set_essid(globals.config.wm_net_if_info[i].net_if, netif_config_info[j].essid);
               operations->set_rts_threshold(globals.config.wm_net_if_info[i].net_if, netif_config_info[j].rts_threshold);
               operations->set_frag_threshold(globals.config.wm_net_if_info[i].net_if, netif_config_info[j].frag_threshold);
               operations->set_beacon_interval(globals.config.wm_net_if_info[i].net_if, netif_config_info[j].beacon_interval);
               operations->set_bit_rate(globals.config.wm_net_if_info[i].net_if, netif_config_info[j].txrate);
               strcpy(al_netif_config_info->essid, netif_config_info[j].essid);
	            operations->set_hide_ssid(globals.config.wm_net_if_info[i].net_if,netif_config_info[j].hide_ssid);

               /**
                * If the Transmit Power is specified as 0, turn of the radio
                */

               if (netif_config_info[j].txpower == 0)
               {
                  operations->set_mode(globals.config.wm_net_if_info[i].net_if, AL_802_11_MODE_INFRA, 0);
                  AL_ATOMIC_SET(globals.config.wm_net_if_info[i].net_if->buffering_state, AL_NET_IF_BUFFERING_STATE_DROP);
               }
               else
               {
                  if (operations->get_mode(globals.config.wm_net_if_info[i].net_if) == AL_802_11_MODE_INFRA)
                  {
                     operations->set_mode(globals.config.wm_net_if_info[i].net_if, AL_802_11_MODE_MASTER, 0);
                     AL_ATOMIC_SET(globals.config.wm_net_if_info[i].net_if->buffering_state, AL_NET_IF_BUFFERING_STATE_NONE);
                  }
                  operations->set_tx_power(globals.config.wm_net_if_info[i].net_if, &power_info);
               }
            }

            break;
         }
      }
   }
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Exit %s<%d>\n",__func__,__LINE__);

   return 0;
}


al_net_if_stat_t *access_point_get_stats(AL_CONTEXT_PARAM_DECL_SINGLE)
{
   return &globals.access_point_traffic_stats;
}


access_point_netif_config_info_t *access_point_get_netif_config_info(AL_CONTEXT_PARAM_DECL al_net_if_t *al_net_if)
{
   int i;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Enter %s<%d>\n",__func__,__LINE__);
   for (i = 0; i < globals.config.wm_net_if_count; i++)
   {
      if (globals.config.wm_net_if_info[i].net_if == al_net_if)
      {
         al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Exit %s<%d>\n",__func__,__LINE__);
         return &globals.config.wm_net_if_info[i];
      }
   }
   al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP:ERROR:  %s<%d> net_if not found for :%s\n",__func__,__LINE__,
   al_net_if->name);
   return NULL;
}


access_point_vlan_info_t *access_point_add_vlan_info(AL_CONTEXT_PARAM_DECL access_point_vlan_info_t *vlan_info)
{
   return access_point_vlan_add(AL_CONTEXT vlan_info);
}


int access_point_set_dot1x_handler(AL_CONTEXT_PARAM_DECL access_point_on_dot1x_auth_init_t on_auth_init,
                                   access_point_on_dot1x_auth_start_t                      on_auth_start,
                                   access_point_on_dot1x_auth_delete_t                     on_auth_delete,
                                   access_point_on_eapol_rx_t                              on_eapol_rx)
{
   on_dot1x_auth_init_handler  = on_auth_init;
   on_dot1x_auth_start_handler = on_auth_start;
   on_eapol_rx_handler         = on_eapol_rx;
   on_dot1x_auth_delete        = on_auth_delete;

   return 0;
}


int access_point_set_dot1x_auth_result(AL_CONTEXT_PARAM_DECL access_point_sta_security_info_t *sta_security_info)
{
   access_point_sta_entry_t *node;
   int flag;
   al_802_11_operations_t           *operations;
   unsigned long                    flags;
   access_point_netif_config_info_t *netif_config_info;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Enter %s<%d>\n",__func__,__LINE__);
   node = access_point_get_sta(AL_CONTEXT & sta_security_info->dot11.destination);
   flag = 0;

   if (node != NULL)
   {
      netif_config_info = (access_point_netif_config_info_t *)node->sta.net_if->config.ext_config_info;

      if (AL_NET_IF_IS_WIRELESS(node->sta.net_if))
      {
         operations = (al_802_11_operations_t *)node->sta.net_if->get_extended_operations(node->sta.net_if);
      }
      else
      {
         operations = NULL;
      }

      if (sta_security_info->authenticated)
      {
         al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MESH_AP:INFO: %s<%d> 802.1x auth success for STA "
		 ""AL_NET_ADDR_STR, __func__, __LINE__, AL_NET_ADDR_TO_STR(&sta_security_info->dot11.destination));
         node->sta.auth_state |= ACCESS_POINT_STA_AUTH_STATE_802_1X_AUTHENTICATED;
         if (sta_security_info->dot11.enabled &&
             (operations != NULL))
         {
            if (node->sta.auth_state & ACCESS_POINT_STA_AUTH_STATE_ENCRYPTION)
            {
               operations->release_security_key(node->sta.net_if, node->sta.key_index, sta_security_info->dot11.destination.bytes);
            }
            node->sta.auth_state |= ACCESS_POINT_STA_AUTH_STATE_ENCRYPTION;
            node->sta.key_index   = operations->set_security_key(node->sta.net_if, &sta_security_info->dot11, 0);
            al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MESH_AP:INFO: %s<%d> Enabling encryption with key index %d "
			"for STA "AL_NET_ADDR_STR, __func__, __LINE__, node->sta.key_index,
			AL_NET_ADDR_TO_STR(&sta_security_info->dot11.destination));
         }
         else
         {
            node->sta.auth_state &= ~ACCESS_POINT_STA_AUTH_STATE_ENCRYPTION;
         }
         if ((sta_security_info->vlan_tag != -1) &&
             (node->sta.vlan_info == &default_vlan_info))
         {
            if (netif_config_info->security_info.radius_based_vlan)
            {
			   al_spin_lock(sta_splock, sta_spvar);
               node->sta.vlan_info = access_point_vlan_tag_get(AL_CONTEXT sta_security_info->vlan_tag);
               if (node->sta.vlan_info == NULL)
               {
                  node->sta.vlan_info = access_point_vlan_add_indirect(AL_CONTEXT sta_security_info->vlan_tag, 1);
               }
               node->sta.auth_state |= ACCESS_POINT_STA_AUTH_STATE_INDIRECT_VLAN;
			  al_spin_unlock(sta_splock, sta_spvar);
               al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,
                            "MESH_AP:INFO: %s<%d> Setting In-direct VLAN tag %d via 802.1x for STA "AL_NET_ADDR_STR,
                            __func__, __LINE__, sta_security_info->vlan_tag,
							AL_NET_ADDR_TO_STR(&sta_security_info->dot11.destination));


               if (AL_ATOMIC_GET(netif_config_info->sta_count))
               {
                  AL_ATOMIC_DEC(netif_config_info->sta_count);
               }

               access_point_incr_net_if_vlan_count(AL_CONTEXT node->sta.vlan_info, node->sta.net_if);
            }
            else
            {
               al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,
                            "MESh_AP:INFO: %s<%d> Ignoring In-direct VLAN tag %d for STA "AL_NET_ADDR_STR,
                            __func__, __LINE__, sta_security_info->vlan_tag,
                            AL_NET_ADDR_TO_STR(&sta_security_info->dot11.destination));
            }
         }
      }
      else
      {
         al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MESH_AP:INFO: %s<%d> 802.1x auth failure for STA "AL_NET_ADDR_STR,
		 __func__, __LINE__, AL_NET_ADDR_TO_STR(&sta_security_info->dot11.destination));
         if (AL_NET_IF_IS_WIRELESS(node->sta.net_if))
         {
            access_point_release_sta(AL_CONTEXT node);
            access_point_release_sta_ex(AL_CONTEXT node, 1, 1);
            flag = 1;
         }
      }
      if (!flag)
      {
         access_point_release_sta(AL_CONTEXT node);
      }
   }
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Exit %s<%d>\n",__func__,__LINE__);

   return 0;
}


void access_point_set_ds_group_cipher_type(AL_CONTEXT_PARAM_DECL unsigned char type)
{
   globals.ds_group_cipher_type = type;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Enter %s<%d>\n",__func__,__LINE__);
   if (globals.ds_group_cipher_type == AL_802_11_SECURITY_INFO_RSN_CIPHER_NONE)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MESH_AP:INFO: %s<%d> Wireless DS with no group cipher\n",
	  __func__, __LINE__);
   }
   else
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MESH_AP:INFO: %s<%d> Wireless DS with group cipher %d\n",
	  __func__, __LINE__, globals.ds_group_cipher_type);
   }
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Exit %s<%d>\n",__func__,__LINE__);
}


void access_point_enable_wm_encryption(AL_CONTEXT_PARAM_DECL unsigned char enable)
{
   int i, j;
   al_802_11_operations_t    *operations;
   al_802_11_security_info_t no_encr;
   mesh_conf_if_info_t *netif_conf;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Enter %s<%d>\n",__func__,__LINE__);
   if(reboot_in_progress == 1){
       return; /*Dont process this request when reboot_in_progress */
   }
   memset(&no_encr, 0, sizeof(al_802_11_security_info_t));

   for (i = 0; i < globals.config.wm_net_if_count; i++)
   {
      for (j = 0; j < mesh_config->if_count; j++)
      {
         netif_conf = &mesh_config->if_info[j];
         if ((netif_conf->use_type == AL_CONF_IF_USE_TYPE_AP) && !strcmp(globals.config.wm_net_if_info[i].net_if->name, netif_conf->name))       
            continue;
      }  
      if (!AL_NET_IF_IS_WIRELESS(globals.config.wm_net_if_info[i].net_if))
      {
         continue;
      }
      operations = globals.config.wm_net_if_info[i].net_if->get_extended_operations(globals.config.wm_net_if_info[i].net_if);
      if (enable)
      {
         if (globals.config.wm_net_if_info[i].group_key.enabled)
         {
            for (j = 0; j < mesh_config->if_count; j++)
            {
                netif_conf = &mesh_config->if_info[j];
                if ((netif_conf->use_type == AL_CONF_IF_USE_TYPE_WM) && !strcmp(globals.config.wm_net_if_info[i].net_if->name, netif_conf->name))
                    operations->set_security_key(globals.config.wm_net_if_info[i].net_if,
                                                &globals.config.wm_net_if_info[i].group_key,
                                                0);
            }
         }
         operations->set_security_info(globals.config.wm_net_if_info[i].net_if,
                                       &globals.config.wm_net_if_info[i].security_info.dot11);
      }
      else
      {
         operations->set_security_info(globals.config.wm_net_if_info[i].net_if,
                                       &no_encr);
      }
   }
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Exit %s<%d>\n",__func__,__LINE__);
}


int access_point_update_sta_last_rx(AL_CONTEXT_PARAM_DECL al_net_addr_t *address, int evaluate_link)
{
   access_point_sta_entry_t *node;

   node = access_point_get_sta(AL_CONTEXT address);

   if (node != NULL)
   {
      node->sta.last_packet_received = al_get_tick_count(AL_CONTEXT_SINGLE);

      if (node->sta.is_imcp && evaluate_link)
      {
         mesh_ext_send_wm_excerciser(node->sta.net_if, &node->sta.address);
      }

      access_point_release_sta(AL_CONTEXT node);
   }

   return 0;
}


int access_point_set_tester_testee_mode(AL_CONTEXT_PARAM_DECL int mode)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Enter %s<%d>\n",__func__,__LINE__);
   if ((mode != ACCESS_POINT_MODE_AP) && (globals.ap_mode != ACCESS_POINT_MODE_AP))
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "AP  : globals.ap_mode = %d  , mode = %d", globals.ap_mode, mode);
      return -1;
   }

   globals.ap_mode = mode;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Exit %s<%d>\n",__func__,__LINE__);
   return 0;
}


int access_point_get_sta_essid(AL_CONTEXT_PARAM_DECL al_net_addr_t *address, char *sta_essid, unsigned char *essid_length)
{
   access_point_sta_info_t  sta;
   al_802_11_operations_t   *extended_operations;
   access_point_sta_entry_t *sta_entry;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Enter %s<%d>\n",__func__,__LINE__);
   sta_entry = access_point_get_sta(AL_CONTEXT address);

   if (sta_entry == NULL)
   {
      *essid_length = 0;
      return 0;
   }

   sta = sta_entry->sta;

   if (sta.vlan_info != &default_vlan_info)
   {
      *essid_length = strlen(sta.vlan_info->config.essid);
      memcpy(sta_essid, sta.vlan_info->config.essid, *essid_length);
   }
   else
   {
      extended_operations = (al_802_11_operations_t *)sta.net_if->get_extended_operations(sta.net_if);

      if (extended_operations != NULL)
      {
         extended_operations->get_essid(sta.net_if, sta_essid, AL_802_11_ESSID_MAX_SIZE);
         *essid_length = strlen(sta_essid);
      }
      else
      {
         *essid_length = 0;
      }
   }

   access_point_release_sta(AL_CONTEXT sta_entry);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Exit %s<%d>\n",__func__,__LINE__);
   return 0;
}


int access_point_set_dot11e_category_info(AL_CONTEXT_PARAM_DECL int count, al_dot11e_category_info_t *info)
{
   al_802_11_operations_t    *operations;
   al_dot11e_category_info_t *ap_dot11e_category_info;
   int i,j;
   mesh_conf_if_info_t *netif_conf;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Enter %s<%d>\n",__func__,__LINE__);
   ap_dot11e_category_info = (al_dot11e_category_info_t *)&globals.dot11e_config;

   if (ap_dot11e_category_info->count > 0)
   {
      al_heap_free(AL_CONTEXT ap_dot11e_category_info->category_info);
   }

   memset(&globals.dot11e_config, 0, sizeof(access_point_dot11e_category_t));

   ap_dot11e_category_info->count         = count;
   ap_dot11e_category_info->category_info = (al_dot11e_category_details_t *)al_heap_alloc(AL_CONTEXT sizeof(al_dot11e_category_details_t) * count AL_HEAP_DEBUG_PARAM);

   for (i = 0; i < count; i++)
   {
      ap_dot11e_category_info->category_info[i].category        = info->category_info[i].category;
      ap_dot11e_category_info->category_info[i].burst_time      = info->category_info[i].burst_time;
      ap_dot11e_category_info->category_info[i].acwmin          = info->category_info[i].acwmin;
      ap_dot11e_category_info->category_info[i].acwmax          = info->category_info[i].acwmax;
      ap_dot11e_category_info->category_info[i].aifsn           = info->category_info[i].aifsn;
      ap_dot11e_category_info->category_info[i].disable_backoff = info->category_info[i].disable_backoff;
   }

   for (i = 0; i < globals.config.wm_net_if_count; i++)
   {
      for (j = 0; j < mesh_config->if_count; j++)
         {
            netif_conf = &mesh_config->if_info[j];
            if ((netif_conf->use_type == AL_CONF_IF_USE_TYPE_AP) && !strcmp(globals.config.wm_net_if_info[i].net_if->name, netif_conf->name))       
               continue;
         }  
      if (!AL_NET_IF_IS_WIRELESS(globals.config.wm_net_if_info[i].net_if))
      {
         continue;
      }

      operations = globals.config.wm_net_if_info[i].net_if->get_extended_operations(globals.config.wm_net_if_info[i].net_if);
      operations->set_dot11e_category_info(globals.config.wm_net_if_info[i].net_if, ap_dot11e_category_info);
   }

   if (globals.config.ds_net_if_info.net_if->encapsulation == AL_NET_IF_ENCAPSULATION_802_11)
   {
      operations = globals.config.ds_net_if_info.net_if->get_extended_operations(globals.config.ds_net_if_info.net_if);
      operations->set_dot11e_category_info(globals.config.ds_net_if_info.net_if, ap_dot11e_category_info);
   }

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Exit %s<%d>\n",__func__,__LINE__);
   return 0;
}


int access_point_get_dot11e_category_info(AL_CONTEXT_PARAM_DECL al_dot11e_category_info_t *dot11e_info)
{
   al_dot11e_category_info_t *ap_dot11e_config;
   int i;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Enter %s<%d>\n",__func__,__LINE__);
   ap_dot11e_config = (al_dot11e_category_info_t *)&globals.dot11e_config;

   for (i = 0; i < ap_dot11e_config->count; i++)
   {
      memcpy(&dot11e_info->category_info[i], &ap_dot11e_config->category_info[i], sizeof(al_dot11e_category_details_t));
   }

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Exit %s<%d>\n",__func__,__LINE__);
   return 0;
}


int access_point_set_acl_list(AL_CONTEXT int count, access_point_acl_entry_t *entries)
{
   access_point_acl_set_list(AL_CONTEXT count, entries);
   return 0;
}


void access_point_set_buffering(AL_CONTEXT al_net_addr_t *address, int start, unsigned int timeout)
{
   access_point_sta_entry_t *sta_entry;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Enter %s<%d>\n",__func__,__LINE__);
   sta_entry = access_point_get_sta(AL_CONTEXT address);

   if (sta_entry != NULL)
   {
      if (start)
      {
         sta_entry->sta.auth_state          |= ACCESS_POINT_STA_AUTH_STATE_BUFFERING;
         sta_entry->sta.buffering_start_time = al_get_tick_count(AL_CONTEXT_SINGLE);
         sta_entry->sta.buffering_end_time   = sta_entry->sta.buffering_start_time;
         sta_entry->sta.buffering_end_time  += (al_u64_t)timeout;
      }
      else
      {
         sta_entry->sta.auth_state &= ~ACCESS_POINT_STA_AUTH_STATE_BUFFERING;
      }

      access_point_release_sta(AL_CONTEXT sta_entry);
   }
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Exit %s<%d>\n",__func__,__LINE__);
}


int access_point_set_sta_vlan(AL_CONTEXT al_net_addr_t *address, unsigned short vlan_tag)
{
   access_point_sta_entry_t *entry;
   unsigned long            flags;
   int encr;
   access_point_netif_config_info_t *netif_config_info;
   al_802_11_operations_t *operations;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Enter %s<%d>\n",__func__,__LINE__);
   entry = access_point_get_sta(AL_CONTEXT address);

   if (entry == NULL)
   {
      return -2;           /* NOT FOUND */
   }
   if ((vlan_tag < 1) || (vlan_tag > 4094) || (entry->sta.vlan_info != &default_vlan_info))
   {
//SPAWAR
      access_point_release_sta(AL_CONTEXT entry);
//SPAWAR_END
      return -1;           /* WRONG PARAM */
   }
   al_spin_lock(sta_splock, sta_spvar);
   encr = 0;
   netif_config_info = (access_point_netif_config_info_t *)entry->sta.net_if->config.ext_config_info;

   if (entry->sta.auth_state & ACCESS_POINT_STA_AUTH_STATE_RSN &&
       entry->sta.auth_state & ACCESS_POINT_STA_AUTH_STATE_ENCRYPTION)
   {
      encr = 1;
   }

   entry->sta.vlan_info = access_point_vlan_tag_get(AL_CONTEXT vlan_tag);

   if (entry->sta.vlan_info == NULL)
   {
      entry->sta.vlan_info = access_point_vlan_add_indirect(AL_CONTEXT vlan_tag, encr);
   }

   entry->sta.auth_state |= ACCESS_POINT_STA_AUTH_STATE_INDIRECT_VLAN;

   al_spin_unlock(sta_splock, sta_spvar);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,
                "MESH_AP:INFO:  %s<%d> Setting In-direct VLAN tag %d via command line for STA "AL_NET_ADDR_STR,
                __func__, __LINE__, vlan_tag, AL_NET_ADDR_TO_STR(&entry->sta.address));


   if (AL_ATOMIC_GET(netif_config_info->sta_count))
   {
      AL_ATOMIC_DEC(netif_config_info->sta_count);
   }

   access_point_incr_net_if_vlan_count(AL_CONTEXT entry->sta.vlan_info, entry->sta.net_if);


   /**
    * Now for the tricky part:
    *	We send a disassoc message to the STA without informing the MESH and the STA management code
    *	This will cause the STA to associate back, thereby retaining its VLAN information.
    */
   operations = entry->sta.net_if->get_extended_operations(entry->sta.net_if);
   if (operations == NULL)
   {
      access_point_release_sta(AL_CONTEXT entry);
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP : ERROR : Not able to get extended operations. Hence "
                      "unable to send deauth %s : %d\n", __func__,__LINE__);
      return -1;
   }
   operations->send_deauth(entry->sta.net_if, entry->sta.net_if->config.hw_addr.bytes, &entry->sta.address);
   access_point_release_sta(AL_CONTEXT entry);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Exit %s<%d>\n",__func__,__LINE__);
   return 0;
}


int access_point_remove_sta_from_multicast_table(AL_CONTEXT_PARAM_DECL al_net_if_t *wm_net_if, al_net_addr_t *src_sta_addr)
{
   access_point_netif_config_info_t *wm_net_if_info;

   AL_ASSERT("access_point_remove_sta_from_multicast_table", wm_net_if != NULL);

   wm_net_if_info = (access_point_netif_config_info_t *)wm_net_if->config.ext_config_info;

   return access_point_remove_mcast_sta_entry(AL_CONTEXT wm_net_if_info, src_sta_addr);
}


int access_point_verify_option(AL_CONTEXT_PARAM_DECL unsigned short option_code)
{
   al_conf_option_t *option;
   int              ret;
   unsigned short   code;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Enter %s<%d>\n",__func__,__LINE__);
   for (option = globals.config.options; option != NULL; option = option->next)
   {
      ret = meshap_option_decode(AL_CONTEXT option->key, use_mac_address, &code);

      if (ret == MESHAP_OPTION_DECODE_SUCCESS)
      {
		 al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Exit %s<%d>\n",__func__,__LINE__);
         return 0;
      }
   }

   return -1;
}


int access_point_arp_add_entry(AL_CONTEXT_PARAM_DECL al_net_addr_t *server_ip, int use_incoming_net_if_addr)
{
   int use_type;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Enter %s<%d>\n",__func__,__LINE__);
   use_type = (use_incoming_net_if_addr == 1) ? MESHAP_ARP_USE_ADDR_INCOMING_NET_IF : MESHAP_ARP_USE_ADDR_DS_NET_IF;
   meshap_arp_add_entry((meshap_arp_instance_t)globals.arp_instance, server_ip->bytes, use_type);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Exit %s<%d>\n",__func__,__LINE__);

   return 0;
}


int access_point_arp_remove_entry(AL_CONTEXT_PARAM_DECL al_net_addr_t *server_ip)
{
   meshap_arp_remove_entry((meshap_arp_instance_t)globals.arp_instance, server_ip->bytes);
   return 0;
}


int access_point_enable_thread(AL_CONTEXT_PARAM_DECL int enable)
{
   unsigned long flags;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Enter %s<%d>\n",__func__,__LINE__);
   if (enable)
   {
      AL_ATOMIC_SET(globals.ap_thread_disabled, 0);

      al_set_event(AL_CONTEXT globals.queue_event_handle);
   }
   else
   {
      AL_ATOMIC_SET(globals.ap_thread_disabled, 1);
   }
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Exit %s<%d>\n",__func__,__LINE__);

   return 0;
}


extern void mesh_clear_imcp_sta(void);

void
mesh_clear_imcp_sta()
{
   access_point_sta_entry_t *next;
   access_point_sta_entry_t *node;
   unsigned long            flags;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Enter %s<%d>\n",__func__,__LINE__);
   if ((ap_enable_disable_processing == 0) && (globals.ap_state == ACCESS_POINT_STATE_STARTING))
   {
      return;
   }

   node = access_point_get_first_sta(AL_CONTEXT_SINGLE);
   al_spin_lock(sta_splock, sta_spvar);
   for ( ; node != NULL; )
   {
      next = node->next_list;
      if (next != NULL)
      {
         AL_ATOMIC_INC(next->sta.ref_count);
      }
      node = next;
   }
   al_spin_unlock(sta_splock, sta_spvar);

   node = access_point_get_first_sta(AL_CONTEXT_SINGLE);
   if (node)
       access_point_release_sta(AL_CONTEXT node);
   for ( ; node != NULL; )
   {
      next = node->next_list;

      if (node->sta.is_imcp)
      {
         access_point_release_sta(AL_CONTEXT node);
         access_point_release_sta_ex(AL_CONTEXT node, 1, 1);
      } else {
         access_point_release_sta(AL_CONTEXT node);
      }
      node = next;
   }
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Exit %s<%d>\n",__func__,__LINE__);
}
