/********************************************************************************
* MeshDynamics
* --------------
* File     : access_point_multicast.c
* Comments : Access Point Multi-cast table
* Created  : 6/20/2007
* Author   : Prachiti Gaikwad
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  3  |8/14/2007 | access_point_mcast_purge_indirect_entries Added | Sriram |
* -----------------------------------------------------------------------------
* |  2  |8/14/2007 | Changes for interrupt locking                   | Sriram |
* -----------------------------------------------------------------------------
* |  1  |7/3/2007  | Heap debug params added                         | Sriram |
* -----------------------------------------------------------------------------
* |  0  |6/20/2007 | Created.                                        |Prachiti|
* -----------------------------------------------------------------------------
********************************************************************************/

#include <linux/string.h>
#include <linux/preempt.h>
#include "al.h"
#include "access_point.h"
#include "access_point_sta.h"
#include "access_point_multicast.h"
#include "mesh_ap.h"

al_spinlock_t mcast_splock;
AL_DEFINE_PER_CPU(int, mcast_spvar);

struct al_mutex mcast_mutex;
int mcast_muvar;
int mcast_mupid;

#define _ACCESS_POINT_MULTICAST_HASH_SIZE    32

#ifdef  __GNUC__
#define _ACCESS_POINT_MULTICAST_HASH_FUNCTION(addr) \
   ({                                               \
      int hash;                                     \
      hash = (addr)->bytes[0] * 1                   \
             + (addr)->bytes[1] * 2                 \
             + (addr)->bytes[2] * 3                 \
             + (addr)->bytes[3] * 4                 \
             + (addr)->bytes[4] * 5                 \
             + (addr)->bytes[5] * 6;                \
      hash;                                         \
   }                                                \
   )
#else
static int _ACCESS_POINT_MULTICAST_HASH_FUNCTION(al_net_addr_t *addr)
{
   int hash;                      \
   hash = (addr)->bytes[0] * 1    \
          + (addr)->bytes[1] * 2  \
          + (addr)->bytes[2] * 3  \
          + (addr)->bytes[3] * 4  \
          + (addr)->bytes[4] * 5  \
          + (addr)->bytes[5] * 6; \
   return hash;                   \
}
#endif

/**
 * The following functions prefixed with an __ (2 underscores)
 * assume that interrupt locking has been done by the caller.
 */
static void __remove_from_direct_sta_list(AL_CONTEXT_PARAM_DECL
                                          access_point_mcast_t          *mcast_entry,
                                          access_point_mcast_sta_list_t *node)
{
   AL_ASSERT("_remove_from_sta_list", node != NULL);

   if (node->next != NULL)
   {
      node->next->prev = node->prev;
   }
   if (node->prev != NULL)
   {
      node->prev->next = node->next;
   }
   if (node == mcast_entry->stas)
   {
      mcast_entry->stas = node->next;
   }

   mcast_entry->sta_count--;

   al_heap_free(AL_CONTEXT node);
}


static void __remove_from_indirect_sta_list(AL_CONTEXT_PARAM_DECL
                                            access_point_mcast_t          *mcast_entry,
                                            access_point_mcast_sta_list_t *node)
{
   AL_ASSERT("__remove_from_indirect_sta_list", node != NULL);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Enter %s<%d>\n",__func__,__LINE__);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,
                "MESH_AP:INFO: %s<%d>Removing in-direct STA " AL_NET_ADDR_STR " from "AL_NET_ADDR_STR,
                __func__, __LINE__, AL_NET_ADDR_TO_STR(&node->sta_addr),
                AL_NET_ADDR_TO_STR(&mcast_entry->mcast_addr));

   if (node->next != NULL)
   {
      node->next->prev = node->prev;
   }
   if (node->prev != NULL)
   {
      node->prev->next = node->next;
   }
   if (node == mcast_entry->indirect_stas)
   {
      mcast_entry->indirect_stas = node->next;
   }

   mcast_entry->indirect_sta_count--;

   al_heap_free(AL_CONTEXT node);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Exit %s<%d>\n",__func__,__LINE__);
}


static AL_INLINE void __remove_all_direct_sta_list(AL_CONTEXT_PARAM_DECL access_point_mcast_t *mcast_entry)
{
   access_point_mcast_sta_list_t *node;
   access_point_mcast_sta_list_t *temp;


   node = mcast_entry->stas;

   while (node != NULL)
   {
      temp = node->next;
      __remove_from_direct_sta_list(AL_CONTEXT mcast_entry, node);
      node = temp;
   }
}


static AL_INLINE void __remove_all_indirect_sta_list(AL_CONTEXT_PARAM_DECL access_point_mcast_t *mcast_entry)
{
   access_point_mcast_sta_list_t *node;
   access_point_mcast_sta_list_t *temp;


   node = mcast_entry->indirect_stas;
   while (node != NULL)
   {
      temp = node->next;
      __remove_from_indirect_sta_list(AL_CONTEXT mcast_entry, node);
      node = temp;
   }
}


/**
 * Called when either the multi-cast entry has no direct or in-direct children
 * or when Un-initializing
 */
static void __delete_mcast_entry(AL_CONTEXT_PARAM_DECL
                                 access_point_netif_config_info_t *wm_net_if_info,
                                 access_point_mcast_t             *node)
{
   int bucket;
   int hash_value;

   AL_ASSERT("__delete_mcast_entry", node != NULL);


   hash_value = _ACCESS_POINT_MULTICAST_HASH_FUNCTION(&node->mcast_addr);
   bucket     = hash_value % wm_net_if_info->multicast_hash_length;

   /*
    * Remove all direct and in-direct stas (Paranoid call for surety)
    */

   __remove_all_direct_sta_list(AL_CONTEXT node);
   __remove_all_indirect_sta_list(AL_CONTEXT node);

   /*
    * Remove from hash
    */

   if (node->next_hash != NULL)
   {
      node->next_hash->prev_hash = node->prev_hash;
   }
   if (node->prev_hash != NULL)
   {
      node->prev_hash->next_hash = node->next_hash;
   }
   if (node == wm_net_if_info->multicast_hash[bucket])
   {
      wm_net_if_info->multicast_hash[bucket] = node->next_hash;
   }

   /*
    * Remove from list
    */

   if (node->next_list != NULL)
   {
      node->next_list->prev_list = node->prev_list;
   }
   if (node->prev_list != NULL)
   {
      node->prev_list->next_list = node->next_list;
   }
   if (node == wm_net_if_info->multicast_list_head)
   {
      wm_net_if_info->multicast_list_head = node->next_list;
   }
   if (node == wm_net_if_info->multicast_list_tail)
   {
      wm_net_if_info->multicast_list_tail = node->prev_list;
   }

   al_heap_free(AL_CONTEXT node);
}


static access_point_mcast_sta_list_t *__lookup_direct_sta_list(AL_CONTEXT_PARAM_DECL
                                                               access_point_mcast_t *mcast_entry,
                                                               al_net_addr_t        *sta_addr)
{
   access_point_mcast_sta_list_t *node;

   node = mcast_entry->stas;

   while (node != NULL)
   {
      if (AL_NET_ADDR_EQUAL(&node->sta_addr, sta_addr))
      {
         return node;
      }
      node = node->next;
   }

   al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP:ERROR: %s<%d> Node is NULL\n",__func__,__LINE__);
   return NULL;
}


static access_point_mcast_sta_list_t *__add_direct_sta_list(AL_CONTEXT_PARAM_DECL
                                                            access_point_mcast_t *mcast_entry,
                                                            al_net_addr_t        *sta_addr)
{
   access_point_mcast_sta_list_t *node;

   node = __lookup_direct_sta_list(AL_CONTEXT mcast_entry, sta_addr);

   if (node == NULL)
   {
      /* not found,this is a new Entry*/
      node = (access_point_mcast_sta_list_t *)al_heap_alloc(AL_CONTEXT sizeof(access_point_mcast_sta_list_t)AL_HEAP_DEBUG_PARAM);
      memcpy(&node->sta_addr, sta_addr, sizeof(al_net_addr_t));

      node->prev = NULL;
      node->next = mcast_entry->stas;

      if (mcast_entry->stas != NULL)
      {
         mcast_entry->stas->prev = node;
      }

      mcast_entry->stas = node;

      mcast_entry->sta_count++;
   }

   return node;
}


static access_point_mcast_sta_list_t *__lookup_indirect_sta_list(AL_CONTEXT_PARAM_DECL
                                                                 access_point_mcast_t *mcast_entry,
                                                                 al_net_addr_t        *sta_addr)
{
   access_point_mcast_sta_list_t *node;

   node = mcast_entry->indirect_stas;

   while (node != NULL)
   {
      if (AL_NET_ADDR_EQUAL((&node->sta_addr), (sta_addr)))
      {
         return node;
      }

      node = node->next;
   }

   al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP:ERROR: %s<%d> Node is NULL\n",__func__,__LINE__);
   return NULL;
}


static access_point_mcast_sta_list_t *__add_indirect_sta_list(AL_CONTEXT_PARAM_DECL
                                                              access_point_mcast_t *mcast_entry,
                                                              al_net_addr_t        *sta_addr)
{
   access_point_mcast_sta_list_t *node;

   node = __lookup_indirect_sta_list(mcast_entry, sta_addr);

   if (node == NULL)
   {
      /* not found,this is a new Entry */
      node = (access_point_mcast_sta_list_t *)al_heap_alloc(AL_CONTEXT sizeof(access_point_mcast_sta_list_t)AL_HEAP_DEBUG_PARAM);
      memcpy(&node->sta_addr, sta_addr, sizeof(al_net_addr_t));

      node->prev = NULL;
      node->next = mcast_entry->indirect_stas;

      if (mcast_entry->indirect_stas != NULL)
      {
         mcast_entry->indirect_stas->prev = node;
      }

      mcast_entry->indirect_stas = node;

      mcast_entry->indirect_sta_count++;
   }

   return node;
}


static access_point_mcast_t *__mcast_get_entry(AL_CONTEXT_PARAM_DECL
                                               access_point_netif_config_info_t *wm_net_if_info,
                                               al_net_addr_t                    *mcast_addr)
{
   int hash_value;
   int bucket;
   access_point_mcast_t *multicast_entry;

   AL_ASSERT("__mcast_get_entry", wm_net_if_info != NULL);
   AL_ASSERT("__mcast_get_entry", mcast_addr != NULL);

   hash_value = _ACCESS_POINT_MULTICAST_HASH_FUNCTION(mcast_addr);
   bucket     = hash_value % wm_net_if_info->multicast_hash_length;

   multicast_entry = wm_net_if_info->multicast_hash[bucket];

   while (multicast_entry != NULL)
   {
      if (AL_NET_ADDR_EQUAL(&multicast_entry->mcast_addr, mcast_addr))
      {
         return multicast_entry;
      }

      multicast_entry = multicast_entry->next_hash;
   }

   return NULL;
}


static void __mcast_entry_remove_sta(AL_CONTEXT_PARAM_DECL
                                     access_point_netif_config_info_t *wm_net_if_info,
                                     access_point_mcast_t             *mcast_entry,
                                     al_net_addr_t                    *src_sta_addr)
{
   access_point_mcast_sta_list_t *mcast_sta;

   /* Remove src_sta from direct/indirect list */

   mcast_sta = __lookup_direct_sta_list(AL_CONTEXT mcast_entry, src_sta_addr);

   if (mcast_sta != NULL)
   {
      __remove_from_direct_sta_list(AL_CONTEXT mcast_entry, mcast_sta);
   }
   else
   {
      mcast_sta = __lookup_indirect_sta_list(AL_CONTEXT mcast_entry, src_sta_addr);

      if (mcast_sta != NULL)
      {
         __remove_from_indirect_sta_list(AL_CONTEXT mcast_entry, mcast_sta);
      }
   }

   /**
    * Paranoid count check condition is <= 0 as oppossed to == 0
    */

   if ((mcast_entry->indirect_sta_count <= 0) && (mcast_entry->sta_count <= 0))
   {
      __delete_mcast_entry(AL_CONTEXT wm_net_if_info, mcast_entry);
   }
}


static int __mcast_remove_entry(AL_CONTEXT_PARAM_DECL
                                access_point_netif_config_info_t *wm_net_if_info,
                                al_net_addr_t                    *mcast_addr,
                                al_net_addr_t                    *src_sta_addr)
{
   access_point_mcast_t *mcast_entry;

   mcast_entry = __mcast_get_entry(AL_CONTEXT wm_net_if_info, mcast_addr);

   if (mcast_entry != NULL)
   {
      __mcast_entry_remove_sta(AL_CONTEXT wm_net_if_info, mcast_entry, src_sta_addr);

      return 0;
   }

   return -1;
}


/**
 * Returns the Multicast entry for the provided multi-cast MAC address.
 * Returns NULL if not found.
 */
access_point_mcast_t *access_point_mcast_get_entry(AL_CONTEXT_PARAM_DECL
                                                   access_point_netif_config_info_t *wm_net_if_info,
                                                   al_net_addr_t                    *mcast_addr)
{
   unsigned long        flags;
   access_point_mcast_t *multicast_entry;

   al_mutex_lock(mcast_mutex, mcast_muvar, mcast_mupid);

   multicast_entry = __mcast_get_entry(AL_CONTEXT wm_net_if_info, mcast_addr);

   al_mutex_unlock(mcast_mutex, mcast_muvar, mcast_mupid);

   return multicast_entry;
}


/**
 * Called by access_point_mcast_add_entry to add the new_entry to the Hash table and the list
 * Also adds the src_sta_addr to either the direct or indirect list of the new_entry
 */
static AL_INLINE void _mcast_add_helper(AL_CONTEXT_PARAM_DECL
                                        access_point_netif_config_info_t *wm_net_if_info,
                                        access_point_mcast_t             *new_entry,
                                        al_net_addr_t                    *src_sta_addr)
{
   int           hash_value;
   int           bucket;
   //unsigned long flags;

   access_point_sta_entry_t *src_sta;

   src_sta = NULL;

   new_entry->next_hash          = NULL;
   new_entry->prev_hash          = NULL;
   new_entry->next_list          = NULL;
   new_entry->prev_list          = NULL;
   new_entry->indirect_sta_count = 0;
   new_entry->sta_count          = 0;
   new_entry->stas          = NULL;
   new_entry->indirect_stas = NULL;


   hash_value = _ACCESS_POINT_MULTICAST_HASH_FUNCTION(&new_entry->mcast_addr);
   bucket     = hash_value % wm_net_if_info->multicast_hash_length;


   al_mutex_lock(mcast_mutex, mcast_muvar, mcast_mupid);

   /**
    *	Add to hash
    */

   if (wm_net_if_info->multicast_hash[bucket] == NULL)
   {
      wm_net_if_info->multicast_hash[bucket] = new_entry;
   }
   else
   {
      new_entry->next_hash = wm_net_if_info->multicast_hash[bucket];
      wm_net_if_info->multicast_hash[bucket]->prev_hash = new_entry;
      wm_net_if_info->multicast_hash[bucket]            = new_entry;
   }


   /**
    *	Add to list
    */

   if (wm_net_if_info->multicast_list_head == NULL)
   {
      wm_net_if_info->multicast_list_head = new_entry;
      wm_net_if_info->multicast_list_tail = new_entry;
   }
   else
   {
      wm_net_if_info->multicast_list_tail->next_list = new_entry;
      new_entry->prev_list = wm_net_if_info->multicast_list_tail;
      wm_net_if_info->multicast_list_tail = new_entry;
   }


   /**
    * Add to the direct/indirect sta list
    */

   src_sta = access_point_get_sta(AL_CONTEXT src_sta_addr);

   if (src_sta != NULL)
   {
      __add_direct_sta_list(AL_CONTEXT new_entry, src_sta_addr);
   }
   else
   {
      /* check if src_sta belongs down the tree */
      if (mesh_ap_is_table_entry_present(AL_CONTEXT src_sta_addr) == 0)
      {
         __add_indirect_sta_list(AL_CONTEXT new_entry, src_sta_addr);
      }
   }
   al_mutex_unlock(mcast_mutex, mcast_muvar, mcast_mupid);

   if (src_sta != NULL)
   {
      access_point_release_sta(src_sta);
   }
}


/**
 * Creates a new multi-cast entry (if required) , and adds the src_sta_addr
 * to the direct or in-direct list of the entry (if required)
 */
void access_point_mcast_add_entry(AL_CONTEXT_PARAM_DECL
                                  access_point_netif_config_info_t *wm_net_if_info,
                                  al_net_addr_t                    *mcast_addr,
                                  al_net_addr_t                    *src_sta_addr)
{
   access_point_mcast_t *mcast_entry;
   int hash_value;
   int bucket;
   access_point_sta_entry_t      *src_sta;
   access_point_mcast_sta_list_t *mcast_sta;
   unsigned long                 flags;

   AL_ASSERT("access_point_mcast_add_entry", wm_net_if_info != NULL);
   AL_ASSERT("access_point_mcast_add_entry", mcast_addr != NULL);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Enter %s<%d>\n",__func__,__LINE__);
   mcast_sta   = NULL;
   mcast_entry = access_point_mcast_get_entry(AL_CONTEXT wm_net_if_info, mcast_addr);

   if (mcast_entry != NULL)
   {
      int tree_child;

      src_sta    = access_point_get_sta(AL_CONTEXT src_sta_addr);
      tree_child = mesh_ap_is_table_entry_present(AL_CONTEXT src_sta_addr);

      /* Add src_sta to the direct/indirect list */

      al_mutex_lock(mcast_mutex, mcast_muvar, mcast_mupid);
      if ((src_sta == NULL) && (tree_child == 0))
      {
         __add_indirect_sta_list(AL_CONTEXT mcast_entry, src_sta_addr);
      }
      else
      {
         __add_direct_sta_list(AL_CONTEXT mcast_entry, src_sta_addr);
      }
	  al_mutex_unlock(mcast_mutex, mcast_muvar, mcast_mupid);

      if (src_sta != NULL)
      {
         access_point_release_sta(src_sta);
      }

      al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Exit %s<%d>\n",__func__,__LINE__);
      return;
   }

   /* Add new entry since not found in the hash */


   hash_value = _ACCESS_POINT_MULTICAST_HASH_FUNCTION(mcast_addr);
   bucket     = hash_value % wm_net_if_info->multicast_hash_length;

   mcast_entry = (access_point_mcast_t *)al_heap_alloc(AL_CONTEXT sizeof(access_point_mcast_t)AL_HEAP_DEBUG_PARAM);

   memset(mcast_entry, 0, sizeof(access_point_mcast_t));

   memcpy(&(mcast_entry->mcast_addr), mcast_addr, sizeof(al_net_addr_t));

   if (src_sta_addr != NULL)
   {
      _mcast_add_helper(wm_net_if_info, mcast_entry, src_sta_addr);
   }
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Exit %s<%d>\n",__func__,__LINE__);
}


/**
 * Removes the src_sta_addr from the entry for mcast_addr. If src_sta_addr was the
 * last direct/in-direct sta in the entry's list then the entry itself is deleted.
 */
int access_point_mcast_remove_entry(AL_CONTEXT_PARAM_DECL
                                    access_point_netif_config_info_t *wm_net_if_info,
                                    al_net_addr_t                    *mcast_addr,
                                    al_net_addr_t                    *src_sta_addr)
{
   unsigned long flags;
   int           ret;

   AL_ASSERT("access_point_mcast_remove_entry", wm_net_if_info != NULL);
   AL_ASSERT("access_point_mcast_remove_entry", mcast_addr != NULL);

   al_mutex_lock(mcast_mutex, mcast_muvar, mcast_mupid);

   ret = __mcast_remove_entry(AL_CONTEXT wm_net_if_info, mcast_addr, src_sta_addr);

   al_mutex_unlock(mcast_mutex, mcast_muvar, mcast_mupid);

   return ret;
}


/**
 * Removes the src_sta_addr from all multi-cast entries subscribed on the given net_if
 */
int access_point_remove_mcast_sta_entry(AL_CONTEXT_PARAM_DECL
                                        access_point_netif_config_info_t *wm_net_if_info,
                                        al_net_addr_t                    *src_sta_addr)
{
   access_point_mcast_t *node;
   access_point_mcast_t *temp;
   unsigned long        flags;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Enter %s<%d>\n",__func__,__LINE__);
  al_mutex_lock(mcast_mutex, mcast_muvar, mcast_mupid);

   node = wm_net_if_info->multicast_list_head;

   while (node != NULL)
   {
      temp = node->next_list;
      __mcast_remove_entry(AL_CONTEXT wm_net_if_info, &node->mcast_addr, src_sta_addr);
      node = temp;
   }

  al_mutex_unlock(mcast_mutex, mcast_muvar, mcast_mupid);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Exit %s<%d>\n",__func__,__LINE__);

   return 0;
}


int access_point_multicast_initialize(AL_CONTEXT_PARAM_DECL access_point_netif_config_info_t *wm_net_if_info)
{
   int           i;
   unsigned long flags;

   AL_ASSERT("access_point_multicast_initialize", wm_net_if_info != NULL);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Enter %s<%d>\n",__func__,__LINE__);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MESH_AP:INFO: %s<%d> %s Initializing multi-cast tables\n",
   __func__, __LINE__, wm_net_if_info->net_if->name);


   al_spin_lock_init(&mcast_splock);

   al_mutex_lock(mcast_mutex, mcast_muvar, mcast_mupid);

   wm_net_if_info->multicast_hash_length = _ACCESS_POINT_MULTICAST_HASH_SIZE;
   wm_net_if_info->multicast_hash        = (access_point_mcast_t **)al_heap_alloc(AL_CONTEXT sizeof(access_point_mcast_t *) * wm_net_if_info->multicast_hash_length AL_HEAP_DEBUG_PARAM);

   for (i = 0; i < wm_net_if_info->multicast_hash_length; i++)
   {
      wm_net_if_info->multicast_hash[i] = NULL;
   }

   wm_net_if_info->multicast_list_head = NULL;
   wm_net_if_info->multicast_list_tail = NULL;

   al_mutex_unlock(mcast_mutex, mcast_muvar, mcast_mupid);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Exit %s<%d>\n",__func__,__LINE__);

   return 0;
}


int access_point_multicast_uninitialize(AL_CONTEXT_PARAM_DECL access_point_netif_config_info_t *wm_net_if_info)
{
	access_point_mcast_t *node;
	access_point_mcast_t *temp;
	unsigned long        flags;

	AL_ASSERT("access_point_multicast_uninitialize", wm_net_if_info != NULL);
    al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Enter %s<%d>\n",__func__,__LINE__);

	al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MESH_AP:INFO: %s<%d> %s Un-initializing multi-cast tables\n",
	__func__, __LINE__, wm_net_if_info->net_if->name);

	al_mutex_lock(mcast_mutex, mcast_muvar, mcast_mupid);

	node = wm_net_if_info->multicast_list_head;
	while (node != NULL)
	{
		temp = node->next_list;
		__delete_mcast_entry(AL_CONTEXT wm_net_if_info, node);
		node = temp;
	}

	al_mutex_unlock(mcast_mutex, mcast_muvar, mcast_mupid);

	al_heap_free(AL_CONTEXT wm_net_if_info->multicast_hash);
    al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Exit %s<%d>\n",__func__,__LINE__);

	return 0;
}


void access_point_mcast_purge_indirect_entries(AL_CONTEXT_PARAM_DECL access_point_netif_config_info_t *wm_net_if_info)
{
   access_point_mcast_t          *entry;
   access_point_mcast_t          *temp_entry;
   access_point_mcast_sta_list_t *sta;
   access_point_mcast_sta_list_t *temp_sta;
   unsigned long                 flags;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Enter %s<%d>\n",__func__,__LINE__);
   al_mutex_lock(mcast_mutex, mcast_muvar, mcast_mupid);

   entry = wm_net_if_info->multicast_list_head;

   while (entry != NULL)
   {
      temp_entry = entry->next_list;

      sta = entry->indirect_stas;

      while (sta != NULL)
      {
         temp_sta = sta->next;

         if (mesh_ap_is_table_entry_present(AL_CONTEXT & sta->sta_addr) != 0)
         {
            /**
             * Remove the STA from the in-direct list
             */

            al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,
                         "MESH_AP:INFO: %s<%d> Purging in-direct STA " AL_NET_ADDR_STR " from "AL_NET_ADDR_STR,
                         __func__, __LINE__, AL_NET_ADDR_TO_STR(&sta->sta_addr),
                         AL_NET_ADDR_TO_STR(&entry->mcast_addr));

            __remove_from_indirect_sta_list(AL_CONTEXT entry, sta);
         }

         sta = temp_sta;
      }

      /**
       * As a result, the entry's direct and indirect counts might have reached 0.
       * If so, we delete the entry itself.
       */

      if ((entry->indirect_sta_count <= 0) && (entry->sta_count <= 0))
      {
         __delete_mcast_entry(AL_CONTEXT wm_net_if_info, entry);
      }

      entry = temp_entry;
   }

   al_mutex_unlock(mcast_mutex, mcast_muvar, mcast_mupid);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Exit %s<%d>\n",__func__,__LINE__);
}
