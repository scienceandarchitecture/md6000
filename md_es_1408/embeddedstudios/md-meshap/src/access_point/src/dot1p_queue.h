/********************************************************************************
* MeshDynamics
* --------------
* File     : dot1p_queue.h
* Comments : dot1p Queue Header
* Created  : 11/14/2005
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  1  |6/12/2006 | dot1p_queue_print_hstats added                  | Sriram |
* -----------------------------------------------------------------------------
* |  0  |11/14/2005| Created                                         | Bindu  |
* -----------------------------------------------------------------------------
********************************************************************************/

#ifndef __DOT1P_QUEUE_H__
#define __DOT1P_QUEUE_H__

#define _PACKET_IS_WDS(fc)       ((fc & AL_802_11_FC_FROMDS) && (fc & AL_802_11_FC_TODS))

#define _PACKET_IS_FROMDS(fc)    ((fc & AL_802_11_FC_FROMDS) && !(fc & AL_802_11_FC_TODS))

#define _PACKET_IS_TODS(fc)      ((!(fc & AL_802_11_FC_FROMDS)) && (fc & AL_802_11_FC_TODS))

#define MULTIPLE_AP_THREAD  0
#include <linux/spinlock.h>

struct dot1p_queue_entry
{
   al_u64_t                 timestamp;
   al_net_if_t              *net_if;
   al_packet_t              *packet;
   struct dot1p_queue_entry *next;
#if MULTIPLE_AP_THREAD
   struct dot1p_queue_entry *prev;
#endif
};

typedef struct dot1p_queue_entry   dot1p_queue_entry_t;

struct dot1p_pool
{
   dot1p_queue_entry_t node;
   int                 pool_item_type;
   struct dot1p_pool   *next_pool;
};

typedef struct dot1p_pool   dot1p_pool_t;


struct dot1p_queue
{
   int                 queue_semaphore_handle;
   dot1p_queue_entry_t *dot1p_queue_head;
   dot1p_queue_entry_t *dot1p_queue_tail;
#if MULTIPLE_AP_THREAD
   dot1p_queue_entry_t *dot1p_queue_fhead;
   dot1p_queue_entry_t *dot1p_queue_ftail;
#endif
   al_spinlock_t          dot1p_Qsplock;
   al_spinlock_t          dot1pQ_imcpsplock;
   char				   current_HQindx;
   dot1p_queue_entry_t *dot1p_queue_imcphead;
   dot1p_queue_entry_t *dot1p_queue_imcptail;
   int                 max_level;
   int                 current_level;
   int                 total_packets_added;
   int                 total_packets_removed;
};

typedef struct dot1p_queue   dot1p_queue_t;


//void				dot1p_initialize_pool			(AL_CONTEXT_PARAM_DECL_SINGLE);
//void				dot1p_uninitialize_pool			(AL_CONTEXT_PARAM_DECL_SINGLE);

dot1p_queue_t **dot1p_queue_initialize(AL_CONTEXT_PARAM_DECL_SINGLE);
int dot1p_queue_uninitialize(AL_CONTEXT_PARAM_DECL access_point_queue_t *ap_queue);
int dot1p_queue_add_packet(AL_CONTEXT_PARAM_DECL access_point_queue_t *ap_queue, al_net_if_t *al_net_if, al_packet_t *packet);
al_packet_t *dot1p_queue_remove_packet(AL_CONTEXT_PARAM_DECL access_point_queue_t *ap_queue, al_net_if_t **net_if);
int dot1p_queue_empty_check(AL_CONTEXT_PARAM_DECL access_point_queue_t *ap_queue);

//int					dot1p_queue_print_stats			(AL_CONTEXT_PARAM_DECL struct access_point_queue* ap_queue,char* print_p);
int dot1p_queue_print_stats(AL_CONTEXT_PARAM_DECL struct access_point_queue *ap_queue, struct seq_file *m);
int dot1p_queue_requeue_packet(AL_CONTEXT_PARAM_DECL struct access_point_queue *ap_queue, al_net_if_t *al_net_if, al_packet_t *packet);

//int					dot1p_queue_print_hstats		(AL_CONTEXT_PARAM_DECL struct access_point_queue* ap_queue,char* print_p);
int dot1p_queue_print_hstats(AL_CONTEXT_PARAM_DECL struct access_point_queue *ap_queue, struct seq_file *m);
void _dot1p_free_entry(AL_CONTEXT_PARAM_DECL dot1p_pool_t *pool_item);
 #endif/*__DOT1P_QUEUE_H__*/
