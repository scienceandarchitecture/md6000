/********************************************************************************
* MeshDynamics
* --------------
* File     : access_point_tester_testee.c
* Comments : AccessPoint tester main file
* Created  : 9/25/2005
* Author   : Abhijit Ayarekar
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  2  |03/14/2006| Disabled all code until Fixed                   | Sriram |
* -----------------------------------------------------------------------------
* |  1  |01/17/2006| Changes for aid                                 | Sriram |
* -----------------------------------------------------------------------------
* |  0  |9/25/2005 | Created.                                        | Abhijit|
* -----------------------------------------------------------------------------
********************************************************************************/

#include <linux/string.h>
#include "al.h"
#include "al_impl_context.h"
#include "access_point.h"
#include "access_point_globals.h"
#include "al_print_log.h"
#include "access_point_tester_testee.h"
#include "access_point_sta.h"
#include "access_point_data.h"
#include "al_socket.h"
#include "al_net_addr.h"

#if 0

#define _DS_MAC                                                       (&tester_testee_data.if_info[0].net_if->config.hw_addr)
#define _DS_NET_IF                                                    (tester_testee_data.if_info[0].net_if)

#define LQCP_UDP_PORT                                                 0xeeee

#define INDEX_WLAN_ALL                                                -1
#define INDEX_IXP0                                                    0
#define INDEX_IXP1                                                    1
#define INDEX_WLAN0                                                   2
#define INDEX_WLAN1                                                   3
#define INDEX_WLAN2                                                   4
#define INDEX_WLAN3                                                   5


#define LQCP_ASSOCIATION_RETRY_INTERVAL                               10000
#define LQCP_PACKET_SEND_INTERVAL                                     100
#define LQCP_CHANNEL_SWITCH_INTERVAL                                  15000
#define REBOOT_INTERVAL_TYPE_MANUAL                                   0xFF

#define LQCP_SNIP_PACKET_TYPE_TESTER_TESTEE_MODE_INIT                 (unsigned short)(100)
#define LQCP_SNIP_PACKET_TYPE_TESTER_TESTEE_MODE_START_REQ            (unsigned short)(101)
#define LQCP_SNIP_PACKET_TYPE_TESTER_TESTEE_MODE_START_RES            (unsigned short)(102)
#define LQCP_SNIP_PACKET_TYPE_TESTER_TESTEE_MODE_STOP_REQ             (unsigned short)(103)
#define LQCP_SNIP_PACKET_TYPE_TESTER_TESTEE_MODE_STOP_RES             (unsigned short)(104)
#define LQCP_SNIP_PACKET_TYPE_TESTER_TESTEE_ANALYSIS                  (unsigned short)(105)
#define LQCP_SNIP_PACKET_TYPE_TESTER_DATA_REQUEST                     (unsigned short)(106)
#define LQCP_SNIP_PACKET_TYPE_TESTER_DATA_RESPONSE                    (unsigned short)(107)
#define LQCP_SNIP_PACKET_TYPE_TESTER_TESTEE_RESET_REQUEST             (unsigned short)(108)
#define LQCP_SNIP_PACKET_TYPE_TESTER_TESTEE_RESET_RESPONSE            (unsigned short)(109)
#define LQCP_SNIP_PACKET_TYPE_TESTER_TESTEE_CHANNELSWITCH_REQUEST     (unsigned short)(110)
#define LQCP_SNIP_PACKET_TYPE_TESTER_TESTEE_CHANNELSWITCH_RESPONSE    (unsigned short)(111)

#define LQCP_CHANNEL_SWITCH_REQUEST_DENIED                            0
#define LQCP_CHANNEL_SWITCH_REQUEST_FORWARDED                         1
#define LQCP_CHANNEL_SWITCH_REQUEST_COMPLETED                         2

#define LQCP_STATE_INITIALIZED                                        1
#define LQCP_STATE_WAITING                                            2
#define LQCP_STATE_STARTED                                            3
#define LQCP_STATE_QUITTING                                           4

#define SET_LQCP_STATE(state)    _lqcp_state = state;
#define GET_LQCP_STATE                                                _lqcp_state

#define LQCP_WLAN_INTERFACE_COUNT                                     4
#define LQCP_TEST_ALL_INTERFACES                                      0xFF

static access_point_tester_testee_globals_t tester_testee_data;

static int _stop_worker_threads_event_handle;
static int _stop_worker_threads_semaphore_handle;
static int _stop_packet_receiver_event_handle;

static int  a_channels[] = { 52, 60, 149, 157, 165 };
static int  bg_channels[] = { 1, 6, 11 };
static int  a_channel_index;
static int  bg_channel_index;
static int  _active_interface_index;
static char _single_link_essid[] = { "md_analyser" };
static int  _lqcp_state;


static int _get_next_mac_address(al_net_addr_t *mac_addr, int offset);
static int _initialize_interface_link_mac_address_for_all_link_test();
static int _initialize_interface_link_mac_address_for_single_link_test(int interface_index, int link_interface_index);
static int _tester_testee_setup_interfaces_for_all_link_test(AL_CONTEXT_PARAM_DECL);
static int _tester_testee_setup_interfaces_for_single_link_test(AL_CONTEXT_PARAM_DECL);

static tester_testee_if_info_t testee_if_info[] =
{
   {
    { { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 }, 6 }, "ixp0",                                                                                                                               "Testee",          "Testee_ixp0",    AL_NET_IF_FILTER_MODE_UNICAST | AL_NET_IF_FILTER_MODE_MULTICAST | AL_NET_IF_FILTER_MODE_BROADCAST | AL_NET_IF_FILTER_MODE_PROMISCOUS,
    { { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 }, 6 }, AL_NET_IF_FILTER_MODE_UNICAST | AL_NET_IF_FILTER_MODE_MULTICAST | AL_NET_IF_FILTER_MODE_BROADCAST | AL_NET_IF_FILTER_MODE_PROMISCOUS,
    2367, 2367, 100, 100, 0, 0, 0, 0, 0
},
   {
    { { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 }, 6 }, "ixp1",                                                                                                                               "Testee",          "Testee_ixp1",    AL_NET_IF_FILTER_MODE_PROMISCOUS,
    { { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 }, 6 }, AL_NET_IF_FILTER_MODE_PROMISCOUS,                                                                                                                  2367,             2367,                                                                                                                                  100, 100, 0, 0, 0, 0, 0
},
   {
    { { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 }, 6 }, "wlan0",                                                                                                                              "md_testee_wlan0", "md_testee_wlan", AL_802_11_MODE_MASTER,
    { { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 }, 6 }, AL_802_11_MODE_MASTER,                                                                                                                             2367,             2367,                                                                                                                                  100, 100, 0, 1, 0, 0, 0
},
   {
    { { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 }, 6 }, "wlan1",                                                                                                                              "md_tester_wlan0", "md_testee_wlan", AL_802_11_MODE_INFRA,
    { { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 }, 6 }, AL_802_11_MODE_INFRA,                                                                                                                              2367,             2367,                                                                                                                                  100, 100, 0, 0, 0, 0, 0
},
   {
    { { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 }, 6 }, "wlan2",                                                                                                                              "md_testee_wlan2", "md_testee_wlan", AL_802_11_MODE_MASTER,
    { { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 }, 6 }, AL_802_11_MODE_MASTER,                                                                                                                             2367,             2367,                                                                                                                                  100, 100, 0, 2, 0, 0, 0
},
   {
    { { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 }, 6 }, "wlan3",                                                                                                                              "md_tester_wlan3", "md_testee_wlan", AL_802_11_MODE_INFRA,
    { { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 }, 6 }, AL_802_11_MODE_INFRA,                                                                                                                              2367,             2367,                                                                                                                                  100, 100, 0, 0, 0, 0, 0
},
};

static tester_testee_if_info_t tester_if_info[] =
{
   {
    { { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 }, 6 }, "ixp0",                                                                                                                               "Tester",          "Tester_ixp0",    AL_NET_IF_FILTER_MODE_UNICAST | AL_NET_IF_FILTER_MODE_MULTICAST | AL_NET_IF_FILTER_MODE_BROADCAST | AL_NET_IF_FILTER_MODE_PROMISCOUS,
    { { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 }, 6 }, AL_NET_IF_FILTER_MODE_UNICAST | AL_NET_IF_FILTER_MODE_MULTICAST | AL_NET_IF_FILTER_MODE_BROADCAST | AL_NET_IF_FILTER_MODE_PROMISCOUS,
    2367, 2367, 100, 100, 0, 0, 0
},
   {
    { { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 }, 6 }, "ixp1",                                                                                                                               "Tester",          "Tester_ixp1",    AL_NET_IF_FILTER_MODE_PROMISCOUS,
    { { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 }, 6 }, AL_NET_IF_FILTER_MODE_PROMISCOUS,                                                                                                                  2367,             2367,                                                                                                                                  100, 100, 0, 0, 0, 0, 0
},
   {
    { { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 }, 6 }, "wlan0",                                                                                                                              "md_tester_wlan0", "md_tester_wlan", AL_802_11_MODE_MASTER,
    { { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 }, 6 }, AL_802_11_MODE_MASTER,                                                                                                                             2367,             2367,                                                                                                                                  100, 100, 0, 0, 0, 0, 0
},
   {
    { { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 }, 6 }, "wlan1",                                                                                                                              "md_testee_wlan0", "md_tester_wlan", AL_802_11_MODE_INFRA,
    { { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 }, 6 }, AL_802_11_MODE_INFRA,                                                                                                                              2367,             2367,                                                                                                                                  100, 100, 0, 1, 0, 0, 0
},
   {
    { { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 }, 6 }, "wlan2",                                                                                                                              "md_testee_wlan2", "md_tester_wlan", AL_802_11_MODE_MASTER,
    { { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 }, 6 }, AL_802_11_MODE_INFRA,                                                                                                                              2367,             2367,                                                                                                                                  100, 100, 0, 2, 0, 0, 0
},
   {
    { { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 }, 6 }, "wlan3",                                                                                                                              "md_tester_wlan3", "md_tester_wlan", AL_802_11_MODE_MASTER,
    { { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 }, 6 }, AL_802_11_MODE_MASTER,                                                                                                                             2367,             2367,                                                                                                                                  100, 100, 0, 0, 0, 0, 0
},
};

static int _send_packet(AL_CONTEXT_PARAM_DECL al_net_if_t *net_if, al_packet_t *main_packet)
{
   unsigned char *temp;
   al_packet_t   *packet;

   AL_PRINT_LOG_FLOW_0("AP	: send_packet()");

   AL_ASSERT("MESH_AP	: _send_packet", net_if != NULL);
   AL_ASSERT("MESH_AP	: _send_packet", main_packet != NULL);

   packet = al_allocate_packet(AL_CONTEXT AL_PACKET_DIRECTION_OUT AL_PACKET_DEBUG_PARAM);

   if (packet == NULL)
   {
      return -1;
   }

   AL_ASSERT("AP	: _send_packet", packet != NULL);

   temp = packet->buffer;
   memcpy(temp, main_packet->buffer, main_packet->buffer_length);
   memcpy(packet, main_packet, sizeof(al_packet_t));
   packet->buffer = temp;

   net_if->transmit(net_if, packet);
   return 0;
}


static int _send_ds_net_if(AL_CONTEXT_PARAM_DECL al_net_if_t *net_if, al_packet_t *main_packet)
{
   if (AL_NET_IF_IS_WIRELESS(net_if))
   {
      memcpy(&main_packet->addr_3, &main_packet->addr_1, sizeof(al_net_addr_t));
      memcpy(&main_packet->addr_2, &net_if->config.hw_addr, sizeof(al_net_addr_t));
      memcpy(&main_packet->addr_1, &main_packet->addr_1, sizeof(al_net_addr_t));
      /* set toDS to 1 as packet going up */
      main_packet->frame_control = AL_802_11_FC_TODS;
   }

   return _send_packet(AL_CONTEXT net_if, main_packet);
}


static int _send_wm_net_if(AL_CONTEXT_PARAM_DECL al_net_if_t *net_if, al_packet_t *main_packet)
{
   if (AL_NET_IF_IS_WIRELESS(net_if))
   {
      memcpy(&main_packet->addr_2, &net_if->config.hw_addr, sizeof(al_net_addr_t));
      memcpy(&main_packet->addr_3, &net_if->config.hw_addr, sizeof(al_net_addr_t));
      /* set FROMDS to 1 as packet going down */
      main_packet->frame_control = AL_802_11_FC_FROMDS;
   }
   return _send_packet(AL_CONTEXT net_if, main_packet);
}


static int _tester_testee_send_imcp_packet(AL_CONTEXT_PARAM_DECL al_net_if_t *net_if, unsigned char *buffer, int buffer_length)
{
   int         ret;
   al_packet_t *main_packet;
   int         net_if_count;
   al_net_if_t *net_if_all;
   int         i, flags;

   AL_PRINT_LOG_FLOW_0("AP	: _send_tester_imcp_packet()");

   AL_ASSERT("AP	: _send_tester_imcp_packet", buffer != NULL);

   main_packet = al_allocate_packet(AL_CONTEXT AL_PACKET_DIRECTION_OUT AL_PACKET_DEBUG_PARAM);

   if (main_packet == NULL)
   {
      return -1;
   }

   AL_ASSERT("MESH_AP	: _send_tester_imcp_packet", main_packet != NULL);

   /* Generate UDP Header packet*/

   /*if(net_if == NULL) {*/
   ret = udp_setup_packet(main_packet,
                          _DS_MAC,
                          LQCP_UDP_PORT,
                          &broadcast_net_addr,
                          LQCP_UDP_PORT,
                          buffer,
                          buffer_length);

   /*}else{
    *      ret =  udp_setup_packet(main_packet,
    *                                                       _DS_MAC,
    *                                                       LQCP_UDP_PORT,
    *                                                       &tester_testee_data.peer_mac_address,
    *                                                       LQCP_UDP_PORT,
    *                                                       buffer,
    *                                                       buffer_length);
    *
    * }*/

   if (ret)
   {
		(tx_rx_pkt_stats.packet_drop[60])++;
		al_release_packet(AL_CONTEXT main_packet);
      return ret;
   }

   /* Generate IP Header packet*/
   ret = ip_setup_packet(main_packet);
   if (ret)
   {
		(tx_rx_pkt_stats.packet_drop[61])++;
		al_release_packet(AL_CONTEXT main_packet);
      return ret;
   }


   _send_ds_net_if(AL_CONTEXT _DS_NET_IF, main_packet);

   if (net_if == NULL)
   {
      net_if_count = al_get_net_if_count(AL_CONTEXT_SINGLE);
      for (i = 0; i < net_if_count; i++)
      {
         net_if_all = al_get_net_if(AL_CONTEXT i);

         flags = net_if_all->get_flags(net_if_all);

         if ((flags & AL_NET_IF_FLAGS_LINKED) && (flags & AL_NET_IF_FLAGS_UP))
         {
            if (net_if_all != _DS_NET_IF)
            {
               _send_wm_net_if(AL_CONTEXT net_if_all, main_packet);
            }
         }
      }
   }
   else
   {
      _send_wm_net_if(AL_CONTEXT net_if, main_packet);
   }

   al_release_packet(AL_CONTEXT main_packet);
   return 0;
}


static int _get_next_a_channel()
{
   if (++a_channel_index == sizeof(a_channels))
   {
      a_channel_index = 0;
   }

   return a_channels[a_channel_index];
}


static int _get_next_bg_channel()
{
   if (++bg_channel_index == sizeof(bg_channels))
   {
      bg_channel_index = 0;
   }

   return bg_channels[bg_channel_index];
}


int _tester_testee_stop_thread(AL_CONTEXT_PARAM_DECL al_thread_param_t *param)
{
   unsigned char packet[2400];
   unsigned char lqcp_tester_testee_stop_res_packet[] = { 'L', 'Q', 'C', 'P', 1, 0, LQCP_SNIP_PACKET_TYPE_TESTER_TESTEE_MODE_STOP_RES };
   unsigned char *p;

   al_set_thread_name(AL_CONTEXT "stop_thread");

   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "AP	: _tester_testee_stop_thread  LQCP_SNIP_PACKET_TYPE_TESTER_TESTEE_MODE_STOP_REQ");

   al_set_event(AL_CONTEXT _stop_worker_threads_event_handle);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "AP	: _tester_testee_stop_thread : Waiting for worker threads.");
   al_wait_on_semaphore(AL_CONTEXT _stop_worker_threads_semaphore_handle, AL_WAIT_TIMEOUT_INFINITE);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "AP	: _tester_testee_stop_thread : Analysis process stopped.");
   al_destroy_semaphore(AL_CONTEXT _stop_worker_threads_semaphore_handle);

   _tester_testee_setup_interfaces_for_single_link_test(AL_CONTEXT);

   p = packet;

   memcpy(p, lqcp_tester_testee_stop_res_packet, sizeof(lqcp_tester_testee_stop_res_packet));
   p += sizeof(lqcp_tester_testee_stop_res_packet);

   memcpy(p, &(_DS_NET_IF->config.hw_addr.bytes), 6);
   p += 6;
   _tester_testee_send_imcp_packet(AL_CONTEXT NULL, packet, p - packet + 15);

   SET_LQCP_STATE(LQCP_STATE_WAITING)

   return 0;
}


int _tester_testee_reboot_thread(AL_CONTEXT_PARAM_DECL al_thread_param_t *param)
{
   unsigned char packet[2400];
   unsigned char lqcp_tester_testee_reset_res_packet[] = { 'L', 'Q', 'C', 'P', 1, 0, LQCP_SNIP_PACKET_TYPE_TESTER_TESTEE_RESET_RESPONSE };
   unsigned char lqcp_tester_testee_reset_req_packet[] = { 'L', 'Q', 'C', 'P', 1, 0, LQCP_SNIP_PACKET_TYPE_TESTER_TESTEE_RESET_REQUEST };
   unsigned char *p;

   al_set_thread_name(AL_CONTEXT "reboot_thread");

   if (GET_LQCP_STATE == LQCP_STATE_STARTED)
   {
      al_set_event(AL_CONTEXT _stop_worker_threads_event_handle);
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "AP	: access_point_tester_testee_reboot_thread : Waiting for worker threads.");
      al_wait_on_semaphore(AL_CONTEXT _stop_worker_threads_semaphore_handle, AL_WAIT_TIMEOUT_INFINITE);
      al_destroy_semaphore(AL_CONTEXT _stop_worker_threads_semaphore_handle);
   }

   al_set_event(AL_CONTEXT globals.queue_event_handle);
   al_set_event(AL_CONTEXT _stop_packet_receiver_event_handle);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "AP	: Tester/Testee mode system rebooting");

   /*  Send reboot request to peer */
   p = packet;
   memcpy(p, lqcp_tester_testee_reset_req_packet, sizeof(lqcp_tester_testee_reset_res_packet));
   p += sizeof(lqcp_tester_testee_reset_res_packet);

   memcpy(p, &(tester_testee_data.peer_mac_address.bytes), 6);
   p += 6;
   _tester_testee_send_imcp_packet(AL_CONTEXT NULL, packet, p - packet + 15);

   /* Send reboot response */
   p = packet;
   memcpy(p, lqcp_tester_testee_reset_res_packet, sizeof(lqcp_tester_testee_reset_res_packet));
   p += sizeof(lqcp_tester_testee_reset_res_packet);

   memcpy(p, &(_DS_NET_IF->config.hw_addr.bytes), 6);
   p += 6;
   _tester_testee_send_imcp_packet(AL_CONTEXT NULL, packet, p - packet + 15);

   al_reboot_board(AL_CONTEXT_SINGLE);

   return 0;
}


int _tester_testee_analysis_thread(AL_CONTEXT_PARAM_DECL al_thread_param_t *param)
{
   int           ret, i;
   unsigned char packet[2400];
   unsigned char lqcp_signature[] = { 'L', 'Q', 'C', 'P', 1, 0, LQCP_SNIP_PACKET_TYPE_TESTER_TESTEE_ANALYSIS };
   unsigned char *p;
   unsigned int  wait_time = tester_testee_data.analysis_interval * 1000;

   al_set_thread_name(AL_CONTEXT "analyser_thread");

   while (1)
   {
      ret = al_wait_on_event(AL_CONTEXT _stop_worker_threads_event_handle, wait_time);
      if (ret == AL_WAIT_STATE_SIGNAL)
      {
         al_release_semaphore(AL_CONTEXT _stop_worker_threads_semaphore_handle, 1);
         return 0;
      }

      p = packet;
      memcpy(p, lqcp_signature, 7);
      p += 7;
      memcpy(p, &(_DS_NET_IF->config.hw_addr.bytes), 6);
      p += 6;

      if (_active_interface_index == LQCP_TEST_ALL_INTERFACES)
      {
         *p = 4; /* interface count currently hard coded */ p += 1;

         for (i = 2; i < 6; i++)
         {
            memcpy(p, &tester_testee_data.if_info[i].channel, 1);
            p += 1;
            memcpy(p, &tester_testee_data.if_info[i].peer_rssi, 4);
            p += 4;
            memcpy(p, &tester_testee_data.if_info[i].rssi, 4);
            p += 4;
            memcpy(p, &tester_testee_data.if_info[i].peer_bit_rate, 4);
            p += 4;
            memcpy(p, &tester_testee_data.if_info[i].bit_rate, 4);
            p += 4;
         }
      }
      else
      {
         *p = 1; /* interface count currently hard coded */ p += 1;

         memcpy(p, &tester_testee_data.if_info[_active_interface_index].channel, 1);
         p += 1;
         memcpy(p, &tester_testee_data.if_info[_active_interface_index].peer_rssi, 4);
         p += 4;
         memcpy(p, &tester_testee_data.if_info[_active_interface_index].rssi, 4);
         p += 4;
         memcpy(p, &tester_testee_data.if_info[_active_interface_index].peer_bit_rate, 4);
         p += 4;
         memcpy(p, &tester_testee_data.if_info[_active_interface_index].bit_rate, 4);
         p += 4;
      }

      /*
       *	broadcast analysis broadcast packet on all interfaces
       */

      _tester_testee_send_imcp_packet(AL_CONTEXT NULL, packet, p - packet + 15);
   }

   return 0;
}


static int _testee_ap_thread(AL_CONTEXT_PARAM_DECL al_thread_param_t *param)
{
   al_net_if_t   *net_if;
   unsigned char lqcp_tester_data_req_packet[] =
   {
      'L',  'Q',  'C',  'P',     1, 0, LQCP_SNIP_PACKET_TYPE_TESTER_DATA_REQUEST,
      0x00, 0x00, 0x00, 0x00, 0x00, 0x00
   };
   int ret;
   tester_testee_if_info_t *if_info;

   net_if = (al_net_if_t *)param->param;
   if (net_if == NULL)
   {
      return -1;
   }

   if_info = (tester_testee_if_info_t *)net_if->config.ext_config_info;

   /*
    *	start sending packets to parent
    *
    */

   while (1)
   {
      ret = al_wait_on_event(_stop_worker_threads_event_handle, LQCP_PACKET_SEND_INTERVAL);
      if (ret == AL_WAIT_STATE_SIGNAL)
      {
         //al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "AP : %s _testee_sta_thread : quitting",if_info->name);
         al_release_semaphore(AL_CONTEXT _stop_worker_threads_semaphore_handle, 1);
         return 0;
      }

      memcpy(&lqcp_tester_data_req_packet[7], tester_testee_data.peer_mac_address.bytes, 6);
      _tester_testee_send_imcp_packet(AL_CONTEXT net_if, lqcp_tester_data_req_packet, sizeof(lqcp_tester_data_req_packet));
   }

   return 0;
}


static int _tester_testee_process_auth(AL_CONTEXT_PARAM_DECL al_net_if_t *al_net_if, al_net_addr_t *sta_addr)
{
   access_point_sta_entry_t *sta_entry;

   AL_ASSERT("al_process_auth", al_net_if != NULL);
   AL_ASSERT("al_process_auth", sta_addr != NULL);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "AP  : authentication response for "AL_NET_ADDR_STR, AL_NET_ADDR_TO_STR(sta_addr));

   sta_entry = access_point_add_sta_ex(AL_CONTEXT al_net_if, sta_addr, 1);

   sta_entry->sta.state = ACCESS_POINT_STA_STATE_AUTHENTICATED;
   sta_entry->sta.last_packet_received = al_get_tick_count(AL_CONTEXT_SINGLE);

   return 0;
}


static int _tester_testee_process_assoc(AL_CONTEXT_PARAM_DECL al_net_if_t *al_net_if,
                                        al_net_addr_t                     *sta_addr,
                                        unsigned short                    capability,
                                        int                               ie_count,
                                        al_802_11_information_element_t   *ie,
                                        unsigned char                     *imcp_key_ie_valid_out,
                                        al_802_11_information_element_t   *imcp_key_ie_out)
{
   access_point_sta_entry_t *sta;
   int                i;
   int                rsn_parsed;
   al_802_11_rsn_ie_t rsn_ie;
   int                md_ie_found;
   al_802_11_md_ie_t  md_ie;

   int phy_mode;
   tester_testee_if_info_t *if_info;
   al_802_11_operations_t  *extended_operations;
   int ret;
   al_802_11_information_element_t *ie_essid;

/*
 *      char								net_if_essid[AL_802_11_ESSID_MAX_SIZE + 1];
 *      int									net_if_essid_length;
 */

   AL_ASSERT("al_process_assoc", al_net_if != NULL);
   AL_ASSERT("al_process_assoc", sta_addr != NULL);


   ie_essid            = NULL;
   extended_operations = al_net_if->get_extended_operations(al_net_if);
   if_info             = (tester_testee_if_info_t *)al_net_if->config.ext_config_info;
   sta = access_point_get_sta_ex(AL_CONTEXT sta_addr, 1);

   if (sta == NULL)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "AP  : associating sta over %s was NULL", al_net_if->name);
      return -1;
   }
   else
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "AP  : sta associated over %s "AL_NET_ADDR_STR, al_net_if->name, AL_NET_ADDR_TO_STR(sta_addr));
   }

   rsn_parsed  = 0;
   md_ie_found = 0;

   for (i = 0; i < ie_count; i++)
   {
      if (((ie[i].element_id == AL_802_11_EID_RSN) ||
           (ie[i].element_id == AL_802_11_EID_VENDOR_PRIVATE)) &&
          !rsn_parsed)
      {
         ret = al_802_11_parse_rsn_ie(AL_CONTEXT & ie[i], &rsn_ie);
         al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "AP  : al_802_11_parse_rsn_ie returned %d", ret);
         if (ret == 0)
         {
            rsn_parsed                 = 1;
            sta->sta.auth_state       |= ACCESS_POINT_STA_AUTH_STATE_RSN;
            sta->sta.rsn_ie.element_id = ie[i].element_id;
            sta->sta.rsn_ie.length     = ie[i].length;
            memcpy(sta->sta.rsn_ie.data, ie[i].data, ie[i].length);
            al_802_11_cleanup_rsn_ie(AL_CONTEXT & rsn_ie);
         }
      }

      if ((ie[i].element_id == AL_802_11_EID_VENDOR_PRIVATE) &&
          !md_ie_found)
      {
         ret = al_802_11_parse_md_ie(AL_CONTEXT & ie[i], &md_ie);
         al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "AP  : al_802_11_parse_md_ie returned %d", ret);
         if ((ret == 0) &&
             (md_ie.type == AL_802_11_MD_IE_TYPE_ASSOC_REQUEST))
         {
            md_ie_found      = 1;
            sta->sta.is_imcp = 1;
         }
      }

      if ((ie[i].element_id == AL_802_11_EID_SSID) && (ie_essid == NULL))
      {
         ie_essid = &ie[i];

         if (ie_essid->length <= 0)
         {
            al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "AP  : Invalid ESSID IE length %d for "AL_NET_ADDR_STR " associating sta over %s", ie_essid->length, AL_NET_ADDR_TO_STR(sta_addr), al_net_if->name);
            access_point_release_sta_ex2(AL_CONTEXT sta, 0, 0, 1);
            return -1;
         }

/*
 *                      memset(net_if_essid,0,AL_802_11_ESSID_MAX_SIZE + 1);
 *                      extended_operations->get_essid(al_net_if,net_if_essid,AL_802_11_ESSID_MAX_SIZE);
 *                      net_if_essid_length			= strlen(net_if_essid);
 *
 *                      if(memcmp(net_if_essid,ie_essid->data,ie_essid->length)) {
 *
 *                              al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"AP  : invalid essid %s for associating sta over %s",ie_essid->data,al_net_if->name);
 *                              access_point_release_sta_ex2(AL_CONTEXT sta,0,0,1);
 *                              return -1;
 *                      }
 */
      }
   }

/*
 *      if(sta->sta.is_imcp == 1 && !AL_NET_ADDR_EQUAL(sta_addr,&(if_info->link_mac_address))) {
 *
 *              al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"AP  : sta_addr != link_address "AL_NET_ADDR_STR"!="AL_NET_ADDR_STR,AL_NET_ADDR_TO_STR(sta_addr),AL_NET_ADDR_TO_STR(&(if_info->link_mac_address)));
 *              access_point_release_sta_ex2(AL_CONTEXT sta,0,0,1);
 *              return -1;
 *      }
 */
   sta->sta.capability = capability;

   if (sta->sta.state == ACCESS_POINT_STA_STATE_ASSOCIATED)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "AP  : associating STA over %s was already associated over %s", al_net_if->name, sta->sta.net_if->name);

      sta->sta.last_packet_received = al_get_tick_count(AL_CONTEXT_SINGLE);
      ret = sta->sta.aid;

      access_point_release_sta_ex2(AL_CONTEXT sta, 0, 0, 1);

      return ret;
   }

   if ((globals.ap_state == ACCESS_POINT_STATE_STARTING) && sta->sta.is_imcp)
   {
      access_point_release_sta_ex2(AL_CONTEXT sta, 0, 0, 1);
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "AP  : ACCESS_POINT_STATE_STARTING returning -1");
      return -1;
   }

   phy_mode                      = extended_operations->get_phy_mode(al_net_if);
   sta->sta.state                = ACCESS_POINT_STA_STATE_ASSOCIATED;
   sta->sta.association_time     = al_get_tick_count(AL_CONTEXT_SINGLE);
   sta->sta.last_packet_received = al_get_tick_count(AL_CONTEXT_SINGLE);
   sta->sta.aid                  = access_point_sta_get_aid(AL_CONTEXT al_net_if);
   ret = sta->sta.aid;

   if (!(capability & AL_802_11_CAPABILITY_SHORT_SLOT_TIME) &&
       ((phy_mode == AL_802_11_PHY_MODE_G) || (phy_mode == AL_802_11_PHY_MODE_BG)))
   {
      /* found b client */

      sta->sta.b_client = 1;

      if (++if_info->long_slot_time_count == 1)
      {
         extended_operations->set_slot_time_type(al_net_if, AL_802_11_SLOT_TIME_TYPE_LONG);

         if (!(capability & AL_802_11_CAPABILITY_SHORT_PREAMBLE))
         {
            if (++if_info->long_preamble_count == 1)
            {
               extended_operations->set_preamble_type(al_net_if, AL_802_11_PREAMBLE_TYPE_LONG);
               extended_operations->set_erp_info(al_net_if,
                                                 AL_802_11_ERP_INFO_NON_ERP_PRESENT
                                                 | AL_802_11_ERP_INFO_USE_PROTECTION
                                                 | AL_802_11_ERP_INFO_BARKER_PREAMBLE_MODE);
            }
         }
         else
         {
            extended_operations->set_erp_info(al_net_if,
                                              AL_802_11_ERP_INFO_NON_ERP_PRESENT
                                              | AL_802_11_ERP_INFO_USE_PROTECTION);
         }
      }

      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,
                   "AP  : 802.11b %s STA "AL_NET_ADDR_STR " associated over %s with essid : %s",
                   rsn_parsed ? "RSN" : md_ie_found ? "IMCP" : "",
                   AL_NET_ADDR_TO_STR(sta_addr),
                   al_net_if->name,
                   ie_essid->data);
   }
   else
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,
                   "AP  : %s STA "AL_NET_ADDR_STR " associated over %s with essid : %s",
                   rsn_parsed ? "RSN" : md_ie_found ? "IMCP" : "",
                   AL_NET_ADDR_TO_STR(sta_addr),
                   al_net_if->name,
                   ie_essid->data);
   }

   access_point_release_sta_ex2(AL_CONTEXT sta, 0, 0, 1);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "AP : tester_testee_assoc_handler returning %d", ret);
   return ret;
}


static int _testee_sta_thread(AL_CONTEXT_PARAM_DECL al_thread_param_t *param)
{
   al_net_if_t             *net_if;
   tester_testee_if_info_t *if_info;
   al_802_11_operations_t  *operations;
   int               ret, flags;
   al_802_11_md_ie_t md_ie_in;
   al_802_11_md_ie_t md_ie_out;
   unsigned char     lqcp_tester_data_req_packet[] =
   {
      'L',  'Q',  'C',  'P',     1, 0, LQCP_SNIP_PACKET_TYPE_TESTER_DATA_REQUEST,
      0x00, 0x00, 0x00, 0x00, 0x00, 0x00
   };

   char *link_essid;

   al_set_thread_name(AL_CONTEXT "testee_sta_thread");

   net_if = (al_net_if_t *)param->param;

   if (net_if == NULL)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "AP		: testee_sta_thread : net_if = NULL");
      al_release_semaphore(AL_CONTEXT _stop_worker_threads_semaphore_handle, 1);
      return 0;
   }

   if_info    = (tester_testee_if_info_t *)net_if->config.ext_config_info;
   operations = (al_802_11_operations_t *)net_if->get_extended_operations(net_if);

   if (_active_interface_index == LQCP_TEST_ALL_INTERFACES)
   {
      link_essid = if_info->essid;
   }
   else
   {
      link_essid = _single_link_essid;
   }

   /*
    *	Associate to predefined address
    */

   memset(&md_ie_in, 0, sizeof(al_802_11_md_ie_t));
   memset(&md_ie_out, 0, sizeof(al_802_11_md_ie_t));

_associate:

   if_info->peer_rssi     = 0;
   if_info->rssi          = 0;
   if_info->peer_bit_rate = 0;
   if_info->bit_rate      = 0;

   while (1)
   {
      ret = al_wait_on_event(_stop_worker_threads_event_handle, LQCP_ASSOCIATION_RETRY_INTERVAL);
      if (ret == AL_WAIT_STATE_SIGNAL)
      {
         al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "AP : %s testee_sta_thread : quitting", if_info->name);
         al_release_semaphore(AL_CONTEXT _stop_worker_threads_semaphore_handle, 1);
         return 0;
      }

      md_ie_in.type = AL_802_11_MD_IE_TYPE_ASSOC_REQUEST;
      md_ie_in.data.assoc_request.assoc_type = AL_802_11_MD_IE_ASSOCIATION_TYPE_FINAL;

      if (!operations->associate(net_if, &if_info->link_mac_address, if_info->channel, link_essid, (int)strlen(link_essid),
                                 2000, &md_ie_in, &md_ie_out))
      {
         al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "AP : testee_sta_thread : %s associated to "AL_NET_ADDR_STR, if_info->name, AL_NET_ADDR_TO_STR(&if_info->link_mac_address));
         break;
      }
      else
      {
         al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "AP : testee_sta_thread : %s association failed for "AL_NET_ADDR_STR, if_info->name, AL_NET_ADDR_TO_STR(&if_info->link_mac_address));
      }
   }


   /*
    *	start sending packets to parent
    *
    */

   while (1)
   {
      ret = al_wait_on_event(_stop_worker_threads_event_handle, 100);
      if (ret == AL_WAIT_STATE_SIGNAL)
      {
         al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "AP : %s testee_sta_thread : quitting", if_info->name);
         al_release_semaphore(AL_CONTEXT _stop_worker_threads_semaphore_handle, 1);
         return 0;
      }

      flags = net_if->get_flags(net_if);
      if (!(flags & AL_NET_IF_FLAGS_LINKED) || !(flags & AL_NET_IF_FLAGS_UP) || !(flags & AL_NET_IF_FLAGS_ASSOCIATED))
      {
         al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "AP : testee_sta_thread : %s disassociated from "AL_NET_ADDR_STR, if_info->name, AL_NET_ADDR_TO_STR(&if_info->link_mac_address));
         goto _associate;
      }

      memcpy(&lqcp_tester_data_req_packet[7], tester_testee_data.peer_mac_address.bytes, 6);
      _tester_testee_send_imcp_packet(AL_CONTEXT net_if, lqcp_tester_data_req_packet, sizeof(lqcp_tester_data_req_packet));
   }

   return 0;
}


static int _tester_sta_thread(AL_CONTEXT_PARAM_DECL al_thread_param_t *param)
{
   al_net_if_t             *net_if;
   tester_testee_if_info_t *if_info;
   al_802_11_operations_t  *operations;
   int               ret;
   al_802_11_md_ie_t md_ie_in;
   al_802_11_md_ie_t md_ie_out;
   int               flags;

   al_set_thread_name(AL_CONTEXT "tester_sta_thread");

   net_if = (al_net_if_t *)param->param;

   if (net_if == NULL)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "AP		: _tester_sta_thread : net_if = NULL");
      al_release_semaphore(AL_CONTEXT _stop_worker_threads_semaphore_handle, 1);
      return 0;
   }

   if_info    = (tester_testee_if_info_t *)net_if->config.ext_config_info;
   operations = (al_802_11_operations_t *)net_if->get_extended_operations(net_if);

   /*
    *	Associate to predefined address
    */

   memset(&md_ie_in, 0, sizeof(al_802_11_md_ie_t));
   memset(&md_ie_out, 0, sizeof(al_802_11_md_ie_t));

   while (1)
   {
      ret = al_wait_on_event(_stop_worker_threads_event_handle, LQCP_ASSOCIATION_RETRY_INTERVAL);
      if (ret == AL_WAIT_STATE_SIGNAL)
      {
         al_release_semaphore(AL_CONTEXT _stop_worker_threads_semaphore_handle, 1);
         al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "AP : %s tester_sta_thread : quitting", if_info->name);
         return 0;
      }

      flags = net_if->get_flags(net_if);
      if ((flags & AL_NET_IF_FLAGS_LINKED) && (flags & AL_NET_IF_FLAGS_UP) && (flags & AL_NET_IF_FLAGS_ASSOCIATED))
      {
         continue;
      }

      md_ie_in.type = AL_802_11_MD_IE_TYPE_ASSOC_REQUEST;
      md_ie_in.data.assoc_request.assoc_type = AL_802_11_MD_IE_ASSOCIATION_TYPE_FINAL;

      if (!operations->associate(net_if, &if_info->link_mac_address, if_info->channel, if_info->essid, (int)strlen(if_info->essid),
                                 2000, &md_ie_in, &md_ie_out))
      {
         al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "AP : tester_sta_thread : %s associated to "AL_NET_ADDR_STR, if_info->name, AL_NET_ADDR_TO_STR(&if_info->link_mac_address));
      }
      else
      {
         al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "AP : tester_sta_thread : %s association failed for "AL_NET_ADDR_STR, if_info->name, AL_NET_ADDR_TO_STR(&if_info->link_mac_address));
      }
   }

   return 0;
}


static int _tester_testee_setup_interfaces_for_all_link_test(AL_CONTEXT_PARAM_DECL)
{
   int                       i, j;
   al_net_if_t               *net_if;
   char                      buf[256];
   int                       al_net_if_count;
   tester_testee_if_info_t   *if_info;
   int                       phy_mode;
   al_802_11_operations_t    *operations;
   al_802_11_tx_power_info_t power_info;
   int                       a_channel_index;
   int                       bg_channel_index;
   int                       channel = 0;

   AL_PRINT_LOG_FLOW_0("MESH_AP	: _tester_testee_setup_interfaces_for_all_link_test()");

   al_net_if_count = al_get_net_if_count(AL_CONTEXT_SINGLE);

   a_channel_index  = 0;
   bg_channel_index = 0;

   for (i = 0; i < tester_testee_data.if_count - 1; i++)
   {
      if_info = &tester_testee_data.if_info[i];

      for (j = 0; j < al_net_if_count; j++)
      {
         net_if = al_get_net_if(AL_CONTEXT j);

         if (net_if == NULL)
         {
            continue;
         }
         else
         {
            net_if->get_name(net_if, buf, 255);
         }

         if (!strcmp(if_info->name, buf))
         {
            net_if->config.ext_config_info = if_info;
            if_info->net_if = net_if;
            break;
         }
      }

      if (if_info->net_if == NULL)
      {
         continue;
      }

      if (AL_NET_IF_IS_ETHERNET(if_info->net_if))
      {
         if_info->net_if->set_filter_mode(if_info->net_if, if_info->mode);
      }
      else if (AL_NET_IF_IS_WIRELESS(if_info->net_if))
      {
         operations = if_info->net_if->get_extended_operations(if_info->net_if);

         AL_ASSERT("_tester_testee_setup_interfaces_for_all_link_test", operations != NULL);

         operations->set_assoc_hook(if_info->net_if, _tester_testee_process_assoc);
         operations->set_auth_hook(if_info->net_if, _tester_testee_process_auth);

         operations->set_mode(if_info->net_if, if_info->mode, 0);
         operations->set_essid(if_info->net_if, if_info->essid);
         operations->set_rts_threshold(if_info->net_if, if_info->rts_threshold);
         operations->set_frag_threshold(if_info->net_if, if_info->frag_threshold);
         operations->set_beacon_interval(if_info->net_if, if_info->beacon_interval);

         phy_mode = operations->get_phy_mode(if_info->net_if);

         if (phy_mode == AL_802_11_PHY_MODE_A)
         {
            if (tester_testee_data.ap_mode == ACCESS_POINT_MODE_TESTEE)
            {
               if (i == INDEX_WLAN0)
               {
                  channel = a_channels[a_channel_index + 1];
               }
               else if (i == INDEX_WLAN1)
               {
                  channel = a_channels[a_channel_index - 1];
               }
               else
               {
                  channel = a_channels[a_channel_index];
               }
            }
            else
            {
               channel = a_channels[a_channel_index];
            }

            a_channel_index++;
         }
         else if (phy_mode == AL_802_11_PHY_MODE_BG)
         {
            if (tester_testee_data.ap_mode == ACCESS_POINT_MODE_TESTEE)
            {
               if (i == INDEX_WLAN0)
               {
                  channel = bg_channels[bg_channel_index + 1];
               }
               else if (i == INDEX_WLAN1)
               {
                  channel = bg_channels[bg_channel_index - 1];
               }
               else
               {
                  channel = bg_channels[bg_channel_index];
               }
            }
            else
            {
               channel = bg_channels[bg_channel_index];
            }

            bg_channel_index++;
         }

         operations->set_channel(if_info->net_if, channel);
         power_info.power = if_info->tx_power;
         operations->set_tx_power(if_info->net_if, &power_info);

         operations->set_bit_rate(if_info->net_if, if_info->tx_rate);
      }
   }

   return 0;
}


static int _tester_testee_setup_interfaces_for_single_link_test(AL_CONTEXT_PARAM_DECL)
{
   int                       i, j;
   al_net_if_t               *net_if;
   char                      buf[256];
   int                       al_net_if_count;
   tester_testee_if_info_t   *if_info;
   int                       phy_mode;
   al_802_11_operations_t    *operations;
   al_802_11_tx_power_info_t power_info;

   AL_PRINT_LOG_FLOW_0("MESH_AP	: _tester_testee_setup_interfaces_for_single_link_test()");

   al_net_if_count = al_get_net_if_count(AL_CONTEXT_SINGLE);

   for (i = 0; i < al_net_if_count; i++)
   {
      net_if = al_get_net_if(AL_CONTEXT i);

      if (net_if == NULL)
      {
         continue;
      }
      else
      {
         net_if->get_name(net_if, buf, 255);
      }

      for (j = 0; j < tester_testee_data.if_count - 1; j++)
      {
         if (!strcmp(tester_testee_data.if_info[j].name, buf))
         {
            net_if->config.ext_config_info       = &tester_testee_data.if_info[j];
            tester_testee_data.if_info[j].net_if = net_if;
            break;
         }
      }

      if_info = (tester_testee_if_info_t *)net_if->config.ext_config_info;

      if (if_info == NULL)
      {
         continue;
      }


      if (AL_NET_IF_IS_ETHERNET(net_if))
      {
         net_if->set_filter_mode(net_if, if_info->mode);
      }
      else if (AL_NET_IF_IS_WIRELESS(net_if))
      {
         operations = net_if->get_extended_operations(net_if);

         AL_ASSERT("_tester_testee_setup_interfaces_for_single_link_test", operations != NULL);

         operations->set_assoc_hook(net_if, _tester_testee_process_assoc);
         operations->set_auth_hook(net_if, _tester_testee_process_auth);
         operations->set_mode(net_if, if_info->single_link_mode, 0);
         operations->set_essid(net_if, if_info->single_link_essid);
         operations->set_rts_threshold(net_if, if_info->rts_threshold);
         operations->set_frag_threshold(net_if, if_info->frag_threshold);
         operations->set_beacon_interval(net_if, if_info->beacon_interval);

         phy_mode = operations->get_phy_mode(net_if);

         if (phy_mode == AL_802_11_PHY_MODE_A)
         {
            operations->set_channel(net_if, _get_next_a_channel());
         }
         else if (phy_mode == AL_802_11_PHY_MODE_BG)
         {
            operations->set_channel(net_if, _get_next_bg_channel());
         }

         power_info.power = if_info->tx_power;
         operations->set_tx_power(net_if, &power_info);

         operations->set_bit_rate(net_if, if_info->tx_rate);
      }
   }

   return 0;
}


static al_net_if_t *_get_net_if_by_name(char *if_name)
{
   int         net_if_count;
   al_net_if_t *net_if;
   int         i;
   char        buf[256];

   net_if_count = al_get_net_if_count(AL_CONTEXT_SINGLE);

   for (i = 0; i < net_if_count; i++)
   {
      net_if = al_get_net_if(AL_CONTEXT i);

      if (net_if == NULL)
      {
         continue;
      }
      else
      {
         net_if->get_name(net_if, buf, 255);
         if (!strcmp(if_name, buf))
         {
            return net_if;
         }
      }
   }

   return NULL;
}


static int _tester_testee_start_all_interface_test()
{
   int                     i;
   al_net_if_t             *net_if;
   tester_testee_if_info_t *if_info;
   int                     worker_thread_count;


   AL_PRINT_LOG_FLOW_0("MESH_AP	: _tester_testee_setup_interfaces_for_all_link_test()");

   worker_thread_count = 0;

   for (i = 0; i < tester_testee_data.if_count - 1; i++)
   {
      if_info = (tester_testee_if_info_t *)&tester_testee_data.if_info[i];

      if (if_info == NULL)
      {
         continue;
      }

      net_if = if_info->net_if;

      if (!AL_NET_IF_IS_WIRELESS(net_if))
      {
         continue;
      }

      if (tester_testee_data.ap_mode == ACCESS_POINT_MODE_TESTER)
      {
         if (if_info->mode == AL_802_11_MODE_INFRA)
         {
            al_create_thread(AL_CONTEXT _tester_sta_thread, net_if, AL_THREAD_PRIORITY_NORMAL, "_tester_sta_thread");
            worker_thread_count++;
         }
      }
      else if (tester_testee_data.ap_mode == ACCESS_POINT_MODE_TESTEE)
      {
         if (if_info->mode == AL_802_11_MODE_INFRA)
         {
            al_create_thread(AL_CONTEXT _testee_sta_thread, net_if, AL_THREAD_PRIORITY_NORMAL, "_testee_sta_thread");
         }
         else
         {
            al_create_thread(AL_CONTEXT _testee_ap_thread, net_if, AL_THREAD_PRIORITY_NORMAL, "_testee_ap_thread");
         }
         worker_thread_count++;
      }
   }

   return worker_thread_count;
}


static int _tester_testee_start_single_interface_test(int interface_index, int channel, int link_interface_index, char *essid)
{
   al_net_if_t             *net_if;
   tester_testee_if_info_t *if_info;
   al_802_11_operations_t  *operations;

   if_info = &tester_testee_data.if_info[interface_index];
   net_if  = if_info->net_if;

   if (net_if == NULL)
   {
      net_if = _get_net_if_by_name(if_info->name);
      if (net_if == NULL)
      {
         return -1;
      }
   }

   if (!AL_NET_IF_IS_WIRELESS(net_if))
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "AP : _tester_testee_start_interface_test : net_if not wireless");
      return -1;
   }

   operations = net_if->get_extended_operations(net_if);
   if (operations == NULL)
   {
      return -1;
   }

   if_info->channel = channel;

   operations->set_essid(net_if, essid);
   operations->set_channel(net_if, channel);

   if (tester_testee_data.ap_mode == ACCESS_POINT_MODE_TESTER)
   {
      operations->set_assoc_hook(net_if, _tester_testee_process_assoc);
      operations->set_auth_hook(net_if, _tester_testee_process_auth);
      operations->set_mode(net_if, AL_802_11_MODE_MASTER, 0);
   }
   else if (tester_testee_data.ap_mode == ACCESS_POINT_MODE_TESTEE)
   {
      operations->set_mode(net_if, AL_802_11_MODE_INFRA, 0);
      al_create_thread(AL_CONTEXT _testee_sta_thread, net_if, AL_THREAD_PRIORITY_NORMAL, "_testee_sta_thread");
      return 1;
   }

   return 0;
}


static int _process_start_analysis_request(al_packet_t *in_packet)
{
   int           interface_index, channel, link_interface_index;
   int           worker_thread_count, ret;
   unsigned char *p;
   unsigned char packet[2400];
   unsigned char lqcp_tester_start_res_packet[] = { 'L', 'Q', 'C', 'P', 1, 0, LQCP_SNIP_PACKET_TYPE_TESTER_TESTEE_MODE_START_RES };

   p = in_packet->buffer + in_packet->position + 41;      /* skip ip header,UDP header,LQCP header,packet_type,ds mac address */

   interface_index = (int)*p;
   p++;
   channel = (int)*p;
   p++;
   link_interface_index = (int)*p;
   p++;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "AP : tester_testee_receive_thread : if_index = %d,channel = %d,link_if_index = %d", interface_index, channel, link_interface_index);

   _active_interface_index = interface_index;

   worker_thread_count = 0;

   if (interface_index == LQCP_TEST_ALL_INTERFACES)
   {
      /* run test on all interfaces */

      _tester_testee_setup_interfaces_for_all_link_test(AL_CONTEXT);
      _initialize_interface_link_mac_address_for_all_link_test();

      ret = _tester_testee_start_all_interface_test();

      if (ret > 0)              /* threads have been started for this interface */
      {
         worker_thread_count += ret;
      }
   }
   else
   {
      /* run test on single interface */

      _tester_testee_setup_interfaces_for_single_link_test(AL_CONTEXT);
      _initialize_interface_link_mac_address_for_single_link_test(interface_index, link_interface_index);

      ret = _tester_testee_start_single_interface_test(interface_index, channel, link_interface_index, _single_link_essid);

      if (ret > 0)              /* threads have been started for this interface */
      {
         worker_thread_count += ret;
      }
   }

   if (tester_testee_data.ap_mode == ACCESS_POINT_MODE_TESTEE)
   {
      worker_thread_count++;
      al_create_thread_on_cpu(AL_CONTEXT _tester_testee_analysis_thread, NULL,0, "_tester_testee_analysis_thread");
   }

   _stop_worker_threads_semaphore_handle = al_create_semaphore(worker_thread_count, 0);

   if (worker_thread_count <= 0)
   {
      al_release_semaphore(AL_CONTEXT _stop_worker_threads_semaphore_handle, 1);
   }

   p = packet;
   memcpy(p, lqcp_tester_start_res_packet, sizeof(lqcp_tester_start_res_packet));
   p = packet + sizeof(lqcp_tester_start_res_packet);

   memcpy(p, &(_DS_NET_IF->config.hw_addr.bytes), 6);
   p += 6;
   _tester_testee_send_imcp_packet(AL_CONTEXT NULL, packet, p - packet + 15);

   SET_LQCP_STATE(LQCP_STATE_STARTED)

   return 0;
}


static int _process_analysis_packet(al_packet_t *packet)
{
   unsigned char out_packet[2400];
   unsigned char *p;

   p = packet->buffer + packet->position + 28;      /* skip ip header,UDP header*/

   memcpy(out_packet, p, packet->data_length);
   _tester_testee_send_imcp_packet(AL_CONTEXT NULL, out_packet, packet->data_length);

   return 0;
}


static int _process_channel_switch_request(al_packet_t *packet)
{
   unsigned char          lqcp_channel_switch_res_packet[] = { 'L', 'Q', 'C', 'P', 1, 0, LQCP_SNIP_PACKET_TYPE_TESTER_TESTEE_CHANNELSWITCH_RESPONSE };
   unsigned char          lqcp_channel_switch_req_packet[] = { 'L', 'Q', 'C', 'P', 1, 0, LQCP_SNIP_PACKET_TYPE_TESTER_TESTEE_CHANNELSWITCH_REQUEST };
   unsigned char          out_packet[2400];
   unsigned char          *p;
   al_802_11_operations_t *ex_op;
   unsigned char          channel;
   al_net_if_t            *net_if = NULL;

   p       = packet->buffer + packet->position + 41; /* skip ip header,UDP header,LQCP header,packet_type,ds mac address */
   channel = *p;


   /*  Send channel switch request to peer */
   p = out_packet;
   memcpy(p, lqcp_channel_switch_req_packet, sizeof(lqcp_channel_switch_req_packet));
   p += sizeof(lqcp_channel_switch_req_packet);

   memcpy(p, &(tester_testee_data.peer_mac_address.bytes), 6);
   p += 6;
   *p = channel;
   _tester_testee_send_imcp_packet(AL_CONTEXT NULL, out_packet, p - out_packet + 15);


   /* Send channel switch response */
   p = out_packet;
   memcpy(p, lqcp_channel_switch_res_packet, sizeof(lqcp_channel_switch_res_packet));
   p += sizeof(lqcp_channel_switch_res_packet);

   memcpy(p, &(_DS_NET_IF->config.hw_addr.bytes), 6);
   p += 6;

   net_if = tester_testee_data.if_info[_active_interface_index].net_if;
   if (net_if == NULL)
   {
      *p = LQCP_CHANNEL_SWITCH_REQUEST_DENIED;
      p += 1;
      *p = 0;
      _tester_testee_send_imcp_packet(AL_CONTEXT NULL, out_packet, p - out_packet + 15);
      return -1;
   }

   ex_op = (al_802_11_operations_t *)net_if->get_extended_operations(net_if);
   if (ex_op == NULL)
   {
      *p = LQCP_CHANNEL_SWITCH_REQUEST_DENIED;
      p += 1;
      *p = 0;
      _tester_testee_send_imcp_packet(AL_CONTEXT NULL, out_packet, p - out_packet + 15);
      return -1;
   }

   *p = LQCP_CHANNEL_SWITCH_REQUEST_COMPLETED;
   p += 1;
   *p = channel;
   p += 1;
   _tester_testee_send_imcp_packet(AL_CONTEXT NULL, out_packet, p - out_packet + 15);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "AP	: _process_channel_switch_request channel switching to %d", channel);

   ex_op->set_channel(net_if, channel);
   channel = ex_op->get_channel(net_if);
   tester_testee_data.if_info[_active_interface_index].channel = channel;

   p--;
   *p = channel;
   p += 1;
   _tester_testee_send_imcp_packet(AL_CONTEXT NULL, out_packet, p - out_packet + 15);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "AP	: _process_channel_switch_request channel switched to %d", channel);
   return 0;
}


static int _process_tester_data_request(al_packet_t *packet, al_net_if_t *net_if)
{
   unsigned char          lqcp_tester_data_res_packet[] = { 'L', 'Q', 'C', 'P', 1, 0, LQCP_SNIP_PACKET_TYPE_TESTER_DATA_RESPONSE };
   unsigned char          out_packet[2400];
   unsigned char          *p;
   unsigned int           bit_rate;
   al_802_11_operations_t *ex_op;

   p = out_packet;

   memcpy(p, lqcp_tester_data_res_packet, sizeof(lqcp_tester_data_res_packet));
   p += sizeof(lqcp_tester_data_res_packet);

   memcpy(p, tester_testee_data.peer_mac_address.bytes, 6);
   p += 6;
   memcpy(p, &packet->rssi, 4);
   p += 4;

   ex_op = (al_802_11_operations_t *)net_if->get_extended_operations(net_if);
   if (ex_op == NULL)
   {
      return -1;
   }
   bit_rate = ex_op->get_bit_rate(net_if);

   memcpy(p, &bit_rate, 4);
   p += 4;

   _tester_testee_send_imcp_packet(AL_CONTEXT net_if, out_packet, p - out_packet + 15);

   return 0;
}


static int _process_tester_data_response(al_packet_t *packet, al_net_if_t *net_if)
{
   unsigned int            peer_rssi;
   int                     peer_bit_rate;
   unsigned char           *p;
   unsigned int            bit_rate;
   al_802_11_operations_t  *ex_op;
   tester_testee_if_info_t *if_info;

   if_info = (tester_testee_if_info_t *)net_if->config.ext_config_info;

   if (if_info == NULL)
   {
      return -1;
   }

   p = packet->buffer + packet->position + 41;      /* skip ip header,UDP header,LQCP header,packet_type,ds mac address */

   if_info->rssi = (if_info->rssi + packet->rssi) / 2;

   memcpy(&peer_rssi, p, 4);
   if_info->peer_rssi = (if_info->peer_rssi + peer_rssi) / 2;
   p += 4;

   memcpy(&peer_bit_rate, p, 4);
   if_info->peer_bit_rate = (if_info->bit_rate + peer_bit_rate) / 2;

   ex_op = (al_802_11_operations_t *)net_if->get_extended_operations(net_if);
   if (ex_op == NULL)
   {
      return -1;
   }
   bit_rate          = ex_op->get_bit_rate(net_if);
   if_info->bit_rate = (if_info->bit_rate + bit_rate) / 2;

   return 0;
}


static int _tester_testee_receive_thread(AL_CONTEXT_PARAM_DECL al_thread_param_t *param)
{
   int           ret;
   al_packet_t   *packet;
   al_net_if_t   *net_if;
   unsigned char *p;
   unsigned char *buf;
   unsigned char packet_type;


   AL_PRINT_LOG_FLOW_0("AP		: ap_tester_thread()");
   al_set_thread_name(AL_CONTEXT "analyser_receive_thread");

   while (1)
   {
      al_reset_event(AL_CONTEXT globals.queue_event_handle);
      ret = al_wait_on_event(AL_CONTEXT globals.queue_event_handle, AL_WAIT_TIMEOUT_INFINITE);
      ret = al_wait_on_event(_stop_packet_receiver_event_handle, 0);

      if (ret == AL_WAIT_STATE_SIGNAL)
      {
         al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "AP	: _tester_testee_receive_thread  breaking 1");
         break;
      }

_next_packet:

      packet = globals.ap_queue->queue_remove_packet(AL_CONTEXT globals.ap_queue, &net_if);
      if (packet == NULL)
      {
         ret = al_wait_on_event(_stop_packet_receiver_event_handle, 0);
         if (ret == AL_WAIT_STATE_SIGNAL)
         {
            al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "AP	: _tester_testee_receive_thread  breaking 2");
            break;
         }
         continue;
      }

      ret = _packet_is_lqcp_udp_ip(AL_CONTEXT packet);
      if (ret != 0)
      {
         al_release_packet(AL_CONTEXT packet);
         goto _next_packet;
      }

      buf = packet->buffer + packet->position;

      p = buf + 34;            /* IP HEADER = 20 + UDP HEADER = 8 + LQCP HEADER = 6 */

      packet_type = *p;
      p++;

/*
 *              if(packet_type == LQCP_SNIP_PACKET_TYPE_TESTER_TESTEE_ANALYSIS) {
 *
 *                      if(tester_testee_data.ap_mode != ACCESS_POINT_MODE_TESTER || GET_LQCP_STATE != LQCP_STATE_STARTED) {
 *                              al_release_packet(AL_CONTEXT packet);
 *                              goto _next_packet;
 *                      }
 *
 *                      if(memcmp(p,&(tester_testee_data.peer_mac_address.bytes),6)) {
 *                              al_release_packet(AL_CONTEXT packet);
 *                              goto _next_packet;
 *                      }
 *
 *                      _process_analysis_packet(packet);
 *                      goto _next_packet;
 *
 *              }
 */

      if (memcmp(p, &(_DS_NET_IF->config.hw_addr.bytes), 6))
      {
         al_release_packet(AL_CONTEXT packet);
         goto _next_packet;
      }
      else
      {
         p += 6;                        // skip ds mac address
      }
      if (packet_type == LQCP_SNIP_PACKET_TYPE_TESTER_DATA_REQUEST)
      {
         if ((tester_testee_data.ap_mode != ACCESS_POINT_MODE_TESTER) || (GET_LQCP_STATE != LQCP_STATE_STARTED))
         {
            al_release_packet(AL_CONTEXT packet);
            goto _next_packet;
         }

         _process_tester_data_request(packet, net_if);

         al_release_packet(AL_CONTEXT packet);
         goto _next_packet;
      }
      else if (packet_type == LQCP_SNIP_PACKET_TYPE_TESTER_DATA_RESPONSE)
      {
         if ((tester_testee_data.ap_mode != ACCESS_POINT_MODE_TESTEE) || (GET_LQCP_STATE != LQCP_STATE_STARTED))
         {
            al_release_packet(AL_CONTEXT packet);
            goto _next_packet;
         }

         _process_tester_data_response(packet, net_if);

         al_release_packet(AL_CONTEXT packet);
         goto _next_packet;
      }

      if (packet_type == LQCP_SNIP_PACKET_TYPE_TESTER_TESTEE_CHANNELSWITCH_REQUEST)
      {
         _process_channel_switch_request(packet);
         goto _next_packet;
      }
      else if (packet_type == LQCP_SNIP_PACKET_TYPE_TESTER_TESTEE_RESET_REQUEST)
      {
         SET_LQCP_STATE(LQCP_STATE_QUITTING)
         al_create_thread_on_cpu(AL_CONTEXT _tester_testee_reboot_thread, NULL,0, "_tester_testee_reboot_thread");
      }
      else if (packet_type == LQCP_SNIP_PACKET_TYPE_TESTER_TESTEE_MODE_START_REQ)
      {
         if (GET_LQCP_STATE == LQCP_STATE_STARTED)
         {
            al_release_packet(AL_CONTEXT packet);
            goto _next_packet;
         }

         _process_start_analysis_request(packet);
      }
      else if (packet_type == LQCP_SNIP_PACKET_TYPE_TESTER_TESTEE_MODE_STOP_REQ)
      {
         al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "AP	: _tester_testee_receive_thread  waiting 1");
         if (GET_LQCP_STATE != LQCP_STATE_STARTED)
         {
            al_release_packet(AL_CONTEXT packet);
            al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "AP	: _tester_testee_receive_thread  waiting 2");
            goto _next_packet;
         }

         al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "AP	: _tester_testee_receive_thread  waiting 3");
         al_create_thread_on_cpu(AL_CONTEXT _tester_testee_stop_thread, NULL,0, "_tester_testee_stop_thread");
         al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "AP	: _tester_testee_receive_thread  waiting 4");
      }

      al_release_packet(AL_CONTEXT packet);
      goto _next_packet;
   }

   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "AP		: Analyser receive thread stopped");

   return 0;
}


static int _start_link_quality_analysis()
{
   int           worker_thread_count, ret;
   unsigned char *p;
   unsigned char packet[2400];
   unsigned char lqcp_tester_start_res_packet[] = { 'L', 'Q', 'C', 'P', 1, 0, LQCP_SNIP_PACKET_TYPE_TESTER_TESTEE_MODE_START_RES };

   _active_interface_index = tester_testee_data.interface_index;

   worker_thread_count = 0;

   if (tester_testee_data.interface_index == LQCP_TEST_ALL_INTERFACES)
   {
      /* run test on all interfaces */

      _tester_testee_setup_interfaces_for_all_link_test(AL_CONTEXT);
      _initialize_interface_link_mac_address_for_all_link_test();

      ret = _tester_testee_start_all_interface_test();

      if (ret > 0)              /* threads have been started for this interface */
      {
         worker_thread_count += ret;
      }
   }
   else
   {
      /* run test on single interface */

      _tester_testee_setup_interfaces_for_single_link_test(AL_CONTEXT);
      _initialize_interface_link_mac_address_for_single_link_test(tester_testee_data.interface_index, tester_testee_data.link_interface_index);

      ret = _tester_testee_start_single_interface_test(tester_testee_data.interface_index, tester_testee_data.channel, tester_testee_data.link_interface_index, _single_link_essid);

      if (ret > 0)              /* threads have been started for this interface */
      {
         worker_thread_count += ret;
      }
   }

   if (tester_testee_data.ap_mode == ACCESS_POINT_MODE_TESTEE)
   {
      worker_thread_count += 2;
      al_create_thread_on_cpu(AL_CONTEXT _tester_testee_analysis_thread, NULL,0, "_tester_testee_analysis_thread");
   }

   _stop_worker_threads_semaphore_handle = al_create_semaphore(worker_thread_count, 0);

   if (worker_thread_count <= 0)
   {
      al_release_semaphore(AL_CONTEXT _stop_worker_threads_semaphore_handle, 1);
   }

   p = packet;
   memcpy(p, lqcp_tester_start_res_packet, sizeof(lqcp_tester_start_res_packet));
   p = packet + sizeof(lqcp_tester_start_res_packet);

   memcpy(p, &(_DS_NET_IF->config.hw_addr.bytes), 6);
   p += 6;
   _tester_testee_send_imcp_packet(AL_CONTEXT NULL, packet, p - packet + 15);

   SET_LQCP_STATE(LQCP_STATE_STARTED)

   return 0;
}


int access_point_tester_testee_start_thread(AL_CONTEXT_PARAM_DECL al_thread_param_t *param)
{
   unsigned char lqcp_tester_testee_mode_init_packet[] = { 'L', 'Q', 'C', 'P', 1, 0, LQCP_SNIP_PACKET_TYPE_TESTER_TESTEE_MODE_INIT };
   unsigned char packet[2400];
   unsigned char *p;

   AL_PRINT_LOG_FLOW_0("AP		: access_point_tester_testee_start_thread()");
   al_set_thread_name(AL_CONTEXT "ap_tester_testee_start");

   _tester_testee_setup_interfaces_for_single_link_test(AL_CONTEXT);

   _stop_worker_threads_event_handle  = al_create_event(AL_CONTEXT 0);
   _stop_packet_receiver_event_handle = al_create_event(AL_CONTEXT 0);

   al_create_thread_on_cpu(AL_CONTEXT _tester_testee_receive_thread, NULL,0, "_tester_testee_receive_thread");

   globals.ap_state             = ACCESS_POINT_STATE_STARTED;
   ap_enable_disable_processing = 1;

   al_thread_sleep(AL_CONTEXT 1000);

   SET_LQCP_STATE(LQCP_STATE_WAITING)

   if (tester_testee_data.ap_mode == ACCESS_POINT_MODE_TESTER)
   {
      AL_PRINT_LOG_MSG_0("AP		: -----------Access Point Tester started-----------");
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "AP		: : -----------Access Point Tester started-----------");
   }
   else
   {
      AL_PRINT_LOG_MSG_0("AP		: -----------Access Point Testee started-----------");
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "AP		: : -----------Access Point Testee started-----------");
   }

   _start_link_quality_analysis();

   p = packet;

   memcpy(p, lqcp_tester_testee_mode_init_packet, sizeof(lqcp_tester_testee_mode_init_packet));
   p += sizeof(lqcp_tester_testee_mode_init_packet);

   memcpy(p, &(_DS_NET_IF->config.hw_addr.bytes), 6);
   p += 6;
   _tester_testee_send_imcp_packet(AL_CONTEXT NULL, packet, p - packet + 15);

   return 0;
}


static int _get_next_mac_address(al_net_addr_t *mac_addr, int offset)
{
   int i, j;

   for (i = 0; i < offset; i++)
   {
      for (j = mac_addr->length - 1; j >= 0; j--)
      {
         if (++mac_addr->bytes[j] > 0)
         {
            break;
         }
      }
   }

   return 0;
}


static int _initialize_interface_link_mac_address_for_single_link_test(int interface_index, int link_interface_index)
{
   tester_testee_if_info_t *if_info;

   if_info = &tester_testee_data.if_info[interface_index];

   memcpy(&if_info->link_mac_address, &tester_testee_data.peer_mac_address, sizeof(al_net_addr_t));
   _get_next_mac_address(&if_info->link_mac_address, link_interface_index);

   return 0;
}


static int _initialize_interface_link_mac_address_for_all_link_test()
{
   memcpy(&tester_testee_data.if_info[INDEX_WLAN0].link_mac_address, &tester_testee_data.peer_mac_address, sizeof(al_net_addr_t));
   _get_next_mac_address(&tester_testee_data.if_info[INDEX_WLAN0].link_mac_address, INDEX_WLAN1);
   memcpy(&tester_testee_data.if_info[INDEX_WLAN0].wm_mac_address, &tester_testee_data.ds_mac_address, sizeof(al_net_addr_t));
   _get_next_mac_address(&tester_testee_data.if_info[INDEX_WLAN0].wm_mac_address, INDEX_WLAN0);

   memcpy(&tester_testee_data.if_info[INDEX_WLAN1].link_mac_address, &tester_testee_data.peer_mac_address, sizeof(al_net_addr_t));
   _get_next_mac_address(&tester_testee_data.if_info[INDEX_WLAN1].link_mac_address, INDEX_WLAN0);
   memcpy(&tester_testee_data.if_info[INDEX_WLAN1].wm_mac_address, &tester_testee_data.ds_mac_address, sizeof(al_net_addr_t));
   _get_next_mac_address(&tester_testee_data.if_info[INDEX_WLAN1].wm_mac_address, INDEX_WLAN1);

   memcpy(&tester_testee_data.if_info[INDEX_WLAN2].link_mac_address, &tester_testee_data.peer_mac_address, sizeof(al_net_addr_t));
   _get_next_mac_address(&tester_testee_data.if_info[INDEX_WLAN2].link_mac_address, INDEX_WLAN2);
   memcpy(&tester_testee_data.if_info[INDEX_WLAN2].wm_mac_address, &tester_testee_data.ds_mac_address, sizeof(al_net_addr_t));
   _get_next_mac_address(&tester_testee_data.if_info[INDEX_WLAN2].wm_mac_address, INDEX_WLAN2);

   memcpy(&tester_testee_data.if_info[INDEX_WLAN3].link_mac_address, &tester_testee_data.peer_mac_address, sizeof(al_net_addr_t));
   _get_next_mac_address(&tester_testee_data.if_info[INDEX_WLAN3].link_mac_address, INDEX_WLAN3);
   memcpy(&tester_testee_data.if_info[INDEX_WLAN3].wm_mac_address, &tester_testee_data.ds_mac_address, sizeof(al_net_addr_t));
   _get_next_mac_address(&tester_testee_data.if_info[INDEX_WLAN3].wm_mac_address, INDEX_WLAN3);

   return 0;
}


int access_point_tester_testee_initialize(AL_CONTEXT_PARAM_DECL access_point_tester_testee_globals_t *data)
{
   memset(&tester_testee_data, 0, sizeof(access_point_tester_testee_globals_t));
   memcpy(&tester_testee_data, data, sizeof(access_point_tester_testee_globals_t));

   if (data->ap_mode == ACCESS_POINT_MODE_TESTER)
   {
      tester_testee_data.if_count = (int)sizeof(tester_if_info) / sizeof(tester_testee_if_info_t);
      tester_testee_data.if_info  = (tester_testee_if_info_t *)&tester_if_info;

      AL_PRINT_LOG_MSG_0("AP		: -----------Access Point initialized for Tester mode-----------");
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "AP		: : -----------Access Point initialized for Tester mode-----------");
   }
   else if (data->ap_mode == ACCESS_POINT_MODE_TESTEE)
   {
      tester_testee_data.if_count = (int)sizeof(testee_if_info) / sizeof(tester_testee_if_info_t);
      tester_testee_data.if_info  = (tester_testee_if_info_t *)&testee_if_info;

      AL_PRINT_LOG_MSG_0("AP		: -----------Access Point initialized for Testee mode-----------");
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "AP		: : -----------Access Point initialized for Testee mode-----------");
   }
   else
   {
      return -1;
   }


   _initialize_interface_link_mac_address_for_all_link_test();

   a_channel_index  = -1;
   bg_channel_index = -1;

   SET_LQCP_STATE(LQCP_STATE_INITIALIZED)

   return 0;
}


#else

int access_point_tester_testee_initialize(AL_CONTEXT_PARAM_DECL access_point_tester_testee_globals_t *data)
{
   return -1;
}


int access_point_tester_testee_start_thread(AL_CONTEXT_PARAM_DECL al_thread_param_t *param)
{
   return -1;
}
#endif
