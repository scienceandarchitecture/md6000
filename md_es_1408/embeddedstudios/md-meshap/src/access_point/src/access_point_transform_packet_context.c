/********************************************************************************
* MeshDynamics
* --------------
* File     : _transform_packet_context.c
* Comments : Packet Context for Media Transformation functionality
* Created  : 11/27/2007
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  0  |11/27/2007| Created                                         | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

/**
 * This is a private source file included by access_point_transform.c
 * It was created to improve readability.
 */

struct _packet_context
{
   int                              match_type;
   int                              transform_id;
   al_net_if_t                      *src_net_if;
   al_packet_t                      *packet;
   al_net_if_t                      *dest_net_if;
   access_point_netif_config_info_t *dest_net_if_config_info;
   al_net_addr_t                    imm_dest_addr;
   al_net_addr_t                    *dest_addr;
   al_net_addr_t                    *src_addr;
   al_net_addr_t                    *imm_src_addr;
   access_point_sta_entry_t         *dest_sta_info;
   access_point_sta_entry_t         *src_sta_info;
   access_point_sta_entry_t         *imm_dest_sta_info;
   unsigned short                   src_vlan_tag;
   unsigned char                    src_dot1p_priority;
};
typedef struct _packet_context   _packet_context_t;


static void _init_packet_context(AL_CONTEXT_PARAM_DECL _packet_context_t *packet_context)
{
   memset(packet_context, 0, sizeof(_packet_context_t));
}


static void _cleanup_packet_context(AL_CONTEXT_PARAM_DECL _packet_context_t *packet_context)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Enter %s<%d>\n",__func__,__LINE__);
   if (packet_context->dest_sta_info != NULL)
   {
      access_point_release_sta(AL_CONTEXT packet_context->dest_sta_info);
   }

   if (packet_context->src_sta_info != NULL)
   {
      access_point_release_sta(AL_CONTEXT packet_context->src_sta_info);
   }

   if (packet_context->imm_dest_sta_info != NULL)
   {
      access_point_release_sta(AL_CONTEXT packet_context->imm_dest_sta_info);
   }
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Exit %s<%d>\n",__func__,__LINE__);
}


static int _set_src_packet_context(AL_CONTEXT_PARAM_DECL _packet_context_t *packet_context)
{
   /**
    * Called for TODS packets received over WM
    */

   packet_context->src_sta_info = access_point_get_sta(AL_CONTEXT packet_context->src_addr);

   if (packet_context->src_sta_info == NULL)
   {
      /**
       * TODS packet from a STA not associated with us, drop the packet.
       */
	  (tx_rx_pkt_stats.packet_drop[126])++;
      return -1;
   }

   if (packet_context->packet->dot1q_tag == AL_PACKET_VLAN_TAG_TYPE_UNTAGGED)
   {
      if ((packet_context->src_sta_info->sta.vlan_info != NULL) &&
          (packet_context->src_sta_info->sta.vlan_info != &default_vlan_info))
      {
         packet_context.src_vlan_tag       = packet_context->src_sta_info->sta.vlan_info->vlan_tag;
         packet_context.src_dot1p_priority = packet_context->src_sta_info->sta.vlan_info->dot1p_priority;
      }
   }
   else
   {
      if ((packet_context->src_sta_info->sta.vlan_info == NULL) ||
          (packet_context->src_sta_info->sta.vlan_info == &default_vlan_info) ||
          (packet_context->src_sta_info->sta.vlan_info->vlan_tag != packet_context->packet->dot1q_tag))
      {
         /**
          * Invalid Tagged packet received from direct child, drop the packet
          */
	     (tx_rx_pkt_stats.packet_drop[127])++;
         return -1;
      }

      packet_context.src_vlan_tag       = packet_context->src_sta_info->sta.vlan_info->vlan_tag;
      packet_context.src_dot1p_priority = packet_context->src_sta_info->sta.vlan_info->dot1p_priority;
   }

   return 0;
}


static int _set_dest_packet_context(AL_CONTEXT_PARAM_DECL _packet_context_t *packet_context)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Enter %s<%d>\n",__func__,__LINE__);
   if (AL_NET_ADDR_IS_BROADCAST(packet_context->dest_addr) ||
       AL_NET_ADDR_IS_MULTICAST(packet_context->dest_addr))
   {
      packet_context->dest_net_if             = NULL;
      packet_context->dest_net_if_config_info = NULL;
      packet_context->dest_sta_info           = NULL;
      packet_context->imm_dest_sta_info       = NULL;

      memset(&packet_context->imm_dest_addr, 0, sizeof(al_net_addr_t));

      return 0;
   }

   packet_context->dest_sta_info = access_point_get_sta(AL_CONTEXT packet_context->dest_addr);

   if ((packet_context->dest_sta_info == NULL) &&
       (on_data_reception_handler != NULL))
   {
      int            decision;
      unsigned short frame_control_out;

      decision = on_data_reception_handler(AL_CONTEXT packet_context->src_net_if,
                                           packet_context->packet->rssi,
                                           packet_context->src_addr,
                                           packet_context->src_addr,
                                           packet_context->dest_addr,
                                           &frame_control_out,
                                           &packet_context->imm_dest_addr,
                                           &packet_context->dest_net_if);

      if (decision == ACCESS_POINT_PACKET_DECISION_DROP)
      {
         return -1;
      }

      packet_context->imm_dest_sta_info = access_point_get_sta(AL_CONTEXT & packet_context->imm_dest_addr);

      packet_context->dest_net_if_config_info = (access_point_netif_config_info_t *)packet_context->dest_net_if->config.ext_config_info;
   }
   else if (packet_context->dest_sta_info != NULL)
   {
      if (packet_context->src_vlan_tag == AL_PACKET_VLAN_TAG_TYPE_UNTAGGED)
      {
         if (packet_context->dest_sta_info->sta.vlan_info != &default_vlan_info)
         {
            return -1;
         }
      }
      else
      {
         /**
          * Since the source packet is tagged, the destination sta must not
          * belong to the default VLAN or to any other VLAN.
          */
         if ((packet_context->dest_sta_info->sta.vlan_info == NULL) ||
             (packet_context->dest_sta_info->sta.vlan_info == &default_vlan_info) ||
             (packet_context->dest_sta_info->sta.vlan_info->vlan_tag != packet_context->src_vlan_tag))
         {
            return -1;
         }
      }
   }
   else
   {
      /**
       * Paranoid check case of on_data_reception_handler being NULL
       * and packet_context->dest_sta_info is NULL
       */
      return -1;
   }

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Exit %s<%d>\n",__func__,__LINE__);

   return 0;
}
