/********************************************************************************
* MeshDynamics
* --------------
* File     : access_point_acl.h
* Comments : Access point ACL implementation
* Created  : 3/13/2006
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  1  |6/14/2006 | Changes for disallow_other_sta	              |Prachiti|
* -----------------------------------------------------------------------------
* |  0  |3/13/2006 | Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

#ifndef __ACCESS_POINT_ACL_H__
#define __ACCESS_POINT_ACL_H__

void access_point_acl_initialize(AL_CONTEXT_PARAM_DECL_SINGLE);
void access_point_acl_uninitialize(AL_CONTEXT_PARAM_DECL_SINGLE);

int access_point_acl_lookup_entry(AL_CONTEXT al_net_addr_t *addr_in,
                                  access_point_vlan_info_t **vlan_out,
                                  int                      *allow_out,
                                  int                      *dot11e_enabled_out,
                                  int                      *dot11e_category_out);

void access_point_acl_set_list(AL_CONTEXT_PARAM_DECL int count, access_point_acl_entry_t *entries);

int access_point_acl_get_disallow_other_stas(AL_CONTEXT_PARAM_DECL_SINGLE);

extern al_spinlock_t acl_splock;
AL_DECLARE_PER_CPU(int, acl_spvar);

#endif /*__ACCESS_POINT_ACL_H__*/
