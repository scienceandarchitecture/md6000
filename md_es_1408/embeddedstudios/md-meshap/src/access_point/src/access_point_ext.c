/********************************************************************************
* MeshDynamics
* --------------
* File     : access_point_ext.c
* Comments : Access point external interface
* Created  : 6/15/2005
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* | 10  |6/20/2007 | access_point_ext_print_mcast_stas added added   | Sriram |
* -----------------------------------------------------------------------------
* |  9  |5/17/2007 | access_point_ext_set_sta_vlan added             | Sriram |
* -----------------------------------------------------------------------------
* |  8  |4/19/2007 |  Changes for forced disassociation              | Sriram |
* -----------------------------------------------------------------------------
* |  7  |1/13/2007 | access_point_ext_print_vlan_info added          | Sriram |
* -----------------------------------------------------------------------------
* |  6  |1/13/2007 | access_point_ext_print_wm_info added            | Sriram |
* -----------------------------------------------------------------------------
* |  5  |5/31/2006 | access_point_ext_print_hqueue_stat implemented  | Sriram |
* -----------------------------------------------------------------------------
* |  4  |3/14/2006 | ATOMIC type changes                             | Sriram |
* -----------------------------------------------------------------------------
* |  3  |01/04/2006| access_point_ext_print_queue_stat added         | Bindu  |
* -----------------------------------------------------------------------------
* |  2  |6/18/2005 | Enumerate STAs added                            | Sriram |
* -----------------------------------------------------------------------------
* |  1  |6/17/2005 | Added test flags setting                        | Sriram |
* -----------------------------------------------------------------------------
* |  0  |6/15/2005 | Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

#include <linux/string.h>
#include <linux/kernel.h>
#include <linux/seq_file.h>
#include "al.h"
#include "access_point.h"
#include "access_point_globals.h"
#include "al_impl_context.h"
#include "access_point_sta.h"
#include "access_point_vlan.h"

int access_point_ext_disassociate_sta(unsigned char *address)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
   al_net_addr_t addr;

   memset(&addr, 0, sizeof(al_net_addr_t));

   addr.length = 6;
   memcpy(addr.bytes, address, 6);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
   return access_point_disassociate_sta_ex(AL_CONTEXT & addr, 0, NULL);
}


int access_point_ext_set_test_flags(unsigned int flags)
{
   ap_test_flags = flags;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "AP: Accepting Test flags = 0x%08X", ap_test_flags);
   return 0;
}


int access_point_ext_enumerate_stas(void *callback, void (*enum_routine)(void *callback, const access_point_sta_info_t *sta))
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
   access_point_sta_entry_t *next;
   access_point_sta_entry_t *node;
   unsigned long            flags;

   node = access_point_get_first_sta(AL_CONTEXT_SINGLE);

   for ( ; node != NULL; )
   {
	  al_spin_lock(sta_splock, sta_spvar);
      next = node->next_list;
      if (next != NULL)
      {
         AL_ATOMIC_INC(next->sta.ref_count);
      }
	  al_spin_unlock(sta_splock, sta_spvar);

      if (enum_routine != NULL)
      {
         enum_routine(callback, &node->sta);
      }

      access_point_release_sta(AL_CONTEXT node);

      node = next;
   }

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
   return 0;
}


int access_point_ext_print_queue_stat(struct seq_file *m)
{
   return globals.ap_queue->queue_print_stats(AL_CONTEXT globals.ap_queue, m);
}


int access_point_ext_print_hqueue_stat(struct seq_file *m)
{
   return globals.ap_queue->queue_print_hstats(AL_CONTEXT globals.ap_queue, m);
}


int access_point_ext_print_wm_info(struct seq_file *m)
{
   //char*								p;
   int i;
   access_point_netif_config_info_t *net_if_config_info;

   //p		= print_p;

   seq_printf(m,
              "------------------------------------------------------\r\n"
              "IFNAME|MCC|DCC|UBCAST      |TBCAST      |UBCASTR     |\r\n"
              "------------------------------------------------------\r\n");


   for (i = 0; i < globals.config.wm_net_if_count; i++)
   {
      net_if_config_info = (access_point_netif_config_info_t *)globals.config.wm_net_if_info[i].net_if->config.ext_config_info;

      seq_printf(m,
                 "%6s|%03d|%03d|%012d|%012d|%012d|\r\n",
                 globals.config.wm_net_if_info[i].net_if->name,
                 AL_ATOMIC_GET(net_if_config_info->imcp_sta_count),
                 AL_ATOMIC_GET(net_if_config_info->sta_count),
                 net_if_config_info->untagged_bcast_count,
                 net_if_config_info->tagged_bcast_count,
                 net_if_config_info->untagged_bcast_reordered_count);
   }

   //return (p - print_p);
   return 0;
}


static int _print_vlan_list_info(access_point_vlan_entry_t *list_head, struct seq_file *m)
{
   access_point_netif_config_info_t *net_if_config_info;
   access_point_vlan_entry_t        *temp;
   int i;

   //char*								p;

   temp = list_head;
   //p		= print_p;

   while (temp != NULL)
   {
      seq_printf(m, "%04d|%4s|%4s|",
                 temp->vlan.vlan_tag,
                 temp->vlan.config.security_info.dot11.rsn.enabled ? "Yes" : "No",
                 access_point_vlan_get_vlan_encryption(AL_CONTEXT temp->vlan.vlan_tag) ? "Yes" : "No");

      for (i = 0; i < globals.config.wm_net_if_count; i++)
      {
         net_if_config_info = (access_point_netif_config_info_t *)globals.config.wm_net_if_info[i].net_if->config.ext_config_info;
         seq_printf(m,
                    "%06d|",
                    access_point_get_net_if_vlan_count(AL_CONTEXT temp->vlan.vlan_tag, net_if_config_info));
      }

      seq_printf(m, "\r\n");

      temp = temp->next_list;
   }

   //return (p - print_p);
   return 0;
}


int access_point_ext_print_vlan_info(struct seq_file *m)
{
   //char*								p;
   int i;

   //p	= print_p;

   /** Column Headers */

   seq_printf(m,
              "-------------------------------------------------------------------\r\n"
              "VTAG|ENCS|ENCB|");

   for (i = 0; i < globals.config.wm_net_if_count; i++)
   {
      seq_printf(m, "%6s|", globals.config.wm_net_if_info[i].net_if->name);
   }

   seq_printf(m,
              "\r\n"
              "-------------------------------------------------------------------\r\n");


   /** VLAN Data */

   //p	+= _print_vlan_list_info(vlan_list_head,m);
   _print_vlan_list_info(vlan_list_head, m);

   _print_vlan_list_info(in_direct_vlan_tag_list_head, m);

   //return (p - print_p);
   return 0;
}


int access_point_ext_set_sta_vlan(unsigned char *address, unsigned short tag)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
   al_net_addr_t addr;

   memset(&addr, 0, sizeof(al_net_addr_t));

   addr.length = 6;
   memcpy(addr.bytes, address, 6);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
   return access_point_set_sta_vlan(AL_CONTEXT & addr, tag);
}


static int _print_mcast_sta_list(access_point_mcast_sta_list_t *list_head, const char *if_name, al_net_addr_t *mcast_addr, int direct, struct seq_file *m)
{
   access_point_mcast_sta_list_t *temp;

   //char*							p;

   temp = list_head;
   //p		= print_p;


   while (temp != NULL)
   {
      seq_printf(m,
                 AL_NET_ADDR_STR "|%6s|"AL_NET_ADDR_STR "|%9s|\r\n",
                 AL_NET_ADDR_TO_STR(&temp->sta_addr),
                 if_name,
                 AL_NET_ADDR_TO_STR(mcast_addr),
                 direct ? "direct  " : "in-direct");

      temp = temp->next;
   }

   //return (p - print_p);
   return 0;
}


static int _enumerate_mcast_entries(access_point_mcast_t *list_head, const char *if_name, struct seq_file *m)
{
   access_point_mcast_t *node;

   //char*					p;

   node = list_head;
   //p		= print_p;

   while (node != NULL)
   {
      _print_mcast_sta_list(node->stas, if_name, &node->mcast_addr, 1, m);
      _print_mcast_sta_list(node->indirect_stas, if_name, &node->mcast_addr, 0, m);

      node = node->next_list;
   }

   //return (p - print_p);
   return 0;
}


int access_point_ext_print_mcast_stas(struct seq_file *m)
{
   //char*								p;
   int i;
   access_point_netif_config_info_t *net_if_config_info;

   //p	= print_p;

   /** Column Headers */

   seq_printf(m,
              "-----------------------------------------------------------\r\n"
              "STATION ADDR     |IFNAME|MULTICAST ADDR      |DIRECT CHILD|\r\n"
              "-----------------------------------------------------------\r\n");


   for (i = 0; i < globals.config.wm_net_if_count; i++)
   {
      net_if_config_info = (access_point_netif_config_info_t *)globals.config.wm_net_if_info[i].net_if->config.ext_config_info;

      _enumerate_mcast_entries(net_if_config_info->multicast_list_head,
                               net_if_config_info->net_if->name,
                               m);
   }

   //return (p - print_p);
   return 0;
}


int access_point_ext_verify_option(unsigned short option_code)
{
   return access_point_verify_option(AL_CONTEXT option_code);
}
