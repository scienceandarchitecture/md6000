/********************************************************************************
* MeshDynamics
* --------------
* File     : access_point_vlan.h
* Comments : Access Point VLAN Header file
* Created  : 2/3/2005
* Author   : Abhijit Ayarekar
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  6  |2/8/2007  | Changes for In-direct VLAN                      | Sriram |
* -----------------------------------------------------------------------------
* |  5  |1/11/2007 | Changes for VLAN BCAST optimization             | Sriram |
* -----------------------------------------------------------------------------
* |  4  |01/04/2006| Tag hash added                                  | Bindu  |
* -----------------------------------------------------------------------------
* |  3  |4/25/2005 | Misc changes                                    | Sriram |
* -----------------------------------------------------------------------------
* |  2  |4/12/2005 | Encryption handling corrected                   | Sriram |
* -----------------------------------------------------------------------------
* |  1  |4/11/2005 | Changes after merging                           | Sriram |
* -----------------------------------------------------------------------------
* |  0  |2/3/2005  | created                                         | Abhijit|
* -----------------------------------------------------------------------------
********************************************************************************/

#include <linux/preempt.h>
#ifndef _ACCESS_POINT_VLAN_H_
#define _ACCESS_POINT_VLAN_H_

#ifndef ACCESS_POINT_VLAN_HASH_SIZE
#define ACCESS_POINT_VLAN_HASH_SIZE                   23
#endif

#define ACCESS_POINT_VLAN_SUCCESS                     0
#define ACCESS_POINT_VLAN_ERROR_VLAN_NOT_FOUND        1
#define ACCESS_POINT_VLAN_ERROR_INVALID_VLAN_ESSID    2
#define ACCESS_POINT_VLAN_CONFIG_READ_ERROR           3
#define ACCESS_POINT_VLAN_DEFAULT_NAME                "default"

struct access_point_vlan_entry
{
   access_point_vlan_info_t       vlan;

   struct access_point_vlan_entry *prev_hash;
   struct access_point_vlan_entry *next_hash;
   struct access_point_vlan_entry *next_list;
   struct access_point_vlan_entry *prev_list;

   struct access_point_vlan_entry *prev_tag_hash;
   struct access_point_vlan_entry *next_tag_hash;
};

typedef struct access_point_vlan_entry   access_point_vlan_entry_t;

AL_DECLARE_GLOBAL_EXTERN(int vlan_count);
AL_DECLARE_GLOBAL_EXTERN(access_point_vlan_info_t default_vlan_info);
AL_DECLARE_GLOBAL_EXTERN(access_point_vlan_entry_t * vlan_hash[ACCESS_POINT_VLAN_HASH_SIZE]);
AL_DECLARE_GLOBAL_EXTERN(access_point_vlan_entry_t * vlan_list_head);
AL_DECLARE_GLOBAL_EXTERN(access_point_vlan_entry_t * vlan_list_tail);
AL_DECLARE_GLOBAL_EXTERN(unsigned char vlan_encryption_bitmap[512 /* 4096 /8 */]);

AL_DECLARE_GLOBAL_EXTERN(access_point_vlan_entry_t * vlan_tag_hash[ACCESS_POINT_VLAN_HASH_SIZE]);

AL_DECLARE_GLOBAL_EXTERN(access_point_vlan_entry_t * in_direct_vlan_hash[ACCESS_POINT_VLAN_HASH_SIZE]);
AL_DECLARE_GLOBAL_EXTERN(access_point_vlan_entry_t * in_direct_vlan_tag_list_head);
AL_DECLARE_GLOBAL_EXTERN(access_point_vlan_entry_t * in_direct_vlan_tag_list_tail);

static AL_INLINE int access_point_vlan_get_vlan_encryption(AL_CONTEXT_PARAM_DECL unsigned short tag)
{
   int           index;
   int           bit;
   unsigned char mask;

   index = tag / 8;
   bit   = tag % 8;
   mask  = 1 << bit;

   return(vlan_encryption_bitmap[index] & mask);
}


static AL_INLINE int access_point_get_net_if_vlan_count(AL_CONTEXT_PARAM_DECL unsigned short tag, access_point_netif_config_info_t *netif_config_info)
{
   unsigned long flags;
   int           ret;

   al_disable_interrupts(flags);

#ifdef ACCESS_POINT_VLAN_BCAST_OPTIMIZE
   ret = netif_config_info->vlan_child_count[tag];
#else
   ret = netif_config_info->vlan_child_count[0];
#endif

   al_enable_interrupts(flags);

   return ret;
}


int access_point_vlan_destroy_essid_info(AL_CONTEXT_PARAM_DECL al_802_11_essid_info_t *info);

int access_point_vlan_create_essid_info(AL_CONTEXT_PARAM_DECL int *count,
                                        al_802_11_essid_info_t    **info);

access_point_vlan_info_t *access_point_vlan_get(AL_CONTEXT_PARAM_DECL const char *essid);
void access_point_vlan_remove(AL_CONTEXT_PARAM_DECL access_point_vlan_entry_t *vlan_entry);
access_point_vlan_info_t *access_point_vlan_add(AL_CONTEXT_PARAM_DECL access_point_vlan_info_t *vlan_info);
access_point_vlan_info_t *access_point_vlan_add_indirect(AL_CONTEXT_PARAM_DECL unsigned short tag, unsigned char rsn_enabled);

access_point_vlan_info_t *access_point_vlan_tag_get(AL_CONTEXT_PARAM_DECL unsigned short tag);

void access_point_incr_net_if_vlan_count(AL_CONTEXT_PARAM_DECL access_point_vlan_info_t *vlan, al_net_if_t *net_if);
void access_point_decr_net_if_vlan_count(AL_CONTEXT_PARAM_DECL access_point_vlan_info_t *vlan, al_net_if_t *net_if);

int access_point_vlan_uninitialize(AL_CONTEXT_PARAM_DECL_SINGLE);
int access_point_vlan_initialize(AL_CONTEXT_PARAM_DECL_SINGLE);

#endif
