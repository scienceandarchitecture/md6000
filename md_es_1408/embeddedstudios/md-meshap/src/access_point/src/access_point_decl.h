/********************************************************************************
* MeshDynamics
* --------------
* File     : access_point_decl.h
* Comments : Access point type declrations
* Created  : 4/21/2004
* Author   : Bindu Parakala
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  1  |5/19/2004 | changed value of _ACCESS_POINT_PACKET_POOL_SIZE | Bindu  |
* -----------------------------------------------------------------------------
* |  0  |4/21/2004 | Created.                                        | Bindu  |
* -----------------------------------------------------------------------------
********************************************************************************/


#ifndef __ACCESS_POINT_DECL_H__
#define __ACCESS_POINT_DECL_H__

#define _ACCESS_POINT_POOL_ITEM_TYPE_POOL    0
#define _ACCESS_POINT_POOL_ITEM_TYPE_HEAP    1
#define _ACCESS_POINT_PACKET_POOL_SIZE       64

struct _access_point_queue_node_pool_item
{
   access_point_queue_t                      queue_node;
   int                                       pool_item_type;
   struct _access_point_queue_node_pool_item *next_pool;
};

typedef struct _access_point_queue_node_pool_item   _access_point_queue_node_pool_item_t;

#endif /*__ACCESS_POINT_DECL_H__*/
