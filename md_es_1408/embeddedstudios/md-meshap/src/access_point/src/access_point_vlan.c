/********************************************************************************
* MeshDynamics
* --------------
* File     : access_point_vlan.c
* Comments : Access Point VLAN implamentation file
* Created  : 2/3/2005
* Author   : Abhijit Ayarekar
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  8  |2/8/2007  | Fixes for In-direct VLAN implementation         | Sriram |
* -----------------------------------------------------------------------------
* |  7  |1/11/2007 | Changes for VLAN BCAST optimization             | Sriram |
* -----------------------------------------------------------------------------
* |  6  |12/6/2006 | In-direct VLAN implementation (NPS)             | Sriram |
* -----------------------------------------------------------------------------
* |  5  |3/14/2006 | Default VLAN name set                           | Sriram |
* -----------------------------------------------------------------------------
* |  4  |01/04/2006| Tag hash added                                  | Bindu  |
* -----------------------------------------------------------------------------
* |  3  |4/25/2005 | Misc changes                                    | Sriram |
* -----------------------------------------------------------------------------
* |  2  |4/12/2005 | Encryption handling corrected                   | Sriram |
* -----------------------------------------------------------------------------
* |  1  |4/11/2005 | Changes after merging                           | Sriram |
* -----------------------------------------------------------------------------
* |  0  |2/3/2005  | Created                                         | Abhijit|
* -----------------------------------------------------------------------------
********************************************************************************/

#include <linux/string.h>
#include <linux/kernel.h>
#include "al_context.h"
#include "al_802_11.h"
#include "al.h"
#include "al_conf.h"
#include "al_ip.h"
#include "access_point.h"
#include "al_assert.h"
#include "access_point_vlan.h"
#include "al_print_log.h"
#include "access_point_globals.h"


/**
 *	vlan info
 */

AL_DECLARE_GLOBAL(int vlan_count);
AL_DECLARE_GLOBAL(access_point_vlan_info_t default_vlan_info);
AL_DECLARE_GLOBAL(access_point_vlan_entry_t * vlan_hash[ACCESS_POINT_VLAN_HASH_SIZE]);
AL_DECLARE_GLOBAL(access_point_vlan_entry_t * vlan_list_head);
AL_DECLARE_GLOBAL(access_point_vlan_entry_t * vlan_list_tail);

/**
 * The VLAN vlan_encryption_bitmap stores 1-bit per VLAN to indicate
 * whether encryption has been enabled or not. The bitmap is indexed by
 * the VLAN tag.
 */

AL_DECLARE_GLOBAL(unsigned char vlan_encryption_bitmap[512 /* 4096 /8 */]);

AL_DECLARE_GLOBAL(access_point_vlan_entry_t * vlan_tag_hash[ACCESS_POINT_VLAN_HASH_SIZE]);

/**
 * In-direct VLANS (received from RADIUS server)
 */

AL_DECLARE_GLOBAL(access_point_vlan_entry_t * in_direct_vlan_hash[ACCESS_POINT_VLAN_HASH_SIZE]);
AL_DECLARE_GLOBAL(access_point_vlan_entry_t * in_direct_vlan_tag_list_head);
AL_DECLARE_GLOBAL(access_point_vlan_entry_t * in_direct_vlan_tag_list_tail);

/**
 * Hash function for VLANs
 */
static AL_INLINE int _ACCESS_POINT_VLAN_HASH_FUNCTION(const char *essid)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
   int hash = 0;
   int i;

   for (i = 0; i < strlen(essid); i++)
   {
      hash += ((int)essid[i] - 32) * (i + 1);
   }
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
   return hash;
}


static AL_INLINE void _access_point_vlan_set_vlan_encryption(AL_CONTEXT_PARAM_DECL unsigned short tag, int set)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
   int           index;
   int           bit;
   unsigned char mask;

   index = tag / 8;
   bit   = tag % 8;
   mask  = 1 << bit;

   if (set)
   {
      vlan_encryption_bitmap[index] |= mask;
   }
   else
   {
      vlan_encryption_bitmap[index] &= ~mask;
   }
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
}


int access_point_vlan_destroy_essid_info(AL_CONTEXT_PARAM_DECL al_802_11_essid_info_t *info)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
   if (info == NULL)
   {
       al_print_log(AL_CONTEXT AL_LOG_TYPE_DEBUG,"info is NULL nothing to free :: func : %s LINE : %d\n", __func__,__LINE__);
      return 0;
   }

   al_heap_free(AL_CONTEXT info);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
   return 0;
}


int access_point_vlan_create_essid_info(AL_CONTEXT_PARAM_DECL int *count,
                                        al_802_11_essid_info_t    **essid_info)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
   access_point_vlan_entry_t *node;
   int i;
   al_802_11_essid_info_t *info;

   *count = vlan_count;

   info = (al_802_11_essid_info_t *)al_heap_alloc(AL_CONTEXT sizeof(al_802_11_essid_info_t) * vlan_count AL_HEAP_DEBUG_PARAM);

   memset(info, 0, sizeof(al_802_11_essid_info_t) * vlan_count);

   /**
    *	Now we copy information from remaining vlan list
    */

   for (node = vlan_list_head, i = 0; node != NULL; node = node->next_list, i++)
   {
      strcpy(info[i].essid, node->vlan.config.essid);
      info[i].vlan_tag = node->vlan.vlan_tag;
      memcpy(&info[i].sec_info, &node->vlan.config.security_info, sizeof(al_802_11_security_info_t));
   }

   *essid_info = info;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
   return 0;
}


access_point_vlan_info_t *access_point_vlan_get(AL_CONTEXT_PARAM_DECL const char *essid)
{
   int hash_value, bucket;
   access_point_vlan_entry_t *out_vlan_entry;

   AL_ASSERT("access_point_get_vlan", essid != NULL);

   hash_value = _ACCESS_POINT_VLAN_HASH_FUNCTION(essid);
   bucket     = hash_value % ACCESS_POINT_VLAN_HASH_SIZE;

   out_vlan_entry = vlan_hash[bucket];

   while (out_vlan_entry != NULL)
   {
      if (!strcmp(out_vlan_entry->vlan.config.essid, essid))
      {
         al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"MESHAP : INFO : config essid are equal : %s : %d\n", __func__,__LINE__);
         return &out_vlan_entry->vlan;
      }

      out_vlan_entry = out_vlan_entry->next_hash;
   }

   return NULL;
}


void access_point_vlan_remove(AL_CONTEXT_PARAM_DECL access_point_vlan_entry_t *vlan_entry)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
   int hash_value, bucket;

   AL_ASSERT("remove_vlan", vlan_entry != NULL);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "AP  : Removing VLAN %s", vlan_entry->vlan.name);

   if (!strcmp(vlan_entry->vlan.name, ACCESS_POINT_VLAN_DEFAULT_NAME))
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_DEBUG,"Access point has default vlan name :: func : %s LINE : %d\n", __func__,__LINE__);
      return;
   }

   hash_value = _ACCESS_POINT_VLAN_HASH_FUNCTION(vlan_entry->vlan.config.essid);
   bucket     = hash_value % ACCESS_POINT_VLAN_HASH_SIZE;

   /**
    *	Remove from ESSID hash
    */

   if (vlan_entry->next_hash != NULL)
   {
      vlan_entry->next_hash->prev_hash = vlan_entry->prev_hash;
   }
   if (vlan_entry->prev_hash != NULL)
   {
      vlan_entry->prev_hash->next_hash = vlan_entry->next_hash;
   }
   if (vlan_hash[bucket] == vlan_entry)
   {
      vlan_hash[bucket] = vlan_entry->next_hash;
   }

   /**
    * Remove from Tag Hash
    */

   hash_value = vlan_entry->vlan.vlan_tag;
   bucket     = hash_value % ACCESS_POINT_VLAN_HASH_SIZE;

   if (vlan_entry->next_tag_hash != NULL)
   {
      vlan_entry->next_tag_hash->prev_tag_hash = vlan_entry->prev_tag_hash;
   }
   if (vlan_entry->prev_tag_hash != NULL)
   {
      vlan_entry->prev_tag_hash->next_tag_hash = vlan_entry->next_tag_hash;
   }
   if (vlan_hash[bucket] == vlan_entry)
   {
      vlan_hash[bucket] = vlan_entry->next_tag_hash;
   }

   /**
    *	Remove from list
    */

   if (vlan_entry->next_list != NULL)
   {
      vlan_entry->next_list->prev_list = vlan_entry->prev_list;
   }
   if (vlan_entry->prev_list != NULL)
   {
      vlan_entry->prev_list->next_list = vlan_entry->next_list;
   }
   if (vlan_entry == vlan_list_head)
   {
      vlan_list_head = vlan_entry->next_list;
   }
   if (vlan_entry == vlan_list_tail)
   {
      vlan_list_tail = vlan_entry->prev_list;
   }

   --vlan_count;

   al_heap_free(AL_CONTEXT vlan_entry);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
}


static AL_INLINE void _vlan_add_helper(AL_CONTEXT_PARAM_DECL access_point_vlan_entry_t *new_vlan_entry)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
   int hash_value;
   int bucket;

   new_vlan_entry->next_hash     = NULL;
   new_vlan_entry->prev_hash     = NULL;
   new_vlan_entry->next_list     = NULL;
   new_vlan_entry->prev_list     = NULL;
   new_vlan_entry->next_tag_hash = NULL;
   new_vlan_entry->prev_tag_hash = NULL;

   hash_value = _ACCESS_POINT_VLAN_HASH_FUNCTION(new_vlan_entry->vlan.config.essid);
   bucket     = hash_value % ACCESS_POINT_VLAN_HASH_SIZE;

   /**
    *	Add to ESSID hash
    */

   if (vlan_hash[bucket] == NULL)
   {
      vlan_hash[bucket] = new_vlan_entry;
   }
   else
   {
      new_vlan_entry->next_hash    = vlan_hash[bucket];
      vlan_hash[bucket]->prev_hash = new_vlan_entry;
      vlan_hash[bucket]            = new_vlan_entry;
   }

   /**
    * Add to Tag Hash
    */

   hash_value = new_vlan_entry->vlan.vlan_tag;
   bucket     = hash_value % ACCESS_POINT_VLAN_HASH_SIZE;

   if (vlan_tag_hash[bucket] == NULL)
   {
      vlan_tag_hash[bucket] = new_vlan_entry;
   }
   else
   {
      new_vlan_entry->next_tag_hash        = vlan_tag_hash[bucket];
      vlan_tag_hash[bucket]->prev_tag_hash = new_vlan_entry;
      vlan_tag_hash[bucket] = new_vlan_entry;
   }

   /**
    *	Add to VLAN list
    */

   if (vlan_list_head == NULL)
   {
      vlan_list_head = new_vlan_entry;
      vlan_list_tail = new_vlan_entry;
   }
   else
   {
      vlan_list_tail->next_list = new_vlan_entry;
      new_vlan_entry->prev_list = vlan_list_tail;
      vlan_list_tail            = new_vlan_entry;
   }


   ++vlan_count;

   /**
    * Make sure WEP is never be allowed for a VLAN
    */

   new_vlan_entry->vlan.config.security_info.dot11.wep.enabled = 0;

   if (new_vlan_entry->vlan.config.security_info.dot11.rsn.enabled)
   {
      _access_point_vlan_set_vlan_encryption(AL_CONTEXT new_vlan_entry->vlan.vlan_tag, 1);
   }
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
}


access_point_vlan_info_t *access_point_vlan_add(AL_CONTEXT_PARAM_DECL access_point_vlan_info_t *vlan_info)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
   access_point_vlan_entry_t *new_vlan_entry;
   int hash_value;
   int bucket;

   AL_ASSERT("add_vlan", vlan_info != NULL);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "AP  : Adding VLAN %s, tag=%d", vlan_info->name, vlan_info->vlan_tag);

   if (!strcmp(vlan_info->name, ACCESS_POINT_VLAN_DEFAULT_NAME))
   {
       al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION," Both essid are same func : %s LINE : %d\n", __func__,__LINE__);
      memcpy(&default_vlan_info, vlan_info, sizeof(access_point_vlan_info_t));
      return &default_vlan_info;
   }

   hash_value = _ACCESS_POINT_VLAN_HASH_FUNCTION(vlan_info->config.essid);
   bucket     = hash_value % ACCESS_POINT_VLAN_HASH_SIZE;

   new_vlan_entry = vlan_hash[bucket];

   while (new_vlan_entry != NULL)
   {
      if (!strcmp(vlan_info->config.essid, new_vlan_entry->vlan.config.essid))
      {
         al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION," Both essid are same func : %s LINE : %d\n", __func__,__LINE__);
         return &new_vlan_entry->vlan;
      }

      new_vlan_entry = new_vlan_entry->next_hash;
   }

   /**
    * VLAN ESSID was not found in the list, hence create a new entry
    */

   new_vlan_entry = (access_point_vlan_entry_t *)al_heap_alloc(AL_CONTEXT sizeof(access_point_vlan_entry_t)AL_HEAP_DEBUG_PARAM);

   memset(new_vlan_entry, 0, sizeof(access_point_vlan_entry_t));

   memcpy(&(new_vlan_entry->vlan), vlan_info, sizeof(access_point_vlan_info_t));

   _vlan_add_helper(AL_CONTEXT new_vlan_entry);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"MESHAP : INFO : VLAN being added : %s : %d\n", __func__,__LINE__);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
   return &new_vlan_entry->vlan;
}


access_point_vlan_info_t *access_point_vlan_add_indirect(AL_CONTEXT_PARAM_DECL unsigned short tag,
                                                         unsigned char                        rsn_enabled)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
   access_point_vlan_entry_t *new_vlan_entry;

   new_vlan_entry = (access_point_vlan_entry_t *)al_heap_alloc(AL_CONTEXT sizeof(access_point_vlan_entry_t)AL_HEAP_DEBUG_PARAM);

   memset(new_vlan_entry, 0, sizeof(access_point_vlan_entry_t));

   new_vlan_entry->vlan.vlan_tag = tag;

   sprintf(new_vlan_entry->vlan.config.essid, "MD-PRIV-SSID-TAG-%d", tag);
   new_vlan_entry->vlan.config.security_info.dot11.rsn.enabled = rsn_enabled;

   _vlan_add_helper(AL_CONTEXT new_vlan_entry);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
   return &new_vlan_entry->vlan;
}


access_point_vlan_info_t *access_point_vlan_tag_get(AL_CONTEXT_PARAM_DECL unsigned short tag)
{
   access_point_vlan_entry_t *out_entry;
   int hash_value;
   int bucket;

   hash_value = tag;
   bucket     = hash_value % ACCESS_POINT_VLAN_HASH_SIZE;

   out_entry = vlan_tag_hash[bucket];

   while (out_entry != NULL)
   {
      if (out_entry->vlan.vlan_tag == tag)
      {
         return &out_entry->vlan;
      }

      out_entry = out_entry->next_tag_hash;
   }

   return NULL;
}


void access_point_incr_net_if_vlan_count(AL_CONTEXT_PARAM_DECL access_point_vlan_info_t *vlan, al_net_if_t *net_if)
{
   access_point_netif_config_info_t *netif_config_info;
   unsigned long flags;

   netif_config_info = (access_point_netif_config_info_t *)net_if->config.ext_config_info;

   al_disable_interrupts(flags);

#ifdef ACCESS_POINT_VLAN_BCAST_OPTIMIZE
   ++netif_config_info->vlan_child_count[vlan->vlan_tag];
#else
   ++netif_config_info->vlan_child_count[0];
#endif

   al_enable_interrupts(flags);
}


void access_point_decr_net_if_vlan_count(AL_CONTEXT_PARAM_DECL access_point_vlan_info_t *vlan, al_net_if_t *net_if)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
   access_point_netif_config_info_t *netif_config_info;
   unsigned long flags;

   netif_config_info = (access_point_netif_config_info_t *)net_if->config.ext_config_info;

   al_disable_interrupts(flags);

#ifdef ACCESS_POINT_VLAN_BCAST_OPTIMIZE
   if (netif_config_info->vlan_child_count[vlan->vlan_tag] > 0)
   {
      --netif_config_info->vlan_child_count[vlan->vlan_tag];
   }
#else
   if (netif_config_info->vlan_child_count[0] > 0)
   {
      --netif_config_info->vlan_child_count[0];
   }
#endif

   al_enable_interrupts(flags);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
}


int access_point_vlan_uninitialize(AL_CONTEXT_PARAM_DECL_SINGLE)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
   access_point_vlan_entry_t *node;
   access_point_vlan_entry_t *temp;
   int i;

   node = vlan_list_head;

   while (node != NULL)
   {
      temp = node->next_list;
      access_point_vlan_remove(AL_CONTEXT node);
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"MESHAP : INFO : ap_vlan being removed : %s : %d\n", __func__,__LINE__);
      node = temp;
   }

   vlan_count     = 0;
   vlan_list_head = NULL;
   vlan_list_tail = NULL;

   /**
    * Clear all entries in the In-direct VLAN Hash
    */

   for (i = 0; i < ACCESS_POINT_VLAN_HASH_SIZE; i++)
   {
      node = in_direct_vlan_hash[i];
      while (node != NULL)
      {
         temp = node->next_hash;
         al_heap_free(AL_CONTEXT node);
         node = temp;
      }
      in_direct_vlan_hash[i] = NULL;
   }

   in_direct_vlan_tag_list_head = NULL;
   in_direct_vlan_tag_list_tail = NULL;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
   return 0;
}


int access_point_vlan_initialize(AL_CONTEXT_PARAM_DECL_SINGLE)
{
   int i;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
   vlan_count     = 0;
   vlan_list_head = NULL;
   vlan_list_tail = NULL;

   memset(vlan_encryption_bitmap, 0, sizeof(vlan_encryption_bitmap));

   memset(&default_vlan_info, 0, sizeof(access_point_vlan_info_t));

   for (i = 0; i < ACCESS_POINT_VLAN_HASH_SIZE; i++)
   {
      vlan_hash[i] = NULL;
   }

   /**
    * Hash for VLAN Tags
    */

   for (i = 0; i < ACCESS_POINT_VLAN_HASH_SIZE; i++)
   {
      vlan_tag_hash[i] = NULL;
   }

   /**
    * Hash for In-direct VLANs
    */

   in_direct_vlan_tag_list_head = NULL;
   in_direct_vlan_tag_list_tail = NULL;

   for (i = 0; i < ACCESS_POINT_VLAN_HASH_SIZE; i++)
   {
      in_direct_vlan_hash[i] = NULL;
   }

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
   return 0;
}
