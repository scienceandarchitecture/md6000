/********************************************************************************
* MeshDynamics
* --------------
* File     : access_point_transform.h
* Comments : Media Transformation Functionality
* Created  : 11/8/2007
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  0  |11/8/2007 | Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

#ifndef __ACCESS_POINT_TRANSFORM_H__
#define __ACCESS_POINT_TRANSFORM_H__

#if !defined(ACCESS_POINT_TRANSFORM_SLOT_COUNT)
#define ACCESS_POINT_TRANSFORM_SLOT_COUNT    8
#endif

struct access_point_transform_adapter
{
   al_packet_t * (*transform)    (al_packet_t *packet);
   al_packet_t * (*correct)              (al_packet_t *packet);
   al_packet_t * (*de_transform) (al_packet_t *packet);
};
typedef struct access_point_transform_adapter   access_point_transform_adapter_t;

#define ACCESS_POINT_TRANSFORM_ERR_SUCCESS          0
#define ACCESS_POINT_TRANSFORM_ERR_INVALID_SLOT     -1
#define ACCESS_POINT_TRANSFORM_ERR_SLOT_BUSY        -2
#define ACCESS_POINT_TRANSFORM_ERR_SEND_AS_USUAL    -3
#define ACCESS_POINT_TRANSFORM_ERR_DROP             -4

void access_point_transform_initialize(AL_CONTEXT_PARAM_DECL_SINGLE);
void access_point_transform_uninitialize(AL_CONTEXT_PARAM_DECL_SINGLE);
int access_point_register_transform(AL_CONTEXT_PARAM_DECL int slot, access_point_transform_adapter_t *adapter);
void access_point_unregister_transform(AL_CONTEXT_PARAM_DECL int slot);
int access_point_transorm_process_ds_packet(AL_CONTEXT_PARAM_DECL al_net_if_t *net_if, al_packet_t *packet, int transform_type, int transform_id);
int access_point_transorm_process_wm_packet(AL_CONTEXT_PARAM_DECL al_net_if_t *net_if, al_packet_t *packet, int transform_type, int transform_id);

#endif /*__ACCESS_POINT_TRANSFORM_H__*/
