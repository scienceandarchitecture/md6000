/********************************************************************************
* MeshDynamics
* --------------
* File     : access_point_dot11i.h
* Comments : 802.11i support code
* Created  : 3/29/2005
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  0  |3/29/2005 | Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

#ifndef __ACCESS_POINT_DOT11I_H__
#define __ACCESS_POINT_DOT11I_H__

int access_point_dot11i_initialize(AL_CONTEXT_PARAM_DECL_SINGLE);
int access_point_dot11i_uninitialize(AL_CONTEXT_PARAM_DECL_SINGLE);
int access_point_dot11i_add_sta(AL_CONTEXT_PARAM_DECL al_net_addr_t *sta_addr);

extern al_spinlock_t dot11i_splock;
AL_DECLARE_PER_CPU(int, dot11i_spvar);

#endif /*__ACCESS_POINT_DOT11I_H__*/
