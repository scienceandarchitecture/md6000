/********************************************************************************
* MeshDynamics
* --------------
* File     : access_point_globals.c
* Comments : Globals Extern Variables
* Created  : 5/31/2004
* Author   : Bindu Parakala
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  5  |6/17/2005 | Added test_flags                                | Sriram |
* -----------------------------------------------------------------------------
* |  4  |4/6/2005  | Changes for 802.1x/802.11i                      | Sriram |
* -----------------------------------------------------------------------------
* |  3  |7/24/2004 | ap_enable_disable_processing var added          | Anand  |
* -----------------------------------------------------------------------------
* |  2  |7/19/2004 | if_transmit_list removed                        | Sriram |
* -----------------------------------------------------------------------------
* |  1  |7/19/2004 | if_transmit_list added                          | Bindu  |
* -----------------------------------------------------------------------------
* |  0  |5/31/2004 | Created                                         | Bindu  |
* -----------------------------------------------------------------------------
********************************************************************************/

#include <linux/string.h>
#include "al.h"
#include "access_point.h"
#include "access_point_thread.h"
#include "access_point_globals.h"


AL_DECLARE_GLOBAL(access_point_globals_t globals);
AL_DECLARE_GLOBAL(access_point_on_start_t on_start_handler);
AL_DECLARE_GLOBAL(access_point_on_start_t on_start_handler2);
AL_DECLARE_GLOBAL(access_point_on_stop_t on_stop_handler);
AL_DECLARE_GLOBAL(access_point_on_auth_request_t on_auth_request_handler);
AL_DECLARE_GLOBAL(access_point_on_assoc_request_t on_assoc_request_handler);
AL_DECLARE_GLOBAL(access_point_on_deauth_t on_deauth_handler);
AL_DECLARE_GLOBAL(access_point_on_disassoc_t on_disassoc_handler);
AL_DECLARE_GLOBAL(access_point_on_probe_request_t on_probe_request_handler);
AL_DECLARE_GLOBAL(access_point_on_data_reception_t on_data_reception_handler);
AL_DECLARE_GLOBAL(access_point_on_before_transmit_t on_before_transmit_handler);
AL_DECLARE_GLOBAL(access_point_on_imcp_packet_t on_imcp_packet_handler);
AL_DECLARE_GLOBAL(access_point_on_dot1x_auth_init_t on_dot1x_auth_init_handler);
AL_DECLARE_GLOBAL(access_point_on_dot1x_auth_start_t on_dot1x_auth_start_handler);
AL_DECLARE_GLOBAL(access_point_on_dot1x_auth_delete_t on_dot1x_auth_delete);
AL_DECLARE_GLOBAL(access_point_on_eapol_rx_t on_eapol_rx_handler);
AL_DECLARE_GLOBAL(int sta_queue_semaphore);
AL_DECLARE_GLOBAL(int monitor_exit_handle);
AL_DECLARE_GLOBAL(unsigned char ap_enable_disable_processing);
AL_DECLARE_GLOBAL(unsigned int ap_test_flags);
