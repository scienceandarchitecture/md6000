/********************************************************************************
* MeshDynamics
* --------------
* File     : access_point_thread.h
* Comments : Acces Point Thread Function Header
* Created  : 4/9/2004
* Author   : Bindu Parakala
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  5  |6/29/2004 | monitor_exit_handle							  | Anand  |
* -----------------------------------------------------------------------------
* |  4  |5/31/2004 | removed extrern defs                            | Bindu  |
* -----------------------------------------------------------------------------
* |  3  |5/7/2004  | Added stop_event_handle var to AP globals       | Bindu  |
* -----------------------------------------------------------------------------
* |  2  |4/22/2004 | Added AL_DECLARE_GLOBAL_EXTERN's                | Bindu P|
* -----------------------------------------------------------------------------
* |  1  |4/22/2004 | **Made (major) changes for AL_CONTEXT**         | Bindu P|
* -----------------------------------------------------------------------------
* |  0  |4/9/2004  |  Created                                        | Bindu  |
* -----------------------------------------------------------------------------
********************************************************************************/

#ifndef __ACCESS_POINT_THREAD_H__
#define __ACCESS_POINT_THREAD_H__

int access_point_thread(AL_CONTEXT_PARAM_DECL al_thread_param_t *param);
int tx_thread(void);
void imcp_thread(void);

void process_al_802_11_fc_type_mgt_packet(AL_CONTEXT_PARAM_DECL al_net_if_t *net_if, int sub_type, al_packet_t *packet);

//void process_al_802_11_fc_type_ctrl_packet(AL_CONTEXT_PARAM_DECL al_net_if_t* net_if, int sub_type, al_packet_t* packet);
void process_al_802_11_fc_type_data_packet(AL_CONTEXT_PARAM_DECL al_net_if_t *net_if, int sub_type, al_packet_t *packet);

#endif /*__ACCESS_POINT_THREAD_H__*/
