/********************************************************************************
* MeshDynamics
* --------------
* File     : access_point_data.h
* Comments : Process Data Packets
* Created  : 4/12/2004
* Author   : Bindu Parakala
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  6  |8/22/2008 | Changes for SIP                                 |Abhijit |
* -----------------------------------------------------------------------------
* |  5  |01/01/2006| Additions for analyser                          | Abhijit|
* -----------------------------------------------------------------------------
* |  4  |12/8/2004 | Packet type checking fixed                      | Sriram |
* -----------------------------------------------------------------------------
* |  3  |6/11/2004 | packet_is_imcp_udp_ip made static AL_INLINE     | Sriram |
* -----------------------------------------------------------------------------
* |  2  |5/11/2004 | packet_is_imcp_udp_ip() added as static         | Anand  |
* -----------------------------------------------------------------------------
* |  1  |4/22/2004 | **Made (major) changes for AL_CONTEXT**         | Bindu  |
* -----------------------------------------------------------------------------
* |  0  |4/12/2004 | Created                                         | Bindu  |
* -----------------------------------------------------------------------------
********************************************************************************/

#include "al.h"
#include "al_packet.h"
#include "al_net_if.h"
#include <linux/kernel.h>
#include <linux/module.h>

#ifndef __ACCESS_POINT_DATA_H__
#define __ACCESS_POINT_DATA_H__

void process_al_802_11_fc_stype_data_packet(AL_CONTEXT_PARAM_DECL al_net_if_t *net_if, al_packet_t *packet);

#define _IP_BYTE_POSITION_PROTOCOL        (9)
#define _IP_PROTO_UDP                     17                           /* user datagram protocol */
#define _IP_PROTO_ICMP                    1                            /* icmp */
#define _IP_MIN_HEADER_LENGTH             20
#define _UDP_WORD_POSITION_SRC_PORT       0
#define _UDP_WORD_POSITION_DST_PORT       (_UDP_WORD_POSITION_SRC_PORT + 2)
#define _UDP_WORD_POSITION_UDP_LENGTH     (_UDP_WORD_POSITION_DST_PORT + 2)
#define _UDP_WORD_POSITION_UDP_CHKSUM     (_UDP_WORD_POSITION_UDP_LENGTH + 2)
#define _UDP_POSITION_DATA                (_UDP_WORD_POSITION_UDP_CHKSUM + 2)
#define _UDP_HEADER_LENGTH                (_UDP_POSITION_DATA)

#define _ACCESS_POINT_IMCP_PORT_BYTE_2    0xDE
#define _ACCESS_POINT_IMCP_PORT_BYTE_1    0xDE
#define _ACCESS_POINT_LQCP_PORT_BYTE_2    0xEE
#define _ACCESS_POINT_LQCP_PORT_BYTE_1    0xEE

#define _IMCP_SIGNATURE_SIZE              4
#define _LQCP_SIGNATURE_SIZE              4

#define _DHCP_SERVER_PORT                 67

static AL_INLINE int _packet_is_sip_udp_ip(AL_CONTEXT_PARAM_DECL al_packet_t *packet, unsigned short sip_port)
{
   unsigned char  *p;
   unsigned short dst_port;

   if (packet->type != al_be16_to_cpu(AL_PACKET_TYPE_IP))
   {
      return -1;
   }

   if (packet->data_length < 28)
   {
      return -2;
   }

   p = packet->buffer + packet->position;

   /**
    * Make sure its a UDP packet
    */

   if (p[_IP_BYTE_POSITION_PROTOCOL] != _IP_PROTO_UDP)
   {
      return -3;
   }

   memcpy(&dst_port, &p[_IP_MIN_HEADER_LENGTH + _UDP_WORD_POSITION_DST_PORT], 2);
   dst_port = al_be16_to_cpu(dst_port);

   if (dst_port != sip_port)
   {
      return -4;
   }

   return 0;
}


static AL_INLINE int _packet_is_dhcp_udp_ip(AL_CONTEXT_PARAM_DECL al_packet_t *packet)
{
   unsigned char  *p;
   unsigned short dst_port;

   if (packet->type != al_be16_to_cpu(AL_PACKET_TYPE_IP))
   {
      return -1;
   }

   if (packet->data_length < 28)
   {
      return -2;
   }

   p = packet->buffer + packet->position;

   /**
    * Make sure its a UDP packet
    */

   if (p[_IP_BYTE_POSITION_PROTOCOL] != _IP_PROTO_UDP)
   {
      return -3;
   }

   memcpy(&dst_port, &p[_IP_MIN_HEADER_LENGTH + _UDP_WORD_POSITION_DST_PORT], 2);
   dst_port = al_be16_to_cpu(dst_port);

   if (dst_port != _DHCP_SERVER_PORT)
   {
      return -4;
   }

   return 0;
}


static AL_INLINE int _packet_is_imcp_udp_ip(AL_CONTEXT_PARAM_DECL al_packet_t *packet)
{
   static const unsigned char _imcp_signature[] =
   {
      'I', 'M', 'C', 'P'
   };

   unsigned char *p;

   /**
    * Make sure its a IP packet
    */


   if (packet->type != al_be16_to_cpu(AL_PACKET_TYPE_IP))
   {
      return -1;
   }

   if (packet->data_length < 32)
   {
      return -2;
   }

   p = packet->buffer + packet->position;

   /**
    * Make sure its a UDP packet
    */

   if ((p[_IP_BYTE_POSITION_PROTOCOL] != _IP_PROTO_UDP) ||
       (p[_IP_MIN_HEADER_LENGTH + _UDP_WORD_POSITION_DST_PORT] != _ACCESS_POINT_IMCP_PORT_BYTE_2) ||
       (p[_IP_MIN_HEADER_LENGTH + _UDP_WORD_POSITION_DST_PORT + 1] != _ACCESS_POINT_IMCP_PORT_BYTE_1))
   {
      return -3;
   }

   p += _IP_MIN_HEADER_LENGTH + _UDP_HEADER_LENGTH;

   if (!memcmp(p, _imcp_signature, _IMCP_SIGNATURE_SIZE))
   {
      return 0;
   }

   return -4;
}


static AL_INLINE int _packet_is_lqcp_udp_ip(AL_CONTEXT_PARAM_DECL al_packet_t *packet)
{
   static unsigned char _lqcp_signature[] =
   {
      'L', 'Q', 'C', 'P'
   };

   unsigned char *p;

   /**
    * Make sure its a IP packet
    */

   AL_PRINT_LOG_FLOW_0("AP		: _packet_is_lqcp_udp_ip()");

   if (packet->type != al_be16_to_cpu(AL_PACKET_TYPE_IP))
   {
      return -1;
   }

   if (packet->data_length < 32)
   {
      return -2;
   }

   p = packet->buffer + packet->position;

   /**
    * Make sure its a UDP packet
    */

   if ((p[_IP_BYTE_POSITION_PROTOCOL] != _IP_PROTO_UDP) ||
       (p[_IP_MIN_HEADER_LENGTH + _UDP_WORD_POSITION_DST_PORT] != _ACCESS_POINT_LQCP_PORT_BYTE_2) ||
       (p[_IP_MIN_HEADER_LENGTH + _UDP_WORD_POSITION_DST_PORT + 1] != _ACCESS_POINT_LQCP_PORT_BYTE_1))
   {
      return -3;
   }

   p += _IP_MIN_HEADER_LENGTH + _UDP_HEADER_LENGTH;

   if (!memcmp(p, _lqcp_signature, _LQCP_SIGNATURE_SIZE))
   {
      return 0;
   }

   return -4;
}
#endif /*__ACCESS_POINT_DATA_H__*/
