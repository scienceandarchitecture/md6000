/********************************************************************************
* MeshDynamics
* --------------
* File     : access_point_multicast.h
* Comments : Access Point Multi-cast table
* Created  : 6/20/2007
* Author   : Prachiti Gaikwad
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  1  |8/14/2007 | access_point_mcast_purge_indirect_entries Added | Sriram |
* -----------------------------------------------------------------------------
* |  0  |6/20/2007 | Created.                                        |Prachiti|
* -----------------------------------------------------------------------------
********************************************************************************/


#ifndef __ACCESS_POINT_MULTICAST_H__
#define __ACCESS_POINT_MULTICAST_H__

int access_point_multicast_initialize(AL_CONTEXT_PARAM_DECL
                                      access_point_netif_config_info_t *wm_net_if_info);

int access_point_multicast_uninitialize(AL_CONTEXT_PARAM_DECL
                                        access_point_netif_config_info_t *wm_net_if_info);

void access_point_mcast_add_entry(AL_CONTEXT_PARAM_DECL
                                  access_point_netif_config_info_t *wm_net_if_info,
                                  al_net_addr_t                    *mcast_addr,
                                  al_net_addr_t                    *src_sta_addr);

int access_point_mcast_remove_entry(AL_CONTEXT_PARAM_DECL
                                    access_point_netif_config_info_t *wm_net_if_info,
                                    al_net_addr_t                    *mcast_addr,
                                    al_net_addr_t                    *src_sta_addr);

int access_point_remove_mcast_sta_entry(AL_CONTEXT_PARAM_DECL
                                        access_point_netif_config_info_t *wm_net_if_info,
                                        al_net_addr_t                    *src_sta_addr);

access_point_mcast_t *access_point_mcast_get_entry(AL_CONTEXT_PARAM_DECL
                                                   access_point_netif_config_info_t *wm_net_if_info,
                                                   al_net_addr_t                    *mcast_addr);

void access_point_mcast_purge_indirect_entries(AL_CONTEXT_PARAM_DECL
                                               access_point_netif_config_info_t *wm_net_if_info);

#endif
