/********************************************************************************
* MeshDynamics
* --------------
* File     : access_point_mgt.c
* Comments : Process Management Packets
* Created  : 4/12/2004
* Author   : Bindu Parakala
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* | 28  |7/17/2007 | authentication_time Added to STA INFO           | Sriram |
* -----------------------------------------------------------------------------
* | 27  |01/17/2006| Changes for aid                                 | Sriram |
* -----------------------------------------------------------------------------
* | 26  |9/22/2005 | STA fully released upon Dis-assoc               | Sriram |
* -----------------------------------------------------------------------------
* | 25  |11/16/2004| Change due to al_802_11 changes (Probe response)| Anand  |
* -----------------------------------------------------------------------------
* | 24  |7/24/2004 | sta_release call changes                        | Anand  |
* -----------------------------------------------------------------------------
* | 23  |7/22/2004 | Deauth processing, called release_sta_ex        | Bindu  |
* -----------------------------------------------------------------------------
* | 22  |7/2/2004  | Misc changes for release sta                    | Anand  |
* -----------------------------------------------------------------------------
* | 21  |6/30/2004 | Bug fixed check for pseudowm added           | Anand  |
* -----------------------------------------------------------------------------
* | 20  |6/21/2004 | Misc changes - access_point_release_sta         | Bindu  |
* -----------------------------------------------------------------------------
* | 19  |6/14/2004 | Added AL_ASSERT macro                           | Bindu  |
* -----------------------------------------------------------------------------
* | 18  |6/15/2004 | checking for ssid_length added in asoc/re-assoc | Bindu  |
* -----------------------------------------------------------------------------
* | 17  |6/10/2004 | Misc changes including re-assoc handling        | Sriram |
* -----------------------------------------------------------------------------
* | 16  |6/4/2004  | Print log macros used                           | Bindu  |
* -----------------------------------------------------------------------------
* | 15  |5/31/2004 | fixed misc bugs                                 | Bindu  |
* -----------------------------------------------------------------------------
* | 14  |5/31/2004 | included access_point_sta.h                     | Bindu  |
* -----------------------------------------------------------------------------
* | 13  |5/31/2004 | included access_point_thread.h etc              | Bindu  |
* -----------------------------------------------------------------------------
* | 12  |5/31/2004 | included access_point_globals.h                 | Bindu  |
* -----------------------------------------------------------------------------
* | 11  |5/21/2004 | Log type added to al_print_log                  | Anand  |
* -----------------------------------------------------------------------------
* | 10  |5/20/2004 | fixed al_print_log statements  & misc bugs      | Bindu  |
* -----------------------------------------------------------------------------
* |  9  |5/13/2004 | seperated assoc & re-assoc methods              | Bindu  |
* -----------------------------------------------------------------------------
* |  8  |5/12/2004 | status_code_out, supported_rates_out            | Bindu  |
* -----------------------------------------------------------------------------
* |  7  |5/7/2004  | bug fixed in assigment of net_if config         | Anand  |
* -----------------------------------------------------------------------------
* |  6  |5/6/2004  | Change in Auth packet processing                | Anand  |
* -----------------------------------------------------------------------------
* |  5  |5/5/2004  | Packet Debug params added                       | Anand  |
* -----------------------------------------------------------------------------
* |  4  |4/30/2004 | Added al_print_log sts                          | Bindu  |
* -----------------------------------------------------------------------------
* |  3  |4/23/2004 | Added packet rx & tx time of STA's to AP Queue  | Bindu  |
* -----------------------------------------------------------------------------
* |  2  |4/22/2004 | Made changes for packet buffer position setting | Bindu  |
* -----------------------------------------------------------------------------
* |  1  |4/22/2004 | **Made (major) changes for AL_CONTEXT**         | Bindu  |
* -----------------------------------------------------------------------------
* |  0  |4/12/2004 | Created                                         | Bindu  |
* -----------------------------------------------------------------------------
********************************************************************************/

#include <linux/string.h>
#include <linux/kernel.h>
#include "al.h"
#include "access_point.h"
#include "access_point_sta.h"
#include "al_impl_context.h"
#include "access_point_thread.h"
#include "access_point_sta.h"
#include "access_point_globals.h"
#include "access_point_mgt.h"
#include "al_print_log.h"
#include "mesh_defs.h"
#include "mesh.h"

extern void dump_bytes(char *D, int count);
extern void enable_disable_processing(AL_CONTEXT_PARAM_DECL unsigned char enable);
AL_DECLARE_GLOBAL_EXTERN(int mesh_state);

int process_al_802_11_fc_stype_re_assoc_req(AL_CONTEXT_PARAM_DECL al_packet_t *packet, unsigned char *supported_rates, unsigned char *supported_rates_length)
{
   unsigned short capability_info;
   unsigned short listen_interval_position;
   unsigned char  ssid[33];
   unsigned char  *p;
   unsigned char  element_id;
   unsigned char  ssid_length;
   al_net_addr_t  current_ap_addr;


	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MESHAP : FLOW : Enter %s: %d\n", __func__, __LINE__);

   AL_ASSERT("process_al_802_11_fc_stype_re_assoc_req", packet != NULL);

   /**
    *  RE-Association Request
    */

   p = packet->buffer + packet->position;

   capability_info          = AL_802_11_PACKET_GET_WORD16(p, AL_802_11_REASSOCIATION_REQUEST_CAPABILITY_POSITION);
   listen_interval_position = AL_802_11_PACKET_GET_WORD16(p, AL_802_11_REASSOCIATION_REQUEST_LISTENINTERVAL_POSITION);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "RE #2");

   memcpy(current_ap_addr.bytes, p + AL_802_11_REASSOCIATION_REQUEST_CURRENTAP_POSITION, 6);
   element_id = AL_802_11_PACKET_GET_BYTE(p, AL_802_11_REASSOCIATION_REQUEST_SSID_POSITION);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "RE #3");
   ssid_length = AL_802_11_PACKET_GET_BYTE(p, AL_802_11_REASSOCIATION_REQUEST_SSID_POSITION + 1);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "RE #4");

   memset(ssid, 0, sizeof(ssid));
   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "RE #5");

   if (element_id == AL_802_11_EID_SSID)
   {
      memcpy(ssid, p + AL_802_11_REASSOCIATION_REQUEST_SSID_START_POSITION, ssid_length);
   }

   element_id = AL_802_11_PACKET_GET_BYTE(p, AL_802_11_REASSOCIATION_REQUEST_SUPPORTEDRATES_POSITION(ssid_length));
   *supported_rates_length = AL_802_11_PACKET_GET_BYTE(p, AL_802_11_REASSOCIATION_REQUEST_SUPPORTEDRATES_POSITION(ssid_length) + 1);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "RE #6");

   if (element_id == AL_802_11_EID_SUPP_RATES)
   {
      memcpy(supported_rates, p + AL_802_11_REASSOCIATION_REQUEST_SUPPORTEDRATES_START_POSITION(ssid_length), *supported_rates_length);
   }
   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "RE #7");

	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MESHAP : FLOW : Exit %s: %d\n", __func__, __LINE__);
   return 0;
}


int process_al_802_11_fc_stype_assoc_req_helper(AL_CONTEXT_PARAM_DECL al_packet_t *packet, unsigned char *supported_rates, unsigned char *supported_rates_length)
{
   unsigned short capability_info;
   unsigned short listen_interval_position;
   unsigned char  ssid[33];
   unsigned char  *p;
   unsigned char  element_id;
   unsigned char  ssid_length;


   AL_ASSERT("process_al_802_11_fc_stype_assoc_req_helper", packet != NULL);
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MESHAP : FLOW : Enter %s: %d\n", __func__, __LINE__);
   p = packet->buffer + packet->position;

   /**
    * Processing Association Request
    */

   capability_info          = AL_802_11_PACKET_GET_WORD16(p, AL_802_11_ASSOCIATION_REQUEST_CAPABILITY_POSITION);
   listen_interval_position = AL_802_11_PACKET_GET_WORD16(p, AL_802_11_ASSOCIATION_REQUEST_LISTENINTERVAL_POSITION);
   element_id  = AL_802_11_PACKET_GET_BYTE(p, AL_802_11_ASSOCIATION_REQUEST_SSID_POSITION);
   ssid_length = AL_802_11_PACKET_GET_BYTE(p, AL_802_11_ASSOCIATION_REQUEST_SSID_POSITION + 1);

   memset(ssid, 0, sizeof(ssid));

   if (element_id == AL_802_11_EID_SSID)
   {
      memcpy(ssid, p + AL_802_11_ASSOCIATION_REQUEST_SSID_START_POSITION, ssid_length);
   }

   element_id = AL_802_11_PACKET_GET_BYTE(p, AL_802_11_ASSOCIATION_REQUEST_SUPPORTEDRATES_POSITION(ssid_length));
   *supported_rates_length = AL_802_11_PACKET_GET_BYTE(p, AL_802_11_ASSOCIATION_REQUEST_SUPPORTEDRATES_POSITION(ssid_length) + 1);

   if (element_id == AL_802_11_EID_SUPP_RATES)
   {
      memcpy(supported_rates, p + AL_802_11_ASSOCIATION_REQUEST_SUPPORTEDRATES_START_POSITION(ssid_length), *supported_rates_length);
   }
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MESHAP : FLOW : Exit %s: %d\n", __func__, __LINE__);
   return 0;
}

void process_al_802_11_fc_stype_probe_req_packet(AL_CONTEXT_PARAM_DECL al_net_if_t *net_if, al_packet_t *packet)
{
   al_net_if_config_t       *net_if_config;
   unsigned char            *p;
   unsigned char            ssid[33];
   unsigned char            ssid_length;
   unsigned char            element_id;
   unsigned char            supported_rates[8];
   unsigned char            supported_rates_length;
   unsigned char            supported_rates_out[8];
   unsigned char            supported_rates_length_out;
   al_802_11_operations_t   *extended_operations;
   al_packet_t              *response_packet;
   access_point_sta_entry_t *sta;
   unsigned short           our_beacon_interval;
   unsigned short           our_capability_info;
   unsigned char            our_channel;
   al_u64_t                 timestamp;

	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MESHAP : FLOW : Enter %s: %d\n", __func__, __LINE__);
   AL_ASSERT("process_al_802_11_fc_stype_probe_req_packet", net_if != NULL);
   AL_ASSERT("process_al_802_11_fc_stype_probe_req_packet", packet != NULL);

   net_if_config = &net_if->config;

   extended_operations = net_if->get_extended_operations(net_if);
   AL_ASSERT("process_al_802_11_fc_stype_probe_req_packet", extended_operations != NULL);

   sta = access_point_get_sta(AL_CONTEXT & packet->addr_2);

   if (!AL_NET_ADDR_EQUAL((&net_if_config->hw_addr), (&packet->addr_1)) ||
       !AL_NET_ADDR_EQUAL((&net_if_config->hw_addr), (&packet->addr_3)))
   {
      if (sta != NULL)
      {
         access_point_release_sta(AL_CONTEXT sta);
      }
		al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MESHAP : INFO : Drop probe request not destined to us. hw_addr: "MACSTR" addr_1 "MACSTR" addr_2 "MACSTR" %s: %d\n", MAC2STR(net_if_config->hw_addr.bytes), MAC2STR(packet->addr_1.bytes), MAC2STR(packet->addr_3.bytes), __func__, __LINE__);
      return;
   }

   sta->sta.last_packet_received = al_get_tick_count(AL_CONTEXT_SINGLE);

   p = packet->buffer + packet->position;

   extended_operations->get_supported_rates(net_if, supported_rates_out, sizeof(supported_rates_out));
   supported_rates_length_out = sizeof(supported_rates_out);

   element_id  = AL_802_11_PACKET_GET_BYTE(p, AL_802_11_PROBE_REQUEST_SSID_POSITION);
   ssid_length = AL_802_11_PACKET_GET_BYTE(p, AL_802_11_PROBE_REQUEST_SSID_POSITION + 1);

   memset(ssid, 0, sizeof(ssid));

   if (element_id == AL_802_11_EID_SSID)
   {
      memcpy(ssid, p + AL_802_11_PROBE_REQUEST_SSID_START_POSITION, ssid_length);
   }

   element_id             = AL_802_11_PACKET_GET_BYTE(p, AL_802_11_PROBE_REQUEST_SUPPORTEDRATES_POSITION(ssid_length));
   supported_rates_length = AL_802_11_PACKET_GET_BYTE(p, AL_802_11_PROBE_REQUEST_SUPPORTEDRATES_POSITION(ssid_length) + 1);

   memset(supported_rates, 0, sizeof(supported_rates));

   if (element_id == AL_802_11_EID_SUPP_RATES)
   {
      memcpy(supported_rates, p + AL_802_11_PROBE_REQUEST_SUPPORTEDRATES_START_POSITION(ssid_length), supported_rates_length);
   }

   if (on_probe_request_handler != NULL)
   {
      on_probe_request_handler(AL_CONTEXT net_if,
                               &sta->sta.address,
                               supported_rates,
                               supported_rates_length,
                               supported_rates_out,
                               &supported_rates_length_out);
   }

   response_packet = al_allocate_packet(AL_CONTEXT AL_PACKET_DIRECTION_OUT AL_PACKET_DEBUG_PARAM);

   memcpy(&response_packet->addr_1, &packet->addr_2, sizeof(al_net_addr_t));
   memcpy(&response_packet->addr_2, &net_if_config->hw_addr, sizeof(al_net_addr_t));
   memcpy(&response_packet->addr_3, &net_if_config->hw_addr, sizeof(al_net_addr_t));

   our_beacon_interval = extended_operations->get_beacon_interval(net_if);
   our_capability_info = AL_802_11_CAPABILITY_ESS;
   our_channel         = extended_operations->get_channel(net_if);
   timestamp           = al_get_tick_count(AL_CONTEXT_SINGLE);

   response_packet->position    -= (AL_802_11_PROBE_RESP_MIN_SIZE + ssid_length);
   response_packet->data_length += (AL_802_11_PROBE_RESP_MIN_SIZE + ssid_length);

   p = response_packet->buffer + response_packet->position;

   memcpy(p, &timestamp, sizeof(al_u64_t));
   p += sizeof(al_u64_t);

   AL_802_11_PACKET_SET_WORD16(p, AL_802_11_PROBE_RESPONSE_BEACON_INTERVAL, our_beacon_interval);
   AL_802_11_PACKET_SET_WORD16(p, AL_802_11_PROBE_RESPONSE_CAPABILITY_POSITION, our_capability_info);

   AL_802_11_PACKET_SET_BYTE(p, AL_802_11_PROBE_RESPONSE_SSID_POSITION, AL_802_11_EID_SSID);
   AL_802_11_PACKET_SET_BYTE(p, AL_802_11_PROBE_RESPONSE_SSID_POSITION + 1, ssid_length);
   memcpy(p + AL_802_11_PROBE_RESPONSE_SSID_START_POSITION, ssid, ssid_length);


   AL_802_11_PACKET_SET_BYTE(p, AL_802_11_PROBE_RESPONSE_SUPPORTEDRATES_POSITION(ssid_length), AL_802_11_EID_SUPP_RATES);
   AL_802_11_PACKET_SET_BYTE(p, AL_802_11_PROBE_RESPONSE_SUPPORTEDRATES_POSITION(ssid_length) + 1, supported_rates_length_out);
   memcpy(p + AL_802_11_PROBE_RESPONSE_SUPPORTEDRATES_START_POSITION(ssid_length), supported_rates_out, supported_rates_length_out);

   AL_802_11_PACKET_SET_BYTE(p, AL_802_11_PROBE_RESPONSE_DS_PARAM_POSITION(ssid_length, supported_rates_length_out), AL_802_11_EID_DS_PARAMS);
   AL_802_11_PACKET_SET_BYTE(p, AL_802_11_PROBE_RESPONSE_SUPPORTEDRATES_POSITION(ssid_length) + 1, 1);
   AL_802_11_PACKET_SET_BYTE(p, AL_802_11_PROBE_RESPONSE_DS_PARAM_START_POSITION(ssid_length, supported_rates_length_out), our_channel);

   sta->sta.last_packet_transmitted = al_get_tick_count(AL_CONTEXT_SINGLE);
   extended_operations->send_management_frame(net_if, AL_802_11_FC_STYPE_PROBE_RESP, response_packet);
   if (sta != NULL)
   {
      access_point_release_sta(AL_CONTEXT sta);
   }
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MESHAP : FLOW : Exit %s: %d\n", __func__, __LINE__);
}


void process_al_802_11_fc_stype_disassoc_packet(AL_CONTEXT_PARAM_DECL al_net_if_t *net_if, al_packet_t *packet)
{
   al_net_if_config_t       *net_if_config;
   access_point_sta_entry_t *sta;
   al_802_11_operations_t   *operations;

   AL_ASSERT("process_al_802_11_fc_stype_disassoc_packet", net_if != NULL);
   AL_ASSERT("process_al_802_11_fc_stype_disassoc_packet", packet != NULL);

   net_if_config = &net_if->config;
   AL_ASSERT("process_al_802_11_fc_stype_disassoc_packet", net_if_config != NULL);

   if (!AL_NET_ADDR_EQUAL((&net_if_config->hw_addr), (&packet->addr_1)) ||
       !AL_NET_ADDR_EQUAL((&net_if_config->hw_addr), (&packet->addr_3)))
   {
	  (tx_rx_pkt_stats.packet_drop[81])++;
      return;
   }


   sta = access_point_get_sta(AL_CONTEXT & packet->addr_2);

   if (sta == NULL)
   {
	  (tx_rx_pkt_stats.packet_drop[82])++;
      return;
   }

   access_point_release_sta(AL_CONTEXT sta);
   access_point_release_sta_ex(AL_CONTEXT sta, 1, 0);
}


void process_al_802_11_fc_stype_auth_packet(AL_CONTEXT_PARAM_DECL al_net_if_t *net_if, al_packet_t *packet)
{
   al_net_if_config_t       *net_if_config;
   unsigned char            *p;
   unsigned short           algorithm;
   unsigned short           atsn;
   unsigned short           status;
   int                      algorithm_specific_info_length;
   unsigned short           status_out;
   int                      algorithm_specific_info_length_out;
   unsigned char            *algorithm_specific_info_out;
   access_point_sta_entry_t *sta_entry;
   al_packet_t              *response_packet;
   al_802_11_operations_t   *extended_operations;

	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MESHAP : FLOW : Enter %s: %d\n", __func__, __LINE__);

   AL_ASSERT("process_al_802_11_fc_stype_auth_packet", net_if != NULL);
   AL_ASSERT("process_al_802_11_fc_stype_auth_packet", packet != NULL);

   net_if_config = &net_if->config;
   AL_ASSERT("process_al_802_11_fc_stype_auth_packet", net_if_config != NULL);

   if (!AL_NET_ADDR_EQUAL((&net_if_config->hw_addr), (&packet->addr_1)) ||
       !AL_NET_ADDR_EQUAL((&net_if_config->hw_addr), (&packet->addr_3)))
   {
		al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MESHAP : INFO : Drop auth not destined to us. hw_addr: "MACSTR" addr_1 "MACSTR" addr_2 "MACSTR" %s: %d\n", MAC2STR(net_if_config->hw_addr.bytes), MAC2STR(packet->addr_1.bytes), MAC2STR(packet->addr_3.bytes), __func__, __LINE__);
      return;
   }


   p = packet->buffer + packet->position;

   algorithm = AL_802_11_PACKET_GET_WORD16(p, AL_802_11_AUTHENTICATION_ALGORITHM_POSITION);
   atsn      = AL_802_11_PACKET_GET_WORD16(p, AL_802_11_AUTHENTICATION_ATSN_POSITION);
   status    = AL_802_11_PACKET_GET_WORD16(p, AL_802_11_AUTHENTICATION_STATUS_POSITION);

   algorithm_specific_info_length = packet->data_length - (packet->position + AL_802_11_AUTHENTICATION_CHALLENGETEXT_POSITION);

   if (on_auth_request_handler != NULL)
   {
      on_auth_request_handler(AL_CONTEXT net_if,
                              &packet->addr_2,
                              algorithm,
                              atsn,
                              status,
                              p + AL_802_11_AUTHENTICATION_CHALLENGETEXT_POSITION,
                              algorithm_specific_info_length,
                              &status_out,
                              &algorithm_specific_info_out,
                              &algorithm_specific_info_length_out);
   }
   else
   {
      if (algorithm == AL_802_11_AUTHENTICATION_OPEN)
      {
         status_out = AL_802_11_STATUS_SUCCESS;
         algorithm_specific_info_length_out = 0;
         algorithm_specific_info_out        = NULL;
      }
      else
      {
         status_out = AL_802_11_STATUS_NOT_SUPPORTED_AUTH_ALG;
         algorithm_specific_info_length_out = 0;
         algorithm_specific_info_out        = NULL;
      }
   }

   /**
    * Make an entry for this STA if successfully authenticated
    */

   sta_entry = NULL;

   if ((status_out == AL_802_11_STATUS_SUCCESS) || (status_out == AL_802_11_STATUS_NOT_SUPPORTED_AUTH_ALG))
   {
      sta_entry            = access_point_add_sta(AL_CONTEXT net_if, &packet->addr_2);
      sta_entry->sta.state = ACCESS_POINT_STA_STATE_AUTHENTICATED;
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MESHAP: INFO : STA_STATE CHANGE :%d:%d:%d new State = ACCESS_POINT_STA_STATE_AUTHENTICATED %s: %d", sta_entry->sta.address.bytes[3], sta_entry->sta.address.bytes[4], sta_entry->sta.address.bytes[5], __func__, __LINE__);
      sta_entry->sta.last_packet_received = al_get_tick_count(AL_CONTEXT_SINGLE);
   }

   if (algorithm_specific_info_out == NULL)
   {
      algorithm_specific_info_length_out = 0;
   }

   /**
    * Send out authentication response
    */

   ++atsn;

   response_packet = al_allocate_packet(AL_CONTEXT AL_PACKET_DIRECTION_OUT AL_PACKET_DEBUG_PARAM);

   memcpy(&response_packet->addr_1, &packet->addr_2, sizeof(al_net_addr_t));
   memcpy(&response_packet->addr_2, &net_if_config->hw_addr, sizeof(al_net_addr_t));
   memcpy(&response_packet->addr_3, &net_if_config->hw_addr, sizeof(al_net_addr_t));

   response_packet->position    -= (AL_802_11_AUTHENTICATION_MIN_SIZE + algorithm_specific_info_length_out);
   response_packet->data_length += (AL_802_11_AUTHENTICATION_MIN_SIZE + algorithm_specific_info_length_out);

   p = response_packet->buffer + response_packet->position;

   packet->data_length = (6 + algorithm_specific_info_length_out);

   AL_802_11_PACKET_SET_WORD16(p, AL_802_11_AUTHENTICATION_ALGORITHM_POSITION, algorithm);
   AL_802_11_PACKET_SET_WORD16(p, AL_802_11_AUTHENTICATION_ATSN_POSITION, atsn);
   AL_802_11_PACKET_SET_WORD16(p, AL_802_11_AUTHENTICATION_STATUS_POSITION, status_out);

   if (algorithm_specific_info_out != NULL)
   {
      memcpy(p + AL_802_11_AUTHENTICATION_CHALLENGETEXT_POSITION, algorithm_specific_info_out, algorithm_specific_info_length_out);
   }

   extended_operations = net_if->get_extended_operations(net_if);
   AL_ASSERT("process_al_802_11_fc_stype_auth_packet", extended_operations != NULL);

   if (sta_entry != NULL)
   {
      sta_entry->sta.last_packet_transmitted = al_get_tick_count(AL_CONTEXT_SINGLE);
   }

   extended_operations->send_management_frame(net_if, AL_802_11_FC_STYPE_AUTH, response_packet);
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MESHAP : FLOW : Exit %s: %d\n", __func__, __LINE__);
}


void process_al_802_11_fc_stype_deauth_packet(AL_CONTEXT_PARAM_DECL al_net_if_t *net_if, al_packet_t *packet)
{
   al_net_if_config_t       *net_if_config;
   unsigned short           deauth_reason_code;
   access_point_sta_entry_t *sta;
   unsigned char            *p;
   al_u64_t                 tick_count;
   al_u64_t                 diff;
   unsigned long            flags;


   AL_ASSERT("process_al_802_11_fc_stype_deauth_packet", net_if != NULL);
   AL_ASSERT("process_al_802_11_fc_stype_deauth_packet", packet != NULL);

   net_if_config = &net_if->config;
   AL_ASSERT("process_al_802_11_fc_stype_deauth_packet", net_if_config != NULL);

   if (!AL_NET_ADDR_EQUAL((&net_if_config->hw_addr), (&packet->addr_1)) ||
       !AL_NET_ADDR_EQUAL((&net_if_config->hw_addr), (&packet->addr_3)))
   {
	  (tx_rx_pkt_stats.packet_drop[83])++;
      return;
   }


   sta = access_point_get_sta(AL_CONTEXT & packet->addr_2);

   if (sta == NULL)
   {
	  (tx_rx_pkt_stats.packet_drop[84])++;
      return;
   }


   tick_count = al_get_tick_count(AL_CONTEXT_SINGLE);
   diff       = (al_s64_t)(tick_count - sta->sta.authentication_time);

   if ((sta->sta.state == ACCESS_POINT_STA_STATE_ASSOCIATED) ||
       (sta->sta.state == ACCESS_POINT_STA_STATE_AUTHENTICATED))
   {
      p = packet->buffer + packet->position;
      deauth_reason_code = AL_802_11_PACKET_GET_WORD16(p, AL_802_11_DEAUTHENTICATION_NOTIFICATION_REASON_CODE_POSITION);

      if (on_deauth_handler != NULL)
      {
         on_deauth_handler(AL_CONTEXT net_if, &sta->sta.address, deauth_reason_code);
      }
   }

release_sta:
   access_point_release_sta(AL_CONTEXT sta);
   access_point_release_sta_ex(AL_CONTEXT sta, 1, 0);
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MESHAP : FLOW : Exit %s: %d\n", __func__, __LINE__);
}

void send_deauth_notification(AL_CONTEXT_PARAM_DECL al_net_if_t *net_if, al_net_addr_t *dest, unsigned short reason_code)
{
   al_packet_t *packet;
   al_802_11_operations_t *extended_ops;
   unsigned char *p;

	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MESHAP : FLOW : Enter %s: %d\n", __func__, __LINE__);
   AL_ASSERT("send_deauth_notification", net_if != NULL);
   AL_ASSERT("send_deauth_notification", dest != NULL);

   extended_ops = (al_802_11_operations_t *)net_if->get_extended_operations(net_if);

   if (extended_ops == NULL)
   {
		al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESHAP : ERROR : Invalid ops %s: %d\n", __func__, __LINE__);
      return;
   }

   packet = al_allocate_packet(AL_CONTEXT AL_PACKET_DIRECTION_OUT AL_PACKET_DEBUG_PARAM);

   if (packet == NULL) {
		al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESHAP : ERROR : Memory allocation Failed %s: %d\n", __func__, __LINE__);
   	return;
   }

   packet->position    -= AL_802_11_DEAUTH_MIN_SIZE;
   packet->data_length += AL_802_11_DEAUTH_MIN_SIZE;

   p = packet->buffer + packet->position;

   memcpy(&packet->addr_1, dest, sizeof(al_net_addr_t));
   memcpy(&packet->addr_2, &net_if->config.hw_addr, sizeof(al_net_addr_t));
   memcpy(&packet->addr_3, &net_if->config.hw_addr, sizeof(al_net_addr_t));

   AL_802_11_PACKET_SET_WORD16(p, AL_802_11_DEAUTHENTICATION_NOTIFICATION_REASON_CODE_POSITION, reason_code);

   extended_ops->send_management_frame(net_if, AL_802_11_FC_STYPE_DEAUTH, packet);
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MESHAP : FLOW : Exit %s: %d\n", __func__, __LINE__);
}


void send_disassoc_notification(AL_CONTEXT_PARAM_DECL al_net_if_t *net_if, al_net_addr_t *dest, unsigned short reason_code)
{
   al_packet_t *packet;
   al_802_11_operations_t *extended_ops;
   unsigned char *p;

   AL_ASSERT("send_disassoc_notification", net_if != NULL);
   AL_ASSERT("send_disassoc_notification", dest != NULL);
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MESHAP : FLOW : Enter %s: %d\n", __func__, __LINE__);

   extended_ops = (al_802_11_operations_t *)net_if->get_extended_operations(net_if);

   if (AL_NET_IF_IS_ETHERNET(net_if))
   {
		al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "MESHAP : ERROR : Disassoc received on Non-wlan interface %s: %d\n", __func__, __LINE__);
      return;
   }

   packet = al_allocate_packet(AL_CONTEXT AL_PACKET_DIRECTION_OUT AL_PACKET_DEBUG_PARAM);

   packet->position    -= AL_802_11_DISASSOC_MIN_SIZE;
   packet->data_length += AL_802_11_DISASSOC_MIN_SIZE;

   p = packet->buffer + packet->position;

   memcpy(&packet->addr_1, dest, sizeof(al_net_addr_t));
   memcpy(&packet->addr_2, &net_if->config.hw_addr, sizeof(al_net_addr_t));
   memcpy(&packet->addr_3, &net_if->config.hw_addr, sizeof(al_net_addr_t));

   AL_802_11_PACKET_SET_WORD16(p, AL_802_11_DISASSOCIATION_NOTIFICATION_REASON_CODE_POSITION, reason_code);

   extended_ops->send_management_frame(net_if, AL_802_11_FC_STYPE_DISASSOC, packet);
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW, "MESHAP : FLOW : Exit %s: %d\n", __func__, __LINE__);
}
