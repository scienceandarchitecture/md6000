/********************************************************************************
* MeshDynamics
* --------------
* File     : access_point_acl.c
* Comments : ACL list implementation
* Created  : 3/13/2006
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  3  |4/19/2007 |  Changes for forced disassociation              | Sriram |
* -----------------------------------------------------------------------------
* |  2  |11/14/2006| Changes for heap debug support                  | Sriram |
* -----------------------------------------------------------------------------
* |  1  |6/14/2006 | Changes for disallow_other_sta	              |Prachiti|
* -----------------------------------------------------------------------------
* |  0  |3/13/2006 | Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

#include <linux/string.h>

#include "al_context.h"
#include "al_802_11.h"
#include "al.h"
#include "al_conf.h"
#include "al_ip.h"
#include "access_point.h"
#include "al_assert.h"
#include "access_point_vlan.h"
#include "al_print_log.h"
#include "access_point_globals.h"
#include "access_point_sta.h"

/**
 * The hash function for STAs
 */

#ifdef  __GNUC__
#define _ACCESS_POINT_ACL_STA_HASH_FUNCTION(addr) \
   ({                                             \
      int hash;                                   \
      hash = (addr)->bytes[0] * 1                 \
             + (addr)->bytes[1] * 2               \
             + (addr)->bytes[2] * 3               \
             + (addr)->bytes[3] * 4               \
             + (addr)->bytes[4] * 5               \
             + (addr)->bytes[5] * 6;              \
      hash;                                       \
   }                                              \
   )
#else
static int _ACCESS_POINT_ACL_STA_HASH_FUNCTION(al_net_addr_t *addr)
{
   int hash;                      \
   hash = (addr)->bytes[0] * 1    \
          + (addr)->bytes[1] * 2  \
          + (addr)->bytes[2] * 3  \
          + (addr)->bytes[3] * 4  \
          + (addr)->bytes[4] * 5  \
          + (addr)->bytes[5] * 6; \
   return hash;                   \
}
#endif

al_spinlock_t acl_splock;
AL_DEFINE_PER_CPU(int, acl_spvar);

struct _access_point_acl_entry_info
{
   access_point_acl_entry_t            entry;
   access_point_vlan_info_t            *vlan_info;
   int                                 hash_value;
   struct _access_point_acl_entry_info *next_hash;
   struct _access_point_acl_entry_info *next_list;
};

typedef struct _access_point_acl_entry_info   _access_point_acl_entry_info_t;

AL_DECLARE_GLOBAL(_access_point_acl_entry_info_t * *_acl_hash);
AL_DECLARE_GLOBAL(_access_point_acl_entry_info_t * _acl_list_head);
AL_DECLARE_GLOBAL(int _acl_hash_length);
AL_DECLARE_GLOBAL(int _disallow_other_stas);

void access_point_acl_initialize(AL_CONTEXT_PARAM_DECL_SINGLE)
{
   /**
    * Initialize the Hash table and the List
    */
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
	al_spin_lock_init(&acl_splock);

   _acl_hash_length     = 13;
   _disallow_other_stas = 0;
   _acl_hash            = (_access_point_acl_entry_info_t **)al_heap_alloc(AL_CONTEXT sizeof(_access_point_acl_entry_info_t *) * _acl_hash_length AL_HEAP_DEBUG_PARAM);
   memset(_acl_hash, 0, sizeof(_access_point_acl_entry_info_t *) * _acl_hash_length);

   _acl_list_head = NULL;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
}


void access_point_acl_uninitialize(AL_CONTEXT_PARAM_DECL_SINGLE)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
   _access_point_acl_entry_info_t *temp;

   /**
    * First clear up the list, and then the Hash table
    */

   temp = _acl_list_head;

   while (temp != NULL)
   {
      _acl_list_head = temp->next_list;
      al_heap_free(AL_CONTEXT temp);
      temp = _acl_list_head;
   }

   al_heap_free(AL_CONTEXT _acl_hash);

   _acl_list_head       = NULL;
   _acl_hash            = NULL;
   _disallow_other_stas = 0;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
}


void access_point_acl_set_list(AL_CONTEXT int count, access_point_acl_entry_t *entries)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
   unsigned long flags;
   _access_point_acl_entry_info_t *temp;
   int i;
   int bucket;
   access_point_sta_entry_t *sta_entry;

   al_spin_lock(acl_splock, acl_spvar);

   temp = _acl_list_head;

   while (temp != NULL)
   {
      _acl_list_head = temp->next_list;
      al_heap_free(AL_CONTEXT temp);
      temp = _acl_list_head;
   }

   _acl_list_head = NULL;

   for (i = 0; i < _acl_hash_length; i++)
   {
      _acl_hash[i] = NULL;
   }

   _disallow_other_stas = 0;

   al_spin_unlock(acl_splock, acl_spvar);

   /**
    * Now we add up the new entries to the list and to the Hash
    * After that we send disconnect messages to entries that
    * have been blocked. Entries for which VLAN's have been changed
    * will have no effect until we reboot
    */

   /**
    * If we find the FF:FF:FF:FF:FF:FF with allow = false in the list then,
    * do not add the special entry in the Hash and set the disallow_other_stas field = true
    */

   for (i = 0; i < count; i++)
   {
      if (AL_NET_ADDR_IS_BROADCAST(&entries[i].sta_mac))
      {
         if (!(entries[i].allow))                 /*This is a special entry*/
         {
            _disallow_other_stas = 1;
         }
         continue;
      }

      temp = (_access_point_acl_entry_info_t *)al_heap_alloc(AL_CONTEXT sizeof(_access_point_acl_entry_info_t)AL_HEAP_DEBUG_PARAM);

      memset(temp, 0, sizeof(_access_point_acl_entry_info_t));

      memcpy(&temp->entry, &entries[i], sizeof(access_point_acl_entry_t));

      if (_acl_list_head != NULL)
      {
         temp->next_list = _acl_list_head;
      }

      _acl_list_head = temp;

      temp->hash_value = _ACCESS_POINT_ACL_STA_HASH_FUNCTION(&temp->entry.sta_mac);
      bucket           = temp->hash_value % _acl_hash_length;

      if (_acl_hash[bucket] != NULL)
      {
         temp->next_hash = _acl_hash[bucket];
      }

	  al_spin_lock(acl_splock, acl_spvar);

      _acl_hash[bucket] = temp;

	  al_spin_unlock(acl_splock, acl_spvar);

      sta_entry = access_point_get_sta(AL_CONTEXT & temp->entry.sta_mac);

      if (sta_entry != NULL)
      {
         sta_entry->sta.auth_state     |= ACCESS_POINT_STA_AUTH_STATE_ACL;
         sta_entry->sta.dot11e_enabled  = temp->entry.dot11e_enabled;
         sta_entry->sta.dot11e_category = temp->entry.dot11e_category;

         access_point_release_sta(AL_CONTEXT sta_entry);
 
         if (!temp->entry.allow)
         {
            access_point_disassociate_sta_ex(AL_CONTEXT &temp->entry.sta_mac, 0, NULL);
         }
      }
   }
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
}


int access_point_acl_lookup_entry(AL_CONTEXT al_net_addr_t *addr_in,
                                  access_point_vlan_info_t **vlan_out,
                                  int                      *allow_out,
                                  int                      *dot11e_enabled_out,
                                  int                      *dot11e_category_out)
{
   int           hash_value;
   int           bucket;
   unsigned long flags;
   _access_point_acl_entry_info_t *temp;

   hash_value = _ACCESS_POINT_ACL_STA_HASH_FUNCTION(addr_in);
   bucket     = hash_value % _acl_hash_length;

   al_spin_lock(acl_splock, acl_spvar);

   temp = _acl_hash[bucket];

   while (temp != NULL)
   {
      if (AL_NET_ADDR_EQUAL(&temp->entry.sta_mac, addr_in))
      {
         if (temp->vlan_info == NULL)
         {
            temp->vlan_info = access_point_vlan_tag_get(AL_CONTEXT temp->entry.vlan_tag);
            if (temp->vlan_info == NULL)
            {
               temp->vlan_info = &default_vlan_info;
            }
         }

         *vlan_out            = temp->vlan_info;
         *allow_out           = temp->entry.allow;
         *dot11e_enabled_out  = temp->entry.dot11e_enabled;
         *dot11e_category_out = temp->entry.dot11e_category;

         al_spin_unlock(acl_splock, acl_spvar);

         return 1;
      }
      temp = temp->next_hash;
   }

   al_spin_unlock(acl_splock, acl_spvar);

   return 0;
}


int access_point_acl_get_disallow_other_stas(AL_CONTEXT_PARAM_DECL_SINGLE)
{
   return _disallow_other_stas;
}
