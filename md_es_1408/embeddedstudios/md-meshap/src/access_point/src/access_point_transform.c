/********************************************************************************
* MeshDynamics
* --------------
* File     : access_point_transform.c
* Comments : Media Transformation Functionality
* Created  : 11/8/2007
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  0  |11/8/2007 | Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

#include <linux/string.h>
#include "al.h"
#include "al_802_11.h"
#include "access_point.h"
#include "al_impl_context.h"
#include "mesh_defs.h"
#include "imcp_snip.h"
#include "access_point_thread.h"
#include "access_point_sta.h"
#include "access_point_globals.h"
#include "al_print_log.h"
#include "access_point_mgt.h"
#include "access_point_data.h"
#include "al_conf.h"
#include "access_point_vlan.h"
#include "access_point_dot11i.h"
#include "access_point_acl.h"
#include "mesh_ap.h"
#include "access_point_effistream.h"
#include "access_point_multicast.h"
#include "access_point_transform.h"

AL_DECLARE_GLOBAL(access_point_transform_adapter_t * adapters[ACCESS_POINT_TRANSFORM_SLOT_COUNT]);

static int AL_INLINE _transform_ds_packet(AL_CONTEXT_PARAM_DECL al_net_if_t *net_if, al_packet_t *packet, int transform_id)
{
   _packet_context_t packet_context;
   al_packet_t       *transformed_packet;
   int               ret;

   /**
    * We only transform packets coming from DS if we _are_ a ROOT node.
    */

   if (!AL_NET_IF_IS_ETHERNET(net_if))
   {
      (tx_rx_pkt_stats.packet_drop[128])++;
      return ACCESS_POINT_TRANSFORM_ERR_SEND_AS_USUAL;
   }

   if (!_check_ethernet_ingress(net_if, packet))
   {
      (tx_rx_pkt_stats.packet_drop[129])++;
      return ACCESS_POINT_TRANSFORM_ERR_DROP;
   }

   /**
    * Setup the packet context from the source perspective
    */

   ret = ACCESS_POINT_TRANSFORM_ERR_SEND_AS_USUAL;

   _init_packet_context(AL_CONTEXT & packet_context);

   packet_context.packet             = packet;
   packet_context.src_net_if         = net_if;
   packet_context.dest_addr          = &packet->addr_1;
   packet_context.src_addr           = &packet->addr_2;
   packet_context.src_vlan_tag       = packet->dot1q_tag;
   packet_context.src_dot1p_priority = packet->dot1p_priority;
   packet_context.imm_src_addr       = NULL;
   packet_context.src_sta_info       = NULL;
   packet_context.match_type         = MATCH_TRANSFORM_TYPE_TRANSFORM;
   packet_context.transform_id       = transform_id;

   /**
    * Setup the packet context from the destination perspective.
    * All filter checks are carried out at this step, so that
    * sub-sequent steps can carry out tasks clearly.
    */

   if (_set_dest_packet_context(AL_CONTEXT & packet_context) != 0)
   {
      ret = ACCESS_POINT_TRANSFORM_ERR_DROP;
      goto _out;
   }

   /**
    * If the destination is our own direct child, we send as usual
    */

   if (packet_context.dest_sta_info != NULL)
   {
      goto _out;
   }

   /**
    * If the destination net_if is ETHERNET we send as USUAL
    * e.g. Going to a ETHERNET WM
    */

   if (AL_NET_IF_IS_ETHERNET(packet_context.dest_net_if))
   {
      goto _out;
   }

   ret = ACCESS_POINT_TRANSFORM_ERR_SUCCESS;
   transformed_packet = adapters[transform_id]->transform(packet);

   if (AL_NET_ADDR_IS_BROADCAST(packet_context.dest_addr) ||
       AL_NET_ADDR_IS_MULTICAST(packet_context.dest_addr))
   {
      /**
       * Transmit transformed packets on WM and then regular with mesh ignore flag.
       * The transformed packets shall contain the VLAN tag if applicable.
       */

      _tx_bcast_transformed_wm(AL_CONTEXT & packet_context, transformed_packet);
      _tx_bcast_regular_wm(AL_CONTEXT & packet_context, packet);

      /**
       * Need to release the transformed_packet as a copy of the packet is sent on the net_if's
       */
		tx_rx_pkt_stats.tx_transform_pkt_on_wm++;
      al_release_packet(AL_CONTEXT transformed_packet);
   }
   else
   {
      _tx_unicast_transformed_wm(AL_CONTEXT & packet_context, transformed_packet);

      /**
       * No need to release the transformed_packet as it is directly sent on the net_if's
       */
   }

_out:

   _cleanup_packet_context(AL_CONTEXT & packet_context);

   return ret;
}


static int AL_INLINE _transform_wm_packet(AL_CONTEXT_PARAM_DECL al_net_if_t *net_if, al_packet_t *packet, int transform_id)
{
   _packet_context_t packet_context;
   al_packet_t       *transformed_packet;
   int               ret;

   if (AL_NET_IF_IS_ETHERNET(net_if) && !_check_ethernet_ingress(net_if, packet))
   {
      (tx_rx_pkt_stats.packet_drop[130])++;
      return ACCESS_POINT_TRANSFORM_ERR_DROP;
   }

   /**
    * Setup the packet context from the source perspective
    */

   ret = ACCESS_POINT_TRANSFORM_ERR_SEND_AS_USUAL;

   _init_packet_context(AL_CONTEXT & packet_context);

   packet_context.packet       = packet;
   packet_context.src_net_if   = net_if;
   packet_context.match_type   = MATCH_TRANSFORM_TYPE_TRANSFORM;
   packet_context.transform_id = transform_id;
   packet_context.imm_src_addr = NULL;

   if (AL_NET_IF_IS_WIRELESS(net_if))
   {
      if (_PACKET_IS_WDS(packet->frame_control))
      {
         /**
          * Case of a Regular packet sent by a child mesh node
          * This is only possible if the child mesh node did not have
          * the Effistream rule for the transformation
          * In this case we send as usual.
          */

         goto _out;
      }
      else if (_PACKET_IS_TODS(packet->frame_control))
      {
         /**
          * Case of a regular packet sent by a direct child
          */

         packet_context.dest_addr = &packet->addr_3;
         packet_context.src_addr  = &packet->addr_2;
      }
   }
   else
   {
      packet_context.dest_addr = &packet->addr_1;
      packet_context.src_addr  = &packet->addr_2;
   }

   /**
    * Setup the packet context from the source perspective.
    */

   if (_set_src_packet_context(AL_CONTEXT & packet_context) != 0)
   {
      ret = ACCESS_POINT_TRANSFORM_ERR_DROP;
      goto _out;
   }

   /**
    * Setup the packet context from the destination perspective.
    * No filter checks are carried out beyond this step, so that
    * sub-sequent steps can carry out tasks clearly.
    */

   if (_set_dest_packet_context(AL_CONTEXT & packet_context) != 0)
   {
      ret = ACCESS_POINT_TRANSFORM_ERR_DROP;
      goto _out;
   }

   /**
    * If the destination is our own direct child, we send as usual
    */

   if (packet_context.dest_sta_info != NULL)
   {
      goto _out;
   }

   /**
    * If the destination net_if is ETHERNET we send as USUAL
    * e.g. Direct child packets going to DS for a ROOT node or
    * Direct child packets going to a ETHERNET WM
    */

   if (AL_NET_IF_IS_ETHERNET(packet_context.dest_net_if))
   {
      goto _out;
   }

   ret = ACCESS_POINT_TRANSFORM_ERR_SUCCESS;
   transformed_packet = adapters[transform_id]->transform(packet);

   if (AL_NET_ADDR_IS_BROADCAST(packet_context.dest_addr) ||
       AL_NET_ADDR_IS_MULTICAST(packet_context.dest_addr))
   {
      /**
       * 1) Transmit transformed packet on DS if we are a RELAY node or REGULAR packet on DS if ROOT node
       * 2) Transmit transformed packet on WM and then regular with mesh ignore flag
       */

      if (AL_NET_IF_IS_ETHERNET(globals.config.ds_net_if_info.net_if))
      {
         _tx_bcast_regular_ds(AL_CONTEXT & _packet_context, packet);
      }
      else
      {
         _tx_transformed_ds(AL_CONTEXT & _packet_context, transformed_packet);
      }

      _tx_bcast_transformed_wm(AL_CONTEXT & packet_context, transformed_packet);
      _tx_bcast_regular_wm(AL_CONTEXT & packet_context, packet);

      /**
       * Need to release the transformed packet as a copy is sent on the net_if's
       */

		tx_rx_pkt_stats.tx_transform_pkt_on_ds++;
      al_release_packet(AL_CONTEXT transformed_packet);
   }
   else
   {
      if (packet_context.dest_net_if == globals.config.ds_net_if_info.net_if)
      {
         _tx_transformed_ds(AL_CONTEXT & packet_context, transformed_packet);
      }
      else
      {
         _tx_unicast_transformed_wm(AL_CONTEXT & packet_context, transformed_packet);
      }

      /**
       * No need to release the transformed packet as the transformed_packet itself is sent on the net_if's
       */
   }

_out:

   _cleanup_packet_context(AL_CONTEXT & packet_context);

   return ret;
}


static int AL_INLINE _de_transform_ds_packet(AL_CONTEXT_PARAM_DECL al_net_if_t *net_if, al_packet_t *packet, int transform_id)
{
   _packet_context_t packet_context;
   al_packet_t       *corrected_packet;
   al_packet_t       *regular_packet;
   int               ret;
   static int count;

   /**
    * We only de-transform packets coming from DS if we are _not_ a ROOT node.
    */

   if (AL_NET_IF_IS_ETHERNET(net_if))
   {
      (tx_rx_pkt_stats.packet_drop[131])++;
      return ACCESS_POINT_TRANSFORM_ERR_SEND_AS_USUAL;
   }

   /**
    * Before we proceed at this stage, we need to correct any errors
    * found in the packet, and operate on the corrected packet.
    */

   corrected_packet = adapters[transform_id]->correct(packet);

   if (corrected_packet == NULL)
   {
      /**
       * The packet could not be corrected
       */
      (tx_rx_pkt_stats.packet_drop[132])++;
      return ACCESS_POINT_TRANSFORM_ERR_DROP;
   }


   /**
    * Setup the packet context from the source perspective
    */

   ret = ACCESS_POINT_TRANSFORM_ERR_SEND_AS_USUAL;

   _init_packet_context(AL_CONTEXT & packet_context);

   packet_context.packet             = corrected_packet;
   packet_context.src_net_if         = net_if;
   packet_context.src_sta_info       = NULL;
   packet_context.match_type         = MATCH_TRANSFORM_TYPE_DETRANSFORM;
   packet_context.transform_id       = transform_id;
   packet_context.src_vlan_tag       = corrected_packet->dot1q_tag;
   packet_context.src_dot1p_priority = corrected_packet->dot1p_priority;

   if (_PACKET_IS_WDS(packet->frame_control))
   {
      /**
       * Unicast packets for direct and indirect children
       */
      packet_context.dest_addr    = &corrected_packet->addr_3;
      packet_context.src_addr     = &corrected_packet->addr_4;
      packet_context.imm_src_addr = &corrected_packet->addr_2;
   }
   else
   {
      /**
       * Broadcast or self unicast packets
       */
      packet_context.dest_addr    = &corrected_packet->addr_1;
      packet_context.src_addr     = &corrected_packet->addr_2;
      packet_context.imm_src_addr = NULL;
   }

   /**
    * Setup the packet context from the destination perspective.
    * All filter checks are carried out at this step, so that
    * sub-sequent steps can carry out tasks clearly.
    */

   if (_set_dest_packet_context(AL_CONTEXT & packet_context) != 0)
   {
      ret = ACCESS_POINT_TRANSFORM_ERR_DROP;
	  (tx_rx_pkt_stats.packet_drop[65])++;
      al_release_packet(AL_CONTEXT corrected_packet);
      goto _out;
   }

   if (packet_context.dest_sta_info != NULL)
   {
      /**
       * The destination is a direct child of ours, hence we de-transform
       * the packet
       */

      regular_packet = adapters[transform_id]->de_transform(corrected_packet);
   }
   else
   {
   }



   /**
    * If the destination is our own direct child, we send as usual
    */

   if (packet_context.dest_sta_info != NULL)
   {
      goto _out;
   }

   /**
    * If the destination net_if is ETHERNET we send as USUAL
    * e.g. Going to a ETHERNET WM
    */

   if (AL_NET_IF_IS_ETHERNET(packet_context.dest_net_if))
   {
      goto _out;
   }

   ret = ACCESS_POINT_TRANSFORM_ERR_SUCCESS;
   transformed_packet = adapters[transform_id]->transform(packet);

   if (AL_NET_ADDR_IS_BROADCAST(packet_context.dest_addr) ||
       AL_NET_ADDR_IS_MULTICAST(packet_context.dest_addr))
   {
      /**
       * Transmit transformed packets on WM and then regular with mesh ignore flag.
       * The transformed packets shall contain the VLAN tag if applicable.
       */

      _tx_bcast_transformed_wm(AL_CONTEXT & packet_context, transformed_packet);
      _tx_bcast_regular_wm(AL_CONTEXT & packet_context, packet);

      /**
       * Need to release the transformed_packet as a copy of the packet is sent on the net_if's
       */
		tx_rx_pkt_stats.tx_de_transform_pkt_on_ds++;
      al_release_packet(AL_CONTEXT transformed_packet);
   }
   else
   {
      _tx_unicast_transformed_wm(AL_CONTEXT & packet_context, transformed_packet);

      /**
       * No need to release the transformed packet as the transformed_packet itself is sent on the net_if's
       */
   }

_out:

   _cleanup_packet_context(AL_CONTEXT & packet_context);

   return ret;
}


static int AL_INLINE _de_transform_wm_packet(AL_CONTEXT_PARAM_DECL al_net_if_t *net_if, al_packet_t *packet, int transform_id)
{
}


int access_point_transorm_process_ds_packet(AL_CONTEXT_PARAM_DECL al_net_if_t *net_if, al_packet_t *packet, int transform_type, int transform_id)
{
   if ((transform_id < 0) ||
       (transform_id >= ACCESS_POINT_TRANSFORM_SLOT_COUNT) ||
       (adapters[transform_id] == NULL))
   {
      goto _out;
   }

   switch (transform_type)
   {
   case MATCH_TRANSFORM_TYPE_TRANSFORM:
      return _transform_ds_packet(net_if, packet, transform_id);

   case MATCH_TRANSFORM_TYPE_DETRANSFORM:
      return _de_transform_ds_packet(net_if, packet, transform_id);
   }

_out:

   return ACCESS_POINT_TRANSFORM_ERR_SEND_AS_USUAL;
}


int access_point_transorm_process_wm_packet(AL_CONTEXT_PARAM_DECL al_net_if_t *net_if, al_packet_t *packet, int transform_type, int transform_id)
{
   if ((transform_id < 0) ||
       (transform_id >= ACCESS_POINT_TRANSFORM_SLOT_COUNT) ||
       (adapters[transform_id] == NULL))
   {
      goto _out;
   }

   switch (transform_type)
   {
   case MATCH_TRANSFORM_TYPE_TRANSFORM:
      return _transform_wm_packet(net_if, packet, transform_id);

   case MATCH_TRANSFORM_TYPE_DETRANSFORM:
      return _de_transform_wm_packet(net_if, packet, transform_id);
   }

_out:

   return ACCESS_POINT_TRANSFORM_ERR_SEND_AS_USUAL;
}


void access_point_transform_initialize(AL_CONTEXT_PARAM_DECL_SINGLE)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
   int i;

   for (i = 0; i < ACCESS_POINT_TRANSFORM_SLOT_COUNT; i++)
   {
      adapters[i] = NULL;
   }
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
}


void access_point_transform_uninitialize(AL_CONTEXT_PARAM_DECL_SINGLE)
{
}


int access_point_register_transform(AL_CONTEXT_PARAM_DECL int slot, access_point_transform_adapter_t *adapter)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
   if ((slot < 0) || (slot >= ACCESS_POINT_TRANSFORM_SLOT_COUNT))
   {
       al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESHAP : ERROR : Invalid slot : %s : %d\n", __func__,__LINE__);
      return ACCESS_POINT_TRANSFORM_ERR_INVALID_SLOT;
   }

   if (adapters[slot] != NULL)
   {
       al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESHAP : ERROR : slot busy : %s : %d\n", __func__,__LINE__);
      return ACCESS_POINT_TRANSFORM_ERR_SLOT_BUSY;
   }

   adapters[slot] = adapter;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
   return ACCESS_POINT_TRANSFORM_ERR_SUCCESS;
}


void access_point_unregister_transform(AL_CONTEXT_PARAM_DECL int slot)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
   if ((slot < 0) || (slot >= ACCESS_POINT_TRANSFORM_SLOT_COUNT))
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESHAPP : ERROR : slot registered : %s : %d\n", __func__,__LINE__);
      return;
   }

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
   adapters[slot] = NULL;
}
