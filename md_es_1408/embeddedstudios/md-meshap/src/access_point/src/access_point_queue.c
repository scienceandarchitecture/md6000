/********************************************************************************
* MeshDynamics
* --------------
* File     : access_point_queue.c
* Comments : Interface based access_point_queue impl file.
* Created  : 2/4/2005
* Author   : Anand Bhange
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  2  |5/31/2006 | queue_print_hstats added                        | Sriram |
* -----------------------------------------------------------------------------
* |  1  |1/04/2006 | Added dot1p_queue implementation                | Bindu  |
* -----------------------------------------------------------------------------
* |  0  |2/04/2005 | Created                                         | Anand  |
* -----------------------------------------------------------------------------
********************************************************************************/

#include <linux/string.h>
#include "al.h"
#include "access_point.h"
#include "al_impl_context.h"
#include "al_print_log.h"


/*## QUEUE IMPL INCLUDE FILES START##*/
#include "access_point_fifo_queue.h"
#include <linux/seq_file.h>
#include "dot1p_queue.h"
/*## QUEUE IMPL INCLUDE FILES END##*/

access_point_queue_t *access_point_queue_initialize(AL_CONTEXT_PARAM_DECL char queue_type)
{
   access_point_queue_t *queue = (access_point_queue_t *)al_heap_alloc(AL_CONTEXT sizeof(access_point_queue_t)AL_HEAP_DEBUG_PARAM);
   int i;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Enter %s<%d>\n",__func__,__LINE__);
   switch (queue_type)
   {
   case QUEUE_FIFO:
      queue->queue_uninitialize   = fifo_queue_uninitialize;
      queue->queue_add_packet     = fifo_queue_add_packet;
      queue->queue_remove_packet  = fifo_queue_remove_packet;
      queue->queue_empty_check    = fifo_queue_empty_check;
      queue->queue_print_stats    = fifo_queue_print_stats;
      queue->queue_requeue_packet = fifo_queue_requeue_packet;
      queue->queue_print_hstats   = fifo_queue_print_hstats;
      queue->queue_data        = (void *)fifo_queue_initialize(AL_CONTEXT_SINGLE);
      break;

   case QUEUE_DOT1P:

      queue->queue_uninitialize   = dot1p_queue_uninitialize;
      queue->queue_add_packet     = dot1p_queue_add_packet;
      queue->queue_remove_packet  = dot1p_queue_remove_packet;
      queue->queue_empty_check    = dot1p_queue_empty_check;
      queue->queue_print_stats    = dot1p_queue_print_stats;
      queue->queue_requeue_packet = dot1p_queue_requeue_packet;
      queue->queue_print_hstats   = dot1p_queue_print_hstats;
	  queue->queue_data    = (void *)dot1p_queue_initialize();
      break;
   }
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Exit %s<%d>\n",__func__,__LINE__);
   return queue;
}
