/********************************************************************************
* MeshDynamics
* --------------
* File     : access_point_thread.c
* Comments : Access Point Thread Function
* Created  : 4/9/2004
* Author   : Bindu Parakala
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* | 37  |1/27/2009 | Changes for Uplink Scan Functionality           | Sriram |
* -----------------------------------------------------------------------------
* | 36  |7/13/2007 | process_al_802_11_fc_type_mgt_packet changed    | Sriram |
* -----------------------------------------------------------------------------
* | 35  |1/25/2006 | removed dot1p_queue variable                    | Bindu  |
* -----------------------------------------------------------------------------
* | 34  |1/04/2006 | Added dot1p_queue implementation                | Bindu  |
* -----------------------------------------------------------------------------
* | 33  |10/5/2005 | Changes for enabling MIP                        | Sriram |
* -----------------------------------------------------------------------------
* | 32  |9/19/2005 | IMCP packet handling during init changed        | Sriram |
* -----------------------------------------------------------------------------
* | 31  |2/09/2005 | Interface based packet Queue                    | Anand  |
* -----------------------------------------------------------------------------
* | 30  |12/14/2004| Avoided blind usage of stay awake count         | Sriram |
* -----------------------------------------------------------------------------
* | 29  |7/24/2004 | Thread loop made more readable                  | Sriram |
* -----------------------------------------------------------------------------
* | 28  |7/24/2004 | enable/disable for packet processing            | Anand  |
* -----------------------------------------------------------------------------
* | 27  |7/19/2004 | Changed variable names for readability          | Sriram |
* -----------------------------------------------------------------------------
* | 26  |7/5/2004  | packet retry count to avoid sleep & awake       | Anand  |
* -----------------------------------------------------------------------------
* | 25  |7/5/2004  | ap thread exit in STOPPING state                | Anand  |
* -----------------------------------------------------------------------------
* | 24  |6/11/2004 | Misc changes                                    | Sriram |
* -----------------------------------------------------------------------------
* | 23  |6/11/2004 | Misc changes                                    | Sriram |
* -----------------------------------------------------------------------------
* | 22  |6/10/2004 | Misc changes                                    | Sriram |
* -----------------------------------------------------------------------------
* | 21  |6/4/2004  | Fixed packet transit delay problem              | Sriram |
* -----------------------------------------------------------------------------
* | 20  |6/4/2004  | Print log macros used                           | Bindu  |
* -----------------------------------------------------------------------------
* | 19  |6/1/2004  | fixed misc bugs                                 | Bindu  |
* -----------------------------------------------------------------------------
* | 18  |5/31/2004 | al_set_thread_name called                       | Bindu  |
* -----------------------------------------------------------------------------
* | 17  |5/31/2004 | fixed misc bugs                                 | Bindu  |
* -----------------------------------------------------------------------------
* | 16  |5/31/2004 | included access_point_sta.h                     | Bindu  |
* -----------------------------------------------------------------------------
* | 15  |5/31/2004 | shifted globals vars to access_point_globals.c  | Bindu  |
* -----------------------------------------------------------------------------
* | 14  |5/31/2004 | included access_point_globals.h                 | Bindu  |
* -----------------------------------------------------------------------------
* | 13  |5/28/2004 | Packet released                                 | Bindu  |
* -----------------------------------------------------------------------------
* | 12  |5/25/2004 | Bug Fixed - AP stop thread loop break           | Anand  |
* -----------------------------------------------------------------------------
* | 11  |5/25/2004 | Thread wait for packet queue is empty           | Anand  |
* -----------------------------------------------------------------------------
* | 10  |5/21/2004 | Log type added to al_print_log                  | Anand  |
* -----------------------------------------------------------------------------
* |  9  |5/20/2004 | ret_type added fpr IMCP handler in thread fn    | Bindu  |
* -----------------------------------------------------------------------------
* |  8  |5/18/2004 | checking for 802.3 packets type, sub-type       | Bindu  |
* -----------------------------------------------------------------------------
* |  7  |5/13/2004 | renamed assoc & re-assoc methods                | Bindu  |
* -----------------------------------------------------------------------------
* |  6  |5/11/2004 | IMCP Packet checking added in AP thread         | Anand  |
* -----------------------------------------------------------------------------
* |  5  |5/7/2004  | stop_event_handle event set in threadfunction   | Bindu  |
* -----------------------------------------------------------------------------
* |  4  |4/30/2004 | Added al_print_log sts                          | Bindu  |
* -----------------------------------------------------------------------------
* |  3  |4/30/2004 | Thread exit on AP = ACCESS_POINT_STATE_STOPPED  | Bindu  |
* -----------------------------------------------------------------------------
* |  2  |4/22/2004 | **Made (major) changes for AL_CONTEXT**         | Bindu  |
* -----------------------------------------------------------------------------
* |  1  |4/10/2004 | added Mesh hooks                                | Bindu  |
* -----------------------------------------------------------------------------
* |  0  |4/9/2004  |  Created                                        | Bindu  |
* -----------------------------------------------------------------------------
********************************************************************************/

#include <linux/string.h>
#include <linux/module.h>
#include "al.h"
#include "access_point.h"
#include "al_impl_context.h"
#include "access_point_mgt.h"
#include "al_print_log.h"
#include "access_point_data.h"
#include "access_point_thread.h"
#include "access_point_sta.h"
#include "access_point_queue.h"
#include "access_point_globals.h"
#include "dot1p_queue.h"

#define _ACCESS_POINT_THREAD_SHOULD_BREAK \
   (globals.ap_state == ACCESS_POINT_STATE_STOPPING) || (globals.ap_state == ACCESS_POINT_STATE_STOPPED)
#define _IMCP_THREAD_SHOULD_BREAK \
   (globals.ap_state == ACCESS_POINT_STATE_STOPPING) || (globals.ap_state == ACCESS_POINT_STATE_STOPPED)

#define _ACCESS_POINT_THREAD_ALLOW_ONLY_IMCP                                   \
   ((type == AL_802_11_FC_TYPE_DATA) && (sub_type == AL_802_11_FC_STYPE_DATA)) \
   && ((globals.ap_state == ACCESS_POINT_STATE_STARTING) || (ap_enable_disable_processing == 0))

static void _ap_thread_watchdog_routine(void)
{
    printk(KERN_EMERG "^^^^watchdog timer expired looks packet not coming to queue^^^^\n");
    //printk(KERN_EMERG "Due to AP thread stucked going for reboot...\n");
    //al_reboot_board(AL_CONTEXT AL_REBOOT_BOARD_CODE_MESH_THREAD_WATCHDOG);
}

extern int upstack_pkt_queue_init_done;
int           watchdog_id_ap;
int access_point_thread(AL_CONTEXT_PARAM_DECL al_thread_param_t *param)
{
   int         ret;
   int         ret_type;
   al_packet_t *packet;
   al_net_if_t *net_if;
   int         type;
   int         sub_type;
   int         stay_awake_count;
   unsigned int			thread_num = thread_stat.ap_thread;
   int ap_thread_timer = 50;
   watchdog_id_ap = al_create_watchdog(AL_CONTEXT "ap_thread",
                                         ap_thread_timer * 1000 * 15,
                                         _ap_thread_watchdog_routine);
	thread_stat.ap_thread++;
	if (thread_stat.ap_thread > MAX_AP_THREAD)
		printk(KERN_EMERG"access_point_thread() running more than MAX_AP_THREAD\n");

   AL_PRINT_LOG_FLOW_0("AP		: access_point_thread()");
   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"INFO: Ap thread start\n");
   al_set_thread_name(AL_CONTEXT "ap_thread");

   upstack_pkt_queue_init_done = 1;
   AL_ATOMIC_SET(globals.ap_thread_disabled, 0);
   al_set_watchdog_timeout(AL_CONTEXT watchdog_id_ap, ap_thread_timer * 1000 * 15);
   while (1)
   {
      al_reset_event(AL_CONTEXT globals.queue_event_handle);
      ret = al_wait_on_event(AL_CONTEXT globals.queue_event_handle, AL_WAIT_TIMEOUT_INFINITE);
      if (_ACCESS_POINT_THREAD_SHOULD_BREAK)
      {
		 (tx_rx_pkt_stats.packet_drop[80])++;
         break;
      }

_next_packet:
	  thread_stat.ap_thread_count[thread_num]++;

      if (AL_ATOMIC_GET(globals.ap_thread_disabled) == 1)
      {
	     (tx_rx_pkt_stats.packet_drop[30])++;
         continue;
      }
	  net_if = NULL;
      packet = globals.ap_queue->queue_remove_packet(AL_CONTEXT globals.ap_queue, &net_if);


      if (packet == NULL)
      {

		  if(reboot_in_progress) {
			  printk(KERN_EMERG"-----Terminating 'ap_thread' thread-----\n");
			  threads_status &= ~THREAD_STATUS_BIT0_MASK;
			  (tx_rx_pkt_stats.packet_drop[29])++;
		  }

         stay_awake_count = globals.config.stay_awake_count < ACCESS_POINT_MIN_STAY_AWAKE_COUNT ? ACCESS_POINT_MIN_STAY_AWAKE_COUNT : globals.config.stay_awake_count;
         while (stay_awake_count > 0)
         {
            if (globals.ap_queue->queue_empty_check(AL_CONTEXT globals.ap_queue))
            {
               packet = globals.ap_queue->queue_remove_packet(AL_CONTEXT globals.ap_queue, &net_if);
               break;
            }
            --stay_awake_count;
         }

         if (packet == NULL)
         {
           if(reboot_in_progress) {
                printk(KERN_EMERG"-----Terminating 'ap_thread' thread-----\n");
                threads_status &= ~THREAD_STATUS_BIT0_MASK;
                (tx_rx_pkt_stats.packet_drop[29])++;
            }

            if (_ACCESS_POINT_THREAD_SHOULD_BREAK)
            {
               break;
            }
            continue;
         }
      }

	  if(packet->pkt_status == RX_PKT)
	  {
	     packet->pkt_status = AP_TRD_PKT;
	  }

      if (packet->flags & AL_PACKET_FLAGS_REQUEUED)
      {
         type     = AL_802_11_FC_TYPE_DATA;
         sub_type = AL_802_11_FC_STYPE_DATA;
      }
      else
      {
         if ((net_if != NULL) && (net_if->encapsulation == AL_NET_IF_ENCAPSULATION_802_11))
         {
            type     = AL_802_11_FC_GET_TYPE(packet->frame_control);
            sub_type = AL_802_11_FC_GET_STYPE(packet->frame_control);
//QOS_SUPPORT
         if(IS_NETIF_11NAC(net_if))
                sub_type |= AL_802_11_FC_STYPE_QOS;

         }
         else
         {
            type     = AL_802_11_FC_TYPE_DATA;
            sub_type = AL_802_11_FC_STYPE_DATA;
         }
      }

	  if(reboot_in_progress) {
		  (tx_rx_pkt_stats.packet_drop[29])++;
        al_release_packet(AL_CONTEXT packet);
		  goto _next_packet;
	  }

	  if(((net_if!= NULL) && AL_NET_IF_IS_ETHERNET(net_if)) || (!net_if)) {
	  packet->src_addr_sta_entry = access_point_get_sta(AL_CONTEXT &packet->addr_2);
	  packet->dest_addr_sta_entry = access_point_get_sta(AL_CONTEXT &packet->addr_1);
	  } else {
		  if (_PACKET_IS_TODS(packet->frame_control)) {
			  packet->src_addr_sta_entry = access_point_get_sta(AL_CONTEXT &packet->addr_2);
			  packet->dest_addr_sta_entry = access_point_get_sta(AL_CONTEXT &packet->addr_3);
		  } else if((_PACKET_IS_WDS(packet->frame_control))) {
			  packet->src_addr_sta_entry = access_point_get_sta(AL_CONTEXT &packet->addr_4);
			  packet->dest_addr_sta_entry = access_point_get_sta(AL_CONTEXT &packet->addr_3);
	      } else if((_PACKET_IS_FROMDS(packet->frame_control))) {
			  packet->src_addr_sta_entry = access_point_get_sta(AL_CONTEXT &packet->addr_3);
			  packet->dest_addr_sta_entry = access_point_get_sta(AL_CONTEXT &packet->addr_1);
		  }
	  }

      if (_ACCESS_POINT_THREAD_ALLOW_ONLY_IMCP &&
          (net_if != NULL))
      {

         if (on_imcp_packet_handler != NULL)
         {
            if (_packet_is_imcp_udp_ip(AL_CONTEXT packet) == 0)
            {
               ret_type = on_imcp_packet_handler(AL_CONTEXT net_if, packet);
			   if(!ret_type || ret_type < 0) {
				   (tx_rx_pkt_stats.packet_drop[62])++;
			   }
               al_release_packet(AL_CONTEXT packet);
               goto _next_packet;
            }
            else
            {
               if (net_if == globals.config.ds_net_if_info.net_if)
               {
				  (tx_rx_pkt_stats.packet_drop[63])++;
                  al_release_packet(AL_CONTEXT packet);
                  goto _next_packet;
               }

               if (AL_NET_IF_IS_WIRELESS(net_if))
               {
                  if (AL_NET_ADDR_EQUAL(&packet->addr_3, &net_if->config.hw_addr) ||
                      AL_NET_ADDR_IS_BROADCAST(&packet->addr_3) ||
                      AL_NET_ADDR_IS_MULTICAST(&packet->addr_3))
                  {
                     al_send_packet_up_stack(AL_CONTEXT net_if, packet);
                     goto _next_packet;
                  }
               }
               else
               {
                  if (AL_NET_ADDR_EQUAL(&packet->addr_1, &net_if->config.hw_addr) ||
                      AL_NET_ADDR_IS_BROADCAST(&packet->addr_1) ||
                      AL_NET_ADDR_IS_MULTICAST(&packet->addr_1))
                  {
                     al_send_packet_up_stack(AL_CONTEXT net_if, packet);
                     goto _next_packet;
                  }
               }
            }
         }
      }
      else if ((globals.ap_state == ACCESS_POINT_STATE_STARTED) || (globals.ap_state == ACCESS_POINT_STATE_STARTING))
      {
         AL_PRINT_LOG_MSG_0("AP	: Mgm / Data Packet");

         /*
          * Process the packet retreived from Queue
          */

         switch (type)
         {
         case AL_802_11_FC_TYPE_MGMT:
            process_al_802_11_fc_type_mgt_packet(AL_CONTEXT net_if, sub_type, packet);
				tx_rx_pkt_stats.access_point_process_mgmt_pkt++;
            break;

         case AL_802_11_FC_TYPE_DATA:
            if ((globals.ap_state == ACCESS_POINT_STATE_STARTED) && (ap_enable_disable_processing == 1))
            {
               process_al_802_11_fc_type_data_packet(AL_CONTEXT net_if, sub_type, packet);
					tx_rx_pkt_stats.access_point_process_data_pkt++;
            }
            break;

         default:
            al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "AP   : Dropping unknown packet type 0x%02X of sub-type 0x%02X over %s", type, sub_type, net_if->name);
         }
      }
      else if (_ACCESS_POINT_THREAD_SHOULD_BREAK)
      {
		 (tx_rx_pkt_stats.packet_drop[64])++;
         al_release_packet(AL_CONTEXT packet);
         break;
      }
      else
      {
         AL_PRINT_LOG_ERROR_0("AP		: Unknown state ???????????????????????????");
      }
		tx_rx_pkt_stats.access_point_process_pkt++;
      al_release_packet(AL_CONTEXT packet);
      goto _next_packet;
   }
   al_set_event(AL_CONTEXT globals.stop_event_handle);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"INFO:	: Ap thread stopped\n");

   return 0;
}


void process_al_802_11_fc_type_mgt_packet(AL_CONTEXT_PARAM_DECL al_net_if_t *net_if, int sub_type, al_packet_t *packet)
{

   switch (sub_type)
   {
   case AL_802_11_FC_STYPE_DISASSOC:
      process_al_802_11_fc_stype_disassoc_packet(AL_CONTEXT net_if, packet);
      break;

   case AL_802_11_FC_STYPE_DEAUTH:
      process_al_802_11_fc_stype_deauth_packet(AL_CONTEXT net_if, packet);
      break;

   default:
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "AP   : Dropping MGMT packet of sub-type 0x%02X over %s", sub_type, net_if->name);
   }
}

void imcp_thread()
{
	dot1p_queue_t **queue;
	dot1p_queue_entry_t *entry;
	al_net_if_t *net_if;
	al_packet_t *packet;
	int ret;

	MESH_USAGE_PROFILING_INIT
	/*ap_queue initialization is taking place after imcp thread get
	 *schedule so waiting till ap_queue get initialize*/
	while(1) {
		if(globals.ap_queue) {
			break;
		}else {
			al_thread_sleep(AL_CONTEXT 2000);
		}
	}
	queue = (dot1p_queue_t **)globals.ap_queue->queue_data;

	while(1) {
		al_reset_event(AL_CONTEXT globals.imcp_queue_event_handle);
		ret = al_wait_on_event(AL_CONTEXT globals.imcp_queue_event_handle, AL_WAIT_TIMEOUT_INFINITE);

		if (_IMCP_THREAD_SHOULD_BREAK) {
			(tx_rx_pkt_stats.packet_drop[136])++;
			break;
		}
next_packet:
		thread_stat.imcp_thread_count++;

		if(reboot_in_progress) {
			al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MESH_AP:INFO: "
			"-----Terminating 'imcp_thread' thread-----\n");
			threads_status &= ~THREAD_STATUS_BIT16_MASK;
			(tx_rx_pkt_stats.packet_drop[137])++;
			break;
		}

		/*De-queue IMCP packets from 0th IMCP queue*/
		MESH_USAGE_PROFILE_START(start)
		al_spin_lock(queue[0]->dot1pQ_imcpsplock, flags);
		MESH_USAGE_PROFILE_END(profiling_info.dot1pQ_imcpsplock_1,index,start,end)
		if(queue[0]->dot1p_queue_imcphead) {
			packet = NULL;
			entry = queue[0]->dot1p_queue_imcphead;
			queue[0]->dot1p_queue_imcphead = queue[0]->dot1p_queue_imcphead->next;
			net_if = entry->net_if;
			packet  = entry->packet;
			q_packet_stats.imcp_pktQ_total_packet_removed_count++;
			q_packet_stats.imcp_pktQ_current_packet_count--;
		    MESH_USAGE_PROFILE_START(start)
			al_spin_unlock(queue[0]->dot1pQ_imcpsplock, flags);
			MESH_USAGE_PROFILE_END(profiling_info.dot1pQ_imcpspunlock_1,index,start,end)
			if(packet->pkt_status)
			{
			packet->pkt_status = IMCP_TRD_PKT;
			}
			process_al_802_11_fc_stype_data_packet(AL_CONTEXT net_if, packet);
			al_release_packet(AL_CONTEXT packet);
			goto next_packet;
		}else {
		    MESH_USAGE_PROFILE_START(start)
			al_spin_unlock(queue[0]->dot1pQ_imcpsplock, flags);
			MESH_USAGE_PROFILE_END(profiling_info.dot1pQ_imcpspunlock_1,index,start,end)
			continue;
		}
	}
}

void remove_from_fqueue_addingto_imcpqueue(al_packet_t *packet)
{
	dot1p_queue_t **queue, **queue1;
	dot1p_queue_entry_t *entry = NULL;
	int hindx, pindx;

	entry = (dot1p_queue_entry_t *)&packet->dot1p_entry;
	queue1   = (dot1p_queue_t **)globals.ap_queue->queue_data;
    MESH_USAGE_PROFILING_INIT

	if(!entry) {
		(tx_rx_pkt_stats.packet_drop[135])++;
		al_release_packet(AL_CONTEXT packet);
		return;
	}

#if MULTIPLE_AP_THREAD
	/*Removing IMCP packets from F-queue*/
	pindx = 0;
	hindx = 0;
	queue    = (dot1p_queue_t **)globals.ap_queue->queue_data[pindx];
	MESH_USAGE_PROFILE_START(start)
	al_spin_lock(queue[hindx]->dot1p_Qsplock, flags);
	MESH_USAGE_PROFILE_END(profiling_info.dot1p_Qsplock,index,start,end)
	if(entry->prev) {
		entry->prev->next = entry->next;
	}
	if(entry->next) {
		entry->next->prev = entry->prev;
	}
	if(entry == queue[hindx]->dot1p_queue_fhead) {
		queue[hindx]->dot1p_queue_fhead = queue[hindx]->dot1p_queue_fhead->next;
	}
	MESH_USAGE_PROFILE_START(start)
	al_spin_unlock(queue[hindx]->dot1p_Qsplock, flags);
	MESH_USAGE_PROFILE_END(profiling_info.dot1p_Qspunlock,index,start,end)
	entry->prev = NULL;
#endif

	entry->next = NULL;

	/*Enqueueing IMCP packets into 0th IMCP queue*/
	MESH_USAGE_PROFILE_START(start)
	al_spin_lock(queue1[0]->dot1pQ_imcpsplock, flags);
	MESH_USAGE_PROFILE_END(profiling_info.dot1pQ_imcpsplock_2,index,start,end)
	if(queue1[0]->dot1p_queue_imcphead == NULL) {
		queue1[0]->dot1p_queue_imcphead = entry;
		queue1[0]->dot1p_queue_imcptail = entry;
	}else {
		queue1[0]->dot1p_queue_imcptail->next = entry;
#if MULTIPLE_AP_THREAD
		entry->prev = queue1[0]->dot1p_queue_imcptail;
#endif
		queue1[0]->dot1p_queue_imcptail = entry;
	}
	q_packet_stats.imcp_pktQ_current_packet_count++;
	if(q_packet_stats.imcp_pktQ_current_packet_count > q_packet_stats.imcp_pktQ_current_packet_max_count) {
		q_packet_stats.imcp_pktQ_current_packet_max_count = q_packet_stats.imcp_pktQ_current_packet_count;
	}
	q_packet_stats.imcp_pktQ_total_packet_added_count++;
	al_set_event(AL_CONTEXT globals.imcp_queue_event_handle);
	MESH_USAGE_PROFILE_START(start)
	al_spin_unlock(queue1[0]->dot1pQ_imcpsplock, flags);
	MESH_USAGE_PROFILE_END(profiling_info.dot1pQ_imcpspunlock_2,index,start,end)
}

void process_al_802_11_fc_type_data_packet(AL_CONTEXT_PARAM_DECL al_net_if_t *net_if, int sub_type, al_packet_t *packet)
{

   if (_packet_is_imcp_udp_ip(AL_CONTEXT packet) == 0) {
	   al_add_packet_reference(packet);
	   remove_from_fqueue_addingto_imcpqueue(packet);
	   return;
   }

   switch (sub_type)
   {
//QOS_SUPPORT
   case AL_802_11_FC_STYPE_QOS:
   case AL_802_11_FC_STYPE_DATA:
      process_al_802_11_fc_stype_data_packet(AL_CONTEXT net_if, packet);
      break;

   default:
      process_al_802_11_fc_stype_data_packet(AL_CONTEXT net_if, packet);
   }
}
