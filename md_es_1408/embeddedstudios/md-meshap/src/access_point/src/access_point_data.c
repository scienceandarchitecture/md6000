/********************************************************************************
* MeshDynamics
* --------------
* File     : access_point_data.c
* Comments : Process Data Packets
* Created  : 4/12/2004
* Author   : Bindu Parakala
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* | 77  |8/22/2008 | Changes for SIP                                 |Abhijit |
* -----------------------------------------------------------------------------
* | 76  |7/23/2007 | DEAUTH sent instead of DISASSOC                 | Sriram |
* -----------------------------------------------------------------------------
* | 75  |6/20/2007 | Changes for IGMP Snooping                       |Prachiti|
* -----------------------------------------------------------------------------
* | 74  |6/6/2007  | Changes for EFFISTREAM BitRate                  |Prachiti|
* -----------------------------------------------------------------------------
* | 73  |4/23/2007 | Child notified upon forced disassoc             | Sriram |
* ----------------------------------------------------------------------------
* | 72  |4/19/2007 | Removed un-necessary flow statements            | Sriram |
* -----------------------------------------------------------------------------
* | 71  |2/23/2007 | Changes for EFFISTREAM                          |Prachiti|
* -----------------------------------------------------------------------------
* | 70  |2/8/2007  | Changes for In-direct VLAN                      | Sriram |
* -----------------------------------------------------------------------------
* | 69  |1/29/2007 | Changes for Ethernet VLAN restriction           | Sriram |
* -----------------------------------------------------------------------------
* | 68  |1/11/2007 | Changes for VLAN BCAST optimization             | Sriram |
* -----------------------------------------------------------------------------
* | 67  |11/8/2006 | Changes for compression support                 | Sriram |
* -----------------------------------------------------------------------------
* | 66  |6/14/2006 | Misc corrections                                | Sriram |
* -----------------------------------------------------------------------------
* | 65  |6/14/2006 | Changes for disallow_other_sta	              |Prachiti|
* -----------------------------------------------------------------------------
* | 64  |3/14/2006 | ACL changes                                     | Sriram |
* -----------------------------------------------------------------------------
* | 63  |03/08/2006| DS BC/MC packet bug fix corrected               | Sriram |
* -----------------------------------------------------------------------------
* | 62  |03/02/2006| DS BC/MC packet bug fix                         | Sriram |
* -----------------------------------------------------------------------------
* | 61  |10/5/2005 | MIP related misc changes                        | Sriram |
* -----------------------------------------------------------------------------
* | 60  |10/5/2005 | Changes for enabling MIP                        | Sriram |
* -----------------------------------------------------------------------------
* | 59  |9/22/2005 | VLAN info NULL check added                      | Sriram |
* -----------------------------------------------------------------------------
* | 58  |9/12/2005 | IMCP packet checking logic fixed                | Sriram |
* -----------------------------------------------------------------------------
* | 57  |6/18/2005 | STA stats updated                               | Sriram |
* -----------------------------------------------------------------------------
* | 56  |5/9/2005  | Service type checking corrected                 | Sriram |
* -----------------------------------------------------------------------------
* | 55  |4/25/2005 | Psuedo WM bug fixed                             | Sriram |
* -----------------------------------------------------------------------------
* | 54  |4/13/2005 | Corrections to security implementation          | Sriram |
* -----------------------------------------------------------------------------
* | 53  |4/12/2005 | Packet allocation NULL checking added           | Sriram |
* -----------------------------------------------------------------------------
* | 52  |4/12/2005 | Broadcast packet encryption handling corrected  | Sriram |
* -----------------------------------------------------------------------------
* | 51  |4/11/2005 | Vlan implementation corrected                   | Sriram |
* -----------------------------------------------------------------------------
* | 50  |4/6/2005  | Changes for 802.1x/802.11i                      | Sriram |
* -----------------------------------------------------------------------------
* | 49  |2/24/2005 | Implemented vlan tag checking                   | Abhijit|
* -----------------------------------------------------------------------------
* | 48  |1/6/2005  | Implemented service type checking               | Sriram |
* -----------------------------------------------------------------------------
* | 47  |12/22/2004| Changes for 802.11b client in 802.11g mode      | Sriram |
* -----------------------------------------------------------------------------
* | 46  |7/24/2004 | Fixed minor compilation warnings                | Sriram |
* -----------------------------------------------------------------------------
* | 45  |7/24/2004 | sta_release call changes                        | Anand  |
* -----------------------------------------------------------------------------
* | 44  |7/23/2004 | SEND_UP_IMCP processing added                   | Anand  |
* -----------------------------------------------------------------------------
* | 43  |7/19/2004 | Fixed bit rate and last tx time code            | Sriram |
* -----------------------------------------------------------------------------
* | 42  |7/19/2004 | rssi added in on_data_reception                 | Anand  |
* -----------------------------------------------------------------------------
* | 41  |7/19/2004 | if_transmit_list, to set transmit rate          | Bindu  |
* -----------------------------------------------------------------------------
* | 40  |7/2/2004  | Misc changes for release sta                    | Anand  |
* -----------------------------------------------------------------------------
* | 39  |6/30/2004 | Bug fixed - sta purging if packet come from DS  | Anand  |
* -----------------------------------------------------------------------------
* | 38  |6/29/2004 | Buf Fixed-Sta RefCount, pkt drop for pseudo WM  | Anand  |
* -----------------------------------------------------------------------------
* | 37  |6/21/2004 | Misc Changes - access_point_release_sta         | Bindu  |
* -----------------------------------------------------------------------------
* | 36  |6/17/2004 | Misc changes                                    | Sriram |
* -----------------------------------------------------------------------------
* | 35  |6/16/2004 | Fixes for pSeudo WM                             | Anand  |
* -----------------------------------------------------------------------------
* | 34  |6/14/2004 | Added AL_ASSERT macro                           | Bindu  |
* -----------------------------------------------------------------------------
* | 33  |6/11/2004 | packet_is_imcp_udp_ip shifted to .h file        | Sriram |
* -----------------------------------------------------------------------------
* | 32  |6/11/2004 | Changes in IMCP handling                        | Sriram |
* -----------------------------------------------------------------------------
* | 31  |6/10/2004 | Removed compilation warnings                    | Sriram |
* -----------------------------------------------------------------------------
* | 30  |6/10/2004 | Removed compilation warnings                    | Sriram |
* -----------------------------------------------------------------------------
* | 29  |6/10/2004 | Misc changes including bcast handling           | Sriram |
* -----------------------------------------------------------------------------
* | 28  |6/9/2004  | Misc bugs + STA verification for DS packets     | Sriram |
* -----------------------------------------------------------------------------
* | 27  |6/7/2004  | Fixed packet->type checking bug                 | Sriram |
* -----------------------------------------------------------------------------
* | 26  |6/4/2004  | Print log macros used                           | Bindu  |
* -----------------------------------------------------------------------------
* | 25  |6/4/2004  | Misc fixes for Torna                            | Sriram |
* -----------------------------------------------------------------------------
* | 24  |6/2/2004  | Used config and encapsulation directly          | Sriram |
* -----------------------------------------------------------------------------
* | 23  |6/2/2004  | fixed packet position & addr copying bugs       | Bindu  |
* -----------------------------------------------------------------------------
* | 22  |6/2/2004  | fixed misc bugs in process_wm_packet()          | Bindu  |
* -----------------------------------------------------------------------------
* | 21  |5/31/2004 | fixed misc bugs                                 | Bindu  |
* -----------------------------------------------------------------------------
* | 20  |5/31/2004 | included access_point_sta.h                     | Bindu  |
* -----------------------------------------------------------------------------
* | 19  |5/31/2004 | included access_point_globals.h                 | Bindu  |
* -----------------------------------------------------------------------------
* | 18  |5/28/2004 | Changed broadcast packet handling               | Bindu  |
* -----------------------------------------------------------------------------
* | 17  |5/28/2004 | Packet released                                 | Bindu  |
* -----------------------------------------------------------------------------
* | 16  |5/28/2004 | Reference count changes                         | Bindu  |
* -----------------------------------------------------------------------------
* | 15  |5/28/2004 | removed busy flag setting for packet            | Bindu  |
* -----------------------------------------------------------------------------
* | 14  |5/26/2004 | addr checking correted for up stack packets     | Anand  |
* -----------------------------------------------------------------------------
* | 13  |5/26/2004 | log msg added.                                  | Anand  |
* -----------------------------------------------------------------------------
* | 12  |5/21/2004 | return values corrected in check imcp func      | Anand  |
* -----------------------------------------------------------------------------
* | 11  |5/21/2004 | Log type added to al_print_log                  | Anand  |
* -----------------------------------------------------------------------------
* | 10  |5/20/2004 | corrected processing of IMCP Data Packet        | Bindu  |
* -----------------------------------------------------------------------------
* |  9  |5/20/2004 | fixed al_print_log statements                   | Bindu  |
* -----------------------------------------------------------------------------
* |  8  |5/11/2004 | _packet_is_imcp_udp_ip() prototype changed      | Anand  |
* -----------------------------------------------------------------------------
* |  7  |5/6/2004  | imcp packet check moved to top                  |Prachiti|
* -----------------------------------------------------------------------------
* |  6  |5/5/2004  | Packet Debug params added                       | Anand  |
* -----------------------------------------------------------------------------
* |  5  |4/30/2004 | Added al_print_log sts                          | Bindu  |
* -----------------------------------------------------------------------------
* |  4  |4/23/2004 | Added pseudo-wm packet processing               | Bindu  |
* -----------------------------------------------------------------------------
* |  3  |4/23/2004 | Added packet rx & tx time of STA's to AP Queue  | Bindu  |
* -----------------------------------------------------------------------------
* |  2  |4/22/2004 | Made changes for packet buffer position setting | Bindu  |
* -----------------------------------------------------------------------------
* |  1  |4/22/2004 | **Made (major) changes for AL_CONTEXT**         | Bindu  |
* -----------------------------------------------------------------------------
* |  0  |4/12/2004 | Created                                         | Bindu  |
* -----------------------------------------------------------------------------
********************************************************************************/

#include <linux/string.h>
#include <linux/module.h>
#include "al.h"
#include "al_802_11.h"
#include "access_point.h"
#include "al_impl_context.h"
#include "sip_conf.h"
#include "mesh_defs.h"
#include "imcp_snip.h"
#include "access_point_thread.h"
#include "access_point_sta.h"
#include "access_point_globals.h"
#include "al_print_log.h"
#include "access_point_mgt.h"
#include "access_point_data.h"
#include "al_conf.h"
#include "access_point_vlan.h"
#include "access_point_dot11i.h"
#include "access_point_acl.h"
#include "mesh_ap.h"
#include "access_point_effistream.h"
#include "access_point_multicast.h"
#include "meshap_igmp.h"
#include "meshap_dhcp.h"
#include "meshap_arp.h"
#include "mesh_defs.h"
#include "mesh_table.h"
#include "mesh.h"
#include "mesh_globals.h"
#include "meshap_sip.h"

#define _PRINT_HW_ADDRESS(msg, addr)                                    \
   al_print_log(AL_CONTEXT AL_LOG_TYPE_DEBUG, "%s - %x:%x:%x:%x:%x:%x", \
                msg,                                                    \
                addr->bytes[0],                                         \
                addr->bytes[1],                                         \
                addr->bytes[2],                                         \
                addr->bytes[3],                                         \
                addr->bytes[4],                                         \
                addr->bytes[5])


#define _PACKET_IS_WDS(fc)                  ((fc & AL_802_11_FC_FROMDS) && (fc & AL_802_11_FC_TODS))

#define _PACKET_IS_FROMDS(fc)               ((fc & AL_802_11_FC_FROMDS) && !(fc & AL_802_11_FC_TODS))

#define _PACKET_IS_TODS(fc)                 ((!(fc & AL_802_11_FC_FROMDS)) && (fc & AL_802_11_FC_TODS))

#define _NET_IF_SECURITY_ENABLED(config)    ((config)->security_info.dot11.wep.enabled || (config)->security_info.dot11.rsn.enabled)

#define _IGMP_SUPPORT                                    (globals.config.igmp_support)
#define _DHCP_SUPPORT                                    (globals.config.dhcp_support)
#define _SIP_SUPPORT                                     (globals.config.sip_info.sip_enabled)

#define _TRANSMIT_HELPER_TYPE_UNICAST_BUFFER_DISABLED    0
#define _TRANSMIT_HELPER_TYPE_UNICAST_BUFFER_ENABLED     1
#define _TRANSMIT_HELPER_TYPE_BROADCAST                  2

extern int func_mode;


static AL_INLINE int _check_ethernet_ingress(AL_CONTEXT_PARAM_DECL al_net_if_t *net_if,
                                             al_packet_t                       *packet)
{
   /**
    * Ingress logic for Ethernet devices:
    *
    * If the interface is setup to receive specific VLANs only, then all untagged
    * packets coming over this interface is assumed to be tagged for that VLAN.
    */

   switch (net_if->config.allowed_vlan_tag)
   {
   case AL_NET_IF_CONFIG_ALLOWED_VLAN_NONE:
      if (packet->dot1q_tag != AL_PACKET_VLAN_TAG_TYPE_UNTAGGED)
      {
         return 0;
      }
      break;

   case AL_NET_IF_CONFIG_ALLOWED_VLAN_ALL:
      return 1;

   default:
      if (packet->dot1q_tag == AL_PACKET_VLAN_TAG_TYPE_UNTAGGED)
      {
         packet->dot1q_mode     = AL_PACKET_DOT_1Q_MODE_TAG_ADD;
         packet->dot1q_tag      = net_if->config.allowed_vlan_tag;
         packet->dot1p_priority = 0;
      }
      else if (packet->dot1q_tag != net_if->config.allowed_vlan_tag)
      {
         return 0;
      }
   }

   return 1;
}


static AL_INLINE void _set_effistream_bit_rate(AL_CONTEXT_PARAM_DECL al_net_if_t *net_if,
                                               al_packet_t                       *packet)
{
   access_point_netif_config_info_t *net_if_config_info;

   net_if_config_info = (access_point_netif_config_info_t *)net_if->config.ext_config_info;

   if (net_if_config_info != NULL)
   {
      packet->bit_rate = net_if_config_info->mbps_to_rate_index[packet->bit_rate];
   }
}


static AL_INLINE void _transmit_helper(AL_CONTEXT_PARAM_DECL al_net_if_t *net_if,
                                       int                               type,
                                       al_packet_t                       *packet)
{
   int requeue;

   requeue = 0;


   switch (AL_ATOMIC_GET(net_if->buffering_state))
   {
   case AL_NET_IF_BUFFERING_STATE_NONE:
      if (type == _TRANSMIT_HELPER_TYPE_UNICAST_BUFFER_ENABLED)
      {
         requeue = 1;
      }
      break;

   case AL_NET_IF_BUFFERING_STATE_REQUEUE:
      requeue = 1;
      break;

   case AL_NET_IF_BUFFERING_STATE_DROP:
      al_release_packet(AL_CONTEXT packet);
	  (tx_rx_pkt_stats.packet_drop[2])++;
      return;
   }

   if (requeue == 1)
   {
      globals.ap_queue->queue_requeue_packet(AL_CONTEXT globals.ap_queue, net_if, packet);
   }
   else
   {
      if (packet->flags & AL_PACKET_FLAGS_FIXED_RATE)
      {
         _set_effistream_bit_rate(net_if, packet);
      }
      net_if->transmit(net_if, packet);
   }
}


static AL_INLINE int _process_igmp_membership_packet(AL_CONTEXT_PARAM_DECL al_net_if_t *net_if,
                                                     al_packet_t                       *packet,
                                                     al_net_addr_t                     *src_addr)
{
   meshap_igmp_group_info_t         group_info;
   al_net_addr_t                    mcast_addr;
   access_point_netif_config_info_t *net_if_config_info;
   int i;
   int ret;

   ret = 0;

   net_if_config_info = (access_point_netif_config_info_t *)net_if->config.ext_config_info;

   if (net_if_config_info == NULL)
   {
      return -1;
   }

   /* check if it is a IGMP packet */

   ret = meshap_igmp_process_membership_packet(packet, &group_info);

   if ((ret == -1) || (group_info.count <= 0))
   {
      return -1;
   }

   for (i = 0; i < group_info.count; i++)
   {
      memset(&mcast_addr, 0, sizeof(al_net_addr_t));

      MESHAP_IGMP_MCAST_MAC_FROM_IP_ADDR(&mcast_addr, group_info.group_record[i].address);

      switch (group_info.group_record[i].type)
      {
      case MESHAP_IGMP_TYPE_JOIN_GROUP:
         /*add to mcast hash*/
         al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,
                      "AP : IGMP join group packet received for " AL_NET_ADDR_STR,
                      AL_NET_ADDR_TO_STR(src_addr));
         access_point_mcast_add_entry(net_if_config_info, &mcast_addr, src_addr);
         return 0;

      case MESHAP_IGMP_TYPE_LEAVE_GROUP:
         /*leave from mcast hash*/
         al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,
                      "AP : IGMP leave group packet received for " AL_NET_ADDR_STR,
                      AL_NET_ADDR_TO_STR(src_addr));
         access_point_mcast_remove_entry(net_if_config_info, &mcast_addr, src_addr);
         return 0;
      }
   }

   return 1;
}


static AL_INLINE access_point_mcast_t *_find_multicast_entry(AL_CONTEXT_PARAM_DECL al_net_if_t *net_if,
                                                             al_packet_t                       *packet,
                                                             al_net_addr_t                     *dst_addr)
{
   access_point_netif_config_info_t *net_if_config_info;

   net_if_config_info = (access_point_netif_config_info_t *)net_if->config.ext_config_info;

   return access_point_mcast_get_entry(AL_CONTEXT net_if_config_info, dst_addr);
}


static AL_INLINE int _wm_multicast_helper(AL_CONTEXT_PARAM_DECL al_packet_t *packet,
                                          access_point_mcast_t              *mcast_entry,
                                          al_net_addr_t                     *src_addr,
                                          al_net_addr_t                     *dest_addr,
                                          access_point_netif_config_info_t  *net_if_config_info)
{
   access_point_sta_entry_t *sta_entry;
   int ret;

   ret = -1;
   if((packet->src_addr_sta_entry != NULL)) {
	   sta_entry = packet->src_addr_sta_entry;
   } else {
   sta_entry = access_point_get_sta(AL_CONTEXT src_addr);
   packet->src_addr_sta_entry = sta_entry;
   }

   if (sta_entry != NULL)       /*direct child */
   {
      if ((mcast_entry->indirect_sta_count > 1) || (mcast_entry->sta_count > 0))
      {
         ret = 0;
      }
   }
   else if (mesh_ap_is_table_entry_present(AL_CONTEXT src_addr) == 0)         /* in-direct child */
   {
      if ((mcast_entry->sta_count > 0) || (AL_ATOMIC_GET(net_if_config_info->imcp_sta_count) > 1))
      {
         ret = 0;
      }
   }

   return ret;
}


static AL_INLINE void _wm_broadcast_send_helper(AL_CONTEXT_PARAM_DECL access_point_netif_config_info_t *net_if_config_info,
                                                al_net_if_t                                            *rx_net_if,
                                                al_packet_t                                            *packet,
                                                al_net_addr_t                                          *dest_addr,
                                                al_net_addr_t                                          *src_addr,
                                                int                                                    i,
                                                int                                                    encrypt,
                                                unsigned short                                         vlan_tag,
                                                unsigned char                                          dot1p_priority,
                                                int                                                    mesh_ignore)
{
   al_packet_t *out_packet;

   if (_IGMP_SUPPORT && AL_NET_ADDR_IS_MULTICAST(dest_addr))
   {
      access_point_mcast_t *mcast_entry;

      mcast_entry = _find_multicast_entry(globals.config.wm_net_if_info[i].net_if, packet, dest_addr);

      if (mcast_entry == NULL)
      {
		 (tx_rx_pkt_stats.packet_drop[3])++;
         return;
      }

      if (rx_net_if == globals.config.wm_net_if_info[i].net_if)
      {
         /**
          * For same WM:
          *		Lookup dest mac in mcast entry hash table
          *			if not present do not transmit
          *			if present and src is an indirect child
          *				transmit if (mcast entry�s sta_count > 0) or (wm�s imcp_sta_count > 1)
          *			if present and src is a direct child
          *				transmit if (mcast entry�s sta_count > 1 or indr_sta_count > 0)
          */

         if (_wm_multicast_helper(AL_CONTEXT packet, mcast_entry, src_addr, dest_addr, net_if_config_info) == -1)
         {
		    (tx_rx_pkt_stats.packet_drop[4])++;
            return;
         }
      }
      else
      {
         /**
          * For other WMs:
          *		Do not transmit if there are no direct or in-direct STAs
          */

         if ((mcast_entry->indirect_sta_count <= 0) && (mcast_entry->sta_count <= 0))
         {
		    (tx_rx_pkt_stats.packet_drop[5])++;
            return;
         }
      }
   }

   out_packet = al_allocate_packet(AL_CONTEXT AL_PACKET_DIRECTION_OUT AL_PACKET_DEBUG_PARAM);

   if (out_packet != NULL)
   {
      out_packet->position -= packet->data_length;
      memcpy(out_packet->buffer + out_packet->position, packet->buffer + packet->position, packet->data_length);
      out_packet->data_length = packet->data_length;
      out_packet->type        = packet->type;

      if (vlan_tag == AL_PACKET_VLAN_TAG_TYPE_UNTAGGED)
      {
         out_packet->dot1q_mode = AL_PACKET_DOT_1Q_MODE_NONE;
         if (mesh_ignore)
         {
            out_packet->flags |= AL_PACKET_FLAGS_MESH_IGNORE;
            ++net_if_config_info->untagged_bcast_reordered_count;
         }
         else
         {
            ++net_if_config_info->untagged_bcast_count;
         }
      }
      else
      {
         out_packet->dot1q_mode     = AL_PACKET_DOT_1Q_MODE_TAG_ADD;
         out_packet->dot1q_tag      = vlan_tag;
         out_packet->dot1p_priority = dot1p_priority;
         ++net_if_config_info->tagged_bcast_count;
      }

      if (AL_NET_IF_IS_WIRELESS(globals.config.wm_net_if_info[i].net_if))
      {
         memcpy(&out_packet->addr_1, dest_addr, sizeof(al_net_addr_t));
         memcpy(&out_packet->addr_2, &globals.config.wm_net_if_info[i].net_if->config.hw_addr, sizeof(al_net_addr_t));
         memcpy(&out_packet->addr_3, src_addr, sizeof(al_net_addr_t));
         out_packet->frame_control = AL_802_11_FC_FROMDS;
      }
      else
      {
         memcpy(&out_packet->addr_1, dest_addr, sizeof(al_net_addr_t));
         memcpy(&out_packet->addr_2, src_addr, sizeof(al_net_addr_t));
      }

      out_packet->flags &= ~AL_PACKET_FLAGS_ENCRYPTION;
      out_packet->flags &= ~AL_PACKET_FLAGS_COMPRESSION;

      if (encrypt && net_if_config_info->group_key.enabled)
      {
         out_packet->flags    |= AL_PACKET_FLAGS_ENCRYPTION;
         out_packet->key_index = net_if_config_info->group_key.key_index;
      }

      if (packet->flags & AL_PACKET_FLAGS_DOT11E_ENABLED)
      {
         out_packet->flags          |= AL_PACKET_FLAGS_DOT11E_ENABLED;
         out_packet->dot11e_category = packet->dot11e_category;
      }

      if (packet->flags & AL_PACKET_FLAGS_FIXED_RATE)
      {
         out_packet->flags   |= AL_PACKET_FLAGS_FIXED_RATE;
         out_packet->bit_rate = packet->bit_rate;
         _set_effistream_bit_rate(globals.config.wm_net_if_info[i].net_if, out_packet);
      }

      globals.config.wm_net_if_info[i].net_if->transmit(globals.config.wm_net_if_info[i].net_if, out_packet);
   }
}


#define _ATLEAST_ONE      0
#define _MORE_THAN_ONE    1

static AL_INLINE void _untagged_wm_broadcast_helper(AL_CONTEXT_PARAM_DECL al_net_if_t *rx_net_if,
                                                    al_packet_t                       *packet,
                                                    al_net_addr_t                     *dest_addr,
                                                    al_net_addr_t                     *src_addr,
                                                    int                               is_imcp,
                                                    int                               imcp_sta_compare_count,
                                                    int                               sta_compare_count)
{
   int i;
   access_point_netif_config_info_t *net_if_config_info;
   int encrypt;
   int send;


   for (i = 0; i < globals.config.wm_net_if_count; i++)
   {
      encrypt = 0;
      send    = 0;

      if ((globals.config.wm_net_if_info[i].net_if == rx_net_if) &&
          AL_NET_IF_IS_ETHERNET(rx_net_if))
      {
         continue;
      }

      net_if_config_info = (access_point_netif_config_info_t *)globals.config.wm_net_if_info[i].net_if->config.ext_config_info;

      if (!is_imcp)
      {
         encrypt = _NET_IF_SECURITY_ENABLED(net_if_config_info);

         if (AL_NET_IF_IS_ETHERNET(globals.config.wm_net_if_info[i].net_if))
         {
            send = 1;
         }
         else
         {
            if (rx_net_if != globals.config.wm_net_if_info[i].net_if)
            {
               if ((AL_ATOMIC_GET(net_if_config_info->sta_count) > 0) ||
                   (AL_ATOMIC_GET(net_if_config_info->imcp_sta_count) > 0))
               {
                  send = 1;
               }
            }
            else
            {
               if ((AL_ATOMIC_GET(net_if_config_info->sta_count) > sta_compare_count) ||
                   (AL_ATOMIC_GET(net_if_config_info->imcp_sta_count) > imcp_sta_compare_count))
               {
                  send = 1;
               }
            }
         }
      }
      else
      {
         send    = 1;
         encrypt = _NET_IF_SECURITY_ENABLED(net_if_config_info);
      }

      if (!send)
      {
         continue;
      }

      _wm_broadcast_send_helper(AL_CONTEXT net_if_config_info,
                                rx_net_if,
                                packet,
                                dest_addr,
                                src_addr,
                                i,                                      /* WM index */
                                encrypt,
                                AL_PACKET_VLAN_TAG_TYPE_UNTAGGED,
                                0,
                                0);
   }
}


static AL_INLINE void _tagged_wm_broadcast_helper(AL_CONTEXT_PARAM_DECL al_net_if_t *rx_net_if,
                                                  al_packet_t                       *packet,
                                                  al_net_addr_t                     *dest_addr,
                                                  al_net_addr_t                     *src_addr,
                                                  int                               imcp_sta_compare_count,
                                                  int                               vlan_sta_compare_count,
                                                  unsigned short                    vlan_tag,
                                                  unsigned char                     dot1p_priority)
{
   int i;
   access_point_netif_config_info_t *net_if_config_info;
   int encrypt;
   int tagged_send;
   int untagged_send;

#ifndef ACCESS_POINT_VLAN_BCAST_OPTIMIZE
   vlan_sta_compare_count = _ATLEAST_ONE;
#endif

   for (i = 0; i < globals.config.wm_net_if_count; i++)
   {
      if ((globals.config.wm_net_if_info[i].net_if == rx_net_if) &&
          AL_NET_IF_IS_ETHERNET(rx_net_if))
      {
         continue;
      }

      net_if_config_info = (access_point_netif_config_info_t *)globals.config.wm_net_if_info[i].net_if->config.ext_config_info;
      tagged_send        = 0;
      untagged_send      = 0;

      if (globals.config.wm_net_if_info[i].net_if == rx_net_if)
      {
         if (AL_ATOMIC_GET(net_if_config_info->imcp_sta_count) > imcp_sta_compare_count)
         {
            tagged_send = 1;
         }
         if (access_point_get_net_if_vlan_count(AL_CONTEXT vlan_tag, net_if_config_info) > vlan_sta_compare_count)
         {
            untagged_send = 1;
         }
      }
      else
      {
         if (AL_NET_IF_IS_ETHERNET(globals.config.wm_net_if_info[i].net_if))
         {
            /**
             * Egress rules for ETHERNET devices.
             */
            switch (globals.config.wm_net_if_info[i].net_if->config.allowed_vlan_tag)
            {
            case AL_NET_IF_CONFIG_ALLOWED_VLAN_NONE:

               /**
                * Tagged BCAST packet going to Ethernet WM configured to
                * allow no TAGGED packets.
                */
               untagged_send = 0;
               tagged_send   = 0;
               break;

            case AL_NET_IF_CONFIG_ALLOWED_VLAN_ALL:

               /**
                * Tagged BCAST packet going to Ethernet WM configured to allow
                * all VLANS. Here we assume that a VLAN switch is connected
                * to the port, and would untag the packet appropriately.
                */
               tagged_send   = 1;
               untagged_send = 0;
               break;

            default:

               /**
                * Tagged BCAST packet going to Ethernet WM configured to allow
                * specific VLAN tagged packets only. S it as untagged if the
                * source VLAN tag matches. Otherwise do not send.
                */
               if (globals.config.wm_net_if_info[i].net_if->config.allowed_vlan_tag == vlan_tag)
               {
                  tagged_send   = 0;
                  untagged_send = 1;
               }
               else
               {
                  tagged_send   = 0;
                  untagged_send = 0;
               }
            }
         }
         else
         {
            if (AL_ATOMIC_GET(net_if_config_info->imcp_sta_count) > 0)
            {
               tagged_send = 1;
            }
            if (access_point_get_net_if_vlan_count(AL_CONTEXT vlan_tag, net_if_config_info) > 0)
            {
               untagged_send = 1;
            }
         }
      }

      if (tagged_send)
      {
         encrypt = _NET_IF_SECURITY_ENABLED(net_if_config_info);
         _wm_broadcast_send_helper(AL_CONTEXT net_if_config_info,
                                   rx_net_if,
                                   packet,
                                   dest_addr,
                                   src_addr,
                                   i,                                           /* WM index */
                                   encrypt,
                                   vlan_tag,
                                   dot1p_priority,
                                   0);
      }

      if (untagged_send)
      {
         encrypt = access_point_vlan_get_vlan_encryption(AL_CONTEXT vlan_tag);
         _wm_broadcast_send_helper(AL_CONTEXT net_if_config_info,
                                   rx_net_if,
                                   packet,
                                   dest_addr,
                                   src_addr,
                                   i,                                           /* WM index */
                                   encrypt,
                                   AL_PACKET_VLAN_TAG_TYPE_UNTAGGED,
                                   0,
                                   tagged_send ? 1 : 0);
      }
   }
}


static AL_INLINE void _ds_broadcast_helper(AL_CONTEXT_PARAM_DECL al_packet_t *packet,
                                           al_net_addr_t                     *dst_addr,
                                           al_net_addr_t                     *src_addr,
                                           unsigned short                    vlan_tag,
                                           unsigned char                     dot1p_priority)
{
   al_net_addr_t          parent_ap;
   al_802_11_operations_t *extended_operations;
   al_packet_t            *out_packet;
   al_net_if_t            *send_net_if;

   send_net_if = globals.config.ds_net_if_info.net_if;
   out_packet  = al_allocate_packet(AL_CONTEXT AL_PACKET_DIRECTION_OUT AL_PACKET_DEBUG_PARAM);

   if (out_packet != NULL)
   {
      out_packet->position -= packet->data_length;
      memcpy(out_packet->buffer + out_packet->position, packet->buffer + packet->position, packet->data_length);
      out_packet->type        = packet->type;
      out_packet->data_length = packet->data_length;

      if (AL_NET_IF_IS_WIRELESS(send_net_if))
      {
         extended_operations = send_net_if->get_extended_operations(send_net_if);
         AL_ASSERT("_ds_broadcast_helper", extended_operations != NULL);
         extended_operations->get_bssid(send_net_if, &parent_ap);

         memcpy(&out_packet->addr_1, &parent_ap, sizeof(al_net_addr_t));
         memcpy(&out_packet->addr_2, &send_net_if->config.hw_addr, sizeof(al_net_addr_t));
         memcpy(&out_packet->addr_3, dst_addr, sizeof(al_net_addr_t));
         memcpy(&out_packet->addr_4, src_addr, sizeof(al_net_addr_t));
         out_packet->frame_control = AL_802_11_FC_TODS | AL_802_11_FC_FROMDS;

         if (packet->flags & AL_PACKET_FLAGS_DOT11E_ENABLED)
         {
            out_packet->flags          |= AL_PACKET_FLAGS_DOT11E_ENABLED;
            out_packet->dot11e_category = packet->dot11e_category;
         }

         if (packet->flags & AL_PACKET_FLAGS_EFFISTREAM)
         {
            out_packet->flags |= AL_PACKET_FLAGS_EFFISTREAM;
         }

         if (packet->flags & AL_PACKET_FLAGS_FIXED_RATE)
         {
            out_packet->flags   |= AL_PACKET_FLAGS_FIXED_RATE;
            out_packet->bit_rate = packet->bit_rate;
         }
      }
      else
      {
         memcpy(&out_packet->addr_1, dst_addr, sizeof(al_net_addr_t));
         memcpy(&out_packet->addr_2, src_addr, sizeof(al_net_addr_t));
      }

      if (vlan_tag == AL_PACKET_VLAN_TAG_TYPE_UNTAGGED)
      {
         out_packet->dot1q_mode = AL_PACKET_DOT_1Q_MODE_NONE;
      }
      else
      {
         /**
          * Egress rules for Ethernet devices.
          * No checking is made here to see if send_net_if is ETHERNET
          * because wireless interfaces would be configured with
          * AL_NET_IF_CONFIG_ALLOWED_VLAN_ALL option.
          * With this we avoid an else block thereby reducing code
          * size and improving speed.
          */

         switch (send_net_if->config.allowed_vlan_tag)
         {
         case AL_NET_IF_CONFIG_ALLOWED_VLAN_NONE:
            al_release_packet(AL_CONTEXT out_packet);
		    (tx_rx_pkt_stats.packet_drop[6])++;
            return;

         case AL_NET_IF_CONFIG_ALLOWED_VLAN_ALL:
            out_packet->dot1p_priority = dot1p_priority;
            out_packet->dot1q_tag      = vlan_tag;
            out_packet->dot1q_mode     = AL_PACKET_DOT_1Q_MODE_TAG_ADD;
            break;

         default:
            if (send_net_if->config.allowed_vlan_tag != vlan_tag)
            {
               al_release_packet(AL_CONTEXT out_packet);
		       (tx_rx_pkt_stats.packet_drop[7])++;
               return;
            }
         }
      }

      _transmit_helper(AL_CONTEXT send_net_if, _TRANSMIT_HELPER_TYPE_BROADCAST, out_packet);
   }
}


static AL_INLINE void _process_ds_broadcast_packet(AL_CONTEXT_PARAM_DECL al_net_if_t *net_if,
                                                   al_packet_t                       *packet,
                                                   al_net_addr_t                     *dest_addr,
                                                   al_net_addr_t                     *src_addr,
                                                   int                               is_imcp)
{

   AL_ASSERT("_process_ds_broadcast_packet", net_if != NULL);
   AL_ASSERT("_process_ds_broadcast_packet", packet != NULL);
   AL_ASSERT("_process_ds_broadcast_packet", dest_addr != NULL);
   AL_ASSERT("_process_ds_broadcast_packet", src_addr != NULL);

   /**
    * For wireless DS we only take broadcast packets that are
    * are not marked for ignoring.
    */

   if (AL_NET_IF_IS_WIRELESS(net_if) && !is_imcp)
   {
      if (packet->flags & AL_PACKET_FLAGS_MESH_IGNORE)
      {
		 (tx_rx_pkt_stats.packet_drop[8])++;
         return;
      }
   }

   if (AL_NET_IF_IS_ETHERNET(net_if) && EFFISTREAM_ENABLED)
   {
      if (match_and_apply_criteria(AL_CONTEXT
                                   packet, globals.config.criteria_root->first_child) == MATCH_ACTION_DROP)
      {
		 (tx_rx_pkt_stats.packet_drop[9])++;
         return;
      }
   }

   if (packet->dot1q_tag == AL_PACKET_VLAN_TAG_TYPE_UNTAGGED)
   {
      _untagged_wm_broadcast_helper(AL_CONTEXT net_if, packet, dest_addr, src_addr, is_imcp, _ATLEAST_ONE, _ATLEAST_ONE);
   }
   else
   {
      _tagged_wm_broadcast_helper(AL_CONTEXT net_if, packet, dest_addr, src_addr, _ATLEAST_ONE, _ATLEAST_ONE, packet->dot1q_tag, packet->dot1p_priority);
   }

   globals.access_point_traffic_stats.tx_packets++;
   globals.access_point_traffic_stats.tx_bytes = globals.access_point_traffic_stats.tx_bytes + packet->data_length;

   if (!is_imcp && (packet->dot1q_tag == AL_PACKET_VLAN_TAG_TYPE_UNTAGGED))	//VLAN
   {
      al_add_packet_reference(AL_CONTEXT packet);
      al_send_packet_up_stack(AL_CONTEXT net_if, packet);
   }
}


static AL_INLINE int _process_ds_unicast_packet_vlan_info(AL_CONTEXT_PARAM_DECL al_packet_t *packet,
                                                          access_point_sta_entry_t          *dest_sta,
                                                          int                               decision)
{
   if (dest_sta == NULL)
   {
      /**
       * Destination is not a direct child, could be somewhere down the tree
       */
      if (packet->dot1q_tag == AL_PACKET_VLAN_TAG_TYPE_UNTAGGED)
      {
         packet->dot1q_mode = AL_PACKET_DOT_1Q_MODE_NONE;
      }
      else
      {
         packet->dot1q_mode = AL_PACKET_DOT_1Q_MODE_TAG_ADD;
      }
   }
   else
   {
      /**
       * Destination is a direct child
       */
      if (packet->dot1q_tag == AL_PACKET_VLAN_TAG_TYPE_UNTAGGED)
      {
         /**
          * Case of a Untagged packet to a Direct child
          * In this case if the destination does not belong
          * to the default VLAN we drop the packet
          */

         if (dest_sta->sta.vlan_info != &default_vlan_info)
         {
            return ACCESS_POINT_PACKET_DECISION_DROP;
         }

         packet->dot1q_mode = AL_PACKET_DOT_1Q_MODE_NONE;
      }
      else
      {
         /**
          * Case of a tagged packet to a Direct child.
          * In this case if the destination belongs to the
          * default VLAN, we drop the packet.
          * For destinations on Ethernet WMs we add the tag,
          * else we remove it.
          */

         if (dest_sta->sta.vlan_info == &default_vlan_info)
         {
            return ACCESS_POINT_PACKET_DECISION_DROP;
         }

         if (AL_NET_IF_IS_ETHERNET(dest_sta->sta.net_if))
         {
            /**
             * Egress rules for ETHERNET devices.
             */
            switch (dest_sta->sta.net_if->config.allowed_vlan_tag)
            {
            case AL_NET_IF_CONFIG_ALLOWED_VLAN_NONE:
               return ACCESS_POINT_PACKET_DECISION_DROP;

            case AL_NET_IF_CONFIG_ALLOWED_VLAN_ALL:
               packet->dot1q_mode = AL_PACKET_DOT_1Q_MODE_TAG_ADD;
               break;

            default:
               if (dest_sta->sta.net_if->config.allowed_vlan_tag == packet->dot1q_tag)
               {
                  packet->dot1q_mode = AL_PACKET_DOT_1Q_MODE_TAG_REMOVE;
               }
               else
               {
                  return ACCESS_POINT_PACKET_DECISION_DROP;
               }
            }
         }
         else
         {
            packet->dot1q_mode = AL_PACKET_DOT_1Q_MODE_TAG_REMOVE;
         }
      }
   }

   return decision;
}


static AL_INLINE int _process_ds_unicast_packet_effistream(AL_CONTEXT_PARAM_DECL al_packet_t *packet,
                                                           int                               decision)
{
   int match_ret;

   AL_ASSERT("_process_ds_unicast_packet_effistream", packet != NULL);


   if (AL_NET_IF_IS_ETHERNET(globals.config.ds_net_if_info.net_if))
   {
      /**
       * For ROOT nodes, packets coming from DS are checked for Effistream criteria matching.
       * The actions that are set are carried forward all the way to the last end point.
       */

      if (EFFISTREAM_ENABLED)
      {
         match_ret = match_and_apply_criteria(AL_CONTEXT packet, globals.config.criteria_root->first_child);

         if (match_ret == MATCH_ACTION_DROP)
         {
            return -1;
         }

         if (match_ret == MATCH_ACTION_NO_ACK)
         {
            packet->flags |= AL_PACKET_FLAGS_EFFISTREAM;
         }
      }
   }

   if (decision != ACCESS_POINT_PACKET_DECISION_SEND_WM)
   {
      return -1;
   }

   return decision;
}


static AL_INLINE void _process_ds_unicast_packet(AL_CONTEXT_PARAM_DECL al_net_if_t *net_if,
                                                 al_packet_t                       *packet,
                                                 al_net_addr_t                     *dest_addr,
                                                 al_net_addr_t                     *src_addr,
                                                 int                               is_imcp)
{
   access_point_sta_entry_t *dest_sta;
   access_point_sta_entry_t *sta;
   int            decision;
   unsigned short frame_control_out;
   al_net_addr_t  immediate_dest_addr_out;
   al_net_if_t    *net_if_out;
   int            sta_buffering_enabled;

   AL_ASSERT("_process_ds_unicast_packet", net_if != NULL);
   AL_ASSERT("_process_ds_unicast_packet", packet != NULL);
   AL_ASSERT("_process_ds_unicast_packet", dest_addr != NULL);
   AL_ASSERT("_process_ds_unicast_packet", src_addr != NULL);
   if((packet->dest_addr_sta_entry != NULL)) {
	   sta = packet->dest_addr_sta_entry;
   } else {
   sta = access_point_get_sta(AL_CONTEXT dest_addr);
   packet->dest_addr_sta_entry = sta;
   }
   sta_buffering_enabled = 0;

   if ((sta == NULL) ||
       (sta->sta.state != ACCESS_POINT_STA_STATE_ASSOCIATED))
   {
      if (on_data_reception_handler != NULL)
      {
         decision = on_data_reception_handler(AL_CONTEXT net_if,
                                              packet->rssi,
                                              &packet->addr_2,
                                              src_addr,
                                              dest_addr,
                                              &frame_control_out,
                                              &immediate_dest_addr_out,
                                              &net_if_out);
      }
      else
      {
		 (tx_rx_pkt_stats.packet_drop[138])++;
         decision = ACCESS_POINT_PACKET_DECISION_DROP;
      }
   }
   else
   {
      frame_control_out = AL_802_11_FC_FROMDS;
      decision          = ACCESS_POINT_PACKET_DECISION_SEND_WM;
      net_if_out        = sta->sta.net_if;
   }

   /* vlan checking start */
   dest_sta = sta;
   decision = _process_ds_unicast_packet_vlan_info(packet, dest_sta, decision);

   /* vlan checking  */

   decision = _process_ds_unicast_packet_effistream(AL_CONTEXT packet, decision);

   if (decision == ACCESS_POINT_PACKET_DECISION_SEND_WM)
   {
      if (AL_NET_IF_IS_WIRELESS(net_if_out))
      {
         if (frame_control_out & AL_802_11_FC_FROMDS && frame_control_out & AL_802_11_FC_TODS)
         {
            memcpy(&packet->addr_1, &immediate_dest_addr_out, sizeof(al_net_addr_t));
            memcpy(&packet->addr_2, &net_if_out->config.hw_addr, sizeof(al_net_addr_t));
            memcpy(&packet->addr_3, dest_addr, sizeof(al_net_addr_t));
            memcpy(&packet->addr_4, src_addr, sizeof(al_net_addr_t));
			dest_sta = access_point_get_sta(AL_CONTEXT & immediate_dest_addr_out);
            if (dest_sta != NULL)
            {
               dest_sta->sta.last_packet_transmitted = al_get_tick_count(AL_CONTEXT_SINGLE);
               dest_sta->sta.bytes_received         += packet->data_length;
               if (dest_sta->sta.b_client)
               {
                  packet->flags |= AL_PACKET_FLAGS_USE_802_11B_RATES;
               }
               packet->flags &= ~AL_PACKET_FLAGS_ENCRYPTION;
               if (dest_sta->sta.auth_state & ACCESS_POINT_STA_AUTH_STATE_ENCRYPTION)
               {
                  packet->flags |= AL_PACKET_FLAGS_ENCRYPTION;
               }
               if (dest_sta->sta.auth_state & ACCESS_POINT_STA_AUTH_STATE_COMPRESSION)
               {
                  packet->flags |= AL_PACKET_FLAGS_COMPRESSION;
               }
               packet->key_index     = dest_sta->sta.key_index;
               sta_buffering_enabled = (dest_sta->sta.auth_state & ACCESS_POINT_STA_AUTH_STATE_BUFFERING);
			   access_point_release_sta(AL_CONTEXT dest_sta);
			}
         }
         else if (frame_control_out & AL_802_11_FC_FROMDS)
         {
            memcpy(&packet->addr_1, dest_addr, sizeof(al_net_addr_t));
            memcpy(&packet->addr_2, &net_if_out->config.hw_addr, sizeof(al_net_addr_t));
            memcpy(&packet->addr_3, src_addr, sizeof(al_net_addr_t));
            if (sta->sta.b_client)
            {
               packet->flags |= AL_PACKET_FLAGS_USE_802_11B_RATES;
            }
            packet->flags &= ~AL_PACKET_FLAGS_ENCRYPTION;
            if (sta->sta.auth_state & ACCESS_POINT_STA_AUTH_STATE_ENCRYPTION)
            {
               packet->flags |= AL_PACKET_FLAGS_ENCRYPTION;
            }
            if (sta->sta.auth_state & ACCESS_POINT_STA_AUTH_STATE_COMPRESSION)
            {
               packet->flags |= AL_PACKET_FLAGS_COMPRESSION;
            }
            packet->key_index     = sta->sta.key_index;
            sta_buffering_enabled = (sta->sta.auth_state & ACCESS_POINT_STA_AUTH_STATE_BUFFERING);
         }
         packet->frame_control = frame_control_out;
      }
      else
      {
         memcpy(&packet->addr_1, dest_addr, sizeof(al_net_addr_t));
         memcpy(&packet->addr_2, src_addr, sizeof(al_net_addr_t));
      }

      if (sta != NULL)
      {
         sta->sta.last_packet_transmitted = al_get_tick_count(AL_CONTEXT_SINGLE);
         sta->sta.bytes_received         += packet->data_length;
      }

      globals.access_point_traffic_stats.tx_packets++;
      globals.access_point_traffic_stats.tx_bytes = globals.access_point_traffic_stats.tx_bytes + packet->data_length;

      al_add_packet_reference(AL_CONTEXT packet);
      _transmit_helper(AL_CONTEXT net_if_out, (sta_buffering_enabled != 0), packet);
   }

}

static AL_INLINE int _get_addresses(al_net_if_t *net_if, al_packet_t *packet, al_net_addr_t **dst_addr, al_net_addr_t **src_addr)
{
	int type;

	if (AL_NET_IF_IS_WIRELESS(net_if)){
		type = AL_802_11_FC_GET_TYPE(packet->frame_control);

		if (type == AL_802_11_FC_TYPE_MGMT)
		{
			return 0;
		}

		if (_PACKET_IS_TODS(packet->frame_control))
		{
			*dst_addr = &packet->addr_3;
			*src_addr = &packet->addr_2;
		}
		else if (_PACKET_IS_WDS(packet->frame_control))
		{
			*dst_addr = &packet->addr_3;
			*src_addr = &packet->addr_4;
		}
		else if (_PACKET_IS_FROMDS(packet->frame_control))
		{
			*dst_addr = &packet->addr_1;
			*src_addr = &packet->addr_3;
		}
		else
		{
			/* Ad-hoc packet , not handled by us */
			return -1;
		}
	}else if (AL_NET_IF_IS_ETHERNET(net_if)) {
		*dst_addr = &packet->addr_1;
		*src_addr = &packet->addr_2;
	}else {
		return -1;
	}

	return 1;
}

static AL_INLINE void get_dot11e_priority (AL_CONTEXT_PARAM_DECL al_net_if_t *al_net_if, al_packet_t *packet)
{
	access_point_sta_entry_t *dst_sta;
	access_point_sta_entry_t *src_sta;
	al_net_addr_t            *dst_addr = NULL;
	al_net_addr_t            *src_addr = NULL;
	int ret;
	access_point_vlan_info_t *vlan_info;

	if (al_net_if == NULL){
		return;
	}

	ret = _get_addresses(al_net_if, packet, &dst_addr, &src_addr);

	if ((ret == -1) || (ret == 0)) {
		return ;
	}

	if (globals.config.ds_net_if_info.net_if == al_net_if) {

		if((packet->dest_addr_sta_entry != NULL)) {
			dst_sta = packet->dest_addr_sta_entry;
		}else {
			dst_sta = access_point_get_sta_ex(AL_CONTEXT dst_addr, 0);
			packet->dest_addr_sta_entry = dst_sta;
		}

		if (dst_sta != NULL) {

			if (dst_sta->sta.vlan_info == &default_vlan_info) {
				if (dst_sta->sta.auth_state & ACCESS_POINT_STA_AUTH_STATE_ACL) {
					if (dst_sta->sta.dot11e_enabled) {
						packet->flags |= AL_PACKET_FLAGS_DOT11E_ENABLED;
					}
					packet->dot11e_category = dst_sta->sta.dot11e_category;
				}
			}else {
				if (dst_sta->sta.vlan_info->dot11e_enabled) {
					packet->flags |= AL_PACKET_FLAGS_DOT11E_ENABLED;
				}

				packet->dot11e_category = dst_sta->sta.vlan_info->dot11e_category;
			}

		}else {
			if (packet->dot1q_tag != AL_PACKET_VLAN_TAG_TYPE_UNTAGGED) {
				vlan_info = access_point_vlan_tag_get(AL_CONTEXT packet->dot1q_tag);
				if (vlan_info != NULL) {
					if (vlan_info->dot11e_enabled){
						packet->flags |= AL_PACKET_FLAGS_DOT11E_ENABLED;
					}
					packet->dot11e_category = vlan_info->dot11e_category;
				}else {
					packet->dot11e_category = FILL_DOT11E_CATEGORY_FROM_QOS_CONTROL(packet->qos_control);
				}
			}else {
				if (AL_NET_IF_IS_ETHERNET(al_net_if)) {
					int allow;
					int dot11e_enabled = 0;
					int dot11e_category = 0;

					if (access_point_acl_lookup_entry(AL_CONTEXT dst_addr, &vlan_info,
								&allow, &dot11e_enabled, &dot11e_category)){
						if (dot11e_enabled) {
							packet->flags |= AL_PACKET_FLAGS_DOT11E_ENABLED;
						}
						packet->dot11e_category = dot11e_category;
					}
				}

			}
		}
	}else {
		if((packet->src_addr_sta_entry != NULL)) {
			src_sta = packet->src_addr_sta_entry;
		}else {
			src_sta = access_point_get_sta_ex(AL_CONTEXT src_addr, 0);
			packet->src_addr_sta_entry = src_sta;
		}

		if (src_sta != NULL) {
			if (src_sta->sta.vlan_info == &default_vlan_info) {
				if (src_sta->sta.auth_state & ACCESS_POINT_STA_AUTH_STATE_ACL) {
					if (src_sta->sta.dot11e_enabled) {
						packet->flags |= AL_PACKET_FLAGS_DOT11E_ENABLED;
					}
					packet->dot11e_category = src_sta->sta.dot11e_category;
				}
			}else {
				if (src_sta->sta.vlan_info->dot11e_enabled) {
					packet->flags |= AL_PACKET_FLAGS_DOT11E_ENABLED;
				}
				packet->dot11e_category = src_sta->sta.vlan_info->dot11e_category;
			}

		}else {
			if (packet->dot1q_tag != AL_PACKET_VLAN_TAG_TYPE_UNTAGGED) {
				vlan_info = access_point_vlan_tag_get(AL_CONTEXT packet->dot1q_tag);
				if (vlan_info != NULL) {
					if (vlan_info->dot11e_enabled) {
						packet->flags |= AL_PACKET_FLAGS_DOT11E_ENABLED;
					}
					packet->dot11e_category = vlan_info->dot11e_category;
				}else {
					packet->dot11e_category = FILL_DOT11E_CATEGORY_FROM_QOS_CONTROL(packet->qos_control);
				}
			}else {
				packet->dot11e_category = FILL_DOT11E_CATEGORY_FROM_QOS_CONTROL(packet->qos_control);
			}

		}
	}
	return ;
}

static AL_INLINE void _process_ds_packet(AL_CONTEXT_PARAM_DECL al_net_if_t *net_if, al_packet_t *packet, int is_imcp)
{
   al_net_addr_t            src_addr;
   al_net_addr_t            dst_addr;
   al_net_addr_t            intermediate_addr;
   unsigned char            packet_for_us;
   int                      i;
   int                      drop;
   access_point_sta_entry_t *sta;
   unsigned long            verification_time;

   AL_ASSERT("_process_ds_packet", net_if != NULL);
   AL_ASSERT("_process_ds_packet", packet != NULL);

   if (AL_NET_IF_IS_ETHERNET(net_if))
   {
      memcpy(&dst_addr, &packet->addr_1, sizeof(al_net_addr_t));
      memcpy(&src_addr, &packet->addr_2, sizeof(al_net_addr_t));

      if (!_check_ethernet_ingress(AL_CONTEXT net_if, packet))
      {
		 (tx_rx_pkt_stats.packet_drop[10])++;
         return;
      }
   }
   else
   {
      if (_PACKET_IS_WDS(packet->frame_control))
      {
         memcpy(&dst_addr, &packet->addr_3, sizeof(al_net_addr_t));
         memcpy(&intermediate_addr, &packet->addr_1, sizeof(al_net_addr_t));
         memcpy(&src_addr, &packet->addr_4, sizeof(al_net_addr_t));
      }
      else if (_PACKET_IS_FROMDS(packet->frame_control))
      {
         memcpy(&dst_addr, &packet->addr_1, sizeof(al_net_addr_t));
         memcpy(&src_addr, &packet->addr_3, sizeof(al_net_addr_t));
         memcpy(&intermediate_addr, &packet->addr_1, sizeof(al_net_addr_t));
      }
      else if (_PACKET_IS_TODS(packet->frame_control))
      {
		 (tx_rx_pkt_stats.packet_drop[11])++;
         return;
      }

      if (!AL_NET_ADDR_EQUAL((&intermediate_addr), (&net_if->config.hw_addr)) &&
          !AL_NET_ADDR_IS_BROADCAST(&intermediate_addr) &&
          !AL_NET_ADDR_IS_MULTICAST(&intermediate_addr))
      {
		 (tx_rx_pkt_stats.packet_drop[12])++;
         return;
      }
   }

   packet_for_us = 0;

   if (AL_NET_ADDR_EQUAL((&dst_addr), (&net_if->config.hw_addr)))
   {
      packet_for_us = 1;
   }
   else
   {
      for (i = 0; i < globals.config.wm_net_if_count; i++)
      {
         if (AL_NET_ADDR_EQUAL((&dst_addr), (&globals.config.wm_net_if_info[i].net_if->config.hw_addr)))
         {
            packet_for_us = 1;
            break;
         }
      }
   }

   if (packet_for_us)
   {
      al_add_packet_reference(AL_CONTEXT packet);
      al_send_packet_up_stack(AL_CONTEXT net_if, packet);
      return;
   }

   drop = 0;

   if (AL_NET_ADDR_IS_BROADCAST(&dst_addr) || AL_NET_ADDR_IS_MULTICAST(&dst_addr))
   {
	  if((packet->src_addr_sta_entry != NULL)) {
		  sta = packet->src_addr_sta_entry;
	  } else {
      sta = access_point_get_sta(AL_CONTEXT & src_addr);
	  packet->src_addr_sta_entry = sta;
	  }
      if (sta != NULL)            /** Own child */
      {
         al_u64_t tickcount;
         al_u64_t timeout;

         tickcount = al_get_tick_count(AL_CONTEXT_SINGLE);

         if (sta->sta.is_imcp)
         {
            verification_time = 5;
         }
         else
         {
            verification_time = 5;
         }

         timeout = verification_time * HZ + sta->sta.last_packet_received;
         if (al_timer_after(tickcount, timeout))
		 {
			 al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,
					 "AP : %s child's BC/MC packet came from DS (VT=%d ms) Removing child "
					 "" AL_NET_ADDR_STR,sta->sta.is_imcp ? "IMCP" : "NON-IMCP",
					 verification_time, AL_NET_ADDR_TO_STR(&src_addr));
			 access_point_release_sta(AL_CONTEXT sta);
			 access_point_release_sta_ex(AL_CONTEXT sta, 1, 1);
			 packet->src_addr_sta_entry = NULL;
         }
         else
         {
            drop = 1;
         }
      }
      else               /** Not our own child */

      {
         if (mesh_ap_is_table_entry_present(AL_CONTEXT & src_addr) == 0)                /** Tree Child */
         {
            drop = 1;
         }
      }
   }
   else
   {
	  if((packet->src_addr_sta_entry != NULL)) {
		  sta = packet->src_addr_sta_entry;
	  } else {
      sta = access_point_get_sta(AL_CONTEXT & src_addr);
	  packet->src_addr_sta_entry = sta;
	  }

      if (sta != NULL)
      {
         al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "AP : childs UC packet came from "
		 "DS Removing child " AL_NET_ADDR_STR, AL_NET_ADDR_TO_STR(&src_addr));
		 access_point_release_sta(AL_CONTEXT sta);
		 access_point_release_sta_ex(AL_CONTEXT sta, 1, 1);
		 packet->src_addr_sta_entry = NULL;
      }
   }

   if (drop)
   {
	  (tx_rx_pkt_stats.packet_drop[13])++;
      return;
   }

   if ((AL_NET_ADDR_IS_BROADCAST(&dst_addr)) ||
       (AL_NET_ADDR_IS_MULTICAST(&dst_addr)))
   {
      _process_ds_broadcast_packet(AL_CONTEXT net_if, packet, &dst_addr, &src_addr, is_imcp);
   }
   else
   {
      _process_ds_unicast_packet(AL_CONTEXT net_if, packet, &dst_addr, &src_addr, is_imcp);
   }
}


static AL_INLINE void _process_wm_broadcast_packet(AL_CONTEXT_PARAM_DECL al_net_if_t *net_if,
                                                   al_packet_t                       *packet,
                                                   al_net_addr_t                     *src_addr,
                                                   al_net_addr_t                     *dst_addr,
                                                   int                               is_imcp)
{
   access_point_vlan_info_t *src_vlan;
   access_point_sta_entry_t *src_sta;
   unsigned short           vlan_tag;
   unsigned char            dot1p_priority;
   int imcp_sta_compare_count;
   int sta_compare_count;
   int is_wds;
   int match_ret;

   AL_ASSERT("_process_wm_broadcast_packet", net_if != NULL);
   AL_ASSERT("_process_wm_broadcast_packet", packet != NULL);
   AL_ASSERT("_process_wm_broadcast_packet", dst_addr != NULL);
   AL_ASSERT("_process_wm_broadcast_packet", src_addr != NULL);

   is_wds   = 0;
   src_vlan = &default_vlan_info;

   if (AL_NET_IF_IS_WIRELESS(net_if))
   {
      is_wds = (packet->frame_control & AL_802_11_FC_FROMDS) && (packet->frame_control & AL_802_11_FC_TODS);
   }

   if (!is_wds)
   {
	   if((packet->src_addr_sta_entry != NULL)) {
		   src_sta = packet->src_addr_sta_entry;
	   }else {
		   src_sta = access_point_get_sta(AL_CONTEXT src_addr);
		   packet->src_addr_sta_entry = src_sta;
	   }

      if (src_sta != NULL)
      {
         src_vlan = src_sta->sta.vlan_info;

         if (EFFISTREAM_ENABLED)
         {
            match_ret = match_and_apply_criteria(AL_CONTEXT packet, globals.config.criteria_root->first_child);

            if (match_ret == MATCH_ACTION_DROP)
            {
		   (tx_rx_pkt_stats.packet_drop[14])++;
       return;
            }

            if (match_ret == MATCH_ACTION_NO_ACK)
            {
               packet->flags |= AL_PACKET_FLAGS_EFFISTREAM;
            }
         }
      }

      if (src_vlan == NULL)
      {
         src_vlan = &default_vlan_info;
      }
   }

   if (packet->dot1q_tag == AL_PACKET_VLAN_TAG_TYPE_UNTAGGED)
   {
      if (!is_wds)
      {
         if (src_vlan == &default_vlan_info)
         {
            vlan_tag               = AL_PACKET_VLAN_TAG_TYPE_UNTAGGED;
            dot1p_priority         = 0;
            sta_compare_count      = _MORE_THAN_ONE;
            imcp_sta_compare_count = _ATLEAST_ONE;
         }
         else
         {
            vlan_tag               = src_vlan->vlan_tag;
            dot1p_priority         = src_vlan->dot1p_priority;
            sta_compare_count      = _MORE_THAN_ONE;
            imcp_sta_compare_count = _ATLEAST_ONE;
         }
      }
      else
      {
         vlan_tag               = AL_PACKET_VLAN_TAG_TYPE_UNTAGGED;
         dot1p_priority         = 0;
         sta_compare_count      = _ATLEAST_ONE;
         imcp_sta_compare_count = _MORE_THAN_ONE;
      }
   }
   else
   {
      vlan_tag       = packet->dot1q_tag;
      dot1p_priority = packet->dot1p_priority;

      if (!is_wds)
      {
         if (src_vlan->vlan_tag != packet->dot1q_tag)
         {
		    (tx_rx_pkt_stats.packet_drop[15])++;
            return;
         }
         sta_compare_count      = _MORE_THAN_ONE;
         imcp_sta_compare_count = _ATLEAST_ONE;
      }
      else
      {
         sta_compare_count      = _ATLEAST_ONE;
         imcp_sta_compare_count = _MORE_THAN_ONE;
      }
   }

   if (_IGMP_SUPPORT && AL_NET_ADDR_IS_MULTICAST(dst_addr))
   {
      if (_process_igmp_membership_packet(AL_CONTEXT net_if, packet, src_addr) == 0)
      {
         /**
          * This is a IGMP membership packet, hence only forward on the DS side and return
          */
         _ds_broadcast_helper(AL_CONTEXT packet, dst_addr, src_addr, vlan_tag, dot1p_priority);
         globals.access_point_traffic_stats.tx_packets++;
         globals.access_point_traffic_stats.tx_bytes = globals.access_point_traffic_stats.tx_bytes + packet->data_length;
         return;
      }
   }

   _ds_broadcast_helper(AL_CONTEXT packet, dst_addr, src_addr, vlan_tag, dot1p_priority);

   if (vlan_tag == AL_PACKET_VLAN_TAG_TYPE_UNTAGGED)
   {
      _untagged_wm_broadcast_helper(AL_CONTEXT net_if,
                                    packet,
                                    dst_addr,
                                    src_addr,
                                    is_imcp,
                                    imcp_sta_compare_count,
                                    sta_compare_count);
   }
   else
   {
      _tagged_wm_broadcast_helper(AL_CONTEXT net_if,
                                  packet,
                                  dst_addr,
                                  src_addr,
                                  imcp_sta_compare_count,
                                  sta_compare_count,
                                  vlan_tag,
                                  dot1p_priority);
   }
   if (!is_imcp && (vlan_tag == AL_PACKET_VLAN_TAG_TYPE_UNTAGGED))		//VLAN
   {
      al_add_packet_reference(AL_CONTEXT packet);
      al_send_packet_up_stack(AL_CONTEXT net_if, packet);
   }

   globals.access_point_traffic_stats.tx_packets++;
   globals.access_point_traffic_stats.tx_bytes = globals.access_point_traffic_stats.tx_bytes + packet->data_length;
}


static AL_INLINE int _process_wm_unicast_packet_vlan_info(AL_CONTEXT_PARAM_DECL al_packet_t *packet,
                                                          access_point_sta_entry_t          *dest_sta,
                                                          access_point_sta_entry_t          *src_sta,
                                                          int                               decision)
{
   if (decision == ACCESS_POINT_PACKET_DECISION_SEND_DS)
   {
      if (src_sta != NULL)
      {
         /**
          * Case of a packet from Direct child to be sent to DS
          * In this case, if the direct child belongs to a non-default
          * VLAN, we add the tag.
          */
         if ((src_sta->sta.vlan_info != NULL) &&
             (src_sta->sta.vlan_info != &default_vlan_info))
         {
            packet->dot1p_priority = src_sta->sta.vlan_info->dot1p_priority;
            packet->dot1q_mode     = AL_PACKET_DOT_1Q_MODE_TAG_ADD;
            packet->dot1q_tag      = src_sta->sta.vlan_info->vlan_tag;
         }
         else
         {
            packet->dot1q_mode = AL_PACKET_DOT_1Q_MODE_NONE;
         }
      }
      else
      {
         /**
          * Case of a packet from In-direct child to be sent to DS
          * If the packet came as tagged, we send it as tagged,
          * otherwise we send it untagged.
          */
         if (packet->dot1q_tag != AL_PACKET_VLAN_TAG_TYPE_UNTAGGED)
         {
            packet->dot1q_mode = AL_PACKET_DOT_1Q_MODE_TAG_ADD;
         }
         else
         {
            packet->dot1q_mode = AL_PACKET_DOT_1Q_MODE_NONE;
         }
      }

      if ((packet->dot1q_mode == AL_PACKET_DOT_1Q_MODE_TAG_ADD) &&
          AL_NET_IF_IS_ETHERNET(globals.config.ds_net_if_info.net_if))
      {
         /**
          * Egress rules for ethernet devices
          */
         switch (globals.config.ds_net_if_info.net_if->config.allowed_vlan_tag)
         {
         case AL_NET_IF_CONFIG_ALLOWED_VLAN_NONE:

            /**
             * Tagged packet being sent to a NET_IF that is configured to not accept
             * any tagged packets
             */
            return ACCESS_POINT_PACKET_DECISION_DROP;

         case AL_NET_IF_CONFIG_ALLOWED_VLAN_ALL:

            /**
             * Tagged packet being sent to a NET_IF that is configured to accept
             * all tagged packets. Hence do nothing.
             */
            break;

         default:

            /**
             * Tagged packet being sent to a NET_IF that is configured to accept
             * only specific VLAN tag packets. If the TAG matches then send as untagged
             * else drop.
             */
            if (globals.config.ds_net_if_info.net_if->config.allowed_vlan_tag == packet->dot1q_tag)
            {
               packet->dot1q_mode = AL_PACKET_DOT_1Q_MODE_NONE;
            }
            else
            {
               return ACCESS_POINT_PACKET_DECISION_DROP;
            }
         }
      }
   }
   else if (decision == ACCESS_POINT_PACKET_DECISION_SEND_WM)
   {
      if (packet->dot1q_tag == AL_PACKET_VLAN_TAG_TYPE_UNTAGGED)
      {
         if (src_sta == NULL)
         {
            if (dest_sta != NULL)
            {
               /**
                * Case of Untagged packet from In-direct child
                * going to a direct child. Hence if this direct
                * child does not belong to the default VLAN
                * we drop the packet.
                */

               if (dest_sta->sta.vlan_info != &default_vlan_info)
               {
                  return ACCESS_POINT_PACKET_DECISION_DROP;
               }
               else
               {
                  packet->dot1q_mode = AL_PACKET_DOT_1Q_MODE_NONE;
               }
            }
            else
            {
               /**
                * Case of Untagged packet from In-direct child
                * going to an In-direct child.
                */
               packet->dot1q_mode = AL_PACKET_DOT_1Q_MODE_NONE;
            }
         }
         else
         {
            if (dest_sta != NULL)
            {
               /**
                * Case of Untagged packet from Direct child
                * going to a direct child. If both do not belong to
                * the same VLAN we drop the packet. If the destination
                * belongs to a non-default VLAN and is on a ETHERNET WM
                * we add the tag. Otherwise we do not add a tag.
                */

               if (dest_sta->sta.vlan_info != src_sta->sta.vlan_info)
               {
                  return ACCESS_POINT_PACKET_DECISION_DROP;
               }

               packet->dot1q_mode = AL_PACKET_DOT_1Q_MODE_NONE;

               if ((dest_sta->sta.vlan_info != NULL) &&
                   (dest_sta->sta.vlan_info != &default_vlan_info) &&
                   AL_NET_IF_IS_ETHERNET(dest_sta->sta.net_if))
               {
                  /**
                   * Egress rules for Ethernet downlinks.
                   */
                  switch (dest_sta->sta.net_if->config.allowed_vlan_tag)
                  {
                  case AL_NET_IF_CONFIG_ALLOWED_VLAN_NONE:
                     return ACCESS_POINT_PACKET_DECISION_DROP;                                          /* Pathological case */

                  case AL_NET_IF_CONFIG_ALLOWED_VLAN_ALL:
                     packet->dot1p_priority = dest_sta->sta.vlan_info->dot1p_priority;
                     packet->dot1q_mode     = AL_PACKET_DOT_1Q_MODE_TAG_ADD;
                     packet->dot1q_tag      = dest_sta->sta.vlan_info->vlan_tag;
                     break;

                  default:
                     if (dest_sta->sta.net_if->config.allowed_vlan_tag != src_sta->sta.vlan_info->vlan_tag)
                     {
                        return ACCESS_POINT_PACKET_DECISION_DROP;
                     }
                  }
               }
            }
            else
            {
               /**
                * Case of Untagged packet from Direct child
                * going to an In-direct child. If the source belongs
                * to a non-default VLAN, we add the tag.
                */
               if ((src_sta->sta.vlan_info != NULL) &&
                   (src_sta->sta.vlan_info != &default_vlan_info))
               {
                  packet->dot1p_priority = src_sta->sta.vlan_info->dot1p_priority;
                  packet->dot1q_mode     = AL_PACKET_DOT_1Q_MODE_TAG_ADD;
                  packet->dot1q_tag      = src_sta->sta.vlan_info->vlan_tag;
               }
               else
               {
                  packet->dot1q_mode = AL_PACKET_DOT_1Q_MODE_NONE;
               }
            }
         }
      }
      else
      {
         if (src_sta == NULL)
         {
            if (dest_sta != NULL)
            {
               /**
                * Case of Tagged packet from In-direct child
                * going to a direct child. If the direct child's
                * vlan tag does not match the packet's tag, we drop
                * the packet. If the direct child is on ETHERNET WM
                * we add the tag, else we remove it.
                */

               if ((dest_sta->sta.vlan_info == NULL) ||
                   (dest_sta->sta.vlan_info == &default_vlan_info) ||
                   (dest_sta->sta.vlan_info->vlan_tag != packet->dot1q_tag))
               {
                  return ACCESS_POINT_PACKET_DECISION_DROP;
               }
               packet->dot1q_mode = AL_PACKET_DOT_1Q_MODE_TAG_REMOVE;

               if (AL_NET_IF_IS_ETHERNET(dest_sta->sta.net_if))
               {
                  /**
                   * Egress rules for Ethernet downlinks.
                   */
                  switch (dest_sta->sta.net_if->config.allowed_vlan_tag)
                  {
                  case AL_NET_IF_CONFIG_ALLOWED_VLAN_NONE:
                     return ACCESS_POINT_PACKET_DECISION_DROP;                                          /* Pathological Case */

                  case AL_NET_IF_CONFIG_ALLOWED_VLAN_ALL:
                     packet->dot1p_priority = dest_sta->sta.vlan_info->dot1p_priority;
                     packet->dot1q_mode     = AL_PACKET_DOT_1Q_MODE_TAG_ADD;
                     packet->dot1q_tag      = dest_sta->sta.vlan_info->vlan_tag;
                     break;

                  default:
                     if (dest_sta->sta.net_if->config.allowed_vlan_tag != packet->dot1q_tag)
                     {
                        return ACCESS_POINT_PACKET_DECISION_DROP;
                     }
                  }
               }
            }
            else
            {
               /**
                * Case of Tagged packet from In-direct child
                * going to an In-direct child.
                */
               packet->dot1q_mode = AL_PACKET_DOT_1Q_MODE_TAG_ADD;
            }
         }
         else
         {
            if (dest_sta != NULL)
            {
               /**
                * Case of Tagged packet from Direct child
                * going to a Direct child. If the VLANS dont match
                * we drop the packet. For destinations on Ethernet WMs
                * we add the tag, else we remove it.
                */

               if ((src_sta->sta.vlan_info == NULL) ||
                   (src_sta->sta.vlan_info != dest_sta->sta.vlan_info) ||
                   (src_sta->sta.vlan_info->vlan_tag != packet->dot1q_tag))
               {
                  return ACCESS_POINT_PACKET_DECISION_DROP;
               }

               packet->dot1q_mode = AL_PACKET_DOT_1Q_MODE_TAG_REMOVE;

               if ((dest_sta->sta.vlan_info != NULL) &&
                   AL_NET_IF_IS_ETHERNET(dest_sta->sta.net_if))
               {
                  /**
                   * Egress rules for Ethernet downlinks.
                   */

                  switch (dest_sta->sta.net_if->config.allowed_vlan_tag)
                  {
                  case AL_NET_IF_CONFIG_ALLOWED_VLAN_NONE:
                     return ACCESS_POINT_PACKET_DECISION_DROP;                                          /* Pathological Case */

                  case AL_NET_IF_CONFIG_ALLOWED_VLAN_ALL:
                     packet->dot1p_priority = dest_sta->sta.vlan_info->dot1p_priority;
                     packet->dot1q_mode     = AL_PACKET_DOT_1Q_MODE_TAG_ADD;
                     packet->dot1q_tag      = dest_sta->sta.vlan_info->vlan_tag;
                     break;

                  default:
                     if (dest_sta->sta.net_if->config.allowed_vlan_tag != packet->dot1q_tag)
                     {
                        return ACCESS_POINT_PACKET_DECISION_DROP;
                     }
                  }
               }
            }
            else
            {
               /**
                * Case of Tagged packet from Direct child
                * going to an In-direct child. If the source belongs
                * to a non-default VLAN, we add the tag.
                */

               if ((src_sta->sta.vlan_info == NULL) ||
                   (src_sta->sta.vlan_info == &default_vlan_info) ||
                   (src_sta->sta.vlan_info->vlan_tag != packet->dot1q_tag))
               {
                  return ACCESS_POINT_PACKET_DECISION_DROP;
               }
               packet->dot1p_priority = src_sta->sta.vlan_info->dot1p_priority;
               packet->dot1q_mode     = AL_PACKET_DOT_1Q_MODE_TAG_ADD;
               packet->dot1q_tag      = src_sta->sta.vlan_info->vlan_tag;
            }
         }
      }
   }
   return decision;
}


static AL_INLINE int _process_wm_unicast_packet_effistream(AL_CONTEXT_PARAM_DECL int decision,
                                                           al_packet_t               *packet,
                                                           access_point_sta_entry_t  *src_sta)
{
   if (decision == ACCESS_POINT_PACKET_DECISION_DROP)
   {
      return decision;
   }

   if ((src_sta != NULL) && EFFISTREAM_ENABLED)
   {
      int match_ret;

      match_ret = match_and_apply_criteria(AL_CONTEXT packet, globals.config.criteria_root->first_child);

      if (match_ret == MATCH_ACTION_DROP)
      {
		 (tx_rx_pkt_stats.packet_drop[139])++;
         return ACCESS_POINT_PACKET_DECISION_DROP;
      }

      if (match_ret == MATCH_ACTION_NO_ACK)
      {
         packet->flags |= AL_PACKET_FLAGS_EFFISTREAM;
      }
   }

   return decision;
}


static AL_INLINE void _process_wm_unicast_packet(AL_CONTEXT_PARAM_DECL al_net_if_t *net_if,
                                                 al_packet_t                       *packet,
                                                 al_net_addr_t                     *src_addr,
                                                 al_net_addr_t                     *dst_addr,
                                                 int                               is_imcp,
                                                 int                               service_type)
{
   access_point_sta_entry_t *sta;
   access_point_sta_entry_t *dest_sta;
   access_point_sta_entry_t *src_sta = NULL;
   int                    decision;
   unsigned short         frame_control_out;
   al_net_addr_t          immediate_dest_addr_out;
   al_net_addr_t          parent_ap;
   al_net_if_t            *net_if_out;
   al_802_11_operations_t *extended_operations;
   int                    sta_buffering_enabled;

   AL_ASSERT("_process_wm_unicast_packet", net_if != NULL);
   AL_ASSERT("_process_wm_unicast_packet", packet != NULL);
   AL_ASSERT("_process_wm_unicast_packet", dst_addr != NULL);
   AL_ASSERT("_process_wm_unicast_packet", src_addr != NULL);

   sta_buffering_enabled = 0;
   if((packet->dest_addr_sta_entry != NULL)) {
	   sta = packet->dest_addr_sta_entry;
   } else {
       sta = access_point_get_sta(AL_CONTEXT dst_addr);
	   packet->dest_addr_sta_entry = sta;
   }

   if ((sta == NULL) ||
       (sta->sta.state != ACCESS_POINT_STA_STATE_ASSOCIATED))
   {
      if (on_data_reception_handler != NULL)
      {
         decision = on_data_reception_handler(AL_CONTEXT net_if,
                                              packet->rssi,
                                              &packet->addr_2,
                                              src_addr,
                                              dst_addr,
                                              &frame_control_out,
                                              &immediate_dest_addr_out,
                                              &net_if_out);
      }
      else
      {
         decision   = ACCESS_POINT_PACKET_DECISION_SEND_DS;
         net_if_out = globals.config.ds_net_if_info.net_if;
      }
   }
   else
   {
      frame_control_out = AL_802_11_FC_FROMDS;
      net_if_out        = sta->sta.net_if;
      if (AL_NET_IF_IS_ETHERNET(net_if) && (net_if == net_if_out))
      {
		 (tx_rx_pkt_stats.packet_drop[140])++;
         decision = ACCESS_POINT_PACKET_DECISION_DROP;
      }
      else
      {
         decision = ACCESS_POINT_PACKET_DECISION_SEND_WM;
      }
   }

   /* vlan checking start */
   if((packet->src_addr_sta_entry != NULL)) {
	   src_sta = packet->src_addr_sta_entry;
   } else {
   src_sta  = access_point_get_sta(AL_CONTEXT src_addr);
   packet->src_addr_sta_entry = src_sta;
   }
   dest_sta = sta;
   decision = _process_wm_unicast_packet_vlan_info(packet, dest_sta, src_sta, decision);


   if (decision == ACCESS_POINT_PACKET_DECISION_DROP)
   {
	  (tx_rx_pkt_stats.packet_drop[16])++;
      return;
   }

   /* vlan checking end */

   decision = _process_wm_unicast_packet_effistream(decision, packet, src_sta);


   if (decision == ACCESS_POINT_PACKET_DECISION_DROP)
   {
	  (tx_rx_pkt_stats.packet_drop[17])++;
      return;
   }

   if (decision == ACCESS_POINT_PACKET_DECISION_SEND_WM)
   {
      if (AL_NET_IF_IS_WIRELESS(net_if_out))
      {
         if (frame_control_out & AL_802_11_FC_FROMDS && frame_control_out & AL_802_11_FC_TODS)
         {
            memcpy(&packet->addr_1, &immediate_dest_addr_out, sizeof(al_net_addr_t));
            memcpy(&packet->addr_2, &net_if_out->config.hw_addr, sizeof(al_net_addr_t));
            memcpy(&packet->addr_3, dst_addr, sizeof(al_net_addr_t));
            memcpy(&packet->addr_4, src_addr, sizeof(al_net_addr_t));
			dest_sta = access_point_get_sta(AL_CONTEXT & immediate_dest_addr_out);
            if (dest_sta != NULL)
            {
               dest_sta->sta.last_packet_transmitted = al_get_tick_count(AL_CONTEXT_SINGLE);
               dest_sta->sta.bytes_received         += packet->data_length;
               if (dest_sta->sta.b_client)
               {
                  packet->flags |= AL_PACKET_FLAGS_USE_802_11B_RATES;
               }
               packet->flags &= ~AL_PACKET_FLAGS_ENCRYPTION;
               if (dest_sta->sta.auth_state & ACCESS_POINT_STA_AUTH_STATE_ENCRYPTION)
               {
                  packet->flags |= AL_PACKET_FLAGS_ENCRYPTION;
               }
               if (dest_sta->sta.auth_state & ACCESS_POINT_STA_AUTH_STATE_COMPRESSION)
               {
                  packet->flags |= AL_PACKET_FLAGS_COMPRESSION;
               }
               packet->key_index     = dest_sta->sta.key_index;
               sta_buffering_enabled = (dest_sta->sta.auth_state & ACCESS_POINT_STA_AUTH_STATE_BUFFERING);
			   access_point_release_sta(AL_CONTEXT dest_sta);
			}
         }
         else if (frame_control_out & AL_802_11_FC_FROMDS)
         {
            memcpy(&packet->addr_1, dst_addr, sizeof(al_net_addr_t));
            memcpy(&packet->addr_2, &net_if_out->config.hw_addr, sizeof(al_net_addr_t));
            memcpy(&packet->addr_3, src_addr, sizeof(al_net_addr_t));
            if (sta->sta.b_client)
            {
               packet->flags |= AL_PACKET_FLAGS_USE_802_11B_RATES;
            }
            packet->flags &= ~AL_PACKET_FLAGS_ENCRYPTION;
            if (sta->sta.auth_state & ACCESS_POINT_STA_AUTH_STATE_ENCRYPTION)
            {
               packet->flags |= AL_PACKET_FLAGS_ENCRYPTION;
            }
            if (sta->sta.auth_state & ACCESS_POINT_STA_AUTH_STATE_COMPRESSION)
            {
               packet->flags |= AL_PACKET_FLAGS_COMPRESSION;
            }
            packet->key_index     = sta->sta.key_index;
            sta_buffering_enabled = (sta->sta.auth_state & ACCESS_POINT_STA_AUTH_STATE_BUFFERING);
         }
         packet->frame_control = frame_control_out;
      }
      else
      {
         memcpy(&packet->addr_1, dst_addr, sizeof(al_net_addr_t));
         memcpy(&packet->addr_2, src_addr, sizeof(al_net_addr_t));
      }

      if (sta != NULL)
      {
         sta->sta.last_packet_transmitted = al_get_tick_count(AL_CONTEXT_SINGLE);
         sta->sta.bytes_received         += packet->data_length;
      }
   }
   else if (decision == ACCESS_POINT_PACKET_DECISION_SEND_DS)
   {
      if (AL_NET_IF_IS_WIRELESS(net_if_out))
      {
         if (frame_control_out & AL_802_11_FC_FROMDS && frame_control_out & AL_802_11_FC_TODS)
         {
            extended_operations = net_if_out->get_extended_operations(net_if_out);
            extended_operations->get_bssid(net_if_out, &parent_ap);
            memcpy(&packet->addr_1, &parent_ap, sizeof(al_net_addr_t));
            memcpy(&packet->addr_2, &net_if_out->config.hw_addr, sizeof(al_net_addr_t));
            memcpy(&packet->addr_3, dst_addr, sizeof(al_net_addr_t));
            memcpy(&packet->addr_4, src_addr, sizeof(al_net_addr_t));
            packet->frame_control = frame_control_out;
         }
         else if (frame_control_out & AL_802_11_FC_TODS)
         {
            /**
             * This case should never occur, hence log this event
             */
            AL_PRINT_LOG_ERROR_0("AP		: Error - value of frame control");
         }
      }
      else
      {
         memcpy(&packet->addr_1, dst_addr, sizeof(al_net_addr_t));
         memcpy(&packet->addr_2, src_addr, sizeof(al_net_addr_t));
      }
   }

   al_add_packet_reference(AL_CONTEXT packet);

   _transmit_helper(AL_CONTEXT net_if_out, (sta_buffering_enabled != 0), packet);

   globals.access_point_traffic_stats.tx_packets++;
   globals.access_point_traffic_stats.tx_bytes = globals.access_point_traffic_stats.tx_bytes + packet->data_length;
}


static AL_INLINE void _process_wm_packet(AL_CONTEXT_PARAM_DECL al_net_if_t *net_if, al_packet_t *packet, int is_imcp, int service_type)
{
   access_point_sta_entry_t *sta;
   al_net_addr_t            src_addr;
   al_net_addr_t            dst_addr;
   al_net_addr_t            ta_addr;
   unsigned char            packet_for_us;
   int i;
   access_point_netif_config_info_t *config_info;


   AL_ASSERT("_process_wm_packet", net_if != NULL);
   AL_ASSERT("_process_wm_packet", packet != NULL);

   if (AL_NET_IF_IS_WIRELESS(net_if) && !AL_NET_ADDR_EQUAL(&packet->addr_1, &net_if->config.hw_addr))
   {
	  (tx_rx_pkt_stats.packet_drop[18])++;
      return;
   }

   /**
    * If DHCP support is enabled, we need to watch for DHCP requests and ARP requests
    * coming from the WM.
    */

   if (!is_imcp)
   {
      int         ret;
      al_packet_t *packet_out;

      if ((_DHCP_SUPPORT) && (_packet_is_dhcp_udp_ip(AL_CONTEXT packet) == 0))
      {
         packet_out = NULL;

	     tx_rx_pkt_stats.wm_pkt_process_dhcp++;
         ret = meshap_dhcp_process_packet((meshap_dhcp_instance_t)globals.dhcp_instance,
                                          net_if,
                                          packet,
                                          &packet_out);

         if (ret == MESHAP_DHCP_RETVAL_CONTINUE)
         {
            goto _ahead;
         }

         if (ret != MESHAP_DHCP_RETVAL_SUCCESS)
         {
	        (tx_rx_pkt_stats.packet_drop[124])++;
            return;
         }

         if (packet_out != NULL)
         {
            /**
             * Queue the packet for transmission and return
             */

            net_if->transmit(net_if, packet_out);
         }else {
	        (tx_rx_pkt_stats.packet_drop[125])++;
		 }

         return;
      }
      else if ((_SIP_SUPPORT) && (_packet_is_sip_udp_ip(AL_CONTEXT packet, globals.config.sip_info.port) == 0))
      {
			tx_rx_pkt_stats.wm_pkt_process_sip++;
         ret = meshap_process_sip_packet((meshap_sip_instance_t)globals.sip_instance,
                                         net_if,
                                         packet);

         if (ret == MESHAP_SIP_RETVAL_CONTINUE)
         {
            goto _ahead;
         }

         return;
      }
      else if (packet->type == al_be16_to_cpu(AL_PACKET_TYPE_ARP))
      {
         /* ARP requests will be processed for DHCP, SIP */

         packet_out = NULL;

			tx_rx_pkt_stats.wm_pkt_process_arp++;

         ret = meshap_arp_process_packet((meshap_arp_instance_t)globals.arp_instance,
                                         net_if,
                                         packet,
                                         &packet_out);


         if (ret == MESHAP_ARP_RETVAL_CONTINUE)
         {
            goto _ahead;
         }

         if (ret != MESHAP_ARP_RETVAL_SUCCESS)
         {
            return;
         }

         if (packet_out != NULL)
         {
            /**
             * Queue the packet for transmission and return
             */

            net_if->transmit(net_if, packet_out);
         }else {
	        (tx_rx_pkt_stats.packet_drop[57])++;
		 }

         return;
      }
   }

_ahead:

   packet_for_us = 0;


   if (AL_NET_IF_IS_WIRELESS(net_if))
   {
      if (_PACKET_IS_TODS(packet->frame_control))
      {
         memcpy(&dst_addr, &packet->addr_3, sizeof(al_net_addr_t));
         memcpy(&src_addr, &packet->addr_2, sizeof(al_net_addr_t));             /** check it */
         memcpy(&ta_addr, &packet->addr_2, sizeof(al_net_addr_t));              /** check it */
      }
      else if (_PACKET_IS_WDS(packet->frame_control))
      {
         memcpy(&dst_addr, &packet->addr_3, sizeof(al_net_addr_t));
         memcpy(&ta_addr, &packet->addr_2, sizeof(al_net_addr_t));              /** check it */
         memcpy(&src_addr, &packet->addr_4, sizeof(al_net_addr_t));
      }
      else
      {
		 (tx_rx_pkt_stats.packet_drop[19])++;
         return;
      }
   }
   else if (AL_NET_IF_IS_ETHERNET(net_if))
   {
      memcpy(&dst_addr, &packet->addr_1, sizeof(al_net_addr_t));
      memcpy(&src_addr, &packet->addr_2, sizeof(al_net_addr_t));        /** check it */
      memcpy(&ta_addr, &packet->addr_2, sizeof(al_net_addr_t));         /** check it */

      if (!_check_ethernet_ingress(AL_CONTEXT net_if, packet))
      {
		 (tx_rx_pkt_stats.packet_drop[20])++;
         return;
      }
   }

   if((packet->src_addr_sta_entry != NULL)) {
	   sta = packet->src_addr_sta_entry;
   } else {
   sta = access_point_get_sta(AL_CONTEXT & ta_addr);
   packet->src_addr_sta_entry = sta;
   }

   /**
    * For  pseudo-wm's we blindly add the transmitter to our
    * associated sta list
    */

   if (AL_NET_IF_IS_ETHERNET(net_if) && (sta == NULL))
   {
      unsigned short           status_code_out;
      int                      allow;
      access_point_vlan_info_t *vlan_info;
      int                      dot11e_enabled;
      int                      dot11e_catgory;

      vlan_info      = &default_vlan_info;
      dot11e_enabled = 0;
      dot11e_catgory = 0;
      allow          = -1;

      if (access_point_acl_lookup_entry(AL_CONTEXT & ta_addr, &vlan_info, &allow, &dot11e_enabled, &dot11e_catgory))
      {
         if (!allow)
         {
		    (tx_rx_pkt_stats.packet_drop[21])++;
            return;
         }
      }
      else
      {
         if (access_point_acl_get_disallow_other_stas(AL_CONTEXT_SINGLE))
         {
		    (tx_rx_pkt_stats.packet_drop[22])++;
            return;
         }
      }

      sta = access_point_add_sta(AL_CONTEXT net_if, &ta_addr);
      AL_ATOMIC_INC(sta->sta.ref_count);
      sta->sta.state     = ACCESS_POINT_STA_STATE_ASSOCIATED;
      sta->sta.vlan_info = vlan_info;

      if (allow != -1)
      {
         /**
          * Entry was found in the ACL list as allowed
          */
         sta->sta.auth_state |= ACCESS_POINT_STA_AUTH_STATE_ACL;
         if (vlan_info == &default_vlan_info)
         {
            sta->sta.dot11e_enabled  = dot11e_enabled;
            sta->sta.dot11e_category = dot11e_catgory;
            config_info = (access_point_netif_config_info_t *)net_if->config.ext_config_info;
            AL_ATOMIC_INC(config_info->sta_count);
         }
         else
         {
            access_point_incr_net_if_vlan_count(AL_CONTEXT vlan_info, net_if);
         }
      }
      else
      {
         /**
          * No Entry was found in the ACL list. The Disallow other stas was
          * also not set. Now if the packet was tagged, we look up the
          * packet's tag and assign that VLAN to the STA. Otherwise
          * default rules apply.
          */
         if (packet->dot1q_tag != AL_PACKET_VLAN_TAG_TYPE_UNTAGGED)
         {
            vlan_info = access_point_vlan_tag_get(AL_CONTEXT packet->dot1q_tag);
            if (vlan_info != NULL)
            {
               sta->sta.vlan_info = vlan_info;
               access_point_incr_net_if_vlan_count(AL_CONTEXT vlan_info, net_if);
            }
            else
            {
               config_info = (access_point_netif_config_info_t *)net_if->config.ext_config_info;
               AL_ATOMIC_INC(config_info->sta_count);
            }
         }
         else
         {
            config_info = (access_point_netif_config_info_t *)net_if->config.ext_config_info;
            AL_ATOMIC_INC(config_info->sta_count);
         }
      }


      access_point_dot11i_add_sta(AL_CONTEXT & ta_addr);

      /*inform mesh about 802.3 child */

      if (on_assoc_request_handler != NULL)
      {
         on_assoc_request_handler(AL_CONTEXT net_if, &ta_addr, NULL, (unsigned char)NULL, &status_code_out, NULL, (unsigned char *)NULL);
      }

   }
   else if ((sta == NULL) || (sta->sta.state != ACCESS_POINT_STA_STATE_ASSOCIATED))
   {
      al_802_11_operations_t *operations = net_if->get_extended_operations(net_if);
      /**
       * Drop the received packet & create & send a disassociation notification
       */
        al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "AP: Received CLASS 3 frame from "
		"un-associated STA "AL_NET_ADDR_STR " sending DEAUTH...", AL_NET_ADDR_TO_STR(&ta_addr));
//MeshapTag: How will hostapd know about this ?
      if (operations != NULL)
      {
          operations->send_deauth(net_if, net_if->config.hw_addr.bytes, &ta_addr);
      }
      else
      {
         al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"AP : ERROR : Not able to get extended operations "
            "unable to send deauth %s : %d\n", __func__,__LINE__);
      }

	   (tx_rx_pkt_stats.packet_drop[23])++;
      return;
   }

   /**
    * In case of VLAN, the config_info is retrieved based on the essid
    * used during association by the STA. This is only if the net_if
    * is a wireless one. For ethernet net_if's we always use the net_if
    */

   if (AL_NET_IF_IS_WIRELESS(net_if))
   {
      if ((sta->sta.vlan_info == NULL) || (sta->sta.vlan_info == &default_vlan_info))
      {
         config_info = (access_point_netif_config_info_t *)net_if->config.ext_config_info;
      }
      else
      {
         if (sta->sta.auth_state & ACCESS_POINT_STA_AUTH_STATE_INDIRECT_VLAN)
         {
            config_info = (access_point_netif_config_info_t *)net_if->config.ext_config_info;
         }
         else
         {
            config_info = &sta->sta.vlan_info->config;
         }
      }
   }
   else
   {
      config_info = (access_point_netif_config_info_t *)net_if->config.ext_config_info;
   }

   sta->sta.last_packet_received = al_get_tick_count(AL_CONTEXT_SINGLE);
   sta->sta.bytes_transmitted   += packet->data_length;

   if (packet->type == al_be16_to_cpu(AL_PACKET_TYPE_EAPOL))
   {
      if (on_eapol_rx_handler != NULL)
      {
         on_eapol_rx_handler(net_if, config_info, packet);
		 (tx_rx_pkt_stats.packet_drop[24])++;
         return;
      }
   }

   if (config_info->security_info.dot11.rsn.enabled &&
       config_info->security_info.dot11.rsn.auth_modes & AL_802_11_SECURITY_INFO_RSN_AUTH_1X &&
       sta->sta.auth_state & ACCESS_POINT_STA_AUTH_STATE_802_1X_INITIATED &&
       !(sta->sta.auth_state & ACCESS_POINT_STA_AUTH_STATE_802_1X_AUTHENTICATED))
   {
	  (tx_rx_pkt_stats.packet_drop[25])++;
      return;
   }

   if (AL_NET_IF_IS_WIRELESS(net_if))
   {
      if (sta->sta.transmit_bit_rate != packet->bit_rate)
      {
         sta->sta.transmit_bit_rate = packet->bit_rate;
      }
      sta->sta.rssi = (sta->sta.rssi + packet->rssi) / 2;
   }

   if (AL_NET_ADDR_EQUAL((&dst_addr), (&globals.config.ds_net_if_info.net_if->config.hw_addr)))
   {
      packet_for_us = 1;
   }
   else
   {
      for (i = 0; i < globals.config.wm_net_if_count; i++)
      {
         if (AL_NET_ADDR_EQUAL((&dst_addr), (&globals.config.wm_net_if_info[i].net_if->config.hw_addr)))
         {
            packet_for_us = 1;
            break;
         }
      }
   }
   if (packet_for_us)
   {
      al_add_packet_reference(AL_CONTEXT packet);
      al_send_packet_up_stack(AL_CONTEXT net_if, packet);
      return;
   }



   if ((AL_NET_ADDR_IS_BROADCAST(&dst_addr)) ||
       (AL_NET_ADDR_IS_MULTICAST(&dst_addr)))
   {
      _process_wm_broadcast_packet(AL_CONTEXT net_if, packet, &src_addr, &dst_addr, is_imcp);
   }
   else
   {
      _process_wm_unicast_packet(AL_CONTEXT net_if, packet, &src_addr, &dst_addr, is_imcp, service_type);
   }
}


static AL_INLINE void _process_mip_broadcast_packet(AL_CONTEXT_PARAM_DECL al_packet_t *packet)
{
   /**
    * Broadcast packets are sent UNTAGGED over DS and all WM's
    */

   _ds_broadcast_helper(AL_CONTEXT packet, &packet->addr_1, &packet->addr_2, AL_PACKET_VLAN_TAG_TYPE_UNTAGGED, 0);

   _untagged_wm_broadcast_helper(AL_CONTEXT NULL,
                                 packet,
                                 &packet->addr_1,
                                 &packet->addr_2,
                                 0,
                                 _ATLEAST_ONE,
                                 _ATLEAST_ONE);
}


static AL_INLINE void _process_mip_unicast_packet(AL_CONTEXT_PARAM_DECL al_packet_t *packet)
{
   access_point_sta_entry_t *sta;
   access_point_sta_entry_t *dest_sta;
   int                    decision;
   unsigned short         frame_control_out;
   al_net_addr_t          immediate_dest_addr_out;
   al_net_addr_t          parent_ap;
   al_net_if_t            *net_if;
   al_net_if_t            *net_if_out;
   al_802_11_operations_t *extended_operations;
   al_net_addr_t          dest_addr;
   al_net_addr_t          src_addr;
   int                    sta_buffering_enabled;

   AL_ASSERT("_process_mip_unicast_packet", packet != NULL);

   memcpy(&dest_addr, &packet->addr_1, sizeof(al_net_addr_t));
   memcpy(&src_addr, &packet->addr_2, sizeof(al_net_addr_t));

   sta_buffering_enabled = 0;

   net_if = globals.config.wm_net_if_info[0].net_if;
   if((packet->dest_addr_sta_entry != NULL)) {
	   sta = packet->dest_addr_sta_entry;
   } else {
   sta = access_point_get_sta(AL_CONTEXT & dest_addr);
   packet->dest_addr_sta_entry = sta;
   }

   if ((sta == NULL) ||
       (sta->sta.state != ACCESS_POINT_STA_STATE_ASSOCIATED))
   {
      if (on_data_reception_handler != NULL)
      {
         decision = on_data_reception_handler(AL_CONTEXT net_if,
                                              0,
                                              &src_addr,
                                              &src_addr,
                                              &dest_addr,
                                              &frame_control_out,
                                              &immediate_dest_addr_out,
                                              &net_if_out);
      }
      else
      {
         decision   = ACCESS_POINT_PACKET_DECISION_SEND_DS;
         net_if_out = globals.config.ds_net_if_info.net_if;
      }
   }
   else
   {
      frame_control_out = AL_802_11_FC_FROMDS;
      net_if_out        = sta->sta.net_if;
      decision          = ACCESS_POINT_PACKET_DECISION_SEND_WM;
   }

   if (decision == ACCESS_POINT_PACKET_DECISION_SEND_WM)
   {
      if (AL_NET_IF_IS_WIRELESS(net_if_out))
      {
         if (frame_control_out & AL_802_11_FC_FROMDS && frame_control_out & AL_802_11_FC_TODS)
         {
            memcpy(&packet->addr_1, &immediate_dest_addr_out, sizeof(al_net_addr_t));
            memcpy(&packet->addr_2, &net_if_out->config.hw_addr, sizeof(al_net_addr_t));
            memcpy(&packet->addr_3, &dest_addr, sizeof(al_net_addr_t));
            memcpy(&packet->addr_4, &src_addr, sizeof(al_net_addr_t));
			dest_sta = access_point_get_sta(AL_CONTEXT & immediate_dest_addr_out);
			if (dest_sta != NULL)
            {
               dest_sta->sta.last_packet_transmitted = al_get_tick_count(AL_CONTEXT_SINGLE);
               dest_sta->sta.bytes_received         += packet->data_length;
               if (dest_sta->sta.b_client)
               {
                  packet->flags |= AL_PACKET_FLAGS_USE_802_11B_RATES;
               }
               packet->flags &= ~AL_PACKET_FLAGS_ENCRYPTION;
               if (dest_sta->sta.auth_state & ACCESS_POINT_STA_AUTH_STATE_ENCRYPTION)
               {
                  packet->flags |= AL_PACKET_FLAGS_ENCRYPTION;
               }
               if (dest_sta->sta.auth_state & ACCESS_POINT_STA_AUTH_STATE_COMPRESSION)
               {
                  packet->flags |= AL_PACKET_FLAGS_COMPRESSION;
               }
               packet->key_index     = dest_sta->sta.key_index;
               sta_buffering_enabled = (dest_sta->sta.auth_state & ACCESS_POINT_STA_AUTH_STATE_BUFFERING);
			   access_point_release_sta(AL_CONTEXT dest_sta);
            }
         }
         else if (frame_control_out & AL_802_11_FC_FROMDS)
         {
            memcpy(&packet->addr_1, &dest_addr, sizeof(al_net_addr_t));
            memcpy(&packet->addr_2, &net_if_out->config.hw_addr, sizeof(al_net_addr_t));
            memcpy(&packet->addr_3, &src_addr, sizeof(al_net_addr_t));
            if (sta->sta.b_client)
            {
               packet->flags |= AL_PACKET_FLAGS_USE_802_11B_RATES;
            }
            packet->flags &= ~AL_PACKET_FLAGS_ENCRYPTION;
            if (sta->sta.auth_state & ACCESS_POINT_STA_AUTH_STATE_ENCRYPTION)
            {
               packet->flags |= AL_PACKET_FLAGS_ENCRYPTION;
            }
            if (sta->sta.auth_state & ACCESS_POINT_STA_AUTH_STATE_COMPRESSION)
            {
               packet->flags |= AL_PACKET_FLAGS_COMPRESSION;
            }
            packet->key_index     = sta->sta.key_index;
            sta_buffering_enabled = (sta->sta.auth_state & ACCESS_POINT_STA_AUTH_STATE_BUFFERING);
         }
         packet->frame_control = frame_control_out;
      }

      if (sta != NULL)
      {
         sta->sta.last_packet_transmitted = al_get_tick_count(AL_CONTEXT_SINGLE);
         sta->sta.bytes_received         += packet->data_length;
      }
   }
   else if (decision == ACCESS_POINT_PACKET_DECISION_SEND_DS)
   {
      if (AL_NET_IF_IS_WIRELESS(net_if_out))
      {
         extended_operations = net_if_out->get_extended_operations(net_if_out);
         extended_operations->get_bssid(net_if_out, &parent_ap);
         memcpy(&packet->addr_1, &parent_ap, sizeof(al_net_addr_t));
         memcpy(&packet->addr_2, &net_if_out->config.hw_addr, sizeof(al_net_addr_t));
         memcpy(&packet->addr_3, &dest_addr, sizeof(al_net_addr_t));
         packet->frame_control = AL_802_11_FC_TODS;
      }
   }

   al_add_packet_reference(AL_CONTEXT packet);

   _transmit_helper(AL_CONTEXT net_if_out, (sta_buffering_enabled != 0), packet);

   globals.access_point_traffic_stats.tx_packets++;
   globals.access_point_traffic_stats.tx_bytes = globals.access_point_traffic_stats.tx_bytes + packet->data_length;
}


static AL_INLINE void _process_mip_packet(AL_CONTEXT_PARAM_DECL al_packet_t *packet)
{
   al_net_addr_t dst_addr;

   memcpy(&dst_addr, &packet->addr_1, sizeof(al_net_addr_t));

   if ((AL_NET_ADDR_IS_BROADCAST(&dst_addr)) ||
       (AL_NET_ADDR_IS_MULTICAST(&dst_addr)))
   {
      _process_mip_broadcast_packet(AL_CONTEXT packet);
   }
   else
   {
      _process_mip_unicast_packet(AL_CONTEXT packet);
   }
}


void process_al_802_11_fc_stype_data_packet(AL_CONTEXT_PARAM_DECL al_net_if_t *net_if, al_packet_t *packet)
{
   unsigned char came_over_ds;
   int           ret;
   int           is_imcp;
   int           service_type;

   MESH_USAGE_PROFILING_INIT
#define _PACKET_IS_FOR_US    (came_over_ds == 2)
#define _PACKET_IS_FOR_US_BY_DEFAULT()    do { came_over_ds = 2; } while (0)
#define _PACKET_CAME_OVER_DS()            do { came_over_ds = 1; } while (0)
#define _PACKET_CAME_OVER_WM()            do { came_over_ds = 0; } while (0)
#define _PACKET_CAME_OVER_MIP()           do { came_over_ds = 3; } while (0)

   AL_ASSERT("process_al_802_11_fc_stype_data_packet", packet != NULL);

   /*
    * Check if the packet is coming over eth1 and the _FUNC_MODE is LFN and eth1 is configured as DS interface.
    * Is yes, then we shouldn't process any pakcets coming over eth1 on LFs,
    */

   if ((net_if != NULL) &&
       ((!strcmp(net_if->name, "eth1"))) &&
       ((func_mode == _FUNC_MODE_LFN) || (func_mode == _FUNC_MODE_FFN) || (func_mode == _FUNC_MODE_FFR)))
   {
      int i;
      mesh_conf_if_info_t *netif_conf;

      for (i = 0; i < mesh_config->if_count; i++)
      {
         netif_conf = &mesh_config->if_info[i];
         if (strcmp(netif_conf->name, "eth1") && strcmp(netif_conf->name, "eth0"))
         {
            continue;
         }
         if ((netif_conf->use_type == AL_CONF_IF_USE_TYPE_DS) && !strcmp(netif_conf->name, "eth1"))
         {
	        (tx_rx_pkt_stats.packet_drop[26])++;
            return;
         }
      }
   }

   if (packet->flags & AL_PACKET_FLAGS_REQUEUED)
   {
      access_point_sta_entry_t *sta_entry;
      int      sta_buffering_enabled;
      al_u64_t tick;

      sta_buffering_enabled = 0;
	  if((packet->dest_addr_sta_entry != NULL)) {
		  sta_entry = packet->dest_addr_sta_entry;
	  }else {
           sta_entry = access_point_get_sta(AL_CONTEXT & packet->addr_1);
		   packet->dest_addr_sta_entry = sta_entry;
	  }
      if (sta_entry != NULL) {
		  tick = al_get_tick_count(AL_CONTEXT_SINGLE);
		  sta_buffering_enabled = (sta_entry->sta.auth_state & ACCESS_POINT_STA_AUTH_STATE_BUFFERING);
		  if (al_timer_after(tick, sta_entry->sta.buffering_end_time) && sta_buffering_enabled)
		  {
			  sta_buffering_enabled      = 0;
			  sta_entry->sta.auth_state &= ~ACCESS_POINT_STA_AUTH_STATE_BUFFERING;
		  }
	  }

      al_add_packet_reference(AL_CONTEXT packet);

      _transmit_helper(AL_CONTEXT net_if, (sta_buffering_enabled != 0), packet);

      return;
   }

   is_imcp      = 0;
   ret          = -1;
   service_type = AL_CONF_IF_SERVICE_ALL;

   get_dot11e_priority(net_if, packet);

   if ((on_imcp_packet_handler != NULL) && (net_if != NULL))
   {
      if (_packet_is_imcp_udp_ip(AL_CONTEXT packet) == 0)
      {
         is_imcp = 1;

         ret = on_imcp_packet_handler(AL_CONTEXT net_if, packet);

         if (ret == ACCESS_POINT_PACKET_DECISION_DROP)
         {
            /* free packet */
	        (tx_rx_pkt_stats.packet_drop[27])++;
            return;
         }
         else if (ret < 0)
         {
            is_imcp = 0;
         }
      }
      else
      {
         globals.access_point_traffic_stats.rx_packets++;
         globals.access_point_traffic_stats.rx_bytes = globals.access_point_traffic_stats.rx_bytes + packet->data_length;
      }
   }


   _PACKET_IS_FOR_US_BY_DEFAULT();

   /**
    * First check if the packet came from DS or any of the WMs
    */

   if (net_if == NULL)
   {
      _PACKET_CAME_OVER_MIP();
   }
   else if (globals.config.ds_net_if_info.net_if == net_if)
   {
      /**
       * The packet came from DS
       */
      _PACKET_CAME_OVER_DS();
   }
   else
   {
      int i;
      for (i = 0; i < globals.config.wm_net_if_count; i++)
      {
         if (globals.config.wm_net_if_info[i].net_if == net_if)
         {
            /**
             * The packet came from WM (Wireless)
             */
            _PACKET_CAME_OVER_WM();
            service_type = globals.config.wm_net_if_info[i].service;
            break;
         }
      }
   }

   if (_PACKET_IS_FOR_US || (ret == ACCESS_POINT_PACKET_DECISION_SEND_UP_IMCP))
   {
      al_add_packet_reference(AL_CONTEXT packet);
      al_send_packet_up_stack(AL_CONTEXT net_if, packet);
      return;
   }

   switch (came_over_ds)
   {
   case 0:
	  MESH_USAGE_PROFILE_START(start)
      _process_wm_packet(AL_CONTEXT net_if, packet, is_imcp, service_type);
	  MESH_USAGE_PROFILE_END(profiling_info.process_wm_pkt_func_tprof,index,start,end)
      break;

   case 1:
	  MESH_USAGE_PROFILE_START(start)
      _process_ds_packet(AL_CONTEXT net_if, packet, is_imcp);
	  MESH_USAGE_PROFILE_END(profiling_info.process_ds_pkt_func_tprof,index,start,end)
      break;

   case 2:
      /** Must not happen */
	  (tx_rx_pkt_stats.packet_drop[28])++;
      break;

   case 3:
	  MESH_USAGE_PROFILE_START(start)
      _process_mip_packet(AL_CONTEXT packet);
	  MESH_USAGE_PROFILE_END(profiling_info.process_mip_pkt_func_tprof,index,start,end)
      break;
   }

#undef _PACKET_IS_FOR_US
#undef _PACKET_IS_FOR_US_BY_DEFAULT
#undef _PACKET_CAME_OVER_DS
#undef _PACKET_CAME_OVER_WM
#undef _PACKET_CAME_OVER_MIP
}
