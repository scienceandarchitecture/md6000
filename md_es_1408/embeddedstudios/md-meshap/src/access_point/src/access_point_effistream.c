/********************************************************************************
* MeshDynamics
* --------------
* File     : access_point_effistream.c
* Comments : Match criteria etc. for EffiStream
* Created  : 2/21/2007
* Author   : Prachiti Gaikwad
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  2  |11/8/2007 | Changes for queued retry                        |Prachiti|
* -----------------------------------------------------------------------------
* |  1  |6/6/2007  | Changes for action BitRate added                |Prachiti|
* -----------------------------------------------------------------------------
* |  0  |2/21/2007 | Created.                                        |Prachiti|
* -----------------------------------------------------------------------------
********************************************************************************/

#include <linux/string.h>
#include "al.h"
#include "al_802_11.h"
#include "access_point.h"
#include "access_point_effistream.h"

#include "al_impl_context.h"
#include "al_socket.h"
#include "al_endian.h"

#define _ERROR                        -1
#define _SUCCESS                      1

#define _RTP_VERSION_POSITION         0
#define _RTP_PAYLOAD_TYPE_POSITION    1

#define _RTP_GET_VERSION(p)         (p[_RTP_VERSION_POSITION] >> 6)
#define _RTP_GET_EXTENTION(p)       ((p[_RTP_VERSION_POSITION] >> 4) & 0x01)
#define _RTP_GET_CSRC_COUNT(p)      (p[_RTP_VERSION_POSITION] & 0x0F)
#define _RTP_GET_PAYLOAD_TYPE(p)    (p[_RTP_PAYLOAD_TYPE_POSITION] & 0x7F)

#define _RTP_HEADER_LENGTH                       12

#define _TCP_WORD_POSITION_SRC_PORT              0
#define _TCP_WORD_POSITION_DST_PORT              (_TCP_WORD_POSITION_SRC_PORT + 2)
#define _TCP_DWORD_POSITION_SEQ_NUMBER           (_TCP_WORD_POSITION_DST_PORT + 2)
#define _TCP_DWORD_POSITION_ACK_NUMBER           (_TCP_DWORD_POSITION_SEQ_NUMBER + 4)
#define _TCP_DWORD_POSITION_DATA_OFFSET_FIELD    (_TCP_DWORD_POSITION_ACK_NUMBER + 4)

/**
 * TCP Data Offset is 4 bits of Data Offset Field. 'Data Offset Field' is 16-bit word in the TCP
 * header which has :
 *
 *  15 14 13 12 11 10 09 08 07 06 05 04 03 02 01 00
 * +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
 * |DATA OFFSET|    RESERVED     |  CONTROL BITS   |
 * +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
 *
 */

#define  _TCP_GET_DATA_OFFSET_BITS(data_offset_field)    (((data_offset_field) >> 12))

#define  _IS_BETWEEN_RANGE(start, end, val)              ((val) >= (start) && (val) <= (end))

static AL_INLINE int _check_for_eth_type(AL_CONTEXT_PARAM_DECL al_packet_t *packet, al_conf_effistream_criteria_t *criteria)
{
   if (packet->type == criteria->match.value)
   {
      return _SUCCESS;
   }

   return _ERROR;
}


static AL_INLINE int _check_for_eth_dst(AL_CONTEXT_PARAM_DECL al_packet_t *packet, al_conf_effistream_criteria_t *criteria)
{
   al_net_addr_t *dst_addr;

   dst_addr = NULL;

   if (packet->flags & AL_PACKET_FLAGS_FRAME_CONTROL)
   {
      int from_ds;
      int to_ds;

      to_ds   = (packet->frame_control & AL_802_11_FC_TODS);
      from_ds = (packet->frame_control & AL_802_11_FC_FROMDS);

      if (to_ds && from_ds)
      {
         dst_addr = &packet->addr_3;
      }
      else if (to_ds)
      {
         dst_addr = &packet->addr_3;
      }
      else if (from_ds)
      {
         dst_addr = &packet->addr_1;
      }
   }
   else
   {
      /**
       * This is an ETHERNET packet, hence destination is in addr_1
       */
      dst_addr = &packet->addr_1;
   }

   if (dst_addr == NULL)
   {
      return _ERROR;
   }

   if (AL_NET_ADDR_IS_BROADCAST(&criteria->match.mac_address))
   {
      if (AL_NET_ADDR_IS_BROADCAST(dst_addr) ||
          AL_NET_ADDR_IS_MULTICAST(dst_addr))
      {
         return _SUCCESS;
      }
   }
   else if (AL_NET_ADDR_EQUAL(&criteria->match.mac_address, dst_addr))
   {
      return _SUCCESS;
   }

   return _ERROR;
}


static AL_INLINE int _check_for_eth_src(AL_CONTEXT_PARAM_DECL al_packet_t *packet, al_conf_effistream_criteria_t *criteria)
{
   if (AL_NET_ADDR_EQUAL(&criteria->match.mac_address, &packet->addr_2))
   {
      return _SUCCESS;
   }

   return _ERROR;
}


static AL_INLINE int _check_for_ip_tos(AL_CONTEXT_PARAM_DECL al_packet_t *packet, al_conf_effistream_criteria_t *criteria)
{
   unsigned char *p;

   p = packet->buffer + packet->position;

   if (p[IP_BYTE_POSITION_TOS] == criteria->match.value)
   {
      return _SUCCESS;
   }

   return _ERROR;
}


static AL_INLINE int _check_for_ip_diffsrv(AL_CONTEXT_PARAM_DECL al_packet_t *packet, al_conf_effistream_criteria_t *criteria)
{
   unsigned char *p;

   p = packet->buffer + packet->position;

   if (p[IP_BYTE_POSITION_TOS] == criteria->match.value)
   {
      return _SUCCESS;
   }

   return _ERROR;
}


#define _CHECK_IP_BYTE(ip_address, i)    ((criteria->match.ip_address[i] != 0xFF) && (ip_address[i] != criteria->match.ip_address[i]))

static AL_INLINE int _check_for_ip_src(AL_CONTEXT_PARAM_DECL al_packet_t *packet, al_conf_effistream_criteria_t *criteria)
{
   unsigned char *p;
   unsigned char ip_address[4];
   unsigned char matched;

   matched = 1;

   p = packet->buffer + packet->position;

   memcpy(ip_address, p + IP_DWORD_POSITION_SRCADDR, sizeof(ip_address));

   if (_CHECK_IP_BYTE(ip_address, 0) ||
       _CHECK_IP_BYTE(ip_address, 1) ||
       _CHECK_IP_BYTE(ip_address, 2) ||
       _CHECK_IP_BYTE(ip_address, 3))
   {
      matched = 0;
   }

   return (matched == 1) ? _SUCCESS : _ERROR;
}


static AL_INLINE int _check_for_ip_dst(AL_CONTEXT_PARAM_DECL al_packet_t *packet, al_conf_effistream_criteria_t *criteria)
{
   unsigned char *p;
   unsigned char ip_address[4];
   unsigned char matched;

   matched = 1;

   p = packet->buffer + packet->position;

   memcpy(ip_address, p + IP_DWORD_POSITION_DSTADDR, sizeof(ip_address));

   if (_CHECK_IP_BYTE(ip_address, 0) ||
       _CHECK_IP_BYTE(ip_address, 1) ||
       _CHECK_IP_BYTE(ip_address, 2) ||
       _CHECK_IP_BYTE(ip_address, 3))
   {
      matched = 0;
   }

   return (matched == 1) ? _SUCCESS : _ERROR;
}


#undef _CHECK_IP_BYTE

static AL_INLINE int _check_for_ip_proto(AL_CONTEXT_PARAM_DECL al_packet_t *packet, al_conf_effistream_criteria_t *criteria)
{
   unsigned char *p;

   p = packet->buffer + packet->position;

   if (p[IP_BYTE_POSITION_PROTOCOL] == criteria->match.value)
   {
      return _SUCCESS;
   }

   return _ERROR;
}


static AL_INLINE int _check_for_udp_src_port(AL_CONTEXT_PARAM_DECL al_packet_t *packet, al_conf_effistream_criteria_t *criteria)
{
   unsigned short ip_header_length;
   unsigned char  *p;
   unsigned short port;

   p = packet->buffer + packet->position;

   if (p[IP_BYTE_POSITION_PROTOCOL] != IP_PROTO_UDP)
   {
      return _ERROR;
   }

   ip_header_length  = IP_HEADER_GET_LENGTH(p[IP_BYTE_POSITION_VHL]);
   ip_header_length *= 4;               /* IP header length is the number of 32 bit words in the IP header */

   p += ip_header_length;

   memcpy(&port, p + UDP_WORD_POSITION_SRC_PORT, sizeof(port));

   port = al_be16_to_cpu(port);

   if (_IS_BETWEEN_RANGE(criteria->match.range.min_val, criteria->match.range.max_val, port))
   {
      return _SUCCESS;
   }

   return _ERROR;
}


static AL_INLINE int _check_for_udp_dst_port(AL_CONTEXT_PARAM_DECL al_packet_t *packet, al_conf_effistream_criteria_t *criteria)
{
   unsigned short ip_header_length;
   unsigned char  *p;
   unsigned short port;

   p = packet->buffer + packet->position;

   if (p[IP_BYTE_POSITION_PROTOCOL] != IP_PROTO_UDP)
   {
      return _ERROR;
   }

   ip_header_length  = IP_HEADER_GET_LENGTH(p[IP_BYTE_POSITION_VHL]);
   ip_header_length *= 4;               /* IP header length is the number of 32 bit words in the IP header */

   p += ip_header_length;

   memcpy(&port, p + UDP_WORD_POSITION_DST_PORT, sizeof(port));

   port = al_be16_to_cpu(port);

   if (_IS_BETWEEN_RANGE(criteria->match.range.min_val, criteria->match.range.max_val, port))
   {
      return _SUCCESS;
   }

   return _ERROR;
}


static AL_INLINE int _check_for_udp_length(AL_CONTEXT_PARAM_DECL al_packet_t *packet, al_conf_effistream_criteria_t *criteria)
{
   unsigned short ip_header_length;
   unsigned char  *p;
   unsigned short udp_length;

   p = packet->buffer + packet->position;

   if (p[IP_BYTE_POSITION_PROTOCOL] != IP_PROTO_UDP)
   {
      return _ERROR;
   }

   ip_header_length  = IP_HEADER_GET_LENGTH(p[IP_BYTE_POSITION_VHL]);
   ip_header_length *= 4;               /* IP header length is the number of 32 bit words in the IP header */

   p += ip_header_length;

   memcpy(&udp_length, p + UDP_WORD_POSITION_UDP_LENGTH, sizeof(udp_length));

   udp_length = al_be16_to_cpu(udp_length);

   if (_IS_BETWEEN_RANGE(criteria->match.range.min_val, criteria->match.range.max_val, udp_length))
   {
      return _SUCCESS;
   }

   return _ERROR;
}


static AL_INLINE int _check_for_tcp_src_port(AL_CONTEXT_PARAM_DECL al_packet_t *packet, al_conf_effistream_criteria_t *criteria)
{
   unsigned short ip_header_length;
   unsigned char  *p;
   unsigned short port;

   p = packet->buffer + packet->position;

   if (p[IP_BYTE_POSITION_PROTOCOL] != IP_PROTO_TCP)
   {
      return _ERROR;
   }

   ip_header_length  = IP_HEADER_GET_LENGTH(p[IP_BYTE_POSITION_VHL]);
   ip_header_length *= 4;               /* IP header length is the number of 32 bit words in the IP header */

   p += ip_header_length;

   memcpy(&port, p + _TCP_WORD_POSITION_SRC_PORT, sizeof(unsigned short));

   port = al_be16_to_cpu(port);

   if (_IS_BETWEEN_RANGE(criteria->match.range.min_val, criteria->match.range.max_val, port))
   {
      return _SUCCESS;
   }

   return _ERROR;
}


static AL_INLINE int _check_for_tcp_dst_port(AL_CONTEXT_PARAM_DECL al_packet_t *packet, al_conf_effistream_criteria_t *criteria)
{
   unsigned short ip_header_length;
   unsigned char  *p;
   unsigned short port;

   p = packet->buffer + packet->position;

   if (p[IP_BYTE_POSITION_PROTOCOL] != IP_PROTO_TCP)
   {
      return _ERROR;
   }

   ip_header_length  = IP_HEADER_GET_LENGTH(p[IP_BYTE_POSITION_VHL]);
   ip_header_length *= 4;               /* IP header length is the number of 32 bit words in the IP header */

   p += ip_header_length;

   memcpy(&port, p + _TCP_WORD_POSITION_DST_PORT, sizeof(unsigned short));

   port = al_be16_to_cpu(port);

   if (_IS_BETWEEN_RANGE(criteria->match.range.min_val, criteria->match.range.max_val, port))
   {
      return _SUCCESS;
   }

   return _ERROR;
}


static AL_INLINE int _check_for_tcp_length(AL_CONTEXT_PARAM_DECL al_packet_t *packet, al_conf_effistream_criteria_t *criteria)
{
   unsigned char  *p;
   unsigned short data_offset_field;
   unsigned short ip_header_length;
   unsigned short tcp_header_length;
   unsigned short ip_total_length;
   unsigned short tcp_data_length;

   p = packet->buffer + packet->position;

   if (p[IP_BYTE_POSITION_PROTOCOL] != IP_PROTO_TCP)
   {
      return _ERROR;
   }

   memcpy(&ip_total_length, &p[IP_WORD_POSITION_TOTLEN], sizeof(short));
   ip_total_length = al_be16_to_cpu(ip_total_length);

   ip_header_length  = IP_HEADER_GET_LENGTH(p[IP_BYTE_POSITION_VHL]);
   ip_header_length *= 4;               /* IP header length is the number of 32 bit words in the IP header */

   p = p + ip_header_length;

   memcpy(&data_offset_field, p + _TCP_DWORD_POSITION_DATA_OFFSET_FIELD, sizeof(short));

   data_offset_field = al_be16_to_cpu(data_offset_field);

   tcp_header_length  = _TCP_GET_DATA_OFFSET_BITS(data_offset_field);
   tcp_header_length *= 4;              /* Data offset is the number of 32 bit words (i.e 4 bytes) in the TCP header (RFC 793) */

   tcp_data_length = ip_total_length - ip_header_length - tcp_header_length;

   if (_IS_BETWEEN_RANGE(criteria->match.range.min_val, criteria->match.range.max_val, tcp_data_length))
   {
      return _SUCCESS;
   }

   return _ERROR;
}


static AL_INLINE int _check_for_rtp_version(AL_CONTEXT_PARAM_DECL al_packet_t *packet, al_conf_effistream_criteria_t *criteria)
{
   unsigned short ip_header_length;
   unsigned char  *p;
   unsigned char  rtp_version;
   unsigned short temp;

   rtp_version = 0;

   p = packet->buffer + packet->position;

   if (p[IP_BYTE_POSITION_PROTOCOL] != IP_PROTO_UDP)
   {
      return _ERROR;
   }

   ip_header_length  = IP_HEADER_GET_LENGTH(p[IP_BYTE_POSITION_VHL]);
   ip_header_length *= 4;               /* IP header length is the number of 32 bit words in the IP header */

   p += ip_header_length;
   p += UDP_HEADER_LENGTH;

   memcpy(&temp, p, 2);

   rtp_version = _RTP_GET_VERSION(p);

   if (criteria->match.value == rtp_version)
   {
      return _SUCCESS;
   }

   return _ERROR;
}


static AL_INLINE int _check_for_rtp_payload(AL_CONTEXT_PARAM_DECL al_packet_t *packet, al_conf_effistream_criteria_t *criteria)
{
   unsigned short ip_header_length;
   unsigned char  *p;
   unsigned char  rtp_payload;

   p = packet->buffer + packet->position;

   if (p[IP_BYTE_POSITION_PROTOCOL] != IP_PROTO_UDP)
   {
      return _ERROR;
   }

   ip_header_length  = IP_HEADER_GET_LENGTH(p[IP_BYTE_POSITION_VHL]);
   ip_header_length *= 4;               /* IP header length is the number of 32 bit words in the IP header */

   p += ip_header_length;
   p += UDP_HEADER_LENGTH;

   rtp_payload = _RTP_GET_PAYLOAD_TYPE(p);

   if (criteria->match.value == rtp_payload)
   {
      return _SUCCESS;
   }

   return _ERROR;
}


static AL_INLINE int _check_for_rtp_length(AL_CONTEXT_PARAM_DECL al_packet_t *packet, al_conf_effistream_criteria_t *criteria)
{
   unsigned short ip_header_length;
   unsigned char  rtp_cc;
   unsigned short temp;
   unsigned char  *p;
   unsigned int   length;

   length = 0;

   p = packet->buffer + packet->position;

   if (p[IP_BYTE_POSITION_PROTOCOL] != IP_PROTO_UDP)
   {
      return _ERROR;
   }

   ip_header_length  = IP_HEADER_GET_LENGTH(p[IP_BYTE_POSITION_VHL]);
   ip_header_length *= 4;               /* IP header length is the number of 32 bit words in the IP header */

   p += ip_header_length;


   memcpy(&temp, &p[UDP_WORD_POSITION_UDP_LENGTH], 2);
   length  = al_be16_to_cpu(temp);
   length -= UDP_HEADER_LENGTH;

   p += UDP_HEADER_LENGTH;

   rtp_cc = _RTP_GET_CSRC_COUNT(p);

   length -= (_RTP_HEADER_LENGTH + rtp_cc * 4);

   if (_RTP_GET_EXTENTION(p))
   {
      int ext_length;

      p += (_RTP_HEADER_LENGTH + rtp_cc * 4);

      memcpy(&temp, &p[2], 2);
      ext_length = al_be16_to_cpu(temp);

      length -= (ext_length + 4);
   }

   if (_IS_BETWEEN_RANGE(
          criteria->match.range.min_val,
          criteria->match.range.max_val, length) &&
       (length > 0))
   {
      return _SUCCESS;
   }

   return _ERROR;
}


static AL_INLINE int _check_criteria(AL_CONTEXT_PARAM_DECL al_packet_t *packet, al_conf_effistream_criteria_t *criteria)
{
   int ret;

   ret = _ERROR;

   switch (criteria->match_id)
   {
   case AL_CONF_EFFISTREAM_MATCH_ID_ETH_TYPE:
      ret = _check_for_eth_type(packet, criteria);
      break;

   case AL_CONF_EFFISTREAM_MATCH_ID_ETH_DST:
      ret = _check_for_eth_dst(packet, criteria);
      break;

   case AL_CONF_EFFISTREAM_MATCH_ID_ETH_SRC:
      ret = _check_for_eth_src(packet, criteria);
      break;

   case AL_CONF_EFFISTREAM_MATCH_ID_IP_TOS:
      ret = _check_for_ip_tos(packet, criteria);
      break;

   case AL_CONF_EFFISTREAM_MATCH_ID_IP_DIFFSRV:
      ret = _check_for_ip_diffsrv(packet, criteria);
      break;

   case AL_CONF_EFFISTREAM_MATCH_ID_IP_SRC:
      ret = _check_for_ip_src(packet, criteria);
      break;

   case AL_CONF_EFFISTREAM_MATCH_ID_IP_DST:
      ret = _check_for_ip_dst(packet, criteria);
      break;

   case AL_CONF_EFFISTREAM_MATCH_ID_IP_PROTO:
      ret = _check_for_ip_proto(packet, criteria);
      break;

   case AL_CONF_EFFISTREAM_MATCH_ID_UDP_SRC_PORT:
      ret = _check_for_udp_src_port(packet, criteria);
      break;

   case AL_CONF_EFFISTREAM_MATCH_ID_UDP_DST_PORT:
      ret = _check_for_udp_dst_port(packet, criteria);
      break;

   case AL_CONF_EFFISTREAM_MATCH_ID_UDP_LENGTH:
      ret = _check_for_udp_length(packet, criteria);
      break;

   case AL_CONF_EFFISTREAM_MATCH_ID_TCP_SRC_PORT:
      ret = _check_for_tcp_src_port(packet, criteria);
      break;

   case AL_CONF_EFFISTREAM_MATCH_ID_TCP_DST_PORT:
      ret = _check_for_tcp_dst_port(packet, criteria);
      break;

   case AL_CONF_EFFISTREAM_MATCH_ID_TCP_LENGTH:
      ret = _check_for_tcp_length(packet, criteria);
      break;

   case AL_CONF_EFFISTREAM_MATCH_ID_RTP_VERSION:
      ret = _check_for_rtp_version(packet, criteria);
      break;

   case AL_CONF_EFFISTREAM_MATCH_ID_RTP_PAYLOAD:
      ret = _check_for_rtp_payload(packet, criteria);
      break;

   case AL_CONF_EFFISTREAM_MATCH_ID_RTP_LENGTH:
      ret = _check_for_rtp_length(packet, criteria);
      break;
   }

   return ret;
}


/**
 * Caller assumes dot11e category is applied by this func
 */
int match_and_apply_criteria(AL_CONTEXT_PARAM_DECL al_packet_t *packet, al_conf_effistream_criteria_t *criteria)
{
   int ret;
   int criteria_matched;

   ret = MATCH_ACTION_NONE;
   criteria_matched = _check_criteria(packet, criteria);

   if (criteria_matched == _SUCCESS)
   {
      if (criteria->first_child != NULL)
      {
         ret = match_and_apply_criteria(packet, criteria->first_child);
      }
      else
      {
         /* Action */

         if (criteria->action.dot11e_category != 0xFF)
         {
            packet->flags          |= AL_PACKET_FLAGS_DOT11E_ENABLED;
            packet->dot11e_category = criteria->action.dot11e_category;
         }

         if (criteria->action.bit_rate != 0)                 /* This is actual bit rate in mbps */

         {
            /**
             * We set the actual bit rate i.e. in mbps in packet's bit_rate field.
             * Later,in transmit_helper,we get the appropriate bit_rate_index from
             * the net_if_config's array,and set it accordingly.We cannot set the
             * index beforehand because we need all the net_if's config in broadcast
             */

            packet->flags   |= AL_PACKET_FLAGS_FIXED_RATE;
            packet->bit_rate = criteria->action.bit_rate;
         }

         if (criteria->action.queued_retry)
         {
            packet->flags |= AL_PACKET_FLAGS_QUEUED_RETRY;
         }

         if (criteria->action.drop)
         {
            return MATCH_ACTION_DROP;
         }
         else if (criteria->action.no_ack)
         {
            return MATCH_ACTION_NO_ACK;
         }
         else
         {
            return MATCH_ACTION_NONE;
         }
      }
   }
   else
   {
      if (criteria->next_sibling != NULL)
      {
         ret = match_and_apply_criteria(packet, criteria->next_sibling);
      }
   }

   return ret;
}


int match_and_set_dot1p_priority(AL_CONTEXT_PARAM_DECL al_packet_t *packet, int priority, al_conf_effistream_criteria_t *criteria)
{
   int criteria_matched;

   criteria_matched = _check_criteria(packet, criteria);

   if (criteria_matched == _SUCCESS)
   {
      if (criteria->first_child != NULL)
      {
         priority = match_and_set_dot1p_priority(packet, priority, criteria->first_child);
      }
      else
      {
         if (criteria->action.dot11e_category != 0xFF)
         {
            /*
             *
             */
            priority = ((criteria->action.dot11e_category) * 2);
         }
      }
   }
   else
   {
      if (criteria->next_sibling != NULL)
      {
         priority = match_and_set_dot1p_priority(packet, priority, criteria->next_sibling);
      }
   }

   return priority;
}
