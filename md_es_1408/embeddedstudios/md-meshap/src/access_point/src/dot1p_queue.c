/********************************************************************************
* MeshDynamics
* --------------
* File     : dot1p_queue.c
* Comments : dot1p Queue
* Created  : 11/14/2005
* Author   : Bindu Parakala
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  7  |11/8/2007 | Changes for effistream based priority           | Sriram |
* -----------------------------------------------------------------------------
* |  6  |8/16/2007 | DOT1P stat printing improved                    | Sriram |
* -----------------------------------------------------------------------------
* |  5  |3/15/2007 | Pool size taken from Makefile                   | Sriram |
* -----------------------------------------------------------------------------
* |  4  |5/31/2006 | dot1p_queue_print_hstats added                  | Sriram |
* -----------------------------------------------------------------------------
* |  3  |03/08/2006| Misc changes                                    | Sriram |
* -----------------------------------------------------------------------------
* |  2  |03/03/2006| Changed Rules for .11e & .1p packet processing  | Bindu  |
* -----------------------------------------------------------------------------
* |  1  |02/15/2006| Changes for dot11e                              | Bindu  |
* -----------------------------------------------------------------------------
* |  0  |11/14/2005| Created                                         | Bindu  |
* -----------------------------------------------------------------------------
********************************************************************************/

#include "al.h"
#include "al_impl_context.h"
#include "access_point_queue.h"
#include <linux/seq_file.h>
#include "dot1p_queue.h"
#include "dot1p-impl.h"
#include "access_point.h"
#include "access_point_vlan.h"
#include "access_point_sta.h"
#include "al_net_addr.h"
#include "access_point_globals.h"
#include "access_point_acl.h"
#include "meshap_hproc_format.h"
#include "access_point_effistream.h"

#include <linux/seq_file.h>

#include <linux/spinlock.h>
atomic_t current_PQindx;
#define _DOT1P_QUEUE_PACKET_POOL_SIZE       2048

#define _DOT1P_QUEUE_COUNT                  NUMBER_OF_HQUEUES
#define _DOT1P_QUEUE_POOL_ITEM_TYPE_POOL    0
#define _DOT1P_QUEUE_POOL_ITEM_TYPE_HEAP    1

dot1p_queue_entry_t *dot1p_temp_queue_head = NULL;
dot1p_queue_entry_t *dot1p_temp_queue_tail = NULL;

static int          total_pool;
static int          total_heap;
static int          requeued;
static int          total_requeued;
struct queue_and_packet_stats q_packet_stats;

static AL_INLINE int _get_addresses(al_net_if_t *net_if, al_packet_t *packet, al_net_addr_t *dst_addr, al_net_addr_t *src_addr);
static AL_INLINE int _get_non_empty_highest_priority_queue(dot1p_queue_t **queue);
static AL_INLINE int _get_HQindx(dot1p_queue_t **queue, int indx);
static AL_INLINE unsigned char _get_priority_for_incoming_pkt(al_net_if_t *al_net_if, al_packet_t *packet);

extern int watchdog_id_ap;

static AL_INLINE void _dot1p_add_packet_helper(AL_CONTEXT_PARAM_DECL int indx,
                                               dot1p_queue_t             **queue,
                                               al_net_if_t               *al_net_if,
                                               al_packet_t               *packet)
{
   dot1p_queue_entry_t *entry;
   unsigned long flags;
   static int count;
   MESH_USAGE_PROFILING_INIT
   if (q_packet_stats.dot1p_queue_current_packet_count == _DOT1P_QUEUE_PACKET_POOL_SIZE) {
       count++;
   }else {
       count = 0;
   }

   if (count < 10) {
       al_update_watchdog_ap_thread(AL_CONTEXT watchdog_id_ap);
   }

   entry = (dot1p_queue_entry_t *)&packet->dot1p_entry;
#if MULTIPLE_AP_THREAD
   if (packet) {
	   packet->status = PACKET_UNASSIGNED;
	   packet->entry = entry;
   }
#endif
   entry->net_if = al_net_if;
   entry->packet = packet;
   entry->next = NULL;
   
   MESH_USAGE_PROFILE_START(start)
   al_spin_lock(queue[indx]->dot1p_Qsplock, flags);
   MESH_USAGE_PROFILE_END(profiling_info.dot1p_Qsplock_1,index,start,end)
   if (queue[indx]->dot1p_queue_head == NULL){
	   queue[indx]->dot1p_queue_head = entry;
	   queue[indx]->dot1p_queue_tail = entry;
   }else{
	   queue[indx]->dot1p_queue_tail->next = entry;
	   queue[indx]->dot1p_queue_tail       = entry;
   }
   ++queue[indx]->current_level;
   ++queue[indx]->total_packets_added;
   q_packet_stats.dot1p_queue_total_packet_added_count++;
   q_packet_stats.dot1p_queue_current_packet_count++;

   if(q_packet_stats.dot1p_queue_current_packet_count > q_packet_stats.dot1p_queue_current_packet_max_count) {
	   q_packet_stats.dot1p_queue_current_packet_max_count = q_packet_stats.dot1p_queue_current_packet_count;
   }

   MESH_USAGE_PROFILE_START(start)
   al_spin_unlock(queue[indx]->dot1p_Qsplock, flags);
   MESH_USAGE_PROFILE_END(profiling_info.dot1p_Qspunlock_1,index,start,end)

   if (queue[indx]->current_level > queue[indx]->max_level)
   {
      queue[indx]->max_level = queue[indx]->current_level;
   }
}


dot1p_queue_t **dot1p_queue_initialize(AL_CONTEXT_PARAM_DECL_SINGLE)
{
   dot1p_queue_t **queue;
   dot1p_queue_t *temp;
   int           i;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Enter %s<%d>\n",__func__,__LINE__);
   queue = (dot1p_queue_t **)dot1p_heap_alloc(sizeof(dot1p_queue_t) * _DOT1P_QUEUE_COUNT);
   memset(queue, 0, sizeof(dot1p_queue_t) * _DOT1P_QUEUE_COUNT);

   for (i = 0; i < _DOT1P_QUEUE_COUNT; i++)
   {
      temp = (dot1p_queue_t *)dot1p_heap_alloc(sizeof(dot1p_queue_t));
      memset(temp, 0, sizeof(dot1p_queue_t));
      temp->dot1p_queue_head       = NULL;
      temp->dot1p_queue_tail       = NULL;
#if MULTIPLE_AP_THREAD
      temp->dot1p_queue_fhead      = NULL;
      temp->dot1p_queue_ftail      = NULL;
#endif
      temp->dot1p_Qsplock     = AL_SPIN_LOCK_UNLOCKED(temp->dot1p_Qsplock);
      temp->dot1pQ_imcpsplock     = AL_SPIN_LOCK_UNLOCKED(temp->dot1pQ_imcpsplock);
      temp->queue_semaphore_handle = dot1p_create_lock();
      queue[i] = temp;
   }

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Exit %s<%d>\n",__func__,__LINE__);

   return queue;
}


int dot1p_queue_uninitialize(AL_CONTEXT_PARAM_DECL access_point_queue_t *ap_queue)
{
   dot1p_queue_t **queue;
   dot1p_pool_t  *pool_item;
   int           i;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Enter %s<%d>\n",__func__,__LINE__);
#if 0
   int           j;
   for(j = 0;j < NUMBER_OF_PQUEUES; j++) {
	   queue = (dot1p_queue_t **)ap_queue->queue_data[j];
	   for (i = 0; i < _DOT1P_QUEUE_COUNT; i++)
	   {
		   for (pool_item = (dot1p_pool_t *)queue[i]->dot1p_queue_head; pool_item != NULL; pool_item = pool_item->next_pool)
		   {
			   if (pool_item->pool_item_type == _DOT1P_QUEUE_POOL_ITEM_TYPE_POOL)
			   {
				   pool_item->next_pool = available_head;
				   available_head       = pool_item;
			   }
		   }
		   dot1p_destroy_lock(queue[i]->queue_semaphore_handle);
		   dot1p_heap_free(queue[i]);
	   }
	   dot1p_heap_free(queue);
   }
#endif

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP:FLOW: Exit %s<%d>\n",__func__,__LINE__);

   return 0;
}

int get_hash_from_2mac_addr(al_net_addr_t *addr1, al_net_addr_t *addr2)
{
	int hash;
	hash = addr1->bytes[0]| 1
		+ addr1->bytes[1] | 2
		+ addr1->bytes[2] | 4
		+ addr1->bytes[3] | 8
		+ addr1->bytes[4] | 16
		+ addr1->bytes[5] | 32;
	hash += addr2->bytes[0] | 1
		+ addr2->bytes[1] | 2
		+ addr2->bytes[2] | 4
		+ addr2->bytes[3] | 8
		+ addr2->bytes[4] | 16
		+ addr2->bytes[5] | 32;
	return hash;
}

int get_queue_index(al_packet_t *packet)
{
	int index, hash;
	hash = get_hash_from_2mac_addr(&packet->addr_1, &packet->addr_2);
	index = hash % NUMBER_OF_HQUEUES;
	return index;

}

int dot1p_queue_add_packet(AL_CONTEXT_PARAM_DECL access_point_queue_t *ap_queue, al_net_if_t *al_net_if, al_packet_t *packet)
{
   dot1p_queue_t **queue;


	/* Bz-219: QoS ACL List for MD6000
	Solution: update qos_control field based on dot11e_category access category */
	if(!packet->qos_control)
		packet->qos_control = 2 * packet->dot11e_category;
 

   queue    = (dot1p_queue_t **)ap_queue->queue_data;
   _dot1p_add_packet_helper(AL_CONTEXT 0, queue, al_net_if, packet);

   return 0;
}


static AL_INLINE int _get_addresses(al_net_if_t *net_if, al_packet_t *packet, al_net_addr_t *dst_addr, al_net_addr_t *src_addr)
{
   int type;

   if (AL_NET_IF_IS_WIRELESS(net_if))
   {
      type = AL_802_11_FC_GET_TYPE(packet->frame_control);

      if (type == AL_802_11_FC_TYPE_MGMT)
      {
         return 0;
      }

      if (_PACKET_IS_TODS(packet->frame_control))
      {
         memcpy(dst_addr, &packet->addr_3, sizeof(al_net_addr_t));
         memcpy(src_addr, &packet->addr_2, sizeof(al_net_addr_t));
      }
      else if (_PACKET_IS_WDS(packet->frame_control))
      {
         memcpy(dst_addr, &packet->addr_3, sizeof(al_net_addr_t));
         memcpy(src_addr, &packet->addr_4, sizeof(al_net_addr_t));
      }
      else if (_PACKET_IS_FROMDS(packet->frame_control))
      {
         memcpy(dst_addr, &packet->addr_1, sizeof(al_net_addr_t));
         memcpy(src_addr, &packet->addr_3, sizeof(al_net_addr_t));
      }
      else
      {
         /* Ad-hoc packet , not handled by us */
         return -1;
      }
   }
   else if (AL_NET_IF_IS_ETHERNET(net_if))
   {
      memcpy(dst_addr, &packet->addr_1, sizeof(al_net_addr_t));
      memcpy(src_addr, &packet->addr_2, sizeof(al_net_addr_t));
   }
   else
   {
      return -1;
   }

   return 1;
}


static AL_INLINE unsigned char _get_priority_for_incoming_pkt(al_net_if_t *al_net_if, al_packet_t *packet)
{
   /**
    *	====================================================================================================
    *								Rules for 802.1p & 802.11e Packets
    *	====================================================================================================
    *
    *	1. FROM_DS
    *
    *		1.1	To IMMEDIATE_CHILD
    *			802.1p	- VLAN_INFO of Child's Entry.
    *			802.11e	- VLAN_INFO of Child's Entry. (if 'default' VLAN then get corresponding IF Entry)
    *
    *		1.2	To RELAY_CHILD
    *
    *			1.2.1 TAGGED PACKET
    *				802.1p	- VLAN_INFO of TAG.
    *				802.11e	- VLAN_INFO of TAG. ()
    *
    *			1.2.2 UNTAGGED PACKET
    *
    *				a. CATEGORY IN WDS HEADER (i.e packet 802.11e flag set & value present)
    *					802.1p	- 0.
    *					802.11e	- CATEGORY of WDS HEADER.
    *				b. NO CATEGORY IN WDS HEADER
    *					802.1p	- 0.
    *					802.11e	- No Category.(i.e 802.11e Flag not set so Regular 802.11 Tx)
    *
    *	2. FROM_WM
    *
    *		2.1 From IMMEDIATE_CHILD
    *			802.1p	- VLAN_INFO of Child's Entry.
    *			802.11e	- VLAN_INFO of Child's Entry.
    *
    *		2.2 From RELAY_CHILD
    *
    *			2.2.1 TAGGED_PACKET
    *				802.1p	- VLAN_INFO of TAG.
    *				802.11e	- VLAN_INFO of TAG.
    *
    *			2.2.2 UNTAGGED_PACKET
    *
    *				a. CATEGORY IN WDS HEADER (i.e packet 802.11e flag set & category present)
    *					802.1p	- 0.
    *					802.11e	- CATEGORY of WDS HEADER.
    *				b. NO CATEGORY IN WDS HEADER
    *					802.1p	- 0.
    *					802.11e	- No Category.(i.e 802.11eFlag not set so Regular 802.11 Tx)
    *
    *	====================================================================================================
    */

   unsigned char            priority;
   access_point_sta_entry_t *dst_sta;
   access_point_sta_entry_t *src_sta;
   al_net_addr_t            dst_addr;
   al_net_addr_t            src_addr;
   int ret;
   access_point_vlan_info_t *vlan_info;
   int in_softirq;

   priority = 0;

   if (al_net_if == NULL)
   {
      priority = 0;
      return priority;
   }

   ret = _get_addresses(al_net_if, packet, &dst_addr, &src_addr);

   if ((ret == -1) || (ret == 0))
   {
      priority = 0;
      return priority;
   }

   in_softirq = in_softirq();
   if (globals.config.ds_net_if_info.net_if == al_net_if)       /* 1 */

   {
      dst_sta = access_point_get_sta_ex(AL_CONTEXT & dst_addr, in_softirq);

      if (dst_sta != NULL)            /* 1.1 */

      {
         if (dst_sta->sta.vlan_info == NULL)
         {
            //priority = 0; //QOS_SUPPORT: read 11e_category from qos_control
	    packet->dot11e_category = FILL_DOT11E_CATEGORY_FROM_QOS_CONTROL(packet->qos_control);
		//printk(KERN_EMERG "QOS_SUPPORT: %s-%d packet->dot11e_category=%d \n", __func__, __LINE__, packet->dot11e_category);
	    priority = packet->dot11e_category;
            goto _rel1;
         }

         priority = dst_sta->sta.vlan_info->dot1p_priority;

         if (dst_sta->sta.vlan_info == &default_vlan_info)
         {
            if (dst_sta->sta.auth_state & ACCESS_POINT_STA_AUTH_STATE_ACL)
            {
               if (dst_sta->sta.dot11e_enabled)
               {
                  packet->flags |= AL_PACKET_FLAGS_DOT11E_ENABLED;
               }
               packet->dot11e_category = dst_sta->sta.dot11e_category;
            }
         }
         else
         {
            if (dst_sta->sta.vlan_info->dot11e_enabled)
            {
               packet->flags |= AL_PACKET_FLAGS_DOT11E_ENABLED;
            }

            packet->dot11e_category = dst_sta->sta.vlan_info->dot11e_category;
         }

_rel1:

         access_point_release_sta_ex2(AL_CONTEXT dst_sta, 0, 0, in_softirq);
      }
      else               /* 1.2 */

      {
         if (packet->dot1q_tag != AL_PACKET_VLAN_TAG_TYPE_UNTAGGED)                 /* 1.2.1 */

         {
            vlan_info = access_point_vlan_tag_get(AL_CONTEXT packet->dot1q_tag);

            if (vlan_info != NULL)
            {
               if (vlan_info->dot11e_enabled)
               {
                  packet->flags |= AL_PACKET_FLAGS_DOT11E_ENABLED;
               }
               packet->dot11e_category = vlan_info->dot11e_category;
               priority = vlan_info->dot1p_priority;
            }
            else
            {
                //priority = 0; //QOS_SUPPORT: read 11e_category from qos_control
		packet->dot11e_category = FILL_DOT11E_CATEGORY_FROM_QOS_CONTROL(packet->qos_control);
		//printk(KERN_EMERG "QOS_SUPPORT: %s-%d packet->dot11e_category=%d \n", __func__, __LINE__, packet->dot11e_category);
		priority = packet->dot11e_category;
            }
         }
         else                    /* 1.2.2 */

         {
            priority = 0;

            if (AL_NET_IF_IS_ETHERNET(al_net_if))
            {
               int allow;
               int dot11e_enabled;
               int dot11e_category;

               dot11e_enabled  = 0;
               dot11e_category = 0;

               if (access_point_acl_lookup_entry(AL_CONTEXT & dst_addr, &vlan_info, &allow, &dot11e_enabled, &dot11e_category))
               {
                  if (dot11e_enabled)
                  {
                     packet->flags |= AL_PACKET_FLAGS_DOT11E_ENABLED;
                  }
                  packet->dot11e_category = dot11e_category;
               }
            }

            /** 1.2.2.1 and 1.2.2.2 are implicit in the wireless DS case */
         }
      }

      if (AL_NET_IF_IS_ETHERNET(al_net_if))
      {
         if (EFFISTREAM_ENABLED)
         {
            priority = match_and_set_dot1p_priority(AL_CONTEXT packet, priority, globals.config.criteria_root->first_child);
         }
      }
      else
      {
         if (packet->flags & AL_PACKET_FLAGS_DOT11E_ENABLED)
         {
            priority = packet->dot11e_category * 2;
         }
      }
   }
   else          /* 2 */


   {
      src_sta = access_point_get_sta_ex(AL_CONTEXT & src_addr, in_softirq);

      if (src_sta != NULL)            /* 2.1 */

      {
         if (src_sta->sta.vlan_info == NULL)
         {
            //priority = 0; //QOS_SUPPORT: read 11e_category from qos_control
	    packet->dot11e_category = FILL_DOT11E_CATEGORY_FROM_QOS_CONTROL(packet->qos_control);
	    //printk(KERN_EMERG "QOS_SUPPORT: %s-%d packet->dot11e_category=%d \n", __func__, __LINE__, packet->dot11e_category);
	    priority = packet->dot11e_category;
            goto _rel2;
         }

         priority = src_sta->sta.vlan_info->dot1p_priority;

         if (src_sta->sta.vlan_info == &default_vlan_info)
         {
            if (src_sta->sta.auth_state & ACCESS_POINT_STA_AUTH_STATE_ACL)
            {
               if (src_sta->sta.dot11e_enabled)
               {
                  packet->flags |= AL_PACKET_FLAGS_DOT11E_ENABLED;
               }
               packet->dot11e_category = src_sta->sta.dot11e_category;
            }
         }
         else
         {
            if (src_sta->sta.vlan_info->dot11e_enabled)
            {
               packet->flags |= AL_PACKET_FLAGS_DOT11E_ENABLED;
            }

            packet->dot11e_category = src_sta->sta.vlan_info->dot11e_category;
         }

         if (EFFISTREAM_ENABLED)
         {
            priority = match_and_set_dot1p_priority(AL_CONTEXT packet, priority, globals.config.criteria_root->first_child);
         }

_rel2:

         access_point_release_sta_ex2(AL_CONTEXT src_sta, 0, 0, in_softirq);
      }
      else               /* 2.2 */

      {
         if (packet->dot1q_tag != AL_PACKET_VLAN_TAG_TYPE_UNTAGGED)                 /* 2.2.1 */

         {
            vlan_info = access_point_vlan_tag_get(AL_CONTEXT packet->dot1q_tag);

            if (vlan_info != NULL)
            {
               if (vlan_info->dot11e_enabled)
               {
                  packet->flags |= AL_PACKET_FLAGS_DOT11E_ENABLED;
               }
               packet->dot11e_category = vlan_info->dot11e_category;
               priority = vlan_info->dot1p_priority;
            }
            else
            {
               //priority = 0; //QOS_SUPPORT: read 11e_category from qos_control
		       packet->dot11e_category = FILL_DOT11E_CATEGORY_FROM_QOS_CONTROL(packet->qos_control);
		      //printk(KERN_EMERG "QOS_SUPPORT: %s-%d packet->dot11e_category=%d \n", __func__, __LINE__, packet->dot11e_category);
	    	   priority = packet->dot11e_category;
            }
         }
         else                    /* 2.2.2 */

         {
            //priority = 0; //QOS_SUPPORT: read 11e_category from qos_control
		    packet->dot11e_category = FILL_DOT11E_CATEGORY_FROM_QOS_CONTROL(packet->qos_control);
		    //printk(KERN_EMERG "QOS_SUPPORT: %s-%d packet->dot11e_category=%d \n", __func__, __LINE__, packet->dot11e_category);
	    	priority = packet->dot11e_category;
            /* 2.2.2.1 and 2.2.2.2 are implicit */
         }

         if (packet->flags & AL_PACKET_FLAGS_DOT11E_ENABLED)
         {
            priority = packet->dot11e_category * 2;
         }
      }
   }

   return priority;
}


al_packet_t *dot1p_queue_remove_packet(AL_CONTEXT_PARAM_DECL access_point_queue_t *ap_queue, al_net_if_t **net_if)
{
   dot1p_queue_t       **queue;
   dot1p_queue_entry_t *entry;
   int                 hindx;
   dot1p_pool_t        *pool_item;
   al_packet_t         *packet = NULL;
   unsigned long		flags;

   MESH_USAGE_PROFILING_INIT
	   if(dot1p_temp_queue_head) {
		   entry = dot1p_temp_queue_head;
		   dot1p_temp_queue_head = dot1p_temp_queue_head->next;
		   entry->next = NULL;
		   q_packet_stats.dot1p_temp_queue_total_packet_removed_count++;
		   q_packet_stats.dot1p_temp_queue_current_packet_count--;
		   *net_if = entry->net_if;
		   packet  = entry->packet;
		   if (packet && packet->flags & AL_PACKET_FLAGS_REQUEUED) {
			   --requeued;
		   }
		   return packet;
	   }
	   else {
		   queue    = (dot1p_queue_t **)ap_queue->queue_data;
		   hindx = 0;
		   MESH_USAGE_PROFILE_START(start)
		   al_spin_lock(queue[hindx]->dot1p_Qsplock, flags);
		   MESH_USAGE_PROFILE_END(profiling_info.dot1p_Qsplock_2,index,start,end)
		   if(!queue[hindx]->dot1p_queue_head) {
			   MESH_USAGE_PROFILE_START(start)
			   al_spin_unlock(queue[hindx]->dot1p_Qsplock, flags);
			   MESH_USAGE_PROFILE_END(profiling_info.dot1p_Qspunlock_2,index,start,end)
			   return NULL;
			}
		   dot1p_temp_queue_head = queue[hindx]->dot1p_queue_head;
		   dot1p_temp_queue_tail = queue[hindx]->dot1p_queue_tail;
		   queue[hindx]->dot1p_queue_head = NULL;
		   queue[hindx]->dot1p_queue_tail = NULL;
		   q_packet_stats.dot1p_queue_total_packet_removed_count += q_packet_stats.dot1p_queue_current_packet_count;
		   q_packet_stats.dot1p_temp_queue_total_packet_added_count += q_packet_stats.dot1p_queue_current_packet_count;
		   q_packet_stats.dot1p_temp_queue_current_packet_count += q_packet_stats.dot1p_queue_current_packet_count;
		   q_packet_stats.dot1p_queue_current_packet_count = 0;
		   MESH_USAGE_PROFILE_START(start)
		   al_spin_unlock(queue[hindx]->dot1p_Qsplock, flags);
		   MESH_USAGE_PROFILE_END(profiling_info.dot1p_Qspunlock_2,index,start,end)
		   if(q_packet_stats.dot1p_temp_queue_current_packet_count > q_packet_stats.dot1p_temp_queue_current_packet_max_count) {
				   q_packet_stats.dot1p_temp_queue_current_packet_max_count = q_packet_stats.dot1p_temp_queue_current_packet_count;
			}
		   if(dot1p_temp_queue_head) {
			   entry = dot1p_temp_queue_head;
			   dot1p_temp_queue_head = dot1p_temp_queue_head->next;
			   entry->next = NULL;
			   q_packet_stats.dot1p_temp_queue_total_packet_removed_count++;
			   q_packet_stats.dot1p_temp_queue_current_packet_count--;
			   *net_if = entry->net_if;
			   packet  = entry->packet;
			   if (packet && packet->flags & AL_PACKET_FLAGS_REQUEUED) {
				   --requeued;
			   }
			   return packet;
		   }
		   return NULL;
	   }
}


int dot1p_queue_empty_check(AL_CONTEXT_PARAM_DECL access_point_queue_t *ap_queue)
{
   dot1p_queue_t **queue;
   int           i, pindx, hindx;

   if(!ap_queue) {
	   //al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"MESH_AP:ERROR: %s<%d> ap_queue is NULL\n", __func__, __LINE__);
	   return 0;
   }

   queue    = (dot1p_queue_t **)ap_queue->queue_data;
   hindx = 0;
   hindx = _get_HQindx(queue, hindx);
   
   if (hindx == -1) {
	   return 0;
   }else {
	   return 1;
   }
}

static AL_INLINE int _get_HQindx(dot1p_queue_t **queue, int indx)
{
   int i;
   char cur_indx;
   unsigned long flags;
  
   for (i = indx; i < NUMBER_OF_HQUEUES; i++)
   {
      if (queue[i] && (queue[i]->dot1p_queue_head != NULL))
      {
         return i;
      }
   }
   
   return -1;
}


int dot1p_queue_print_stats(AL_CONTEXT_PARAM_DECL struct access_point_queue *ap_queue, struct seq_file *m)
{
   dot1p_queue_t **queue;
   int           i, j;
   al_packet_pool_info_t al_info;

   for(j = 0;j < NUMBER_OF_PQUEUES; j++) {
	   queue = (dot1p_queue_t **)ap_queue->queue_data;
	   al_get_packet_pool_info(AL_CONTEXT & al_info);

	   seq_printf(m,
			   "+---+----------+----------+----------+----------+----------+\n"
			   "|   |TOTAL POOL|TOTAL HEAP|REQUEUED  |TOT REQ   |    SIZE  |\n"
			   "+---+----------+----------+----------+----------+----------+\n");

	   seq_printf(m,
			   "|   |%010d|%010d|%010d|%010d|%010d|\n",
			   total_pool,
			   total_heap,
			   requeued,
			   total_requeued,
			   _DOT1P_QUEUE_PACKET_POOL_SIZE);

	   seq_printf(m,
			   "+---+----------+----------+----------+----------+----------+\n"
			   "|   | CURRENT  |   MAX    |   ADDED  |  REMOVED |          |\n"
			   "+---+----------+----------+----------+----------+----------+\n"
			   );

	   for (i = 0; i < _DOT1P_QUEUE_COUNT; i++)
	   {
		   seq_printf(m,
				   "|%03d|%010d|%010d|%010d|%010d|          |\n",
				   i,
				   queue[i]->current_level,
				   queue[i]->max_level,
				   queue[i]->total_packets_added,
				   queue[i]->total_packets_removed);
	   }

	   seq_printf(m,
			   "+---+----------+----------+----------+----------+----------+\n"
			   "|   | CURRENT  |   MIN    |HEAP ALLOC|HEAP FREE |   SIZE   |\n"
			   "+---+----------+----------+----------+----------+----------+\n"
			   );

	   seq_printf(m,
			   "|AL |%010d|%010d|%010d|%010d|%010d|\n",
			   al_info.pool_current,
			   al_info.pool_min,
			   0,
			   0,
			   al_info.pool_max);

	   seq_printf(m,
			   "|2K |%010d|%010d|%010d|%010d|%010d|\n",
			   al_info.gen_2k_current,
			   al_info.gen_2k_min,
			   al_info.gen_2k_heap_alloc,
			   al_info.gen_2k_heap_free,
			   al_info.gen_2k_max);

	   seq_printf(m,
			   "|MIP|%010d|%010d|%010d|%010d|%010d|\n",
			   0,
			   0,
			   al_info.mip_skb_alloc,
			   al_info.mip_skb_free,
			   0);
   }
   return 0;
}


int dot1p_queue_requeue_packet(AL_CONTEXT_PARAM_DECL struct access_point_queue *ap_queue, al_net_if_t *al_net_if, al_packet_t *packet)
{
   dot1p_queue_t **queue;
   
   queue = (dot1p_queue_t **)ap_queue->queue_data;

   packet->flags |= AL_PACKET_FLAGS_REQUEUED;
   ++requeued;
   ++total_requeued;

   _dot1p_add_packet_helper(AL_CONTEXT 0, queue, al_net_if, packet);

   return 0;
}


//int dot1p_queue_print_hstats(AL_CONTEXT_PARAM_DECL struct access_point_queue* ap_queue,char* print_p)
int dot1p_queue_print_hstats(AL_CONTEXT_PARAM_DECL struct access_point_queue *ap_queue, struct seq_file *m)
{
   dot1p_queue_t **queue;
   int           i, j;

   for(j = 0;j < NUMBER_OF_PQUEUES; j++) {
	   queue = (dot1p_queue_t **)ap_queue->queue_data;

	   TABLE_START(m);
	   TABLE_ROW_HEAD_START(m);
	   TABLE_ROW_HEAD(m, "Queue ");
	   TABLE_ROW_HEAD(m, "Current Level ");
	   TABLE_ROW_HEAD(m, "Max Level");
	   TABLE_ROW_HEAD(m, "Total Packets Added ");
	   TABLE_ROW_HEAD(m, "Total Packets Removed ");
	   TABLE_ROW_HEAD_END(m);

	   for (i = 0; i < _DOT1P_QUEUE_COUNT; i++)
	   {
		   TABLE_ROW_DATA_START(m);
		   TABLE_ROW_DATA_INT(m, i);
		   TABLE_ROW_DATA_INT(m, queue[i]->current_level);
		   TABLE_ROW_DATA_INT(m, queue[i]->max_level);
		   TABLE_ROW_DATA_INT(m, queue[i]->total_packets_added);
		   TABLE_ROW_DATA_INT(m, queue[i]->total_packets_removed);
		   TABLE_ROW_DATA_END(m);
	   }

	   TABLE_END(m);

	   LINE_INT_INT(m, "Total Pool", total_pool, "  Total Heap", total_heap);
   }

   return 0;
}
