/********************************************************************************
* MeshDynamics
* --------------
* File     : access_point_effistream.h
* Comments : Match criteria etc. for EffiStream
* Created  : 2/21/2007
* Author   : Prachiti Gaikwad
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  1  |11/8/2007 | Changes for queued retry                        |Prachiti|
* -----------------------------------------------------------------------------
* |  0  |2/21/2007 | Created.                                        |Prachiti|
* -----------------------------------------------------------------------------
********************************************************************************/

#ifndef _ACCESS_POINT_EFFISTREAM_H_
#define _ACCESS_POINT_EFFISTREAM_H_

#define MATCH_ACTION_NONE                   0
#define MATCH_ACTION_NO_ACK                 1
#define MATCH_ACTION_QUEUED_RETRY           2
#define MATCH_ACTION_DROP                   -2

#define MATCH_TRANSFORM_TYPE_NONE           0
#define MATCH_TRANSFORM_TYPE_TRANSFORM      1
#define MATCH_TRANSFORM_TYPE_DETRANSFORM    2

#define EFFISTREAM_ENABLED                  (globals.config.criteria_root->first_child != NULL)

int match_and_apply_criteria(AL_CONTEXT_PARAM_DECL al_packet_t *packet, al_conf_effistream_criteria_t *criteria);
int match_and_set_dot1p_priority(AL_CONTEXT_PARAM_DECL al_packet_t *packet, int priority, al_conf_effistream_criteria_t *criteria);

#endif /* _ACCESS_POINT_EFFISTREAM_H_ */
