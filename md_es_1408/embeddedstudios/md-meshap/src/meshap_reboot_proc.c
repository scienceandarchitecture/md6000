/********************************************************************************
* MeshDynamics
* --------------
* File     : meshap_reboot_proc.c
* Comments : Reboot information /proc Entry
* Created  : 7/25/2007
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  1  |8/6/2007  | REBOOT flip-bit flag printed                    | Sriram |
* -----------------------------------------------------------------------------
* |  0  |7/25/2007 | Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

#include <generated/autoconf.h>
#include <linux/version.h>
#include <linux/module.h>
#include <linux/init.h>
#include <linux/if.h>
#include <linux/netdevice.h>
#include <linux/skbuff.h>
#include <linux/cache.h>
#include <linux/pci.h>
#include <linux/wireless.h>
#include <asm/uaccess.h>
#include <net/iw_handler.h>
#include <linux/delay.h>
#include <linux/seq_file.h>
#include "al.h"
#include "al_impl_context.h"
#include "al_802_11.h"
#include "meshap_proc.h"
#include "torna_hw_id.h"

//static int _reboot_list_routine(char *page, char **start, off_t off,int count, int *eof, void *data)
static int _reboot_list_routine(struct seq_file *m, void *v)
{
   torna_reboot_info_t info;
   unsigned int        stack_dump[64];
   char                *p;
   int                 ret;
   int                 i;
   unsigned int        pc_reg;
   unsigned int        caller_reg;
   unsigned char       flag;

   /*if(off != 0) {
    * eof	= 1;
    *      return 0;
    * }*/

   memset(&info, 0, sizeof(torna_reboot_info_t));

   ret = torna_get_reboot_info(&info, &flag, stack_dump);

   memcpy(&pc_reg, info.pc_register, sizeof(info.pc_register));
   memcpy(&caller_reg, info.caller_register, sizeof(info.caller_register));

   /**start		= page + off;
    * p			= page;*/

   /*if(ret == 0) {
    *
    *      p			+= sprintf(p,"Code: %d Flag: %d\n",info.code,flag);
    *      p			+= sprintf(p,"Process: %s\n",info.process_name);
    *      p			+= sprintf(p,"Mem Used: %d MB\n",info.mem_used);
    *      p			+= sprintf(p,"Temp: %d\n",info.temp);
    *      p			+= sprintf(p,"Voltage: %d\n",info.voltage);
    *      p			+= sprintf(p,"PC REG: %08X\n",pc_reg);
    *      p			+= sprintf(p,"CALLER REG: %08X\n",caller_reg);
    *      p			+= sprintf(p,"STACK DUMP: %d\n",info.stack_dump_count);
    *
    *      for(i = 0; i < info.stack_dump_count; i++) {
    *              if((i % 8) == 0)
    *                      p	+= sprintf(p,"\n");
    *              p	+= sprintf(p,"%08X ",stack_dump[i]);
    *      }
    * }*/
   if (ret == 0)
   {
      seq_printf(m, "Code: %d Flag: %d\n", info.code, flag);
      seq_printf(m, "Process: %s\n", info.process_name);
      seq_printf(m, "Mem Used: %d MB\n", info.mem_used);
      seq_printf(m, "Temp: %d\n", info.temp);
      seq_printf(m, "Voltage: %d\n", info.voltage);
      seq_printf(m, "PC REG: %08X\n", pc_reg);
      seq_printf(m, "CALLER REG: %08X\n", caller_reg);
      seq_printf(m, "STACK DUMP: %d\n", info.stack_dump_count);

      for (i = 0; i < info.stack_dump_count; i++)
      {
         if ((i % 8) == 0)
         {
            seq_printf(m, "\n");
         }
         seq_printf(m, "%08X ", stack_dump[i]);
      }
   }
}


static int _reboot_list_proc_open(struct inode *inode, struct file *file)
{
   return single_open(file, _reboot_list_routine, NULL);
}


static const struct file_operations proc_file_fops =
{
   .owner   = THIS_MODULE,
   .open    = _reboot_list_proc_open,
   .read    = seq_read,
   .llseek  = seq_lseek,
   .release = single_release,
};



int meshap_reboot_proc_initialize(void)
{
   meshap_proc_add_read_entry("reboot", &proc_file_fops, NULL);

   return 0;
}
