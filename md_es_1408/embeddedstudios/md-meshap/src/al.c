/********************************************************************************
* MeshDynamics
* --------------
* File     : al.c
* Comments : Torna MeshAP abstraction layer
* Created  : 5/25/2004
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  28 |8/22/2008 | Changes for SIP                                 |Abhijit |
* -----------------------------------------------------------------------------
* |  27 |8/16/2007 | al_get_packet_pool_info Added                   | Sriram |
* -----------------------------------------------------------------------------
* |  26 |8/16/2007 | al_get_cpu_load_info Added                      | Sriram |
* -----------------------------------------------------------------------------
* |  25 |7/24/2007 | REBOOT logging changes                          | Sriram |
* -----------------------------------------------------------------------------
* |  24 |7/17/2007 | Added al_get_board_mem_info                     | Sriram |
* -----------------------------------------------------------------------------
* |  23 |7/9/2007  | Added al_enable_reset_generator                 | Sriram |
* -----------------------------------------------------------------------------
* |  22 |2/6/2007  | FIPS related changes                            |Prachiti|
* -----------------------------------------------------------------------------
* |  21 |11/16/2006| Added Gen 2K Buffer Pool                        | Sriram |
* -----------------------------------------------------------------------------
* |  20 |5/30/2006 | Send up stack fix for local private address     | Sriram |
* -----------------------------------------------------------------------------
* |  19 |3/14/2006 | Changes for ACL                                 | Bindu  |
* -----------------------------------------------------------------------------
* |  18 |02/15/2006| Changes for dot11e                              | Bindu  |
* -----------------------------------------------------------------------------
* |  17 |10/5/2005 | Changes for enabling MIP                        | Sriram |
* -----------------------------------------------------------------------------
* |  16 |9/22/2005 | Interrupt disable/enable semantics changed      | Sriram |
* -----------------------------------------------------------------------------
* |  15 |6/16/2005 | Randomizer fixed                                | Sriram |
* -----------------------------------------------------------------------------
* |  14 |5/9/2005  | Watchdog timer additional changes               | Sriram |
* -----------------------------------------------------------------------------
* |  13 |5/8/2005  | Watchdog timer framework added                  | Sriram |
* -----------------------------------------------------------------------------
* |  12 |4/22/2005 | Added al_restart_mesh                           | Sriram |
* -----------------------------------------------------------------------------
* |  11 |4/17/2005 | Added al_reboot_board                           | Sriram |
* -----------------------------------------------------------------------------
* |  10 |4/6/2005  | Security changes                                | Sriram |
* -----------------------------------------------------------------------------
* |  9  |03/09/2005| al_notify_message moved to al_impl              | Anand  |
* -----------------------------------------------------------------------------
* |  8  |03/05/2005| al_notify_message implemented                   | Anand  |
* -----------------------------------------------------------------------------
* |  7  |12/26/2004| Removed interrupt functions                     | Sriram |
* -----------------------------------------------------------------------------
* |  6  |12/25/2004| Added conditional compiling for MIP             | Sriram |
* -----------------------------------------------------------------------------
* |  5  |12/22/2004| Made changes to enhance robustness              | Sriram |
* -----------------------------------------------------------------------------
* |  4  |12/15/2004| Changes for get_ds_if TDDI call                 | Sriram |
* -----------------------------------------------------------------------------
* |  3  |9/03/2004 | Changes for mip interface                       | Anand  |
* -----------------------------------------------------------------------------
* |  2  |7/22/2004 | al_send_packet_up_stack and misc changes        | Sriram |
* -----------------------------------------------------------------------------
* |  1  |6/9/2004  | Misc changes                                    | Sriram |
* -----------------------------------------------------------------------------
* |  0  |5/25/2004 | Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

#include <generated/autoconf.h>

#include <linux/module.h>
#include <linux/init.h>
#include <linux/version.h>
#include <linux/netdevice.h>
#include <linux/skbuff.h>
#include <linux/wireless.h>
#include <linux/random.h>
#include <linux/inetdevice.h>
#include <linux/kernel_stat.h>
#include <linux/smp.h>
#include <linux/etherdevice.h>
#include <linux/kernel.h>
#include <linux/mm.h>
#include <linux/semaphore.h>
#include <linux/sched.h>
#include <asm/div64.h>

#include "meshap.h"
#include "meshap_logger.h"
#include "al.h"
#include "al_802_11.h"
#include "meshap_core.h"
#include "meshap_ip_device.h"
#include "torna_log.h"
#include "al_packet_impl.h"
#include "al_conf.h"
#include "dot11e_conf.h"
#include "acl_conf.h"
#include "mobility_conf.h"
#include "meshap_tddi.h"
#include "fips_impl.h"
#include "sip_conf.h"
#include "access_point.h"
#include <linux/wait.h>
#include <linux/kthread.h>
#include <linux/err.h>

//RAMESH16MIG defining macro INTERRUPTIBLE_SLEEP_ON
#include <linux/wait.h>
#define INTERRUPTIBLE_SLEEP_ON(LOCKD_EXIT, WAIT)             \
   do {                                                      \
      prepare_to_wait(LOCKD_EXIT, WAIT, TASK_INTERRUPTIBLE); \
      schedule();                                            \
      finish_wait(LOCKD_EXIT, WAIT);                         \
   } while (0)
//RAMESH16MIG defining macro INTERRUPTIBLE_SLEEP_ON_TIMEOUT
#define INTERRUPTIBLE_SLEEP_ON_TIMEOUT(LOCKD_EXIT, HZ, WAIT) \
   do {                                                      \
/*					int retval;*/                                 \
      prepare_to_wait(LOCKD_EXIT, WAIT, TASK_INTERRUPTIBLE); \
      ret = schedule_timeout(HZ);                            \
      finish_wait(LOCKD_EXIT, WAIT);                         \
/*					retval;*/                                     \
   } while (0)
//RAMESH16MIG

#define _FIPS_ENABLED(reg_domain)    (reg_domain & 0x0200)

AL_DECLARE_GLOBAL_EXTERN(int mesh_state);
AL_DECLARE_GLOBAL_EXTERN(access_point_globals_t globals);

struct _meshap_al_watchdog
{
   char                       thread_name[32];
   int                        timeout_in_millis;
   al_watchdog_routine_t      routine;
   long                       last_heard;
   unsigned char              suspended;
   struct _meshap_al_watchdog *next;
   struct _meshap_al_watchdog *prev;
};

typedef struct _meshap_al_watchdog   _meshap_al_watchdog_t;

struct _meshap_al_thread
{
   al_thread_param_t    actual_param;
   al_thread_function_t function;
   int                  nice_value;
};

typedef struct _meshap_al_thread   _meshap_al_thread_t;

struct _meshap_al_semaphore
{
   struct semaphore semahpore;
   int              max_count;                                          /** Max Count is not used in Linux */
   int              initial_count;
};

typedef struct _meshap_al_semaphore   _meshap_al_semaphore_t;

struct _meshap_al_event
{
   wait_queue_head_t wqh;
   int               manual_reset;
   MESHAP_ATOMIC     state;                             /* 0 for not-signalled 1 for signalled */
};

typedef struct _meshap_al_event   _meshap_al_event_t;

/**
 * MESHAP_AL_GEN_2KB name is used here so that it is clear
 * these general purpose buffers are for 2KB or less allocations
 * only. Callers wanting more should call al_heap_alloc
 */

#define _MESHAP_AL_GEN_2KB_MAX_COUNT    40

#define _MESHAP_AL_GEN_2KB_TYPE_POOL    0
#define _MESHAP_AL_GEN_2KB_TYPE_HEAP    1

struct _meshap_al_gen_2KB_buffer_pool
{
   unsigned char                         buffer[2304]; /* The 2KB buffer */
   unsigned char                         type;         /* _MESHAP_AL_GEN_2KB_TYPE_* */
   struct _meshap_al_gen_2KB_buffer_pool *next_pool;
};

typedef struct _meshap_al_gen_2KB_buffer_pool   _meshap_al_gen_2KB_buffer_pool_t;

al_spinlock_t buf_2k_splock;
AL_DEFINE_PER_CPU(int, buf_2k_spvar);

#define RAND_MAX    0x7fff

#define pkt_set_word(packet,position,value) do {\
  unsigned short word; \
  word = (unsigned short)value; \
  memcpy((unsigned char*)packet + position,&word,2); \
}while(0)

static long holdrand;
static _meshap_al_watchdog_t *_watchdog_head;
static int           _watchdog_semaphore;
static int           _watchdog_quit_event;
static int           *_watchdog_thread_quit_ptr;
static unsigned long _last_jiffies;
static _meshap_al_gen_2KB_buffer_pool_t *_buffer_pool;
static _meshap_al_gen_2KB_buffer_pool_t *_buffer_pool_head;
static int _buffer_pool_count;
static int _buffer_pool_count_min;
static int _buffer_pool_count_heap_alloc;
static int _buffer_pool_count_heap_free;

static int _meshap_al_thread(void *data)
{
   _meshap_al_thread_t *thread;

   int ret;

   if (!data)
   {
      printk(KERN_EMERG "RAMESH16MIG : @@@ func : %s line : %d data is NULL \n", __func__, __LINE__);
   }

   thread = (_meshap_al_thread_t *)data;

   thread->actual_param.thread_id = current->pid;
//RAMESH16MIG
//	daemonize ("meshap_al_thread");

   ret = thread->function(&thread->actual_param);

   kfree(thread);

   complete_and_exit(NULL, ret);

   return ret;
}


#ifdef _AL_HEAP_DEBUG

static inline void _add_heap_debug_info_item(void *memblock, size_t size, const char *file_name, int line)
{
   meshap_heap_debug_info_t *item;
   int           bucket;
   unsigned long flags;

   item = (meshap_heap_debug_info_t *)kmalloc(sizeof(meshap_heap_debug_info_t), GFP_ATOMIC);
   memset(item, 0, sizeof(meshap_heap_debug_info_t));

   item->file_name   = file_name;
   item->line_number = line;
   item->memblock    = memblock;
   item->size        = size;
   item->timestamp   = jiffies;

   bucket = ((unsigned int)memblock) % MESHAP_HEAP_DEBUG_HASH_SIZE;

   al_spin_lock(core_gbl_splock, core_gbl_spvar);

   if (size > meshap_core_globals.max_block_size)
   {
      meshap_core_globals.max_block_size = size;
   }

   meshap_core_globals.active_total += size;

   if (meshap_core_globals.active_total > meshap_core_globals.active_max)
   {
      meshap_core_globals.active_max = meshap_core_globals.active_total;
   }

   meshap_core_globals.total_alloc += size;
   ++meshap_core_globals.alloc_count;


   /** Add to hash */

   item->next_hash = meshap_core_globals.heap_debug_hash[bucket];
   if (meshap_core_globals.heap_debug_hash[bucket] != NULL)
   {
      meshap_core_globals.heap_debug_hash[bucket]->prev_hash = item;
   }
   meshap_core_globals.heap_debug_hash[bucket] = item;

   /** Add to list */

   item->next_list = meshap_core_globals.heap_debug_list;
   if (meshap_core_globals.heap_debug_list != NULL)
   {
      meshap_core_globals.heap_debug_list->prev_list = item;
   }
   meshap_core_globals.heap_debug_list = item;

   al_spin_unlock(core_gbl_splock, core_gbl_spvar);
}


static inline void _remove_heap_debug_info_item(void *memblock)
{
   int                      bucket;
   unsigned long            flags;
   meshap_heap_debug_info_t *item;

   bucket = ((unsigned int)memblock) % MESHAP_HEAP_DEBUG_HASH_SIZE;

   al_spin_lock(core_gbl_splock, core_gbl_spvar);

   item = meshap_core_globals.heap_debug_hash[bucket];

   while (item != NULL)
   {
      if (item->memblock == memblock)
      {
         /** Remove from Hash */
         if (item->prev_hash != NULL)
         {
            item->prev_hash->next_hash = item->next_hash;
         }
         if (item->next_hash != NULL)
         {
            item->next_hash->prev_hash = item->prev_hash;
         }
         if (meshap_core_globals.heap_debug_hash[bucket] == item)
         {
            meshap_core_globals.heap_debug_hash[bucket] = item->next_hash;
         }
         /** Remove from list */
         if (item->prev_list != NULL)
         {
            item->prev_list->next_list = item->next_list;
         }
         if (item->next_list != NULL)
         {
            item->next_list->prev_list = item->prev_list;
         }
         if (meshap_core_globals.heap_debug_list == item)
         {
            meshap_core_globals.heap_debug_list = item->next_list;
         }
         break;
      }
      item = item->next_hash;
   }

   if (item != NULL)
   {
      meshap_core_globals.active_total -= item->size;
      meshap_core_globals.total_freed  += item->size;
      ++meshap_core_globals.free_count;
   }
   else
   {
      ++meshap_core_globals.free_miss_count;
   }

   if (meshap_core_globals.last_freed_item != NULL)
   {
      kfree(meshap_core_globals.last_freed_item);
   }

   meshap_core_globals.last_freed_item = item;

   al_spin_unlock(core_gbl_splock, core_gbl_spvar);
}
#endif

#ifndef _AL_HEAP_DEBUG
void *al_heap_alloc(AL_CONTEXT_PARAM_DECL size_t size)
#else
void *al_heap_alloc(AL_CONTEXT_PARAM_DECL size_t size, const char *file_name, int line)
#endif
{
   void *p;

   p = kmalloc(size, GFP_ATOMIC);
   if (p == NULL)
   {
      printk(KERN_INFO "al_heap_alloc : kmalloc returned NULL\n");
   }

   if(p) {
	   memset(p, 0, size);
   }

#ifdef _AL_HEAP_DEBUG
   _add_heap_debug_info_item(p, size, file_name, line);
#endif

   return p;
}

void al_heap_free(AL_CONTEXT_PARAM_DECL void *memblock)
{
#ifdef _AL_HEAP_DEBUG
   _remove_heap_debug_info_item(memblock);
#endif
   kfree(memblock);
}

int al_create_thread_on_cpu(AL_CONTEXT_PARAM_DECL al_thread_function_t thread_function, void *param, int cpu, char *name)
{
   struct task_struct *task;
   task = kthread_create(thread_function, param, name);
   if (IS_ERR(task)) {
	return 1;
   }

   kthread_bind(task, cpu);
   wake_up_process(task);
   return 0;
}
//RAMESH16MIG passing name to al_create_thread
int al_create_thread(AL_CONTEXT_PARAM_DECL al_thread_function_t thread_function, void *param, int priority, char *name)
{
   _meshap_al_thread_t *thread;

   thread = (_meshap_al_thread_t *)kmalloc(sizeof(_meshap_al_thread_t), GFP_ATOMIC);

   if (thread == NULL)
   {
      printk(KERN_EMERG "al_create_thread: kmalloc returned NULL\n");
   }

   memset(thread, 0, sizeof(_meshap_al_thread_t));

   switch (priority)
   {
   case AL_THREAD_PRIORITY_LOW:
      thread->nice_value = 20;
      break;

   case AL_THREAD_PRIORITY_BELOW_NORMAL:
      thread->nice_value = 15;
      break;

   case AL_THREAD_PRIORITY_NORMAL:
      thread->nice_value = 10;
      break;

   case AL_THREAD_PRIORITY_ABOVE_NORMAL:
      thread->nice_value = 5;
      break;

   case AL_THREAD_PRIORITY_HIGH:
      thread->nice_value = 0;
      break;

   case AL_THREAD_PRIORITY_REAL_TIME:
      thread->nice_value = -5;
      break;

   default:
      thread->nice_value = 10;
   }

   thread->function           = thread_function;
   thread->actual_param.param = param;

//RAMESH16MIG passing name as 3rd parameter
//	KERNEL_THREAD(_meshap_al_thread,thread,CLONE_FS|CLONE_FILES);
   KERNEL_THREAD(_meshap_al_thread, thread, name);

   return 0;
}


int al_post_thread_message(AL_CONTEXT_PARAM_DECL int thread_id, int message_id, void *message_data)
{
   /** TODO */
   return -1;
}


int al_get_thread_message(AL_CONTEXT_PARAM_DECL int thread_id, al_thread_message_t *message)
{
   /** TODO */
   return -1;
}


int al_create_semaphore(AL_CONTEXT_PARAM_DECL int max_count, int initial_count)
{
   _meshap_al_semaphore_t *semaphore;

   semaphore = (_meshap_al_semaphore_t *)kmalloc(sizeof(_meshap_al_semaphore_t), GFP_ATOMIC);

   semaphore->initial_count = initial_count;
   semaphore->max_count     = max_count;

   sema_init(&semaphore->semahpore, initial_count);

   return (int)semaphore;
}


static inline int sem_getcount(struct semaphore *sem)
{
   return (int)(&(sem)->count);
}


int al_wait_on_semaphore(AL_CONTEXT_PARAM_DECL int semaphore_id, unsigned int timeout)
{
   _meshap_al_semaphore_t *semaphore;
   int count;

   semaphore = (_meshap_al_semaphore_t *)semaphore_id;

   if (in_interrupt())
   {
      count = sem_getcount(&semaphore->semahpore);
      if (count != 0)
      {
         down(&semaphore->semahpore);
      }
      printk(KERN_INFO "========= al_wait_on_semaphore : Called from interrupt context =================\n");
      return AL_WAIT_STATE_SIGNAL;
   }

   if (timeout == 0)
   {
      /**
       * Return the current state of the semaphore
       */

      count = sem_getcount(&semaphore->semahpore);

      return(count == 0 ? AL_WAIT_STATE_TIMEOUT : AL_WAIT_STATE_SIGNAL);
   }
   else
   {
      /**
       * Under Linux as of now we have not implemented waiting
       * with a timeout for a semaphore.
       */

      down(&semaphore->semahpore);

      return AL_WAIT_STATE_SIGNAL;
   }
}


int al_release_semaphore(AL_CONTEXT_PARAM_DECL int semaphore_id, int release_count)
{
   _meshap_al_semaphore_t *semaphore;

   semaphore = (_meshap_al_semaphore_t *)semaphore_id;

   while (release_count--)
   {
      up(&semaphore->semahpore);
   }

   return 0;
}


int al_destroy_semaphore(AL_CONTEXT_PARAM_DECL int semaphore_id)
{
   _meshap_al_semaphore_t *semaphore;

   semaphore = (_meshap_al_semaphore_t *)semaphore_id;

   kfree(semaphore);

   return 0;
}


int al_create_event(AL_CONTEXT_PARAM_DECL int manual_reset)
{
   _meshap_al_event_t *event;

   event = (_meshap_al_event_t *)kmalloc(sizeof(_meshap_al_event_t), GFP_ATOMIC);

   init_waitqueue_head(&event->wqh);

   event->manual_reset = manual_reset;

   MESHAP_ATOMIC_SET(event->state, 0);

   return (int)event;
}


int al_wait_on_event(AL_CONTEXT_PARAM_DECL int event_id, unsigned int timeout)
{
   _meshap_al_event_t *event;
   unsigned long      flags;
   int                ret;

//RAMESH16MIG defining DEFINE_WAIT for INTERRUPTIBLE_SLEEP_ON_TIMEOUT
   DEFINE_WAIT(wait);
//RAMESH16MIG
   if (in_interrupt())
   {
      printk(KERN_INFO "========= al_wait_on_event : Called from interrupt context =================\n");
      return AL_WAIT_STATE_SIGNAL;
   }

   event = (_meshap_al_event_t *)event_id;


   if (MESHAP_ATOMIC_GET(event->state) == 1)
   {
      return AL_WAIT_STATE_SIGNAL;
   }


   if (timeout == AL_WAIT_TIMEOUT_INFINITE)
   {
      ret = AL_WAIT_STATE_SIGNAL;
//RAMESH16MIG check whether we can replace interruptible_sleep_on with wait_event_interruptible
      INTERRUPTIBLE_SLEEP_ON(&event->wqh, &wait);
   }
   else
   {
//RAMESH16MIG replacing interruptible_sleep_on_timeout
//		if(interruptible_sleep_on_timeout(&event->wqh,(timeout * HZ)/1000) != 0)
      INTERRUPTIBLE_SLEEP_ON_TIMEOUT(&event->wqh, ((timeout * HZ) / 1000), &wait);
#if 0
      if (!ret)
      {
//		if(INTERRUPTIBLE_SLEEP_ON_TIMEOUT(&event->wqh, (timeout * HZ)/1000, &wait) != 0)
         ret = AL_WAIT_STATE_SIGNAL;
      }
      else
      {
         ret = AL_WAIT_STATE_TIMEOUT;
      }
#endif
      ret = AL_WAIT_STATE_TIMEOUT;
//RAMESH16MIG
   }

   return ret;
}


int al_set_event(AL_CONTEXT_PARAM_DECL int event_id)
{
   _meshap_al_event_t *event;

   event = (_meshap_al_event_t *)event_id;

   wake_up(&event->wqh);

   if (event->manual_reset == 0)
   {
      MESHAP_ATOMIC_SET(event->state, 0);
   }

   return 0;
}


int al_reset_event(AL_CONTEXT_PARAM_DECL int event_id)
{
   _meshap_al_event_t *event;
   unsigned long      flags;

   event = (_meshap_al_event_t *)event_id;

   MESHAP_ATOMIC_SET(event->state, 0);

   return 0;
}


int al_destroy_event(AL_CONTEXT_PARAM_DECL int event_id)
{
   _meshap_al_event_t *event;

   event = (_meshap_al_event_t *)event_id;

   kfree(event);

   return 0;
}


al_u64_t al_get_tick_count(AL_CONTEXT_PARAM_DECL_SINGLE)
{
#if 0
   al_u64_t tick;

   /**
    * We only support 64 bit millisecond counting (Basically 1 overflow of jiffies)
    * This makes it 584942417.35 years of millisecond tick counting!!!
    * Jiffies overlfow after 490 days of operation.
    */

   if (jiffies < _last_jiffies)
   {
      tick  = 0xFFFFFFFF;
      tick += jiffies;
   }
   else
   {
      _last_jiffies = jiffies;
      tick          = jiffies;
   }

   //tick	*= 1000;
   //tick	/= HZ;
   //tick = do_div(tick, HZ);

   return tick;
#endif
   return get_jiffies_64();
   //return jiffies;
}


int al_timer_after(AL_CONTEXT_PARAM_DECL al_u64_t timer1, al_u64_t timer2)
{
   return time_after64(timer1, timer2);
}


int al_timer_after_eq(AL_CONTEXT_PARAM_DECL al_u64_t timer1, al_u64_t timer2)
{
   return time_after_eq64(timer1, timer2);
}


int al_timer_before(AL_CONTEXT_PARAM_DECL al_u64_t timer1, al_u64_t timer2)
{
   return time_before64(timer1, timer2);
}


int al_timer_before_eq(AL_CONTEXT_PARAM_DECL al_u64_t timer1, al_u64_t timer2)
{
   return time_before_eq64(timer1, timer2);
}


void al_thread_sleep(AL_CONTEXT_PARAM_DECL int milliseconds)
{
   if (in_interrupt())
   {
      printk(KERN_INFO "========= al_thread_sleep : Called from interrupt context =================\n");
      return;
   }

   set_current_state(TASK_INTERRUPTIBLE);
   schedule_timeout((milliseconds * HZ) / 1000);
   set_current_state(TASK_RUNNING);
}


int al_get_net_if_count(AL_CONTEXT_PARAM_DECL_SINGLE)
{
   return meshap_core_get_created_net_if_count();
}


al_net_if_t *al_get_net_if(AL_CONTEXT_PARAM_DECL int index)
{
   meshap_core_net_if_t *core_net_if;

   core_net_if = meshap_core_get_created_net_if(index);
   return (core_net_if != NULL) ? &core_net_if->al_net_if : NULL;
}


int al_set_on_receive_hook(AL_CONTEXT_PARAM_DECL al_on_receive_t on_recieve)
{
   meshap_core_globals.on_receive = on_recieve;
   return 0;
}


int al_set_on_before_transmit_hook(AL_CONTEXT_PARAM_DECL al_on_before_transmit_t on_before_transmit)
{
   meshap_core_globals.on_before_transmit = on_before_transmit;
   return 0;
}


int al_set_on_phy_link_notify_hook(AL_CONTEXT_PARAM_DECL al_on_phy_link_notify_t on_phy_link_notify)
{
   meshap_core_globals.on_phy_link_notify = on_phy_link_notify;
   return 0;
}


static void _process_up_stack_packet(AL_CONTEXT_PARAM_DECL al_net_if_t *al_net_if, meshap_core_packet_t *core_packet)
{
   meshap_core_net_if_t *core_net_if;
   struct net_device    *mip_dev;
   struct in_device     *in_dev;
   unsigned char        mip_ip_addr[4];
   unsigned char        *packet_data;
   struct ethhdr        *ethhdr;

   static const unsigned char _multicast_addr_prefix[] =
   {
      0x01, 0x00, 0x5E
   };

   /**
    * For packet's going upstack, we send them up using the following rules :
    *	If destination IP address in the packet matches mip or if broadcast, the packet goes in via mip
    *	else depending on the destination MAC address, the packet goes up via that interface
    */

   core_net_if = (meshap_core_net_if_t *)al_net_if;
   mip_dev     = dev_get_by_name(&init_net, "mip0");
   packet_data = core_packet->al_packet.buffer + core_packet->al_packet.position;
   in_dev      = (struct in_device *)mip_dev->ip_ptr;

   if ((in_dev != NULL) && (in_dev->ifa_list != NULL))
   {
      memcpy(mip_ip_addr, &in_dev->ifa_list->ifa_address, 4);
   }
   else
   {
      memset(mip_ip_addr, 0, 4);
   }

   switch (al_be16_to_cpu(core_packet->al_packet.type))
   {
   case AL_PACKET_TYPE_IP:
      if (!memcmp(core_packet->al_packet.addr_1.bytes, broadcast_net_addr.bytes, ETH_ALEN) ||
          !memcmp(core_packet->al_packet.addr_1.bytes, _multicast_addr_prefix, sizeof(_multicast_addr_prefix)))
      {
         core_packet->skb->dev = mip_dev;
      }
      else
      {
         unsigned char packet_dest_ip[4];

         memcpy(packet_dest_ip, packet_data + 16, 4);              /** Destination Address is at offset 16 */

         if (!memcmp(mip_ip_addr, packet_dest_ip, 4))
         {
            core_packet->skb->dev = mip_dev;
         }
         else
         {
            core_packet->skb->dev = core_net_if->dev;
         }
      }
         skb_push(core_packet->skb, ETH_HLEN);
         ethhdr = (struct ethhdr *)core_packet->skb->data;
         memcpy(ethhdr->h_dest, core_packet->al_packet.addr_1.bytes, ETH_ALEN);
         memcpy(ethhdr->h_source, core_packet->al_packet.addr_2.bytes, ETH_ALEN);
         ethhdr->h_proto = core_packet->al_packet.type;
         skb_set_mac_header(core_packet->skb, 0);
         skb_set_network_header(core_packet->skb, ETH_HLEN);
         skb_set_transport_header(core_packet->skb, ETH_HLEN + 20);
         skb_pull(core_packet->skb, ETH_HLEN);
      break;

   case AL_PACKET_TYPE_ARP:
      {
         unsigned short opcode;
         unsigned char  arp_target_ip[4];

         memcpy(&opcode, packet_data + 6, 2);              /** Opcode is at offset 6 */

         opcode = al_be16_to_cpu(opcode);

			tx_rx_pkt_stats.sending_arp_pkt_up_stack++;

         switch (opcode)
         {
         case 1:                                        /** ARP request */
         case 2:                                        /** ARP reply */
            memcpy(arp_target_ip, packet_data + 24, 4); /** Target Address is at offset 24 */
            if (!memcmp(mip_ip_addr, arp_target_ip, 4))
            {
               core_packet->skb->dev = mip_dev;
            }
            else
            {
               core_packet->skb->dev = core_net_if->dev;
            }
            break;

         default:
            core_packet->skb->dev = mip_dev;
         }
      }
      break;

   default:       /** All other packets go up via MIP */
      core_packet->skb->dev = mip_dev;
   }

   dev_put(mip_dev);

   core_packet->skb->pkt_type = PACKET_HOST;
}

static inline unsigned short _net_sum_words(unsigned char* packet, int length)
{
  unsigned long   total;
  unsigned short  n,*p,carries,chksum;

  total   = 0;
  n       = length / 2;
  p       = (unsigned short*)packet;

  while(n--)
    total += *p++;

  if(length & 1)
    total += *((unsigned char*)p);

  while(1) {
    carries = (unsigned short)(total >> 16);
    if(!carries)
      break;
    total   = (total & 0xFFFF) + carries;
  }

  chksum = (unsigned short)total;

  return chksum;
}

static inline unsigned short _net_chksum(unsigned short sum, unsigned char* packet, int length)
{
  unsigned long   temp;
  unsigned short  carries;

  temp    = sum;

  temp += _net_sum_words(packet,length);

  while(1) {
    carries = (unsigned short)(temp >> 16);
    if(!carries)
      break;
    temp    = (temp & 0xFFFF) + carries;
  }

  return htons(~((unsigned short)temp));
}

int al_send_packet_up_stack_mgmt_gw(AL_CONTEXT_PARAM_DECL al_net_if_t* al_net_if,al_packet_t* src_packet)
{
  al_packet_t*  packet;
  char       *temp = NULL, *buf = NULL, *p = NULL;
  char      *tempbuf = NULL;
  int       default_ip_addr = 0x0A00000A;
  unsigned short  chksum;

  /* Here we need to do the following:-
     a. Create a copy of the buffer
     b. Update the UDP header to reflect the port of Mgmt GW
     c. Send the packet upstack.
   */
  packet = al_allocate_packet(AL_CONTEXT AL_PACKET_DIRECTION_OUT AL_PACKET_DEBUG_PARAM);

  if(packet == NULL)
    return -1;

  AL_ASSERT("MESH_AP  : send_packet",packet != NULL);

  temp = packet->buffer;

  memcpy(temp, src_packet->buffer, src_packet->buffer_length);
  memcpy(packet, src_packet, sizeof(al_packet_t));

  packet->buffer = temp;

  skb_reserve(((meshap_core_packet_t *)packet)->skb, packet->position);
  skb_put (((meshap_core_packet_t *)packet)->skb, packet->data_length);

  buf         = packet->buffer + packet->position;
  /* set source ip address as default ip address for imcp packets
     upstack to management gateway */
  pkt_set_word(buf,IP_WORD_POSITION_CHKSUM,0);
  memcpy(buf + IP_DWORD_POSITION_SRCADDR,&default_ip_addr,4);

  tempbuf   = buf;
  chksum    = _net_chksum(0,tempbuf,IP_MIN_HEADER_LENGTH);
  chksum    = htons(chksum);
  pkt_set_word(buf,IP_WORD_POSITION_CHKSUM,chksum);

  p = buf + 20; // IP Header Length
  pkt_set_word(p, UDP_WORD_POSITION_DST_PORT, htons(MGMT_GW_UDP_PORT));
  pkt_set_word(p, UDP_WORD_POSITION_UDP_CHKSUM, 0);

  al_add_packet_reference(packet);
  al_send_packet_up_stack(al_net_if, packet);
  al_release_packet(packet);

  return 0;
}

void meshap_process_upstack_pkt(void *data)
{
	struct sk_buff *skb = NULL;
    int pkt_poll = 0, quelen = 0;
	MESH_USAGE_PROFILING_INIT
	while (1) {
		thread_stat.upstack_thread_count++;
		skb = meshap_skb_dequeue(&temp_upstack_pkt_queue, NULL);
		if(skb) {
			pkt_poll = 0;
			MESH_USAGE_PROFILE_START(start)
			thread_stat.upstack_thread_count++;
		   q_packet_stats.temp_upstack_pktQ_total_packet_removed_count++;
           q_packet_stats.temp_upstack_pktQ_current_packet_count--;
			local_bh_disable();
			NETIF_RECEIVE_SKB(skb);
			local_bh_enable();
			MESH_USAGE_PROFILE_END(profiling_info.upstack_process_pkt_func_tprof,index,start,end)
			continue;
		}else {
tryagain:
			quelen = skb_queue_len(&upstack_pkt_queue);
			if(quelen) {
				al_spin_lock_bh_t(&skb_upstack_que_splock);
				quelen = skb_queue_len(&upstack_pkt_queue);
				meshap_splice_skb_queue(&upstack_pkt_queue, &temp_upstack_pkt_queue);
				al_spin_unlock_bh_t(&skb_upstack_que_splock);
				AL_IMPL_ATOMIC_ADD(quelen, q_packet_stats.upstack_pktQ_total_packet_removed_count);
				AL_IMPL_ATOMIC_SUB(quelen, q_packet_stats.upstack_pktQ_current_packet_count);
				q_packet_stats.temp_upstack_pktQ_total_packet_added_count +=quelen;
				q_packet_stats.temp_upstack_pktQ_current_packet_count +=quelen;
				if(q_packet_stats.temp_upstack_pktQ_current_packet_count > q_packet_stats.temp_upstack_pktQ_current_packet_max_count) {
					q_packet_stats.temp_upstack_pktQ_current_packet_max_count = q_packet_stats.temp_upstack_pktQ_current_packet_count;
				}
			}else {
				if(pkt_poll < PKT_POLL_RETRY_COUNT) {
					pkt_poll++;
					goto tryagain;
				}else{
					pkt_poll = 0;
					set_current_state(TASK_INTERRUPTIBLE);
					schedule();
				}
			}
		}
	}
}

int al_send_packet_up_stack(AL_CONTEXT_PARAM_DECL al_net_if_t *al_net_if, al_packet_t *packet)
{
   meshap_core_packet_t *core_packet;
   struct sk_buff       *skb;
   MESH_USAGE_PROFILING_INIT

   core_packet = (meshap_core_packet_t *)packet;

   MESH_USAGE_PROFILE_START(start)

   if (core_packet->incoming_if == NULL)
   {
      skb = al_packet_impl_create_skb((meshap_core_net_if_t *)al_net_if, core_packet);
	  AL_IMPL_ATOMIC_INC(q_packet_stats.upstack_pktQ_total_packet_added_count);
	  AL_IMPL_ATOMIC_INC(q_packet_stats.upstack_pktQ_current_packet_count);
	  if(q_packet_stats.upstack_pktQ_current_packet_count.counter > q_packet_stats.upstack_pktQ_current_packet_max_count.counter) {
		  q_packet_stats.upstack_pktQ_current_packet_max_count.counter = q_packet_stats.upstack_pktQ_current_packet_count.counter;
	  }
	  meshap_skb_enqueue(&upstack_pkt_queue, skb, &skb_upstack_que_splock);
	  wake_up_process(meshap_upstack_pkt_process_task);
   }
   else
   {
#ifndef _MESHAP_NO_MIP_
      _process_up_stack_packet(AL_CONTEXT al_net_if, core_packet);
#endif
      /* skb	= skb_copy(core_packet->skb,GFP_ATOMIC); */
      skb = skb_get(core_packet->skb);
      if (skb != NULL)
      {
	      AL_IMPL_ATOMIC_INC(q_packet_stats.upstack_pktQ_total_packet_added_count);
	      AL_IMPL_ATOMIC_INC(q_packet_stats.upstack_pktQ_current_packet_count);
		  if(q_packet_stats.upstack_pktQ_current_packet_count.counter > q_packet_stats.upstack_pktQ_current_packet_max_count.counter) {
			  q_packet_stats.upstack_pktQ_current_packet_max_count.counter = q_packet_stats.upstack_pktQ_current_packet_count.counter;
		  }
		  meshap_skb_enqueue(&upstack_pkt_queue, skb, &skb_upstack_que_splock);
	      wake_up_process(meshap_upstack_pkt_process_task);
      }
   }

	tx_rx_pkt_stats.rx_up_stack_pkt++;
    al_release_packet(packet);
    MESH_USAGE_PROFILE_END(profiling_info.upstack_send_pkt_func_tprof,index,start,end)

   return 0;
}


#ifndef _AL_PACKET_DEBUG
al_packet_t *al_allocate_packet(AL_CONTEXT_PARAM_DECL int direction)
#else
al_packet_t * al_allocate_packet(AL_CONTEXT_PARAM_DECL int direction, const char *filename, int line)
#endif
{
   meshap_core_packet_t *core_packet;

   core_packet = al_packet_impl_create();

   if (core_packet == NULL)
   {
      return NULL;
   }

   if (direction == AL_PACKET_DIRECTION_OUT)
   {
      core_packet->al_packet.position = core_packet->al_packet.buffer_length;
   }
   else
   {
      core_packet->al_packet.position = 0;
   }

   core_packet->al_packet.data_length = 0;
   core_packet->al_packet.ref_count   = 1;

   return &core_packet->al_packet;
}

void al_release_packet(AL_CONTEXT_PARAM_DECL al_packet_t *packet)
{
   meshap_core_packet_t *core_packet;

   core_packet = (meshap_core_packet_t *)packet;

   --packet->ref_count;

   if ((MESHAP_ATOMIC_GET(core_packet->ref_count) == 1) ||
       MESHAP_ATOMIC_DEC_AND_TEST(core_packet->ref_count))
   {
      al_packet_impl_destroy(core_packet);
   }
}


void al_add_packet_reference(AL_CONTEXT_PARAM_DECL al_packet_t *packet)
{
   meshap_core_packet_t *core_packet;

   core_packet = (meshap_core_packet_t *)packet;

   ++packet->ref_count;
   MESHAP_ATOMIC_INC(core_packet->ref_count);
}


int al_open_file(AL_CONTEXT_PARAM_DECL int file_id, int mode)
{
   /** TODO */
   return -1;
}


int al_read_file_line(AL_CONTEXT_PARAM_DECL int file_handle, char *string, int n)
{
   /** TODO */
   return -1;
}


int al_read_file(AL_CONTEXT_PARAM_DECL int file_handle, unsigned char *buffer, int bytes)
{
   /** TODO */
   return -1;
}


int al_write_file(AL_CONTEXT_PARAM_DECL int file_handle, unsigned char *buffer, int bytes)
{
   /** TODO */
   return -1;
}


int al_puts_file(AL_CONTEXT_PARAM_DECL int file_handle, const char *string)
{
   /** TODO */
   return -1;
}


int al_printf_file(AL_CONTEXT_PARAM_DECL int file_handle, const char *format, ...)
{
   /** TODO */
   return -1;
}


int al_close_file(AL_CONTEXT_PARAM_DECL int file_handle)
{
   /** TODO */
   return -1;
}


int al_print_log(AL_CONTEXT_PARAM_DECL int log_type, const char *format, ...)
{
   va_list args;

   if (log_type & meshap_core_globals.log_mask)
   {
      va_start(args, format);
      vsnprintf(meshap_core_globals.log_buffer, sizeof(meshap_core_globals.log_buffer), format, args);
      va_end(args);
      printk(KERN_INFO "%s", meshap_core_globals.log_buffer);
   }

   return 0;
}
EXPORT_SYMBOL(al_print_log);

void al_set_thread_name(AL_CONTEXT_PARAM_DECL const char *name)
{
   int len;

   len = strlen(name);
   memset(current->comm, 0, sizeof(current->comm));
   memcpy(current->comm, name, len > sizeof(current->comm) - 1 ? sizeof(current->comm) - 1 : len);
}


int al_get_conf_handle(AL_CONTEXT_PARAM_DECL_SINGLE)
{
   int ret;

   if (meshap_core_globals.config_string != NULL)
   {
      ret = al_conf_parse(meshap_core_globals.config_string);
   }
   else
   {
      ret = -1;
   }
   return ret;
}


int al_get_dot11e_conf_handle(AL_CONTEXT_PARAM_DECL_SINGLE)
{
   int ret;

   if (meshap_core_globals.dot11e_config_string != NULL)
   {
      ret = dot11e_conf_parse(meshap_core_globals.dot11e_config_string);
   }
   else
   {
      ret = -1;
   }

   return ret;
}


int al_get_acl_conf_handle(AL_CONTEXT_PARAM_DECL_SINGLE)
{
   int ret;

   if (meshap_core_globals.acl_config_string != NULL)
   {
      ret = acl_conf_parse(meshap_core_globals.acl_config_string);
   }
   else
   {
      ret = -1;
   }
   return ret;
}


int al_get_mobility_conf_handle(AL_CONTEXT_PARAM_DECL_SINGLE)
{
   int ret;

   if (meshap_core_globals.mobility_conf_string != NULL)
   {
      ret = mobility_conf_parse(AL_CONTEXT meshap_core_globals.mobility_conf_string);
   }
   else
   {
      ret = -1;
   }

   return ret;
}


int al_notify_ds_net_if(AL_CONTEXT_PARAM_DECL al_net_if_t *net_if)
{
   meshap_tddi_set_ds_net_if(net_if);

   al_enable_reset_generator(AL_CONTEXT 1, 6000);
   al_strobe_reset_generator(AL_CONTEXT_SINGLE);

   return meshap_ip_device_set_net_if((meshap_core_net_if_t *)net_if);
}


int al_random_initialize(AL_CONTEXT_PARAM_DECL unsigned char *seed)
{
   unsigned char *p = (unsigned char *)&holdrand;

   memcpy(p, seed, 4);

   return 0;
}


long al_random(AL_CONTEXT_PARAM_DECL int min, int max)
{
   long scaled_down;

   holdrand    = (((holdrand * 214013L + 2531011L) >> 16) & RAND_MAX);
   scaled_down = (holdrand) % (max - min);

   return(scaled_down + min);
}


int al_get_board_info(AL_CONTEXT_PARAM_DECL al_board_info_t *board_info)
{
   al_impl_board_info_t board_impl_info;

   al_impl_get_board_info(&board_impl_info);

   board_info->valid_values_mask = board_impl_info.valid_values_mask;
   board_info->temperature       = board_impl_info.temperature;
   board_info->voltage           = board_impl_info.voltage;
   return 0;
}


int al_notify_message(AL_CONTEXT_PARAM_DECL al_notify_message_t *message)
{
   al_impl_notify_message_t impl_message;

   impl_message.message_type = message->message_type;
   impl_message.message_data = message->message_data;
   al_impl_notify_message(&impl_message);
   return 0;
}


void al_enable_reset_generator(AL_CONTEXT_PARAM_DECL int enable_or_disable, int interval_in_millis)
{
   al_impl_enable_reset_generator(enable_or_disable, interval_in_millis);
}


void al_strobe_reset_generator(AL_CONTEXT_PARAM_DECL_SINGLE)
{
   al_impl_strobe_reset_generator();
}


void al_cryptographic_random_bytes(AL_CONTEXT_PARAM_DECL unsigned char *buffer, int length)
{
   /**
    * THIS FUNCTIONS NEEDS TO USE ANSI X9.31 AND NOT
    * KERNEL ENTROPY (get_random_bytes) FOR FIPS 140-2 COMPLIANCE
    */

#ifdef FIPS_1402_COMPLIANT
   unsigned short reg_domain = meshap_tddi_get_regdomain();
   int            ret;

   if (_FIPS_ENABLED(reg_domain))
   {
      ret = get_fips_compliant_random_bytes(buffer, length);
      if (ret == RANDOM_NUMBER_REPEATED)
      {
         printk(KERN_INFO "\nMESH_AP :Random Number Repeated.Rebooting now ...");
         al_reboot_board(AL_CONTEXT AL_REBOOT_BOARD_CODE_FIPS_RNG);
      }
   }
   else
   {
      get_random_bytes(buffer, length);
   }
#else
   get_random_bytes(buffer, length);
#endif
}


void al_reboot_board(AL_CONTEXT_PARAM_DECL unsigned char reboot_code)
{
   meshap_core_reboot_machine(reboot_code);
}


void al_get_board_mem_info(AL_CONTEXT_PARAM_DECL unsigned int *max_mem, unsigned int *free_mem)
{
   struct sysinfo si;

   si_meminfo(&si);

   *max_mem  = si.totalram * PAGE_SIZE;
   *free_mem = si.freeram * PAGE_SIZE;
}


#define cputime64_add(__a, __b)    ((__a) + (__b))

void al_get_cpu_load_info(AL_CONTEXT_PARAM_DECL unsigned char *avg_load)
{
   int           i;
   unsigned long sum;
   unsigned long idle;
   unsigned long jiff;

   sum  = 0;
   idle = 0;
   jiff = jiffies;
#if (LINUX_VERSION_CODE < KERNEL_VERSION(3, 3, 0))
   for_each_possible_cpu(i)
   {
      sum  = cputime64_add(sum, kstat_cpu(i).cpustat.user);
      sum  = cputime64_add(sum, kstat_cpu(i).cpustat.nice);
      sum  = cputime64_add(sum, kstat_cpu(i).cpustat.system);
      idle = cputime64_add(sum, kstat_cpu(i).cpustat.idle);
   }

#else
   for_each_possible_cpu(i)
   {
      sum  = cputime64_add(sum, kcpustat_cpu(i).cpustat[CPUTIME_USER]);
      sum  = cputime64_add(sum, kcpustat_cpu(i).cpustat[CPUTIME_NICE]);
      sum  = cputime64_add(sum, kcpustat_cpu(i).cpustat[CPUTIME_SYSTEM]);
      idle = cputime64_add(sum, kcpustat_cpu(i).cpustat[CPUTIME_IDLE]);
   }
#endif
   if (avg_load != NULL)
   {
      cputime64_t quotient;
      cputime64_t base;

      /**
       * GCC generates a call to udivdi3 for 64-bit
       * division, if we directly do (sum * 100)/(sum + idle)
       */

      quotient = sum * 100;
      base     = sum + idle;

      do_div(quotient, base);

      *avg_load = (unsigned char)quotient;
   }
}


void al_get_gps_info(AL_CONTEXT_PARAM_DECL char *longitude, char *latitude, char *altitude, char *speed)
{
   meshap_get_gps_info(longitude, latitude, altitude, speed);
}


int al_atoi(AL_CONTEXT_PARAM_DECL char *string)
{
   return simple_strtol(string, NULL, 10);
}


void al_restart_mesh(AL_CONTEXT_PARAM_DECL_SINGLE)
{
   meshap_core_stop_mesh();
   al_thread_sleep(AL_CONTEXT 10000);
   meshap_core_start_mesh();
}


static int _watchdog_thread_function(AL_CONTEXT_PARAM_DECL al_thread_param_t *param)
{
   int *watchdog_thread_quit_ptr;
   _meshap_al_watchdog_t *watchdog;

   watchdog_thread_quit_ptr = (int *)param->param;

   al_set_thread_name(AL_CONTEXT "md_watch");
   printk(KERN_INFO "md_watch : Starting\n");

   while (1)
   {
      //al_thread_sleep(AL_CONTEXT 2000);
	  thread_stat.md_watch_thread_count++;
      al_thread_sleep(AL_CONTEXT 20000);

      if (*watchdog_thread_quit_ptr)
      {
         break;
      }
	  if(reboot_in_progress) {
		  printk(KERN_EMERG"-----Terminating 'md_watch' thread-----\n");
		  threads_status &= ~THREAD_STATUS_BIT4_MASK;
		  break;
	  }
      al_strobe_reset_generator(AL_CONTEXT_SINGLE);

      al_wait_on_semaphore(AL_CONTEXT _watchdog_semaphore, AL_WAIT_TIMEOUT_INFINITE);

      watchdog = _watchdog_head;

      while (watchdog != NULL)
      {
         if ((jiffies - watchdog->last_heard > ((watchdog->timeout_in_millis * HZ) / 1000)) &&
             !watchdog->suspended)
         {
            printk(KERN_INFO "\nmd_watch : Thread %s caught napping...", watchdog->thread_name);
            if (watchdog->routine != NULL)
            {
               watchdog->routine();
            }
            else
            {
				printk(KERN_INFO "i222222md_watch : Starting\n");
               al_reboot_board(AL_CONTEXT AL_REBOOT_BOARD_CODE_AL_WATCHDOG_THREAD);
            }
         }
         watchdog = watchdog->next;
      }

      al_release_semaphore(AL_CONTEXT _watchdog_semaphore, 1);
   }

   kfree(watchdog_thread_quit_ptr);
   al_set_event(AL_CONTEXT _watchdog_quit_event);
   printk(KERN_INFO "md_watch : Quitting\n");

   return 0;
}


int al_create_watchdog(AL_CONTEXT_PARAM_DECL const char *thread_name, int timeout_in_millis, al_watchdog_routine_t routine)
{
   _meshap_al_watchdog_t *watchdog;

   watchdog = (_meshap_al_watchdog_t *)kmalloc(sizeof(_meshap_al_watchdog_t), GFP_ATOMIC);

   if (watchdog == NULL)
   {
      return 0;
   }

   memset(watchdog, 0, sizeof(_meshap_al_watchdog_t));

   watchdog->routine           = routine;
   watchdog->timeout_in_millis = timeout_in_millis;
   watchdog->last_heard        = jiffies;
   strcpy(watchdog->thread_name, thread_name);

   al_wait_on_semaphore(AL_CONTEXT _watchdog_semaphore, AL_WAIT_TIMEOUT_INFINITE);

   watchdog->next = _watchdog_head;
   if (_watchdog_head != NULL)
   {
      _watchdog_head->prev = watchdog;
   }
   _watchdog_head = watchdog;

   al_release_semaphore(AL_CONTEXT _watchdog_semaphore, 1);

   printk(KERN_INFO "md_watch : Created watch dog for thread %s\n", watchdog->thread_name);

   return (int)watchdog;
}


void al_destroy_watchdog(AL_CONTEXT_PARAM_DECL int watchdog_id)
{
   _meshap_al_watchdog_t *watchdog;

   watchdog = (_meshap_al_watchdog_t *)watchdog_id;

   al_wait_on_semaphore(AL_CONTEXT _watchdog_semaphore, AL_WAIT_TIMEOUT_INFINITE);

   if (watchdog->prev)
   {
      watchdog->prev->next = watchdog->next;
   }
   if (watchdog->next)
   {
      watchdog->next->prev = watchdog->prev;
   }
   if (watchdog == _watchdog_head)
   {
      _watchdog_head = _watchdog_head->next;
   }

   al_release_semaphore(AL_CONTEXT _watchdog_semaphore, 1);

   printk(KERN_INFO "md_watch : Destroying watch dog for thread %s\n", watchdog->thread_name);

   kfree(watchdog);
}


void al_update_watchdog(AL_CONTEXT_PARAM_DECL int watchdog_id)
{
   _meshap_al_watchdog_t *watchdog;

   watchdog = (_meshap_al_watchdog_t *)watchdog_id;

   al_wait_on_semaphore(AL_CONTEXT _watchdog_semaphore, AL_WAIT_TIMEOUT_INFINITE);

   watchdog->last_heard = jiffies;

   al_release_semaphore(AL_CONTEXT _watchdog_semaphore, 1);
}

void al_update_watchdog_ap_thread(AL_CONTEXT_PARAM_DECL int watchdog_id)
{
   _meshap_al_watchdog_t *watchdog;

   watchdog = (_meshap_al_watchdog_t *)watchdog_id;
   if(!watchdog) {
		printk(KERN_EMERG" DETECTED RACE CONDITION %s<%d> REBOOT_IN_PROGRESS:%d MESH_STATE:%d AP_THREAD_DISABLED:%d \n",
		__func__, __LINE__,reboot_in_progress, mesh_state, AL_ATOMIC_GET(globals.ap_thread_disabled));
		return;
   }
   watchdog->last_heard = jiffies;
}


void al_set_watchdog_timeout(AL_CONTEXT_PARAM_DECL int watchdog_id, int timeout_in_millis)
{
   _meshap_al_watchdog_t *watchdog;

   watchdog = (_meshap_al_watchdog_t *)watchdog_id;

   al_wait_on_semaphore(AL_CONTEXT _watchdog_semaphore, AL_WAIT_TIMEOUT_INFINITE);

   watchdog->timeout_in_millis = timeout_in_millis;

   al_release_semaphore(AL_CONTEXT _watchdog_semaphore, 1);
}


void al_suspend_watchdog(AL_CONTEXT_PARAM_DECL int watchdog_id)
{
   _meshap_al_watchdog_t *watchdog;

   watchdog = (_meshap_al_watchdog_t *)watchdog_id;

   al_wait_on_semaphore(AL_CONTEXT _watchdog_semaphore, AL_WAIT_TIMEOUT_INFINITE);

   watchdog->suspended = 1;

   al_release_semaphore(AL_CONTEXT _watchdog_semaphore, 1);

   printk(KERN_INFO "md_watch : Suspended watch dog for thread %s\n", watchdog->thread_name);
}


void al_resume_watchdog(AL_CONTEXT_PARAM_DECL int watchdog_id)
{
   _meshap_al_watchdog_t *watchdog;

   watchdog = (_meshap_al_watchdog_t *)watchdog_id;

   al_wait_on_semaphore(AL_CONTEXT _watchdog_semaphore, AL_WAIT_TIMEOUT_INFINITE);

   watchdog->suspended = 0;

   al_release_semaphore(AL_CONTEXT _watchdog_semaphore, 1);

   printk(KERN_INFO "md_watch : Resumed watch dog for thread %s\n", watchdog->thread_name);
}


unsigned char *al_get_gen_2KB_buffer(AL_CONTEXT_PARAM_DECL_SINGLE)
{
   _meshap_al_gen_2KB_buffer_pool_t *temp;
   unsigned long flags;

   temp = NULL;

   al_spin_lock(buf_2k_splock, buf_2k_spvar);

   if (_buffer_pool_head != NULL)
   {
      temp = _buffer_pool_head;
      _buffer_pool_head = _buffer_pool_head->next_pool;

      --_buffer_pool_count;

      if (_buffer_pool_count < _buffer_pool_count_min)
      {
         _buffer_pool_count_min = _buffer_pool_count;
      }
   }

   al_spin_unlock(buf_2k_splock, buf_2k_spvar);

   if (temp == NULL)
   {
      temp       = (_meshap_al_gen_2KB_buffer_pool_t *)al_heap_alloc(AL_CONTEXT sizeof(_meshap_al_gen_2KB_buffer_pool_t)AL_HEAP_DEBUG_PARAM);
      temp->type = _MESHAP_AL_GEN_2KB_TYPE_HEAP;
      ++_buffer_pool_count_heap_alloc;
   }

   memset(temp->buffer, 0, 2048);

   return temp->buffer;
}


void al_release_gen_2KB_buffer(AL_CONTEXT_PARAM_DECL unsigned char *buffer)
{
   unsigned long flags;
   _meshap_al_gen_2KB_buffer_pool_t *temp;

   temp = (_meshap_al_gen_2KB_buffer_pool_t *)buffer;

   if (temp->type == _MESHAP_AL_GEN_2KB_TYPE_HEAP)
   {
      al_heap_free(AL_CONTEXT temp);
      ++_buffer_pool_count_heap_free;
      return;
   }

   al_spin_lock(buf_2k_splock, buf_2k_spvar);
   temp->next_pool   = _buffer_pool_head;
   _buffer_pool_head = temp;
   ++_buffer_pool_count;
   al_spin_unlock(buf_2k_splock, buf_2k_spvar);
}


void al_get_packet_pool_info(AL_CONTEXT_PARAM_DECL al_packet_pool_info_t *info)
{
   info->gen_2k_max        = _MESHAP_AL_GEN_2KB_MAX_COUNT;
   info->gen_2k_current    = _buffer_pool_count;
   info->gen_2k_min        = _buffer_pool_count_min;
   info->gen_2k_heap_alloc = _buffer_pool_count_heap_alloc;
   info->gen_2k_heap_free  = _buffer_pool_count_heap_alloc;

   al_packet_impl_get_pool_info(info);

   meshap_ip_device_get_info(info);
}


void al_get_gen_2KB_buffer_pool_info(AL_CONTEXT_PARAM_DECL int *cur, int *min, int *total_heap_alloc, int *total_heap_free)
{
   if (cur != NULL)
   {
      *cur = _buffer_pool_count;
   }
   if (min != NULL)
   {
      *min = _buffer_pool_count_min;
   }
   if (total_heap_alloc != NULL)
   {
      *total_heap_alloc = _buffer_pool_count_heap_alloc;
   }
   if (total_heap_free != NULL)
   {
      *total_heap_free = _buffer_pool_count_heap_free;
   }
}


void al_initialize(AL_CONTEXT_PARAM_DECL_SINGLE)
{
   _meshap_al_gen_2KB_buffer_pool_t *temp_buffer;
   unsigned long flags;
   int           i;

   _watchdog_head             = NULL;
   _watchdog_semaphore        = al_create_semaphore(AL_CONTEXT 1, 1);
   _watchdog_quit_event       = al_create_event(AL_CONTEXT 1);
   _watchdog_thread_quit_ptr  = (int *)kmalloc(sizeof(_meshap_al_watchdog_t), GFP_ATOMIC);
   *_watchdog_thread_quit_ptr = 0;
   _last_jiffies = 0;

   al_spin_lock_init(&buf_2k_splock);

   al_spin_lock(buf_2k_splock, buf_2k_spvar);

   temp_buffer       = NULL;
   _buffer_pool_head = NULL;

   _buffer_pool = (_meshap_al_gen_2KB_buffer_pool_t *)al_heap_alloc(AL_CONTEXT sizeof(_meshap_al_gen_2KB_buffer_pool_t) * _MESHAP_AL_GEN_2KB_MAX_COUNT AL_HEAP_DEBUG_PARAM);

   for (i = 0; i < _MESHAP_AL_GEN_2KB_MAX_COUNT; i++)
   {
      _buffer_pool[i].next_pool = NULL;
      _buffer_pool[i].type      = _MESHAP_AL_GEN_2KB_TYPE_POOL;

      if (_buffer_pool_head == NULL)
      {
         _buffer_pool_head = &_buffer_pool[i];
      }
      else
      {
         temp_buffer->next_pool = &_buffer_pool[i];
      }

      temp_buffer = &_buffer_pool[i];
   }

   _buffer_pool_count            = _MESHAP_AL_GEN_2KB_MAX_COUNT;
   _buffer_pool_count_min        = _MESHAP_AL_GEN_2KB_MAX_COUNT;
   _buffer_pool_count_heap_alloc = 0;
   _buffer_pool_count_heap_free  = 0;

   al_spin_unlock(buf_2k_splock, buf_2k_spvar);

   al_create_thread(AL_CONTEXT _watchdog_thread_function, (void *)_watchdog_thread_quit_ptr, AL_THREAD_PRIORITY_NORMAL, "_watchdog_thread_function");

   threads_status |= THREAD_STATUS_BIT4_MASK;
}


void al_uninitialize(AL_CONTEXT_PARAM_DECL_SINGLE)
{
   _meshap_al_watchdog_t *watchdog;
   _meshap_al_watchdog_t *temp;
   unsigned long         flags;

   al_spin_lock(buf_2k_splock, buf_2k_spvar);
   al_heap_free(AL_CONTEXT _buffer_pool);
   _buffer_pool_head = NULL;
   _buffer_pool      = NULL;
   al_spin_unlock(buf_2k_splock, buf_2k_spvar);

   al_reset_event(AL_CONTEXT _watchdog_quit_event);
   *_watchdog_thread_quit_ptr = 1;
   al_wait_on_event(AL_CONTEXT _watchdog_quit_event, AL_WAIT_TIMEOUT_INFINITE);
   al_destroy_event(AL_CONTEXT _watchdog_quit_event);

   al_wait_on_semaphore(AL_CONTEXT _watchdog_semaphore, AL_WAIT_TIMEOUT_INFINITE);

   watchdog = _watchdog_head;

   while (watchdog)
   {
      temp = watchdog->next;
      kfree(watchdog);
      watchdog = temp;
   }

   al_release_semaphore(AL_CONTEXT _watchdog_semaphore, 1);

   al_destroy_event(AL_CONTEXT _watchdog_semaphore);
}


int al_get_sip_conf_handle(AL_CONTEXT_PARAM_DECL_SINGLE)
{
   sip_conf_handle_t ret;

   ret = NULL;
   if (meshap_core_globals.sip_config_string != NULL)
   {
      ret = sip_conf_parse(AL_CONTEXT meshap_core_globals.sip_config_string);
   }

   return (int)ret;
}


/*  prototype to check the phy mode type as N OR AC or both
 * @function : cheks the interface physical mode and returns the value
 * input arguments : reference to phy_mode
 * return value : 1-only N ; 2-only AC ; 3-both N & AC ; 0-none 
 * */
int meshap_is_N_or_AC_supported_interface(int phy_mode)
{

    switch (phy_mode){
        case AL_CONF_IF_PHY_SUB_TYPE_802_11_N_2_4GHZ:
        case AL_CONF_IF_PHY_SUB_TYPE_802_11_N_5GHZ:
        case AL_CONF_IF_PHY_SUB_TYPE_802_11_AN:
        case AL_CONF_IF_PHY_SUB_TYPE_802_11_BGN:
            return AL_PHY_SUB_TYPE_SUPPORT_ONLY_N ;

        case AL_CONF_IF_PHY_SUB_TYPE_802_11_AC:
          //  return AL_PHY_SUB_TYPE_SUPPORT_ONLY_AC ;
            return AL_PHY_SUB_TYPE_SUPPORT_BOTH_N_AND_AC ;


        case AL_CONF_IF_PHY_SUB_TYPE_802_11_ANAC:
            return AL_PHY_SUB_TYPE_SUPPORT_BOTH_N_AND_AC ;

        default : 
            return AL_PHY_SUB_TYPE_NOT_SUPPORT_N_OR_AC ;
    }

}
