/********************************************************************************
* MeshDynamics
* --------------
* File     : meshap_version_proc.h
* Comments : Meshap /proc FS Version Info Content
* Created  : 12/26/2004
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  0  |12/26/2004| Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

#ifndef __MESHAP_VERSION_PROC_H__
#define __MESHAP_VERSION_PROC_H__

void meshap_version_proc_initialize(void);

#endif /*__MESHAP_VERSION_PROC_H__*/
