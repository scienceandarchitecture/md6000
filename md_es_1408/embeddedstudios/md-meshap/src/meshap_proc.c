/********************************************************************************
* MeshDynamics
* --------------
* File     : meshap_proc.c
* Comments : Meshap /proc FS manager
* Created  : 12/26/2004
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  1  |6/18/2005 | Added meshap_proc_mkdir                         | Sriram |
* -----------------------------------------------------------------------------
* |  0  |12/26/2004| Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

#include <generated/autoconf.h>
#include <linux/version.h>
#include <linux/module.h>
#include <linux/init.h>
#include <linux/if.h>
#include <linux/netdevice.h>
#include <linux/skbuff.h>
#include <linux/cache.h>
#include <linux/pci.h>
#include <linux/wireless.h>
#include <asm/uaccess.h>
#include <net/iw_handler.h>
#include <linux/delay.h>

#include "al.h"
#include "meshap_proc.h"

static struct proc_dir_entry *_meshap_proc_entry = NULL;

int meshap_proc_initialize()
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__); 

   _meshap_proc_entry = proc_mkdir("meshap", init_net.proc_net);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESH_AP : FLOW : Enter: %s<%d>\n",__func__,__LINE__);
   return 0;
}


void meshap_proc_add_read_entry(const char *name, struct file_operations *proc_file_fops, void *data)
{
   proc_create_data(name, 0, _meshap_proc_entry, proc_file_fops, data);
}


struct proc_dir_entry *meshap_proc_mkdir(const char *name)
{
   return proc_mkdir(name, _meshap_proc_entry);
}
