/********************************************************************************
* MeshDynamics
* --------------
* File     : meshap_sdk_proc.c
* Comments : Meshap /proc FS version information
* Created  : 01/23/2007
* Author   : Abhijit Ayarekar
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  0  |01/23/2007| Created.                                        | Abhijit|
* -----------------------------------------------------------------------------
********************************************************************************/

#include <generated/autoconf.h>
#include <linux/version.h>
#include <linux/module.h>
#include <linux/init.h>
#include <linux/if.h>
#include <linux/netdevice.h>
#include <linux/skbuff.h>
#include <linux/cache.h>
#include <linux/pci.h>
#include <linux/wireless.h>
#include <asm/uaccess.h>
#include <net/iw_handler.h>
#include <linux/delay.h>
#include "al.h"
#include "al_impl_context.h"
#include "al_802_11.h"
#include "meshap_proc.h"
#include "access_point.h"
#include "access_point_ext.h"

#include "meshap_sdk_proc.h"


struct _sta_list_data
{
   char *p;
};

typedef struct _sta_list_data   _sta_list_data_t;

static struct proc_dir_entry *_meshap_sdk_proc_entry = NULL;

static void _meshap_sdk_proc_sta_initialize(void);

//static int		_sta_list_routine						(char *page, char **start, off_t off,int count, int *eof, void *data);
static int _sta_list_routine(struct seq_file *m, void *v);
static void _sta_enum_routine(void *callback, const access_point_sta_info_t *sta);


int meshap_sdk_proc_initialize(void)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
   _meshap_sdk_proc_entry = meshap_proc_mkdir("sdk");
   _meshap_sdk_proc_sta_initialize();

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
   return 0;
}


void meshap_sdk_proc_add_read_entry(const char *name, struct file_operations *fops, void *data)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
   proc_create_data(name, 0, _meshap_sdk_proc_entry, fops, data);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
   /*TBD -S
    * create_proc_read_entry(name,0,_meshap_sdk_proc_entry,routine,data);
    * TBD -E */
}


//static int _sta_list_routine(char *page, char **start, off_t off,int count, int *eof, void *data)
static int _sta_list_routine(struct seq_file *m, void *v)
{

   seq_printf(m,
              "--------------------------------------------------------------\r\n"
              "STATION ADDR     |IFNAME|VLAN TAG|11b|SIG|RATE|LAST PACKET RX |\r\n"
              "--------------------------------------------------------------\r\n");

   access_point_ext_enumerate_stas((void *)m, _sta_enum_routine);

   return 0;
}


static void _sta_enum_routine(void *callback, const access_point_sta_info_t *sta)
{
   struct seq_file *m;
   al_u64_t        tc;

   m  = (struct seq_file *)callback;
   tc = al_get_tick_count(AL_CONTEXT_SINGLE);

   seq_printf(m,
              AL_NET_ADDR_STR "|%6s|%08d|%3s|%03d|%04d|%014d|\r\n",
              AL_NET_ADDR_TO_STR(&sta->address),
              sta->net_if->name,
              sta->vlan_info->vlan_tag,
              sta->b_client ? "Yes" : "No ",
              sta->rssi,
              sta->transmit_bit_rate,
              (int)(tc - sta->last_packet_received));
}


static int _sta_list_proc_open(struct inode *inode, struct file *file)
{
   return single_open(file, _sta_list_routine, NULL);
}


static struct file_operations _sta_list_fops =
{
   .owner   = THIS_MODULE,
   .open    = _sta_list_proc_open,
   .read    = seq_read,
   .llseek  = seq_lseek,
   .release = single_release,
};

static void _meshap_sdk_proc_sta_initialize(void)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Enter %s : %d\n", __func__,__LINE__);
   meshap_sdk_proc_add_read_entry("sta-list", &_sta_list_fops, NULL);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"MESHAP : FLOW : Exit %s : %d\n", __func__,__LINE__);
}
