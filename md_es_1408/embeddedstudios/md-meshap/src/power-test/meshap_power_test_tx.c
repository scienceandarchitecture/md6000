/********************************************************************************
* MeshDynamics
* --------------
* File     : meshap_power_test_tx.c
* Comments : Automatic power test transmit
* Created  : 7/16/2006
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  3  |4/11/2007 | Changes for Revision 1.3                        | Sriram |
* -----------------------------------------------------------------------------
* |  2  |11/14/2006| Changes for heap debug support                  | Sriram |
* -----------------------------------------------------------------------------
* |  1  |9/19/2006 | Bug Fixed                                       | Sriram |
* -----------------------------------------------------------------------------
* |  0  |7/16/2006 | Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

extern int reboot_in_progress;
extern int threads_status;
extern struct thread_status thread_stat;

/**
 * This is a private source file included by meshap_power_test.c, hence
 * It was created to improve readability.
 */
static int _transmit_thread_function(AL_CONTEXT_PARAM_DECL al_thread_param_t *param)
{
   unsigned char            *buffer;
   int                      data_length;
   meshap_power_test_data_t data;
   int                      i;

   al_set_thread_name(AL_CONTEXT "dBmTestTx");

_back_to_wait:

   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,
                "meshap_power_test: dBmTestTx thread waiting...\n");

   thread_stat.dBmTestTx_thread_count++;
   al_wait_on_event(AL_CONTEXT _state.tx_start_event, AL_WAIT_TIMEOUT_INFINITE);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,
                "meshap_power_test: dBmTestTx thread starting...\n");

   buffer = (unsigned char *)al_heap_alloc(1024 AL_HEAP_DEBUG_PARAM);
   memset(buffer, 0, 1024);

   while (1)
   {
	   if(reboot_in_progress) {
		   printk(KERN_EMERG"-----Terminating 'dBmTestTx' thread-----\n");
		   threads_status &= ~THREAD_STATUS_BIT13_MASK;
		   break;
	   }
      if (_state.tx_delay == 0)
      {
         al_thread_sleep(AL_CONTEXT 200);
      }
      else
      {
         al_thread_sleep(AL_CONTEXT _state.tx_delay * 20);
      }

      /**
       * If the test was stopped we go back to wait
       */

      if (!_state.started)
      {
         goto _back_to_wait;
      }

      for (i = 0; i < 4; i++)
      {
         if ((_state.radio_devices[i] == NULL) || !_state.radio_devices[i]->initialized)
         {
            continue;
         }

         al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,
                      "meshap_power_test: TX DUT data on %s to %02X:%02X:%02X:%02X:%02X:%02X\n",
                      _state.radio_devices[i]->name,
                      _state.radio_devices[i]->peer_mac[0],
                      _state.radio_devices[i]->peer_mac[1],
                      _state.radio_devices[i]->peer_mac[2],
                      _state.radio_devices[i]->peer_mac[3],
                      _state.radio_devices[i]->peer_mac[4],
                      _state.radio_devices[i]->peer_mac[5]);

         data.type = MESHAP_POWER_TEST_DATA_TYPE_DUT_DATA;
         data.data.dut_data.rx_signal = _state.radio_devices[i]->signal;

         meshap_power_test_data_setup_buffer(&data, buffer, &data_length);

         meshap_power_test_transmit(_state.radio_devices[i],
                                    _state.radio_devices[i]->peer_mac,
                                    _state.radio_devices[i]->mac_address,
                                    _state.radio_devices[i]->rate_index,
                                    buffer,
                                    data_length);
      }
   }

   return 0;
}
