/********************************************************************************
* MeshDynamics
* --------------
* File     : meshap_power_test_data.h
* Comments : Automatic power testing data
* Created  : 7/15/2006
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  2  |4/11/2007 | Changes for Revision 1.3                        | Sriram |
* -----------------------------------------------------------------------------
* |  1  |2/26/2007 | Changes for Revision 1.2                        | Sriram |
* -----------------------------------------------------------------------------
* |  0  |7/15/2006 | Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

#ifndef __MESHAP_POWER_TEST_DATA_H__
#define __MESHAP_POWER_TEST_DATA_H__

#define MESHAP_POWER_TEST_DATA_TYPE_START         1
#define MESHAP_POWER_TEST_DATA_TYPE_DUT_DATA      2
#define MESHAP_POWER_TEST_DATA_TYPE_REF_DATA      3
#define MESHAP_POWER_TEST_DATA_TYPE_REF_REPORT    4
#define MESHAP_POWER_TEST_DATA_TYPE_STOP          5

#define MESHAP_POWER_TEST_MODE_NONE               0
#define MESHAP_POWER_TEST_MODE_REFERENCE          1
#define MESHAP_POWER_TEST_MODE_DUT                2

struct meshap_power_test_data
{
   unsigned char type;

   union
   {
      struct
      {
         unsigned char test_mode;                                       /** 1 = Test Reference, 2 = DUT */
         unsigned char radio_phy_modes[4];
         unsigned char radio_modes[4];
         unsigned char peer_mac[6];                             /** First address  */
         /** Added in Rev 1.2 */
         unsigned char radio_channels[4];
         /** Added in Rev 1.3 */
         unsigned char rate_indices[4];
         unsigned char inter_delay;                             /** In multiples of 20 ms e.g. 0 = 250 ms 1 = 20 ms, 100 = 2000 ms */
      }
      test_start;

      struct
      {
         unsigned char rx_signal;
      }
      dut_data;

      struct
      {
         unsigned char change_channel;                          /** 1 to change */
      }
      ref_data;

      struct
      {
         char          interface_name[33];
         unsigned char tx_signal;
         unsigned char rx_signal;
         unsigned char channel;
         /** Added in Rev 1.3 */
         unsigned char tx_rate;
      }
      ref_report;
   }
                 data;
};

typedef struct meshap_power_test_data   meshap_power_test_data_t;

int meshap_power_test_data_process_buffer(unsigned char *buffer_in, int length_in, meshap_power_test_data_t *test_data_out);
int meshap_power_test_data_setup_buffer(meshap_power_test_data_t *test_data_in, unsigned char *buffer_out, int *length_out);

#endif /*__MESHAP_POWER_TEST_H__*/
