/********************************************************************************
* MeshDynamics
* --------------
* File     : meshap_power_test_data.c
* Comments : Automatic power test data processing
* Created  : 7/15/2006
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  2  |4/11/2007 | Changes for Revision 1.3                        | Sriram |
* -----------------------------------------------------------------------------
* |  1  |2/26/2007 | Changes for Revision 1.2                        | Sriram |
* -----------------------------------------------------------------------------
* |  0  |7/15/2006 | Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

#include <linux/string.h>
#include "meshap_power_test_data.h"

static const unsigned char _signature[] =
{
   'M', 'D', 'T', 'E', 'S', 'T', 'M', 'D'
};

int meshap_power_test_data_process_buffer(unsigned char            *buffer_in,
                                          int                      length_in,
                                          meshap_power_test_data_t *test_data_out)
{
   unsigned char *p;
   int           length;

   memset(test_data_out, 0, sizeof(meshap_power_test_data_t));

   p = buffer_in;

   if (memcmp(p, _signature, 8))
   {
      return -1;
   }

   p += 8;

   test_data_out->type = *p++;

   switch (test_data_out->type)
   {
   case MESHAP_POWER_TEST_DATA_TYPE_START:
      test_data_out->data.test_start.test_mode = *p++;
      memcpy(test_data_out->data.test_start.radio_phy_modes, p, 4);
      p += 4;
      memcpy(test_data_out->data.test_start.radio_modes, p, 4);
      p += 4;
      memcpy(test_data_out->data.test_start.peer_mac, p, 6);
      p += 6;
      /** Rev 1.2 */
      if (length_in > 24)
      {
         memcpy(test_data_out->data.test_start.radio_channels, p, 4);
         p += 4;
      }
      /** Rev 1.3 */
      if (length_in > 28)
      {
         memcpy(test_data_out->data.test_start.rate_indices, p, 4);
         p += 4;
         test_data_out->data.test_start.inter_delay = *p++;
      }
      break;

   case MESHAP_POWER_TEST_DATA_TYPE_DUT_DATA:
      test_data_out->data.dut_data.rx_signal = *p++;
      break;

   case MESHAP_POWER_TEST_DATA_TYPE_REF_DATA:
      test_data_out->data.ref_data.change_channel = *p++;
      break;

   case MESHAP_POWER_TEST_DATA_TYPE_REF_REPORT:
      length = *p++;
      memcpy(test_data_out->data.ref_report.interface_name, p, length);
      p += length;
      test_data_out->data.ref_report.interface_name[length] = 0;
      test_data_out->data.ref_report.tx_signal = *p++;
      test_data_out->data.ref_report.rx_signal = *p++;
      test_data_out->data.ref_report.channel   = *p++;
      /** Rev 1.3 */
      if (length_in - length > 13)
      {
         test_data_out->data.ref_report.tx_rate = *p++;
      }
      break;

   /** Rev 1.3 */
   case MESHAP_POWER_TEST_DATA_TYPE_STOP:
      /** No data for Stopping test */
      break;

   default:
      return -1;
   }

   return 0;
}


int meshap_power_test_data_setup_buffer(meshap_power_test_data_t *test_data_in,
                                        unsigned char            *buffer_out,
                                        int                      *length_out)
{
   int           length;
   unsigned char *p;

   p           = buffer_out;
   *length_out = 0;

   memcpy(p, _signature, 8);
   p += 8;

   *p++ = test_data_in->type;

   switch (test_data_in->type)
   {
   case MESHAP_POWER_TEST_DATA_TYPE_START:
      *p++ = test_data_in->data.test_start.test_mode;
      memcpy(p, test_data_in->data.test_start.radio_phy_modes, 4);
      p += 4;
      memcpy(p, test_data_in->data.test_start.radio_modes, 4);
      p += 4;
      memcpy(p, test_data_in->data.test_start.peer_mac, 6);
      p += 6;
      memcpy(p, test_data_in->data.test_start.radio_channels, 4);
      p += 4;
      memcpy(p, test_data_in->data.test_start.rate_indices, 4);
      p   += 4;
      *p++ = test_data_in->data.test_start.inter_delay;
      break;

   case MESHAP_POWER_TEST_DATA_TYPE_DUT_DATA:
      *p++ = test_data_in->data.dut_data.rx_signal;
      break;

   case MESHAP_POWER_TEST_DATA_TYPE_REF_DATA:
      *p++ = test_data_in->data.ref_data.change_channel;
      break;

   case MESHAP_POWER_TEST_DATA_TYPE_REF_REPORT:
      length = strlen(test_data_in->data.ref_report.interface_name);
      *p++   = length;
      memcpy(p, test_data_in->data.ref_report.interface_name, length);
      p   += length;
      *p++ = test_data_in->data.ref_report.tx_signal;
      *p++ = test_data_in->data.ref_report.rx_signal;
      *p++ = test_data_in->data.ref_report.channel;
      *p++ = test_data_in->data.ref_report.tx_rate;
      break;

   case MESHAP_POWER_TEST_DATA_TYPE_STOP:
      /** No data for Stopping test */
      break;

   default:
      return -1;
   }

   *length_out = (p - buffer_out);

   return 0;
}
