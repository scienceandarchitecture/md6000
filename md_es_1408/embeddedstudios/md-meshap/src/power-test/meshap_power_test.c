/********************************************************************************
* MeshDynamics
* --------------
* File     : meshap_power_test.c
* Comments : Automatic power test
* Created  : 7/15/2006
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  4  |4/11/2007 | Changes for Revision 1.3                        | Sriram |
* -----------------------------------------------------------------------------
* |  3  |2/26/2007 | Changes for Revision 1.2                        | Sriram |
* -----------------------------------------------------------------------------
* |  2  |11/14/2006| Changes for heap debug support                  | Sriram |
* -----------------------------------------------------------------------------
* |  1  |9/19/2006 | Bug Fixed                                       | Sriram |
* -----------------------------------------------------------------------------
* |  0  |7/15/2006 | Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

#include "al.h"
#include <linux/string.h>
#include <linux/kernel.h>
#include <linux/preempt.h>
#include "meshap_power_test.h"
#include "meshap_power_test_impl.h"
#include "meshap_power_test_data.h"

struct _test_data_queue
{
   unsigned char              src_mac[6];
   unsigned char              dst_mac[6];
   unsigned int               signal;
   unsigned int               tx_rate;
   meshap_power_test_data_t   data;
   meshap_power_test_device_t *device;
   struct _test_data_queue    *next;
};

typedef struct _test_data_queue   _test_data_queue_t;

typedef struct _test_state
{
   unsigned char              started;
   unsigned char              test_mode;
   unsigned char              peer_mac[6];
   int                        tx_delay;
   meshap_power_test_device_t *start_device;
   meshap_power_test_device_t *radio_devices[4];
   int                        tx_start_event;
   int                        receiver_event;
   _test_data_queue_t         *queue_head;
   _test_data_queue_t         *queue_tail;
} _test_state_t;

static _test_state_t _state;

static AL_INLINE int _start_power_test(meshap_power_test_device_t *device_in,
                                       meshap_power_test_data_t   *data,
                                       unsigned char              *dst_address);

static AL_INLINE void _stop_power_test(meshap_power_test_device_t *device_in);

static int _receiver_thread_function(AL_CONTEXT_PARAM_DECL al_thread_param_t *param);
static int _transmit_thread_function(AL_CONTEXT_PARAM_DECL al_thread_param_t *param);

static const int _dot11AChannels[] = { 149, 157, 52, 165 };
static const int _dot11BChannels[] = { 1, 6, 11, 4 };

void meshap_power_test_initialize(void)
{
   memset(&_state, 0, sizeof(_test_state_t));

   _state.started        = 0;
   _state.test_mode      = MESHAP_POWER_TEST_MODE_NONE;
   _state.tx_start_event = al_create_event(AL_CONTEXT 1);
   _state.receiver_event = al_create_event(AL_CONTEXT 1);

   memset(_state.peer_mac, 0, sizeof(_state.peer_mac));

   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "meshap_power_test: Initialized\n");
}


void meshap_power_test_uninitialize(void)
{
   _state.started   = 0;
   _state.test_mode = MESHAP_POWER_TEST_MODE_NONE;
   memset(_state.peer_mac, 0, sizeof(_state.peer_mac));
}


meshap_power_test_device_t *meshap_power_test_create_device(const char    *name,
                                                            int           type,
                                                            unsigned char *mac_address,
                                                            void          *impl_data)
{
   meshap_power_test_device_t *data;
   int radio_index;

   data = (meshap_power_test_device_t *)al_heap_alloc(AL_CONTEXT sizeof(meshap_power_test_device_t)AL_HEAP_DEBUG_PARAM);
   memset(data, 0, sizeof(meshap_power_test_device_t));

   memcpy(data->mac_address, mac_address, 6);
   data->impl_data = impl_data;
   data->type      = type;
   strcpy(data->name, name);

   radio_index = -1;

   if (!strcmp(name, "wlan0"))
   {
      radio_index = 0;
   }
   else if (!strcmp(name, "wlan1"))
   {
      radio_index = 1;
   }
   else if (!strcmp(name, "wlan2"))
   {
      radio_index = 2;
   }
   else if (!strcmp(name, "wlan3"))
   {
      radio_index = 3;
   }

   if ((data->type == MESHAP_POWER_TEST_DEVICE_TYPE_802_11) &&
       (radio_index >= 0) && (radio_index <= 3) &&
       (_state.radio_devices[radio_index] == NULL))
   {
      _state.radio_devices[radio_index] = data;
      _state.radio_devices[radio_index]->initialized = 0;
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "meshap_power_test: Added radio device %s at index %d\n", name, radio_index);
   }
   else
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "meshap_power_test: Added device %s\n", name);
   }

   return data;
}


int meshap_power_test_process(meshap_power_test_device_t *device_in,
                              unsigned char              *dst_address,
                              unsigned char              *src_address,
                              unsigned int               signal,
                              unsigned int               tx_rate,
                              unsigned char              *buffer_in,
                              int                        length_in)
{
   _test_data_queue_t *queue_item;
   int                ret;
   unsigned long      flags;

   queue_item = (_test_data_queue_t *)al_heap_alloc(AL_CONTEXT sizeof(_test_data_queue_t)AL_HEAP_DEBUG_PARAM);
   memset(queue_item, 0, sizeof(_test_data_queue_t));

   ret = meshap_power_test_data_process_buffer(buffer_in, length_in, &queue_item->data);

   if (ret != 0)
   {
      al_heap_free(AL_CONTEXT queue_item);
      return MESHAP_POWER_TEST_PROCESS_IGNORE;
   }

   queue_item->device  = device_in;
   queue_item->signal  = signal;
   queue_item->tx_rate = tx_rate;

   memcpy(queue_item->src_mac, src_address, 6);
   memcpy(queue_item->dst_mac, dst_address, 6);

   ret = MESHAP_POWER_TEST_PROCESS_DROP;

   if (queue_item->data.type == MESHAP_POWER_TEST_DATA_TYPE_START)
   {
      ret = _start_power_test(device_in, &queue_item->data, dst_address);
      al_heap_free(AL_CONTEXT queue_item);
   }
   else if (queue_item->data.type == MESHAP_POWER_TEST_DATA_TYPE_STOP)
   {
      if (_state.started)
      {
         _stop_power_test(device_in);
      }
      al_heap_free(AL_CONTEXT queue_item);
   }
   else
   {
      if (_state.started)
      {
         al_disable_interrupts(flags);

         if (_state.queue_head == NULL)
         {
            _state.queue_head = queue_item;
         }
         else
         {
            _state.queue_tail->next = queue_item;
         }

         _state.queue_tail = queue_item;

         al_enable_interrupts(flags);

         al_set_event(AL_CONTEXT _state.receiver_event);
      }
      else
      {
         al_heap_free(AL_CONTEXT queue_item);
         ret = MESHAP_POWER_TEST_PROCESS_IGNORE;
      }
   }

   return ret;
}


static AL_INLINE void _get_peer_mac(unsigned char *base_mac_in,
                                    int           radio_index_in,
                                    unsigned char *peer_mac_out)
{
   int i;

   memcpy(peer_mac_out, base_mac_in, 6);

   for (i = 0; i < 2 + radio_index_in; i++)
   {
      if (peer_mac_out[5] == 0xFF)
      {
         peer_mac_out[5] = 0;
         if (peer_mac_out[4] == 0xFF)
         {
            peer_mac_out[4] = 0;
            ++peer_mac_out[3];
         }
         else
         {
            ++peer_mac_out[4];
         }
      }
      else
      {
         ++peer_mac_out[5];
      }
   }
}


static AL_INLINE void _stop_power_test(meshap_power_test_device_t *device_in)
{
   if (device_in->type != MESHAP_POWER_TEST_DEVICE_TYPE_ETHERNET)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "meshap_power_test: Ignoring stop packet over non-ethernet device %s\n", device_in->name);
      return;
   }

   _state.started = 0;
}


static AL_INLINE int _start_power_test(meshap_power_test_device_t *device_in,
                                       meshap_power_test_data_t   *data,
                                       unsigned char              *dst_address)
{
   int i;

   if (device_in->type != MESHAP_POWER_TEST_DEVICE_TYPE_ETHERNET)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "meshap_power_test: Ignoring start packet over non-ethernet device %s\n", device_in->name);
      return MESHAP_POWER_TEST_PROCESS_IGNORE;
   }

   if (memcmp(dst_address, device_in->mac_address, 6))
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,
                   "meshap_power_test: Ignoring start packet sent to %02X:%02X:%02X:%02X:%02X:%02X over %s\n",
                   dst_address[0], dst_address[1], dst_address[2], dst_address[3], dst_address[4], dst_address[5],
                   device_in->name);
      return MESHAP_POWER_TEST_PROCESS_IGNORE;
   }

   if (_state.started && (data->data.test_start.test_mode != _state.test_mode))
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,
                   "meshap_power_test: Ignoring start packet when already started due to mode mismatch [New:%d,Curent:%d]\n",
                   data->data.test_start.test_mode, _state.test_mode);
      return MESHAP_POWER_TEST_PROCESS_IGNORE;
   }

   _state.test_mode = data->data.test_start.test_mode;

   if ((_state.test_mode != MESHAP_POWER_TEST_MODE_REFERENCE) &&
       (_state.test_mode != MESHAP_POWER_TEST_MODE_DUT))
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,
                   "meshap_power_test: Ignoring start packet with invalid mode %d\n", _state.test_mode);
      _state.test_mode = MESHAP_POWER_TEST_MODE_NONE;
      return MESHAP_POWER_TEST_PROCESS_IGNORE;
   }

   memcpy(_state.peer_mac, data->data.test_start.peer_mac, 6);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,
                "meshap_power_test: Peer Base MAC is %02X:%02X:%02X:%02X:%02X:%02X\n",
                _state.peer_mac[0],
                _state.peer_mac[1],
                _state.peer_mac[2],
                _state.peer_mac[3],
                _state.peer_mac[4],
                _state.peer_mac[5]);

   _state.tx_delay = data->data.test_start.inter_delay;

   /**
    * We need to initialize the radios to the correct modes
    */

   for (i = 0; i < 4; i++)
   {
      if (data->data.test_start.radio_phy_modes[i] == MESHAP_POWER_TEST_PHY_MODE_NONE)
      {
         if (_state.radio_devices[i] != NULL)
         {
            _state.radio_devices[i]->initialized = 0;
         }

         al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,
                      "meshap_power_test: Device wlan%d will not be used\n", i);

         continue;
      }

      _get_peer_mac(_state.peer_mac, i, _state.radio_devices[i]->peer_mac);

      if (_state.test_mode == MESHAP_POWER_TEST_MODE_REFERENCE)
      {
         if (data->data.test_start.radio_modes[i] == MESHAP_POWER_TEST_OP_MODE_UPLINK)
         {
            data->data.test_start.radio_modes[i] = MESHAP_POWER_TEST_OP_MODE_DOWNLINK;
         }
         else if (data->data.test_start.radio_modes[i] == MESHAP_POWER_TEST_OP_MODE_DOWNLINK)
         {
            data->data.test_start.radio_modes[i] = MESHAP_POWER_TEST_OP_MODE_UPLINK;
         }
      }

      _state.radio_devices[i]->phy_mode = data->data.test_start.radio_phy_modes[i];
      _state.radio_devices[i]->mode     = data->data.test_start.radio_modes[i];

      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,
                   "meshap_power_test: wlan%d used in Phy Mode %d and Op Mode %d Peer %02X:%02X:%02X:%02X:%02X:%02X\n",
                   i,
                   _state.radio_devices[i]->phy_mode,
                   _state.radio_devices[i]->mode,
                   _state.radio_devices[i]->peer_mac[0],
                   _state.radio_devices[i]->peer_mac[1],
                   _state.radio_devices[i]->peer_mac[2],
                   _state.radio_devices[i]->peer_mac[3],
                   _state.radio_devices[i]->peer_mac[4],
                   _state.radio_devices[i]->peer_mac[5]);

      meshap_power_test_init_device(_state.radio_devices[i],
                                    data->data.test_start.radio_phy_modes[i],
                                    data->data.test_start.radio_modes[i]);

      if (data->data.test_start.radio_channels[i] == 0)
      {
         switch (data->data.test_start.radio_phy_modes[i])
         {
         case MESHAP_POWER_TEST_PHY_MODE_A:
            data->data.test_start.radio_channels[i] = _dot11AChannels[i];
            break;

         case MESHAP_POWER_TEST_PHY_MODE_B:
         case MESHAP_POWER_TEST_PHY_MODE_BG:
         case MESHAP_POWER_TEST_PHY_MODE_G:
            data->data.test_start.radio_channels[i] = _dot11BChannels[i];
            break;
         }
      }

      meshap_power_test_set_channel(_state.radio_devices[i], data->data.test_start.radio_channels[i]);
      _state.radio_devices[i]->channel    = data->data.test_start.radio_channels[i];
      _state.radio_devices[i]->rate_index = data->data.test_start.rate_indices[i];

      _state.radio_devices[i]->signal      = 0;
      _state.radio_devices[i]->initialized = 1;
   }

   if (_state.test_mode == MESHAP_POWER_TEST_MODE_DUT)
   {
      al_set_event(AL_CONTEXT _state.tx_start_event);
   }

   _state.start_device = device_in;
   _state.started      = 1;

   return MESHAP_POWER_TEST_PROCESS_DROP;
}


void meshap_power_test_start(void)
{
   al_reset_event(_state.tx_start_event);
   al_reset_event(_state.receiver_event);

   /** Create receiver and transmit threads */

   al_create_thread_on_cpu(AL_CONTEXT _receiver_thread_function, NULL,0, "_receiver_thread_function");
   threads_status |= THREAD_STATUS_BIT12_MASK;
   al_create_thread_on_cpu(AL_CONTEXT _transmit_thread_function, NULL,0, "_transmit_thread_function");
   threads_status |= THREAD_STATUS_BIT13_MASK;
}

void terminate_dBmTestTx_thread(void)
{
	if(_state.tx_start_event)
		al_set_event(AL_CONTEXT _state.tx_start_event);
}
void terminate_dBmTestRx_thread(void)
{
	if(_state.receiver_event)
		al_set_event(AL_CONTEXT _state.receiver_event);
}

#include "meshap_power_test_rx.c"
#include "meshap_power_test_tx.c"
