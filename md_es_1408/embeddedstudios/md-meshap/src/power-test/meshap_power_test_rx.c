/********************************************************************************
* MeshDynamics
* --------------
* File     : meshap_power_test_rx.c
* Comments : Automatic power test receive function
* Created  : 7/16/2006
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  3  |4/11/2007 | Changes for Revision 1.3                        | Sriram |
* -----------------------------------------------------------------------------
* |  2  |2/26/2007 | Revision 1.2 changes                            | Sriram |
* -----------------------------------------------------------------------------
* |  1  |11/14/2006| Changes for heap debug support                  | Sriram |
* -----------------------------------------------------------------------------
* |  0  |7/16/2006 | Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

/**
 * This is a private source file included by meshap_power_test.c, hence
 * It was created to improve readability.
 */
extern int reboot_in_progress;
extern int threads_status;
extern struct thread_status thread_stat;

static AL_INLINE void _process_dut_data(_test_data_queue_t *item)
{
   unsigned char            *buffer;
   int                      data_length;
   meshap_power_test_data_t data;

   if (_state.test_mode != MESHAP_POWER_TEST_MODE_REFERENCE)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,
                   "meshap_power_test: Ignoring DUT Data when not in reference mode\n");
      return;
   }

   if (!item->device->initialized)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,
                   "meshap_power_test: Ignoring DUT Data over un-used device %s\n", item->device->name);
      return;
   }


   if (memcmp(item->src_mac, item->device->peer_mac, 6))
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,
                   "meshap_power_test: Ignoring DUT Data sent by %02X:%02X:%02X:%02X:%02X:%02X over %s\n",
                   item->src_mac[0], item->src_mac[1], item->src_mac[2], item->src_mac[3], item->src_mac[4], item->src_mac[5],
                   item->device->name);
      return;
   }

   buffer = (unsigned char *)al_heap_alloc(1024 AL_HEAP_DEBUG_PARAM);
   memset(buffer, 0, 1024);


   /**
    * Whenever the DUT transmits data, we send back a REF data and a
    * ref report to the console
    */

   data.type = MESHAP_POWER_TEST_DATA_TYPE_REF_DATA;
   data.data.ref_data.change_channel = 0;

   meshap_power_test_data_setup_buffer(&data, buffer, &data_length);

   meshap_power_test_transmit(item->device,
                              item->device->peer_mac,
                              item->device->mac_address,
                              item->device->rate_index,
                              buffer,
                              data_length);

   data.type = MESHAP_POWER_TEST_DATA_TYPE_REF_REPORT;
   data.data.ref_report.channel = item->device->channel;
   strcpy(data.data.ref_report.interface_name, item->device->name);
   data.data.ref_report.rx_signal = item->data.data.dut_data.rx_signal;
   data.data.ref_report.tx_signal = item->signal;
   data.data.ref_report.tx_rate   = item->tx_rate;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,
                "meshap_power_test: Received DUT Data over %s RX:%d RSSI TX:%d RSSI %d Mbps\n",
                item->device->name,
                data.data.ref_report.rx_signal,
                data.data.ref_report.tx_signal,
                data.data.ref_report.tx_rate);

   meshap_power_test_data_setup_buffer(&data, buffer, &data_length);

   meshap_power_test_transmit(_state.start_device,
                              broadcast_net_addr.bytes,
                              _state.start_device->mac_address,
                              0,
                              buffer,
                              data_length);

   al_heap_free(buffer);
}


static AL_INLINE void _process_ref_data(_test_data_queue_t *item)
{
   if (_state.test_mode != MESHAP_POWER_TEST_MODE_DUT)
   {
      return;
   }

   if (memcmp(item->src_mac, item->device->peer_mac, 6))
   {
      return;
   }

   /**
    * Whenever we receive a packet from the reference,
    * we copy the signal stength into our global state
    */

   item->device->signal = item->signal;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,
                "meshap_power_test: Received REF Data over %s RX:%d RSSI\n",
                item->device->name,
                item->signal);
}


static int _receiver_thread_function(AL_CONTEXT_PARAM_DECL al_thread_param_t *param)
{
   unsigned long      flags;
   _test_data_queue_t *item;

   al_set_thread_name(AL_CONTEXT "dBmTestRx");

   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,
                "meshap_power_test: dBmTestRx thread starting...\n", _state.test_mode);


   while (1)
   {
_sleep:
	  thread_stat.dBmTestRx_thread_count++;
      al_wait_on_event(AL_CONTEXT _state.receiver_event, AL_WAIT_TIMEOUT_INFINITE);
_next:
	  if(reboot_in_progress) {
		  printk(KERN_EMERG"-----Terminating 'dBmTestRx' thread-----\n");
		  threads_status &= ~THREAD_STATUS_BIT12_MASK;
		  break;
	  }

      if (_state.queue_head != NULL)
      {
         al_disable_interrupts(flags);
         item = _state.queue_head;
         _state.queue_head = _state.queue_head->next;
         al_enable_interrupts(flags);
      }
      else
      {
         goto _sleep;
      }

      switch (item->data.type)
      {
      case MESHAP_POWER_TEST_DATA_TYPE_DUT_DATA:
         _process_dut_data(item);
         break;

      case MESHAP_POWER_TEST_DATA_TYPE_REF_DATA:
         _process_ref_data(item);
         break;
      }

      al_heap_free(AL_CONTEXT item);
      goto _next;
   }

   return 0;
}
