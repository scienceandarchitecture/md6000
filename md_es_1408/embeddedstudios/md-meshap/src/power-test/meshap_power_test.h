/********************************************************************************
* MeshDynamics
* --------------
* File     : meshap_power_test.h
* Comments : Automatic power test implementation
* Created  : 7/15/2006
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  1  |4/11/2007 | Changes for Revision 1.3                        | Sriram |
* -----------------------------------------------------------------------------
* |  0  |7/15/2006 | Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

#ifndef __MESHAP_POWER_TEST_H__
#define __MESHAP_POWER_TEST_H__

#define MESHAP_POWER_TEST_DEVICE_TYPE_ETHERNET    1
#define MESHAP_POWER_TEST_DEVICE_TYPE_802_11      2

struct meshap_power_test_device
{
   char          name[33];
   int           type;
   unsigned char mac_address[6];
   void          *impl_data;
   unsigned char phy_mode;
   unsigned char mode;
   unsigned char peer_mac[6];
   int           channel;
   int           signal;
   int           rate_index;
   int           initialized;
};

typedef struct meshap_power_test_device   meshap_power_test_device_t;

#define MESHAP_POWER_TEST_PROCESS_IGNORE    0           /** i.e proceed as usual */
#define MESHAP_POWER_TEST_PROCESS_DROP      1           /** Packet dropped */

void meshap_power_test_initialize(void);
void meshap_power_test_uninitialize(void);

void meshap_power_test_start(void);

meshap_power_test_device_t *meshap_power_test_create_device(const char *name, int type, unsigned char *mac_address, void *impl_data);

int meshap_power_test_process(meshap_power_test_device_t *device_in,
                              unsigned char              *dst_address,
                              unsigned char              *src_address,
                              unsigned int               signal,
                              unsigned int               tx_rate,
                              unsigned char              *buffer_in,
                              int                        length_in);

#endif /*__MESHAP_POWER_TEST_H__*/
