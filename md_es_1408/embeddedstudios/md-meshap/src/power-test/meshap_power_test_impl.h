/********************************************************************************
* MeshDynamics
* --------------
* File     : meshap_power_test_impl.h
* Comments : Automatic power test implementation
* Created  : 7/15/2006
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  1  |4/11/2007 | Changes for Revision 1.3                        | Sriram |
* -----------------------------------------------------------------------------
* |  0  |7/15/2006 | Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

#ifndef __MESHAP_POWER_TEST_IMPL_H__
#define __MESHAP_POWER_TEST_IMPL_H__

#define MESHAP_POWER_TEST_PHY_MODE_NONE       0
#define MESHAP_POWER_TEST_PHY_MODE_A          1
#define MESHAP_POWER_TEST_PHY_MODE_B          2
#define MESHAP_POWER_TEST_PHY_MODE_BG         3
#define MESHAP_POWER_TEST_PHY_MODE_G          4

#define MESHAP_POWER_TEST_OP_MODE_NONE        0
#define MESHAP_POWER_TEST_OP_MODE_UPLINK      1
#define MESHAP_POWER_TEST_OP_MODE_DOWNLINK    2

void meshap_power_test_init_device(meshap_power_test_device_t *device, int phy_mode, int mode);

int meshap_power_test_set_channel(meshap_power_test_device_t *device, int channel);

void meshap_power_test_transmit(meshap_power_test_device_t *device,
                                unsigned char              *dst_mac,
                                unsigned char              *src_mac,
                                int                        rate_index,
                                unsigned char              *buffer,
                                int                        length);

#endif /*__MESHAP_POWER_TEST_IMPL_H__*/
