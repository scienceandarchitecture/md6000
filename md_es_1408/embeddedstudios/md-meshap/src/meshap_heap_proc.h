/********************************************************************************
* MeshDynamics
* --------------
* File     : meshap_heap_proc.h
* Comments : Meshap heap informarion /proc entry
* Created  : 11/14/2006
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  0  |11/14/2006| Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

#ifndef __MESHAP_HEAP_PROC_H__
#define __MESHAP_HEAP_PROC_H__

#ifdef _AL_HEAP_DEBUG
void meshap_heap_proc_initialize();
#endif

#endif /*__MESHAP_HEAP_PROC_H__*/
