/********************************************************************************
* MeshDynamics
* --------------
* File     : mesh_sip.c
* Comments : SIP Mesh Implementation
* Created  : 2/28/2008
* Author   : Abhijit Ayarekar
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  0  |2/28/2008 | Created.                                        |Abhijit |
* -----------------------------------------------------------------------------
********************************************************************************/

#include <linux/string.h>
#include <linux/kernel.h>
#include "al.h"
#include "al_802_11.h"
#include "sip_conf.h"
#include "meshap_sip.h"
#include "imcp_snip.h"
#include "mesh_imcp.h"
#include "sip_packet_parser.h"
#include "sip_data_helper.h"
#include "sip_dlg_helper.h"
#include "sip_mesh_helper.h"

#define MESH_SIP_PACKET_STA_REGISTRATION           1
#define MESH_SIP_PACKET_NEW_CALL_DIALOG            2
#define MESH_SIP_PACKET_CALL_DIALOG_STATUS         3
#define MESH_SIP_PACKET_LATEST_SIP_INFO_REQUEST    4

static int _process_pending_status_for_caller(meshap_sip_dlg_table_entry_t *dlg_entry, unsigned char *buffer)
{
   unsigned char *p;
   unsigned char len;

   p = buffer;
   switch (dlg_entry->caller_dlg.call_state)
   {
   case CALL_STATE_INVITE:
      if (dlg_entry->callee_dlg.call_state == CALL_STATE_STATUS_ERROR)
      {
         len = *p;
         p  += 1;
         memcpy(dlg_entry->extra_info, p, len);
         p += len;
         dlg_entry->extra_info[len] = 0;
         dlg_entry->callee_dlg.cseq = *p;
         p += 1;
      }
      break;

   case CALL_STATE_RINGING:
      /* we do nothing for status ok since we already have sdp info in extra info */
      if (dlg_entry->callee_dlg.call_state == CALL_STATE_RINGING)
      {
         len = *p;
         p  += 1;
         memcpy(dlg_entry->caller_dlg.to_tag, p, len);
         p += len;
         dlg_entry->caller_dlg.to_tag[len] = 0;
         len = *p;
         p  += 1;
         memcpy(dlg_entry->callee_dlg.to_tag, p, len);
         p += len;
         dlg_entry->callee_dlg.to_tag[len] = 0;
         memset(dlg_entry->extra_info, 0, 1024);
      }
      else if (dlg_entry->callee_dlg.call_state == CALL_STATE_STATUS_ERROR)
      {
         len = *p;
         p  += 1;
         memcpy(dlg_entry->extra_info, p, len);
         p += len;
         dlg_entry->extra_info[len] = 0;
         dlg_entry->callee_dlg.cseq = *p;
         p += 1;
      }
      break;

   case CALL_STATE_ESTABLISHED:
      if (dlg_entry->callee_dlg.call_state == CALL_STATE_BYE)
      {
         len = *p;
         p  += 1;
         memcpy(dlg_entry->caller_dlg.branch, p, len);
         p += len;
         dlg_entry->caller_dlg.branch[len] = 0;
         dlg_entry->caller_dlg.cseq        = *p;
         p  += 1;
         len = *p;
         p  += 1;
         memcpy(dlg_entry->callee_dlg.branch, p, len);
         p += len;
         dlg_entry->callee_dlg.branch[len] = 0;
         dlg_entry->callee_dlg.cseq        = *p;
         p += 1;
         memset(dlg_entry->extra_info, 0, 1024);
      }
      break;

   case CALL_STATE_CANCEL:
      if (dlg_entry->callee_dlg.call_state != CALL_STATE_END)
      {
         dlg_entry->caller_dlg.cseq = *p;
      }
      p += 1;
      memset(dlg_entry->extra_info, 0, 1024);
      break;
   }

   return(p - buffer);
}


static int _process_pending_status_for_callee(meshap_sip_dlg_table_entry_t *dlg_entry, unsigned char *buffer)
{
   unsigned char *p;
   unsigned char len;

   p = buffer;
   switch (dlg_entry->callee_dlg.call_state)
   {
   case CALL_STATE_STATUS_OK:
      if (dlg_entry->caller_dlg.call_state == CALL_STATE_ESTABLISHED)
      {
         len = *p;
         p  += 1;
         memcpy(dlg_entry->caller_dlg.branch, p, len);
         p += len;
         dlg_entry->caller_dlg.branch[len] = 0;
         dlg_entry->caller_dlg.cseq        = *p;
         memset(dlg_entry->extra_info, 0, 1024);
      }
      else if (dlg_entry->caller_dlg.call_state == CALL_STATE_BYE)
      {
         len = *p;
         p  += 1;
         memcpy(dlg_entry->caller_dlg.branch, p, len);
         p += len;
         dlg_entry->caller_dlg.branch[len] = 0;
         dlg_entry->caller_dlg.cseq        = *p;
         p  += 1;
         len = *p;
         p  += 1;
         memcpy(dlg_entry->callee_dlg.branch, p, len);
         p += len;
         dlg_entry->callee_dlg.branch[len] = 0;
         dlg_entry->callee_dlg.cseq        = *p;
         p += 1;
         memset(dlg_entry->extra_info, 0, 1024);
      }
      break;

   case CALL_STATE_STATUS_ERROR:
      if (dlg_entry->caller_dlg.call_state == CALL_STATE_END)
      {
         len = *p;
         p  += 1;
         memcpy(dlg_entry->caller_dlg.branch, p, len);
         p += len;
         dlg_entry->caller_dlg.branch[len] = 0;
         dlg_entry->caller_dlg.cseq        = *p;
         memset(dlg_entry->extra_info, 0, 1024);
      }
      break;

   case CALL_STATE_ESTABLISHED:
      if (dlg_entry->caller_dlg.call_state == CALL_STATE_BYE)
      {
         len = *p;
         p  += 1;
         memcpy(dlg_entry->caller_dlg.branch, p, len);
         p += len;
         dlg_entry->caller_dlg.branch[len] = 0;
         dlg_entry->caller_dlg.cseq        = *p;
         p  += 1;
         len = *p;
         p  += 1;
         memcpy(dlg_entry->callee_dlg.branch, p, len);
         p += len;
         dlg_entry->callee_dlg.branch[len] = 0;
         dlg_entry->callee_dlg.cseq        = *p;
         p += 1;
         memset(dlg_entry->extra_info, 0, 1024);
      }
      break;

   case CALL_STATE_CANCEL:
      if (dlg_entry->caller_dlg.call_state != CALL_STATE_END)
      {
         dlg_entry->callee_dlg.cseq = *p;
      }
      p += 1;
      memset(dlg_entry->extra_info, 0, 1024);
      break;
   }

   return(p - buffer);
}


static int _process_call_dialog_status(unsigned char *packet_data)
{
   unsigned char                *p;
   unsigned int                 dlg_id;
   meshap_sip_dlg_table_entry_t *dlg_entry;
   meshap_sip_sta_info_t        *caller_info;
   meshap_sip_sta_info_t        *callee_info;
   unsigned char                pending_status;
   unsigned short               extra_info_len;
   unsigned int                 le32;
   unsigned short               le16;

   p = packet_data;

   memcpy(&le32, p, 4);
   p     += 4;
   dlg_id = al_impl_le32_to_cpu(le32);

   dlg_entry = _sip_data_find_dlg_by_dlg_id(dlg_id);
   if (dlg_entry == NULL)
   {
      return -1;
   }

   pending_status = *p;
   p += 1;

   caller_info = _sip_data_find_sip_sta(p, STA_SEARCH_TYPE_MAC);
   p          += 6;
   if (caller_info == NULL)
   {
      memset(dlg_entry, 0, sizeof(meshap_sip_dlg_table_entry_t));
      return -2;
   }

   memcpy(&le16, p, 2);
   p += 2;
   caller_info->rport = al_impl_le16_to_cpu(le16);
   memcpy(&caller_info->ip_addr.bytes, p, 4);
   p += 4;

   callee_info = _sip_data_find_sip_sta(p, STA_SEARCH_TYPE_MAC);
   p          += 6;
   if (callee_info == NULL)
   {
      memset(dlg_entry, 0, sizeof(meshap_sip_dlg_table_entry_t));
      return -2;
   }

   memcpy(&le16, p, 2);
   p += 2;
   callee_info->rport = al_impl_le16_to_cpu(le16);
   memcpy(&callee_info->ip_addr.bytes, p, 4);
   p += 4;

   dlg_entry->caller_dlg.call_state = *p;
   p += 1;
   dlg_entry->callee_dlg.call_state = *p;
   p += 1;

   memcpy(&le16, p, 2);
   p += 2;
   extra_info_len = al_impl_le16_to_cpu(le16);

   memcpy(dlg_entry->extra_info, p, extra_info_len);
   dlg_entry->extra_info[extra_info_len] = 0;

   if (pending_status == PENDING_STATUS_FOR_NONE)
   {
      return _sip_dlg_handle_call_dlg(dlg_entry, pending_status);
   }

   if (pending_status == PENDING_STATUS_FOR_CALLER)
   {
      p += _process_pending_status_for_caller(dlg_entry, p);
      _REGISTER_STA(callee_info->flags);
   }
   else if (pending_status == PENDING_STATUS_FOR_CALLEE)
   {
      p += _process_pending_status_for_callee(dlg_entry, p);
      _REGISTER_STA(caller_info->flags);
   }

   if (_sip_data_evaluate_sta_locations(dlg_entry) != 0)
   {
      memset(dlg_entry, 0, sizeof(meshap_sip_dlg_table_entry_t));
      return -1;
   }

   if ((pending_status == PENDING_STATUS_FOR_CALLER) && !_IS_STA_LOCAL(caller_info->flags))
   {
      return 0;
   }

   if ((pending_status == PENDING_STATUS_FOR_CALLEE) && !_IS_STA_LOCAL(callee_info->flags))
   {
      return 0;
   }

   _sip_dlg_handle_call_dlg(dlg_entry, pending_status);

   return 0;
}


static int _process_new_call_dialog(unsigned char *packet_data)
{
   unsigned char                *p;
   unsigned int                 dlg_id;
   meshap_sip_dlg_table_entry_t *dlg_entry;
   meshap_sip_sta_info_t        *caller_info;
   meshap_sip_sta_info_t        *callee_info;
   unsigned char                len;
   unsigned int                 le32;

   p = packet_data;

   memcpy(&le32, p, 4);
   p     += 4;
   dlg_id = al_impl_le32_to_cpu(le32);

   dlg_entry = _sip_data_find_dlg_by_dlg_id(dlg_id);
   if (dlg_entry == NULL)
   {
      dlg_entry = _sip_data_get_empty_dlg(0);
      if (dlg_entry == NULL)
      {
         return -1;
      }
   }

   caller_info = _sip_data_find_sip_sta(p, STA_SEARCH_TYPE_MAC);
   p          += 6;
   if (caller_info == NULL)
   {
      memset(dlg_entry, 0, sizeof(meshap_sip_dlg_table_entry_t));
      return -2;
   }

   callee_info = _sip_data_find_sip_sta(p, STA_SEARCH_TYPE_MAC);
   p          += 6;
   if (callee_info == NULL)
   {
      memset(dlg_entry, 0, sizeof(meshap_sip_dlg_table_entry_t));
      return -2;
   }

   dlg_entry->caller_info = caller_info;
   dlg_entry->callee_info = callee_info;

   dlg_entry->id = dlg_id;
   len           = *p;
   p            += 1;
   memcpy(dlg_entry->caller_dlg.branch, p, len);
   p += len;
   dlg_entry->caller_dlg.branch[len] = 0;
   len = *p;
   p  += 1;
   memcpy(dlg_entry->caller_dlg.call_id, p, len);
   p += len;
   dlg_entry->caller_dlg.call_id[len] = 0;
   len = *p;
   p  += 1;
   memcpy(dlg_entry->caller_dlg.from_tag, p, len);
   p += len;
   dlg_entry->caller_dlg.from_tag[len] = 0;
   len = *p;
   p  += 1;
   memcpy(dlg_entry->caller_dlg.to_tag, p, len);
   p += len;
   dlg_entry->caller_dlg.to_tag[len] = 0;
   dlg_entry->caller_dlg.cseq        = *p;
   p += 1;
   dlg_entry->caller_dlg.call_initiator = 1;

   len = *p;
   p  += 1;
   memcpy(dlg_entry->callee_dlg.branch, p, len);
   p += len;
   dlg_entry->callee_dlg.branch[len] = 0;
   len = *p;
   p  += 1;
   memcpy(dlg_entry->callee_dlg.call_id, p, len);
   p += len;
   dlg_entry->callee_dlg.call_id[len] = 0;
   len = *p;
   p  += 1;
   memcpy(dlg_entry->callee_dlg.from_tag, p, len);
   p += len;
   dlg_entry->callee_dlg.from_tag[len] = 0;
   len = *p;
   p  += 1;
   memcpy(dlg_entry->callee_dlg.to_tag, p, len);
   p += len;
   dlg_entry->callee_dlg.to_tag[len] = 0;
   len = *p;
   p  += 1;
   memcpy(dlg_entry->callee_dlg.rinstance, p, len);
   p += len;
   dlg_entry->callee_dlg.rinstance[len] = 0;
   dlg_entry->callee_dlg.cseq           = *p;
   p += 1;

   return 0;
}


static int _process_sta_registration(unsigned char *packet_data)
{
   meshap_sip_sta_info_t *sta_info;
   unsigned char         *p;
   unsigned short        le16;
   unsigned char         is_registered;

   sta_info = _sip_data_find_sip_sta(packet_data, STA_SEARCH_TYPE_MAC);
   if (sta_info == NULL)
   {
      return -1;
   }

   p = packet_data + 6;

   is_registered = *p;
   p            += 1;
   memcpy(sta_info->ip_addr.bytes, p, 4);
   p += 4;

   memcpy(&le16, p, 2);
   p += 2;
   sta_info->rport = al_impl_le16_to_cpu(le16);

   _SET_STA_LOCATION_REMOTE(sta_info->flags);

   if (is_registered == 0)
   {
      _UNREGISTER_STA(sta_info->flags);
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "%s unregistered through mesh\n", sta_info->info.extn);
   }
   else
   {
      _REGISTER_STA(sta_info->flags);
      sta_info->last_check_time = al_get_tick_count(AL_CONTEXT);
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "%s registered through mesh\n", sta_info->info.extn);
   }

   return 0;
}


int meshap_process_imcp_sip_packet(al_net_addr_t *sender_mac, unsigned char *packet_data, unsigned short data_length)
{
   int ret;

   ret = 0;

   switch (packet_data[0])
   {
   case MESH_SIP_PACKET_STA_REGISTRATION:
      ret = _process_sta_registration(packet_data + 1);
      break;

   case MESH_SIP_PACKET_NEW_CALL_DIALOG:
      ret = _process_new_call_dialog(packet_data + 1);
      break;

   case MESH_SIP_PACKET_CALL_DIALOG_STATUS:
      ret = _process_call_dialog_status(packet_data + 1);
      break;

   case MESH_SIP_PACKET_LATEST_SIP_INFO_REQUEST:
      ret = _sip_mesh_helper_bcast_latest_sip_info();
      break;
   }

   return ret;
}


int _sip_mesh_helper_bcast_sta_registration(meshap_sip_sta_info_t *sta_info)
{
   unsigned char  buffer[32];
   unsigned char  *p;
   int            imcp_packet_type;
   unsigned short le16;

   imcp_packet_type = IMCP_SNIP_PACKET_TYPE_SIP_PRIVATE;

   memset(buffer, 0, 32);
   p = buffer;

   *p = MESH_SIP_PACKET_STA_REGISTRATION;
   p += 1;
   memcpy(p, sta_info->info.mac, 6);
   p += 6;
   *p = (_IS_STA_REGISTERED(sta_info->flags)) ? 1 : 0;
   p += 1;
   memcpy(p, sta_info->ip_addr.bytes, 4);
   p   += 4;
   le16 = al_impl_cpu_to_le16(sta_info->rport);
   memcpy(p, &le16, 2);
   p += 2;

   mesh_imcp_send_extern_packet(AL_CONTEXT imcp_packet_type, buffer, (unsigned short)(p - buffer));

   return 0;
}


int _sip_mesh_helper_bcast_new_call_dialog(meshap_sip_dlg_table_entry_t *dlg_entry)
{
   unsigned char buffer[1024];
   unsigned char *p;
   int           imcp_packet_type;
   unsigned char len;
   unsigned int  le32;

   imcp_packet_type = IMCP_SNIP_PACKET_TYPE_SIP_PRIVATE;

   memset(buffer, 0, 1024);
   p = buffer;

   *p = MESH_SIP_PACKET_NEW_CALL_DIALOG;
   p += 1;

   le32 = al_impl_cpu_to_le32(dlg_entry->id);
   memcpy(p, &le32, 4);
   p += 4;

   memcpy(p, dlg_entry->caller_info->info.mac, 6);
   p += 6;
   memcpy(p, dlg_entry->callee_info->info.mac, 6);
   p += 6;

   len = strlen(dlg_entry->caller_dlg.branch);
   *p  = len;
   p  += 1;
   memcpy(p, dlg_entry->caller_dlg.branch, len);
   p  += len;
   len = strlen(dlg_entry->caller_dlg.call_id);
   *p  = len;
   p  += 1;
   memcpy(p, dlg_entry->caller_dlg.call_id, len);
   p  += len;
   len = strlen(dlg_entry->caller_dlg.from_tag);
   *p  = len;
   p  += 1;
   memcpy(p, dlg_entry->caller_dlg.from_tag, len);
   p  += len;
   len = strlen(dlg_entry->caller_dlg.to_tag);
   *p  = len;
   p  += 1;
   memcpy(p, dlg_entry->caller_dlg.to_tag, len);
   p += len;
   *p = dlg_entry->caller_dlg.cseq;
   p += 1;

   len = strlen(dlg_entry->callee_dlg.branch);
   *p  = len;
   p  += 1;
   memcpy(p, dlg_entry->callee_dlg.branch, len);
   p  += len;
   len = strlen(dlg_entry->callee_dlg.call_id);
   *p  = len;
   p  += 1;
   memcpy(p, dlg_entry->callee_dlg.call_id, len);
   p  += len;
   len = strlen(dlg_entry->callee_dlg.from_tag);
   *p  = len;
   p  += 1;
   memcpy(p, dlg_entry->callee_dlg.from_tag, len);
   p  += len;
   len = strlen(dlg_entry->callee_dlg.to_tag);
   *p  = len;
   p  += 1;
   memcpy(p, dlg_entry->callee_dlg.to_tag, len);
   p  += len;
   len = strlen(dlg_entry->callee_dlg.rinstance);
   *p  = len;
   p  += 1;
   memcpy(p, dlg_entry->callee_dlg.rinstance, len);
   p += len;
   *p = dlg_entry->callee_dlg.cseq;
   p += 1;

   mesh_imcp_send_extern_packet(imcp_packet_type, buffer, (unsigned short)(p - buffer));

   return 0;
}


static int _setup_pending_status_for_caller(meshap_sip_dlg_table_entry_t *dlg_entry, unsigned char *buffer)
{
   unsigned short extra_info_len;
   unsigned char  *p;
   unsigned char  len;
   unsigned short le16;

   p = buffer;
   switch (dlg_entry->caller_dlg.call_state)
   {
   case CALL_STATE_INVITE:
      if (dlg_entry->callee_dlg.call_state == CALL_STATE_STATUS_ERROR)
      {
         /* we have to send error status line and new cseq */
         extra_info_len = strlen(dlg_entry->extra_info) + 1;
         le16           = al_impl_cpu_to_le16(extra_info_len);
         memcpy(p, &le16, 2);
         p += 2;

         len = strlen(dlg_entry->extra_info);
         *p  = len;
         p  += 1;
         memcpy(p, dlg_entry->extra_info, len);
         p += len;
         *p = dlg_entry->callee_dlg.cseq;
         p += 1;
      }
      break;

   case CALL_STATE_RINGING:
      if (dlg_entry->callee_dlg.call_state == CALL_STATE_RINGING)
      {
         /* we hav to send new to_tag's for both dialogs */
         extra_info_len = strlen(dlg_entry->caller_dlg.to_tag) +
                          strlen(dlg_entry->callee_dlg.to_tag) +
                          2;
         le16 = al_impl_cpu_to_le16(extra_info_len);
         memcpy(p, &le16, 2);
         p += 2;

         len = strlen(dlg_entry->caller_dlg.to_tag);
         *p  = len;
         p  += 1;
         memcpy(p, dlg_entry->caller_dlg.to_tag, len);
         p  += len;
         len = strlen(dlg_entry->callee_dlg.to_tag);
         *p  = len;
         p  += 1;
         memcpy(p, dlg_entry->callee_dlg.to_tag, len);
         p += len;
      }
      else if (dlg_entry->callee_dlg.call_state == CALL_STATE_STATUS_OK)
      {
         /* we have to send sdp info from status message */
         extra_info_len = strlen(dlg_entry->extra_info);
         le16           = al_impl_cpu_to_le16(extra_info_len);
         memcpy(p, &le16, 2);
         p += 2;
         memcpy(p, dlg_entry->extra_info, extra_info_len);
         p += extra_info_len;
      }
      else if (dlg_entry->callee_dlg.call_state == CALL_STATE_STATUS_ERROR)
      {
         /* we have to send error status line and new cseq */
         extra_info_len = strlen(dlg_entry->extra_info) + 1;
         le16           = al_impl_cpu_to_le16(extra_info_len);
         memcpy(p, &le16, 2);
         p  += 2;
         len = strlen(dlg_entry->extra_info);
         *p  = len;
         p  += 1;
         memcpy(p, dlg_entry->extra_info, len);
         p += len;
         *p = dlg_entry->callee_dlg.cseq;
         p += 1;
      }
      break;

   case CALL_STATE_ESTABLISHED:
      if (dlg_entry->callee_dlg.call_state == CALL_STATE_BYE)
      {
         /* usually a new branch is setup in bye call, hence we send new branch id's */
         extra_info_len = strlen(dlg_entry->caller_dlg.branch) +
                          strlen(dlg_entry->callee_dlg.branch) +
                          4;
         le16 = al_impl_cpu_to_le16(extra_info_len);
         memcpy(p, &le16, 2);
         p += 2;

         len = strlen(dlg_entry->caller_dlg.branch);
         *p  = len;
         p  += 1;
         memcpy(p, dlg_entry->caller_dlg.branch, len);
         p  += len;
         *p  = dlg_entry->caller_dlg.cseq;
         p  += 1;
         len = strlen(dlg_entry->callee_dlg.branch);
         *p  = len;
         p  += 1;
         memcpy(p, dlg_entry->callee_dlg.branch, len);
         p += len;
         *p = dlg_entry->callee_dlg.cseq;
         p += 1;
      }
      break;

   case CALL_STATE_CANCEL:
      if (dlg_entry->callee_dlg.call_state == CALL_STATE_END)
      {
         extra_info_len = 0;
         memcpy(p, &extra_info_len, 2);
         p += 2;
      }
      else
      {
         extra_info_len = 1;
         le16           = al_impl_cpu_to_le16(extra_info_len);
         memcpy(p, &le16, 2);
         p += 2;
         *p = dlg_entry->callee_dlg.cseq;
         p += 1;
      }
      break;
   }

   return(p - buffer);
}


static int _setup_pending_status_for_callee(meshap_sip_dlg_table_entry_t *dlg_entry, unsigned char *buffer)
{
   unsigned short extra_info_len;
   unsigned char  *p;
   unsigned char  len;
   unsigned short le16;

   p = buffer;
   switch (dlg_entry->callee_dlg.call_state)
   {
   case CALL_STATE_START:
      if (dlg_entry->caller_dlg.call_state == CALL_STATE_INVITE)
      {
         /* we have to send sdp info */
         extra_info_len = strlen(dlg_entry->extra_info);
         le16           = al_impl_cpu_to_le16(extra_info_len);
         memcpy(p, &le16, 2);
         p += 2;
         memcpy(p, dlg_entry->extra_info, extra_info_len);
         p += extra_info_len;
      }
      break;

   case CALL_STATE_STATUS_OK:
      if (dlg_entry->caller_dlg.call_state == CALL_STATE_ESTABLISHED)
      {
         /* ack message can have new branch and cseq, so we send both */
         extra_info_len = strlen(dlg_entry->caller_dlg.branch) + 2;
         le16           = al_impl_cpu_to_le16(extra_info_len);
         memcpy(p, &le16, 2);
         p  += 2;
         len = strlen(dlg_entry->caller_dlg.branch);
         *p  = len;
         p  += 1;
         memcpy(p, dlg_entry->caller_dlg.branch, len);
         p += len;
         *p = dlg_entry->caller_dlg.cseq;
         p += 1;
      }
      else if (dlg_entry->caller_dlg.call_state == CALL_STATE_BYE)
      {
         /* usually a new branch is setup in bye call, hence we send new branch id's */
         extra_info_len = strlen(dlg_entry->caller_dlg.branch) +
                          strlen(dlg_entry->callee_dlg.branch) +
                          4;
         le16 = al_impl_cpu_to_le16(extra_info_len);
         memcpy(p, &le16, 2);
         p += 2;

         len = strlen(dlg_entry->caller_dlg.branch);
         *p  = len;
         p  += 1;
         memcpy(p, dlg_entry->caller_dlg.branch, len);
         p  += len;
         *p  = dlg_entry->caller_dlg.cseq;
         p  += 1;
         len = strlen(dlg_entry->callee_dlg.branch);
         *p  = len;
         p  += 1;
         memcpy(p, dlg_entry->callee_dlg.branch, len);
         p += len;
         *p = dlg_entry->callee_dlg.cseq;
         p += 1;
      }
      break;

   case CALL_STATE_STATUS_ERROR:
      if (dlg_entry->caller_dlg.call_state == CALL_STATE_END)
      {
         /* ack message can have new branch and cseq, so we send both */
         extra_info_len = strlen(dlg_entry->caller_dlg.branch) + 2;
         le16           = al_impl_cpu_to_le16(extra_info_len);
         memcpy(p, &le16, 2);
         p += 2;

         len = strlen(dlg_entry->caller_dlg.branch);
         *p  = len;
         p  += 1;
         memcpy(p, dlg_entry->caller_dlg.branch, len);
         p += len;
         *p = dlg_entry->caller_dlg.cseq;
         p += 1;
      }
      break;

   case CALL_STATE_END:
      if (dlg_entry->caller_dlg.call_state == CALL_STATE_END)
      {
         /* ack message can have new branch and cseq, so we send both */
         extra_info_len = strlen(dlg_entry->caller_dlg.branch) + 2;
         le16           = al_impl_cpu_to_le16(extra_info_len);
         memcpy(p, &le16, 2);
         p += 2;

         len = strlen(dlg_entry->caller_dlg.branch);
         *p  = len;
         p  += 1;
         memcpy(p, dlg_entry->caller_dlg.branch, len);
         p += len;
         *p = dlg_entry->caller_dlg.cseq;
         p += 1;
      }
      break;

   case CALL_STATE_ESTABLISHED:
      if (dlg_entry->caller_dlg.call_state == CALL_STATE_BYE)
      {
         /* usually a new branch is setup in bye call, hence we send new branch id's */
         extra_info_len = strlen(dlg_entry->caller_dlg.branch) +
                          strlen(dlg_entry->callee_dlg.branch) +
                          4;
         le16 = al_impl_cpu_to_le16(extra_info_len);
         memcpy(p, &le16, 2);
         p += 2;

         len = strlen(dlg_entry->caller_dlg.branch);
         *p  = len;
         p  += 1;
         memcpy(p, dlg_entry->caller_dlg.branch, len);
         p  += len;
         *p  = dlg_entry->caller_dlg.cseq;
         p  += 1;
         len = strlen(dlg_entry->callee_dlg.branch);
         *p  = len;
         p  += 1;
         memcpy(p, dlg_entry->callee_dlg.branch, len);
         p += len;
         *p = dlg_entry->callee_dlg.cseq;
         p += 1;
      }
      break;

   case CALL_STATE_CANCEL:
      if (dlg_entry->caller_dlg.call_state == CALL_STATE_END)
      {
         extra_info_len = 0;
         memcpy(p, &extra_info_len, 2);
         p += 2;
      }
      else
      {
         /* cancel msg is sent with new cseq so we update it */
         extra_info_len = 1;
         le16           = al_impl_cpu_to_le16(extra_info_len);
         memcpy(p, &le16, 2);
         p += 2;

         *p = dlg_entry->caller_dlg.cseq;
         p += 1;
      }
      break;
   }

   return(p - buffer);
}


int _sip_mesh_helper_bcast_call_dialog_status(meshap_sip_dlg_table_entry_t *dlg_entry, unsigned char pending_status)
{
   unsigned char  buffer[1024];
   unsigned char  *p;
   unsigned short extra_info_len;
   unsigned int   le32;
   unsigned short le16;

   memset(buffer, 0, 1024);
   p = buffer;

   *p = MESH_SIP_PACKET_CALL_DIALOG_STATUS;
   p += 1;

   le32 = al_impl_cpu_to_le32(dlg_entry->id);
   memcpy(p, &le32, 4);
   p += 4;

   *p = pending_status;
   p += 1;
   memcpy(p, dlg_entry->caller_info->info.mac, 6);
   p += 6;

   le16 = al_impl_cpu_to_le16(dlg_entry->caller_info->rport);
   memcpy(p, &le16, 2);
   p += 2;

   memcpy(p, dlg_entry->caller_info->ip_addr.bytes, 4);
   p += 4;
   memcpy(p, dlg_entry->callee_info->info.mac, 6);
   p += 6;

   le16 = al_impl_cpu_to_le16(dlg_entry->callee_info->rport);
   memcpy(p, &le16, 2);
   p += 2;

   memcpy(p, dlg_entry->callee_info->ip_addr.bytes, 4);
   p += 4;
   *p = dlg_entry->caller_dlg.call_state;
   p += 1;
   *p = dlg_entry->callee_dlg.call_state;
   p += 1;

   if (pending_status == PENDING_STATUS_FOR_CALLER)
   {
      p += _setup_pending_status_for_caller(dlg_entry, p);
   }
   else if (pending_status == PENDING_STATUS_FOR_CALLEE)
   {
      p += _setup_pending_status_for_callee(dlg_entry, p);
   }
   else
   {
      extra_info_len = 0;
      memcpy(p, &extra_info_len, 2);
      p += 2;
   }

   mesh_imcp_send_extern_packet(IMCP_SNIP_PACKET_TYPE_SIP_PRIVATE, buffer, (unsigned short)(p - buffer));

   return 0;
}


int _sip_mesh_helper_request_latest_sip_info()
{
   unsigned char buffer[8];

   memset(buffer, 0, 8);

   buffer[0] = MESH_SIP_PACKET_LATEST_SIP_INFO_REQUEST;
   mesh_imcp_send_extern_packet(IMCP_SNIP_PACKET_TYPE_SIP_PRIVATE, buffer, (unsigned short)8);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MESHAP_SIP : Requesting latest SIP info\n");
   return 0;
}


int _sip_mesh_helper_bcast_latest_sip_info(void)
{
   int i;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MESHAP_SIP : Sending latest SIP info\n");
   for (i = 0; i < _MAX_STA_COUNT; i++)
   {
      if (_IS_STA_REGISTERED(_config.sta_list[i].flags) && _IS_STA_LOCAL(_config.sta_list[i].flags))
      {
         _sip_mesh_helper_bcast_sta_registration(&_config.sta_list[i]);
      }
   }

   for (i = 0; i < _MAX_CALL_DIALOGS; i++)
   {
      if (!_IS_CALL_DLG_IN_USE(_dlg_table[i].flags))
      {
         continue;
      }

      _sip_mesh_helper_bcast_new_call_dialog(&_dlg_table[i]);
      _sip_mesh_helper_bcast_call_dialog_status(&_dlg_table[i], PENDING_STATUS_FOR_NONE);
   }

   return 0;
}
