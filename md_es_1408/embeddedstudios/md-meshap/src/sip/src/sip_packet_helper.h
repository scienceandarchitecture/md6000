/********************************************************************************
* MeshDynamics
* --------------
* File     : sip_packet_helper.h
* Comments : SIP packet implementation
* Created  : 5/21/2008
* Author   : Abhijit Ayarekar
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  0  |5/21/2008 | Created.                                        |Abhijit |
* -----------------------------------------------------------------------------
********************************************************************************/

#ifndef __SIP_PACKET_HELPER_H__
#define __SIP_PACKET_HELPER_H__

#define _SIP_STATUS_200_STR        "SIP/2.0 200 Ok\r\n"
#define _SIP_STATUS_200_STR_LEN    strlen(_SIP_STATUS_200_STR)

int _sip_packet_helper_initialize(void);
int _sip_packet_helper_uninitialize(void);
int _sip_packet_helper_send_packet(meshap_sip_sta_info_t *sta_info, unsigned char *buffer, int buffer_length);
int _sip_packet_helper_process_sip_packet(sip_header_info_t *sip_header, al_net_if_t *net_if, al_packet_t *packet_in);

#endif /*__SIP_PACKET_HELPER_H__*/
