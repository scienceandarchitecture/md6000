
/********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : mesh_plugin_event_handler.h
 * Comments : mesh plugin event handler implementation
 * Created  : 5/21/2008
 * Author   : Abhijit Ayarekar
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * -----------------------------------------------------------------------------
 * | No  |Date      |  Comment                                        | Author |
 * -----------------------------------------------------------------------------
 * |  0  |5/21/2008 | Created.                                        |Abhijit |
 * -----------------------------------------------------------------------------
 ********************************************************************************/

#include <linux/string.h>
#include <linux/kernel.h>
#include "al.h"
#include "al_802_11.h"
#include "al_ip.h"
#include "al_socket.h"
#include "access_point.h"
#include "access_point_ext.h"
#include "mesh_ext.h"
#include "sip_conf.h"
#include "meshap_sip.h"
#include "mesh_plugin.h"
#include "sip_plugin_event_handler.h"
#include "sip_packet_parser.h"
#include "sip_data_helper.h"
#include "sip_mesh_helper.h"

static unsigned int							_mesh_plugin_event_id_map; 
static mesh_ext_known_access_point_info_t 	_current_parent;

static int _sip_plugin_on_parent_shift_handler(void)
{
	mesh_ext_known_access_point_info_t parent;
	
	memset(&parent,0,sizeof(mesh_ext_known_access_point_info_t));
	
	mesh_ext_get_current_parent_info(&parent);
/*	
	if(memcmp(&parent.root_bssid, &_current_parent.root_bssid, sizeof(al_net_addr_t)) == 0)
		return 0;
*/	
	al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "MESHAP_SIP : Parent root bssid changed, refreshing sip information\n");
	
	_sip_mesh_helper_request_latest_sip_info();
	_sip_mesh_helper_bcast_latest_sip_info();
	
	return 0;
}

static int _sip_plugin_on_heartbeat_received_handler(void* event_param)
{
	mesh_plugin_heartbeat_info_t*	hb_info;
	int								i;
	meshap_sip_sta_info_t*			sip_sta_info;
	
	hb_info = (mesh_plugin_heartbeat_info_t*)event_param;
	
	for(i=0;i<hb_info->children_count;i++) {
		
		sip_sta_info = _sip_data_find_sip_sta(hb_info->immediate_sta[i].bytes, STA_SEARCH_TYPE_MAC);
		if(sip_sta_info == NULL)
			continue;
		
		_SET_STA_LOCATION_REMOTE(sip_sta_info->flags);
		sip_sta_info->last_check_time 	= al_get_tick_count(AL_CONTEXT);
		sip_sta_info->net_if			= NULL;
	}
	
	return 0;
}

static int _sip_plugin_on_sta_associated_handler(void* event_param)
{

	al_net_addr_t*				sta_addr;
	meshap_sip_sta_info_t*		sip_sta_info;
	int							i;
	
	sta_addr 		= (al_net_addr_t*)event_param;
	sip_sta_info 	= _sip_data_find_sip_sta(sta_addr->bytes, STA_SEARCH_TYPE_MAC);
	
	if(sip_sta_info == NULL)
		return -1;

	_SET_STA_LOCATION_LOCAL(sip_sta_info->flags);
	
	for(i=0;i<_MAX_CALL_DIALOGS;i++) {

		if(_dlg_table[i].id == 0 || !_IS_CALL_DLG_IN_USE(_dlg_table[i].flags))
			continue;
		
		if(_dlg_table[i].caller_info == sip_sta_info)
			_SET_DLG_VERIFY_CALLER(_dlg_table[i].flags)
		else if(_dlg_table[i].callee_info == sip_sta_info)
			_SET_DLG_VERIFY_CALLEE(_dlg_table[i].flags)
			
	}
	
	return 0;
}

static int _sip_plugin_event_handler(unsigned int event_id, void* event_param)
{
	int ret;
	
	ret = 0;
	switch(event_id) {
		case MESH_PLUGIN_EVENT_PARENT_SHIFTED:
			ret = _sip_plugin_on_parent_shift_handler();
			break;
		case MESH_PLUGIN_EVENT_HEARTBEAT_RECEIVED:
			ret = _sip_plugin_on_heartbeat_received_handler(event_param);
			break;
		case MESH_PLUGIN_EVENT_STA_ASSOCIATED:
			ret = _sip_plugin_on_sta_associated_handler(event_param);
			break;
	}
	
	return ret;
}

int _sip_plugin_handler_initialize(void)
{
	
	_mesh_plugin_event_id_map =	MESH_PLUGIN_EVENT_PARENT_SHIFTED		|
								MESH_PLUGIN_EVENT_HEARTBEAT_RECEIVED	|
								MESH_PLUGIN_EVENT_STA_ASSOCIATED;

	memset(&_current_parent,0,sizeof(mesh_ext_known_access_point_info_t));
	
	mesh_ext_get_current_parent_info(&_current_parent);
	
	add_mesh_event_handler(_mesh_plugin_event_id_map, _sip_plugin_event_handler);

	return 0;
}


int _sip_plugin_handler_uninitialize(void)
{
	remove_mesh_event_handler(_mesh_plugin_event_id_map, _sip_plugin_event_handler);
	return 0;
}
