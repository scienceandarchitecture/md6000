/********************************************************************************
* MeshDynamics
* --------------
* File     : meshap_sip.h
* Comments : call dlg cleanup thread implementation
* Created  : 1/16/2008
* Author   : Abhijit Ayarekar
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  0  |1/16/2008 | Created.                                        |Abhijit |
* -----------------------------------------------------------------------------
********************************************************************************/

#ifndef __SIP_CLEANUP_HELPER_H__
#define __SIP_CLEANUP_HELPER_H__


int _sip_cleanup_helper_initialize(void);
int _sip_cleanup_helper_uninitialize(void);

#endif /*__SIP_CLEANUP_HELPER_H__*/
