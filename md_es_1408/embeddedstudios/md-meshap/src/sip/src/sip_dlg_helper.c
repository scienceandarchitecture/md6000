/********************************************************************************
* MeshDynamics
* --------------
* File     : sip_dlg_helper.c
* Comments : sip call dlg helper implementation
* Created  : 5/21/2008
* Author   : Abhijit Ayarekar
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  0  |5/21/2008 | Created.                                        |Abhijit |
* -----------------------------------------------------------------------------
********************************************************************************/
#include <linux/kernel.h>
#include <linux/string.h>
#include "al.h"
#include "al_802_11.h"
#include "al_ip.h"
#include "al_socket.h"
#include "sip_conf.h"
#include "meshap_sip.h"
#include "sip_packet_parser.h"
#include "sip_data_helper.h"
#include "sip_packet_helper.h"
#include "sip_dlg_helper.h"
#include "sip_mesh_helper.h"

#define _CRLF        "\r\n"
#define _CRLF_LEN    2

static int _handle_pending_for_caller_update(meshap_sip_dlg_table_entry_t *dlg_entry);
static int _handle_pending_for_callee_update(meshap_sip_dlg_table_entry_t *dlg_entry);

static int ___forward_trying(meshap_sip_dlg_table_entry_t *dlg_entry);
static int ___ring_peer(meshap_sip_dlg_table_entry_t *dlg_entry);
static int ___forward_client_success(meshap_sip_dlg_table_entry_t *dlg_entry);
static int ___forward_client_error(meshap_sip_dlg_table_entry_t *dlg_entry);
static int ___forward_bye(meshap_sip_dlg_table_entry_t *dlg_entry, unsigned char pending_status);
static int ___forward_cancel_status(meshap_sip_dlg_table_entry_t *dlg_entry, unsigned char pending_status);
static int ___forward_cancel_request(meshap_sip_dlg_table_entry_t *dlg_entry, unsigned char pending_status);
static int ___forward_bye_success(meshap_sip_dlg_table_entry_t *dlg_entry, unsigned char pending_status);
static int ___invite_callee(meshap_sip_dlg_table_entry_t *dlg_entry);
static int ___forward_ack(meshap_sip_dlg_table_entry_t *dlg_entry);

int _sip_dlg_handle_call_dlg(meshap_sip_dlg_table_entry_t *dlg_entry, unsigned char pending_status)
{
   meshap_sip_dlg_info_t *callee_dlg;
   meshap_sip_dlg_info_t *caller_dlg;

   caller_dlg = &dlg_entry->caller_dlg;
   callee_dlg = &dlg_entry->callee_dlg;

   if (pending_status == PENDING_STATUS_FOR_CALLER)
   {
      _handle_pending_for_caller_update(dlg_entry);
   }
   else if (pending_status == PENDING_STATUS_FOR_CALLEE)
   {
      _handle_pending_for_callee_update(dlg_entry);
   }

   if ((caller_dlg->call_state == CALL_STATE_END) && (callee_dlg->call_state == CALL_STATE_END))
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "Clearing call dialog %x\n", dlg_entry->id);
      _sip_mesh_helper_bcast_call_dialog_status(dlg_entry, PENDING_STATUS_FOR_NONE);
      memset(dlg_entry, 0, sizeof(meshap_sip_dlg_table_entry_t));
   }

   return MESHAP_SIP_RETVAL_DISCARD;
}


static int _handle_pending_for_caller_update(meshap_sip_dlg_table_entry_t *dlg_entry)
{
   meshap_sip_dlg_info_t *callee_dlg;
   meshap_sip_dlg_info_t *caller_dlg;

   caller_dlg = &dlg_entry->caller_dlg;
   callee_dlg = &dlg_entry->callee_dlg;

   switch (caller_dlg->call_state)
   {
   case CALL_STATE_START:

      /*
       * caller cannot be in this state here since it has to initiate the call
       * it will always be in CALL_STATE_INVITE
       */
      break;

   case CALL_STATE_INVITE:
   case CALL_STATE_TRYING:
   case CALL_STATE_RINGING:
      if (callee_dlg->call_state == CALL_STATE_TRYING)
      {
         ___forward_trying(dlg_entry);
      }
      if (callee_dlg->call_state == CALL_STATE_RINGING)
      {
         ___ring_peer(dlg_entry);
      }
      else if (callee_dlg->call_state == CALL_STATE_STATUS_OK)
      {
         ___forward_client_success(dlg_entry);
      }
      else if (callee_dlg->call_state == CALL_STATE_STATUS_ERROR)
      {
         ___forward_client_error(dlg_entry);
      }
      break;

   case CALL_STATE_ESTABLISHED:
      if (callee_dlg->call_state == CALL_STATE_BYE)
      {
         ___forward_bye(dlg_entry, PENDING_STATUS_FOR_CALLER);
      }
      else if (callee_dlg->call_state == CALL_STATE_ESTABLISHED)
      {
         ___forward_client_success(dlg_entry);
      }
      break;

   case CALL_STATE_CANCEL:
      if (dlg_entry->callee_dlg.call_state == CALL_STATE_END)
      {
         ___forward_cancel_status(dlg_entry, PENDING_STATUS_FOR_CALLER);
      }
      else
      {
         ___forward_cancel_request(dlg_entry, PENDING_STATUS_FOR_CALLER);
      }
      break;

   case CALL_STATE_BYE:
      if (dlg_entry->callee_dlg.call_state == CALL_STATE_END)
      {
         ___forward_bye_success(dlg_entry, PENDING_STATUS_FOR_CALLER);
      }
      break;

   case CALL_STATE_END:
      if (dlg_entry->callee_dlg.call_state == CALL_STATE_END)
      {
         ___forward_bye_success(dlg_entry, PENDING_STATUS_FOR_CALLER);
      }
      else if (callee_dlg->call_state == CALL_STATE_STATUS_ERROR)
      {
         ___forward_client_error(dlg_entry);
      }
      break;

   case CALL_STATE_STATUS_ERROR:
      if (callee_dlg->call_state == CALL_STATE_STATUS_ERROR)
      {
         ___forward_client_error(dlg_entry);
      }
      break;
   }

   return 0;
}


static int _handle_pending_for_callee_update(meshap_sip_dlg_table_entry_t *dlg_entry)
{
   meshap_sip_dlg_info_t *callee_dlg;
   meshap_sip_dlg_info_t *caller_dlg;

   caller_dlg = &dlg_entry->caller_dlg;
   callee_dlg = &dlg_entry->callee_dlg;

   switch (callee_dlg->call_state)
   {
   case CALL_STATE_START:
      ___invite_callee(dlg_entry);
      break;

   case CALL_STATE_RINGING:
      if (caller_dlg->call_state == CALL_STATE_CANCEL)
      {
         ___forward_cancel_request(dlg_entry, PENDING_STATUS_FOR_CALLEE);
      }
      break;

   case CALL_STATE_STATUS_OK:
      if (caller_dlg->call_state == CALL_STATE_ESTABLISHED)
      {
         ___forward_ack(dlg_entry);
      }
      else if (caller_dlg->call_state == CALL_STATE_BYE)
      {
         ___forward_bye(dlg_entry, PENDING_STATUS_FOR_CALLEE);
      }
      break;

   case CALL_STATE_STATUS_ERROR:
      if (caller_dlg->call_state == CALL_STATE_END)
      {
         ___forward_ack(dlg_entry);
      }
      break;

   case CALL_STATE_ESTABLISHED:
      if (caller_dlg->call_state == CALL_STATE_BYE)
      {
         ___forward_bye(dlg_entry, PENDING_STATUS_FOR_CALLEE);
      }
      break;

   case CALL_STATE_BYE:
      if (caller_dlg->call_state == CALL_STATE_END)
      {
         ___forward_bye_success(dlg_entry, PENDING_STATUS_FOR_CALLEE);
      }
      break;

   case CALL_STATE_CANCEL:
      if (dlg_entry->caller_dlg.call_state == CALL_STATE_END)
      {
         ___forward_cancel_status(dlg_entry, PENDING_STATUS_FOR_CALLEE);
      }
      break;

   case CALL_STATE_END:
      if (dlg_entry->caller_dlg.call_state == CALL_STATE_BYE)
      {
         ___forward_bye(dlg_entry, PENDING_STATUS_FOR_CALLEE);
      }
      else if (caller_dlg->call_state == CALL_STATE_END)
      {
         ___forward_bye_success(dlg_entry, PENDING_STATUS_FOR_CALLEE);
      }
      break;
   }

   return 0;
}


static int ___forward_trying(meshap_sip_dlg_table_entry_t *dlg_entry)
{
   unsigned char         *buffer;
   unsigned char         *p;
   unsigned char         *ip_bytes;
   meshap_sip_dlg_info_t *caller_dlg;

   _sip_mesh_helper_bcast_call_dialog_status(dlg_entry, PENDING_STATUS_FOR_CALLER);

   if (!_IS_STA_LOCAL(dlg_entry->caller_info->flags))
   {
      return 0;
   }

   caller_dlg = &dlg_entry->caller_dlg;

   buffer = al_get_gen_2KB_buffer(AL_CONTEXT_SINGLE);
   p      = buffer;
   memset(buffer, 0, 2048);

   p += sprintf(p, "SIP/2.0 100 Trying\r\n");

   ip_bytes = dlg_entry->caller_info->ip_addr.bytes;
   p       += sprintf(p, "Via: SIP/2.0/UDP %d.%d.%d.%d:%d;branch=%s;rport=%d\r\n", ip_bytes[0], ip_bytes[1],
                      ip_bytes[2], ip_bytes[3],
                      dlg_entry->caller_info->rport,
                      dlg_entry->caller_dlg.branch,
                      dlg_entry->caller_info->rport);

   p += sprintf(p, "To: \"%s\"<sip:%s@%d.%d.%d.%d>\r\n", dlg_entry->caller_info->info.extn,
                dlg_entry->caller_info->info.extn,
                _config.server_ip[0], _config.server_ip[1],
                _config.server_ip[2], _config.server_ip[3]);

   p += sprintf(p, "From: \"%s\"<sip:%s@%d.%d.%d.%d>;tag=%s\r\n", dlg_entry->callee_info->info.extn,
                dlg_entry->callee_info->info.extn,
                _config.server_ip[0], _config.server_ip[1],
                _config.server_ip[2], _config.server_ip[3],
                caller_dlg->from_tag);

   p += sprintf(p, "Call-ID: %s\r\n", caller_dlg->call_id);

   p += sprintf(p, "CSeq: %d INVITE\r\n", caller_dlg->cseq);

   p += sprintf(p, "Content-Length: 0\r\n");

   memcpy(p, _CRLF, _CRLF_LEN);
   p += _CRLF_LEN;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "Forwarding trying status %s -> %s\n", dlg_entry->callee_info->info.extn,
                dlg_entry->caller_info->info.extn);

   _sip_packet_helper_send_packet(dlg_entry->caller_info, buffer, (int)(p - buffer));

   al_release_gen_2KB_buffer(AL_CONTEXT buffer);

   return CALL_STATE_TRYING;
}


static int ___ring_peer(meshap_sip_dlg_table_entry_t *dlg_entry)
{
   unsigned char         *buffer;
   unsigned char         *p;
   unsigned char         *ip_bytes;
   meshap_sip_dlg_info_t *caller_dlg;

   _sip_mesh_helper_bcast_call_dialog_status(dlg_entry, PENDING_STATUS_FOR_CALLER);

   if (!_IS_STA_LOCAL(dlg_entry->caller_info->flags))
   {
      return 0;
   }

   caller_dlg = &dlg_entry->caller_dlg;

   buffer = al_get_gen_2KB_buffer(AL_CONTEXT_SINGLE);
   p      = buffer;
   memset(buffer, 0, 2048);

   p += sprintf(p, "SIP/2.0 180 Ringing\r\n");

   ip_bytes = dlg_entry->caller_info->ip_addr.bytes;
   p       += sprintf(p, "Via: SIP/2.0/UDP %d.%d.%d.%d:%d;branch=%s;rport=%d\r\n", ip_bytes[0], ip_bytes[1],
                      ip_bytes[2], ip_bytes[3],
                      dlg_entry->caller_info->rport,
                      dlg_entry->caller_dlg.branch,
                      dlg_entry->caller_info->rport);

   p += sprintf(p, "Contact: <sip:%s@%d.%d.%d.%d:%d>\r\n", dlg_entry->caller_info->info.extn,
                _config.server_ip[0], _config.server_ip[1],
                _config.server_ip[2], _config.server_ip[3],
                _config.port);

   p += sprintf(p, "To: \"%s\"<sip:%s@%d.%d.%d.%d>;tag=%s\r\n", dlg_entry->callee_info->info.extn,
                dlg_entry->callee_info->info.extn,
                _config.server_ip[0], _config.server_ip[1],
                _config.server_ip[2], _config.server_ip[3],
                caller_dlg->to_tag);

   p += sprintf(p, "From: \"%s\"<sip:%s@%d.%d.%d.%d>;tag=%s\r\n", dlg_entry->caller_info->info.extn,
                dlg_entry->caller_info->info.extn,
                _config.server_ip[0], _config.server_ip[1],
                _config.server_ip[2], _config.server_ip[3],
                caller_dlg->from_tag);

   p += sprintf(p, "Call-ID: %s\r\n", caller_dlg->call_id);

   p += sprintf(p, "CSeq: %d INVITE\r\n", caller_dlg->cseq);

   p += sprintf(p, "Content-Length: 0\r\n");

   memcpy(p, _CRLF, _CRLF_LEN);
   p += _CRLF_LEN;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "Forwarding ringing status %s -> %s\n", dlg_entry->callee_info->info.extn,
                dlg_entry->caller_info->info.extn);

   _sip_packet_helper_send_packet(dlg_entry->caller_info, buffer, (int)(p - buffer));

   al_release_gen_2KB_buffer(AL_CONTEXT buffer);

   return CALL_STATE_RINGING;
}


static int ___forward_client_success(meshap_sip_dlg_table_entry_t *dlg_entry)
{
   unsigned char         *buffer;
   unsigned char         *p;
   unsigned char         *ip_bytes;
   meshap_sip_dlg_info_t *caller_dlg;

   _sip_mesh_helper_bcast_call_dialog_status(dlg_entry, PENDING_STATUS_FOR_CALLER);

   if (!_IS_STA_LOCAL(dlg_entry->caller_info->flags))
   {
      return 0;
   }

   caller_dlg = &dlg_entry->caller_dlg;

   buffer = al_get_gen_2KB_buffer(AL_CONTEXT_SINGLE);
   p      = buffer;
   memset(buffer, 0, 2048);

   memcpy(p, _SIP_STATUS_200_STR, _SIP_STATUS_200_STR_LEN);
   p += _SIP_STATUS_200_STR_LEN;

   ip_bytes = dlg_entry->caller_info->ip_addr.bytes;
   p       += sprintf(p, "Via: SIP/2.0/UDP %d.%d.%d.%d:%d;branch=%s;rport=%d\r\n", ip_bytes[0], ip_bytes[1],
                      ip_bytes[2], ip_bytes[3],
                      dlg_entry->caller_info->rport,
                      caller_dlg->branch,
                      dlg_entry->caller_info->rport);

   p += sprintf(p, "Contact: <sip:%s@%d.%d.%d.%d:%d>\r\n", dlg_entry->caller_info->info.extn,
                _config.server_ip[0], _config.server_ip[1],
                _config.server_ip[2], _config.server_ip[3],
                _config.port);

   p += sprintf(p, "To: \"%s\"<sip:%s@%d.%d.%d.%d>;tag=%s\r\n", dlg_entry->caller_info->info.extn,
                dlg_entry->caller_info->info.extn,
                _config.server_ip[0], _config.server_ip[1],
                _config.server_ip[2], _config.server_ip[3],
                caller_dlg->to_tag);


   p += sprintf(p, "From: \"%s\"<sip:%s@%d.%d.%d.%d>;tag=%s\r\n", dlg_entry->callee_info->info.extn,
                dlg_entry->callee_info->info.extn,
                _config.server_ip[0], _config.server_ip[1],
                _config.server_ip[2], _config.server_ip[3],
                caller_dlg->from_tag);

   p += sprintf(p, "Call-ID: %s\r\n", caller_dlg->call_id);

   p += sprintf(p, "CSeq: %d INVITE\r\n", caller_dlg->cseq);

   p += sprintf(p, "Content-Type: application/sdp\r\n");

   p += sprintf(p, "Content-Length: %d\r\n", (int)strlen(dlg_entry->extra_info));

   memcpy(p, _CRLF, _CRLF_LEN);
   p += _CRLF_LEN;

   p += sprintf(p, "%s", dlg_entry->extra_info);

   memcpy(p, _CRLF, _CRLF_LEN);
   p += _CRLF_LEN;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "Forwarding ok status %s -> %s\n", dlg_entry->callee_info->info.extn,
                dlg_entry->caller_info->info.extn);

   _sip_packet_helper_send_packet(dlg_entry->caller_info, buffer, (int)(p - buffer));

   al_release_gen_2KB_buffer(AL_CONTEXT buffer);

   return 0;
}


static int ___forward_client_error(meshap_sip_dlg_table_entry_t *dlg_entry)
{
   unsigned char         *buffer;
   unsigned char         *p;
   unsigned char         *ip_bytes;
   meshap_sip_dlg_info_t *caller_dlg;

   dlg_entry->caller_dlg.call_state = CALL_STATE_STATUS_ERROR;
   dlg_entry->callee_dlg.call_state = CALL_STATE_STATUS_ERROR;

   _sip_mesh_helper_bcast_call_dialog_status(dlg_entry, PENDING_STATUS_FOR_CALLER);

   if (!_IS_STA_LOCAL(dlg_entry->caller_info->flags))
   {
      return 0;
   }

   caller_dlg = &dlg_entry->caller_dlg;

   buffer = al_get_gen_2KB_buffer(AL_CONTEXT_SINGLE);
   p      = buffer;
   memset(buffer, 0, 2048);

   p += sprintf(p, "%s\r\n", dlg_entry->extra_info);

   ip_bytes = dlg_entry->caller_info->ip_addr.bytes;
   p       += sprintf(p, "Via: SIP/2.0/UDP %d.%d.%d.%d:%d;branch=%s;rport=%d\r\n", ip_bytes[0], ip_bytes[1],
                      ip_bytes[2], ip_bytes[3],
                      dlg_entry->caller_info->rport,
                      caller_dlg->branch,
                      dlg_entry->caller_info->rport);

   p += sprintf(p, "To: \"%s\"<sip:%s@%d.%d.%d.%d>;tag=%s\r\n", dlg_entry->callee_info->info.extn,
                dlg_entry->callee_info->info.extn,
                _config.server_ip[0], _config.server_ip[1],
                _config.server_ip[2], _config.server_ip[3],
                caller_dlg->to_tag);


   p += sprintf(p, "From: \"%s\"<sip:%s@%d.%d.%d.%d>;tag=%s\r\n", dlg_entry->caller_info->info.extn,
                dlg_entry->caller_info->info.extn,
                _config.server_ip[0], _config.server_ip[1],
                _config.server_ip[2], _config.server_ip[3],
                caller_dlg->from_tag);

   p += sprintf(p, "Call-ID: %s\r\n", caller_dlg->call_id);

   p += sprintf(p, "CSeq: %d INVITE\r\n", caller_dlg->cseq);

   p += sprintf(p, "Content-Length: 0\r\n");

   memcpy(p, _CRLF, _CRLF_LEN);
   p += _CRLF_LEN;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "Forwarding unavailable status %s -> %s\n", dlg_entry->callee_info->info.extn,
                dlg_entry->caller_info->info.extn);

   _sip_packet_helper_send_packet(dlg_entry->caller_info, buffer, (int)(p - buffer));

   al_release_gen_2KB_buffer(AL_CONTEXT buffer);

   return 0;
}


static int ___forward_bye(meshap_sip_dlg_table_entry_t *dlg_entry, unsigned char pending_status)
{
   unsigned char         *buffer;
   unsigned char         *p;
   unsigned char         *ip_bytes;
   meshap_sip_dlg_info_t *sender_dlg;
   meshap_sip_sta_info_t *sender_sta;
   meshap_sip_dlg_info_t *rcvr_dlg;
   meshap_sip_sta_info_t *rcvr_sta;

   _sip_mesh_helper_bcast_call_dialog_status(dlg_entry, pending_status);

   sender_dlg = &dlg_entry->caller_dlg;
   sender_sta = dlg_entry->caller_info;
   rcvr_dlg   = &dlg_entry->callee_dlg;
   rcvr_sta   = dlg_entry->callee_info;

   if (pending_status == PENDING_STATUS_FOR_CALLER)
   {
      sender_dlg = &dlg_entry->callee_dlg;
      sender_sta = dlg_entry->callee_info;
      rcvr_dlg   = &dlg_entry->caller_dlg;
      rcvr_sta   = dlg_entry->caller_info;
   }

   if (!_IS_STA_LOCAL(rcvr_sta->flags))
   {
      return 0;
   }

   buffer = al_get_gen_2KB_buffer(AL_CONTEXT_SINGLE);
   p      = buffer;
   memset(buffer, 0, 2048);

   ip_bytes = rcvr_sta->ip_addr.bytes;
   p       += sprintf(p, "BYE sip:%s@%d.%d.%d.%d:%d", rcvr_sta->info.extn, ip_bytes[0],
                      ip_bytes[1], ip_bytes[2], ip_bytes[3],
                      rcvr_sta->rport);

   if ((pending_status == PENDING_STATUS_FOR_CALLEE) && (strlen(rcvr_dlg->rinstance) > 0))
   {
      p += sprintf(p, ";rinstance=%s SIP/2.0\r\n", rcvr_dlg->rinstance);
   }
   else
   {
      p += sprintf(p, " SIP/2.0\r\n");
   }

   p += sprintf(p, "Via: SIP/2.0/UDP %d.%d.%d.%d:%d;branch=%s;rport\r\n", _config.server_ip[0],
                _config.server_ip[1],
                _config.server_ip[2],
                _config.server_ip[3],
                _config.port,
                rcvr_dlg->branch);
   p += sprintf(p, "Max-Forwards: 70\r\n");

   if (pending_status == PENDING_STATUS_FOR_CALLEE)
   {
      p += sprintf(p, "Contact: <sip:%s@%d.%d.%d.%d:%d>\r\n", sender_sta->info.extn, _config.server_ip[0],
                   _config.server_ip[1], _config.server_ip[2],
                   _config.server_ip[3], _config.port);

      p += sprintf(p, "To: \"%s\"<sip:%s@%d.%d.%d.%d>;tag=%s\r\n", rcvr_sta->info.extn, rcvr_sta->info.extn,
                   _config.server_ip[0], _config.server_ip[1],
                   _config.server_ip[2], _config.server_ip[3],
                   rcvr_dlg->to_tag);

      p += sprintf(p, "From: \"%s\"<sip:%s@%d.%d.%d.%d>;tag=%s\r\n", sender_sta->info.extn, sender_sta->info.extn,
                   _config.server_ip[0], _config.server_ip[1],
                   _config.server_ip[2], _config.server_ip[3],
                   rcvr_dlg->from_tag);
   }
   else
   {
      p += sprintf(p, "Contact: <sip:%s@%d.%d.%d.%d:%d>\r\n", rcvr_sta->info.extn, _config.server_ip[0],
                   _config.server_ip[1], _config.server_ip[2],
                   _config.server_ip[3], _config.port);

      p += sprintf(p, "To: \"%s\"<sip:%s@%d.%d.%d.%d>;tag=%s\r\n", sender_sta->info.extn, sender_sta->info.extn,
                   _config.server_ip[0], _config.server_ip[1],
                   _config.server_ip[2], _config.server_ip[3],
                   rcvr_dlg->from_tag);

      p += sprintf(p, "From: \"%s\"<sip:%s@%d.%d.%d.%d>;tag=%s\r\n", rcvr_sta->info.extn, rcvr_sta->info.extn,
                   _config.server_ip[0], _config.server_ip[1],
                   _config.server_ip[2], _config.server_ip[3],
                   rcvr_dlg->to_tag);
   }

   p += sprintf(p, "Call-ID: %s\r\n", rcvr_dlg->call_id);

   p += sprintf(p, "CSeq: %d BYE\r\n", rcvr_dlg->cseq);

   p += sprintf(p, "Content-Length: 0\r\n");

   memcpy(p, _CRLF, _CRLF_LEN);
   p += _CRLF_LEN;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "Forwarding Bye request %s -> %s\n", sender_sta->info.extn,
                rcvr_sta->info.extn);

   _sip_packet_helper_send_packet(rcvr_sta, buffer, (int)(p - buffer));

   al_release_gen_2KB_buffer(AL_CONTEXT buffer);

   return 0;
}


static int ___forward_cancel_status(meshap_sip_dlg_table_entry_t *dlg_entry, unsigned char pending_status)
{
   unsigned char         *buffer;
   unsigned char         *p;
   unsigned char         *ip_bytes;
   meshap_sip_dlg_info_t *sender_dlg;
   meshap_sip_sta_info_t *sender_sta;
   meshap_sip_dlg_info_t *rcvr_dlg;
   meshap_sip_sta_info_t *rcvr_sta;

   _sip_mesh_helper_bcast_call_dialog_status(dlg_entry, pending_status);

   sender_dlg = &dlg_entry->callee_dlg;
   sender_sta = dlg_entry->callee_info;
   rcvr_dlg   = &dlg_entry->caller_dlg;
   rcvr_sta   = dlg_entry->caller_info;

   if (pending_status == PENDING_STATUS_FOR_CALLEE)
   {
      sender_dlg = &dlg_entry->caller_dlg;
      sender_sta = dlg_entry->caller_info;
      rcvr_dlg   = &dlg_entry->callee_dlg;
      rcvr_sta   = dlg_entry->callee_info;
   }

   if (!_IS_STA_LOCAL(rcvr_sta->flags))
   {
      return 0;
   }

   buffer = al_get_gen_2KB_buffer(AL_CONTEXT_SINGLE);
   p      = buffer;
   memset(buffer, 0, 2048);

   memcpy(p, dlg_entry->extra_info, strlen(dlg_entry->extra_info));
   p += strlen(dlg_entry->extra_info);

   ip_bytes = rcvr_sta->ip_addr.bytes;
   p       += sprintf(p, "Via: SIP/2.0/UDP %d.%d.%d.%d:%d;branch=%s;rport=%d\r\n", ip_bytes[0], ip_bytes[1],
                      ip_bytes[2], ip_bytes[3],
                      rcvr_sta->rport,
                      rcvr_dlg->branch,
                      rcvr_sta->rport);

   p += sprintf(p, "Contact: <sip:%s@%d.%d.%d.%d:%d>\r\n", sender_sta->info.extn, _config.server_ip[0],
                _config.server_ip[1], _config.server_ip[2],
                _config.server_ip[3], _config.port);

   p += sprintf(p, "To: \"%s\"<sip:%s@%d.%d.%d.%d>;tag=%s\r\n", rcvr_sta->info.extn, rcvr_sta->info.extn,
                _config.server_ip[0], _config.server_ip[1],
                _config.server_ip[2], _config.server_ip[3],
                rcvr_dlg->to_tag);


   p += sprintf(p, "From: \"%s\"<sip:%s@%d.%d.%d.%d>;tag=%s\r\n", sender_sta->info.extn, sender_sta->info.extn,
                _config.server_ip[0], _config.server_ip[1],
                _config.server_ip[2], _config.server_ip[3],
                rcvr_dlg->from_tag);

   p += sprintf(p, "Call-ID: %s\r\n", rcvr_dlg->call_id);

   p += sprintf(p, "CSeq: %d CANCEL\r\n", rcvr_dlg->cseq);

   p += sprintf(p, "Content-Length: 0\r\n");

   memcpy(p, _CRLF, _CRLF_LEN);
   p += _CRLF_LEN;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "Forwarding ok status %s -> %s\n", sender_sta->info.extn,
                rcvr_sta->info.extn);

   _sip_packet_helper_send_packet(rcvr_sta, buffer, (int)(p - buffer));

   al_release_gen_2KB_buffer(AL_CONTEXT buffer);

   return 0;
}


static int ___forward_cancel_request(meshap_sip_dlg_table_entry_t *dlg_entry, unsigned char pending_status)
{
   unsigned char         *buffer;
   unsigned char         *p;
   unsigned char         *ip_bytes;
   meshap_sip_dlg_info_t *sender_dlg;
   meshap_sip_sta_info_t *sender_sta;
   meshap_sip_dlg_info_t *rcvr_dlg;
   meshap_sip_sta_info_t *rcvr_sta;

   sender_dlg = &dlg_entry->caller_dlg;
   sender_sta = dlg_entry->caller_info;
   rcvr_dlg   = &dlg_entry->callee_dlg;
   rcvr_sta   = dlg_entry->callee_info;

   if (pending_status == PENDING_STATUS_FOR_CALLER)
   {
      sender_dlg = &dlg_entry->callee_dlg;
      sender_sta = dlg_entry->callee_info;
      rcvr_dlg   = &dlg_entry->caller_dlg;
      rcvr_sta   = dlg_entry->caller_info;
   }

   _sip_mesh_helper_bcast_call_dialog_status(dlg_entry, pending_status);

   if (!_IS_STA_LOCAL(rcvr_sta->flags))
   {
      return 0;
   }

   buffer = al_get_gen_2KB_buffer(AL_CONTEXT_SINGLE);
   p      = buffer;
   memset(buffer, 0, 2048);

   ip_bytes = rcvr_sta->ip_addr.bytes;
   p       += sprintf(p, "CANCEL sip:%s@%d.%d.%d.%d:%d", rcvr_sta->info.extn, ip_bytes[0],
                      ip_bytes[1], ip_bytes[2], ip_bytes[3],
                      rcvr_sta->rport);

   if ((pending_status == PENDING_STATUS_FOR_CALLEE) && (strlen(rcvr_dlg->rinstance) > 0))
   {
      p += sprintf(p, ";rinstance=%s SIP/2.0\r\n", rcvr_dlg->rinstance);
   }
   else
   {
      p += sprintf(p, " SIP/2.0\r\n");
   }

   p += sprintf(p, "Via: SIP/2.0/UDP %d.%d.%d.%d:%d;branch=%s;rport\r\n", _config.server_ip[0],
                _config.server_ip[1],
                _config.server_ip[2],
                _config.server_ip[3],
                _config.port,
                rcvr_dlg->branch);

   p += sprintf(p, "To: \"%s\"<sip:%s@%d.%d.%d.%d>\r\n", rcvr_sta->info.extn, rcvr_sta->info.extn,
                _config.server_ip[0], _config.server_ip[1],
                _config.server_ip[2], _config.server_ip[3]);

   p += sprintf(p, "From: \"%s\"<sip:%s@%d.%d.%d.%d>;tag=%s\r\n", sender_sta->info.extn, sender_sta->info.extn,
                _config.server_ip[0], _config.server_ip[1],
                _config.server_ip[2], _config.server_ip[3],
                rcvr_dlg->from_tag);

   p += sprintf(p, "Call-ID: %s\r\n", rcvr_dlg->call_id);

   p += sprintf(p, "CSeq: %d CANCEL\r\n", rcvr_dlg->cseq);

   p += sprintf(p, "Content-Length: 0\r\n");

   memcpy(p, _CRLF, _CRLF_LEN);
   p += _CRLF_LEN;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "Forwarding Cancel request %s -> %s\n", sender_sta->info.extn,
                rcvr_sta->info.extn);

   _sip_packet_helper_send_packet(rcvr_sta, buffer, (int)(p - buffer));

   al_release_gen_2KB_buffer(AL_CONTEXT buffer);

   return 0;
}


static int ___forward_bye_success(meshap_sip_dlg_table_entry_t *dlg_entry, unsigned char pending_status)
{
   unsigned char         *buffer;
   unsigned char         *p;
   unsigned char         *ip_bytes;
   meshap_sip_dlg_info_t *sender_dlg;
   meshap_sip_sta_info_t *sender_sta;
   meshap_sip_dlg_info_t *rcvr_dlg;
   meshap_sip_sta_info_t *rcvr_sta;

   _sip_mesh_helper_bcast_call_dialog_status(dlg_entry, pending_status);

   sender_dlg = &dlg_entry->caller_dlg;
   sender_sta = dlg_entry->caller_info;
   rcvr_dlg   = &dlg_entry->callee_dlg;
   rcvr_sta   = dlg_entry->callee_info;

   if (pending_status == PENDING_STATUS_FOR_CALLER)
   {
      sender_dlg = &dlg_entry->callee_dlg;
      sender_sta = dlg_entry->callee_info;
      rcvr_dlg   = &dlg_entry->caller_dlg;
      rcvr_sta   = dlg_entry->caller_info;
   }

   if (!_IS_STA_LOCAL(rcvr_sta->flags))
   {
      return 0;
   }

   buffer = al_get_gen_2KB_buffer(AL_CONTEXT_SINGLE);
   p      = buffer;
   memset(buffer, 0, 2048);

   memcpy(p, _SIP_STATUS_200_STR, _SIP_STATUS_200_STR_LEN);
   p += _SIP_STATUS_200_STR_LEN;

   ip_bytes = rcvr_sta->ip_addr.bytes;
   p       += sprintf(p, "Via: SIP/2.0/UDP %d.%d.%d.%d:%d;branch=%s;rport=%d\r\n", ip_bytes[0], ip_bytes[1],
                      ip_bytes[2], ip_bytes[3],
                      rcvr_sta->rport,
                      rcvr_dlg->branch,
                      rcvr_sta->rport);

   p += sprintf(p, "To: \"%s\"<sip:%s@%d.%d.%d.%d>;tag=%s\r\n", rcvr_sta->info.extn, rcvr_sta->info.extn,
                _config.server_ip[0], _config.server_ip[1],
                _config.server_ip[2], _config.server_ip[3],
                rcvr_dlg->to_tag);


   p += sprintf(p, "From: \"%s\"<sip:%s@%d.%d.%d.%d>;tag=%s\r\n", sender_sta->info.extn, sender_sta->info.extn,
                _config.server_ip[0], _config.server_ip[1],
                _config.server_ip[2], _config.server_ip[3],
                rcvr_dlg->from_tag);

   p += sprintf(p, "Call-ID: %s\r\n", rcvr_dlg->call_id);

   p += sprintf(p, "CSeq: %d BYE\r\n", rcvr_dlg->cseq);

   p += sprintf(p, "Content-Length: 0\r\n");

   memcpy(p, _CRLF, _CRLF_LEN);
   p += _CRLF_LEN;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "Forwarding bye status %s -> %s\n", sender_sta->info.extn,
                rcvr_sta->info.extn);

   _sip_packet_helper_send_packet(rcvr_sta, buffer, (int)(p - buffer));

   al_release_gen_2KB_buffer(AL_CONTEXT buffer);

   return 0;
}


static int ___invite_callee(meshap_sip_dlg_table_entry_t *dlg_entry)
{
   meshap_sip_dlg_info_t *callee_dlg;
   unsigned char         *buffer;
   unsigned char         *p;
   unsigned char         *ip_bytes;

   _sip_mesh_helper_bcast_call_dialog_status(dlg_entry, PENDING_STATUS_FOR_CALLEE);

   if (!_IS_STA_LOCAL(dlg_entry->callee_info->flags))
   {
      return 0;
   }

   callee_dlg = &dlg_entry->callee_dlg;
   buffer     = al_get_gen_2KB_buffer(AL_CONTEXT_SINGLE);
   p          = buffer;
   memset(buffer, 0, 2048);

   ip_bytes = dlg_entry->callee_info->ip_addr.bytes;
   p       += sprintf(p, "INVITE sip:%s@%d.%d.%d.%d:%d;rinstance=%s SIP/2.0\r\n", dlg_entry->callee_info->info.extn,
                      ip_bytes[0], ip_bytes[1],
                      ip_bytes[2], ip_bytes[3],
                      dlg_entry->callee_info->rport,
                      callee_dlg->rinstance);

   p += sprintf(p, "Via: SIP/2.0/UDP %d.%d.%d.%d:%d;branch=%s;rport\r\n", _config.server_ip[0],
                _config.server_ip[1],
                _config.server_ip[2],
                _config.server_ip[3],
                _config.port,
                callee_dlg->branch);
   p += sprintf(p, "Max-Forwards: 70\r\n");

   p += sprintf(p, "Contact: <sip:%s@%d.%d.%d.%d:%d>\r\n", dlg_entry->caller_info->info.extn,
                _config.server_ip[0], _config.server_ip[1],
                _config.server_ip[2], _config.server_ip[3],
                _config.port);

   p += sprintf(p, "To: \"%s\"<sip:%s@%d.%d.%d.%d>\r\n", dlg_entry->callee_info->info.extn,
                dlg_entry->callee_info->info.extn,
                _config.server_ip[0], _config.server_ip[1],
                _config.server_ip[2], _config.server_ip[3]);

   p += sprintf(p, "From: \"%s\"<sip:%s@%d.%d.%d.%d>;tag=%s\r\n", dlg_entry->caller_info->info.extn,
                dlg_entry->caller_info->info.extn,
                _config.server_ip[0], _config.server_ip[1],
                _config.server_ip[2], _config.server_ip[3],
                callee_dlg->from_tag);

   p += sprintf(p, "Call-ID: %s\r\n", callee_dlg->call_id);

   p += sprintf(p, "CSeq: %d INVITE\r\n", callee_dlg->cseq);

   p += sprintf(p, "Content-Type: application/sdp\r\n");

   p += sprintf(p, "Content-Length: %d\r\n", (int)strlen(dlg_entry->extra_info));

   memcpy(p, _CRLF, _CRLF_LEN);
   p += _CRLF_LEN;

   p += sprintf(p, "%s", dlg_entry->extra_info);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "Inviting %s -> %s\n", dlg_entry->caller_info->info.extn,
                dlg_entry->callee_info->info.extn);

   _sip_packet_helper_send_packet(dlg_entry->callee_info, buffer, (int)(p - buffer));

   memset(dlg_entry->extra_info, 0, 1024);

   al_release_gen_2KB_buffer(AL_CONTEXT buffer);

   return 0;
}


static int ___forward_ack(meshap_sip_dlg_table_entry_t *dlg_entry)
{
   unsigned char         *buffer;
   unsigned char         *p;
   unsigned char         *ip_bytes;
   meshap_sip_dlg_info_t *callee_dlg;

   _sip_mesh_helper_bcast_call_dialog_status(dlg_entry, PENDING_STATUS_FOR_CALLEE);

   if (!_IS_STA_LOCAL(dlg_entry->callee_info->flags))
   {
      return 0;
   }

   callee_dlg = &dlg_entry->callee_dlg;

   buffer = al_get_gen_2KB_buffer(AL_CONTEXT_SINGLE);
   p      = buffer;
   memset(buffer, 0, 2048);

   ip_bytes = dlg_entry->callee_info->ip_addr.bytes;
   p       += sprintf(p, "ACK sip:%s@%d.%d.%d.%d:%d", dlg_entry->callee_info->info.extn,
                      ip_bytes[0], ip_bytes[1],
                      ip_bytes[2], ip_bytes[3],
                      dlg_entry->callee_info->rport);

   if (strlen(callee_dlg->rinstance) > 0)
   {
      p += sprintf(p, ";rinstance=%s ", callee_dlg->rinstance);
   }

   p += sprintf(p, "SIP/2.0\r\n");

   p += sprintf(p, "Via: SIP/2.0/UDP %d.%d.%d.%d:%d;branch=%s;rport\r\n", _config.server_ip[0],
                _config.server_ip[1],
                _config.server_ip[2],
                _config.server_ip[3],
                _config.port,
                callee_dlg->branch);
   p += sprintf(p, "Max-Forwards: 70\r\n");

   p += sprintf(p, "Contact: <sip:%s@%d.%d.%d.%d:%d>\r\n", dlg_entry->caller_info->info.extn,
                _config.server_ip[0], _config.server_ip[1],
                _config.server_ip[2], _config.server_ip[3],
                _config.port);

   p += sprintf(p, "To: \"%s\"<sip:%s@%d.%d.%d.%d>;tag=%s\r\n", dlg_entry->callee_info->info.extn,
                dlg_entry->callee_info->info.extn,
                _config.server_ip[0], _config.server_ip[1],
                _config.server_ip[2], _config.server_ip[3],
                callee_dlg->to_tag);


   p += sprintf(p, "From: \"%s\"<sip:%s@%d.%d.%d.%d>;tag=%s\r\n", dlg_entry->caller_info->info.extn,
                dlg_entry->caller_info->info.extn,
                _config.server_ip[0], _config.server_ip[1],
                _config.server_ip[2], _config.server_ip[3],
                callee_dlg->from_tag);

   p += sprintf(p, "Call-ID: %s\r\n", callee_dlg->call_id);

   p += sprintf(p, "CSeq: %d ACK\r\n", callee_dlg->cseq);

   p += sprintf(p, "Content-Length: 0\r\n");

   memcpy(p, _CRLF, _CRLF_LEN);
   p += _CRLF_LEN;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "Forwarding Ack request %s -> %s\n", dlg_entry->caller_info->info.extn,
                dlg_entry->callee_info->info.extn);

   _sip_packet_helper_send_packet(dlg_entry->callee_info, buffer, (int)(p - buffer));

   al_release_gen_2KB_buffer(AL_CONTEXT buffer);

   return 0;
}
