/********************************************************************************
* MeshDynamics
* --------------
* File     : sip_packet_parser.c
* Comments : Parser for the SIP protocol
* Created  : 8/26/2008
* Author   : Abhijit Ayarekar
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  0  |8/26/2008 | Created.                                        | Abhijit|
* -----------------------------------------------------------------------------
********************************************************************************/

#include <linux/string.h>

#include "al.h"
#include "sip_packet_parser.h"
#include "conf_common.h"

enum
{
   SIP_TOKEN_VERSION=1,
   SIP_TOKEN_REGISTER,
   SIP_TOKEN_VIA,
   SIP_TOKEN_CONTACT,
   SIP_TOKEN_TO,
   SIP_TOKEN_FROM,
   SIP_TOKEN_CALL_ID,
   SIP_TOKEN_CSEQ,
   SIP_TOKEN_EXPIRES,
   SIP_TOKEN_PROXY_AUTHORIZATION,
   SIP_TOKEN_SUBSCRIBE,
   SIP_TOKEN_INVITE,
   SIP_TOKEN_CONTENT_TYPE,
   SIP_TOKEN_CONTENT_LENGTH,
   SIP_TOKEN_200,
   SIP_TOKEN_180,
   SIP_TOKEN_ACK,
   SIP_TOKEN_BYE,
   SIP_TOKEN_480,
   SIP_TOKEN_CANCEL,
   SIP_TOKEN_487,
   SIP_TOKEN_486,
};

static struct
{
   const char *token;
   int        code;
}
_token_info[] =
{
   { "SIP/2.0",             SIP_TOKEN_VERSION             },
   { "REGISTER",            SIP_TOKEN_REGISTER            },
   { "Via",                 SIP_TOKEN_VIA                 },
   { "Contact",             SIP_TOKEN_CONTACT             },
   { "To",                  SIP_TOKEN_TO                  },
   { "From",                SIP_TOKEN_FROM                },
   { "Call-ID",             SIP_TOKEN_CALL_ID             },
   { "CSeq",                SIP_TOKEN_CSEQ                },
   { "Expires",             SIP_TOKEN_EXPIRES             },
   { "Proxy-Authorization", SIP_TOKEN_PROXY_AUTHORIZATION },
   { "SUBSCRIBE",           SIP_TOKEN_SUBSCRIBE           },
   { "INVITE",              SIP_TOKEN_INVITE              },
   { "Content-Type",        SIP_TOKEN_CONTENT_TYPE        },
   { "Content-Length",      SIP_TOKEN_CONTENT_LENGTH      },
   { "200",                 SIP_TOKEN_200                 },
   { "180",                 SIP_TOKEN_180                 },
   { "ACK",                 SIP_TOKEN_ACK                 },
   { "BYE",                 SIP_TOKEN_BYE                 },
   { "480",                 SIP_TOKEN_480                 },
   { "CANCEL",              SIP_TOKEN_CANCEL              },
   { "487",                 SIP_TOKEN_487                 },
   { "486",                 SIP_TOKEN_486                 },
};

static struct
{
   const char *id;
   int        len;
}
_sip_ids[] =
{
   { "SIP/2.0",   7 },
   { "REGISTER",  8 },
   { "INVITE",    6 },
   { "BYE",       3 },
   { "CANCEL",    6 },
   { "ACK",       3 },
   { "SUBSCRIBE", 9 },
};

struct _priv_data
{
   sip_header_info_t *parsed_header;
   unsigned char     current_line[256];
   unsigned char     current_token[256];
   unsigned char     *line_ptr;
   unsigned char     current_token_code;
};

typedef struct _priv_data   _priv_data_t;

static int _get_next_token_from_data(unsigned char *data, unsigned char *seps, int sep_count, unsigned char *token)
{
   unsigned char *p;
   unsigned char *s;
   int           i;

   p = data;
   s = token;

   while (*p == ' ' || *p == '\t')
   {
      p++;
   }
   while (*p != 0)
   {
      for (i = 0; i < sep_count; i++)
      {
         if (*p == seps[i])
         {
            p++;
            goto label_ret;
         }
      }

/*
 *              if(*p >= 'A' && *p <= 'Z')
 * s = (*p-'A')+'a';
 *              else
 */
      *s = *p;
      s++;
      p++;
   }

label_ret:
   *s = 0;
   return (int)(p - data);
}


static int _get_next_token(_priv_data_t *priv, unsigned char *seps, int sep_count)
{
   unsigned char *p;
   unsigned char *s;
   int           i;
   int           bytes_read;

   memset(priv->current_token, 0, 256);
   priv->current_token_code = 0;

   p = priv->line_ptr;
   s = &priv->current_token[0];

   while (*p == ' ' || *p == '\t')
   {
      p++;
   }
   while (*p != 0)
   {
      for (i = 0; i < sep_count; i++)
      {
         if (*p == seps[i])
         {
            p++;
            goto label_ret;
         }
      }

/*
 *              if(*p >= 'A' && *p <= 'Z')
 * s = (*p-'A')+'a';
 *              else
 */
      *s = *p;
      s++;
      p++;
   }

label_ret:
   *s = 0;
   for (i = 0; i < sizeof(_token_info) / sizeof(*_token_info); i++)
   {
      if (strcmp(priv->current_token, _token_info[i].token) == 0)
      {
         priv->current_token_code = _token_info[i].code;
         break;
      }
   }

   bytes_read     = (int)(p - priv->line_ptr);
   priv->line_ptr = p;
   return bytes_read;
}


static int _get_line(_priv_data_t *priv, unsigned char *data)
{
   unsigned char *p;
   unsigned char *s;

   p = data;

   memset(priv->current_line, 0, 256);

   priv->line_ptr = &priv->current_line[0];
   s = &priv->current_line[0];

   while (*p == ' ' || *p == '\t')
   {
      p++;
   }

   while (*p != '\r' && *p != '\n' && *p != 0)
   {
      *s = *p;
      p++;
      s++;
   }

   while ((*p == '\r' || *p == '\n') && *p != 0)
   {
      p++;
   }

   *s = 0;

   return (int)(p - data);
}


static int _parse_ip_addr(_priv_data_t *priv, unsigned char *data, unsigned char *out_ip)
{
   unsigned char *p;
   unsigned char ip_str[3];
   int           octet;
   int           val;

   p = data;
   for (octet = 0; octet < 4; octet++)
   {
      memset(ip_str, 0, 3);
      p += _get_next_token_from_data(p, ".:", 2, ip_str);
      al_conf_atoi(ip_str, &val);
      out_ip[octet] = val;
   }

/*
 *      octet		= 0;
 *      bytes_read	= 0;
 *      p			= data;
 *
 *      while(bytes_read < 15 && octet < 4) {
 *
 *              i = 0;
 *              memset(ip_str, 0, 3);
 *              while(*p != ' ' && *p != '.' && i < 3) {
 *                      ip_str[i] = *p;
 *                      i++;
 *                      p++;
 *                      bytes_read++;
 *              }
 *
 *              p++;	//skip . or space
 *              bytes_read++;
 *
 *              al_conf_atoi(ip_str, &val);
 *              out_ip[octet++] = val;
 *
 *      }
 */
   return 0;
}


static int _parse_request_line(_priv_data_t *priv)
{
   unsigned char data[256];
   unsigned char field[256];
   unsigned char field_name[32];
   unsigned char *p;

   //sip:104@192.168.254.169:5060;rinstance=4dceee18ae9bf5f8
   memset(data, 0, 256);
   strcpy(data, priv->current_token);

   p = data;
   memset(field, 0, 256);
   p += _get_next_token_from_data(p, "@", 1, field);            //skip sip:104@		192.168.254.169:5060;rinstance=4dceee18ae9bf5f8

   memset(field, 0, 256);
   p += _get_next_token_from_data(p, ";", 1, field);       //get ip				rinstance=4dceee18ae9bf5f8
   _parse_ip_addr(priv, field, priv->parsed_header->server_ip);

   memset(field, 0, 256);
   p += _get_next_token_from_data(p, ";", 1, field);            //get rinstance

   if (strlen(field) <= 0)
   {
      return -1;
   }

   p = field;
   memset(field_name, 0, 32);
   p += _get_next_token_from_data(p, "=", 1, field_name);

   if (strcmp(field_name, "rinstance") == 0)
   {
      p += _get_next_token_from_data(p, NULL, 0, priv->parsed_header->rinstance);
   }

   return 0;
}


static int _parse_sip_type_subtype(_priv_data_t *priv)
{
   int sub_type;

   switch (priv->current_token_code)
   {
   case SIP_TOKEN_VERSION:
      priv->parsed_header->type = SIP_TYPE_STATUS;
      _get_next_token(priv, " ", 1);
      al_conf_atoi(priv->current_token, &sub_type);
      priv->parsed_header->sub_type     = sub_type;
      priv->parsed_header->status_class = sub_type / 100;
      break;

   case SIP_TOKEN_REGISTER:
      priv->parsed_header->type     = SIP_TYPE_REQUEST;
      priv->parsed_header->sub_type = SIP_SUB_TYPE_REQUEST_REGISTER;
      break;

   case SIP_TOKEN_SUBSCRIBE:
      priv->parsed_header->type     = SIP_TYPE_REQUEST;
      priv->parsed_header->sub_type = SIP_SUB_TYPE_REQUEST_SUBSCRIBE;
      break;

   case SIP_TOKEN_INVITE:
      priv->parsed_header->type     = SIP_TYPE_REQUEST;
      priv->parsed_header->sub_type = SIP_SUB_TYPE_REQUEST_INVITE;
      break;

   case SIP_TOKEN_ACK:
      priv->parsed_header->type     = SIP_TYPE_REQUEST;
      priv->parsed_header->sub_type = SIP_SUB_TYPE_REQUEST_ACK;
      break;

   case SIP_TOKEN_BYE:
      priv->parsed_header->type     = SIP_TYPE_REQUEST;
      priv->parsed_header->sub_type = SIP_SUB_TYPE_REQUEST_BYE;
      break;

   case SIP_TOKEN_CANCEL:
      priv->parsed_header->type     = SIP_TYPE_REQUEST;
      priv->parsed_header->sub_type = SIP_SUB_TYPE_REQUEST_CANCEL;
      break;

   default:
      priv->parsed_header->type     = SIP_TYPE_UNKNOWN;
      priv->parsed_header->sub_type = 0;
      break;
   }

   return 0;
}


static int _sip_packet_parse_header(_priv_data_t *priv, unsigned char *packet_data, int data_length)
{
   unsigned char *p;
   unsigned char token[256];
   int           i;
   int           found;

   p     = packet_data;
   found = 0;
   for (i = 0; i < sizeof(_sip_ids) / sizeof(*_sip_ids); i++)
   {
      if (memcmp(_sip_ids[i].id, p, _sip_ids[i].len) == 0)
      {
         found = 1;
         break;
      }
   }

   if (found == 0)
   {
      priv->parsed_header->type = SIP_TYPE_UNKNOWN;
      return 0;
   }

   p = packet_data + _get_line(priv, packet_data);

   strcpy(priv->parsed_header->status_line, priv->current_line);

   _get_next_token(priv, " ", 1);

   _parse_sip_type_subtype(priv);

   if (priv->parsed_header->type == SIP_TYPE_UNKNOWN)
   {
      goto label_ret;
   }

   switch (priv->parsed_header->sub_type)
   {
   case SIP_SUB_TYPE_REQUEST_REGISTER:
      _get_next_token(priv, ":", 1);                    //skip sip:
      _get_next_token(priv, " ", 1);
      strcpy(token, priv->current_token);
      _parse_ip_addr(priv, priv->current_token, priv->parsed_header->server_ip);
      break;

   case SIP_SUB_TYPE_REQUEST_ACK:
   case SIP_SUB_TYPE_REQUEST_BYE:
   case SIP_SUB_TYPE_REQUEST_INVITE:
      _get_next_token(priv, " ", 1);
      _parse_request_line(priv);
      break;
   }

label_ret:
   return (int)(p - packet_data);
}


int sip_packet_parse_header(unsigned char *packet_data, int data_length, sip_header_info_t *out_parsed_header)
{
   _priv_data_t priv;

   memset(&priv, 0, sizeof(_priv_data_t));
   priv.parsed_header = out_parsed_header;

   return _sip_packet_parse_header(&priv, packet_data, data_length);
}


static int __parse_address_header(_priv_data_t *priv, int token_code)
{
   unsigned char     *p;
   unsigned char     token[128];
   int               bytes_read;
   sip_client_info_t *client_info;

   if (token_code == SIP_TOKEN_FROM)
   {
      p           = priv->parsed_header->from.unparsed_str;
      client_info = &priv->parsed_header->from;
   }
   else if (token_code == SIP_TOKEN_TO)
   {
      p           = priv->parsed_header->to.unparsed_str;
      client_info = &priv->parsed_header->to;
   }
   else
   {
      return -1;
   }

   // eg. "101"<sip:101@192.168.254.1>;tag=0178804a

   bytes_read = _get_next_token_from_data(p, "<", 1, token);
   if ((bytes_read > 0) && (token[0] == '"'))
   {
      //currently ignore display name  ie. "101"
   }

   p += bytes_read;
   p += _get_next_token_from_data(p, ":", 1, token);                     //skip sip: or sips: from sip:101@192.168.254.1>;tag=0178804a
   memset(client_info->extn, 0, 32);
   bytes_read = _get_next_token_from_data(p, "@", 1, client_info->extn); //now we hv 192.168.254.1>;tag=0178804a

   p += _get_next_token_from_data(p, ";", 1, token);                     //skip ip address, so we might have tag=0178804a

   memset(token, 0, 128);
   p += _get_next_token_from_data(p, "=", 1, token);
   if (strcmp(token, "tag") == 0)
   {
      p += _get_next_token_from_data(p, NULL, 0, client_info->tag);
   }

   return 0;
}


static int __parse_via_header(_priv_data_t *priv)
{
   unsigned char *p;
   unsigned char field_name[128];
   int           bytes_read;
   int           total_bytes;
   int           ret;
   unsigned char field[256];

   p           = priv->parsed_header->via.unparsed_str;
   total_bytes = strlen(priv->parsed_header->via.unparsed_str);
   bytes_read  = 0;

   /* skip initial SIP/2.0/UDP x.x.x.x:x */
   memset(field, 0, 128);
   ret         = _get_next_token_from_data(p, ";", 1, field);
   bytes_read += ret;
   p          += ret;

   if (bytes_read >= total_bytes)
   {
      return bytes_read;
   }

   while (bytes_read < total_bytes)
   {
      memset(field, 0, 256);
      ret         = _get_next_token_from_data(p, ";", 1, field);
      bytes_read += ret;
      p          += ret;

      memset(field_name, 0, 128);
      ret = _get_next_token_from_data(field, "=", 1, field_name);

      if (strcmp(field_name, "branch") == 0)
      {
         _get_next_token_from_data(field + ret, NULL, 0, priv->parsed_header->via.branch);
         return bytes_read;
      }
   }

   return bytes_read;
}


static int __parse_contact_header(_priv_data_t *priv, int token_code)
{
   unsigned char *p;
   unsigned char token[128];
   int           bytes_read;

   p = priv->parsed_header->contact;

   // here were only interested in extracting rinstance if it exists
   // eg. Contact: <sip:103@192.168.254.178:48638;rinstance=72612d6b7fcb7e10>

   bytes_read = _get_next_token_from_data(p, ";", 1, token);
   if (bytes_read == (int)strlen(p))
   {
      return 0;           //rinstance is prbbly not present
   }

   p += bytes_read;
   memset(token, 0, 128);
   p += _get_next_token_from_data(p, "=", 1, token);
   if (strcmp(token, "rinstance") == 0)
   {
      p += _get_next_token_from_data(p, ">", 1, priv->parsed_header->rinstance);
   }

   return 0;
}


static int _parse_sip_headers(_priv_data_t *priv, unsigned char *data)
{
   int           bytes_read;
   unsigned char *p;
   unsigned char *header_data_ptr;
   int           last_token_code;
   unsigned char token[32];

   p = data;
   while (1)
   {
      bytes_read = _get_line(priv, p);
      if (bytes_read <= 0)
      {
         break;
      }

      p += bytes_read;

      _get_next_token(priv, ":", 1);
      header_data_ptr = NULL;
      last_token_code = priv->current_token_code;

      switch (priv->current_token_code)
      {
      case SIP_TOKEN_VIA:
         header_data_ptr = priv->parsed_header->via.unparsed_str;
         break;

      case SIP_TOKEN_CONTACT:
         header_data_ptr = priv->parsed_header->contact;
         break;

      case SIP_TOKEN_TO:
         header_data_ptr = priv->parsed_header->to.unparsed_str;
         break;

      case SIP_TOKEN_FROM:
         header_data_ptr = priv->parsed_header->from.unparsed_str;
         break;

      case SIP_TOKEN_CALL_ID:
         header_data_ptr = priv->parsed_header->call_id;
         break;

      case SIP_TOKEN_CSEQ:
         header_data_ptr = priv->parsed_header->cseq.unparsed_str;
         break;

      case SIP_TOKEN_EXPIRES:
         _get_next_token(priv, NULL, 0);
         al_conf_atoi(priv->current_token, &priv->parsed_header->expires);
         break;

      case SIP_TOKEN_PROXY_AUTHORIZATION:
         header_data_ptr = priv->parsed_header->proxy_auth;
         break;

      case SIP_TOKEN_CONTENT_TYPE:
         header_data_ptr = priv->parsed_header->content_type;
         break;

      case SIP_TOKEN_CONTENT_LENGTH:
         _get_next_token(priv, NULL, 0);
         al_conf_atoi(priv->current_token, &priv->parsed_header->content_length);
         goto label_exit;
         break;

      default:
         header_data_ptr = NULL;
         break;
      }

      if (header_data_ptr != NULL)
      {
         _get_next_token(priv, NULL, 0);
         strcpy(header_data_ptr, priv->current_token);
      }

      if (last_token_code == SIP_TOKEN_CONTACT)
      {
         __parse_contact_header(priv, last_token_code);
      }
      else if ((last_token_code == SIP_TOKEN_FROM) || (last_token_code == SIP_TOKEN_TO))
      {
         __parse_address_header(priv, last_token_code);
      }
      else if (last_token_code == SIP_TOKEN_VIA)
      {
         __parse_via_header(priv);
      }
      else if (last_token_code == SIP_TOKEN_CSEQ)
      {
         memset(token, 0, 32);
         _get_next_token_from_data(priv->parsed_header->cseq.unparsed_str, " ", 1, token);
         al_conf_atoi(token, &priv->parsed_header->cseq.sqnr);
      }
   }

label_exit:
   return (int)(p - data);
}


static int _parse_sip_body(_priv_data_t *priv, unsigned char *data)
{
   unsigned char *p;
   unsigned char *q;
   int           remaining_bytes;

   remaining_bytes = priv->parsed_header->content_length;
   p = data;
   q = priv->parsed_header->message_body;

   p += _get_line(priv, p);
   p += _get_line(priv, p);
   p += _get_line(priv, p);
   remaining_bytes = (remaining_bytes - (p - data));

   while (remaining_bytes > 0)
   {
      *q++ = *p++;
      remaining_bytes--;
   }

   return (int)(p - data);
}


int sip_packet_parse(unsigned char *packet_data, int data_length, sip_header_info_t *out_parsed_header)
{
   unsigned char *p;
   _priv_data_t  priv;

   memset(&priv, 0, sizeof(_priv_data_t));
   priv.parsed_header = out_parsed_header;

   p = packet_data + _sip_packet_parse_header(&priv, packet_data, data_length);

   if (out_parsed_header->type == SIP_TYPE_UNKNOWN)
   {
      goto label_ret;           //donot process status packets
   }
   p += _parse_sip_headers(&priv, p);

   if (priv.parsed_header->content_length > 0)
   {
      p += _parse_sip_body(&priv, p);
   }

label_ret:
   return (int)(p - packet_data);
}
