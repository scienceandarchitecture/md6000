/********************************************************************************
* MeshDynamics
* --------------
* File     : sip_packet_parser.h
* Comments : Meshdynamics SIP packet parser
* Created  : 1/23/2008
* Author   : Abhijit Ayarekar
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  0  |1/23/2008 | Created.                                        |Abhijit |
* -----------------------------------------------------------------------------
********************************************************************************/

#ifndef __SIP_PACKET_PARSER_H__
#define __SIP_PACKET_PARSER_H__

#define SIP_TYPE_UNKNOWN                  0
#define SIP_TYPE_REQUEST                  1
#define SIP_TYPE_STATUS                   2

#define SIP_STATUS_CLASS_INFORMATION      1
#define SIP_STATUS_CLASS_SUCCESS          2
#define SIP_STATUS_CLASS_CLIENT_ERROR     4

#define SIP_SUB_TYPE_REQUEST_REGISTER     1
#define SIP_SUB_TYPE_REQUEST_SUBSCRIBE    2
#define SIP_SUB_TYPE_REQUEST_INVITE       3
#define SIP_SUB_TYPE_REQUEST_ACK          4
#define SIP_SUB_TYPE_REQUEST_CANCEL       5
#define SIP_SUB_TYPE_REQUEST_BYE          6

#define SIP_SUB_TYPE_STATUS_100           100
#define SIP_SUB_TYPE_STATUS_180           180

#define SIP_HEADER_TYPE_VIA               1
#define SIP_HEADER_TYPE_CONTACT           2
#define SIP_HEADER_TYPE_TO                3
#define SIP_HEADER_TYPE_FROM              4
#define SIP_HEADER_TYPE_CALL_ID           5
#define SIP_HEADER_TYPE_CSEQ              6

struct sip_client_info
{
   unsigned char unparsed_str[256];
   unsigned char extn[32];
   unsigned char tag[64];
};
typedef struct sip_client_info   sip_client_info_t;

struct sip_via_info
{
   unsigned char unparsed_str[256];
   unsigned char branch[128];
};

typedef struct sip_via_info   sip_via_info_t;

struct sip_cseq_info
{
   unsigned char unparsed_str[32];
   int           sqnr;
};

typedef struct sip_cseq_info   sip_cseq_info_t;

struct sip_header_info
{
   unsigned char     client_mac_addr[6];
   unsigned char     type;
   unsigned short    sub_type;
   unsigned char     status_class;
   unsigned char     status_line[128];
   unsigned char     server_ip[4];
   unsigned char     client_ip[4];
   sip_via_info_t    via;
   unsigned char     contact[256];
   unsigned char     rinstance[32];
   sip_client_info_t to;
   sip_client_info_t from;
   unsigned char     call_id[256];
   sip_cseq_info_t   cseq;
   int               expires;
   unsigned short    rport;
   unsigned char     proxy_auth[256];
   unsigned char     content_type[32];
   int               content_length;
   unsigned char     message_body[1024];
};

typedef struct sip_header_info   sip_header_info_t;

int sip_packet_parse_header(unsigned char *packet_data, int data_length, sip_header_info_t *out_parsed_header);
int sip_packet_parse(unsigned char *packet_data, int data_length, sip_header_info_t *out_parsed_header);

#endif
