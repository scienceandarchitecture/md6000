/********************************************************************************
* MeshDynamics
* --------------
* File     : sip_data_helper.c
* Comments : sip data & data helper implementation
* Created  : 5/21/2008
* Author   : Abhijit Ayarekar
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  1  |11/17/2008| Fixes for adhoc only mode                       | Sriram |
* -----------------------------------------------------------------------------
* |  0  |5/21/2008 | Created.                                        |Abhijit |
* -----------------------------------------------------------------------------
********************************************************************************/
#include <linux/kernel.h>
#include <linux/string.h>
#include "al.h"
#include "al_802_11.h"
#include "al_ip.h"
#include "al_socket.h"
#include "mesh_ext.h"
#include "sip_conf.h"
#include "meshap_sip.h"
#include "sip_packet_parser.h"
#include "sip_data_helper.h"
#include "sip_dlg_helper.h"
#include "access_point.h"

meshap_sip_dlg_table_entry_t _dlg_table[_MAX_CALL_DIALOGS];
_sip_config_t                _config;

static unsigned char _dialog_count;

int _sip_data_initialize(al_net_if_t *ds_net_if, sip_conf_info_t *info)
{
   unsigned char *info_ptr;
   int           i;

   memset(&_config, 0, sizeof(_sip_config_t));

   _config.adhoc_only  = info->adhoc_only;
   _config.sip_enabled = info->sip_enabled;
   _config.silent_mode = 1;
   _config.sip_header  = (sip_header_info_t *)al_heap_alloc(AL_CONTEXT sizeof(sip_header_info_t));

   if (_config.sip_header == NULL)
   {
      return -1;
   }

   memset(_config.sip_header, 0, sizeof(sip_header_info_t));

   memcpy(&_config.server_ip, info->server_ip, 4);

   _config.ds_net_if = ds_net_if;
   _config.port      = info->port;
   _config.sta_count = (info->sta_count > _MAX_STA_COUNT) ? _MAX_STA_COUNT : info->sta_count;
   info_ptr          = (unsigned char *)info->sta_info;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "======== SIP: Registry ========\n");
   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "Server-IP:port : %d.%d.%d.%d:%d\n", _config.server_ip[0],
                _config.server_ip[1],
                _config.server_ip[2],
                _config.server_ip[3],
                _config.port);
   for (i = 0; i < _config.sta_count; i++)
   {
      memcpy(&_config.sta_list[i].info, info_ptr, sizeof(sip_conf_sta_info_t));
      info_ptr += sizeof(sip_conf_sta_info_t);

      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "Ext : %s, Mac: %02x:%02x:%02x:%02x:%02x:%02x\n",
                   _config.sta_list[i].info.extn,
                   _config.sta_list[i].info.mac[0],
                   _config.sta_list[i].info.mac[1],
                   _config.sta_list[i].info.mac[2],
                   _config.sta_list[i].info.mac[3],
                   _config.sta_list[i].info.mac[4],
                   _config.sta_list[i].info.mac[5]);
   }
   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "====================================\n");

   _dialog_count = 1;
   memset(_dlg_table, 0, sizeof(meshap_sip_dlg_table_entry_t) * _MAX_CALL_DIALOGS);

   _config.silent_mode = _sip_data_check_silent_mode();

   return 0;
}


int _sip_data_uninitialize(void)
{
   if (_config.sip_header != NULL)
   {
      al_heap_free(AL_CONTEXT _config.sip_header);
   }

   return 0;
}


static int ____create_rinstance(char *out_rinstance)
{
   unsigned char random_bytes[8];

   al_cryptographic_random_bytes(AL_CONTEXT random_bytes, 8);
   return sprintf(out_rinstance, "%02x%02x%02x%02x%02x%02x%02x%02x", random_bytes[0], random_bytes[1],
                  random_bytes[2], random_bytes[3],
                  random_bytes[4], random_bytes[5],
                  random_bytes[6], random_bytes[7]);
}


static int ____create_call_id(char *out_call_id)
{
   unsigned char random_bytes[23];
   char          *p;
   int           i;
   int           ret;

   al_cryptographic_random_bytes(AL_CONTEXT random_bytes, 23);

   p = out_call_id;
   for (i = 0; i < 23; i++)
   {
      sprintf(p, "%02x", random_bytes[i]);
      p += 2;
   }

   ret = (int)(p - out_call_id);
   return ret;
}


int _sip_data_create_branch(char *out_branch)
{
   unsigned char random_bytes[8];

   al_cryptographic_random_bytes(AL_CONTEXT random_bytes, 8);
   return sprintf(out_branch, "z9hG4bK-d87543-%02x%02x%02x%02x%02x%02x%02x%02x-1--d87543-", random_bytes[0], random_bytes[1],
                  random_bytes[2], random_bytes[3],
                  random_bytes[4], random_bytes[5],
                  random_bytes[6], random_bytes[7]);
}


int _sip_data_create_tag(char *out_tag)
{
   unsigned char random_bytes[4];

   al_cryptographic_random_bytes(AL_CONTEXT random_bytes, 4);
   return sprintf(out_tag, "%02x%02x%02x%02x", random_bytes[0], random_bytes[1],
                  random_bytes[2], random_bytes[3]);
}


static void __init_dialog_info(meshap_sip_dlg_info_t *info)
{
   info->call_state = CALL_STATE_START;
   info->cseq       = 1;
   _sip_data_create_tag(info->to_tag);
   ____create_rinstance(info->rinstance);
   _sip_data_create_branch(info->branch);
   ____create_call_id(info->call_id);
   _sip_data_create_tag(info->from_tag);
}


static void __init_dialog_table_entry(meshap_sip_dlg_table_entry_t *entry)
{
   memset(entry, 0, sizeof(meshap_sip_dlg_table_entry_t));

   entry->id  = _dialog_count++;
   entry->id |= (_config.ds_net_if->config.hw_addr.bytes[3] << 8);
   entry->id |= (_config.ds_net_if->config.hw_addr.bytes[4] << 16);
   entry->id |= (_config.ds_net_if->config.hw_addr.bytes[5] << 24);
}


meshap_sip_dlg_table_entry_t *_sip_data_get_empty_dlg(int initialize_values)
{
   int i;
   int found;

   found = 0;
   for (i = 0; i < _MAX_CALL_DIALOGS; i++)
   {
      if (_dlg_table[i].flags == DLG_TABLE_FLAGS_UNUSED)
      {
         found = 1;
         break;
      }
   }

   if (found == 1)
   {
      __init_dialog_table_entry(&_dlg_table[i]);
      _dlg_table[i].flags = DLG_TABLE_FLAGS_USED;

      if (initialize_values == 1)
      {
         __init_dialog_info(&_dlg_table[i].callee_dlg);
         __init_dialog_info(&_dlg_table[i].caller_dlg);
      }

      return &_dlg_table[i];
   }

   return NULL;
}


meshap_sip_dlg_table_entry_t *_sip_data_find_dlg_by_call_id(unsigned char *call_id)
{
   int i;

   for (i = 0; i < _MAX_CALL_DIALOGS; i++)
   {
      if ((strcmp(_dlg_table[i].callee_dlg.call_id, call_id) == 0) ||
          (strcmp(_dlg_table[i].caller_dlg.call_id, call_id) == 0))
      {
         return &_dlg_table[i];
      }
   }

   return NULL;
}


meshap_sip_dlg_table_entry_t *_sip_data_find_dlg_by_dlg_id(unsigned int dialog_id)
{
   int i;

   for (i = 0; i < _MAX_CALL_DIALOGS; i++)
   {
      if ((_dlg_table[i].id != 0) && (_dlg_table[i].id == dialog_id))
      {
         return &_dlg_table[i];
      }
   }

   return NULL;
}


meshap_sip_sta_info_t *_sip_data_find_sip_sta(void *compare_data, int sta_search_type)
{
   int i;
   int ret;

   for (i = 0; i < _config.sta_count; i++)
   {
      ret = -1;
      switch (sta_search_type)
      {
      case STA_SEARCH_TYPE_EXTN:
         ret = strcmp((const char *)compare_data, _config.sta_list[i].info.extn);
         break;

      case STA_SEARCH_TYPE_MAC:
         ret = memcmp(compare_data, _config.sta_list[i].info.mac, 6);
         break;

      case STA_SEARCH_TYPE_IP:
         ret = memcmp(compare_data, &_config.sta_list[i].ip_addr.bytes, 4);
         break;

      default:
         return NULL;
      }

      if (ret == 0)
      {
         return &_config.sta_list[i];
      }
   }

   return NULL;
}


int _sip_data_evaluate_sta_locations(meshap_sip_dlg_table_entry_t *dlg_table_entry)
{
   meshap_sip_sta_info_t *peer;

   peer = dlg_table_entry->caller_info;
   if (peer == NULL)
   {
      return -1;
   }

   _sip_data_evaluate_sta_location(peer);

   peer = dlg_table_entry->callee_info;
   if (peer == NULL)
   {
      return -2;
   }

   _sip_data_evaluate_sta_location(peer);

   return 0;
}


int _sip_data_evaluate_sta_location(meshap_sip_sta_info_t *sip_sta_info)
{
   access_point_sta_info_t ap_sta_info;
   al_net_addr_t           sta_mac_addr;

   memset(&sta_mac_addr, 0, sizeof(al_net_addr_t));
   memset(&ap_sta_info, 0, sizeof(access_point_sta_info_t));
   memcpy(&sta_mac_addr.bytes, sip_sta_info->info.mac, 6);
   sta_mac_addr.length = 6;

   access_point_get_sta_info(AL_CONTEXT & sta_mac_addr, &ap_sta_info);

   if (ap_sta_info.net_if == NULL)
   {
      _SET_STA_LOCATION_REMOTE(sip_sta_info->flags)
      sip_sta_info->net_if = NULL;
   }
   else
   {
      _SET_STA_LOCATION_LOCAL(sip_sta_info->flags)
      sip_sta_info->net_if = ap_sta_info.net_if;
   }

   return 0;
}


int _sip_data_check_silent_mode()
{
   int           func_mode;
   int           silent_mode;
   al_net_addr_t server_addr;

   silent_mode = _config.silent_mode;

   /*
    * Full mode is enabled by default hence silent_mode = 0
    */
   if (_config.adhoc_only == 0)
   {
      silent_mode = 0;
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "SIP Ad-hoc only mode is disabled");
      goto _ret;
   }
   else
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "SIP Ad-hoc only mode is enabled");
   }

   func_mode = mesh_ext_get_func_mode();

   if ((func_mode == MESH_EXT_FUNC_MODE_FFR) || (func_mode == MESH_EXT_FUNC_MODE_FFN))
   {
      silent_mode = 1;
   }
   else
   {
      silent_mode = 0;
   }

_ret:
   if (_config.silent_mode != silent_mode)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "SIP silent mode changed from %d to %d", _config.silent_mode, silent_mode);

      _config.silent_mode = silent_mode;

      memset(&server_addr, 0, sizeof(al_net_addr_t));
      memcpy(&server_addr.bytes, _config.server_ip, 4);

      if (silent_mode == 1)
      {
         access_point_arp_remove_entry(AL_CONTEXT & server_addr);
      }
      else
      {
         access_point_arp_add_entry(AL_CONTEXT & server_addr, 1);
      }
   }

   return silent_mode;
}
