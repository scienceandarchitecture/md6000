/********************************************************************************
* MeshDynamics
* --------------
* File     : sip_plugin_event_handler.h
* Comments : mesh plugin event handler implementation
* Created  : 5/21/2008
* Author   : Abhijit Ayarekar
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  0  |5/21/2008 | Created.                                        |Abhijit |
* -----------------------------------------------------------------------------
********************************************************************************/

#ifndef __SIP_PLUGIN_EVENT_HANDLER_H__
#define __SIP_PLUGIN_EVENT_HANDLER_H__

int _sip_plugin_handler_initialize(void);
int _sip_plugin_handler_uninitialize(void);

#endif /*__MESH_PLUGIN_EVENT_HANDLER_H__*/
