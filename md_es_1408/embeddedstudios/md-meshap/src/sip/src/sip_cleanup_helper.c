/********************************************************************************
* MeshDynamics
* --------------
* File     : meshap_sip.h
* Comments : call dlg cleanup thread implementation
* Created  : 1/16/2008
* Author   : Abhijit Ayarekar
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  0  |1/16/2008 | Created.                                        |Abhijit |
* -----------------------------------------------------------------------------
********************************************************************************/
#include <linux/kernel.h>
#include <linux/string.h>
#include <linux/sched.h>
#include "al.h"
#include "al_802_11.h"
#include "al_ip.h"
#include "al_socket.h"
#include "access_point.h"
#include "access_point_ext.h"
#include "mesh_ext.h"
#include "sip_conf.h"
#include "meshap_sip.h"
#include "sip_packet_parser.h"
#include "sip_data_helper.h"
#include "sip_packet_helper.h"
#include "sip_mesh_helper.h"
#include "sip_dlg_helper.h"

#define _CRLF        "\r\n"
#define _CRLF_LEN    2

static int _cleanup_thread_id;
int _cleanup_thread_exit_event;

static int __verify_call_dialog_for_caller(meshap_sip_dlg_table_entry_t *dlg_entry)
{
   unsigned char         *buffer;
   unsigned char         *p;
   unsigned char         *ip_bytes;
   meshap_sip_dlg_info_t *call_dlg;

   call_dlg = &dlg_entry->caller_dlg;
   buffer   = al_get_gen_2KB_buffer(AL_CONTEXT_SINGLE);
   p        = buffer;
   memset(buffer, 0, 2048);

   ip_bytes = dlg_entry->caller_info->ip_addr.bytes;

   p += sprintf(p, "OPTIONS sip:%s@%d.%d.%d.%d SIP/2.0\r\n", dlg_entry->caller_info->info.extn,
                ip_bytes[0], ip_bytes[1],
                ip_bytes[2], ip_bytes[3]);

   p += sprintf(p, "Max-Forwards: 70\r\n");

   p += sprintf(p, "Via: SIP/2.0/UDP %d.%d.%d.%d:%d;branch=%s\r\n", _config.server_ip[0], _config.server_ip[1],
                _config.server_ip[2], _config.server_ip[3],
                _config.port,
                call_dlg->branch);

   p += sprintf(p, "Contact: <sip:%s@%d.%d.%d.%d:%d>\r\n", dlg_entry->caller_info->info.extn,
                _config.server_ip[0], _config.server_ip[1],
                _config.server_ip[2], _config.server_ip[3],
                _config.port);

   p += sprintf(p, "To: \"%s\"<sip:%s@%d.%d.%d.%d>;tag=%s\r\n", dlg_entry->callee_info->info.extn,
                dlg_entry->callee_info->info.extn,
                _config.server_ip[0], _config.server_ip[1],
                _config.server_ip[2], _config.server_ip[3],
                call_dlg->from_tag);


   p += sprintf(p, "From: \"%s\"<sip:%s@%d.%d.%d.%d>;tag=%s\r\n", dlg_entry->caller_info->info.extn,
                dlg_entry->caller_info->info.extn,
                _config.server_ip[0], _config.server_ip[1],
                _config.server_ip[2], _config.server_ip[3],
                call_dlg->to_tag);

   p += sprintf(p, "Call-ID: %s\r\n", call_dlg->call_id);

   p += sprintf(p, "CSeq: %d OPTIONS\r\n", ++call_dlg->cseq);

   p += sprintf(p, "Accept: application/sdp\r\n");

   p += sprintf(p, "Content-Length: 0\r\n");

   memcpy(p, _CRLF, _CRLF_LEN);
   p += _CRLF_LEN;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "Sending dialog verification msg to %s\n", dlg_entry->caller_info->info.extn);

   _sip_packet_helper_send_packet(dlg_entry->caller_info, buffer, (int)(p - buffer));

   al_release_gen_2KB_buffer(AL_CONTEXT buffer);

   return 0;
}


static int __verify_call_dialog_for_callee(meshap_sip_dlg_table_entry_t *dlg_entry)
{
   unsigned char         *buffer;
   unsigned char         *p;
   unsigned char         *ip_bytes;
   meshap_sip_dlg_info_t *call_dlg;

   call_dlg = &dlg_entry->callee_dlg;
   buffer   = al_get_gen_2KB_buffer(AL_CONTEXT_SINGLE);
   p        = buffer;
   memset(buffer, 0, 2048);

   ip_bytes = dlg_entry->callee_info->ip_addr.bytes;

   p += sprintf(p, "OPTIONS sip:%s@%d.%d.%d.%d SIP/2.0\r\n", dlg_entry->callee_info->info.extn,
                ip_bytes[0], ip_bytes[1],
                ip_bytes[2], ip_bytes[3]);

   p += sprintf(p, "Max-Forwards: 70\r\n");

   p += sprintf(p, "Via: SIP/2.0/UDP %d.%d.%d.%d:%d;branch=%s\r\n", _config.server_ip[0], _config.server_ip[1],
                _config.server_ip[2], _config.server_ip[3],
                _config.port,
                call_dlg->branch);

   p += sprintf(p, "Contact: <sip:%s@%d.%d.%d.%d:%d>\r\n", dlg_entry->callee_info->info.extn,
                _config.server_ip[0], _config.server_ip[1],
                _config.server_ip[2], _config.server_ip[3],
                _config.port);

   p += sprintf(p, "To: \"%s\"<sip:%s@%d.%d.%d.%d>;tag=%s\r\n", dlg_entry->callee_info->info.extn,
                dlg_entry->callee_info->info.extn,
                _config.server_ip[0], _config.server_ip[1],
                _config.server_ip[2], _config.server_ip[3],
                call_dlg->to_tag);


   p += sprintf(p, "From: \"%s\"<sip:%s@%d.%d.%d.%d>;tag=%s\r\n", dlg_entry->caller_info->info.extn,
                dlg_entry->caller_info->info.extn,
                _config.server_ip[0], _config.server_ip[1],
                _config.server_ip[2], _config.server_ip[3],
                call_dlg->from_tag);

   p += sprintf(p, "Call-ID: %s\r\n", call_dlg->call_id);

   p += sprintf(p, "CSeq: %d OPTIONS\r\n", ++call_dlg->cseq);

   p += sprintf(p, "Accept: application/sdp\r\n");

   p += sprintf(p, "Content-Length: 0\r\n");

   memcpy(p, _CRLF, _CRLF_LEN);
   p += _CRLF_LEN;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "Sending dialog verification msg to %s\n", dlg_entry->callee_info->info.extn);

   _sip_packet_helper_send_packet(dlg_entry->callee_info, buffer, (int)(p - buffer));

   al_release_gen_2KB_buffer(AL_CONTEXT buffer);

   return 0;
}


static int __check_sip_sta_activity(meshap_sip_sta_info_t *sip_sta_info)
{
   access_point_sta_info_t ap_sta_info;
   al_net_addr_t           al_net_addr;
   al_u64_t                current_time;
   al_u64_t                timeout;

   _sip_data_evaluate_sta_location(sip_sta_info);

   if (_IS_STA_LOCAL(sip_sta_info->flags))
   {
      memset(&ap_sta_info, 0, sizeof(access_point_sta_info_t));
      memcpy(&al_net_addr.bytes, sip_sta_info->info.mac, 6);
      al_net_addr.length = 6;

      access_point_get_sta_info(AL_CONTEXT & al_net_addr, &ap_sta_info);

      if (ap_sta_info.net_if == NULL)
      {
         _SET_STA_LOCATION_REMOTE(sip_sta_info->flags);
         sip_sta_info->last_check_time = al_get_tick_count(AL_CONTEXT);
         al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "Sip sta %s has moved.\n", sip_sta_info->info.extn);
         return 1;
      }
   }
   else
   {
      current_time = al_get_tick_count(AL_CONTEXT);
      timeout      = sip_sta_info->last_check_time + 60 * HZ;
      //if(current_time - sip_sta_info->last_check_time > 60000) {
      if (al_timer_after(current_time, timeout))
      {
         al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "Not heard from Sip sta %s in atleast %d jiffies.\n", sip_sta_info->info.extn,
                      (int)(current_time - sip_sta_info->last_check_time));
         return 2;
      }
   }

   return 0;
}


static int _cleanup_thread_function(AL_CONTEXT_PARAM_DECL al_thread_param_t *param)
{
   int i;
   int sta_activity;

   al_set_thread_name(AL_CONTEXT "sip_cleanup_thread");

   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "Dialog cleanup thread starting\n");

   /*
    * since this thread starts in init function
    * we can safely ask for active dialogs in mesh
    */
   _sip_mesh_helper_request_latest_sip_info();

   while (1)
   {
	   thread_stat.sip_cleanup_thread_count++;
	   if(reboot_in_progress) {
		   printk(KERN_EMERG"-----Terminating 'sip_cleanup_thread' thread-----\n");
		   threads_status &= ~THREAD_STATUS_BIT14_MASK;
		   break;
	   }

      if (al_wait_on_event(AL_CONTEXT _cleanup_thread_exit_event, 0) == AL_WAIT_STATE_SIGNAL)
      {
         break;
      }

      al_thread_sleep(5000);

      for (i = 0; i < _MAX_CALL_DIALOGS; i++)
      {
         if ((_dlg_table[i].id == 0) || !_IS_CALL_DLG_IN_USE(_dlg_table[i].flags))
         {
            continue;
         }

         if (_IS_CALL_DLG_UNDER_VERIFICATION(_dlg_table[i].flags))
         {
            if (_IS_STA_LOCAL(_dlg_table[i].caller_info->flags) && _IS_DLG_VERIFY_CALLER(_dlg_table[i].flags))
            {
               __verify_call_dialog_for_caller(&_dlg_table[i]);
            }
            if (_IS_STA_LOCAL(_dlg_table[i].callee_info->flags) && _IS_DLG_VERIFY_CALLEE(_dlg_table[i].flags))
            {
               __verify_call_dialog_for_callee(&_dlg_table[i]);
            }
         }

         sta_activity  = __check_sip_sta_activity(_dlg_table[i].caller_info);
         sta_activity |= __check_sip_sta_activity(_dlg_table[i].callee_info);

         if (((_dlg_table[i].caller_dlg.call_state != CALL_STATE_ESTABLISHED) && (_dlg_table[i].caller_dlg.call_state != CALL_STATE_STATUS_OK)) ||
             ((_dlg_table[i].callee_dlg.call_state != CALL_STATE_ESTABLISHED) && (_dlg_table[i].callee_dlg.call_state != CALL_STATE_STATUS_OK)) ||
             (sta_activity != 0))
         {
            _dlg_table[i].call_init_uninit_time++;
         }
         else
         {
            _dlg_table[i].call_init_uninit_time = 0;
         }

         if (_dlg_table[i].call_init_uninit_time > 5)
         {
            al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "Clearing dirty dialog %x (%d, %d), dirty_time=%d\n", _dlg_table[i].id,
                         _dlg_table[i].caller_dlg.call_state,
                         _dlg_table[i].callee_dlg.call_state,
                         _dlg_table[i].call_init_uninit_time * 5);
            _dlg_table[i].caller_dlg.call_state = CALL_STATE_END;
            _dlg_table[i].callee_dlg.call_state = CALL_STATE_END;
            _sip_mesh_helper_bcast_call_dialog_status(&_dlg_table[i], PENDING_STATUS_FOR_CLEANUP);
            memset(&_dlg_table[i], 0, sizeof(meshap_sip_dlg_table_entry_t));
         }
      }

      for (i = 0; i < _config.sta_count; i++)
      {
         if (!_IS_STA_REGISTERED(_config.sta_list[i].flags))
         {
            continue;
         }

         if (__check_sip_sta_activity(&_config.sta_list[i]) != 0)
         {
            _UNREGISTER_STA(_config.sta_list[i].flags);
            al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "Sta %s unregistered.\n", _config.sta_list[i].info.extn);
         }
      }
   }

   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "Dialog cleanup thread exiting\n");
   return 0;
}


int _sip_cleanup_helper_initialize(void)
{
   _cleanup_thread_exit_event = al_create_event(AL_CONTEXT 1);
   al_reset_event(AL_CONTEXT _cleanup_thread_exit_event);
   al_create_thread_on_cpu(AL_CONTEXT _cleanup_thread_function, NULL,0, "_cleanup_thread_function");
   threads_status |= THREAD_STATUS_BIT14_MASK;
   return 0;
}


int _sip_cleanup_helper_uninitialize(void)
{
   al_set_event(AL_CONTEXT _cleanup_thread_exit_event);
   return 0;
}
