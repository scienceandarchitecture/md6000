/********************************************************************************
* MeshDynamics
* --------------
* File     : meshap_sip.c
* Comments : SIP Implementation
* Created  : 1/16/2008
* Author   : Abhijit Ayarekar
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  0  |1/16/2008 | Created.                                        |Abhijit |
* -----------------------------------------------------------------------------
********************************************************************************/

#include <linux/kernel.h>
#include <linux/string.h>
#include "al.h"
#include "al_802_11.h"
#include "al_ip.h"
#include "al_socket.h"
#include "sip_conf.h"
#include "meshap_sip.h"
#include "sip_packet_parser.h"
#include "access_point.h"
#include "mesh_ext.h"
#include "imcp_snip.h"
#include "mesh_imcp.h"
#include "mesh_plugin.h"
#include "mesh_ext.h"
#include "sip_data_helper.h"
#include "sip_plugin_event_handler.h"
#include "sip_cleanup_helper.h"
#include "sip_packet_helper.h"
#include "sip_dlg_helper.h"

meshap_sip_instance_t meshap_sip_initialize(al_net_if_t *ds_net_if, sip_conf_info_t *info)
{
   if (_sip_data_initialize(ds_net_if, info) != 0)
   {
      return NULL;
   }

   _sip_plugin_handler_initialize();

   _sip_packet_helper_initialize();

   mesh_imcp_set_sip_private_packet_handler(AL_CONTEXT meshap_process_imcp_sip_packet);

   _sip_cleanup_helper_initialize();

   return (meshap_sip_instance_t)&_config;
}


void meshap_sip_uninitialize(meshap_sip_instance_t instance)
{
   if (instance == NULL)
   {
      return;
   }

   _sip_cleanup_helper_uninitialize();

   mesh_imcp_set_sip_private_packet_handler(AL_CONTEXT NULL);

   _sip_packet_helper_uninitialize();

   _sip_plugin_handler_uninitialize();

   _sip_data_uninitialize();
}


int meshap_sip_enumerate_stas(void *callback, void (*enum_routine)(void *callback, const meshap_sip_sta_info_t *sta))
{
   int i;

   for (i = 0; i < _config.sta_count; i++)
   {
      if (_IS_STA_REGISTERED(_config.sta_list[i].flags))
      {
         if (enum_routine != NULL)
         {
            enum_routine(callback, &_config.sta_list[i]);
         }
      }
   }

   return 0;
}


int meshap_sip_enumerate_call_dlgs(void *callback, void (*enum_routine)(void *callback, const meshap_sip_dlg_table_entry_t *dlg))
{
   int i;

   for (i = 0; i < _MAX_CALL_DIALOGS; i++)
   {
      if (!_IS_CALL_DLG_IN_USE(_dlg_table[i].flags))
      {
         continue;
      }

      if (enum_routine != NULL)
      {
         enum_routine(callback, &_dlg_table[i]);
      }
   }

   return 0;
}


int meshap_process_sip_packet(meshap_sip_instance_t instance,
                              al_net_if_t           *net_if,
                              al_packet_t           *packet_in)
{
   unsigned char           *data;
   int                     data_length;
   unsigned char           *p;
   sip_header_info_t       *sip_header;
   access_point_sta_info_t ap_sta_info;
   _sip_config_t           *config;

#define _IP_HEADER_SRC_ADDR_POSITION     12
#define _UDP_HEADER_SRC_PORT_POSITION    0

   if (instance == NULL)
   {
      return MESHAP_SIP_RETVAL_CONTINUE;
   }

   config     = (_sip_config_t *)instance;
   sip_header = config->sip_header;

   memset(sip_header, 0, sizeof(sip_header_info_t));

   data        = packet_in->buffer + packet_in->position + IP_MIN_HEADER_LENGTH + UDP_HEADER_LENGTH;
   data_length = packet_in->data_length - (IP_MIN_HEADER_LENGTH + UDP_HEADER_LENGTH);

   sip_packet_parse_header(data, data_length, sip_header);

   memcpy(&sip_header->client_mac_addr, &packet_in->addr_2, sizeof(al_net_addr_t));

   p = packet_in->buffer + packet_in->position + _IP_HEADER_SRC_ADDR_POSITION;
   memcpy(sip_header->client_ip, p, 4);

   p = packet_in->buffer + packet_in->position + IP_MIN_HEADER_LENGTH + _UDP_HEADER_SRC_PORT_POSITION;
   memcpy(&sip_header->rport, p, 2);
   sip_header->rport = al_be16_to_cpu(sip_header->rport);

   sip_packet_parse(data, data_length, sip_header);

   memset(&ap_sta_info, 0, sizeof(access_point_sta_info_t));
   access_point_get_sta_info(AL_CONTEXT & packet_in->addr_2, &ap_sta_info);

   if (ap_sta_info.net_if == NULL)
   {
      return MESHAP_SIP_RETVAL_CONTINUE;
   }

   return _sip_packet_helper_process_sip_packet(sip_header, net_if, packet_in);

#undef _IP_HEADER_SRC_ADDR_POSITION
#undef _UDP_HEADER_SRC_PORT_POSITION
}
