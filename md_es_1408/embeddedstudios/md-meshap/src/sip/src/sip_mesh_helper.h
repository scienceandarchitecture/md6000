/********************************************************************************
* MeshDynamics
* --------------
* File     : sip_mesh_helper.h
* Comments : SIP Mesh Interface
* Created  : 5/22/2008
* Author   : Abhijit Ayarekar
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  0  |5/22/2008 | Created.                                        |Abhijit |
* -----------------------------------------------------------------------------
********************************************************************************/
#ifndef __SIP_MESH_HELPER_H__
#define __SIP_MESH_HELPER_H__

int _sip_mesh_helper_bcast_sta_registration(meshap_sip_sta_info_t *sta_info);
int _sip_mesh_helper_bcast_new_call_dialog(meshap_sip_dlg_table_entry_t *dlg_entry);
int _sip_mesh_helper_bcast_call_dialog_status(meshap_sip_dlg_table_entry_t *dlg_entry, unsigned char pending_status);
int _sip_mesh_helper_bcast_latest_sip_info(void);
int _sip_mesh_helper_request_latest_sip_info(void);

#endif /*__SIP_MESH_HELPER_H__*/
