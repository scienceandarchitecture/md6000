/********************************************************************************
* MeshDynamics
* --------------
* File     : sip_data_helper.h
* Comments : sip data & data helper
* Created  : 5/21/2008
* Author   : Abhijit Ayarekar
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  0  |5/21/2008 | Created.                                        |Abhijit |
* -----------------------------------------------------------------------------
********************************************************************************/

#ifndef __SIP_DATA_HELPER_H__
#define __SIP_DATA_HELPER_H__

#define _MAX_CALL_DIALOGS             32
#define _MAX_STA_COUNT                16

#define _STA_FLAGS_REGISTERED         0x00000001
#define _STA_FLAGS_LOCATION_REMOTE    0x00000002

#define _IS_STA_REGISTERED(flags)                 ((flags & _STA_FLAGS_REGISTERED) != 0)
#define _UNREGISTER_STA(flags)                    do { flags &= ~(_STA_FLAGS_REGISTERED); } while (0);
#define _REGISTER_STA(flags)                      do { flags |= _STA_FLAGS_REGISTERED; } while (0);

#define _SET_STA_LOCATION_REMOTE(flags)           do { flags |= _STA_FLAGS_LOCATION_REMOTE; } while (0);
#define _SET_STA_LOCATION_LOCAL(flags)            do { flags &= ~(_STA_FLAGS_LOCATION_REMOTE); } while (0);
#define _IS_STA_LOCAL(flags)                      ((flags & _STA_FLAGS_LOCATION_REMOTE) == 0)

#define _IS_CALL_DLG_IN_USE(flags)                (((flags & DLG_TABLE_FLAGS_USED) != 0))

#define _IS_CALL_DLG_UNDER_VERIFICATION(flags)    (((flags & DLG_TABLE_FLAGS_VERIFY_CALLER) != 0) || ((flags & DLG_TABLE_FLAGS_VERIFY_CALLEE) != 0))

#define _IS_DLG_VERIFY_CALLER(flags)              (((flags & DLG_TABLE_FLAGS_VERIFY_CALLER) != 0))
#define _SET_DLG_VERIFY_CALLER(flags)             do { flags |= DLG_TABLE_FLAGS_VERIFY_CALLER; } while (0);
#define _RESET_DLG_VERIFY_CALLER(flags)           do { flags &= ~(DLG_TABLE_FLAGS_VERIFY_CALLER); } while (0);
#define _IS_DLG_VERIFY_CALLEE(flags)              (((flags & DLG_TABLE_FLAGS_VERIFY_CALLEE) != 0))
#define _SET_DLG_VERIFY_CALLEE(flags)             do { flags |= DLG_TABLE_FLAGS_VERIFY_CALLEE; } while (0);
#define _RESET_DLG_VERIFY_CALLEE(flags)           do { flags &= ~(DLG_TABLE_FLAGS_VERIFY_CALLEE); } while (0);

#define STA_SEARCH_TYPE_EXTN    1
#define STA_SEARCH_TYPE_MAC     2
#define STA_SEARCH_TYPE_IP      3

struct _sip_config
{
   unsigned char         silent_mode;
   unsigned char         adhoc_only;
   unsigned char         sip_enabled;
   sip_header_info_t     *sip_header;
   al_net_if_t           *ds_net_if;
   unsigned short        port;
   unsigned char         server_ip[4];
   int                   sta_count;
   meshap_sip_sta_info_t sta_list[_MAX_STA_COUNT];
};

typedef struct _sip_config   _sip_config_t;

extern meshap_sip_dlg_table_entry_t _dlg_table[_MAX_CALL_DIALOGS];
extern _sip_config_t                _config;

int _sip_data_initialize(al_net_if_t *ds_net_if, sip_conf_info_t *info);
int _sip_data_uninitialize(void);
int _sip_data_create_branch(char *out_branch);
int _sip_data_create_tag(char *out_tag);
meshap_sip_sta_info_t *_sip_data_find_sip_sta(void *compare_data, int sta_search_type);
meshap_sip_dlg_table_entry_t *_sip_data_find_dlg_by_call_id(unsigned char *call_id);
meshap_sip_dlg_table_entry_t *_sip_data_find_dlg_by_dlg_id(unsigned int dialog_id);
meshap_sip_dlg_table_entry_t *_sip_data_get_empty_dlg(int initialize_values);
int _sip_data_evaluate_sta_locations(meshap_sip_dlg_table_entry_t *dlg_table_entry);
int _sip_data_evaluate_sta_location(meshap_sip_sta_info_t *sip_sta_info);
int _sip_data_check_silent_mode(void);

#endif /*__SIP_DATA_HELPER_H__*/
