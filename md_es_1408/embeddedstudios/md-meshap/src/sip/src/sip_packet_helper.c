
/********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : sip_packet_helper.c
 * Comments : Helper module for SIP
 * Created  : 8/26/2008
 * Author   : Abhijit Ayarekar
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * -----------------------------------------------------------------------------
 * | No  |Date      |  Comment                                        | Author |
 * -----------------------------------------------------------------------------
 * |  0  |8/26/2008 | Created.                                        | Abhijit|
 * -----------------------------------------------------------------------------
 ********************************************************************************/


#include <linux/kernel.h>
#include <linux/string.h>
#include "al.h"
#include "al_802_11.h"
#include "al_ip.h"
#include "al_socket.h"
#include "access_point.h"
#include "access_point_ext.h"
#include "mesh_ext.h"
#include "sip_conf.h"
#include "meshap_sip.h"
#include "sip_packet_parser.h"
#include "sip_packet_helper.h"
#include "sip_data_helper.h"
#include "sip_dlg_helper.h"
#include "sip_mesh_helper.h"

#define _IP_HEADER_DEST_ADDR_POSITION			16

#define _SIP_STATUS_100							100
#define _SIP_STATUS_200							200
#define _SIP_STATUS_404							404
#define _SIP_STATUS_407							407
#define _SIP_STATUS_480							480
#define _SIP_STATUS_481							481
#define _SIP_STATUS_489							489
#define _SIP_STATUS_500							500

#define _SIP_STATUS_100_STR						"SIP/2.0 100 Trying\r\n"
#define _SIP_STATUS_100_STR_LEN					strlen(_SIP_STATUS_100_STR)

#define _SIP_STATUS_404_STR						"SIP/2.0 404 User Unknown\r\n"
#define _SIP_STATUS_404_STR_LEN					strlen(_SIP_STATUS_404_STR)

#define _SIP_STATUS_407_STR						"SIP/2.0 407 Proxy Authentication Required\r\n"
#define _SIP_STATUS_407_STR_LEN					strlen(_SIP_STATUS_407_STR)

#define _SIP_STATUS_480_STR						"SIP/2.0 480 Temporarily Unavailable\r\n"
#define _SIP_STATUS_480_STR_LEN					strlen(_SIP_STATUS_480_STR)

#define _SIP_STATUS_481_STR						"SIP/2.0 481 Transaction Does Not Exist\r\n"
#define _SIP_STATUS_481_STR_LEN					strlen(_SIP_STATUS_481_STR)

#define _SIP_STATUS_487_STR						"SIP/2.0 487 Request Terminated\r\n"
#define _SIP_STATUS_487_STR_LEN					strlen(_SIP_STATUS_487_STR)

#define _SIP_STATUS_489_STR						"SIP/2.0 489 Event Package Not Supported\r\n"
#define _SIP_STATUS_489_STR_LEN					strlen(_SIP_STATUS_489_STR)

#define _SIP_STATUS_500_STR						"SIP/2.0 500 Server Internal Error\r\n"
#define _SIP_STATUS_500_STR_LEN					strlen(_SIP_STATUS_500_STR)

#define _SIP_HEADER_ALLOW						"Allow: INVITE, ACK, CANCEL, BYE\r\n"
#define _SIP_HEADER_ALLOW_LEN					strlen(_SIP_HEADER_ALLOW)

#define _SIP_HEADER_CONTENT_LENGTH				"Content-Length: 0\r\n"
#define _SIP_HEADER_CONTENT_LENGTH_LEN			strlen(_SIP_HEADER_CONTENT_LENGTH)

#define _CRLF									"\r\n"
#define _CRLF_LEN								2

static unsigned int								_session_id;
static unsigned int								_session_version;

static int __send_status						(int status, sip_header_info_t*	sip_header, meshap_sip_sta_info_t* sta_info);

static int _handle_register_request				(meshap_sip_sta_info_t* sta_info, sip_header_info_t* sip_header, al_packet_t* packet_in);
static int _handle_subscribe_request			(meshap_sip_sta_info_t*	sta_info, sip_header_info_t* sip_header, al_packet_t* packet_in);
static int _handle_direct_invite_request		(meshap_sip_sta_info_t*	sta_info, sip_header_info_t* sip_header);
static int _handle_sta_ack_request				(meshap_sip_sta_info_t* sta_info, sip_header_info_t* sip_header);
static int _handle_sta_bye_request				(meshap_sip_sta_info_t* sta_info, sip_header_info_t* sip_header);
static int _handle_sta_cancel_request			(meshap_sip_sta_info_t* sta_info, sip_header_info_t* sip_header);
static int _handle_direct_call_status			(meshap_sip_sta_info_t* sta_info, sip_header_info_t* sip_header);

static int __handle_dlg_verification_response	(meshap_sip_dlg_table_entry_t* dlg_table_entry, meshap_sip_sta_info_t* sta_info, sip_header_info_t* sip_header);
static int __handle_status_from_callee			(sip_header_info_t* sip_header, meshap_sip_dlg_table_entry_t* dlg_table_entry);
static int __handle_status_from_caller			(sip_header_info_t* sip_header, meshap_sip_dlg_table_entry_t* dlg_table_entry);

int _sip_packet_helper_initialize(void)
{
	_session_id			= 1;
	_session_version	= 1;
	return 0;
}

int _sip_packet_helper_uninitialize()
{
	return 0;
}

int _sip_packet_helper_process_sip_packet(sip_header_info_t*	sip_header,
										al_net_if_t*			net_if,
										al_packet_t*			packet_in)
{
	meshap_sip_sta_info_t*			sip_sta_info;
	int						ret;
	unsigned char*			p;

#define _IP_HEADER_SRC_ADDR_POSITION 12

	p			= packet_in->buffer + packet_in->position + _IP_HEADER_DEST_ADDR_POSITION;
	if(memcmp(p, _config.server_ip, 4) != 0) { 
		return MESHAP_SIP_RETVAL_CONTINUE;
	}

	sip_sta_info = _sip_data_find_sip_sta(&packet_in->addr_2.bytes, STA_SEARCH_TYPE_MAC);
	if(sip_sta_info == NULL) {
		return MESHAP_SIP_RETVAL_CONTINUE;
	}

	sip_sta_info->rport				= sip_header->rport;
	sip_sta_info->net_if			= net_if;
	sip_sta_info->last_check_time 	= al_get_tick_count(AL_CONTEXT);
	
	p = packet_in->buffer + packet_in->position + _IP_HEADER_SRC_ADDR_POSITION;
	memcpy(sip_sta_info->ip_addr.bytes, p, 4);

	if(!_IS_STA_REGISTERED(sip_sta_info->flags) || !_IS_STA_LOCAL(sip_sta_info->flags))
		_sip_mesh_helper_bcast_sta_registration(sip_sta_info);

	_SET_STA_LOCATION_LOCAL(sip_sta_info->flags)
	_REGISTER_STA(sip_sta_info->flags)

	if(_sip_data_check_silent_mode() != 0) {
		return MESHAP_SIP_RETVAL_CONTINUE;
	}

	ret = MESHAP_SIP_RETVAL_CONTINUE;

	if(sip_header->type == SIP_TYPE_REQUEST) {
		
		switch(sip_header->sub_type) {
			case SIP_SUB_TYPE_REQUEST_REGISTER:
				ret = _handle_register_request(sip_sta_info, sip_header, packet_in);
				break;
			case SIP_SUB_TYPE_REQUEST_SUBSCRIBE:
				ret = _handle_subscribe_request(sip_sta_info, sip_header, packet_in);
				break;
			case SIP_SUB_TYPE_REQUEST_INVITE:
				ret = _handle_direct_invite_request(sip_sta_info, sip_header);
				break;
			case SIP_SUB_TYPE_REQUEST_ACK:
				ret = _handle_sta_ack_request(sip_sta_info, sip_header);
				break;
			case SIP_SUB_TYPE_REQUEST_BYE:
				ret = _handle_sta_bye_request(sip_sta_info, sip_header);
				break;
			case SIP_SUB_TYPE_REQUEST_CANCEL:
				ret = _handle_sta_cancel_request(sip_sta_info, sip_header);
				break;
			default:
				return MESHAP_SIP_RETVAL_DISCARD;
		}

	} else if(sip_header->type == SIP_TYPE_STATUS) {

		al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "%s from %s\n", sip_header->status_line,
																				sip_sta_info->info.extn);
		ret = _handle_direct_call_status(sip_sta_info, sip_header);

	}

	return ret;

#undef _IP_HEADER_SRC_ADDR_POSITION
}


int _sip_packet_helper_send_packet(meshap_sip_sta_info_t* sta_info, unsigned char* buffer, int buffer_length)
{
	al_net_addr_t				sta_mac_addr;
	al_packet_t*				packet_out;
	al_net_addr_t				relay_ap_addr;
	al_net_if_t*				out_net_if;
	mesh_ext_table_entry_t		ext_entry;
	access_point_sta_info_t		ap_sta_info;
	
	packet_out	= al_allocate_packet(AL_CONTEXT AL_PACKET_DIRECTION_OUT);
	if(packet_out == NULL)
		return -1;

	memset(&ap_sta_info, 0, sizeof(access_point_sta_info_t));
	memset(&sta_mac_addr, 0, sizeof(al_net_addr_t));

	sta_mac_addr.length = 6;
	memcpy(sta_mac_addr.bytes, sta_info->info.mac, 6);
	
	access_point_get_sta_info(AL_CONTEXT &sta_mac_addr, &ap_sta_info);

	if(ap_sta_info.net_if != NULL) {

		out_net_if = ap_sta_info.net_if;
		memcpy(&relay_ap_addr, &sta_mac_addr, sizeof(al_net_addr_t)); 

	} else {
		
		if(mesh_ext_get_address_info(sta_info->info.mac, &ext_entry) != 0)
			return -1;

		memcpy(&relay_ap_addr, &ext_entry.relay_ap_addr, sizeof(al_net_addr_t)); 
		out_net_if = ext_entry.net_if;

	}

	if(out_net_if == NULL)
		return -1;

	udp_setup_packet(packet_out,
					&out_net_if->config.hw_addr,
					_config.port,
					&relay_ap_addr,
					sta_info->rport,
					buffer, buffer_length);

	ip_setup_packet_ex(packet_out, _config.server_ip, sta_info->ip_addr.bytes);

	if(AL_NET_IF_IS_WIRELESS(out_net_if)) {

		if(ap_sta_info.net_if != NULL) {
			
			packet_out->frame_control	= AL_802_11_FC_FROMDS;
			memcpy(&packet_out->addr_3,&packet_out->addr_2,sizeof(al_net_addr_t));
			memcpy(&packet_out->addr_2,&out_net_if->config.hw_addr,sizeof(al_net_addr_t));

		} else {
			
			packet_out->frame_control	= (AL_802_11_FC_FROMDS | AL_802_11_FC_TODS);

			memcpy(&packet_out->addr_1, &relay_ap_addr, sizeof(al_net_addr_t));
			memcpy(&packet_out->addr_2,&out_net_if->config.hw_addr,sizeof(al_net_addr_t));
			memcpy(&packet_out->addr_3, &sta_mac_addr, sizeof(al_net_addr_t));
			memcpy(&packet_out->addr_4, &_config.ds_net_if->config.hw_addr, sizeof(al_net_addr_t));

		}

	}
	
	out_net_if->transmit(out_net_if, packet_out);

	tx_rx_pkt_stats.tx_sip_pkt++;
	al_release_packet(AL_CONTEXT packet_out);

	return 0;
}

static int __send_status(int status, sip_header_info_t*	sip_header, meshap_sip_sta_info_t* sta_info)
{
	unsigned char*	p;
	unsigned char*	buffer;
	unsigned char	tag[4];

	buffer		= al_get_gen_2KB_buffer(AL_CONTEXT_SINGLE);
	p			= buffer;

	al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "Sending status %d\n", status);

	switch(status) {
		case _SIP_STATUS_100:
			memcpy(p, _SIP_STATUS_100_STR,_SIP_STATUS_100_STR_LEN);			p	+= _SIP_STATUS_100_STR_LEN;
			break;
		case _SIP_STATUS_200:
			memcpy(p, _SIP_STATUS_200_STR,_SIP_STATUS_200_STR_LEN);			p	+= _SIP_STATUS_200_STR_LEN;
			break;
		case _SIP_STATUS_404:
			memcpy(p, _SIP_STATUS_404_STR, _SIP_STATUS_404_STR_LEN);		p	+= _SIP_STATUS_404_STR_LEN;
			break;
		case _SIP_STATUS_407:
			memcpy(p, _SIP_STATUS_407_STR, _SIP_STATUS_407_STR_LEN);		p	+= _SIP_STATUS_407_STR_LEN;
			break;
		case _SIP_STATUS_481:
			memcpy(p, _SIP_STATUS_481_STR, _SIP_STATUS_481_STR_LEN);		p	+= _SIP_STATUS_481_STR_LEN;
			break;
		case _SIP_STATUS_489:
			memcpy(p, _SIP_STATUS_489_STR, _SIP_STATUS_489_STR_LEN);		p	+= _SIP_STATUS_489_STR_LEN;
			break;

	} 

	p += sprintf(p, "Via: SIP/2.0/UDP %d.%d.%d.%d:%d;branch=%s;rport=%d\r\n", _config.server_ip[0],
																					_config.server_ip[1],
																					_config.server_ip[2],
																					_config.server_ip[3],
																					_config.port,
																					sip_header->via.branch,
																					sip_header->rport);

	if(sip_header->expires > 0 && status == _SIP_STATUS_200) {
		
		p += sprintf(p, "Contact: %s;expires=%d\r\n", sip_header->contact, sip_header->expires);
		
		memcpy(p, _SIP_HEADER_ALLOW, _SIP_HEADER_ALLOW_LEN);				p	+= _SIP_HEADER_ALLOW_LEN;

	} else if(status == _SIP_STATUS_407) {
		
		p += sprintf(p, "Proxy-Authenticate: Digest nonce=\"12845779034:a108e724bdee13e0f60f806186c75226\",algorithm=MD5,realm=\"MD_SIP_Server\"\r\n");

	}

	if(status != _SIP_STATUS_100) {
		
		al_cryptographic_random_bytes(AL_CONTEXT (char*)tag, 4);
		p += sprintf(p, "To: %s;tag=%02x%02x%02x%02x\r\n", sip_header->to.unparsed_str, tag[0],
																	tag[1],tag[2],tag[3]);
	} else {
		p += sprintf(p, "To: %s\r\n", sip_header->to.unparsed_str);
	}

	p += sprintf(p, "From: %s\r\n", sip_header->from.unparsed_str);

	p += sprintf(p, "Call-ID: %s\r\n", sip_header->call_id);

	p += sprintf(p, "CSeq: %s\r\n", sip_header->cseq.unparsed_str);

	memcpy(p, _SIP_HEADER_CONTENT_LENGTH, _SIP_HEADER_CONTENT_LENGTH_LEN);	p	+= _SIP_HEADER_CONTENT_LENGTH_LEN;

	memcpy(p, _CRLF, _CRLF_LEN);											p	+= _CRLF_LEN;

	_sip_packet_helper_send_packet(sta_info, buffer, (int)(p-buffer));

	al_release_gen_2KB_buffer(AL_CONTEXT buffer);

	return 0;

}


static int _handle_register_request(meshap_sip_sta_info_t*		sta_info,
										sip_header_info_t*		sip_header,
										al_packet_t*			packet_in)
{
	if(strcmp(sta_info->info.extn, sip_header->from.extn) != 0)
		return __send_status(_SIP_STATUS_404, sip_header, sta_info);

	if(sip_header->expires <= 0) {
		al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "Unregistering sta %s\n", sta_info->info.extn);
		_UNREGISTER_STA(sta_info->flags);
	} else {
		al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "Registering sta %s\n", sta_info->info.extn);
		_REGISTER_STA(sta_info->flags);
		memcpy(&sta_info->ip_addr.bytes, sip_header->client_ip, 4);
		sta_info->rport = sip_header->rport;	
		al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "%s is at %d.%d.%d.%d:%d\n", sta_info->info.extn,
																						sta_info->ip_addr.bytes[0],
																						sta_info->ip_addr.bytes[1],
																						sta_info->ip_addr.bytes[2],
																						sta_info->ip_addr.bytes[3],
																						sta_info->rport);
	}
	
	_sip_mesh_helper_bcast_sta_registration(sta_info);
	__send_status(_SIP_STATUS_200, sip_header, sta_info);

	return MESHAP_SIP_RETVAL_DISCARD;
}

static int _handle_subscribe_request(meshap_sip_sta_info_t*		sta_info,
									 sip_header_info_t*	sip_header,
									 al_packet_t*		packet_in)
{
	return __send_status(_SIP_STATUS_489, sip_header, sta_info);
}

static int _handle_direct_invite_request(meshap_sip_sta_info_t*		sta_info,
										 sip_header_info_t*			sip_header)
{
	meshap_sip_dlg_info_t*			caller_dlg;
	meshap_sip_dlg_info_t*			callee_dlg;
	meshap_sip_dlg_table_entry_t*	dlg_table_entry;
	unsigned char*					p_body;

	dlg_table_entry = _sip_data_find_dlg_by_call_id(sip_header->call_id);
	if(dlg_table_entry == NULL) {
		dlg_table_entry = _sip_data_get_empty_dlg(1);
		if(dlg_table_entry == NULL)
			return __send_status(_SIP_STATUS_500, sip_header, sta_info);
	}

	dlg_table_entry->caller_info = sta_info;
	dlg_table_entry->callee_info = _sip_data_find_sip_sta(sip_header->to.extn, STA_SEARCH_TYPE_EXTN);
	
	if(dlg_table_entry->callee_info == NULL) {
		__send_status(_SIP_STATUS_404, sip_header, sta_info);
		memset(dlg_table_entry, 0, sizeof(meshap_sip_dlg_table_entry_t));
		return MESHAP_SIP_RETVAL_DISCARD;
	}

	if(!_IS_STA_REGISTERED(dlg_table_entry->callee_info->flags)) {
		__send_status(_SIP_STATUS_404, sip_header, sta_info);
		memset(dlg_table_entry, 0, sizeof(meshap_sip_dlg_table_entry_t));
		return MESHAP_SIP_RETVAL_DISCARD;
	}

	caller_dlg = &dlg_table_entry->caller_dlg;
	callee_dlg = &dlg_table_entry->callee_dlg;

	strcpy(caller_dlg->branch, sip_header->via.branch);
	strcpy(caller_dlg->from_tag, sip_header->from.tag);
	strcpy(caller_dlg->call_id, sip_header->call_id);
	memset(dlg_table_entry->extra_info, 0, 1024);
	
	p_body = dlg_table_entry->extra_info;

	p_body	+= sprintf(p_body, "v=0\r\n");
	p_body	+= sprintf(p_body, "o=- %d %d IN IP4 %d.%d.%d.%d\r\n", _session_id++, _session_version++,
																	_config.server_ip[0], _config.server_ip[1],
																	_config.server_ip[2], _config.server_ip[3]);
	p_body	+= sprintf(p_body, "s=MDPS Audio call\r\n");
	p_body	+= sprintf(p_body, "%s", sip_header->message_body);

	caller_dlg->cseq			= sip_header->cseq.sqnr;
	caller_dlg->call_initiator	= 1;
	
	if(_sip_data_evaluate_sta_locations(dlg_table_entry) != 0) {
		memset(dlg_table_entry, 0, sizeof(meshap_sip_dlg_table_entry_t));
		return MESHAP_SIP_RETVAL_DISCARD;
	}

	_sip_mesh_helper_bcast_new_call_dialog(dlg_table_entry);

	//__send_status(_SIP_STATUS_100, sip_header, sta_info);

	caller_dlg->call_state = CALL_STATE_INVITE;
	callee_dlg->call_state = CALL_STATE_START;

	return _sip_dlg_handle_call_dlg(dlg_table_entry, PENDING_STATUS_FOR_CALLEE);
}

static int _handle_sta_ack_request(meshap_sip_sta_info_t*	sta_info,
										sip_header_info_t*	sip_header)
{
	meshap_sip_dlg_table_entry_t*	dlg_entry;
	
	dlg_entry	= _sip_data_find_dlg_by_call_id(sip_header->call_id);

	if(dlg_entry == NULL) {
		return __send_status(_SIP_STATUS_481, sip_header, sta_info);
	}

	if(_sip_data_evaluate_sta_locations(dlg_entry) != 0) {
		memset(dlg_entry, 0, sizeof(meshap_sip_dlg_table_entry_t));
		return MESHAP_SIP_RETVAL_DISCARD;
	}

	dlg_entry->caller_dlg.cseq = sip_header->cseq.sqnr;
	strcpy(dlg_entry->caller_dlg.branch, sip_header->via.branch);

	/* sta sends ack when either call is setup or is ended due to error */
	if(dlg_entry->caller_dlg.call_state == CALL_STATE_STATUS_ERROR ||
		dlg_entry->caller_dlg.call_state == CALL_STATE_END) {
		dlg_entry->caller_dlg.call_state = CALL_STATE_END;
	} else 
		dlg_entry->caller_dlg.call_state = CALL_STATE_ESTABLISHED;

	return _sip_dlg_handle_call_dlg(dlg_entry, PENDING_STATUS_FOR_CALLEE);

}

static int _handle_sta_bye_request(meshap_sip_sta_info_t*	sta_info,
										sip_header_info_t*	sip_header)
{
	meshap_sip_dlg_table_entry_t*	dlg_entry;
	meshap_sip_dlg_info_t*			sender_dlg;
	meshap_sip_dlg_info_t*			rcvr_dlg;
	unsigned char					pending_status;

	dlg_entry	= _sip_data_find_dlg_by_call_id(sip_header->call_id);

	if(dlg_entry == NULL) {
		return __send_status(_SIP_STATUS_481, sip_header, sta_info);
	}

	if(_sip_data_evaluate_sta_locations(dlg_entry) != 0) {
		memset(dlg_entry, 0, sizeof(meshap_sip_dlg_table_entry_t));
		return MESHAP_SIP_RETVAL_DISCARD;
	}

	sender_dlg		= &dlg_entry->caller_dlg;
	rcvr_dlg		= &dlg_entry->callee_dlg;
	pending_status	= PENDING_STATUS_FOR_CALLEE;

	if(sta_info != dlg_entry->caller_info) {
		sender_dlg		= &dlg_entry->callee_dlg;
		rcvr_dlg		= &dlg_entry->caller_dlg;
		pending_status	= PENDING_STATUS_FOR_CALLER;
	}

	sender_dlg->cseq = sip_header->cseq.sqnr;

	if(strcmp(sender_dlg->branch, sip_header->via.branch) != 0) {
		strcpy(sender_dlg->branch, sip_header->via.branch);
		_sip_data_create_branch(rcvr_dlg->branch);
		rcvr_dlg->cseq++;
	}

	sender_dlg->call_state = CALL_STATE_BYE;

	return _sip_dlg_handle_call_dlg(dlg_entry, pending_status);

}

static int _handle_sta_cancel_request(meshap_sip_sta_info_t*	sta_info,
										sip_header_info_t*		sip_header)
{
	meshap_sip_dlg_table_entry_t*	dlg_entry;
	meshap_sip_dlg_info_t*			sender_dlg;
	unsigned char					pending_status;

	dlg_entry	= _sip_data_find_dlg_by_call_id(sip_header->call_id);

	if(dlg_entry == NULL) {
		return __send_status(_SIP_STATUS_481, sip_header, sta_info);
	}

	if(sta_info == dlg_entry->caller_info && !_IS_STA_REGISTERED(dlg_entry->callee_info->flags)) {
		__send_status(_SIP_STATUS_200, sip_header, sta_info);
		memset(dlg_entry, 0, sizeof(meshap_sip_dlg_table_entry_t));
		return MESHAP_SIP_RETVAL_DISCARD;
	} else if(sta_info == dlg_entry->callee_info && !_IS_STA_REGISTERED(dlg_entry->caller_info->flags)) {
		__send_status(_SIP_STATUS_200, sip_header, sta_info);
		memset(dlg_entry, 0, sizeof(meshap_sip_dlg_table_entry_t));
		return MESHAP_SIP_RETVAL_DISCARD;
	}

	if(_sip_data_evaluate_sta_locations(dlg_entry) != 0) {
		memset(dlg_entry, 0, sizeof(meshap_sip_dlg_table_entry_t));
		return MESHAP_SIP_RETVAL_DISCARD;
	}

	sender_dlg		= &dlg_entry->caller_dlg;
	pending_status	= PENDING_STATUS_FOR_CALLEE;

	if(sta_info != dlg_entry->caller_info) {
		sender_dlg		= &dlg_entry->callee_dlg;
		pending_status	= PENDING_STATUS_FOR_CALLER;
	}

	sender_dlg->cseq = sip_header->cseq.sqnr;

	sender_dlg->call_state = CALL_STATE_CANCEL;

	return _sip_dlg_handle_call_dlg(dlg_entry, pending_status);

}

static int _handle_direct_call_status(meshap_sip_sta_info_t*	sta_info,
										sip_header_info_t*		sip_header)
{
	meshap_sip_dlg_table_entry_t*	dlg_table_entry;
	unsigned char					pending_status;

	dlg_table_entry = _sip_data_find_dlg_by_call_id(sip_header->call_id);

	if(dlg_table_entry == NULL)
		return MESHAP_SIP_RETVAL_DISCARD;

	pending_status = PENDING_STATUS_FOR_NONE;
	
	if(_IS_CALL_DLG_UNDER_VERIFICATION(dlg_table_entry->flags)) {
		if(__handle_dlg_verification_response(dlg_table_entry, sta_info, sip_header) != 0) {
			return _sip_dlg_handle_call_dlg(dlg_table_entry, pending_status);
		}
	} else {
	
		if(_sip_data_evaluate_sta_locations(dlg_table_entry) != 0) {
			memset(dlg_table_entry, 0, sizeof(meshap_sip_dlg_table_entry_t));
			return MESHAP_SIP_RETVAL_DISCARD;
		}
	
		if(sta_info == dlg_table_entry->callee_info)
			pending_status = __handle_status_from_callee(sip_header, dlg_table_entry);
		else 
			pending_status = __handle_status_from_caller(sip_header, dlg_table_entry);
		
	}
	
	return _sip_dlg_handle_call_dlg(dlg_table_entry, pending_status);

}

static int __handle_dlg_verification_response(meshap_sip_dlg_table_entry_t*	dlg_table_entry, 
												meshap_sip_sta_info_t* 		sta_info, 
												sip_header_info_t* 			sip_header)
{
	if(sta_info != dlg_table_entry->caller_info && sta_info != dlg_table_entry->callee_info)
		return 0;
	
	al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "Verification of call dialog %x\n", dlg_table_entry->id);
	al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "Sta says : %s\n", sip_header->status_line);
	
	if(sip_header->status_class != SIP_STATUS_CLASS_SUCCESS) {
		dlg_table_entry->callee_dlg.call_state	= CALL_STATE_END;
		dlg_table_entry->caller_dlg.call_state	= CALL_STATE_END;
		return -1;
	}
	
	if(sta_info == dlg_table_entry->caller_info)
		_RESET_DLG_VERIFY_CALLER(dlg_table_entry->flags)
	else if(sta_info == dlg_table_entry->callee_info)	
		_RESET_DLG_VERIFY_CALLEE(dlg_table_entry->flags)
		
	return 0;
}

static int __handle_status_from_callee(sip_header_info_t* sip_header, meshap_sip_dlg_table_entry_t*	dlg_table_entry)
{
	unsigned char*	p_body;
	int 			pending_status;
	
	pending_status = PENDING_STATUS_FOR_NONE;
	
	switch(sip_header->status_class) {
		case SIP_STATUS_CLASS_INFORMATION:
			if(sip_header->sub_type == SIP_SUB_TYPE_STATUS_100) {
				dlg_table_entry->callee_dlg.call_state	= CALL_STATE_TRYING;
				dlg_table_entry->caller_dlg.call_state	= CALL_STATE_TRYING;
				pending_status							= PENDING_STATUS_FOR_CALLER;
			} else if(sip_header->sub_type == SIP_SUB_TYPE_STATUS_180) {
				strcpy(dlg_table_entry->callee_dlg.to_tag, sip_header->to.tag);
				_sip_data_create_tag(dlg_table_entry->caller_dlg.to_tag);
				dlg_table_entry->callee_dlg.call_state	= CALL_STATE_RINGING;
				dlg_table_entry->caller_dlg.call_state	= CALL_STATE_RINGING;
				pending_status							= PENDING_STATUS_FOR_CALLER;
			}
			break;
		case SIP_STATUS_CLASS_CLIENT_ERROR:
			dlg_table_entry->callee_dlg.cseq 	= sip_header->cseq.sqnr;
			memset(dlg_table_entry->extra_info, 0, 1024);
			strcpy(dlg_table_entry->extra_info, sip_header->status_line);
			pending_status						= PENDING_STATUS_FOR_CALLER;
			if(dlg_table_entry->callee_dlg.call_state == CALL_STATE_CANCEL) {
				dlg_table_entry->callee_dlg.call_state	= CALL_STATE_END;
			} else {
				dlg_table_entry->callee_dlg.call_state	= CALL_STATE_STATUS_ERROR;
			}
			break;
		case SIP_STATUS_CLASS_SUCCESS:
			if(dlg_table_entry->callee_dlg.call_state == CALL_STATE_RINGING) {

				memset(dlg_table_entry->extra_info, 0, 1024);
				
				p_body = dlg_table_entry->extra_info;

				p_body	+= sprintf(p_body, "v=0\r\n");
				p_body	+= sprintf(p_body, "o=- %d %d IN IP4 %d.%d.%d.%d\r\n", _session_id++, _session_version++,
																				_config.server_ip[0], _config.server_ip[1],
																				_config.server_ip[2], _config.server_ip[3]);
				p_body	+= sprintf(p_body, "s=MDPS Audio call\r\n");
				p_body	+= sprintf(p_body, "%s", sip_header->message_body);

				dlg_table_entry->callee_dlg.call_state	= CALL_STATE_STATUS_OK;
				pending_status							= PENDING_STATUS_FOR_CALLER;

			} else if(dlg_table_entry->callee_dlg.call_state == CALL_STATE_STATUS_OK ||
						dlg_table_entry->callee_dlg.call_state == CALL_STATE_ESTABLISHED) {

				if(dlg_table_entry->caller_dlg.call_state == CALL_STATE_ESTABLISHED) {
				
					dlg_table_entry->callee_dlg.call_state	= CALL_STATE_ESTABLISHED;
					pending_status							= PENDING_STATUS_FOR_CALLER;
					
				} else if(dlg_table_entry->caller_dlg.call_state == CALL_STATE_BYE) {

					dlg_table_entry->callee_dlg.call_state	= CALL_STATE_END;
					dlg_table_entry->caller_dlg.call_state	= CALL_STATE_END;
					pending_status							= PENDING_STATUS_FOR_CALLER;

				} 

			} else if(dlg_table_entry->caller_dlg.call_state == CALL_STATE_STATUS_ERROR) {

				dlg_table_entry->callee_dlg.call_state	= CALL_STATE_END;
				dlg_table_entry->caller_dlg.call_state	= CALL_STATE_END;
				pending_status							= PENDING_STATUS_FOR_CALLER;

			}
			break;

	}
	
	return pending_status;
}

static int __handle_status_from_caller(sip_header_info_t* sip_header, meshap_sip_dlg_table_entry_t*	dlg_table_entry)
{
	int 			pending_status;
	
	pending_status = PENDING_STATUS_FOR_NONE;

	switch(sip_header->status_class) {

		case SIP_STATUS_CLASS_CLIENT_ERROR:
			if(dlg_table_entry->caller_dlg.call_state == CALL_STATE_CANCEL) {
				
				dlg_table_entry->caller_dlg.cseq = sip_header->cseq.sqnr;
				memset(dlg_table_entry->extra_info, 0, 1024);
				strcpy(dlg_table_entry->extra_info, sip_header->status_line);
				dlg_table_entry->caller_dlg.call_state	= CALL_STATE_END;
				pending_status							= PENDING_STATUS_FOR_CALLEE;

			}
			break;
		case SIP_STATUS_CLASS_SUCCESS:
			if(dlg_table_entry->callee_dlg.call_state == CALL_STATE_BYE) {
				
				dlg_table_entry->caller_dlg.call_state	= CALL_STATE_END;
				dlg_table_entry->callee_dlg.call_state	= CALL_STATE_END;
				pending_status							= PENDING_STATUS_FOR_CALLEE;

			}
			break;
	}

	return pending_status;
}

