/********************************************************************************
* MeshDynamics
* --------------
* File     : meshap_sip.h
* Comments : SIP Implementation
* Created  : 1/16/2008
* Author   : Abhijit Ayarekar
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  0  |1/16/2008 | Created.                                        |Abhijit |
* -----------------------------------------------------------------------------
********************************************************************************/

#ifndef __MESHAP_SIP_H__
#define __MESHAP_SIP_H__

typedef void * meshap_sip_instance_t;

struct meshap_sip_sta_info
{
   sip_conf_sta_info_t info;
   al_ip_addr_t        ip_addr;
   unsigned short      rport;
   unsigned char       flags;
   al_net_if_t         *net_if;
   al_u64_t            last_check_time;
};

typedef struct meshap_sip_sta_info   meshap_sip_sta_info_t;

struct meshap_sip_dlg_info
{
   unsigned char call_state;
   unsigned char call_initiator;
   unsigned char branch[256];
   unsigned char rinstance[32];
   unsigned char from_tag[64];
   unsigned char to_tag[64];
   unsigned char call_id[256];
   unsigned char cseq;
};

typedef struct meshap_sip_dlg_info   meshap_sip_dlg_info_t;

#define DLG_TABLE_FLAGS_UNUSED           0
#define DLG_TABLE_FLAGS_USED             1
#define DLG_TABLE_FLAGS_VERIFY_CALLER    2
#define DLG_TABLE_FLAGS_VERIFY_CALLEE    4

struct meshap_sip_dlg_table_entry
{
   unsigned int          id;
   unsigned char         flags;
   meshap_sip_dlg_info_t caller_dlg;
   meshap_sip_dlg_info_t callee_dlg;
   meshap_sip_sta_info_t *caller_info;
   meshap_sip_sta_info_t *callee_info;
   unsigned char         extra_info[1024];
   unsigned char         call_init_uninit_time;                                 //is a multiple of 5 seconds
};

typedef struct meshap_sip_dlg_table_entry   meshap_sip_dlg_table_entry_t;

#define MESHAP_SIP_RETVAL_SUCCESS     0
#define MESHAP_SIP_RETVAL_DISCARD     -1
#define MESHAP_SIP_RETVAL_CONTINUE    -2

meshap_sip_instance_t meshap_sip_initialize(al_net_if_t *ds_net_if, sip_conf_info_t *info);
void meshap_sip_uninitialize(meshap_sip_instance_t instance);
int meshap_process_sip_packet(meshap_sip_instance_t instance, al_net_if_t *net_if, al_packet_t *packet_in);
int meshap_sip_enumerate_stas(void *callback, void (*enum_routine)(void *callback, const meshap_sip_sta_info_t *sta));
int meshap_sip_enumerate_call_dlgs(void *callback, void (*enum_routine)(void *callback, const meshap_sip_dlg_table_entry_t *dlg));
int meshap_process_imcp_sip_packet(al_net_addr_t *sender_mac, unsigned char *packet_data, unsigned short data_length);

#endif
