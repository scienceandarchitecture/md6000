/********************************************************************************
* MeshDynamics
* --------------
* File     : meshap_mesh_proc.h
* Comments : Mesh proc interface
* Created  : 6/18/2005
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  0  |6/18/2005 | Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

#include "meshap_proc.h"

#ifndef __MESHAP_MESH_PROC_H__
#define __MESHAP_MESH_PROC_H__

int meshap_mesh_proc_initialize(void);

void meshap_mesh_proc_add_read_entry(const char *name, const struct file_operations *proc_file_fops, void *data);
#endif /*__MESHAP_MESH_PROC_H__*/
