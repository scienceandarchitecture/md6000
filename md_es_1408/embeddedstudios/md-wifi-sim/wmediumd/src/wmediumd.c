/*
 *	wmediumd, wireless medium simulator for mac80211_hwsim kernel module
 *	Copyright (c) 2011 cozybit Inc.
 *
 *	Author:	Javier Lopez 	<jlopex@cozybit.com>
 *		Javier Cardona	<javier@cozybit.com>
 *
 *	This program is free software; you can redistribute it and/or
 *	modify it under the terms of the GNU General Public License
 *	as published by the Free Software Foundation; either version 2
 *	of the License, or (at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program; if not, write to the Free Software
 *	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 *	02110-1301, USA.
 */

#include <netlink/netlink.h>
#include <netlink/genl/genl.h>
#include <netlink/genl/ctrl.h>
#include <netlink/genl/family.h>
#include <stdint.h>
#include <getopt.h>
#include <signal.h>
//#include <pcap/pcap.h>

#include "rawsocket.h"
#include "wmediumd.h"
#include "probability.h"
#include "mac_address.h"
#include "ieee80211.h"
#include "config.h"


#define VERSION_STR 0.1

#define VLAN_ETH_HLEN   18
#define VLAN_ETH_FRAME_LEN      1518

int beacon_cnt ;


struct nl_sock *sock;
struct nl_cb *cb;
struct nl_cache *cache;
struct genl_family *family;

int running = 0;
struct jammer_cfg jam_cfg;
double *prob_matrix;
int size;

static int received = 0;
static int sent = 0;
static int dropped = 0;
static int acked = 0;

extern int sock_fd;
extern struct sockaddr_ll s_addr; 
extern unsigned char rcv_mac[6];
extern  unsigned char snd_mac[6];
extern pthread_mutex_t wmq_lock;


/*
 * 	Generates a random double value
 */
int init_pcap(void);
double generate_random_double()
{

    return rand()/((double)RAND_MAX+1);
}

/*
 *	Send a tx_info frame to the kernel space.
 */

int send_tx_info_frame_nl(struct mac_address *src,
        unsigned int flags, int signal,
        struct hwsim_tx_rate *tx_attempts,
        unsigned long cookie)
{
    struct nl_msg *msg = nlmsg_alloc();
    if (!msg) {
        printf("Error allocating new message MSG!\n");
		goto out;
	}

	genlmsg_put(msg, NL_AUTO_PID, NL_AUTO_SEQ, genl_family_get_id(family),
		    0, NLM_F_REQUEST, HWSIM_CMD_TX_INFO_FRAME, VERSION_NR);

	int rc;
	rc = nla_put(msg, HWSIM_ATTR_ADDR_TRANSMITTER,
		     sizeof(struct mac_address), src);
	rc = nla_put_u32(msg, HWSIM_ATTR_FLAGS, flags);
	rc = nla_put_u32(msg, HWSIM_ATTR_SIGNAL, signal);
	rc = nla_put(msg, HWSIM_ATTR_TX_INFO,
		     IEEE80211_MAX_RATES_PER_TX *
		     sizeof(struct hwsim_tx_rate), tx_attempts);

	rc = nla_put_u64(msg, HWSIM_ATTR_COOKIE, cookie);

	if(rc!=0) {
		printf("Error filling payload\n");
		goto out;
	}

	nl_send_auto_complete(sock,msg);
	nlmsg_free(msg);
	return 0;
out:
	nlmsg_free(msg);
	return -1;
}

/*
 * 	Send a cloned frame to the kernel space.
 */

int send_cloned_frame_msg(struct mac_address *dst,
			  char *data, int data_len, int rate_idx, int signal, int chan_no)
{
	struct nl_msg *msg = nlmsg_alloc();
	if (!msg) {
		printf("Error allocating new message MSG!\n");
		goto out;
	}

	genlmsg_put(msg, NL_AUTO_PID, NL_AUTO_SEQ, genl_family_get_id(family),
		    0, NLM_F_REQUEST, HWSIM_CMD_FRAME, VERSION_NR);

	int rc;
	rc = nla_put(msg, HWSIM_ATTR_ADDR_RECEIVER,
		     sizeof(struct mac_address), dst);
	rc = nla_put(msg, HWSIM_ATTR_FRAME, data_len, data);
	rc = nla_put_u32(msg, HWSIM_ATTR_RX_RATE, rate_idx);
	rc = nla_put_u32(msg, HWSIM_ATTR_SIGNAL, signal);
    rc = nla_put_u32(msg, HWSIM_ATTR_FREQ, chan_no);

	if(rc!=0) {
		printf("Error filling payload\n");
		goto out;
	}

	nl_send_auto_complete(sock,msg);
	nlmsg_free(msg);
	return 0;
out:
	nlmsg_free(msg);
	return -1;
}

/*
 * 	Get a signal value by rate index
 */

int get_signal_by_rate(int rate_idx)
{
	const int rate2signal [] =
		{ -80,-77,-74,-71,-69,-66,-64,-62,-59,-56,-53,-50 };
	if (rate_idx >= 0 || rate_idx < IEEE80211_AVAILABLE_RATES)
		return rate2signal[rate_idx];
	return 0;
}

/*
 * 	Send a frame applying the loss probability of the link
 */

int send_frame_msg_apply_prob_and_rate(struct mac_address *src,
				       struct mac_address *dst,
				       char *data, int data_len, int rate_idx)
{

	/* At higher rates higher loss probability*/
	double prob_per_link = find_prob_by_addrs_and_rate(prob_matrix,
							   src,dst, rate_idx);
	double random_double = generate_random_double();

	if (random_double < prob_per_link) {
		//printf("dropped\n");
		dropped++;
		return 0;
	} else {

		//printf("sent\n");
		/*received signal level*/
        int freq = 2347;
		int signal = get_signal_by_rate(rate_idx);

		send_cloned_frame_msg(dst,data,data_len,rate_idx,signal,freq);
		sent++;
		return 1;
	}
}

/*
 * 	Set a tx_rate struct to not valid values
 */

void set_all_rates_invalid(struct hwsim_tx_rate* tx_rate)
{
	int i;
	/* set up all unused rates to be -1 */
	for (i=0; i < IEEE80211_MAX_RATES_PER_TX; i++) {
        	tx_rate[i].idx = -1;
		tx_rate[i].count = 0;
	}
}

/*
 *	Determine whether we should be jamming this transmitting mac.
 */
int jam_mac(struct jammer_cfg *jcfg, struct mac_address *src)
{
	int jam = 0, i;

	for (i = 0; i < jcfg->nmacs; i++) {
		if (!memcmp(&jcfg->macs[i], src, sizeof(struct mac_address))) {
			jam = 1;
			break;
		}
	}
	return jam;
}

/*
 * 	Iterate all the radios and send a copy of the frame to each interface.
 */

void send_frames_to_radios_with_retries(struct mac_address *src, char*data,
					int data_len, unsigned int flags,
					struct hwsim_tx_rate *tx_rates,
					unsigned long cookie)
{

	struct mac_address *dst;
	struct ieee80211_hdr *hdr = (struct ieee80211_hdr *)data;
	struct hwsim_tx_rate tx_attempts[IEEE80211_MAX_RATES_PER_TX];

	int round = 0, tx_ok = 0, counter, i;

	if (jam_cfg.jam_all || jam_mac(&jam_cfg, src)) {
		printf("medium busy!\n");
		return;
	}
	/* We prepare the tx_attempts struct */
	set_all_rates_invalid(tx_attempts);

	while (round < IEEE80211_MAX_RATES_PER_TX &&
	       tx_rates[round].idx != -1 && tx_ok!=1) {

		counter = 1;

		/* Set rate index and flags used for this round */
		tx_attempts[round].idx = tx_rates[round].idx;

		while(counter <= tx_rates[round].count && tx_ok !=1 ) {

			/* Broadcast the frame to all the radio ifaces*/
			for (i=0;i<size;i++) {

				dst =  get_mac_address(i);

				/*
				 * If origin and destination are the
				 * same just skip this iteration
				*/
				if(memcmp(src,dst,sizeof(struct mac_address))
					  == 0 ){
					continue;
				}

				/* Try to send it to a radio and if
				 * the frame is destined to this radio tx_ok
				*/
				if(send_frame_msg_apply_prob_and_rate(
					src, dst, data, data_len,
					tx_attempts[round].idx) &&
					memcmp(dst, hdr->addr1,
					sizeof(struct mac_address))==0) {
						tx_ok = 1;
				}
			}
			tx_attempts[round].count = counter;
			counter++;
		}
		round++;
	}

	if (tx_ok) {
		/* if tx is done and acked a frame with the tx_info is
		 * sent to original radio iface
		*/
		acked++;
		int signal = get_signal_by_rate(tx_attempts[round-1].idx);
		/* Let's flag this frame as ACK'ed */
		flags |= HWSIM_TX_STAT_ACK;
		send_tx_info_frame_nl(src, flags, signal, tx_attempts, cookie);
	} else {
		send_tx_info_frame_nl(src, flags, 0, tx_attempts, cookie);
	}
}

unsigned char out_buff[VLAN_ETH_FRAME_LEN+ETH_FCS_LEN];    //Buffer for ethernet frame
unsigned char *gptr;

void *malloc_aligned(unsigned int size)
{
    void *ptr;
    int n = posix_memalign(&ptr, 256, size);
    if (n != 0) 
    {
        perror("malloc error");
    }
    memset(ptr, 0, size);
    return ptr;
}

wmd_pkt_t* create_wmd_packet(char* data, int data_len, int chan_no)
{
    wmd_pkt_t *packet = (wmd_pkt_t *)malloc_aligned(sizeof(wmd_pkt_t));

    if (packet != NULL)
    {
        packet->data = data;
        packet->data_len = data_len;
        packet->chan_no = chan_no;
        packet->next = NULL;
        return packet;
    }
    else
    {
        free(gptr);
        return NULL;
    }

}

/*
 * 	Callback function to process messages received from kernel
 */
static int process_messages_cb(struct nl_msg *msg, void *arg)
{

    struct nlattr *attrs[HWSIM_ATTR_MAX+1];
    /* netlink header */
    struct nlmsghdr *nlh = nlmsg_hdr(msg);
    /* generic netlink header*/
    struct genlmsghdr *gnlh = nlmsg_data(nlh);

    static long count = 0;
    pthread_t enq_thread;

    if(gnlh->cmd == HWSIM_CMD_FRAME) {
        /* we get the attributes*/
        genlmsg_parse(nlh, 0, attrs, HWSIM_ATTR_MAX, NULL);
        //if (attrs[HWSIM_ATTR_ADDR_TRANSMITTER]) {
            //struct mac_address *src = (struct mac_address*)
              //  nla_data(attrs[HWSIM_ATTR_ADDR_TRANSMITTER]);

            unsigned int data_len =
                nla_len(attrs[HWSIM_ATTR_FRAME]);

            char* data = (char*)nla_data(attrs[HWSIM_ATTR_FRAME]);
            //unsigned int flags =
              //  nla_get_u32(attrs[HWSIM_ATTR_FLAGS]);

            //struct hwsim_tx_rate *tx_rates =
              //  (struct hwsim_tx_rate*)
               // nla_data(attrs[HWSIM_ATTR_TX_INFO]);

            unsigned int chan_no = nla_get_u32(attrs[HWSIM_ATTR_FREQ]);

            //unsigned long cookie = nla_get_u64(attrs[HWSIM_ATTR_COOKIE]);
            received++;


            unsigned short chan1 = htons(chan_no);
            memcpy(data, (void*)rcv_mac, ETH_ALEN);
            memcpy(data+6, (void*)snd_mac, ETH_ALEN);
            memcpy(data+12, "\x81\x00", sizeof(short));
            memcpy(data+14, &chan1,  sizeof(short));
            memcpy(data+16, "\xAA\xBB",  sizeof(short));


            sendto(sock_fd, data , data_len  , 0, (struct sockaddr*)&s_addr, sizeof(s_addr));
    }
    return 0;
}

void * wmd_xmit(void *buf1)
{
    
    wmd_pkt_t *buf = (wmd_pkt_t *)buf1;
    int sent;

    struct vlan_ethhdr *out_hdr= (struct vlan_ethhdr *)buf->data;

    memcpy(out_hdr->h_dest, (void*)rcv_mac, ETH_ALEN);
    memcpy(out_hdr->h_source, (void*)snd_mac, ETH_ALEN);
    out_hdr->h_vlan_proto               = htons(ETH_P_8021Q);
    out_hdr->h_vlan_TCI                 = htons(buf->chan_no);
    out_hdr->h_vlan_encapsulated_proto  = htons(TUNNEL_PACKET_SIGNATURE);

    sent = sendto(sock_fd, buf->data, VLAN_ETH_HLEN + buf->data_len , 0, (struct sockaddr*)&s_addr, sizeof(s_addr));

#if 0
    //dump_bytes(buf->data,buf->data_len + VLAN_ETH_HLEN);
    //dump_bytes(buf->data,16);
    printf(" TX from wmediumd sent %d bytes \n",sent);
#endif
}

/*
 * 	Send a register message to kernel
 */

int send_register_msg()
{
    struct nl_msg *msg = nlmsg_alloc();
    if (!msg) {
        printf("Error allocating new message MSG!\n");
        return -1;
    }

    genlmsg_put(msg, NL_AUTO_PID, NL_AUTO_SEQ, genl_family_get_id(family),
            0, NLM_F_REQUEST, HWSIM_CMD_REGISTER, VERSION_NR);
    nl_send_auto_complete(sock,msg);
    nlmsg_free(msg);

    return 0;

}

/*
 *	Signal handler
 */
void kill_handler() {
    running = 0;
}



/*
 * 	Init netlink
 */

void init_netlink()
{

    cb = nl_cb_alloc(NL_CB_CUSTOM);
    if (!cb) {
        printf("Error allocating netlink callbacks\n");
        exit(EXIT_FAILURE);
    }

    sock = nl_socket_alloc_cb(cb);
    if (!sock) {
        printf("Error allocationg netlink socket\n");
        exit(EXIT_FAILURE);
    }
    genl_connect(sock);
    genl_ctrl_alloc_cache(sock, &cache);
    nl_socket_set_buffer_size(sock,262144,262144);
    nl_socket_set_msg_buf_size(sock,65536);

    family = genl_ctrl_search_by_name(cache, "MAC80211_HWSIM");

    if (!family) {
        printf("Family MAC80211_HWSIM not registered\n");
        exit(EXIT_FAILURE);
    }

    nl_cb_set(cb, NL_CB_MSG_IN, NL_CB_CUSTOM, process_messages_cb, NULL);

}

/*
 *	Print the CLI help
 */

void print_help(int exval)
{
    printf("wmediumd v%s - a wireless medium simulator\n", VERSION_STR);
    printf("wmediumd [-h] [-V] [-c FILE] [-o FILE]\n\n");

    printf("  -h              print this help and exit\n");
    printf("  -V              print version and exit\n\n");

    printf("  -c FILE         set intput config file\n");
    printf("  -o FILE         set output config file\n\n");

    exit(exval);
}


int main(int argc, char* argv[]) {

    int opt, ifaces;

    /* Set stdout buffering to line mode */
    setvbuf (stdout, NULL, _IOLBF, BUFSIZ);

    /* no arguments given */
    if(argc == 1) {
        fprintf(stderr, "This program needs arguments....\n\n");
        print_help(EXIT_FAILURE);
    }

    while((opt = getopt(argc, argv, "hVc:o:")) != -1) {
        switch(opt) {
            case 'h':
                print_help(EXIT_SUCCESS);
                break;
            case 'V':
                printf("wmediumd v%s - a wireless medium simulator "
                        "for mac80211_hwsim\n", VERSION_STR);
                exit(EXIT_SUCCESS);
                break;
            case 'c':
                printf("Input configuration file: %s\n", optarg);
                load_config(optarg);
                break;
            case 'o':
                printf("Output configuration file: %s\n", optarg);
                printf("How many interfaces are active?\n");
                scanf("%d",&ifaces);
                if (ifaces < 2) {
                    printf("active interfaces must be at least 2\n");
                    exit(EXIT_FAILURE);
                }
                write_config(optarg, ifaces, 0.0);
                break;
            case ':':
                printf("wmediumd: Error - Option `%c' "
                        "needs a value\n\n", optopt);
                print_help(EXIT_FAILURE);
                break;
            case '?':
                printf("wmediumd: Error - No such option:"
                        " `%c'\n\n", optopt);
                print_help(EXIT_FAILURE);
                break;
        }

    }

    if (optind < argc)
        print_help(EXIT_FAILURE);

    print_prob_matrix(prob_matrix);

    /*Handle kill signals*/
    running = 1;
    signal(SIGUSR1, kill_handler);

    /*init netlink*/
    init_netlink();

    /*init rawsocket for TX*/
    if (init_rawsocket() < 0)
        goto free_mem;

    /* init wmediumD_link for Coordinates*/
    init_wmediumD_link();

    /*init pcap for RX*/
    init_pcap();

    /*Send a register msg to the kernel*/
    if (send_register_msg()==0)
        printf("REGISTER SENT!\n");

    /*We wait for incoming msg*/
    while(running) {
        nl_recvmsgs_default(sock);
    }

free_mem:
    /*Free all memory*/
    free(sock);
    free(cb);
    free(cache);
    free(family);
    free(prob_matrix);

    return EXIT_SUCCESS;
}
