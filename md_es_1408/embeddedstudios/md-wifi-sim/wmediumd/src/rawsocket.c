
#include "mac_address.h"
#include "rawsocket.h"
#include <pcap.h> /* if this gives you an error try pcap/pcap.h */
#include <string.h>
#include <sys/socket.h>
#include <netinet/in.h>


/* Globals */

int sock_fd = 0;                 /* socket descriptor */
struct sockaddr_ll s_addr;      /*sock addr */

unsigned int total_recv_packets = 0;

char errbuf[PCAP_ERRBUF_SIZE];
pcap_t* pcap_descr;                  /* global pcap discriptor */
unsigned char *packet;
struct pcap_pkthdr hdr;         /* pcap.h */
struct ether_header *eptr;      /* net/ethernet.h */

unsigned char in_buff1[ VLAN_ETH_FRAME_LEN + ETH_FCS_LEN];
unsigned char in_buff[ VLAN_ETH_FRAME_LEN +  ETH_FCS_LEN];
unsigned char rcv_mac[ETH_ALEN] = {'\0'};
unsigned char snd_mac[ETH_ALEN] = {'\0'};

extern void dump_bytes(unsigned char *D,  int count);
extern wmd_pkt_t * create_wmd_packet(char *, int , int);
extern void * malloc_aligned(unsigned int size);

wmd_queue_t * wmd_q;


void pcap_rx_handler(u_char *useless,const struct pcap_pkthdr* hdr,const u_char*
        packet)
{

    int     data_len = 0; 
    int     rate_idx = 0;
    int     signal = 0;
    unsigned int chan_no = 0 ;
    int     i;
    static long count = 0;

    
    if( (memcmp(snd_mac,&packet[ETH_ALEN],ETH_ALEN) && 
                ((ntohs(*(unsigned short *)(packet + TUNNEL_PACKET_SIGNATURE_LOC))) == TUNNEL_PACKET_SIGNATURE)
                ))  {

        data_len = hdr->len - VLAN_ETH_HLEN;
        rate_idx = 3;
        signal = xyz_get_signal(&packet[ETH_ALEN]);
        if (signal == 0)
        {
            return;
        }

        chan_no = ntohs(*((unsigned short*)(packet + ETH_HLEN)));

        send_cloned_frame_msg(&packet[ETH_ALEN], &packet[VLAN_ETH_HLEN], data_len, rate_idx, signal, chan_no);
    }
}
void * pcap_thread_handler(void *ptr)
{
    while (1)
    {
        pcap_loop(pcap_descr,-1, pcap_rx_handler, NULL);
    }

}

int init_rawsocket()
{

    struct ifreq ifr; 
    int ifindex = 0;       /*ethernet interface index */
    int i=0;

    int prog;


    pthread_t tx_thread;

    /* open socket */
    sock_fd = socket(PF_PACKET, SOCK_RAW, htons(ETH_P_ALL));

    if (sock_fd == -1) {
        perror("socket():");
        exit(1);
    }

    setsockopt(sock_fd, SOL_SOCKET, SO_BINDTODEVICE, TUNNEL_INTF, 4);

    printf("Successfully opened socket: %i\n", sock_fd);

    /* Interface Index */
    strncpy(ifr.ifr_name, TUNNEL_INTF, IFNAMSIZ);
    if (ioctl(sock_fd, SIOCGIFINDEX, &ifr) == -1) {
        perror("SIOCGIFINDEX");
        exit(1);
    }
    ifindex = ifr.ifr_ifindex;
    printf("Successfully got interface index: %i\n", ifindex);


    memset(&ifr, 0, sizeof(ifr));
    strcpy(ifr.ifr_name, TUNNEL_INTF);
    if (ioctl(sock_fd, SIOCGIFINDEX, &ifr) < 0) {
        printf("EthRawSockStart(): ioctl() SIOCGIFINDEX failed! error: %d (NIC: %s)",errno,ifr.ifr_name);
        return ;
    }

    /* MAC address */
    if (ioctl(sock_fd, SIOCGIFHWADDR, &ifr) == -1) {
        perror("SIOCGIFINDEX");
        exit(1);
    }

    memcpy(snd_mac,ifr.ifr_hwaddr.sa_data,ETH_ALEN);
    memcpy(rcv_mac,ifr.ifr_hwaddr.sa_data,ETH_ALEN);

    rcv_mac[5] = rcv_mac[5] + 10;


    /**
     * prepare sockaddr_ll & Bind to ifindex 
     */
    s_addr.sll_family   = PF_PACKET;
    s_addr.sll_protocol = htons(ETH_P_ALL);
    s_addr.sll_ifindex  = ifindex;
    s_addr.sll_halen    = ETH_ALEN;
    
    memcpy(s_addr.sll_addr,rcv_mac,ETH_ALEN);
    s_addr.sll_addr[6]  = 0x00;
    s_addr.sll_addr[7]  = 0x00;

    if (bind(sock_fd, (struct sockaddr *) &s_addr, sizeof(s_addr)) == -1) {
        // error
        perror("binding:");
        exit(1);
    }

    /* set interface to promiscious mode */
    strncpy(ifr.ifr_name,TUNNEL_INTF,IFNAMSIZ);
    ioctl(sock_fd,SIOCGIFFLAGS,&ifr);
    ifr.ifr_flags |= IFF_PROMISC;
    ioctl(sock_fd,SIOCGIFFLAGS,&ifr);

    if(wmq_init() == FAILURE)
        return -1;
    pthread_create( &tx_thread, NULL, wmd_tx_thread, (void*) NULL);

    return 0;
}


int wmq_init()
{
    wmd_q = malloc_aligned(sizeof(wmd_queue_t));
    if (wmd_q != NULL)
    {
        wmd_q->divider.next = NULL;
        wmd_q->head = &wmd_q->divider;
        wmd_q->tail = &wmd_q->divider;
        if(pthread_mutex_init(&wmd_q->H_lock, NULL))
        {
            perror("mutex init failure");
            return FAILURE;
        }

        if(pthread_mutex_init(&wmd_q->T_lock, NULL))
        {
            perror("mutex init failure");
            return FAILURE;
        }
        return SUCCESS;
    }
    else
        return FAILURE;
}

wmd_pkt_t *wmd_tx_dequeue(void)
{

    wmd_pkt_t *packet, *new_head;

    while(1)
    {
        pthread_mutex_lock(&wmd_q->H_lock);
        packet = wmd_q->head;
        new_head = packet->next;

        if(new_head == NULL)
        {
            pthread_mutex_unlock(&wmd_q->H_lock); /* List Empty ;; return; */
            return NULL;
        }
        wmd_q->head = new_head;
        pthread_mutex_unlock(&wmd_q->H_lock);

        if (packet == &wmd_q->divider)
        {
            wmd_tx_enqueue(packet);
            continue;
        }
        packet->next = QUEUE_POISON1;

        return packet;
    }

}

int wmd_tx_enqueue(wmd_pkt_t *packet)
{
    wmd_pkt_t *tail;

    static int i = 1;

    if(packet == NULL)
        return;

#if 1
    if(i)
    {
        packet->next = QUEUE_POISON1;
        i = 0;
    }
    else
    {
        packet->next = NULL;
    }
#endif

    pthread_mutex_lock(&wmd_q->T_lock);
    wmd_q->tail->next = packet;
    wmd_q->tail = packet;
    pthread_mutex_unlock(&wmd_q->T_lock);
    return;
}

void * wmd_tx_thread(void * buf)
{
    wmd_pkt_t *packet;

    while(1)
    {
        packet = wmd_tx_dequeue();
        if(packet == NULL)
        {
            usleep(0.5 * 1000); /* workaround for sync */
            continue;
        }
        else
        {
            wmd_xmit(packet);

            free(packet->data);
            packet->data = NULL;
            free(packet);
            packet = NULL;
        }
    }
}

int init_pcap(void)
{
    pthread_t rx_thread;
    int i;

    u_char *ptr; /* printing out hardware header info */

    if(TUNNEL_INTF == NULL)
    {
        printf("%s\n",errbuf);
        exit(1);
    }

    pcap_descr = pcap_open_live(TUNNEL_INTF,BUFSIZ,1,0,errbuf);

    if(pcap_descr == NULL)
    {
        printf("pcap_open_live(): %s\n",errbuf);
        exit(1);
    }

    pthread_create(&rx_thread, NULL, pcap_thread_handler, NULL);
}

