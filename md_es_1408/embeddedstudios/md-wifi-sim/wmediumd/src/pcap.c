
#include "mac_address.h"
//#include "rawsocket.h"
#include <pcap.h> /* if this gives you an error try pcap/pcap.h */
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <errno.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netinet/if_ether.h> /* includes net/ethernet.h */



/* Globals */

char *dev = "eth2";
char errbuf[PCAP_ERRBUF_SIZE];
pcap_t* descr;
const u_char *packet;
struct pcap_pkthdr hdr;     /* pcap.h */
struct ether_header *eptr;  /* net/ethernet.h */


unsigned char in_buff1[ ETH_FRAME_LEN+ETH_FCS_LEN ];

extern void dump_bytes(unsigned char *D,  int count);


void * pcap_routine(void *ptr) 
{

    int recv;
//    struct ethhdr *in_hdr = (struct ethhdr *)in_buff1;
    while (1) {

        memset ( in_buff1, '\0', ETH_FRAME_LEN+ETH_FCS_LEN );
        packet = pcap_next(descr,&hdr);

        while(packet == NULL)
        {/* dinna work *sob* */
            packet = pcap_next(descr,&hdr);
            //printf("Didn't grab packet\n");
            //exit(1);
        }
        printf("PCAP Packet: \n");
        dump_bytes((unsigned char *)packet, hdr.len);
        printf("\n\n");
        //in_buff1 = (unsigned char*)packet;

#if 0

        if( (packet[0] == 0x52 && packet[1] == 0x54 ) ) {
            printf ( " < %s : %d >  \n", __func__,__LINE__ );
            dump_bytes(in_buff1,recv);
        }
        else{
            //printf ( " < %s : %d >  \n", __func__,__LINE__ );
            goto out;
        }
#endif
#if 0

        char *data = NULL;  /* = &in_buff1[18];*/
        data = (char*)malloc(recv - 14);
        memcpy(data,&in_buff1[14],recv - 14);

        int data_len = recv -14;
        int rate_idx = 11;
        int signal = 80;

        send_cloned_frame_msg(&in_buff1[6],data,data_len,rate_idx,signal);

        printf ( " RX on wmediumd of len %d \n", data_len);

        dump_bytes(data,data_len);
        free(data);

#endif 

    }
out:
    return NULL;
}

int create_pcap(void)
{
    pthread_t rx_thread;
     int i;

    u_char *ptr; /* printing out hardware header info */

    /* grab a device to peak into... */
    //dev = pcap_lookupdev(errbuf);

    if(dev == NULL)
    {
        printf("%s\n",errbuf);
        exit(1);
    }

    printf("DEV: %s\n",dev);

    descr = pcap_open_live(dev,BUFSIZ,1,-1,errbuf);

    if(descr == NULL)
    {
        printf("pcap_open_live(): %s\n",errbuf);
        exit(1);
    }

    pthread_create(&rx_thread, NULL, pcap_routine, NULL);
 

}
