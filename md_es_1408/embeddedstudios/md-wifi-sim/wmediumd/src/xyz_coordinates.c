
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <unistd.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>


#define SERV_IP "127.0.0.1"
#define BUF_LEN 512
#define PORT 4975

#define XYZ_HASH_LENGTH 32 
#define DEFAULT_STATION_SIGNAL -40

struct info
{
    char mac[6];
    int xyz[3];

    int signal;
    int distance;
};
typedef struct info xyz_info_t;

struct xyz_coordinate_entry
{
    struct info xyz_info;
    struct xyz_coordinate_entry *next;
};
typedef struct xyz_coordinate_entry xyz_coordinate_entry_t;

static unsigned int XYZ_COORDINATES_HASH_FUNCTION(char *addr)
{
    unsigned int hash;                      \
        hash = addr[0]     \
        + addr[1]   \
        + addr[2]   \
        + addr[3] *1  \
        + addr[4] *2 \
        + addr[5] *3; \
        return hash;                   \
}

extern unsigned char snd_mac[6];

/** Globals **/
xyz_info_t self_info;
struct xyz_coordinate_entry **xyz_hash ;
pthread_t xyz_thread;

/** RSSI approx : Excellent ( -40 to -50dB); Great ( -50 to -60dB); 
    Good ( -60 to -70dB); Fair ( -70 to -80dB); Poor ( -80 to -90dB); **/
int dist_signal [5][2] = { {250,-40}, {500, -50}, {750, -60}, {1000, -70}, {1250, -90} };

/** Prototypes **/
void xyz_show_coordinates ();
int  xyz_get_signal(char *mac);
void xyz_coordinate_handler( char *buf);
int xyz_add_coordinates( char *mac, int distance, int signal );
int xyz_update_coordinates( char *mac, int distance, int signal );
void * xyz_coordinates();

int init_wmediumD_link()
{

    int i;
    /** Hash init -S **/
    xyz_hash = (struct xyz_coordinate_entry**)calloc( XYZ_HASH_LENGTH, sizeof(struct xyz_coordinate_entry*));
    
    if (xyz_hash)
    {
        for ( i = 0; i < XYZ_HASH_LENGTH; i++)
        {
            xyz_hash[i] = NULL;
        }
    }
    
    /** Hash init -E **/

    pthread_create(&xyz_thread, NULL, xyz_coordinates,NULL);

    return 0;
}

void * xyz_coordinates()
{
    struct sockaddr_in si_me, si_other;
    int s, slen=sizeof(struct sockaddr_in), ret;
    char buf[BUF_LEN] = {'\0'};

    if( (s = socket(AF_INET, SOCK_DGRAM, 0)) == -1 )
    {
        perror(" socket creation failed \n" );
        exit (1);
    }

    memset( (char*)&si_me, 0, sizeof(si_me));

    si_me.sin_family        = AF_INET;
    si_me.sin_port          = htons(PORT);
    si_me.sin_addr.s_addr   = inet_addr(SERV_IP);

    if ( bind(s, (struct sockaddr*)&si_me, sizeof(si_me)) == -1 )
    {
        perror(" bind errro ");
        close(s);
        exit(1);
    }

    while ( 1 )
    {
        if ( (ret = recvfrom(s, buf, BUF_LEN, 0, (struct sockaddr*)&si_other, &slen)) == -1 )
        {
            perror(" error recvfrom \n");
            continue;
        }

/*
        char *cptr = (char *)malloc(ret+1);
        if (cptr != NULL)
        {
            memcpy(cptr,buf,ret);
            cptr[ret] = '\0';
        }*/

        printf(" Received packet from %s : %d \n, Data: %s \n\n ",
                inet_ntoa(si_other.sin_addr), ntohs(si_other.sin_port), buf);
        
        //xyz_coordinate_handler(cptr);
        xyz_coordinate_handler(buf);
        //free(cptr);
        //cptr = NULL;
        //sleep(1);
    }
}


void xyz_coordinate_handler( char *buf)
{

    char mac_addr[6] = {'\0'};
    char mac_addr1[6] = {'\0'};
    int xyz[3] = {'\0'};
    //char xyz[3] = {'\0'};
    //char *xyz = (char *)malloc( (sizeof(char)*3) );
    //char *mac_addr = (char *)malloc( (sizeof(char)*6) );
    char action = 0;

    char *token ;
    const char s[2] = " ";
    int distance = 0, signal = 0, dist_index = 0;

    int i=0, j=0;
    int ref = 0;



    printf(" Received COnfig is %s \n", buf);

    //memset(mac_addr, 0 , 6*sizeof(char) );
    //memset(xyz, 0 , 3*sizeof(char) );


    if ( (token = strtok(buf, s)) != NULL )
    {
        sscanf( token, "%hhx:%hhx:%hhx:%hhx:%hhx:%hhx",  &mac_addr[0], &mac_addr[1], &mac_addr[2],&mac_addr[3], &mac_addr[4], &mac_addr[5] );
        mac_addr[5] = mac_addr[5]+1;
        memcpy ( mac_addr1, mac_addr, 6*sizeof(char) );
        /* TBD - Identify Our own MAC to identify */
        if( !memcmp(snd_mac,mac_addr1, 6*sizeof(char)) )
        {
            ref = 1;
        }
    }

    if ( (token = strtok(NULL, s)) != NULL )
    {
        sscanf( token, "%d:%d:%d",  &xyz[0], &xyz[1], &xyz[2] );
        printf("xyz coordinates =%d : %d :%d\n",xyz[0],xyz[1],xyz[2]);
    }

    if ( (token = strtok(NULL, s)) != NULL )
    {
        action = atoi((char*)token) ;

    }

    if ( ref )
    {
        memcpy ( self_info.mac, mac_addr1, 6*sizeof(char) );
        memcpy ( self_info.xyz, xyz, 3*sizeof(int) );
        ref = 0;
    } 
    else
    {
        /** Calcuate the distance wrt to ref_info **/
        distance = sqrt( pow((self_info.xyz[0]- xyz[0]),2) + pow((self_info.xyz[1]- xyz[1]),2) + pow((self_info.xyz[2]- xyz[2]),2) );

        /** Calculate the dist_index based on the actual index **/
        if (distance < 0 || distance > 1250) // TBD: Handle cases of Packet Drop and 
                                                //      upper and threshold signals
        {
            printf ( " Drop the packet \n" );
            signal = 0; // Assigning signal 0 
        }
        else
        {
            if ( distance >=0  && distance <=250)
                dist_index = 0;
            else if ( distance >250 && distance <=500)
                dist_index = 1;
            else if ( distance >500 && distance <=750)
                dist_index = 2;
            else if (distance >750  && distance <=1000)
                dist_index = 3;
            else if (distance >1000  && distance <=1250)
                dist_index = 4;

        /** Fetch the signal based on the dist_index **/
            signal = dist_signal[dist_index][1];
        }

        printf("action =%d signal =%d distance =%d\n",action,signal,distance);

        if(action == 1)
        {   
            xyz_add_coordinates( mac_addr1, distance, signal);
        }
        else 
        {
            xyz_update_coordinates( mac_addr1, distance, signal);
        }
    }
        //free(mac_addr);
        //mac_addr = NULL;
}
int xyz_add_coordinates( char *mac, int distance, int signal )
{
    int index; 
    unsigned int hash_value;

    struct xyz_coordinate_entry *node = NULL;

    node = (struct xyz_coordinate_entry*)malloc(sizeof(struct xyz_coordinate_entry));
    if ( node )
    {
        node->xyz_info.signal    = signal;
        node->xyz_info.distance  = distance;
        memcpy(&node->xyz_info.mac, mac, 6);

        node->next = NULL;
    }

    hash_value = XYZ_COORDINATES_HASH_FUNCTION(mac);
    index = hash_value % XYZ_HASH_LENGTH;

    if ( xyz_hash[index] != NULL ) // add to head chaining
    {

        node->next          = xyz_hash[index];
        xyz_hash[index]     = node;
    }
    else
    {
        xyz_hash[index] = node;
    }

}

int xyz_update_coordinates( char *mac, int distance, int signal )
{
    int index;
    unsigned int hash_value;
    
    struct xyz_coordinate_entry *node = NULL;

    hash_value = XYZ_COORDINATES_HASH_FUNCTION(mac);
    index = hash_value % XYZ_HASH_LENGTH;

    if (xyz_hash == NULL)
    {
        return 0;
    }
    else
    {
        for ( node = xyz_hash[index]; node != NULL; node = node->next )
        {
            if ( !memcmp(&node->xyz_info.mac, mac, 6) )
            {
                node->xyz_info.signal = signal;
                node->xyz_info.distance  = distance;
                //break;
                return 0;
            }
            //continue;
        }
        return 0;
    }

}

int  xyz_get_signal(char *mac)
{
    int index;
    unsigned int hash_value;

    struct xyz_coordinate_entry *node = NULL;
    struct xyz_coordinate_entry *temp = NULL;


    hash_value = XYZ_COORDINATES_HASH_FUNCTION(mac);
    index = hash_value % XYZ_HASH_LENGTH;

    if (xyz_hash == NULL)
    {
        return 0;
    }
    else if (xyz_hash[index] == NULL){
        return DEFAULT_STATION_SIGNAL;
    }
    else
    {
        for ( temp = xyz_hash[index]; temp != NULL; temp = temp->next )
        {
            if ( !memcmp(&temp->xyz_info.mac, mac, 6) )
            {
                return temp->xyz_info.signal;
            }
            //continue;
        }
        return DEFAULT_STATION_SIGNAL;
    }
}

void xyz_show_coordinates ( int index )
{

    return 0;

    struct xyz_coordinate_entry *node = NULL;

    for ( node = xyz_hash[index]; node != NULL; node = node->next )
    {    
        printf ( " DISPLAY : Entery for dis %d , signal %d \n", node->xyz_info.distance, node->xyz_info.signal );
    }

}
