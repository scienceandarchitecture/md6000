#ifndef __RAWSOCKET_H__
#define __RAWSOCKET_H__

#include <pthread.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <sys/time.h>
#include <asm/types.h>
#include <math.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <errno.h>
#include <linux/if.h>
#include <linux/if_packet.h>
#include <linux/if_ether.h>
#include <linux/if_vlan.h>
#include <linux/if_arp.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <unistd.h>

#define TUNNEL_INTF "eth2"
#define TUNNEL_PACKET_SIGNATURE 0xAABB
#define TUNNEL_PACKET_SIGNATURE_1 0xAA
#define TUNNEL_PACKET_SIGNATURE_2 0xBB
#define TUNNEL_PACKET_SIGNATURE_SIZE 2
#define TUNNEL_PACKET_SIGNATURE_LOC 16
#define TUNNEL_PACKET_SIGNATURE_LOC_1 16
#define TUNNEL_PACKET_SIGNATURE_LOC_2 17
#define ETH_ALEN 6
#define VLAN_ETH_FRAME_LEN      1518
#define VLAN_ETH_HLEN 18
#define LIST_EMPTY 0
#define LIST_NOT_EMPTY 1
#define SUCCESS 0
#define FAILURE 1

#define QUEUE_POISON1 ((void*)0xCAFEBAB5)

struct vlan_ethhdr {
    unsigned char   h_dest[ETH_ALEN];
    unsigned char   h_source[ETH_ALEN];
    unsigned short  h_vlan_proto;
    unsigned short  h_vlan_TCI;
    unsigned short  h_vlan_encapsulated_proto;
};

struct wmd_pkt {
    char *data;
    int data_len;
    int chan_no;
    struct wmd_pkt *next;
};

typedef struct wmd_pkt wmd_pkt_t;

struct wmd_queue {
    wmd_pkt_t *head;
    wmd_pkt_t *tail;
    pthread_mutex_t H_lock;
    pthread_mutex_t T_lock;
    wmd_pkt_t divider;
};

typedef struct wmd_queue wmd_queue_t;

int init_rawsocket(void);
wmd_pkt_t* create_wmd_packet(char* data, int data_len, int chan_no);
void * wmd_xmit(void *buf1);
void * wmd_tx_thread(void * buf);
int wmd_tx_enqueue(wmd_pkt_t *packet);
//void * wmd_tx_enqueue(void * );
wmd_pkt_t *wmd_tx_dequeue(void);
int is_wmq_empty();
int is_wmq_empty();
int wmq_init();
void *malloc_aligned(unsigned int size);


#endif /* __RAWSOCKET_H__ */
