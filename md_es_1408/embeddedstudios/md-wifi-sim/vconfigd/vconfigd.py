#!/usr/bin/python

import filelock
from copy import copy
from subprocess import Popen,PIPE,STDOUT
import sys, threading, os, socket, json, time
import ConfigParser
import subprocess
import fcntl
import struct
import shutil
import logging

MAS_DEST_ADDR = ('192.168.122.1',4960)
VM_COORDINATES_PORT = 4970
MAX_BYTES = 16384
WMEDIUMD_DEST_PORT = ('localhost',4975)
WMEDIUMD_SEND_COORDINATES = 0
VM_MOBILE_COUNT = 0

SRC_CONFIG_DIR = "/md-configs/"
DEST_CONFIG_PATH = "/etc/meshap.conf"

MESHAP_MODULE = "/lib/modules/4.4.0/meshap.ko"
CONFIGD = "/sbin/configd"

lock = filelock.FileLock("vm_config.ini")
Config = ConfigParser.RawConfigParser()
Config.read("vm_config.ini")
Config.sections()

MODULE_BOOT_COMMAND_LIST = ["insmod lib/modules/4.4.0/cfg80211.ko","insmod lib/modules/4.4.0/board_info.ko","insmod lib/modules/4.4.0/torna_hw_id.ko","insmod lib/modules/4.4.0/meshap.ko","insmod lib/modules/4.4.0/mac80211.ko","mknod /dev/tddi c 251 0"]

START_AP_COMMAND_LIST = []
WMEDIUMD_AP_COMMAND_LIST = ["ifconfig eth2 up"]


def ConfigSectionMap(section):    #function to read data from local_database_status_file.ini
    dict1 = {}
    options = Config.options(section)
    for option in options:
        try:
            dict1[option] = Config.get(section, option)
            if dict1[option] == -1:
                DebugPrint("skip: %s" % option)
        except:
            print("exception on %s!" % option)
            dict1[option] = None
    return dict1

def command_output_buffer(cmd):
  	p=Popen(cmd, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
        time.sleep(1)


def get_ip_address(ifname):
        s = socket.socket(socket.AF_INET,socket.SOCK_DGRAM)
        ETH3_IP = socket.inet_ntoa( fcntl.ioctl(s.fileno(), 0x8915, struct.pack('256s', ifname[:15]))[20:24] )
	s.close()
	return ETH3_IP


MGMT_IP = get_ip_address("eth3")

def vm_board_mac(MGMT_MAC):
    count = '2'
    FINAL_BOARD_MAC = vm_mac_generation(MGMT_MAC,count)
    return FINAL_BOARD_MAC

def vm_rmacs_vlan_appmac(MGMT_MAC):
    count = '4'
    FINAL_MACS_LIST = list()
    for i in range(0,21):
        MACS_DATA = 0
        new_count = hex(int(count) + i)
        MACS_DATA = vm_mac_generation(MGMT_MAC,str(new_count))
        FINAL_MACS_LIST.append(MACS_DATA)

    return FINAL_MACS_LIST


def vm_mac_generation(MGMT_MAC, count):
        data = MGMT_MAC.split(":")
        BASE_MAC = data[0]+":"+data[1]+":"+data[2]
        data_prev = data[3]+data[4]+data[5]
        data_new1  = hex(int(data_prev,16)+int(count,16))
        data_len = len(data_new1)
        if data_len == 3:
                data_new = "00000"+data_new1[2:]
        elif data_len == 4:
                data_new = "0000"+data_new1[2:]
        elif data_len == 5:
                data_new = "000"+data_new1[2:]
        elif data_len == 6:
                data_new = "00"+data_new1[2:]
        elif data_len == 7:
                data_new = "0"+data_new1[2:]
        elif data_len == 8:
                data_new = data_new1[2:]

        FINAL_MAC = BASE_MAC+":"+data_new[0]+data_new[1]+":"+data_new[2]+data_new[3]+":"+data_new[4]+data_new[5]

        return FINAL_MAC


def vm_config_write(vm_data):
	lock.acquire()
	section_identifier = vm_data['mgmt_ip']
        cfgfile=open("vm_config.ini",'w')
        Config.add_section(section_identifier)
        Config.set(section_identifier,'vm_name',vm_data['vm_name'])
        Config.set(section_identifier,'mip_ip',vm_data['mip_ip'])
        Config.set(section_identifier,'mgmt_mac',vm_data['mgmt_mac'])
        Config.set(section_identifier,'model_number',vm_data['model_number'])
        Config.set(section_identifier,'vm_role',vm_data['vm_role'])
        if vm_data['vm_role'] == "MOBILE":
            Config.set(section_identifier,'vm_mobile_speed',vm_data['vm_mobile_speed'])
            Config.set(section_identifier,'vm_mobile_distance',vm_data['vm_mobile_distance'])

        Config.set(section_identifier,'vm_coordinates',vm_data['vm_coordinates'])
        Config.write(cfgfile)
        cfgfile.close
	lock.release()

        return True


def vm_coordinates_write(str_vm_coordinates):
        NUM_OF_DATA = len(str_vm_coordinates)
        for i in range(1,NUM_OF_DATA):
                DATA = "data_"+str(i)
	        section_identifier = str_vm_coordinates[DATA]['mgmt_ip']
		section_available_list = Config.sections()
		if section_identifier not in section_available_list:
			lock.acquire()
                	cfgfile=open("vm_config.ini",'w')
                	Config.add_section(section_identifier)
                	Config.set(section_identifier,'vm_coordinates',str_vm_coordinates[DATA]['vm_coordinates'])
                	Config.set(section_identifier,'mgmt_mac',str_vm_coordinates[DATA]['mgmt_mac'])
                	Config.write(cfgfile)
                	cfgfile.close
			lock.release()
                        if WMEDIUMD_SEND_COORDINATES == 1:
                                vm_coordinates_updateto_wmediumd(str_vm_coordinates[DATA]['vm_coordinates'],str_vm_coordinates[DATA]['mgmt_mac'],str_vm_coordinates['action_tag'])

def vm_coordinates_update(str_vm_coordinates):
        section_identifier = str_vm_coordinates['data_1']['mgmt_ip']
        section_available_list = Config.sections()
        if section_identifier in section_available_list:
                lock.acquire()
                cfgfile=open("vm_config.ini",'w')
                Config.set(section_identifier,'vm_coordinates',str_vm_coordinates['data_1']['vm_coordinates'])
                Config.write(cfgfile)
                cfgfile.close
                lock.release()
                vm_coordinates_updateto_wmediumd(str_vm_coordinates['data_1']['vm_coordinates'],str_vm_coordinates['data_1']['mgmt_mac'],                               str_vm_coordinates['action_tag'])


def vm_coordinates_delete(str_vm_coordinates):
        section_identifier = str_vm_coordinates['data_1']['mgmt_ip']
	section_available_list = Config.sections()
	if section_identifier in section_available_list:
		lock.acquire()
        	Config.remove_section(section_identifier)
        	cfgfile=open("vm_config.ini",'w')
        	Config.write(cfgfile)
        	cfgfile.close
		lock.release()
                vm_coordinates_updateto_wmediumd(str_vm_coordinates['data_1']['vm_coordinates'],str_vm_coordinates['data_1']['mgmt_mac'],str_vm_coordinates['action_tag'])


def vm_get_config_server():
        vm_config_data = {}
        vm_config_data['mgmt_ip'] = MGMT_IP
        vm_config_data['status'] = 'enable'
        vm_config_data['CONFIG'] = 'YES'
        try:
                vm_config_status_data = json.dumps(vm_config_data)
        except ValueError:
                return False
        try:
                vm_config_status_sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
                vm_config_status_sock.bind((MGMT_IP, 4980))
        except socket.error:
                return False

        vm_config_status_sock.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)

        try:
                vm_config_status_sock.sendto(vm_config_status_data, MAS_DEST_ADDR)

	        str_vm_config_data,server_address  =  vm_config_status_sock.recvfrom(MAX_BYTES)
        except socket.error:
                return False
        try:
                received_vm_config_data = json.loads(str_vm_config_data)
        except ValueError:
                return False

        vm_config_write(received_vm_config_data)

        vm_config_status_sock.close()

        return True


def vm_send_status_server():
	vm_status_data = {}
        vm_status_data['mgmt_ip'] = MGMT_IP
        vm_status_data['status'] = 'enable'
        vm_status_data['CONFIG'] = 'NO'
        try:
                vm_status_update = json.dumps(vm_status_data)
        except ValueError:
                return False
        try:
                vm_status_sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
                vm_status_sock.bind((MGMT_IP, 4980))
        except socket.error:
                return False
        vm_status_sock.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
        try:
	        while True:
        	        vm_status_sock.sendto(vm_status_update,MAS_DEST_ADDR)
		        time.sleep(10)
        except socket.error:
                return False

        vm_status_sock.close()

        return True

def vm_coordinates_recv_server():
        try:
                vm_coordinates_sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
                vm_coordinates_sock.bind((MGMT_IP,VM_COORDINATES_PORT))
        except socket.error:
                print "<Vconfigd>: Failed To Open Socket for vm_coordinates"
        while True:

                str_vm_coordinates,vm_address  =  vm_coordinates_sock.recvfrom(MAX_BYTES)
                str_vm_coordinates = json.loads(str_vm_coordinates)
                str_vm_coordinates_copy = copy(str_vm_coordinates)
                if str_vm_coordinates_copy['action_tag'] == "CREATE":
                        vm_coordinates_write(str_vm_coordinates_copy)

                elif str_vm_coordinates_copy['action_tag'] =="DELETE" :
                        vm_coordinates_delete(str_vm_coordinates_copy)

                elif str_vm_coordinates_copy['action_tag'] =="UPDATE" :
                        vm_coordinates_update(str_vm_coordinates_copy)


def vm_coordinates_sendto_wmediumd():
        vmtowmedium_coordinates_sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        mgmt_mac_data = ConfigSectionMap(MGMT_IP)['mgmt_mac']
        vm_coordinates_data = ConfigSectionMap(MGMT_IP)['vm_coordinates']
        vm_coordinates_action_data = "1"
        FINAL_WMEDIUMD_DATA = mgmt_mac_data+" " +vm_coordinates_data+" " +vm_coordinates_action_data
        vmtowmedium_coordinates_sock.sendto(FINAL_WMEDIUMD_DATA,WMEDIUMD_DEST_PORT)

        section_available_list = Config.sections()
        for wmediumd_section_identifier in section_available_list:
                if wmediumd_section_identifier != MGMT_IP:
                        mgmt_mac_data = ConfigSectionMap(wmediumd_section_identifier)['mgmt_mac']
                        vm_coordinates_data = ConfigSectionMap(wmediumd_section_identifier)['vm_coordinates']
                        vm_coordinates_action_data = "1"
                        FINAL_WMEDIUMD_DATA = mgmt_mac_data+" " +vm_coordinates_data+" " +vm_coordinates_action_data
                        vmtowmedium_coordinates_sock.sendto(FINAL_WMEDIUMD_DATA,WMEDIUMD_DEST_PORT)
        vmtowmedium_coordinates_sock.close()
        return True

def vm_coordinates_updateto_wmediumd(vm_coordinates_data,mgmt_mac_data,vm_coordinates_action_data):
        if vm_coordinates_action_data == "CREATE":
                temp_action = "1"
        elif vm_coordinates_action_data == "DELETE":
                temp_action = "0"
        elif vm_coordinates_action_data == "UPDATE":
                temp_action = "2"

        FINAL_WMEDIUMD_DATA = mgmt_mac_data+" " +vm_coordinates_data+" " +temp_action
        vmtowmedium_update_coordinates_sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        vmtowmedium_update_coordinates_sock.sendto(FINAL_WMEDIUMD_DATA,WMEDIUMD_DEST_PORT)
        vmtowmedium_update_coordinates_sock.close()


def vm_onetime_boot_configuration():
	MODEL_NUMBER = ConfigSectionMap(MGMT_IP)['model_number']
        MIP_IP = ConfigSectionMap(MGMT_IP)['mip_ip']
        MIP_DATA = MIP_IP.split(".")
        str_mip_ip ="uci set network.mip.ipaddr="+MIP_IP
        str_mip_gateway ="uci set network.mip.gateway="+MIP_DATA[0]+"."+MIP_DATA[1]+"."+MIP_DATA[2]+".1"
        str_mip_broadcast ="uci set network.mip.broadcast="+MIP_DATA[0]+"."+MIP_DATA[1]+"."+MIP_DATA[2]+".255"+" && uci commit"
        START_AP_COMMAND_LIST.insert(0,str_mip_ip)
        START_AP_COMMAND_LIST.insert(1,str_mip_gateway)
        START_AP_COMMAND_LIST.insert(2,str_mip_broadcast)
	str_boot ="cd lib && ln -s libalconf.so libalconf.so.1 && cd .."
      	command_output_buffer(str_boot)
        CONFIG_FILE_NAME = MODEL_NUMBER+".conf"
        SRC_CONFIG_PATH = SRC_CONFIG_DIR+CONFIG_FILE_NAME
        try:
                shutil.copy(SRC_CONFIG_PATH,DEST_CONFIG_PATH)
        except:
                return False
#START# this section is used to create mac address for patcher

	MGMT_MAC    = ConfigSectionMap(MGMT_IP)['mgmt_mac']
        BOARD_MAC   = vm_board_mac(MGMT_MAC)
        MAC_DATA    = vm_rmacs_vlan_appmac(MGMT_MAC)

        str_patcher_host = "patcher_host -m "+MESHAP_MODULE+" -d "+CONFIGD+" -c "+DEST_CONFIG_PATH+" -b "+BOARD_MAC+" -e "+MAC_DATA[0]+" -r \""+MAC_DATA[1]+" "+MAC_DATA[2]+" "+MAC_DATA[3]+" "+MAC_DATA[4]+" "+MAC_DATA[5]+" "+MAC_DATA[6]+" "+MAC_DATA[7]+"\" -j \""+MAC_DATA[8]+" "+MAC_DATA[9]+" "+MAC_DATA[10]+" "+MAC_DATA[11]+" "+MAC_DATA[12]+"\" -a \""+MAC_DATA[13]+" "+MAC_DATA[14]+" "+MAC_DATA[15]+" "+MAC_DATA[16]+"\""


	str_patcher   = "patcher -k cybernetics! "+BOARD_MAC
      	command_output_buffer(str_patcher_host)
        time.sleep(20)
      	command_output_buffer(str_patcher)
        time.sleep(10)

        return True

#END#

def vm_boot_configuration():
	MODEL_NUMBER = ConfigSectionMap(MGMT_IP)['model_number']

#START# This section add hw_sim module with required number of radios
	if MODEL_NUMBER[3] == '1':
		str_cmd ="insmod lib/modules/4.4.0/mac80211_hwsim.ko radios=1"
		str_cmd1 ="cp wmediumd-configs/wmediumd_1.conf etc/wmediumd_1.conf"
                str_cmd2 ="wmediumd -c etc/wmediumd_1.conf > /dev/null &"
	elif MODEL_NUMBER[3] == '2':
		str_cmd ="insmod lib/modules/4.4.0/mac80211_hwsim.ko radios=2"
		str_cmd1 ="cp wmediumd-configs/wmediumd_2.conf etc/wmediumd_2.conf"
                str_cmd2 ="wmediumd -c etc/wmediumd_2.conf > /dev/null &"
	elif MODEL_NUMBER[3] == '3':
		str_cmd ="insmod lib/modules/4.4.0/mac80211_hwsim.ko radios=3"
		str_cmd1 ="cp wmediumd-configs/wmediumd_3.conf etc/wmediumd_3.conf"
                str_cmd2 ="wmediumd -c etc/wmediumd_3.conf > /dev/null &"
	elif MODEL_NUMBER[3] == '4':
		str_cmd ="insmod lib/modules/4.4.0/mac80211_hwsim.ko radios=4"
		str_cmd1 ="cp wmediumd-configs/wmediumd_4.conf etc/wmediumd_4.conf"
                str_cmd2 ="wmediumd -c etc/wmediumd_4.conf > /dev/null &"
	elif MODEL_NUMBER[3] == '5':
		str_cmd ="insmod lib/modules/4.4.0/mac80211_hwsim.ko radios=5"
		str_cmd1 ="cp wmediumd-configs/wmediumd_5.conf etc/wmediumd_5.conf"
                str_cmd2 ="wmediumd -c etc/wmediumd_5.conf > /dev/null &"
	elif MODEL_NUMBER[3] == '6':
		str_cmd ="insmod lib/modules/4.4.0/mac80211_hwsim.ko radios=6"
		str_cmd1 ="cp wmediumd-configs/wmediumd_6.conf etc/wmediumd_6.conf"
                str_cmd2 ="wmediumd -c etc/wmediumd_6.conf > /dev/null &"

        MODULE_BOOT_COMMAND_LIST.insert(5,str_cmd)
	WMEDIUMD_AP_COMMAND_LIST.insert(1,str_cmd1)
	WMEDIUMD_AP_COMMAND_LIST.insert(2,str_cmd2)
#END#
#START# This section loads required module

	for str_module in MODULE_BOOT_COMMAND_LIST:
		command_output_buffer(str_module)

	for str_wmediumd in WMEDIUMD_AP_COMMAND_LIST:
                subprocess.call(str_wmediumd,shell=True)

        return True
#END#


def vm_start_ap_configuration():
	VM_ROLE = ConfigSectionMap(MGMT_IP)['vm_role']
	VM_ROLE = VM_ROLE.upper()

	if VM_ROLE == "ROOT":
		str_cmd = "ifconfig eth0 up"
		START_AP_COMMAND_LIST.insert(0,str_cmd)
        elif VM_ROLE == "MOBILE":
                global VM_MOBILE_COUNT
                VM_MOBILE_COUNT = 1


#START# This section loads required start_ap command for getting mesh up
	for str_ap in START_AP_COMMAND_LIST:
		command_output_buffer(str_ap)
        try:
                subprocess.call("/root/startap.sh",shell=True)
        except:
                return False

#END#
        return True

def vm_mobile_node_operation():
        os.system("python mobile_node.py & ")



if __name__ == "__main__":
        print "*****************************************"
        print "                Vconfigd                 "
        print "*****************************************"
        try:
                VM_COORDINATES = threading.Thread(target=vm_coordinates_recv_server)
                VM_COORDINATES.start()
                time.sleep(2)
        except:
                print "<Vconfigd>: Failed To start vm_coordinates_recv_server"
        section_list = Config.sections()#if no file exists,then instance will contain empty dataset.
	if MGMT_IP not in section_list:
		get_config_ret = vm_get_config_server()
                if get_config_ret == False:
                        print "<Vconfigd>: Failed To get Config From MAS"
                        sys.exit()
		onetime_boot_ret = vm_onetime_boot_configuration()
                if onetime_boot_ret == False:
                        print "<Vconfigd>: Failed To Applying onetime_boot_configuration"
                        sys.exit()
        vm_boot_ret = vm_boot_configuration()
        if vm_boot_ret == False:
        	print "<Vconfigd>: Failed To Applying boot_configuration"
            	sys.exit()
	vm_start_ret= vm_start_ap_configuration()
        if vm_start_ret == False:
                print "<Vconfigd>: Failed To Applying start_ap_configuration"
                sys.exit()
        global WMEDIUMD_SEND_COORDINATES
        WMEDIUMD_SEND_COORDINATES = 1
        vm_coordinates_ret = vm_coordinates_sendto_wmediumd()
        if vm_coordinates_ret == False:
                print "<Vconfigd>: Failed To send coordinates to wmediumd"
                sys.exit()
        if VM_MOBILE_COUNT == 1:
                try:
                        VM_MOBILE = threading.Thread(target=vm_mobile_node_operation)
                        VM_MOBILE.start()
                except:
                        print "<Vconfigd>: Failed To start vm_mobile_node_operation"

	vm_status_ret = vm_send_status_server()
        if vm_status_ret == False:
                print "<Vconfigd>: Failed To Send Status to MAS"
                sys.exit()


