/********************************************************************************
* MeshDynamics
* --------------
* File     : impl.h
* Comments : Miscd implementation header for linux
* Created  : 5/14/2005
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  0  |5/14/2005 | Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

#ifndef __IMPL_H__
#define __IMPL_H__

#include <unistd.h>
#include <pthread.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/ioctl.h>

#define CLOSE_SOCKET(fd)    close(fd)
#define CONF_FILE_PATH    "/etc/meshap.conf"

void impl_init();
void impl_uninit();

#endif /*__IMPL_H__*/
