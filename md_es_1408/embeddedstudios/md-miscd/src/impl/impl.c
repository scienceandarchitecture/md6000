/********************************************************************************
* MeshDynamics
* --------------
* File     : impl.c
* Comments : Misc implementation for Linux
* Created  : 5/14/2005
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  1  |8/10/2007 | Changes for libalconf                           | Sriram |
* -----------------------------------------------------------------------------
* |  0  |5/14/2005 | Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

#include <stdlib.h>
#include "impl.h"
#include <stdarg.h>
#include "al.h"
#include "al_context.h"
#include "al_impl_context.h"

struct _miscd_al_thread
{
   al_thread_function_t function;
   al_thread_param_t    actual_param;
};

typedef struct _miscd_al_thread   _miscd_al_thread_t;

void impl_init()
{
}


void impl_uninit()
{
}


static void *_posix_thread(void *data)
{
   _miscd_al_thread_t *param;

   param = (_miscd_al_thread_t *)data;

   param->function(&param->actual_param);

   free(param);

   pthread_detach(pthread_self());
   pthread_exit(NULL);

   return NULL;
}


int al_create_thread(AL_CONTEXT_PARAM_DECL al_thread_function_t thread_function, void *param, int priority, char *name)
{
   pthread_t          thread;
   _miscd_al_thread_t *t;

   t                     = (_miscd_al_thread_t *)malloc(sizeof(_miscd_al_thread_t));
   t->function           = thread_function;
   t->actual_param.param = param;

   pthread_create(&thread, NULL, _posix_thread, (void *)t);

   return 0;
}
