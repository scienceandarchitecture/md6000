/********************************************************************************
* MeshDynamics
* --------------
* File     : miscd_packet.h
* Comments : Miscllaneous Daemon Packet Format
* Created  : 5/14/2005
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  0  |5/14/2005 | Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

#ifndef __MISCD_PACKET_H__
#define __MISCD_PACKET_H__

#ifdef __cplusplus
extern "C" {
#endif

#define MISCD_REQUEST_SET_IF_PHY_MODE          1
#define MISCD_REQUEST_SET_CONF_PHY_MODE        2
#define MISCD_REQUEST_EXECUTE_SHELL_COMMAND    3

#define MISCD_RESPONSE_OK                      0
#define MISCD_RESPONSE_ERR                     1

struct miscd_request_packet
{
   unsigned char request;
   unsigned int  request_length;
   unsigned char *request_data;
};

typedef struct miscd_request_packet   miscd_request_packet_t;

struct misc_response_packet
{
   unsigned char response;
   unsigned int  response_length;
   unsigned char *response_data;
};

typedef struct misc_response_packet   misc_response_packet_t;

miscd_request_packet_t *miscd_create_set_if_phy_mode_request(const char    *if_name,
                                                             unsigned char phy_mode);

int misc_process_set_if_phy_mode_request(miscd_request_packet_t *request,
                                         char                   *if_name,
                                         unsigned char          *phy_mode);


miscd_request_packet_t *miscd_create_set_conf_phy_mode_request(const char    *if_name,
                                                               unsigned char phy_mode);

int misc_process_set_conf_phy_mode_request(miscd_request_packet_t *request,
                                           char                   *if_name,
                                           unsigned char          *phy_mode);

miscd_request_packet_t *miscd_create_execute_shell_command_request(const char *command);

int miscd_process_execute_shell_command_request(miscd_request_packet_t *request,
                                                char                   *command);

/**
 * Only for packets created
 */

void miscd_destroy_request_packet(miscd_request_packet_t *request);

#ifdef __cplusplus
}
#endif

#endif /*__MISCD_PACKET_H__*/
