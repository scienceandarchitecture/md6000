/********************************************************************************
* MeshDynamics
* --------------
* File     : miscd_packet.c
* Comments : Miscllaneous daemon packet processing
* Created  : 5/14/2005
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  0  |5/14/2005 | Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

#include <stdlib.h>
#include <string.h>

#include "impl.h"
#include "miscd_packet.h"

void miscd_destroy_request_packet(miscd_request_packet_t *request)
{
   if (request->request_data != NULL)
   {
      free(request->request_data);
   }
   free(request);
}


miscd_request_packet_t *miscd_create_set_if_phy_mode_request(const char    *if_name,
                                                             unsigned char phy_mode)
{
   miscd_request_packet_t *request;
   unsigned char          if_name_length;

   request = (miscd_request_packet_t *)malloc(sizeof(miscd_request_packet_t));

   memset(request, 0, sizeof(miscd_request_packet_t));

   request->request        = MISCD_REQUEST_SET_IF_PHY_MODE;
   if_name_length          = strlen(if_name);
   request->request_length = 1 + if_name_length + 1;
   request->request_data   = (unsigned char *)malloc(request->request_length);

   memcpy(request->request_data, &if_name_length, 1);
   memcpy(request->request_data + 1, if_name, if_name_length);
   memcpy(request->request_data + 1 + if_name_length, &phy_mode, 1);

   return request;
}


int misc_process_set_if_phy_mode_request(miscd_request_packet_t *request,
                                         char                   *if_name,
                                         unsigned char          *phy_mode)
{
   unsigned char if_name_length;

   if (request->request != MISCD_REQUEST_SET_IF_PHY_MODE)
   {
      return -1;
   }
   if (request->request_length <= 0)
   {
      return -1;
   }
   if (request->request_data[0] != request->request_length - 2)
   {
      return 0;
   }

   if_name_length = request->request_data[0];

   memset(if_name, 0, if_name_length + 1);
   memcpy(if_name, &request->request_data[1], if_name_length);
   *phy_mode = request->request_data[1 + if_name_length];

   return 0;
}


miscd_request_packet_t *miscd_create_set_conf_phy_mode_request(const char    *if_name,
                                                               unsigned char phy_mode)
{
   miscd_request_packet_t *request;
   unsigned char          if_name_length;

   request = (miscd_request_packet_t *)malloc(sizeof(miscd_request_packet_t));

   memset(request, 0, sizeof(miscd_request_packet_t));

   request->request        = MISCD_REQUEST_SET_CONF_PHY_MODE;
   if_name_length          = strlen(if_name);
   request->request_length = 1 + if_name_length + 1;
   request->request_data   = (unsigned char *)malloc(request->request_length);

   memcpy(request->request_data, &if_name_length, 1);
   memcpy(request->request_data + 1, if_name, if_name_length);
   memcpy(request->request_data + 1 + if_name_length, &phy_mode, 1);

   return request;
}


int misc_process_set_conf_phy_mode_request(miscd_request_packet_t *request,
                                           char                   *if_name,
                                           unsigned char          *phy_mode)
{
   unsigned char if_name_length;

   if (request->request != MISCD_REQUEST_SET_CONF_PHY_MODE)
   {
      return -1;
   }
   if (request->request_length <= 0)
   {
      return -1;
   }
   if (request->request_data[0] != request->request_length - 2)
   {
      return 0;
   }

   if_name_length = request->request_data[0];

   memset(if_name, 0, if_name_length + 1);
   memcpy(if_name, &request->request_data[1], if_name_length);
   *phy_mode = request->request_data[1 + if_name_length];

   return 0;
}


miscd_request_packet_t *miscd_create_execute_shell_command_request(const char *command)
{
   miscd_request_packet_t *request;
   unsigned int           command_length;
   unsigned int           cl;

   request = (miscd_request_packet_t *)malloc(sizeof(miscd_request_packet_t));

   memset(request, 0, sizeof(miscd_request_packet_t));

   request->request        = MISCD_REQUEST_EXECUTE_SHELL_COMMAND;
   command_length          = strlen(command);
   request->request_length = 4 + command_length;
   request->request_data   = (unsigned char *)malloc(request->request_length);
   cl = MISCD_HTONL(command_length);

   memcpy(request->request_data, &cl, 4);
   memcpy(request->request_data + 4, command, command_length);

   return request;
}


int miscd_process_execute_shell_command_request(miscd_request_packet_t *request,
                                                char                   *command)
{
   unsigned int command_length;
   unsigned int cl;

   if (request->request != MISCD_REQUEST_EXECUTE_SHELL_COMMAND)
   {
      return -1;
   }
   if (request->request_length <= 0)
   {
      return -2;
   }

   memcpy(&cl, request->request_data, 4);

   command_length = MISCD_NTOHL(cl);

   memset(command, 0, command_length + 1);
   memcpy(command, request->request_data + 4, command_length);

   return command_length;
}
