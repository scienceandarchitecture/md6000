/********************************************************************************
* MeshDynamics
* --------------
* File     : miscd.c
* Comments : Miscllaneous tasks daemon
* Created  : 5/14/2005
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  2  |5/14/2008 | Misc crash fix                                  |Prachiti|
* -----------------------------------------------------------------------------
* |  1  |8/10/2007 | Changes for libalconf                           | Sriram |
* -----------------------------------------------------------------------------
* |  0  |5/14/2005 | Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

#include <sys/types.h>
#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <errno.h>

#include "impl.h"
#include "miscd.h"
#include "miscd_packet.h"

#include "al.h"
#include "al_context.h"
#include "al_impl_context.h"
#include "al_conf.h"
#include "al_conf_lib.h"

#define _CRITICAL_ERR    -2

struct _client_thread_data
{
   int                client_fd;
   struct sockaddr_in client_addr;
   int                err_code;
};

typedef struct _client_thread_data   _client_thread_data_t;

static int _client_thread(AL_CONTEXT_PARAM_DECL al_thread_param_t *param);


static int _recv_minimum(int           fd,
                         unsigned char *buffer,
                         int           length);

static int _process_request(_client_thread_data_t  *data,
                            miscd_request_packet_t *request);

static void _process_set_if_phy_mode(_client_thread_data_t  *data,
                                     miscd_request_packet_t *request);

static void _process_set_conf_phy_mode(_client_thread_data_t  *data,
                                       miscd_request_packet_t *request);

static int _process_execute_command(_client_thread_data_t  *data,
                                    miscd_request_packet_t *request);

static void _send_response(_client_thread_data_t  *data,
                           misc_response_packet_t *reponse);

static int _send_all(int s, unsigned char *data, int length);


int sock_fd;

int main(int argc, char **argv)
{
   int                   client_fd;
   struct sockaddr_in    client_addr;
   struct sockaddr_in    serv_addr;
   int                   clilen;
   _client_thread_data_t *data;
   int                   on;
   int                   ret;

   printf("miscd: Starting...\n");

   impl_init();

   sock_fd = socket(AF_INET, SOCK_STREAM, 0);
   on      = 1;

   memset(&serv_addr, 0, sizeof(serv_addr));

   serv_addr.sin_family      = AF_INET;
   serv_addr.sin_addr.s_addr = htonl(INADDR_ANY);
   serv_addr.sin_port        = htons(MISCD_TCP_PORT);

   ret = setsockopt(sock_fd, SOL_SOCKET, SO_REUSEADDR, (char *)&on, sizeof(on));
   if (ret < 0)
   {
      printf("setsockopt() failed\n");
      CLOSE_SOCKET(sock_fd);
      exit(-1);
   }

   if (bind(sock_fd, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0)
   {
      printf("miscd: Could not bind to port 0x%x\n", MISCD_TCP_PORT);
      CLOSE_SOCKET(sock_fd);
      exit(-2);
   }

   printf("miscd: Bound to TCP port 0x%x\n", MISCD_TCP_PORT);

   fflush(stdout);

   listen(sock_fd, 5);

   while (1)
   {
      clilen    = sizeof(client_addr);
      client_fd = accept(sock_fd, (struct sockaddr *)&client_addr, &clilen);
      data      = (_client_thread_data_t *)malloc(sizeof(_client_thread_data_t));

      memset(data, 0, sizeof(_client_thread_data_t));

      data->client_fd = client_fd;
      memcpy(&data->client_addr, &client_addr, sizeof(client_addr));

      al_create_thread(AL_CONTEXT _client_thread, (void *)data, 0, "_client_thread");
   }

   impl_uninit();

   return 0;
}


static int _client_thread(AL_CONTEXT_PARAM_DECL al_thread_param_t *param)
{
   _client_thread_data_t  *ctd;
   miscd_request_packet_t req_packet;
   unsigned int           req_length_n;
   int  len;
   int  err, ret;
   char request_data[8192];

   err = 0;

   ctd = (_client_thread_data_t *)param->param;

   len = _recv_minimum(ctd->client_fd, (unsigned char *)&req_packet.request, 1);
   if (len != 1)
   {
      err = -1;
      goto _quit;
   }

   len = _recv_minimum(ctd->client_fd, (unsigned char *)&req_length_n, 4);

   if (len != 4)
   {
      err = -2;
      goto _quit;
   }

   req_packet.request_length = ntohl(req_length_n);
   req_packet.request_data   = request_data;

   len = _recv_minimum(ctd->client_fd, req_packet.request_data, req_packet.request_length);

   if ((unsigned int)len != req_packet.request_length)
   {
      err = -3;
      goto _quit;
   }

   ret = _process_request(ctd, &req_packet);


_quit:
   CLOSE_SOCKET(ctd->client_fd);
   free(param->param);
   return err;
}


static int _recv_minimum(int fd, unsigned char *buffer, int length)
{
   unsigned char *p;
   int           len;
   int           left;

   left = length;
   p    = buffer;

   while (left > 0)
   {
      len = recv(fd, p, left, 0);
      if (len < 0)
      {
         return -1;
      }
      p    += len;
      left -= len;
   }

   return length;
}


static int _send_all(int s, unsigned char *data, int length)
{
   int           l;
   unsigned char *p;
   int           sent;

   p = data;
   l = length;

   while (l > 0)
   {
      sent = send(s, (const char *)p, l, 0);
      if (sent < 0)
      {
         return -1;
      }
      l -= sent;
      p += sent;
   }

   return length;
}


static int _process_request(_client_thread_data_t *ctd, miscd_request_packet_t *request)
{
   int ret;

   ret = 0;

   switch (request->request)
   {
   case MISCD_REQUEST_SET_IF_PHY_MODE:
      _process_set_if_phy_mode(ctd, request);
      break;

   case MISCD_REQUEST_SET_CONF_PHY_MODE:
      _process_set_conf_phy_mode(ctd, request);
      break;

   case MISCD_REQUEST_EXECUTE_SHELL_COMMAND:
      ret = _process_execute_command(ctd, request);
      break;
   }

   return ret;
}


static void _process_set_if_phy_mode(_client_thread_data_t *ctd, miscd_request_packet_t *request)
{
   char                   if_name[64];
   unsigned char          phy_mode;
   int                    ret;
   char                   command[128];
   misc_response_packet_t response;

   ret = misc_process_set_if_phy_mode_request(request, if_name, &phy_mode);

   if (ret)
   {
      printf("miscd: Error %d processing SET_IF_PHY_MODE from %s\n", ret, inet_ntoa(ctd->client_addr.sin_addr));
      return;
   }

   printf("miscd: Processing SET_IF_PHY_MODE from %s (%s,%d)\n", inet_ntoa(ctd->client_addr.sin_addr), if_name, phy_mode);

   sprintf(command, "iwconfig %s sens %d", if_name, phy_mode);
   system(command);

   response.response        = MISCD_RESPONSE_OK;
   response.response_length = 0;
   response.response_data   = NULL;

   _send_response(ctd, &response);
}


static void _process_set_conf_phy_mode(_client_thread_data_t *ctd, miscd_request_packet_t *request)
{
   char                   if_name[64];
   unsigned char          phy_mode;
   int                    ret;
   int                    al_conf_handle;
   int                    i;
   int                    if_count;
   al_conf_if_info_t      if_info;
   misc_response_packet_t response;

   ret = misc_process_set_conf_phy_mode_request(request, if_name, &phy_mode);

   if (ret)
   {
      printf("miscd: Error %d processing SET_CONF_PHY_MODE from %s\n", ret, inet_ntoa(ctd->client_addr.sin_addr));
      return;
   }

   printf("miscd: Processing SET_CONF_PHY_MODE from %s (%s,%d)\n", inet_ntoa(ctd->client_addr.sin_addr), if_name, phy_mode);

   al_conf_handle = libalconf_read_config_data();

   if_count = al_conf_get_if_count(AL_CONTEXT al_conf_handle);

   for (i = 0; i < if_count; i++)
   {
      al_conf_get_if(AL_CONTEXT al_conf_handle, i, &if_info);
      if (!strcmp(if_info.name, if_name))
      {
         if_info.phy_sub_type = phy_mode;
         al_conf_set_if(AL_CONTEXT al_conf_handle, i, &if_info);
         break;
      }
   }

   al_conf_put(AL_CONTEXT al_conf_handle);

   al_conf_close(AL_CONTEXT al_conf_handle);

   response.response        = MISCD_RESPONSE_OK;
   response.response_length = 0;
   response.response_data   = NULL;

   _send_response(ctd, &response);
}


static int _process_execute_command(_client_thread_data_t *ctd, miscd_request_packet_t *request)
{
   int  ret;
   char command[4096];
   char resp_data[4096];
   misc_response_packet_t response;
   int  fd;
   char command_file_name[1024];
   char output_file_name[1024];
   char output_command[1024];

   time_t now;

   ret = miscd_process_execute_shell_command_request(request, command);

   if (ret <= 0)
   {
      printf("ret == %d\n", ret);
      return -1;
   }

   time(&now);

   sprintf(command_file_name, "exec_command_%ld.sh", now);

   sprintf(output_file_name, "exec_command_op_%ld.txt", now);

   sprintf(output_command, "%s &>%s;", command, output_file_name);

   system(output_command);

   response.response_data = resp_data;

   fd = open(output_file_name, O_RDONLY);

   if (fd < 0)
   {
      return -1;
   }

   response.response        = MISCD_RESPONSE_OK;
   response.response_length = lseek(fd, 0, SEEK_END);

   if (response.response_length > 4095)
   {
      response.response_length = 4095;
   }

   lseek(fd, 0, SEEK_SET);

   read(fd, response.response_data, response.response_length);

   response.response_data[response.response_length] = 0;

   close(fd);

   _send_response(ctd, &response);

   unlink(output_file_name);
   unlink(command_file_name);

   return 0;

err:
   response.response        = MISCD_RESPONSE_ERR;
   response.response_data   = resp_data;
   response.response_length = strlen("ERROR") + 1;
   strcpy(response.response_data, "ERROR");
   printf("miscd:%s\n", resp_data);
   _send_response(ctd, &response);

   return _CRITICAL_ERR;
}


static void _send_response(_client_thread_data_t *data, misc_response_packet_t *response)
{
   int temp;

   _send_all(data->client_fd, &response->response, 1);
   temp = htonl(response->response_length);
   _send_all(data->client_fd, (unsigned char *)&temp, 4);
   if (response->response_length && (response->response_data != NULL))
   {
      _send_all(data->client_fd, response->response_data, response->response_length);
   }
}
