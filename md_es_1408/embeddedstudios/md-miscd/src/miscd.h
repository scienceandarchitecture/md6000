/********************************************************************************
* MeshDynamics
* --------------
* File     : miscd.h
* Comments : Miscllaneous Tasks Daemon
* Created  : 5/14/2005
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  0  |5/14/2005 | Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

#ifndef __MISCD_H__
#define __MISCD_H__

#define MISCD_TCP_PORT    0xBEBE

#endif /*__MISCD_H__*/
