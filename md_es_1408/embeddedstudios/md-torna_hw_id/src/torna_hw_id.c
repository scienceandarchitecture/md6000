/********************************************************************************
* MeshDynamics
* --------------
* File     : torna_hw_id.c
* Comments : Torna HW ID Module for Laguna
* Created  : 12/05/2008
* Author   : Abhijit Ayarekar
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  33 |12/05/2008| Created										  |Abhijit |
* -----------------------------------------------------------------------------
********************************************************************************/

#include <linux/version.h>

#if (LINUX_VERSION_CODE < KERNEL_VERSION(2, 6, 19))
#include <linux/config.h>
#else
#include <generated/autoconf.h>
#endif

#include <linux/version.h>
#include <linux/module.h>
#include <linux/netdevice.h>

#include <torna_types.h>
#include <torna_hw_id.h>


MODULE_AUTHOR("Meshdynamics, Inc");
MODULE_DESCRIPTION("Torna Hardware ID implementation for Laguna");
MODULE_SUPPORTED_DEVICE("Laguna");
MODULE_LICENSE("Proprietary");

//static char *version = _TORNA_VERSION_STRING_;

struct _torna_hw_id_address_list
{
   unsigned char                    address[ETH_ALEN];
   struct _torna_hw_id_address_list *all_next;
   struct _torna_hw_id_address_list *unallocated_next;
};

typedef struct _torna_hw_id_address_list   _torna_hw_id_address_list_t;

struct _torna_hw_id_data
{
   torna_hw_id_header_t        header;
   _torna_hw_id_address_list_t *all_addresses;
   _torna_hw_id_address_list_t *unallocated_addresses;
};

typedef struct _torna_hw_id_data   _torna_hw_id_data_t;

static _torna_hw_id_data_t _address_data =
{
   all_addresses                   : NULL,
   unallocated_addresses   : NULL
};

static spinlock_t _lock;

static const unsigned char _data_buffer[] =
{
   0x00, 0x12, 0xCE,
   0xFF, 0xFF, 0xFF,
   0x00, 0x12, 0xCE,
   0x00, 0x00, 0x01,
   0x00, 0x00, 0x00, 0x00, 0x00,
   6,
   0x00, 0x12, 0xCE, 0x00, 0x07, 0x00,
   0x00, 0x12, 0xCE, 0x00, 0x07, 0x01,
   0x00, 0x12, 0xCE, 0x00, 0x07, 0x02,
   0x00, 0x12, 0xCE, 0x00, 0x07, 0x03,
   0x00, 0x12, 0xCE, 0x00, 0x07, 0x04,
   0x00, 0x12, 0xCE, 0x00, 0x07, 0x05
};

static void _torna_hw_id_read(const unsigned char *buffer, int buffer_length);

static int __init torna_hw_id_init_module(void)
{
   spin_lock_init(&_lock);

//	_torna_hw_id_read(_data_buffer, sizeof(_data_buffer));

   return 0;
}


static void __init torna_hw_id_cleanup_module(void)
{
}


int torna_put_reboot_info(torna_reboot_info_t *info, unsigned int *stack_dump_info)
{
   return 0;
}


int torna_get_reboot_info(torna_reboot_info_t *info, unsigned char *flag, unsigned int *stack_dump_info)
{
   memset(info, 0, sizeof(torna_reboot_info_t));
   *flag = 0;

   return 0;
}


int torna_get_product_oui_id(unsigned char *product_id)
{
/*
 *      memcpy(product_id,_address_data.header.product_oui,3);
 *      memcpy(product_id + 3,_address_data.header.product_id,3);
 */
   return 0;
}


int torna_hw_id_get_address(unsigned char *address)
{
/*
 *      int	ret	= TORNA_HW_ID_NO_MORE_ADDRESSES;
 *
 *      spin_lock_irq(&_lock);
 *
 *      if(_address_data.unallocated_addresses != NULL) {
 *              memcpy(address,_address_data.unallocated_addresses->address,ETH_ALEN);
 *              _address_data.unallocated_addresses	= _address_data.unallocated_addresses->unallocated_next;
 *              ret	= TORNA_HW_ID_SUCCESS;
 *      }
 *
 *      spin_unlock_irq(&_lock);
 *
 *      return ret;
 */
   return 0;
}


int torna_get_generic_id(unsigned char *generic_id)
{
   //memcpy(generic_id,_address_data.header.generic_id,5);
   return 0;
}


static void _torna_hw_id_read(const unsigned char *buffer, int buffer_length)
{
   int i;
   _torna_hw_id_address_list_t *node;
   _torna_hw_id_address_list_t *all_tail;
   _torna_hw_id_address_list_t *unallocated_tail;
   const unsigned char         *p;

   spin_lock_init(&_lock);

   printk(KERN_INFO "\ntorna_hw_id: Reading Product information from Buffer\n");

   printk(KERN_INFO "\tReading header...(%d bytes)", sizeof(torna_hw_id_header_t));

   p = buffer;

   memcpy((unsigned char *)&_address_data.header, p, sizeof(torna_hw_id_header_t));
   p += sizeof(torna_hw_id_header_t);

   printk("[OK:%d addresses]\n", _address_data.header.address_count);

   if ((_address_data.header.md_oui[0] != 0x00) ||
       (_address_data.header.md_oui[1] != 0x12) ||
       (_address_data.header.md_oui[2] != 0xCE))
   {
      memset(&_address_data, 0, sizeof(_address_data));
      return;
   }

   all_tail         = NULL;
   unallocated_tail = NULL;

   for (i = 0; i < _address_data.header.address_count; i++)
   {
      node = (_torna_hw_id_address_list_t *)kmalloc(sizeof(_torna_hw_id_address_list_t), GFP_KERNEL);

      memcpy(node->address, p, ETH_ALEN);
      p += ETH_ALEN;

      node->all_next         = NULL;
      node->unallocated_next = NULL;

      if (_address_data.all_addresses == NULL)
      {
         _address_data.all_addresses = node;
      }
      else
      {
         all_tail->all_next = node;
      }

      all_tail = node;

      if (_address_data.unallocated_addresses == NULL)
      {
         _address_data.unallocated_addresses = node;
      }
      else
      {
         unallocated_tail->unallocated_next = node;
      }

      unallocated_tail = node;

      printk(KERN_INFO "\tAddress[%d]= " MACSTR "\n", i, MAC2STR(node->address));
   }
}


EXPORT_SYMBOL(torna_hw_id_get_address);
EXPORT_SYMBOL(torna_get_product_oui_id);
EXPORT_SYMBOL(torna_get_generic_id);
EXPORT_SYMBOL(torna_put_reboot_info);
EXPORT_SYMBOL(torna_get_reboot_info);

module_init(torna_hw_id_init_module);
module_exit(torna_hw_id_cleanup_module);
