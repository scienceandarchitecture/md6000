
##********************************************************************************
 # MeshDynamics 
 # -------------- 
 # File     : check_vital.sh
 # Comments : Checks vital files at startup
 # Created  : 4/22/2005
 # Author   : Sriram Dayanandan
 # Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
 #
 # File Revision History 
 # -----------------------------------------------------------------------------
 # | No  |Date      |  Comment                                        | Author |
 # -----------------------------------------------------------------------------
 # |  0  |4/22/2005 | Created.                                        | Sriram |
 # -----------------------------------------------------------------------------
 #*******************************************************************************/

FLAG=0

if [ -e /lib/modules/2.4.24-uc0/kernel/drivers/net/atheros.o ]
then
	echo
else
	FLAG=0
fi

if [ -e /lib/modules/2.4.24-uc0/kernel/drivers/net/ath_hal.o ]
then
	echo
else
	FLAG=0
fi

if [ -e /lib/modules/2.4.24-uc0/kernel/drivers/net/shasta_ixp425_eth.o ]
then
	echo
else
	FLAG=0
fi

if [ -e /lib/modules/2.4.24-uc0/kernel/drivers/torna_hw_id.o ]
then
	echo
else
	FLAG=0
fi

if [ -e /lib/modules/2.4.24-uc0/kernel/drivers/meshap.o ]
then
	echo
else
	FLAG=0
fi

if [ -e /lib/modules/2.4.24-uc0/kernel/drivers/shasta_board_info_drv.o ]
then
	echo
else
	FLAG=0
fi

if [ -e /root/startap.sh ]
then
	echo
else
	FLAG=1
fi

if [ -e /root/load_torna ]
then
	echo
else
	FLAG=1
fi

if [ -e /root/mktargz.sh ]
then
	echo
else
	FLAG=1
fi

if [ -e /root/post_fs_change.sh ]
then
	echo
else
	FLAG=1
fi

if [ $FLAG == 1 ] 
then
	/etc/recover.sh
fi
