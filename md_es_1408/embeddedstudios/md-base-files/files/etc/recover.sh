
##********************************************************************************
 # MeshDynamics 
 # -------------- 
 # File     : recover.sh
 # Comments : Script for recovering using backup
 # Created  : 4/22/2005
 # Author   : Sriram Dayanandan
 # Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
 #
 # File Revision History 
 # -----------------------------------------------------------------------------
 # | No  |Date      |  Comment                                        | Author |
 # -----------------------------------------------------------------------------
 # |  0  |4/22/2005 | Created.                                        | Sriram |
 # -----------------------------------------------------------------------------
 #*******************************************************************************/

echo "Recovering system using Factory defaults..."

rm -rf /jffs2

reboot
