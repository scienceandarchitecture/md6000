
##********************************************************************************
 # MeshDynamics 
 # -------------- 
 # File     : post_fs_changes.sh
 # Comments : Script for committing changes to flash
 # Created  : 4/22/2004
 # Author   : Sriram Dayanandan
 # Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
 #
 # File Revision History 
 # -----------------------------------------------------------------------------
 # | No  |Date      |  Comment                                        | Author |
 # -----------------------------------------------------------------------------
 # |  0  |4/22/2004 | Created.                                        | Sriram |
 # -----------------------------------------------------------------------------
 #*******************************************************************************/

echo "Shasta Post FS change script"

if [ $# -eq 0 ]
then
        echo "usage : post_fs_change type"
        echo
		echo "type = config, binary, bkconfig or bkbinary"
        exit
fi

echo "Done!"
