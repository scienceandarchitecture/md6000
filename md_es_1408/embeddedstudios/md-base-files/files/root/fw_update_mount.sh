##********************************************************************************
 # MeshDynamics 
 # -------------- 
 # File     : fw_update_mount.sh
 # Comments : Script for Firmware Update
 # Created  : 05/11/2006
 # Author   : Sriram Dayanandan
 # Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
 #
 # File Revision History 
 # -------------------------------------------------------------------------------
 # | No  |Date       |  Comment                                        | Author |
 # -------------------------------------------------------------------------------
 # |  0  |06/12/2006 | Created.                                        | Sriram |
 # -------------------------------------------------------------------------------
 #*******************************************************************************/

FWUPDATE_MTD=`cat /proc/mtd | grep fwupdate| cut -d: -f1 | cut -dd -f2`

if [ -e /dev/mtd$FWUPDATE_MTD ]
then

	mke2fs /dev/ram2

	mount /dev/ram2 /root/updates

	# Unzip fwupdate files

	cd /root/updates && gunzip < /dev/mtd$FWUPDATE_MTD | tar xv 

fi

