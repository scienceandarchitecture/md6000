##********************************************************************************
 # MeshDynamics 
 # -------------- 
 # File     : fw_update_save.sh
 # Comments : Script for Firmware Update
 # Created  : 06/08/2006
 # Author   : Prachiti Gaikwad
 # Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
 #
 # File Revision History 
 # ------------------------------------------------------------------------------
 # |  1  |06/13/2006 | Backward compatibility changes.                 | Sriram  |
 # ------------------------------------------------------------------------------
 # |  0  |05/11/2006 | Created.                                        | Prachiti|
 # ------------------------------------------------------------------------------
 #*******************************************************************************/


FWUPDATE_MTD=`cat /proc/mtd | grep fwupdate| cut -d: -f1 | cut -dd -f2`

if [ -e /dev/mtd$FWUPDATE_MTD ]
then

	meshd lock_reboot 1

	mke2fs /dev/ram3
	mkdir /fwupdate-temp
	mount /dev/ram3 /fwupdate-temp

	# Tar-ball all the new files

	cd /root/updates && tar cvf /fwupdate-temp/fwupdate.tar .

	# Gzip the fwupdate file

	gzip -9 /fwupdate-temp/fwupdate.tar

	# Write the fwupdate.tar.gz onto flash

	dd if=/fwupdate-temp/fwupdate.tar.gz of=/dev/mtdblock$FWUPDATE_MTD
	dd if=/dev/mtdblock$FWUPDATE_MTD of=/dev/null count=1

	# Unmmount ram2 and ram3

	umount /dev/ram3
	rm -rf /fwupdate-temp

	umount /dev/ram2

	meshd lock_reboot 0

else

	meshd lock_reboot 1
	/root/post_fs_change.sh  binary
	/root/post_fs_change.sh  config
	meshd lock_reboot 0

fi
