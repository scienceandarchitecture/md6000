
/********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : board_info_drv.c
 * Comments : Board info Driver Impl for Laguna
 * Created  : 12/05/2008
 * Author   : Abhijit Ayarekar
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * -----------------------------------------------------------------------------
 * | No  |Date      |  Comment                                        | Author |
 * -----------------------------------------------------------------------------
 * |  0  |12/05/2008| Created                                         |Abhijit |
 * -----------------------------------------------------------------------------
 ********************************************************************************/


#include <linux/version.h>

#if (LINUX_VERSION_CODE < KERNEL_VERSION(2,6,19))
	#include <linux/config.h>
#else
	#include <generated/autoconf.h>
#endif

#include <linux/module.h>
#include <linux/init.h>
#include <linux/version.h>
#include <linux/netdevice.h>
#include <linux/skbuff.h>
#include <linux/wireless.h>
#include <linux/proc_fs.h>
#include <linux/delay.h>
#include <linux/pci.h>
#include <linux/leds.h>

#include "torna_log.h"

TORNA_DEBUG_MASK_DECLARE(_TORNA_DEBUG_MASK_VALUE_);

MODULE_AUTHOR			("Meshdynamics, Inc");
MODULE_DESCRIPTION		("Laguna Board Info Driver version _TORNA_VERSION_STRING_");
MODULE_LICENSE			("GPL");
MODULE_SUPPORTED_DEVICE("Laguna");

/**
 * GPS data
 */

static char					_longitude[32];
static char					_latitude[32];
static char					_altitude[32];
static char					_speed[32];

static struct proc_dir_entry*	_brd_info_proc_entry	= NULL;
static struct led_classdev *meshap_led_cdev = NULL;
static struct timer_list timer;

int meshap_get_board_temp(void)
{		
	return 0;
}

int meshap_get_board_voltage(void)
{
	return 0;
}

void meshap_set_led_on(void) 
{
	if (meshap_led_cdev) {
		del_timer(&timer);
		led_brightness_set(meshap_led_cdev, LED_FULL);
		meshap_led_cdev->brightness = LED_FULL;
	}
}

void meshap_set_led_off(void) 
{
	if (meshap_led_cdev) {
		del_timer(&timer);
		led_brightness_set(meshap_led_cdev, LED_OFF);
		meshap_led_cdev->brightness = LED_OFF;
	}
}

void meshap_set_led_blink(void) // Blink on 1 second intervals
{
	unsigned long delay_on = 1000;
	unsigned long delay_off = 1000;

	if (meshap_led_cdev) {
		del_timer(&timer);
		led_blink_set(meshap_led_cdev, &delay_on, &delay_off);
		meshap_led_cdev->blink_delay_on = delay_on;
		meshap_led_cdev->blink_delay_off = delay_off;
	}
}

void meshap_set_led_blink_fast(void) // Blink on 0.5 second intervals
{
	unsigned long delay_on = 200;
	unsigned long delay_off = 200;
	if (meshap_led_cdev) {
		del_timer(&timer);
		led_blink_set(meshap_led_cdev, &delay_on, &delay_off);
		meshap_led_cdev->blink_delay_on = delay_on;
		meshap_led_cdev->blink_delay_off = delay_off;
	}
}

void meshap_set_led_blink_once(void) // Blink once on 1 second interval
{

	if (meshap_led_cdev) {
		led_brightness_set(meshap_led_cdev, LED_OFF);
		meshap_led_cdev->brightness = LED_OFF;
		mod_timer(&timer, jiffies + msecs_to_jiffies(1000));
	}
}

void meshap_enable_reset_generator(unsigned char enable_or_disable, int interval_in_millis)
{

}

void meshap_strobe_reset_generator(void)
{

}

int meshap_get_gpio(unsigned char line)
{
	return 0;
}

void meshap_set_gpio(unsigned char line, int value)
{
}

void meshap_set_gps_info(const char* longitude , const char* latitude, const char* altitude, const char* speed)
{
#define _IS_SPACE_OR_NULL(c) ((c) == 0 || (c) == ' ')

	static const char _un_init_value[] = {'0','.','0','0','0','0','0','0',0};

	if(!_IS_SPACE_OR_NULL(*latitude) && memcmp(latitude,_un_init_value,8))
		strcpy(_latitude,latitude);

	if(!_IS_SPACE_OR_NULL(*longitude) && memcmp(longitude,_un_init_value,8))
		strcpy(_longitude,longitude);

	if(!_IS_SPACE_OR_NULL(*altitude))
		strcpy(_altitude,altitude);

	if(!_IS_SPACE_OR_NULL(*speed))
		strcpy(_speed,speed);

#undef _IS_SPACE_OR_NULL
}

void meshap_get_gps_info(char* longitude , char* latitude, char* altitude, char* speed)
{
	strcpy(latitude,_latitude);
	strcpy(longitude,_longitude);
	strcpy(altitude,_altitude);
	strcpy(speed,_speed);
}

static int _temp_read_routine(char *page, char **start, off_t off,int count, int *eof, void *data)
{
	char			*p;

	if(off != 0) {
		*eof	= 1;
		return 0;
	}

	*start				= page + off;
	p					= page;

	p	+= sprintf(p,"N/A\n");

	return (p - page);	
}

static int _volt_read_routine(char *page, char **start, off_t off,int count, int *eof, void *data)
{
	char			*p;

	if(off != 0) {
		*eof	= 1;
		return 0;
	}

	*start				= page + off;
	p					= page;

	p	+= sprintf(p,"N/A");

	return (p - page);	
}

static int _coord_read_routine(char *page, char **start, off_t off,int count, int *eof, void *data)
{
	char			*p;

	if(off != 0) {
		*eof	= 1;
		return 0;
	}

	*start				= page + off;
	p					= page;

	p	+= sprintf(p,"%s,%s,%s\n",_longitude,_latitude,_altitude);

	return (p - page);	
}

static int _speed_read_routine(char *page, char **start, off_t off,int count, int *eof, void *data)
{
	char			*p;

	if(off != 0) {
		*eof	= 1;
		return 0;
	}

	*start				= page + off;
	p					= page;

	p	+= sprintf(p,"%s (Km/h)\n",_speed);

	return (p - page);	
}

static void meshap_led_timer(unsigned long data)
{
  struct led_classdev *led_cdev = (struct led_classdev *) data;
	if (led_cdev->brightness == LED_OFF) {
		led_brightness_set(meshap_led_cdev, LED_FULL);
		led_cdev->brightness = LED_FULL;
		mod_timer(&timer, jiffies + msecs_to_jiffies(1000));
	} else {
		meshap_set_led_off();
		led_cdev->brightness = LED_OFF;
	}
}

static void meshap_trig_activate(struct led_classdev *led_cdev)
{
	if (meshap_led_cdev == NULL) {
		led_cdev->trigger_data = (void *)1;
		meshap_led_cdev = led_cdev;
		setup_timer(&timer, meshap_led_timer, (unsigned long) led_cdev);
	}
}

static void meshap_trig_deactivate(struct led_classdev *led_cdev)
{
	if (led_cdev->trigger_data)
		del_timer_sync(&timer);
	led_cdev->trigger_data = NULL;
	meshap_led_cdev = NULL;
}

static struct led_trigger meshap_led_trigger = {
  .name     = "meshap",
  .activate = meshap_trig_activate,
	.deactivate = meshap_trig_deactivate,
};

static int __init _meshap_init(void)
{

	led_trigger_register(&meshap_led_trigger);

	strcpy(_longitude,"0.0");
	strcpy(_latitude,"0.0");
	strcpy(_altitude,"0.0");
	strcpy(_speed,"0.0");

	_brd_info_proc_entry	= proc_mkdir("brdinfo",NULL);
	create_proc_read_entry("voltage",0,_brd_info_proc_entry,_volt_read_routine,NULL);
	create_proc_read_entry("temp",0,_brd_info_proc_entry,_temp_read_routine,NULL);
	create_proc_read_entry("coord",0,_brd_info_proc_entry,_coord_read_routine,NULL);
	create_proc_read_entry("speed",0,_brd_info_proc_entry,_speed_read_routine,NULL);

	return 0;
}

static void __exit _meshap_exit(void)
{
	led_trigger_unregister(&meshap_led_trigger);
}

EXPORT_SYMBOL(meshap_get_board_temp);
EXPORT_SYMBOL(meshap_get_board_voltage);
EXPORT_SYMBOL(meshap_set_led_on);
EXPORT_SYMBOL(meshap_set_led_off);
EXPORT_SYMBOL(meshap_set_led_blink);
EXPORT_SYMBOL(meshap_set_led_blink_fast);
EXPORT_SYMBOL(meshap_set_led_blink_once);
EXPORT_SYMBOL(meshap_enable_reset_generator);
EXPORT_SYMBOL(meshap_strobe_reset_generator);
EXPORT_SYMBOL(meshap_get_gpio);
EXPORT_SYMBOL(meshap_set_gpio);
EXPORT_SYMBOL(meshap_set_gps_info);
EXPORT_SYMBOL(meshap_get_gps_info);

module_init(_meshap_init);
module_exit(_meshap_exit);

