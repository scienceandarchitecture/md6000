/********************************************************************************
* MeshDynamics
* --------------
* File     : auto_start.h
* Comments : Auto Start Command Parser
* Created  : 10/16/2007
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  0  |10/16/2007 | Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

#ifndef __AUTO_START_H__
#define __AUTO_START_H__

typedef void * auto_start_token_t;


auto_start_token_t auto_start_open(const char *file_name);
void auto_start_save(auto_start_token_t token);
void auto_start_close(auto_start_token_t token);

typedef void (*auto_start_enum_t)       (void *cookie, int number, const char *command_block_string);

void auto_start_enumerate_blocks(auto_start_token_t token, auto_start_enum_t enum_func, void *cookie);
void auto_start_append_block(auto_start_token_t token, int block_number, const char *command);
int auto_start_create_block(auto_start_token_t token);
void auto_start_delete_block(auto_start_token_t token, int block_number);

#endif /*__AUTO_START_H__*/
