
###******************************************************************************
 # MeshDynamics 
 # -------------- 
 # File     : Rules.make
 # Comments : Global definitions for Torna alconfset compilations
 # Created  : 11/9/2004
 # Author   : Abhijit Ayarekar
 # Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 # File Revision History 
 # -----------------------------------------------------------------------------
 # | No  |Date      |  Comment                                        | Author |
 # -----------------------------------------------------------------------------
 # |  0  |11/9/2004  | Created.                                       | Abhijit|
 # -----------------------------------------------------------------------------
 #*******************************************************************************/

ifndef CC
	CC :=/usr/local/bin/arm-linux-gcc
endif

ifndef LD
	LD := /usr/local/bin/arm-linux-ld
endif

CFLAGS=-O3 -Wall -c $(EXTRA_CFLAGS)

include $(TOPDIR)/Rules.make
include $(TOPDIR)/arch.inc

