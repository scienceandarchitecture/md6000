/********************************************************************************
* MeshDynamics
* --------------
* File     : alconfset.c
* Comments : Generic alconf field reader
* Created  : 11/9/2004
* Author   : Abhijit Ayarekar
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  6  |1/27/2009 | Changes for Uplink Scan Functionality           | Sriram |
* -----------------------------------------------------------------------------
* |  5  |10/13/2008| Added RX on all option for antport              | Sriram |
* -----------------------------------------------------------------------------
* |  4  |8/27/2008 | Changes for options                             | Sriram |
* -----------------------------------------------------------------------------
* |  3  |11/21/2006| Added AL Gen2k implementation                   | Sriram |
* -----------------------------------------------------------------------------
* |  2  |03/12/04  | Implemented al_fns & change in call to fns      |        |
* |     |          | setting values in config file (param changes)   | Bindu  |
* -----------------------------------------------------------------------------
* |  1  |02/12/04  | Added fns to set:mesh_id, imcp_key,if phy_type, |		   |
* |    |			| sub_type, use_type, if_bonding, if_preamble_type|        |
* |    |			| if_slot_time_type, if_service	(edited fns too)  | Bindu  |
* -----------------------------------------------------------------------------
* |  0  |11/9/2004 | Created.                                        | Abhijit|
* -----------------------------------------------------------------------------
********************************************************************************/


#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <getopt.h>
#include <string.h>
#include <strings.h>
#include "al.h"
#include "al_context.h"
#include "al_impl_context.h"
#include "al_conf.h"
#include "dot11e_conf.h"
#include "auto_start.h"

static int _set_name(int argc, int optind, char **argv, const char *operation, int conf_handle);
static int _set_mesh_id(int argc, int optind, char **argv, const char *operation, int conf_handle);
static int _set_mesh_imcp_key(int argc, int optind, char **argv, const char *operation, int conf_handle);
static int _set_global_dca(int argc, int optind, char **argv, const char *operation, int conf_handle);
static int _set_stay_awake_count(int argc, int optind, char **argv, const char *operation, int conf_handle);
static int _set_bridge_aging(int argc, int optind, char **argv, const char *operation, int conf_handle);
static int _set_reg_domain_code(int argc, int optind, char **argv, const char *operation, int conf_handle);
static int _set_country_code(int argc, int optind, char **argv, const char *operation, int conf_handle);
static int _set_medium_type(int argc, int optind, char **argv, const char *operation, int conf_handle);
static int _set_medium_sub_type(int argc, int optind, char **argv, const char *operation, int conf_handle);
static int _set_use_type(int argc, int optind, char **argv, const char *operation, int conf_handle);
static int _set_channel(int argc, int optind, char **argv, const char *operation, int conf_handle);
static int _set_essid(int argc, int optind, char **argv, const char *operation, int conf_handle);
static int _set_rts(int argc, int optind, char **argv, const char *operation, int conf_handle);
static int _set_frag(int argc, int optind, char **argv, const char *operation, int conf_handle);
static int _set_beacint(int argc, int optind, char **argv, const char *operation, int conf_handle);
static int _set_dca(int argc, int optind, char **argv, const char *operation, int conf_handle);
static int _set_dca_list(int argc, int optind, char **argv, const char *operation, int conf_handle);
static int _set_ant_port(int argc, int optind, char **argv, const char *operation, int conf_handle);
static int _set_txrate(int argc, int optind, char **argv, const char *operation, int conf_handle);
static int _set_txpower(int argc, int optind, char **argv, const char *operation, int conf_handle);
static int _set_preamble(int argc, int optind, char **argv, const char *operation, int conf_handle);

#if MDE_80211N_SUPPORT 
static int _set_ldpc(int argc, int optind, char **argv, const char *operation, int conf_handle);
static int _set_smps(int argc, int optind, char **argv, const char *operation, int conf_handle);
static int _set_tx_stbc(int argc, int optind, char **argv, const char *operation, int conf_handle);
static int _set_rx_stbc(int argc, int optind, char **argv, const char *operation, int conf_handle);
static int _set_delayed_ba(int argc, int optind, char **argv, const char *operation, int conf_handle);
static int _set_intolerant(int argc, int optind, char **argv, const char *operation, int conf_handle);
static int _set_lsig_txop(int argc, int optind, char **argv, const char *operation, int conf_handle);
static int _set_gi_20(int argc, int optind, char **argv, const char *operation, int conf_handle);
static int _set_gi_40(int argc, int optind, char **argv, const char *operation, int conf_handle);
static int _set_dsss_cck_40(int argc, int optind, char **argv, const char *operation, int conf_handle);
static int _set_gfmode(int argc, int optind, char **argv, const char *operation, int conf_handle);
static int _set_ht_bandwidth(int argc, int optind, char **argv, const char *operation, int conf_handle);
static int _set_max_amsdu_len(int argc, int optind, char **argv, const char *operation, int conf_handle);
static int _set_ampdu_enable(int argc, int optind, char **argv, const char *operation, int conf_handle);
static int _set_max_ampdu_len(int argc, int optind, char **argv, const char *operation, int conf_handle);
#endif
#if MDE_80211AC_SUPPORT 
static int _set_max_mpdu_len(int argc, int optind, char **argv, const char *operation, int conf_handle);
static int _set_supported_channel_width(int argc, int optind, char **argv, const char *operation, int conf_handle);
static int _set_rx_ldpc(int argc, int optind, char **argv, const char *operation, int conf_handle);
static int _set_gi_80(int argc, int optind, char **argv, const char *operation, int conf_handle);
static int _set_gi_160(int argc, int optind, char **argv, const char *operation, int conf_handle);
static int _set_vtx_stbc(int argc, int optind, char **argv, const char *operation, int conf_handle);
static int _set_vrx_stbc(int argc, int optind, char **argv, const char *operation, int conf_handle);
static int _set_su_beamformer_cap(int argc, int optind, char **argv, const char *operation, int conf_handle);
static int _set_su_beamformee_cap(int argc, int optind, char **argv, const char *operation, int conf_handle);
static int _set_beamformee_sts_count(int argc, int optind, char **argv, const char *operation, int conf_handle);
static int _set_sounding_dimensions(int argc, int optind, char **argv, const char *operation, int conf_handle);
static int _set_mu_beamformer_cap(int argc, int optind, char **argv, const char *operation, int conf_handle);
static int _set_mu_beamformee_cap(int argc, int optind, char **argv, const char *operation, int conf_handle);
static int _set_vht_txop_ps(int argc, int optind, char **argv, const char *operation, int conf_handle);
static int _set_htc_vht_cap(int argc, int optind, char **argv, const char *operation, int conf_handle);
static int _set_rx_ant_pattern_consistency(int argc, int optind, char **argv, const char *operation, int conf_handle);
static int _set_tx_ant_pattern_consistency(int argc, int optind, char **argv, const char *operation, int conf_handle);
static int _set_vht_oper_bandwidth(int argc, int optind, char **argv, const char *operation, int conf_handle);
static int _set_seg0_center_freq(int argc, int optind, char **argv, const char *operation, int conf_handle);
static int _set_seg1_center_freq(int argc, int optind, char **argv, const char *operation, int conf_handle);
#endif
static int _set_slot(int argc, int optind, char **argv, const char *operation, int conf_handle);
static int _set_link_opt(int argc, int optind, char **argv, const char *operation, int conf_handle);
static int _set_ack_time(int argc, int optind, char **argv, const char *operation, int conf_handle);
static int _set_hide_essid(int argc, int optind, char **argv, const char *operation, int conf_handle);
static int _set_preferred_parent(int argc, int optind, char **argv, const char *operation, int conf_handle);
static int _set_hb_interval(int argc, int optind, char **argv, const char *operation, int conf_handle);
static int _set_igmp(int argc, int optind, char **argv, const char *operation, int conf_handle);
static int _set_adhoc(int argc, int optind, char **argv, const char *operation, int conf_handle);
static int _set_dhcp(int argc, int optind, char **argv, const char *operation, int conf_handle);
static int _set_forced_root(int argc, int optind, char **argv, const char *operation, int conf_handle);
static int _set_fips(int argc, int optind, char **argv, const char *operation, int conf_handle);
static int _set_dfs(int argc, int optind, char **argv, const char *operation, int conf_handle);
static int _set_mobility_index(int argc, int optind, char **argv, const char *operation, int conf_handle);
static int _set_mobility_mode(int argc, int optind, char **argv, const char *operation, int conf_handle);
static int _set_gps(int argc, int optind, char **argv, const char *operation, int conf_handle);
static int _set_logmon(int argc, int optind, char **argv, const char *operation, int conf_handle);
static int _set_autostart(int argc, int optind, char **argv, const char *operation, int conf_handle);
static int _set_location(int argc, int optind, char **argv, const char *operation, int conf_handle);
static int _set_acwmin(int argc, int optind, char **argv, const char *operation, int conf_handle);
static int _set_acwmax(int argc, int optind, char **argv, const char *operation, int conf_handle);
static int _set_aifsn(int argc, int optind, char **argv, const char *operation, int conf_handle);
static int _set_backoff(int argc, int optind, char **argv, const char *operation, int conf_handle);
static int _set_burst(int argc, int optind, char **argv, const char *operation, int conf_handle);
static int _set_use_virt(int argc, int optind, char **argv, const char *operation, int conf_handle);
static int _set_option(int argc, int optind, char **argv, const char *operation, int conf_handle);

static int _save_config(int argc, int optind, char **argv, const char *operation, int conf_handle);
static int _save_fw(int argc, int optind, char **argv, const char *operation, int conf_handle);
static int _fail_over_ethernet(int argc, int optind, char **argv, const char *operation, int conf_handle);
static int _set_server_ip_addr(int argc, int optind, char **argv, const char *openration, int conf_handle);
static int _set_mgmt_gw_addr(int argc, int optind, char **argv, const char *openration, int conf_handle);
static int _set_mgmt_gw_enable(int argc, int optind, char **argv, const char *openration, int conf_handle);
static int _set_mgmt_gw_certificates(int argc, int optind, char **argv, const char *openration, int conf_handle);
static int _set_disable_backhaul_security(int argc, int optind, char **argv, const char *operation, int conf_handle);

static struct
{
   const char *name;
   int        (*func)(int argc, int optind, char **argv, const char *operation, int conf_handle);
}
_field_retrievers[] =
{
   { "name",                       _set_name                       },
   { "mesh_id",                    _set_mesh_id                    },
   { "mesh_imcp_key",              _set_mesh_imcp_key              },
   { "globdca",                    _set_global_dca                 },
   { "sac",                        _set_stay_awake_count           },
   { "aging",                      _set_bridge_aging               },
   { "regdom",                     _set_reg_domain_code            },
   { "country",                    _set_country_code               },
   { "medtype",                    _set_medium_type                },
   { "subtype",                    _set_medium_sub_type            },
   { "usetype",                    _set_use_type                   },
   { "channel",                    _set_channel                    },
   { "essid",                      _set_essid                      },
   { "rts",                        _set_rts                        },
   { "frag",                       _set_frag                       },
   { "beacint",                    _set_beacint                    },
   { "dca",                        _set_dca                        },
   { "dcalist",                    _set_dca_list                   },
   { "antport",                    _set_ant_port                   },
   { "txrate",                     _set_txrate                     },
   { "preamble",                   _set_preamble                   },
#if MDE_80211N_SUPPORT 
   { "ldpc",                       _set_ldpc                       },
   { "smps",                       _set_smps                       },
   { "tx_stbc",                    _set_tx_stbc                    },
   { "rx_stbc",                    _set_rx_stbc                    },
   { "delayed_ba",                 _set_delayed_ba                 },
   { "intolerant",                 _set_intolerant                 },
   { "lsig_txop",                  _set_lsig_txop                  },
   { "gi_20",                      _set_gi_20                      },
   { "gi_40",                      _set_gi_40                      },
   { "dsss_cck_40",                _set_dsss_cck_40                },
   { "ht_bandwidth",               _set_ht_bandwidth               },
   { "max_amsdu_len",              _set_max_amsdu_len              },
   { "ampdu_enable",               _set_ampdu_enable               },
   { "max_ampdu_len",              _set_max_ampdu_len              },
   { "gfmode",                     _set_gfmode                     },
#endif

#if MDE_80211AC_SUPPORT   
   { "max_mpdu_len",               _set_max_mpdu_len               },
   { "supported_channel_width",    _set_supported_channel_width    },
   { "rx_ldpc",                    _set_rx_ldpc                    },
   { "gi_80",                      _set_gi_80                      },
   { "gi_160",                     _set_gi_160                     },
   { "vtx_stbc",                   _set_vtx_stbc                   },
   { "vrx_stbc",                   _set_vrx_stbc                   },
   { "su_beamformer_cap",          _set_su_beamformer_cap          },
   { "su_beamformee_cap",          _set_su_beamformee_cap          },
   { "beamformee_sts_count",       _set_beamformee_sts_count       },
   { "sounding_dimensions",        _set_sounding_dimensions        },
   { "mu_beamformer_cap",          _set_mu_beamformer_cap          },
   { "mu_beamformee_cap",          _set_mu_beamformee_cap          },
   { "vht_txop_ps",                _set_vht_txop_ps                },
   { "htc_vht_cap",                _set_htc_vht_cap                },
   { "rx_ant_pattern_consistency", _set_rx_ant_pattern_consistency },
   { "tx_ant_pattern_consistency", _set_tx_ant_pattern_consistency },
   { "vht_oper_bandwidth",         _set_vht_oper_bandwidth         },
   { "seg0_center_freq",           _set_seg0_center_freq           },
   { "seg1_center_freq",           _set_seg1_center_freq           },
#endif
   { "slot",                       _set_slot                       },
   { "linkopt",                    _set_link_opt                   },
   { "acktime",                    _set_ack_time                   },
   { "hidessid",                   _set_hide_essid                 },
   { "txpower",                    _set_txpower                    },
   { "prefpar",                    _set_preferred_parent           },
   { "hbint",                      _set_hb_interval                },
   { "igmp",                       _set_igmp                       },
   { "adhoc",                      _set_adhoc                      },
   { "dhcp",                       _set_dhcp                       },
   { "forcedroot",                 _set_forced_root                },
   { "fips",                       _set_fips                       },
   { "dfs",                        _set_dfs                        },
   { "mobindex",                   _set_mobility_index             },
   { "mobmode",                    _set_mobility_mode              },
   { "gps",                        _set_gps                        },
   { "logmon",                     _set_logmon                     },
   { "location",                   _set_location                   },
   { "acwmin",                     _set_acwmin                     },
   { "acwmax",                     _set_acwmax                     },
   { "aifsn",                      _set_aifsn                      },
   { "backoff",                    _set_backoff                    },
   { "burst",                      _set_burst                      },
   { "autostart",                  _set_autostart                  },
   { "usevirt",                    _set_use_virt                   },
   { "option",                     _set_option                     },
   { "save",                       _save_config                    },
   { "savefw",                     _save_fw                        },
   { "failOverEthernet",           _fail_over_ethernet             },
   { "server_ip_addr",             _set_server_ip_addr             },
	{ "mgmt_gw_addr",		  			  _set_mgmt_gw_addr					 },
	{ "mgmt_gw_enable",				  _set_mgmt_gw_enable				 },
	{ "mgmt_gw_certificates",		  _set_mgmt_gw_certificates		 },
	{ "disable_backhaul_security",  _set_disable_backhaul_security	 }
};


static void _usage(void)
{
   fprintf(stderr,
           "\n"
           "usage: alconfset [-v] [-h] [-f configfile] fieldname [fieldparams]\n"
           "-v         : for knowing the version\n"
           "-h         : for help\n"
           "-f         : specifiy configfile (/etc/meshap.conf is default)\n"
           "fieldname  : \n"
           "   mesh_id,mesh_imcp_key,name,globdca,sac,aging,regdom\n"
           "   country,medtype,subtype,usetype,channel,essid,rts,frag,\n"
           "   beacint,dca,dcalist,antport,txrate,preamble,ldpc,smps,tx_stbc,rx_stbc,delayed_ba,gfmode,\n"
           "   max_amsdu_len,ht_bandwidth,dsss_cck_40,gi_40,gi_20,lsig_txop,intolerant,ampdu_enable,slot,linkopt,\n"                  
           "   max_ampdu_len,max_mpdu_len,supported_channel_width,acktime,hidessid,txpower,prefpar,hbint,igmp,adhoc,fips,\n"
           "   rx_ldpc,gi_80,gi_160,vtx_stbc,vrx_stbc,su_beamformer_cap,su_beamformee_cap,beamformee_sts_count,sounding_dimensions,\n"
           "   mu_beamformer_cap,mu_beamformee_cap,vht_txop_ps,htc_vht_cap,rx_ant_pattern_consistency,tx_ant_pattern_consistency,\n"
           "   vht_oper_bandwidth,seg0_center_freq,seg1_center_freq,\n"
           "   dhcp,gps,logmon,forcedroot,dfs,mobindex,mobmode,location,\n"
           "   usevirt,acwmin,acwmax,aifsn,backoff,burst,autostart,option,\n"
           "   save,savefw,failOverEthernet, server_ip_addr, mgmt_gw_enable, mgmt_gw_addr, mgmt_gw_certificates </path/xyz.crt>:</path/xyz.key>,\n"
           );
}


#define _DEFAULT_CONF_FILE_PATH        "/etc/meshap.conf"
#define _DEFAULT_11E_CONF_FILE_PATH    "/etc/dot11e.conf"

int main(int argc, char **argv)
{
   char          config_file_path[256];
   char          field[64];
   int           ch;
   int           i;
   int           fd;
   off_t         offset;
   unsigned char *file_data;
   int           al_conf_handle;
   int           ret;

   strcpy(config_file_path, _DEFAULT_CONF_FILE_PATH);
   *field = 0;

   while ((ch = getopt(argc, argv, "vhf:")) != EOF)
   {
      switch (ch)
      {
      case 'v':
         fprintf(stdout,
                 "\nMeshDynamics alconfset Version %s\n"
                 "Copyright (c) 2002-2007 Meshdynamics, Inc\n"
                 "All rights reserved.\n\n",
                 _TORNA_VERSION_STRING_);
         return 0;

      case 'h':
         _usage();
         return 0;

      case 'f':
         strcpy(config_file_path, optarg);
         break;
      }
   }

   if (optind >= argc)
   {
      fprintf(stderr, "\nNo field specified\n");
      _usage();
      return -1;
   }


   /**
    * Read in the config file into memory
    */

   fd = open(config_file_path, O_RDONLY);

   if (fd == -1)
   {
      fprintf(stderr, "Error opening %s for reading\n", config_file_path);
      return -2;
   }

   offset = lseek(fd, 0, SEEK_END);

   file_data = (unsigned char *)malloc(offset);
   memset(file_data, 0, offset + 1);

   lseek(fd, 0, SEEK_SET);

   read(fd, file_data, offset);

   close(fd);

   al_conf_handle = al_conf_parse(AL_CONTEXT file_data);

   if (al_conf_handle == -1)
   {
      free(file_data);
      return -3;
   }

   free(file_data);

   strcpy(field, argv[optind]);

   for (i = 0; i < sizeof(_field_retrievers) / sizeof(*_field_retrievers); i++)
   {
      if (!strcmp(field, _field_retrievers[i].name))
      {
         ret = _field_retrievers[i].func(argc, optind, argv, field, al_conf_handle);
         if (ret == 0)
         {
            al_conf_put(AL_CONTEXT al_conf_handle);
         }
         al_conf_close(AL_CONTEXT al_conf_handle);
         return ret;
      }
   }

   al_conf_close(AL_CONTEXT al_conf_handle);

   fprintf(stderr, "\nUnknown field %s\n", field);
   _usage();

   return -1;
}


static int _set_name(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   int  ret;
   char *value;

   if (argc <= optind + 1)
   {
      fprintf(stderr, "usage: alconfset name <<value>>\n");
      return -4;
   }

   value = argv[optind + 1];

   ret = al_conf_set_name(AL_CONTEXT conf_handle, value, strlen(value));

   if (!ret)
   {
      fprintf(stderr, "Success[name=%s]\n", value);
   }
   else
   {
      fprintf(stderr, "<<err %d>>\n", ret);
   }

   return ret;
}


static int _set_mesh_id(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   int  ret;
   char *value;

   if (argc <= optind + 1)
   {
      fprintf(stderr, "usage: alconfset mesh_id <<value>>\n");
      return -4;
   }

   value = argv[optind + 1];

   ret = al_conf_set_mesh_id(AL_CONTEXT conf_handle, value, strlen(value));

   if (!ret)
   {
      fprintf(stderr, "Success[mesh_id=%s]\n", value);
   }
   else
   {
      fprintf(stdout, "<<err %d>>\n", ret);
   }

   return ret;
}


static int _set_mesh_imcp_key(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
#define _MAX_LEN         256
#define _MAC_ADDR_LEN    6

   char          mesh_imcp_key_buffer[256];
   unsigned char output[256], input[1024];
   char          mac_address_ascii[32];
   char          mac_address[_MAC_ADDR_LEN];
   char          text_key[_MAX_LEN];
   int           text_key_len;
   int           ret;


   if (argc <= optind + 2)
   {
      fprintf(stderr, "usage: alconfset mesh_imcp_key <<self_mac_address>> <<value>>\n");
      return -4;
   }

   memset(output, 0, 256);
   memset(mesh_imcp_key_buffer, 0, 256);

   strcpy(mac_address_ascii, argv[optind + 1]);
   strcpy(text_key, argv[optind + 2]);

   text_key_len = strlen(text_key);

   mac_address_ascii_to_binary(mac_address_ascii, mac_address);

   ret = al_encode_key(AL_CONTEXT(unsigned char *) text_key, text_key_len, (unsigned char *)mac_address, output);

   if (ret <= 0)
   {
      fprintf(stderr, "Error encoding key (%d)\n", ret);
      return -4;
   }

   memcpy(mesh_imcp_key_buffer, output, ret);

   /*
    *	Copy encoded key to file
    */

   ret = al_conf_set_mesh_imcp_key_buffer(AL_CONTEXT conf_handle, mesh_imcp_key_buffer, ret);

   if (ret != 0)
   {
      printf("<<err %d>>\n", ret);
      return 1;
   }
   else
   {
      fprintf(stderr, "Success[mesh_imcp_key=%s]\n", text_key);
   }

   return ret;
}


static int _set_global_dca(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   int  ret;
   char *value;
   int  nvalue;

   if (argc <= optind + 1)
   {
      fprintf(stderr, "usage: alconfset globdca <<value=0 or 1>>\n");
      return -4;
   }

   value  = argv[optind + 1];
   nvalue = atoi(value);

   if ((nvalue != 0) && (nvalue != 1))
   {
      fprintf(stderr, "usage: alconfset globdca <<value=0 or 1>>\n");
      return -4;
   }

   ret = al_conf_set_dynamic_channel_allocation(AL_CONTEXT conf_handle, nvalue);

   if (!ret)
   {
      fprintf(stderr, "Success[globdca=%d]\n", nvalue);
   }
   else
   {
      fprintf(stdout, "<<err %d>>\n", ret);
   }

   return ret;
}


static int _set_use_virt(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   int  ret;
   char *value;
   int  nvalue;

   if (argc <= optind + 1)
   {
      fprintf(stderr, "usage: alconfset usevirt <<value (0 or 1)>>\n");
      return -4;
   }

   value  = argv[optind + 1];
   nvalue = atoi(value);

   if ((nvalue != 0) && (nvalue != 1))
   {
      fprintf(stderr, "usage: alconfset usevirt <<value (0 or 1)>>\n");
      return -4;
   }

   ret = al_conf_set_use_virt_if(AL_CONTEXT conf_handle, nvalue);

   if (!ret)
   {
      fprintf(stderr, "Success[usevirt=%d]\n", nvalue);
   }
   else
   {
      fprintf(stdout, "<<err %d>>\n", ret);
   }

   return ret;
}


static int _set_stay_awake_count(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   int  ret;
   char *value;
   int  nvalue;

   if (argc <= optind + 1)
   {
      fprintf(stderr, "usage: alconfset sac <<value (non-zero positive)>>\n");
      return -4;
   }

   value  = argv[optind + 1];
   nvalue = atoi(value);

   if (nvalue <= 0)
   {
      fprintf(stderr, "usage: alconfset sac <<value (non-zero positive)>>\n");
      return -4;
   }

   ret = al_conf_set_stay_awake_count(AL_CONTEXT conf_handle, nvalue);

   if (!ret)
   {
      fprintf(stderr, "Success[sac=%d]\n", nvalue);
   }
   else
   {
      fprintf(stdout, "<<err %d>>\n", ret);
   }

   return ret;
}


static int _set_bridge_aging(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   int  ret;
   char *value;
   int  nvalue;

   if (argc <= optind + 1)
   {
      fprintf(stderr, "usage: alconfset aging <<value (non-zero possitive)>>\n");
      return -4;
   }

   value  = argv[optind + 1];
   nvalue = atoi(value);

   if (nvalue <= 0)
   {
      fprintf(stderr, "usage: alconfset aging <<value (non-zero possitive)>>\n");
      return -4;
   }

   ret = al_conf_set_bridge_ageing_time(AL_CONTEXT conf_handle, nvalue);

   if (!ret)
   {
      fprintf(stderr, "Success[aging=%d]\n", nvalue);
   }
   else
   {
      fprintf(stdout, "<<err %d>>\n", ret);
   }

   return ret;
}


static int _set_reg_domain_code(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   int            ret;
   char           *value;
   int            nvalue;
   unsigned short regdom;

   if (argc <= optind + 1)
   {
      fprintf(stderr, "usage: alconfset regdom <<0=NONE,1=FCC,2=ETSI,3=CUSTOM>>\n");
      return -4;
   }

   value  = argv[optind + 1];
   nvalue = atoi(value);

   if ((nvalue < 0) || (nvalue > 3))
   {
      fprintf(stderr, "usage: alconfset regdom <<0=NONE,1=FCC,2=ETSI,3=CUSTOM>>\n");
      return -4;
   }

   regdom  = al_conf_get_regulatory_domain(AL_CONTEXT conf_handle);
   regdom &= 0xFF00;
   regdom |= nvalue;

   ret = al_conf_set_regulatory_domain(AL_CONTEXT conf_handle, regdom);

   if (!ret)
   {
      fprintf(stderr, "Success[regdom=%d]\n", nvalue);
   }
   else
   {
      fprintf(stdout, "<<err %d>>\n", ret);
   }

   return ret;
}


static int _set_country_code(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   int  ret;
   char *value;
   int  nvalue;

   if (argc <= optind + 1)
   {
      fprintf(stderr, "usage: alconfset country <<value (non-zero positive)>>\n");
      return -4;
   }

   value  = argv[optind + 1];
   nvalue = atoi(value);

   if (nvalue <= 0)
   {
      fprintf(stderr, "usage: alconfset country <<value (non-zero positive)>>\n");
      return -4;
   }

   ret = al_conf_set_country_code(AL_CONTEXT conf_handle, nvalue);

   if (!ret)
   {
      fprintf(stderr, "Success[country=%d]\n", nvalue);
   }
   else
   {
      fprintf(stdout, "<<err %d>>\n", ret);
   }

   return ret;
}


typedef int (*conf_if_callback_t)(int argc, int optind, char **argv, char *value, int nvalue, al_conf_if_info_t *if_info);

static int _if_iterator(int argc, int optind, char **argv, int conf_handle, conf_if_callback_t func)
{
   char              *if_name;
   char              *value;
   int               nvalue;
   int               i;
   int               count;
   al_conf_if_info_t if_info;
   int               ret;

   if (argc <= optind + 2)
   {
      return -4;
   }

   if_name = argv[optind + 1];
   value   = argv[optind + 2];
   nvalue  = atoi(value);

   count = al_conf_get_if_count(AL_CONTEXT conf_handle);

   for (i = 0; i < count; i++)
   {
      al_conf_get_if(AL_CONTEXT conf_handle, i, &if_info);

      if (!strcmp(if_info.name, if_name))
      {
         ret = func(argc, optind, argv, value, nvalue, &if_info);

         if (ret == 0)
         {
            al_conf_set_if(AL_CONTEXT conf_handle, i, &if_info);
         }

         return ret;
      }
   }

	fprintf(stderr, "<<err interface %s not found>>\n",if_name);   

   return -5;
}


static int _med_type_callback(int argc, int optind, char **argv, char *value, int nvalue, al_conf_if_info_t *if_info)
{
   if_info->phy_type = nvalue;
   fprintf(stderr, "Success[%s medtype=%d]\n", if_info->name, nvalue);
   return 0;
}


static int _set_medium_type(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   int ret;

   ret = _if_iterator(argc, optind, argv, conf_handle, _med_type_callback);

   if (ret == -4)
   {
      fprintf(stderr, "usage: alconfset medtype <<if-name>> <<value 0=ethernet,1=802.11>>\n");
   }

   return ret;
}


static int _sub_type_callback(int argc, int optind, char **argv, char *value, int nvalue, al_conf_if_info_t *if_info)
{
   if (!strcmp(value, "x"))
   {
      nvalue = AL_CONF_IF_PHY_SUB_TYPE_IGNORE;
   }
   else if (!strcmp(value, "a"))
   {
      nvalue = AL_CONF_IF_PHY_SUB_TYPE_802_11_A;
   }
   else if (!strcmp(value, "b"))
   {
      nvalue = AL_CONF_IF_PHY_SUB_TYPE_802_11_B;
   }
   else if (!strcmp(value, "g"))
   {
      nvalue = AL_CONF_IF_PHY_SUB_TYPE_802_11_G;
   }
   else if (!strcmp(value, "bg"))
   {
      nvalue = AL_CONF_IF_PHY_SUB_TYPE_802_11_BG;
   }
   else if (!strcmp(value, "psq"))
   {
      nvalue = AL_CONF_IF_PHY_SUB_TYPE_802_11_PSQ;
   }
   else if (!strcmp(value, "psh"))
   {
      nvalue = AL_CONF_IF_PHY_SUB_TYPE_802_11_PSH;
   }
   else if (!strcmp(value, "psf"))
   {
      nvalue = AL_CONF_IF_PHY_SUB_TYPE_802_11_PSF;
   }
#if (MDE_80211N_SUPPORT || MDE_80211AC_SUPPORT) 
        else if (!strcmp(value, "n_2_4G"))
        {
                nvalue = AL_CONF_IF_PHY_SUB_TYPE_802_11_N_2_4GHZ;
	    }
        else if (!strcmp(value, "n_5G"))
        {
                nvalue = AL_CONF_IF_PHY_SUB_TYPE_802_11_N_5GHZ;
	    }
        else if (!strcmp(value, "ac"))
        {
                nvalue = AL_CONF_IF_PHY_SUB_TYPE_802_11_AC;
        }
        else if (!strcmp(value, "bgn"))
        {
                nvalue = AL_CONF_IF_PHY_SUB_TYPE_802_11_BGN;
        }
        else if (!strcmp(value, "an"))
        {
                nvalue = AL_CONF_IF_PHY_SUB_TYPE_802_11_AN;
        }
        else if (!strcmp(value, "anac"))
        {
                nvalue = AL_CONF_IF_PHY_SUB_TYPE_802_11_ANAC;
	}
#endif
   else
   {
      return -4;
   }

   if_info->phy_sub_type = nvalue;

   fprintf(stderr, "Success[%s subtype=%d]\n", if_info->name, nvalue);
   if((if_info->phy_sub_type == AL_CONF_IF_PHY_SUB_TYPE_802_11_N_5GHZ) || (if_info->phy_sub_type == AL_CONF_IF_PHY_SUB_TYPE_802_11_BGN) ||
		   (if_info->phy_sub_type == AL_CONF_IF_PHY_SUB_TYPE_802_11_AN) ||(if_info->phy_sub_type == AL_CONF_IF_PHY_SUB_TYPE_802_11_N_2_4GHZ))
   {
	   if_info->ht_capab.ldpc = AL_CONF_IF_HT_PARAM_ENABLED;
	   if_info->ht_capab.ht_bandwidth = AL_CONF_IF_HT_PARAM_20;
	   if_info-> ht_capab.gfmode = AL_CONF_IF_HT_PARAM_DISABLED;
	   if_info->ht_capab.smps = AL_CONF_IF_HT_PARAM_STATIC;
	   if_info->ht_capab.gi_20 = AL_CONF_IF_HT_PARAM_SHORT;
	   if_info->ht_capab.gi_40 = AL_CONF_IF_HT_PARAM_SHORT;
	   if_info->ht_capab.tx_stbc = AL_CONF_IF_HT_PARAM_ENABLED;
	   if_info->ht_capab.rx_stbc = AL_CONF_IF_HT_PARAM_ENABLED;
	   if_info->ht_capab.delayed_ba = AL_CONF_IF_HT_PARAM_DISABLED;
	   if_info->ht_capab.max_amsdu_len = 0;
	   if_info->ht_capab.dsss_cck_40 = AL_CONF_IF_HT_PARAM_ALLOW;
	   if_info->ht_capab.intolerant = AL_CONF_IF_HT_PARAM_ENABLED;
	   if_info->ht_capab.lsig_txop = AL_CONF_IF_HT_PARAM_DISABLED;
	   if_info->frame_aggregation.ampdu_enable = 1;
	   if_info->frame_aggregation.max_ampdu_len = 64;
   }
   if((if_info->phy_sub_type == AL_CONF_IF_PHY_SUB_TYPE_802_11_AC) || (if_info->phy_sub_type == AL_CONF_IF_PHY_SUB_TYPE_802_11_ANAC))
   {
	   if_info->vht_capab.max_mpdu_len = 3895;
	   if_info->vht_capab.supported_channel_width = 0;
	   if_info->vht_capab.rx_ldpc = AL_CONF_IF_HT_PARAM_ENABLED;
	   if_info->vht_capab.gi_80 = AL_CONF_IF_HT_PARAM_SHORT;
	   if_info->vht_capab.gi_160 = AL_CONF_IF_HT_PARAM_LONG;
	   if_info->vht_capab.tx_stbc = AL_CONF_IF_HT_PARAM_DISABLED;
	   if_info->vht_capab.rx_stbc = AL_CONF_IF_HT_PARAM_DISABLED;
	   if_info->vht_capab.su_beamformer_cap = AL_CONF_IF_VHT_PARAM_NO;
	   if_info->vht_capab.su_beamformee_cap = AL_CONF_IF_VHT_PARAM_NO;
	   if_info->vht_capab.beamformee_sts_count = 1;
	   if_info->vht_capab.sounding_dimensions = 2;
	   if_info->vht_capab.mu_beamformer_cap = AL_CONF_IF_VHT_PARAM_NO;
	   if_info->vht_capab.mu_beamformee_cap = AL_CONF_IF_VHT_PARAM_NO;
	   if_info->vht_capab.vht_txop_ps = AL_CONF_IF_VHT_PARAM_NO;
	   if_info->vht_capab.htc_vht_cap = AL_CONF_IF_VHT_PARAM_NO;
	   if_info->vht_capab.rx_ant_pattern_consistency = AL_CONF_IF_VHT_PARAM_YES;
	   if_info->vht_capab.tx_ant_pattern_consistency = AL_CONF_IF_VHT_PARAM_YES;
	   if_info->vht_capab.vht_oper_bandwidth = 0;
	   if_info->vht_capab.seg0_center_freq = 0;
	   if_info->vht_capab.seg1_center_freq = 0;
   }

   return 0;
}


static int _set_medium_sub_type(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   int ret;

   ret = _if_iterator(argc, optind, argv, conf_handle, _sub_type_callback);

   if (ret == -4)
   {
		fprintf(stderr, "usage: alconfset subtype <<if-name>> <<value x,a,b,g,bg,psq,psh,psf,n_2_4G,n_5G,ac,bgn,an,anac>>\n");
   }


   return ret;
}


static int _use_type_callback(int argc, int optind, char **argv, char *value, int nvalue, al_conf_if_info_t *if_info)
{
   if (!strcmp(value, "ds"))
   {
      nvalue = AL_CONF_IF_USE_TYPE_DS;
   }
   else if (!strcmp(value, "wm"))
   {
      nvalue = AL_CONF_IF_USE_TYPE_WM;
   }
   else if (!strcmp(value, "ap"))
   {
      nvalue = AL_CONF_IF_USE_TYPE_AP;
   }
   else if (!strcmp(value, "pmon"))
   {
      nvalue = AL_CONF_IF_USE_TYPE_PMON;
   }
   else if (!strcmp(value, "amon"))
   {
      nvalue = AL_CONF_IF_USE_TYPE_AMON;
   }
   else
   {
      return -4;
   }

   if_info->use_type = nvalue;

   fprintf(stderr, "Success[%s usetype=%d]\n", if_info->name, nvalue);

   return 0;
}


static int _set_use_type(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   int ret;

   ret = _if_iterator(argc, optind, argv, conf_handle, _use_type_callback);

   if (ret == -4)
   {
      fprintf(stderr, "usage: alconfset usetype <<if-name>> <<value ds,wm,pmon,amon>>\n");
   }

   return ret;
}


static int _channel_callback(int argc, int optind, char **argv, char *value, int nvalue, al_conf_if_info_t *if_info)
{
   if (nvalue <= 0)
   {
      return -4;
   }

   if_info->wm_channel = nvalue;
   fprintf(stderr, "Success[%s channel=%d]\n", if_info->name, nvalue);

   return 0;
}


static int _set_channel(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   int ret;

   ret = _if_iterator(argc, optind, argv, conf_handle, _channel_callback);

   if (ret == -4)
   {
      fprintf(stderr, "usage: alconfset channel <<if-name>> <<channel>>\n");
   }

   return ret;
}


static int _essid_callback(int argc, int optind, char **argv, char *value, int nvalue, al_conf_if_info_t *if_info)
{
   strcpy(if_info->essid, value);
   fprintf(stderr, "Success[%s essid=%s]\n", if_info->name, value);

   return 0;
}


static int _set_essid(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   int ret;

   ret = _if_iterator(argc, optind, argv, conf_handle, _essid_callback);

   if (ret == -4)
   {
      fprintf(stderr, "usage: alconfset essid <<if-name>> <<essid>>\n");
   }

   return ret;
}


static int _rts_callback(int argc, int optind, char **argv, char *value, int nvalue, al_conf_if_info_t *if_info)
{
   if (nvalue <= 0)
   {
      return -4;
   }

   if_info->rts_th = nvalue;
   fprintf(stderr, "Success[%s rts=%d]\n", if_info->name, nvalue);

   return 0;
}


static int _set_rts(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   int ret;

   ret = _if_iterator(argc, optind, argv, conf_handle, _rts_callback);

   if (ret == -4)
   {
      fprintf(stderr, "usage: alconfset rts <<if-name>> <<rts>>\n");
   }

   return ret;
}


static int _frag_callback(int argc, int optind, char **argv, char *value, int nvalue, al_conf_if_info_t *if_info)
{
   if (nvalue <= 0)
   {
      return -4;
   }

   if_info->frag_th = nvalue;
   fprintf(stderr, "Success[%s frag=%d]\n", if_info->name, nvalue);

   return 0;
}


static int _set_frag(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   int ret;

   ret = _if_iterator(argc, optind, argv, conf_handle, _frag_callback);

   if (ret == -4)
   {
      fprintf(stderr, "usage: alconfset frag <<if-name>> <<frag>>\n");
   }

   return ret;
}


static int _beacint_callback(int argc, int optind, char **argv, char *value, int nvalue, al_conf_if_info_t *if_info)
{
   if (nvalue <= 0)
   {
      return -4;
   }

   if_info->beacon_interval = nvalue;
   fprintf(stderr, "Success[%s beacint=%d]\n", if_info->name, nvalue);
   return 0;
}


static int _set_beacint(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   int ret;

   ret = _if_iterator(argc, optind, argv, conf_handle, _beacint_callback);

   if (ret == -4)
   {
      fprintf(stderr, "usage: alconfset beacint <<if-name>> <<interval>>\n");
   }

   return ret;
}


static int _dca_callback(int argc, int optind, char **argv, char *value, int nvalue, al_conf_if_info_t *if_info)
{
   if ((nvalue < 0) || (nvalue > 1))
   {
      return -4;
   }

   if_info->dca = nvalue;

   fprintf(stderr, "Success[%s dca=%d]\n", if_info->name, nvalue);

   return 0;
}


static int _set_dca(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   int ret;

   ret = _if_iterator(argc, optind, argv, conf_handle, _dca_callback);

   if (ret == -4)
   {
      fprintf(stderr, "usage: alconfset dca <<if-name>> <<0-1>>\n");
   }

   return ret;
}


static int _dcalist_callback(int argc, int optind, char **argv, char *value, int nvalue, al_conf_if_info_t *if_info)
{
   int j;

   if ((nvalue <= 0) ||
       (argc <= optind + 2 + nvalue))
   {
      return -4;
   }

   if_info->dca_list_count = nvalue;

   for (j = 0; j < nvalue; j++)
   {
      if_info->dca_list[j] = atoi(argv[optind + 3 + j]);
   }

   return 0;
}


static int _set_dca_list(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   int ret;

   ret = _if_iterator(argc, optind, argv, conf_handle, _dcalist_callback);

   if (ret == -4)
   {
      fprintf(stderr, "usage: alconfset dcalist <<if-name>> <<count>> <<channels>>\n");
   }

   return ret;
}


static int _antport_callback(int argc, int optind, char **argv, char *value, int nvalue, al_conf_if_info_t *if_info)
{
   int rx_on_all;

   rx_on_all = 0;

   if (argc > optind + 2 + 1)
   {
      rx_on_all = atoi(argv[optind + 3]);

      if ((rx_on_all < 0) || (rx_on_all > 1))
      {
         return -5;
      }
   }

   if ((nvalue < 1) || (nvalue > 14))
   {
      return -4;
   }

   if_info->txant = nvalue;

   if (rx_on_all)
   {
      if_info->txant |= 0x10;
   }

   fprintf(stderr, "Success[%s txant=%d]\n", if_info->name, nvalue);

   return 0;
}


static int _set_ant_port(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   int ret;

   ret = _if_iterator(argc, optind, argv, conf_handle, _antport_callback);

   if (ret == -4)
   {
      fprintf(stderr, "usage: alconfset antport <<if-name>> <<1-14>> [1 for RX on all, 0 otherwise]\n");
   }

   return ret;
}


static int _txrate_callback(int argc, int optind, char **argv, char *value, int nvalue, al_conf_if_info_t *if_info)
{
   if (nvalue < 0)
   {
      return -4;
   }

   if_info->txrate = nvalue;
   fprintf(stderr, "Success[%s txrate=%d]\n", if_info->name, nvalue);

   return 0;
}


static int _set_txrate(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   int ret;

   ret = _if_iterator(argc, optind, argv, conf_handle, _txrate_callback);

   if (ret == -4)
   {
      fprintf(stderr, "usage: alconfset txrate <<if-name>> <<mbps/0=auto>>\n");
   }

   return ret;
}


static int _preamble_callback(int argc, int optind, char **argv, char *value, int nvalue, al_conf_if_info_t *if_info)
{
   if (!strcmp(value, "long"))
   {
      nvalue = AL_CONF_IF_PREAMBLE_TYPE_LONG;
   }
   else if (!strcmp(value, "short"))
   {
      nvalue = AL_CONF_IF_PREAMBLE_TYPE_SHORT;
   }
   else
   {
      return -4;
   }

   if_info->preamble_type = nvalue;
   fprintf(stderr, "Success[%s preamble=%d]\n", if_info->name, nvalue);

   return 0;
}


static int _set_preamble(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   int ret;

   ret = _if_iterator(argc, optind, argv, conf_handle, _preamble_callback);

   if (ret == -4)
   {
      fprintf(stderr, "usage: alconfset preamble <<if-name>> <<long or short>>\n");
   }

   return ret;
}


#if MDE_80211N_SUPPORT 

static int _ldpc_callback(int argc, int optind, char **argv, char *value, int nvalue, al_conf_if_info_t *if_info)
{
   if (!strcmp(value, "enabled"))
   {
      nvalue = AL_CONF_IF_HT_PARAM_ENABLED;
   }
   else if (!strcmp(value, "disabled"))
   {
      nvalue = AL_CONF_IF_HT_PARAM_DISABLED;
   }
   else
   {
      return -4;
   }

   if_info->ht_capab.ldpc = nvalue;
   fprintf(stderr, "Success[%s ldpc=%d]\n", if_info->name, nvalue);

   return 0;
}


static int _set_ldpc(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   int ret;

   ret = _if_iterator(argc, optind, argv, conf_handle, _ldpc_callback);

   if (ret == -4)
   {
      fprintf(stderr, "usage: alconfset ldpc <<if-name>> <<enabled or disabled>>\n");
   }

   return ret;
}


static int _smps_callback(int argc, int optind, char **argv, char *value, int nvalue, al_conf_if_info_t *if_info)
{
   if (!strcmp(value, "disabled"))
   {
      nvalue = AL_CONF_IF_HT_PARAM_SMPS_DISABLED;
   }
   else if (!strcmp(value, "static"))
   {
      nvalue = AL_CONF_IF_HT_PARAM_STATIC;
   }
   else if (!strcmp(value, "dynamic"))
   {
      nvalue = AL_CONF_IF_HT_PARAM_DYNAMIC;
   }
   else
   {
      return -4;
   }

   if_info->ht_capab.smps = nvalue;
   fprintf(stderr, "Success[%s smps=%d]\n", if_info->name, nvalue);

   return 0;
}


static int _set_smps(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   int ret;

   ret = _if_iterator(argc, optind, argv, conf_handle, _smps_callback);

   if (ret == -4)
   {
      fprintf(stderr, "usage: alconfset smps <<if-name>> <<disabled or static or dynamic>>\n");
   }

   return ret;
}


static int _tx_stbc_callback(int argc, int optind, char **argv, char *value, int nvalue, al_conf_if_info_t *if_info)
{
   if (!strcmp(value, "enabled"))
   {
      nvalue = AL_CONF_IF_HT_PARAM_ENABLED;
   }
   else if (!strcmp(value, "disabled"))
   {
      nvalue = AL_CONF_IF_HT_PARAM_DISABLED;
   }
   else
   {
      return -4;
   }

   if_info->ht_capab.tx_stbc = nvalue;
   fprintf(stderr, "Success[%s tx_stbc=%d]\n", if_info->name, nvalue);

   return 0;
}


static int _set_tx_stbc(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   int ret;

   ret = _if_iterator(argc, optind, argv, conf_handle, _tx_stbc_callback);

   if (ret == -4)
   {
      fprintf(stderr, "usage: alconfset tx_stbc <<if-name>> <<enabled or disabled>>\n");
   }

   return ret;
}


static int _rx_stbc_callback(int argc, int optind, char **argv, char *value, int nvalue, al_conf_if_info_t *if_info)
{
   if (!strcmp(value, "enabled"))
   {
      nvalue = AL_CONF_IF_HT_PARAM_ENABLED;
   }
   else if (!strcmp(value, "disabled"))
   {
      nvalue = AL_CONF_IF_HT_PARAM_DISABLED;
   }
   else
   {
      return -4;
   }

   if_info->ht_capab.rx_stbc = nvalue;
   fprintf(stderr, "Success[%s rx_stbc=%d]\n", if_info->name, nvalue);

   return 0;
}


static int _set_rx_stbc(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   int ret;

   ret = _if_iterator(argc, optind, argv, conf_handle, _rx_stbc_callback);

   if (ret == -4)
   {
      fprintf(stderr, "usage: alconfset rx_stbc <<if-name>> <<enabled or disabled>>\n");
   }

   return ret;
}


static int _delayed_ba_callback(int argc, int optind, char **argv, char *value, int nvalue, al_conf_if_info_t *if_info)
{
   if (!strcmp(value, "enabled"))
   {
      nvalue = AL_CONF_IF_HT_PARAM_ENABLED;
   }
   else if (!strcmp(value, "disabled"))
   {
      nvalue = AL_CONF_IF_HT_PARAM_DISABLED;
   }
   else
   {
      return -4;
   }

   if_info->ht_capab.delayed_ba = nvalue;
   fprintf(stderr, "Success[%s delayed_ba=%d]\n", if_info->name, nvalue);

   return 0;
}


static int _set_delayed_ba(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   int ret;

   ret = _if_iterator(argc, optind, argv, conf_handle, _delayed_ba_callback);

   if (ret == -4)
   {
      fprintf(stderr, "usage: alconfset delayed_ba <<if-name>> <<enabled or disabled>>\n");
   }

   return ret;
}


static int _intolerant_callback(int argc, int optind, char **argv, char *value, int nvalue, al_conf_if_info_t *if_info)
{
   if (!strcmp(value, "enabled"))
   {
      nvalue = AL_CONF_IF_HT_PARAM_ENABLED;
   }
   else if (!strcmp(value, "disabled"))
   {
      nvalue = AL_CONF_IF_HT_PARAM_DISABLED;
   }
   else
   {
      return -4;
   }

   if_info->ht_capab.intolerant = nvalue;
   fprintf(stderr, "Success[%s intolerant=%d]\n", if_info->name, nvalue);

   return 0;
}


static int _set_intolerant(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   int ret;

   ret = _if_iterator(argc, optind, argv, conf_handle, _intolerant_callback);

   if (ret == -4)
   {
      fprintf(stderr, "usage: alconfset intolerant <<if-name>> <<enabled or disabled>>\n");
   }

   return ret;
}


static int _lsig_txop_callback(int argc, int optind, char **argv, char *value, int nvalue, al_conf_if_info_t *if_info)
{
   if (!strcmp(value, "enabled"))
   {
      nvalue = AL_CONF_IF_HT_PARAM_ENABLED;
   }
   else if (!strcmp(value, "disabled"))
   {
      nvalue = AL_CONF_IF_HT_PARAM_DISABLED;
   }
   else
   {
      return -4;
   }

   if_info->ht_capab.lsig_txop = nvalue;
   fprintf(stderr, "Success[%s lsig_txop=%d]\n", if_info->name, nvalue);

   return 0;
}


static int _set_lsig_txop(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   int ret;

   ret = _if_iterator(argc, optind, argv, conf_handle, _lsig_txop_callback);

   if (ret == -4)
   {
      fprintf(stderr, "usage: alconfset lsig_txop <<if-name>> <<enabled or disabled>>\n");
   }

   return ret;
}


static int _gfmode_callback(int argc, int optind, char **argv, char *value, int nvalue, al_conf_if_info_t *if_info)
{
   if (!strcmp(value, "enabled"))
   {
      nvalue = AL_CONF_IF_HT_PARAM_ENABLED;
   }
   else if (!strcmp(value, "disabled"))
   {
      nvalue = AL_CONF_IF_HT_PARAM_DISABLED;
   }
   else
   {
      return -4;
   }

   if_info->ht_capab.gfmode = nvalue;
   fprintf(stderr, "Success[%s gfmode=%d]\n", if_info->name, nvalue);

   return 0;
}


static int _set_gfmode(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   int ret;

   ret = _if_iterator(argc, optind, argv, conf_handle, _gfmode_callback);

   if (ret == -4)
   {
      fprintf(stderr, "usage: alconfset gfmode <<if-name>> <<enabled or disabled>>\n");
   }

   return ret;
}


static int _gi_20_callback(int argc, int optind, char **argv, char *value, int nvalue, al_conf_if_info_t *if_info)
{
   if (!strcmp(value, "long"))
   {
      nvalue = AL_CONF_IF_HT_PARAM_LONG;
   }
   else if (!strcmp(value, "auto"))
   {
      nvalue = AL_CONF_IF_HT_PARAM_AUTO;
   }
   else if (!strcmp(value, "short"))
   {
      nvalue = AL_CONF_IF_HT_PARAM_SHORT;
   }
   else
   {
      return -4;
   }

   if_info->ht_capab.gi_20 = nvalue;
   fprintf(stderr, "Success[%s gi_20=%d]\n", if_info->name, nvalue);

   return 0;
}


static int _set_gi_20(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   int ret;

   ret = _if_iterator(argc, optind, argv, conf_handle, _gi_20_callback);

   if (ret == -4)
   {
      fprintf(stderr, "usage: alconfset gi_20 <<if-name>> <<long or auto or short>>\n");
   }

   return ret;
}


static int _gi_40_callback(int argc, int optind, char **argv, char *value, int nvalue, al_conf_if_info_t *if_info)
{
   if (!strcmp(value, "long"))
   {
      nvalue = AL_CONF_IF_HT_PARAM_LONG;
   }
   else if (!strcmp(value, "auto"))
   {
      nvalue = AL_CONF_IF_HT_PARAM_AUTO;
   }
   else if (!strcmp(value, "short"))
   {
      nvalue = AL_CONF_IF_HT_PARAM_SHORT;
   }
   else
   {
      return -4;
   }

   if_info->ht_capab.gi_40 = nvalue;
   fprintf(stderr, "Success[%s gi_40=%d]\n", if_info->name, nvalue);

   return 0;
}


static int _set_gi_40(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   int ret;

   ret = _if_iterator(argc, optind, argv, conf_handle, _gi_40_callback);

   if (ret == -4)
   {
      fprintf(stderr, "usage: alconfset gi_40 <<if-name>> <<long or auto or short>>\n");
   }

   return ret;
}


static int _dsss_cck_40_callback(int argc, int optind, char **argv, char *value, int nvalue, al_conf_if_info_t *if_info)
{
   if (!strcmp(value, "allow"))
   {
      nvalue = AL_CONF_IF_HT_PARAM_ALLOW;
   }
   else if (!strcmp(value, "deny"))
   {
      nvalue = AL_CONF_IF_HT_PARAM_DENY;
   }
   else
   {
      return -4;
   }

   if_info->ht_capab.dsss_cck_40 = nvalue;
   fprintf(stderr, "Success[%s dsss_cck_40=%d]\n", if_info->name, nvalue);

   return 0;
}


static int _set_dsss_cck_40(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   int ret;

   ret = _if_iterator(argc, optind, argv, conf_handle, _dsss_cck_40_callback);

   if (ret == -4)
   {
      fprintf(stderr, "usage: alconfset dsss_cck_40 <<if-name>> <<allow or deny>>\n");
   }

   return ret;
}


static int _ht_bandwidth_callback(int argc, int optind, char **argv, char *value, int nvalue, al_conf_if_info_t *if_info)
{
   if (!strcmp(value, "40+"))
   {
      nvalue = AL_CONF_IF_HT_PARAM_40_ABOVE;
   }
   else if (!strcmp(value, "40-"))
   {
      nvalue = AL_CONF_IF_HT_PARAM_40_BELOW;
   }
   else if (!strcmp(value, "20"))
   {
      nvalue = AL_CONF_IF_HT_PARAM_20;
   }
   else
   {
      return -4;
   }

   if_info->ht_capab.ht_bandwidth = nvalue;
   fprintf(stderr, "Success[%s ht_bandwidth=%d]\n", if_info->name, nvalue);

   return 0;
}


static int _set_ht_bandwidth(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   int ret;

   ret = _if_iterator(argc, optind, argv, conf_handle, _ht_bandwidth_callback);

   if (ret == -4)
   {
      fprintf(stderr, "usage: alconfset ht_bandwidth <<if-name>> <<Bandwidth>>\n");
   }

   return ret;
}


static int _max_amsdu_len_callback(int argc, int optind, char **argv, char *value, int nvalue, al_conf_if_info_t *if_info)
{
   if (nvalue < 0)
   {
      return -4;
   }

   if ((nvalue == 1) || (nvalue == 0))
   {
      if_info->ht_capab.max_amsdu_len = nvalue;
      fprintf(stderr, "Success[%s max_amsdu_len=%d]\n", if_info->name, nvalue);
   }
   else
   {
      if_info->ht_capab.max_amsdu_len = 0;
      fprintf(stderr, "usage: alconfset max_amsdu_len <<if-name>> << 0=>3839 / 1=>7935>>\n");
   }
   return 0;
}


static int _set_max_amsdu_len(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   int ret;

   ret = _if_iterator(argc, optind, argv, conf_handle, _max_amsdu_len_callback);

   if (ret == -4)
   {
      fprintf(stderr, "usage: alconfset max_amsdu_len <<if-name>> <<max_amsdu_len integer: 0=>3839/1=>7935>>\n");
   }

   return ret;
}


static int _ampdu_enable_callback(int argc, int optind, char **argv, char *value, int nvalue, al_conf_if_info_t *if_info)
{
   if (nvalue < 0)
   {
      return -4;
   }
   else if ((nvalue == 0) || (nvalue == 1))
   {
      if_info->frame_aggregation.ampdu_enable = nvalue;
      fprintf(stderr, "Success[%s ampdu_enable=%d]\n", if_info->name, nvalue);
   }
   else
   {
      fprintf(stderr, "usage: alconfset ampdu_enable <<if-name>> << 0 or 1 >>\n");
   }

   return 0;
}


static int _set_ampdu_enable(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   int ret;

   ret = _if_iterator(argc, optind, argv, conf_handle, _ampdu_enable_callback);

   if (ret == -4)
   {
      fprintf(stderr, "usage: alconfset ampdu_enable <<if-name>> <<ampdu_enable integer: 0 or 1>>\n");
   }

   return ret;
}


static int _max_ampdu_len_callback(int argc, int optind, char **argv, char *value, int nvalue, al_conf_if_info_t *if_info)
{
   if (nvalue < 0)
   {
      return -4;
   }

   if ((nvalue == 8) || (nvalue == 16) || (nvalue == 32) || (nvalue == 64))
   {
      if_info->frame_aggregation.max_ampdu_len = nvalue;
      fprintf(stderr, "Success[%s max_ampdu_len=%d]\n", if_info->name, nvalue);
   }
   else
   {
      fprintf(stderr, "usage:  alconfset max_ampdu_len <<if-name>> <<valid integer 8,16,32,or 64>>\n");
   }

   return 0;
}


static int _set_max_ampdu_len(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   int ret;

   ret = _if_iterator(argc, optind, argv, conf_handle, _max_ampdu_len_callback);

   if (ret == -4)
   {
      fprintf(stderr, "usage: alconfset max_ampdu_len <<if-name>> <<eg: 64KB>>\n");
   }

   return ret;
}
#endif

#if MDE_80211AC_SUPPORT     

static int _max_mpdu_len_callback(int argc, int optind, char **argv, char *value, int nvalue, al_conf_if_info_t *if_info)
{
   if (nvalue < 0)
   {
      return -4;
   }
   if ((nvalue == 3895) || (nvalue == 7991) || (nvalue == 11454))
   {
      if_info->vht_capab.max_mpdu_len = nvalue;
      fprintf(stderr, "Success[%s max_mpdu_len=%d]\n", if_info->name, nvalue);
   }
   else
   {
      fprintf(stderr, "usage:  alconfset max_mpdu_len <<if-name>> <<valid integer 3895, 7991 or 11454>>\n");
   }

   return 0;
}


static int _set_max_mpdu_len(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   int ret;

   ret = _if_iterator(argc, optind, argv, conf_handle, _max_mpdu_len_callback);

   if (ret == -4)
   {
      fprintf(stderr, "usage: alconfset max_mpdu_len <<if-name>> <<max_mpdu_len integer value>>\n");
   }

   return ret;
}


static int _supported_channel_width_callback(int argc, int optind, char **argv, char *value, int nvalue, al_conf_if_info_t *if_info)
{
   if (nvalue < 0)
   {
      return -4;
   }

   if ((nvalue == 0) || (nvalue == 1) || (nvalue == 2))
   {
      if_info->vht_capab.supported_channel_width = nvalue;
      fprintf(stderr, "Success[%s supported_channel_width=%d]\n", if_info->name, nvalue);
   }
   else
   {
      fprintf(stderr, "usage:  alconfset supported_channel_width <<if-name>> <<valid integer 0, 1 or 2>>\n");
   }
   return 0;
}


static int _set_supported_channel_width(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   int ret;

   ret = _if_iterator(argc, optind, argv, conf_handle, _supported_channel_width_callback);

   if (ret == -4)
   {
      fprintf(stderr, "usage: alconfset supported_channel_width <<if-name>> <<supported_channel_width integer value>>\n");
   }

   return ret;
}


static int _rx_ldpc_callback(int argc, int optind, char **argv, char *value, int nvalue, al_conf_if_info_t *if_info)
{
   if (!strcmp(value, "enabled"))
   {
      nvalue = AL_CONF_IF_HT_PARAM_ENABLED;
   }
   else if (!strcmp(value, "disabled"))
   {
      nvalue = AL_CONF_IF_HT_PARAM_DISABLED;
   }
   else
   {
      return -4;
   }

   if_info->vht_capab.rx_ldpc = nvalue;
   fprintf(stderr, "Success[%s rx_ldpc=%d]\n", if_info->name, nvalue);

   return 0;
}


static int _set_rx_ldpc(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   int ret;

   ret = _if_iterator(argc, optind, argv, conf_handle, _rx_ldpc_callback);

   if (ret == -4)
   {
      fprintf(stderr, "usage: alconfset rx_ldpc <<if-name>> <<enabled or disabled>>\n");
   }

   return ret;
}


static int _gi_80_callback(int argc, int optind, char **argv, char *value, int nvalue, al_conf_if_info_t *if_info)
{
   if (!strcmp(value, "long"))
   {
      nvalue = AL_CONF_IF_HT_PARAM_LONG;
   }
   else if (!strcmp(value, "auto"))
   {
      nvalue = AL_CONF_IF_HT_PARAM_AUTO;
   }
   else if (!strcmp(value, "short"))
   {
      nvalue = AL_CONF_IF_HT_PARAM_SHORT;
   }
   else
   {
      return -4;
   }

   if_info->vht_capab.gi_80 = nvalue;
   fprintf(stderr, "Success[%s gi_80=%d]\n", if_info->name, nvalue);

   return 0;
}


static int _set_gi_80(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   int ret;

   ret = _if_iterator(argc, optind, argv, conf_handle, _gi_80_callback);

   if (ret == -4)
   {
      fprintf(stderr, "usage: alconfset gi_80 <<if-name>> <<long or auto or short>>\n");
   }

   return ret;
}


static int _gi_160_callback(int argc, int optind, char **argv, char *value, int nvalue, al_conf_if_info_t *if_info)
{
   if (!strcmp(value, "long"))
   {
      nvalue = AL_CONF_IF_HT_PARAM_LONG;
   }
   else if (!strcmp(value, "auto"))
   {
      nvalue = AL_CONF_IF_HT_PARAM_AUTO;
   }
   else if (!strcmp(value, "short"))
   {
      nvalue = AL_CONF_IF_HT_PARAM_SHORT;
   }
   else
   {
      return -4;
   }

   if_info->vht_capab.gi_160 = nvalue;
   fprintf(stderr, "Success[%s gi_160=%d]\n", if_info->name, nvalue);

   return 0;
}


static int _set_gi_160(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   int ret;

   ret = _if_iterator(argc, optind, argv, conf_handle, _gi_160_callback);

   if (ret == -4)
   {
      fprintf(stderr, "usage: alconfset gi_160 <<if-name>> <<long or auto or short>>\n");
   }

   return ret;
}


static int _vtx_stbc_callback(int argc, int optind, char **argv, char *value, int nvalue, al_conf_if_info_t *if_info)
{
   if (!strcmp(value, "enabled"))
   {
      nvalue = AL_CONF_IF_HT_PARAM_ENABLED;
   }
   else if (!strcmp(value, "disabled"))
   {
      nvalue = AL_CONF_IF_HT_PARAM_DISABLED;
   }
   else
   {
      return -4;
   }

   if_info->vht_capab.tx_stbc = nvalue;
   fprintf(stderr, "Success[%s vtx_stbc=%d]\n", if_info->name, nvalue);

   return 0;
}


static int _set_vtx_stbc(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   int ret;

   ret = _if_iterator(argc, optind, argv, conf_handle, _vtx_stbc_callback);

   if (ret == -4)
   {
      fprintf(stderr, "usage: alconfset vtx_stbc <<if-name>> <<enabled or disabled>>\n");
   }

   return ret;
}


static int _vrx_stbc_callback(int argc, int optind, char **argv, char *value, int nvalue, al_conf_if_info_t *if_info)
{
   if (!strcmp(value, "enabled"))
   {
      nvalue = AL_CONF_IF_HT_PARAM_ENABLED;
   }
   else if (!strcmp(value, "disabled"))
   {
      nvalue = AL_CONF_IF_HT_PARAM_DISABLED;
   }
   else
   {
      return -4;
   }

   if_info->vht_capab.rx_stbc = nvalue;
   fprintf(stderr, "Success[%s vrx_stbc=%d]\n", if_info->name, nvalue);

   return 0;
}


static int _set_vrx_stbc(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   int ret;

   ret = _if_iterator(argc, optind, argv, conf_handle, _vrx_stbc_callback);

   if (ret == -4)
   {
      fprintf(stderr, "usage: alconfset vrx_stbc <<if-name>> <<enabled or disabled>>\n");
   }

   return ret;
}


static int _su_beamformer_cap_callback(int argc, int optind, char **argv, char *value, int nvalue, al_conf_if_info_t *if_info)
{
   if (!strcmp(value, "yes"))
   {
      nvalue = AL_CONF_IF_VHT_PARAM_YES;
   }
   else if (!strcmp(value, "no"))
   {
      nvalue = AL_CONF_IF_VHT_PARAM_NO;
   }
   else
   {
      return -4;
   }

   if_info->vht_capab.su_beamformer_cap = nvalue;
   fprintf(stderr, "Success[%s su_beamformer_cap=%d]\n", if_info->name, nvalue);

   return 0;
}


static int _set_su_beamformer_cap(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   int ret;

   ret = _if_iterator(argc, optind, argv, conf_handle, _su_beamformer_cap_callback);

   if (ret == -4)
   {
      fprintf(stderr, "usage: alconfset su_beamformer_cap <<if-name>> <<yes or no>>\n");
   }

   return ret;
}


static int _su_beamformee_cap_callback(int argc, int optind, char **argv, char *value, int nvalue, al_conf_if_info_t *if_info)
{
   if (!strcmp(value, "yes"))
   {
      nvalue = AL_CONF_IF_VHT_PARAM_YES;
   }
   else if (!strcmp(value, "no"))
   {
      nvalue = AL_CONF_IF_VHT_PARAM_NO;
   }
   else
   {
      return -4;
   }

   if_info->vht_capab.su_beamformee_cap = nvalue;
   fprintf(stderr, "Success[%s su_beamformee_cap=%d]\n", if_info->name, nvalue);

   return 0;
}


static int _set_su_beamformee_cap(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   int ret;

   ret = _if_iterator(argc, optind, argv, conf_handle, _su_beamformee_cap_callback);

   if (ret == -4)
   {
      fprintf(stderr, "usage: alconfset su_beamformee_cap <<if-name>> <<yes or no>>\n");
   }

   return ret;
}


static int _beamformee_sts_count_callback(int argc, int optind, char **argv, char *value, int nvalue, al_conf_if_info_t *if_info)
{
   if (nvalue < 0)
   {
      return -4;
   }
   if ((nvalue == 1) || (nvalue == 2) || (nvalue == 3))
   {
      if_info->vht_capab.beamformee_sts_count = nvalue;
      fprintf(stderr, "Success[%s beamformee_sts_count=%d]\n", if_info->name, nvalue);
   }
   else
   {
      fprintf(stderr, "usage:  alconfset beamformee_sts_count <<if-name>> <<valid integer 1, 2 or 3>>\n");
   }

   return 0;
}


static int _set_beamformee_sts_count(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   int ret;

   ret = _if_iterator(argc, optind, argv, conf_handle, _beamformee_sts_count_callback);

   if (ret == -4)
   {
      fprintf(stderr, "usage: alconfset beamformee_sts_count <<if-name>> <<beamformee_sts_count integer value>>\n");
   }

   return ret;
}


static int _sounding_dimensions_callback(int argc, int optind, char **argv, char *value, int nvalue, al_conf_if_info_t *if_info)
{
   if (nvalue < 0)
   {
      return -4;
   }
   if ((nvalue == 1) || (nvalue == 2) || (nvalue == 3))
   {
      if_info->vht_capab.sounding_dimensions = nvalue;
      fprintf(stderr, "Success[%s sounding_dimensions=%d]\n", if_info->name, nvalue);
   }
   else
   {
      fprintf(stderr, "usage:  alconfset sounding_dimensions <<if-name>> <<valid integer 1, 2 or 3>>\n");
   }

   return 0;
}


static int _set_sounding_dimensions(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   int ret;

   ret = _if_iterator(argc, optind, argv, conf_handle, _sounding_dimensions_callback);

   if (ret == -4)
   {
      fprintf(stderr, "usage: alconfset sounding_dimensions <<if-name>> <<sounding_dimensions integer value>>\n");
   }

   return ret;
}


static int _mu_beamformer_cap_callback(int argc, int optind, char **argv, char *value, int nvalue, al_conf_if_info_t *if_info)
{
   if (!strcmp(value, "yes"))
   {
      nvalue = AL_CONF_IF_VHT_PARAM_YES;
   }
   else if (!strcmp(value, "no"))
   {
      nvalue = AL_CONF_IF_VHT_PARAM_NO;
   }
   else
   {
      return -4;
   }

   if_info->vht_capab.mu_beamformer_cap = nvalue;
   fprintf(stderr, "Success[%s mu_beamformer_cap=%d]\n", if_info->name, nvalue);

   return 0;
}


static int _set_mu_beamformer_cap(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   int ret;

   ret = _if_iterator(argc, optind, argv, conf_handle, _mu_beamformer_cap_callback);

   if (ret == -4)
   {
      fprintf(stderr, "usage: alconfset mu_beamformer_cap <<if-name>> <<yes or no>>\n");
   }

   return ret;
}


static int _mu_beamformee_cap_callback(int argc, int optind, char **argv, char *value, int nvalue, al_conf_if_info_t *if_info)
{
   if (!strcmp(value, "yes"))
   {
      nvalue = AL_CONF_IF_VHT_PARAM_YES;
   }
   else if (!strcmp(value, "no"))
   {
      nvalue = AL_CONF_IF_VHT_PARAM_NO;
   }
   else
   {
      return -4;
   }

   if_info->vht_capab.mu_beamformee_cap = nvalue;
   fprintf(stderr, "Success[%s mu_beamformee_cap=%d]\n", if_info->name, nvalue);

   return 0;
}


static int _set_mu_beamformee_cap(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   int ret;

   ret = _if_iterator(argc, optind, argv, conf_handle, _mu_beamformee_cap_callback);

   if (ret == -4)
   {
      fprintf(stderr, "usage: alconfset mu_beamformee_cap <<if-name>> <<yes or no>>\n");
   }

   return ret;
}


static int _vht_txop_ps_callback(int argc, int optind, char **argv, char *value, int nvalue, al_conf_if_info_t *if_info)
{
   if (!strcmp(value, "yes"))
   {
      nvalue = AL_CONF_IF_VHT_PARAM_YES;
   }
   else if (!strcmp(value, "no"))
   {
      nvalue = AL_CONF_IF_VHT_PARAM_NO;
   }
   else
   {
      return -4;
   }

   if_info->vht_capab.vht_txop_ps = nvalue;
   fprintf(stderr, "Success[%s vht_txop_ps=%d]\n", if_info->name, nvalue);

   return 0;
}


static int _set_vht_txop_ps(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   int ret;

   ret = _if_iterator(argc, optind, argv, conf_handle, _vht_txop_ps_callback);

   if (ret == -4)
   {
      fprintf(stderr, "usage: alconfset vht_txop_ps <<if-name>> <<yes or no>>\n");
   }

   return ret;
}


static int _htc_vht_cap_callback(int argc, int optind, char **argv, char *value, int nvalue, al_conf_if_info_t *if_info)
{
   if (!strcmp(value, "yes"))
   {
      nvalue = AL_CONF_IF_VHT_PARAM_YES;
   }
   else if (!strcmp(value, "no"))
   {
      nvalue = AL_CONF_IF_VHT_PARAM_NO;
   }
   else
   {
      return -4;
   }

   if_info->vht_capab.htc_vht_cap = nvalue;
   fprintf(stderr, "Success[%s htc_vht_cap=%d]\n", if_info->name, nvalue);

   return 0;
}


static int _set_htc_vht_cap(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   int ret;

   ret = _if_iterator(argc, optind, argv, conf_handle, _htc_vht_cap_callback);

   if (ret == -4)
   {
      fprintf(stderr, "usage: alconfset htc_vht_cap <<if-name>> <<yes or no>>\n");
   }

   return ret;
}


static int _rx_ant_pattern_consistency_callback(int argc, int optind, char **argv, char *value, int nvalue, al_conf_if_info_t *if_info)
{
   if (!strcmp(value, "yes"))
   {
      nvalue = AL_CONF_IF_VHT_PARAM_YES;
   }
   else if (!strcmp(value, "no"))
   {
      nvalue = AL_CONF_IF_VHT_PARAM_NO;
   }
   else
   {
      return -4;
   }

   if_info->vht_capab.rx_ant_pattern_consistency = nvalue;
   fprintf(stderr, "Success[%s rx_ant_pattern_consistency=%d]\n", if_info->name, nvalue);

   return 0;
}


static int _set_rx_ant_pattern_consistency(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   int ret;

   ret = _if_iterator(argc, optind, argv, conf_handle, _rx_ant_pattern_consistency_callback);

   if (ret == -4)
   {
      fprintf(stderr, "usage: alconfset rx_ant_pattern_consistency <<if-name>> <<yes or no>>\n");
   }

   return ret;
}


static int _tx_ant_pattern_consistency_callback(int argc, int optind, char **argv, char *value, int nvalue, al_conf_if_info_t *if_info)
{
   if (!strcmp(value, "yes"))
   {
      nvalue = AL_CONF_IF_VHT_PARAM_YES;
   }
   else if (!strcmp(value, "no"))
   {
      nvalue = AL_CONF_IF_VHT_PARAM_NO;
   }
   else
   {
      return -4;
   }

   if_info->vht_capab.tx_ant_pattern_consistency = nvalue;
   fprintf(stderr, "Success[%s tx_ant_pattern_consistency=%d]\n", if_info->name, nvalue);

   return 0;
}


static int _set_tx_ant_pattern_consistency(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   int ret;

   ret = _if_iterator(argc, optind, argv, conf_handle, _tx_ant_pattern_consistency_callback);

   if (ret == -4)
   {
      fprintf(stderr, "usage: alconfset tx_ant_pattern_consistency <<if-name>> <<yes or no>>\n");
   }

   return ret;
}


static int _vht_oper_bandwidth_callback(int argc, int optind, char **argv, char *value, int nvalue, al_conf_if_info_t *if_info)
{
   if (nvalue < 0)
   {
      return -4;
   }
   if ((nvalue == 0) || (nvalue == 1) || (nvalue == 2) || (nvalue == 3))
   {
      if_info->vht_capab.vht_oper_bandwidth = nvalue;
      fprintf(stderr, "Success[%s vht_oper_bandwidth=%d]\n", if_info->name, nvalue);
   }
   else
   {
      fprintf(stderr, "usage:  alconfset vht_oper_bandwidth <<if-name>> <<valid integer 0,1, 2 or 3>>\n");
   }
   return 0;
}


static int _set_vht_oper_bandwidth(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   int ret;

   ret = _if_iterator(argc, optind, argv, conf_handle, _vht_oper_bandwidth_callback);

   if (ret == -4)
   {
      fprintf(stderr, "usage: alconfset vht_oper_bandwidth <<if-name>> <<supported_channel_width integer value>>\n");
   }

   return ret;
}


static int _seg0_center_freq_callback(int argc, int optind, char **argv, char *value, int nvalue, al_conf_if_info_t *if_info)
{
   if (nvalue < 0)
   {
      return -4;
   }
   if ((nvalue == 0) || (nvalue == 42) || (nvalue == 58) || (nvalue == 155))
   {
      if_info->vht_capab.seg0_center_freq = nvalue;
      fprintf(stderr, "Success[%s seg0_center_freq=%d]\n", if_info->name, nvalue);
   }
   else
   {
      fprintf(stderr, "usage:  alconfset seg0_center_freq <<if-name>> <<valid integer 0,42, 58 or 155>>\n");
   }
   return 0;
}


static int _set_seg0_center_freq(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   int ret;

   ret = _if_iterator(argc, optind, argv, conf_handle, _seg0_center_freq_callback);

   if (ret == -4)
   {
      fprintf(stderr, "usage: alconfset seg0_center_freq <<if-name>> <<supported_channel_width integer value>>\n");
   }

   return ret;
}


static int _seg1_center_freq_callback(int argc, int optind, char **argv, char *value, int nvalue, al_conf_if_info_t *if_info)
{
   if (nvalue < 0)
   {
      return -4;
   }
   if ((nvalue == 0) || (nvalue == 42) || (nvalue == 58) || (nvalue == 155))
   {
      if_info->vht_capab.seg1_center_freq = nvalue;
      fprintf(stderr, "Success[%s seg1_center_freq=%d]\n", if_info->name, nvalue);
   }
   else
   {
      fprintf(stderr, "usage:  alconfset seg1_center_freq <<if-name>> <<valid integer 0,42, 58 or 155>>\n");
   }
   return 0;
}


static int _set_seg1_center_freq(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   int ret;

   ret = _if_iterator(argc, optind, argv, conf_handle, _seg1_center_freq_callback);

   if (ret == -4)
   {
      fprintf(stderr, "usage: alconfset seg1_center_freq <<if-name>> <<supported_channel_width integer value>>\n");
   }

   return ret;
}
#endif

static int _slot_callback(int argc, int optind, char **argv, char *value, int nvalue, al_conf_if_info_t *if_info)
{
   if (!strcmp(value, "long"))
   {
      nvalue = AL_CONF_IF_SLOT_TIME_TYPE_LONG;
   }
   else if (!strcmp(value, "short"))
   {
      nvalue = AL_CONF_IF_SLOT_TIME_TYPE_SHORT;
   }
   else
   {
      return -4;
   }

   if_info->slot_time_type = nvalue;
   fprintf(stderr, "Success[%s slot=%d]\n", if_info->name, nvalue);

   return 0;
}


static int _set_slot(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   int ret;

   ret = _if_iterator(argc, optind, argv, conf_handle, _slot_callback);

   if (ret == -4)
   {
      fprintf(stderr, "usage: alconfset slot <<if-name>> <<long or short>>\n");
   }

   return ret;
}


static int _linkopt_callback(int argc, int optind, char **argv, char *value, int nvalue, al_conf_if_info_t *if_info)
{
   if (!strcmp(value, "backhaul"))
   {
      nvalue = AL_CONF_IF_SERVICE_BACKHAUL_ONLY;
   }
   else if (!strcmp(value, "client"))
   {
      nvalue = AL_CONF_IF_SERVICE_CLIENT_ONLY;
   }
   else if (!strcmp(value, "all"))
   {
      nvalue = AL_CONF_IF_SERVICE_ALL;
   }
   else
   {
      return -4;
   }

   if_info->service = nvalue;
   fprintf(stderr, "Success[%s linkopt=%d]\n", if_info->name, nvalue);

   return 0;
}


static int _set_link_opt(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   int ret;

   ret = _if_iterator(argc, optind, argv, conf_handle, _linkopt_callback);

   if (ret == -4)
   {
      fprintf(stderr, "usage: alconfset linkopt <<if-name>> <<all/backhaul/client>>\n");
   }

   return ret;
}


static int _acktime_callback(int argc, int optind, char **argv, char *value, int nvalue, al_conf_if_info_t *if_info)
{
   if (nvalue <= 0)
   {
      return -4;
   }

   if_info->ack_timeout = nvalue;
   fprintf(stderr, "Success[%s acktime=%d]\n", if_info->name, nvalue);

   return 0;
}


static int _set_ack_time(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   int ret;

   ret = _if_iterator(argc, optind, argv, conf_handle, _acktime_callback);

   if (ret == -4)
   {
      fprintf(stderr, "usage: alconfset acktime <<if-name>> <<µs>>\n");
   }

   return ret;
}


static int _hidessid_callback(int argc, int optind, char **argv, char *value, int nvalue, al_conf_if_info_t *if_info)
{
   if ((nvalue != 0) && (nvalue != 1))
   {
      return -4;
   }

   if_info->hide_ssid = nvalue;
   fprintf(stderr, "Success[%s hidessid=%d]\n", if_info->name, nvalue);

   return 0;
}


static int _set_hide_essid(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   int ret;

   ret = _if_iterator(argc, optind, argv, conf_handle, _hidessid_callback);

   if (ret == -4)
   {
      fprintf(stderr, "usage: alconfset hidessid <<if-name>> <<0 or 1>>\n");
   }

   return ret;
}


static int _txpower_callback(int argc, int optind, char **argv, char *value, int nvalue, al_conf_if_info_t *if_info)
{
   char buf[128];

   if ((nvalue < 0) || (nvalue > 100))
   {
      return -4;
   }

   if_info->txpower = nvalue;

   if (nvalue != 0)
   {
      sprintf(buf, "iwconfig %s txpower %dmW\n", if_info->name, nvalue);
   }
   else
   {
      sprintf(buf, "iwconfig %s txpower 0\n", if_info->name);
   }

   system(buf);
   fprintf(stderr, "Success[%s txpower=%d]\n", if_info->name, nvalue);

   return 0;
}


static int _set_txpower(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   int ret;

   ret = _if_iterator(argc, optind, argv, conf_handle, _txpower_callback);

   if (ret == -4)
   {
      fprintf(stderr, "usage: alconfset txpower <<if-name>> <<0 to 100>>\n");
   }

   return ret;
}


static int _set_preferred_parent(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   int           ret;
   char          *value;
   al_net_addr_t addr;

   if (argc <= optind + 1)
   {
      fprintf(stderr, "usage: alconfset prefpar <<MAC-ID>>\n");
      return -4;
   }

   value = argv[optind + 1];

   memset(&addr, 0, sizeof(al_net_addr_t));

   addr.length = 6;

   mac_address_ascii_to_binary(value, addr.bytes);

   ret = al_conf_set_preferred_parent(AL_CONTEXT conf_handle, &addr);

   if (!ret)
   {
      fprintf(stderr, "Success[prefpar=%s]\n", value);
   }
   else
   {
      fprintf(stdout, "<<err %d>>\n", ret);
   }

   return ret;
}


static int _set_hb_interval(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   int  ret;
   char *value;
   int  nvalue;

   if (argc <= optind + 1)
   {
      fprintf(stderr, "usage: alconfset hbint <<value (non-zero possitive)>>\n");
      return -4;
   }

   value  = argv[optind + 1];
   nvalue = atoi(value);

   if (nvalue <= 0)
   {
      fprintf(stderr, "usage: alconfset hbint <<value (non-zero possitive)>>\n");
      return -4;
   }

   ret = al_conf_set_heartbeat_interval(AL_CONTEXT conf_handle, nvalue);

   if (!ret)
   {
      fprintf(stderr, "Success[hbint=%d]\n", nvalue);
   }
   else
   {
      fprintf(stdout, "<<err %d>>\n", ret);
   }

   return ret;
}


static int _set_igmp(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   int  ret;
   char *value;
   int  nvalue;
   int  curval;

   if (argc <= optind + 1)
   {
      fprintf(stderr, "usage: alconfset igmp <<0 or 1>>\n");
      return -4;
   }

   value  = argv[optind + 1];
   nvalue = atoi(value);

   if ((nvalue != 0) && (nvalue != 1))
   {
      fprintf(stderr, "usage: alconfset igmp <<0 or 1>>\n");
      return -4;
   }

   curval  = al_conf_get_hop_cost(AL_CONTEXT conf_handle);
   curval &= 0xFE;

   if (nvalue)
   {
      curval |= 0x01;
   }

   ret = al_conf_set_hop_cost(AL_CONTEXT conf_handle, curval);

   if (!ret)
   {
      fprintf(stderr, "Success[igmp=%d]\n", nvalue);
   }
   else
   {
      fprintf(stdout, "<<err %d>>\n", ret);
   }

   return ret;
}


static int _set_adhoc(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   int  ret;
   char *value;
   int  adhoc_mode;
   int  sectored_usage;
   int  begin_in_infra;
   int  curval;

   if (argc <= optind + 3)
   {
      goto _usage;
   }

   value      = argv[optind + 1];
   adhoc_mode = atoi(value);

   if ((adhoc_mode != 0) && (adhoc_mode != 1))
   {
      goto _usage;
   }

   value          = argv[optind + 2];
   begin_in_infra = atoi(value);

   if ((begin_in_infra != 0) && (begin_in_infra != 1))
   {
      goto _usage;
   }

   value          = argv[optind + 3];
   sectored_usage = atoi(value);

   if ((sectored_usage != 0) && (sectored_usage != 1))
   {
      goto _usage;
   }

   curval = al_conf_get_hop_cost(AL_CONTEXT conf_handle);

   curval &= 0xFD;              /* ADHOC mask */
   curval &= 0xF7;              /* Begin in INFRA mask */
   curval &= 0xBF;              /* Sectored Usage mask */

   if (adhoc_mode)
   {
      curval |= 0x02;
   }

   if (begin_in_infra)
   {
      curval |= 0x08;
   }

   if (sectored_usage)
   {
      curval |= 0x40;
   }

   ret = al_conf_set_hop_cost(AL_CONTEXT conf_handle, curval);

   if (!ret)
   {
      fprintf(stderr, "Success[adhoc=%d begin_in_infra = %d sectored_usage=%d]\n", adhoc_mode, begin_in_infra, sectored_usage);
   }
   else
   {
      fprintf(stdout, "<<err %d>>\n", ret);
   }

   return ret;

_usage:

   fprintf(stderr, "usage: alconfset adhoc <<adhoc_mode=0/1>> <<begin_in_infra=0/1>> <<sectored_usage=0/1>>\n");

   return -4;
}


static int _ip_address_ascii_to_binary(const char *ascii, unsigned char *binary)
{
   int        i;
   char       temp[16];
   const char *src;
   char       *dest;

   memset(binary, 0, 4);

   for (i = 0, src = ascii; i < 4; i++)
   {
      if (*src == 0)
      {
         break;
      }

      dest = temp;

      while (*src != 0 && *src != '.')
      {
         *dest++ = *src++;
      }

      *dest = 0;

      binary[i] = atoi(temp);

      if (*src != 0)
      {
         ++src;
      }
   }

   return (i != 4) ? -1 : 0;
}


static int _set_dhcp(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   int                 ret;
   char                *value;
   int                 enabled;
   int                 nvalue;
   int                 curval;
   al_conf_dhcp_info_t dhcp_info;

   if (argc <= optind + 1)
   {
      goto _param_err;
   }

   value   = argv[optind + 1];
   enabled = atoi(value);

   if ((enabled != 0) && (enabled != 1))
   {
      goto _param_err;
   }

   if (enabled == 1)
   {
      if (argc <= optind + 1 + 6)
      {
         goto _param_err;
      }

      al_conf_get_dhcp_info(AL_CONTEXT conf_handle, &dhcp_info);

      if (!strcmp(argv[optind + 1 + 1], "random"))
      {
         dhcp_info.dhcp_mode = AL_CONF_DHCP_MODE_RANDOM;
      }
      else if (!strcmp(argv[optind + 1 + 1], "fixed"))
      {
         dhcp_info.dhcp_mode = AL_CONF_DHCP_MODE_FIXED;
      }
      else
      {
         goto _param_err;
      }

      ret = _ip_address_ascii_to_binary(argv[optind + 1 + 2], dhcp_info.dhcp_net_id);

      if (ret != 0)
      {
         fprintf(stderr, "Error in Network ID\n");
         goto _param_err;
      }

      _ip_address_ascii_to_binary(argv[optind + 1 + 3], dhcp_info.dhcp_mask);

      if (ret != 0)
      {
         goto _param_err;
      }

      _ip_address_ascii_to_binary(argv[optind + 1 + 4], dhcp_info.dhcp_gateway);

      if (ret != 0)
      {
         goto _param_err;
      }

      _ip_address_ascii_to_binary(argv[optind + 1 + 5], dhcp_info.dhcp_dns);

      if (ret != 0)
      {
         goto _param_err;
      }

      nvalue = atoi(argv[optind + 1 + 6]);

      if (nvalue <= 0)
      {
         goto _param_err;
      }

      dhcp_info.dhcp_lease_time = nvalue;

      al_conf_set_dhcp_info(AL_CONTEXT conf_handle, &dhcp_info);
   }

   curval  = al_conf_get_hop_cost(AL_CONTEXT conf_handle);
   curval &= 0xEF;

   if (enabled)
   {
      curval |= 0x10;
   }

   ret = al_conf_set_hop_cost(AL_CONTEXT conf_handle, curval);

   if (!ret)
   {
      fprintf(stderr, "Success[dhcp=%d]\n", enabled);
   }
   else
   {
      fprintf(stdout, "<<err %d>>\n", ret);
   }

   return ret;


_param_err:

   fprintf(stderr,
           "usage: alconfset dhcp <EN> [<MODE> <NET> <MASK> <GW> <DNS> <LT>]\n"
           "       EN = 1 for enabling, 0 for disabling\n"
           "       MODE = random or fixed\n"
           "       NET = Network in dotted decimal form\n"
           "       MASK = Subnet mask in dotted decimal form\n"
           "       GW = Gateway IP in dotted decimal form\n"
           "       DNS = DNS IP in dotted decimal form\n"
           "       LT = Lease time in seconds\n");

   return -4;
}


static int _set_gps(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   int                ret;
   char               *value;
   int                enabled;
   int                nvalue;
   al_conf_gps_info_t gps_info;

   if (argc <= optind + 1)
   {
      goto _param_err;
   }

   value   = argv[optind + 1];
   enabled = atoi(value);

   if ((enabled != 0) && (enabled != 1))
   {
      goto _param_err;
   }

   al_conf_get_gps_info(AL_CONTEXT conf_handle, &gps_info);

   gps_info.gps_enabled = enabled;

   if (enabled == 1)
   {
      if (argc <= optind + 1 + 4)
      {
         goto _param_err;
      }

      strcpy(gps_info.gps_input_dev, argv[optind + 1 + 1]);

      ret = _ip_address_ascii_to_binary(argv[optind + 1 + 2], gps_info.gps_push_dest_ip);

      if (ret != 0)
      {
         goto _param_err;
      }

      nvalue = atoi(argv[optind + 1 + 3]);

      if (nvalue < 0)
      {
         goto _param_err;
      }

      gps_info.gps_push_port = nvalue;

      nvalue = atoi(argv[optind + 1 + 4]);

      if (nvalue < 0)
      {
         goto _param_err;
      }

      gps_info.gps_push_interval = nvalue;
   }

   ret = al_conf_set_gps_info(AL_CONTEXT conf_handle, &gps_info);

   if (!ret)
   {
      fprintf(stderr, "Success[gps=%d]\n", enabled);
   }
   else
   {
      fprintf(stderr, "<<err %d>>\n", ret);
   }

   return ret;


_param_err:

   fprintf(stderr,
           "usage: alconfset gps <EN> [<device> <dest_ip> <dest_port> <tx_interval>]\n"
           "       EN = 1 for enabling, 0 for disabling\n"
           "       device = Serial port device e.g. /dev/ttyS0\n"
           "       dest_ip = Push destination IP address\n"
           "       dest_port = Push destination UDP port\n"
           "       tx_interval = Push destination transmit interval\n");

   return -4;
}


static int _set_logmon(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   int  ret;
   char *value;
   int  nvalue;
   al_conf_logmon_info_t logmon;

   if (argc <= optind + 2)
   {
      goto _param_err;
   }

   al_conf_get_logmon_info(AL_CONTEXT conf_handle, &logmon);

   ret = _ip_address_ascii_to_binary(argv[optind + 1], logmon.logmon_dest_ip);

   if (ret != 0)
   {
      goto _param_err;
   }

   nvalue = atoi(argv[optind + 2]);

   if (nvalue < 0)
   {
      goto _param_err;
   }

   logmon.logmon_dest_port = nvalue;

   ret = al_conf_set_logmon_info(AL_CONTEXT conf_handle, &logmon);

   if (!ret)
   {
      fprintf(stderr,
              "Success[logmon=%d.%d.%d.%d:%d]\n",
              logmon.logmon_dest_ip[0],
              logmon.logmon_dest_ip[1],
              logmon.logmon_dest_ip[2],
              logmon.logmon_dest_ip[3],
              logmon.logmon_dest_port);
   }
   else
   {
      fprintf(stderr, "<<err %d>>\n", ret);
   }

   return ret;

_param_err:

   fprintf(stderr,
           "usage: alconfset logmon <dest_ip> <dest_port>\n"
           "       dest_ip = Push destination IP address\n"
           "       dest_port = Push destination UDP port\n");


   return -4;
}


static int _set_forced_root(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   int  ret;
   char *value;
   int  nvalue;
   int  curval;

   if (argc <= optind + 1)
   {
      fprintf(stderr, "usage: alconfset forcedroot <<0 or 1>>\n");
      return -4;
   }

   value  = argv[optind + 1];
   nvalue = atoi(value);

   if ((nvalue != 0) && (nvalue != 1))
   {
      fprintf(stderr, "usage: alconfset forcedroot <<0 or 1>>\n");
      return -4;
   }

   curval  = al_conf_get_hop_cost(AL_CONTEXT conf_handle);
   curval &= 0xFB;

   if (nvalue)
   {
      curval |= 0x04;
   }

   ret = al_conf_set_hop_cost(AL_CONTEXT conf_handle, curval);

   if (!ret)
   {
      fprintf(stderr, "Success[forcedroot=%d]\n", nvalue);
   }
   else
   {
      fprintf(stdout, "<<err %d>>\n", ret);
   }

   return ret;
}


static int _set_fips(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   int            ret;
   char           *value;
   int            nvalue;
   unsigned short regdom;

   if (argc <= optind + 1)
   {
      fprintf(stderr, "usage: alconfset fips <<0 or 1>>\n");
      return -4;
   }

   value  = argv[optind + 1];
   nvalue = atoi(value);

   if ((nvalue != 0) && (nvalue != 1))
   {
      fprintf(stderr, "usage: alconfset fips <<0 or 1>>\n");
      return -4;
   }

   regdom  = al_conf_get_regulatory_domain(AL_CONTEXT conf_handle);
   regdom &= 0xFDFF;

   if (nvalue)
   {
      regdom |= 0x200;
   }

   ret = al_conf_set_regulatory_domain(AL_CONTEXT conf_handle, regdom);

   if (!ret)
   {
      fprintf(stderr, "Success[fips=%d]\n", nvalue);
   }
   else
   {
      fprintf(stdout, "<<err %d>>\n", ret);
   }

   return ret;
}


static int _set_dfs(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   int            ret;
   char           *value;
   int            nvalue;
   unsigned short regdom;

   if (argc <= optind + 1)
   {
      fprintf(stderr, "usage: alconfset dfs <<0 or 1>>\n");
      return -4;
   }

   value  = argv[optind + 1];
   nvalue = atoi(value);

   if ((nvalue != 0) && (nvalue != 1))
   {
      fprintf(stderr, "usage: alconfset dfs <<0 or 1>>\n");
      return -4;
   }

   regdom  = al_conf_get_regulatory_domain(AL_CONTEXT conf_handle);
   regdom &= 0xFEFF;

   if (nvalue)
   {
      regdom |= 0x100;
   }

   ret = al_conf_set_regulatory_domain(AL_CONTEXT conf_handle, regdom);

   if (!ret)
   {
      fprintf(stderr, "Success[dfs=%d]\n", nvalue);
   }
   else
   {
      fprintf(stdout, "<<err %d>>\n", ret);
   }

   return ret;
}


static int _set_mobility_index(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   int  ret;
   char *value;
   int  nvalue;
   int  curval;

   if (argc <= optind + 1)
   {
      fprintf(stderr, "usage: alconfset mobindex <<0-20>>\n");
      return -4;
   }

   value  = argv[optind + 1];
   nvalue = atoi(value);

   if ((nvalue < 0) || (nvalue > 20))
   {
      fprintf(stderr, "usage: alconfset mobindex <<0-20>>\n");
      return -4;
   }

   curval  = al_conf_get_las_interval(AL_CONTEXT conf_handle);
   curval &= 0x80;
   curval |= nvalue;

   ret = al_conf_set_las_interval(AL_CONTEXT conf_handle, curval);

   if (!ret)
   {
      fprintf(stderr, "Success[mobindex=%d]\n", nvalue);
   }
   else
   {
      fprintf(stdout, "<<err %d>>\n", ret);
   }

   return ret;
}


static int _set_mobility_mode(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   int  ret;
   char *value;
   int  nvalue;
   int  curval;

   if (argc <= optind + 1)
   {
      fprintf(stderr, "usage: alconfset mobmode <<0 or 1>>\n");
      return -4;
   }

   value  = argv[optind + 1];
   nvalue = atoi(value);

   if ((nvalue != 0) && (nvalue != 1))
   {
      fprintf(stderr, "usage: alconfset mobmode <<0 or 1>>\n");
      return -4;
   }

   curval  = al_conf_get_las_interval(AL_CONTEXT conf_handle);
   curval &= 0x7F;

   if (nvalue)
   {
      curval |= 0x80;
   }

   ret = al_conf_set_las_interval(AL_CONTEXT conf_handle, curval);

   if (!ret)
   {
      fprintf(stderr, "Success[mobmode=%d]\n", nvalue);
   }
   else
   {
      fprintf(stdout, "<<err %d>>\n", ret);
   }

   return ret;
}


static int _set_location(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   int  ret;
   char *value;
   int  nvalue;
   int  curval;

   if (argc <= optind + 1)
   {
      fprintf(stderr, "usage: alconfset location <<0 or 1>>\n");
      return -4;
   }

   value  = argv[optind + 1];
   nvalue = atoi(value);

   if ((nvalue != 0) && (nvalue != 1))
   {
      fprintf(stderr, "usage: alconfset location <<0 or 1>>\n");
      return -4;
   }

   curval  = al_conf_get_hop_cost(AL_CONTEXT conf_handle);
   curval &= 0xDF;

   if (nvalue)
   {
      curval |= 0x20;
   }

   ret = al_conf_set_hop_cost(AL_CONTEXT conf_handle, curval);

   if (!ret)
   {
      fprintf(stderr, "Success[location=%d]\n", nvalue);
   }
   else
   {
      fprintf(stdout, "<<err %d>>\n", ret);
   }

   return ret;
}


static int _set_option(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   const char       *subop;
   al_conf_option_t *options;
   al_conf_option_t *temp;
   int              i;

   if (argc <= optind + 1)
   {
      goto _usage;
   }

   subop = argv[optind + 1];

   if (!strcmp(subop, "list"))
   {
      options = al_conf_get_options(AL_CONTEXT conf_handle);

      for (temp = options; temp != NULL; temp = temp->next)
      {
         for (i = 0; i < 16; i++)
         {
            fprintf(stderr, "%s%02X", (i != 0) ? ":" : "", temp->key[i]);
         }

         fprintf(stderr, "\n\n");
      }

      while (options != NULL)
      {
         temp = options->next;
         al_heap_free(AL_CONTEXT options);
         options = temp;
      }
   }
   else if (!strcmp(subop, "add"))
   {
      if (argc <= optind + 2)
      {
         goto _usage;
      }

      if (strlen(argv[optind + 2]) != (15 * 3) + 2)
      {
         fprintf(stderr, "Invalid key format\n");
         goto _usage;
      }

      temp = (al_conf_option_t *)al_heap_alloc(AL_CONTEXT sizeof(al_conf_option_t)AL_HEAP_DEBUG_PARAM);

      mac_address_ascii_to_binary(argv[optind + 2], temp->key);

      options = al_conf_get_options(AL_CONTEXT conf_handle);

      temp->next = options;

      al_conf_set_options(AL_CONTEXT conf_handle, temp);

      fprintf(stderr, "Success[option added]\n");
   }
   else if (!strcmp(subop, "del"))
   {
      al_conf_option_t *prev;
      int              index;

      if (argc <= optind + 2)
      {
         goto _usage;
      }

      index = atoi(argv[optind + 2]);

      if (index < 0)
      {
         fprintf(stderr, "Invalid option index\n");
         goto _usage;
      }

      options = al_conf_get_options(AL_CONTEXT conf_handle);

      for (i = 0, temp = options, prev = NULL; temp != NULL; prev = temp, temp = temp->next, i++)
      {
         if (i == index)
         {
            if (prev != NULL)
            {
               prev->next = temp->next;
            }
            else
            {
               options = options->next;
            }

            al_heap_free(AL_CONTEXT temp);

            break;
         }
      }

      al_conf_set_options(AL_CONTEXT conf_handle, options);

      fprintf(stderr, "Success[option deleted]\n");
   }
   else
   {
      goto _usage;
   }

   return 0;

_usage:

   fprintf(stderr,
           "usage: \n"
           "\talconfset option list\n"
           "\talconfset option add option-key\n"
           "\talconfset option del option-index\n");

   return -4;
}


typedef int (*dot11e_int_callback_t)(int category, dot11e_conf_category_details_t *category_info, int value);

static int _dot11e_category_helper(int category, int value, dot11e_int_callback_t callback)
{
   int           fd;
   off_t         offset;
   unsigned char *file_data;
   int           dot11e_handle;
   dot11e_conf_category_details_t category_info;

   /**
    * Read in the config file into memory
    */

   fd = open(_DEFAULT_11E_CONF_FILE_PATH, O_RDONLY);

   if (fd == -1)
   {
      fprintf(stderr, "Error opening "_DEFAULT_11E_CONF_FILE_PATH " for reading\n");
      return -5;
   }

   offset = lseek(fd, 0, SEEK_END);

   file_data = (unsigned char *)malloc(offset);
   memset(file_data, 0, offset + 1);

   lseek(fd, 0, SEEK_SET);

   read(fd, file_data, offset);

   close(fd);

   dot11e_handle = dot11e_conf_parse(AL_CONTEXT file_data);

   if (dot11e_handle == 0)
   {
      fprintf(stderr, "Error parsing "_DEFAULT_11E_CONF_FILE_PATH);
      return -6;
   }

   free(file_data);

   dot11e_conf_get_category_info(AL_CONTEXT dot11e_handle, category, &category_info);

   callback(category, &category_info, value);

   dot11e_conf_set_category_info(AL_CONTEXT dot11e_handle, category, &category_info);

   dot11e_conf_put(AL_CONTEXT dot11e_handle);

   dot11e_conf_close(AL_CONTEXT dot11e_handle);

   return 0;
}


static int _acwmin_callback(int category, dot11e_conf_category_details_t *category_info, int value)
{
   category_info->acwmin = value;

   fprintf(stderr, "Success[acwmin for category %d=%d]\n", category, value);

   return 0;
}


static int _set_acwmin(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   int category;
   int value;
   int temp;

   if (argc <= optind + 2)
   {
      goto _usage;
   }

   category = atoi(argv[optind + 1]);
   value    = atoi(argv[optind + 2]);

   if ((category < 0) || (category > 3) || (value > 1023))
   {
      goto _usage;
   }

   _dot11e_category_helper(category, value, _acwmin_callback);

   return 0;


_usage:

   fprintf(stderr, "usage: alconfset acwmin <<category [0-3]>> <<value[0,1,3,7,15,31,63,...,1023]>>\n");

   return -4;
}


static int _acwax_callback(int category, dot11e_conf_category_details_t *category_info, int value)
{
   category_info->acwmax = value;

   fprintf(stderr, "Success[acwmax for category %d=%d]\n", category, value);

   return 0;
}


static int _set_acwmax(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   int category;
   int value;
   int temp;

   if (argc <= optind + 2)
   {
      goto _usage;
   }

   category = atoi(argv[optind + 1]);
   value    = atoi(argv[optind + 2]);

   if ((category < 0) || (category > 3) || (value > 1023))
   {
      goto _usage;
   }

   _dot11e_category_helper(category, value, _acwax_callback);

   return 0;

_usage:

   fprintf(stderr, "usage: alconfset acwmax <<category [0-3]>> <<value[0,1,3,7,15,31,63,...,1023]>>\n");

   return -4;
}


static int _aifsn_callback(int category, dot11e_conf_category_details_t *category_info, int value)
{
   category_info->aifsn = value;

   fprintf(stderr, "Success[aifsn for category %d=%d]\n", category, value);

   return 0;
}


static int _set_aifsn(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   int category;
   int value;
   int temp;

   if (argc <= optind + 2)
   {
      goto _usage;
   }

   category = atoi(argv[optind + 1]);
   value    = atoi(argv[optind + 2]);

   if ((category < 0) || (category > 3))
   {
      goto _usage;
   }

   _dot11e_category_helper(category, value, _aifsn_callback);

   return 0;

_usage:

   fprintf(stderr, "usage: alconfset aifsn <<category [0-3]>> <<value>>\n");

   return -4;
}


static int _backoff_callback(int category, dot11e_conf_category_details_t *category_info, int value)
{
   category_info->disable_backoff = !value;

   fprintf(stderr, "Success[backoff for category %d=%d]\n", category, value);

   return 0;
}


static int _set_backoff(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   int category;
   int value;
   int temp;

   if (argc <= optind + 2)
   {
      goto _usage;
   }

   category = atoi(argv[optind + 1]);
   value    = atoi(argv[optind + 2]);

   if ((category < 0) || (category > 3) || (value < 0) || (value > 1))
   {
      goto _usage;
   }

   _dot11e_category_helper(category, value, _backoff_callback);

   return 0;

_usage:

   fprintf(stderr, "usage: alconfset backoff <<category [0-3]>> <<value = 0 or 1>>\n");

   return -4;
}


static int _burst_callback(int category, dot11e_conf_category_details_t *category_info, int value)
{
   category_info->burst_time = value;

   fprintf(stderr, "Success[burst for category %d=%d]\n", category, value);

   return 0;
}


static int _set_burst(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   int category;
   int value;
   int temp;

   if (argc <= optind + 2)
   {
      goto _usage;
   }

   category = atoi(argv[optind + 1]);
   value    = atoi(argv[optind + 2]);

   if ((category < 0) || (category > 3) || (value < 0))
   {
      goto _usage;
   }

   _dot11e_category_helper(category, value, _burst_callback);

   return 0;

_usage:

   fprintf(stderr, "usage: alconfset burst <<category [0-3]>> <<value>>\n");

   return -4;
}


static void _auto_start_enum(void *cookie, int number, const char *command_block_string)
{
   fprintf(stderr,
           "------- BLOCK %d ----------------\n"
           "%s\n"
           "---------------------------------\n\n",
           number,
           command_block_string);
}


static void _autostart_list(auto_start_token_t token)
{
   fprintf(stderr, "\n");

   auto_start_enumerate_blocks(token, _auto_start_enum, NULL);

   fprintf(stderr, "\n");
}


static int _autostart_create(auto_start_token_t token)
{
   return auto_start_create_block(token);
}


static void _autostart_add(auto_start_token_t token, int command_block_number, const char *command)
{
   auto_start_append_block(token, command_block_number, command);
}


static void _autostart_del(auto_start_token_t token, int command_block_number)
{
   auto_start_delete_block(token, command_block_number);
}


static int _set_autostart(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   const char         *subop;
   int                command_block_number;
   const char         *command;
   auto_start_token_t token;
   int                ret;

   token = NULL;

   if (argc <= optind + 1)
   {
      goto _usage;
   }

   token = auto_start_open("/root/configd_spawn.sh");

   subop = argv[optind + 1];

   if (!strcmp(subop, "list"))
   {
      _autostart_list(token);
   }
   else if (!strcmp(subop, "create"))
   {
      ret = _autostart_create(token);

      fprintf(stderr, "Success[Block %d created]\n", ret);

      auto_start_save(token);
   }
   else if (!strcmp(subop, "add"))
   {
      if (argc <= optind + 3)
      {
         goto _usage;
      }

      command_block_number = atoi(argv[optind + 2]);
      command = argv[optind + 3];

      _autostart_add(token, command_block_number, command);

      auto_start_save(token);

      fprintf(stderr, "Success[Block %d appended]\n", command_block_number);
   }
   else if (!strcmp(subop, "del"))
   {
      if (argc <= optind + 2)
      {
         goto _usage;
      }

      command_block_number = atoi(argv[optind + 2]);

      _autostart_del(token, command_block_number);

      auto_start_save(token);

      fprintf(stderr, "Success[Block %d deleted]\n", command_block_number);
   }
   else
   {
      goto _usage;
   }

   return 0;

   auto_start_close(token);

_usage:

   if (token != NULL)
   {
      auto_start_close(token);
   }

   fprintf(stderr,
           "usage: alconfset autostart list\n"
           "       alconfset autostart create\n"
           "       alconfset autostart add <<command-block-number>> <<command>>\n"
           "       alconfset autostart del <<command-block-number>>\n");

   return -4;
}


static int _save_config(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   int  sqnr;
   char buf[128];

   /**
    * Increment CONFIG SQNR, Set REBOOT REQD flag, and execute POST FS CHANGE
    */

   sqnr = al_conf_get_config_sqnr(AL_CONTEXT conf_handle);

   al_conf_set_config_sqnr(AL_CONTEXT conf_handle, ++sqnr);

   al_conf_put(AL_CONTEXT conf_handle);

   sprintf(buf, "/sbin/meshd config_sqnr %d", sqnr);
   system(buf);

   sprintf(buf, "/sbin/meshd set_reboot_flag 1");
   system(buf);

   sprintf(buf, "/root/post_fs_change.sh config");
   system(buf);

   fprintf(stderr, "Success[config saved]\n");

   return -4;
}


static int _save_fw(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   char buf[128];

   sprintf(buf, "/root/post_fs_change.sh binary");
   system(buf);

   fprintf(stderr, "Success[firmware saved]\n");
}


static int _set_server_ip_addr(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   int           ret = 0;
   unsigned char *value;

   if (argc <= optind + 1)
   {
      goto _usage;
   }

   value = argv[optind + 1];
   ret   = al_conf_set_server_addr(AL_CONTEXT conf_handle, value);
   if (!ret)
   {
      fprintf(stderr, "Success[IP_ADDR = %s]\n", value);
   }
   else
   {
      fprintf(stdout, "<<err %d>> ipaddr = %s\n", ret, value);
   }

   return ret;

_usage:

   fprintf(stderr, "usage: alconfset server_ip_addr <IP_ADDR> eg: alconfset server_ip_addr 10.10.10.10\n");

   return -4;
}

static int _set_mgmt_gw_addr(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   int           ret = 0;
   unsigned char *value;

   if (argc <= optind + 1)
   {
      goto _usage;
   }

   value = argv[optind + 1];
   ret   = al_conf_set_mgmt_gw_addr(AL_CONTEXT conf_handle, value);
   if (!ret)
   {
      fprintf(stderr, "Success[MGMT_GW_ADDR = %s]\n", value);
   }
   else
   {
      fprintf(stdout, "<<err %d>> mgmt_gw_addr = %s\n", ret, value);
   }

   return ret;

_usage:

   fprintf(stderr, "usage: alconfset mgmt_gw_addr <IP_ADDR> eg: alconfset mgmt_gw_addr <url:port>\n");

	return -4;
}

static int _set_mgmt_gw_certificates(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   int           ret = 0;
   unsigned char *value;

   if (argc <= optind + 1)
   {
      goto _usage;
   }

   value = argv[optind + 1];
   ret   = al_conf_set_mgmt_gw_certificates(AL_CONTEXT conf_handle, value);
   if (!ret)
   {
      fprintf(stderr, "Success[MGMT_GW_CERTIFICATES = %s]\n", value);
   }
   else
   {
      fprintf(stdout, "<<err %d>> mgmt_gw_certificates = %s\n", ret, value);
   }

   return ret;

_usage:

   fprintf(stderr, "usage: alconfset mgmt_gw_certificates <certificate_path>:<key_path> eg: alconfset mgmt_gw_certificates </path/xyz.crt:/path/abc.key>\n");

	return -4;
}

static int _set_mgmt_gw_enable(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   int  ret;
   char *value;
   int  nvalue;

   if (argc <= optind + 1)
   {
      fprintf(stderr, "usage: alconfset mgmt_gw_enable <0/1>\n");
      return -4;
   }

   value  = argv[optind + 1];
   nvalue = atoi(value);

   if (nvalue < 0 || nvalue > 1)
   {
      fprintf(stderr, "usage: alconfset mgmt_gw_enable <0/1>\n");
      return -4;
   }

   ret = al_conf_set_mgmt_gw_enable(AL_CONTEXT conf_handle, nvalue);

   if (!ret)
   {
      fprintf(stderr, "Success[mgmt_gw_enable=%d]\n", nvalue);
   }
   else
   {
      fprintf(stdout, "<<err %d>>\n", ret);
   }

   return ret;
}

static int _fail_over_ethernet(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
   int  ret;
   char *value;
   int  on_off;
   int  power_on_default;
   int  scan_freq_secs;
   char *server_ip_addr;

   if (argc <= optind + 4)
   {
      goto _usage;
   }

   value  = argv[optind + 1];
   on_off = atoi(value);

   if ((on_off != 0) && (on_off != 1) && (on_off != 2))
   {
      goto _usage;
   }

   value            = argv[optind + 2];
   power_on_default = atoi(value);

   if ((power_on_default != 0) && (power_on_default != 1))
   {
      goto _usage;
   }
/* need to talk about how to use this parameter*/

   value          = argv[optind + 3];
   scan_freq_secs = atoi(value);

   if ((scan_freq_secs != 0) && (scan_freq_secs != 1))
   {
      goto _usage;
   }

   /* Read the server ip address which will be used to check the eth1 link connectivity */
   server_ip_addr = argv[optind + 4];

   ret = al_conf_set_failover_enabled(AL_CONTEXT conf_handle, on_off, power_on_default, scan_freq_secs, server_ip_addr);


   if (!ret)
   {
      fprintf(stderr, "Success[on_off=%d power_on_default = %d scan_freq_secs=%d] server_ip_addr = %s\n", on_off, power_on_default, scan_freq_secs, server_ip_addr);
   }
   else
   {
      fprintf(stdout, "<<err %d>>\n", ret);
   }

   return ret;

_usage:

   fprintf(stderr, "usage: alconfset failOverEthernet <<off/enable/enable_with_ping=0/1/2>> <<power_on_default=0/1>> <<scan_freq_secs=0/1>>\n");

   return -4;
}


static int _set_disable_backhaul_security(int argc, int optind, char **argv, const char *operation, int conf_handle)
{
	int  ret;
	int value;

	if (argc <= optind + 1)
	{
		return -4;
	}

	value = atoi(argv[optind + 1]);

	if ((value != 0) && (value != 1))
	{
		return -4;
	}

	ret = al_conf_set_disable_backhaul_security(AL_CONTEXT conf_handle, value);

	if (!ret)
	{
		fprintf(stderr, "Success[disable_backhaul_security=%d]\n", value);
	}
	else
	{
		fprintf(stdout, "<<err %d>>\n", ret);
	}

	return ret;
}
