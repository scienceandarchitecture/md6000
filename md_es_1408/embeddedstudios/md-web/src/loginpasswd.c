#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "command_params.h"
#include "login.h"
#include "passwd.h"

static link_list_t *list_head;

static int _execute_auth()
{
   link_list_t *username_ptr;
   link_list_t *passwd_ptr;
   link_list_t *redirect_ptr;

   username_ptr = get_param(list_head, COMMAND_PARAM_USER);
   passwd_ptr   = get_param(list_head, COMMAND_PARAM_PASS);
   redirect_ptr = get_param(list_head, COMMAND_PARAM_REDIRECTTO);

   printf("Content-type: text/html\n\n");

   if (login(username_ptr->data->value, passwd_ptr->data->value) == 0)
   {
      /* write back success on web page */
      printf("<META HTTP-EQUIV=\"Refresh\" Content=\"0; URL=/cgi-bin/%s.cgi\">", redirect_ptr->data->value);
   }
   else
   {
      /* write back login failed on web page */

      printf("<H1 align=\"center\">Login failed.");
      printf("<META HTTP-EQUIV=\"Refresh\" Content=\"2; URL=/cgi-bin/Status.cgi\">");
   }

   return 0;
}


static int _execute_setpass()
{
   link_list_t *username_ptr;
   link_list_t *oldkey_ptr;
   link_list_t *newkey_ptr;
   FILE        *script_file_ptr;

   username_ptr = get_param(list_head, COMMAND_PARAM_USER);
   oldkey_ptr   = get_param(list_head, COMMAND_PARAM_OLDKEY);
   newkey_ptr   = get_param(list_head, COMMAND_PARAM_NEWKEY);

   printf("Content-type: text/html\n\n");

   if (login(username_ptr->data->value, oldkey_ptr->data->value) != 0)
   {
      printf("<H1 align=\"center\">Incorrect old password.Password change failed.");
      printf("<META HTTP-EQUIV=\"Refresh\" Content=\"2; URL=/cgi-bin/Status.cgi\">");
      return -1;
   }

   if (setpasswd(username_ptr->data->value, newkey_ptr->data->value) == 0)
   {
      script_file_ptr = fopen("/home/httpd/cmd_script.sh", "w");
      fprintf(script_file_ptr, "#!/bin/sh\n");
      fprintf(script_file_ptr, "/root/post_fs_change.sh config\n");
      fclose(script_file_ptr);
      system("chmod +x cmd_script.sh");
      system("./cmd_script.sh");
      system("rm cmd_script.sh");

      printf("<H1 align=\"center\">Password change successful.<BR>");
   }
   else
   {
      printf("<H1 align=\"center\">Password change failed.<BR>");
   }

   printf("<META HTTP-EQUIV=\"Refresh\" Content=\"2; URL=/cgi-bin/Status.cgi\">");

   return 0;
}


static int _execute_cmd()
{
#define READ_PIPE     0
#define WRITE_PIPE    1

   link_list_t *cmd_ptr;
   int         cmd_pipe[2];
   int         pid;
   char        buffer[256];
   int         n;
   FILE        *cmd_out;
   FILE        *script_file_ptr;

   cmd_ptr = get_param(list_head, COMMAND_PARAM_COMMAND);

   pipe(cmd_pipe);      // create the pipe

   pid = fork();
   if (pid == 0)   // we re the child

   {
      close(cmd_pipe[READ_PIPE]);
      close(1);                  // close screen
      dup(cmd_pipe[WRITE_PIPE]); // duplicate pipe to file descr 1
      close(cmd_pipe[WRITE_PIPE]);
      script_file_ptr = fopen("cmd_script.sh", "w");
      fprintf(script_file_ptr, "#!/bin/sh\n");
      fprintf(script_file_ptr, "%s 2>&1\n", cmd_ptr->data->value);
      fclose(script_file_ptr);
      system("chmod +x cmd_script.sh");
      system("./cmd_script.sh");
      system("rm cmd_script.sh");
      exit(0);
   }

   // we're the parent
   close(cmd_pipe[WRITE_PIPE]);

   if ((cmd_out = fopen("cmd_out", "w")) <= 0)
   {
      return -1;
   }

   fprintf(cmd_out, "%s\n", cmd_ptr->data->value);

   while (1)
   {
      // read from our pipe

      memset(buffer, 0, sizeof(buffer));

      n = read(cmd_pipe[READ_PIPE], buffer, sizeof(buffer));

      if (n <= 0)
      {
         break;
      }

      fprintf(cmd_out, "%s", buffer);
   }

   fclose(cmd_out);

   printf("Content-type: text/html\n\n");
   printf("<META HTTP-EQUIV=\"Refresh\" Content=\"0; URL=/cgi-bin/ExecCommand.cgi\">");

#undef READ_PIPE
#undef WRITE_PIPE

   return 0;
}


int main()
{
   unsigned int content_length;
   char         *buffer, *endptr;
   link_list_t  *facility_ptr;
   const char   *len;

   list_head = NULL;
   len       = getenv("CONTENT_LENGTH");

   if (len == NULL)
   {
      return 0;
   }

   content_length = strtol(len, &endptr, 10);

   /*printf("CONTENT_LENGTH = %d<BR>",content_length);*/

   buffer = (char *)malloc((unsigned int)content_length);

   memset(buffer, 0, content_length);

   fread(buffer, 1, (unsigned int)content_length, stdin);

/*
 *      printf("Content-type: text/html\n\n");
 *      printf("CONTENT = %s<BR>",buffer);
 */
   url_decode(buffer, buffer, content_length);

   list_head = split_params(buffer, content_length);

   facility_ptr = get_param(list_head, COMMAND_PARAM_FACILITY);

   if (facility_ptr == NULL)
   {
      goto clean_and_exit;
   }

/*
 *      printf("Facility is %s<BR>",facility_ptr->data->value);
 */
   if (!strcmp(facility_ptr->data->value, COMMAND_PARAM_FACILITY_TYPE_AUTH))
   {
      _execute_auth();
   }
   else if (!strcmp(facility_ptr->data->value, COMMAND_PARAM_FACILITY_TYPE_SETPASS))
   {
      _execute_setpass();
   }
   else if (!strcmp(facility_ptr->data->value, COMMAND_PARAM_FACILITY_TYPE_EXEC))
   {
      _execute_cmd();
   }

clean_and_exit:

   free(buffer);

   return 0;
}
