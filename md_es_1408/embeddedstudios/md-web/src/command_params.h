#ifndef __COMMANDLINE_PARAMS__
#define __COMMANDLINE_PARAMS__

#define COMMAND_PARAM_FACILITY                 "facility"
#define COMMAND_PARAM_USER                     "user"
#define COMMAND_PARAM_PASS                     "pass"
#define COMMAND_PARAM_REDIRECTTO               "redirectto"
#define COMMAND_PARAM_OLDKEY                   "oldKey"
#define COMMAND_PARAM_NEWKEY                   "newKey"
#define COMMAND_PARAM_CONFIRMKEY               "confirmKey"
#define COMMAND_PARAM_COMMAND                  "command"


#define COMMAND_PARAM_FACILITY_TYPE_AUTH       "auth"
#define COMMAND_PARAM_FACILITY_TYPE_SETPASS    "setpass"
#define COMMAND_PARAM_FACILITY_TYPE_EXEC       "exec"

struct params
{
   char name[256];
   char value[256];
};

typedef struct params   params_t;

struct link_list
{
   params_t         *data;
   struct link_list *next_link;
   struct link_list *prev_link;
};

typedef struct link_list   link_list_t;

int url_decode(char *src_buf, char *dst_buf, int buffer_length);
link_list_t *split_params(char *src_buf, int buffer_length);
link_list_t *get_param(link_list_t *list_head, char *param_name);

#endif
