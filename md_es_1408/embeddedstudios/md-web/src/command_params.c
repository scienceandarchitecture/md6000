#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "command_params.h"

static int _hex_atoi(const char *string, int *value)
{
   int  c;              /* current char */
   long total;          /* current total */
   int  err;
   char *p;
   char buf[32];

#define _ishexalpha(c)             ((c >= 'A' && c <= 'F') || (c >= 'a' && c <= 'f'))
#define _isspace(c)                (c == '\t' || c == '\r' || c == '\n' || c == '\f' || c == ' ')
#define _isdigit(c)                (c >= 48 && c <= 48 + 9)
#define _ishexdigit(c)             (_isdigit(c) || _ishexalpha(c))
#define _get_hex_alpha_value(c)    ((c >= 'A' && c <= 'F') ? (10 + c - 'A') : (10 + c - 'a'))
#define _get_hex_value(c)          (_isdigit(c) ? (c - '0') : _get_hex_alpha_value(c))

   err = 0;

   /* skip whitespace */

   while (_isspace((int)(unsigned char)*string))
   {
      ++string;
   }

   strcpy(buf, string);

   p = buf;
   while (*p != 0)
   {
      p++;
   }
   p--;
   while (_isspace(*p))
   {
      *p = 0;
      p--;
   }

   p     = buf;
   c     = (int)(unsigned char)*p++;
   total = 0;

   while (_ishexdigit(c))
   {
      total = 16 * total + _get_hex_value(c);       /* accumulate digit */
      c     = (int)(unsigned char)*p++;             /* get next char */
   }

   if (c != 0)
   {
      err = -1;
   }

   *value = total;        /* return result, negated if necessary */

   return err;

#undef _isspace
#undef _isdigit
#undef _ishexalpha
#undef _ishexdigit
#undef _get_hex_alpha_value
#undef _get_hex_value
}


int url_decode(char *src_buf, char *dst_buf, int buffer_length)
{
   int  src_pos, dst_pos;
   char hex_str[5];
   int  hex_value;

   for (src_pos = 0, dst_pos = 0; src_pos < buffer_length; src_pos++, dst_pos++)
   {
      if (src_buf[src_pos] == '+')
      {
         dst_buf[dst_pos] = ' ';
      }
      else if (src_buf[src_pos] == '%')
      {
         memset(hex_str, 0, 5);
         strncpy(hex_str, &src_buf[src_pos + 1], 2);

         if (!strcmp(hex_str, "26") || !strcmp(hex_str, "3D"))
         {
            dst_buf[dst_pos++] = '%';
            strcpy(&dst_buf[dst_pos], hex_str);
         }
         else
         {
            _hex_atoi(hex_str, &hex_value);
            dst_buf[dst_pos] = hex_value;
         }

         src_pos += 2;
      }
      else
      {
         dst_buf[dst_pos] = src_buf[src_pos];
      }
   }

   dst_buf[dst_pos] = 0;

   return 0;
}


link_list_t *split_params(char *src_buf, int buffer_length)
{
   char        *param_set;
   char        name[256], *value;
   params_t    *param;
   link_list_t *list_ptr;
   link_list_t *list_head;

   list_head = NULL;

   param_set = strtok(src_buf, "&");

   while (param_set != NULL)
   {
      sscanf(param_set, "%[^=]", name);
      value = strstr(param_set, "=");
      value++;

      param = (params_t *)malloc(sizeof(params_t));

      strcpy(param->name, name);
      strcpy(param->value, value);

      list_ptr = (link_list_t *)malloc(sizeof(link_list_t));

      list_ptr->next_link = NULL;
      list_ptr->prev_link = NULL;

      list_ptr->data = param;

      list_ptr->next_link = list_head;

      if (list_head != NULL)
      {
         list_head->prev_link = list_ptr;
      }

      list_head = list_ptr;

      param_set = strtok(NULL, "&");
   }

   return list_head;
}


link_list_t *get_param(link_list_t *list_head, char *param_name)
{
   link_list_t *list_ptr;

   if (list_head == NULL)
   {
      return NULL;
   }

   list_ptr = list_head;

   while (list_ptr != NULL)
   {
      if (list_ptr->data == NULL)
      {
         continue;
      }

      if (!strcmp(list_ptr->data->name, param_name))
      {
         return list_ptr;
      }

      list_ptr = list_ptr->next_link;
   }

   return NULL;
}
