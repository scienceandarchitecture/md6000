#ifndef __DUMP_H__
#define __DUMP_H__

#include "configd.h"

int dump_start_ip_if_packet(imcp_start_ip_if_data_t *imcp_start_ip_if_data);
int dump_ipconfig_request_response_packet(imcp_ipconfig_rr_data_t *imcp_ipconfig_rr_data);
int dump_ipconfig_info_packet(imcp_ipconfig_data_t *imcp_ipconfig_data);
int dump_apconfig_request_response_packet(imcp_apconfig_rr_data_t *imcp_apconfig_rr_data);
int dump_apconfig_packet(imcp_apconfig_data_t *imcp_apconfig_data);
int dump_reset_packet(imcp_reset_data_t *imcp_reset_data);

#endif
