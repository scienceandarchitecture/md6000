/********************************************************************************
* MeshDynamics
* --------------
* File     : configd_helper.c
* Comments :
* Created  : 3/7/2005
* Author   : Anand Bhange
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  20 |8/22/2008 | Changes for FIPS and SIP                        |Abhijit |
* -----------------------------------------------------------------------------
* |  19 |8/10/2007 | Changes for libalconf                           | Sriram |
* -----------------------------------------------------------------------------
* |  18 |01/01/2007| Added Private Channel implementation            |Prachiti|
* -----------------------------------------------------------------------------
* |  17 |5/12/2006 | Changes for Firmware Update                     | Bindu  |
* -----------------------------------------------------------------------------
* |  16 |3/29/2006 | dot11e \0 problem fixed                         | Sriram |
* -----------------------------------------------------------------------------
* |  15 |3/15/2006 | Added .11e enabled & category bits ACL Packets  | Bindu  |
* |     |          | Removed vlan_name								  |        |
* -----------------------------------------------------------------------------
* |  14 |3/14/2006 | Changes for ACL                                 | Bindu  |
* -----------------------------------------------------------------------------
* |  13 |02/15/2006| dot11e packets added				              | Bindu  |
* -----------------------------------------------------------------------------
* |  12 |02/08/2006| Config SQNR and Hide SSID added				  | Sriram |
* -----------------------------------------------------------------------------
* |  11 |11/30/2005| phy_sub_type fix								  |Prachiti|
* -----------------------------------------------------------------------------
* |  10 |11/11/2005| ACK_Timeout added								  |Prachiti|
* -----------------------------------------------------------------------------
* |  9  |11/11/2005| Regulatory Domain and Country code added        |Prachiti|
* -----------------------------------------------------------------------------
* |  8  |9/15/2005 | WATCHD killed when updating configd             | Sriram |
* -----------------------------------------------------------------------------
* |  7  |5/19/2005 | interface txrate changes						  | Abhijit|
* -----------------------------------------------------------------------------
* |  6  |5/19/2005 | fcc_certified,etsi_certified changes            | Abhijit|
* -----------------------------------------------------------------------------
* |  5  |5/8/2005  | Merged GPS/Desc changes                         | Sriram |
* -----------------------------------------------------------------------------
* |  4  |4/22/2005 | Merged changes by Prachiti                      | Sriram |
* -----------------------------------------------------------------------------
* |  3  |4/15/2005 | Misc changes + code cleanup                     | Sriram |
* -----------------------------------------------------------------------------
* |  2  |4/12/2005 | radius_secret_key chaged to array               |Prachiti|
* -----------------------------------------------------------------------------
* |  1  |03/07/2005| Default 802.11e Enabled added to vlan packet    | Anand  |
* -----------------------------------------------------------------------------
********************************************************************************/

#define FILE_HOSTS                 "/etc/hosts"
#define FILE_MESHAP_CONF           "/etc/meshap.conf"
#define FILE_IFCONFIG_MIP0         "/etc/sysconfig/network-devices/ifconfig.mip0"
#define FILE_NETWORK               "/etc/sysconfig/network"
#define FILE_MESH_UPDATE_SERVER    "/sbin/mesh_update_server"

#define FILE_DOT11E_CONF           "/etc/dot11e.conf"
#define FILE_ACL_CONF              "/etc/acl.conf"
#define FILE_SIP_CONF              "/etc/sip.conf"


char update_path[] = "/root/updates/";

extern int sock;


int copy_al_conf_security_info(al_security_info_t *security_info, al_security_info_t *src_security_info);

AL_DECLARE_GLOBAL(al_conf_if_info_t old_al_if_info[CURRENT_EXPECTED_MAX_IF_COUNT]);
/*
 * update /etc/hosts file
 */
int update_hosts_file()
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter : %s  : %d\n",__func__,__LINE__);
   FILE *fp;
   char host_name[256];
   char *short_host_name;

   fp = fopen(FILE_HOSTS, "w+");

   if (!fp)
   {
      al_print_log(AL_LOG_TYPE_ERROR,"\nCONFIGD:\tError opening "FILE_HOSTS " for writing   : %d\n",__func__,__LINE__);
      return -1;
   }

   fprintf(fp, "# Begin /etc/hosts (network card version)\n\n");

   strcpy((char *)host_name, (const char *)imcp_ipconfig_data->host_name);

   short_host_name = strtok((char *)host_name, ".");

   fprintf(fp, "127.0.0.1  %s %s localhost.localdomain localhost\n\n", imcp_ipconfig_data->host_name, short_host_name);

   fprintf(fp, "# End /etc/hosts (network card version)\n");

   fclose(fp);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit : %s  : %d\n",__func__,__LINE__);

   return 0;
}


/*
 *	Write ap config file to /etc/meshap.conf disk
 */
int write_config_data(int al_conf_handle)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter : %s  : %d\n",__func__,__LINE__);
   FILE              *fp;
   int               al_if_count, i, j, dca_list_count;
   char              essid[33], name[129];
   al_net_addr_t     preferred_parent;
   al_conf_if_info_t al_if_info;
   int               ret;	

   al_conf_set_beacon_interval(al_conf_handle, imcp_apconfig_data->beacon_interval);
   al_conf_set_change_resistance(al_conf_handle, imcp_apconfig_data->cr_threshold);
   al_conf_set_dynamic_channel_allocation(al_conf_handle, imcp_apconfig_data->dca);
   al_conf_set_description(al_conf_handle, imcp_apconfig_data->description, imcp_apconfig_data->desc_length);
   al_conf_set_essid(al_conf_handle, imcp_apconfig_data->essid, imcp_apconfig_data->essid_length);
   al_conf_set_frag_th(al_conf_handle, imcp_apconfig_data->frag_threshold);
   al_conf_set_heartbeat_interval(al_conf_handle, imcp_apconfig_data->heartbeat_interval);
   al_conf_set_heartbeat_miss_count(al_conf_handle, imcp_apconfig_data->heartbeat_miss_count);
   al_conf_set_hop_cost(al_conf_handle, imcp_apconfig_data->hop_cost);
   al_conf_set_gps_x_coordinate(al_conf_handle, imcp_apconfig_data->gps_x_coordinate, imcp_apconfig_data->x_coordinate_length);
   al_conf_set_gps_y_coordinate(al_conf_handle, imcp_apconfig_data->gps_y_coordinate, imcp_apconfig_data->y_coordinate_length);
   al_conf_set_fcc_certified(al_conf_handle, imcp_apconfig_data->fcc_certified_operation);
   al_conf_set_etsi_certified(al_conf_handle, imcp_apconfig_data->etsi_certified_operation);
   al_conf_set_ds_tx_rate(al_conf_handle, imcp_apconfig_data->ds_tx_rate);
   al_conf_set_ds_tx_power(al_conf_handle, imcp_apconfig_data->ds_tx_power);

   /*
    *	Find and update only available interface from the config file.
    */
   memset(old_al_if_info, 0, sizeof(al_conf_if_info_t) * CURRENT_EXPECTED_MAX_IF_COUNT);
   al_if_count = al_conf_get_if_count(al_conf_handle);
   for (i = 0; i < al_if_count; i++)
   {
      memset(&al_if_info, 0, sizeof(al_conf_if_info_t));
      al_conf_get_if(al_conf_handle, i, &al_if_info);
      al_conf_get_if(al_conf_handle, i, &old_al_if_info[i]);
      for (j = 0; j < imcp_apconfig_data->interface_count; j++)
      {
         if (!strcmp(al_if_info.name, imcp_apconfig_data->if_info[j].name))
         {
            al_if_info.wm_channel   = imcp_apconfig_data->if_info[j].wm_channel;
            al_if_info.dca          = imcp_apconfig_data->if_info[j].dca;
            al_if_info.txpower      = imcp_apconfig_data->if_info[j].txpower;
            al_if_info.txrate       = imcp_apconfig_data->if_info[j].txrate;
            al_if_info.service      = imcp_apconfig_data->if_info[j].service;
            al_if_info.phy_sub_type = imcp_apconfig_data->if_info[j].phy_sub_type;

            memset(&al_if_info.security_info, 0, sizeof(al_security_info_t));
            memcpy(&al_if_info.security_info, &imcp_apconfig_data->if_info[j].security_info, sizeof(al_security_info_t));

			if(strlen(imcp_apconfig_data->if_info[j].essid) <= 32)
			{
				strcpy(al_if_info.essid,imcp_apconfig_data->if_info[j].essid);
			}

            al_if_info.rts_th          = imcp_apconfig_data->if_info[j].rts_th;
            al_if_info.frag_th         = imcp_apconfig_data->if_info[j].frag_th;
            al_if_info.beacon_interval = imcp_apconfig_data->if_info[j].beacon_interval;
            
            al_if_info.ht_capab.ldpc  = imcp_apconfig_data->if_info[j].ht_capab.ldpc;
            al_if_info.ht_capab.ht_bandwidth  = imcp_apconfig_data->if_info[j].ht_capab.ht_bandwidth;
            al_if_info.ht_capab.sec_offset = imcp_apconfig_data->if_info[j].ht_capab.sec_offset;
            al_if_info.ht_capab.gi_20  = imcp_apconfig_data->if_info[j].ht_capab.gi_20;
            al_if_info.ht_capab.gi_40  = imcp_apconfig_data->if_info[j].ht_capab.gi_40;
            al_if_info.ht_capab.tx_stbc  = imcp_apconfig_data->if_info[j].ht_capab.tx_stbc;
            al_if_info.ht_capab.rx_stbc  = imcp_apconfig_data->if_info[j].ht_capab.rx_stbc;
            al_if_info.ht_capab.gfmode  = imcp_apconfig_data->if_info[j].ht_capab.gfmode;
            al_if_info.ht_capab.max_amsdu_len  = imcp_apconfig_data->if_info[j].ht_capab.max_amsdu_len;
            al_if_info.ht_capab.intolerant  = imcp_apconfig_data->if_info[j].ht_capab.intolerant;
            al_if_info.frame_aggregation.ampdu_enable  = imcp_apconfig_data->if_info[j].frame_aggregation.ampdu_enable;
            al_if_info.frame_aggregation.max_ampdu_len  = imcp_apconfig_data->if_info[j].frame_aggregation.max_ampdu_len ;
            al_if_info.vht_capab.max_mpdu_len  = imcp_apconfig_data->if_info[j].vht_capab.max_mpdu_len ;
			
			al_if_info.vht_capab.rx_ldpc =  al_if_info.ht_capab.ldpc;
			al_if_info.vht_capab.tx_stbc = al_if_info.ht_capab.tx_stbc;
			al_if_info.vht_capab.rx_stbc = al_if_info.ht_capab.rx_stbc;
			al_if_info.vht_capab.vht_oper_bandwidth = imcp_apconfig_data->if_info[j].vht_capab.vht_oper_bandwidth;
			al_if_info.vht_capab.seg0_center_freq =  imcp_apconfig_data->if_info[j].vht_capab.seg0_center_freq;
			al_if_info.vht_capab.gi_80 = imcp_apconfig_data->if_info[j].vht_capab.gi_80;


	


            /* hack - NMS corrupts the dca_list and channels with fixed configuration.
             * Till we have a fix from NMS we avoid any updates from NSM on the dca_list parameters
             */
#if 1
            al_if_info.dca_list_count = imcp_apconfig_data->if_info[j].dca_list_count;

            if (al_if_info.dca_list_count > 0)
            {
               memset(al_if_info.dca_list, 0, sizeof(al_if_info.dca_list));
               memcpy(al_if_info.dca_list, imcp_apconfig_data->if_info[j].dca_list, al_if_info.dca_list_count * sizeof(int));
            }
#endif
            al_conf_set_if(al_conf_handle, i, &al_if_info);
         }
      }
   }

   al_conf_set_las_interval(al_conf_handle, imcp_apconfig_data->las_interval);
   al_conf_set_max_allowable_hops(al_conf_handle, imcp_apconfig_data->max_allowable_hops);

   al_conf_set_name(al_conf_handle, imcp_apconfig_data->name, imcp_apconfig_data->name_length);

   memcpy(preferred_parent.bytes, imcp_apconfig_data->preferred_parent, 6);
   preferred_parent.length = 6;

   al_conf_set_preferred_parent(al_conf_handle, &preferred_parent);
   al_conf_set_rts_th(al_conf_handle, imcp_apconfig_data->rts_threshold);
   al_conf_set_signal_map(al_conf_handle, imcp_apconfig_data->signal_map);

   al_conf_put(al_conf_handle);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit : %s  : %d\n",__func__,__LINE__);

   return 0;
}


int write_vlan_config_info(int al_conf_handle)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter : %s  : %d\n",__func__,__LINE__);
   int                   len;
   int                   i;
   int                   j;
   int                   index;
   al_conf_vlan_info_t   al_conf_vlan_config_info;
   imcp_vlan_info_data_t *ivconf;

   len = imcp_vlan_config_info_data->vlan_count;

   al_conf_set_vlan_count(al_conf_handle, len);

   memset(&al_conf_vlan_config_info, 0, sizeof(al_conf_vlan_info_t));
   strcpy(al_conf_vlan_config_info.name, "default");
   al_conf_vlan_config_info.tag             = imcp_vlan_config_info_data->tag;
   al_conf_vlan_config_info.dot1p_priority  = imcp_vlan_config_info_data->dot1p_priority;
   al_conf_vlan_config_info.dot11e_enabled  = imcp_vlan_config_info_data->dot11e_enabled;
   al_conf_vlan_config_info.dot11e_category = imcp_vlan_config_info_data->dot11e_category;

   al_conf_set_vlan(al_conf_handle, 0, &al_conf_vlan_config_info);

   for (index = 0; index < len - 1; index++)
   {
      ivconf = &imcp_vlan_config_info_data->imcp_vlan_info[index];

      memset(&al_conf_vlan_config_info, 0, sizeof(al_conf_vlan_info_t));

      strcpy(al_conf_vlan_config_info.name, ivconf->vlan_name);

      memcpy(al_conf_vlan_config_info.ip_address, ivconf->ip_address, 4);
      al_conf_vlan_config_info.tag             = ivconf->vlan_tag;
      al_conf_vlan_config_info.dot11e_enabled  = ivconf->dot11e_enabled;
      al_conf_vlan_config_info.dot1p_priority  = ivconf->dot1p_p;
      al_conf_vlan_config_info.dot11e_category = ivconf->dot11e_category;
      strcpy(al_conf_vlan_config_info.essid, ivconf->essid);

      al_conf_vlan_config_info.rts_th          = ivconf->rts_threshold;
      al_conf_vlan_config_info.frag_th         = ivconf->frag_threshold;
      al_conf_vlan_config_info.beacon_interval = ivconf->beacon_int;
      al_conf_vlan_config_info.service         = ivconf->service_type;
      al_conf_vlan_config_info.txpower         = ivconf->transmit_power;

      memset(&al_conf_vlan_config_info.security_info, 0, sizeof(al_security_info_t));

      copy_al_conf_security_info(&al_conf_vlan_config_info.security_info, &ivconf->security_info);

      al_conf_set_vlan(al_conf_handle, index + 1, &al_conf_vlan_config_info);
   }
   al_conf_put(al_conf_handle);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit : %s  : %d\n",__func__,__LINE__);

   return 0;
}


int copy_al_conf_security_info(al_security_info_t *security_info, al_security_info_t *src_security_info)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter : %s  : %d\n",__func__,__LINE__);
   int i, j;
   int len = 0;

   security_info->_security_none.enabled  = src_security_info->_security_none.enabled;
   security_info->_security_wep.enabled   = src_security_info->_security_wep.enabled;
   security_info->_security_wep.strength  = src_security_info->_security_wep.strength;
   security_info->_security_wep.key_index = src_security_info->_security_wep.key_index;


   if (src_security_info->_security_wep.strength == 40)
   {
      for (i = 0; i < AL_802_11_MAX_SECURITY_KEYS; i++)
      {
         memcpy(&security_info->_security_wep.keys[i].bytes, &src_security_info->_security_wep.keys[i].bytes, 5);
      }
   }
   else
   {
      memcpy(&security_info->_security_wep.keys[0].bytes, &src_security_info->_security_wep.keys[0].bytes, 13);
   }

   security_info->_security_rsn_psk.enabled = src_security_info->_security_rsn_psk.enabled;
   security_info->_security_rsn_psk.mode    = src_security_info->_security_rsn_psk.mode;


   len = src_security_info->_security_rsn_psk.key_buffer_len;
   if (len > 0)
   {
      //security_info->_security_rsn_psk.key_buffer = (unsigned char*)malloc(sizeof(len));
      memset(&security_info->_security_rsn_psk.key_buffer, 0, len + 1);
      memcpy(&security_info->_security_rsn_psk.key_buffer, src_security_info->_security_rsn_psk.key_buffer, len);
   }

   security_info->_security_rsn_psk.cipher_tkip       = src_security_info->_security_rsn_psk.cipher_tkip;
   security_info->_security_rsn_psk.cipher_ccmp       = src_security_info->_security_rsn_psk.cipher_ccmp;
   security_info->_security_rsn_psk.group_key_renewal = src_security_info->_security_rsn_psk.group_key_renewal;
   security_info->_security_rsn_radius.enabled        = src_security_info->_security_rsn_radius.enabled;
   security_info->_security_rsn_radius.mode           = src_security_info->_security_rsn_radius.mode;

   memcpy(security_info->_security_rsn_radius.radius_server, src_security_info->_security_rsn_radius.radius_server, 4);

   security_info->_security_rsn_radius.radius_port = src_security_info->_security_rsn_radius.radius_port;

   len = src_security_info->_security_rsn_radius.radius_secret_key_len;

   if (len > 0)
   {
      //security_info->_security_rsn_radius.radius_secret_key = (unsigned char*)malloc(sizeof(len));
      memset(&security_info->_security_rsn_radius.radius_secret_key, 0, len + 1);
      memcpy(&security_info->_security_rsn_radius.radius_secret_key, src_security_info->_security_rsn_radius.radius_secret_key, len);
   }

   security_info->_security_rsn_radius.cipher_tkip       = src_security_info->_security_rsn_radius.cipher_tkip;
   security_info->_security_rsn_radius.cipher_ccmp       = src_security_info->_security_rsn_radius.cipher_ccmp;
   security_info->_security_rsn_radius.group_key_renewal = src_security_info->_security_rsn_radius.group_key_renewal;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit : %s  : %d\n",__func__,__LINE__);
}


/*
 * Read acl.conf file, parse it and return parser handle i.e acl_conf_handle
 */
int read_acl_config_data()
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter : %s  : %d\n",__func__,__LINE__);
   int           offset;
   int           acl_conf_handle;
   int           fd;
   unsigned char *file_data;

   /**
    * Read in the config file into memory
    */

   fd = open(FILE_ACL_CONF, O_RDONLY);

   if (fd == -1)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : tError opening "FILE_ACL_CONF " for reading... : %s : %d\n",__func__,__LINE__);
      fprintf(stderr, "CONFIGD:\tError opening "FILE_ACL_CONF " for reading...\n");
      return -1;
   }

   offset = lseek(fd, 0, SEEK_END);

   if (offset == -1)
   {
      close(fd);
      fprintf(stderr, "CONFIGD:\tError seeking end of "FILE_ACL_CONF "...\n");
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : Error seeking end of "FILE_ACL_CONF "... %s : %d\n",__func__,__LINE__);
      return -1;
   }

   file_data = (unsigned char *)malloc(offset + 1);
   memset(file_data, 0, offset + 1);

   lseek(fd, 0, SEEK_SET);

   read(fd, file_data, offset);

   close(fd);

   acl_conf_handle = acl_conf_parse(AL_CONTEXT file_data);

   if (acl_conf_handle == 0)
   {
      free(file_data);
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : al_conf_parse returns null %s : %d\n",__func__,__LINE__);
      return -1;
   }

   free(file_data);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit : %s  : %d\n",__func__,__LINE__);
   return acl_conf_handle;
}


/*
 * Read dot11e.conf file, parse it and return parser handle i.e dot11e_conf_handle
 */
int read_dot11e_category_data()
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter : %s  : %d\n",__func__,__LINE__);
   int           offset;
   int           dot11e_conf_handle;
   int           fd;
   unsigned char *file_data;

   /**
    * Read in the config file into memory
    */

   fd = open(FILE_DOT11E_CONF, O_RDONLY);

   if (fd == -1)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : "FILE_DOT11E_CONF " for reading... : %s : %d\n",__func__,__LINE__);
      fprintf(stderr, "CONFIGD:\tError opening "FILE_DOT11E_CONF " for reading...\n");
      return -1;
   }

   offset = lseek(fd, 0, SEEK_END);

   if (offset == -1)
   {
      close(fd);
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : Error seeking end of "FILE_DOT11E_CONF" for reading : %s : %d\n",__func__,__LINE__);
      fprintf(stderr, "CONFIGD:\tError seeking end of "FILE_DOT11E_CONF "...\n");
      return -1;
   }

   file_data = (unsigned char *)malloc(offset + 1);
   memset(file_data, 0, offset + 1);

   lseek(fd, 0, SEEK_SET);

   read(fd, file_data, offset);

   close(fd);

   file_data[offset] = 0;

   dot11e_conf_handle = dot11e_conf_parse(AL_CONTEXT file_data);

   if (dot11e_conf_handle == 0)
   {
      free(file_data);
      al_print_log(AL_LOG_TYPE_ERROR,"\nError in dot11e_conf,returning null   : %d\n",__LINE__);
      return -1;
   }

   free(file_data);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit : %s  : %d\n",__func__,__LINE__);
   return dot11e_conf_handle;
}


/*
 * write data to /etc/sysconfig/network-devices/ifconfig.interfacename (mip0/wlan1) file
 */
int update_ifconfig_file()
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter : %s  : %d\n",__func__,__LINE__);
   FILE *fp;
   char command[1024];

   memset(command, 0, 1024);

   sprintf(command, "uci set network.mip.ipaddr=%d.%d.%d.%d",
           imcp_ipconfig_data->ip_address[0],
           imcp_ipconfig_data->ip_address[1],
           imcp_ipconfig_data->ip_address[2],
           imcp_ipconfig_data->ip_address[3]
           );
   EXECUTE_COMMAND(command);

   sprintf(command, "uci set network.mip.netmask=%d.%d.%d.%d",
           imcp_ipconfig_data->subnet_mask[0],
           imcp_ipconfig_data->subnet_mask[1],
           imcp_ipconfig_data->subnet_mask[2],
           imcp_ipconfig_data->subnet_mask[3]
           );

   EXECUTE_COMMAND(command);

   sprintf(command, "uci set network.mip.broadcast=%d.%d.%d.255",
           imcp_ipconfig_data->ip_address[0],
           imcp_ipconfig_data->ip_address[1],
           imcp_ipconfig_data->ip_address[2]
           );

   EXECUTE_COMMAND(command);

   EXECUTE_COMMAND("uci commit");

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit : %s  : %d\n",__func__,__LINE__);
   return 0;
}


/*
 * read data from /etc/sysconfig/network-devices/ifconfig.interfacename (mip0/wlan1) file
 * and update imcp_ipconfig_data
 */
int read_from_ifconfig_file()
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter : %s  : %d\n",__func__,__LINE__);
   FILE *fp;
   char buffer[80];
   int  bytes[4];
   char command[1024];

   memset(command, 0, 1024);

   sprintf(command, "uci get network.mip.ipaddr > /tmp/network");
   EXECUTE_COMMAND(command);

   fp = fopen("/tmp/network", "r");

   if (!fp)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : opening file failed %s : %d\n",__func__,__LINE__);
      return -1;
   }

   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"CONFIGD : INFO : Reading "FILE_IFCONFIG_MIP0" %s : %d\n",__func__,__LINE__);

   fscanf(fp, "%s", buffer);
   sscanf(buffer, "%d.%d.%d.%d", &bytes[0],
          &bytes[1],
          &bytes[2],
          &bytes[3]
          );
   imcp_ipconfig_data->ip_address[0] = bytes[0];
   imcp_ipconfig_data->ip_address[1] = bytes[1];
   imcp_ipconfig_data->ip_address[2] = bytes[2];
   imcp_ipconfig_data->ip_address[3] = bytes[3];
   fclose(fp);

   sprintf(command, "uci get network.mip.netmask > /tmp/network");
   EXECUTE_COMMAND(command);

   fp = fopen("/tmp/network", "r");

   if (!fp)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : opening file failed %s : %d\n",__func__,__LINE__);
      return -1;
   }

   fscanf(fp, "%s", buffer);
   sscanf(buffer, "%d.%d.%d.%d", &bytes[0],
          &bytes[1],
          &bytes[2],
          &bytes[3]
          );
   imcp_ipconfig_data->subnet_mask[0] = bytes[0];
   imcp_ipconfig_data->subnet_mask[1] = bytes[1];
   imcp_ipconfig_data->subnet_mask[2] = bytes[2];
   imcp_ipconfig_data->subnet_mask[3] = bytes[3];

   fclose(fp);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit : %s  : %d\n",__func__,__LINE__);
   return 0;
}


/*
 * write data to /etc/sysconfig/network file
 */
int update_network_file()
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter : %s  : %d\n",__func__,__LINE__);
   FILE *fp;
   char command[1024];

   memset(command, 0, 1024);


   sprintf(command, "uci set system.@system[0].hostname=%s", imcp_ipconfig_data->host_name);
   EXECUTE_COMMAND(command);

   sprintf(command, "uci set network.mip.gateway=%d.%d.%d.%d",
           imcp_ipconfig_data->gateway[0],
           imcp_ipconfig_data->gateway[1],
           imcp_ipconfig_data->gateway[2],
           imcp_ipconfig_data->gateway[3]
           );
   EXECUTE_COMMAND(command);
   EXECUTE_COMMAND("uci commit");

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit : %s  : %d\n",__func__,__LINE__);
   return 0;
}


/*
 * Read from /etc/sysconfig/network file and update data into imcp_ipconfig_data
 */
int read_from_network_file()
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter : %s  : %d\n",__func__,__LINE__);
   FILE *fp;
   char line[80];
   char buf[15];
   int  bytes[4];

   char command[1024];

   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"\nReading "FILE_NETWORK "\n");

   memset(command, 0, 1024);

   sprintf(command, "uci get system.@system[0].hostname > /tmp/network");
   EXECUTE_COMMAND(command);

   fp = fopen("/tmp/network", "r");

   if (!fp)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : opening file failed %s : %d\n",__func__,__LINE__);
      return -1;
   }

   fscanf(fp, "%s", line);
   sscanf(line, "%s", imcp_ipconfig_data->host_name);
   fclose(fp);

   sprintf(command, "uci get network.mip.gateway > /tmp/network");
   EXECUTE_COMMAND(command);

   fp = fopen("/tmp/network", "r");

   if (!fp)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : opening file failed %s : %d\n",__func__,__LINE__);
      return -1;
   }

   fscanf(fp, "%s", line);
   sscanf(line, "%d.%d.%d.%d", &bytes[0],
          &bytes[1],
          &bytes[2],
          &bytes[3]);
   imcp_ipconfig_data->gateway[0] = bytes[0];
   imcp_ipconfig_data->gateway[1] = bytes[1];
   imcp_ipconfig_data->gateway[2] = bytes[2];
   imcp_ipconfig_data->gateway[3] = bytes[3];

   fclose(fp);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit : %s  : %d\n",__func__,__LINE__);
   return 0;
}


int read_ipconfig_data_from_files()
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter : %s  : %d\n",__func__,__LINE__);
   if (read_from_network_file() != 0)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : failed reading from networkfile %s : %d\n",__func__,__LINE__);
      return -1;
   }

   if (read_from_ifconfig_file() != 0)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : failed reading from configfile %s : %d\n",__func__,__LINE__);
      return -1;
   }

   imcp_ipconfig_data->host_name_length = strlen(imcp_ipconfig_data->host_name);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit : %s  : %d\n",__func__,__LINE__);


   return 0;
}


/************************************ Post - Update Server Calls ******************************/

inf_file_data_t *read_inf_file(char *file_name)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter : %s  : %d\n",__func__,__LINE__);
   FILE *fp;
   struct inf_file_data *inf_file_data_entry;
   int  bytes_read = 0;
   int  file_size  = 0;
   int  ret        = 0;
   char buffer[1024];
   char *ptr;
   int  temp;


   fp = fopen(file_name, "r");

   if (fp == NULL)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR :failed opening file %s : %d\n",__func__,__LINE__);
      return NULL;
   }

   fseek(fp, 0, SEEK_END);
   file_size = ftell(fp);

   DEBUG(_DEBUG_LOW, "CONFIGD:	read_inf_file - file_size %d\n", file_size);


   rewind(fp);

   while (ret < file_size)
   {
      inf_file_data_entry = (struct inf_file_data *)malloc(sizeof(struct inf_file_data));

      memset(inf_file_data_entry->file_name, 0, sizeof(inf_file_data_entry->file_name));
      inf_file_data_entry->type = 0;
      memset(inf_file_data_entry->file_path, 0, sizeof(inf_file_data_entry->file_path));
      inf_file_data_entry->next_link = NULL;
      memset(buffer, 0, 1024);

      ret = fscanf(fp, "%s", buffer);
      DEBUG(_DEBUG_LOW, "CONFIGD:	read_inf_file - fscanf ret : %d\n", ret);

      if (ret != 1)
      {
         break;
      }

      ptr = (char *)strtok(buffer, ",");
      sscanf(ptr, "%s", inf_file_data_entry->file_name);

      ptr = (char *)strtok(NULL, ",");
      sscanf(ptr, "%d", &temp);

      inf_file_data_entry->type = temp;

      ptr = (char *)strtok(NULL, ",");
      sscanf(ptr, "%s", inf_file_data_entry->file_path);



      if (imcp_sw_update_req_data->inf_file_data_head != NULL)
      {
         inf_file_data_entry->next_link = imcp_sw_update_req_data->inf_file_data_head;
      }

      imcp_sw_update_req_data->inf_file_data_head = inf_file_data_entry;

      ret = ftell(fp);
      DEBUG(_DEBUG_LOW, "CONFIGD:	read_inf_file - ret : %d\n", ret);
      DEBUG(_DEBUG_LOW, "CONFIGD:	read_inf_file - Filename : %s Type : %d Filepath : %s\n", inf_file_data_entry->file_name, inf_file_data_entry->type, inf_file_data_entry->file_path);
   }

   fclose(fp);
   DEBUG(_DEBUG_LOW, "CONFIGD:	read_inf_file - reutrning  head\n");
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit : %s  : %d\n",__func__,__LINE__);
   return imcp_sw_update_req_data->inf_file_data_head;
}


int write_inf_file(char *file_name)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter : %s  : %d\n",__func__,__LINE__);
   FILE *fp;
   int  file_count = 0;

   inf_file_data_t *inf_file_data_temp = imcp_sw_update_req_data->inf_file_data_head;

   DEBUG(_DEBUG_LOW, "CONFIGD:	write_inf_file\n");

   fp = fopen(file_name, "w");

   if (inf_file_data_temp == NULL)
   {
      fprintf(fp, "");
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : no installation.inf found line %s : %d\n",__func__,__LINE__);
      return 0;
   }

   while (inf_file_data_temp != NULL)
   {
      fprintf(fp, "%s,%d,%s\n", inf_file_data_temp->file_name, inf_file_data_temp->type, inf_file_data_temp->file_path);
      DEBUG(_DEBUG_LOW, "CONFIGD:	write_inf_file - Filename : %s Type : %d Filepath : %s\n", inf_file_data_temp->file_name, inf_file_data_temp->type, inf_file_data_temp->file_path);

      inf_file_data_temp = inf_file_data_temp->next_link;
      file_count++;
   }

   fclose(fp);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit : %s  : %d\n",__func__,__LINE__);
   return file_count;
}


inf_file_data_t *remove_entry_from_inf_list(inf_file_data_t *cur)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter : %s  : %d\n",__func__,__LINE__);
   inf_file_data_t *temp, *prev;

   temp = imcp_sw_update_req_data->inf_file_data_head;

   if (cur == temp)
   {
      DEBUG(_DEBUG_LOW, "CONFIGD:	current is HEAD -> %s\n", cur->file_name);
      imcp_sw_update_req_data->inf_file_data_head = imcp_sw_update_req_data->inf_file_data_head->next_link;
      prev = imcp_sw_update_req_data->inf_file_data_head;
   }
   else
   {
      while (temp != cur)
      {
         DEBUG(_DEBUG_LOW, "CONFIGD:	temp is  -> %s\n", temp->file_name);
         prev = temp;
         temp = temp->next_link;
      }
      prev->next_link = temp->next_link;
   }
   DEBUG(_DEBUG_LOW, "CONFIGD: freeing temp   -> %s\n", temp->file_name);
   free(temp);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit : %s  : %d\n",__func__,__LINE__);
   return imcp_sw_update_req_data->inf_file_data_head;
}


/*
 * This function is never called, changes for Firmware 2.4.71
 * Previous working - configd always checked for firmware updates at start up. It spawned update server,
 *					  update server copied all firmware files to /root/updates
 *					  & killed configd as configd cant update itself.
 *					  it then copied configd & restarted configd.
 *
 * Present working - Configd receives update firmware packet, spawns update server,
 *					 update server copies all firmware files to /root/updates & does a post_fs_change
 *					 configd then sets reboot flag, on reboot fw_update.sh checks  for files in /root/updates.
 *					 if files are found, they are copied to resp dirs, post_fs_change is done & board is implicitly rebooted.
 */
int check_update()
{
   /*
    *	check update folder for any updates
    */

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter : %s  : %d\n",__func__,__LINE__);
   unsigned char mask;
   char          inf_file_path[1024];
   int           i;
   int           ret   = 0;
   int           found = 0;

   inf_file_data_t *inf_file_data_temp = NULL;

   EXECUTE_PRE_FS_CHANGE

      mask = 0;

   memset(inf_file_path, 0, 1024);

   strcpy(inf_file_path, update_path);

   strcat(inf_file_path, "installation.inf");

   DEBUG(_DEBUG_LOW, "\nCONFIGD :check_update function inf file %s\n ", inf_file_path);

   read_inf_file(inf_file_path);
   inf_file_data_temp = imcp_sw_update_req_data->inf_file_data_head;


   if (inf_file_data_temp == NULL)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : no installation.inf found %s : %d\n",__func__,__LINE__);
      return 0;
   }

   while (inf_file_data_temp != NULL)
   {
      mask = mask | inf_file_data_temp->type;
      inf_file_data_temp = inf_file_data_temp->next_link;
   }

   update_configd(mask);
   update_scp_web_oth_dmn(mask);
   update_map_and_drv(mask);

   /*
    *	Following call needs to be segregated according to files types that are updated
    */

   EXECUTE_POST_FS_CHANGE(POST_FS_PARAM_TYPE_BINARY)
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit : %s  : %d\n",__func__,__LINE__);
}


int update_configd(unsigned char mask)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter : %s  : %d\n",__func__,__LINE__);
   char update_file_path[1024];
   char inf_file_path[1024];
   char command[1024];
   char *file_name;
   char *con_file_path;
   int  i, file_count;

   inf_file_data_t *inf_file_data_cur = imcp_sw_update_req_data->inf_file_data_head;


   memset(update_file_path, 0, 1024);
   memset(inf_file_path, 0, 1024);
   memset(command, 0, 1024);

   if ((mask & MASK_CFD) != MASK_CFD)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : Error found %s : %d\n",__func__,__LINE__);
      return 0;
   }

   DEBUG(_DEBUG_LOW, "\nCONFIGD :update_configd\n ");

   while (inf_file_data_cur != NULL)
   {
      if (inf_file_data_cur->type == MASK_CFD)
      {
         con_file_path = inf_file_data_cur->file_path;
         break;
      }
      inf_file_data_cur = inf_file_data_cur->next_link;
   }

   EXECUTE_COMMAND("kill -9 `pidof watchd`");

   pid = fork();
   DEBUG(_DEBUG_LOW, "\nCONFIGD :pid = %d\n", pid);

   switch (pid)
   {
   case -1:
      perror("update_configd : fork error");
      exit(-1);
      break;

   case 0:

      DEBUG(_DEBUG_LOW, "\nCONFIGD :update_configd in child process restarter\n ");
      strcpy(update_file_path, update_path);
      if (inf_file_data_cur == NULL)
      {
         DEBUG(_DEBUG_LOW, "\nCONFIGD : update_configd : inf_data = NULL\n");
         return 0;
      }

      strcat(update_file_path, inf_file_data_cur->file_name);

      remove_entry_from_inf_list(inf_file_data_cur);

      memset(inf_file_path, 0, 1024);
      strcpy(inf_file_path, update_path);
      strcat(inf_file_path, "installation.inf");
      DEBUG(_DEBUG_LOW, "\nCONFIGD :write_inf_entry %s\n", inf_file_path);
      write_inf_file(inf_file_path);

      sleep(1);

      DEBUG(_DEBUG_LOW, "\nCONFIGD :update_configd /sbin/mesh_update_server -c %s %s", con_file_path, update_file_path);
      sprintf(command, FILE_MESH_UPDATE_SERVER);
      execl(command, command, "-c", con_file_path, update_file_path, NULL);

      break;

   default:
      DEBUG(_DEBUG_LOW, "\nCONFIGD :update_configd in parent process\n");
      close(sock);
      un_init_data();

      exit(0);
   }
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit : %s  : %d\n",__func__,__LINE__);
   /* call restarter function here*/
   // restart_configd(ini_file_data[i].file_name)
   //clean up any data structure used;
}


int update_map_and_drv(unsigned char mask)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter : %s  : %d\n",__func__,__LINE__);
   char update_file_path[1024];
   char command[1024];
   char *file_name;
   int  i, file_count;

   inf_file_data_t *inf_file_data_cur = imcp_sw_update_req_data->inf_file_data_head;

   if ((mask & MASK_DRV) != MASK_DRV)
   {
      if ((mask & MASK_MAP) != MASK_MAP)
      {
    	 al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : Error found %s : %d\n",__func__,__LINE__);
         return 0;
      }
   }

   i = 0;

   memset(command, 0, 256);
   memset(update_file_path, 0, 256);

   DEBUG(_DEBUG_LOW, "\nCONFIGD :update_map_and_drv\n");

   while (inf_file_data_cur != NULL)
   {
      if ((inf_file_data_cur->type == MASK_MAP) || (inf_file_data_cur->type == MASK_DRV))
      {
         strcpy(update_file_path, update_path);
         file_name = inf_file_data_cur->file_name;
         strcat(update_file_path, file_name);

         DEBUG(_DEBUG_LOW, "\nCONFIGD :update_map_and_drv cat %s > %s", update_file_path, inf_file_data_cur->file_path);
         sprintf(command, "cp %s %s", update_file_path, inf_file_data_cur->file_path);
         EXECUTE_COMMAND(command)

         DEBUG(_DEBUG_LOW, "\nCONFIGD :update_map_and_drv rm -f %s", update_file_path);

         sprintf(command, "rm -f %s", update_file_path);
         EXECUTE_COMMAND(command);

         inf_file_data_cur = remove_entry_from_inf_list(inf_file_data_cur);

         /*
          * if(inf_file_data_cur == NULL){
          *      break;
          * }
          */
         memset(update_file_path, 0, 1024);
         strcpy(update_file_path, update_path);
         strcat(update_file_path, "installation.inf");
         write_inf_file(update_file_path);
      }
      else
      {
         inf_file_data_cur = inf_file_data_cur->next_link;
      }
   }
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit : %s  : %d\n",__func__,__LINE__);
}


int update_scp_web_oth_dmn(unsigned char mask)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter : %s  : %d\n",__func__,__LINE__);
   char update_file_path[1024];
   char command[1024];
   char *file_name;
   int  i;

   inf_file_data_t *inf_file_data_cur  = imcp_sw_update_req_data->inf_file_data_head;
   inf_file_data_t *inf_file_data_prev = NULL;


   memset(command, 0, 256);
   memset(update_file_path, 0, 256);

   while (inf_file_data_cur != NULL)
   {
      if ((inf_file_data_cur->type == MASK_OTH) ||
          (inf_file_data_cur->type == MASK_WEB) ||
          (inf_file_data_cur->type == MASK_SCP) ||
          (inf_file_data_cur->type == MASK_DMN) ||
          (inf_file_data_cur->type == MASK_EXE))
      {
         memset(command, 0, sizeof(command));
         strcpy(update_file_path, update_path);

         file_name = inf_file_data_cur->file_name;
         strcat(update_file_path, file_name);
         DEBUG(_DEBUG_LOW, "\nCONFIGD :update_scp_web_oth_dmn update_file_path %s\n", update_file_path);

         DEBUG(_DEBUG_LOW, "\nCONFIGD :update_scp_web_oth_dmn cat %s > %s", update_file_path, inf_file_data_cur->file_path);
         sprintf(command, "cp %s %s", update_file_path, inf_file_data_cur->file_path);
         EXECUTE_COMMAND(command)
         memset(command, 0, sizeof(command));

         DEBUG(_DEBUG_LOW, "\nCONFIGD :update_scp chmod +x %s", inf_file_data_cur->file_path);
         sprintf(command, "chmod +x %s", inf_file_data_cur->file_path);
         EXECUTE_COMMAND(command)
         memset(command, 0, sizeof(command));

         if (inf_file_data_cur->type != MASK_EXE)
         {
            DEBUG(_DEBUG_LOW, "\nCONFIGD :update_scp_web_oth_dmn /root/dos2unix %s", inf_file_data_cur->file_path);
            sprintf(command, "/root/dos2unix %s", inf_file_data_cur->file_path);
            EXECUTE_COMMAND(command)
            memset(command, 0, sizeof(command));
         }
         else
         {
            DEBUG(_DEBUG_LOW, "\nCONFIGD :update_scp_web_oth_dmn %s&", inf_file_data_cur->file_path);
            sprintf(command, "%s&", inf_file_data_cur->file_path);
            EXECUTE_COMMAND(command)
            memset(command, 0, sizeof(command));
         }

         DEBUG(_DEBUG_LOW, "\nCONFIGD :update_scp_web_oth_dmn rm -f %s", update_file_path);
         sprintf(command, "rm -f %s", update_file_path);
         EXECUTE_COMMAND(command)

         inf_file_data_cur = remove_entry_from_inf_list(inf_file_data_cur);

         /*
          * if(inf_file_data_cur == NULL){
          *      break;
          * }
          */

         memset(update_file_path, 0, 1024);
         strcpy(update_file_path, update_path);
         strcat(update_file_path, "installation.inf");
         write_inf_file(update_file_path);
      }
      else
      {
         inf_file_data_cur = inf_file_data_cur->next_link;
      }
   }
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit : %s  : %d\n",__func__,__LINE__);
}


int delete_inf_file()
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter : %s  : %d\n",__func__,__LINE__);
   char inf_file_path[1024];
   char command[1024];

   inf_file_data_t *inf_file_data_temp = imcp_sw_update_req_data->inf_file_data_head;

   memset(inf_file_path, 0, 1024);

   strcpy(inf_file_path, update_path);

   strcat(inf_file_path, "installation.inf");

   DEBUG(_DEBUG_LOW, "\nCONFIGD :delete_inf_file rm -f %s", inf_file_path);
   sprintf(command, "rm -f %s", inf_file_path);
   EXECUTE_COMMAND(command)

   while (imcp_sw_update_req_data->inf_file_data_head != NULL)
   {
      free(inf_file_data_temp);
      inf_file_data_temp = inf_file_data_temp->next_link;
   }

   imcp_sw_update_req_data->inf_file_data_head = NULL;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit : %s  : %d\n",__func__,__LINE__);

   return 0;
}


int write_ack_timeout_config_data(int al_conf_handle)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter : %s  : %d\n",__func__,__LINE__);
   FILE              *fp;
   int               al_if_count, i, j;
   al_conf_if_info_t al_if_info;

   /*
    *	Find and update only available interface from the config file.
    */
   al_if_count = al_conf_get_if_count(al_conf_handle);

   for (i = 0; i < al_if_count; i++)
   {
      memset(&al_if_info, 0, sizeof(al_conf_if_info_t));
      al_conf_get_if(al_conf_handle, i, &al_if_info);
      al_conf_get_if(al_conf_handle, i, &old_al_if_info[i]);

      for (j = 0; j < imcp_ack_timeout_config_data->interface_count; j++)
      {
         if (!strcmp(al_if_info.name, imcp_ack_timeout_config_data->if_info[j].name))
         {
            al_if_info.ack_timeout = imcp_ack_timeout_config_data->if_info[j].ack_timeout;
            al_conf_set_if(al_conf_handle, i, &al_if_info);
         }
      }
   }

   al_conf_put(al_conf_handle);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit : %s  : %d\n",__func__,__LINE__);

   return 0;
}


/*
 *	Write reg domain config file to /etc/meshap.conf disk
 */
int write_reg_domain_country_code_config_data(int al_conf_handle)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter : %s  : %d\n",__func__,__LINE__);
   al_conf_set_regulatory_domain(al_conf_handle, imcp_reg_domain_config_data->reg_domain);
   al_conf_set_country_code(al_conf_handle, imcp_reg_domain_config_data->country_code);


   al_conf_put(al_conf_handle);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit : %s  : %d\n",__func__,__LINE__);
   return 0;
}


int write_hide_ssid_config_data(int al_conf_handle)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter : %s  : %d\n",__func__,__LINE__);
   FILE              *fp;
   int               al_if_count, i, j;
   al_conf_if_info_t al_if_info;

   /*
    *	Find and update only available interface from the config file.
    */
   al_if_count = al_conf_get_if_count(al_conf_handle);

   for (i = 0; i < al_if_count; i++)
   {
      memset(&al_if_info, 0, sizeof(al_conf_if_info_t));
      al_conf_get_if(al_conf_handle, i, &al_if_info);
      al_conf_get_if(al_conf_handle, i, &old_al_if_info[i]);

      for (j = 0; j < imcp_hide_ssid_config_data->interface_count; j++)
      {
         if (!strcmp(al_if_info.name, imcp_hide_ssid_config_data->if_info[j].name))
         {
            al_if_info.hide_ssid = imcp_hide_ssid_config_data->if_info[j].hide_ssid;
            al_conf_set_if(al_conf_handle, i, &al_if_info);
         }
      }
   }

   al_conf_put(al_conf_handle);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit : %s  : %d\n",__func__,__LINE__);

   return 0;
}


int write_acl_data(int acl_conf_handle)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter : %s  : %d\n",__func__,__LINE__);
   int             i;
   acl_conf_info_t info;
   int             ret;

   acl_conf_set_info_count(AL_CONTEXT acl_conf_handle, imcp_acl_config_data->count);

   for (i = 0; i < imcp_acl_config_data->count; i++)
   {
      memcpy(info.sta_mac, imcp_acl_config_data->entries[i].mac_address, 6);
      info.vlan_tag        = imcp_acl_config_data->entries[i].vlan_tag;
      info.allow           = imcp_acl_config_data->entries[i].allow;
      info.dot11e_enabled  = imcp_acl_config_data->entries[i].dot11e_enabled;
      info.dot11e_category = imcp_acl_config_data->entries[i].dot11e_category;

      ret = acl_conf_set_info(AL_CONTEXT acl_conf_handle, i, &info);
      if (ret == -1)
      {
         al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : Error while setting ACL info %s : %d\n",__func__,__LINE__);
         return ret;
      }
   }

   ret = acl_conf_put(AL_CONTEXT acl_conf_handle);
   if (ret == -1)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : Error while setting ACL info %s : %d\n",__func__,__LINE__);
      return ret;
   }

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit : %s  : %d\n",__func__,__LINE__);
   return 0;
}


int write_dot11e_category_data(int dot11e_conf_handle, config_validation_report_t *validation_report)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter : %s  : %d\n",__func__,__LINE__);
   int i;
   dot11e_conf_category_details_t category_info;
   int ret;

   dot11e_conf_set_category_info_count(AL_CONTEXT dot11e_conf_handle, imcp_dot11e_category_data->count);

   for (i = 0; i < imcp_dot11e_category_data->count; i++)
   {
      category_info.category        = imcp_dot11e_category_data->category_info[i].category;
      category_info.burst_time      = imcp_dot11e_category_data->category_info[i].burst_time;
      category_info.acwmin          = imcp_dot11e_category_data->category_info[i].acwmin;
      category_info.acwmax          = imcp_dot11e_category_data->category_info[i].acwmax;
      category_info.aifsn           = imcp_dot11e_category_data->category_info[i].aifsn;
      category_info.disable_backoff = imcp_dot11e_category_data->category_info[i].disable_backoff;

      ret = dot11e_conf_set_category_info(AL_CONTEXT dot11e_conf_handle, i, &category_info);
      if (ret == -1)
      {
         al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : Error while setting dot11e category info %s : %d\n",__func__,__LINE__);
         return ret;
      }
   }

   ret = dot11e_conf_put(AL_CONTEXT dot11e_conf_handle);
   if (ret == -1)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : Error while create config string from data %s : %d\n",__func__,__LINE__);
      return ret;
   }

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit : %s  : %d\n",__func__,__LINE__);
   return 0;
}

int validating_dot11e_cat(imcp_dot11e_category_details_t *category_info)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter : %s  : %d\n",__func__,__LINE__);
		if( (category_info->category == DOT11E_CATEGORY_TYPE_AC_BK && category_info->acwmin < 15) || 
			(category_info->category == DOT11E_CATEGORY_TYPE_AC_BE && category_info->acwmin < 15) || 
			(category_info->category == DOT11E_CATEGORY_TYPE_AC_VI && category_info->acwmin < 7) || 
			(category_info->category == DOT11E_CATEGORY_TYPE_AC_VO && category_info->acwmin < 3))
			{
				VAL_PRINTF("cwmin_val : %d\ncwmax_val : %d\ncategory : %d (0 - Background, 1 - Besteffort, 2 - Video, 3 - voice)\n",category_info->acwmin,category_info->acwmax,category_info->category);
      				al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : Invalid configurations : cwmin_val : %d\ncwmax_val : %d\ncategory : %d (0 - Background, 1 - Besteffort, 2 - Video, 3 - voice) %s : %d\n",category_info->acwmin,category_info->acwmax,category_info->category,__func__,__LINE__);
				return 1;
			}
			 
			if(((category_info->acwmin != 3) && (category_info->acwmin != 7) && (category_info->acwmin != 15) && 
			(category_info->acwmin != 31) && (category_info->acwmin != 63) && (category_info->acwmin != 127) && 
			(category_info->acwmin != 255) && (category_info->acwmin != 511) && (category_info->acwmin != 1023)) && 
			((category_info->acwmax != 3) && (category_info->acwmax != 7) && (category_info->acwmax != 15) && (category_info->acwmax != 31) 
			|| (category_info->acwmax != 63) && (category_info->acwmax != 127) && (category_info->acwmax != 255) && 
			(category_info->acwmax != 511) && (category_info->acwmax != 1023)))
			{
				VAL_PRINTF("cwmin_val : %d\ncwmax_val : %d\ncategory : %d (0 - Background, 1 - Besteffort, 2 - Video, 3 - voice)\n",category_info->acwmin,category_info->acwmax,category_info->category);
      				al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : Invalid configurations : cwmin_val : %d\ncwmax_val : %d\ncategory : %d (0 - Background, 1 - Besteffort, 2 - Video, 3 - voice) %s : %d\n",category_info->acwmin,category_info->acwmax,category_info->category,__func__,__LINE__);
				return 1;
			}
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit : %s  : %d\n",__func__,__LINE__);
}

int write_dot11e_if_config_data(int al_conf_handle)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter : %s  : %d\n",__func__,__LINE__);
   int               i;
   int               j;
   int               al_if_count;
   al_conf_if_info_t al_if_info;


   al_if_count = al_conf_get_if_count(AL_CONTEXT al_conf_handle);

   for (i = 0; i < imcp_dot11e_if_config_data->if_count; i++)
   {
      for (j = 0; j < al_if_count; j++)
      {
         al_conf_get_if(AL_CONTEXT al_conf_handle, j, &al_if_info);

         if (strcmp(al_if_info.name, imcp_dot11e_if_config_data->if_info[i].if_name) == 0)
         {
            al_if_info.dot11e_enabled  = imcp_dot11e_if_config_data->if_info[i].enabled;
            al_if_info.dot11e_category = imcp_dot11e_if_config_data->if_info[i].category_index;

            al_conf_set_if(AL_CONTEXT al_conf_handle, j, &al_if_info);
         }
      }
   }

   al_conf_put(al_conf_handle);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit : %s  : %d\n",__func__,__LINE__);
   return 0;
}


int write_priv_channel_config_data(int al_conf_handle)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter : %s  : %d\n",__func__,__LINE__);
   FILE              *fp;
   int               al_if_count, i, j;
   al_conf_if_info_t al_if_info;

   /*
    *	Find and update only available interface from the config file.
    */
   al_if_count = al_conf_get_if_count(al_conf_handle);

   for (i = 0; i < al_if_count; i++)
   {
      memset(&al_if_info, 0, sizeof(al_conf_if_info_t));
      al_conf_get_if(al_conf_handle, i, &al_if_info);

      if (!strcmp(al_if_info.name, imcp_priv_channel_data->if_info.name))
      {
         al_if_info.priv_channel_bw        = imcp_priv_channel_data->if_info.priv_channel_bw;
         al_if_info.priv_channel_ant_max   = imcp_priv_channel_data->if_info.priv_channel_ant_max;
         al_if_info.priv_channel_ctl       = imcp_priv_channel_data->if_info.priv_channel_ctl;
         al_if_info.priv_channel_max_power = imcp_priv_channel_data->if_info.priv_channel_max_power;
         al_if_info.priv_channel_count     = imcp_priv_channel_data->if_info.priv_channel_count;

         memset(al_if_info.priv_channel_numbers, 0, sizeof(unsigned char));
         memset(al_if_info.priv_frequencies, 0, sizeof(unsigned short));

         memcpy(al_if_info.priv_channel_numbers,
                imcp_priv_channel_data->if_info.priv_channel_numbers,
                sizeof(unsigned char) * imcp_priv_channel_data->if_info.priv_channel_count);

         memcpy(al_if_info.priv_frequencies,
                imcp_priv_channel_data->if_info.priv_frequencies,
                sizeof(unsigned short) * imcp_priv_channel_data->if_info.priv_channel_count);

         memcpy(al_if_info.priv_channel_flags,
                imcp_priv_channel_data->if_info.priv_channel_flags,
                sizeof(unsigned char) * imcp_priv_channel_data->if_info.priv_channel_count);


         al_conf_set_if(al_conf_handle, i, &al_if_info);
         break;
      }
   }

   al_conf_put(al_conf_handle);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit : %s  : %d\n",__func__,__LINE__);

   return 0;
}


/*
 * Read sip.conf file, parse it and return parser handle i.e sip_conf_handle
 */
sip_conf_handle_t read_sip_config_data()
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter : %s  : %d\n",__func__,__LINE__);
   int               offset;
   sip_conf_handle_t sip_conf_handle;
   int               fd;
   unsigned char     *file_data;

   /**
    * Read in the config file into memory
    */

   fd = open(FILE_SIP_CONF, O_RDONLY);

   if (fd == -1)
   {
      fprintf(stderr, "CONFIGD:\tError opening "FILE_SIP_CONF " for reading...\n");
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : Error opening "FILE_SIP_CONF " for reading... %s : %d\n",__func__,__LINE__);
      return 0;
   }

   offset = lseek(fd, 0, SEEK_END);

   if (offset == -1)
   {
      close(fd);
      fprintf(stderr, "CONFIGD:\tError seeking  of "FILE_ACL_CONF "...\n");
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : Error seeking  of "FILE_ACL_CONF "... %s : %d\n",__func__,__LINE__ );
      return 0;
   }

   file_data = (unsigned char *)malloc(offset);

   lseek(fd, 0, SEEK_SET);

   read(fd, file_data, offset);

   close(fd);

   sip_conf_handle = sip_conf_parse(AL_CONTEXT file_data);

   if (sip_conf_handle == 0)
   {
      free(file_data);
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : sip_conf_parse failed %s : %d\n",__func__,__LINE__);
      return 0;
   }

   free(file_data);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit : %s  : %d\n",__func__,__LINE__);
   return sip_conf_handle;
}


int write_sip_data(sip_conf_handle_t sip_conf_handle)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter : %s  : %d\n",__func__,__LINE__);
   int                 i;
   sip_conf_sta_info_t info;
   int                 ret;

   sip_conf_set_server_ip(AL_CONTEXT sip_conf_handle, imcp_sip_config_data->ip_address);
   sip_conf_set_sip_enabled(AL_CONTEXT sip_conf_handle, (int)imcp_sip_config_data->enabled);
   sip_conf_set_adhoc_only(AL_CONTEXT sip_conf_handle, (int)imcp_sip_config_data->adhoc_only);
   sip_conf_set_sip_port(AL_CONTEXT sip_conf_handle, imcp_sip_config_data->port);
   sip_conf_set_sta_count(AL_CONTEXT sip_conf_handle, (int)imcp_sip_config_data->count);

   for (i = 0; i < imcp_sip_config_data->count; i++)
   {
      memcpy(info.mac, imcp_sip_config_data->entries[i].mac_address, 6);
      sprintf(info.extn, "%d", imcp_sip_config_data->entries[i].extn);

      ret = sip_conf_set_sta_info(AL_CONTEXT sip_conf_handle, i, &info);
      if (ret == -1)
      {
         al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : write_sip_data : TRACE 5.3 %s : %d\n",__func__,__LINE__);
         return ret;
      }
   }

   ret = sip_conf_put(AL_CONTEXT sip_conf_handle);
   if (ret == -1)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : Error while create config string from data %s : %d\n",__func__,__LINE__);
      return ret;
   }

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit : %s  : %d\n",__func__,__LINE__);
   return 0;
}
