/********************************************************************************
* MeshDynamics
* --------------
* File     : configd.h
* Comments : Configd main header file
* Created  : 3/7/2005
* Author   : Anand Bhange
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  21 |8/22/2008 | Changes for FIPS and SIP                        |Abhijit |
* -----------------------------------------------------------------------------
* |  20 |1/17/2008 | Queued Retry bit corrected                      |Prachiti|
* -----------------------------------------------------------------------------
* |  19 |11/7/2007 | Changes for queued retry                        |Prachiti|
* -----------------------------------------------------------------------------
* |  18 |3/27/2007 | references to imcp_priv_channel_rr_data removed | Abhijit|
* -----------------------------------------------------------------------------
* |  17 |2/22/2007 | Added Effistream Req/Resp and Info packets      | Sriram |
* -----------------------------------------------------------------------------
* |  16 |01/01/2007| Added Private Channel implementation            |Prachiti|
* -----------------------------------------------------------------------------
* |  15 |5/12/2006 | Changes for Firmware Update                     | Bindu  |
* -----------------------------------------------------------------------------
* |  14 |3/15/2006 | Added .11e enabled & category to ACL            | Bindu  |
* |     |          | Removed vlan_name								  |        |
* -----------------------------------------------------------------------------
* |  13 |3/14/2006 | vlan_name added to acl_entry_t                  | Bindu  |
* -----------------------------------------------------------------------------
* |  12 |3/13/2006 | ACL list implemented                            | Sriram |
* -----------------------------------------------------------------------------
* |  11 |02/22/2006| dot11e packets added		                      |Bindu   |
* -----------------------------------------------------------------------------
* |  10 |02/20/2006| Config SQNR removed							  | Abhijit|
* -----------------------------------------------------------------------------
* |  9  |02/08/2006| Config SQNR and Hide SSID added				  | Sriram |
* -----------------------------------------------------------------------------
* |  8  |11/11/2005| ACK_Timeout added								  |Prachiti|
* -----------------------------------------------------------------------------
* |  7  |11/11/2005| Regulatory Domain and Country code added        |Prachiti|
* -----------------------------------------------------------------------------
* |  6  |6/9/2005  | TX Rate added to VLAN                           | Sriram |
* -----------------------------------------------------------------------------
* |  5  |5/19/2005 | fcc_certified,etsi_certified added to apconfig  | Abhijit|
* -----------------------------------------------------------------------------
* |  4  |5/17/2005 | Configd Watchdog packet added                   | Sriram |
* -----------------------------------------------------------------------------
* |  3  |5/8/2005  | Merged model/gps changes                        | Sriram |
* -----------------------------------------------------------------------------
* |  2  |4/15/2005 | extern def of _MAC_ADDRESS added                | Sriram |
* -----------------------------------------------------------------------------
* |  1  |03/07/2005| Default 802.11e Enabled added to vlan packet    | Anand  |
* -----------------------------------------------------------------------------
********************************************************************************/

#ifndef __IPCONFIG_H_
#define __IPCONFIG_H_

//RAMESH16MIG included header file for inet_addr
#include <arpa/inet.h>

#include "al.h"
#include "al_context.h"
#include "al_impl_context.h"
#include "al_802_11.h"
#include "al_conf.h"
#include <net/if.h>
#include "list.h"

extern unsigned char *_MAC_ADDRESS;
extern unsigned char *_BEGIN_SIGNATURE;
extern unsigned char *_VLANMAC_ADDRESS;
#define _DEBUG_ON                                        100                                      // always displayed
#define _DEBUG_OFF                                       0
#define _DEBUG_LOW                                       1                                        // only if Low or higher
#define _DEBUG_MEDIUM                                    2
#define _DEBUG_HIGH                                      3

#define AL_802_11_MAX_SECURITY_KEYS                      4
#define AL_802_11_MAX_KEY_LENGTH                         13

/*
 *	IMCP Packet types
 */
#define IMCP_RESET                                       9
#define IMCP_AP_CONFIGURATION_INFO                       11
#define IMCP_AP_CONFIG_REQUEST_RESPONSE                  12
#define IMCP_HW_INFO_REQUEST_RESPONSE							13
#define IMCP_AP_IP_CONFIGURATION_INFO                    15
#define IMCP_AP_IP_CONFIG_REQUEST_RESPONSE               16
#define IMCP_AP_START_IP_INTERFACE                       17
#define IMCP_SW_UPDATE_REQUEST                           18
#define IMCP_SW_UPDATE_RESPONSE                          19
#define IMCP_MESH_KEY_UPDATE                             22
#define IMCP_GENERAL_RESPONSE                            23
#define IMCP_VLAN_CONFIGURATION_INFO                     24
#define IMCP_CONFIG_REQUEST_RESPONSE                     25
#define IMCP_CONFIGD_WATCHDOG_REQUEST                    27
#define IMCP_CONFIGD_WATCHDOG_RESPONSE                   28

#define IMCP_RESTORE_DEFAULTS_REQUEST                    34

#define IMCP_REG_DOMAIN_COUNTRY_CODE_REQUEST_RESPONSE    35
#define IMCP_REG_DOMAIN_COUNTRY_CODE_INFO                36

#define IMCP_ACK_TIMEOUT_REQUEST_RESPONSE                37
#define IMCP_ACK_TIMEOUT_INFO                            38
#define IMCP_STATION_INFO_REQUEST_RESPONSE					41
#define IMCP_DOT11E_CATEGORY_CONFIGURATION_INFO          43
#define IMCP_DOT11E_IF_CONFIGURATION_INFO                44

#define IMCP_HIDE_SSID_INFO                              46

#define IMCP_ACL_CONFIGURATION_INFO                      47

#define IMCP_IF_PRIVATE_CHANNEL_INFO_REQUEST_RESPONSE    50
#define IMCP_IF_PRIVATE_CHANNEL_INFO                     51

#define IMCP_EFFISTREAM_REQUEST_RESPONSE                 52
#define IMCP_EFFISTREAM_INFO                             53

#define IMCP_AP_SIP_CONFIGURATION_INFO                   61

#define IMCP_MAC_ADDR_REQUEST_RESPONSE							62
#define IMCP_MAC_ADDR_INFO_REQUEST								63

#define IMCP_MGMT_GW_CONFIG_REQUEST								64
#define IMCP_MGMT_GW_CONFIG_RESPONSE							65
/*
 *	IMCP Packet request / response types
 */
#define TYPE_REQUEST                                     0
#define TYPE_RESPONSE                                    1


/*
 *	IMCP Reset types
 */
#define RESET_TYPE_REBOOT_SYSTEM    1
#define RESET_TYPE_RESTART_MESH     2
#define RESET_TYPE_HALT_MESH			3


#define IMCP_SERVER_PORT            0xdede

#define IMCP_DSTYPE_802_3           1


/*
 *  IMCP SW RESPONSE TYPE
 */

#define RESPONSE_NOT_READY    0
#define RESPONSE_READY        1
#define INVALID_USER          2

/*
 * IMCP config request types
 */

#define         TYPE_AP_CONFIG_REQUEST             0x01
#define         TYPE_AP_IP_CONFIG_REQUEST          0x02
#define         TYPE_AP_VLAN_CONFIG_REQUEST        0x04
#define         TYPE_AP_DOT11E_CATEGORY_REQUEST    0x08
#define         TYPE_AP_DOT11E_CONFIG_REQUEST      0x10
#define         TYPE_IF_HIDDEN_SSID_REQUEST        0x20
#define         TYPE_ACL_CONFIG_REQUEST            0x40
#define         TYPE_EXTEND_TO_BYTE_2              0x80

/*
 *	following definitions are for byte2 of rewuest types
 *	byte1=0x80 for using byte2
 */
#define         TYPE_SIP_CONFIG_REQUEST            0x01


/*
 *	ICMP Software update mask
 */

#define MASK_CFD    1
#define MASK_DRV    2
#define MASK_MAP    4
#define MASK_SCP    8
#define MASK_WEB    16
#define MASK_OTH    32
#define MASK_EXE    64
#define MASK_DMN    128

/**
 *	Security info types
 */

#define SECURITY_INFO_TYPE_DEFAULT       1
#define SECURITY_INFO_TYPE_WEP           2
#define SECURITY_INFO_TYPE_RSN_PSK       3
#define SECURITY_INFO_TYPE_RSN_RAD       4

#define CURRENT_EXPECTED_MAX_IF_COUNT    8
#define MAC_ADDRESS_LENGTH               6


#define IMCP_ID_LEN_SIZE                        	(unsigned short)(4)
#define IMCP_UPDATED_RESPONSE_FIXED_SIZE            9 /*Includes mac, res_type, packet_type, res_id*/
#define IMCP_PARAM_NAME_LEN_SIZE					1 /*Includes 1byte for param_name_len*/
#define IMCP_PARAM_VALUE_LEN_SIZE					1 /*Include 1byte for param_value_len field*/
#define IMCP_PARAM_IF_NAME_LEN_SIZE					1 /*Include 1byte for if_name_len size*/

#define IMCP_LEN_MASK                           	(unsigned short)(0x7fff)
#define IMCP_EXT_MASK                           	(unsigned short)(0x8000)

#define IMCP_DEFAULT_FLAG_VALUE                 	(unsigned short)(0)

#define IMCP_IF_802_3_INFO_ID				(unsigned short)(2)
#define IMCP_IF_802_11_INFO_ID				(unsigned short)(3)
#define IMCP_IF_802_15_4_INFO_ID			(unsigned short)(4)
#define IMCP_IP_CONFIG_INFO_ID				(unsigned short)(17)
#define IMCP_SW_UPDATE_REQUEST_INFO_ID			(unsigned short)(19)
#define IMCP_MESH_IMCP_KEY_UPDATE_INFO_ID		(unsigned short)(23)
#define IMCP_VLAN_CONFIG_INFO_ID			(unsigned short)(25)
#define IMCP_CONFIGD_WATCHDOG_REQUEST_INFO_ID		(unsigned short)(27)
#define IMCP_REG_DOMAIN_REQ_RES_INFO_ID			(unsigned short)(35)
#define IMCP_REG_DOMAIN_INFO_ID				(unsigned short)(36)
#define IMCP_ACK_TIMEOUT_REQ_RES_INFO_ID		(unsigned short)(37)
#define IMCP_ACK_TIMEOUT_INFO_ID			(unsigned short)(38)
#define IMCP_DOT11E_CATEGORY_INFO_ID			(unsigned short)(43)
#define IMCP_DOT11E_CONFIG_INFO_ID			(unsigned short)(44)
#define IMCP_PRIVATE_CHANNEL_INFO_ID            	(unsigned short)(46)
#define IMCP_EFFISTREAM_REQ_RES_INFO_ID			(unsigned short)(47)
#define IMCP_EFFISTREAM_INFO_ID				(unsigned short)(48)
#define IMCP_SIP_CONFIG_INFO_ID				(unsigned short)(56)
#define IMCP_AP_CONFIG_INFO_REQ_RES_INFO_ID		(unsigned short)(58) 
#define IMCP_HIDE_SSID_CONFIG_INFO_ID			(unsigned short)(59)
#define IMCP_HIDE_SSID_CONFIG_INFO_REQ_RES_INFO_ID	(unsigned short)(60)
#define IMCP_CONFIG_UPDATED_ID				(unsigned short)(61)
#define IMCP_ACL_CONFIG_INFO_ID				(unsigned short)(62)
#define IMCP_ACL_CONFIG_INFO_REQ_RES_INFO_ID    	(unsigned short)(63)
#define IMCP_SIP_CONFIG_INFO_REQ_RES_INFO_ID    	(unsigned short)(64)
#define IMCP_PRIVATE_CHANNEL_INFO_REQ_RES_INFO_ID	(unsigned short)(65)
#define IMCP_MAC_ADDR_REQUEST_RESPONSE_INFO_ID		(unsigned short)(66)
#define IMCP_SW_UPDATE_REQUEST_RESPONSE_INFO_ID		(unsigned short)(67)
#define IMCP_MGMT_GW_CONFIG_INFO_ID						(unsigned short) (68)

#define IMCP_IF_802_11_EXT_11N_INFO_ID			(unsigned short)(111)
#define IMCP_IF_802_11_EXT_11N_11AC_INFO_ID		(unsigned short)(112)
#define IMCP_IP_CONFIG_FIXED_LEN			(unsigned short)(121)
#define IMCP_FIXED_ETH_LEN				(unsigned short)(119)
#define FIXED_802_11_SIZE 				(unsigned short)(127)

#define IMCP_MSG_PROCESS_RECEIVE_ID_AND_LEN(buffer_ptr,node_id,node_len)     {\
                                                                memcpy(&node_id, buffer_ptr, sizeof(unsigned short));\
                                                                node_id = al_impl_le16_to_cpu(node_id);\
                                                                al_print_log(AL_CONTEXT AL_LOG_TYPE_DEBUG," Receive_node_id : %hd func : %s LINE : %d\n",node_id,__func__,__LINE__);\
                                                                buffer_ptr += sizeof(unsigned short);\
                                                                memcpy(&node_len, buffer_ptr, sizeof(unsigned short));\
                                                                node_len = al_impl_le16_to_cpu(node_len);\
                                                                if(node_len & IMCP_EXT_MASK)\
                                                                {\
                                                                        al_print_log(AL_CONTEXT AL_LOG_TYPE_DEBUG,"Extension bit is setted func : %s LINE : %d\n",__func__,__LINE__);\
                                                                }\
                                                                node_len = node_len & IMCP_LEN_MASK;\
                                                                al_print_log(AL_CONTEXT AL_LOG_TYPE_DEBUG," Receive_node_len : %hd func : %s LINE : %d\n",node_len,__func__,__LINE__); \
                                                                buffer_ptr += sizeof(unsigned short); \
                                                          }\

#define IMCP_MSG_PROCESS_SEND_ID_AND_LEN(buffer_ptr,node_id,node_len)    {\
                                                                node_id = al_impl_cpu_to_le16(node_id);\
                                                                memcpy(buffer_ptr, &node_id, sizeof(unsigned short));\
                                                                al_print_log(AL_CONTEXT AL_LOG_TYPE_DEBUG," sending Node_id : %hd func : %s LINE : %d\n",node_id,__func__,__LINE__);\
                                                                buffer_ptr += sizeof(unsigned short); \
                                                                node_len = al_impl_cpu_to_le16(node_len & IMCP_LEN_MASK);\
                                                                al_print_log(AL_CONTEXT AL_LOG_TYPE_DEBUG," sending Node_len : %hd func : %s LINE : %d\n",node_len,__func__,__LINE__);\
                                                                memcpy(buffer_ptr,&node_len, sizeof(unsigned short));\
                                                                buffer_ptr += 2;\
                                                        }\




#define CHECK_FOR_N_SUBTYPE_TO_ADD_HT_CAPABILITES(sub_type)  ((sub_type == AL_CONF_IF_PHY_SUB_TYPE_802_11_N_2_4GHZ)||\
        (sub_type == AL_CONF_IF_PHY_SUB_TYPE_802_11_N_5GHZ) || (sub_type == AL_CONF_IF_PHY_SUB_TYPE_802_11_BGN) || (sub_type == AL_CONF_IF_PHY_SUB_TYPE_802_11_AN))

#define CHECK_FOR_AC_SUBTYPE_TO_ADD_VHT_CAPABILITES(sub_type)  ((sub_type == AL_CONF_IF_PHY_SUB_TYPE_802_11_AC)||(sub_type == AL_CONF_IF_PHY_SUB_TYPE_802_11_ANAC)) 

#define IMCP_HT_CAP_FIXED_LEN     4+1+1+4+1+1+1+1+1+4+1 /*IMCP_ID_LEN_SIZE + ldpc + ht_bandwidth + sec_offset + gi_20 + gi_40 + tx_stbc +rx_stbc +gf_mode +max_amsdu_len + ht_capabilites_intolerent */	

#define IMCP_VHT_CAP_FIXED_LEN     4/*IMCP_ID_LEN_SIZE + ldpc + ht_bandwidth + sec_offset + gi_20 + gi_40 + tx_stbc +rx_stbc +gf_mode +max_amsdu_len + ht_capabilites_intolerent */	

#define IMCP_FRAM_AGRE_FIXED_LEN  1+4 /*ampdu_enable + max_amsdu_len*/

#define WEP_KEY_STRENGTH_40_OR_60_BIT 40
#define WEP_KEY_STRENGTH_104_OR_128_BIT 104 
	
struct connect_status
{
    int prev_assoc;
    int channel;
};
typedef struct connect_status connect_status_t;

struct imcp_reset
{
   unsigned char mac_address[6];
   unsigned char reset_type;
};

typedef struct imcp_reset   imcp_reset_data_t;

struct imcp_apconfig
{
   unsigned char     mac_address[6];
   unsigned char     request_response_type;
   unsigned char     response_id;
   unsigned char     preferred_parent[6];
   unsigned char     signal_map[8];
   unsigned char     heartbeat_interval;
   unsigned char     heartbeat_miss_count;
   unsigned char     hop_cost;
   unsigned char     max_allowable_hops;
   unsigned char     las_interval;
   unsigned char     cr_threshold;
   unsigned char     name_length;
   unsigned char     name[256];
   unsigned char     desc_length;
   unsigned char     description[256];
   unsigned char     x_coordinate_length;
   unsigned char     gps_x_coordinate[256];
   unsigned char     y_coordinate_length;
   unsigned char     gps_y_coordinate[256];
   unsigned char     essid_length;
   char              essid[256];
   unsigned int      rts_threshold;
   unsigned int      frag_threshold;
   unsigned int      beacon_interval;
   int               failover_enabled;
   int               power_on_default;
   unsigned char     server_ip_addr[128];
   unsigned char     dca;
   unsigned char     stay_awake_count;
   unsigned char     bridge_ageing_time;
   unsigned char     interface_count;
   unsigned char     fcc_certified_operation;
   unsigned char     etsi_certified_operation;
   unsigned int      ds_tx_rate;
   unsigned char     ds_tx_power;
   unsigned char     ds_ack_timeout;

   al_conf_if_info_t if_info[CURRENT_EXPECTED_MAX_IF_COUNT];
};

typedef struct imcp_apconfig   imcp_apconfig_data_t;


struct imcp_apconfig_rr
{
   unsigned char mac_address[6];
   unsigned char request_response_type;
   unsigned char response_id;
};

typedef struct imcp_apconfig_rr   imcp_apconfig_rr_data_t;



struct imcp_ipconfig
{
   unsigned char mac_address[6];
   unsigned char request_response_type;
   unsigned char response_id;
   unsigned char host_name_length;
   char          host_name[256];
   unsigned char ip_address[15];
   unsigned char subnet_mask[15];
   unsigned char gateway[15];
};

typedef struct imcp_ipconfig   imcp_ipconfig_data_t;


struct imcp_ipconfig_rr
{
   unsigned char mac_address[6];
   unsigned char request_response_type;
   unsigned char response_id;
};

typedef struct imcp_ipconfig_rr   imcp_ipconfig_rr_data_t;


struct imcp_start_ip_if
{
   unsigned char mac_address[6];
   unsigned char interface_name_length;
   char          interface_name[256];
};

typedef struct imcp_start_ip_if   imcp_start_ip_if_data_t;


struct inf_file_data
{
   char                 file_name[256];
   unsigned char        type;
   char                 file_path[1024];
   struct inf_file_data *next_link;
};

typedef struct inf_file_data   inf_file_data_t;

struct imcp_sw_update_req
{
   unsigned char        sender_mac_address[6];
   unsigned char        response_id;
   unsigned int         sw_version;
   unsigned char        uid_len;
   unsigned char        uid[256];
   struct inf_file_data *inf_file_data_head;
};

typedef struct imcp_sw_update_req   imcp_sw_update_req_data_t;


struct imcp_mesh_key_update
{
   unsigned char mac_address[6];
   unsigned char response_id;
   unsigned char new_meshid_length;
   unsigned char new_meshid[256];
   unsigned char new_imcp_key_length;
   unsigned char new_imcp_key[256];
};

typedef struct imcp_mesh_key_update   imcp_mesh_key_update_data_t;

struct imcp_config_rr
{
   unsigned char mac_address[6];
   unsigned char request_response_type;
   unsigned char response_id;
   unsigned char type;
};

typedef struct imcp_config_rr   imcp_config_rr_data_t;


struct imcp_vlan_info_data
{
//	unsigned char			vlan_name_length;
   unsigned char      vlan_name[256];
   unsigned char      ip_address[4];
   short              vlan_tag;
   unsigned char      dot1p_p;
   unsigned char      dot11e_enabled;
   unsigned char      dot11e_category;
//	unsigned char			essid_len;
   char               essid[256];
   unsigned int       rts_threshold;
   unsigned int       frag_threshold;
   unsigned int       beacon_int;
   unsigned char      service_type;
   unsigned char      transmit_power;
   int                txrate;
   al_security_info_t security_info;
};

typedef  struct imcp_vlan_info_data   imcp_vlan_info_data_t;

struct imcp_vlan_config_info
{
   unsigned char         mac_address[6];
//	unsigned char			request_response_type;
//	unsigned char			response_id;
   unsigned char         vlan_count;
   short                 tag;
   unsigned char         dot1p_priority;
   unsigned char         dot11e_enabled;
   unsigned char         dot11e_category;
   imcp_vlan_info_data_t *imcp_vlan_info;
};

typedef struct imcp_vlan_config_info   imcp_vlan_config_info_data_t;


struct imcp_reg_domain_config_data
{
   unsigned char  mac_address[6];
   unsigned short reg_domain;
   unsigned short country_code;
   unsigned char  request_response_type;
   unsigned char  response_id;
};

typedef struct imcp_reg_domain_config_data   imcp_reg_domain_config_data_t;


struct imcp_reg_domain_rr
{
   unsigned char mac_address[6];
   unsigned char request_response_type;
   unsigned char response_id;
};

typedef struct imcp_reg_domain_rr   imcp_reg_domain_rr_data_t;

struct imcp_ack_timeout_config_data
{
   unsigned char     mac_address[6];
   unsigned char     interface_count;
   al_conf_if_info_t if_info[CURRENT_EXPECTED_MAX_IF_COUNT];
   unsigned char     request_response_type;
   unsigned char     response_id;
};

typedef struct imcp_ack_timeout_config_data   imcp_ack_timeout_config_data_t;


struct imcp_ack_timeout_rr
{
   unsigned char mac_address[6];
   unsigned char request_response_type;
   unsigned char response_id;
};

typedef struct imcp_ack_timeout_rr   imcp_ack_timeout_rr_data_t;

struct imcp_hide_ssid_config_data
{
   unsigned char     mac_address[6];
   unsigned char     interface_count;
   al_conf_if_info_t if_info[CURRENT_EXPECTED_MAX_IF_COUNT];
   unsigned char     request_response_type;
   unsigned char     response_id;
};

typedef struct imcp_hide_ssid_config_data   imcp_hide_ssid_config_data_t;

/** 802.11e Categories
 *
 *	AC_BK    = 0		(background)
 *	AC_BE	= 1		(best_effort)
 *	AC_VI	= 2		(video)
 *	AC_VO	= 3		(voice)
 */

struct imcp_dot11e_category_details
{
   unsigned char  category;
   unsigned short acwmin;
   unsigned short acwmax;
   unsigned char  aifsn;
   unsigned char  disable_backoff;
   unsigned int   burst_time;
};

typedef struct imcp_dot11e_category_details   imcp_dot11e_category_details_t;

struct imcp_dot11e_category
{
   unsigned char                  mac_address[6];
   unsigned char                  request_response;
   unsigned char                  response_id;
   unsigned char                  count;
   imcp_dot11e_category_details_t category_info[4];                     /* 802.11e Category Info */
};

typedef struct imcp_dot11e_category   imcp_dot11e_category_t;


struct imcp_dot11e_if_config_details
{
   unsigned char if_name[256];
   int           if_index;
   unsigned char if_essid[256];
   int           category_index;
   int           enabled;
};

typedef struct imcp_dot11e_if_config_details   imcp_dot11e_if_config_details_t;

struct imcp_dot11e_if_config
{
   unsigned char                   mac_address[6];
   unsigned char                   request_response;
   unsigned char                   response_id;
   unsigned char                   if_count;
   imcp_dot11e_if_config_details_t if_info[CURRENT_EXPECTED_MAX_IF_COUNT];
};

typedef struct imcp_dot11e_if_config   imcp_dot11e_if_config_t;

#define IMCP_ACL_ENTRY_GET_VLAN_TAG(s)               ((s) & 0x0FFF)
#define IMCP_ACL_ENTRY_GET_ALLOW_BIT(s)              (((s) >> 12) & 0x1)
#define IMCP_ACL_ENTRY_GET_DOT11E_ENABLED_BIT(s)     (((s) >> 13) & 0x1)
#define IMCP_ACL_ENTRY_GET_DOT11E_CATEGORY_BIT(s)    (((s) >> 14) & 0x3)


struct imcp_acl_entry
{
   unsigned char  mac_address[6];
   unsigned short vlan_tag;
   unsigned char  allow;
   unsigned char  dot11e_enabled;
   unsigned char  dot11e_category;
};

typedef struct imcp_acl_entry   imcp_acl_entry_t;

struct imcp_acl_config
{
   unsigned char    mac_address[6];
   unsigned char    request_response;
   unsigned char    response_id;
   unsigned char    count;
   imcp_acl_entry_t *entries;
};

typedef struct imcp_acl_config   imcp_acl_config_t;

struct imcp_priv_channel_config
{
   unsigned char     mac_address[6];
   unsigned char     request_response;
   unsigned char     response_id;
   al_conf_if_info_t if_info;
};

typedef struct imcp_priv_channel_config   imcp_priv_channel_config_t;

struct imcp_effistream_rr
{
   unsigned char mac_address[6];
   unsigned char request_response_type;
   unsigned char response_id;
};
typedef struct imcp_effistream_rr   imcp_effistream_rr_data_t;

struct al_conf_effistream_criteria;

struct imcp_effistream_config
{
   unsigned char                      mac_address[6];
   unsigned char                      request_response;
   unsigned char                      response_id;
   struct al_conf_effistream_criteria *criteria_root;
};
typedef struct imcp_effistream_config   imcp_effistream_config_t;


#define IMCP_EFFISTREAM_ACTION_OPTION_NO_ACK          0x01
#define IMCP_EFFISTREAM_ACTION_OPTION_DROP            0x02
#define IMCP_EFFISTREAM_ACTION_OPTION_QUEUED_RETRY    0x04

struct imcp_sip_entry
{
   unsigned char extn;
   unsigned char mac_address[6];
};

typedef struct imcp_sip_entry   imcp_sip_entry_t;

struct imcp_sip_config
{
   unsigned char    enabled;
   unsigned char    adhoc_only;
   unsigned char    mac_address[6];
   unsigned char    request_response;
   unsigned char    response_id;
   unsigned char    ip_address[4];
   unsigned short   port;
   unsigned char    count;
   imcp_sip_entry_t *entries;
};

typedef struct imcp_sip_config   imcp_sip_config_t;

#define MAC_SIZE		6
#define MAX_RADIOS 	6
#define MAX_VLANS		5
#define MAX_APPS		4

struct radio_mac
{
	unsigned char if_len;
	unsigned char if_name[IFNAMSIZ];
	unsigned char mac[MAC_SIZE];
};

struct vlan_mac
{
	unsigned char if_len;
	unsigned char if_name[IFNAMSIZ];
	unsigned char mac[MAC_SIZE];
};
struct app_mac
{
	unsigned char if_len;
	unsigned char app_name[IFNAMSIZ];
	unsigned char mac[MAC_SIZE];
};
struct imcp_mac_addr_req_resp
{
	unsigned char rCount;
	unsigned char vCount;
	unsigned char aCount;
	unsigned short node_id;
	unsigned short node_len;
	char board_mac[MAC_SIZE];
	struct radio_mac raddr[MAX_RADIOS];
	struct vlan_mac vaddr[MAX_VLANS];
	struct app_mac app_addr[MAX_APPS];
};

typedef struct imcp_mac_addr_req_resp imcp_mac_addr_req_resp_t;

enum actions
{
	FW_GET_KEY,
	FW_SET_KEY,
	FW_UPGRADE_INIT,
	FW_UPGRADE_STARTING,
	FW_DOWNLOAD_FAILED,
	FW_UPGRADE_FAILED,
	FW_UNKNOWN_ACTION,
	FW_UPGRADE_TIMEOUT
};

struct mgmt_gw_info_req {
	unsigned short node_id;
	unsigned short node_len;
	unsigned char	mac_address[6];
	unsigned char	mgmt_gw_enable;
	unsigned char	mgmt_gw_addr_len;
	unsigned char	mgmt_gw_addr[256];
	unsigned short	mgmt_gw_cert_len;
	unsigned char	mgmt_gw_cert_path;
	unsigned short	mgmt_gw_key_len;
	unsigned char	mgmt_gw_key[];
};
typedef struct mgmt_gw_info_req mgmt_gw_info_req_t;

struct imcp_sw_update_request
{
	int action;
	int username_len;
	int filepath_len;
	char username[32];
	char filepath[256];	
};
typedef struct imcp_sw_update_request imcp_sw_update_request_t;

struct imcp_sw_update_response
{
	unsigned short node_id;
	unsigned short node_len;
	char board_mac[MAC_SIZE];
	int action;
	unsigned int key_len;
	unsigned char pub_key[512];
};
typedef struct imcp_sw_update_response imcp_sw_update_response_t;

struct imcp_sw_update_data_addr
{
	struct list_head list;
	char *buffer;
	struct sockaddr_in sysupgrd_socket;
	int pkt_type;
};
typedef struct imcp_sw_update_data_addr imcp_sw_update_data_addr_t;

#define RUNTIME_VALID_QA_TEST 0

#if RUNTIME_VALID_QA_TEST
#define MAX_NODE_DESC_LEN_VALUE		25
#define MAX_NODE_NAME_LEN_VALUE		25
#define MAX_FRAG_TH_VALUE			2000
#define MAX_RTS_TH_VALUE			2000
#define MAX_TX_POWER_VALUE			50
#define MAX_VLAN_TAG_VALUE			400
#define MAX_ESSID_LEN_VALUE			32
#define MAX_VLAN_NAME_VALUE			32
#define MAX_VLAN_DOT1P_VALUE		4
#define MAX_HB_VALUE				50
#else
#define MAX_NODE_DESC_LEN_VALUE		255
#define MAX_NODE_NAME_LEN_VALUE		255
#define MAX_FRAG_TH_VALUE			2346
#define MAX_RTS_TH_VALUE			2347
#define MAX_TX_POWER_VALUE			100
#define MAX_VLAN_TAG_VALUE			4095
#define MAX_ESSID_LEN_VALUE			32
#define MAX_VLAN_NAME_VALUE			32
#define MAX_VLAN_DOT1P_VALUE		7
#define MAX_HB_VALUE				100
#endif
struct config_validation_report
{
	unsigned char       category_id;
	unsigned char       if_name_len;
	char                if_name[256];
	unsigned char       param_name_len;
	char                param_name[256];
	unsigned char       value_len;
	unsigned char       value[256];
};

typedef struct config_validation_report config_validation_report_t;

enum validation_error_codes
{
	SUCCESS         =   0,
	INVALID,
	OUT_OF_RANGE,
	UNSUPPORTED,
	NODE_CONFIG_FAILURE,
	NODE_INTERFACE_CONFIG_FAIL
};

typedef enum config_category_id
{
	NODE_CONFIG =0,
	IF_CONFIG
} CONFIG_CATEGORY_ID;

typedef enum if_param_id_list
{
	IF_PHY_TYPE = 0,
	IF_SUB_PHY_TYPE,
	IF_USE_TYPE,
	IF_CHANNEL,
	IF_DCA_CH_LIST,
	IF_ESSID_LENGTH,
	IF_TX_POWER,
	IF_TX_RATE,
	IF_RTS_TH,
	IF_FRAG_TH,
	IF_VLAN_TAG,
	IF_VLAN_DOT1P_PRIORITY,
	IF_VLAN_ESSID,
	IF_SECURITY,
    WEP_ENABLED_ERROR_MSG,
	IF_HT_CAP_LDPC,
	IF_HT_CAP_CH_BANDWIDTH,
	IF_HT_CAP_CH_SEC_OFFSET,
	IF_HT_CAP_GI_20,
	IF_HT_CAP_GI_40,
	IF_HT_CAP_TX_STBC,
	IF_HT_CAP_RX_STBC,
	IF_HT_CAP_GF_MODE,
	IF_HT_CAP_40MZ_INTOLERANT,
	IF_HT_CAP_AMPDU,
	IF_HT_CAP_AMSDU,
	IF_VHT_CAP_MAX_MPDU_LENGTH,
	IF_VHT_CAP_GI_80,
	IF_80MHZ_CH,
	NODE_NAME_LENGTH,
	NODE_DESCRIPTION_LENGTH,
	NODE_ESSID_LENGTH,
	NODE_FRAG_TH,
	NODE_RTS_TH,
	NODE_HB_INTERVAL,
	NODE_VLAN_COUNT,
	NODE_VLAN_NAME_LENGTH,
	NODE_VLAN_ESSID_LENGTH,
	NODE_DOT11E_CAT,
	NODE_PREFERRED_PARENT,
	NODE_GPS_X_COORDINATE,
	NODE_GPS_Y_COORDINATE,
	TDDI_RESPONSE_INVALID,

	IF_PARAM_ID_MAX
} IF_PARAM_ID;

struct param_id_name_map_table {
	unsigned char   param_id;
	char            param_name[256];
};

typedef struct param_id_name_map_table param_id_name_map_table_t;

typedef enum config_value_type
{
	INTEGER = 0,
	UNSIGNEDCHAR,
	UNSIGNEDSHORT,
	STRING
} CONFIG_VAL_TYPE;


//SPAWAR
enum drv_type
{
   ATH_5K_DRIVER = 5,
};

void set_ath_debug_level(enum drv_type type);
//SPAWAR_END

int un_init_data();
int update_configd(unsigned char mask);
int update_map_and_drv(unsigned char mask);
int update_scp_web_oth_dmn(unsigned char mask);

#endif
