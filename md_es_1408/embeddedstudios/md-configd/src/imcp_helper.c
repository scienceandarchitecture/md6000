/********************************************************************************
 * MeshDynamics
 * --------------
 * File     : imcp_helper.c
 * Comments : imcp packet functions
 * Created  : 10/15/2004
 * Author   : Abhijit Ayarekar
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
 *
 * File Revision History
 * -----------------------------------------------------------------------------
 * | No  |Date      |  Comment                                        | Author |
 * -----------------------------------------------------------------------------
 * |  44 |8/27/2008 | Used common configuration parser routines       | Sriram |
 * -----------------------------------------------------------------------------
 * |  43 |8/22/2008 | Changes for FIPS and SIP                        |Abhijit |
 * -----------------------------------------------------------------------------
 * |  42 |1/22/2008 | Board GPS coordinates updated on APCONFIG       | Sriram |
 * -----------------------------------------------------------------------------
 * |  41 |11/7/2007 | Changes for queued retry                        |Prachiti|
 * -----------------------------------------------------------------------------
 * |  40 |6/6/2007  | bitrate added in effistream                     |Prachiti|
 * -----------------------------------------------------------------------------
 * |  39 |3/27/2007 | references to imcp_priv_channel_rr_data removed | Abhijit|
 * -----------------------------------------------------------------------------
 * |  38 |3/21/2007 | Number of reserved bytes changes to 5           | Sriram |
 * -----------------------------------------------------------------------------
 * |  37 |3/9/2007  | Address Filtering bug fixed in custom channel   | Abhijit|
 * -----------------------------------------------------------------------------
 * |  36 |2/23/2007 | Added MAC address filtering to various packets  | Sriram |
 * -----------------------------------------------------------------------------
 * |  35 |2/23/2007 | Added Effistream Req/Resp and Info packets      | Sriram |
 * -----------------------------------------------------------------------------
 * |  34 |01/01/2007| Added Private Channel implementation            |Prachiti|
 * -----------------------------------------------------------------------------
 * |  33 |9/25/2006 | IMCP key mismatch message removed               | Sriram |
 * -----------------------------------------------------------------------------
 * |  32 |5/12/2006 | Changes for Firmware Update                     | Bindu  |
 * -----------------------------------------------------------------------------
 * |  31 |3/29/2006 | Restore defaults logic modified                 | Sriram |
 * -----------------------------------------------------------------------------
 * |  30 |3/15/2006 | Added .11e enabled & category bits ACL Packets  | Bindu  |
 * |     |          | Removed vlan_name								  |        |
 * -----------------------------------------------------------------------------
 * |  29 |3/14/2006 | Changes for ACL Packets                         | Bindu  |
 * -----------------------------------------------------------------------------
 * |  28 |3/13/2006 | ACL list implemented                            | Sriram |
 * -----------------------------------------------------------------------------
 * |  27 |03/03/2006| Rules changed for .1p & .11e packet processing  | Bindu  |
 * -----------------------------------------------------------------------------
 * |  26 |02/27/2006| MESH ID verification enhanced                   | Sriram |
 * -----------------------------------------------------------------------------
 * |  25 |02/22/2006| Changes for dot11e packets                      | Bindu  |
 * -----------------------------------------------------------------------------
 * |  24 |02/20/2006| Config SQNR removed							  | Sriram |
 * -----------------------------------------------------------------------------
 * |  23 |02/08/2006| Fix for Bug 1153				                  | Sriram |
 * -----------------------------------------------------------------------------
 * |  22 |02/08/2006| Config SQNR and Hide SSID added				  | Sriram |
 * -----------------------------------------------------------------------------
 * |  21 |11/11/2005| ACK_Timeout added								  |Prachiti|
 * -----------------------------------------------------------------------------
 * |  20 |11/11/2005| Regulatory Domain and Country code added        |Prachiti|
 * -----------------------------------------------------------------------------
 * | 19  |10/26/2005| mesh_id_length bug fix in mesh key update       | Abhijit|
 * -----------------------------------------------------------------------------
 * | 18  |9/15/2005 | Case insensitive MESHID checking                | Sriram |
 * -----------------------------------------------------------------------------
 * | 17  |6/16/2005 | Fixes on item 16                                | Sriram |
 * -----------------------------------------------------------------------------
 * | 16  |6/9/2005  | TX Rate added to VLAN                           | Sriram |
* -----------------------------------------------------------------------------
* | 15  |5/19/2005 | interface txrate changes						  | Abhijit|
* -----------------------------------------------------------------------------
* | 14  |5/19/2005 | ds_tx_rate,ds_tx_power changes				  | Abhijit|
* -----------------------------------------------------------------------------
* | 13  |5/19/2005 | fcc_certified,etsi_certified changes            | Abhijit|
* -----------------------------------------------------------------------------
* | 12  |5/17/2005 | Configd Watchdog packet added                   | Sriram |
* -----------------------------------------------------------------------------
* | 11  |5/8/2005  | Merged model/GPS changes                        | Sriram |
* -----------------------------------------------------------------------------
* | 10  |4/15/2005 | Misc changes + code cleanup                     | Sriram |
* -----------------------------------------------------------------------------
* |  9  |4/12/2005 | misc bug fixes                                  | Abhijit|
* -----------------------------------------------------------------------------
* |  8  |4/12/2005 | radius_secret_key chaged to array               |Prachiti|
* -----------------------------------------------------------------------------
* |  7  |03/07/2005| Default 802.11e Enabled added to vlan packet    | anand  |
* -----------------------------------------------------------------------------
* |  6  |2/15/2005 | vlan packets added						      | Amit   |
* -----------------------------------------------------------------------------
* |  5  |1/10/2005 | meshd configure called on reset                 | Sriram |
* -----------------------------------------------------------------------------
* |  4  |1/11/2005 | essid,etc added in interfaces				      |Prachiti|
* -----------------------------------------------------------------------------
* |  3  |12/27/2004| response sent with old key in update_key        |Prachiti|
* -----------------------------------------------------------------------------
* |  2  |12/27/2004| changed response_code from int to char          |Prachiti|
* -----------------------------------------------------------------------------
* |  1  |10/15/2004| removed phy_sub_typ from ap_config_info         | Abhijit|
* -----------------------------------------------------------------------------
* |  0  |10/15/2004| Created                                         | Abhijit|
* -----------------------------------------------------------------------------
********************************************************************************/

#include <sys/ioctl.h>
#include <net/if.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <time.h>
#include <sys/wait.h>
#include <linux/version.h>

#include "list.h"

#if LINUX_VERSION_CODE == KERNEL_VERSION(4,4,0)
#define x86_ONLY
#endif

#ifdef TARGET_CNS3XXX
/* Max VLAN interfaces Supported by ath5k is 3 + 1(default) */
#define MAX_VLAN_COUNT	4
#elif defined (TARGET_IMX6)
/* Max VLAN interfaces Supported by ath9k and ath10k is 7 + 1(default) */
#define MAX_VLAN_COUNT	6
#else
/* Considering MAX VLAN COUNT as 8 Based on the highest supported count i.e TARGET_IMX6  */
#define MAX_VLAN_COUNT	6
#endif

#define CLOCKID CLOCK_REALTIME
#define SIG SIGUSR1

#define errResponse(msg)    do { 	perror(msg);\
										imcp_sw_update_response_data-> action = FW_UPGRADE_FAILED;\
				                  send_sw_response_packet(imcp_sw_update_response_data-> action, &client_addr);\
                               } while (0)
pthread_t prev_tid;

int make_security_info_packet_buffer(unsigned char **buffer, al_security_info_t *security_info);
static unsigned char validate_node_description(unsigned char desc_length, config_validation_report_t  *validation_report);
static unsigned char validate_node_heartbeat_interval(unsigned char heartbeat_interval, config_validation_report_t  *validation_report);
static unsigned char validate_interface_essid(al_conf_if_info_t *if_info, int essid_length, config_validation_report_t *validation_report);
static unsigned char  validate_node_name(unsigned char name_length, config_validation_report_t  *validation_report);
int validate_freq_and_seg0_freq(int curr_channel, al_conf_if_info_t *if_info, config_validation_report_t  *validation_report);
int validate_80mhz_allowed_channel(al_conf_if_info_t *if_info, int channel, config_validation_report_t  *validation_report);

unsigned char IMCP_SIGNATURE[] = { 'I', 'M', 'C', 'P', 5, 0 };


#define UPGRADE_TIMEOUT (60*3) //Seconds
#define FIRMWARE_PATH "/tmp/md_builds"

int g_fw_upgrade_status = 0;

struct sockaddr_in *g_client_addr;
struct list_head imcp_sysupgrade_request;

char buffer_conf[105];
char buffer1_conf[105];

void dump_bytes(char *D, int count)
{
   int i, base = 0;

   printf("-------- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- ----------------\n");
   for ( ; base + 16 < count; base += 16)
   {
      printf("%08X ", base);

      for (i = 0; i < 16; i++)
      {
         printf("%02X ", D[base + i]);
      }

      printf("\n");
   }
   printf("%08X ", base);
   for (i = base; i < count; i++)
   {
      printf("%02X ", D[i]);
   }
   for ( ; i < base + 16; i++)
   {
      printf("   ");
   }
   for ( ; i < base + 16; i++)
   {
      printf(" ");
   }
   printf("\n");
   printf("-------- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- ----------------\n");
   printf("\n");
}

int _strcmp_i(const char *s1, const char *s2, int len)
{
   	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter: %s: %d\n",__func__,__LINE__);
	int  i;
	char c1, c2;

	for (i = 0; i < len; i++)
	{
		if ((s1[i] >= 'A') && (s1[i] <= 'Z'))
		{
			c1 = s1[i] + ('a' - 'A');
		}
		else
		{
			c1 = s1[i];
		}
		if ((s2[i] >= 'A') && (s2[i] <= 'Z'))
		{
			c2 = s2[i] + ('a' - 'A');
		}
		else
		{
			c2 = s2[i];
		}
		if (c1 != c2)
		{
			return c1 - c2;
		}
	}
   	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit: %s: %d\n",__func__,__LINE__);

	return 0;
}


/*
 *	returns imcp packet type or -1 on error
 */
int verify_imcp_signature(unsigned char *buffer, unsigned short *imcp_flags)
{
   	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter: %s: %d\n",__func__,__LINE__);
	int  ret = -1;
	int  packet_mesh_id_length;
	unsigned short  packet_type; 
	char packet_mesh_id[256];
	unsigned short flags;

	if (memcmp(buffer, IMCP_SIGNATURE, sizeof(IMCP_SIGNATURE)))
	{
	   	al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : IMCP Signature verification failed : %s: %d\n",__func__,__LINE__);
		return -1;
	}
	buffer += sizeof(IMCP_SIGNATURE);

	memcpy(&flags,buffer,2);
	*imcp_flags = al_impl_le16_to_cpu(flags);
	buffer += 2;

	memcpy(&packet_type,buffer,2);
	packet_type = al_impl_le16_to_cpu(packet_type);
	buffer += 2;
	
	if (packet_type != IMCP_CONFIGD_WATCHDOG_REQUEST)
	{
		packet_mesh_id_length = *buffer;
		buffer++;

		memcpy(packet_mesh_id, buffer, packet_mesh_id_length);
		packet_mesh_id[packet_mesh_id_length] = 0;

		if (packet_mesh_id_length == mesh_id_length)
		{
			if (_strcmp_i((const char *)packet_mesh_id, (const char *)mesh_id, mesh_id_length))
			{
	   			al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : mesh_id mismatch line: %s: %d\n",__func__,__LINE__);
				return -2;
			}
		}
		else
		{
	   		al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : mesh_id_length mismatch line: %s: %d\n",__func__,__LINE__);
			return -2;
		}
	}

	return packet_type;
}


/*
 *	Copy IMCP signature to buffer and return bytes written
 */
int copy_imcp_signature_packet_type_meshid(unsigned char *buffer, unsigned short packet_type) //archana changed 
{
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter: %s: %d\n",__func__,__LINE__);
	int start_position = (int)buffer;
	unsigned short cpu2le16;  

	memcpy(buffer, IMCP_SIGNATURE, sizeof(IMCP_SIGNATURE));
	buffer += sizeof(IMCP_SIGNATURE);

	cpu2le16 = IMCP_DEFAULT_FLAG_VALUE;
	cpu2le16 = al_impl_cpu_to_le16(cpu2le16);
	memcpy(buffer, &cpu2le16 , sizeof(unsigned short));
	buffer += sizeof(unsigned short);


	cpu2le16 = al_impl_cpu_to_le16((unsigned short)packet_type);
	memcpy(buffer, &cpu2le16, sizeof(unsigned short));
	buffer += sizeof(unsigned short);



	*buffer = mesh_id_length;
	buffer++;

	memcpy(buffer, mesh_id, mesh_id_length);
	buffer += mesh_id_length;

	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit: %s: %d\n",__func__,__LINE__);
	return (int)buffer - start_position;
}


int get_imcp_signature_mesh_id_size()
{
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter: %s: %d\n",__func__,__LINE__);
	/* IMCP signature 4 bytes, Version 2 bytes, packet_type 1 byte, meshid_length 1 byte, meshid  meshid_length bytes */
	return sizeof(IMCP_SIGNATURE) + 2 + 2 + 1 + mesh_id_length;
}


int get_packet_type_offset()
{
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter: %s: %d\n",__func__,__LINE__);
	return (sizeof(IMCP_SIGNATURE) + 1);
}


int increment_config_sqnr(int al_conf_handle)
{
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter: %s: %d\n",__func__,__LINE__);
	int config_sqnr;

	config_sqnr  = al_conf_get_config_sqnr(AL_CONTEXT al_conf_handle);
	config_sqnr += 1;

	/* if sqnr value overflows then reset it to 1 */
	if (config_sqnr < 0)
	{
		al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"CONFIGD : INFO : sqnr value overflows,reset it to 1: %s : %d\n",__func__,__LINE__);
		config_sqnr = 1;
	}

	al_conf_set_config_sqnr(AL_CONTEXT al_conf_handle, config_sqnr);
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit: %s: %d\n",__func__,__LINE__);
	return config_sqnr;
}


int set_mesh_config_sqnr(int sqnr)
{
    al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter: %s: %d\n",__func__,__LINE__);
	char buffer[1024];

	sprintf(buffer, "/sbin/meshd config_sqnr %d", sqnr);
	EXECUTE_COMMAND(buffer)
}


/*
 *	send acknowledgement for ipconfig update
 */
int send_imcp_ipconfig_updated_reply(struct sockaddr_in *client_addr, unsigned char response_id)
{
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter: %s: %d\n",__func__,__LINE__);
	unsigned char *buffer;
	unsigned char *buffer_ptr;
	unsigned int  packet_size = get_imcp_signature_mesh_id_size() + 8;
	int           ret         = 0;

	buffer = (unsigned char *)malloc(packet_size);

	buffer_ptr = buffer;

	buffer_ptr += copy_imcp_signature_packet_type_meshid(buffer_ptr, IMCP_AP_IP_CONFIG_REQUEST_RESPONSE);

	memcpy(buffer_ptr, imcp_ipconfig_data->mac_address, 6);
	buffer_ptr += 6;

	*buffer_ptr = TYPE_RESPONSE;
	buffer_ptr++;

	*buffer_ptr = response_id;

	ret = send_packet(buffer, packet_size, client_addr);
      	al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"CONFIGD : INFO : IMCP_IP_CONFIG_UPDATE_RESPONSE Packet sent (Bytes = %d) : %s : %d\n",ret,__func__,__LINE__);


	free(buffer);

	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit: %s: %d\n",__func__,__LINE__);
	return 0;
}


/*
 *	Send ip configuration info to client
 */
int send_ip_config_info_reply(struct sockaddr_in *client_addr, unsigned char response_id)
{
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter: %s: %d\n",__func__,__LINE__);
	unsigned char buffer[1024];
	unsigned char *buffer_ptr;
	int           sock, clilen;
	int           ret;
	int           packet_size = get_imcp_signature_mesh_id_size() + IMCP_IP_CONFIG_FIXED_LEN + 
				    imcp_ipconfig_data->host_name_length + IMCP_ID_LEN_SIZE; 
	unsigned short node_id;
	unsigned short node_len;


	buffer_ptr = buffer;


	buffer_ptr += copy_imcp_signature_packet_type_meshid(buffer_ptr, IMCP_AP_IP_CONFIGURATION_INFO);

	node_id		= IMCP_IP_CONFIG_INFO_ID;
	node_len	= IMCP_IP_CONFIG_FIXED_LEN + imcp_ipconfig_data->host_name_length; 
	IMCP_MSG_PROCESS_SEND_ID_AND_LEN(buffer_ptr,node_id,node_len);

		memcpy(buffer_ptr, imcp_ipconfig_data->mac_address, 6);
	buffer_ptr += 6;

	*buffer_ptr = imcp_ipconfig_data->request_response_type;
	buffer_ptr++;

	*buffer_ptr = imcp_ipconfig_data->response_id;
	buffer_ptr++;

	*buffer_ptr = imcp_ipconfig_data->host_name_length;
	buffer_ptr++;

	if (imcp_ipconfig_data->host_name_length > 0)
	{
		memcpy(buffer_ptr, imcp_ipconfig_data->host_name, imcp_ipconfig_data->host_name_length);
		buffer_ptr += imcp_ipconfig_data->host_name_length;
	}

	memcpy(buffer_ptr, imcp_ipconfig_data->ip_address, 4);
	buffer_ptr += 4;

	memcpy(buffer_ptr, imcp_ipconfig_data->subnet_mask, 4);
	buffer_ptr += 4;

	memcpy(buffer_ptr, imcp_ipconfig_data->gateway, 4);
	buffer_ptr += 4;

	ret = send_packet(buffer, packet_size, client_addr);
      	al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"CONFIGD : INFO : IMCP_IP_CONFIG Packet sent (Bytes = %d) : %s : %d\n",ret,__func__,__LINE__);
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit: %s: %d\n",__func__,__LINE__);


	return 0;
}


/*
 *  Read ap interface configuration from al_conf and update it to imcp_apconfig_data
 */
int update_apconfig_interface_data(int al_conf_handle)
{
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter: %s: %d\n",__func__,__LINE__);
	int if_no;

	unsigned short node_id;
	unsigned short node_len;

	for (if_no = 0; if_no < imcp_apconfig_data->interface_count; if_no++)
	{
		al_conf_get_if(al_conf_handle, if_no, &(imcp_apconfig_data->if_info[if_no]));
	}

	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit: %s: %d\n",__func__,__LINE__);
	return 0;
}


int update_vlan_config_data(int al_conf_handle)
{
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter: %s: %d\n",__func__,__LINE__);
	int                 i;
	int                 vlan_index;
	int                 vlan_count;
	al_conf_vlan_info_t al_vlan_info;
	al_security_info_t  *security_info;
	int                 len;

	vlan_count = al_conf_get_vlan_count(al_conf_handle);

	if (vlan_count <= 0)
	{
		al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : Invalid vlan_count: %s  line : %d\n",__func__,__LINE__);
		return -1;
	}

	imcp_vlan_config_info_data->vlan_count = vlan_count;

	/**
	 *	malloc vlan_info structure with vlan_count -1 since donot we copy default vlan_info in the allocated structure
	 *	default vlan_info is copied in variables defined in imcp_vlan_config_info_data
	 */

	imcp_vlan_config_info_data->imcp_vlan_info = (imcp_vlan_info_data_t *)malloc(sizeof(imcp_vlan_info_data_t) * (vlan_count - 1));
	memset(imcp_vlan_config_info_data->imcp_vlan_info, 0, sizeof(imcp_vlan_info_data_t) * (vlan_count - 1));

	vlan_index = 0;
	for (i = 0; i < imcp_vlan_config_info_data->vlan_count; i++)
	{
		al_conf_get_vlan(al_conf_handle, i, &al_vlan_info);

		if (!strcmp(al_vlan_info.name, "default"))
		{
			imcp_vlan_config_info_data->tag             = al_vlan_info.tag;
			imcp_vlan_config_info_data->dot1p_priority  = al_vlan_info.dot1p_priority;
			imcp_vlan_config_info_data->dot11e_enabled  = al_vlan_info.dot11e_enabled;
			imcp_vlan_config_info_data->dot11e_category = al_vlan_info.dot11e_category;
			continue;
		}

		strcpy(imcp_vlan_config_info_data->imcp_vlan_info[vlan_index].vlan_name, al_vlan_info.name);
		memcpy(imcp_vlan_config_info_data->imcp_vlan_info[vlan_index].ip_address, al_vlan_info.ip_address, 4);

		imcp_vlan_config_info_data->imcp_vlan_info[vlan_index].vlan_tag        = al_vlan_info.tag;
		imcp_vlan_config_info_data->imcp_vlan_info[vlan_index].dot1p_p         = al_vlan_info.dot1p_priority;
		imcp_vlan_config_info_data->imcp_vlan_info[vlan_index].dot11e_enabled  = al_vlan_info.dot11e_enabled;
		imcp_vlan_config_info_data->imcp_vlan_info[vlan_index].dot11e_category = al_vlan_info.dot11e_category;

		strcpy(imcp_vlan_config_info_data->imcp_vlan_info[vlan_index].essid, al_vlan_info.essid);

		imcp_vlan_config_info_data->imcp_vlan_info[vlan_index].rts_threshold  = al_vlan_info.rts_th;
		imcp_vlan_config_info_data->imcp_vlan_info[vlan_index].frag_threshold = al_vlan_info.frag_th;
		imcp_vlan_config_info_data->imcp_vlan_info[vlan_index].beacon_int     = al_vlan_info.beacon_interval;
		imcp_vlan_config_info_data->imcp_vlan_info[vlan_index].service_type   = al_vlan_info.service;
		imcp_vlan_config_info_data->imcp_vlan_info[vlan_index].transmit_power = al_vlan_info.txpower;
		imcp_vlan_config_info_data->imcp_vlan_info[vlan_index].txrate         = al_vlan_info.txrate;

		memcpy(&imcp_vlan_config_info_data->imcp_vlan_info[vlan_index].security_info,
				&al_vlan_info.security_info,
				sizeof(al_security_info_t));

		vlan_index++;
	}

	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit: %s: %d\n",__func__,__LINE__);
	return 0;
}


int update_acl_data(int acl_conf_handle)
{
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter: %s: %d\n",__func__,__LINE__);
	acl_conf_info_t info;
	int             i;
	int             acl_count;

	acl_count = acl_conf_get_info_count(AL_CONTEXT acl_conf_handle);
	if (acl_count <= 0)
	{
		al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : Invalid acl_count line func:%s Line:%d\n",__func__,__LINE__);
		return -1;
	}

	imcp_acl_config_data->count = acl_count;

	if (imcp_acl_config_data->count > 0)
	{
		imcp_acl_config_data->entries = (imcp_acl_entry_t *)malloc(sizeof(imcp_acl_entry_t) * imcp_acl_config_data->count);
		memset(imcp_acl_config_data->entries, 0, sizeof(imcp_acl_entry_t) * imcp_acl_config_data->count);
	}

	for (i = 0; i < imcp_acl_config_data->count; i++)
	{
		acl_conf_get_info(AL_CONTEXT acl_conf_handle, i, &info);

		memcpy(imcp_acl_config_data->entries[i].mac_address, info.sta_mac, 6);
		imcp_acl_config_data->entries[i].vlan_tag        = info.vlan_tag;
		imcp_acl_config_data->entries[i].allow           = info.allow;
		imcp_acl_config_data->entries[i].dot11e_enabled  = info.dot11e_enabled;
		imcp_acl_config_data->entries[i].dot11e_category = info.dot11e_category;
	}
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit: %s: %d\n",__func__,__LINE__);

	return 0;
}


int update_dot11e_category_data(int dot11e_conf_handle)
{
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter: %s: %d\n",__func__,__LINE__);
	dot11e_conf_category_details_t category_info;
	int i;

	imcp_dot11e_category_data->count = dot11e_conf_get_category_info_count(AL_CONTEXT dot11e_conf_handle);

	for (i = 0; i < imcp_dot11e_category_data->count; i++)
	{
		memset(&imcp_dot11e_category_data->category_info[i], 0, sizeof(imcp_dot11e_category_details_t));

		dot11e_conf_get_category_info(AL_CONTEXT dot11e_conf_handle, i, &category_info);

		imcp_dot11e_category_data->category_info[i].category        = category_info.category;
		imcp_dot11e_category_data->category_info[i].burst_time      = category_info.burst_time;
		imcp_dot11e_category_data->category_info[i].acwmin          = category_info.acwmin;
		imcp_dot11e_category_data->category_info[i].acwmax          = category_info.acwmax;
		imcp_dot11e_category_data->category_info[i].aifsn           = category_info.aifsn;
		imcp_dot11e_category_data->category_info[i].disable_backoff = category_info.disable_backoff;
	}
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit: %s: %d\n",__func__,__LINE__);
	return 0;
}


int update_dot11e_if_config_data(int al_conf_handle)
{
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter: %s: %d\n",__func__,__LINE__);
	int               i;
	al_conf_if_info_t al_conf_if_info;

	imcp_dot11e_if_config_data->if_count = al_conf_get_if_count(al_conf_handle);

	for (i = 0; i < imcp_dot11e_if_config_data->if_count; i++)
	{
		memset(&imcp_dot11e_if_config_data->if_info[i], 0, sizeof(imcp_dot11e_if_config_details_t));
		memset(&al_conf_if_info, 0, sizeof(al_conf_if_info_t));

		al_conf_get_if(AL_CONTEXT al_conf_handle, i, &al_conf_if_info);

		imcp_dot11e_if_config_data->if_info[i].if_index       = i;
		imcp_dot11e_if_config_data->if_info[i].enabled        = al_conf_if_info.dot11e_enabled;
		imcp_dot11e_if_config_data->if_info[i].category_index = al_conf_if_info.dot11e_category;

		memset(imcp_dot11e_if_config_data->if_info[i].if_name, 0, 256);
		strcpy(imcp_dot11e_if_config_data->if_info[i].if_name, al_conf_if_info.name);
		memset(imcp_dot11e_if_config_data->if_info[i].if_essid, 0, 256);
		strcpy(imcp_dot11e_if_config_data->if_info[i].if_essid, al_conf_if_info.essid);
	}
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit: %s: %d\n",__func__,__LINE__);
	return 0;
}


/*
 *  Read ap configuration from al_conf and update it to imcp_apconfig_data
 */
int update_apconfig_data(int al_conf_handle)
{
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter: %s: %d\n",__func__,__LINE__);
	int           i;
	al_net_addr_t preferred_parent;
	unsigned char *server_ip_addr;

	imcp_apconfig_data->request_response_type = TYPE_RESPONSE;

	al_conf_get_preferred_parent(al_conf_handle, &preferred_parent);

	for (i = 0; i < 6; i++)
	{
		imcp_apconfig_data->preferred_parent[i] = preferred_parent.bytes[i];
	}

	al_conf_get_signal_map(al_conf_handle, imcp_apconfig_data->signal_map);

	imcp_apconfig_data->heartbeat_interval = al_conf_get_heartbeat_interval(al_conf_handle);

	imcp_apconfig_data->heartbeat_miss_count = al_conf_get_heartbeat_miss_count(al_conf_handle);

	imcp_apconfig_data->hop_cost = al_conf_get_hop_cost(al_conf_handle);

	imcp_apconfig_data->max_allowable_hops = al_conf_get_max_allowable_hops(al_conf_handle);

	imcp_apconfig_data->las_interval = al_conf_get_las_interval(al_conf_handle);

	imcp_apconfig_data->cr_threshold = al_conf_get_change_resistance(al_conf_handle);

	al_conf_get_name(al_conf_handle, imcp_apconfig_data->name, 256);

	imcp_apconfig_data->name_length = strlen(imcp_apconfig_data->name);

	al_conf_get_description(al_conf_handle, imcp_apconfig_data->description, 256);

	imcp_apconfig_data->desc_length = strlen(imcp_apconfig_data->description);

	al_conf_get_gps_x_coordinate(al_conf_handle, imcp_apconfig_data->gps_x_coordinate, 256);	

	imcp_apconfig_data->x_coordinate_length = strlen(imcp_apconfig_data->gps_x_coordinate);

	al_conf_get_gps_y_coordinate(al_conf_handle, imcp_apconfig_data->gps_y_coordinate, 256);

	imcp_apconfig_data->y_coordinate_length = strlen(imcp_apconfig_data->gps_y_coordinate);

	al_conf_get_essid(al_conf_handle, imcp_apconfig_data->essid, 256);

	imcp_apconfig_data->essid_length = strlen(imcp_apconfig_data->essid);

	imcp_apconfig_data->rts_threshold = al_conf_get_rts_th(al_conf_handle);

	imcp_apconfig_data->frag_threshold = al_conf_get_frag_th(al_conf_handle);

	imcp_apconfig_data->beacon_interval = al_conf_get_beacon_interval(al_conf_handle);

	imcp_apconfig_data->dca = al_conf_get_dynamic_channel_allocation(al_conf_handle);

	imcp_apconfig_data->stay_awake_count = al_conf_get_stay_awake_count(al_conf_handle);

	imcp_apconfig_data->bridge_ageing_time = al_conf_get_bridge_ageing_time(al_conf_handle);

	imcp_apconfig_data->fcc_certified_operation = al_conf_get_fcc_certified(al_conf_handle);

	imcp_apconfig_data->etsi_certified_operation = al_conf_get_etsi_certified(al_conf_handle);

	imcp_apconfig_data->ds_tx_rate = al_conf_get_ds_tx_rate(al_conf_handle);

	imcp_apconfig_data->ds_tx_power = al_conf_get_ds_tx_power(al_conf_handle);

	imcp_apconfig_data->failover_enabled = al_conf_get_failover_enabled(al_conf_handle);

	imcp_apconfig_data->power_on_default = al_conf_get_power_on_default(al_conf_handle);

	server_ip_addr = al_conf_get_server_addr(al_conf_handle);
	memset(imcp_apconfig_data->server_ip_addr, 0, 128);
	memcpy(imcp_apconfig_data->server_ip_addr, server_ip_addr, strlen(server_ip_addr));

	imcp_apconfig_data->interface_count = al_conf_get_if_count(al_conf_handle);

	if (imcp_apconfig_data->interface_count > 0)
	{
		memset(imcp_apconfig_data->if_info, 0, sizeof(al_conf_if_info_t) * imcp_apconfig_data->interface_count);
		update_apconfig_interface_data(al_conf_handle);  
	}
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit: %s: %d\n",__func__,__LINE__);

	return 0;
}


int update_private_channel_config_data(int al_conf_handle)
{
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter: %s: %d\n",__func__,__LINE__);
	int               i;
	int               if_count;
	al_conf_if_info_t al_conf_if_info;

	if_count = al_conf_get_if_count(al_conf_handle);

	for (i = 0; i < if_count; i++)
	{
		memset(&al_conf_if_info, 0, sizeof(al_conf_if_info_t));

		al_conf_get_if(AL_CONTEXT al_conf_handle, i, &al_conf_if_info);

		if (!strcmp(al_conf_if_info.name, imcp_priv_channel_data->if_info.name))
		{
			memcpy(&imcp_priv_channel_data->if_info, &al_conf_if_info, sizeof(al_conf_if_info_t));
      			al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : Invalid interface %s found: %s: %d\n",imcp_priv_channel_data->if_info.name,__func__,__LINE__);
			return 0;
		}
	}
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit: %s: %d\n",__func__,__LINE__);
	return 0;
}


/*
 *	Send acknowledgement for ap configuration update
 */
int send_ap_config_updated_reply(struct sockaddr_in *client_addr, unsigned char response_id)
{
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter: %s: %d\n",__func__,__LINE__);
	unsigned char *buffer;
	unsigned char *buffer_ptr;
	int           packet_size = get_imcp_signature_mesh_id_size() + 8;
	int           ret;

	buffer = (unsigned char *)malloc(packet_size);

	buffer_ptr = buffer;

	buffer_ptr += copy_imcp_signature_packet_type_meshid(buffer_ptr, IMCP_AP_CONFIG_REQUEST_RESPONSE);

	memcpy(buffer_ptr, imcp_apconfig_rr_data->mac_address, 6);
	buffer_ptr += 6;

	*buffer_ptr = TYPE_RESPONSE;
	buffer_ptr++;

	*buffer_ptr = response_id;
	buffer_ptr++;

	ret = send_packet(buffer, packet_size, client_addr);
      	al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"CONFIGD : INFO : IMCP_AP_CONFIG_UPDATE_RESPONSE  Packet sent (Bytes = %d) : %s : %d\n",ret,__func__,__LINE__);

	free(buffer);
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit: %s: %d\n",__func__,__LINE__);

	return 0;
}

int send_config_updated_response(struct sockaddr_in *client_addr, unsigned char response_id, unsigned char packet_type, unsigned char err_code, config_validation_report_t  *validation_report)
{
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter: %s: %d\n",__func__,__LINE__);
	unsigned char *buffer;
	unsigned char *buffer_ptr;
	int           packet_size = get_imcp_signature_mesh_id_size() + IMCP_UPDATED_RESPONSE_FIXED_SIZE + IMCP_ID_LEN_SIZE;
	int           ret;
	unsigned short node_id;
	unsigned short node_len;

	if (err_code == SUCCESS) {
		packet_size += sizeof(err_code);
	}
	else if (err_code != SUCCESS) {
		packet_size += sizeof(err_code) + sizeof(validation_report->category_id) +  validation_report->param_name_len +  validation_report->value_len + IMCP_PARAM_NAME_LEN_SIZE + IMCP_PARAM_VALUE_LEN_SIZE;


		if (validation_report->category_id == IF_CONFIG)
			packet_size += validation_report->if_name_len + IMCP_PARAM_IF_NAME_LEN_SIZE;
	}


	buffer = (unsigned char *)malloc(packet_size);

	buffer_ptr = buffer;

	buffer_ptr += copy_imcp_signature_packet_type_meshid(buffer_ptr, IMCP_CONFIG_REQUEST_RESPONSE);

	node_id      = IMCP_CONFIG_UPDATED_ID;
	node_len     = 9 + packet_size - (get_imcp_signature_mesh_id_size() - IMCP_ID_LEN_SIZE);
	IMCP_MSG_PROCESS_SEND_ID_AND_LEN(buffer_ptr,node_id,node_len);

	memcpy(buffer_ptr, imcp_apconfig_rr_data->mac_address, 6);

	buffer_ptr += 6;

	*buffer_ptr = TYPE_RESPONSE;
	buffer_ptr++;

	*buffer_ptr = packet_type;
	buffer_ptr++;

	*buffer_ptr = response_id;
	buffer_ptr++;

	/* Report Config request success or failure status */
	*buffer_ptr = err_code;
	buffer_ptr++;

	if (err_code != SUCCESS) {
		/* Add ID to indicate config parameter associated to Node or Interface related configuraiton */
		*buffer_ptr = validation_report->category_id;
		buffer_ptr++;

		/* Add interface name and its length if parameter associated interface */
		if (validation_report->category_id == IF_CONFIG) {
			*buffer_ptr = validation_report->if_name_len;
			buffer_ptr++;
			memcpy(buffer_ptr, validation_report->if_name, validation_report->if_name_len);
			buffer_ptr += validation_report->if_name_len;
		}

		/* Add parameter name and its name length */
		*buffer_ptr = validation_report->param_name_len;
		buffer_ptr++;
		memcpy(buffer_ptr, validation_report->param_name, validation_report->param_name_len);
		buffer_ptr += validation_report->param_name_len;

		/* Add incorrect value length and received value from NMS */
		*buffer_ptr = validation_report->value_len;
		buffer_ptr++;
		memcpy(buffer_ptr, validation_report->value, validation_report->value_len);
		buffer_ptr += validation_report->value_len;
	}


	ret = send_packet(buffer, packet_size, client_addr);
      	al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"CONFIGD : INFO : IMCP_CONFIG_UPDATE_RESPONSE Packet sent (Bytes = %d) : %s : %d\n",ret,__func__,__LINE__);

	free(buffer);
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit: %s: %d\n",__func__,__LINE__);

	return 0;
}


int send_dot11e_category_info_reply(struct sockaddr_in *client_addr, unsigned char response_id)
{
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter: %s: %d\n",__func__,__LINE__);
	int            size;
	int            i, j;
	int            ret;
	int            category_name_length;
	int            category_size;
	int            category_name_length_bytes;
	int            packet_size;
	unsigned int   cpu2le32;
	unsigned short cpu2le16;
	unsigned char  buffer[1024];
	unsigned char  *p;
	char           *categories[] = { "ac_bk", "ac_be", "ac_vi", "ac_vo" };
	unsigned short node_id;
	unsigned short node_len;

#define MAC_ADDRESS_LENGTH    6

	/*
	 * category_size =	category_count			+
	 *			category_name_length		+
	 *			category_name_length bytes	+
	 *			burst_time			+
	 *			acwmin				+
	 *			acwmax				+
	 *			aifsn				+
	 *			diable_backoff
	 */

	category_name_length_bytes = 5;
	category_size = 1 + category_name_length_bytes + 4 + 2 + 2 + 1 + 1;


	size        = (imcp_dot11e_category_data->count * category_size);
	packet_size = get_imcp_signature_mesh_id_size() + size + 9 + IMCP_ID_LEN_SIZE; 

	memset(buffer, 0, 1024);

	p  = buffer;
	p += copy_imcp_signature_packet_type_meshid(p, IMCP_DOT11E_CATEGORY_CONFIGURATION_INFO);

	node_id 	= IMCP_DOT11E_CATEGORY_INFO_ID;
	node_len	= packet_size - get_imcp_signature_mesh_id_size() - IMCP_ID_LEN_SIZE; 
	IMCP_MSG_PROCESS_SEND_ID_AND_LEN(p,node_id,node_len);

		memcpy(p, imcp_dot11e_category_data->mac_address, MAC_ADDRESS_LENGTH);
	p += MAC_ADDRESS_LENGTH;
	*p = TYPE_RESPONSE;
	p += 1;
	*p = response_id;
	p += 1;
	*p = imcp_dot11e_category_data->count;
	p += 1;

	for (i = 0; i < imcp_dot11e_category_data->count; i++)
	{
		for (j = 0; j < sizeof(categories); j++)
		{
			if (imcp_dot11e_category_data->category_info[i].category == j)
			{
				category_name_length = strlen(categories[j]);
				*p = category_name_length;
				p += 1;
				memcpy(p, categories[j], category_name_length);
				p += category_name_length;

				cpu2le32 = al_impl_cpu_to_le32(imcp_dot11e_category_data->category_info[i].burst_time);
				memcpy(p, &cpu2le32, sizeof(unsigned int));
				p += sizeof(unsigned int);

				cpu2le16 = al_impl_cpu_to_le16(imcp_dot11e_category_data->category_info[i].acwmin);
				memcpy(p, &cpu2le16, sizeof(unsigned short));
				p += sizeof(unsigned short);

				cpu2le16 = al_impl_cpu_to_le16(imcp_dot11e_category_data->category_info[i].acwmax);
				memcpy(p, &cpu2le16, sizeof(unsigned short));
				p += sizeof(unsigned short);

				*p = imcp_dot11e_category_data->category_info[i].aifsn;
				p += 1;
				*p = imcp_dot11e_category_data->category_info[i].disable_backoff;
				p += 1;

				break;
			}
		}
	}

	ret = send_packet(buffer, packet_size, client_addr);
	if (ret == -1)
	{
      		al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : Sending packet failed: %s: %d\n",__func__,__LINE__);
		return ret;
	}

      	al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"CONFIGD : INFO : IMCP_DOT11E_CATEGORY Packet sent (Bytes = %d) : %s : %d\n",ret,__func__,__LINE__);
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit: %s: %d\n",__func__,__LINE__);

	return 0;


#undef MAC_ADDRESS_LENGTH
}


/*
 *	Send dot11e IF config configuration info to client
 */
int send_dot11e_if_config_info_reply(struct sockaddr_in *client_addr, unsigned char response_id)
{
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter: %s: %d\n",__func__,__LINE__);
	int           packet_size;
	int           dot11e_if_config_size;
	int           length;
	unsigned char buffer[1024];
	unsigned char *p;
	int           i;
	int           ret;
	unsigned short node_id;
	unsigned short node_len;


	dot11e_if_config_size = 0;
	for (i = 0; i < imcp_dot11e_if_config_data->if_count; i++)
	{
		dot11e_if_config_size += 1;           // if_name_length
		dot11e_if_config_size += strlen(imcp_dot11e_if_config_data->if_info[i].if_name);
		dot11e_if_config_size += 1;           //dot11e enabled
		dot11e_if_config_size += 1;           // category_index
		dot11e_if_config_size += 1;           // if_essid_length
		dot11e_if_config_size += strlen(imcp_dot11e_if_config_data->if_info[i].if_essid);
	}

	packet_size = get_imcp_signature_mesh_id_size() + dot11e_if_config_size + 9;
	packet_size += IMCP_ID_LEN_SIZE;	

	memset(buffer, 0, 1024);
	p = buffer;

	p += copy_imcp_signature_packet_type_meshid(p, IMCP_DOT11E_IF_CONFIGURATION_INFO);

	node_id		= IMCP_DOT11E_CONFIG_INFO_ID;
	node_len	= packet_size - get_imcp_signature_mesh_id_size() - IMCP_ID_LEN_SIZE;
	IMCP_MSG_PROCESS_SEND_ID_AND_LEN(p,node_id,node_len);

		memcpy(p, imcp_dot11e_if_config_data->mac_address, 6);
	p += 6;

	*p = TYPE_RESPONSE;
	p += 1;
	*p = response_id;
	p += 1;
	*p = imcp_dot11e_if_config_data->if_count;
	p += 1;

	for (i = 0; i < imcp_dot11e_if_config_data->if_count; i++)
	{
		length = strlen(imcp_dot11e_if_config_data->if_info[i].if_name);
		*p     = length;
		p     += 1;

		memcpy(p, imcp_dot11e_if_config_data->if_info[i].if_name, length);
		p += length;

		*p = imcp_dot11e_if_config_data->if_info[i].enabled;
		p += 1;

		*p = imcp_dot11e_if_config_data->if_info[i].category_index;
		p += 1;

		length = strlen(imcp_dot11e_if_config_data->if_info[i].if_essid);
		*p     = length;
		p     += 1;

		memcpy(p, imcp_dot11e_if_config_data->if_info[i].if_essid, length);
		p += length;
	}

	ret = send_packet(buffer, packet_size, client_addr);
      	al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"CONFIGD : INFO : IMCP_DOT11E_IF_CONFIG Packet sent (Bytes = %d) : %s : %d\n",ret,__func__,__LINE__);
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit: %s: %d\n",__func__,__LINE__);

	return 0;
}


/*
 *	Send ap configuration info to client
 */
int send_apconfig_info_reply(struct sockaddr_in *client_addr, unsigned char response_id)
{
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter: %s: %d\n",__func__,__LINE__);
	int           sock, clilen, i, j;
	int           size, if_size;
        int 	      extented_len;
	int           packet_size;
	unsigned char buffer[1024];
	unsigned char *buffer_ptr;
	unsigned int  cpu2le;
	int           ret;
	int           if_name_length, essid_length;

	unsigned short node_id;
	unsigned short node_len;

	essid_length = 0;

	size = imcp_apconfig_data->essid_length +
		imcp_apconfig_data->name_length +
		imcp_apconfig_data->desc_length +
		imcp_apconfig_data->x_coordinate_length +
		imcp_apconfig_data->y_coordinate_length;


	if_size = 0;

	for (i = 0; i < imcp_apconfig_data->interface_count; i++)
	{
		if_name_length = strlen(imcp_apconfig_data->if_info[i].name);

		essid_length = strlen(imcp_apconfig_data->if_info[i].essid);

		if_size += (if_name_length + imcp_apconfig_data->if_info[i].dca_list_count);

		if_size += 17 + essid_length;           /* rts_th + frag_th + beacon_interval + txrate + essid_length */

		if_size += get_security_info_size(&imcp_apconfig_data->if_info[i].security_info);


		if(CHECK_FOR_N_SUBTYPE_TO_ADD_HT_CAPABILITES(imcp_apconfig_data->if_info[i].phy_sub_type))/*MACRO will give the info about phy_sub_type related N things*/
		{
                       	if_size += IMCP_HT_CAP_FIXED_LEN + IMCP_FRAM_AGRE_FIXED_LEN ;
		}
		else if(CHECK_FOR_AC_SUBTYPE_TO_ADD_VHT_CAPABILITES(imcp_apconfig_data->if_info[i].phy_sub_type))
		{
			if_size += IMCP_HT_CAP_FIXED_LEN + IMCP_FRAM_AGRE_FIXED_LEN + IMCP_VHT_CAP_FIXED_LEN;
		}   
	}

	if_size += (10 * imcp_apconfig_data->interface_count); /*1 byte fields so 10*/
	if_size += (imcp_apconfig_data->interface_count * IMCP_ID_LEN_SIZE);

	packet_size = get_imcp_signature_mesh_id_size() + 56 + size + if_size;
	packet_size += IMCP_ID_LEN_SIZE;

	buffer_ptr = buffer;

	buffer_ptr += copy_imcp_signature_packet_type_meshid(buffer_ptr, IMCP_AP_CONFIGURATION_INFO);


	node_id		= IMCP_AP_CONFIG_INFO_REQ_RES_INFO_ID;
	node_len	= packet_size - get_imcp_signature_mesh_id_size() - IMCP_ID_LEN_SIZE; //Here including all the sub fields node_id & len
	IMCP_MSG_PROCESS_SEND_ID_AND_LEN(buffer_ptr, node_id, node_len);


	memcpy(buffer_ptr, imcp_apconfig_data->mac_address, 6);
	buffer_ptr += 6;

	*buffer_ptr = imcp_apconfig_data->request_response_type;
	buffer_ptr++;

	*buffer_ptr = response_id;
	buffer_ptr++;

	for (i = 0; i < 6; i++)
	{
		*buffer_ptr = imcp_apconfig_data->preferred_parent[i];
		buffer_ptr++;
	}

	memcpy(buffer_ptr, imcp_apconfig_data->signal_map, 8);
	buffer_ptr += 8;

	*buffer_ptr = imcp_apconfig_data->heartbeat_interval;
	buffer_ptr++;

	*buffer_ptr = imcp_apconfig_data->heartbeat_miss_count;
	buffer_ptr++;

	*buffer_ptr = imcp_apconfig_data->hop_cost;
	buffer_ptr++;

	*buffer_ptr = imcp_apconfig_data->max_allowable_hops;
	buffer_ptr++;

	*buffer_ptr = imcp_apconfig_data->las_interval;
	buffer_ptr++;

	*buffer_ptr = imcp_apconfig_data->cr_threshold;
	buffer_ptr++;

	*buffer_ptr = imcp_apconfig_data->name_length;
	buffer_ptr++;

	if (imcp_apconfig_data->name_length > 0)
	{
		memcpy(buffer_ptr, imcp_apconfig_data->name, imcp_apconfig_data->name_length);
		buffer_ptr += imcp_apconfig_data->name_length;
	}

	*buffer_ptr = imcp_apconfig_data->essid_length;
	buffer_ptr++;

	if (imcp_apconfig_data->essid_length > 0)
	{
		memcpy(buffer_ptr, imcp_apconfig_data->essid, imcp_apconfig_data->essid_length);
		buffer_ptr += imcp_apconfig_data->essid_length;
	}

	*buffer_ptr = imcp_apconfig_data->desc_length;
	buffer_ptr++;

	if (imcp_apconfig_data->desc_length > 0)
	{
		memcpy(buffer_ptr, imcp_apconfig_data->description, imcp_apconfig_data->desc_length);
		buffer_ptr += imcp_apconfig_data->desc_length;
	}

	*buffer_ptr = imcp_apconfig_data->x_coordinate_length;
	buffer_ptr++;

	if (imcp_apconfig_data->x_coordinate_length > 0)
	{
		memcpy(buffer_ptr, imcp_apconfig_data->gps_x_coordinate, imcp_apconfig_data->x_coordinate_length);
		buffer_ptr += imcp_apconfig_data->x_coordinate_length;
	}

	*buffer_ptr = imcp_apconfig_data->y_coordinate_length;
	buffer_ptr++;

	if (imcp_apconfig_data->y_coordinate_length > 0)
	{
		memcpy(buffer_ptr, imcp_apconfig_data->gps_y_coordinate, imcp_apconfig_data->y_coordinate_length);
		buffer_ptr += imcp_apconfig_data->y_coordinate_length;
	}


	cpu2le = al_impl_cpu_to_le32(imcp_apconfig_data->rts_threshold);
	memcpy(buffer_ptr, &cpu2le, sizeof(unsigned int));
	buffer_ptr += sizeof(unsigned int);

	cpu2le = al_impl_cpu_to_le32(imcp_apconfig_data->frag_threshold);
	memcpy(buffer_ptr, &cpu2le, sizeof(unsigned int));
	buffer_ptr += sizeof(unsigned int);

	cpu2le = al_impl_cpu_to_le32(imcp_apconfig_data->beacon_interval);
	memcpy(buffer_ptr, &cpu2le, sizeof(unsigned int));
	buffer_ptr += sizeof(unsigned int);

	*buffer_ptr = imcp_apconfig_data->dca;
	buffer_ptr++;

	*buffer_ptr = imcp_apconfig_data->stay_awake_count;
	buffer_ptr++;

	*buffer_ptr = imcp_apconfig_data->bridge_ageing_time;
	buffer_ptr++;

	*buffer_ptr = imcp_apconfig_data->fcc_certified_operation;
	buffer_ptr++;

	*buffer_ptr = imcp_apconfig_data->etsi_certified_operation;
	buffer_ptr++;

	cpu2le = al_impl_cpu_to_le32(imcp_apconfig_data->ds_tx_rate);
	memcpy(buffer_ptr, &cpu2le, sizeof(unsigned int));
	buffer_ptr += sizeof(unsigned int);

	*buffer_ptr = imcp_apconfig_data->ds_tx_power;
	buffer_ptr++;

	*buffer_ptr = imcp_apconfig_data->interface_count;
	buffer_ptr++;

	if (imcp_apconfig_data->interface_count > 0)
	{
		for (i = 0; i < imcp_apconfig_data->interface_count; i++)
		{
			if(imcp_apconfig_data->if_info[i].phy_type == AL_CONF_IF_PHY_TYPE_ETHERNET)
			{
				node_id         = IMCP_IF_802_3_INFO_ID;
				node_len        = IMCP_FIXED_ETH_LEN + strlen(imcp_apconfig_data->if_info[i].name);
				IMCP_MSG_PROCESS_SEND_ID_AND_LEN(buffer_ptr,node_id,node_len);

				if_name_length = strlen(imcp_apconfig_data->if_info[i].name);

				*buffer_ptr = if_name_length;
				buffer_ptr++;

				memcpy(buffer_ptr, imcp_apconfig_data->if_info[i].name, if_name_length);
				buffer_ptr += if_name_length;

				*buffer_ptr = imcp_apconfig_data->if_info[i].phy_type;
				buffer_ptr++;

				/* TODO remove this field */
				*buffer_ptr = imcp_apconfig_data->if_info[i].phy_sub_type;
				buffer_ptr++;

				if (imcp_apconfig_data->if_info[i].use_type == AL_CONF_IF_USE_TYPE_AP)
				{
					imcp_apconfig_data->if_info[i].use_type = AL_CONF_IF_USE_TYPE_WM;
				}
				*buffer_ptr = imcp_apconfig_data->if_info[i].use_type;
				buffer_ptr++;

				cpu2le = al_impl_cpu_to_le32(imcp_apconfig_data->if_info[i].txrate);
				memcpy(buffer_ptr, &cpu2le, sizeof(unsigned int));
				buffer_ptr += sizeof(unsigned int);

				*buffer_ptr = imcp_apconfig_data->if_info[i].service;
				buffer_ptr++;

			}
			else
			{
				node_id	= IMCP_IF_802_11_INFO_ID;
				node_len       = FIXED_802_11_SIZE + strlen(imcp_apconfig_data->if_info[i].name) +  strlen(imcp_apconfig_data->if_info[i].essid)
					+ imcp_apconfig_data->if_info[i].dca_list_count + get_security_info_size(&imcp_apconfig_data->if_info[i].security_info);

                                if((CHECK_FOR_N_SUBTYPE_TO_ADD_HT_CAPABILITES(imcp_apconfig_data->if_info[i].phy_sub_type)) || (CHECK_FOR_AC_SUBTYPE_TO_ADD_VHT_CAPABILITES(imcp_apconfig_data->if_info[i].phy_sub_type)))/*Macro will give info about phy_sub_type related to N things */
				{
					node_len |=  IMCP_EXT_MASK; /*setting ext_bit as ht_capab fields are present ARchana uncommented*/
				} 
                                node_id = al_impl_cpu_to_le16(node_id);
                                memcpy(buffer_ptr, &node_id, sizeof(unsigned short));
                                                                
				buffer_ptr += sizeof(unsigned short); 
                                node_len = al_impl_cpu_to_le16(node_len);
                                memcpy(buffer_ptr,&node_len, sizeof(unsigned short));
                                buffer_ptr += 2;

                                 
				if_name_length = strlen(imcp_apconfig_data->if_info[i].name);

				*buffer_ptr = if_name_length;
				buffer_ptr++;

				memcpy(buffer_ptr, imcp_apconfig_data->if_info[i].name, if_name_length);
				buffer_ptr += if_name_length;

				*buffer_ptr = imcp_apconfig_data->if_info[i].phy_type;
				buffer_ptr++;

				/* TODO remove this field */
				*buffer_ptr = imcp_apconfig_data->if_info[i].phy_sub_type;
				buffer_ptr++;

				if (imcp_apconfig_data->if_info[i].use_type == AL_CONF_IF_USE_TYPE_AP)
				{
					imcp_apconfig_data->if_info[i].use_type = AL_CONF_IF_USE_TYPE_WM;
				}
				*buffer_ptr = imcp_apconfig_data->if_info[i].use_type;
				buffer_ptr++;

				*buffer_ptr = imcp_apconfig_data->if_info[i].wm_channel;
				buffer_ptr++;

				*buffer_ptr = imcp_apconfig_data->if_info[i].bonding;
				buffer_ptr++;

				*buffer_ptr = imcp_apconfig_data->if_info[i].dca;
				buffer_ptr++;

				*buffer_ptr = imcp_apconfig_data->if_info[i].txpower;
				buffer_ptr++;

				cpu2le = al_impl_cpu_to_le32(imcp_apconfig_data->if_info[i].txrate);
				memcpy(buffer_ptr, &cpu2le, sizeof(unsigned int));
				buffer_ptr += sizeof(unsigned int);

				*buffer_ptr = imcp_apconfig_data->if_info[i].service;
				buffer_ptr++;

				essid_length = 0;
				essid_length = strlen(imcp_apconfig_data->if_info[i].essid);
				*buffer_ptr  = essid_length;
				buffer_ptr++;

				if (essid_length > 0)
				{
					memcpy(buffer_ptr, imcp_apconfig_data->if_info[i].essid, essid_length);
					buffer_ptr += essid_length;
				}

				cpu2le = al_impl_cpu_to_le32(imcp_apconfig_data->if_info[i].rts_th);
				memcpy(buffer_ptr, &cpu2le, sizeof(unsigned int));
				buffer_ptr += sizeof(unsigned int);

				cpu2le = al_impl_cpu_to_le32(imcp_apconfig_data->if_info[i].frag_th);
				memcpy(buffer_ptr, &cpu2le, sizeof(unsigned int));
				buffer_ptr += sizeof(unsigned int);

				cpu2le = al_impl_cpu_to_le32(imcp_apconfig_data->if_info[i].beacon_interval);
				memcpy(buffer_ptr, &cpu2le, sizeof(unsigned int));
				buffer_ptr += sizeof(unsigned int);

				*buffer_ptr = imcp_apconfig_data->if_info[i].dca_list_count;
				buffer_ptr++;

				if (imcp_apconfig_data->if_info[i].dca_list_count > 0)
				{
					for (j = 0; j < imcp_apconfig_data->if_info[i].dca_list_count; j++)
					{
						buffer_ptr[j] = imcp_apconfig_data->if_info[i].dca_list[j];
					}
					buffer_ptr += imcp_apconfig_data->if_info[i].dca_list_count;
				}

				make_security_info_packet_buffer(&buffer_ptr, &imcp_apconfig_data->if_info[i].security_info); 
				if(CHECK_FOR_N_SUBTYPE_TO_ADD_HT_CAPABILITES(imcp_apconfig_data->if_info[i].phy_sub_type))
				{

					make_ht_capab_info_packet_buffer(&buffer_ptr, &imcp_apconfig_data->if_info[i].ht_capab);
					make_fram_agre_info_packet_buffer(&buffer_ptr, &imcp_apconfig_data->if_info[i].frame_aggregation);
				}
				else if(CHECK_FOR_AC_SUBTYPE_TO_ADD_VHT_CAPABILITES(imcp_apconfig_data->if_info[i].phy_sub_type))
				{	
					make_vht_capab_info_packet_buffer(&buffer_ptr, &imcp_apconfig_data->if_info[i]);


				}
                                
			} 
		} 
	}

	ret = send_packet(buffer, packet_size, client_addr);
      	al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"CONFIGD : INFO : IMCP_AP_CONFIG Packet sent (Bytes = %d) : %s : %d\n",ret,__func__,__LINE__);
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit: %s: %d\n",__func__,__LINE__);

	return 0;
}


int get_security_info_size(al_security_info_t *security_info)
{
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter: %s: %d\n",__func__,__LINE__);
	int i;
	int size;

	size = 8;          //Fixed part of Security length

	if (security_info->_security_wep.enabled)
	{
		size += 3;
		for (i = 0; i < security_info->_security_wep.key_count; i++)
		{
			size += security_info->_security_wep.keys[i].len;
		}
	}

	if (security_info->_security_rsn_psk.enabled)
	{
		size += 6 + security_info->_security_rsn_psk.key_buffer_len;
	}

	if (security_info->_security_rsn_radius.enabled)
	{
		size += 14 + security_info->_security_rsn_radius.radius_secret_key_len;
	}

	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit: %s: %d\n",__func__,__LINE__);
	return size;
}


int make_security_info_packet_buffer(unsigned char **buffer, al_security_info_t *sec_info)
{
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter: %s: %d\n",__func__,__LINE__);
	unsigned char  key_length = 0;
	unsigned int   i          = 0;
	unsigned char  *buffer_ptr;
	unsigned int   cpu2le;
	unsigned int cpu2le32;
	unsigned char  decode_output[512];
	int            len;

	buffer_ptr = *buffer;

	*buffer_ptr = SECURITY_INFO_TYPE_DEFAULT;
	buffer_ptr++;

	*buffer_ptr = sec_info->_security_none.enabled;
	buffer_ptr++;

	*buffer_ptr = SECURITY_INFO_TYPE_WEP;
	buffer_ptr++;

	*buffer_ptr = sec_info->_security_wep.enabled;
	buffer_ptr++;

	if (sec_info->_security_wep.enabled)
	{
		*buffer_ptr = sec_info->_security_wep.strength;
		buffer_ptr++;

		*buffer_ptr = sec_info->_security_wep.key_index;
		buffer_ptr++;

		switch (sec_info->_security_wep.strength)
		{
			case 40:
				key_length  = (40 / 8) * 4;
				*buffer_ptr = key_length;
				buffer_ptr++;
				for (i = 0; i < 4; i++)
				{
					memcpy(buffer_ptr, sec_info->_security_wep.keys[i].bytes, 40 / 8);
					buffer_ptr += (40 / 8);
				}
				break;

			case 104:
				key_length  = (104 / 8);
				*buffer_ptr = key_length;
				buffer_ptr++;
				memcpy(buffer_ptr, sec_info->_security_wep.keys[i].bytes, 104 / 8);
				buffer_ptr += (104 / 8);
		}
	}

	*buffer_ptr = SECURITY_INFO_TYPE_RSN_PSK;
	buffer_ptr++;

	*buffer_ptr = sec_info->_security_rsn_psk.enabled;
	buffer_ptr++;

	if (sec_info->_security_rsn_psk.enabled)
	{
		*buffer_ptr = sec_info->_security_rsn_psk.mode;
		buffer_ptr++;

		*buffer_ptr = 32;
		buffer_ptr++;                                 /** PSK length is always 32 bytes */
		al_decode_key(AL_CONTEXT sec_info->_security_rsn_psk.key_buffer, _MAC_ADDRESS, decode_output);
		memcpy(buffer_ptr, decode_output, 32);
		buffer_ptr += 32;

		*buffer_ptr = sec_info->_security_rsn_psk.cipher_tkip;
		buffer_ptr++;

		*buffer_ptr = sec_info->_security_rsn_psk.cipher_ccmp;
		buffer_ptr++;

		cpu2le32 = al_impl_cpu_to_le32(sec_info->_security_rsn_psk.group_key_renewal);
		memcpy(buffer_ptr, &cpu2le32, 4);
		buffer_ptr += 4;
	}

	*buffer_ptr = SECURITY_INFO_TYPE_RSN_RAD;
	buffer_ptr++;

	*buffer_ptr = sec_info->_security_rsn_radius.enabled;
	buffer_ptr++;

	if (sec_info->_security_rsn_radius.enabled)
	{
		*buffer_ptr = sec_info->_security_rsn_radius.mode;
		buffer_ptr++;
		memcpy(buffer_ptr, sec_info->_security_rsn_radius.radius_server, 4);
		buffer_ptr += 4;

		cpu2le = al_impl_cpu_to_le32(sec_info->_security_rsn_radius.radius_port);
		memcpy(buffer_ptr, &cpu2le, sizeof(unsigned int));
		buffer_ptr += sizeof(unsigned int);

		memset(decode_output, 0, sizeof(decode_output));
		al_decode_key(AL_CONTEXT sec_info->_security_rsn_radius.radius_secret_key, _MAC_ADDRESS, decode_output);
		len         = strlen((char *)decode_output);
		*buffer_ptr = len;
		buffer_ptr++;
		memcpy(buffer_ptr, decode_output, len);
		buffer_ptr += len;

		*buffer_ptr = sec_info->_security_rsn_radius.cipher_tkip;
		buffer_ptr++;

		*buffer_ptr = sec_info->_security_rsn_radius.cipher_ccmp;
		buffer_ptr++;

		cpu2le32 = al_impl_cpu_to_le32(sec_info->_security_rsn_radius.group_key_renewal);
		memcpy(buffer_ptr, &cpu2le32, 4);
		buffer_ptr += 4;
	}

	*buffer = buffer_ptr;
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit: %s: %d\n",__func__,__LINE__);
}

int make_ht_capab_info_packet_buffer(unsigned char **buffer, al_ht_capab_t *ht_capab)
{
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter: %s: %d\n",__func__,__LINE__);
	unsigned char  *buffer_ptr;
	unsigned int   cpu2le;
	unsigned short node_id;
	unsigned short node_len;

	buffer_ptr = *buffer;

	node_id		= IMCP_IF_802_11_EXT_11N_INFO_ID;
	node_len	= IMCP_HT_CAP_FIXED_LEN;
	IMCP_MSG_PROCESS_SEND_ID_AND_LEN(buffer_ptr,node_id,node_len);

	*buffer_ptr = ht_capab->ldpc;
	buffer_ptr++;
	
	*buffer_ptr = ht_capab->ht_bandwidth;
	buffer_ptr++;

	cpu2le = al_impl_cpu_to_le32(ht_capab->sec_offset);	
	memcpy(buffer_ptr,&cpu2le, sizeof(unsigned int));
	buffer_ptr +=  sizeof(unsigned int);

	/**buffer_ptr = ht_capab->smps;
	buffer_ptr++;*/

	*buffer_ptr = ht_capab->gi_20;
	buffer_ptr++;

	*buffer_ptr = ht_capab->gi_40;
	buffer_ptr++;

	*buffer_ptr = ht_capab->tx_stbc;
	buffer_ptr++;

	*buffer_ptr = ht_capab->rx_stbc;
	buffer_ptr++;

	/**buffer_ptr = ht_capab->delayed_ba;
	buffer_ptr++;*/

	*buffer_ptr = ht_capab->gfmode;
	buffer_ptr++;

	cpu2le = al_impl_cpu_to_le32(ht_capab->max_amsdu_len);
	memcpy(buffer_ptr,&cpu2le, sizeof(unsigned int));
	buffer_ptr +=  sizeof(unsigned int);


	/**buffer_ptr = ht_capab->dsss_cck_40;
	buffer_ptr++;*/

	*buffer_ptr = ht_capab->intolerant;
	buffer_ptr++;

	/**buffer_ptr = ht_capab->lsig_txop;
	buffer_ptr++;*/

	*buffer = buffer_ptr;
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit: %s: %d\n",__func__,__LINE__);
}
int make_fram_agre_info_packet_buffer(unsigned char **buffer, al_fram_agre_t *frame_aggregation)
{
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter: %s: %d\n",__func__,__LINE__);
	unsigned char  *buffer_ptr;
	unsigned int   cpu2le;

	buffer_ptr = *buffer;

       *buffer_ptr = frame_aggregation->ampdu_enable;
        buffer_ptr++;
	
	cpu2le = al_impl_cpu_to_le32(frame_aggregation->max_ampdu_len);
	memcpy(buffer_ptr,&cpu2le, sizeof(unsigned int));
	buffer_ptr += sizeof(unsigned int);

	*buffer = buffer_ptr;
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit: %s: %d\n",__func__,__LINE__);
}
int make_vht_capab_info_packet_buffer(unsigned char **buffer , al_conf_if_info_t *if_info)
{	
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter: %s: %d\n",__func__,__LINE__);
	unsigned char  *buffer_ptr;
	buffer_ptr = *buffer;
	unsigned short node_id;
        unsigned short node_len;
        unsigned short cpu2le;


		node_id         = IMCP_IF_802_11_EXT_11N_11AC_INFO_ID;
		node_len        = IMCP_HT_CAP_FIXED_LEN + 4 ;
		IMCP_MSG_PROCESS_SEND_ID_AND_LEN(buffer_ptr,node_id,node_len);

		*buffer_ptr = if_info->ht_capab.ldpc;
        buffer_ptr++;
		
		if(if_info->vht_capab.vht_oper_bandwidth == 1) {
			*buffer_ptr = AL_CONF_IF_HT_PARAM_80;
		}
		else {
			*buffer_ptr = if_info->ht_capab.ht_bandwidth;
		}
        buffer_ptr++;

        cpu2le = al_impl_cpu_to_le32(if_info->ht_capab.sec_offset);
        memcpy(buffer_ptr,&cpu2le, sizeof(unsigned int));
        buffer_ptr +=  sizeof(unsigned int);

        /**buffer_ptr = ht_capab->smps;
        buffer_ptr++;*/

        *buffer_ptr = if_info->ht_capab.gi_20;
        buffer_ptr++;

        *buffer_ptr = if_info->ht_capab.gi_40;
        buffer_ptr++;

        *buffer_ptr = if_info->ht_capab.tx_stbc;
        buffer_ptr++;

        *buffer_ptr = if_info->ht_capab.rx_stbc;
        buffer_ptr++;

        /**buffer_ptr = ht_capab->delayed_ba;
        buffer_ptr++;*/

        *buffer_ptr = if_info->ht_capab.gfmode;
        buffer_ptr++;
	
	cpu2le = al_impl_cpu_to_le32(if_info->ht_capab.max_amsdu_len);
        memcpy(buffer_ptr,&cpu2le, sizeof(unsigned int));
        buffer_ptr +=  sizeof(unsigned int);


        /**buffer_ptr = ht_capab->dsss_cck_40;
        buffer_ptr++;*/

        *buffer_ptr = if_info->ht_capab.intolerant;
        buffer_ptr++;

        /**buffer_ptr = ht_capab->lsig_txop;
        buffer_ptr++;*/

       *buffer_ptr = if_info->frame_aggregation.ampdu_enable;
        buffer_ptr++;

        cpu2le = al_impl_cpu_to_le32(if_info->frame_aggregation.max_ampdu_len);
        memcpy(buffer_ptr,&cpu2le, sizeof(unsigned int));
        buffer_ptr += sizeof(unsigned int);

		cpu2le = al_impl_cpu_to_le32(if_info->vht_capab.max_mpdu_len);
		memcpy(buffer_ptr,&cpu2le, sizeof(unsigned int));
		buffer_ptr += sizeof(unsigned int);
		
		*buffer_ptr = if_info->vht_capab.gi_80;
		 buffer_ptr++;

		*buffer = buffer_ptr;
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit: %s: %d\n",__func__,__LINE__);
}

int send_vlan_config_info_reply(struct sockaddr_in *client_addr, unsigned char response_id)
{
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter: %s: %d\n",__func__,__LINE__);
	int            packet_size = 0;
	unsigned char  *buffer;
	unsigned char  *buffer_ptr;
	int            vlan_num = 0;
	int            len;
	int            i, ret;
	int            j;
	unsigned int   cpu2le;
	unsigned short cpu2le16;
	int            key_length = 0;

	al_security_info_t    *sec_info;
	imcp_vlan_info_data_t *vlan_info;
	unsigned short node_id;
	unsigned short node_len;

	packet_size = get_imcp_signature_mesh_id_size() + 14;
	packet_size += IMCP_ID_LEN_SIZE; 

	for (vlan_num = 0; vlan_num < imcp_vlan_config_info_data->vlan_count - 1; vlan_num++)
	{
		packet_size += 29;
		packet_size += strlen(imcp_vlan_config_info_data->imcp_vlan_info[vlan_num].vlan_name);
		packet_size += strlen(imcp_vlan_config_info_data->imcp_vlan_info[vlan_num].essid);
		packet_size += get_security_info_size(&(imcp_vlan_config_info_data->imcp_vlan_info[vlan_num].security_info));
	}

	buffer     = (unsigned char *)malloc(packet_size);   /// remember to free
	buffer_ptr = buffer;

	buffer_ptr += copy_imcp_signature_packet_type_meshid(buffer_ptr, IMCP_VLAN_CONFIGURATION_INFO);

	node_id		= IMCP_VLAN_CONFIG_INFO_ID;
	node_len	= packet_size -  get_imcp_signature_mesh_id_size() - IMCP_ID_LEN_SIZE;	
	IMCP_MSG_PROCESS_SEND_ID_AND_LEN(buffer_ptr,node_id,node_len);

		memcpy(buffer_ptr, imcp_vlan_config_info_data->mac_address, 6);
	buffer_ptr += 6;

	*buffer_ptr = TYPE_RESPONSE;
	buffer_ptr++;

	*buffer_ptr = response_id;
	buffer_ptr++;

	*buffer_ptr = imcp_vlan_config_info_data->vlan_count;
	buffer_ptr++;

	cpu2le16 = al_impl_cpu_to_le16(imcp_vlan_config_info_data->tag);
	memcpy(buffer_ptr, &cpu2le16, 2);
	buffer_ptr += 2;


	*buffer_ptr = imcp_vlan_config_info_data->dot1p_priority;
	buffer_ptr++;

	*buffer_ptr = imcp_vlan_config_info_data->dot11e_enabled;
	buffer_ptr++;

	*buffer_ptr = imcp_vlan_config_info_data->dot11e_category;
	buffer_ptr++;

	/**
	 *	although vlan_count is inclusive of default vlan_info
	 *	but we copy vlan_info exclusive of default vlan_info in for loop below
	 */

	for (vlan_num = 0; vlan_num < imcp_vlan_config_info_data->vlan_count - 1; vlan_num++)
	{
		vlan_info = &(imcp_vlan_config_info_data->imcp_vlan_info[vlan_num]);

		len         = strlen(vlan_info->vlan_name);
		*buffer_ptr = len;
		buffer_ptr++;
		memcpy(buffer_ptr, vlan_info->vlan_name, len);
		buffer_ptr += len;

		memcpy(buffer_ptr, vlan_info->ip_address, 4);
		buffer_ptr += 4;

		cpu2le16 = al_impl_cpu_to_le16(vlan_info->vlan_tag);
		memcpy(buffer_ptr, &cpu2le16, 2);
		buffer_ptr += 2;

		*buffer_ptr = vlan_info->dot11e_enabled;
		buffer_ptr++;

		*buffer_ptr = vlan_info->dot1p_p;
		buffer_ptr++;

		*buffer_ptr = vlan_info->dot11e_category;
		buffer_ptr++;

		len         = strlen(vlan_info->essid);
		*buffer_ptr = len;
		buffer_ptr++;
		memcpy(buffer_ptr, vlan_info->essid, len);
		buffer_ptr += len;

		cpu2le = al_impl_cpu_to_le32(vlan_info->rts_threshold);
		memcpy(buffer_ptr, &cpu2le, sizeof(unsigned int));
		buffer_ptr += sizeof(unsigned int);

		cpu2le = al_impl_cpu_to_le32(vlan_info->frag_threshold);
		memcpy(buffer_ptr, &cpu2le, sizeof(unsigned int));
		buffer_ptr += sizeof(unsigned int);

		cpu2le = al_impl_cpu_to_le32(vlan_info->beacon_int);
		memcpy(buffer_ptr, &cpu2le, sizeof(unsigned int));
		buffer_ptr += sizeof(unsigned int);

		*buffer_ptr = vlan_info->service_type;
		buffer_ptr++;

		*buffer_ptr = vlan_info->transmit_power;
		buffer_ptr++;

		cpu2le = al_impl_cpu_to_le32(vlan_info->txrate);
		memcpy(buffer_ptr, &cpu2le, sizeof(unsigned int));
		buffer_ptr += sizeof(unsigned int);

		/*
		 *	copying of security values.
		 */

		sec_info = &(vlan_info->security_info);

		make_security_info_packet_buffer(&buffer_ptr, sec_info);
	}

	ret = send_packet(buffer, packet_size, client_addr);
      	al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"CONFIGD : INFO : IMCP_VLAN_CONFIG  Packet sent (Bytes = %d) : %s : %d\n",ret,__func__,__LINE__);
	free(buffer);
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit: %s: %d\n",__func__,__LINE__);
	return 0;
}

int send_sw_response_packet(unsigned char response_action, struct sockaddr_in *client_addr)
{
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter: %s: %d\n",__func__,__LINE__);
	unsigned char buffer[512] = {0};
	unsigned int pub_key_len;
   unsigned char *buffer_ptr;
   int           sock, clilen;
   int           ret;
   int           packet_size=0;
   unsigned short node_id = 0;
   unsigned short node_len = 0;
	unsigned char *pub_k;
	FILE          *fp;
	unsigned char			  *action_ptr, *node_id_ptr;
	enum actions resp_act;

	packet_size = get_imcp_signature_mesh_id_size();

	buffer_ptr = buffer;
   buffer_ptr += copy_imcp_signature_packet_type_meshid(buffer_ptr, IMCP_SW_UPDATE_RESPONSE);

	node_id_ptr = buffer_ptr;
	node_id = IMCP_SW_UPDATE_REQUEST_RESPONSE_INFO_ID;		// node_id and node_len added before sending the packet
	buffer_ptr += sizeof(unsigned short);				//skips pointer to node_id
	buffer_ptr += sizeof(unsigned short);				//skips pointer to node_len
	packet_size += sizeof(unsigned short) + sizeof(unsigned short);

	memcpy(buffer_ptr, _MAC_ADDRESS, 6);				// MAC Address of mip0
	buffer_ptr += 6;
	packet_size += 6;

	action_ptr = buffer_ptr;
	*buffer_ptr = response_action;
	buffer_ptr += sizeof(unsigned char);
	packet_size += sizeof(unsigned char);

	switch(response_action)
	{
		case FW_SET_KEY:
			fp = fopen("/tmp/pubkey", "rb");				// opened pubkey file to send key to NMS
			if(fp == NULL)
			{
				perror("fopen");
				resp_act = FW_UPGRADE_FAILED;
				*action_ptr = resp_act;
				goto upgradeFailed;
			}

			fseek(fp, 0, SEEK_END);
			pub_key_len = ftell(fp);					// Length of Public key
			pub_k = (unsigned char *)&pub_key_len;
			fseek(fp, 0, SEEK_SET);

			memcpy(buffer_ptr, pub_k, 2);
			al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"CONFIGD : INFO : tpub_key_len=%d\t : %s: %d\n",(unsigned int)*buffer_ptr,__func__,__LINE__);
			buffer_ptr = buffer_ptr + 2;
			packet_size += sizeof(pub_key_len);

			if((fread(buffer_ptr, 1, pub_key_len, fp))!= pub_key_len)		// buffer_ptr points to public key
			{
				fclose(fp);
				perror("fread()");
				resp_act = FW_UPGRADE_FAILED;
	            *action_ptr = resp_act;
    	        goto upgradeFailed;				
			}
			fclose(fp);

			buffer_ptr += pub_key_len;
			packet_size += pub_key_len;
			node_len = buffer_ptr - action_ptr;
			
			IMCP_MSG_PROCESS_SEND_ID_AND_LEN(node_id_ptr, node_id, node_len);

			ret = send_packet(buffer, packet_size, client_addr);			// Public key is sent to NMS 
      			al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"CONFIGD : INFO : IMCP_SOFTWARE_UPDATE_RESPONSE Packet sent (Bytes = %d) : %s : %d\n",ret,__func__,__LINE__);
			break;

		case FW_UPGRADE_STARTING:
			
			node_len = buffer_ptr - action_ptr;
			IMCP_MSG_PROCESS_SEND_ID_AND_LEN(node_id_ptr, node_id, node_len);

			ret = send_packet(buffer, packet_size, client_addr);		// Upgrade started status is sent to NMS
      			al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"CONFIGD : INFO : IMCP_SOFTWARE_UPDATE_RESPONSE Packet sent (Bytes = %d) : %s : %d\n",ret,__func__,__LINE__);
			break;

		case FW_DOWNLOAD_FAILED:
			node_len = buffer_ptr - action_ptr;
         	IMCP_MSG_PROCESS_SEND_ID_AND_LEN(node_id_ptr, node_id, node_len);

        	ret = send_packet(buffer, packet_size, client_addr);	// Failure status to download firmware image is sent to NMS
      			al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"CONFIGD : INFO : IMCP_SOFTWARE_UPDATE_RESPONSE Packet sent (Bytes = %d),Action: FIRMWARE_DOWNLOAD_FAILED : %s : %d\n",ret,__func__,__LINE__);
			break;

		case FW_UPGRADE_FAILED:
upgradeFailed:
			node_len = buffer_ptr - action_ptr;
			IMCP_MSG_PROCESS_SEND_ID_AND_LEN(node_id_ptr, node_id, node_len);

			ret = send_packet(buffer, packet_size, client_addr);	// Upgrade failure status is sent to NMS
      			al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"CONFIGD : INFO :IMCP_SOFTWARE_UPDATE_RESPONSE Packet sent (Bytes = %d)\tAction: UPGRADE_FAILED: %s : %d\n",ret,__func__,__LINE__);
			break;

		case FW_UPGRADE_TIMEOUT:
			node_len = buffer_ptr - action_ptr;
         IMCP_MSG_PROCESS_SEND_ID_AND_LEN(node_id_ptr, node_id, node_len);

         ret = send_packet(buffer, packet_size, client_addr);	//Timeout is sent to NMS
			al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"CONFIGD : INFO : IMCP_SOFTWARE_UPDATE_RESPONSE Packet sent (Bytes = %d)\tAction: FW_UPGRADE_TIMEOUT : %s: %d\n",ret,__func__,__LINE__);
			break;

		default: /*Should never reach here*/
			al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"CONFIGD : INFO :Response action not found : %s: %d\n",__func__,__LINE__);
      			al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"CONFIGD : INFO : Response action not found : %s : %d\n",__func__,__LINE__);
			resp_act = FW_UPGRADE_FAILED;
			*action_ptr = resp_act;
			goto upgradeFailed;
	}
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit: %s: %d\n",__func__,__LINE__);
}

int imcp_start_ip_if_request_helper(unsigned char *buffer, struct sockaddr_in *client_addr)
{
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter: %s: %d\n",__func__,__LINE__);
	FILE          *fp;
	int           bytes[4];
	unsigned char line[80];
	unsigned char ip_address[10];
	unsigned char subnet_mask[10];
	unsigned char broadcast_address[15];
	char          command[80];
	
	unsigned short  node_id;
   	unsigned short  node_len;
	
	unsigned char *buffer_ptr = buffer + get_imcp_signature_mesh_id_size();
	
   	IMCP_MSG_PROCESS_RECEIVE_ID_AND_LEN(buffer_ptr,node_id,node_len);

	memcpy(imcp_start_ip_if_data->mac_address, buffer_ptr, 6);
	buffer_ptr += 6;

	imcp_start_ip_if_data->interface_name_length = (int)*buffer_ptr++;

	if (imcp_start_ip_if_data->interface_name_length > 0)
	{
		memcpy(imcp_start_ip_if_data->interface_name, buffer_ptr, imcp_start_ip_if_data->interface_name_length);
		imcp_start_ip_if_data->interface_name[imcp_start_ip_if_data->interface_name_length] = 0;
		buffer_ptr += imcp_start_ip_if_data->interface_name_length;
	}
	else
	{
      		al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : Interface Name Length 0 for Start Ip Interfacefunc : %s: %d\n",__func__,__LINE__);
		return -1;
	}

	/*
	 * if(!strcmp(current_if_name,imcp_start_ip_if_data->interface_name)){
	 *      printf("Current %s is  new %s  (Configuration already up), returning", current_if_name, imcp_start_ip_if_data->interface_name);
	 *      return 0;
	 * }
	 */

	sprintf(command, "uci get network.mip.ipaddr > /tmp/network");
	EXECUTE_COMMAND(command);

	fp = fopen("/tmp/network", "r");

	if (!fp)
	{
      		al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"CONFIGD : INFO : /tmp/network not found : %s : %d\n",__func__,__LINE__);
		return;
	}

	fscanf(fp, "%s", line);
	sscanf((const char *)line, "%d.%d.%d.%d", &bytes[0], &bytes[1], &bytes[2], &bytes[3]);
	ip_address[0] = bytes[0];
	ip_address[1] = bytes[1];
	ip_address[2] = bytes[2];
	ip_address[3] = bytes[3];

	fclose(fp);

	sprintf(command, "uci get network.mip.netmask > /tmp/network");
	EXECUTE_COMMAND(command);

	fp = fopen("/tmp/network", "r");

	if (!fp)
	{
      		al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : /tmp/network not found: %s: %d\n",__func__,__LINE__);
		return;
	}

	fscanf(fp, "%s", line);
	sscanf((const char *)line, "%d.%d.%d.%d", &bytes[0], &bytes[1], &bytes[2], &bytes[3]);
	subnet_mask[0] = bytes[0];
	subnet_mask[1] = bytes[1];
	subnet_mask[2] = bytes[2];
	subnet_mask[3] = bytes[3];

	fclose(fp);
	sprintf(command, "uci get network.mip.broadcast > /tmp/network");
	EXECUTE_COMMAND(command);

	fp = fopen("/tmp/network", "r");

	if (!fp)
	{
      		al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : /tmp/network not foundfunc : %s: %d\n",__func__,__LINE__);
		return;
	}

	fscanf(fp, "%s", line);
	sscanf((const char *)line, "%d.%d.%d.%d", &bytes[0], &bytes[1], &bytes[2], &bytes[3]);
	broadcast_address[0] = bytes[0];
	broadcast_address[1] = bytes[1];
	broadcast_address[2] = bytes[2];
	broadcast_address[3] = bytes[3];
	fclose(fp);

	sprintf(command, "ifconfig %s down", current_if_name);
	EXECUTE_COMMAND(command)

		sprintf(command, "ifconfig %s %d.%d.%d.%d netmask %d.%d.%d.%d broadcast %d.%d.%d.%d up",
				imcp_start_ip_if_data->interface_name,
				ip_address[0], ip_address[1],
				ip_address[2], ip_address[3],
				subnet_mask[0], subnet_mask[1],
				subnet_mask[2], subnet_mask[3],
				broadcast_address[0], broadcast_address[1],
				broadcast_address[2], broadcast_address[3]
		       );

	EXECUTE_COMMAND(command)

		strcpy(current_if_name, imcp_start_ip_if_data->interface_name);
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit: %s: %d\n",__func__,__LINE__);

	return 0;
}


int imcp_ipconfig_request_response_helper(unsigned char *buffer, struct sockaddr_in *client_addr)
{
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter: %s: %d\n",__func__,__LINE__);

	unsigned short node_id;
	unsigned short node_len;

	unsigned char *buffer_ptr = buffer + get_imcp_signature_mesh_id_size();

      	al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"CONFIGD : INFO : IMCP_AP_IP_CONFIG_REQUEST_RESPONSE Packet - Received: %s line: %d\n",__func__,__LINE__);

	memset(imcp_ipconfig_rr_data, 0, sizeof(imcp_ipconfig_rr_data_t));

	IMCP_MSG_PROCESS_RECEIVE_ID_AND_LEN(buffer_ptr,node_id,node_len);

	memcpy(imcp_ipconfig_rr_data->mac_address, buffer_ptr, 6);
	buffer_ptr += 6;

	imcp_ipconfig_rr_data->request_response_type = *buffer_ptr;
	buffer_ptr++;

	buffer_ptr++;     /* for type*/

	if (imcp_ipconfig_rr_data->request_response_type == TYPE_RESPONSE)
	{
      	al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : IMCP_AP_IP_CONFIG_REQUEST_RESPONSE Packet ignored: %s line: %d\n",__func__,__LINE__);
		return 0;
	}

	if (memcmp(imcp_ipconfig_rr_data->mac_address, _MAC_ADDRESS, 6))
	{
      	al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : IMCP_AP_IP_CONFIG_REQUEST_RESPONSE Packet ignored: %s line: %d\n",__func__,__LINE__);
		return 0;
	}

	imcp_ipconfig_rr_data->response_id = *buffer_ptr;
	buffer_ptr++;

	if (!read_ipconfig_data_from_files())
	{
		memcpy(imcp_ipconfig_data->mac_address, imcp_ipconfig_rr_data->mac_address, 6);
		imcp_ipconfig_data->request_response_type = TYPE_RESPONSE;
		send_ip_config_info_reply(client_addr, imcp_ipconfig_rr_data->response_id);
	}

	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit: %s: %d\n",__func__,__LINE__);
	return 0;
}

int imcp_ipconfig_info_helper(unsigned char *buffer, struct sockaddr_in *client_addr)
{
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter: %s: %d\n",__func__,__LINE__);
	unsigned char *buffer_ptr = buffer + get_imcp_signature_mesh_id_size();
	int           name_len;
	char          command[80];
	int           config_sqnr;
	int           al_conf_handle;
	unsigned short node_id;
	unsigned short node_len;
	unsigned char  status = 0;	//Added this field for run_time validations

      	al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"CONFIGD : INFO : IMCP_IP_CONFIGURATION_INFO Packet: %s line: %d\n",__func__,__LINE__);

	memset(imcp_ipconfig_data, 0, sizeof(imcp_ipconfig_data_t));

	IMCP_MSG_PROCESS_RECEIVE_ID_AND_LEN(buffer_ptr,node_id,node_len);

	memcpy(imcp_ipconfig_data->mac_address, buffer_ptr, 6);
	buffer_ptr += 6;

	imcp_ipconfig_data->request_response_type = *buffer_ptr;
	buffer_ptr++;

	if (imcp_ipconfig_data->request_response_type == TYPE_RESPONSE)
	{
      		al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : IMCP_IP_CONFIGURATION_INFO  Packet ignored: %s line: %d\n",__func__,__LINE__);
		return 0;
	}

	if (memcmp(imcp_ipconfig_data->mac_address, _MAC_ADDRESS, 6))
	{
      		al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : IMCP_IP_CONFIGURATION_INFO  Packet ignored: %s line: %d\n",__func__,__LINE__);
		return 0;
	}

	imcp_ipconfig_data->response_id = *buffer_ptr;
	buffer_ptr++;

	imcp_ipconfig_data->host_name_length = (int)*buffer_ptr;
	buffer_ptr++;

	if (imcp_ipconfig_data->host_name_length > 0)
	{
		memcpy(imcp_ipconfig_data->host_name, buffer_ptr, imcp_ipconfig_data->host_name_length);
		imcp_ipconfig_data->host_name[imcp_ipconfig_data->host_name_length] = 0;
		buffer_ptr += imcp_ipconfig_data->host_name_length;
	}

	memcpy(imcp_ipconfig_data->ip_address, buffer_ptr, 4);
	buffer_ptr += 4;

	memcpy(imcp_ipconfig_data->subnet_mask, buffer_ptr, 4);
	buffer_ptr += 4;

	memcpy(imcp_ipconfig_data->gateway, buffer_ptr, 4);
	buffer_ptr += 4;

	EXECUTE_PRE_FS_CHANGE
		update_network_file();

	update_ifconfig_file();
	update_hosts_file();

	/*
	 *	Increment config sqnr
	 */
	al_conf_handle = libalconf_read_config_data();
	if (al_conf_handle != -1)
	{
		set_mesh_config_sqnr(increment_config_sqnr(al_conf_handle));
		al_conf_put(AL_CONTEXT al_conf_handle);
		al_conf_close(AL_CONTEXT al_conf_handle);
	}
	EXECUTE_POST_FS_CHANGE(POST_FS_PARAM_TYPE_CONFIG)


#ifdef CONFIGD_NO_MIP
		sprintf(command, "ifconfig %s %d.%d.%d.%d netmask %d.%d.%d.%d broadcast %d.%d.%d.255",
				current_if_name,
				imcp_ipconfig_data->ip_address[0], imcp_ipconfig_data->ip_address[1],
				imcp_ipconfig_data->ip_address[2], imcp_ipconfig_data->ip_address[3],
				imcp_ipconfig_data->subnet_mask[0], imcp_ipconfig_data->subnet_mask[1],
				imcp_ipconfig_data->subnet_mask[2], imcp_ipconfig_data->subnet_mask[3],
				imcp_ipconfig_data->ip_address[0], imcp_ipconfig_data->ip_address[1],
				imcp_ipconfig_data->ip_address[2]
		       );

#else
	sprintf(command, "ifconfig mip0 %d.%d.%d.%d netmask %d.%d.%d.%d broadcast %d.%d.%d.255",
			imcp_ipconfig_data->ip_address[0], imcp_ipconfig_data->ip_address[1],
			imcp_ipconfig_data->ip_address[2], imcp_ipconfig_data->ip_address[3],
			imcp_ipconfig_data->subnet_mask[0], imcp_ipconfig_data->subnet_mask[1],
			imcp_ipconfig_data->subnet_mask[2], imcp_ipconfig_data->subnet_mask[3],
			imcp_ipconfig_data->ip_address[0], imcp_ipconfig_data->ip_address[1],
			imcp_ipconfig_data->ip_address[2]
	       );
#endif

	EXECUTE_COMMAND(command)

		sprintf(command, "route add default gateway %d.%d.%d.%d",
				imcp_ipconfig_data->gateway[0],
				imcp_ipconfig_data->gateway[1],
				imcp_ipconfig_data->gateway[2],
				imcp_ipconfig_data->gateway[3]);

	EXECUTE_COMMAND(command)

	send_config_updated_response(client_addr, imcp_ipconfig_data->response_id, TYPE_AP_IP_CONFIG_REQUEST, SUCCESS, NULL);
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit: %s: %d\n",__func__,__LINE__);
	return 0;
}


int imcp_apconfig_request_response_helper(unsigned char *buffer, struct sockaddr_in *client_addr)
{
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter: %s: %d\n",__func__,__LINE__);
	int                    al_conf_handle;
	int                    size;
	unsigned char          *buffer_ptr;
	int                    dot11e_conf_handle;
	dot11e_conf_category_t *dot11e_data;

	unsigned short	   node_id;
	unsigned short	   node_len;

      	al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"CONFIGD : INFO : IMCP_AP_CONFIG_REQUEST_RESPONSE Packet - Received: %s line: %d\n",__func__,__LINE__);

	size = get_imcp_signature_mesh_id_size();

	buffer_ptr = buffer + size;

	memset(imcp_apconfig_rr_data, 0, sizeof(imcp_apconfig_rr_data_t));

	IMCP_MSG_PROCESS_RECEIVE_ID_AND_LEN(buffer_ptr,node_id,node_len);

	memcpy(imcp_apconfig_rr_data->mac_address, buffer_ptr, 6);
	buffer_ptr += 6;

	imcp_apconfig_rr_data->request_response_type = *buffer_ptr;
	buffer_ptr++;
	buffer_ptr++;      /* for type */
	imcp_apconfig_rr_data->response_id = *buffer_ptr;
	buffer_ptr += 1;


	if (imcp_apconfig_rr_data->request_response_type == TYPE_RESPONSE)
	{
      		al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : IMCP_AP_CONFIG_REQUEST_RESPONSE  Packet ignored: %s line: %d\n",__func__,__LINE__);
		return 0;
	}

	if (memcmp(imcp_apconfig_rr_data->mac_address, _MAC_ADDRESS, 6))
	{
      		al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : IMCP_AP_CONFIG_REQUEST_RESPONSE  Packet ignored: %s line: %d\n",__func__,__LINE__);
		return 0;
	}

	al_conf_handle = libalconf_read_config_data();

	if (al_conf_handle != -1)
	{

		memcpy(imcp_apconfig_data->mac_address, imcp_apconfig_rr_data->mac_address, 6);
		update_apconfig_data(al_conf_handle); 
		al_conf_close(AL_CONTEXT al_conf_handle);
		send_apconfig_info_reply(client_addr, imcp_apconfig_rr_data->response_id);
	}
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit: %s: %d\n",__func__,__LINE__);

	return 0;
}


int read_hide_ssid_config_data(int al_conf_handle)
{
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter: %s: %d\n",__func__,__LINE__);
	int i;
	int if_no;

	imcp_hide_ssid_config_data->request_response_type = TYPE_RESPONSE;

	imcp_hide_ssid_config_data->interface_count = al_conf_get_if_count(al_conf_handle);

	for (if_no = 0; if_no < imcp_hide_ssid_config_data->interface_count; if_no++)
	{
		al_conf_get_if(al_conf_handle, if_no, &(imcp_hide_ssid_config_data->if_info[if_no]));
	}

	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit: %s: %d\n",__func__,__LINE__);
	return 0;
}


/*
 *	Send hide_ssid configuration info to client
 */
int send_hide_ssid_config_info_reply(struct sockaddr_in *client_addr, unsigned char response_id)
{
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter: %s: %d\n",__func__,__LINE__);
	int           sock, clilen, i, j;
	int           size, if_size;
	int           packet_size;
	unsigned char buffer[1024];
	unsigned char *buffer_ptr;
	unsigned int  cpu2le;
	int           ret;
	int           if_name_length;
	unsigned short node_id;
	unsigned short node_len;

	if_size = 6 + 1 + 1 + 1;      /* MAC address, REQ-RES, RESID and Interface count */

	for (i = 0; i < imcp_hide_ssid_config_data->interface_count; i++)
	{
		if_size += 1;

		if_name_length = strlen(imcp_hide_ssid_config_data->if_info[i].name);

		if_size += if_name_length;

		if_size += 1;
	}

	packet_size = get_imcp_signature_mesh_id_size() + if_size + IMCP_ID_LEN_SIZE;

	buffer_ptr = buffer;

	buffer_ptr += copy_imcp_signature_packet_type_meshid(buffer_ptr, IMCP_HIDE_SSID_INFO);

	node_id      = IMCP_HIDE_SSID_CONFIG_INFO_REQ_RES_INFO_ID;
	node_len     = packet_size -  get_imcp_signature_mesh_id_size() - IMCP_ID_LEN_SIZE; 
	IMCP_MSG_PROCESS_SEND_ID_AND_LEN(buffer_ptr,node_id,node_len);

		memcpy(buffer_ptr, imcp_hide_ssid_config_data->mac_address, 6);
	buffer_ptr += 6;

	*buffer_ptr = imcp_hide_ssid_config_data->request_response_type;
	buffer_ptr++;

	*buffer_ptr = response_id;
	buffer_ptr++;

	*buffer_ptr = imcp_hide_ssid_config_data->interface_count;
	buffer_ptr++;

	for (i = 0; i < imcp_hide_ssid_config_data->interface_count; i++)
	{
		if_name_length = strlen(imcp_hide_ssid_config_data->if_info[i].name);

		*buffer_ptr = if_name_length;
		buffer_ptr++;

		memcpy(buffer_ptr, imcp_hide_ssid_config_data->if_info[i].name, if_name_length);
		buffer_ptr += if_name_length;

		*buffer_ptr = imcp_hide_ssid_config_data->if_info[i].hide_ssid;
		buffer_ptr++;
	}


	ret = send_packet(buffer, packet_size, client_addr);
      	al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"CONFIGD : INFO : IMCP_HIDE_SSID_CONFIG Packet sent (Bytes = %d): %s : %d\n",ret,__func__,__LINE__);

	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit: %s: %d\n",__func__,__LINE__);
	return 0;
}


int imcp_hide_ssid_request_response_helper(unsigned char *buffer, struct sockaddr_in *client_addr)
{
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter: %s: %d\n",__func__,__LINE__);
	int al_conf_handle, dot11e_conf_handle;
	unsigned short node_id;
	unsigned short node_len;

	unsigned char *buffer_ptr = buffer + get_imcp_signature_mesh_id_size();

      	al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"CONFIGD : INFO : IMCP_HIDE_SSID_REQUEST_RESPONSE Packet - Received: %s line: %d\n",__func__,__LINE__);

	memset(imcp_config_rr_data, 0, sizeof(imcp_config_rr_data_t));

	IMCP_MSG_PROCESS_RECEIVE_ID_AND_LEN(buffer_ptr,node_id,node_len);

		memcpy(imcp_config_rr_data->mac_address, buffer_ptr, 6);
	buffer_ptr += 6;

	imcp_config_rr_data->request_response_type = *buffer_ptr;
	buffer_ptr++;

	if (imcp_config_rr_data->request_response_type == TYPE_RESPONSE)
	{
		al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : IMCP_HIDE_SSID_REQUEST_RESPONSE Packet ignored: %s: %d\n",__func__,__LINE__);
		return 0;
	}

	if (memcmp(imcp_config_rr_data->mac_address, _MAC_ADDRESS, 6))
	{
		al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : IMCP_HIDE_SSID_REQUEST_RESPONSE Packet ignored: %s: %d\n",__func__,__LINE__);
		return 0;
	}

	imcp_config_rr_data->type = *buffer_ptr;
	buffer_ptr++;

	imcp_config_rr_data->response_id = *buffer_ptr;
	buffer_ptr++;

	al_conf_handle = libalconf_read_config_data();

	if (al_conf_handle != -1)
	{
		memcpy(imcp_hide_ssid_config_data->mac_address, imcp_config_rr_data->mac_address, 6);
		read_hide_ssid_config_data(al_conf_handle);
		al_conf_close(AL_CONTEXT al_conf_handle);
		send_hide_ssid_config_info_reply(client_addr, imcp_config_rr_data->response_id);
	}
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit: %s: %d\n",__func__,__LINE__);
	return 0;
}


/*
 *	Send ACL configuration info to client
 */
int send_acl_config_info_reply(struct sockaddr_in *client_addr, unsigned char response_id)
{
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter: %s: %d\n",__func__,__LINE__);
	int            socki;
	int            packet_size;
	unsigned char  *buffer;
	unsigned char  *buffer_ptr;
	unsigned short temp;
	unsigned short cpu2le16;
	unsigned short le162cpu;
	int            ret;
	int            i;
	int            vlan_name_length;
	unsigned short tag;
	unsigned char  allow;
	unsigned char  enabled;
	unsigned char  category;

	short ptag;
	unsigned short node_id;
	unsigned short node_len;

	packet_size  = get_imcp_signature_mesh_id_size();
	packet_size += 6 + 1 + 1 + 1;                  /* MAC address, REQ-RES, RESID and entry Count */
	packet_size += IMCP_ID_LEN_SIZE;

	buffer = (unsigned char *)malloc(1500);

	buffer_ptr = buffer;

	buffer_ptr += copy_imcp_signature_packet_type_meshid(buffer_ptr, IMCP_ACL_CONFIGURATION_INFO);

	node_id		= IMCP_ACL_CONFIG_INFO_REQ_RES_INFO_ID;
	node_len	= packet_size - get_imcp_signature_mesh_id_size() - IMCP_ID_LEN_SIZE;
	IMCP_MSG_PROCESS_SEND_ID_AND_LEN(buffer_ptr,node_id,node_len);

		memcpy(buffer_ptr, imcp_acl_config_data->mac_address, 6);
	buffer_ptr += 6;

	*buffer_ptr = imcp_acl_config_data->request_response;
	buffer_ptr++;

	*buffer_ptr = response_id;
	buffer_ptr++;

	*buffer_ptr = imcp_acl_config_data->count;
	buffer_ptr++;

	for (i = 0; i < imcp_acl_config_data->count; i++)
	{
		temp = 0;
		ptag = 0;

		memcpy(buffer_ptr, imcp_acl_config_data->entries[i].mac_address, 6);
		buffer_ptr += 6;

		ptag = imcp_acl_config_data->entries[i].vlan_tag;

		temp |= (ptag & 0x0FFF);

		temp |= (imcp_acl_config_data->entries[i].allow << 12);
		temp |= (imcp_acl_config_data->entries[i].dot11e_enabled << 13);
		temp |= (imcp_acl_config_data->entries[i].dot11e_category << 14);


		cpu2le16 = al_impl_cpu_to_le16(temp);
		memcpy(buffer_ptr, &cpu2le16, 2);
		buffer_ptr += 2;

		packet_size += 8;
	}
	ret = send_packet(buffer, packet_size, client_addr);
      	al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"CONFIGD : INFO : IMCP_ACL_CONFIG Packet sent (Bytes = %d) : %s : %d\n",ret,__func__,__LINE__);
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit: %s: %d\n",__func__,__LINE__);

	return 0;
}


int imcp_acl_request_response_helper(unsigned char *buffer, struct sockaddr_in *client_addr)
{
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter: %s: %d\n",__func__,__LINE__);
	int acl_conf_handle;
	unsigned short node_id;
	unsigned short node_len;

	unsigned char *buffer_ptr = buffer + get_imcp_signature_mesh_id_size();

      	al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"CONFIGD : INFO : IMCP_ACL_REQUEST_RESPONSE Packet - Received: %s line: %d\n",__func__,__LINE__);

	memset(imcp_config_rr_data, 0, sizeof(imcp_config_rr_data_t));

	IMCP_MSG_PROCESS_RECEIVE_ID_AND_LEN(buffer_ptr,node_id,node_len);

		memcpy(imcp_config_rr_data->mac_address, buffer_ptr, 6);
	buffer_ptr += 6;

	imcp_config_rr_data->request_response_type = *buffer_ptr;
	buffer_ptr++;

	if (imcp_config_rr_data->request_response_type == TYPE_RESPONSE)
	{
		al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : IMCP_HIDE_SSID_REQUEST_RESPONSE Packet ignored: %s : %d\n",__func__,__LINE__);
		return 0;
	}

	if (memcmp(imcp_config_rr_data->mac_address, _MAC_ADDRESS, 6))
	{
		al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : IMCP_HIDE_SSID_REQUEST_RESPONSE Packet ignored: %s : %d\n",__func__,__LINE__);
		return 0;
	}

	imcp_config_rr_data->type = *buffer_ptr;
	buffer_ptr++;

	imcp_config_rr_data->response_id = *buffer_ptr;
	buffer_ptr++;

	acl_conf_handle = read_acl_config_data();

	if (acl_conf_handle != -1)
	{
		IMCP_MSG_PROCESS_RECEIVE_ID_AND_LEN(buffer_ptr,node_id,node_len);
			memcpy(imcp_acl_config_data->mac_address, imcp_config_rr_data->mac_address, 6);
		imcp_acl_config_data->request_response = TYPE_RESPONSE;
		update_acl_data(acl_conf_handle);
		acl_conf_close(AL_CONTEXT acl_conf_handle);
		send_acl_config_info_reply(client_addr, imcp_config_rr_data->response_id);
		if (imcp_acl_config_data->count > 0)
		{
			free(imcp_acl_config_data->entries);
		}
	}
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit: %s: %d\n",__func__,__LINE__);
	return 0;
}


int imcp_dot11e_if_config_request_response_helper(unsigned char *buffer, struct sockaddr_in *client_addr)
{
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter: %s: %d\n",__func__,__LINE__);
	int           al_conf_handle;
	int           size;
	unsigned char *p;
	int           i;
	unsigned short node_id;
	unsigned short node_len;

#define MAC_ADDRESS_LENGTH    6

      	al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"CONFIGD : INFO : IMCP_DOT11E_IF_CONFIG_REQUEST_RESPONSE Packet - Received: %s line: %d\n",__func__,__LINE__);

	memset(imcp_dot11e_if_config_data, 0, sizeof(imcp_dot11e_if_config_t));

	size = get_imcp_signature_mesh_id_size();
	p    = buffer + size;                        /* Skip the IMCP Header */

	IMCP_MSG_PROCESS_RECEIVE_ID_AND_LEN(p,node_id,node_len);

	memcpy(imcp_dot11e_if_config_data->mac_address, p, MAC_ADDRESS_LENGTH);
	p += MAC_ADDRESS_LENGTH;

	imcp_dot11e_if_config_data->request_response = *p;
	p += 1;
	imcp_dot11e_if_config_data->response_id = *p;
	p += 1;

	p++;      /* Skipping TYPE */

	if (imcp_dot11e_if_config_data->request_response == TYPE_RESPONSE)
	{
		al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : IMCP_DOT11E_IF_CONFIG_REQUEST_RESPONSE Packet ignored: %s: %d\n",__func__,__LINE__);
		return 0;
	}

	if (memcmp(imcp_dot11e_if_config_data->mac_address, _MAC_ADDRESS, 6))
	{
		al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : IMCP_DOT11E_IF_CONFIG_REQUEST_RESPONSE Packet ignored: %s: %d\n",__func__,__LINE__);
		return 0;
	}

	al_conf_handle = libalconf_read_config_data();

	if (al_conf_handle != -1)
	{
		update_dot11e_if_config_data(al_conf_handle);
		al_conf_close(AL_CONTEXT al_conf_handle);
		send_dot11e_if_config_info_reply(client_addr, imcp_dot11e_if_config_data->response_id);
	}

	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit: %s: %d\n",__func__,__LINE__);
	return 0;

#undef MAC_ADDRESS_LENGTH
}


int imcp_dot11e_category_request_response_helper(unsigned char *buffer, struct sockaddr_in *client_addr)
{
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter: %s: %d\n",__func__,__LINE__);
	int           dot11e_conf_handle;
	int           size;
	unsigned char *p;
	unsigned short node_id;
	unsigned short node_len;

#define MAC_ADDRESS_LENGTH    6

      	al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"CONFIGD : INFO : IMCP_DOT11E_CATEGORY_REQUEST_RESPONSE Packet - Received: %s line: %d\n",__func__,__LINE__);

	size = get_imcp_signature_mesh_id_size();

	p = buffer + size;                           /* Skip the IMCP Header */

	memset(imcp_dot11e_category_data, 0, sizeof(imcp_dot11e_category_t));

	IMCP_MSG_PROCESS_RECEIVE_ID_AND_LEN(p,node_id,node_len);

		memcpy(imcp_dot11e_category_data->mac_address, p, MAC_ADDRESS_LENGTH);
	p += MAC_ADDRESS_LENGTH;

	imcp_dot11e_category_data->request_response = *p;
	p++;
	imcp_dot11e_category_data->response_id = *p;
	p++;

	p++;      /* Skipping TYPE */

	if (imcp_dot11e_category_data->request_response == TYPE_RESPONSE)
	{
		al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : IMCP_DOT11E_CATEGORY_REQUEST_RESPONSE Packet ignored: %s: %d\n",__func__,__LINE__);
		return 0;
	}

	if (memcmp(imcp_dot11e_category_data->mac_address, _MAC_ADDRESS, 6))
	{
		al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : IMCP_DOT11E_CATEGORY_REQUEST_RESPONSE Packet ignored: %s: %d\n",__func__,__LINE__);
		return 0;
	}

	dot11e_conf_handle = read_dot11e_category_data();

	if (dot11e_conf_handle != -1)
	{
		update_dot11e_category_data(dot11e_conf_handle);
		dot11e_conf_close(AL_CONTEXT dot11e_conf_handle);
		send_dot11e_category_info_reply(client_addr, imcp_dot11e_category_data->response_id);
	}

#undef MAC_ADDRESS_LENGTH
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit: %s: %d\n",__func__,__LINE__);

	return 0;
}


int send_private_channel_config_info_reply(struct sockaddr_in *client_addr, unsigned char response_id)
{
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter: %s: %d\n",__func__,__LINE__);
	int            sock, clilen, i, j;
	int            size, if_size;
	int            packet_size;
	unsigned char  buffer[1024];
	unsigned char  *buffer_ptr;
	unsigned short cpu2le16;
	int            ret;
	int            if_name_length;
	int            priv_channel_count;
	unsigned short node_id;
	unsigned short node_len;

#define MAC_ADDRESS_LENGTH    6

	/* MAC address, REQ-RES, RESID,Interface name length and Interface name */
	if_name_length = strlen(imcp_priv_channel_data->if_info.name);
	packet_size    = MAC_ADDRESS_LENGTH + 1 + 1 + 1 + if_name_length;

	priv_channel_count = imcp_priv_channel_data->if_info.priv_channel_count;

	/*priv_channel_bw,priv_channel_ant_max,priv_channel_ctl,priv_channel_max_power,priv_channel_count */
	packet_size = packet_size + 5;


	packet_size = packet_size + priv_channel_count +
		(priv_channel_count * (sizeof(unsigned char))) +  /* priv_channel_numbers */
		(priv_channel_count * (sizeof(unsigned short)) +  /* priv_channel_frequencies */
		 (priv_channel_count * (sizeof(unsigned char)))); /* priv_channel_flags */

	packet_size = packet_size + get_imcp_signature_mesh_id_size();

	packet_size += IMCP_ID_LEN_SIZE; 

	buffer_ptr = buffer;

	buffer_ptr += copy_imcp_signature_packet_type_meshid(buffer_ptr, IMCP_IF_PRIVATE_CHANNEL_INFO);

	node_id      = IMCP_PRIVATE_CHANNEL_INFO_REQ_RES_INFO_ID;
	node_len     = packet_size - get_imcp_signature_mesh_id_size() - IMCP_ID_LEN_SIZE;
	IMCP_MSG_PROCESS_SEND_ID_AND_LEN(buffer_ptr,node_id,node_len);

		memcpy(buffer_ptr, imcp_priv_channel_data->mac_address, MAC_ADDRESS_LENGTH);
	buffer_ptr += MAC_ADDRESS_LENGTH;

	*buffer_ptr = imcp_priv_channel_data->request_response;
	buffer_ptr++;

	*buffer_ptr = response_id;
	buffer_ptr++;

	*buffer_ptr = if_name_length;
	buffer_ptr++;
	al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"CONFIGD : INFO :IMCP_PRIVATE_CHANNEL_CONFIG (if_name_length = %d) : %s: %d\n",if_name_length,__func__,__LINE__);

	if (if_name_length > 0)
	{
		memcpy(buffer_ptr, imcp_priv_channel_data->if_info.name, if_name_length);
		buffer_ptr += if_name_length;
	}

	al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"CONFIGD : INFO :IMCP_PRIVATE_CHANNEL_CONFIG (if_name = %s) : %s: %d\n", imcp_priv_channel_data->if_info.name,__func__,__LINE__);


	*buffer_ptr = imcp_priv_channel_data->if_info.priv_channel_bw;
	buffer_ptr++;
	*buffer_ptr = imcp_priv_channel_data->if_info.priv_channel_ant_max;
	buffer_ptr++;
	*buffer_ptr = imcp_priv_channel_data->if_info.priv_channel_ctl;
	buffer_ptr++;
	*buffer_ptr = imcp_priv_channel_data->if_info.priv_channel_max_power;
	buffer_ptr++;
	*buffer_ptr = priv_channel_count;
	buffer_ptr++;


	for (i = 0; i < priv_channel_count; i++)
	{
		*buffer_ptr = imcp_priv_channel_data->if_info.priv_channel_numbers[i];
		buffer_ptr++;

		cpu2le16 = al_impl_cpu_to_le16(imcp_priv_channel_data->if_info.priv_frequencies[i]);
		memcpy(buffer_ptr, &cpu2le16, sizeof(unsigned short));
		buffer_ptr += sizeof(unsigned short);

		*buffer_ptr = imcp_priv_channel_data->if_info.priv_channel_flags[i];
		buffer_ptr++;
	}

	ret = send_packet(buffer, packet_size, client_addr);
      	al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"CONFIGD : INFO : IMCP_PRIVATE_CHANNEL_CONFIG Packet sent (Bytes = %d) : %s : %d\n",ret,__func__,__LINE__);

#undef MAC_ADDRESS_LENGTH
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit: %s: %d\n",__func__,__LINE__);

	return 0;
}


int imcp_if_priv_channel_request_response_helper(unsigned char *buffer, struct sockaddr_in *client_addr)
{
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter: %s: %d\n",__func__,__LINE__);
	int           al_conf_handle;
	int           size;
	int           if_name_length;
	unsigned char *p;
	unsigned short node_id;
	unsigned short node_len;


#define MAC_ADDRESS_LENGTH    6

      	al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"CONFIGD : INFO : IMCP_PRIV_CHANNEL_REQUEST_RESPONSE Packet - Received: %s line: %d\n",__func__,__LINE__);

	size = get_imcp_signature_mesh_id_size();

	p = buffer + size;                           /* Skip the IMCP Header */

	memset(imcp_priv_channel_data, 0, sizeof(imcp_priv_channel_config_t));

	IMCP_MSG_PROCESS_RECEIVE_ID_AND_LEN(p,node_id,node_len);

		memcpy(imcp_priv_channel_data->mac_address, p, MAC_ADDRESS_LENGTH);
	p += MAC_ADDRESS_LENGTH;

	imcp_priv_channel_data->request_response = *p;
	p++;
	imcp_priv_channel_data->response_id = *p;
	p++;
	if_name_length = *p;
	p++;

	memset(&imcp_priv_channel_data->if_info, 0, sizeof(al_conf_if_info_t));
	memcpy(imcp_priv_channel_data->if_info.name, p, if_name_length);

	if (imcp_priv_channel_data->request_response == TYPE_RESPONSE)
	{
		al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : IMCP_PRIV_CHANNEL_REQUEST_RESPONSE Packet ignored: %s: %d\n",__func__,__LINE__);
		return 0;
	}

	if (memcmp(imcp_priv_channel_data->mac_address, _MAC_ADDRESS, MAC_ADDRESS_LENGTH))
	{
		al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : IMCP_PRIV_CHANNEL_REQUEST_RESPONSE Packet ignored: %s: %d\n",__func__,__LINE__);
		return 0;
	}

	al_conf_handle = libalconf_read_config_data();

	if (al_conf_handle != -1)
	{
		imcp_priv_channel_data->request_response = TYPE_RESPONSE;
		update_private_channel_config_data(al_conf_handle);
		al_conf_close(AL_CONTEXT al_conf_handle);
		send_private_channel_config_info_reply(client_addr, imcp_priv_channel_data->response_id);
	}

#undef MAC_ADDRESS_LENGTH

	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit: %s: %d\n",__func__,__LINE__);
	return 0;
}


int imcp_config_request_response_helper(unsigned char *buffer, struct sockaddr_in *client_addr)
{
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter: %s: %d\n",__func__,__LINE__);
	int           al_conf_handle;
	unsigned char *p;
	unsigned short node_id;
	unsigned short node_len;

	p = buffer + get_imcp_signature_mesh_id_size();                    /* IMCP header */

	IMCP_MSG_PROCESS_RECEIVE_ID_AND_LEN(p,node_id,node_len)

		if (memcmp(p, _MAC_ADDRESS, 6))
		{
			al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : IMCP_CONFIG_REQUEST_RESPONSE Packet ignored: %s  : %d\n",__func__,__LINE__);
			return -1;
		}
	p += 6;                    /* ap mac addr */

	if (*p == TYPE_RESPONSE)
	{
		al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : IMCP_CONFIG_REQUEST_RESPONSE Packet ignored: %s  : %d\n",__func__,__LINE__);
		return -1;
	}

	p += 1;

	if ((*p & TYPE_AP_CONFIG_REQUEST) == TYPE_AP_CONFIG_REQUEST)
	{
		imcp_apconfig_request_response_helper(buffer, client_addr);
	}

	if ((*p & TYPE_AP_IP_CONFIG_REQUEST) == TYPE_AP_IP_CONFIG_REQUEST)
	{
		imcp_ipconfig_request_response_helper(buffer, client_addr);
	}

	if ((*p & TYPE_AP_VLAN_CONFIG_REQUEST) == TYPE_AP_VLAN_CONFIG_REQUEST)
	{
		imcp_vlan_request_response_helper(buffer, client_addr);
	}

	if ((*p & TYPE_IF_HIDDEN_SSID_REQUEST) == TYPE_IF_HIDDEN_SSID_REQUEST)
	{
		imcp_hide_ssid_request_response_helper(buffer, client_addr);
	}

	if ((*p & TYPE_AP_DOT11E_CATEGORY_REQUEST) == TYPE_AP_DOT11E_CATEGORY_REQUEST)
	{
		imcp_dot11e_category_request_response_helper(buffer, client_addr);
	}

	if ((*p & TYPE_AP_DOT11E_CONFIG_REQUEST) == TYPE_AP_DOT11E_CONFIG_REQUEST)
	{
		imcp_dot11e_if_config_request_response_helper(buffer, client_addr);
	}

	if ((*p & TYPE_ACL_CONFIG_REQUEST) == TYPE_ACL_CONFIG_REQUEST)
	{
		imcp_acl_request_response_helper(buffer, client_addr);
	}

	if ((*p & TYPE_EXTEND_TO_BYTE_2) == TYPE_EXTEND_TO_BYTE_2)
	{
		p += 2;                           //skip current type & res_id
	}
	else
	{
		al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : Invalid request: %s: %d\n",__func__,__LINE__);
		return 0;
	}

	if ((*p & TYPE_SIP_CONFIG_REQUEST) == TYPE_SIP_CONFIG_REQUEST)
	{
		imcp_sip_config_request_response_helper(buffer, client_addr);
	}

	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit: %s: %d\n",__func__,__LINE__);
	return 0;
}


int imcp_dot11e_category_update_info_helper(unsigned char *buffer, struct sockaddr_in *client_addr)
{
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter: %s: %d\n",__func__,__LINE__);
	unsigned char  *p;
	int            i;
	unsigned char  category_name_length;
	char           category_name[256];
	unsigned short le2cpu16;
	unsigned int   le2cpu32;
	int            dot11e_conf_handle;
	int            al_conf_handle;
	unsigned char  status = 0, ret = 0, err_code = 0;	

	dot11e_conf_category_t *dot11e_data;
	unsigned short          node_id;
	unsigned short          node_len;
	config_validation_report_t  validation_report;

      	al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"CONFIGD : INFO : IMCP_DOT11E_CATEGORY_CONFIGURATION_INFO Packet - Received: %s line: %d\n",__func__,__LINE__);

	p = buffer + get_imcp_signature_mesh_id_size();

	IMCP_MSG_PROCESS_RECEIVE_ID_AND_LEN(p,node_id,node_len);

	memcpy(imcp_dot11e_category_data->mac_address, p, 6);
	p += 6;

	imcp_dot11e_category_data->request_response = *p;
	p += 1;
	imcp_dot11e_category_data->response_id = *p;
	p += 1;

	if (imcp_dot11e_category_data->request_response == TYPE_RESPONSE)
	{
		al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : IMCP_DOT11E_CATEGORY_CONFIGURATION_INFO Packet ignored: %s: %d\n",__func__,__LINE__);
		return 0;
	}

	if (memcmp(imcp_dot11e_category_data->mac_address, _MAC_ADDRESS, 6))
	{
		al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : IMCP_DOT11E_CATEGORY_CONFIGURATION_INFO Packet ignored: %s: %d\n",__func__,__LINE__);
		return 0;
	}

	imcp_dot11e_category_data->count = *p;
	p += 1;

	for (i = 0; i < imcp_dot11e_category_data->count; i++)
	{
		memset(&imcp_dot11e_category_data->category_info[i], 0, sizeof(imcp_dot11e_category_details_t));

		category_name_length = *p;
		p += 1;

		memset(category_name, 0, 256);
		memcpy(category_name, p, category_name_length);
		p += category_name_length;

		if (strcmp(category_name, "ac_bk") == 0)
		{
			imcp_dot11e_category_data->category_info[i].category = DOT11E_CATEGORY_TYPE_AC_BK;
		}
		else if (strcmp(category_name, "ac_be") == 0)
		{
			imcp_dot11e_category_data->category_info[i].category = DOT11E_CATEGORY_TYPE_AC_BE;
		}
		else if (strcmp(category_name, "ac_vi") == 0)
		{
			imcp_dot11e_category_data->category_info[i].category = DOT11E_CATEGORY_TYPE_AC_VI;
		}
		else if (strcmp(category_name, "ac_vo") == 0)
		{
			imcp_dot11e_category_data->category_info[i].category = DOT11E_CATEGORY_TYPE_AC_VO;
		}
		else
		{
			al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : Invalid category: %s: %d\n",__func__,__LINE__);
			return 0;
		}

		memcpy(&le2cpu32, p, sizeof(unsigned int));
		p += sizeof(unsigned int);
		imcp_dot11e_category_data->category_info[i].burst_time = al_impl_le32_to_cpu(le2cpu32);

		memcpy(&le2cpu16, p, sizeof(unsigned short));
		p += sizeof(unsigned short);
		imcp_dot11e_category_data->category_info[i].acwmin = al_impl_le16_to_cpu(le2cpu16);

		memcpy(&le2cpu16, p, sizeof(unsigned short));
		p += sizeof(unsigned short);
		imcp_dot11e_category_data->category_info[i].acwmax = al_impl_le16_to_cpu(le2cpu16);

		imcp_dot11e_category_data->category_info[i].aifsn = *p;
		p += 1;

		imcp_dot11e_category_data->category_info[i].disable_backoff = *p;
		p += 1;
	}

	for (i = 0; i < imcp_dot11e_category_data->count; i++)
	{
		err_code = validate_dot11e_category_data(&imcp_dot11e_category_data->category_info[i], &validation_report);
		if(err_code)
		{
			goto validation_failed;
		}
	}

	dot11e_conf_handle = read_dot11e_category_data();

	if (dot11e_conf_handle > 0)
	{
		EXECUTE_PRE_FS_CHANGE

			/*
			 *	Increment config sqnr
			 */
			al_conf_handle = libalconf_read_config_data();
		if (al_conf_handle != -1)
		{
			set_mesh_config_sqnr(increment_config_sqnr(al_conf_handle));
			al_conf_put(AL_CONTEXT al_conf_handle);
			al_conf_close(AL_CONTEXT al_conf_handle);
		}
		write_dot11e_category_data(dot11e_conf_handle, &validation_report);
		EXECUTE_POST_FS_CHANGE(POST_FS_PARAM_TYPE_CONFIG)
			dot11e_conf_close(AL_CONTEXT dot11e_conf_handle);
	}

	dot11e_conf_handle = 0;
	dot11e_conf_handle = read_dot11e_category_data();


	dot11e_data    = (dot11e_conf_category_t *)dot11e_conf_handle;
	al_conf_handle = libalconf_read_config_data();

	//EXECUTE_MESHD_CONFIGURE
	init_configurations(al_conf_handle, dot11e_conf_handle, 1, 1);

	dot11e_conf_close(AL_CONTEXT dot11e_conf_handle);
	al_conf_close(AL_CONTEXT al_conf_handle);

	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit: %s: %d\n",__func__,__LINE__);
validation_failed:
	send_config_updated_response(client_addr, imcp_apconfig_data->response_id, TYPE_AP_DOT11E_CATEGORY_REQUEST, err_code, &validation_report);
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit: %s: %d\n",__func__,__LINE__);
	return 0;
}


int imcp_dot11e_if_config_update_info_helper(unsigned char *buffer, struct sockaddr_in *client_addr)
{
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter: %s: %d\n",__func__,__LINE__);
	unsigned char *p;
	int           i;
	int           j;
	int           length;
	int           al_conf_handle;
	unsigned short          node_id;
	unsigned short          node_len;
	unsigned char			status = 0;

      	al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"CONFIGD : INFO : IMCP_DOT11E_IF_CONFIGURATION_INFO Packet - Received: %s line: %d\n",__func__,__LINE__);

	p = buffer + get_imcp_signature_mesh_id_size();

	IMCP_MSG_PROCESS_RECEIVE_ID_AND_LEN(p,node_id,node_len);

	memcpy(imcp_dot11e_if_config_data->mac_address, p, 6);
	p += 6;
	imcp_dot11e_if_config_data->request_response = *p;
	p += 1;
	imcp_dot11e_if_config_data->response_id = *p;
	p += 1;

	if (imcp_dot11e_if_config_data->request_response == TYPE_RESPONSE)
	{
		al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : IMCP_DOT11E_IF_CONFIGURATION_INFO Packet ignored: %s: %d\n",__func__,__LINE__);
		return 0;
	}

	if (memcmp(imcp_dot11e_if_config_data->mac_address, _MAC_ADDRESS, 6))
	{
		al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : IMCP_DOT11E_IF_CONFIGURATION_INFO Packet ignored: %s: %d\n",__func__,__LINE__);
		return 0;
	}

	imcp_dot11e_if_config_data->if_count = *p;
	p += 1;


	for (i = 0; i < imcp_dot11e_if_config_data->if_count; i++)
	{
		memset(&imcp_dot11e_if_config_data->if_info[i], 0, sizeof(imcp_dot11e_if_config_details_t));

		imcp_dot11e_if_config_data->if_info[i].if_index = i;

		length = *p;
		p     += 1;
		memset(imcp_dot11e_if_config_data->if_info[i].if_name, 0, 256);
		memcpy(imcp_dot11e_if_config_data->if_info[i].if_name, p, length);
		p += length;

		imcp_dot11e_if_config_data->if_info[i].enabled = *p;
		p += 1;

		imcp_dot11e_if_config_data->if_info[i].category_index = *p;
		p += 1;

		length = *p;
		p     += 1;

		memset(imcp_dot11e_if_config_data->if_info[i].if_essid, 0, 256);
		memcpy(imcp_dot11e_if_config_data->if_info[i].if_essid, p, length);
		p += length;


		/*
		 *  No Runtime variables are maintained.So,copying to APConfig structure is not used anywhere.
		 */

		/*
		 * for(j = 0; j < imcp_apconfig_data->interface_count; j++) {
		 *
		 *      if(strcmp(imcp_dot11e_if_config_data->if_info[i].if_name,imcp_apconfig_data->if_info[j].name) == 0) {
		 *
		 *              imcp_apconfig_data->if_info[j].dot11e_enabled	= imcp_dot11e_if_config_data->if_info[i].enabled;
		 *              imcp_apconfig_data->if_info[j].dot11e_category	= imcp_dot11e_if_config_data->if_info[i].category_index;
		 *
		 *      }
		 * }*/
	}

	al_conf_handle = libalconf_read_config_data();

	if (al_conf_handle > 0)
	{
		EXECUTE_PRE_FS_CHANGE
			set_mesh_config_sqnr(increment_config_sqnr(al_conf_handle));

		write_dot11e_if_config_data(al_conf_handle);
		EXECUTE_POST_FS_CHANGE(POST_FS_PARAM_TYPE_CONFIG)
			al_conf_close(AL_CONTEXT al_conf_handle);
	}

      	al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"CONFIGD : INFO : ACKOWLEDGEMENT FOR DOOT11E IF CONFIG PKT Packet - Sent : %s : %d\n",__func__,__LINE__);

	send_config_updated_response(client_addr, imcp_apconfig_data->response_id, TYPE_AP_DOT11E_CONFIG_REQUEST, SUCCESS, NULL);
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit: %s: %d\n",__func__,__LINE__);
	return 0;
}


int imcp_vlan_request_response_helper(unsigned char *buffer, struct sockaddr_in *client_addr)
{
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter: %s: %d\n",__func__,__LINE__);
	int           al_conf_handle;
	int           size;
	unsigned char request_id;
	unsigned char req_res_type;
	unsigned char *buffer_ptr;

	unsigned short node_id;
	unsigned short node_len;

      	al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"CONFIGD : INFO : IMCP_VLAN_CONFIG_REQUEST_RESPONSE Packet - Received: %s line: %d\n",__func__,__LINE__);

	size = get_imcp_signature_mesh_id_size();

	buffer_ptr = buffer + size;

	memset(imcp_vlan_config_info_data, 0, sizeof(imcp_vlan_config_info_data_t));

	IMCP_MSG_PROCESS_RECEIVE_ID_AND_LEN(buffer_ptr,node_id,node_len);

	memcpy(imcp_vlan_config_info_data->mac_address, buffer_ptr, 6);
	buffer_ptr += 6;

	req_res_type = *buffer_ptr;
	buffer_ptr++;

	request_id = *buffer_ptr;
	buffer_ptr++;

	buffer_ptr++;         /// type was added afterwards

	if (req_res_type == TYPE_RESPONSE)
	{
		al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : IMCP_VLAN_CONFIG_REQUEST_RESPONSE Packet ignored: %s: %d\n",__func__,__LINE__);
		return 0;
	}

	if (memcmp(imcp_vlan_config_info_data->mac_address, _MAC_ADDRESS, 6))
	{
		al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : IMCP_VLAN_CONFIG_REQUEST_RESPONSE Packet ignored: %s: %d\n",__func__,__LINE__);
		return 0;
	}

	al_conf_handle = libalconf_read_config_data();

	if (al_conf_handle != -1)
	{

		update_vlan_config_data(al_conf_handle);
		al_conf_close(AL_CONTEXT al_conf_handle);
		send_vlan_config_info_reply(client_addr, request_id);

		/**
		 *	either free vlan_info list or use static array instead of pointer to vlan_info
		 */
		if (imcp_vlan_config_info_data->imcp_vlan_info != NULL)
		{
			free(imcp_vlan_config_info_data->imcp_vlan_info);
			imcp_vlan_config_info_data->imcp_vlan_info = NULL;
		}
	}

	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit: %s: %d\n",__func__,__LINE__);
	return 0;
}

int validate_80mhz_allowed_channel(al_conf_if_info_t *if_info, int channel, config_validation_report_t  *validation_report)
{
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter: %s: %d\n",__func__,__LINE__);
	if(channel == 165)
	{
		al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : Invalid channel configuration: %s: %d\n",__func__,__LINE__);
		prepare_error_report(if_info, IF_80MHZ_CH, INTEGER, &channel, validation_report);
		return INVALID;
	}
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit: %s: %d\n",__func__,__LINE__);
	return SUCCESS;
}

int validate_freq_and_seg0_freq(int curr_channel, al_conf_if_info_t *if_info, config_validation_report_t  *validation_report)
{
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter: %s: %d\n",__func__,__LINE__);
	int _40plus_ch[] = {1, 2, 3, 4, 5, 6, 7, 36, 44, 52, 60, 149, 157};
	int _40minus_ch[] = {5, 6, 7, 8, 9, 10, 11, 12, 13, 40, 48, 56, 64, 153,161};
	int i,ret;

	ret = validate_80mhz_allowed_channel(if_info, curr_channel, validation_report);
	if(ret != SUCCESS) {
		return ret;
	}

	for(i = 0; i < (sizeof(_40plus_ch) / sizeof(int)); i++)
	{
		if(curr_channel == _40plus_ch[i]) {
			if_info->ht_capab.ht_bandwidth = AL_CONF_IF_HT_PARAM_40_ABOVE;
			goto _setting_center_freq;
		}
	}
	for(i = 0; i < (sizeof(_40minus_ch) / sizeof(int)); i++)
	{
		if(curr_channel == _40minus_ch[i]) {
			if_info->ht_capab.ht_bandwidth = AL_CONF_IF_HT_PARAM_40_BELOW;
			goto _setting_center_freq;
		}
	}

_setting_center_freq :

	if(curr_channel >= 36 && curr_channel <= 48) {
		if_info->vht_capab.seg0_center_freq = 42;
	}
	if((curr_channel >= 52) && (curr_channel <= 64)) {
		if_info->vht_capab.seg0_center_freq = 58;
	}
	if((curr_channel >= 100) && (curr_channel <= 116)) {
		if_info->vht_capab.seg0_center_freq = 106;
	}
	if((curr_channel >= 120) && (curr_channel <= 128)) {
		if_info->vht_capab.seg0_center_freq = 122;
	}
	if((curr_channel >= 132) && (curr_channel <= 144)) {
		if_info->vht_capab.seg0_center_freq = 138;
	}
	if((curr_channel >= 149) && (curr_channel <= 161)) {
		if_info->vht_capab.seg0_center_freq = 155;
	}

	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit: %s: %d\n",__func__,__LINE__);
	return SUCCESS;

}


int imcp_apconfig_interface_info_helper(unsigned char *buffer, int interface_count, config_validation_report_t *validation_report)
{
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter: %s: %d\n",__func__,__LINE__);
	unsigned char     *buffer_ptr = buffer;
	int               dot11e_conf_handle;
	int               i, if_no, bytes_read, length, essid_length;
	al_conf_if_info_t *if_info;
	unsigned char     key_length;
	unsigned char	  err_code = 0;
	unsigned int      le2cpu;
	unsigned int    le2cpu32;
	unsigned int      len;
	int               ret;
	unsigned short    node_id;
	unsigned short    node_len;
	int curr_channel = 0;


	essid_length = 0;

	if (interface_count <= 0)
	{
		al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : Invalid interface count: %s: %d\n",__func__,__LINE__);
		return NODE_INTERFACE_CONFIG_FAIL;
	}

	memset(imcp_apconfig_data->if_info, 0, sizeof(al_conf_if_info_t) * CURRENT_EXPECTED_MAX_IF_COUNT);

	for (if_no = 0; if_no < interface_count; if_no++)
	{
		memcpy(&node_id, buffer_ptr, sizeof(unsigned short));
		node_id = al_impl_le16_to_cpu(node_id);
		buffer_ptr += sizeof(unsigned short);
		memcpy(&node_len, buffer_ptr, sizeof(unsigned short));
		node_len = al_impl_le16_to_cpu(node_len);
		if(node_len & IMCP_EXT_MASK)
		{
			al_print_log(AL_CONTEXT AL_LOG_TYPE_DEBUG,"Extension bit is setted func : %s LINE : %d\n",__func__,__LINE__);
		}
		buffer_ptr += sizeof(unsigned short);


		if_info = &imcp_apconfig_data->if_info[if_no];

		switch(node_id) {
			case IMCP_IF_802_3_INFO_ID :

				length = *buffer_ptr;
				buffer_ptr++;

				if (length > 0)
				{
					memcpy(if_info->name, buffer_ptr, length);
					if_info->name[length] = 0;
					buffer_ptr           += length;
				}

				if_info->phy_type = *buffer_ptr;
				buffer_ptr++;

				/* TODO remve this field */

				if_info->phy_sub_type = *buffer_ptr;
				buffer_ptr++;


				if_info->use_type = *buffer_ptr;
				buffer_ptr++;

				memcpy(&le2cpu, buffer_ptr, sizeof(unsigned int));
				buffer_ptr     += sizeof(unsigned int);
				if_info->txrate = al_impl_le32_to_cpu(le2cpu);

				if_info->service = *buffer_ptr;
				buffer_ptr++;

				break;
			case IMCP_IF_802_11_INFO_ID :
				length = *buffer_ptr;
				buffer_ptr++;

				if (length > 0)
				{
					memcpy(if_info->name, buffer_ptr, length);
					if_info->name[length] = 0;
					buffer_ptr           += length;
				}

				if_info->phy_type = *buffer_ptr;
				buffer_ptr++;

				/* TODO remve this field */

				if_info->phy_sub_type = *buffer_ptr;
				buffer_ptr++;

				if_info->use_type = *buffer_ptr;
				buffer_ptr++;

				if_info->wm_channel = *buffer_ptr;
				buffer_ptr++;

				if_info->bonding = *buffer_ptr;
				buffer_ptr++;

				if_info->dca = *buffer_ptr;
				buffer_ptr++;

				if_info->txpower = *buffer_ptr;
				buffer_ptr++;

				memcpy(&le2cpu, buffer_ptr, sizeof(unsigned int));
				buffer_ptr     += sizeof(unsigned int);
				if_info->txrate = al_impl_le32_to_cpu(le2cpu);

				if_info->service = *buffer_ptr;
				buffer_ptr++;

				essid_length = *buffer_ptr;
				buffer_ptr++;

				if (essid_length > 0)
				{
					if(strcmp(if_info->name, "wlan3")) {
						err_code = validate_interface_essid(if_info, essid_length, validation_report);
						if(err_code){
							al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : returning errorcode:%dfunc : %s: %d\n",err_code,__func__,__LINE__);
							return err_code;
						}
					}

					memcpy(if_info->essid, buffer_ptr, essid_length);
					if_info->essid[essid_length] = 0;
					buffer_ptr += essid_length;
				}

				memcpy(&le2cpu, buffer_ptr, sizeof(unsigned int));
				buffer_ptr     += sizeof(unsigned int);
				if_info->rts_th = al_impl_le32_to_cpu(le2cpu);

				memcpy(&le2cpu, buffer_ptr, sizeof(unsigned int));
				buffer_ptr      += sizeof(unsigned int);
				if_info->frag_th = al_impl_le32_to_cpu(le2cpu);

				memcpy(&le2cpu, buffer_ptr, sizeof(unsigned int));
				buffer_ptr += sizeof(unsigned int);
				if_info->beacon_interval = al_impl_le32_to_cpu(le2cpu);

				if_info->dca_list_count = *buffer_ptr;
				buffer_ptr++;

				if (if_info->dca_list_count > 0)
				{
					memset(if_info->dca_list, 0, if_info->dca_list_count * sizeof(int));
					for (i = 0; i < if_info->dca_list_count; i++)
					{
						if_info->dca_list[i] = *buffer_ptr;
						buffer_ptr++;
					}
				}

				/* Read security info*/

				buffer_ptr++;            //Here the type is skipped

				if_info->security_info._security_none.enabled = *buffer_ptr;
				buffer_ptr++;

				buffer_ptr++;            //Here the type is skipped

				if_info->security_info._security_wep.enabled = *buffer_ptr;
				buffer_ptr++;

				if (if_info->security_info._security_wep.enabled)
                { 
                    if((if_info->phy_sub_type == AL_CONF_IF_PHY_SUB_TYPE_802_11_N_2_4GHZ) || (if_info->phy_sub_type == AL_CONF_IF_PHY_SUB_TYPE_802_11_N_5GHZ) || 
                    (if_info->phy_sub_type == AL_CONF_IF_PHY_SUB_TYPE_802_11_AC) ||(if_info->phy_sub_type == AL_CONF_IF_PHY_SUB_TYPE_802_11_BGN) || 
                    (if_info->phy_sub_type == AL_CONF_IF_PHY_SUB_TYPE_802_11_AN) ||(if_info->phy_sub_type == AL_CONF_IF_PHY_SUB_TYPE_802_11_ANAC)) {
                
                        prepare_error_report(if_info->name, WEP_ENABLED_ERROR_MSG, UNSIGNEDCHAR, &if_info->security_info._security_wep.enabled, validation_report);
                        if_info->security_info._security_wep.enabled = 0;
                        return UNSUPPORTED;
                    }
					if_info->security_info._security_wep.strength = *buffer_ptr;
					buffer_ptr++;

					if_info->security_info._security_wep.key_index = *buffer_ptr;
					buffer_ptr++;

					key_length = *buffer_ptr;
					buffer_ptr++;

					err_code = validate_WEP_security_info(if_info,key_length,validation_report);;
					if(err_code){
						VAL_PRINTF("Failure in interface WEP security settings \n");
						al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : Failure in interface WEP security settings err_code : %d: %s: %d\n",err_code,__func__,__LINE__);
						return err_code;
					}

					//TODO #define Strenth type as 40 and 104
					if (if_info->security_info._security_wep.strength == 40)
					{
						if_info->security_info._security_wep.key_count = 4;                    //suksh
						for (i = 0; i < AL_802_11_MAX_SECURITY_KEYS; i++)
						{
							memcpy(&if_info->security_info._security_wep.keys[i].bytes, buffer_ptr, 5);
							buffer_ptr += 5;
							if_info->security_info._security_wep.keys[i].len = 5;                         //suksh
						}
					}
					else
					{
						if_info->security_info._security_wep.key_count = 1;                    //suksh
						memcpy(&if_info->security_info._security_wep.keys[0].bytes, buffer_ptr, 13);
						buffer_ptr += 13;
						if_info->security_info._security_wep.keys[0].len = 13;                       //suksh
					}
				}

				buffer_ptr++;             //Here the type is skipped ..PSK security reading

				if_info->security_info._security_rsn_psk.enabled = *buffer_ptr;
				buffer_ptr++;

				if (if_info->security_info._security_rsn_psk.enabled)
				{
					if_info->security_info._security_rsn_psk.mode = *buffer_ptr;
					buffer_ptr++;

					len = *buffer_ptr;
					buffer_ptr++;
					memset(if_info->security_info._security_rsn_psk.key_buffer,
							0,
							512);
					ret = al_encode_key(AL_CONTEXT(char *) buffer_ptr,
							len,
							_MAC_ADDRESS,
							if_info->security_info._security_rsn_psk.key_buffer);
					if_info->security_info._security_rsn_psk.key_buffer_len = ret;
					buffer_ptr += len;

					if_info->security_info._security_rsn_psk.cipher_tkip = *buffer_ptr;
					buffer_ptr++;

					if_info->security_info._security_rsn_psk.cipher_ccmp = *buffer_ptr;
					buffer_ptr++;

					memcpy(&le2cpu32, buffer_ptr, sizeof(unsigned int));
					buffer_ptr += sizeof(unsigned int);
					if_info->security_info._security_rsn_psk.group_key_renewal = al_impl_le32_to_cpu(le2cpu32);
				}

				buffer_ptr++;            //The type is skipped here ..The Radius security reading

				if_info->security_info._security_rsn_radius.enabled = *buffer_ptr;
				buffer_ptr++;

				if (if_info->security_info._security_rsn_radius.enabled)
				{
					if_info->security_info._security_rsn_radius.mode = *buffer_ptr;
					buffer_ptr++;

					memcpy(if_info->security_info._security_rsn_radius.radius_server, buffer_ptr, 4);
					buffer_ptr += 4;

					memcpy(&le2cpu, buffer_ptr, sizeof(unsigned int));
					buffer_ptr += sizeof(unsigned int);
					if_info->security_info._security_rsn_radius.radius_port = al_impl_le32_to_cpu(le2cpu);

					len = *buffer_ptr;
					buffer_ptr++;
					memset(&if_info->security_info._security_rsn_radius.radius_secret_key,
							0,
							512);
					ret = al_encode_key(AL_CONTEXT(char *) buffer_ptr,
							len,
							_MAC_ADDRESS,
							if_info->security_info._security_rsn_radius.radius_secret_key);
					buffer_ptr += len;
					if_info->security_info._security_rsn_radius.radius_secret_key_len = ret;

					if_info->security_info._security_rsn_radius.cipher_tkip = *buffer_ptr;
					buffer_ptr++;

					if_info->security_info._security_rsn_radius.cipher_ccmp = *buffer_ptr;
					buffer_ptr++;

					memcpy(&le2cpu32, buffer_ptr, sizeof(unsigned int));
					buffer_ptr += sizeof(unsigned int);
					if_info->security_info._security_rsn_radius.group_key_renewal = al_impl_le32_to_cpu(le2cpu32);
				}
				if(node_len & IMCP_EXT_MASK)
				{
					IMCP_MSG_PROCESS_RECEIVE_ID_AND_LEN(buffer_ptr,node_id,node_len); 

					switch(node_id){
						case IMCP_IF_802_11_EXT_11N_INFO_ID :

							if_info->ht_capab.ldpc = *buffer_ptr;
							buffer_ptr++;	
							
							if_info->ht_capab.ht_bandwidth = *buffer_ptr;
							buffer_ptr++;

							memcpy(&le2cpu, buffer_ptr, sizeof(unsigned int));
							if_info->ht_capab.sec_offset =  al_impl_le32_to_cpu(le2cpu);
							buffer_ptr += sizeof(unsigned int);
							//Archana commented			//	if_info->ht_capab.sec_offset =  al_impl_le32_to_cpu(le2cpu);
#if 0
							if_info->ht_capab.smps = *buffer_ptr;
							printf("Receiving : func : %s : : %d :LINE : %d\n",__func__,,__LINE__);
							buffer_ptr++;
#endif
							if_info->ht_capab.gi_20 = *buffer_ptr;
							buffer_ptr++;

							if(if_info->ht_capab.ht_bandwidth == AL_CONF_IF_HT_PARAM_20) {
								if_info->ht_capab.gi_40 = AL_CONF_IF_HT_PARAM_LONG;
								buffer_ptr++;
							}
							else {
								if_info->ht_capab.gi_40 = *buffer_ptr;
								buffer_ptr++;
							}

							if_info->ht_capab.tx_stbc = *buffer_ptr;
							buffer_ptr++;

							if_info->ht_capab.rx_stbc =  *buffer_ptr;
							buffer_ptr++;
#if 0
							if_info->ht_capab.delayed_ba = *buffer_ptr;
							printf("Receiving : func : %s : : %d :LINE : %d\n",__func__,,__LINE__);
							buffer_ptr++;
#endif
							if_info->ht_capab.gfmode = *buffer_ptr;
							buffer_ptr++;

							memcpy(&le2cpu, buffer_ptr, sizeof(unsigned int));
							buffer_ptr += sizeof(unsigned int);
							if_info->ht_capab.max_amsdu_len =  al_impl_le32_to_cpu(le2cpu);
#if 0
							if_info->ht_capab.dsss_cck_40 = *buffer_ptr;
							printf("Receiving : func : %s : : %d :LINE : %d\n",__func__,,__LINE__);
							buffer_ptr++;
#endif
							if_info->ht_capab.intolerant = *buffer_ptr;
							buffer_ptr++;
#if 0
							if_info->ht_capab.lsig_txop = *buffer_ptr;
							printf("Receiving : func : %s : : %d :LINE : %d\n",__func__,,__LINE__);
							buffer_ptr++;

#endif
							if_info->frame_aggregation.ampdu_enable = *buffer_ptr;
							buffer_ptr++;


							memcpy(&le2cpu, buffer_ptr, sizeof(unsigned int));
							if_info->frame_aggregation.max_ampdu_len  =  al_impl_le32_to_cpu(le2cpu);
							buffer_ptr += sizeof(unsigned int);
							break;
						case IMCP_IF_802_11_EXT_11N_11AC_INFO_ID :
							if_info->ht_capab.ldpc = *buffer_ptr;
							buffer_ptr++;

							if(*buffer_ptr == AL_CONF_IF_HT_PARAM_80) {
								if_info->vht_capab.vht_oper_bandwidth = 1;
									if(if_info->dca == 0)
									{
										curr_channel = if_info->wm_channel;
									}
									else {
											curr_channel = if_info->dca_list[0];
									}
									ret = validate_freq_and_seg0_freq(curr_channel, if_info, validation_report);
									if(ret != SUCCESS) {
										return ret;
									}
							}
							else {
							if_info->ht_capab.ht_bandwidth = *buffer_ptr;
							}
							buffer_ptr++;

							memcpy(&le2cpu, buffer_ptr, sizeof(unsigned int));
							if_info->ht_capab.sec_offset =  al_impl_le32_to_cpu(le2cpu);
							buffer_ptr += sizeof(unsigned int);

							if_info->ht_capab.gi_20 = *buffer_ptr;
							buffer_ptr++;

							if(if_info->ht_capab.ht_bandwidth == AL_CONF_IF_HT_PARAM_20) {
								if_info->ht_capab.gi_40 = AL_CONF_IF_HT_PARAM_LONG;
								buffer_ptr++;
							}

							else {
								if_info->ht_capab.gi_40 = *buffer_ptr;
								buffer_ptr++;
							}

							if_info->ht_capab.tx_stbc = *buffer_ptr;
							buffer_ptr++;

							if_info->ht_capab.rx_stbc =  *buffer_ptr;
							buffer_ptr++;

							if_info->ht_capab.gfmode = *buffer_ptr;
							buffer_ptr++;

							memcpy(&le2cpu, buffer_ptr, sizeof(unsigned int));
							buffer_ptr += sizeof(unsigned int);
							if_info->ht_capab.max_amsdu_len =  al_impl_le32_to_cpu(le2cpu);
							if_info->ht_capab.intolerant = *buffer_ptr;
							buffer_ptr++;

							if_info->frame_aggregation.ampdu_enable = *buffer_ptr;
							buffer_ptr++;


							memcpy(&le2cpu, buffer_ptr, sizeof(unsigned int));
							if_info->frame_aggregation.max_ampdu_len  =  al_impl_le32_to_cpu(le2cpu);
							buffer_ptr += sizeof(unsigned int);


							memcpy(&le2cpu, buffer_ptr, sizeof(unsigned int));
							if_info->vht_capab.max_mpdu_len  =  al_impl_le32_to_cpu(le2cpu);
							buffer_ptr += sizeof(unsigned int);

							if(if_info->ht_capab.ht_bandwidth == AL_CONF_IF_HT_PARAM_20)
							{
								if_info->vht_capab.gi_80 = AL_CONF_IF_HT_PARAM_LONG;
								buffer_ptr++;
							}
							else if((if_info->ht_capab.ht_bandwidth == AL_CONF_IF_HT_PARAM_40_ABOVE) && (if_info->vht_capab.vht_oper_bandwidth != 1))
							{
								if_info->vht_capab.gi_80 = AL_CONF_IF_HT_PARAM_LONG;
								buffer_ptr++;
							}
							else if((if_info->ht_capab.ht_bandwidth == AL_CONF_IF_HT_PARAM_40_BELOW) && (if_info->vht_capab.vht_oper_bandwidth != 1)) {
								if_info->vht_capab.gi_80 = AL_CONF_IF_HT_PARAM_LONG;
								buffer_ptr++;
							}
							else {
								if_info->vht_capab.gi_80 = *buffer_ptr;
								buffer_ptr++;
							}
							break;

						default :
							al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : Entered interface currently not supporting Extension%s: %d\n",__func__,__LINE__);

							buffer_ptr += node_len;

					}

				}
				break;
			case IMCP_IF_802_15_4_INFO_ID : 
				al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : Currently 802.15.4 interface config request/response Not Supported :%s: %d\n",__func__,__LINE__);
				break;
			default :
				al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : Entered INVALID INTERFACE :%s: %d\n",__func__,__LINE__);
				buffer_ptr += node_len;
		}
	}

	bytes_read = (int)(buffer_ptr - buffer);
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit: %s: %d\n",__func__,__LINE__);

	return err_code;
}

#if 1 /*** vlan validations***/
unsigned char validate_vlan_config_info(config_validation_report_t  *validation_report)
{
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter: %s: %d\n",__func__,__LINE__);
	int                   len = 0;
	int                   index;
	unsigned char         ret = 0;

	len = imcp_vlan_config_info_data->vlan_count;
	for (index = 0; index < len - 1; index++)
	{
		ret = validate_vlan_tag_info(imcp_vlan_config_info_data->imcp_vlan_info[index].vlan_tag,validation_report);
		if(ret)
		{
			goto validation_exit;
		}

		ret = validate_vlan_dot1p_priority_info(imcp_vlan_config_info_data->imcp_vlan_info[index].dot1p_p, validation_report);
		if(ret)
		{
			goto validation_exit;
		}
#if 1
		ret = validate_vlan_security_info(NULL,&imcp_vlan_config_info_data->imcp_vlan_info[index].security_info, validation_report);
		if(ret)
		{
			VAL_PRINTF("Failure in vlan security settings \n");

			goto validation_exit;
		}
#endif

	}
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit: %s: %d\n",__func__,__LINE__);

validation_exit:

	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter: %s: %d\n",__func__,__LINE__);
	return ret;
}
#endif

static unsigned char validate_apconfig_info(config_validation_report_t  *validation_report)
{
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter: %s: %d\n",__func__,__LINE__);
	int                          fd;
	tddi_packet_t                *packet;
	tddi_get_hw_config_request_t *request;
	unsigned char                ret = 0;
	int                          i;
	unsigned char                *request_packet;
	unsigned char                response_buffer[sizeof(tddi_packet_t) + sizeof(tddi_get_hw_config_t)] = { 0 };
	tddi_packet_t                *response_packet;
	tddi_get_hw_config_t         *response_data;
	unsigned char                if_no;
	al_conf_if_info_t            *if_info;

	/* TODO : Validate Node related config paramaters except variable length parameters*/
	/* TODO : Validate All interface related config parameters except variable length parameters*/



	ret = validate_node_heartbeat_interval(imcp_apconfig_data->heartbeat_interval, validation_report);
	if(ret)
	{
		VAL_PRINTF("Failure in global heart_beat interval\n");
		goto validation_exit;
	}


	ret = validate_preferred_parent(imcp_apconfig_data->preferred_parent, validation_report);
	if(ret)
	{
		VAL_PRINTF("Failure in Preferred parent validation\n");
		goto validation_exit;
	}
	ret = validate_gps_x_coordinate(imcp_apconfig_data->gps_x_coordinate, validation_report);
	if(ret)
	{
		VAL_PRINTF("Failure in global gps_x_coordinate validation\n");
		goto validation_exit;
	}
	ret = validate_gps_y_coordinate(&imcp_apconfig_data->gps_y_coordinate, validation_report);
	if(ret)
	{
		VAL_PRINTF("Failure in global y_coordinate validation\n");
		goto validation_exit;
	}


	/* NOTE :
	 * Currently validating interface phy sub type, HT, VHT and channel-bandwidth configuration parameters only
	 */
	VAL_PRINTF("*************validating_ht_vht config from NMS - entering*********************\n");

	VAL_PRINTF("Interface count is : %d\n", imcp_apconfig_data->interface_count);

	for (if_no = 0; if_no < imcp_apconfig_data->interface_count; if_no++)
	{
		if_info = &imcp_apconfig_data->if_info[if_no];

		if(strcmp(if_info->name, "wlan3")) {

			ret = validate_interface_txrate(if_info, validation_report);
			if(ret)
			{
				goto validation_exit;
			}
			if (if_info->phy_type != AL_CONF_IF_PHY_TYPE_ETHERNET )
			{

				ret = validate_interface_txpower(if_info, validation_report);
				VAL_PRINTF("txpower : %d if_name : %s\n",imcp_apconfig_data->if_info[if_no].txpower, imcp_apconfig_data->if_info[if_no].name);
				if(ret)
				{
					goto validation_exit;
				}


				ret = validate_interface_rts_threshold(if_info, validation_report);
				if(ret)
				{
					VAL_PRINTF("Failure in interface rts_threshold value\n",imcp_apconfig_data->if_info[if_no].rts_th);
					goto validation_exit;
				}

				ret = validate_interface_frag_threshold(if_info, validation_report);
				if(ret)
				{
					VAL_PRINTF("Failure in interface frag_threshold value\n",imcp_apconfig_data->if_info[if_no].frag_th);
					goto validation_exit;
				}
                
				if(TDDI_802_11_IS_N(if_info->phy_sub_type) || TDDI_802_11_IS_AC(if_info->phy_sub_type)) {
					request_packet = (unsigned char *)malloc(sizeof(tddi_packet_t) + sizeof(tddi_get_hw_config_request_t));
					packet         = (tddi_packet_t *)request_packet;
					request        = (tddi_get_hw_config_request_t *)(request_packet + sizeof(tddi_packet_t));

					packet->version   = TDDI_CURRENT_VERSION;
					packet->signature = TDDI_PACKET_SIGNATURE;
					packet->type      = TDDI_PACKET_TYPE_REQUEST;
					packet->data_size = sizeof(tddi_get_hw_config_request_t);
					packet->sub_type  = TDDI_REQUEST_TYPE_GET_HW_CONFIG;

					strcpy(request->if_name,if_info->name);
					request->sub_type = if_info->phy_sub_type;

					VAL_PRINTF("Interface name : %s\n",if_info->name);

					fd = open(TDDI_FILE_NAME, O_RDWR);

					if (fd == -1)
					{
						VAL_PRINTF(stderr, "Error opening "TDDI_FILE_NAME "\n");
						free(request_packet);
						return NODE_CONFIG_FAILURE;
					}

					ret = write(fd, request_packet, sizeof(tddi_packet_t) + sizeof(tddi_get_hw_config_t));

					if (ret != sizeof(tddi_packet_t) + sizeof(tddi_get_hw_config_t))
					{
						VAL_PRINTF(stderr, "Error writing to "TDDI_FILE_NAME " ret=%d\n", ret);
						free(request_packet);
						return NODE_CONFIG_FAILURE;
					}


					lseek(fd, 0, SEEK_CUR);

					ret = read(fd, response_buffer, sizeof(response_buffer));

					VAL_PRINTF(" ret val : %d, size of response : %d\n",ret,sizeof(response_buffer));

					if (ret != sizeof(response_buffer))
					{
						fprintf(stderr, "configd : Response buffer size error while reading from "TDDI_FILE_NAME " ret=%d,errno=%d\n", ret, errno);
						al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : Response buffer size error while reading from "TDDI_FILE_NAME " ret=%d,errno=%d :%s: %d\n",ret, errno,__func__,__LINE__);
						switch (errno)
						{
							case EINTR:
								al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : EINTR occurred :%s: %d\n",__func__,__LINE__);
								fprintf(stderr, "EINTR occurred\n");
								break;

							case EAGAIN:
								al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : EAGAIN occurred :%s: %d\n",__func__,__LINE__);
								fprintf(stderr, "EAGAIN occurred\n");
								break;

							case EIO:
								al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : EIO occurred :%s: %d\n",__func__,__LINE__);
								fprintf(stderr, "EIO occurred\n");
								break;

							case EBADF:
								al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : EBADF occurred :%s: %d\n",__func__,__LINE__);
								fprintf(stderr, "EBADF occurred\n");
								break;

							case EINVAL:
								al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : EINVAL occurred :%s: %d\n",__func__,__LINE__);
								fprintf(stderr, "EINVAL occurred\n");
								break;

							case EFAULT:
								al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : EFAULT  occurred :%s: %d\n",__func__,__LINE__);
								fprintf(stderr, "EFAULT occurred\n");
								break;
						}
						free(request_packet);
						close(fd);
						return NODE_CONFIG_FAILURE;
					}

					free(request_packet);
					close(fd);

					response_packet = (tddi_packet_t *)response_buffer;
					response_data   = (tddi_get_hw_config_t *)(response_buffer + sizeof(tddi_packet_t));

					switch (response_data->response)
					{
						case TDDI_CONFIGURE_MESH_RESPONSE_SUCCESS:
							al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : Getting md_configs successful :%s: %d\n",__func__,__LINE__);
							VAL_PRINTF(stderr, "Getting md_configs successful\n");
							break;

						case TDDI_CONFIGURE_MESH_RESPONSE_BUSY:
							al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : Mesh busy :%s: %d\n",__func__,__LINE__);
							VAL_PRINTF(stderr, "Mesh busy\n");
							break;

						default :
						   al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : Error occurred in tddi response_data : %s : %d\n",__func__,__LINE__);
						   VAL_PRINTF("Error occurred in tddi response_data : %s : %d\n",__func__,__LINE__);
						   return NODE_CONFIG_FAILURE;
					}

					memset(validation_report, 0, sizeof(config_validation_report_t));

					ret = validate_ht_capabilities(response_data->ht_cap,if_info, validation_report);
					if(ret != SUCCESS)
					{
						al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : HT validation got failed in interface : %s :%s: %d\n",if_info->name,__func__,__LINE__);
						VAL_PRINTF("HT validation got failed in interface : %s\n",if_info->name);
						goto validation_exit;
					}
					else {
						al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"CONFIGD : INFO :HT validation successfully validated :%s: %d\n",__func__,__LINE__);
						VAL_PRINTF("HT validation successfully validated\n");
					}

					ret = validate_vht_capabilities(response_data->vht_cap,if_info, validation_report);
					if(ret != SUCCESS)
					{
						al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : VHT validation got failed in interface : %s :%s: %d\n",if_info->name,__func__,__LINE__);
						VAL_PRINTF("VHT validation got failed in interface : %s\n",if_info->name);
						goto validation_exit;
					}
					else {
						al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"CONFIGD : INFO :HT validation successfully validated :%s: %d\n",__func__,__LINE__);
						VAL_PRINTF("VHT validation successfully validated\n");
					}
				}

			}
		}
	}
	VAL_PRINTF("*************validating_ht_vht request from NMS - exiting*********************\n");
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit: %s: %d\n",__func__,__LINE__);
validation_exit:
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit: %s: %d\n",__func__,__LINE__);
	return ret;
}

int imcp_apconfig_info_helper(unsigned char *buffer, struct sockaddr_in *client_addr)
{
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter: %s: %d\n",__func__,__LINE__);
	int					al_conf_handle, i;
	int					dot11e_conf_handle;
	unsigned char		*buffer_ptr = buffer + get_imcp_signature_mesh_id_size();
	unsigned char		err_code = 0;
	unsigned int		le2cpu;
	int					running_config_flag = 1;
	int					flag = 0;
	unsigned short		node_id;
	unsigned short		node_len;
	int					ret, if_no; 
	config_validation_report_t  validation_report;
	al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"CONFIGD : INFO :IMCP_AP_CONFIGURATION_INFO Packet - Received :%s: %d\n",__func__,__LINE__);


	IMCP_MSG_PROCESS_RECEIVE_ID_AND_LEN(buffer_ptr,node_id,node_len);

	memcpy(imcp_apconfig_data->mac_address, buffer_ptr, 6);
	buffer_ptr += 6;

	imcp_apconfig_data->request_response_type = *buffer_ptr;
	buffer_ptr++;

	imcp_apconfig_data->response_id = *buffer_ptr;
	buffer_ptr++;

	if (imcp_apconfig_data->request_response_type == TYPE_RESPONSE)
	{
		al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : IMCP_AP_CONFIGURATION_INFO Packet ignored func:%s: %d\n",__func__,__LINE__);
		return 0;
	}

	if (memcmp(imcp_apconfig_data->mac_address, _MAC_ADDRESS, 6))
	{
		al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : IMCP_AP_CONFIGURATION_INFO Packet ignored func:%s: %d\n",__func__,__LINE__);
		return 0;
	}

	memcpy(imcp_apconfig_data->preferred_parent, buffer_ptr, 6);
	buffer_ptr += 6;

	memcpy(imcp_apconfig_data->signal_map, buffer_ptr, 8);
	buffer_ptr += 8;

	imcp_apconfig_data->heartbeat_interval = *buffer_ptr;
	buffer_ptr++;

	imcp_apconfig_data->heartbeat_miss_count = *buffer_ptr;
	buffer_ptr++;

	imcp_apconfig_data->hop_cost = *buffer_ptr;
	buffer_ptr++;

	imcp_apconfig_data->max_allowable_hops = *buffer_ptr;
	buffer_ptr++;

	imcp_apconfig_data->las_interval = *buffer_ptr;
	buffer_ptr++;

	imcp_apconfig_data->cr_threshold = *buffer_ptr;
	buffer_ptr++;

	imcp_apconfig_data->name_length = *buffer_ptr;
	buffer_ptr++;
	
	/*For variable length fields we are doing validations immediately after reading from buffer to avoid buffer corruption*/
	if (imcp_apconfig_data->name_length > 0)
	{
		err_code = validate_node_name(imcp_apconfig_data->name_length, &validation_report);
		if(err_code)
		{
			goto validation_failed;
		}
		memcpy(imcp_apconfig_data->name, buffer_ptr, imcp_apconfig_data->name_length);
		imcp_apconfig_data->name[imcp_apconfig_data->name_length] = 0;
		buffer_ptr += imcp_apconfig_data->name_length;
	}

	imcp_apconfig_data->essid_length = *buffer_ptr;
	buffer_ptr++;

	if (imcp_apconfig_data->essid_length > 0)
	{
		memcpy(imcp_apconfig_data->essid, buffer_ptr, imcp_apconfig_data->essid_length);
		imcp_apconfig_data->essid[imcp_apconfig_data->essid_length] = 0;
		buffer_ptr += imcp_apconfig_data->essid_length;
	}

	imcp_apconfig_data->desc_length = *buffer_ptr;
	buffer_ptr++;
	
	/*For variable length fields we are doing validations immediately after reading from buffer to avoid buffer corruption*/
	if (imcp_apconfig_data->desc_length > 0)
	{
		err_code = validate_node_description(imcp_apconfig_data->desc_length, &validation_report);
		if(err_code)
		{
			goto validation_failed;
		}
		memcpy(imcp_apconfig_data->description, buffer_ptr, imcp_apconfig_data->desc_length);
		imcp_apconfig_data->description[imcp_apconfig_data->desc_length] = 0;
		buffer_ptr += imcp_apconfig_data->desc_length;
	}

	imcp_apconfig_data->x_coordinate_length = *buffer_ptr;
	buffer_ptr++;

	if (imcp_apconfig_data->x_coordinate_length > 0)
	{
		memcpy(imcp_apconfig_data->gps_x_coordinate, buffer_ptr, imcp_apconfig_data->x_coordinate_length);
		imcp_apconfig_data->gps_x_coordinate[imcp_apconfig_data->x_coordinate_length] = 0;
		buffer_ptr += imcp_apconfig_data->x_coordinate_length;
	}

	imcp_apconfig_data->y_coordinate_length = *buffer_ptr;
	buffer_ptr++;

	if (imcp_apconfig_data->y_coordinate_length > 0)
	{
		memcpy(imcp_apconfig_data->gps_y_coordinate, buffer_ptr, imcp_apconfig_data->y_coordinate_length);
		imcp_apconfig_data->gps_y_coordinate[imcp_apconfig_data->y_coordinate_length] = 0;
		buffer_ptr += imcp_apconfig_data->y_coordinate_length;
	}


	memcpy(&le2cpu, buffer_ptr, sizeof(unsigned int));
	buffer_ptr += sizeof(unsigned int);
	imcp_apconfig_data->rts_threshold = al_impl_le32_to_cpu(le2cpu);

	memcpy(&le2cpu, buffer_ptr, sizeof(unsigned int));
	buffer_ptr += sizeof(unsigned int);
	imcp_apconfig_data->frag_threshold = al_impl_le32_to_cpu(le2cpu);

	memcpy(&le2cpu, buffer_ptr, sizeof(unsigned int));
	buffer_ptr += sizeof(unsigned int);
	imcp_apconfig_data->beacon_interval = al_impl_le32_to_cpu(le2cpu);

	imcp_apconfig_data->dca = *buffer_ptr;
	buffer_ptr++;
	imcp_apconfig_data->dca = 1;

	/** Skip stay awake count */
	buffer_ptr++;

	/** Skip Bridge aeging time */
	buffer_ptr++;

	imcp_apconfig_data->fcc_certified_operation = *buffer_ptr;
	buffer_ptr++;

	imcp_apconfig_data->etsi_certified_operation = *buffer_ptr;
	buffer_ptr++;

	memcpy(&le2cpu, buffer_ptr, sizeof(unsigned int));
	buffer_ptr += sizeof(unsigned int);
	imcp_apconfig_data->ds_tx_rate = al_impl_le32_to_cpu(le2cpu);

	imcp_apconfig_data->ds_tx_power = *buffer_ptr;
	buffer_ptr++;

	imcp_apconfig_data->interface_count = *buffer_ptr;
	buffer_ptr++;

	if (imcp_apconfig_data->interface_count > 0)
	{       
		err_code = imcp_apconfig_interface_info_helper(buffer_ptr, imcp_apconfig_data->interface_count, &validation_report);
		if(err_code != SUCCESS)
		{
			goto validation_failed;
		}
	}

	_write_gps_info(imcp_apconfig_data->gps_x_coordinate, imcp_apconfig_data->gps_y_coordinate);

	/*validating all node parameterds except variable length parameters*/
	err_code = validate_apconfig_info(&validation_report);
	if(err_code) {
		goto validation_failed;
	}

	al_conf_handle = libalconf_read_config_data();

	al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"CONFIGD : INFO :Reboot required check routine is called :%s: %d\n",__func__,__LINE__);
	flag = reboot_required_parameter_check_for_hostapd(al_conf_handle);
	if (al_conf_handle > 0)
	{
		EXECUTE_PRE_FS_CHANGE
			set_mesh_config_sqnr(increment_config_sqnr(al_conf_handle));
			write_config_data(al_conf_handle);
		EXECUTE_POST_FS_CHANGE(POST_FS_PARAM_TYPE_CONFIG)
			al_conf_close(AL_CONTEXT al_conf_handle);
	}

	if (flag)
	{
		al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"CONFIGD : INFO :Nothing to be done after reboot value is set :%s: %d\n",__func__,__LINE__);
	}
	else
	{
		dot11e_conf_handle = read_dot11e_category_data();
		if (dot11e_conf_handle == -1)
		{
			al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : Could not read dot11e configuration func:%s: %d\n",__func__,__LINE__);
			return 0;
		}

		al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"CONFIGD : INFO : Reboot flag is not set init_configuration is called for hostapd run time :%s: %d\n",__func__,__LINE__);

		/*call hostapd routine to set the configurations in file*/

		al_conf_handle = libalconf_read_config_data();

		//EXECUTE_MESHD_CONFIGURE
		init_configurations(al_conf_handle, dot11e_conf_handle, running_config_flag, 0);
	}

	al_conf_close(AL_CONTEXT al_conf_handle);
	dot11e_conf_close(AL_CONTEXT dot11e_conf_handle);
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit: %s: %d\n",__func__,__LINE__);

validation_failed:
	send_config_updated_response(client_addr, imcp_apconfig_data->response_id, TYPE_AP_CONFIG_REQUEST, err_code, &validation_report);
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit: %s: %d\n",__func__,__LINE__);
	return 0;
}

static unsigned char validate_node_name(unsigned char name_length, config_validation_report_t  *validation_report)
{
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter: %s: %d\n",__func__,__LINE__);
	if(name_length > MAX_NODE_NAME_LEN_VALUE)
	{
		prepare_error_report(NULL, NODE_NAME_LENGTH, UNSIGNEDCHAR, &name_length, validation_report);
		al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : Invalid node name length : %s: %d\n",__func__,__LINE__);
		return OUT_OF_RANGE;
	}
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit: %s: %d\n",__func__,__LINE__);
	return SUCCESS;
}


static unsigned char validate_node_description(unsigned char desc_length, config_validation_report_t  *validation_report)
{
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter: %s: %d\n",__func__,__LINE__);
	if(desc_length >  MAX_NODE_DESC_LEN_VALUE)
	{
		prepare_error_report(NULL, NODE_DESCRIPTION_LENGTH, UNSIGNEDCHAR, &desc_length, validation_report);
		al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : Invalid node description length : %s: %d\n",__func__,__LINE__);
		return OUT_OF_RANGE;
	}
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit: %s: %d\n",__func__,__LINE__);
	return SUCCESS;
}


static unsigned char validate_interface_essid(al_conf_if_info_t *if_info, int essid_length, config_validation_report_t *validation_report)
{
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter: %s: %d\n",__func__,__LINE__);
	if(essid_length > MAX_ESSID_LEN_VALUE)
	{
		prepare_error_report(if_info, IF_ESSID_LENGTH, INTEGER, &essid_length, validation_report);
		al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : Invalid Essid : %s: %d\n",__func__,__LINE__);
		return OUT_OF_RANGE;
	}
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit: %s: %d\n",__func__,__LINE__);
	return SUCCESS;
}


int copy_security_wep_key(al_security_info_t *al_conf, al_security_info_t *imcp_conf)
{
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter: %s: %d\n",__func__,__LINE__);
	int  i, j;
	char *p = NULL, *q = NULL, *ret_buffer, *ret_buffer1;
	int  ret;


	p = (char *)malloc(105);
	if (p == NULL)
	{
		al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : ERROR : failed to allocate memory for p : %s: %d\n",__func__,__LINE__);
	}

	q = (char *)malloc(105);
	if (q == NULL)
	{
		al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : ERROR : failed to allocate memory for q : %s: %d\n",__func__,__LINE__);
	}

	memset(p, 0, 105);
	memset(q, 0, 105);

	ret_buffer = p;

	for (i = 0; i < al_conf->_security_wep.key_count; i++)
	{
		memset(buffer_conf, 0, 105);
		for (j = 0; j <= al_conf->_security_wep.keys[i].len - 1; j++)
		{
			sprintf(buffer_conf, "%02X", al_conf->_security_wep.keys[i].bytes[j]);
			ret = strlen(buffer_conf);
			memcpy(ret_buffer, buffer_conf, ret);
			ret_buffer += ret;
			if (j < al_conf->_security_wep.keys[i].len - 1)
			{
				*ret_buffer = ':';
				ret_buffer++;
			}
		}
		if (i < al_conf->_security_wep.key_count - 1)
		{
			*ret_buffer = ',';
			ret_buffer++;
		}
	}

	ret = ret_buffer - p;

	memcpy(buffer_conf, p, ret);

	ret         = 0;
	ret_buffer1 = q;

	for (i = 0; i < imcp_conf->_security_wep.key_count; i++)
	{
		memset(buffer1_conf, 0, 105);
		al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"CONFIGD : INFO :len =%d : %s: %d\n",imcp_conf->_security_wep.keys[i].len,__func__,__LINE__);
		for (j = 0; j <= imcp_conf->_security_wep.keys[i].len - 1; j++)
		{
			sprintf(buffer1_conf, "%02X", imcp_conf->_security_wep.keys[i].bytes[j]);
			ret = strlen(buffer1_conf);
			memcpy(ret_buffer1, buffer1_conf, ret);
			ret_buffer1 += ret;
			if (j < imcp_conf->_security_wep.keys[i].len - 1)
			{
				*ret_buffer1 = ':';
				ret_buffer1++;
			}
		}
		if (i < imcp_conf->_security_wep.key_count - 1)
		{
			*ret_buffer1 = ',';
			ret_buffer1++;
		}
	}

	ret = ret_buffer1 - q;
	memcpy(buffer1_conf, q, ret);
	free(p);
	free(q);
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit: %s: %d\n",__func__,__LINE__);
	return 0;
}


int reboot_required_parameter_check_for_hostapd(int al_conf_handle)
{
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter: %s: %d\n",__func__,__LINE__);
	int             i, j, ret;
	int             al_if_count;
	int             Reboot_Req = 0;
	_al_conf_data_t *data;

	al_conf_if_info_t *conf_if_info;

	data = (_al_conf_data_t *)al_conf_handle;
	_security_wep_t        _security_wep;
	_security_rsn_psk_t    _security_rsn_psk;
	_security_rsn_radius_t _security_rsn_radius;


	for (i = 0; i < data->if_count; i++)
	{
		conf_if_info = &data->if_info[i];

		for (j = 0; j < imcp_apconfig_data->interface_count; j++)
		{
			if (!strcmp(conf_if_info->name, imcp_apconfig_data->if_info[j].name))
			{
				if ((conf_if_info->use_type == AL_CONF_IF_USE_TYPE_AP) && (imcp_apconfig_data->if_info[j].use_type == AL_CONF_IF_USE_TYPE_AP))
				{
					copy_security_wep_key(&conf_if_info->security_info, &imcp_apconfig_data->if_info[j].security_info);

					if (conf_if_info->wm_channel != imcp_apconfig_data->if_info[j].wm_channel)
					{
						Reboot_Req = 1;
						al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"CONFIGD : INFO : Reboot parameter found wm_channel : %s: %d\n",__func__,__LINE__);
						break;
					}
					else if (conf_if_info->security_info._security_none.enabled != imcp_apconfig_data->if_info[j].security_info._security_none.enabled)
					{
						Reboot_Req = 1;
						al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"CONFIGD : INFO : Reboot parameter found security none : %s: %d\n",__func__,__LINE__);
						break;
					}
					else if (imcp_apconfig_data->if_info[j].security_info._security_wep.enabled == 1)
					{
						if (conf_if_info->security_info._security_wep.strength != imcp_apconfig_data->if_info[j].security_info._security_wep.strength)
						{
							Reboot_Req = 1;
							al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"CONFIGD : INFO : Reboot parameter found security strength : %s: %d\n",__func__,__LINE__);
							break;
						}
						else if (conf_if_info->security_info._security_wep.key_index != imcp_apconfig_data->if_info[j].security_info._security_wep.key_index)
						{
							Reboot_Req = 1;
							al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"CONFIGD : INFO : Reboot parameter found security key index : %s: %d\n",__func__,__LINE__);
							break;
						}
						else if (strcmp(buffer_conf, buffer1_conf))
						{
							Reboot_Req = 1;
							al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"CONFIGD : INFO : Reboot parameter found security key index buffer=%s\t buffer1=%s\t : %s: %d\n",buffer_conf, buffer1_conf,__func__,__LINE__);
							break;
						}
					}
					else if (imcp_apconfig_data->if_info[j].security_info._security_rsn_psk.enabled == 1)
					{
						if (conf_if_info->security_info._security_rsn_psk.mode != imcp_apconfig_data->if_info[j].security_info._security_rsn_psk.mode)
						{
							Reboot_Req = 1;
							al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"CONFIGD : INFO : Reboot parameter found RSn psk mode : %s: %d\n",__func__,__LINE__);
							break;
						}
						else if (conf_if_info->security_info._security_rsn_psk.cipher_tkip != imcp_apconfig_data->if_info[j].security_info._security_rsn_psk.cipher_tkip)
						{
							Reboot_Req = 1;
							al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"CONFIGD : INFO : Reboot parameter found RSn psk cipher_tkip : %s: %d\n",__func__,__LINE__);
							break;
						}
						else if (conf_if_info->security_info._security_rsn_psk.cipher_ccmp != imcp_apconfig_data->if_info[j].security_info._security_rsn_psk.cipher_ccmp)
						{
							Reboot_Req = 1;
							al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"CONFIGD : INFO : Reboot parameter found RSn psk cipher_ccmp : %s: %d\n",__func__,__LINE__);
							break;
						}
						else if (conf_if_info->security_info._security_rsn_psk.group_key_renewal != imcp_apconfig_data->if_info[j].security_info._security_rsn_psk.group_key_renewal)
						{
							Reboot_Req = 1;
							al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"CONFIGD : INFO : Reboot parameter found RSn psk group_key_renewal : %s: %d\n",__func__,__LINE__);
							break;
						}
						else if (strcmp(conf_if_info->security_info._security_rsn_psk.key_buffer, imcp_apconfig_data->if_info[j].security_info._security_rsn_psk.key_buffer))
						{
							Reboot_Req = 1;
							al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"CONFIGD : INFO : Reboot parameter found RSn psk key buffer : %s: %d\n",__func__,__LINE__);
							break;
						}
					}
					else if (imcp_apconfig_data->if_info[j].security_info._security_rsn_radius.enabled == 1)
					{
						if (conf_if_info->security_info._security_rsn_radius.mode != imcp_apconfig_data->if_info[j].security_info._security_rsn_radius.mode)
						{
							Reboot_Req = 1;
							al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"CONFIGD : INFO : Reboot parameter found RSn radius mode : %s: %d\n",__func__,__LINE__);
							break;
						}
						else if (conf_if_info->security_info._security_rsn_radius.radius_port != imcp_apconfig_data->if_info[j].security_info._security_rsn_radius.radius_port)
						{
							Reboot_Req = 1;
							al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"CONFIGD : INFO : Reboot parameter found RSn radius port : %s: %d\n",__func__,__LINE__);
							break;
						}
						else if (conf_if_info->security_info._security_rsn_radius.cipher_tkip != imcp_apconfig_data->if_info[j].security_info._security_rsn_radius.cipher_tkip)
						{
							Reboot_Req = 1;
							al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"CONFIGD : INFO : Reboot parameter found RSn radius cipher_tkip : %s: %d\n",__func__,__LINE__);
							break;
						}
						else if (conf_if_info->security_info._security_rsn_radius.cipher_ccmp != imcp_apconfig_data->if_info[j].security_info._security_rsn_radius.cipher_ccmp)
						{
							Reboot_Req = 1;
							al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"CONFIGD : INFO : Reboot parameter found RSn radius cipher_ccmp : %s: %d\n",__func__,__LINE__);
							break;
						}
						else if (conf_if_info->security_info._security_rsn_radius.group_key_renewal != imcp_apconfig_data->if_info[j].security_info._security_rsn_radius.group_key_renewal)
						{
							Reboot_Req = 1;
							al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"CONFIGD : INFO : Reboot parameter found RSn radius group_key_renewal : %s: %d\n",__func__,__LINE__);
							break;
						}
						else if (memcmp(conf_if_info->security_info._security_rsn_radius.radius_server, imcp_apconfig_data->if_info[j].security_info._security_rsn_radius.radius_server, 4))
						{
							Reboot_Req = 1;
							al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"CONFIGD : INFO : Reboot parameter found RSn radius server : %s: %d\n",__func__,__LINE__);
							break;
						}
						else if (memcmp(conf_if_info->security_info._security_rsn_radius.radius_secret_key, imcp_apconfig_data->if_info[j].security_info._security_rsn_radius.radius_secret_key, imcp_apconfig_data->if_info[j].security_info._security_rsn_radius.radius_secret_key_len))
						{
							Reboot_Req = 1;
							al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"CONFIGD : INFO : Reboot parameter found RSn radius secret key : %s: %d\n",__func__,__LINE__);
							break;
						}
					}
				}
			}
		}
		if (Reboot_Req)
		{
			break;
		}
	}
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit: %s: %d\n",__func__,__LINE__);

	return Reboot_Req;
}


int imcp_vlan_config_info_helper(unsigned char *buffer, struct sockaddr_in *client_addr)
{
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter: %s: %d\n",__func__,__LINE__);
	int            al_conf_handle;
	unsigned char  *buffer_ptr = buffer + get_imcp_signature_mesh_id_size();
	unsigned int   le2cpu;
	int            vlan_count = 0;
	int            VLAN_count = 0;
	int            index;
	int            i;
	int            j;
	int            len        = 0;
	int            key_length = 0;
	unsigned char  req_res_type;
	unsigned char  request_id;
	int            byteno;
	unsigned short le2cpu16;
	unsigned int   le2cpu32;
	int            ret;
	unsigned short node_id;
	unsigned short node_len;
	unsigned char  err_code = 0;
   int Patched_vlanCount = 0;
	config_validation_report_t validation_report;

	al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"CONFIGD : INFO : IMCP_VLAN_CONFIGURATION_INFO Packet - Received: %s line: %d\n",__func__,__LINE__);

	IMCP_MSG_PROCESS_RECEIVE_ID_AND_LEN(buffer_ptr,node_id,node_len);

	memcpy(imcp_vlan_config_info_data->mac_address, buffer_ptr, 6);
	buffer_ptr += 6;

	req_res_type = *buffer_ptr;
	buffer_ptr++;

	if (req_res_type == TYPE_RESPONSE)
	{
		al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : IMCP_VLAN_CONFIGURATION_INFO Packet ignored fun : %s: %d\n",__func__,__LINE__);
		return 0;
	}

	if (memcmp(imcp_vlan_config_info_data->mac_address, _MAC_ADDRESS, 6))
	{
		al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : IMCP_VLAN_CONFIGURATION_INFO Packet ignored fun : %s: %d\n",__func__,__LINE__);
		return 0;
	}

	request_id = *buffer_ptr;
	buffer_ptr++;

	vlan_count = *buffer_ptr;
	imcp_vlan_config_info_data->vlan_count = vlan_count;
	buffer_ptr++;

	memcpy(&le2cpu16, buffer_ptr, sizeof(unsigned short));
	buffer_ptr += sizeof(unsigned short);
	imcp_vlan_config_info_data->tag = al_impl_le16_to_cpu(le2cpu16);

	imcp_vlan_config_info_data->dot1p_priority = *buffer_ptr;
	buffer_ptr++;

	imcp_vlan_config_info_data->dot11e_enabled = *buffer_ptr;
	buffer_ptr++;

	imcp_vlan_config_info_data->dot11e_category = *buffer_ptr;
	buffer_ptr++;

	if (vlan_count <= 0)
	{
		al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : Invalid vlan count func:%s : %d\n",__func__,__LINE__);
		return 0;
	}
   Patched_vlanCount = _BEGIN_SIGNATURE[18];

   /*Vlan interfaces count not including default vlan*/
   if (vlan_count) {
     VLAN_count = vlan_count - 1;
   }

   if (VLAN_count > Patched_vlanCount) {
	  VAL_PRINTF("Created VLAN interfaces %d greater than Patched VLANs %d : Configured VLAN count is %d \n", VLAN_count, Patched_vlanCount, Patched_vlanCount);
	  prepare_error_report(NULL, NODE_VLAN_COUNT, INTEGER, &VLAN_count, &validation_report);
	  err_code = OUT_OF_RANGE;
	  goto validation_failed;
   }

	if (vlan_count > MAX_VLAN_COUNT)
	{
		VAL_PRINTF("Max VLAN interfaces allowed %d : Configured VLAN count is %d \n", MAX_VLAN_COUNT, vlan_count);
		prepare_error_report(NULL, NODE_VLAN_COUNT, INTEGER, &vlan_count, &validation_report);
		err_code = OUT_OF_RANGE;
		goto validation_failed;
	}

	imcp_vlan_config_info_data->imcp_vlan_info = (imcp_vlan_info_data_t *)malloc(sizeof(imcp_vlan_info_data_t) * (vlan_count - 1));

	for (index = 0; index < vlan_count - 1; index++)
	{
		len = *buffer_ptr;
		buffer_ptr++;
		
		err_code = validate_vlan_name_length(len, &validation_report);
		if(err_code)
		{
			goto validation_failed;
		}
		memcpy(imcp_vlan_config_info_data->imcp_vlan_info[index].vlan_name, buffer_ptr, len);
		imcp_vlan_config_info_data->imcp_vlan_info[index].vlan_name[len] = 0;
		buffer_ptr += len;

		memcpy(imcp_vlan_config_info_data->imcp_vlan_info[index].ip_address, buffer_ptr, 4);
		buffer_ptr += 4;

		memcpy(&le2cpu16, buffer_ptr, sizeof(unsigned short));
		buffer_ptr += sizeof(unsigned short);
		imcp_vlan_config_info_data->imcp_vlan_info[index].vlan_tag = al_impl_le16_to_cpu(le2cpu16);

		imcp_vlan_config_info_data->imcp_vlan_info[index].dot11e_enabled = *buffer_ptr;
		buffer_ptr++;

		imcp_vlan_config_info_data->imcp_vlan_info[index].dot1p_p = *buffer_ptr;
		buffer_ptr++;

		imcp_vlan_config_info_data->imcp_vlan_info[index].dot11e_category = *buffer_ptr;
		buffer_ptr++;

		len = *buffer_ptr;
		buffer_ptr++;
		
		err_code = validate_vlan_essid_length(len, &validation_report);
		if(err_code)
		{
			goto validation_failed;
		}
		memcpy(imcp_vlan_config_info_data->imcp_vlan_info[index].essid, buffer_ptr, len);
		imcp_vlan_config_info_data->imcp_vlan_info[index].essid[len] = 0;
		buffer_ptr += len;

		memcpy(&le2cpu, buffer_ptr, sizeof(unsigned int));
		buffer_ptr += sizeof(unsigned int);
		imcp_vlan_config_info_data->imcp_vlan_info[index].rts_threshold = al_impl_le32_to_cpu(le2cpu);

		memcpy(&le2cpu, buffer_ptr, sizeof(unsigned int));
		buffer_ptr += sizeof(unsigned int);
		imcp_vlan_config_info_data->imcp_vlan_info[index].frag_threshold = al_impl_le32_to_cpu(le2cpu);

		memcpy(&le2cpu, buffer_ptr, sizeof(unsigned int));
		buffer_ptr += sizeof(unsigned int);
		imcp_vlan_config_info_data->imcp_vlan_info[index].beacon_int = al_impl_le32_to_cpu(le2cpu);

		imcp_vlan_config_info_data->imcp_vlan_info[index].service_type = *buffer_ptr;
		buffer_ptr++;

		imcp_vlan_config_info_data->imcp_vlan_info[index].transmit_power = *buffer_ptr;
		buffer_ptr++;

		memcpy(&le2cpu, buffer_ptr, sizeof(unsigned int));
		buffer_ptr += sizeof(unsigned int);
		imcp_vlan_config_info_data->imcp_vlan_info[index].txrate = al_impl_le32_to_cpu(le2cpu);

		buffer_ptr++;            //Here the type is skipped

		imcp_vlan_config_info_data->imcp_vlan_info[index].security_info._security_none.enabled = *buffer_ptr;
		buffer_ptr++;

		buffer_ptr++;            //Here the type is skipped

		imcp_vlan_config_info_data->imcp_vlan_info[index].security_info._security_wep.enabled = *buffer_ptr;
		buffer_ptr++;

		if (imcp_vlan_config_info_data->imcp_vlan_info[index].security_info._security_wep.enabled)
		{
			imcp_vlan_config_info_data->imcp_vlan_info[index].security_info._security_wep.strength = *buffer_ptr;
			buffer_ptr++;

			imcp_vlan_config_info_data->imcp_vlan_info[index].security_info._security_wep.key_index = *buffer_ptr;
			buffer_ptr++;

			key_length = *buffer_ptr;
			buffer_ptr++;

			//TODO #define Strenth type as 40 and 104
			if (imcp_vlan_config_info_data->imcp_vlan_info[index].security_info._security_wep.strength == 40)
			{
				for (i = 0; i < AL_802_11_MAX_SECURITY_KEYS; i++)
				{
					memcpy(&imcp_vlan_config_info_data->imcp_vlan_info[index].security_info._security_wep.keys[i].bytes,
							buffer_ptr,
							5);
					buffer_ptr += 5;
				}
			}
			else
			{
				memcpy(&imcp_vlan_config_info_data->imcp_vlan_info[index].security_info._security_wep.keys[0].bytes,
						buffer_ptr,
						13);
				buffer_ptr += 13;
			}
		}

		buffer_ptr++;             //Here the type is skipped ..PSK security reading

		imcp_vlan_config_info_data->imcp_vlan_info[index].security_info._security_rsn_psk.enabled = *buffer_ptr;
		buffer_ptr++;

		if (imcp_vlan_config_info_data->imcp_vlan_info[index].security_info._security_rsn_psk.enabled)
		{
			imcp_vlan_config_info_data->imcp_vlan_info[index].security_info._security_rsn_psk.mode = *buffer_ptr;
			buffer_ptr++;

			len = *buffer_ptr;
			buffer_ptr++;
			memset(imcp_vlan_config_info_data->imcp_vlan_info[index].security_info._security_rsn_psk.key_buffer,
					0,
					512);
			ret = al_encode_key(AL_CONTEXT(char *) buffer_ptr,
					len,
					_MAC_ADDRESS,
					imcp_vlan_config_info_data->imcp_vlan_info[index].security_info._security_rsn_psk.key_buffer);

			imcp_vlan_config_info_data->imcp_vlan_info[index].security_info._security_rsn_psk.key_buffer_len = ret;
			buffer_ptr += len;

			imcp_vlan_config_info_data->imcp_vlan_info[index].security_info._security_rsn_psk.cipher_tkip = *buffer_ptr;
			buffer_ptr++;

			imcp_vlan_config_info_data->imcp_vlan_info[index].security_info._security_rsn_psk.cipher_ccmp = *buffer_ptr;
			buffer_ptr++;

			memcpy(&le2cpu32, buffer_ptr, 4);
			buffer_ptr += 4;
			imcp_vlan_config_info_data->imcp_vlan_info[index].security_info._security_rsn_psk.group_key_renewal = al_impl_le32_to_cpu(le2cpu32);
		}

		buffer_ptr++;            //The type is skipped here ..The Radius security reading

		imcp_vlan_config_info_data->imcp_vlan_info[index].security_info._security_rsn_radius.enabled = *buffer_ptr;
		buffer_ptr++;

		if (imcp_vlan_config_info_data->imcp_vlan_info[index].security_info._security_rsn_radius.enabled)
		{
			imcp_vlan_config_info_data->imcp_vlan_info[index].security_info._security_rsn_radius.mode = *buffer_ptr;
			buffer_ptr++;

			memcpy(imcp_vlan_config_info_data->imcp_vlan_info[index].security_info._security_rsn_radius.radius_server, buffer_ptr, 4);
			buffer_ptr += 4;

			memcpy(&le2cpu, buffer_ptr, sizeof(unsigned int));
			buffer_ptr += sizeof(unsigned int);
			imcp_vlan_config_info_data->imcp_vlan_info[index].security_info._security_rsn_radius.radius_port = al_impl_le32_to_cpu(le2cpu);

			len = *buffer_ptr;
			buffer_ptr++;
			memset(&imcp_vlan_config_info_data->imcp_vlan_info[index].security_info._security_rsn_radius.radius_secret_key,
					0,
					512);
			ret = al_encode_key(AL_CONTEXT(char *) buffer_ptr,
					len,
					_MAC_ADDRESS,
					imcp_vlan_config_info_data->imcp_vlan_info[index].security_info._security_rsn_radius.radius_secret_key);
			imcp_vlan_config_info_data->imcp_vlan_info[index].security_info._security_rsn_radius.radius_secret_key_len = ret;
			buffer_ptr += len;

			imcp_vlan_config_info_data->imcp_vlan_info[index].security_info._security_rsn_radius.cipher_tkip = *buffer_ptr;
			buffer_ptr++;

			imcp_vlan_config_info_data->imcp_vlan_info[index].security_info._security_rsn_radius.cipher_ccmp = *buffer_ptr;
			buffer_ptr++;

			memcpy(&le2cpu32, buffer_ptr, sizeof(unsigned int));
			buffer_ptr += sizeof(unsigned int);
			imcp_vlan_config_info_data->imcp_vlan_info[index].security_info._security_rsn_radius.group_key_renewal = al_impl_le32_to_cpu(le2cpu32);
		}
	}
	
	err_code = validate_vlan_config_info(&validation_report);
	if(err_code)
	{
		goto validation_failed;
	}

	al_conf_handle = libalconf_read_config_data();

	if (al_conf_handle > 0)
	{
		EXECUTE_PRE_FS_CHANGE
			set_mesh_config_sqnr(increment_config_sqnr(al_conf_handle));

		write_vlan_config_info(al_conf_handle);
		EXECUTE_POST_FS_CHANGE(POST_FS_PARAM_TYPE_CONFIG)
			al_conf_close(AL_CONTEXT al_conf_handle);
	}

validation_failed:
	free(imcp_vlan_config_info_data->imcp_vlan_info);
	imcp_vlan_config_info_data->imcp_vlan_info = NULL;

	send_config_updated_response(client_addr, request_id, TYPE_AP_VLAN_CONFIG_REQUEST, err_code, &validation_report);
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit: %s: %d\n",__func__,__LINE__);
	return 0;
}


int imcp_reset_helper(unsigned char *buffer, struct sockaddr_in *client_addr)
{
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter: %s: %d\n",__func__,__LINE__);
	unsigned short node_id;
	unsigned short node_len;
	unsigned char *buffer_ptr = buffer + get_imcp_signature_mesh_id_size();

	IMCP_MSG_PROCESS_RECEIVE_ID_AND_LEN(buffer_ptr,node_id,node_len);   

	memcpy(imcp_reset_data->mac_address, buffer_ptr, 6);
	buffer_ptr += 6;

	imcp_reset_data->reset_type = *buffer_ptr;


	if (memcmp(imcp_reset_data->mac_address, _MAC_ADDRESS, 6))
	{
		al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : IMCP_RESET Packet ignored Packet ignored: %s : %d\n",__func__,__LINE__);
		return 0;
	}

	if (imcp_reset_data->reset_type == RESET_TYPE_REBOOT_SYSTEM)
	{
		al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"CONFIGD : INFO : IMCP_RESET Packet, Rebooting Mesh : %s : %d\n",__func__,__LINE__);
		EXECUTE_MESHD_REBOOT
	}
	else if (imcp_reset_data->reset_type == RESET_TYPE_RESTART_MESH)
	{
		al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"CONFIGD : INFO : IMCP_RESET Packet, Reseting Mesh : %s : %d\n",__func__,__LINE__);
		EXECUTE_MESHD_STOP
			EXECUTE_MESHD_CONFIGURE
			EXECUTE_MESHD_START
	}
	else if (imcp_reset_data->reset_type == RESET_TYPE_HALT_MESH)
   	{
		al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"CONFIGD : INFO : IMCP_RESET_TYPE_HALT_MESH received : %s : %d\n",__func__,__LINE__);
		EXECUTE_MESHD_HALT
	}
	else
	{
		al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"CONFIGD : INFO : IMCP_RESET Packet : %s : %d\n",__func__,__LINE__);
	}

	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit: %s: %d\n",__func__,__LINE__);
	return 0;
}

/*
Returns:
	0: fw_upgrade not in process
	1: fw_upgrade in process
*/
int get_upgrade_status()
{
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter: %s: %d\n",__func__,__LINE__);
	return g_fw_upgrade_status;
}

static void fw_upgrade_error_handler(int sig, siginfo_t *si, void *uc)
{
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter: %s: %d\n",__func__,__LINE__);
	pthread_t upgrd_tid;
	int ret;
	struct list_head *position, *queue;
	struct imcp_sw_update_data_addr *addr_data_buf = NULL;

	if(!list_empty(&imcp_sysupgrade_request))
	{
		list_for_each_safe(position, queue, &imcp_sysupgrade_request)
		{
			addr_data_buf = list_entry(position, imcp_sw_update_data_addr_t, list);
			if(addr_data_buf != NULL)
			{
				free(((struct imcp_sw_update_data_addr *)addr_data_buf)->buffer);
				list_del(position);
				free(addr_data_buf);
			}
		}
	}

	g_fw_upgrade_status = 0;
	system("killall scp");
	system("killall dropbearkey");
	system("killall sysupgrade");

	pthread_cancel(prev_tid);

	if(g_client_addr != NULL)
	{
		imcp_sw_update_response_data-> action = FW_UPGRADE_TIMEOUT;
		send_sw_response_packet(imcp_sw_update_response_data-> action, g_client_addr);
	}

	g_client_addr = NULL;
	free(imcp_sw_update_response_data);
	imcp_sw_update_response_data = NULL;
	signal(sig, SIG_IGN);

	ret = pthread_create(&upgrd_tid, NULL, imcp_sw_update_request_helper_thread, NULL);
	if(ret)
	{
		al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : Cannot create Fw upgrade thread: %s: %d\n",__func__,__LINE__);
		perror("pthread_create(): Cannot create Fw upgrade thread");
		return;
	}
	prev_tid = upgrd_tid;
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit: %s: %d\n",__func__,__LINE__);
}

int imcp_sw_update_request_helper(unsigned char *buffer, struct sockaddr_in *client_addr, int pkt_type, int buffer_length)
{
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter: %s: %d\n",__func__,__LINE__);
	struct imcp_sw_update_data_addr *thread_arg;

	thread_arg = (struct imcp_sw_update_data_addr *)malloc(sizeof(struct imcp_sw_update_data_addr));
	if(thread_arg == NULL)
	{
		al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : Memory allocation failed func: %s: %d\n",__func__,__LINE__);
		perror("malloc()");
		return -1;
	}

	thread_arg->buffer = (unsigned char *)malloc(buffer_length);
	if(thread_arg->buffer == NULL)
	{
		al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : malloc():thread_arg-buffer Line>: %s line: %d\n",__func__,__LINE__);
		perror("malloc():thread_arg-buffer");
		return -1;
	}

	memcpy(thread_arg->buffer, buffer, buffer_length);
	memcpy(&thread_arg->sysupgrd_socket, client_addr, sizeof(struct sockaddr_in));
	thread_arg->pkt_type = pkt_type;

	list_add_tail(&thread_arg->list, &imcp_sysupgrade_request);
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit: %s: %d\n",__func__,__LINE__);
	return 0;
}

int *imcp_sw_update_request_helper_thread(void *data)
{
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter: %s: %d\n",__func__,__LINE__);
	unsigned char tmp_buf[512];
	unsigned char *buffer;
	struct sockaddr_in *client_addr;

	char cmd_buf[512] = {0};
	char img_path[512] = {0};
	char user_name[32] = {0};
	char path_len = 0, username_len = 0;
	char *scp_ip = NULL;
	int k = 0;
	char board_mac[6] = {0};

	timer_t timerid;
	struct sigevent sev;
	struct itimerspec its;
	long long freq_nanosecs;
	sigset_t mask;
	struct sigaction sa;
	enum actions update_action;
	int retval;
	struct imcp_sw_update_data_addr *addr_data_buf = NULL;
	struct list_head *position, *queue; 

	retval = pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, NULL);
	if(retval)
	{
		perror("pthread_setcancelstate()");
	}
	
	retval = pthread_setcanceltype(PTHREAD_CANCEL_ASYNCHRONOUS, NULL);
	if(retval)
	{
		perror("pthread_setcanceltype()");
	}

	sa.sa_flags = SA_SIGINFO;
	sa.sa_sigaction = fw_upgrade_error_handler;
	sigemptyset(&sa.sa_mask);
	if (sigaction(SIG, &sa, NULL) == -1)
	{
		al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : timer_create: %s: %d\n",__func__,__LINE__);
			return -1;
	}
	sev.sigev_notify = SIGEV_SIGNAL;
	sev.sigev_signo = SIG;
	sev.sigev_value.sival_ptr = &timerid;
/*
		1. Start the Timer to check if any process stuck in infinite loop
					2.	Firmware Upgrade steps:
						i.		NMS Generates IMCP message with FW_KEY action
						ii.	Device Generates Public and Private key using dropbearkey and Send Public key to NMS using 
							 	action FW_SET_KEY
						iii.	NMS sends user name and Path of SysUpgradable Image using action FW_UPGRADE_INIT
						iv.	Device generates IMCP message with action FW_UPGRADE_STARTING.
								Copies Upgradable Image using ssh:
									- If failed Sends message with action FW_DOWNLOAD_FAILED
						v.		Device executes sysupgrade command to start firmware Upgradation
									- If failed sends message with action FW_UPGRADE_FAILED
						Note: If NMS does not receive any response from device, then NMS should take appropriate actions by maintaining some timeouts to recover
*/
		
	while(1)
	{
		if(!list_empty(&imcp_sysupgrade_request))
		{
			list_for_each_safe(position, queue, &imcp_sysupgrade_request)
			{
			if (timer_create(CLOCKID, &sev, &timerid) == -1)
				{
					al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"CONFIGD : INFO : timer_create : %s: %d\n",__func__,__LINE__);
					continue;
				}	
			memset(&its, 0, sizeof(its));
			its.it_value.tv_sec = UPGRADE_TIMEOUT;
				addr_data_buf = list_entry(position, imcp_sw_update_data_addr_t, list);
	 			if(addr_data_buf)
	 			{
		 			switch(addr_data_buf->pkt_type)
		 			{
			 			case IMCP_SW_UPDATE_REQUEST:
							buffer = ((struct imcp_sw_update_data_addr *)addr_data_buf)->buffer;
							client_addr = &((struct imcp_sw_update_data_addr *)addr_data_buf)->sysupgrd_socket;

							g_client_addr = client_addr;
							buffer += get_imcp_signature_mesh_id_size() + 2 /*Node id*/ + 2 /*Node len*/;

							memcpy(board_mac, buffer, 6);
							if (memcmp(board_mac, _MAC_ADDRESS, 6))
							{
								al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : IMCP_RESET Packet ignored Packet ignored: %s : %d\n",__func__,__LINE__);
								return 0;
							}

							buffer += 6;

					 		update_action = *buffer;
					 		buffer += 1;

					 		imcp_sw_update_response_data = (imcp_sw_update_response_t *)malloc(sizeof(imcp_sw_update_response_t));
					 		if(imcp_sw_update_response_data == NULL)
					 		{
								al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : Fatal: Malloc failed for fw_upgrade : %s: %d\n",__func__,__LINE__);
						 		break;
					 		}

			if (timer_settime(timerid, 0, &its, NULL) == -1)			//Starts timer
					 		{
								al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : fw_upgrade: timer_settime : %s: %d\n",__func__,__LINE__);
						 		errResponse("fw_upgrade: timer_settime");
						 		break;
					 		}

			switch(update_action)
			{
					 			case FW_GET_KEY:								//Key generate request from NMS with action FW_GET_KEY
									system("rm -rf /tmp/pubkey");			//Delete if already existing
					sprintf(cmd_buf, "dropbearkey -t rsa -y -f /etc/dropbear/dropbear_rsa_host_key | grep ssh-rsa > /tmp/pubkey");
					al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"CONFIGD : INFO : COMMAND: %s : %s: %d\n", cmd_buf,__func__,__LINE__);
/*
									Public Key: /tmp/pubkey
									Private Key:/etc/dropbear/dropbear_rsa_host_key
*/
					if((system(cmd_buf)) != 0)
					{
						timer_delete(timerid);
						perror("\ndropbearkey");
						al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : dropbearkey  : %s: %d\n",__func__,__LINE__);
						imcp_sw_update_response_data-> action = FW_UPGRADE_FAILED;
										send_sw_response_packet(imcp_sw_update_response_data-> action, client_addr);
								 		errResponse("fw_upgrade: timer_settime");
								 		break;
							 		}
 
					timer_delete(timerid);

					imcp_sw_update_response_data->action = FW_SET_KEY;
							 		send_sw_response_packet(imcp_sw_update_response_data-> action, client_addr);	//Public key is sent
					break;

							 	case FW_UPGRADE_INIT:					// NMS sends username and path to download firmware image

					/* Removing existing files if any*/	
					sprintf(cmd_buf, "rm -rf %s ; mkdir %s", FIRMWARE_PATH, FIRMWARE_PATH);				
					system(cmd_buf);

					username_len = (int)*buffer;						//Extracts username len
					buffer = buffer + sizeof(username_len);

					memcpy(user_name, buffer, username_len);		// Extracts username
					buffer = buffer + username_len;

					path_len = (int)*buffer;							// Extracts firmware path length
					buffer = buffer + sizeof(path_len);

					memcpy(img_path, buffer, path_len);				// Extracts firmware path
					buffer = buffer + path_len;

					imcp_sw_update_response_data-> action = FW_UPGRADE_STARTING;
								 	send_sw_response_packet(imcp_sw_update_response_data-> action, client_addr);	//Device sends message to NMS indicating Update started

								 	scp_ip = inet_ntoa(client_addr->sin_addr);
									al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"CONFIGD : INFO : ip: %s FW_Upgrade: \n\tUser Name:%s\n\tFirmware Path:%s\n: %s: %d\n",scp_ip,user_name, img_path,__func__,__LINE__);

						sprintf(cmd_buf, "ssh -y -i /etc/dropbear/dropbear_rsa_host_key  %s\@%s \"cat %s\" > %s/firmware.bin", \
							user_name, scp_ip, img_path, FIRMWARE_PATH);

					al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"CONFIGD : INFO : COMMAND:\t%s\n : %s: %d\n",cmd_buf,__func__,__LINE__);

					if((system(cmd_buf)) != 0)
               {
						timer_delete(timerid);
                  perror("\nfw_upgrade: ssh");
					al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : fw_upgrade: ssh : %s: %d\n",__func__,__LINE__);

						imcp_sw_update_response_data-> action = FW_DOWNLOAD_FAILED;
						send_sw_response_packet(imcp_sw_update_response_data-> action, &client_addr);
						g_fw_upgrade_status = 0;
									 	errResponse("fw_upgrade: timer_settime");
									 	break;
               }

					sprintf(cmd_buf, "sysupgrade %s/firmware.bin", FIRMWARE_PATH); // make a macro for complete path
					al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"CONFIGD : INFO : COMMAND:\t%s\n : %s: %d\n",cmd_buf,__func__,__LINE__);

								 g_fw_upgrade_status = 1;

					if((system(cmd_buf)) != 0)
               {	
						timer_delete(timerid);
                  perror("\nfw_upgrade: sysupgrade");
					al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : fw_upgrade : sysupgrade: ssh : %s: %d\n",__func__,__LINE__);

						imcp_sw_update_response_data-> action = FW_UPGRADE_FAILED;
						send_sw_response_packet(imcp_sw_update_response_data-> action, &client_addr);

						g_fw_upgrade_status = 0;
									 errResponse("fw_upgrade: timer_settime");
									 break;
               }

					timer_delete(timerid);
								 g_fw_upgrade_status = 0;
					break;

					default:
						timer_delete(timerid);

						imcp_sw_update_response_data->action = FW_UNKNOWN_ACTION;			// This case should not hit. NMS should send correct action
                  send_sw_response_packet(imcp_sw_update_response_data->action, &client_addr);
						al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : fw_upgrade: Unknown Action Received : %s: %d\n",__func__,__LINE__);

			}

							g_client_addr = NULL;
							if(imcp_sw_update_response_data != NULL)
							{
			free(imcp_sw_update_response_data);
							}
							free(((struct imcp_sw_update_data_addr *)addr_data_buf)->buffer);
			timer_delete(timerid);
							list_del(position);
							free(addr_data_buf);
							addr_data_buf = NULL;
			break;
						default:	
							free(((struct imcp_sw_update_data_addr *)addr_data_buf)->buffer);
							list_del(position);
							free(addr_data_buf);
							addr_data_buf = NULL;
		 			} //switch(addr_data_buf->pkt_type ends
	 			} // if(addr_data_buf) ends
	}
		}
		sleep(2);
	}//while(1)
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit: %s: %d\n",__func__,__LINE__);
}

int send_mesh_key_update_response(struct sockaddr_in *client_addr, int response_code)
{
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter: %s: %d\n",__func__,__LINE__);
	unsigned char *buffer;
	unsigned char *buffer_ptr;
	int           sock, clilen;
	int           packet_size = get_imcp_signature_mesh_id_size() + 8 + IMCP_ID_LEN_SIZE;
	unsigned short node_id;
	unsigned short node_len;

	buffer = (unsigned char *)malloc(packet_size);

	buffer_ptr = buffer;

	buffer_ptr += copy_imcp_signature_packet_type_meshid(buffer_ptr, IMCP_GENERAL_RESPONSE);

	node_id = IMCP_MESH_IMCP_KEY_UPDATE_INFO_ID;
	node_len = 8;
	IMCP_MSG_PROCESS_SEND_ID_AND_LEN(buffer_ptr,node_id,node_len);

	memcpy(buffer_ptr, imcp_mesh_key_update_data->mac_address, 6);
	buffer_ptr += 6;

	memcpy(buffer_ptr, &response_code, 1);
	buffer_ptr += 1;

	memcpy(buffer_ptr, &imcp_mesh_key_update_data->response_id, 1);
	buffer_ptr += 1;

	send_packet(buffer, packet_size, client_addr);

	free(buffer);

	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit: %s: %d\n",__func__,__LINE__);
	return 0;
}


int imcp_mesh_key_update_helper(unsigned char *buffer, struct sockaddr_in *client_addr)
{
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter: %s: %d\n",__func__,__LINE__);
	int           al_conf_handle, ret;
	unsigned char *buffer_ptr = buffer + get_imcp_signature_mesh_id_size();
	unsigned char encoded_imcp_key_buffer[1024];
	unsigned short node_id;
	unsigned short node_len;

	al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"CONFIGD : INFO : IMCP_MESH_KEY_UPDATE Packet - Received: %s line: %d\n",__func__,__LINE__);

	IMCP_MSG_PROCESS_RECEIVE_ID_AND_LEN(buffer_ptr,node_id,node_len);

	memcpy(imcp_mesh_key_update_data->mac_address, buffer_ptr, 6);
	buffer_ptr += 6;

	if (memcmp(imcp_mesh_key_update_data->mac_address, _MAC_ADDRESS, 6))
	{
		al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : IMCP_MESH_KEY_UPDATE Packet ignored: %s: %d\n",__func__,__LINE__);
		return 0;
	}

	imcp_mesh_key_update_data->response_id = *buffer_ptr;
	buffer_ptr++;

	imcp_mesh_key_update_data->new_meshid_length = *buffer_ptr;
	buffer_ptr++;

	if (imcp_mesh_key_update_data->new_meshid_length > 0)
	{
		memcpy(imcp_mesh_key_update_data->new_meshid, buffer_ptr, imcp_mesh_key_update_data->new_meshid_length);
		imcp_mesh_key_update_data->new_meshid[imcp_mesh_key_update_data->new_meshid_length] = 0;
		buffer_ptr += imcp_mesh_key_update_data->new_meshid_length;
	}

	imcp_mesh_key_update_data->new_imcp_key_length = *buffer_ptr;
	buffer_ptr++;

	if (imcp_mesh_key_update_data->new_imcp_key_length > 0)
	{
		memcpy(imcp_mesh_key_update_data->new_imcp_key, buffer_ptr, imcp_mesh_key_update_data->new_imcp_key_length);
		imcp_mesh_key_update_data->new_imcp_key[imcp_mesh_key_update_data->new_imcp_key_length] = 0;
		buffer_ptr += imcp_mesh_key_update_data->new_imcp_key_length;
	}

#ifdef _CONFIGD_USE_ENCRYPTION_
	ret = al_encode_key(imcp_mesh_key_update_data->new_imcp_key,
			imcp_mesh_key_update_data->new_imcp_key_length,
			_MAC_ADDRESS,
			encoded_imcp_key_buffer);

	if (ret <= 0)
	{
		send_mesh_key_update_response(client_addr, 1);
		al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : failed encoding key: %s: %d\n",__func__,__LINE__);
		return -1;
	}

	al_conf_handle = libalconf_read_config_data();


	if (al_conf_handle > 0)
	{
		EXECUTE_PRE_FS_CHANGE

			al_conf_set_mesh_id(AL_CONTEXT                 al_conf_handle,
					imcp_mesh_key_update_data->new_meshid,
					imcp_mesh_key_update_data->new_meshid_length);

		al_conf_set_mesh_imcp_key_buffer(AL_CONTEXT al_conf_handle,
				encoded_imcp_key_buffer,
				ret);

		set_mesh_config_sqnr(increment_config_sqnr(al_conf_handle));
		al_conf_put(AL_CONTEXT al_conf_handle);
		al_conf_close(al_conf_handle);
		EXECUTE_POST_FS_CHANGE(POST_FS_PARAM_TYPE_CONFIG)
	}
	else
	{
		send_mesh_key_update_response(client_addr, 1);
		al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : reading configfile failed: %s: %d\n",__func__,__LINE__);
		return -1;
	}

	/*
	 *	S mesh key update response
	 */


	send_mesh_key_update_response(client_addr, 0);

	/*
	 *	Donot overwrite runtime copy of meshid and mesh key
	 *	key update will take effect after reboot
	 */

	/*
	 *      memset(mesh_id,0,sizeof(mesh_id));
	 *      memcpy(mesh_id,imcp_mesh_key_update_data->new_meshid,imcp_mesh_key_update_data->new_meshid_length);
	 *
	 *      mesh_id_length = imcp_mesh_key_update_data->new_meshid_length;
	 *
	 *      memset(mesh_id_key,0,sizeof(mesh_id_key));
	 *      memcpy(mesh_id_key,imcp_mesh_key_update_data->new_imcp_key,imcp_mesh_key_update_data->new_imcp_key_length);
	 */

#else
	send_mesh_key_update_response(client_addr, 2);
#endif
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit: %s: %d\n",__func__,__LINE__);
	return 0;
}


int imcp_configd_watchdog_request_helper(unsigned char *buffer, struct sockaddr_in *client_addr)
{
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter: %s: %d\n",__func__,__LINE__);
	unsigned char *response_buffer;
	unsigned char *response_buffer_ptr;
	unsigned short  node_id;
   	unsigned short  node_len;

	int packet_size = get_imcp_signature_mesh_id_size() + 6 + 8 + 4;

	response_buffer     = (unsigned char *)malloc(packet_size);
	response_buffer_ptr = response_buffer;

	response_buffer_ptr += copy_imcp_signature_packet_type_meshid(response_buffer_ptr, IMCP_CONFIGD_WATCHDOG_RESPONSE);

  	node_id = IMCP_CONFIGD_WATCHDOG_REQUEST_INFO_ID;
  	node_len = (unsigned short)(packet_size);
  	IMCP_MSG_PROCESS_SEND_ID_AND_LEN(response_buffer_ptr,node_id,node_len);


	memcpy(response_buffer_ptr, _MAC_ADDRESS, 6);
	response_buffer_ptr += 6;

	memcpy(response_buffer_ptr, buffer, 8);

	send_packet_ex(response_buffer, packet_size, client_addr, 0, 0);

	free(response_buffer);
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit: %s: %d\n",__func__,__LINE__);

	return 0;
}


int update_reg_domain_config_data(int al_conf_handle)
{
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter: %s: %d\n",__func__,__LINE__);
	int i;

	imcp_reg_domain_config_data->request_response_type = TYPE_RESPONSE;

	imcp_reg_domain_config_data->reg_domain = al_conf_get_regulatory_domain(al_conf_handle);
	al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"CONFIGD : INFO : reg_domain_info : %hd : %s: %d\n",imcp_reg_domain_config_data->reg_domain,__func__,__LINE__);

	imcp_reg_domain_config_data->country_code = al_conf_get_country_code(al_conf_handle);
	al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"CONFIGD : INFO : reg_domain_cc : %hd : %s: %d\n",imcp_reg_domain_config_data->country_code,__func__,__LINE__);

	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit: %s: %d\n",__func__,__LINE__);
	return 0;
}


int send_reg_domain_config_info_reply(struct sockaddr_in *client_addr, unsigned char response_id)
{
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter: %s: %d\n",__func__,__LINE__);
	int            sock, clilen, i, j;
	int            size, if_size;
	int            packet_size;
	unsigned char  buffer[1024];
	unsigned char  *buffer_ptr;
	unsigned int   cpu2le;
	unsigned short cpu2le16;
	int            ret;
	unsigned short node_id;
	unsigned short node_len;

	packet_size = get_imcp_signature_mesh_id_size() + 12 + IMCP_ID_LEN_SIZE;

	buffer_ptr = buffer;

	buffer_ptr += copy_imcp_signature_packet_type_meshid(buffer_ptr, IMCP_REG_DOMAIN_COUNTRY_CODE_INFO);

	node_id      = IMCP_REG_DOMAIN_REQ_RES_INFO_ID;
	node_len     = 12;
	IMCP_MSG_PROCESS_SEND_ID_AND_LEN(buffer_ptr,node_id,node_len);

		memcpy(buffer_ptr, imcp_reg_domain_config_data->mac_address, 6);
	buffer_ptr += 6;

	*buffer_ptr = imcp_reg_domain_config_data->request_response_type;
	buffer_ptr++;

	*buffer_ptr = response_id;
	buffer_ptr++;


	cpu2le16 = al_impl_cpu_to_le16(imcp_reg_domain_config_data->reg_domain);
	memcpy(buffer_ptr, &cpu2le16, 2);
	buffer_ptr += 2;

	cpu2le16 = al_impl_cpu_to_le16(imcp_reg_domain_config_data->country_code);
	memcpy(buffer_ptr, &cpu2le16, 2);
	buffer_ptr += 2;


	ret = send_packet(buffer, packet_size, client_addr);
	al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"CONFIGD : INFO : IMCP_AP_CONFIG Packet sent (Bytes = %d) : %s: %d\n",ret,imcp_reg_domain_config_data->country_code,__func__,__LINE__);
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit: %s: %d\n",__func__,__LINE__);

	return 0;
}


int imcp_reg_domain_country_code_request_response_helper(unsigned char *buffer, struct sockaddr_in *client_addr)
{
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter: %s: %d\n",__func__,__LINE__);
	int al_conf_handle;
	unsigned short node_id;
	unsigned short node_len;

	unsigned char *buffer_ptr = buffer + get_imcp_signature_mesh_id_size();

	al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"CONFIGD : INFO : IMCP_REG_DOMAIN_COUNTRY_CODE_REQUEST_RESPONSE Packet - Received: %s line: %d\n",__func__,__LINE__);

	memset(imcp_reg_domain_rr_data, 0, sizeof(imcp_reg_domain_rr_data_t));

	IMCP_MSG_PROCESS_RECEIVE_ID_AND_LEN(buffer_ptr,node_id,node_len);

	memcpy(imcp_reg_domain_rr_data->mac_address, buffer_ptr, 6);
	buffer_ptr += 6;

	imcp_reg_domain_rr_data->request_response_type = *buffer_ptr;
	buffer_ptr++;

	if (imcp_reg_domain_rr_data->request_response_type == TYPE_RESPONSE)
	{
		al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : IMCP_REG_DOMAIN_COUNTRY_CODE_REQUEST_RESPONSE Packet ignored: %s: %d\n",__func__,__LINE__);
		return 0;
	}

	if (memcmp(imcp_reg_domain_rr_data->mac_address, _MAC_ADDRESS, 6))
	{
		al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : IMCP_REG_DOMAIN_COUNTRY_CODE_REQUEST_RESPONSE Packet ignored: %s: %d\n",__func__,__LINE__);
		return 0;
	}

	imcp_reg_domain_rr_data->response_id = *buffer_ptr;
	buffer_ptr++;


	al_conf_handle = libalconf_read_config_data();

	if (al_conf_handle != -1)
	{
		memcpy(imcp_reg_domain_config_data->mac_address, imcp_reg_domain_rr_data->mac_address, 6);
		update_reg_domain_config_data(al_conf_handle);
		al_conf_close(AL_CONTEXT al_conf_handle);
		send_reg_domain_config_info_reply(client_addr, imcp_reg_domain_rr_data->response_id);
	}


	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit: %s: %d\n",__func__,__LINE__);
	return 0;
}


/*
 *	Send acknowledgement for ap configuration update
 */
int send_reg_domain_config_updated_reply(struct sockaddr_in *client_addr, unsigned char response_id)
{
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter: %s: %d\n",__func__,__LINE__);
	unsigned char *buffer;
	unsigned char *buffer_ptr;
	int           packet_size = get_imcp_signature_mesh_id_size() + 8 + IMCP_ID_LEN_SIZE;
	int           ret;
	unsigned short node_id;
	unsigned short node_len;

	buffer = (unsigned char *)malloc(packet_size);

	buffer_ptr = buffer;

	buffer_ptr += copy_imcp_signature_packet_type_meshid(buffer_ptr, IMCP_REG_DOMAIN_COUNTRY_CODE_REQUEST_RESPONSE);

	node_id      = IMCP_REG_DOMAIN_INFO_ID;
	node_len     = 8;
	IMCP_MSG_PROCESS_SEND_ID_AND_LEN(buffer_ptr,node_id,node_len);

		memcpy(buffer_ptr, imcp_apconfig_rr_data->mac_address, 6);
	buffer_ptr += 6;

	*buffer_ptr = TYPE_RESPONSE;
	buffer_ptr++;

	*buffer_ptr = response_id;
	buffer_ptr++;

	ret = send_packet(buffer, packet_size, client_addr);
	al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"CONFIGD : INFO : IMCP_REG_DOMAIN_COUNTRY_CODE_CONFIG_UPDATE_RESPONSE Packet sent (Bytes = %d) : %s: %d\n",ret,__func__,__LINE__);

	free(buffer);
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit: %s: %d\n",__func__,__LINE__);

	return 0;
}


int imcp_reg_domain_config_info_helper(unsigned char *buffer, struct sockaddr_in *client_addr)
{
	int            al_conf_handle, i;
	unsigned short le2cpu16;
	unsigned char  *buffer_ptr = buffer + get_imcp_signature_mesh_id_size();
	unsigned int   le2cpu;
	unsigned short node_id;
	unsigned short node_len;

	al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"CONFIGD : INFO : IMCP_REG_DOMAIN_COUNTRY_CODE_CONFIGURATION_INFO Packet - Received: %s line: %d\n",__func__,__LINE__);

	IMCP_MSG_PROCESS_RECEIVE_ID_AND_LEN(buffer_ptr,node_id,node_len);

		memcpy(imcp_reg_domain_config_data->mac_address, buffer_ptr, 6);
	buffer_ptr += 6;

	imcp_reg_domain_config_data->request_response_type = *buffer_ptr;
	buffer_ptr++;

	imcp_reg_domain_config_data->response_id = *buffer_ptr;
	buffer_ptr++;

	if (imcp_reg_domain_config_data->request_response_type == TYPE_RESPONSE)
	{
		al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : IMCP_REG_DOMAIN_CC_CONFIGURATION_INFO func:%s: %d\n",__func__,__LINE__);
		return 0;
	}

	if (memcmp(imcp_reg_domain_config_data->mac_address, _MAC_ADDRESS, 6))
	{
		al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : IMCP_REG_DOMAIN_CC_CONFIGURATION_INFO func:%s: %d\n",__func__,__LINE__);
		return 0;
	}


	memcpy(&le2cpu16, buffer_ptr, sizeof(unsigned short));
	buffer_ptr += sizeof(unsigned short);
	imcp_reg_domain_config_data->reg_domain = al_impl_le16_to_cpu(le2cpu16);

	memcpy(&le2cpu16, buffer_ptr, sizeof(unsigned short));
	buffer_ptr += sizeof(unsigned short);
	imcp_reg_domain_config_data->country_code = al_impl_le16_to_cpu(le2cpu16);

	al_conf_handle = libalconf_read_config_data();
	if (al_conf_handle > 0)
	{
		EXECUTE_PRE_FS_CHANGE
			set_mesh_config_sqnr(increment_config_sqnr(al_conf_handle));

		write_reg_domain_country_code_config_data(al_conf_handle);
		EXECUTE_POST_FS_CHANGE(POST_FS_PARAM_TYPE_CONFIG)
			al_conf_close(AL_CONTEXT al_conf_handle);
	}

	send_reg_domain_config_updated_reply(client_addr, imcp_reg_domain_config_data->response_id);
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit: %s: %d\n",__func__,__LINE__);

	return 0;
}


int update_ack_timeout_config_data(int al_conf_handle)
{
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter: %s: %d\n",__func__,__LINE__);
	int i;
	int if_no;

	imcp_ack_timeout_config_data->request_response_type = TYPE_RESPONSE;

	imcp_ack_timeout_config_data->interface_count = al_conf_get_if_count(al_conf_handle);

	for (if_no = 0; if_no < imcp_ack_timeout_config_data->interface_count; if_no++)
	{
		al_conf_get_if(al_conf_handle, if_no, &(imcp_ack_timeout_config_data->if_info[if_no]));
	}
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit: %s: %d\n",__func__,__LINE__);

	return 0;
}


/*
 *	Send ack_timeout configuration info to client
 */
int send_ack_timeout_config_info_reply(struct sockaddr_in *client_addr, unsigned char response_id)
{
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter: %s: %d\n",__func__,__LINE__);
	int           sock, clilen, i, j;
	int           size, if_size;
	int           packet_size;
	unsigned char buffer[1024];
	unsigned char *buffer_ptr;
	unsigned int  cpu2le;
	int           ret;
	int           if_name_length;
	unsigned short  node_id;
   	unsigned short  node_len;

	if_size = 6 + 1 + 1 + 1 + 4;

	for (i = 0; i < imcp_ack_timeout_config_data->interface_count; i++)
	{
		if_size += 1;

		if_name_length = strlen(imcp_ack_timeout_config_data->if_info[i].name);

		if_size += if_name_length;

		if_size += 1;
	}

	packet_size = get_imcp_signature_mesh_id_size() + if_size;

	buffer_ptr = buffer;

	buffer_ptr += copy_imcp_signature_packet_type_meshid(buffer_ptr, IMCP_ACK_TIMEOUT_INFO);

  	node_id = IMCP_ACK_TIMEOUT_REQ_RES_INFO_ID;
  	node_len = (unsigned short)(packet_size);
  	IMCP_MSG_PROCESS_SEND_ID_AND_LEN(buffer_ptr,node_id,node_len);



	memcpy(buffer_ptr, imcp_ack_timeout_config_data->mac_address, 6);

	buffer_ptr += 6;

	*buffer_ptr = imcp_ack_timeout_config_data->request_response_type;
 	buffer_ptr++;

	*buffer_ptr = response_id;
	buffer_ptr++;

	*buffer_ptr = imcp_ack_timeout_config_data->interface_count;
	buffer_ptr++;

	if (imcp_ack_timeout_config_data->interface_count > 0)
	{
		for (i = 0; i < imcp_ack_timeout_config_data->interface_count; i++)
		{
			if_name_length = strlen(imcp_ack_timeout_config_data->if_info[i].name);

			*buffer_ptr = if_name_length;
			buffer_ptr++;

			memcpy(buffer_ptr, imcp_ack_timeout_config_data->if_info[i].name, if_name_length);
			buffer_ptr += if_name_length;

			*buffer_ptr = imcp_ack_timeout_config_data->if_info[i].ack_timeout;
			buffer_ptr++;
		}
	}


	ret = send_packet(buffer, packet_size, client_addr);
	al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"CONFIGD : INFO : IMCP_ack_timeout_CONFIG Packet sent (Bytes = %d) : %s: %d\n",ret,__func__,__LINE__);
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit: %s: %d\n",__func__,__LINE__);

	return 0;
}


int imcp_ack_timeout_request_response_helper(unsigned char *buffer, struct sockaddr_in *client_addr)
{
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter: %s: %d\n",__func__,__LINE__);
	int al_conf_handle;
	unsigned short node_id;
   	unsigned short node_len;


	unsigned char *buffer_ptr = buffer + get_imcp_signature_mesh_id_size();

	al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"CONFIGD : INFO : IMCP_ACK_TIMEOUT_REQUEST_RESPONSE Packet - Received: %s line: %d\n",__func__,__LINE__);

	   IMCP_MSG_PROCESS_RECEIVE_ID_AND_LEN(buffer_ptr,node_id,node_len);/*MACRO for copy node_id & node_len*/


	memset(imcp_ack_timeout_rr_data, 0, sizeof(imcp_ack_timeout_rr_data_t));

	memcpy(imcp_ack_timeout_rr_data->mac_address, buffer_ptr, 6);
	buffer_ptr += 6;

	imcp_ack_timeout_rr_data->request_response_type = *buffer_ptr;
	buffer_ptr++;

	if (imcp_ack_timeout_rr_data->request_response_type == TYPE_RESPONSE)
	{
		al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : IMCP_ACK_TIMEOUT_REQUEST_RESPONSE Packet ignored: %s: %d\n",__func__,__LINE__);
		return 0;
	}

	if (memcmp(imcp_ack_timeout_rr_data->mac_address, _MAC_ADDRESS, 6))
	{
			al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : IMCP_ACK_TIMEOUT_REQUEST_RESPONSE Packet ignored: %s: %d\n",__func__,__LINE__);
		return 0;
	}

	imcp_ack_timeout_rr_data->response_id = *buffer_ptr;
	buffer_ptr++;


	al_conf_handle = libalconf_read_config_data();

	if (al_conf_handle != -1)
	{
		memcpy(imcp_ack_timeout_config_data->mac_address, imcp_ack_timeout_rr_data->mac_address, 6);
		update_ack_timeout_config_data(al_conf_handle);
		al_conf_close(AL_CONTEXT al_conf_handle);
		send_ack_timeout_config_info_reply(client_addr, imcp_ack_timeout_rr_data->response_id);
	}
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit: %s: %d\n",__func__,__LINE__);
	return 0;
}


/*
 *	S acknowledgement for ack_timeout configuration update
 */
int send_ack_timeout_config_updated_reply(struct sockaddr_in *client_addr, unsigned char response_id)
{
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter: %s: %d\n",__func__,__LINE__);
	unsigned char *buffer;
	unsigned char *buffer_ptr;
	unsigned short  node_id;
        unsigned short  node_len;
	int           packet_size = get_imcp_signature_mesh_id_size() + 8 + 4;
	int           ret;

	buffer = (unsigned char *)malloc(packet_size);

	buffer_ptr = buffer;

	buffer_ptr += copy_imcp_signature_packet_type_meshid(buffer_ptr, IMCP_ACK_TIMEOUT_REQUEST_RESPONSE);

        node_id = IMCP_ACK_TIMEOUT_INFO_ID;
        node_len = (unsigned short)(packet_size);
        IMCP_MSG_PROCESS_SEND_ID_AND_LEN(buffer_ptr,node_id,node_len);


	memcpy(buffer_ptr, imcp_ack_timeout_rr_data->mac_address, 6);

	buffer_ptr += 6;

	*buffer_ptr = TYPE_RESPONSE;
	buffer_ptr++;

	*buffer_ptr = response_id;
	buffer_ptr++;

	ret = send_packet(buffer, packet_size, client_addr);
	al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"CONFIGD : INFO : IMCP_ACK_TIMEOUT_CONFIG_UPDATE_RESPONSE (Bytes = %d) : %s: %d\n",ret,__func__,__LINE__);

	free(buffer);
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit: %s: %d\n",__func__,__LINE__);

	return 0;
}


int imcp_ack_timeout_config_info_helper(unsigned char *buffer, struct sockaddr_in *client_addr)
{
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter: %s: %d\n",__func__,__LINE__);
	int               al_conf_handle, i, if_no, length;
	int               dot11e_conf_handle;
	unsigned short    le2cpu16;
	unsigned char     *buffer_ptr = buffer + get_imcp_signature_mesh_id_size();
	unsigned int      le2cpu;
	unsigned short    node_id;   
   	unsigned short    node_len;  
	al_conf_if_info_t *if_info;

	al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"CONFIGD : INFO : IMCP_ACK_TIMEOUT_CONFIGURATION_INFO Packet - Received: %s line: %d\n",__func__,__LINE__);
	
	   IMCP_MSG_PROCESS_RECEIVE_ID_AND_LEN(buffer_ptr,node_id,node_len);


	memcpy(imcp_ack_timeout_config_data->mac_address, buffer_ptr, 6);

	buffer_ptr += 6;

	imcp_ack_timeout_config_data->request_response_type = *buffer_ptr;
	buffer_ptr++;

	imcp_ack_timeout_config_data->response_id = *buffer_ptr;
	buffer_ptr++;

	if (imcp_ack_timeout_config_data->request_response_type == TYPE_RESPONSE)
	{
		al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : IMCP_ACK_TIMEOUT_CONFIGURATION_INFO Packet ignored: %s: %d\n",__func__,__LINE__);
		return 0;
	}

	if (memcmp(imcp_ack_timeout_config_data->mac_address, _MAC_ADDRESS, 6))
	{
		al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : IMCP_ACK_TIMEOUT_CONFIGURATION_INFO Packet ignored: %s: %d\n",__func__,__LINE__);
		return 0;
	}

	imcp_ack_timeout_config_data->interface_count = *buffer_ptr;
	buffer_ptr++;


	if (imcp_ack_timeout_config_data->interface_count <= 0)
	{
		al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : Invalid interface count: %s: %d\n",__func__,__LINE__);
		return -1;
	}

	memset(imcp_ack_timeout_config_data->if_info, 0, sizeof(al_conf_if_info_t) * CURRENT_EXPECTED_MAX_IF_COUNT);

	for (if_no = 0; if_no < imcp_ack_timeout_config_data->interface_count; if_no++)
	{
		if_info = &imcp_ack_timeout_config_data->if_info[if_no];

		length = *buffer_ptr;
		buffer_ptr++;

		if (length > 0)
		{
			memcpy(if_info->name, buffer_ptr, length);
			if_info->name[length] = 0;
			buffer_ptr           += length;
		}

		if_info->ack_timeout = *buffer_ptr;
		buffer_ptr++;
	}

	al_conf_handle = libalconf_read_config_data();

	if (al_conf_handle > 0)
	{
		EXECUTE_PRE_FS_CHANGE
			set_mesh_config_sqnr(increment_config_sqnr(al_conf_handle));

		write_ack_timeout_config_data(al_conf_handle);
		EXECUTE_POST_FS_CHANGE(POST_FS_PARAM_TYPE_CONFIG)
			al_conf_close(AL_CONTEXT al_conf_handle);
	}

	//EXECUTE_MESHD_START

	dot11e_conf_handle = read_dot11e_category_data();

	if (dot11e_conf_handle == -1)
	{
		al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : Could not read dot11e configuration: %s: %d\n",__func__,__LINE__);
		return 0;
	}

	al_conf_handle = libalconf_read_config_data();

	//EXECUTE_MESHD_CONFIGURE
	init_configurations(al_conf_handle, dot11e_conf_handle, 1, 0);

	al_conf_close(AL_CONTEXT al_conf_handle);
	dot11e_conf_close(AL_CONTEXT dot11e_conf_handle);

	send_ack_timeout_config_updated_reply(client_addr, imcp_ack_timeout_config_data->response_id);
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit: %s: %d\n",__func__,__LINE__);

	return 0;
}


int imcp_restore_defaults_request_response_helper(unsigned char *buffer, struct sockaddr_in *client_addr)
{
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter: %s: %d\n",__func__,__LINE__);
	unsigned char mac_address[6];
	char          command[1024];
	unsigned short node_id;
	unsigned short node_len;

	unsigned char *buffer_ptr = buffer + get_imcp_signature_mesh_id_size();

	FILE *fp;

	memset(command, 0, sizeof(command));

	IMCP_MSG_PROCESS_RECEIVE_ID_AND_LEN(buffer_ptr,node_id,node_len);

	memset(mac_address, 0, sizeof(mac_address));
	memcpy(mac_address, buffer_ptr, 6);
	buffer_ptr += 6;

	if (memcmp(mac_address, _MAC_ADDRESS, 6))
	{
			al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : IMCP_RESTORE_DEFAULTS Packet ignored: %s: %d\n",__func__,__LINE__);
			return 0;
		}

	al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"CONFIGD : INFO : IMCP_RESTORE_DEFAULTS_REQUEST Packet - Received : %s: %d\n",__func__,__LINE__);

	al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"CONFIGD : INFO : Restoring configuration.. : %s: %d\n",__func__,__LINE__);


	system("chmod +x /root/restore.sh");
	system("/root/restore.sh");

	EXECUTE_POST_FS_CHANGE(POST_FS_PARAM_TYPE_CONFIG)
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit: %s: %d\n",__func__,__LINE__);

	return 0;
}


/**
 *	Send acknowledgement for hide_ssid configuration update
 */
int send_hide_ssid_config_updated_reply(struct sockaddr_in *client_addr, unsigned char response_id)
{
    al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter: %s: %d\n",__func__,__LINE__);
	unsigned char *buffer;
	unsigned char *buffer_ptr;
	int           packet_size = get_imcp_signature_mesh_id_size() + 9 + IMCP_ID_LEN_SIZE;
	int           ret;
	unsigned short node_id;
	unsigned short node_len;

	buffer = (unsigned char *)malloc(packet_size);

	buffer_ptr = buffer;

	buffer_ptr += copy_imcp_signature_packet_type_meshid(buffer_ptr, IMCP_CONFIG_REQUEST_RESPONSE);

	node_id      = IMCP_HIDE_SSID_CONFIG_INFO_ID;
	node_len     = 9; 
	IMCP_MSG_PROCESS_SEND_ID_AND_LEN(buffer_ptr, node_id, node_len);

	memcpy(buffer_ptr, _MAC_ADDRESS, 6);
	buffer_ptr += 6;

	*buffer_ptr = TYPE_RESPONSE;
	buffer_ptr++;

	*buffer_ptr = TYPE_IF_HIDDEN_SSID_REQUEST;
	buffer_ptr++;

	*buffer_ptr = response_id;
	buffer_ptr++;

	ret = send_packet(buffer, packet_size, client_addr);
	al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"CONFIGD : INFO : IMCP_HIDE_SSID_CONFIGURATION_INFO reply packet sent (Bytes = %d) : %s: %d\n",ret,__func__,__LINE__);

	free(buffer);
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit: %s: %d\n",__func__,__LINE__);

	return 0;
}


int imcp_hide_ssid_config_info_helper(unsigned char *buffer, struct sockaddr_in *client_addr)
{
    al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter: %s: %d\n",__func__,__LINE__);
	int               al_conf_handle, i, if_no, length;
	int               dot11e_conf_handle;
	unsigned short    le2cpu16;
	unsigned char     *buffer_ptr = buffer + get_imcp_signature_mesh_id_size();
	unsigned int      le2cpu;
	al_conf_if_info_t *if_info;
	unsigned short    node_id;
	unsigned short    node_len;

		al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"CONFIGD : INFO : IMCP_HIDE_SSID_CONFIGURATION_INFO Packet - Received: %s line: %d\n",__func__,__LINE__);

	IMCP_MSG_PROCESS_RECEIVE_ID_AND_LEN(buffer_ptr,node_id,node_len);

	memcpy(imcp_hide_ssid_config_data->mac_address, buffer_ptr, 6);
	buffer_ptr += 6;

	imcp_hide_ssid_config_data->request_response_type = *buffer_ptr;
	buffer_ptr++;

	imcp_hide_ssid_config_data->response_id = *buffer_ptr;
	buffer_ptr++;

	if (imcp_hide_ssid_config_data->request_response_type == TYPE_RESPONSE)
	{
		al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : IMCP_HIDE_SSID_CONFIGURATION_INFO Packet ignored: %s : %d\n",__func__,__LINE__);
		return 0;
	}

	if (memcmp(imcp_hide_ssid_config_data->mac_address, _MAC_ADDRESS, 6))
	{
		al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : IMCP_HIDE_SSID_CONFIGURATION_INFO Packet ignored: %s : %d\n",__func__,__LINE__);
		return 0;
	}

	imcp_hide_ssid_config_data->interface_count = *buffer_ptr;
	buffer_ptr++;


	if (imcp_hide_ssid_config_data->interface_count <= 0)
	{
		al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : Invalid interface count: %s: %d\n",__func__,__LINE__);
		return -1;
	}

	memset(imcp_hide_ssid_config_data->if_info, 0, sizeof(al_conf_if_info_t) * CURRENT_EXPECTED_MAX_IF_COUNT);

	for (if_no = 0; if_no < imcp_hide_ssid_config_data->interface_count; if_no++)
	{
		if_info = &imcp_hide_ssid_config_data->if_info[if_no];

		length = *buffer_ptr;
		buffer_ptr++;

		if (length > 0)
		{
			memcpy(if_info->name, buffer_ptr, length);
			if_info->name[length] = 0;
			buffer_ptr           += length;
		}

		if_info->hide_ssid = *buffer_ptr;
		buffer_ptr++;
	}

	al_conf_handle = libalconf_read_config_data();

	if (al_conf_handle > 0)
	{
		EXECUTE_PRE_FS_CHANGE
			set_mesh_config_sqnr(increment_config_sqnr(al_conf_handle));

		write_hide_ssid_config_data(al_conf_handle);
		EXECUTE_POST_FS_CHANGE(POST_FS_PARAM_TYPE_CONFIG)
			al_conf_close(AL_CONTEXT al_conf_handle);
	}

	//EXECUTE_MESHD_START

	dot11e_conf_handle = read_dot11e_category_data();

	if (dot11e_conf_handle == -1)
	{
		al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : Could not read dot11e configuration: %s line : %d\n",__func__,__LINE__);
		return 0;
	}

	al_conf_handle = libalconf_read_config_data();
	//EXECUTE_MESHD_CONFIGURE
	init_configurations(al_conf_handle, dot11e_conf_handle, 1, 0);


	al_conf_close(AL_CONTEXT al_conf_handle);
	dot11e_conf_close(AL_CONTEXT dot11e_conf_handle);

	send_hide_ssid_config_updated_reply(client_addr, imcp_hide_ssid_config_data->response_id);
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit: %s: %d\n",__func__,__LINE__);

	return 0;
}


/**
 *	S acknowledgement for ACL configuration update
 */
int s_acl_config_updated_reply(struct sockaddr_in *client_addr, unsigned char response_id, unsigned char err_code)
{
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter: %s: %d\n",__func__,__LINE__);
	unsigned char *buffer;
	unsigned char *buffer_ptr;
	int           packet_size = get_imcp_signature_mesh_id_size() + 9 + IMCP_ID_LEN_SIZE + 1;
	int           ret;
	unsigned short node_id;
	unsigned short node_len;

	buffer = (unsigned char *)malloc(packet_size);

	buffer_ptr = buffer;

	buffer_ptr += copy_imcp_signature_packet_type_meshid(buffer_ptr, IMCP_CONFIG_REQUEST_RESPONSE);

	node_id      = IMCP_ACL_CONFIG_INFO_ID;
	node_len     = 9; 
	IMCP_MSG_PROCESS_SEND_ID_AND_LEN(buffer_ptr, node_id, node_len);


	memcpy(buffer_ptr, _MAC_ADDRESS, 6);
	buffer_ptr += 6;

	*buffer_ptr = TYPE_RESPONSE;
	buffer_ptr++;

	*buffer_ptr = TYPE_ACL_CONFIG_REQUEST;
	buffer_ptr++;

	*buffer_ptr = response_id;
	buffer_ptr++;

	*buffer_ptr = err_code;
	buffer_ptr++;

	ret = send_packet(buffer, packet_size, client_addr);
	al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"CONFIGD : INFO : IMCP_ACLCONFIGURATION_INFO reply packet sent (Bytes = %d) : %s: %d\n",ret,__func__,__LINE__);

	free(buffer);
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit: %s: %d\n",__func__,__LINE__);

	return 0;
}


int imcp_acl_config_info_helper(unsigned char *buffer, struct sockaddr_in *client_addr)
{
	/**
	 * The ACL entry has 2 bytes that include VLAN tag and ALLOW/DISALLOW bit
	 *
	 * +---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+
	 * |   C   | E |A/D|                 VLAN TAG                      |
	 * +---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+
	 *   15  14  13  12  11  10  9   8   7   6   5   4   3   2   1   0
	 *
	 *	E = 802.11e Enabled Flag
	 *  C = 802.11e Access Category Index
	 */
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter: %s: %d\n",__func__,__LINE__);

	int            i;
	unsigned short temp;
	unsigned short le162cpu;
	unsigned char  *buffer_ptr = buffer + get_imcp_signature_mesh_id_size();
	int            acl_conf_handle;
	int            al_conf_handle;
	int            vlan_name_length;
	int            dot11e_conf_handle;
	unsigned short node_id;
	unsigned short node_len;
	unsigned char err_code = 0;

      	al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"CONFIGD : INFO : IMCP_ACL_CONFIGURATION_INFO Packet - Received: %s line: %d\n",__func__,__LINE__);

	IMCP_MSG_PROCESS_RECEIVE_ID_AND_LEN(buffer_ptr,node_id,node_len);

	memcpy(imcp_acl_config_data->mac_address, buffer_ptr, 6);
	buffer_ptr += 6;

	imcp_acl_config_data->request_response = *buffer_ptr;
	buffer_ptr++;

	imcp_acl_config_data->response_id = *buffer_ptr;
	buffer_ptr++;

	if (imcp_acl_config_data->request_response == TYPE_RESPONSE)
	{
		al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : IMCP_ACL_CONFIGURATION_INFO Packet ignored: %s: %d\n",__func__,__LINE__);
		return 0;
	}

	if (memcmp(imcp_acl_config_data->mac_address, _MAC_ADDRESS, 6))
	{
		al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : IMCP_ACL_CONFIGURATION_INFO Packet ignored: %s: %d\n",__func__,__LINE__);
		return 0;
	}

	imcp_acl_config_data->count = *buffer_ptr;
	buffer_ptr++;
	if (imcp_acl_config_data->count > 0)
	{
		imcp_acl_config_data->entries = (imcp_acl_entry_t *)malloc(sizeof(imcp_acl_entry_t) * imcp_acl_config_data->count);
	}
	for (i = 0; i < imcp_acl_config_data->count; i++)
	{
		temp = 0;

		memset(&imcp_acl_config_data->entries[i], 0, sizeof(imcp_acl_entry_t));

		memcpy(imcp_acl_config_data->entries[i].mac_address, buffer_ptr, 6);
		buffer_ptr += 6;

		memcpy(&temp, buffer_ptr, 2);
		buffer_ptr += 2;
		le162cpu    = al_impl_le16_to_cpu(temp);

		imcp_acl_config_data->entries[i].vlan_tag        = IMCP_ACL_ENTRY_GET_VLAN_TAG(le162cpu);
		imcp_acl_config_data->entries[i].allow           = IMCP_ACL_ENTRY_GET_ALLOW_BIT(le162cpu);
		imcp_acl_config_data->entries[i].dot11e_enabled  = IMCP_ACL_ENTRY_GET_DOT11E_ENABLED_BIT(le162cpu);
		imcp_acl_config_data->entries[i].dot11e_category = IMCP_ACL_ENTRY_GET_DOT11E_CATEGORY_BIT(le162cpu);

		if (imcp_acl_config_data->entries[i].vlan_tag == 0x0FFF)
		{
			imcp_acl_config_data->entries[i].vlan_tag = 0xFFFF;
		}
	}

	acl_conf_handle = read_acl_config_data();

	if (acl_conf_handle > 0)
	{
		EXECUTE_PRE_FS_CHANGE

			/*
			 *	Increment config sqnr
			 */
			al_conf_handle = libalconf_read_config_data();
		if (al_conf_handle != -1)
		{
			set_mesh_config_sqnr(increment_config_sqnr(al_conf_handle));
			al_conf_put(AL_CONTEXT al_conf_handle);
			al_conf_close(AL_CONTEXT al_conf_handle);
		}

		write_acl_data(acl_conf_handle);
		EXECUTE_POST_FS_CHANGE(POST_FS_PARAM_TYPE_CONFIG)
			acl_conf_close(AL_CONTEXT acl_conf_handle);
	}

	dot11e_conf_handle = read_dot11e_category_data();
	al_conf_handle = libalconf_read_config_data();

	//EXECUTE_MESHD_CONFIGURE
	init_configurations(al_conf_handle, dot11e_conf_handle, 1, 1);

	dot11e_conf_close(AL_CONTEXT dot11e_conf_handle);
	al_conf_close(AL_CONTEXT al_conf_handle);

	s_acl_config_updated_reply(client_addr, imcp_acl_config_data->response_id, err_code);

	if (imcp_acl_config_data->count > 0)
	{
		free(imcp_acl_config_data->entries);
	}
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit: %s: %d\n",__func__,__LINE__);

	return 0;

#undef CONFIGD_ACL_ENTRY_GET_VLAN_TAG
#undef CONFIGD_ACL_ENTRY_GET_ALLOW_BIT
#undef CONFIGD_ACL_ENTRY_GET_DOT11E_ENABLED_BIT
#undef CONFIGD_ACL_ENTRY_GET_DOT11E_CATEGORY_BIT
}


/**
 *	Send acknowledgement for priv_channel configuration update
 */
int send_priv_channel_config_updated_reply(struct sockaddr_in *client_addr, unsigned char response_id)
{
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter: %s: %d\n",__func__,__LINE__);
	unsigned char *buffer;
	unsigned char *buffer_ptr;
	int           if_name_length;
	unsigned short node_id;
	unsigned short node_len;

	if_name_length = strlen(imcp_priv_channel_data->if_info.name);

	int packet_size = get_imcp_signature_mesh_id_size() + 9 + if_name_length;
	packet_size += IMCP_ID_LEN_SIZE;	

	int ret;

	buffer = (unsigned char *)malloc(packet_size);

	buffer_ptr = buffer;

	buffer_ptr += copy_imcp_signature_packet_type_meshid(buffer_ptr, IMCP_IF_PRIVATE_CHANNEL_INFO_REQUEST_RESPONSE);

	node_id      = IMCP_PRIVATE_CHANNEL_INFO_ID;
	node_len     = packet_size - get_imcp_signature_mesh_id_size() - IMCP_ID_LEN_SIZE;
	IMCP_MSG_PROCESS_SEND_ID_AND_LEN(buffer_ptr,node_id,node_len);

		memcpy(buffer_ptr, _MAC_ADDRESS, 6);
	buffer_ptr += 6;

	*buffer_ptr = TYPE_RESPONSE;
	buffer_ptr++;

	*buffer_ptr = response_id;
	buffer_ptr++;


	*buffer_ptr = if_name_length;
	buffer_ptr++;

	memcpy(buffer_ptr, &imcp_priv_channel_data->if_info.name, if_name_length);
	buffer_ptr += if_name_length;


	ret = send_packet(buffer, packet_size, client_addr);
	al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"CONFIGD : INFO : IMCP_PRIVATE_CHANNEL_CONFIGURATION_INFO reply packet sent (Bytes = %d) : %s: %d\n",ret,__func__,__LINE__);

	free(buffer);
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit: %s: %d\n",__func__,__LINE__);

	return 0;
}


int imcp_if_priv_channel_info_helper(unsigned char *buffer, struct sockaddr_in *client_addr)
{
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter: %s: %d\n",__func__,__LINE__);
	int            al_conf_handle, i, if_no, if_name_length;
	unsigned short le2cpu16;
	unsigned short node_id;
	unsigned short node_len;

	unsigned char  *buffer_ptr = buffer + get_imcp_signature_mesh_id_size();

	al_conf_if_info_t *if_info;

      	al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"CONFIGD : INFO : IMCP_PRIVATE_CHANNEL_CONFIGURATION_INFO Packet - Received: %s line: %d\n",__func__,__LINE__);

	IMCP_MSG_PROCESS_RECEIVE_ID_AND_LEN(buffer_ptr,node_id,node_len);

		memcpy(imcp_priv_channel_data->mac_address, buffer_ptr, 6);
	buffer_ptr += 6;

	imcp_priv_channel_data->request_response = *buffer_ptr;
	buffer_ptr++;

	imcp_priv_channel_data->response_id = *buffer_ptr;
	buffer_ptr++;

	if (imcp_priv_channel_data->request_response == TYPE_RESPONSE)
	{
		al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : IMCP_PRIVATE_CHANNEL_CONFIGURATION_INFO Packet ignored: %s: %d\n",__func__,__LINE__);
		return 0;
	}

	if (memcmp(imcp_priv_channel_data->mac_address, _MAC_ADDRESS, 6))
	{
		al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : IMCP_PRIVATE_CHANNEL_CONFIGURATION_INFO Packet ignored: %s: %d\n",__func__,__LINE__);
		return 0;
	}

	if_name_length = *buffer_ptr;
	buffer_ptr++;


	if (if_name_length <= 0)
	{
		al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : Invalid lenght: %s: %d\n",__func__,__LINE__);
		return -1;
	}

	memset(&imcp_priv_channel_data->if_info, 0, sizeof(al_conf_if_info_t));
	memcpy(imcp_priv_channel_data->if_info.name, buffer_ptr, if_name_length);

	buffer_ptr = buffer_ptr + if_name_length;

	imcp_priv_channel_data->if_info.priv_channel_bw = *buffer_ptr;
	buffer_ptr++;
	imcp_priv_channel_data->if_info.priv_channel_ant_max = *buffer_ptr;
	buffer_ptr++;
	imcp_priv_channel_data->if_info.priv_channel_ctl = *buffer_ptr;
	buffer_ptr++;
	imcp_priv_channel_data->if_info.priv_channel_max_power = *buffer_ptr;
	buffer_ptr++;
	imcp_priv_channel_data->if_info.priv_channel_count = *buffer_ptr;
	buffer_ptr++;

	for (i = 0; i < imcp_priv_channel_data->if_info.priv_channel_count; i++)
	{
		imcp_priv_channel_data->if_info.priv_channel_numbers[i] = *buffer_ptr;
		buffer_ptr++;

		memset(&le2cpu16, 0, sizeof(unsigned short));
		memcpy(&le2cpu16, buffer_ptr, sizeof(unsigned short));
		buffer_ptr += sizeof(unsigned short);
		imcp_priv_channel_data->if_info.priv_frequencies[i] = al_impl_le16_to_cpu(le2cpu16);

		imcp_priv_channel_data->if_info.priv_channel_flags[i] = *buffer_ptr;
		buffer_ptr++;
	}

	al_conf_handle = libalconf_read_config_data();

	if (al_conf_handle > 0)
	{
		EXECUTE_PRE_FS_CHANGE
			set_mesh_config_sqnr(increment_config_sqnr(al_conf_handle));

		write_priv_channel_config_data(al_conf_handle);
		EXECUTE_POST_FS_CHANGE(POST_FS_PARAM_TYPE_CONFIG)
			al_conf_close(AL_CONTEXT al_conf_handle);
	}

	send_priv_channel_config_updated_reply(client_addr, imcp_priv_channel_data->response_id);
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit: %s: %d\n",__func__,__LINE__);

	return 0;
}


int format_effistream_rules(unsigned char *buffer, al_conf_effistream_criteria_t *criteria)
{
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter: %s: %d\n",__func__,__LINE__);
	unsigned char *p;
	unsigned int  cpu2le;
	al_conf_effistream_criteria_t *temp;
	int child_count;

	p = buffer;

	*p = criteria->match_id;
	p++;

	switch (criteria->match_id)
	{
		case AL_CONF_EFFISTREAM_MATCH_ID_ETH_TYPE:
		case AL_CONF_EFFISTREAM_MATCH_ID_IP_TOS:
		case AL_CONF_EFFISTREAM_MATCH_ID_IP_DIFFSRV:
		case AL_CONF_EFFISTREAM_MATCH_ID_IP_PROTO:
		case AL_CONF_EFFISTREAM_MATCH_ID_RTP_VERSION:
		case AL_CONF_EFFISTREAM_MATCH_ID_RTP_PAYLOAD:
			cpu2le = al_impl_cpu_to_le32(criteria->match.value);
			memcpy(p, &cpu2le, sizeof(int));
			p += sizeof(int);
			break;

		case AL_CONF_EFFISTREAM_MATCH_ID_ETH_DST:
		case AL_CONF_EFFISTREAM_MATCH_ID_ETH_SRC:
			memcpy(p, criteria->match.mac_address.bytes, 6);
			p += 6;
			break;

		case AL_CONF_EFFISTREAM_MATCH_ID_IP_SRC:
		case AL_CONF_EFFISTREAM_MATCH_ID_IP_DST:
			memcpy(p, criteria->match.ip_address, 4);
			p += 4;
			break;

		case AL_CONF_EFFISTREAM_MATCH_ID_UDP_SRC_PORT:
		case AL_CONF_EFFISTREAM_MATCH_ID_UDP_DST_PORT:
		case AL_CONF_EFFISTREAM_MATCH_ID_UDP_LENGTH:
		case AL_CONF_EFFISTREAM_MATCH_ID_TCP_SRC_PORT:
		case AL_CONF_EFFISTREAM_MATCH_ID_TCP_DST_PORT:
		case AL_CONF_EFFISTREAM_MATCH_ID_TCP_LENGTH:
		case AL_CONF_EFFISTREAM_MATCH_ID_RTP_LENGTH:
			cpu2le = al_impl_cpu_to_le32(criteria->match.range.min_val);
			memcpy(p, &cpu2le, sizeof(int));
			p     += sizeof(int);
			cpu2le = al_impl_cpu_to_le32(criteria->match.range.max_val);
			memcpy(p, &cpu2le, sizeof(int));
			p += sizeof(int);
			break;
	}

	child_count = 0;
	temp        = criteria->first_child;

	while (temp != NULL)
	{
		++child_count;
		temp = temp->next_sibling;
	}

	*p = child_count;
	p++;

	if (child_count)
	{
		p += format_effistream_rules(p, criteria->first_child);
	}
	else
	{
		unsigned char options;
		options = 0;
		if (criteria->action.no_ack)
		{
			options |= IMCP_EFFISTREAM_ACTION_OPTION_NO_ACK;
		}
		if (criteria->action.drop)
		{
			options |= IMCP_EFFISTREAM_ACTION_OPTION_DROP;
		}
		if (criteria->action.queued_retry)
		{
			options |= IMCP_EFFISTREAM_ACTION_OPTION_QUEUED_RETRY;
		}

		*p = options;
		p++;
		*p = criteria->action.dot11e_category;
		p++;
		*p = criteria->action.bit_rate;
		p++;
		p += 4;                   /* 4 reserved bytes */
	}

	if (criteria->next_sibling != NULL)
	{
		p += format_effistream_rules(p, criteria->next_sibling);
	}
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit: %s: %d\n",__func__,__LINE__);

	return(p - buffer);
}


int send_effistream_config_info_reply(struct sockaddr_in *client_addr, imcp_effistream_config_t *config)
{
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter: %s: %d\n",__func__,__LINE__);
	int           packet_size;
	unsigned char buffer[1500];
	unsigned char *p;
	int           ret;
	unsigned char rules_count;
	unsigned short  node_id;
        unsigned short  node_len;
	al_conf_effistream_criteria_t *temp;

	packet_size = 0;
	p           = buffer;

	p += copy_imcp_signature_packet_type_meshid(p, IMCP_EFFISTREAM_INFO);

        node_id = IMCP_EFFISTREAM_REQ_RES_INFO_ID;
        node_len = (unsigned short)(packet_size);
        IMCP_MSG_PROCESS_SEND_ID_AND_LEN(p,node_id,node_len);

	memcpy(p, config->mac_address, 6);
	p += 6;

	*p = config->request_response;
	p++;

	*p = config->response_id;
	p++;


	temp        = config->criteria_root->first_child;
	rules_count = 0;

	while (temp != NULL)
	{
		++rules_count;
		temp = temp->next_sibling;
	}

	*p = rules_count;
	p++;

	if (config->criteria_root->first_child != NULL)
	{
		p += format_effistream_rules(p, config->criteria_root->first_child);
	}

	packet_size = p - buffer;

	ret = send_packet(buffer, packet_size, client_addr);

	al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"CONFIGD : INFO : IMCP_EFFISTREAM_CONFIG reply packet sent (Bytes = %d) : %s: %d\n",ret,__func__,__LINE__);
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit: %s: %d\n",__func__,__LINE__);

	return 0;
}


int imcp_effistream_request_response_helper(unsigned char *buffer, struct sockaddr_in *client_addr)
{
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter: %s: %d\n",__func__,__LINE__);
	int al_conf_handle;
	imcp_effistream_rr_data_t imcp_effistream_rr_data;
	imcp_effistream_config_t  imcp_effistream_config;
	unsigned short temp;
   	unsigned short node_id;
   	unsigned short node_len;

	unsigned char *buffer_ptr = buffer + get_imcp_signature_mesh_id_size();

      	al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"CONFIGD : INFO : IMCP_EFFISTREAM_REQUEST_RESPONSE Packet - Received: %s line: %d\n",__func__,__LINE__);

	memset(&imcp_effistream_rr_data, 0, sizeof(imcp_effistream_rr_data_t));

   	IMCP_MSG_PROCESS_RECEIVE_ID_AND_LEN(buffer_ptr,node_id,node_len);

	memcpy(imcp_effistream_rr_data.mac_address, buffer_ptr, 6);
	buffer_ptr += 6;

	imcp_effistream_rr_data.request_response_type = *buffer_ptr;
	buffer_ptr++;
         


	if (imcp_effistream_rr_data.request_response_type == TYPE_RESPONSE)
	{
		al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : IMCP_EFFISTREAM_CONFIGURATION_REQUEST Packet ignored: %s: %d\n",__func__,__LINE__);
		return 0;
	}

	if (memcmp(imcp_effistream_rr_data.mac_address, _MAC_ADDRESS, 6))
	{
		al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : IMCP_EFFISTREAM_CONFIGURATION_REQUEST Packet ignored: %s: %d\n",__func__,__LINE__);
		return 0;
	}

	imcp_effistream_rr_data.response_id = *buffer_ptr;
	buffer_ptr++;

	al_conf_handle = libalconf_read_config_data();

	if (al_conf_handle != -1)
	{
		memcpy(imcp_effistream_config.mac_address, imcp_effistream_rr_data.mac_address, 6);
		imcp_effistream_config.request_response = TYPE_RESPONSE;
		imcp_effistream_config.response_id      = imcp_effistream_rr_data.response_id;
		imcp_effistream_config.criteria_root    = al_conf_get_effistream_root(AL_CONTEXT al_conf_handle);

		send_effistream_config_info_reply(client_addr, &imcp_effistream_config);

		al_conf_close(AL_CONTEXT al_conf_handle);
	}
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit: %s: %d\n",__func__,__LINE__);

	return 0;
}


/**
 *	Send acknowledgement for priv_channel configuration update
 */
int send_effistream_config_updated_reply(struct sockaddr_in *client_addr, unsigned char response_id)
{
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter: %s: %d\n",__func__,__LINE__);
	unsigned char buffer[1500];
	unsigned char *p;
	unsigned short  node_id;
        unsigned short  node_len;
	int           packet_length;
	int           packet_size;
	int           ret;

	p  = buffer;
	packet_length = get_imcp_signature_mesh_id_size() + 8 + IMCP_ID_LEN_SIZE;
	p += copy_imcp_signature_packet_type_meshid(p, IMCP_EFFISTREAM_REQUEST_RESPONSE);

        node_id = IMCP_EFFISTREAM_INFO_ID;
        node_len = (unsigned short)(packet_length);
        IMCP_MSG_PROCESS_SEND_ID_AND_LEN(p,node_id,node_len);

	memcpy(p, _MAC_ADDRESS, 6);
	p += 6;

	*p = TYPE_RESPONSE;
	p++;

	*p = response_id;
	p++;

	packet_size = (p - buffer);

	ret = send_packet(buffer, packet_size, client_addr);

	al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"CONFIGD : INFO : IMCP_EFFISTREAM_CONFIGURATION_INFO reply packet sent (Bytes = %d) : %s: %d\n",ret,__func__,__LINE__);
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit: %s: %d\n",__func__,__LINE__);

	return 0;
}


int parse_effistream_info(unsigned char                 *buffer,
		int                           sibling_count,
		al_conf_effistream_criteria_t *parent,
		al_conf_effistream_criteria_t *prev_sibling)
{
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter: %s: %d\n",__func__,__LINE__);
	al_conf_effistream_criteria_t *current_node;
	unsigned int  le2cpu;
	int           child_count;
	int           ret;
	unsigned char *p;

	p = buffer;

	current_node = (al_conf_effistream_criteria_t *)malloc(sizeof(al_conf_effistream_criteria_t));
	memset(current_node, 0, sizeof(al_conf_effistream_criteria_t));

	current_node->match_id = *p;
	p++;

	switch (current_node->match_id)
	{
		case AL_CONF_EFFISTREAM_MATCH_ID_ETH_TYPE:
		case AL_CONF_EFFISTREAM_MATCH_ID_IP_TOS:
		case AL_CONF_EFFISTREAM_MATCH_ID_IP_DIFFSRV:
		case AL_CONF_EFFISTREAM_MATCH_ID_IP_PROTO:
		case AL_CONF_EFFISTREAM_MATCH_ID_RTP_VERSION:
		case AL_CONF_EFFISTREAM_MATCH_ID_RTP_PAYLOAD:
			memcpy(&le2cpu, p, sizeof(int));
			p += sizeof(int);
			current_node->match.value = al_impl_le32_to_cpu(le2cpu);
			break;

		case AL_CONF_EFFISTREAM_MATCH_ID_ETH_DST:
		case AL_CONF_EFFISTREAM_MATCH_ID_ETH_SRC:
			memset(&current_node->match.mac_address, 0, sizeof(al_net_addr_t));
			current_node->match.mac_address.length = 6;
			memcpy(current_node->match.mac_address.bytes, p, 6);
			p += 6;
			break;

		case AL_CONF_EFFISTREAM_MATCH_ID_IP_SRC:
		case AL_CONF_EFFISTREAM_MATCH_ID_IP_DST:
			memcpy(current_node->match.ip_address, p, 4);
			p += 4;
			break;

		case AL_CONF_EFFISTREAM_MATCH_ID_UDP_SRC_PORT:
		case AL_CONF_EFFISTREAM_MATCH_ID_UDP_DST_PORT:
		case AL_CONF_EFFISTREAM_MATCH_ID_UDP_LENGTH:
		case AL_CONF_EFFISTREAM_MATCH_ID_TCP_SRC_PORT:
		case AL_CONF_EFFISTREAM_MATCH_ID_TCP_DST_PORT:
		case AL_CONF_EFFISTREAM_MATCH_ID_TCP_LENGTH:
		case AL_CONF_EFFISTREAM_MATCH_ID_RTP_LENGTH:
			memcpy(&le2cpu, p, sizeof(int));
			p += sizeof(int);
			current_node->match.range.min_val = al_impl_le32_to_cpu(le2cpu);
			memcpy(&le2cpu, p, sizeof(int));
			p += sizeof(int);
			current_node->match.range.max_val = al_impl_le32_to_cpu(le2cpu);
			break;

		default:
			free(current_node);
			return -1;
	}

	child_count = *p;
	p++;

	if (child_count > 0)
	{
		ret = parse_effistream_info(p, child_count, current_node, NULL);
		if (ret == -1)
		{
			free(current_node);
			return ret;
		}
		p += ret;
	}
	else
	{
		unsigned char options;
		options = *p;
		p++;
		if (options & IMCP_EFFISTREAM_ACTION_OPTION_NO_ACK)
		{
			current_node->action.no_ack = 1;
		}
		if (options & IMCP_EFFISTREAM_ACTION_OPTION_DROP)
		{
			current_node->action.drop = 1;
		}
		if (options & IMCP_EFFISTREAM_ACTION_OPTION_QUEUED_RETRY)
		{
			current_node->action.queued_retry = 1;
		}

		current_node->action.dot11e_category = *p;
		p++;
		current_node->action.bit_rate = *p;
		p++;
		p += 4;                   /* Reserved bytes */
	}

	if (prev_sibling == NULL)
	{
		parent->first_child = current_node;
	}
	else
	{
		prev_sibling->next_sibling = current_node;
	}

	if (sibling_count - 1 > 0)
	{
		ret = parse_effistream_info(p, sibling_count - 1, parent, current_node);
		if (ret == -1)
		{
			free(current_node);
			al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : parsing failed: %s: %d\n",__func__,__LINE__);
			return ret;
		}
		p += ret;
	}

	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit: %s: %d\n",__func__,__LINE__);
	return(p - buffer);
}


int imcp_effistream_info_helper(unsigned char *buffer, struct sockaddr_in *client_addr)
{
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter: %s: %d\n",__func__,__LINE__);
	unsigned char                 *p;
	imcp_effistream_rr_data_t     imcp_effistream_rr_data;
	al_conf_effistream_criteria_t *criteria_root;
	int child_count;
	int ret;
	int al_conf_handle;
	 unsigned short  node_id;
   	unsigned short  node_len;

	p = buffer + get_imcp_signature_mesh_id_size();

      	al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"CONFIGD : INFO : IMCP_EFFISTREAM_CONFIGURATION_INFO Packet - Received: %s line: %d\n",__func__,__LINE__);

	 IMCP_MSG_PROCESS_RECEIVE_ID_AND_LEN(p,node_id,node_len);

	memcpy(imcp_effistream_rr_data.mac_address, p, 6);
	p += 6;
	imcp_effistream_rr_data.request_response_type = *p;
	p++;

	imcp_effistream_rr_data.response_id = *p;
	p++;

	if (imcp_effistream_rr_data.request_response_type == TYPE_RESPONSE)
	{
		al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : IMCP_EFFISTREAM_CONFIGURATION_INFO packet ignored func:%s: %d\n",__func__,__LINE__);
		return 0;
	}

	if (memcmp(imcp_effistream_rr_data.mac_address, _MAC_ADDRESS, 6))
	{
		al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : IMCP_EFFISTREAM_CONFIGURATION_INFO packet ignored func:%s: %d\n",__func__,__LINE__);
		return 0;
	}

	criteria_root = (al_conf_effistream_criteria_t *)malloc(sizeof(al_conf_effistream_criteria_t));
	memset(criteria_root, 0, sizeof(al_conf_effistream_criteria_t));

	child_count = *p;
	p++;

	if (child_count > 0)
	{
		ret = parse_effistream_info(p, child_count, criteria_root, NULL);
	}

	if (ret == -1)
	{
		al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : IMCP_EFFISTREAM_CONFIGURATION_INFO Packet ignored due to processing error: %s: %d\n",__func__,__LINE__);
		free(criteria_root);
		return -1;
	}

	al_conf_handle = libalconf_read_config_data();

	if (al_conf_handle > 0)
	{
		EXECUTE_PRE_FS_CHANGE
			al_conf_set_effistream_root(AL_CONTEXT al_conf_handle, criteria_root);

		set_mesh_config_sqnr(increment_config_sqnr(al_conf_handle));
		al_conf_put(al_conf_handle);
		EXECUTE_POST_FS_CHANGE(POST_FS_PARAM_TYPE_CONFIG)
			al_conf_close(AL_CONTEXT al_conf_handle);
	}

	send_effistream_config_updated_reply(client_addr, imcp_effistream_rr_data.response_id);
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit: %s: %d\n",__func__,__LINE__);

	return 0;
}


int update_sip_data(sip_conf_handle_t sip_conf_handle)
{
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter: %s: %d\n",__func__,__LINE__);
	sip_conf_sta_info_t info;
	int                 i;
	int                 extn;


	sip_conf_get_server_ip(AL_CONTEXT sip_conf_handle, imcp_sip_config_data->ip_address);
	imcp_sip_config_data->enabled    = sip_conf_get_sip_enabled(AL_CONTEXT sip_conf_handle);
	imcp_sip_config_data->adhoc_only = sip_conf_get_adhoc_only(AL_CONTEXT sip_conf_handle);
	imcp_sip_config_data->port       = sip_conf_get_sip_port(AL_CONTEXT sip_conf_handle);
	imcp_sip_config_data->count      = sip_conf_get_sta_count(AL_CONTEXT sip_conf_handle);

	if (imcp_sip_config_data->count <= 0)
	{
		imcp_acl_config_data->entries = NULL;
		al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : Invalid count: %s: %d\n",__func__,__LINE__);
		return 0;
	}

	imcp_sip_config_data->entries = (imcp_sip_entry_t *)malloc(sizeof(imcp_sip_entry_t) * imcp_sip_config_data->count);
	memset(imcp_sip_config_data->entries, 0, sizeof(imcp_sip_entry_t) * imcp_sip_config_data->count);

	for (i = 0; i < imcp_sip_config_data->count; i++)
	{
		sip_conf_get_sta_info(AL_CONTEXT sip_conf_handle, i, &info);

		memcpy(imcp_sip_config_data->entries[i].mac_address, info.mac, 6);
		al_conf_atoi(info.extn, &extn);
		imcp_sip_config_data->entries[i].extn = (unsigned char)extn;
	}
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit: %s: %d\n",__func__,__LINE__);

	return 0;
}


/*
 *	S SIP configuration info to client
 */
int send_sip_config_info_reply(struct sockaddr_in *client_addr, unsigned char response_id)
{
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter: %s: %d\n",__func__,__LINE__);
	int            packet_size;
	unsigned char  *buffer;
	unsigned char  *buffer_ptr;
	unsigned short cpu2le16;
	int            ret;
	int            i;
	unsigned short node_id;
	unsigned short node_len;

	packet_size  = get_imcp_signature_mesh_id_size();
	packet_size += 6 + 1 + 1 + 1;                  /* MAC address, REQ-RES, RESID */
	packet_size += 1 + 1 + 4 + 2 + 1;              /* enabled, adhoc_only, ip-addr, port, sta-count */
	packet_size += IMCP_ID_LEN_SIZE;	

	buffer = (unsigned char *)malloc(1500);
	memset(buffer, 0, 1500);

	buffer_ptr  = buffer;
	buffer_ptr += copy_imcp_signature_packet_type_meshid(buffer_ptr, IMCP_AP_SIP_CONFIGURATION_INFO);

	node_id		= IMCP_SIP_CONFIG_INFO_REQ_RES_INFO_ID;
	node_len	= packet_size - get_imcp_signature_mesh_id_size() - IMCP_ID_LEN_SIZE;
	IMCP_MSG_PROCESS_SEND_ID_AND_LEN(buffer_ptr,node_id,node_len);

		memcpy(buffer_ptr, imcp_sip_config_data->mac_address, 6);
	buffer_ptr += 6;

	*buffer_ptr = imcp_sip_config_data->request_response;
	buffer_ptr++;
	*buffer_ptr = response_id;
	buffer_ptr++;

	*buffer_ptr = imcp_sip_config_data->enabled;
	buffer_ptr++;
	*buffer_ptr = imcp_sip_config_data->adhoc_only;
	buffer_ptr++;
	memcpy(buffer_ptr, imcp_sip_config_data->ip_address, 4);
	buffer_ptr += 4;

	cpu2le16 = al_impl_cpu_to_le16(imcp_sip_config_data->port);
	memcpy(buffer_ptr, &cpu2le16, 2);
	buffer_ptr += 2;

	*buffer_ptr = imcp_sip_config_data->count;
	buffer_ptr++;

	for (i = 0; i < imcp_sip_config_data->count; i++)
	{
		memcpy(buffer_ptr, imcp_sip_config_data->entries[i].mac_address, 6);
		buffer_ptr += 6;
		*buffer_ptr = imcp_sip_config_data->entries[i].extn;
		buffer_ptr++;
		packet_size += 7;
	}

	ret = send_packet(buffer, packet_size, client_addr);
	al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"CONFIGD : INFO : IMCP_SIP_CONFIG Packet sent (Bytes = %d) : %s: %d\n",ret,__func__,__LINE__);

	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit: %s: %d\n",__func__,__LINE__);
	return 0;
}


int imcp_sip_config_request_response_helper(unsigned char *buffer, struct sockaddr_in *client_addr)
{
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter: %s: %d\n",__func__,__LINE__);
	sip_conf_handle_t sip_conf_handle;
	unsigned char     *buffer_ptr;
	unsigned short node_id;
	unsigned short node_len;

	al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"CONFIGD : INFO : IMCP_SIP_CONFIG_REQUEST_RESPONSE Packet - Received : %s: %d\n",__func__,__LINE__);

	buffer_ptr = buffer + get_imcp_signature_mesh_id_size();

	memset(imcp_config_rr_data, 0, sizeof(imcp_config_rr_data_t));

	IMCP_MSG_PROCESS_RECEIVE_ID_AND_LEN(buffer_ptr,node_id,node_len);

		memcpy(imcp_config_rr_data->mac_address, buffer_ptr, 6);
	buffer_ptr += 6;

	imcp_config_rr_data->request_response_type = *buffer_ptr;
	buffer_ptr++;

	imcp_config_rr_data->type = *buffer_ptr;
	buffer_ptr++;

	imcp_config_rr_data->response_id = *buffer_ptr;
	buffer_ptr++;

	buffer_ptr++;        //skip the first type byte

	sip_conf_handle = read_sip_config_data();

	if (sip_conf_handle != 0)
	{
		IMCP_MSG_PROCESS_RECEIVE_ID_AND_LEN(buffer_ptr,node_id,node_len);
			memcpy(imcp_sip_config_data->mac_address, imcp_config_rr_data->mac_address, 6);
		imcp_sip_config_data->request_response = TYPE_RESPONSE;
		update_sip_data(sip_conf_handle);
		sip_conf_close(AL_CONTEXT sip_conf_handle);
		send_sip_config_info_reply(client_addr, imcp_config_rr_data->response_id);
		if (imcp_sip_config_data->count > 0)
		{
			free(imcp_sip_config_data->entries);
		}
	}
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit: %s: %d\n",__func__,__LINE__);
	return 0;
}


/**
 *	S acknowledgement for SIP configuration update
 */
int send_sip_config_updated_reply(struct sockaddr_in *client_addr, unsigned char response_id)
{
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter: %s: %d\n",__func__,__LINE__);
	unsigned char buffer[512];
	unsigned char *buffer_ptr;
	int           packet_size;
	int           ret;
	unsigned short node_id;
	unsigned short node_len;

	packet_size = get_imcp_signature_mesh_id_size() + 10;
	packet_size += IMCP_ID_LEN_SIZE;	
	buffer_ptr  = buffer;
	buffer_ptr += copy_imcp_signature_packet_type_meshid(buffer_ptr, IMCP_CONFIG_REQUEST_RESPONSE);

	node_id      = IMCP_SIP_CONFIG_INFO_ID;
	node_len     = packet_size - get_imcp_signature_mesh_id_size() - IMCP_ID_LEN_SIZE; //Here including all the sub fields node_id & len
	IMCP_MSG_PROCESS_SEND_ID_AND_LEN(buffer_ptr, node_id, node_len);

	memcpy(buffer_ptr, _MAC_ADDRESS, 6);
	buffer_ptr += 6;

	*buffer_ptr = TYPE_RESPONSE;
	buffer_ptr++;

	*buffer_ptr = TYPE_EXTEND_TO_BYTE_2;
	buffer_ptr++;

	*buffer_ptr = response_id;
	buffer_ptr++;

	*buffer_ptr = TYPE_SIP_CONFIG_REQUEST;
	buffer_ptr++;

	ret = send_packet(buffer, packet_size, client_addr);
	al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"CONFIGD : INFO : IMCP_SIP_CONFIGURATION_INFO reply packet sent (Bytes = %d) : %s: %d\n",ret,__func__,__LINE__);

	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit: %s: %d\n",__func__,__LINE__);
	return 0;
}


int imcp_sip_config_info_helper(unsigned char *buffer, struct sockaddr_in *client_addr)
{
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter: %s: %d\n",__func__,__LINE__);
	int               i;
	unsigned short    le162cpu;
	unsigned char     *buffer_ptr;
	sip_conf_handle_t sip_conf_handle;
	int               al_conf_handle;
	int               hop_cost;
	unsigned short    node_id;
	unsigned short    node_len;

	al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"CONFIGD : INFO : IMCP_SIP_CONFIGURATION_INFO Packet - Received : %s  line:%d\n",__func__,__LINE__);

	buffer_ptr = buffer + get_imcp_signature_mesh_id_size();

	IMCP_MSG_PROCESS_RECEIVE_ID_AND_LEN(buffer_ptr,node_id,node_len);

	memcpy(imcp_sip_config_data->mac_address, buffer_ptr, 6);
	buffer_ptr += 6;

	if (memcmp(imcp_sip_config_data->mac_address, _MAC_ADDRESS, 6))
	{
	   	al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : IMCP_SIP_CONFIGURATION_INFO Packet ignored 1 : %s: %d\n",__func__,__LINE__);
		return 0;
	}

	imcp_sip_config_data->request_response = *buffer_ptr;
	buffer_ptr++;

	if (imcp_sip_config_data->request_response == TYPE_RESPONSE)
	{
	   	al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : IMCP_SIP_CONFIGURATION_INFO Packet ignored 2  : %s: %d\n",__func__,__LINE__);
		return 0;
	}

	imcp_sip_config_data->response_id = *buffer_ptr;
	buffer_ptr++;

	imcp_sip_config_data->enabled = *buffer_ptr;
	buffer_ptr++;
	imcp_sip_config_data->adhoc_only = *buffer_ptr;
	buffer_ptr++;

	memcpy(imcp_sip_config_data->ip_address, buffer_ptr, 4);
	buffer_ptr += 4;
	memcpy(&le162cpu, buffer_ptr, 2);
	buffer_ptr += 2;

	imcp_sip_config_data->port  = al_impl_le16_to_cpu(le162cpu);
	imcp_sip_config_data->count = *buffer_ptr;
	buffer_ptr++;

	if (imcp_sip_config_data->count > 0)
	{
		imcp_sip_config_data->entries = (imcp_sip_entry_t *)malloc(sizeof(imcp_sip_entry_t) * imcp_sip_config_data->count);
	}

	for (i = 0; i < imcp_sip_config_data->count; i++)
	{
		memset(&imcp_sip_config_data->entries[i], 0, sizeof(imcp_sip_entry_t));
		memcpy(imcp_sip_config_data->entries[i].mac_address, buffer_ptr, 6);
		buffer_ptr += 6;
		imcp_sip_config_data->entries[i].extn = *buffer_ptr;
		buffer_ptr += 1;
	}

	sip_conf_handle = read_sip_config_data();

	if (sip_conf_handle != 0)
	{
		EXECUTE_PRE_FS_CHANGE

			/*
			 *	Increment config sqnr
			 */
			al_conf_handle = libalconf_read_config_data();
		if (al_conf_handle != -1)
		{
			set_mesh_config_sqnr(increment_config_sqnr(al_conf_handle));
			hop_cost  = al_conf_get_hop_cost(AL_CONTEXT al_conf_handle);
			hop_cost |= 0x80;
			al_conf_set_hop_cost(AL_CONTEXT al_conf_handle, hop_cost);
			al_conf_put(AL_CONTEXT al_conf_handle);
			al_conf_close(AL_CONTEXT al_conf_handle);
		}

		write_sip_data(sip_conf_handle);
		sip_conf_close(AL_CONTEXT sip_conf_handle);

		send_sip_config_updated_reply(client_addr, imcp_sip_config_data->response_id);

		EXECUTE_POST_FS_CHANGE(POST_FS_PARAM_TYPE_CONFIG)
	}
	else
	{
		al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"CONFIGD : INFO : IMCP_SIP_CONFIGURATION_INFO sip_conf_handle = NULL : %s : %d\n",__func__,__LINE__);
	}

	if (imcp_sip_config_data->count > 0)
	{
		free(imcp_sip_config_data->entries);
	}

	al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"CONFIGD : INFO : IMCP_SIP_CONFIGURATION_INFO returning : %s : %d\n",__func__,__LINE__);

	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit: %s: %d\n",__func__,__LINE__);
	return 0;
}

int imcp_mac_addr_request_response_helper(unsigned char *buffer, struct sockaddr_in *client_addr)
{
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter: %s: %d\n",__func__,__LINE__);
	int 						al_conf_handle;
	int							i = 0, packet_size = 0;
	int 						size = 0;
	unsigned char				*mac_buffer= NULL;
	unsigned char				app_mac[6], app_name[IFNAMSIZ];
	unsigned char				*buffer_ptr = NULL, send_buffer[1024] = {0};
	int							fd = -1, ret = 0;
	unsigned char				rCount = 0, vCount = 0, aCount = 0;
	unsigned short 				node_id;
	unsigned short 				node_len;
	struct 						ifreq  ifr;
	unsigned char 				*rC, *vC, *aC;
	_al_conf_data_t 			*data;
	unsigned short 				* node_length;
	al_conf_if_info_t 			*if_info;


   al_conf_handle = libalconf_read_config_data();
   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"CONFIGD : INFO : al_conf_handle : %s: %d\n",__func__,__LINE__);
   if (al_conf_handle == -1)
   { 
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : Could not read configuration : %s: %d\n",__func__,__LINE__);
      return -1;
   }

	fd = socket(AF_INET, SOCK_DGRAM, 0);
	ifr.ifr_addr.sa_family = AF_INET;

	data = (_al_conf_data_t *)al_conf_handle;
	
	imcp_mac_addr_data = (imcp_mac_addr_req_resp_t *)malloc(sizeof(imcp_mac_addr_req_resp_t));	
	if(imcp_mac_addr_data == NULL)
	{
	   	al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : Malloc failed for imcp_mac_addr_data : %s: %d\n",__func__,__LINE__);
		al_conf_close(AL_CONTEXT al_conf_handle);
		return -1;
	}

	buffer_ptr = send_buffer;
	buffer_ptr += copy_imcp_signature_packet_type_meshid(buffer_ptr, IMCP_MAC_ADDR_REQUEST_RESPONSE);
	packet_size = buffer_ptr - send_buffer;

	node_id     = IMCP_MAC_ADDR_REQUEST_RESPONSE_INFO_ID;
	node_len = packet_size - get_imcp_signature_mesh_id_size() - IMCP_ID_LEN_SIZE;      // Node len is modified below 
	IMCP_MSG_PROCESS_SEND_ID_AND_LEN(buffer_ptr,node_id,node_len);
	node_length = buffer_ptr - 2;
	packet_size += sizeof(node_id) + sizeof(node_len);
	
	memcpy(buffer_ptr, _MAC_ADDRESS, 6);
	buffer_ptr += 6;
	packet_size += 6;

	rC = buffer_ptr;
	buffer_ptr += sizeof(unsigned char);
	packet_size += sizeof(unsigned char);
	
	vC = buffer_ptr;
	buffer_ptr += sizeof(unsigned char);
	packet_size += sizeof(unsigned char);

	aC = buffer_ptr;
	buffer_ptr += sizeof(unsigned char);
	packet_size += sizeof(unsigned char);

	al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"CONFIGD : INFO : Board_Mac_addr:%x:%x:%x:%x:%x:%x : %s : %d\n", _MAC_ADDRESS[0], _MAC_ADDRESS[1],
										_MAC_ADDRESS[2], _MAC_ADDRESS[3],_MAC_ADDRESS[4],_MAC_ADDRESS[5],__func__,__LINE__);	

	for (i = 0; i < data->if_count; i++)
	{
		if_info = &data->if_info[i];

		if((if_info->phy_type != AL_CONF_IF_PHY_TYPE_802_11))
			continue;
		
		strncpy(ifr.ifr_name, if_info->name, IFNAMSIZ - 1);

		ret = ioctl(fd, SIOCGIFHWADDR, &ifr);
		if (ret == -1)
		{
	   		al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : error while getting mac addres : %s: %d\n",__func__,__LINE__);
			free(imcp_mac_addr_data);
			al_conf_close(AL_CONTEXT al_conf_handle);
			close(fd);
			return -1;
		}
		al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"CONFIGD : INFO : MAC:%x:%x:%x:%x:%x:%x : %s : %d\n", ifr.ifr_hwaddr.sa_data[0],  ifr.ifr_hwaddr.sa_data[1],
													 	ifr.ifr_hwaddr.sa_data[2], ifr.ifr_hwaddr.sa_data[3], 
														ifr.ifr_hwaddr.sa_data[4], ifr.ifr_hwaddr.sa_data[5],__func__,__LINE__);

		*buffer_ptr = (unsigned char)strlen(ifr.ifr_name);
		buffer_ptr += sizeof(unsigned char);
		packet_size += sizeof(unsigned char);

		memcpy(buffer_ptr, ifr.ifr_name, strlen(ifr.ifr_name));
		buffer_ptr += strlen(ifr.ifr_name);
		packet_size += strlen(ifr.ifr_name);

		memcpy(buffer_ptr, ifr.ifr_hwaddr.sa_data, 6);
		buffer_ptr += 6;	
		packet_size += 6;

		rCount++;
	}
	*rC = rCount;

	for(i = 0; i < data->vlan_count; i++)
	{
		if(!(strncmp("default", data->vlan_info[i].name, 7)))
			continue;
		strncpy(ifr.ifr_name, data->vlan_info[i].name, IFNAMSIZ - 1);

	   ret = ioctl(fd, SIOCGIFHWADDR, &ifr);
		if (ret == -1)
		{
	   		al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : error while getting mac addres : %s: %d\n",__func__,__LINE__);
			free(imcp_mac_addr_data);
			al_conf_close(AL_CONTEXT al_conf_handle);
			close(fd);
			return -1;
		}
		
		*buffer_ptr = (unsigned char)strlen(ifr.ifr_name);
		buffer_ptr += sizeof(unsigned char);
		packet_size += sizeof(unsigned char);

		memcpy(buffer_ptr, ifr.ifr_name, strlen(ifr.ifr_name));
		buffer_ptr += strlen(ifr.ifr_name);
		packet_size += strlen(ifr.ifr_name);

		memcpy(buffer_ptr, ifr.ifr_hwaddr.sa_data, 6);
		buffer_ptr += 6;
		packet_size += 6;

		vCount++;
	}
	*vC = vCount;

	int tmpCount = 0, tmpIndex = 0;

#ifdef x86_ONLY
    aCount = 0;
#else
	aCount = _MAC_ADDRESS[8];
#endif
	*aC = aCount;

	for (i = 0 ; i < aCount ; i++)
	{
		for( tmpIndex = 0 ; tmpIndex < 6 ; tmpIndex++ )
		{
			app_mac[tmpIndex] = _BEGIN_SIGNATURE[tmpIndex + 82 + (tmpCount * 6)];
		}
		tmpCount++;

		memset(app_name, 0, IFNAMSIZ);
		sprintf(app_name, "APP%d", (unsigned char)(i+1));
 
		*buffer_ptr = (unsigned char)strlen(app_name);
		buffer_ptr += sizeof(unsigned char);
		packet_size += sizeof(unsigned char);
	
		memcpy(buffer_ptr, app_name, strlen(app_name));
		buffer_ptr += strlen(app_name);
		packet_size += strlen(app_name);
	
		memcpy(buffer_ptr, app_mac, 6);
		buffer_ptr += 6;
		packet_size += 6;
			
		al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"CONFIGD : INFO : APP_INFO:\t%s:%x:%x:%x:%x:%x:%x : %s : %d\\n", app_name, app_mac[0], app_mac[1],
											 app_mac[2], app_mac[3], app_mac[4], app_mac[5],__func__,__LINE__);
	}
	
	node_len = al_impl_cpu_to_le16(packet_size);
	*node_length = node_len;										// node_len equal to packet_size
#if 1
	ret = send_packet_ex(send_buffer, packet_size, client_addr, 1, 1);	
	if(!ret)
	{
	   	al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : error while sending packet : %s: %d\n",__func__,__LINE__);
	}
#endif
	free(mac_buffer);
	free(imcp_mac_addr_data);
	al_conf_close(AL_CONTEXT al_conf_handle);
	close(fd);
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit: %s: %d\n",__func__,__LINE__);
	return 0;
}

int send_mgmt_gw_config_updated_reply(struct sockaddr_in *client_addr, unsigned int response_status)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter: %s: %d\n",__func__,__LINE__);
   unsigned char buffer[100];
   unsigned char *p;
   unsigned short  node_id;
        unsigned short  node_len;
   int           packet_length;
   int           packet_size;
   int           ret;

   p  = buffer;
   packet_length = get_imcp_signature_mesh_id_size() + 8 + IMCP_ID_LEN_SIZE;
   p += copy_imcp_signature_packet_type_meshid(p, IMCP_MGMT_GW_CONFIG_RESPONSE);

        node_id = IMCP_MGMT_GW_CONFIG_INFO_ID;
        node_len = (unsigned short)(packet_length);
        IMCP_MSG_PROCESS_SEND_ID_AND_LEN(p,node_id,node_len);

   memcpy(p, _MAC_ADDRESS, 6);
   p += 6;

   *p = TYPE_RESPONSE;
   p++;

   *p = (unsigned char)(response_status);
   p++;

   packet_size = (p - buffer);

   dump_bytes(buffer, packet_size);
   ret = send_packet(buffer, packet_size, client_addr);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"CONFIGD : INFO : IMCP_MGMT_GW_CONFIG_RESPONSE packet sent (Bytes = %d): %s : %d\n",ret,__func__,__LINE__);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit: %s: %d\n",__func__,__LINE__);

   return 0;
}

int imcp_mgmt_gw_info_helper(unsigned char *buffer, struct sockaddr_in *client_addr)
{
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter: %s: %d\n",__func__,__LINE__);
	unsigned short int node_id;
	unsigned short	int node_len;
	unsigned char		 tmp;
	unsigned char		 *p;
	unsigned char		 mgmt_gw_enable = 0;
	unsigned char		 addr_len = 0;
	unsigned short	int cert_len = 0, key_len = 0;
	unsigned char		 server_addr[128] = {0};
	unsigned char		 certificates[1024] = {0};
	unsigned char		 private_key[1024] = {0};
	int					 alconf_handle;
	FILE					 *fp;

	p = buffer;
	p += get_imcp_signature_mesh_id_size() + 2 /*Node id*/ + 2 /*Node len*/ + 6 /*Board Mac*/;

	mgmt_gw_enable	= *p;	
	p += 1;

	addr_len = *p;
	p += sizeof(addr_len);

	memcpy(server_addr, p, addr_len);
	p += addr_len;

	cert_len = *((unsigned short int *)p);
	p += sizeof(unsigned short int);

	memcpy(certificates, p, cert_len);
	p += cert_len;

	key_len = *((unsigned short int *)p);
	p += sizeof(unsigned short int);

	memcpy(private_key, p, key_len);
	p += key_len;

	alconf_handle = libalconf_read_config_data();

	if(alconf_handle != -1)
	{
		/* Writing mgmt_gw_enable, mgmt_gw_addr and mgmt_gw_certificates path to meshap.conf*/
		al_conf_set_mgmt_gw_enable(AL_CONTEXT alconf_handle, mgmt_gw_enable);			
		al_conf_set_mgmt_gw_addr(AL_CONTEXT alconf_handle, server_addr);
		al_conf_set_mgmt_gw_certificates(AL_CONTEXT alconf_handle, "/opt/mesh_mgmt.crt:/opt/mesh_mgmt.key");
		al_conf_put(alconf_handle);
		al_conf_close(AL_CONTEXT alconf_handle);
	}
	else
	{
	   	al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : al_conf_handle error : %s: %d\n",__func__,__LINE__);
		send_mgmt_gw_config_updated_reply(client_addr, 0);
	}
	fp = fopen("/tmp/mesh_mgmt.crt", "wb");
	if(fp == NULL)
	{
	   	al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : error opening mgmt certificate file : %s: %d\n",__func__,__LINE__);
		perror("error opening mgmt certificate file");
		send_mgmt_gw_config_updated_reply(client_addr, 0);
		return 1;
	}
	
	if( fwrite(certificates, 1, cert_len, fp) != cert_len)
	{
	   	al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : error writting mgmt certificate file: %s: %d\n",__func__,__LINE__);
		perror("error writting mgmt certificate file");
		send_mgmt_gw_config_updated_reply(client_addr, 0);
		fclose(fp);
		return 1;
	}
	fclose(fp);

	fp = fopen("/tmp/mesh_mgmt.key", "wb");
	if(fp == NULL)
	{
	   	al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : error opening mgmt key file : %s: %d\n",__func__,__LINE__);
		perror("error opening mgmt key file");
		send_mgmt_gw_config_updated_reply(client_addr, 0);
		return 1;
	}
	
	if( fwrite(private_key, 1, key_len, fp) != key_len)
   {
	   	al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : error writting mgmt key file : %s: %d\n",__func__,__LINE__);
		perror("error writting mgmt key file");
		send_mgmt_gw_config_updated_reply(client_addr, 0);
      		fclose(fp);
		return 1;
   }
	fclose(fp);
	send_mgmt_gw_config_updated_reply(client_addr, 1);
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit: %s: %d\n",__func__,__LINE__);
	return 0;
}

int send_hw_info_reply(unsigned char *buf, struct sockaddr_in *client_addr)
{
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter: %s: %d\n",__func__,__LINE__);
	int fd ;
	tddi_packet_t*      packet;
	unsigned char       buffer[sizeof(tddi_packet_t)+sizeof(int)];
	int send_status;
	int tddi_pkt_size;
	int ret_val=0;
	unsigned char *buffer_ptr;
	unsigned char mac_address[7];

	buffer_ptr = buf + get_imcp_signature_mesh_id_size();
	
	memcpy(mac_address, buffer_ptr, 6);
	buffer_ptr += 6;

	packet = (tddi_packet_t *)buffer;

	fd  = open(TDDI_FILE_NAME,O_RDWR);
	if(fd == -1) {
	   	al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR :  Error opening %s : %s: %d\n",TDDI_FILE_NAME,__func__,__LINE__);
		return -1;
	}

	tddi_pkt_size = sizeof(tddi_packet_t);
	memset(packet,0,tddi_pkt_size);

	/* Create node type request packet */
	packet->signature   = TDDI_PACKET_SIGNATURE;
	packet->version     = TDDI_CURRENT_VERSION;
	packet->data_size   = 0;
	packet->type        = TDDI_PACKET_TYPE_REQUEST;
	packet->sub_type    = TDDI_REQUEST_TYPE_SEND_HW_INFO;

	/* write node type request */
	ret_val = write(fd,buffer,sizeof(tddi_packet_t));
	if ( ret_val != sizeof(tddi_packet_t))
   {
	   	al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR :  Failed to write packet to TDDI device %s: %d\n",__func__,__LINE__);
		close(fd);
		return -1;
   }

	lseek(fd,0,SEEK_CUR);

	/* read response packet */
	memset(buffer,0x00,sizeof(buffer));
	ret_val = read(fd,buffer,sizeof(tddi_packet_t)+sizeof(int));
	if ( ret_val != (sizeof(tddi_packet_t)+ sizeof(int)))
   {
	   	al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR :  Failed to read packet from TDDI device  %s: %d\n",__func__,__LINE__);
		close(fd);
		return -1;
	}

	/* close tddi device file descriptor */
	close(fd);
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit: %s: %d\n",__func__,__LINE__);
	return 0;
}

int send_station_info_reply(unsigned char *buf, struct sockaddr_in *client_addr,unsigned int sta_info_type)
{
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter: %s: %d\n",__func__,__LINE__);
	int fd ;
	tddi_packet_t*      packet;
	tddi_station_info_request_t  *sta_info_request;
	unsigned char       buffer[sizeof(tddi_packet_t)+sizeof(tddi_station_info_request_t)];
	int send_status;
	int tddi_pkt_size;
	int ret_val=0;
	unsigned char *buffer_ptr;

	al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"CONFIGD : INFO : Packet Receiver STATION INFO : %s : %d\n",__func__,__LINE__);

	packet = (tddi_packet_t *)buffer;
	sta_info_request        = (tddi_station_info_request_t*)(buffer + sizeof(tddi_packet_t));

	/* pointing to packet data */
	buffer_ptr = buf + get_imcp_signature_mesh_id_size();

	memcpy(sta_info_request->ds_mac,buffer_ptr,6);
	buffer_ptr += 6;

	/* extracting mac addresses */
	al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"CONFIGD : INFO : STA MAC : DS MAC : %x %x %x %x %x %x : %s: %d\n",sta_info_request->ds_mac[0],
			sta_info_request->ds_mac[1],
			sta_info_request->ds_mac[2],
			sta_info_request->ds_mac[3],
			sta_info_request->ds_mac[4],
			sta_info_request->ds_mac[5],__func__,__LINE__);

	if ( sta_info_type == STA_INFO_PKT_TYPE_1 )
	{
		memcpy(sta_info_request->sta_mac,buffer_ptr,6);
		buffer_ptr += 6;

		sta_info_request->sta_info_pkt_type = STA_INFO_PKT_TYPE_1;

		al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"CONFIGD : INFO : STA MAC : %x %x %x %x %x %x : %s: %d\n",sta_info_request->sta_mac[0],
                                sta_info_request->sta_mac[1],
                                sta_info_request->sta_mac[2],
                                sta_info_request->sta_mac[3],
                                sta_info_request->sta_mac[4],
                                sta_info_request->sta_mac[5],__func__,__LINE__);

	}
	/* open tddi device for stationinfo request */
	fd  = open(TDDI_FILE_NAME,O_RDWR);
	if(fd == -1) {
	   	al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : Error opening %s : %s: %d\n",TDDI_FILE_NAME,__func__,__LINE__);
		return -1;
	}

	tddi_pkt_size = sizeof(tddi_packet_t);
	memset(packet,0,tddi_pkt_size);

	/* Create node type request packet */
	packet->signature   = TDDI_PACKET_SIGNATURE;
	packet->version     = TDDI_CURRENT_VERSION;
	packet->data_size   = sizeof(tddi_station_info_request_t);
	packet->type        = TDDI_PACKET_TYPE_REQUEST;
	packet->sub_type    = TDDI_REQUEST_TYPE_SEND_STATION_INFO;

	/* write node type request */
	ret_val = write(fd,buffer,(sizeof(tddi_packet_t)+sizeof(tddi_station_info_request_t)));
	if ( ret_val != (sizeof(tddi_packet_t)+sizeof(tddi_station_info_request_t)))
	{
	   	al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : Failed to write packet to TDDI device : %s: %d\n",__func__,__LINE__);
		close(fd);
		return -1;
	}

	lseek(fd,0,SEEK_CUR);
	/* read response packet */
	memset(buffer,0x00,sizeof(buffer));
	ret_val = read(fd,buffer,sizeof(tddi_packet_t)+sizeof(int));
	if ( ret_val != (sizeof(tddi_packet_t)+ sizeof(int)))
	{
	   	al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : Failed to read packet from TDDI device : %s: %d\n",__func__,__LINE__);
		close(fd);
		return -1;
	}

	/* close tddi device file descriptor */
	close(fd);
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit: %s: %d\n",__func__,__LINE__);
	return 0;
}
