#include <stdio.h>
#include "dump.h"

#ifdef CONFIGD_DEBUG

int dump_start_ip_if_packet(imcp_start_ip_if_data_t *imcp_start_ip_if_data)
{
   printf("\nMAC ADDRESS = %02x:%02x:%02x:%02x:%02x:%02x", imcp_start_ip_if_data->mac_address[0],
          imcp_start_ip_if_data->mac_address[1],
          imcp_start_ip_if_data->mac_address[2],
          imcp_start_ip_if_data->mac_address[3],
          imcp_start_ip_if_data->mac_address[4],
          imcp_start_ip_if_data->mac_address[5]
          );

   printf("\nNL = %d", imcp_start_ip_if_data->interface_name_length);
   printf("\nNAME = %s", imcp_start_ip_if_data->interface_name);

   return 0;
}


int dump_ipconfig_request_response_packet(imcp_ipconfig_rr_data_t *imcp_ipconfig_rr_data)
{
   printf("\nMAC ADDRESS = %02x:%02x:%02x:%02x:%02x:%02x", imcp_ipconfig_rr_data->mac_address[0],
          imcp_ipconfig_rr_data->mac_address[1],
          imcp_ipconfig_rr_data->mac_address[2],
          imcp_ipconfig_rr_data->mac_address[3],
          imcp_ipconfig_rr_data->mac_address[4],
          imcp_ipconfig_rr_data->mac_address[5]
          );

   printf("\nTYPE = %d", imcp_ipconfig_rr_data->request_response_type);
   return 0;
}


int dump_ipconfig_info_packet(imcp_ipconfig_data_t *imcp_ipconfig_data)
{
   printf("\nMAC ADDRESS = %02x:%02x:%02x:%02x:%02x:%02x", imcp_ipconfig_data->mac_address[0],
          imcp_ipconfig_data->mac_address[1],
          imcp_ipconfig_data->mac_address[2],
          imcp_ipconfig_data->mac_address[3],
          imcp_ipconfig_data->mac_address[4],
          imcp_ipconfig_data->mac_address[5]
          );

   printf("\nTYPE = %d", imcp_ipconfig_data->request_response_type);
   printf("\nNL = %d", imcp_ipconfig_data->host_name_length);
   printf("\nHOSTNAME = %s", imcp_ipconfig_data->host_name);

   printf("\nIP ADDRESS = %d.%d.%d.%d", imcp_ipconfig_data->ip_address[0],
          imcp_ipconfig_data->ip_address[1],
          imcp_ipconfig_data->ip_address[2],
          imcp_ipconfig_data->ip_address[3]
          );

   printf("\nNETMASK = %d.%d.%d.%d", imcp_ipconfig_data->subnet_mask[0],
          imcp_ipconfig_data->subnet_mask[1],
          imcp_ipconfig_data->subnet_mask[2],
          imcp_ipconfig_data->subnet_mask[3]
          );

   printf("\nGATEWAY = %d.%d.%d.%d", imcp_ipconfig_data->gateway[0],
          imcp_ipconfig_data->gateway[1],
          imcp_ipconfig_data->gateway[2],
          imcp_ipconfig_data->gateway[3]
          );

   return 0;
}


int dump_apconfig_request_response_packet(imcp_apconfig_rr_data_t *imcp_apconfig_rr_data)
{
   printf("\nMAC ADDRESS = %02x:%02x:%02x:%02x:%02x:%02x", imcp_apconfig_rr_data->mac_address[0],
          imcp_apconfig_rr_data->mac_address[1],
          imcp_apconfig_rr_data->mac_address[2],
          imcp_apconfig_rr_data->mac_address[3],
          imcp_apconfig_rr_data->mac_address[4],
          imcp_apconfig_rr_data->mac_address[5]
          );

   printf("\nTYPE = %d", imcp_apconfig_rr_data->request_response_type);
   return 0;
}


int dump_apconfig_packet(imcp_apconfig_data_t *imcp_apconfig_data)
{
   int i;

   printf("\nMAC ADDRESS=%02x:%02x:%02x:%02x:%02x:%02x", imcp_apconfig_data->mac_address[0],
          imcp_apconfig_data->mac_address[1],
          imcp_apconfig_data->mac_address[2],
          imcp_apconfig_data->mac_address[3],
          imcp_apconfig_data->mac_address[4],
          imcp_apconfig_data->mac_address[5]
          );

   printf("\nSIGNAL MAP=%d:%d:%d:%d:%d:%d:%d:%d", imcp_apconfig_data->signal_map[0], imcp_apconfig_data->signal_map[1],
          imcp_apconfig_data->signal_map[2], imcp_apconfig_data->signal_map[3],
          imcp_apconfig_data->signal_map[4], imcp_apconfig_data->signal_map[5],
          imcp_apconfig_data->signal_map[6], imcp_apconfig_data->signal_map[7]
          );


   printf("\nHBI	=%d", imcp_apconfig_data->heartbeat_interval);
   printf("\nHBMC	=%d", imcp_apconfig_data->heartbeat_miss_count);
   printf("\nHC	=%d", imcp_apconfig_data->hop_cost);
   printf("\nMAH	=%d", imcp_apconfig_data->max_allowable_hops);
   printf("\nLASI	=%d", imcp_apconfig_data->las_interval);
   printf("\nCRT	=%d", imcp_apconfig_data->cr_threshold);
   printf("\nDCA	=%d", imcp_apconfig_data->dca);
   printf("\nESSID	=%s", imcp_apconfig_data->essid);


   printf("\nDCA LIST (%d){", imcp_apconfig_data->dca_channel_count);
   for (i = 0; i < imcp_apconfig_data->dca_channel_count; i++)
   {
      printf("\n%d", imcp_apconfig_data->dca_channel_list[i]);
   }
   printf("\n}", imcp_apconfig_data->dca);

   printf("\nNAME	=%s", imcp_apconfig_data->name);
   printf("\nRTS TH  =%u", imcp_apconfig_data->rts_threshold);
   printf("\nFRAG TH    =%u", imcp_apconfig_data->frag_threshold);
   printf("\nBI   =%u", imcp_apconfig_data->beacon_interval);

   printf("\nPREFERRED PARENT =%02x:%02x:%02x:%02x:%02x:%02x", imcp_apconfig_data->preferred_parent[0],
          imcp_apconfig_data->preferred_parent[1],
          imcp_apconfig_data->preferred_parent[2],
          imcp_apconfig_data->preferred_parent[3],
          imcp_apconfig_data->preferred_parent[4],
          imcp_apconfig_data->preferred_parent[5]
          );

   return 0;
}


int dump_reset_packet(imcp_reset_data_t *imcp_reset_data)
{
   printf("\nMAC ADDRESS = %02x:%02x:%02x:%02x:%02x:%02x", imcp_reset_data->mac_address[0],
          imcp_reset_data->mac_address[1],
          imcp_reset_data->mac_address[2],
          imcp_reset_data->mac_address[3],
          imcp_reset_data->mac_address[4],
          imcp_reset_data->mac_address[5]
          );

   printf("\nRESET TYPE = %d", imcp_reset_data->reset_type);

   return 0;
}


#else

#define dump_reset_packet    (x)0
#define dump_apconfig_packet(x)                  0
#define dump_apconfig_packet(x)                  0
#define dump_ipconfig_info_packet(x)             0
#define dump_ipconfig_info_packet(x)             0
#define dump_start_ip_if_packet(x)               0
#define dump_imcp_sw_update_request_packet(x)    0
#endif
