/********************************************************************************
 * MeshDynamics
 * --------------
 * File     : hostapd_conf.c
 * Comments : This file contains functions for setting iwconfig parameters and iw configuration
 *            file creation
 *
 * Created  : 10/30/2014
 * Author   : Sukshata Ningadalli
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
 **********************************************************************************/

#include <string.h>
#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>

#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include "al.h"
#include "al_context.h"
#include "al_impl_context.h"

#include "al_conf.h"
#include "al_conf_lib.h"
#include "configd.h"
#include "meshap_util.h"
#include "acl_conf.h"
#define MAC2STR(a)    (a)[0], (a)[1], (a)[2], (a)[3], (a)[4], (a)[5]
#define MACSTR        "%02x:%02x:%02x:%02x:%02x:%02x"

#define ARRAY_SIZE(a) (sizeof(a) / sizeof((a)[0]))

static unsigned char mac[6] = { 74, 78, 78, 80, 81, 82 };
int hostapd_is_N_or_AC_supported_interface(int phy_sub_type);
int hostapd_meshap_set_driver(char *buffer);
int hostapd_meshap_set_ieee80211h(char *buffer);
int hostapd_meshap_set_essid(char *buffer, al_conf_if_info_t *if_info);
int hostapd_meshap_set_rts_th(char *buffer, al_conf_if_info_t *if_info);
int hostapd_meshap_set_frag_th(char *buffer, al_conf_if_info_t *if_info);
int hostapd_meshap_set_preamble(char *buffer, al_conf_if_info_t *if_info);
int hostapd_meshap_set_beacon_interval(char *buffer, al_conf_if_info_t *if_info);
int hostapd_meshap_set_country_code(char *buffer, int country_code);
int hostapd_meshap_set_if_name(char *buffer, al_conf_if_info_t *if_info);
int hostapd_meshap_set_channel(char *buffer, al_conf_if_info_t *if_info, int channel);
int hostapd_meshap_set_dca_list(char *buffer, al_conf_if_info_t *if_info, char **conf_string);
int hostapd_meshap_set_phy_sub_type(char *buffer, al_conf_if_info_t *if_info);
int hostapd_meshap_set_hide_ssid(char *buffer, al_conf_if_info_t *if_info);
int hostapd_meshap_set_security_info(char **conf_string, al_conf_if_info_t *if_info, al_security_info_t *security_info);
int hostapd_meshap_set_dot11e_info(char **conf_string, dot11e_conf_category_t *dot11e_data);
int hostapd_meshap_set_vendor_info(char *buffer);
int hostapd_meshap_set_acl(char *buffer, _al_conf_data_t *data, acl_conf_t *acl_data, al_conf_if_info_t *if_info, int vlan_index);
//VLAN
int hostapd_meshap_set_bss(char* buffer,_al_conf_data_t* data, int vlan_index);
int hostapd_meshap_set_ssid(char* buffer,_al_conf_data_t* data, int vlan_index);
int hostapd_meshap_set_bssid(char* buffer,_al_conf_data_t* data, int vlan_index, unsigned char *mac);
#define PHY_IF_COUNT 6
//VLAN_END
int create_hostapd_config_file(_al_conf_data_t *data, acl_conf_t *acl_data, dot11e_conf_category_t *dot11e_data, al_conf_if_info_t *if_info,
                               interface_map_t interface_map, int index, int channel);

int mesh_start_hostapd(interface_map_t interface_map, int flag, int *hostapd_mask, int start_hostapd);
int hostapd_meshap_set_ieee80211n(char *buffer ,al_conf_if_info_t *if_info);
int hostapd_meshap_set_wmm_enabled(char *buffer ,al_conf_if_info_t *if_info);
int hostapd_meshap_set_ht_capab(char **conf_string, al_conf_if_info_t *if_info, al_ht_capab_t *ht_capab_info);
int hostapd_meshap_set_require_ht(char *buffer ,al_conf_if_info_t *if_info);
int hostapd_meshap_set_obss_interval(char *buffer ,al_conf_if_info_t *if_info);

//RAMESH16MIG adding func declaration
int al_decode_key(AL_CONTEXT_PARAM_DECL unsigned char *buffer, unsigned char *mac_address, unsigned char *output);
int hostapd_meshap_set_ieee80211ac(char *buffer ,al_conf_if_info_t *if_info);
//int hostapd_meshap_set_wmm_enabled(char *buffer ,al_conf_if_info_t *if_info); <Need  to discuss furthur>
int hostapd_meshap_set_vht_capab(char **conf_string, al_conf_if_info_t *if_info, al_vht_capab_t *vht_capab_info);
int hostapd_meshap_set_require_vht(char *buffer ,al_conf_if_info_t *if_info);
int hostapd_meshap_set_vht_oper_chwidth(char *buffer ,al_conf_if_info_t *if_info, al_vht_capab_t *vht_capab_info);
int hostapd_meshap_set_vht_oper_centr_freq_seg0_idx(char *buffer ,al_conf_if_info_t *if_info, al_vht_capab_t *vht_capab_info);
int hostapd_meshap_set_vht_oper_centr_freq_seg1_idx(char *buffer ,al_conf_if_info_t *if_info, al_vht_capab_t *vht_capab_info);
extern unsigned char *_WLAN2MAC_ADDRESS;
extern unsigned char *_VIRTUALMAC_ADDRESS;
extern unsigned char *_VLANMAC_ADDRESS;

mesh_vectors_t hostapd_ops =
{
   set_driver:                 hostapd_meshap_set_driver,
   set_ieee80211h:             hostapd_meshap_set_ieee80211h,
   set_essid :                 hostapd_meshap_set_essid,
   set_vendor_info:            hostapd_meshap_set_vendor_info,
   set_rts_th :                hostapd_meshap_set_rts_th,
   set_frag_th :               hostapd_meshap_set_frag_th,
   set_preamble :              hostapd_meshap_set_preamble,
   set_beacon_interval :       hostapd_meshap_set_beacon_interval,
   set_country_code :          hostapd_meshap_set_country_code,
   set_if_name :               hostapd_meshap_set_if_name,
   set_channel :               hostapd_meshap_set_channel,
   set_dca_list:               hostapd_meshap_set_dca_list,
   set_phy_sub_type :          hostapd_meshap_set_phy_sub_type,
   set_hide_ssid :             hostapd_meshap_set_hide_ssid,
   set_security_info :         hostapd_meshap_set_security_info,
   set_dot11e_info :           hostapd_meshap_set_dot11e_info,
   set_ieee80211n:             hostapd_meshap_set_ieee80211n,
   set_wmm_enabled:            hostapd_meshap_set_wmm_enabled,
   set_ht_capab:               hostapd_meshap_set_ht_capab,
   set_require_ht:             hostapd_meshap_set_require_ht,
   set_obss_interval:          hostapd_meshap_set_obss_interval,
   set_ieee80211ac:            hostapd_meshap_set_ieee80211ac,
   set_vht_capab:              hostapd_meshap_set_vht_capab,
   set_require_vht:            hostapd_meshap_set_require_vht,
   set_vht_oper_chwidth:       hostapd_meshap_set_vht_oper_chwidth,
   set_vht_oper_centr_freq_seg0_idx:       hostapd_meshap_set_vht_oper_centr_freq_seg0_idx,
   set_vht_oper_centr_freq_seg1_idx:       hostapd_meshap_set_vht_oper_centr_freq_seg1_idx,
   set_acl:                    hostapd_meshap_set_acl,
//VLAN
   set_bss     :               hostapd_meshap_set_bss,
   set_ssid    :               hostapd_meshap_set_ssid,
   set_bssid   :               hostapd_meshap_set_bssid,
//VLAN_END
};

int hostapd_is_N_or_AC_supported_interface(int phy_mode)
{
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

    switch (phy_mode){
        case AL_CONF_IF_PHY_SUB_TYPE_802_11_N_2_4GHZ:
        case AL_CONF_IF_PHY_SUB_TYPE_802_11_N_5GHZ:
        case AL_CONF_IF_PHY_SUB_TYPE_802_11_AN:
        case AL_CONF_IF_PHY_SUB_TYPE_802_11_BGN:
            return AL_PHY_SUB_TYPE_SUPPORT_ONLY_N ;

        case AL_CONF_IF_PHY_SUB_TYPE_802_11_AC:
            return AL_PHY_SUB_TYPE_SUPPORT_ONLY_AC ;


        case AL_CONF_IF_PHY_SUB_TYPE_802_11_ANAC:
            return AL_PHY_SUB_TYPE_SUPPORT_BOTH_N_AND_AC ;

        default :
            return AL_PHY_SUB_TYPE_NOT_SUPPORT_N_OR_AC ;
    }
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit: %s<%d>\n",__func__,__LINE__);

}


int hostapd_meshap_set_dot11e_info(char **conf_string, dot11e_conf_category_t *dot11e_data)
{
   printf("%s %d\n", __func__, __LINE__);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter: %s<%d>\n",__func__,__LINE__);
   int i, ret;

   char          buffer[BUFFER_SIZE];
   unsigned char buffer1[BUFFER_SIZE];
   char          *ret_buffer;
   char          *p;

   ret_buffer = *conf_string;

   p = ret_buffer;


   printf("acwcin%d", dot11e_data->category_info[0].acwmin);

   int k = 3;
   for (i = 0; i < dot11e_data->count; i++)
   {
      memset(buffer, 0, BUFFER_SIZE);

      ret = sprintf(buffer, "tx_queue_data%d_aifs=%u", k, dot11e_data->category_info[i].aifsn);
      memcpy(p, buffer, strlen(buffer));
      p += ret;
      *p = '\n';
      p++;


      memset(buffer, 0, ret);
      ret = sprintf(buffer, "tx_queue_data%d_cwmin=%u", k, dot11e_data->category_info[i].acwmin);
      memcpy(p, buffer, strlen(buffer));
      p += ret;
      *p = '\n';
      p++;

      memset(buffer, 0, BUFFER_SIZE);
      ret = sprintf(buffer, "tx_queue_data%d_cwmax=%u", k, dot11e_data->category_info[i].acwmax);
      memcpy(p, buffer, strlen(buffer));
      p += ret;
      *p = '\n';
      p++;


      memset(buffer, 0, ret);
      ret = sprintf(buffer, "tx_queue_data%d_burst=%u", k, dot11e_data->category_info[i].burst_time);
      memcpy(p, buffer, strlen(buffer));
      p += ret;
      *p = '\n';
      p++;

      k--;
   }
   *conf_string = p;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return 0;
}


/* SET FUNCTIONS FOR HOSTAPD */
int hostapd_meshap_set_driver(char *buffer)
{
   int ret;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   ret = sprintf(buffer, "driver=nl80211");
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return ret;
}


int hostapd_meshap_set_ieee80211h(char *buffer)
{
   int ret;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   ret = sprintf(buffer, "ieee80211h=1");
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return ret;
}


int hostapd_meshap_set_vendor_info(char *buffer)
{
   int ret;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   ret = sprintf(buffer, "vendor_elements=%s", _vendor_info_header);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return ret;
}


int hostapd_meshap_set_essid(char *buffer, al_conf_if_info_t *if_info)
{
   int ret;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   ret = sprintf(buffer, "ssid=%s", if_info->essid);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return ret;
}


int hostapd_meshap_set_rts_th(char *buffer, al_conf_if_info_t *if_info)
{
   int ret;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   ret = sprintf(buffer, "rts_threshold=%d", if_info->rts_th);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return ret;
}


int hostapd_meshap_set_frag_th(char *buffer, al_conf_if_info_t *if_info)
{
   int ret;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   ret = sprintf(buffer, "fragm_threshold=%d", if_info->frag_th);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return ret;
}


int hostapd_meshap_set_preamble(char *buffer, al_conf_if_info_t *if_info)
{
   int ret;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   ret = sprintf(buffer, "preamble=%u", if_info->preamble_type);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return ret;      /* 1408MIG */
}


int hostapd_meshap_set_beacon_interval(char *buffer, al_conf_if_info_t *if_info)
{
   int ret;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   ret = sprintf(buffer, "beacon_int=%d", if_info->beacon_interval);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return ret;
}


int hostapd_meshap_set_country_code(char *buffer, int country_code)
{
   int ret;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter: %s<%d>\n",__func__,__LINE__);


   switch (country_code)
   {
   case 36:
      ret = sprintf(buffer, "country_code=AU");
      break;

   case 156:
      ret = sprintf(buffer, "country_code=CN");
      break;

   case 276:
      ret = sprintf(buffer, "country_code=DE");
      break;

   case 380:
      ret = sprintf(buffer, "country_code=IT");
      break;

   case 410:
      ret = sprintf(buffer, "country_code=KP");          //need to check korea_roc
      break;

   case 411:
      ret = sprintf(buffer, "country_code=KR");          //need to check korea_roc2
      break;

   case 484:
      ret = sprintf(buffer, "country_code=MX");
      break;

   case 554:
      ret = sprintf(buffer, "country_code=NZ");
      break;

   case 408:
      ret = sprintf(buffer, "country_code=KP");          //NORTH_KOREA
      break;

   case 724:
      ret = sprintf(buffer, "country_code=ES");
      break;

   case 826:
      ret = sprintf(buffer, "country_code=GB");
      break;

   case 840:
      ret = sprintf(buffer, "country_code=US");
      break;

   default:
      ret = sprintf(buffer, "country_code=00");
   }
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return ret;
}


int hostapd_meshap_set_if_name(char *buffer, al_conf_if_info_t *if_info)
{
   int ret;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   ret = sprintf(buffer, "interface=%s", if_info->name);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return ret;
}


int hostapd_meshap_set_channel(char *buffer, al_conf_if_info_t *if_info, int channel)
{
   int ret;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   if (channel)
	{
      ret = sprintf(buffer, "channel=%d", channel);
		if_info->wm_channel = channel;
	}
   else
      ret = sprintf(buffer, "channel=%d", if_info->wm_channel);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return ret;
}


int hostapd_meshap_set_dca_list(char *buffer, al_conf_if_info_t *if_info, char **conf_string)
{
   int  i = 0, ret = 0;
   char *ret_buffer;
   char *p;
   char tmp1[32];
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   ret_buffer = *conf_string;

   p = ret_buffer;

   if (if_info->dca)
   {
      sprintf(buffer, "chanlist=");
      ret = strlen(buffer);
      memcpy(p, buffer, ret);
      p += ret;
      for (i = 0; i < if_info->dca_list_count; i++)
      {
         sprintf(buffer, "%d", if_info->dca_list[i]);
         ret = strlen(buffer);
         memcpy(p, buffer, ret);
         p += ret;
         strncpy(tmp1, " ", 5);
         ret = strlen(tmp1);
         memcpy(p, tmp1, ret);
         p += ret;
      }
   }
   *p = '\n';
   p++;
   *conf_string = p;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return 0;
}


int hostapd_meshap_set_phy_sub_type(char *buffer, al_conf_if_info_t *if_info)
{
   int ret;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   switch (if_info->phy_sub_type)
   {
   case AL_CONF_IF_PHY_SUB_TYPE_802_11_A:
      ret = sprintf(buffer, "hw_mode=a\n");
      break;

   case AL_CONF_IF_PHY_SUB_TYPE_802_11_B:
      ret = sprintf(buffer, "hw_mode=b\n");
      break;

   case AL_CONF_IF_PHY_SUB_TYPE_802_11_G:
      ret = sprintf(buffer, "hw_mode=g\n");
      break;

   case AL_CONF_IF_PHY_SUB_TYPE_802_11_BG:
      ret = sprintf(buffer, "hw_mode=g\n");
      break;
#if 0//Archana added
   case AL_CONF_IF_PHY_SUB_TYPE_802_11_N:
      if(if_info->wm_channel){
          if ((if_info->wm_channel <= 14 && if_info->wm_channel >= 1) ){
              ret = sprintf(buffer, "hw_mode=g\n");
          }else{
              ret = sprintf(buffer, "hw_mode=a\n");
          }
      }
      else if (!if_info->wm_channel ){
          if ((if_info->dca_list[0] <= 14 && if_info->dca_list[0] >= 1) ){
              ret = sprintf(buffer, "hw_mode=g\n");
          }else{
              ret = sprintf(buffer, "hw_mode=a\n");
          }
      }
      break;
#endif
   case AL_CONF_IF_PHY_SUB_TYPE_802_11_N_2_4GHZ:
      ret = sprintf(buffer, "hw_mode=g\n");
      break;

   case AL_CONF_IF_PHY_SUB_TYPE_802_11_N_5GHZ:
      ret = sprintf(buffer, "hw_mode=a\n");
      break;

   case AL_CONF_IF_PHY_SUB_TYPE_802_11_AN:
      ret = sprintf(buffer, "hw_mode=a\n");
      break;

   case AL_CONF_IF_PHY_SUB_TYPE_802_11_BGN:
      ret = sprintf(buffer, "hw_mode=g\n");
      break;

   case AL_CONF_IF_PHY_SUB_TYPE_802_11_AC:
      ret = sprintf(buffer, "hw_mode=a\n");
      break;

   case AL_CONF_IF_PHY_SUB_TYPE_802_11_ANAC:
      ret = sprintf(buffer, "hw_mode=a\n");
      break;

   default:
      ret = sprintf(buffer, "medium_sub_type=x\n");
   }

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return ret;
}


int hostapd_meshap_set_hide_ssid(char *buffer, al_conf_if_info_t *if_info)
{
   int ret;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   ret = sprintf(buffer, "ignore_broadcast_ssid=%u", if_info->hide_ssid);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return ret;
}

int hostapd_meshap_set_acl(char *buffer, _al_conf_data_t *data, acl_conf_t *acl_data, al_conf_if_info_t *if_info, int vlan_index)
{
   char *buffer_out = buffer;
   char bcast[MAC_ADDRESS_LENGTH] = {0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF };
   int deny_mac = 0;
   char accept_mac_string[1024];
   char deny_mac_string[1024];
   char accept_mac_file[128];
   char deny_mac_file[128];
   char acl_buffer[128];
   int len;
   int tag;
   int ret;
   int handle;
   char *accept_mac_ptr = accept_mac_string;
   char *deny_mac_ptr = deny_mac_string;
   int i;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   if (if_info->service == AL_CONF_IF_SERVICE_BACKHAUL_ONLY)
   {
      ret = sprintf(buffer, "macaddr_acl=%u", if_info->service);
   }
   else
   {
      for (i = 0; i < acl_data->count; i++)
      {
         if (!memcmp(bcast, acl_data->info[i].sta_mac, MAC_ADDRESS_LENGTH) && acl_data->info[i].allow == 0)
         {
            deny_mac = 1;
            break;
         }
      }

      ret = sprintf(buffer_out, "macaddr_acl=%u", deny_mac ? 1 : 0);
      buffer_out += ret;

      if (vlan_index == -1)
      {
         tag = ACL_CONF_VLAN_UNTAGGED;
         sprintf(accept_mac_file, "/etc/accept_mac_default");
         sprintf(deny_mac_file, "/etc/deny_mac_default");
      }
      else
      {
         tag = data->vlan_info[vlan_index].tag;
         sprintf(accept_mac_file, "/etc/accept_mac_vlan_%d", data->vlan_info[vlan_index].tag);
         sprintf(deny_mac_file, "/etc/deny_mac_vlan_%d", data->vlan_info[vlan_index].tag);
      }

      ret = sprintf(buffer_out, "\naccept_mac_file=%s\ndeny_mac_file=%s", accept_mac_file, deny_mac_file);
      buffer_out += ret;

      len = sprintf(accept_mac_string,"# Accept MAC address list\n");
      accept_mac_ptr += len;
      len = sprintf(deny_mac_string,"# Deny MAC address list\n");
      deny_mac_ptr += len;

      for (i = 0; i < acl_data->count; i++)
      {
         if (tag == acl_data->info[i].vlan_tag)
          {
             len = sprintf(acl_buffer, MACSTR"\n", MAC2STR(acl_data->info[i].sta_mac));

             if (acl_data->info[i].allow)
             {
                memcpy(accept_mac_ptr, acl_buffer, len);
                accept_mac_ptr += len;
             }
             else
             {
                if (memcmp(bcast, acl_data->info[i].sta_mac, MAC_ADDRESS_LENGTH))
                {
                   memcpy(deny_mac_ptr, acl_buffer, len);
                   deny_mac_ptr += len;
                }
             }
             memset(acl_buffer, 0, len);
          }
      }

      handle = conf_open_file(accept_mac_file);
      if (handle >= 0)
      {
         al_write_file(AL_CONTEXT handle, accept_mac_string, accept_mac_ptr - accept_mac_string);
         al_close_file(AL_CONTEXT handle);
      }

      handle = conf_open_file(deny_mac_file);
      if (handle >= 0)
      {
         al_write_file(AL_CONTEXT handle, deny_mac_string, deny_mac_ptr - deny_mac_string);
         al_close_file(AL_CONTEXT handle);
      }

      ret = buffer_out - buffer;
   }

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit: %s<%d>\n", __func__,__LINE__);
   return ret;
}

int hostapd_meshap_set_security_info(char **conf_string, al_conf_if_info_t *if_info, al_security_info_t *security_info)
{
   char buffer[BUFFER_SIZE];
   char wpa_psk[BUFFER_SIZE];
   char *ret_buffer;
   int  ret, i, j;
   char *p;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   ret_buffer = *conf_string;

   p = ret_buffer;


   if (security_info->_security_wep.enabled)
   {
      memset(buffer, 0, BUFFER_SIZE);
      ret = sprintf(buffer, "auth_algs=2");
      memcpy(p, buffer, strlen(buffer));
      p += ret;
      *p = '\n';
      p++;
      memset(buffer, 0, ret);
      ret = sprintf(buffer, "wep_default_key=%d,", security_info->_security_wep.key_index);
      memcpy(p, buffer, strlen(buffer));
      p += ret;
      *p = '\n';
      p++;

      memset(buffer, 0, ret);
      sprintf(buffer, "wep_key%d=", security_info->_security_wep.key_index);
      ret = strlen(buffer);
      memcpy(p, buffer, ret);
      p += ret;
      for (j = 0; j <= security_info->_security_wep.keys[security_info->_security_wep.key_index].len - 1; j++)
      {
         sprintf(buffer, "%02X", security_info->_security_wep.keys[security_info->_security_wep.key_index].bytes[j]);
         ret = strlen(buffer);
         memcpy(p, buffer, ret);
         p += ret;
      }
      *p = '\n';
      p++;
      *conf_string = p;
      al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
      return 0;
   }
   else if (security_info->_security_rsn_psk.enabled)
   {
      if (security_info->_security_rsn_psk.mode == 0)
      {
         memset(buffer, 0, BUFFER_SIZE);
         ret = sprintf(buffer, "wpa=1");
         memcpy(p, buffer, strlen(buffer));
         p += ret;
         *p = '\n';
         p++;
      }
      else if (security_info->_security_rsn_psk.mode == 1)
      {
         memset(buffer, 0, BUFFER_SIZE);
         ret = sprintf(buffer, "wpa=2");
         memcpy(p, buffer, strlen(buffer));
         p += ret;
         *p = '\n';
         p++;
      }

      memset(buffer, 0, BUFFER_SIZE);
      ret = sprintf(buffer, "wpa_group_rekey=%d", security_info->_security_rsn_psk.group_key_renewal);
      memcpy(p, buffer, strlen(buffer));
      p += ret;
      *p = '\n';
      p++;


      if (security_info->_security_rsn_psk.cipher_tkip)
      {
         memset(buffer, 0, ret);
         ret = sprintf(buffer, "wpa_pairwise=TKIP");
         memcpy(p, buffer, strlen(buffer));
         p += ret;
         *p = '\n';
         p++;

         memset(buffer, 0, ret);
         ret = sprintf(buffer, "rsn_pairwise=TKIP");
         memcpy(p, buffer, strlen(buffer));
         p += ret;
         *p = '\n';
         p++;
      }
      else if (security_info->_security_rsn_psk.cipher_ccmp)
      {
         memset(buffer, 0, ret);
         ret = sprintf(buffer, "wpa_pairwise=CCMP");
         memcpy(p, buffer, strlen(buffer));
         p += ret;
         *p = '\n';
         p++;

         memset(buffer, 0, ret);
         ret = sprintf(buffer, "rsn_pairwise=CCMP");
         memcpy(p, buffer, strlen(buffer));
         p += ret;
         *p = '\n';
         p++;
      }
      memset(buffer, 0, ret);
      ret = al_decode_key(AL_CONTEXT security_info->_security_rsn_psk.key_buffer, _MAC_ADDRESS, buffer);

      for (i = 0; i < 32; i++)
      {
         sprintf(&wpa_psk[i * 2], "%02x", buffer[i]);
      }

      ret = sprintf(buffer, "wpa_psk=%s", wpa_psk);
      memcpy(p, buffer, strlen(buffer));

      p += ret;
      *p = '\n';
      p++;
      *conf_string = p;
      al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
      return 0;
   }
   else if (security_info->_security_rsn_radius.enabled)
   {
      printf("entering radius routine\n");
      memset(buffer, 0, BUFFER_SIZE);

      if (security_info->_security_rsn_radius.mode == 0)
      {
         memset(buffer, 0, BUFFER_SIZE);
         ret = sprintf(buffer, "wpa=1");
         memcpy(p, buffer, strlen(buffer));
         p += ret;
         *p = '\n';
         p++;
      }
      else if (security_info->_security_rsn_radius.mode == 1)
      {
         memset(buffer, 0, BUFFER_SIZE);
         ret = sprintf(buffer, "wpa=2");
         memcpy(p, buffer, strlen(buffer));
         p += ret;
         *p = '\n';
         p++;
      }

      memset(buffer, 0, BUFFER_SIZE);
      ret = sprintf(buffer, "wpa_key_mgmt=WPA-EAP\nieee8021x=1");
      memcpy(p, buffer, strlen(buffer));
      p += ret;
      *p = '\n';
      p++;

      if (security_info->_security_rsn_radius.cipher_tkip)
      {
         memset(buffer, 0, ret);
         ret = sprintf(buffer, "wpa_pairwise=TKIP");
         memcpy(p, buffer, strlen(buffer));
         p += ret;
         *p = '\n';
         p++;

         memset(buffer, 0, ret);
         ret = sprintf(buffer, "rsn_pairwise=TKIP");
         memcpy(p, buffer, strlen(buffer));
         p += ret;
         *p = '\n';
         p++;
      }
      else if (security_info->_security_rsn_radius.cipher_ccmp)
      {
         memset(buffer, 0, ret);
         ret = sprintf(buffer, "wpa_pairwise=CCMP");
         memcpy(p, buffer, strlen(buffer));
         p += ret;
         *p = '\n';
         p++;

         memset(buffer, 0, ret);
         ret = sprintf(buffer, "rsn_pairwise=CCMP");
         memcpy(p, buffer, strlen(buffer));
         p += ret;
         *p = '\n';
         p++;
      }
      memset(buffer, 0, ret);
      ret = sprintf(buffer, "wpa_group_rekey=%d", security_info->_security_rsn_radius.group_key_renewal);
      memcpy(p, buffer, strlen(buffer));
      p += ret;
      *p = '\n';
      p++;

      memset(buffer, 0, ret);

      ret = sprintf(buffer, "auth_server_addr=%d.%d.%d.%d", security_info->_security_rsn_radius.radius_server[0], security_info->_security_rsn_radius.radius_server[1], security_info->_security_rsn_radius.radius_server[2], security_info->_security_rsn_radius.radius_server[3]);

      memcpy(p, buffer, strlen(buffer));
      p += ret;
      *p = '\n';
      p++;

      memset(buffer, 0, ret);
      ret = sprintf(buffer, "auth_server_port=%d", security_info->_security_rsn_radius.radius_port);
      memcpy(p, buffer, strlen(buffer));
      p += ret;
      *p = '\n';
      p++;


      memset(buffer, 0, ret);
//		Ramesh added : Decoding key and then copy to auth_server_shared_secret and wpa_passphrase
      ret = al_decode_key(AL_CONTEXT security_info->_security_rsn_radius.radius_secret_key, _MAC_ADDRESS, buffer);

      memset(wpa_psk, 0, BUFFER_SIZE);
      ret = sprintf(wpa_psk, "auth_server_shared_secret=%s", buffer);
      memcpy(p, wpa_psk, strlen(wpa_psk));

      p += ret;
      *p = '\n';
      p++;

      memset(wpa_psk, 0, BUFFER_SIZE);
      ret = sprintf(wpa_psk, "wpa_passphrase=%s", buffer);
      memcpy(p, wpa_psk, strlen(wpa_psk));

      p += ret;
      *p = '\n';
      p++;


      *conf_string = p;

      /* TBD : Need to add user account details*/
      al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
      return 0;
   }

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return 0;
}

int hostapd_meshap_set_ieee80211n(char *buffer ,al_conf_if_info_t *if_info )
{
    int ret;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

    if(if_info->phy_sub_type ==  AL_CONF_IF_PHY_SUB_TYPE_802_11_N_2_4GHZ ||
            if_info->phy_sub_type == AL_CONF_IF_PHY_SUB_TYPE_802_11_N_5GHZ || 
            if_info->phy_sub_type == AL_CONF_IF_PHY_SUB_TYPE_802_11_AN || 
            if_info->phy_sub_type == AL_CONF_IF_PHY_SUB_TYPE_802_11_BGN ||
            if_info->phy_sub_type == AL_CONF_IF_PHY_SUB_TYPE_802_11_ANAC ||
            /*For pure AC HT capability has to be set*/
            if_info->phy_sub_type == AL_CONF_IF_PHY_SUB_TYPE_802_11_AC )
            
    {
        ret = sprintf(buffer, "ieee80211n=1");
    }
    else{
        ret = sprintf(buffer, "ieee80211n=0");
    }
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
    return ret;
}


int hostapd_meshap_set_wmm_enabled(char *buffer ,al_conf_if_info_t *if_info )
{
   int ret;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   ret = sprintf(buffer, "wmm_enabled=1");

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return ret;
}

int hostapd_meshap_set_ht_capab(char **conf_string, al_conf_if_info_t *if_info, al_ht_capab_t *ht_capab_info)
{
    char buffer[BUFFER_SIZE];
    int  ret;
    char *p;
	 int ok, k;
    p = *conf_string;
	 int allowed[] = { 36, 44, 52, 60, 100, 108, 116, 124, 132, 149, 157};
    al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

    memset(buffer,0,BUFFER_SIZE);

    if(ht_capab_info->ldpc == AL_CONF_IF_HT_PARAM_ENABLED)
    {
        sprintf(buffer,"%s[LDPC]",buffer);
    }
   ok = 0;
   for (k = 0; k < ARRAY_SIZE(allowed); k++) {
      if (if_info->wm_channel == allowed[k]) {
         ok = 1;
         break;
      }
   }
	if (!ok)
		goto skip_not_allowed;
    switch(ht_capab_info->ht_bandwidth){
        case AL_CONF_IF_HT_PARAM_40_ABOVE:
            sprintf(buffer,"%s[HT40+]",buffer);
            break;

        case AL_CONF_IF_HT_PARAM_40_BELOW:
            sprintf(buffer,"%s[HT40-]",buffer);
            break;
    }
skip_not_allowed:
	 al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR, "%s: %d skipping Not allowed channel : %d \n", __func__, __LINE__, if_info->wm_channel);
    if(ht_capab_info->smps == AL_CONF_IF_HT_PARAM_STATIC) // need to check conditions to assain for static and dynamic cases.
    {
        sprintf(buffer,"%s[SMPS-STATIC]",buffer);
    }
    else if(ht_capab_info->smps == AL_CONF_IF_HT_PARAM_DYNAMIC)
    {
        sprintf(buffer,"%s[SMPS-DYNAMIC]",buffer);
    }

    if(ht_capab_info->gfmode == AL_CONF_IF_HT_PARAM_ENABLED)
    {
        sprintf(buffer,"%s[GF]",buffer);
    }

    if(ht_capab_info->gi_20 == AL_CONF_IF_HT_PARAM_AUTO || ht_capab_info->gi_20 == AL_CONF_IF_HT_PARAM_SHORT)
    {
        sprintf(buffer,"%s[SHORT-GI-20]",buffer);
    }

    if(ht_capab_info->gi_40 == AL_CONF_IF_HT_PARAM_AUTO || ht_capab_info->gi_40 == AL_CONF_IF_HT_PARAM_SHORT)
    {
        sprintf(buffer,"%s[SHORT-GI-40]",buffer);
    }

    if(ht_capab_info->tx_stbc == AL_CONF_IF_HT_PARAM_ENABLED)
    {
        sprintf(buffer,"%s[TX-STBC]",buffer);
    }


    if(ht_capab_info->rx_stbc == AL_CONF_IF_HT_PARAM_ENABLED ) // need to check for possible values
    {
        sprintf(buffer,"%s[RX-STBC1]",buffer);
    }

    if(ht_capab_info->delayed_ba == AL_CONF_IF_HT_PARAM_ENABLED )
    {
        sprintf(buffer,"%s[DELAYED-BA]",buffer);
    }

    if(ht_capab_info->max_amsdu_len == AL_CONF_IF_HT_PARAM_ENABLED) 
    {
        sprintf(buffer,"%s[MAX-AMSDU-7935]",buffer);
    }


    if(ht_capab_info->dsss_cck_40 == AL_CONF_IF_HT_PARAM_ENABLED) 
    {
        sprintf(buffer,"%s[DSSS_CCK-40]",buffer);
    }


    if(ht_capab_info->intolerant == AL_CONF_IF_HT_PARAM_ENABLED) 
    {
        sprintf(buffer,"%s[40-INTOLERANT]",buffer);
    }


    if(ht_capab_info->lsig_txop == AL_CONF_IF_HT_PARAM_ENABLED) 
    {
        sprintf(buffer,"%s[LSIG-TXOP-PROT]",buffer);
    }

    if(strlen(buffer) == 0){
        *conf_string = p;
        al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
        return 0;
    }

    ret = sprintf(p,"ht_capab=%s",buffer);
    p += ret;
    *p = '\n';
    p++;
    *conf_string = p;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit: %s<%d>\n",__func__,__LINE__);

    return 0;

}

int hostapd_meshap_set_require_ht(char *buffer, al_conf_if_info_t *if_info)
{
   int ret;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   //if(if_info->phy_sub_type == AL_CONF_IF_PHY_SUB_TYPE_802_11_N){
   if((if_info->phy_sub_type == AL_CONF_IF_PHY_SUB_TYPE_802_11_N_2_4GHZ) || (if_info->phy_sub_type == AL_CONF_IF_PHY_SUB_TYPE_802_11_N_5GHZ )){
   ret = sprintf(buffer, "require_ht=1"); // by default its been disabled
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return ret;
   }
   else {
   ret = sprintf(buffer, "require_ht=0"); // by default its been disabled
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return ret;
   }
}

int hostapd_meshap_set_obss_interval(char *buffer, al_conf_if_info_t *if_info)
{
   int ret;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   ret = sprintf(buffer, "obss_interval=0"); // by default its been disabled
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return ret;
}


int hostapd_meshap_set_ieee80211ac(char *buffer ,al_conf_if_info_t *if_info )
{
    int ret;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

        ret = sprintf(buffer, "ieee80211ac=1");
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit: %s<%d>\n",__func__,__LINE__);

    return ret;
}

int hostapd_meshap_set_vht_capab(char **conf_string, al_conf_if_info_t *if_info, al_vht_capab_t *vht_capab_info)
{
    char buffer[BUFFER_SIZE];
    int  ret;
    char *p;
    p = *conf_string;
    memset(buffer,0,BUFFER_SIZE);
    al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

    switch(vht_capab_info->max_mpdu_len)
    {
        case 7991:
            sprintf(buffer,"%s[MAX-MPDU-7991]",buffer);
            break ;
        case 11454:
            sprintf(buffer,"%s[MAX-MPDU-11454]",buffer);
            break ;
        default :
            sprintf(buffer,"%s[MAX-MPDU-3895]",buffer);
            break ;
    }


    switch(vht_capab_info->supported_channel_width)
    {
        case 1:
            sprintf(buffer,"%s[VHT160]",buffer);
            break ;
        case 2:
            sprintf(buffer,"%s[VHT160-80PLUS80]",buffer);
            break ;
        default :
            break ;
    }
     
    if (vht_capab_info->rx_ldpc == AL_CONF_IF_VHT_PARAM_YES){
        sprintf(buffer,"%s[RXLDPC]",buffer);
    }


    if (vht_capab_info->gi_80 == AL_CONF_IF_VHT_PARAM_YES){
        sprintf(buffer,"%s[SHORT-GI-80]",buffer);
    }


    if (vht_capab_info->gi_160 == AL_CONF_IF_VHT_PARAM_YES){
        sprintf(buffer,"%s[SHORT-GI-160]",buffer);
    }


    if (vht_capab_info->tx_stbc == AL_CONF_IF_VHT_PARAM_YES){
        sprintf(buffer,"%s[TX-STBC-2BY1]",buffer);
    }

    switch(vht_capab_info->rx_stbc)
    {
        case 1:
            sprintf(buffer,"%s[RX-STBC-1]",buffer);
            break ;
        case 2:
            sprintf(buffer,"%s[RX-STBC-12]",buffer);
            break ;
        case 3:
            sprintf(buffer,"%s[RX-STBC-123]",buffer);
            break ;
        case 4:
            sprintf(buffer,"%s[RX-STBC-1234]",buffer);
            break ;
        default :
            break ;
    }
     

    if (vht_capab_info->su_beamformer_cap == AL_CONF_IF_VHT_PARAM_YES){
        sprintf(buffer,"%s[SU-BEAMFORMER]",buffer);
    }

      // need  to check the field properly as there are lot many types of beamformee available
    if (vht_capab_info->su_beamformee_cap == AL_CONF_IF_VHT_PARAM_YES){   
        sprintf(buffer,"%s[SU-BEAMFORMEE]",buffer);
    }

    if (vht_capab_info->mu_beamformer_cap == AL_CONF_IF_VHT_PARAM_YES){   
        sprintf(buffer,"%s[MU-BEAMFORMER]",buffer);
    }

    if (vht_capab_info->mu_beamformee_cap == AL_CONF_IF_VHT_PARAM_YES){   
        sprintf(buffer,"%s[MU-BEAMFORMEE]",buffer);
    }
    // Need to verify is it needed or not
    if (vht_capab_info->su_beamformer_cap == AL_CONF_IF_VHT_PARAM_YES){
    switch(vht_capab_info->beamformee_sts_count)
    {
        case 1:
            sprintf(buffer,"%s[BF-ANTENNA-2]",buffer);
            break ;
        case 2:
            sprintf(buffer,"%s[BF-ANTENNA-3]",buffer);
            break ;
        case 3:
            sprintf(buffer,"%s[BF-ANTENNA-4]",buffer);
            break ;
        default :
            break ;
    }

    }


    if (vht_capab_info->su_beamformer_cap == AL_CONF_IF_VHT_PARAM_YES){
    switch(vht_capab_info->sounding_dimensions)
    {
        case 1:
            sprintf(buffer,"%s[SOUNDING-DIMENSION-2]",buffer);
            break ;
        case 2:
            sprintf(buffer,"%s[SOUNDING-DIMENSION-3]",buffer);
            break ;
        case 3:
            sprintf(buffer,"%s[SOUNDING-DIMENSION-4]",buffer);
            break ;
        default :
            break ;
    }
    }

    if (vht_capab_info->vht_txop_ps == AL_CONF_IF_VHT_PARAM_YES){
        sprintf(buffer,"%s[VHT-TXOP-PS]",buffer);
    }

    if (vht_capab_info->htc_vht_cap == AL_CONF_IF_VHT_PARAM_YES){
        sprintf(buffer,"%s[HTC-VHT]",buffer);
    }


    if (vht_capab_info->rx_ant_pattern_consistency == AL_CONF_IF_VHT_PARAM_YES){
        sprintf(buffer,"%s[RX-ANTENNA-PATTERN]",buffer);
    }

    if (vht_capab_info->tx_ant_pattern_consistency == AL_CONF_IF_VHT_PARAM_YES){
        sprintf(buffer,"%s[TX-ANTENNA-PATTERN]",buffer);
    }

#if 1 // VIJAY_FRAME
    if(if_info->frame_aggregation.ampdu_enable){
        switch(if_info->frame_aggregation.max_ampdu_len){
            case 8 :
                sprintf(buffer,"%s[MAX-A-MPDU-LEN-EXP0]",buffer);
                break;

            case 16 :
                sprintf(buffer,"%s[MAX-A-MPDU-LEN-EXP1]",buffer);
                break;

            case 32 :
                sprintf(buffer,"%s[MAX-A-MPDU-LEN-EXP2]",buffer);
                break;

            case 64 :
                sprintf(buffer,"%s[MAX-A-MPDU-LEN-EXP3]",buffer);
                break;

            case 128 :
                sprintf(buffer,"%s[MAX-A-MPDU-LEN-EXP4]",buffer);
                break;

            case 256 :
                sprintf(buffer,"%s[MAX-A-MPDU-LEN-EXP5]",buffer);
                break;

            case 512 :
                sprintf(buffer,"%s[MAX-A-MPDU-LEN-EXP6]",buffer);
                break;

            case 1024 :
                sprintf(buffer,"%s[MAX-A-MPDU-LEN-EXP7]",buffer);
                break;
        }
    }

#endif


// if none of the flags are set no need to keep that field in hostapd.conf 
    if(strlen(buffer) == 0){
        printf(" None of the VHT capablities are set  \n");
        *conf_string = p;
        al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
        return 0;
    }


    if(buffer){
    ret = sprintf(p,"vht_capab=%s",buffer);
    p += ret;
    *p = '\n';
    p++;
    *conf_string = p;
    al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
    return 0;
    }

}

int hostapd_meshap_set_require_vht(char *buffer, al_conf_if_info_t *if_info)
{
   int ret;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter: %s<%d>\n",__func__,__LINE__);
   ret = sprintf(buffer, "require_vht=1"); // by default its been disabled
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return ret;
}


int hostapd_meshap_set_vht_oper_chwidth(char *buffer, al_conf_if_info_t *if_info, al_vht_capab_t *vht_capab_info)
{
   int ret = 0;
	int allowed[] = { 36, 44, 52, 60, 100, 108, 116, 124, 132, 149, 157};
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   int ok = 0, k;
   for (k = 0; k < ARRAY_SIZE(allowed); k++) {
      if (if_info->wm_channel == allowed[k]) {
         ok = 1;
         break;
      }
   }
   if (ok) {
   switch(vht_capab_info->vht_oper_bandwidth){
       case 0:
           ret = sprintf(buffer, "vht_oper_chwidth=0");
           break ;
       case 1:
           ret = sprintf(buffer, "vht_oper_chwidth=1");
           break ;
       case 2:
           ret = sprintf(buffer, "vht_oper_chwidth=2");
           break ;
       case 3:
           ret = sprintf(buffer, "vht_oper_chwidth=3");
           break ;
       default:
           break ;
   }
   }
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return ret;
}


int hostapd_meshap_set_vht_oper_centr_freq_seg0_idx(char *buffer, al_conf_if_info_t *if_info, al_vht_capab_t *vht_capab_info)
{
   int ret = 0;
	int seg0_center_freq = 0;
	int allowed[] = { 36, 44, 52, 60, 100, 108, 116, 124, 132, 149, 157};
   int ok = 0, k;
   for (k = 0; k < ARRAY_SIZE(allowed); k++) {
      if (if_info->wm_channel == allowed[k]) {
         ok = 1;
         break;
      }
   }
   if(!vht_capab_info->seg0_center_freq)
		return ret;
   if (ok) {
	if (if_info->wm_channel >= 36 && if_info->wm_channel <= 48)
		seg0_center_freq  = (36 + 48)/2;
	if (if_info->wm_channel >= 52 && if_info->wm_channel <= 64)
		seg0_center_freq  = (52 + 64)/2;
	if (if_info->wm_channel >= 100 && if_info->wm_channel <= 112)
		seg0_center_freq  = (100 + 112)/2;
	if (if_info->wm_channel >= 116 && if_info->wm_channel <= 128)
		seg0_center_freq  = (116 + 128)/2;
	if (if_info->wm_channel >= 132 && if_info->wm_channel <= 144)
		seg0_center_freq  = (132 + 144)/2;
	if (if_info->wm_channel >= 149 && if_info->wm_channel <= 161)
		seg0_center_freq  = (149 + 161)/2;
	}
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   if(seg0_center_freq){
       ret = sprintf(buffer, "vht_oper_centr_freq_seg0_idx=%d",seg0_center_freq); // by default its been disabled
   }
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return ret;
}


int hostapd_meshap_set_vht_oper_centr_freq_seg1_idx(char *buffer, al_conf_if_info_t *if_info, al_vht_capab_t *vht_capab_info)
{
   int ret = 0;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   if(vht_capab_info->seg1_center_freq){
       ret = sprintf(buffer, "vht_oper_centr_freq_seg1_idx=%d",vht_capab_info->seg1_center_freq); // by default its been disabled
   }
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return ret;
}

//VLAN
int hostapd_meshap_set_bss(char* buffer,_al_conf_data_t* data, int vlan_index)
{
	int ret = 0;
    al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter: %s<%d>\n",__func__,__LINE__);
	ret = sprintf(buffer,"bss=%s",data->vlan_info[vlan_index].name);
    al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
	return ret;
}

int hostapd_meshap_set_ssid(char* buffer,_al_conf_data_t* data, int vlan_index)
{
	int  ret = 0;
    al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter: %s<%d>\n",__func__,__LINE__);
	ret = sprintf(buffer,"ssid=%s",data->vlan_info[vlan_index].essid);
    al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
	return ret;
}

int hostapd_meshap_set_bssid(char* buffer,_al_conf_data_t* data, int vlan_index, unsigned char *mac)
{
	int ret = 0;
    al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter: %s<%d>\n",__func__,__LINE__);
	ret= sprintf(buffer,"bssid=%02x:%02x:%02x:%02x:%02x:%02x",mac[0],mac[1],mac[2],mac[3],mac[4],mac[5]);
    al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
	return ret;

}
//VLAN_END

/* conf file open routine for file open/write*/
int conf_open_file(char *file_name)
{
   int fd;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter: %s<%d>\n",__func__,__LINE__);


   //fd   = open(file_name, O_CREAT | O_WRONLY | O_TRUNC, S_IRWXU | S_IRWXG);
   fd = open(file_name, O_CREAT | O_WRONLY | O_TRUNC, 0777);

   if (fd == -1)
   {
	  al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : Error while opening file : %s :  %s<%d>\n", __func__, __LINE__, file_name);
      perror("Error opening file");
      printf("\tError opening %s for mode ...\n", file_name);

      return -1;
   }
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   
   return fd;
}


/* mesh start hostapd */



int read_hostapd_pid_file(char *if_name)
{
#if 1 // VIJAY_247
   char host_pid_file[40];
   int hostap_pid = 0 ;
   FILE* file ;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   /* store the pid of hostapd in global variable*/
   sprintf(host_pid_file,HOSTAPD_PID_FILE_NAME_FORMAT,if_name);
   file = fopen (host_pid_file, "r");
   if (file == NULL)
   {
	  al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : fopen() is failed : %s<%d>\n", __func__, __LINE__);
      printf("Error while opening the file\n");
      perror("fopen");
      return 0;
   }
   if(fscanf(file, "%d", &hostap_pid) == -1){
	  al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : fscanf() is failed : %s<%d>\n", __func__, __LINE__);
       printf("Error while fscaning  the file\n");
   return 0;
   }
   fclose(file);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return hostap_pid;
#endif 
}


int mesh_start_hostapd(interface_map_t interface_map, int flag, int *hostapd_mask, int start_hostapd)
{
#define CONF_FILE_LEN 56  
   unsigned char buffer[512], name[20];
   int           i, ret;
   char          tmp1[32];
   char           pid_file[CONF_FILE_LEN], conf_file_buffer[CONF_FILE_LEN];
   int hostap_pid, hostapd_flag = 0;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   if (flag == 0)
   {
      system("killall hostapd");
      sleep(1);
   }

   if (start_hostapd) {
      hostapd_flag = 2;
   }
   else if ((*hostapd_mask) == 1) {
      strcpy(name, "wlan2");
   }
   else if ((*hostapd_mask) == 2) {
      strcpy(name, "v_wlan0");
   }
   else if ((*hostapd_mask) == 3) {
      hostapd_flag = 2;
   }

   for (i = 0; i < interface_map.if_count; i++)
   {
      if (interface_map.if_map[i].use_type == AL_CONF_IF_USE_TYPE_AP)
      {
         if (flag == 0)
         {
             memset(conf_file_buffer, 0, CONF_FILE_LEN);
             memset(pid_file, 0, CONF_FILE_LEN);
             memset(buffer,0,512);
             strcpy(conf_file_buffer,interface_map.if_map[i].file_name);
             sprintf(pid_file,HOSTAPD_PID_FILE_NAME_FORMAT,interface_map.if_map[i].if_name );
             sprintf(buffer, "hostapd -BP %s %s", pid_file, conf_file_buffer);
            system(buffer);
            sleep(2);
         }
         else
         {
             if ((hostapd_flag == 2) || !strcmp(name, interface_map.if_map[i].if_name)) {
             memset(buffer,0,512);
             hostap_pid = read_hostapd_pid_file(interface_map.if_map[i].if_name);
             if(hostap_pid == 0 ){
	             al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : Hostap PID read error : %s<%d>\n", __func__, __LINE__);
                 printf("Hostap PID read error \n");
                 return 0 ;
             }
             sprintf(buffer, "kill -1 %d", hostap_pid);
            system(buffer);
            printf("The configuration is updated on hostapd interface %s %s %d\n", interface_map.if_map[i].if_name, __func__, __LINE__);
            }
         }
      }
   }
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return 0;
}


/* Create hostapd config file */
int create_hostapd_config_file(_al_conf_data_t *data, acl_conf_t *acl_data, dot11e_conf_category_t *dot11e_data, al_conf_if_info_t *if_info,
                               interface_map_t interface_map, int index, int channel)
{
   printf("%s %d\n", __func__, __LINE__);
   char             *config_string = NULL;
   int              config_string_length;
   int              ret, handle;
   char             *p;
   char             buffer[BUFFER_SIZE];
   char             *ret_buffer;
   int              i, j;
	unsigned int tmpIndex,tmpCount=0;
   al_conf_option_t *options;
   config_string_length = 0;
	unsigned char vCount = 0;
   int use_virt_if;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter: %s<%d>\n",__func__,__LINE__);


   if (data == NULL)
   {
	  al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : Data is NULL : %s<%d>\n", __func__, __LINE__);
      return -1;
   }

   handle = libalconf_read_config_data();
   if (-1 == handle)
   {
	  al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : libalconf_read_config_data() is failed (ret : %d) : %s<%d>\n", __func__, __LINE__, handle);
      printf("Could not read configuration file\n");
      return -1;
   }
   ret_buffer = (char *)al_heap_alloc(AL_CONTEXT NO_OF_BUFFERS * BUFFER_SIZE AL_HEAP_DEBUG_PARAM);
   memset(ret_buffer, 0, NO_OF_BUFFERS * BUFFER_SIZE);
   p = ret_buffer;


   memset(buffer, 0, BUFFER_SIZE);
   ret = sprintf(buffer, "# Hostapd config file\n\n");
   memcpy(p, buffer, strlen(buffer));
   p += ret;
   *p = '\n';
   p++;

   memset(buffer, 0, ret);
   ret = interface_map.if_map[index].vector_ptr->set_driver(buffer);
   memcpy(p, buffer, strlen(buffer));
   p += ret;
   *p = '\n';
   p++;

#if 0
   memset(buffer, 0, ret);
   ret = interface_map.if_map[index].vector_ptr->set_ieee80211h(buffer);
   memcpy(p, buffer, strlen(buffer));
   p += ret;
   *p = '\n';
   p++;
#endif
   memset(buffer, 0, ret);
   ret = interface_map.if_map[index].vector_ptr->set_essid(buffer, if_info);
   memcpy(p, buffer, strlen(buffer));
   p += ret;
   *p = '\n';
   p++;
#if 0
   memset(buffer, 0, ret);
   ret = interface_map.if_map[index].vector_ptr->set_vendor_info(buffer);
   memcpy(p, buffer, strlen(buffer));
   p += ret;
   *p = '\n';
   p++;
#endif

   memset(buffer, 0, ret);
   ret = interface_map.if_map[index].vector_ptr->set_rts_th(buffer, if_info);
   memcpy(p, buffer, strlen(buffer));
   p += ret;
   *p = '\n';
   p++;

   memset(buffer, 0, ret);
   ret = interface_map.if_map[index].vector_ptr->set_frag_th(buffer, if_info);
   memcpy(p, buffer, strlen(buffer));
   p += ret;
   *p = '\n';
   p++;

   memset(buffer, 0, ret);
   ret = interface_map.if_map[index].vector_ptr->set_preamble(buffer, if_info);
   memcpy(p, buffer, strlen(buffer));
   p += ret;
   *p = '\n';
   p++;


   memset(buffer, 0, ret);
   ret = interface_map.if_map[index].vector_ptr->set_beacon_interval(buffer, if_info);
   memcpy(p, buffer, strlen(buffer));
   p += ret;
   *p = '\n';
   p++;


   memset(buffer, 0, ret);
   ret = interface_map.if_map[index].vector_ptr->set_country_code(buffer, data->country_code);
   memcpy(p, buffer, strlen(buffer));
   p += ret;
   *p = '\n';
   p++;

   memset(buffer, 0, ret);
   ret = interface_map.if_map[index].vector_ptr->set_if_name(buffer, if_info);
   memcpy(p, buffer, strlen(buffer));
   p += ret;
   *p = '\n';
   p++;

   memset(buffer, 0, ret);
   ret = interface_map.if_map[index].vector_ptr->set_channel(buffer, if_info, channel);
   memcpy(p, buffer, strlen(buffer));
   p += ret;
   *p = '\n';
   p++;

   //ret = interface_map.if_map[index].vector_ptr->set_dca_list(buffer,if_info,&p);

   memset(buffer, 0, ret);
   ret = interface_map.if_map[index].vector_ptr->set_phy_sub_type(buffer, if_info);
   memcpy(p, buffer, strlen(buffer));
   p += ret;
   *p = '\n';
   p++;

   memset(buffer, 0, ret);
   ret = interface_map.if_map[index].vector_ptr->set_hide_ssid(buffer, if_info);
   memcpy(p, buffer, strlen(buffer));
   p += ret;
   *p = '\n';
   p++;

   memset(buffer, 0, ret);
   ret = interface_map.if_map[index].vector_ptr->set_acl(buffer, data, acl_data, if_info, -1);
   memcpy(p, buffer, strlen(buffer));
   p += ret;
   *p = '\n';
   p++;

   /* wmm should have to enable irrespective of any phy_type. */
   memset(buffer, 0, ret);
   ret = interface_map.if_map[index].vector_ptr->set_wmm_enabled(buffer, if_info);
   memcpy(p, buffer, strlen(buffer));
   p += ret;
   *p = '\n';
   p++;

   ret = interface_map.if_map[index].vector_ptr->set_security_info(&p, if_info, &if_info->security_info);
   //ret = interface_map.if_map[index].vector_ptr->set_dca_list(buffer,if_info,&p);
   int phy_type =  hostapd_is_N_or_AC_supported_interface((int)if_info->phy_sub_type);

   if(phy_type == AL_PHY_SUB_TYPE_SUPPORT_ONLY_N || phy_type == AL_PHY_SUB_TYPE_SUPPORT_BOTH_N_AND_AC) {
       if(phy_type == AL_PHY_SUB_TYPE_SUPPORT_ONLY_N ) {
          memset(buffer, 0, ret);
          ret = interface_map.if_map[index].vector_ptr->set_ieee80211n(buffer, if_info);
          memcpy(p, buffer, strlen(buffer));
          p += ret;
          *p = '\n';
          p++;
       }

      ret = interface_map.if_map[index].vector_ptr->set_ht_capab(&p, if_info, &if_info->ht_capab);

      if(phy_type == AL_PHY_SUB_TYPE_SUPPORT_ONLY_N ) {
         memset(buffer, 0, ret);
         ret = interface_map.if_map[index].vector_ptr->set_require_ht(buffer, if_info);
         memcpy(p, buffer, strlen(buffer));
         p += ret;
         *p = '\n';
         p++;
      }

      memset(buffer, 0, ret);
      ret = interface_map.if_map[index].vector_ptr->set_obss_interval(buffer, if_info);
      memcpy(p, buffer, strlen(buffer));
      p += ret;
      *p = '\n';
      p++;
   }

   if(phy_type == AL_PHY_SUB_TYPE_SUPPORT_ONLY_AC || phy_type == AL_PHY_SUB_TYPE_SUPPORT_BOTH_N_AND_AC){
    memset(buffer, 0, ret);
    ret = interface_map.if_map[index].vector_ptr->set_ieee80211n(buffer, if_info);
    memcpy(p, buffer, strlen(buffer));
    p += ret;
    *p = '\n';
    p++;
    ret = interface_map.if_map[index].vector_ptr->set_ht_capab(&p, if_info, &if_info->ht_capab);
    memset(buffer, 0, ret);
    ret = interface_map.if_map[index].vector_ptr->set_ieee80211ac(buffer, if_info);
    memcpy(p, buffer, strlen(buffer));
    p += ret;
    *p = '\n';
    p++;

    interface_map.if_map[index].vector_ptr->set_vht_capab(&p, if_info, &if_info->vht_capab);

    if(phy_type == AL_PHY_SUB_TYPE_SUPPORT_ONLY_AC){
    memset(buffer, 0, ret);
    ret = interface_map.if_map[index].vector_ptr->set_require_vht(buffer, if_info);
    memcpy(p, buffer, strlen(buffer));
    p += ret;
    *p = '\n';
    p++;
}

   memset(buffer, 0, ret);
   ret = interface_map.if_map[index].vector_ptr->set_vht_oper_chwidth(buffer, if_info,&if_info->vht_capab);
   memcpy(p, buffer, strlen(buffer));
   p += ret;
   *p = '\n';
   p++;

   memset(buffer, 0, ret);
   ret = interface_map.if_map[index].vector_ptr->set_vht_oper_centr_freq_seg0_idx(buffer, if_info,&if_info->vht_capab);
   if(ret != 0){
   memcpy(p, buffer, strlen(buffer));
   p += ret;
   *p = '\n';
   p++;
   }else {
       memset(buffer,0,BUFFER_SIZE);
   }

   memset(buffer, 0, ret);
   ret = interface_map.if_map[index].vector_ptr->set_vht_oper_centr_freq_seg1_idx(buffer, if_info,&if_info->vht_capab);
   if(ret != 0){
   memcpy(p, buffer, strlen(buffer));
   p += ret;
   *p = '\n';
   p++;
   }else {
       memset(buffer,0,BUFFER_SIZE);
   }
}


#if 1
   if (dot11e_data)
   {
      printf("%s %d\n", __func__, __LINE__);

      ret = interface_map.if_map[index].vector_ptr->set_dot11e_info(&p, dot11e_data);
      printf("%s %d\n", __func__, __LINE__);
   }
#endif

   use_virt_if = al_conf_get_use_virt_if(AL_CONTEXT handle);
   if ((if_info->phy_type == AL_CONF_IF_PHY_TYPE_802_11) && (if_info->use_type == AL_CONF_IF_USE_TYPE_AP))
   {
      /*set the bssid in hostapd for wlan2 (AP) interface */
      for (tmpIndex = 0 ; tmpIndex < 6 ; tmpIndex++)
      {
          mac[tmpIndex] = _WLAN2MAC_ADDRESS[tmpIndex];      // Offset of wlan2 MAC
      }
      ret = interface_map.if_map[index].vector_ptr->set_bssid(buffer,data, 0, mac);
      memcpy(p,buffer, strlen(buffer));
      p += ret;
      *p = '\n';p++;
   }

   if ((if_info->phy_type == AL_CONF_IF_PHY_TYPE_802_11_VIRTUAL) && (if_info->use_type == AL_CONF_IF_USE_TYPE_AP))
   {
      for (tmpIndex = 0 ; tmpIndex < 6 ; tmpIndex++)
      {
			mac[tmpIndex] = _VIRTUALMAC_ADDRESS[tmpIndex];    //offset of Virtual MAC 
      }
      ret = interface_map.if_map[index].vector_ptr->set_bssid(buffer,data, 0, mac);
      memcpy(p,buffer, strlen(buffer));
      p += ret;
      *p = '\n';p++;
   }
	vCount = _BEGIN_SIGNATURE[18];
	/*write VLAN interafce configuartion only into physical interface hostapd.conf file*/
   if ((if_info->phy_type == AL_CONF_IF_PHY_TYPE_802_11) && (if_info->use_type == AL_CONF_IF_USE_TYPE_AP)) {
	for(i=1;i < data->vlan_count;i++)
   {
		if(i <= vCount)
		{
	   	memset(buffer,0,ret);
	   	ret = interface_map.if_map[index].vector_ptr->set_bss(buffer,data,i);
	   	memcpy(p,buffer, strlen(buffer));
	   	p += ret;
	   	*p = '\n';p++;

		   memset(buffer,0,ret);
		   ret = interface_map.if_map[index].vector_ptr->set_ssid(buffer,data, i);
		   memcpy(p,buffer, strlen(buffer));
		   p += ret;
		   *p = '\n';p++;

         memset(buffer, 0, ret);
         ret = interface_map.if_map[index].vector_ptr->set_acl(buffer, data, acl_data, if_info, i);
         memcpy(p, buffer, strlen(buffer));
         p += ret;
         *p = '\n';
         p++;

		   memset(buffer,0,ret);

			for( tmpIndex = 0 ; tmpIndex < 6 ; tmpIndex++ )
			{
				  mac[tmpIndex] = _VLANMAC_ADDRESS[tmpIndex + (tmpCount * 6)];  
			}
			tmpCount++;

		   ret = interface_map.if_map[index].vector_ptr->set_bssid(buffer,data,i, mac);
		   memcpy(p,buffer, strlen(buffer));
		   p += ret;
		   *p = '\n';p++;
		   ret = interface_map.if_map[index].vector_ptr->set_security_info(&p, if_info, &data->vlan_info[i].security_info);
		}
		else
		{
			al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : Vlan Count exceeds patched vlan count :  %s<%d>\n", __func__, __LINE__);
			printf("Error:%s %d:\tVlan Count exceeds patched vlan count\n", __func__, __LINE__);
		}
   }
   }
      /* copying string pointer */
   *p = '\n';
   p++;

   *p = 0;
   p++;

   ret = p - ret_buffer;

   config_string = (char *)al_heap_alloc(AL_CONTEXT ret + 1 AL_HEAP_DEBUG_PARAM);
   strcpy(config_string, ret_buffer);
   config_string_length = ret;
   printf("%s %d\n", __func__, __LINE__);
   al_heap_free(AL_CONTEXT ret_buffer);

   /* open file in  write mode to write  into hostapd conf file */

   handle = conf_open_file(interface_map.if_map[index].file_name);

   al_write_file(AL_CONTEXT handle, config_string, config_string_length);
   al_close_file(AL_CONTEXT handle);

   al_heap_free(AL_CONTEXT config_string);
   printf("%s %d\n", __func__, __LINE__);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return 0;
}

