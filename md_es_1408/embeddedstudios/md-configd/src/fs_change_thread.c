/********************************************************************************
* MeshDynamics
* --------------
* File     : fs_change_thread.c
* Comments : Configd FS Change Thread
* Created  : 3/10/2006
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  2  |5/21/2008 | Precautionary code added                        | Sriram |
* -----------------------------------------------------------------------------
* |  1  |8/10/2007 | Thread cleanup code added                       | Sriram |
* -----------------------------------------------------------------------------
* |  0  |3/10/2006 | Created.                                        | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

/* Added to use Semaphore */
#include <semaphore.h>  
sem_t mutex;

volatile int _fs_change_update_needed;
volatile int _fs_change_update_wait_count;

static void *_fs_change_thread(void *data)
{
    char command[1024];
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

    printf("\nCONFIGD:\tFS Change thread starting...(%d)", _fs_change_update_wait_count);

    while (1)
    {
        sleep(1);

        if (_fs_change_update_needed)
        {
            if (_fs_change_update_wait_count <= 0)
            {
                /** Execute POST FS CHANGE HERE **/
                printf("\nCONFIGD:\tFS Change thread posting changes...");
                sprintf(command, "/root/post_fs_change.sh  config");
                EXECUTE_COMMAND(command);
                sem_wait(&mutex);/*Locking*/
                _fs_change_update_needed     = 0;
                _fs_change_update_wait_count = 0;
                sem_post(&mutex);/*Unlocking*/
                EXECUTE_UNLOCK_REBOOT;
            }
            else
            {
                --_fs_change_update_wait_count;
            }
        }
    }

    pthread_detach(pthread_self());
    pthread_exit(NULL);
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

    return NULL;
}


static void fs_change_thead_create()
{
   pthread_t thread;
	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   _fs_change_update_needed     = 0;
   _fs_change_update_wait_count = 0;

   pthread_create(&thread, NULL, _fs_change_thread, NULL);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter: %s<%d>\n",__func__,__LINE__);
}
