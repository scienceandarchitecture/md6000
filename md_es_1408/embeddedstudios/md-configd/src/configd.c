/********************************************************************************
* MeshDynamics
* --------------
* File     : configd.c
* Comments : config daemon implementation file
* Created  : 10/15/2004
* Author   : Abhijit Ayarekar
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  31 |8/27/2008 | Used common configuration parser routines       | Sriram |
* -----------------------------------------------------------------------------
* |  30 |8/22/2008 | Changes for SIP and FIPS                        |Abhijit |
* -----------------------------------------------------------------------------
* |  29 |5/14/2008 | Chmod changes for start_gpsd.sh                 | Sriram |
* -----------------------------------------------------------------------------
* |  28 |8/10/2007 | Changes for libalconf                           | Sriram |
* -----------------------------------------------------------------------------
* |  27 |6/21/2007 |BOA web-server killed in FIPS-140 mode           |Sriram  |
* -----------------------------------------------------------------------------
* |  26 |6/21/2007 |mesh_id_key_length used instead of strlen		  |Sriram  |
* -----------------------------------------------------------------------------
* |  25 |3/27/2007 | references to imcp_priv_channel_rr_data removed | Abhijit|
* -----------------------------------------------------------------------------
* |  24 |2/22/2007 | Added Effistream Req/Resp and Info Packets      | Sriram |
* -----------------------------------------------------------------------------
* |  23 |01/01/2007| Added Private Channel implementation            |Prachiti|
* -----------------------------------------------------------------------------
* |  22 |11/21/2006| Added AL Gen2k implementation                   | Sriram |
* -----------------------------------------------------------------------------
* |  21 |9/25/2006 | configd_spawn.sh Invoked                        | Sriram |
* -----------------------------------------------------------------------------
* |  20 |9/25/2006 | IMCP key mismatch message removed               | Sriram |
* -----------------------------------------------------------------------------
* |  19 |06/28/2006| IPerf daemon started                            | Sriram |
* -----------------------------------------------------------------------------
* |  18 |5/12/2006 | Changes for Firmware Update                     | Bindu  |
* -----------------------------------------------------------------------------
* |  17 |3/14/2006 | Changes for ACL								  | Bindu  |
* -----------------------------------------------------------------------------
* |  16 |3/13/2006 | ACL list implemented                            | Sriram |
* -----------------------------------------------------------------------------
* |  15 |03/10/2006| FS Change thread added                          | Sriram |
* -----------------------------------------------------------------------------
* |  14 |02/27/2006| MESH ID verification enhanced                   | Sriram |
* -----------------------------------------------------------------------------
* |  13 |02/22/2006| dot11e category packets  added				  | Bindu  |
* -----------------------------------------------------------------------------
* |  12 |02/20/2006| Config SQNR removed							  | Abhijit|
* -----------------------------------------------------------------------------
* |  11 |02/08/2006| Config SQNR and Hide SSID added				  | Sriram |
* -----------------------------------------------------------------------------
* |  10 |11/11/2005| ACK_Timeout added								  |Prachiti|
* -----------------------------------------------------------------------------
* |  9  |11/11/2005| Regulatory Domain and Country code added        |Prachiti|
* -----------------------------------------------------------------------------
* |  8  |10/04/2005| MIP changes                                     | Sriram |
* -----------------------------------------------------------------------------
* |  7  |9/15/2005 | MeshID checking added                           | Sriram |
* -----------------------------------------------------------------------------
* |  6  |6/9/2005  | Misc changes                                    | Sriram |
* -----------------------------------------------------------------------------
* |  5  |5/17/2005 | Configd Watchdog packet added                   | Sriram |
* -----------------------------------------------------------------------------
* |  4  |4/22/2005 | Merged changes by Prachiti                      | Sriram |
* -----------------------------------------------------------------------------
* |  3  |4/15/2005 | Misc changes, code clean up                     | Sriram |
* -----------------------------------------------------------------------------
* |  2  |4/11/2005 | Misc changes                                    | Sriram |
* -----------------------------------------------------------------------------
* |  1  |4/02/2005 | vlan config changes							  | Amit   |
* -----------------------------------------------------------------------------
* |  0  |10/15/2004| Created										  |abhijit |
* -----------------------------------------------------------------------------
********************************************************************************/

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <fcntl.h>
#include <pthread.h>

#include <sys/ioctl.h>
#include <net/if.h>

#include <string.h>
#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include <unistd.h>

#include "al.h"
#include "al_context.h"
#include "al_impl_context.h"
#include "al_conf.h"
#include "dot11e_conf.h"
#include "acl_conf.h"
#include "al_codec.h"
#include "al_conf_lib.h"
#include "sip_conf.h"

#include "configd.h"
#include "dump.h"

#include "torna_ddi.h"

#include "conf_common.h"
#include "meshap_util.h"
#include "iwconf.h"
#include "hostapd_conf.h"

//RAMESH16MIG included header file for inet_addr
#include <arpa/inet.h>
//SOWNDARYA added header files for validations
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <syslog.h>
#include <sys/resource.h>

AL_DECLARE_GLOBAL_EXTERN(al_conf_if_info_t old_al_if_info[CURRENT_EXPECTED_MAX_IF_COUNT]);
AL_DECLARE_GLOBAL(al_conf_if_info_t new_al_if_info[CURRENT_EXPECTED_MAX_IF_COUNT]);

int compare_AP_data(int al_conf_handle);

//RAMESH16MIG added declaration
int read_from_network_file(void);
int send_packet_ex(void *data, int data_length, struct sockaddr_in *client_addr, int encrypt, int broadcast);
int get_packet_type_offset(void);
int get_imcp_signature_mesh_id_size(void);
int get_security_info_size(al_security_info_t *security_info);
int imcp_vlan_request_response_helper(unsigned char *buffer, struct sockaddr_in *client_addr);
int imcp_sip_config_request_response_helper(unsigned char *buffer, struct sockaddr_in *client_addr);
int reboot_required_parameter_check_for_hostapd(int al_conf_handle);
int init_virtual_configurations(int al_conf_handle);
int create_hostapd_config_file(_al_conf_data_t *data, acl_conf_t *acl_data, dot11e_conf_category_t *dot11e_data, al_conf_if_info_t *if_info, interface_map_t interface_map, int index,
                               int channel);
int mesh_start_hostapd(interface_map_t interface_map, int flag, int *hostapd_mask, int start_hostapd);

#if 1 
int validating_capabilities();
unsigned char validate_ht_capabilities(tddi_ht_cap_t ht_cap, al_conf_if_info_t *if_info, config_validation_report_t *validation_report);
unsigned char validate_frame_aggregation( al_fram_agre_t *frame_aggregation);
unsigned char validate_smps(tddi_ht_cap_t *ht_cap, al_ht_capab_t  *ht_capab);
unsigned char validate_vht_capabilities(tddi_vht_cap_t vht_cap,al_conf_if_info_t *if_info, config_validation_report_t  *validation_report);
unsigned char validate_max_mpdu_len(tddi_vht_cap_t *vht_cap,al_vht_capab_t *vht_capab);
unsigned int mesh_get_ds_iface_channel(_al_conf_data_t *data);


param_id_name_map_table_t   if_param_id_name_map_table[] = {
	{IF_PHY_TYPE, "Interface Medium Type"},
	{IF_SUB_PHY_TYPE, "Supported Protocol"},
	{IF_USE_TYPE, "Interface Use Type"},
	{IF_CHANNEL, "Channel"},
	{IF_DCA_CH_LIST, "DCA Channel List"},
	{IF_ESSID_LENGTH, "Interface SSID"},
	{IF_TX_POWER, "Interface Tx_power"},
	{IF_TX_RATE, "Interface Tx_rate"},
	{IF_RTS_TH, "Interface RTS threshold"},
	{IF_FRAG_TH, "Inteface Fragmentation threshold"},
	{IF_VLAN_TAG, "Vlan tag"},
	{IF_VLAN_DOT1P_PRIORITY, "vlan dot1p priority"},
	{IF_VLAN_ESSID, "vlan essid"},
	{IF_SECURITY, "Security"},
    {WEP_ENABLED_ERROR_MSG, "Can't Enbale WEP Security in 11N/11AC mode Error"},
	{IF_HT_CAP_LDPC, "HT CAPABILITY LDPC"},
	{IF_HT_CAP_CH_BANDWIDTH,"HT CAPABILITY CHANNEL BANDWIDTH"},
	{IF_HT_CAP_CH_SEC_OFFSET, "HT CAPABILITY SECONDARY CHANNEL OFFSET"},
	{IF_HT_CAP_GI_20, "HT CAPABILITY 20 MHz Guard Interval"},
	{IF_HT_CAP_GI_40, "HT CAPABILITY 40 MHz Guard Interval"},
	{IF_HT_CAP_TX_STBC, "HT CAPABILITY Tx STBC"},
	{IF_HT_CAP_RX_STBC, "HT CAPABILITY Rx STBC"},
	{IF_HT_CAP_GF_MODE, "HT CAPABILITY GF Mode"},
	{IF_HT_CAP_40MZ_INTOLERANT, "HT CAPABILITY 40MHz Intolerant"},
	{IF_HT_CAP_AMPDU, "HT CAPABILITY AMPDU Length"},
	{IF_HT_CAP_AMSDU, "HT CAPABILITY AMSDU Length"},
	{IF_VHT_CAP_MAX_MPDU_LENGTH, "VHT CAPABILITY MPDU_LEN"},
	{IF_VHT_CAP_GI_80, "VHT CAPABILITY GI_80"},
	{IF_80MHZ_CH, "VHT capability 80mhz invalid channel"},
	{NODE_NAME_LENGTH, "Node Name"},
	{NODE_DESCRIPTION_LENGTH, "Node Description"},
	{NODE_ESSID_LENGTH, "Node Essid"},
	{NODE_FRAG_TH, "Node frag_threshold"},
	{NODE_RTS_TH, "Node rts_threshold"},
	{NODE_HB_INTERVAL, "Node heartbeat_interval"},
	{NODE_VLAN_COUNT, "Node vlan interfaces"},
	{NODE_VLAN_NAME_LENGTH, "Node vlan name length"},
	{NODE_VLAN_ESSID_LENGTH, "Node vlan Essid length"},
	{NODE_DOT11E_CAT, "Node dot11e_category"},
	{NODE_PREFERRED_PARENT, "Preferred Parent"},
	{NODE_GPS_X_COORDINATE, "GPS X Coordinates"},
	{NODE_GPS_Y_COORDINATE, "GPS Y Coordinates"},
	{TDDI_RESPONSE_INVALID, "Node config failure"}

};


static unsigned char validate_interface_frag_threshold(al_conf_if_info_t *if_info, config_validation_report_t  *validation_report);
static unsigned char validate_node_frag_threshold(int frag_threshold, config_validation_report_t  *validation_report);
static unsigned char validate_interface_rts_threshold(al_conf_if_info_t *if_info, config_validation_report_t  *validation_report);
static unsigned char validate_node_rts_threshold(int rts_th, config_validation_report_t  *validation_report);
static unsigned char validate_interface_txpower(al_conf_if_info_t *if_info, config_validation_report_t  *validation_report);
static unsigned char validate_interface_txrate(al_conf_if_info_t *if_info,config_validation_report_t  *validation_report);
static unsigned char validate_vlan_tag_info(short vlan_tag, config_validation_report_t  *validation_report);
static unsigned char validate_vlan_essid_length(int len, config_validation_report_t  *validation_report);
static unsigned char validate_vlan_name_length(int len, config_validation_report_t  *validation_report);
static unsigned char validate_vlan_dot1p_priority_info(unsigned char dot1p_p, config_validation_report_t  *validation_report);
static unsigned char validate_dot11e_category_data(dot11e_conf_category_details_t *category_info, config_validation_report_t  *validation_report);
unsigned char boot_time_validate_vlan_config(al_conf_vlan_info_t *vlan_info, config_validation_report_t  *validation_report);
unsigned char boot_time_validate_dot11e_category_data(dot11e_conf_category_details_t *category_info, config_validation_report_t  *validation_report);
#endif

static unsigned char validate_preferred_parent(unsigned char *preferred_parent, config_validation_report_t  *validation_report);
static unsigned char validate_gps_x_coordinate(unsigned char *latitude , config_validation_report_t  *validation_report);
static unsigned char validate_gps_y_coordinate(unsigned char *longitude , config_validation_report_t  *validation_report);
unsigned char validate_vlan_security_info(al_conf_if_info_t *if_info,al_security_info_t *security , config_validation_report_t  *validation_report);
unsigned char validate_interface_security_info(al_conf_if_info_t *if_info,al_security_info_t *security,config_validation_report_t  *validation_report);
unsigned char validate_security_info(al_conf_if_info_t *if_info,al_security_info_t *security,config_validation_report_t  *validation_report);
unsigned char validate_WEP_security_info(al_conf_if_info_t *if_info,int key,config_validation_report_t  *validation_report);
unsigned char validate_WPA_WPA2_radius_sec_info(al_conf_if_info_t *if_info,al_security_info_t *security,config_validation_report_t  *validation_report);

// md-manufacturer changes:	sysupgrade status
int get_upgrade_status();
extern struct list_head imcp_sysupgrade_request;
int *imcp_sw_update_request_helper_thread(void *addr_data_buf);
extern pthread_t prev_tid;

#define MAX                               1
#define FS_CHANGE_THREAD_PARAM            2
#define POST_FS_PARAM_TYPE_BINARY         1
#define POST_FS_PARAM_TYPE_CONFIG         2
#define POST_FS_PARAM_TYPE_NOTHING        3

#define EXECUTE_MESHD_REBOOT              system("/sbin/meshd reboot");
#define EXECUTE_PRE_FS_CHANGE             system("/root/pre_fs_change.sh");
#define EXECUTE_POST_FS_CHANGE(TYPE)    execute_post_fs_change(TYPE);
#define EXECUTE_MESHD_START               system("/sbin/meshd start");
#define EXECUTE_MESHD_STOP                system("/sbin/meshd stop");
#define EXECUTE_MESHD_CONFIGURE           system("/sbin/meshd configure");
#define EXECUTE_UNLOAD_TORNA              system("/root/unload_torna");
#define EXECUTE_STARTUP_SH                system("/root/startap.sh");
#define EXECUTE_SYSTEM_REBOOT             system("reboot");
#define EXECUTE_MESH_UPDATE_SERVER        system("/sbin/mesh_update_server");
#define EXECUTE_LOCK_REBOOT               system("meshd lock_reboot 1");
#define EXECUTE_UNLOCK_REBOOT             system("meshd lock_reboot 0");
#define EXECUTE_MESHD_VCONFIGURE          system("/sbin/meshd vconfigure");
#define EXECUTE_MESHD_DOT11E_CONFIGURE    system("/sbin/meshd configure_dot11e");
#define EXECUTE_MESHD_ACL_CONFIGURE       system("/sbin/meshd configure_acl");
#define EXECUTE_MESHD_SIP_CONFIGURE       system("/sbin/meshd configure_sip");
#define EXECUTE_MESHD_HALT						system("/sbin/meshd halt");
#define EXECUTE_COMMAND(command)    do { printf("\nExecuting command : \"%s\"", command); system(command); } while (0);

#define IMCP_USE_ENCRYPTION               (unsigned short)(0x8000) // upper bit in packet type indicates the packet is encrypted 
#define DEVICE_LOCAL_HOST 						"127.0.0.1"

#if 1 

#define _DEFAULT_VALD_FILE_PATH        "/etc/validate.txt"
#define NMS_DEBUG 1

#define VAL_PRINTF(...)\
	if(NMS_DEBUG) {\
		FILE *fp;\
		fp = fopen("/etc/validate.txt","a");\
		int size = 0; \
		struct stat file_status = { 0 }; \
		int status = stat( "/etc/validate.txt", &file_status );\
		if ( 0 != status ) {\
			perror("Failed to read file status");\
			return EXIT_FAILURE;\
		}\
		if(file_status.st_size > 4096) {\
			remove("/etc/validate.txt");\
		 fopen("/etc/validate.txt","a");\
		}\
		fprintf(fp,__VA_ARGS__);\
		fclose(fp);\
	}\
else\
{\
	printf(__VA_ARGS__);\
}

#if 1 //runtime validate


#define VALIDATE_TXRATE_FOR_SUBTYPE_A(txrate)       (txrate == 0 || txrate == 6 || txrate == 9 || txrate == 12 || txrate == 18 || txrate == 24 || txrate == 36 || \
                                                    txrate == 48 || txrate == 54)

#define VALIDATE_TXRATE_FOR_SUBTYPE_BG(txrate)      (txrate == 0 || txrate == 1 || txrate == 2 || txrate == 5 || txrate == 6 || txrate == 9 || txrate == 11 || \
                                                    txrate == 12 || txrate == 18 || txrate == 24 || txrate == 36 || txrate == 48 || txrate == 54)

#define VALIDATE_TXRATE_FOR_SUBTYPE_B(txrate)       (txrate == 0 || txrate == 1 || txrate == 2 || txrate == 5 || txrate == 11)

#define VALIDATE_TXRATE_FOR_SUBTYPE_FOR_G(txrate)   (txrate == 0 || txrate == 6 || txrate == 9 || txrate == 12 || txrate == 18 || txrate == 24 || txrate == 36 || \
                                                     txrate == 48 || txrate == 54)

#define VALIDATE_CAT_ACWMIN_VAL(category, acwmin)   (category == DOT11E_CATEGORY_TYPE_AC_BK && acwmin < 15) ||    \
                                                    (category == DOT11E_CATEGORY_TYPE_AC_BE && acwmin < 15) ||    \
													(category == DOT11E_CATEGORY_TYPE_AC_VI && acwmin < 7) || \
													(category == DOT11E_CATEGORY_TYPE_AC_VO && acwmin < 3)

#define VALIDATE_ACWMIN_VAL(acwmin)                 (acwmin != 3 && acwmin != 7 && acwmin != 15 && acwmin != 31 && acwmin != 63 && \
                                                     acwmin != 127 && acwmin != 255 && acwmin != 511 && acwmin != 1023)

#define VALIDATE_ACWMIN_ACWMAX_VAL(acwmax)          (acwmax != 3 && acwmax != 7 && acwmax != 15 && acwmax != 31 && acwmax != 63 && \
                                                     acwmax != 127 && acwmax != 255 && acwmax != 511 && acwmax != 1023)

#endif

#define BIT(nr)         (1UL << (nr))
#define HT_CAP_INFO_LDPC_CODING_CAP             ((unsigned short) BIT(0))
#define HT_CAP_INFO_SUPP_CHANNEL_WIDTH_SET      ((unsigned short) BIT(1))
#define HT_CAP_INFO_SMPS_MASK                   ((unsigned short) (BIT(2) | BIT(3)))
#define HT_CAP_INFO_SMPS_STATIC                 ((unsigned short) 0)
#define HT_CAP_INFO_SMPS_DYNAMIC                ((unsigned short) BIT(2))
#define HT_CAP_INFO_SMPS_DISABLED               ((unsigned short) (BIT(2) | BIT(3)))
#define HT_CAP_INFO_GREEN_FIELD                 ((unsigned short) BIT(4))
#define HT_CAP_INFO_SHORT_GI20MHZ               ((unsigned short) BIT(5))
#define HT_CAP_INFO_SHORT_GI40MHZ               ((unsigned short) BIT(6))
#define HT_CAP_INFO_TX_STBC                     ((unsigned short) BIT(7))
#define HT_CAP_INFO_RX_STBC_MASK                ((unsigned short) (BIT(8) | BIT(9)))
#define HT_CAP_INFO_RX_STBC_1                   ((unsigned short) BIT(8))
#define HT_CAP_INFO_RX_STBC_12                  ((unsigned short) BIT(9))
#define HT_CAP_INFO_RX_STBC_123                 ((unsigned short) (BIT(8) | BIT(9)))
#define HT_CAP_INFO_DELAYED_BA                  ((unsigned short) BIT(10))
#define HT_CAP_INFO_MAX_AMSDU_SIZE              ((unsigned short) BIT(11))
#define HT_CAP_INFO_DSSS_CCK40MHZ               ((unsigned short) BIT(12))
/* B13 - Reserved (was PSMP support during P802.11n development) */
#define HT_CAP_INFO_40MHZ_INTOLERANT            ((unsigned short) BIT(14))
#define HT_CAP_INFO_LSIG_TXOP_PROTECT_SUPPORT   ((unsigned short) BIT(15))
#define TDDI_802_11_IS_N(sub_type)\
	((sub_type == AL_CONF_IF_PHY_SUB_TYPE_802_11_N_2_4GHZ)|| (sub_type == AL_CONF_IF_PHY_SUB_TYPE_802_11_N_5GHZ) || (sub_type == AL_CONF_IF_PHY_SUB_TYPE_802_11_BGN)\
	 || (sub_type == AL_CONF_IF_PHY_SUB_TYPE_802_11_AN) || (sub_type == AL_CONF_IF_PHY_SUB_TYPE_802_11_ANAC))

#define TDDI_802_11_IS_AC(sub_type)\
	((sub_type == AL_CONF_IF_PHY_SUB_TYPE_802_11_AC) || (sub_type == AL_CONF_IF_PHY_SUB_TYPE_802_11_ANAC))


#define HT_CAP_INFO_LDPC_CAP                            0x01
#define HT_CAP_INFO_GREEN_FIELD_CHECK                   0x01
#define HT_CAP_INFO_GI20MHZ_AUTO                        0x01
#define HT_CAP_INFO_GI20MHZ_SHORT                       0x02
#define HT_CAP_INFO_GI40MHZ_AUTO                        0x01
#define HT_CAP_INFO_GI40MHZ_SHORT                       0x02
#define HT_CAP_INFO_TX_STBC_CHECK                       0x01
#define HT_CAP_INFO_RX_STBC                             0x01
#define HT_CAP_INFO_DELAYED_BA_CHECK                    0x01
#define HT_CAP_INFO_MAX_AMSDU_SIZE_CHECK                0x01
#define HT_CAP_INFO_DSSS_CCK40MHZ_CHECK                 0x01
#define HT_CAP_INFO_LSIG_TXOP                           0x01

#define VHT_CAP_RX_LDPC_INFO                             0x01
#define VHT_CAP_SHORT_GI_80_AUTO                         0x01
#define VHT_CAP_SHORT_GI_80_SHORT                        0x02
#define VHT_CAP_SHORT_GI_160_AUTO                        0x01
#define VHT_CAP_SHORT_GI_160_SHORT                       0x02
#define VHT_CAP_SUPP_CH_160                              0x01
#define VHT_CAP_SUPP_CH_160_80PLUS80                     0x02
#define VHT_CAP_TXSTBC                                   0x01
#define VHT_CAP_RXSTBC                                   0x01
#define VHT_CAP_SU_BEAMFORMER_CAPABLE                    0x01
#define VHT_CAP_SU_BEAMFORMEE_CAPABLE                    0x01
#define VHT_CAP_MU_BEAMFORMER_CAPABLE                    0x01
#define VHT_CAP_MU_BEAMFORMEE_CAPABLE                    0x01
#define VHT_CAP_VHT_TXOP_PS                              0x01
#define VHT_CAP_HTC_VHT                                  0x01
#define VHT_CAP_RX_ANTENNA_PATTERN                       0x01
#define VHT_CAP_TX_ANTENNA_PATTERN                       0x01

#define MESH_VHT_CAP_MAX_MPDU_LENGTH_3895                0
#define MESH_VHT_CAP_MAX_MPDU_LENGTH_7991                1
#define MESH_VHT_CAP_MAX_MPDU_LENGTH_11454               2
#define MESH_VHT_CAP_MAX_MPDU_LENGTH_MASK                ((unsigned int) BIT(0) | BIT(1))
#define MESH_VHT_CAP_SUPP_CHAN_WIDTH_160MHZ              ((unsigned int) BIT(2))
#define MESH_VHT_CAP_SUPP_CHAN_WIDTH_160_80PLUS80MHZ     ((unsigned int) BIT(3))
#define MESH_VHT_CAP_SUPP_CHAN_WIDTH_MASK                ((unsigned int) BIT(2) | BIT(3))
#define MESH_VHT_CAP_RXLDPC                              ((unsigned int) BIT(4))
#define MESH_VHT_CAP_SHORT_GI_80                         ((unsigned int) BIT(5))
#define MESH_VHT_CAP_SHORT_GI_160                        ((unsigned int) BIT(6))
#define MESH_VHT_CAP_TXSTBC                              ((unsigned int) BIT(7))
#define MESH_VHT_CAP_RXSTBC_1                            ((unsigned int) BIT(8))
#define MESH_VHT_CAP_RXSTBC_2                            ((unsigned int) BIT(9))
#define MESH_VHT_CAP_RXSTBC_3                            ((unsigned int) BIT(8) | BIT(9))
#define MESH_VHT_CAP_RXSTBC_4                            ((unsigned int) BIT(10))
#define MESH_VHT_CAP_RXSTBC_MASK                         ((unsigned int) BIT(8) | BIT(9) | \
		BIT(10))
#define MESH_VHT_CAP_SU_BEAMFORMER_CAPABLE               ((unsigned int) BIT(11))
#define MESH_VHT_CAP_SU_BEAMFORMEE_CAPABLE               ((unsigned int) BIT(12))
#define MESH_VHT_CAP_BEAMFORMEE_STS_MAX                  ((unsigned int) BIT(13) | \
		BIT(14) | BIT(15))
#define MESH_VHT_CAP_BEAMFORMEE_STS_OFFSET               13
#define MESH_VHT_CAP_SOUNDING_DIMENSION_MAX              ((unsigned int) BIT(16) | \
		BIT(17) | BIT(18))
#define MESH_VHT_CAP_SOUNDING_DIMENSION_OFFSET           16
#define MESH_VHT_CAP_MU_BEAMFORMER_CAPABLE               ((unsigned int) BIT(19))
#define MESH_VHT_CAP_MU_BEAMFORMEE_CAPABLE               ((unsigned int) BIT(20))
#define MESH_VHT_CAP_VHT_TXOP_PS                         ((unsigned int) BIT(21))
#define MESH_VHT_CAP_HTC_VHT                             ((unsigned int) BIT(22))

#define MESH_VHT_CAP_MAX_A_MPDU_LENGTH_EXPONENT_1        ((unsigned int) BIT(23))
#define MESH_VHT_CAP_MAX_A_MPDU_LENGTH_EXPONENT_2        ((unsigned int) BIT(24))
#define MESH_VHT_CAP_MAX_A_MPDU_LENGTH_EXPONENT_3        ((unsigned int) BIT(23) | BIT(24))
#define MESH_VHT_CAP_MAX_A_MPDU_LENGTH_EXPONENT_4        ((unsigned int) BIT(25))
#define MESH_VHT_CAP_MAX_A_MPDU_LENGTH_EXPONENT_5        ((unsigned int) BIT(23) | BIT(25))
#define MESH_VHT_CAP_MAX_A_MPDU_LENGTH_EXPONENT_6        ((unsigned int) BIT(24) | BIT(25))
#define MESH_VHT_CAP_MAX_A_MPDU_LENGTH_EXPONENT_MAX      ((unsigned int) BIT(23) | \
		BIT(24) | BIT(25))
#define MESH_VHT_CAP_VHT_LINK_ADAPTATION_VHT_UNSOL_MFB   ((unsigned int) BIT(27))
#define MESH_VHT_CAP_VHT_LINK_ADAPTATION_VHT_MRQ_MFB     ((unsigned int) BIT(26) | BIT(27))
#define MESH_VHT_CAP_RX_ANTENNA_PATTERN                  ((unsigned int) BIT(28))
#define MESH_VHT_CAP_TX_ANTENNA_PATTERN                  ((unsigned int) BIT(29))

#define VHT_OPMODE_CHANNEL_WIDTH_MASK           ((unsigned char) BIT(0) | BIT(1))
#define VHT_OPMODE_CHANNEL_RxNSS_MASK           ((unsigned char) BIT(4) | BIT(5) | \
		BIT(6))
#define VHT_OPMODE_NOTIF_RX_NSS_SHIFT           4

#define VHT_RX_NSS_MAX_STREAMS                  8

/* VHT channel widths */
#define MESH_VHT_CHANWIDTH_USE_HT    0
#define MESH_VHT_CHANWIDTH_80MHZ     1
#define MESH_VHT_CHANWIDTH_160MHZ    2
#define MESH_VHT_CHANWIDTH_80P80MHZ  3

#endif


#include "fs_change_thread.c"


static void mesh_ethernet_link_status_create_thread();
#ifdef MD1000
static void mesh_ds_link_status_create_thread();
#endif

int execute_post_fs_change(int type)
{
   char command[1024];
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

#ifndef CONFIGD_NO_FS_CHANGE_THREAD
   if (type == POST_FS_PARAM_TYPE_CONFIG)
   {
      _fs_change_update_wait_count += FS_CHANGE_THREAD_PARAM;
      if (!_fs_change_update_needed)
      {
		 sem_wait(&mutex);/*Locking*/
         _fs_change_update_needed = 1;
		 sem_post(&mutex);/*Unlocking*/
         EXECUTE_LOCK_REBOOT;
      }
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
      return 0;
   }
#endif

   switch (type)
   {
   case POST_FS_PARAM_TYPE_BINARY:
      sprintf(command, "/root/post_fs_change.sh  binary");
      break;

   case POST_FS_PARAM_TYPE_CONFIG:
      sprintf(command, "/root/post_fs_change.sh  config");
      break;

   case POST_FS_PARAM_TYPE_NOTHING:
      sprintf(command, "/root/post_fs_change.sh nothing");
      break;

   default:
      printf("\nCONFIGD	: EXECUTE_POST_FS_CHANGE invalid type");
	  al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : Error as the EXECUTE_POST_FS_CHANGE is of invalid type : %s<%d>\n", __func__, __LINE__);
      return -1;
   }

   EXECUTE_COMMAND(command);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return 0;
}

#ifndef DEFAULT_IF_NAME
#define DEFAULT_IF_NAME    "mip0"
#endif

char current_if_name[256];

pid_t pid;
int   status, died;

int debug_level = _DEBUG_ON;
int sock;

int           mesh_id_length;
int           mesh_id_key_length;
unsigned char mesh_id[256];
unsigned char mesh_id_key[1024];
unsigned char network_broadcast_address[4];

unsigned char _SIGNATURE_[] =
{
   'T',  'O',  'R',  'N',  'A',  'S',  'T', 'A', 'R', 'T',
   0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
   0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
   0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
   0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
   0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
   0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
   0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
   0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
   0xff, 0xff, 0xff, 0xff, 0xff, 0xff,/*1 Virtual Intefce MAC start*/
   0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
   0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
   0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
   0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
   0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
	0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
	0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
	0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
	0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
	0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
   'T',  'O',  'R',  'N',  'A',  'E',  'N', 'D'
};

unsigned char *_BEGIN_SIGNATURE = _SIGNATURE_;
unsigned char *_MAC_ADDRESS     = &_SIGNATURE_[10];
unsigned char *_RMAC_ADDRESS     = &_SIGNATURE_[22];
#ifdef MD1000
unsigned char *_WLAN2MAC_ADDRESS     = &_SIGNATURE_[28];
#else
unsigned char *_WLAN2MAC_ADDRESS     = &_SIGNATURE_[34];
#endif
unsigned char *_VIRTUALMAC_ADDRESS     = &_SIGNATURE_[58];
unsigned char *_VLANMAC_ADDRESS     = &_SIGNATURE_[64];
unsigned char *_APPMAC_ADDRESS     = &_SIGNATURE_[94];
unsigned char *_ETHMAC_ADDRESS     = &_SIGNATURE_[118];
unsigned char *_END_SIGNATURE   = &_SIGNATURE_[124];

void DEBUG(int level, const char *format, ...)
{
   va_list args;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   va_start(args, format);

   if (debug_level >= level)
   {
      vprintf(format, args);
   }
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
}


imcp_reset_data_t            *imcp_reset_data;
imcp_apconfig_data_t         *imcp_apconfig_data;
imcp_apconfig_rr_data_t      *imcp_apconfig_rr_data;
imcp_ipconfig_data_t         *imcp_ipconfig_data;
imcp_ipconfig_rr_data_t      *imcp_ipconfig_rr_data;
imcp_start_ip_if_data_t      *imcp_start_ip_if_data;
imcp_sw_update_req_data_t    *imcp_sw_update_req_data;
imcp_mesh_key_update_data_t  *imcp_mesh_key_update_data;
imcp_config_rr_data_t        *imcp_config_rr_data;
imcp_vlan_config_info_data_t *imcp_vlan_config_info_data;

imcp_reg_domain_rr_data_t      *imcp_reg_domain_rr_data;
imcp_reg_domain_config_data_t  *imcp_reg_domain_config_data;
imcp_ack_timeout_rr_data_t     *imcp_ack_timeout_rr_data;
imcp_ack_timeout_config_data_t *imcp_ack_timeout_config_data;
imcp_hide_ssid_config_data_t   *imcp_hide_ssid_config_data;
imcp_dot11e_category_t         *imcp_dot11e_category_data;
imcp_dot11e_if_config_t        *imcp_dot11e_if_config_data;
imcp_acl_config_t              *imcp_acl_config_data;
imcp_priv_channel_config_t     *imcp_priv_channel_data;
imcp_sip_config_t              *imcp_sip_config_data;
imcp_mac_addr_req_resp_t 		 *imcp_mac_addr_data;
imcp_sw_update_request_t		 *imcp_sw_update_request_data;
imcp_sw_update_response_t 		 *imcp_sw_update_response_data;
mgmt_gw_info_req_t				 *mgmt_gw_info_data;

static void _write_gps_info(char *latitude, char *longitude)
{
   int             fd;
   tddi_packet_t   *packet;
   int             ret;
   tddi_gps_info_t *info;
   unsigned char   buffer[sizeof(tddi_packet_t) + sizeof(tddi_gps_info_t)];

   packet = (tddi_packet_t *)buffer;
   info   = (tddi_gps_info_t *)(buffer + sizeof(tddi_packet_t));
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   memset(info, 0, sizeof(tddi_gps_info_t));

   packet->signature = TDDI_PACKET_SIGNATURE;
   packet->version   = TDDI_CURRENT_VERSION;
   packet->data_size = sizeof(tddi_gps_info_t);
   packet->type      = TDDI_PACKET_TYPE_REQUEST;
   packet->sub_type  = TDDI_REQUEST_TYPE_SET_GPS_INFO;

   strcpy(info->latitude, latitude);
   strcpy(info->longitude, longitude);

   fprintf(stderr, "CONFIGD: Setting initial GPS coordinates to %s,%s\n", longitude, latitude);

   fd = open(TDDI_FILE_NAME, O_RDWR);

   if (fd == -1)
   {
	  al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : Error while opening file : %s : %s<%d>\n", __func__, __LINE__, TDDI_FILE_NAME);
      fprintf(stderr, "CONFIGD: Error opening %s\n", TDDI_FILE_NAME);
      return;
   }

   ret = write(fd, buffer, sizeof(tddi_packet_t) + sizeof(tddi_gps_info_t));

   if (ret != sizeof(tddi_packet_t) + sizeof(tddi_gps_info_t))
   {
	  al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : Error while writing into the file : %s(%d) : %s<%d>\n", TDDI_FILE_NAME, ret, __func__, __LINE__);
      fprintf(stderr, "CONFIGD: Error writing to %s(%d)\n", TDDI_FILE_NAME, ret);
      close(fd);
      return;
   }

   lseek(fd, 0, SEEK_CUR);

   ret = read(fd, buffer, sizeof(tddi_packet_t));

   close(fd);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
}


static int _get_ds_netif()
{
   int                       fd;
   tddi_packet_t             packet;
   int                       ret;
   unsigned char             buffer[sizeof(tddi_packet_t) + sizeof(tddi_get_ds_if_response_t) + 128];
   tddi_packet_t             *response_packet;
   tddi_get_ds_if_response_t *response_data;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   fd = open(TDDI_FILE_NAME, O_RDWR);

   if (fd == -1)
   {
	  al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : Error while opening file %s : %s<%d>\n",TDDI_FILE_NAME, __func__, __LINE__);
      fprintf(stderr, "Error opening "TDDI_FILE_NAME "\n");
      return -1;
   }

   packet.signature = TDDI_PACKET_SIGNATURE;
   packet.version   = TDDI_CURRENT_VERSION;
   packet.data_size = 0;
   packet.type      = TDDI_PACKET_TYPE_REQUEST;
   packet.sub_type  = TDDI_REQUEST_TYPE_GET_DS_IF;

   ret = write(fd, &packet, sizeof(tddi_packet_t));

   if (ret != sizeof(tddi_packet_t))
   {
	  al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : Error while writing into the file %s(ret : %d) : %s<%d>\n",TDDI_FILE_NAME, ret, __func__, __LINE__);
      fprintf(stderr, "Error writing to "TDDI_FILE_NAME " ret=%d\n", ret);
      return -1;
   }

   lseek(fd, 0, SEEK_CUR);

   ret = read(fd, buffer, sizeof(buffer));

   if (ret < sizeof(tddi_packet_t) + sizeof(tddi_get_ds_if_response_t))
   {
	  al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : Error as the size of the buffer is lesser than the expected(size : %d) : %s<%d>\n", __func__, __LINE__,ret);
      return -1;
   }

   close(fd);

   response_packet = (tddi_packet_t *)buffer;
   response_data   = (tddi_get_ds_if_response_t *)(buffer + sizeof(tddi_packet_t));

   switch (response_data->response)
   {
   case TDDI_GET_DS_IF_STARTED:
      memset(current_if_name, 0, 256);
      memcpy(current_if_name, buffer + sizeof(tddi_packet_t) + sizeof(tddi_get_ds_if_response_t), response_data->if_name_length);
      fprintf(stderr, "\nCONFIGD:\tDS net_if is %s\n", current_if_name);
      return 0;

   case TDDI_GET_DS_IF_RESPONSE_SCANNING:
	  al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : Error as the response of DS_IF is in SCANNING : %s<%d>\n", __func__, __LINE__);
      return -1;

   case TDDI_GET_DS_IF_RESPONSE_NOT_STARTED:
	  al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : Error as the response of DS_IF is not started : %s<%d>\n", __func__, __LINE__);
      return -1;
   }
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return 0;
}


static void _wait_for_ap_to_start()
{
   int max;
   int ret;
   
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter: %s<%d>\n",__func__,__LINE__);
   max = 6000;
   printf("ap_start\n");
   while (max--)
   {
      ret = _get_ds_netif();
      if (ret != 0)
      {
         sleep(1);
      }
      else
      {
         break;
      }
   }
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
}


static void _set_ds_netif_ip_address()
{
   FILE          *fp;
   int           bytes[4];
   unsigned char line[80];
   unsigned char ip_address[10];
   unsigned char subnet_mask[10];
   unsigned char broadcast_address[15];
   char          command[80];

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   sprintf(command, "uci get network.mip.ipaddr > /tmp/network");
   EXECUTE_COMMAND(command);

   fp = fopen("/tmp/network", "r");

   if (!fp)
   {
	  al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : Error as /tmp/network file is not found : %s<%d>\n", __func__, __LINE__);
      printf("/tmp/network not found\n");
      return;
   }

   fscanf(fp, "%s", line);
   sscanf((const char *)line, "%d.%d.%d.%d", &bytes[0], &bytes[1], &bytes[2], &bytes[3]);
   ip_address[0] = bytes[0];
   ip_address[1] = bytes[1];
   ip_address[2] = bytes[2];
   ip_address[3] = bytes[3];

   fclose(fp);

   sprintf(command, "uci get network.mip.netmask > /tmp/network");
   EXECUTE_COMMAND(command);

   fp = fopen("/tmp/network", "r");

   if (!fp)
   {
	  al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : Error as /tmp/network file is not found : %s<%d>\n", __func__, __LINE__);
      printf("/tmp/network not found\n");
      return;
   }

   fscanf(fp, "%s", line);
   sscanf((const char *)line, "%d.%d.%d.%d", &bytes[0], &bytes[1], &bytes[2], &bytes[3]);
   subnet_mask[0] = bytes[0];
   subnet_mask[1] = bytes[1];
   subnet_mask[2] = bytes[2];
   subnet_mask[3] = bytes[3];

   fclose(fp);

   sprintf(command, "uci get network.mip.broadcast > /tmp/network");
   EXECUTE_COMMAND(command);

   fp = fopen("/tmp/network", "r");

   if (!fp)
   {
	  al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : Error as /tmp/network file is not found : %s<%d>\n", __func__, __LINE__);
      printf("/tmp/network not found\n");
      return;
   }


   fscanf(fp, "%s", line);
   sscanf((const char *)line, "%d.%d.%d.%d", &bytes[0], &bytes[1], &bytes[2], &bytes[3]);
   broadcast_address[0] = bytes[0];
   broadcast_address[1] = bytes[1];
   broadcast_address[2] = bytes[2];
   broadcast_address[3] = bytes[3];
   fclose(fp);

#ifdef CONFIGD_NO_MIP
   sprintf(command, "ifconfig %s %d.%d.%d.%d netmask %d.%d.%d.%d broadcast %d.%d.%d.%d",
           current_if_name,
           ip_address[0], ip_address[1],
           ip_address[2], ip_address[3],
           subnet_mask[0], subnet_mask[1],
           subnet_mask[2], subnet_mask[3],
           broadcast_address[0], broadcast_address[1],
           broadcast_address[2], broadcast_address[3]
           );
#else
   sprintf(command, "ifconfig mip0 %d.%d.%d.%d netmask %d.%d.%d.%d broadcast %d.%d.%d.%d",
           ip_address[0], ip_address[1],
           ip_address[2], ip_address[3],
           subnet_mask[0], subnet_mask[1],
           subnet_mask[2], subnet_mask[3],
           broadcast_address[0], broadcast_address[1],
           broadcast_address[2], broadcast_address[3]
           );
#endif

   EXECUTE_COMMAND(command)

   read_from_network_file();

   sprintf(command, "route add default gateway %d.%d.%d.%d",
           imcp_ipconfig_data->gateway[0],
           imcp_ipconfig_data->gateway[1],
           imcp_ipconfig_data->gateway[2],
           imcp_ipconfig_data->gateway[3]);

   EXECUTE_COMMAND(command)

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
}


int send_packet(void *data, int data_length, struct sockaddr_in *client_addr)
{
   int ret;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter: %s<%d>\n",__func__,__LINE__);
   ret = send_packet_ex(data, data_length, client_addr, 1, 1);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"CONFIGD : INFO : ret : %d\n",ret);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return ret;
}


/*
 *	send data to client_addr
 */
int send_packet_ex(void *data, int data_length, struct sockaddr_in *client_addr, int encrypt, int broadcast)
{
   int                sock, clilen, ret, on, output_data_length;
   struct sockaddr_in serv_addr;
   char               output[2400];
   char               *buffer_ptr, *output_data_ptr;
   unsigned short     cpu2le16;	
   unsigned short     flags;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   if (encrypt)
   {
#ifdef _CONFIGD_USE_ENCRYPTION_
      buffer_ptr   = data + get_packet_type_offset();
	
      *buffer_ptr |= 0x80;

      output_data_length = get_imcp_signature_mesh_id_size();

      memcpy(output, data, output_data_length);
      buffer_ptr      = data + output_data_length;
      output_data_ptr = output + output_data_length;

      ret = al_encrypt(AL_CONTEXT buffer_ptr, data_length - output_data_length, mesh_id_key, mesh_id_key_length, output_data_ptr, AL_ENCRYPTION_DECRYPTION_METHOD_AES_ECB);
      if (ret > 0)
      {
         output_data_length += ret;
      }
      else
      {
		 al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : Error as Encryption failed dropping packet : %s<%d>\n", __func__, __LINE__);
         printf("\nCONFIGD:\tEncryption failed dropping packet...");
         return -1;
      }
#else
      memcpy(output, data, data_length);
      output_data_length = data_length;
#endif
   }
   else
   {
      memcpy(output, data, data_length);
      output_data_length = data_length;
   }

   if ((sock = socket(AF_INET, SOCK_DGRAM, 0)) < 0)
   {
	  al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : Error opening socket for sending packet : %s<%d>\n", __func__, __LINE__);
      printf("\nCONFIGD:\tError opening socket for sending packet...");
      return -1;
   }

   on = 1;
   setsockopt(sock, SOL_SOCKET, SO_BROADCAST, &on, sizeof(on));


   bzero((char *)&serv_addr, sizeof(serv_addr));

   if (broadcast)
   {
      serv_addr.sin_family      = AF_INET;
      serv_addr.sin_port        = htons(IMCP_SERVER_PORT);
      serv_addr.sin_addr.s_addr = inet_addr("255.255.255.255");
   }
   else
   {
      memcpy(&serv_addr, client_addr, sizeof(struct sockaddr_in));
   }

   clilen = sizeof(struct sockaddr_in);

   ret = sendto(sock, output, output_data_length, 0, (const struct sockaddr *)&serv_addr, clilen);
   if (ret == -1)
   {
	  al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : Error while sending packet : %s<%d>\n", __func__, __LINE__);
      printf("\nCONFIGD:\tError while sending packet...");
      perror("sendto");
   }
	else
	{
   close(sock);
		ret = send_to_mgmt_gw(output,output_data_length);
	}
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit: %s<%d>\n",__func__,__LINE__);	
   return ret;
}


/*
 *	send data to client_addr
 */
int send_unicast_packet(void *data, int data_length, struct sockaddr_in *client_addr)
{
   int sock, clilen;
   int ret;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   if ((sock = socket(AF_INET, SOCK_DGRAM, 0)) < 0)
   {
	  al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : Error while opening socket for sending packet : %s<%d>\n", __func__, __LINE__);
      printf("\nCONFIGD:\tError opening socket for sending packet...");
      return -1;
   }

   clilen = sizeof(struct sockaddr_in);

   ret = sendto(sock, data, data_length, 0, (const struct sockaddr *)client_addr, clilen);
   if (ret == -1)
   {
	  al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : Error while sending unicast packet : %s<%d>\n", __func__, __LINE__);
      printf("\nCONFIGD:\tError while sending unicast packet...");
      perror("sendto");
   }

   close(sock);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit: %s<%d>\n",__func__,__LINE__);

   return ret;
}


#include "configd_helper.c"
#include "imcp_helper.c"

int packet_process_helper(int packet_type, unsigned char *buffer, int buffer_length, struct sockaddr_in *client_addr)
{

//	al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

	/* When firmware upgrade is going on, we should serve only FIRMWARE UPGRADE request*/
	if(get_upgrade_status() && packet_type != IMCP_SW_UPDATE_REQUEST)
	{
	  al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : Error as Fw upgrade is going on : %s<%d>\n", __func__, __LINE__);
			printf("Fw upgrade is going on\n");
			return -1;
	}
   switch (packet_type)
   {
   case IMCP_RESET:             /* Reset packet sent by viewer to node */
   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "CONFIGD : INFO : IMCP_RESET Packet_type : %d : %s<%d>\n", packet_type,__func__,__LINE__);
      imcp_reset_helper(buffer, client_addr);
      break;

   case IMCP_AP_CONFIGURATION_INFO:      /* AP Config packet sent by Viewer to node */
   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "CONFIGD : INFO : IMCP_AP_CONFIGURATION_INFO Packet_type : %d : %s<%d>\n", packet_type,__func__,__LINE__);
      imcp_apconfig_info_helper(buffer, client_addr);
      break;

   case IMCP_AP_CONFIG_REQUEST_RESPONSE:      /* Request sent by Viewer to s back AP config only (Not used )*/
   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "CONFIGD : INFO : IMCP_AP_CONFIG_REQUEST_RESPONSE Packet_type : %d : %s<%d>\n", packet_type, __func__, __LINE__);
      imcp_apconfig_request_response_helper(buffer, client_addr);
      break;

   case IMCP_AP_IP_CONFIGURATION_INFO:      /* IP config packet sent by viewer to node */
   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "CONFIGD : INFO : IMCP_AP_IP_CONFIGURATION_INFO Packet_type : %d : %s<%d>\n", packet_type, __func__, __LINE__);
      imcp_ipconfig_info_helper(buffer, client_addr);
      break;

   case IMCP_AP_IP_CONFIG_REQUEST_RESPONSE:      /* Request sent by Viewer to send back IP config only (Not used )*/
   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "CONFIGD : INFO : IMCP_AP_IP_CONFIG_REQUEST_RESPONSE Packet_type : %d : %s<%d>\n", packet_type, __func__, __LINE__);
      imcp_ipconfig_request_response_helper(buffer, client_addr);
      break;

   case IMCP_AP_START_IP_INTERFACE:      /* Not used */
   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "CONFIGD : INFO : IMCP_AP_START_IP_INTERFACE Packet_type : %d : %s<%d>\n", packet_type, __func__,__LINE__);
      imcp_start_ip_if_request_helper(buffer, client_addr);
      break;

   case IMCP_SW_UPDATE_REQUEST:         /* Sent by NMS to node for performing F/W upgrade */
   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "CONFIGD : INFO : IMCP_SW_UPDATE_REQUEST Packet_type : %d : %s<%d>\n", packet_type, __func__, __LINE__);
      imcp_sw_update_request_helper(buffer, client_addr, packet_type, buffer_length);
      break;

   case IMCP_MESH_KEY_UPDATE:           /* Sent by NMS when either to new network or changing Key */
   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "CONFIGD : INFO : IMCP_MESH_KEY_UPDATE Packet_type : %d : %s<%d>\n", packet_type, __func__, __LINE__);
      imcp_mesh_key_update_helper(buffer, client_addr);
      break;

   case IMCP_CONFIG_REQUEST_RESPONSE:
      /* PACKET NO - 25 : Request sent by Viewer to send back AP, IP, VLAN, DOT11E_CATEGORY config */
   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "CONFIGD : INFO : IMCP_CONFIG_REQUEST_RESPONSE Packet_type : %d : %s<%d>\n", packet_type, __func__, __LINE__);
      imcp_config_request_response_helper(buffer, client_addr);
      break;

   case IMCP_VLAN_CONFIGURATION_INFO:      /* Request sent by Viewer to send back AP,IP and VLAN config */
   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "CONFIGD : INFO : IMCP_VLAN_CONFIGURATION_INFO Packet_type : %d : %s<%d>\n", packet_type, __func__, __LINE__);
      imcp_vlan_config_info_helper(buffer, client_addr);
      break;

   case IMCP_CONFIGD_WATCHDOG_REQUEST:      /* Packet sent by watchd to check if configd is alive */
   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "CONFIGD : INFO : IMCP_CONFIGD_WATCHDOG_REQUEST Packet_type : %d : %s<%d>\n", packet_type, __func__, __LINE__);
      imcp_configd_watchdog_request_helper(buffer, client_addr);
      break;

   case IMCP_RESTORE_DEFAULTS_REQUEST:      /* Request sent by Viewer to restore meshap.conf */
   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "CONFIGD : INFO : IMCP_RESTORE_DEFAULTS_REQUEST Packet_type : %d : %s<%d>\n", packet_type, __func__, __LINE__);
      imcp_restore_defaults_request_response_helper(buffer, client_addr);
      break;

   case IMCP_REG_DOMAIN_COUNTRY_CODE_REQUEST_RESPONSE:      /* Request sent by Viewer to send back reg_domain and country code config */
   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "CONFIGD : INFO : IMCP_REG_DOMAIN_COUNTRY_CODE_REQUEST_RESPONSE Packet_type : %d : %s<%d>\n", packet_type, __func__, __LINE__);
      imcp_reg_domain_country_code_request_response_helper(buffer, client_addr);
      break;

   case IMCP_REG_DOMAIN_COUNTRY_CODE_INFO:     /* REG config packet sent by viewer to node */
   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "CONFIGD : INFO : IMCP_REG_DOMAIN_COUNTRY_CODE_INFO Packet_type : %d : %s<%d>\n", packet_type, __func__, __LINE__);
      imcp_reg_domain_config_info_helper(buffer, client_addr);
      break;

   case IMCP_ACK_TIMEOUT_REQUEST_RESPONSE:      /* Request sent by Viewer to send back ack_timeout config */
   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "CONFIGD : INFO : IMCP_ACK_TIMEOUT_REQUEST_RESPONSE Packet_type : %d : %s<%d>\n", packet_type, __func__, __LINE__);
      imcp_ack_timeout_request_response_helper(buffer, client_addr);
      break;

   case IMCP_ACK_TIMEOUT_INFO:     /* ack_timeout config packet sent by viewer to node */
   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "CONFIGD : INFO : IMCP_ACK_TIMEOUT_INFO Packet_type : %d : %s<%d>\n", packet_type, __func__,__LINE__);
      imcp_ack_timeout_config_info_helper(buffer, client_addr);
      break;

   case IMCP_HIDE_SSID_INFO:      /* Sent by NMS to node, The Req/Resp packet is combined in IMCP_CONFIG_REQUEST_RESPONSE */
   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "CONFIGD : INFO : IMCP_HIDE_SSID_INFO Packet_type : %d : %s<%d>\n", packet_type, __func__, __LINE__);
      imcp_hide_ssid_config_info_helper(buffer, client_addr);
      break;

   case IMCP_DOT11E_CATEGORY_CONFIGURATION_INFO:      //This is not handled
   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "CONFIGD : INFO : IMCP_DOT11E_CATEGORY_CONFIGURATION_INFO Packet_type : %d : %s<%d>\n", packet_type, __func__, __LINE__);
      /* PACKET NO - 43 : Updated 802.11e category configuration sent by viewer, configd sends receipt ack */
      imcp_dot11e_category_update_info_helper(buffer, client_addr);
      break;

   case IMCP_DOT11E_IF_CONFIGURATION_INFO:
   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "CONFIGD : INFO : IMCP_DOT11E_IF_CONFIGURATION_INFO Packet_type : %d : %s<%d>\n", packet_type, __func__, __LINE__);
      /* PACKET NO - 44 : Updated 802.11e if configuration sent by viewer, configd sends receipt ack (Not used )*/
      imcp_dot11e_if_config_update_info_helper(buffer, client_addr);
      break;

   case IMCP_ACL_CONFIGURATION_INFO:      // This is not handled
   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "CONFIGD : INFO : IMCP_ACL_CONFIGURATION_INFO Packet_type : %d : %s<%d>\n", packet_type, __func__, __LINE__);
      /* PACKET NO - 47 : New ACL configuration sent by viewer, configd sends receipt ack */
      imcp_acl_config_info_helper(buffer, client_addr);
      break;

   case IMCP_IF_PRIVATE_CHANNEL_INFO_REQUEST_RESPONSE:
   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "CONFIGD : INFO : IMCP_IF_PRIVATE_CHANNEL_INFO_REQUEST_RESPONSE Packet_type : %d : %s<%d>\n", packet_type, __func__, __LINE__);
      /* PACKET NO - 50 : Request sent by Viewer to send back Private channel info config */
      imcp_if_priv_channel_request_response_helper(buffer, client_addr);
      break;

   case IMCP_IF_PRIVATE_CHANNEL_INFO:
   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "CONFIGD : INFO : IMCP_IF_PRIVATE_CHANNEL_INFO Packet_type : %d : %s<%d>\n", packet_type, __func__, __LINE__);
      /* PACKET NO - 51 : Updated if private channel configuration sent by viewer, configd sends receipt ack */
      imcp_if_priv_channel_info_helper(buffer, client_addr);
      break;

   case IMCP_EFFISTREAM_REQUEST_RESPONSE:
   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "CONFIGD : INFO : IMCP_EFFISTREAM_REQUEST_RESPONSE Packet_type : %d : %s<%d>\n", packet_type, __func__, __LINE__);
      /* PACKET NO - 52 :  Request by viewer for Effistream info */
      imcp_effistream_request_response_helper(buffer, client_addr);
      break;

   case IMCP_EFFISTREAM_INFO:
   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "CONFIGD : INFO : IMCP_EFFISTREAM_INFO Packet_type : %d : %s<%d>\n", packet_type, __func__, __LINE__);
      /* PACKET NO - 53 :  Effistream info sent by viewer */
      imcp_effistream_info_helper(buffer, client_addr);
      break;

   case IMCP_AP_SIP_CONFIGURATION_INFO:
   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "CONFIGD : INFO : IMCP_AP_SIP_CONFIGURATION_INFO Packet_type : %d : %s<%d>\n", packet_type, __func__, __LINE__);
      /* PACKET NO - 61 : sip configuration sent by/to meshviewer */
      imcp_sip_config_info_helper(buffer, client_addr);
      break;

	case IMCP_MAC_ADDR_INFO_REQUEST:
   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "CONFIGD : INFO : IMCP_MAC_ADDR_INFO_REQUEST Packet_type : %d : %s<%d>\n", packet_type, __func__, __LINE__);
		/* PACKET NO - 63 : MAC Address Info requested by Viewer, configd sends MAC addresses*/
		imcp_mac_addr_request_response_helper(buffer, client_addr);
		break;

	case IMCP_MGMT_GW_CONFIG_REQUEST:
   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "CONFIGD : INFO : IMCP_MGMT_GW_CONFIG_REQUEST Packet_type : %d : %s<%d>\n", packet_type, __func__, __LINE__);
		/* PACKET NO - 64: MGMT GW config sent by nms */
		printf("IMCP_MGMT_GW_CONFIG_REQUEST -------->Received......\n");
		imcp_mgmt_gw_info_helper(buffer, client_addr);
		break;

	case IMCP_HW_INFO_REQUEST_RESPONSE:
   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "CONFIGD : INFO : IMCP_HW_INFO_REQUEST_RESPONSE Packet_type : %d : %s<%d>\n", packet_type, __func__, __LINE__);
		/* PACKET NO - 13: HW_INFO request in case of remote viewer */
		printf("\nCONFIGD:\tIMCP_HW_INFO_REQUEST_RESPONSE,Packet Type =%d", packet_type);
		send_hw_info_reply(buffer,client_addr);
		break;

	case IMCP_STATION_INFO_REQUEST_RESPONSE:
   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION, "CONFIGD : INFO : IMCP_STATION_INFO_REQUEST_RESPONSE Packet_type : %d : %s<%d>\n", packet_type, __func__, __LINE__);
		/* PACKET NO - 41: STATION INFO request in case of remote viewer */
		printf("\nCONFIGD:\tIMCP_STATION_INFO_REQUEST_RESPONSE,Packet Type =%d", packet_type);
		send_station_info_reply(buffer,client_addr,STA_INFO_PKT_TYPE_1);
		break;

   default:
      /*printf("\nCONFIGD:\tIMCP Packet not processed , Packet Type = %d",packet_type);*/
      return -1;
   }

   return 0;
}


#if 1 
static unsigned char validate_dot11e_category_data(dot11e_conf_category_details_t *category_info, config_validation_report_t  *validation_report)
{
	if(VALIDATE_CAT_ACWMIN_VAL(category_info->category, category_info->acwmin))   {
		VAL_PRINTF("acwmin_val : %d\n category : %d (0 - Background, 1 - Besteffort, 2 - Video, 3 - voice)\n",category_info->acwmin, category_info->category);
		prepare_error_report(NULL, NODE_DOT11E_CAT, UNSIGNEDSHORT, &category_info->acwmin, validation_report);
		return INVALID;
	}

	if(VALIDATE_ACWMIN_VAL(category_info->acwmin))  {
		VAL_PRINTF("acwmin_val : %d\n category : %d (0 - Background, 1 - Besteffort, 2 - Video, 3 - voice)\n", category_info->acwmin, category_info->category);
		prepare_error_report(NULL, NODE_DOT11E_CAT, UNSIGNEDSHORT, &category_info->acwmin, validation_report);
		return INVALID;
	}

	if(VALIDATE_ACWMIN_ACWMAX_VAL(category_info->acwmax))   {
		VAL_PRINTF("acwmax_val : %d\n category : %d (0 - Background, 1 - Besteffort, 2 - Video, 3 - voice)\n", category_info->acwmax, category_info->category);
		prepare_error_report(NULL, NODE_DOT11E_CAT, UNSIGNEDSHORT, &category_info->acwmax, validation_report);
		return INVALID;
	}
	return SUCCESS;
}

static unsigned char validate_vlan_tag_info(short vlan_tag, config_validation_report_t  *validation_report)
{

	if(vlan_tag < 0 || vlan_tag > MAX_VLAN_TAG_VALUE)
	{
		VAL_PRINTF("Invalid Vlan tag value : %d\n, valid range is 0 to 4095\n", vlan_tag);
		prepare_error_report(NULL, IF_VLAN_TAG, UNSIGNEDSHORT, &vlan_tag, validation_report);
		return INVALID;
	}
	return SUCCESS;
}


static unsigned char validate_vlan_essid_length(int len, config_validation_report_t  *validation_report)
{
	if(len > MAX_ESSID_LEN_VALUE)
	{
		prepare_error_report(NULL, NODE_VLAN_ESSID_LENGTH, INTEGER, &len, validation_report);
		return OUT_OF_RANGE;
	}
	return SUCCESS;
}

static unsigned char validate_vlan_name_length(int len, config_validation_report_t  *validation_report)
{
	if(len > MAX_VLAN_NAME_VALUE)
	{
		prepare_error_report(NULL, NODE_VLAN_NAME_LENGTH, INTEGER, &len, validation_report);
		return OUT_OF_RANGE;
	}
	return SUCCESS;
}

static unsigned char validate_vlan_dot1p_priority_info(unsigned char dot1p_p, config_validation_report_t  *validation_report)
{
	if(dot1p_p < 0 || dot1p_p > MAX_VLAN_DOT1P_VALUE)
	{
		VAL_PRINTF("Invalid Vlan dot1p_p value : %d, valid range is 0 to 7\n", dot1p_p);
		prepare_error_report(NULL, IF_VLAN_DOT1P_PRIORITY, UNSIGNEDCHAR, &dot1p_p, validation_report);
		return INVALID;
	}
	return SUCCESS;
}
unsigned char validate_vlan_security_info(al_conf_if_info_t *if_info,al_security_info_t *security , config_validation_report_t  *validation_report)
{
	unsigned char ret;
	ret = validate_security_info(NULL,security,validation_report);
	return ret;
}


static unsigned char validate_interface_frag_threshold(al_conf_if_info_t *if_info, config_validation_report_t  *validation_report)
{
	if(if_info->frag_th < 1 || if_info->frag_th > MAX_FRAG_TH_VALUE)
	{
		prepare_error_report(if_info, IF_FRAG_TH, INTEGER, &if_info->frag_th, validation_report);
		return INVALID;
	}
	return SUCCESS;
}

static unsigned char validate_node_frag_threshold(int frag_threshold, config_validation_report_t  *validation_report)
{
	if(frag_threshold < 1 || frag_threshold > MAX_FRAG_TH_VALUE)
	{
		prepare_error_report(NULL, NODE_FRAG_TH, INTEGER, &frag_threshold, validation_report);
		return INVALID;
	}
	return SUCCESS;
}

static unsigned char validate_interface_rts_threshold(al_conf_if_info_t *if_info, config_validation_report_t  *validation_report)
{
	if(if_info->rts_th < 1 || if_info->rts_th > MAX_RTS_TH_VALUE)
	{
		prepare_error_report(if_info, IF_RTS_TH, INTEGER, &if_info->rts_th, validation_report);
		return INVALID;
	}
	return SUCCESS;
}

static unsigned char validate_node_rts_threshold(int rts_th, config_validation_report_t  *validation_report)
{
	if(rts_th < 1 || rts_th > MAX_RTS_TH_VALUE)
	{
		prepare_error_report(NULL, NODE_RTS_TH, INTEGER, &rts_th, validation_report);
		return INVALID;
	}
	return SUCCESS;
}

static unsigned char validate_node_heartbeat_interval(unsigned char heartbeat_interval, config_validation_report_t  *validation_report)
{
	if(heartbeat_interval < 1 || heartbeat_interval > MAX_HB_VALUE)
	{
		prepare_error_report(NULL, NODE_HB_INTERVAL, UNSIGNEDCHAR, &heartbeat_interval, validation_report);
		return INVALID;
	}
	return SUCCESS;
}

static unsigned char validate_interface_txpower(al_conf_if_info_t *if_info, config_validation_report_t  *validation_report)
{
	if(if_info->txpower < 0 || if_info->txpower > MAX_TX_POWER_VALUE) {
		VAL_PRINTF("Failure in interface tx_power validation : %d, if_name : %s\n",if_info->txpower, if_info->name);
		prepare_error_report(if_info, IF_TX_POWER, INTEGER, &if_info->txpower, validation_report);
		return INVALID;
	}
	return SUCCESS;
}

int validate_n_related_rates(al_conf_if_info_t *if_info, config_validation_report_t  *validation_report)
{
	int i;

	int _20_short_gi_tx_rates[]     = {7.2, 14.4, 21.7, 28.9, 43.3, 57.8, 65, 0};
	int _20_long_gi_tx_rates[]      = {6.5, 13, 19.5, 26, 39, 52, 58.5,65, 0};
	int _40_short_gi_tx_rates[]     = {15, 30, 45, 60, 90, 120, 135, 150, 0};
	int _40_long_gi_tx_rates[]      = {13.5, 27, 40.5, 54, 81, 108, 121.5, 135, 0};

	if(if_info->ht_capab.ht_bandwidth == AL_CONF_IF_HT_PARAM_20) {
		if(if_info->ht_capab.gi_20 == AL_CONF_IF_HT_PARAM_SHORT)   {
			for(i = 0; i < (sizeof(_20_short_gi_tx_rates) / sizeof(int)); i++) {
				if(if_info->txrate == _20_short_gi_tx_rates[i]) {
					return SUCCESS;
				}
			}
			VAL_PRINTF("Invalid 20mhz short guard interval txrate : %d if_name : %s\n",if_info->txrate,if_info->name);
			prepare_error_report(if_info, IF_TX_RATE, INTEGER, &if_info->txrate, validation_report);
			return INVALID;
		}

		if(if_info->ht_capab.gi_20 == AL_CONF_IF_HT_PARAM_LONG)    {
			for(i = 0; i < (sizeof(_20_long_gi_tx_rates) / sizeof(int)); i++) {
				if(if_info->txrate == _20_long_gi_tx_rates[i]) {
					return SUCCESS;
				}
			}
			VAL_PRINTF("Invalid 20mhz long guard interval txrate : %d if_name : %s\n",if_info->txrate, if_info->name);
			prepare_error_report(if_info, IF_TX_RATE, INTEGER, &if_info->txrate, validation_report);
			return INVALID;
		}
	}

	if(if_info->ht_capab.ht_bandwidth == AL_CONF_IF_HT_PARAM_40_ABOVE || if_info->ht_capab.ht_bandwidth == AL_CONF_IF_HT_PARAM_40_BELOW) {
		if(if_info->ht_capab.gi_40 == AL_CONF_IF_HT_PARAM_SHORT)   {
			for(i = 0; i < (sizeof(_40_short_gi_tx_rates) / sizeof(int)); i++) {
				if(if_info->txrate == _40_short_gi_tx_rates[i]) {
					return SUCCESS;
				}
			}
			VAL_PRINTF("Invalid 40mhz short guard interval txrate : %d if_name : %s\n",if_info->txrate, if_info->name);
			prepare_error_report(if_info, IF_TX_RATE, INTEGER, &if_info->txrate, validation_report);
			return INVALID;
		}

		if(if_info->ht_capab.gi_40 == AL_CONF_IF_HT_PARAM_LONG)    {
			for(i = 0; i < (sizeof(_40_long_gi_tx_rates) / sizeof(int)); i++) {
				if(if_info->txrate == _40_long_gi_tx_rates[i]) {
					return SUCCESS;
				}
			}
			VAL_PRINTF("Invalid 40mhz long guard interval txrate : %d if_name : %s\n",if_info->txrate, if_info->name);
			prepare_error_report(if_info, IF_TX_RATE, INTEGER, &if_info->txrate, validation_report);
			return INVALID;
		}
	}
	return SUCCESS;
}

int validate_ac_related_rates(al_conf_if_info_t *if_info, config_validation_report_t  *validation_report)
{
	int i = 0, ret = 0;

	int _80_short_gi_tx_rates[]     = {32.5, 65, 97.5, 130, 195, 260, 292.5, 325, 390, 0};
	int _80_long_gi_tx_rates[]      = {29.3, 58.5, 87.8, 117, 175.5, 234, 263.3,292.5, 351, 0};


	if(if_info->vht_capab.vht_oper_bandwidth == 0)
	{
		ret = validate_n_related_rates(if_info, validation_report);
		if(ret) {
			return INVALID;
		}
		return SUCCESS;

	}
	if(if_info->vht_capab.vht_oper_bandwidth == 1) {
		if(if_info->vht_capab.gi_80 == AL_CONF_IF_HT_PARAM_SHORT)   {
			for(i = 0; i < (sizeof(_80_short_gi_tx_rates) / sizeof(int)); i++) {
				if(if_info->txrate == _80_short_gi_tx_rates[i]) {
					return SUCCESS;
				}
			}
			VAL_PRINTF("Invalid 80mhz short guard interval txrate : %d if_name : %s\n",if_info->txrate,if_info->name);
			prepare_error_report(if_info, IF_TX_RATE, INTEGER, &if_info->txrate, validation_report);
			return INVALID;
		}

		if(if_info->vht_capab.gi_80 == AL_CONF_IF_HT_PARAM_LONG)    {
			for(i = 0; i < (sizeof(_80_long_gi_tx_rates) / sizeof(int)); i++) {
				if(if_info->txrate == _80_long_gi_tx_rates[i]) {
					return SUCCESS;
				}
			}
			VAL_PRINTF("Invalid 80mhz long guard interval txrate : %d if_name : %s\n",if_info->txrate, if_info->name);
			prepare_error_report(if_info, IF_TX_RATE, INTEGER, &if_info->txrate, validation_report);
			return INVALID;
		}
	}
	if( if_info->vht_capab.vht_oper_bandwidth == 2 || if_info->vht_capab.vht_oper_bandwidth == 3)
	{
		VAL_PRINTF("We are currently not validating for 80mhz, 160mhz & 80+80 mhz\n");
	}

	return SUCCESS;
}


static unsigned char validate_interface_txrate(al_conf_if_info_t *if_info, config_validation_report_t  *validation_report)
{
	int ret ;

	if(if_info->phy_sub_type == AL_CONF_IF_PHY_SUB_TYPE_802_11_A)   {
		if(VALIDATE_TXRATE_FOR_SUBTYPE_A(if_info->txrate)) {
			return SUCCESS;
		}
		else   {
			VAL_PRINTF("Invalid txrate value : %d for subtype 'A' valid values are 6,9,12,18,24,36,48,54 if_name : %s\n",if_info->txrate,if_info->name);
			prepare_error_report(if_info, IF_TX_RATE, INTEGER, &if_info->txrate, validation_report);
			return INVALID;
		}
	}

	if(if_info->phy_sub_type == AL_CONF_IF_PHY_SUB_TYPE_802_11_BG) {
		if(VALIDATE_TXRATE_FOR_SUBTYPE_BG(if_info->txrate))    {
			return SUCCESS;
		}
		else   {
			VAL_PRINTF("Invalid txrate value : %d for subtype 'BG',valid values are 1,2,5,6,9,11,12,18,24,36,48,54 if_name : %s\n",if_info->txrate,if_info->name);
			prepare_error_report(if_info, IF_TX_RATE, INTEGER, &if_info->txrate, validation_report);
			return INVALID;
		}
	}

	if(if_info->phy_sub_type == AL_CONF_IF_PHY_SUB_TYPE_802_11_B)  {
		if(VALIDATE_TXRATE_FOR_SUBTYPE_B(if_info->txrate)) {
			return SUCCESS;
		}
		else   {
			VAL_PRINTF("Invalid txrate value : %d for subtype 'B',valid values are 1,2,5,11 if_name : %s\n",if_info->txrate, if_info->name);
			prepare_error_report(if_info, IF_TX_RATE, INTEGER, &if_info->txrate, validation_report);
			return INVALID;
		}
	}

	if(if_info->phy_sub_type == AL_CONF_IF_PHY_SUB_TYPE_802_11_G)  {
		if(VALIDATE_TXRATE_FOR_SUBTYPE_FOR_G(if_info->txrate)) {
			return SUCCESS;
		}
		else   {
			VAL_PRINTF("Invalid txrate value : %d for subtype 'G',valid values are 6,9,12,18,24,36,48,54 if_name : %s\n",if_info->txrate,if_info->name);
			prepare_error_report(if_info, IF_TX_RATE, INTEGER, &if_info->txrate, validation_report);
			return INVALID;
		}
	}

	if((if_info->phy_sub_type == AL_CONF_IF_PHY_SUB_TYPE_802_11_N_2_4GHZ)|| (if_info->phy_sub_type == AL_CONF_IF_PHY_SUB_TYPE_802_11_N_5GHZ))
	{
		ret = validate_n_related_rates(if_info, validation_report);
		if(ret)
		{
			return INVALID;
		}
		return SUCCESS;
	}
	if(if_info->phy_sub_type == AL_CONF_IF_PHY_SUB_TYPE_802_11_BGN)
	{
		if(VALIDATE_TXRATE_FOR_SUBTYPE_BG(if_info->txrate)) {
			return SUCCESS;
		}
		ret = validate_n_related_rates(if_info, validation_report);
		if(ret) {
			return INVALID;
		}
		return SUCCESS;
	}
	if(if_info->phy_sub_type == AL_CONF_IF_PHY_SUB_TYPE_802_11_AN)
	{
		if(VALIDATE_TXRATE_FOR_SUBTYPE_A(if_info->txrate))
		{
			return SUCCESS;
		}
		ret = validate_n_related_rates(if_info, validation_report);
		if(ret) {
			return INVALID;
		}
		return SUCCESS;
	}

	if(if_info->phy_sub_type == AL_CONF_IF_PHY_SUB_TYPE_802_11_ANAC)
	{
		if(VALIDATE_TXRATE_FOR_SUBTYPE_A(if_info->txrate))  {
			return SUCCESS;
		}
		ret = validate_n_related_rates(if_info, validation_report);
		if(ret == SUCCESS) {
			return SUCCESS;
		}
		ret = validate_ac_related_rates(if_info, validation_report);
		if(ret) {
			return INVALID;
		}
		return SUCCESS;
	}

	if(if_info->phy_sub_type == AL_CONF_IF_PHY_SUB_TYPE_802_11_AC) {
		ret = validate_ac_related_rates(if_info, validation_report);
		if(ret) {
			return INVALID;
		}
	}
	return SUCCESS;
}


#if 1
static unsigned char validate_preferred_parent(unsigned char *preferred_parent,config_validation_report_t  *validation_report)
{
	int i;
	unsigned char mac_addr[30];
	sprintf(mac_addr, "%x:%x:%x:%x:%x:%x",preferred_parent[0],preferred_parent[1],preferred_parent[2],preferred_parent[3],preferred_parent[4],preferred_parent[5]);

	for(i=0;i<6;i++)
	{
		if((preferred_parent[i] < 0)||(preferred_parent[i] > 0xff))
		{
			prepare_error_report(NULL, NODE_PREFERRED_PARENT, STRING, mac_addr , validation_report);
			return OUT_OF_RANGE;
		}
	}
	return SUCCESS;
}
static unsigned char validate_gps_x_coordinate(unsigned char *latitude , config_validation_report_t  *validation_report)
{
	char * token;
	int precision;
	char x_coordinate[256];

	sprintf(x_coordinate, "%s",latitude);
	token = strtok(x_coordinate,".");
	while(token != NULL)
	{
		precision = strlen(token);
		token = strtok(NULL,".");
	}
	if(precision > 6)
	{
		prepare_error_report(NULL,NODE_GPS_X_COORDINATE,STRING,latitude,validation_report);
		return OUT_OF_RANGE;
	}
	return SUCCESS;
}
static unsigned char validate_gps_y_coordinate(unsigned char *longitude , config_validation_report_t  *validation_report)
{
	char * token;
	int precision;
	char y_coordinate[256];

	sprintf(y_coordinate, "%s",longitude);
	token = strtok(y_coordinate,".");
	while(token != NULL)
	{
		precision = strlen(token);
		token = strtok(NULL,".");
	}
	if(precision > 6)
	{
		prepare_error_report(NULL,NODE_GPS_Y_COORDINATE,STRING,longitude,validation_report);
		return OUT_OF_RANGE;
	}
	return SUCCESS;
}
unsigned char validate_interface_security_info(al_conf_if_info_t *if_info,al_security_info_t *security,config_validation_report_t  *validation_report)
{
	unsigned char ret = 0;
	ret =  validate_security_info(if_info,security,validation_report);
	return ret;
}


unsigned char validate_security_info(al_conf_if_info_t *if_info,al_security_info_t *security,config_validation_report_t  *validation_report)
{

	if(security->_security_rsn_radius.enabled == 1)
	{
		validate_WPA_WPA2_radius_sec_info(if_info,security,validation_report);
	}
	else
	{
		return SUCCESS;
	}
}
unsigned char validate_WEP_security_info(al_conf_if_info_t *if_info,int key,config_validation_report_t  *validation_report)
{
	if( if_info->security_info._security_wep.strength == WEP_KEY_STRENGTH_40_OR_60_BIT)
	{
		if( key != 20)
		{
			prepare_error_report(if_info, IF_SECURITY, INTEGER, &key, validation_report);
			return INVALID;
		}
	}
	else if(if_info->security_info._security_wep.strength == WEP_KEY_STRENGTH_104_OR_128_BIT)
	{
		if(key != 13)
		{
			prepare_error_report(if_info, IF_SECURITY, INTEGER,&key, validation_report);
			return INVALID;
		}
	}
	return SUCCESS;
}
unsigned char validate_WPA_WPA2_radius_sec_info(al_conf_if_info_t *if_info,al_security_info_t *security,config_validation_report_t  *validation_report)
{
	if((security->_security_rsn_radius.radius_port < 0)||(security->_security_rsn_radius.radius_port > 65536))
	{
		prepare_error_report(if_info, IF_SECURITY, INTEGER, &security->_security_rsn_radius.radius_port, validation_report);
		return OUT_OF_RANGE;
	}
	return SUCCESS;
}

#endif

int validating_capabilities()
{
	int                          fd;
	tddi_packet_t                *packet;
	tddi_get_hw_config_request_t *request;
	int                          ret,i;
	unsigned char                *request_packet;
	unsigned char                response_buffer[sizeof(tddi_packet_t) + sizeof(tddi_get_hw_config_t)] = { 0 };
	tddi_packet_t                *response_packet;
	tddi_get_hw_config_t         *response_data;
	int                          al_conf_handle;
	_al_conf_data_t              *data;
	config_validation_report_t   validation_report;
	int							 length = 0;
	int							 index = 0;
	int							 dot11e_conf_handle = 0;
	dot11e_conf_category_t		*dot11e_data;

	dot11e_conf_handle	 =	read_dot11e_category_data();
	
	dot11e_data			 = (dot11e_conf_category_t *)dot11e_conf_handle; 

	al_conf_handle       = libalconf_read_config_data();

	data                 = (_al_conf_data_t *)al_conf_handle;

	VAL_PRINTF("*************validating capabilities entering*********************\n");

	length = strlen(data->name);

	ret = validate_node_name(length, &validation_report);
	if(ret != SUCCESS)
	{
		printf("Global Node name validation failed\n");
		return UNSUPPORTED;
	}

	length = strlen(data->description);

	ret = validate_node_description(length, &validation_report);
	if(ret != SUCCESS)
	{
		printf("Global Node name validation failed\n");
		return UNSUPPORTED;
	}
	
	//currently we are not verifying validations for this two parameters
	/*ret = validate_node_frag_threshold(data->frag_th, &validation_report);
	if(ret != SUCCESS)
	{
		printf("Global node frag_th validation failed\n");
		return UNSUPPORTED;
	}

	ret = validate_node_rts_threshold(data->rts_th, &validation_report);
	if(ret != SUCCESS)
	{
		printf("Node rts_th validation failed\n");
		return UNSUPPORTED;
	}*/

	ret = validate_preferred_parent(data->preferred_parent, &validation_report);
	if(ret != SUCCESS)
	{
		printf("global preferred parent validation failed\n");
		return UNSUPPORTED;
	}

	ret = validate_gps_x_coordinate(data->gps_x_coordinate, &validation_report);
	if(ret != SUCCESS)
	{
		printf("global gps_x_coordinate validation failed\n");
		return UNSUPPORTED;
	}

	ret = validate_gps_y_coordinate(data->gps_y_coordinate, &validation_report);
	if(ret != SUCCESS)
	{
		printf("global gps_y_coordinate validation failed\n");
		return UNSUPPORTED;
	}
	
	ret = validate_node_heartbeat_interval(data->heartbeat_interval, &validation_report);
	if(ret != SUCCESS)
	{
		printf("Global heartbeat interval validation failed\n");
		return UNSUPPORTED;
	}

	VAL_PRINTF("vlan_count val : %d\n", data->vlan_count);

	for (index = 1; index < data->vlan_count ; index++)	{
	ret = boot_time_validate_vlan_config(&data->vlan_info[index], &validation_report);
	if(ret != SUCCESS)
	{
		printf("validating vlan config failed\n");
		return UNSUPPORTED;
	}
	}
	
	for(i = 0; i < dot11e_data->count; i++)
	{
		ret = boot_time_validate_dot11e_category_data(&dot11e_data->category_info[i], &validation_report);
		if(ret != SUCCESS)
		{
			printf("dot11e category validation failed\n");
			return UNSUPPORTED;
		}
	}

	for (i = 0; i < data->if_count; i++)
	{
		if (data->if_info[i].phy_type != AL_CONF_IF_PHY_TYPE_ETHERNET)
		{
			if(TDDI_802_11_IS_N(data->if_info[i].phy_sub_type) || TDDI_802_11_IS_AC(data->if_info[i].phy_sub_type)) {
				request_packet = (unsigned char *)malloc(sizeof(tddi_packet_t) + sizeof(tddi_get_hw_config_request_t));
				packet         = (tddi_packet_t *)request_packet;
				request        = (tddi_get_hw_config_request_t *)(request_packet + sizeof(tddi_packet_t));

				packet->version   = TDDI_CURRENT_VERSION;
				packet->signature = TDDI_PACKET_SIGNATURE;
				packet->type      = TDDI_PACKET_TYPE_REQUEST;
				packet->data_size = sizeof(tddi_get_hw_config_request_t);
				packet->sub_type  = TDDI_REQUEST_TYPE_GET_HW_CONFIG;

				strcpy(request->if_name,data->if_info[i].name);
				request->sub_type = data->if_info[i].phy_sub_type;

				fd = open(TDDI_FILE_NAME, O_RDWR);

				if (fd == -1)
				{
					fprintf(stderr, "Error opening "TDDI_FILE_NAME "\n");
					free(request_packet);
					return NODE_CONFIG_FAILURE;
				}

				ret = write(fd, request_packet, sizeof(tddi_packet_t) + sizeof(tddi_get_hw_config_t));

				if (ret != sizeof(tddi_packet_t) + sizeof(tddi_get_hw_config_t))
				{
					fprintf(stderr, "Error writing to "TDDI_FILE_NAME " ret=%d\n", ret);
					free(request_packet);
					return NODE_CONFIG_FAILURE;
				}

				lseek(fd, 0, SEEK_CUR);

				ret = read(fd, response_buffer, sizeof(response_buffer));

				printf(" ret val : %d, size of response : %d\n",ret,sizeof(response_buffer));

				if (ret != sizeof(response_buffer))
				{
					fprintf(stderr, "configd : Response buffer size error while reading from "TDDI_FILE_NAME " ret=%d,errno=%d\n", ret, errno);
					switch (errno)
					{
						case EINTR:
							fprintf(stderr, "EINTR occurred\n");
							break;

						case EAGAIN:
							fprintf(stderr, "EAGAIN occurred\n");
							break;

						case EIO:
							fprintf(stderr, "EIO occurred\n");
							break;
						case EBADF:
							fprintf(stderr, "EBADF occurred\n");
							break;

						case EINVAL:
							fprintf(stderr, "EINVAL occurred\n");
							break;

						case EFAULT:
							fprintf(stderr, "EFAULT occurred\n");
							break;
					}
					free(request_packet);
					close(fd);
					return NODE_CONFIG_FAILURE;
				}

				free(request_packet);
				close(fd);

				response_packet = (tddi_packet_t *)response_buffer;

				response_data   = (tddi_get_hw_config_t *)(response_buffer + sizeof(tddi_packet_t));

				switch (response_data->response)
				{
					case TDDI_CONFIGURE_MESH_RESPONSE_SUCCESS:
						fprintf(stderr, "Getting md_configs successful\n");
						break;

					case TDDI_CONFIGURE_MESH_RESPONSE_BUSY:
						fprintf(stderr, "Mesh busy\n");
						break;

					default :
						al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : Error occurred in tddi response_data : %s : %d\n",__func__,__LINE__);
						VAL_PRINTF("Error occurred in tddi response_data : %s : %d\n",__func__,__LINE__);
			            prepare_error_report(&data->if_info[i], TDDI_RESPONSE_INVALID, UNSIGNEDCHAR, &response_data->response, validation_report);
						return NODE_CONFIG_FAILURE;
				}
				ret = validate_ht_capabilities(response_data->ht_cap,&data->if_info[i],&validation_report);
				if(ret != SUCCESS)
				{
					printf("HT validation got failed in interface : %s\n",data->if_info[i].name);
					return UNSUPPORTED;
				}
				ret = validate_vht_capabilities(response_data->vht_cap,&data->if_info[i], &validation_report);
				if(ret != SUCCESS)
				{
					printf("VHT validation got failed in interface : %s\n",data->if_info[i].name);
					return UNSUPPORTED;
				}
			}

			ret = validate_interface_frag_threshold(&data->if_info[i], &validation_report);
			if(ret != SUCCESS)
			{
				printf("Interface frag_th validation failed\n");
				return UNSUPPORTED;
			}
			ret = validate_interface_rts_threshold(&data->if_info[i], &validation_report);
			if(ret != SUCCESS)
			{
				printf("Interface rts_th validation failed\n");
				return UNSUPPORTED;
			}
			ret = validate_interface_txpower(&data->if_info[i], &validation_report);
			if(ret!= SUCCESS)
			{
				printf("Interface tx_power validation failed\n");
				return UNSUPPORTED;

			}
			ret = validate_interface_txrate(&data->if_info[i], &validation_report);
			if(ret!= SUCCESS)
			{
				printf("Interface txrate validation failed\n");
				return UNSUPPORTED;
			}
			ret = validate_interface_security_info(&data->if_info[i], &data->if_info[i].security_info, &validation_report);
			if(ret != SUCCESS)
			{
				printf("validating interface sec_info failed\n");
				return UNSUPPORTED;
			}

		}
	}
	VAL_PRINTF("*************validating capabilities exiting*********************\n");
	return 0;
}

unsigned char boot_time_validate_dot11e_category_data(dot11e_conf_category_details_t *category_info, config_validation_report_t  *validation_report)
{
	unsigned char ret = 0;
		ret = validate_dot11e_category_data(category_info, validation_report);
		if(ret != SUCCESS)
		{
			printf("dot11e category validation failed\n");
			return UNSUPPORTED;
		}
	return SUCCESS;
}

unsigned char boot_time_validate_vlan_config(al_conf_vlan_info_t *vlan_info, config_validation_report_t  *validation_report)
{
	unsigned char ret = 0;
	int			  length = 0;
	
	VAL_PRINTF("vlan tag val : %d\n",vlan_info->tag);

		ret = validate_vlan_tag_info(vlan_info->tag, validation_report);
		if(ret != SUCCESS)
		{
			printf("validating vlan_tag_info failed\n");
			return UNSUPPORTED;
		}
		VAL_PRINTF("vlan essid : %s\n",vlan_info->essid);
		length = strlen(vlan_info->essid);
		VAL_PRINTF("vlan essid len : %d\n",length);
		ret = validate_vlan_essid_length(length, validation_report);
		if(ret != SUCCESS)
		{
			printf("validating vlan_essid_len_info failed\n");
			return UNSUPPORTED;
		}
		length = strlen(vlan_info->name);
		ret = validate_vlan_name_length(length, validation_report);
		if(ret != SUCCESS)
		{
			printf("validating vlan_name_len_info failed\n");
			return UNSUPPORTED;
		}
		ret = validate_vlan_dot1p_priority_info(vlan_info->dot1p_priority, validation_report);
		if(ret != SUCCESS)
		{
			printf("validating vlan_dot1p_priority_info failed\n");
			return UNSUPPORTED;
		}
		ret = validate_vlan_security_info(NULL, &vlan_info->security_info, validation_report);
		if(ret != SUCCESS)
		{
			printf("validating vlan_sec_info failed\n");
			return UNSUPPORTED;
		}
	return SUCCESS;
}

unsigned char validate_vht_capabilities(tddi_vht_cap_t vht_cap,al_conf_if_info_t *if_info, config_validation_report_t  *validation_report)
{
	int ret_val;


	if(TDDI_802_11_IS_AC(if_info->phy_sub_type))
	{
	    VAL_PRINTF("Interface name in vht : %s\n",if_info->name);
		if(vht_cap.vht_supported == 0)
		{
			VAL_PRINTF("Driver doesnot support vht_capabilities\n");
			return 1;
		}

		ret_val = validate_max_mpdu_len(&vht_cap,&if_info->vht_capab);
		if(ret_val == 1){
			VAL_PRINTF("Driver does not support configured VHT capability [MAX_MPDU_LEN-*]\n Driver supported VHT capability [MAX_MPDU_LEN-*] : %d\n Meshap configured VHT capability [MAX_MPDU_LEN-*] : %d\n",(vht_cap.cap&(MESH_VHT_CAP_MAX_MPDU_LENGTH_MASK)),if_info->vht_capab.max_mpdu_len);
			prepare_error_report(if_info, IF_VHT_CAP_MAX_MPDU_LENGTH, UNSIGNEDCHAR, &if_info->vht_capab.max_mpdu_len, validation_report);
			return 1;
		}

		if(((if_info->vht_capab.supported_channel_width == VHT_CAP_SUPP_CH_160) || (if_info->vht_capab.supported_channel_width == VHT_CAP_SUPP_CH_160_80PLUS80)) && !(vht_cap.cap & MESH_VHT_CAP_SUPP_CHAN_WIDTH_MASK)) {
			VAL_PRINTF("Driver does not support configured VHT capability [SUPP_CHANNEL_WIDTH-*]\n  Driver supported VHT capability [SUPP_CHANNEL_WIDTH-*] : %d\n Meshap configured VHT capability [SUPP_CHANNEL_WIDTH-*] : %d\n",(vht_cap.cap & (MESH_VHT_CAP_SUPP_CHAN_WIDTH_MASK)),if_info->vht_capab.supported_channel_width);
			return 1;
		}

		if (((if_info->vht_capab.rx_ldpc) == VHT_CAP_RX_LDPC_INFO) && !(vht_cap.cap & MESH_VHT_CAP_RXLDPC)) {
			VAL_PRINTF("Driver does not support configured VHT capability [RX_LDPC]\n Driver supported VHT capability [RX_LDPC] : %d\n Meshap configured VHT capability [RX_LDPC] : %d\n",(vht_cap.cap & (MESH_VHT_CAP_RXLDPC)),if_info->vht_capab.rx_ldpc);
			return 1;
		}

		if((((if_info->vht_capab.gi_80) == VHT_CAP_SHORT_GI_80_AUTO) || ((if_info->vht_capab.gi_80) == VHT_CAP_SHORT_GI_80_SHORT)) && !(vht_cap.cap & MESH_VHT_CAP_SHORT_GI_80))  {
			VAL_PRINTF("Driver does not support configured VHT capability [GI_80]\n Driver supported VHT capability [GI_80] : %d\n Meshap configured VHT capability [GI_80] : %d\n",(vht_cap.cap & (MESH_VHT_CAP_SHORT_GI_80)),if_info->vht_capab.gi_80);
			 prepare_error_report(if_info, IF_VHT_CAP_GI_80, UNSIGNEDCHAR, &if_info->vht_capab.gi_80, validation_report);
			return 1;
		}

		if((((if_info->vht_capab.gi_160) == VHT_CAP_SHORT_GI_160_AUTO) || ((if_info->vht_capab.gi_160) == VHT_CAP_SHORT_GI_160_SHORT)) && !(vht_cap.cap & MESH_VHT_CAP_SHORT_GI_160))  {
			VAL_PRINTF("Driver does not support configured VHT capability [GI_160]\n Driver supported VHT capability [GI_160] : %d\n Meshap configured VHT capability [GI_160] : %d\n",(vht_cap.cap & (MESH_VHT_CAP_SHORT_GI_160)),if_info->vht_capab.gi_160);
			return 1;
		}

		if (((if_info->vht_capab.tx_stbc) == VHT_CAP_TXSTBC) && !(vht_cap.cap & MESH_VHT_CAP_TXSTBC)) {
			VAL_PRINTF("Driver does not support configured VHT capability [TX_STBC]\n Driver supported VHT capability [TX_STBC] : %d\n Meshap configured VHT capability [TX_STBC] : %d\n",(vht_cap.cap & (MESH_VHT_CAP_TXSTBC)),if_info->vht_capab.tx_stbc);
			return 1;
		}

		if (((if_info->vht_capab.rx_stbc) == VHT_CAP_RXSTBC) && !(vht_cap.cap & MESH_VHT_CAP_RXSTBC_MASK)) {
			VAL_PRINTF("Driver does not support configured VHT capability [RX_STBC]\n  Driver supported VHT capability [RX_STBC] : %d\n Meshap configured VHT capability [RX_STBC] : %d\n",(vht_cap.cap & (MESH_VHT_CAP_RXSTBC_MASK)),if_info->vht_capab.rx_stbc);
			return 1;
		}


		if (((if_info->vht_capab.su_beamformer_cap) == VHT_CAP_SU_BEAMFORMER_CAPABLE) && !(vht_cap.cap & MESH_VHT_CAP_SU_BEAMFORMER_CAPABLE)) {
			VAL_PRINTF("Driver does not support configured VHT capability [SU_BEAMFORMER_CAPABLE]\n  Driver supported VHT capability [SU_BEAMFORMER_CAPABLE] : %d\n Meshap configured VHT capability [SU_BEAMFORMER_CAPABLE] : %d\n",(vht_cap.cap & (MESH_VHT_CAP_SU_BEAMFORMER_CAPABLE)),if_info->vht_capab.su_beamformer_cap);
			return 1;
		}

		if (((if_info->vht_capab.su_beamformee_cap) == VHT_CAP_SU_BEAMFORMEE_CAPABLE) && !(vht_cap.cap & MESH_VHT_CAP_SU_BEAMFORMEE_CAPABLE)) {
			VAL_PRINTF("Driver does not support configured VHT capability [SU_BEAMFORMEE_CAPABLE]\n Driver supported VHT capability [SU_BEAMFORMEE_CAPABLE] : %d\n Meshap configured VHT capability [SU_BEAMFORMEE_CAPABLE] : %d\n",(vht_cap.cap & (MESH_VHT_CAP_SU_BEAMFORMEE_CAPABLE)),if_info->vht_capab.su_beamformee_cap);
			return 1;
		}

		if (((if_info->vht_capab.mu_beamformer_cap) == VHT_CAP_MU_BEAMFORMER_CAPABLE) && !(vht_cap.cap & MESH_VHT_CAP_MU_BEAMFORMER_CAPABLE)) {
			VAL_PRINTF("Driver does not support configured VHT capability [MU_BEAMFORMER_CAPABLE]\n Driver supported VHT capability [MU_BEAMFORMER_CAPABLE] : %d\n Meshap configured VHT capability [MU_BEAMFORMER_CAPABLE] : %d\n",(vht_cap.cap & (MESH_VHT_CAP_MU_BEAMFORMER_CAPABLE)),if_info->vht_capab.mu_beamformer_cap);
			return 1;
		}

		if (((if_info->vht_capab.mu_beamformee_cap) == VHT_CAP_MU_BEAMFORMEE_CAPABLE) && !(vht_cap.cap & MESH_VHT_CAP_MU_BEAMFORMEE_CAPABLE)) {
			VAL_PRINTF("Driver does not support configured VHT capability [MU_BEAMFORMEE_CAPABLE]\n Driver supported VHT capability [MU_BEAMFORMEE_CAPABLE] : %d\n Meshap configured VHT capability [MU_BEAMFORMEE_CAPABLE] : %d\n",(vht_cap.cap & (MESH_VHT_CAP_MU_BEAMFORMEE_CAPABLE)),if_info->vht_capab.mu_beamformee_cap);
			return 1;
		}

		if (((if_info->vht_capab.vht_txop_ps) == VHT_CAP_VHT_TXOP_PS) && !(vht_cap.cap & MESH_VHT_CAP_VHT_TXOP_PS)) {
			VAL_PRINTF("Driver does not support configured VHT capability [VHT_CAP_VHT_TXOP_PS]\n  Driver supported VHT capability [VHT_CAP_VHT_TXOP_PS] : %d\n Meshap configured VHT capability [VHT_CAP_VHT_TXOP_PS] : %d\n",(vht_cap.cap & (MESH_VHT_CAP_VHT_TXOP_PS)),if_info->vht_capab.vht_txop_ps);
			return 1;
		 }

		if (((if_info->vht_capab.htc_vht_cap) == VHT_CAP_HTC_VHT) && !(vht_cap.cap & MESH_VHT_CAP_HTC_VHT)) {
			VAL_PRINTF("Driver does not support configured VHT capability [VHT_CAP_HTC_VHT]\n Driver supported VHT capability [VHT_CAP_HTC_VHT] : %d\n Meshap configured VHT capability [VHT_CAP_HTC_VHT] : %d\n",(vht_cap.cap & (MESH_VHT_CAP_HTC_VHT)),if_info->vht_capab.htc_vht_cap);
			return 1;
		}

		if (((if_info->vht_capab.rx_ant_pattern_consistency) == VHT_CAP_RX_ANTENNA_PATTERN) && !(vht_cap.cap & MESH_VHT_CAP_RX_ANTENNA_PATTERN)) {
			VAL_PRINTF("Driver does not support configured VHT capability [RX_ANTENNA_PATTERN]\n  Driver supported VHT capability [RX_ANTENNA_PATTERN] : %d\n Meshap configured VHT capability [RX_ANTENNA_PATTERN] : %d\n",(vht_cap.cap & (MESH_VHT_CAP_RX_ANTENNA_PATTERN)),if_info->vht_capab.rx_ant_pattern_consistency);
			return 1;
		}

		if (((if_info->vht_capab.tx_ant_pattern_consistency) == VHT_CAP_TX_ANTENNA_PATTERN) && !(vht_cap.cap & MESH_VHT_CAP_TX_ANTENNA_PATTERN)) {
			VAL_PRINTF("Driver does not support configured VHT capability [TX_ANTENNA_PATTERN]\n  Driver supported VHT capability [TX_ANTENNA_PATTERN] : %d\n Meshap configured VHT capability [TX_ANTENNA_PATTERN] : %d\n",(vht_cap.cap & (MESH_VHT_CAP_TX_ANTENNA_PATTERN)),if_info->vht_capab.tx_ant_pattern_consistency);
			return 1;
		}

		if(if_info->phy_sub_type == AL_CONF_IF_PHY_SUB_TYPE_802_11_ANAC)
		{
		/*	if((if_info->dca == 0) && (if_info->vht_capab.vht_oper_bandwidth == 1))
			{
				if ((( if_info->wm_channel <= 42 ) && ( if_info->vht_capab.seg0_center_freq  != 42)) ||
						(( if_info->wm_channel > 42 ) && (if_info->wm_channel <= 58) && (if_info->vht_capab.seg0_center_freq != 58)) ||
						((if_info->wm_channel > 58) && (if_info->wm_channel <= 106) && (if_info->vht_capab.seg0_center_freq != 106)) ||
						(( if_info->wm_channel > 106 ) && (if_info->wm_channel <= 122)&& (if_info->vht_capab.seg0_center_freq != 122)) ||
						(( if_info->wm_channel > 122) && (if_info->wm_channel <= 138) && (if_info->vht_capab.seg0_center_freq != 138)) ||
						(( if_info->wm_channel > 138 ) && ( if_info->vht_capab.seg0_center_freq != 155 )))      {
					VAL_PRINTF("Driver does not support configured VHT capability [SEG0_CENTER_FREQ]\n");
					return 1;
				}
			}*/

			if((if_info->dca == 1) && (if_info->vht_capab.vht_oper_bandwidth == 1))
			{
				VAL_PRINTF(" we are currently not supporting Dca enabled validation in vht mode");
			}
		}


#if 0 //Need proper valdatons for sounding_dimensions and sts_count

		if ((if_info->vht_capab.sounding_dimensions)  > (vht_cap.cap & MESH_VHT_CAP_SOUNDING_DIMENSION_MAX)) {
			printf("Driver does not support configured VHT capability [SOUNDING_DIMENSIONS]\n");
			return 1;
		}

		if ((if_info->vht_capab.beamformee_sts_count)  > (vht_cap.cap & MESH_VHT_CAP_BEAMFORMEE_STS_MAX)) {
			printf("Driver does not support configured VHT capability [SOUNDING_DIMENSIONS]\n");
			return 1;
		}

#endif

	}
	return 0;
}

unsigned char validate_max_mpdu_len(tddi_vht_cap_t *vht_cap,al_vht_capab_t *vht_capab)
{
	switch(vht_cap->cap&MESH_VHT_CAP_MAX_MPDU_LENGTH_MASK)
	{
		case MESH_VHT_CAP_MAX_MPDU_LENGTH_3895  : if((vht_capab->max_mpdu_len <= 3895))
                                         return 0;
                                      else
                                         return 1;
		case MESH_VHT_CAP_MAX_MPDU_LENGTH_7991  : if((vht_capab->max_mpdu_len <= 7991))
													  return 0;
												  else
													  return 1;
		case MESH_VHT_CAP_MAX_MPDU_LENGTH_11454 : if((vht_capab->max_mpdu_len <= 11454))
													  return 0;
												  else
													  return 1;
		default                                 : return 1;
	}
}

void prepare_error_report(al_conf_if_info_t *if_info, unsigned char param_id, CONFIG_VAL_TYPE val_type, void *value, config_validation_report_t *validation_report)
{
	unsigned char value_len;

	/* If if_info is not NULL then config parameter is interface related */
	if (if_info) {
		validation_report->category_id = IF_CONFIG;
		strcpy(validation_report->if_name, if_info->name);
		VAL_PRINTF("validation_if_name : %s\n",validation_report->if_name);
		validation_report->if_name_len = strlen(if_info->name);
		VAL_PRINTF("param_id : %d\n",param_id);
		strcpy(validation_report->param_name, if_param_id_name_map_table[param_id].param_name);
		VAL_PRINTF("param_name : %s\n",validation_report->param_name);
		validation_report->param_name_len = strlen(if_param_id_name_map_table[param_id].param_name);
		VAL_PRINTF("param_name_len : %d\n",validation_report->param_name_len);
		if (val_type == INTEGER) {
			value_len = sprintf(validation_report->value, "%d", *((int *)value));
			VAL_PRINTF("value is : %s\n",validation_report->value);
		}
		else if (val_type == UNSIGNEDCHAR) {
			value_len = sprintf(validation_report->value, "%d", *((unsigned char *)value));
			VAL_PRINTF("value is : %s\n",validation_report->value);
		}
		else if(val_type == UNSIGNEDSHORT) {
			value_len = sprintf(validation_report->value, "%hd", *((unsigned short *)value));
			VAL_PRINTF("value is : %s\n",validation_report->value);
		}

		else {
			value_len = sprintf(validation_report->value, "%s", (char *)(value));
			VAL_PRINTF("value is : %s\n",validation_report->value);
		}
		validation_report->value_len = value_len;
		VAL_PRINTF("value_len : %d\n", validation_report->value_len);
	}
	else {
		validation_report->category_id = NODE_CONFIG;
		strcpy(validation_report->param_name, if_param_id_name_map_table[param_id].param_name);
		VAL_PRINTF("param_name : %s\n",validation_report->param_name);
		validation_report->param_name_len = strlen(if_param_id_name_map_table[param_id].param_name);
		printf("param_name_len : %d\n",validation_report->param_name_len);
		VAL_PRINTF("param_name_len : %d\n", validation_report->param_name_len);
		if (val_type == INTEGER) {
			value_len = sprintf(validation_report->value, "%d", *((int *)value));
			VAL_PRINTF("value is : %s\n", validation_report->value);
		}
		else if (val_type == UNSIGNEDCHAR) {
			value_len = sprintf(validation_report->value, "%d", *((unsigned char *)value));
			VAL_PRINTF("value is : %s\n",validation_report->value);
		}
		else if(val_type == UNSIGNEDSHORT) {
			value_len = sprintf(validation_report->value, "%hd", *((unsigned short *)value));
			VAL_PRINTF("value is : %s\n",validation_report->value);
		}
		else {
			value_len = sprintf(validation_report->value, "%s", (char *)(value));
			VAL_PRINTF("value is : %s\n",validation_report->value);
		}
		validation_report->value_len = value_len;
		VAL_PRINTF("value_len : %d\n", validation_report->value_len);
	}

}

unsigned char validate_ht_capabilities(tddi_ht_cap_t ht_cap, al_conf_if_info_t *if_info, config_validation_report_t *validation_report)
{

	int ret_val;
	int _20_ch[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 36, 40, 44, 48, 52, 56,60,64,149, 153, 157,161, 165};
	int _40plus_ch[] = {1, 2, 3, 4, 5, 6, 7, 36, 44, 52, 60, 149, 157};
	int _40minus_ch[] = {5, 6, 7, 8, 9, 10, 11, 12, 13, 40, 48, 56, 64, 153,161};
	int i, j;


	    VAL_PRINTF("Entering validate_ht_capabilities\n");
	    VAL_PRINTF("Interface name : %s\n",if_info->name);
		if(ht_cap.ht_supported == 0)
		{
			VAL_PRINTF("Driver doesnot support ht_capabilities\n");
			return UNSUPPORTED;
		}

		VAL_PRINTF("ht_cap value : %d\n",ht_cap.cap);

		if (((if_info->ht_capab.ldpc) == HT_CAP_INFO_LDPC_CAP) && !(ht_cap.cap & HT_CAP_INFO_LDPC_CODING_CAP)) {
			VAL_PRINTF("Driver does not support configured HT capability [LDPC]\n Driver supported HT capability [LDPC]:%d\n Meshap configured HT capability [LDPC]:%d\n",(ht_cap.cap & (HT_CAP_INFO_LDPC_CODING_CAP)),if_info->ht_capab.ldpc);
			 prepare_error_report(if_info,IF_HT_CAP_LDPC , UNSIGNEDCHAR, &if_info->ht_capab.ldpc, validation_report);
			return UNSUPPORTED;
		}

		if(TDDI_802_11_IS_AC(if_info->phy_sub_type) && (if_info->vht_capab.vht_oper_bandwidth != 1)) {

		if ((if_info->ht_capab.ht_bandwidth) > (ht_cap.cap & (HT_CAP_INFO_SUPP_CHANNEL_WIDTH_SET))) {
			VAL_PRINTF("Driver does not support configured HT capability [HT40*]\n Driver supported HT capability [HT40*] : %d\n  Meshap configured HT capability [HT40*] : %d\n",(ht_cap.cap & (HT_CAP_INFO_SUPP_CHANNEL_WIDTH_SET)),if_info->ht_capab.ht_bandwidth);
			prepare_error_report(if_info,IF_HT_CAP_CH_BANDWIDTH , UNSIGNEDCHAR, &if_info->ht_capab.ht_bandwidth, validation_report);
			return UNSUPPORTED;
		}
		}
		
		if(if_info->ht_capab.ht_bandwidth == AL_CONF_IF_HT_PARAM_20) {
		if (((if_info->ht_capab.gi_40 == HT_CAP_INFO_GI40MHZ_AUTO) || (if_info->ht_capab.gi_40 == HT_CAP_INFO_GI40MHZ_SHORT))) {
			VAL_PRINTF("Error occured in SGI at 40mhz as the ht_bandwidth is 20mhz supported\n Ht_bandwidth of interface %s is  : %d\n Meshap configured HT capability [SHORT-GI-40] : %d\n ",if_info->name, if_info->ht_capab.ht_bandwidth, if_info->ht_capab.gi_40);
			 prepare_error_report(if_info,IF_HT_CAP_GI_40 , UNSIGNEDCHAR, &if_info->ht_capab.gi_40, validation_report);
			return UNSUPPORTED;
		}
		}


	if(if_info->use_type != AL_CONF_IF_USE_TYPE_DS) {

				if((if_info->ht_capab.ht_bandwidth == AL_CONF_IF_HT_PARAM_20))
				{
					if(if_info->dca == 0)
					{
						for(i = 0; i < (sizeof(_20_ch) / sizeof(int)); i++)
						{
							if((if_info->wm_channel) != _20_ch[i])
								continue;

							if((if_info->wm_channel) == _20_ch[i])
								goto _label;
						}

						VAL_PRINTF("Driver does not support configured ht_bandwidth as the configured channel is not found in 20 channel list\n");
						prepare_error_report(if_info,IF_HT_CAP_CH_BANDWIDTH , UNSIGNEDCHAR, &if_info->wm_channel, validation_report);
						return UNSUPPORTED;

_label:
						VAL_PRINTF("The configured channel is found in 20 channel list\n");
					}
					else    {
						if(if_info->dca == 1)
						{
							for (i = 0; i < if_info->dca_list_count; i++)
							{
								for(j = 0; j < (sizeof(_20_ch) / sizeof(int)); j++)
								{
									if(if_info->dca_list[i] != _20_ch[j])
										continue;
									else
										goto _label1;
								}
								VAL_PRINTF("Driver does not support configured ht_bandwidth as the configured channel : %d is not found in 20 channel list\n", if_info->dca_list[i]);
								prepare_error_report(if_info,IF_HT_CAP_CH_BANDWIDTH , UNSIGNEDCHAR, &if_info->dca_list[i], validation_report);
								return UNSUPPORTED;

_label1:
								VAL_PRINTF("The configured channel : %d is found in 20 channel list\n",if_info->dca_list[i]);
							}
						}
					}
				}

				if((if_info->ht_capab.ht_bandwidth == AL_CONF_IF_HT_PARAM_40_ABOVE))
				{
					if(if_info->dca == 0)
					{
						for(i = 0; i < (sizeof(_40plus_ch) / sizeof(int)); i++)
						{
							if((if_info->wm_channel) != _40plus_ch[i])
								continue;

							if((if_info->wm_channel) == _40plus_ch[i])
								goto _label2;
						}
						VAL_PRINTF("Driver does not support configured ht_bandwidth as the configured channel is not found in 40+ channel list\n");
						prepare_error_report(if_info,IF_HT_CAP_CH_BANDWIDTH , UNSIGNEDCHAR, &if_info->wm_channel, validation_report);
						return 1;

_label2:
						VAL_PRINTF("The configured channel is found in 40+ channel list\n");
					}

					else    {
						if(if_info->dca == 1)
						{
							for (i = 0; i < if_info->dca_list_count; i++)
							{
								for(j = 0; j < (sizeof(_40plus_ch) / sizeof(int)); j++)
								{
									if(if_info->dca_list[i] != _40plus_ch[j])
										continue;
									else
										goto _label3;
								}
								VAL_PRINTF("Driver does not support configured ht_bandwidth as the configured channel : %d is not found in 40+ channel list\n", if_info->dca_list[i]);
								prepare_error_report(if_info,IF_HT_CAP_CH_BANDWIDTH , UNSIGNEDCHAR, &if_info->dca_list[i], validation_report);
								return UNSUPPORTED;

_label3:
								VAL_PRINTF("The configured channel : %d is found in 40+ channel list\n",if_info->dca_list[i]);
							}
						}
					}

				}
				if((if_info->ht_capab.ht_bandwidth == AL_CONF_IF_HT_PARAM_40_BELOW))
				{
					if(if_info->dca == 0)
					{
						for(i = 0; i < (sizeof(_40minus_ch) / sizeof(int)); i++)
						{
							if((if_info->wm_channel) != _40minus_ch[i])
								continue;

							if((if_info->wm_channel) == _40minus_ch[i])
								goto _label4;
						}
						VAL_PRINTF("Driver does not support configured ht_bandwidth as the configured channel is not found in 40- channel list\n\n");
						prepare_error_report(if_info,IF_HT_CAP_CH_BANDWIDTH , UNSIGNEDCHAR, &if_info->wm_channel, validation_report);
						return UNSUPPORTED;

_label4:
						VAL_PRINTF("The configured channel is found in 40- channel list\n");
					}
					else    {
						if(if_info->dca == 1)
						{
							for (i = 0; i < if_info->dca_list_count; i++)
							{
								for(j = 0; j < (sizeof(_40minus_ch) / sizeof(int)); j++)
								{
									if(if_info->dca_list[i] != _40minus_ch[j])
										continue;
									else
										goto _label5;
								}
								VAL_PRINTF("Driver does not support configured ht_bandwidth as the configured channel : %d is not found in 40- channel list\n", if_info->dca_list[i]);
								prepare_error_report(if_info,IF_HT_CAP_CH_BANDWIDTH , UNSIGNEDCHAR, &if_info->dca_list[i], validation_report);
								return UNSUPPORTED;

_label5:
								VAL_PRINTF("The configured channel : %d is found in 40- channel list\n",if_info->dca_list[i]);
							}
						}
					}

				}
			}


		ret_val = validate_smps(&ht_cap,&if_info->ht_capab);
		if(ret_val == 1) {
			VAL_PRINTF("Driver does not support configured HT capability [SMPS-*]\n Driver supported HT capability  [SMPS-*] : %d\n Meshap configured HT capability [SMPS-*] : %d\n",ht_cap.cap & HT_CAP_INFO_SMPS_MASK,if_info->ht_capab.smps);
			return UNSUPPORTED;
		}

		if ((if_info->ht_capab.gfmode == HT_CAP_INFO_GREEN_FIELD_CHECK) && !(ht_cap.cap & HT_CAP_INFO_GREEN_FIELD)) {
			VAL_PRINTF("Driver does not support configured HT capability [GF]\n Driver supported HT capability [GF] : %d\n Meshap configured HT capability [GF] : %d\n",(ht_cap.cap & (HT_CAP_INFO_GREEN_FIELD)),if_info->ht_capab.gfmode);
			 prepare_error_report(if_info,IF_HT_CAP_GF_MODE , UNSIGNEDCHAR, &if_info->ht_capab.gfmode, validation_report);
			return UNSUPPORTED;
		}

		if (((if_info->ht_capab.gi_20 == HT_CAP_INFO_GI20MHZ_AUTO) || (if_info->ht_capab.gi_20 == HT_CAP_INFO_GI20MHZ_SHORT)) && !(ht_cap.cap & HT_CAP_INFO_SHORT_GI20MHZ)) {
			VAL_PRINTF("Driver does not support configured HT capability [SHORT-GI-20]\n Driver supported HT capability [SHORT-GI-20] : %d\n  Meshap configured HT capability [SHORT-GI-20] : %d\n",(ht_cap.cap & (HT_CAP_INFO_SHORT_GI20MHZ)),if_info->ht_capab.gi_20);
			prepare_error_report(if_info,IF_HT_CAP_GI_20 , UNSIGNEDCHAR, &if_info->ht_capab.gi_20, validation_report);
			return UNSUPPORTED;
		}
		if (((if_info->ht_capab.gi_40 == HT_CAP_INFO_GI40MHZ_AUTO) || (if_info->ht_capab.gi_40 == HT_CAP_INFO_GI40MHZ_SHORT)) && !(ht_cap.cap & HT_CAP_INFO_SHORT_GI40MHZ)) {
			VAL_PRINTF("Driver does not support configured HT capability [SHORT-GI-40]\n Driver supported HT capability [SHORT-GI-40] : %d\n Meshap configured HT capability [SHORT-GI-40] : %d\n",(ht_cap.cap & (HT_CAP_INFO_SHORT_GI40MHZ)),if_info->ht_capab.gi_40);
			 prepare_error_report(if_info,IF_HT_CAP_GI_40 , UNSIGNEDCHAR, &if_info->ht_capab.gi_40, validation_report);
			return UNSUPPORTED;
		}

		if ((if_info->ht_capab.tx_stbc == HT_CAP_INFO_TX_STBC_CHECK) && !(ht_cap.cap & HT_CAP_INFO_TX_STBC)) {
			VAL_PRINTF("Driver does not support configured HT capability [TX-STBC]\n Driver supported HT capability [TX-STBC] : %d\n Meshap configured HT capability [TX-STBC] : %d\n",(ht_cap.cap & (HT_CAP_INFO_TX_STBC)),if_info->ht_capab.tx_stbc);
			 prepare_error_report(if_info,IF_HT_CAP_TX_STBC , UNSIGNEDCHAR, &if_info->ht_capab.tx_stbc, validation_report);
			return UNSUPPORTED;
		}

		if ((if_info->ht_capab.rx_stbc == HT_CAP_INFO_RX_STBC) && (if_info->ht_capab.rx_stbc > (ht_cap.cap & HT_CAP_INFO_RX_STBC_MASK))) {
			VAL_PRINTF("Driver does not support configured HT capability [RX-STBC*]\n Driver supported HT capability [RX-STBC] : %d\n Meshap configured HT capability [RX-STBC] : %d\n",(ht_cap.cap & (HT_CAP_INFO_RX_STBC_MASK)),if_info->ht_capab.rx_stbc);
			 prepare_error_report(if_info, IF_HT_CAP_RX_STBC , UNSIGNEDCHAR, &if_info->ht_capab.rx_stbc, validation_report);
			return UNSUPPORTED;
		}

		if ((if_info->ht_capab.delayed_ba == HT_CAP_INFO_DELAYED_BA_CHECK) && !(ht_cap.cap & HT_CAP_INFO_DELAYED_BA)) {
			VAL_PRINTF("Driver does not support configured HT capability [DELAYED-BA] Driver supported HT capability [DELAYED-BA] : %d\n Meshap configured HT capability [DELAYED-BA] : %d\n",(ht_cap.cap & (HT_CAP_INFO_DELAYED_BA)),if_info->ht_capab.delayed_ba);
			return UNSUPPORTED;
		}

		if ((if_info->ht_capab.max_amsdu_len == HT_CAP_INFO_MAX_AMSDU_SIZE_CHECK) && !(ht_cap.cap & HT_CAP_INFO_MAX_AMSDU_SIZE)) {
			VAL_PRINTF("Driver does not support configured HT capability [MAX-AMSDU-7935]\n Driver supported HT capability [MAX-AMSDU-7935] : %d\n Meshap configured HT capability [MAX-AMSDU-7935] : %d\n",(ht_cap.cap & (HT_CAP_INFO_MAX_AMSDU_SIZE)),if_info->ht_capab.max_amsdu_len);
			prepare_error_report(if_info, IF_HT_CAP_AMSDU , UNSIGNEDCHAR, &if_info->ht_capab.max_amsdu_len, validation_report);
			return UNSUPPORTED;
		}

		if ((if_info->ht_capab.dsss_cck_40 == HT_CAP_INFO_DSSS_CCK40MHZ_CHECK) && !(ht_cap.cap & HT_CAP_INFO_DSSS_CCK40MHZ)) {
			VAL_PRINTF("Driver does not support configured HT capability [DSSS_CCK-40]\n Driver supported HT capability [DSSS_CCK-40] : %d\n Meshap configured HT capability [DSSS_CCK-40] : %d\n",(ht_cap.cap & (HT_CAP_INFO_DSSS_CCK40MHZ)),if_info->ht_capab.dsss_cck_40);
			return UNSUPPORTED;
		}
		if ((if_info->ht_capab.lsig_txop == HT_CAP_INFO_LSIG_TXOP) && !(ht_cap.cap & HT_CAP_INFO_LSIG_TXOP_PROTECT_SUPPORT)) {
			VAL_PRINTF("Driver does not support configured HT capability [LSIG-TXOP-PROT]\n Driver supported HT capability [LSIG-TXOP-PROT] : %d\n Meshap configured HT capability [LSIG-TXOP-PROT] : %d\n",(ht_cap.cap & (HT_CAP_INFO_LSIG_TXOP_PROTECT_SUPPORT)),if_info->ht_capab.lsig_txop);
			return UNSUPPORTED;
		}

		ret_val = validate_frame_aggregation(&if_info->frame_aggregation);

		if(ret_val > ht_cap.ampdu_factor)
		{
			VAL_PRINTF("Driver does not support configured HT capability [AMPDU_FACTOR] Driver supported HT capability [AMPDU_FACTOR] : %d\n Meshap configured HT capability [AMPDU_FACTOR] : %d\n",ht_cap.ampdu_factor,if_info->frame_aggregation.max_ampdu_len);
			prepare_error_report(if_info, IF_HT_CAP_AMPDU, UNSIGNEDCHAR, &if_info->frame_aggregation.max_ampdu_len, validation_report);
			return UNSUPPORTED;
		}

	VAL_PRINTF("Exiting validate_ht_capabilities\n");


	return SUCCESS;
}

unsigned char validate_frame_aggregation( al_fram_agre_t *frame_aggregation)
{
	switch(frame_aggregation->max_ampdu_len)
	{
		case 8  :       return 0;

		case 16 :       return 1;

		case 32 :       return 2;

		case 64 :       return 3;

		default :       return 4;
	}
}

unsigned char validate_smps(tddi_ht_cap_t *ht_cap, al_ht_capab_t  *ht_capab)
{
	switch(ht_cap->cap & HT_CAP_INFO_SMPS_MASK)
	{
		case HT_CAP_INFO_SMPS_STATIC  : if((ht_capab->smps == 0) || (ht_capab->smps == 3))
											return 0;
										else    {
											printf("smps : %d \n",ht_capab->smps );
											return 1;
										}

		case HT_CAP_INFO_SMPS_DYNAMIC : if((ht_capab->smps == 1) || (ht_capab->smps == 0))
											return 0;
										else
											return 1;

		case HT_CAP_INFO_SMPS_DISABLED : if(ht_capab->smps == 3)
											 return 0;
										 else
											 return 1;
		default : return 1;
	}
}
#endif

int init_data()
{
   int                al_conf_handle;
   int                dot11e_conf_handle;
   int                mesh_key_length;
   int                ret;
   int                regdomain_code;
   unsigned char      buffer[1024];
   char               node_name[128];
   char               latitude[128];
   char               longitude[128];
   int                ret1;
   al_conf_gps_info_t gps_info;
   int                flag = 0;      /* This is set to 0 for bootup */
   _al_conf_data_t    *data;
	pthread_t sysupgrade_tid;

   /*
    *	malloc all data structures
    */
   imcp_reset_data            = (imcp_reset_data_t *)malloc(sizeof(imcp_reset_data_t));
   imcp_apconfig_data         = (imcp_apconfig_data_t *)malloc(sizeof(imcp_apconfig_data_t));
   imcp_apconfig_rr_data      = (imcp_apconfig_rr_data_t *)malloc(sizeof(imcp_apconfig_rr_data_t));
   imcp_ipconfig_data         = (imcp_ipconfig_data_t *)malloc(sizeof(imcp_ipconfig_data_t));
   imcp_ipconfig_rr_data      = (imcp_ipconfig_rr_data_t *)malloc(sizeof(imcp_ipconfig_rr_data_t));
   imcp_start_ip_if_data      = (imcp_start_ip_if_data_t *)malloc(sizeof(imcp_start_ip_if_data_t));
   imcp_sw_update_req_data    = (imcp_sw_update_req_data_t *)malloc(sizeof(imcp_sw_update_req_data_t));
   imcp_mesh_key_update_data  = (imcp_mesh_key_update_data_t *)malloc(sizeof(imcp_mesh_key_update_data_t));
   imcp_config_rr_data        = (imcp_config_rr_data_t *)malloc(sizeof(imcp_config_rr_data_t));
   imcp_vlan_config_info_data = (imcp_vlan_config_info_data_t *)malloc(sizeof(imcp_vlan_config_info_data_t));

   imcp_reg_domain_rr_data      = (imcp_reg_domain_rr_data_t *)malloc(sizeof(imcp_reg_domain_rr_data_t));
   imcp_reg_domain_config_data  = (imcp_reg_domain_config_data_t *)malloc(sizeof(imcp_reg_domain_config_data_t));
   imcp_ack_timeout_rr_data     = (imcp_ack_timeout_rr_data_t *)malloc(sizeof(imcp_ack_timeout_rr_data_t));
   imcp_ack_timeout_config_data = (imcp_ack_timeout_config_data_t *)malloc(sizeof(imcp_ack_timeout_config_data_t));

   imcp_hide_ssid_config_data = (imcp_hide_ssid_config_data_t *)malloc(sizeof(imcp_hide_ssid_config_data_t));
   imcp_dot11e_category_data  = (imcp_dot11e_category_t *)malloc(sizeof(imcp_dot11e_category_t));
   imcp_dot11e_if_config_data = (imcp_dot11e_if_config_t *)malloc(sizeof(imcp_dot11e_if_config_t));
   imcp_acl_config_data       = (imcp_acl_config_t *)malloc(sizeof(imcp_acl_config_t));

   imcp_priv_channel_data = (imcp_priv_channel_config_t *)malloc(sizeof(imcp_priv_channel_config_t));
   imcp_sip_config_data   = (imcp_sip_config_t *)malloc(sizeof(imcp_sip_config_t));

   memset(imcp_reset_data, 0, sizeof(imcp_reset_data_t));
   memset(imcp_apconfig_data, 0, sizeof(imcp_apconfig_data_t));
   memset(imcp_apconfig_rr_data, 0, sizeof(imcp_apconfig_rr_data_t));
   memset(imcp_ipconfig_data, 0, sizeof(imcp_ipconfig_data_t));
   memset(imcp_ipconfig_rr_data, 0, sizeof(imcp_ipconfig_rr_data_t));
   memset(imcp_start_ip_if_data, 0, sizeof(imcp_start_ip_if_data_t));
   memset(imcp_sw_update_req_data, 0, sizeof(imcp_sw_update_req_data_t));
   memset(imcp_mesh_key_update_data, 0, sizeof(imcp_mesh_key_update_data_t));
   memset(imcp_config_rr_data, 0, sizeof(imcp_config_rr_data_t));
   memset(imcp_vlan_config_info_data, 0, sizeof(imcp_vlan_config_info_data_t));

   memset(imcp_reg_domain_rr_data, 0, sizeof(imcp_reg_domain_rr_data_t));
   memset(imcp_reg_domain_config_data, 0, sizeof(imcp_reg_domain_config_data_t));
   memset(imcp_ack_timeout_rr_data, 0, sizeof(imcp_ack_timeout_rr_data_t));
   memset(imcp_ack_timeout_config_data, 0, sizeof(imcp_ack_timeout_config_data_t));

   memset(imcp_hide_ssid_config_data, 0, sizeof(imcp_hide_ssid_config_data_t));
   memset(imcp_dot11e_category_data, 0, sizeof(imcp_dot11e_category_t));
   memset(imcp_dot11e_if_config_data, 0, sizeof(imcp_dot11e_if_config_t));
   memset(imcp_acl_config_data, 0, sizeof(imcp_acl_config_t));

   memset(imcp_priv_channel_data, 0, sizeof(imcp_priv_channel_config_t));
   memset(imcp_sip_config_data, 0, sizeof(imcp_sip_config_t));

   imcp_sw_update_req_data->inf_file_data_head = NULL;

   memset(current_if_name, 0, 256);
   memset(mesh_id, 0, 256);
   memset(mesh_id_key, 0, 1024);
   memset(buffer, 0, 1024);

   strcpy(current_if_name, DEFAULT_IF_NAME);

   al_conf_handle = libalconf_read_config_data();
   printf("al_conf_handle\n");
   if (al_conf_handle == -1)
   {
      printf("\nCould not read configuration");
      return 0;
   }
#if 1 /*query for wm interface to start*/
   data = (_al_conf_data_t *)al_conf_handle;
   al_conf_if_info_t *if_info;
   int                i = 0, chan = 0, count = 0;

   while (chan == 0)
   {
       sleep(1);
       if_info = &data->if_info[i];
       if (if_info->use_type == AL_CONF_IF_USE_TYPE_WM)
       {
           chan = get_wm_channel_flag(if_info);
       }
       else
           i++;
   }
#endif
   dot11e_conf_handle = read_dot11e_category_data();

   if (dot11e_conf_handle == -1)
   {
      printf("\nCould not read dot11e configuration");
      return 0;
   }


   al_conf_get_mesh_id(AL_CONTEXT al_conf_handle, mesh_id, 256);
   al_conf_get_mesh_imcp_key_buffer(AL_CONTEXT al_conf_handle, buffer, 1024);

   mesh_id_length     = strlen((const char *)mesh_id);
   mesh_id_key_length = 0;

#ifdef _CONFIGD_USE_ENCRYPTION_
   mesh_id_key_length = al_decode_key(AL_CONTEXT buffer, _MAC_ADDRESS, mesh_id_key);
#else
   printf("\nCONFIGD : Starting without encryption");
#endif

   regdomain_code = al_conf_get_regulatory_domain(AL_CONTEXT al_conf_handle);

   if (regdomain_code & 0x0200)
   {
      printf("\nCONFIGD : Starting in FIPS 140-2 mode, stopping SNMP, MISCD, BOA web server daemon...");
      EXECUTE_COMMAND("mv /bin/boa /bin/oboa&&kill -9 `pidof boa`")
      EXECUTE_COMMAND("mv /sbin/meshsnmpd /sbin/omeshsnmpd&&kill -9 `pidof meshsnmpd`")
      EXECUTE_COMMAND("mv /sbin/miscd /sbin/omiscd&&kill -9 `pidof miscd`")
      EXECUTE_COMMAND("mv /usr/bin/iperf /sbin/oiperf&&kill -9 `pidof iperf`")
   }

   al_conf_get_gps_info(AL_CONTEXT al_conf_handle, &gps_info);
   al_conf_get_name(AL_CONTEXT al_conf_handle, node_name, 128);

   al_conf_get_gps_x_coordinate(AL_CONTEXT al_conf_handle, latitude, 128);
   al_conf_get_gps_y_coordinate(AL_CONTEXT al_conf_handle, longitude, 128);

   _write_gps_info(latitude, longitude);

   if (!read_from_ifconfig_file())
   {
      memcpy(network_broadcast_address, imcp_ipconfig_data->ip_address, 3);
      network_broadcast_address[3] = 255;
   }

   if (gps_info.gps_enabled)
   {
      char command[128];
      EXECUTE_COMMAND("chmod +x /root/start_gpsd.sh")

      sprintf(command,
              "/root/start_gpsd.sh -p %s -t %d.%d.%d.%d,%d,%s-%02X:%02X:%02X:%02X:%02X:%02X,%d",
              gps_info.gps_input_dev,
              gps_info.gps_push_dest_ip[0], gps_info.gps_push_dest_ip[1], gps_info.gps_push_dest_ip[2], gps_info.gps_push_dest_ip[3],
              gps_info.gps_push_port,
              node_name,
              _MAC_ADDRESS[0], _MAC_ADDRESS[1], _MAC_ADDRESS[2], _MAC_ADDRESS[3], _MAC_ADDRESS[4], _MAC_ADDRESS[5],
              gps_info.gps_push_interval);

      EXECUTE_COMMAND(command)
   }

   //  EXECUTE_MESHD_START


   init_configurations(al_conf_handle, dot11e_conf_handle, flag, 0);

   al_conf_close(AL_CONTEXT al_conf_handle);
   dot11e_conf_close(AL_CONTEXT dot11e_conf_handle);

   /*Create a thread to monitor eth1 link state*/
   mesh_ethernet_link_status_create_thread();

#ifdef MD1000
   mesh_ds_link_status_create_thread();
#endif
	INIT_LIST_HEAD(&imcp_sysupgrade_request);
	ret = pthread_create(&sysupgrade_tid, NULL, imcp_sw_update_request_helper_thread, NULL);
	if(ret)
	{
		perror("pthread_create(): Cannot create Fw upgrade thread");
	}
	else
		prev_tid = sysupgrade_tid;
}

static int reinit_configuration(int al_conf_handle, int dot11e_conf_handle, int flag, int channel)
{
    _al_conf_data_t   *data;
    int               acl_conf_handle;
    acl_conf_t        *acl_data;
    interface_map_t   interface_map;
    al_conf_if_info_t *if_info;
    int k, index = 0;
    int hostapd_mask = 0;
    dot11e_conf_category_t *dot11e_data;

    dot11e_data = (dot11e_conf_category_t *)dot11e_conf_handle;

    data = (_al_conf_data_t *)al_conf_handle;

    acl_conf_handle = read_acl_config_data();
    if (acl_conf_handle < 0)
    {
       al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : ACL read failed func: %s: %d\n",__func__,__LINE__);
       return 0;
    }
    acl_data = (acl_conf_t *) acl_conf_handle;
    system("rm /etc/accept_mac_*");
    system("rm /etc/deny_mac_*");

    interface_map.if_map = (_if_map *)malloc(sizeof(_if_map) * data->if_count);

    printf("%s %d\n", __func__, __LINE__);

    if (interface_map.if_map == NULL)
    {
       printf("error allocating memory\n");
       acl_conf_close(AL_CONTEXT acl_conf_handle);
       return 0;
    }
    interface_map.if_count = 0;
    for (k = 0; k < data->if_count; k++)
    {
        if_info = &data->if_info[k];

        if ((if_info->use_type == AL_CONF_IF_USE_TYPE_AP) && (if_info->phy_type == AL_CONF_IF_PHY_TYPE_802_11_VIRTUAL))
        {
            strncpy(interface_map.if_map[index].if_name, if_info->name, 32);

            interface_map.if_map[index].vector_ptr = &hostapd_ops;

            interface_map.if_map[index].use_type = if_info->use_type;

            sprintf(interface_map.if_map[index].file_name, HOSTAPD_NAME_FORMAT, if_info->name);

            if ((if_info->dca != 0) && (if_info->dca_list_count > 0)) {
                channel = if_info->dca_list[0];
                old_al_if_info[k].wm_channel = channel;
             }
            create_hostapd_config_file(data, acl_data, dot11e_data, if_info, interface_map, index, channel);

            index++;
            interface_map.if_count++;
        }
    }
    hostapd_mask = compare_AP_data(al_conf_handle);
    mesh_start_hostapd(interface_map, flag, &hostapd_mask, 0);

    acl_conf_close(AL_CONTEXT acl_conf_handle);
    free(interface_map.if_map);
    return 0;
}

unsigned int mesh_get_wm_iface_channel(_al_conf_data_t *data, al_conf_if_info_t *if_info, int chan)
{
    int k, flag = 0;
    al_conf_if_info_t *if_info_wlan0;
    for (k = 0; k < data->if_count; k++)
    {
        if_info_wlan0 = &data->if_info[k];
        if (if_info && if_info_wlan0->use_type == AL_CONF_IF_USE_TYPE_WM && if_info->phy_type == AL_CONF_IF_PHY_TYPE_802_11_VIRTUAL)
        {
            flag = 1;
            break;
        }
    }
    if (!flag)
    {
        return if_info->dca_list[0];
    }
    return chan = get_wm_channel_flag(if_info_wlan0);
}

unsigned int mesh_get_ds_iface_channel(_al_conf_data_t *data)
{
   FILE *fp;
   char buffer[128];
   int chan = 0, k;
   al_conf_if_info_t *if_info;
   char found = 0;
   char associated = 0;
   for (k = 0; k < data->if_count; k++)
   {
       if_info = &data->if_info[k];
       if (if_info && (if_info->use_type == AL_CONF_IF_USE_TYPE_DS) &&
           ((if_info->phy_type == AL_CONF_IF_PHY_TYPE_802_11_VIRTUAL) ||
           (if_info->phy_type == AL_CONF_IF_PHY_TYPE_802_11)))
       {
           found = 1;
           break;
       }
   }
   if (found)
   {
      while (!associated)
      {
          memset(buffer, 0, sizeof(buffer));
          sprintf(buffer, "iwconfig %s | grep Access > /etc/output.txt", if_info->name);
          system(buffer);
          fp = fopen("/etc/output.txt", "r+");
          if (NULL == fp)
          {
             printf("Failed to open file errno: %d\n", errno);
             return 0;
          }
          while (!feof(fp))
          {
              memset(buffer, 0, sizeof(buffer));
              fscanf(fp, "%s", buffer);
              if (strcmp(buffer, "Access"))
                 continue;
              memset(buffer, 0, sizeof(buffer));
              fscanf(fp, "%s", buffer);
              memset(buffer, 0, sizeof(buffer));
              fscanf(fp, "%s", buffer);
              if (strcmp(buffer, "Not-Associated"))
              {
                 associated = 1;
                 break;
              }
          }
          fclose(fp);
          unlink("/etc/output.txt");
      }
      memset(buffer, 0, sizeof(buffer));
      sprintf(buffer, "iw dev %s info | grep channel > /etc/output.txt", if_info->name);
      system(buffer);
      fp = fopen("/etc/output.txt", "r+");
      if (NULL == fp)
      {
         printf("Failed to open file errno: %d\n", errno);
         return 0;
      }
      while (!feof(fp))
      {
         fscanf(fp, "%s", buffer);
         if (!strcmp(buffer, "channel"))
         {
            memset(buffer, 0, sizeof(buffer));
            fscanf(fp, "%s", buffer);
            chan = atoi(buffer);
            break;
         }
      }
      fclose(fp);
      unlink("/etc/output.txt");
   }
   return chan;
}

int init_virtual_configurations(int al_conf_handle)
{
   int           use_virt_if;
   int           i, ret;
   unsigned char buffer[512];
   int           fd;
   unsigned char mac[6];
   struct ifreq  ifr;

   fd = socket(AF_INET, SOCK_DGRAM, 0);

   ifr.ifr_addr.sa_family = AF_INET;

   _al_conf_data_t *data;
   data = (_al_conf_data_t *)al_conf_handle;
   al_conf_if_info_t *if_info;

   use_virt_if = al_conf_get_use_virt_if(AL_CONTEXT al_conf_handle);
   printf("use_virt_if = %p", use_virt_if);

   if (use_virt_if)
   {
      for (i = 0; i < data->if_count; i++)
      {
         if_info = &data->if_info[i];

         if ((if_info->use_type == AL_CONF_IF_USE_TYPE_DS) && (if_info->phy_type == AL_CONF_IF_PHY_TYPE_802_11_VIRTUAL))
         {
            printf("executing iw commands\n ");
            sprintf(buffer, "iw dev wlan0 interface add %s type station", if_info->name);
            system(buffer);

            strncpy(ifr.ifr_name, if_info->name, IFNAMSIZ - 1);
            ret = ioctl(fd, SIOCGIFHWADDR, &ifr);
            if (ret == -1)
            {
               printf("error while getting mac addr\n");
            }

            memcpy(mac, ifr.ifr_hwaddr.sa_data, 6);

            mac[5] = mac[5] + 1;

            memcpy(ifr.ifr_hwaddr.sa_data, mac, 6);
            ret = ioctl(fd, SIOCSIFHWADDR, &ifr);
            if (ret == -1)
            {
               printf("error while setting mac addr\n");
            }


            close(fd);
         }
         else if (((if_info->use_type == AL_CONF_IF_USE_TYPE_AP) || (if_info->use_type == AL_CONF_IF_USE_TYPE_WM)) &&
                  (if_info->phy_type == AL_CONF_IF_PHY_TYPE_802_11_VIRTUAL))
         {
            sprintf(buffer, "iw dev wlan0 interface add %s type __ap", if_info->name);
            system(buffer);
         }
      }
   }
   else
   {
      printf("Ignoring virtual init normal configuration received\n");
   }

   return NULL;
}

#ifdef MD1000
int mesh_is_ds_iface_connected(_al_conf_data_t *data)
{
    FILE *fp;
    char buffer[128];
    int chan = 0, k;
    al_conf_if_info_t *if_info = NULL;
    char found = 0;
    int associated = 0;
    for (k = 0; k < data->if_count; k++)
    {
        if (data)
        {
            if_info = &data->if_info[k];
        }
        else {
            break;
        }
        if (if_info && (if_info->use_type == AL_CONF_IF_USE_TYPE_DS) &&
                ((if_info->phy_type == AL_CONF_IF_PHY_TYPE_802_11_VIRTUAL) ||
                 (if_info->phy_type == AL_CONF_IF_PHY_TYPE_802_11)))
        {
            found = 1;
            break;
        }
    }
	 if (found)
    {
        memset(buffer, 0, sizeof(buffer));
        sprintf(buffer, "iwconfig %s | grep Access > /etc/output.txt", if_info->name);
        system(buffer);
        fp = fopen("/etc/output.txt", "r+");
        if (NULL == fp)
        {
           printf("Failed to open file errno: %d\n", errno);
           return 0;
        }
    }
    while (!feof(fp))
    {
        memset(buffer, 0, sizeof(buffer));
        fscanf(fp, "%s", buffer);
        if (strcmp(buffer, "Access"))
            continue;
        memset(buffer, 0, sizeof(buffer));
        fscanf(fp, "%s", buffer);
        memset(buffer, 0, sizeof(buffer));
        fscanf(fp, "%s", buffer);
        if (strcmp(buffer, "Not-Associated"))
        {
            associated = 1;
            break;
        }
    }
    fclose(fp);
    unlink("/etc/output.txt");

    return associated;
}

void mesh_get_ds_interface_status(_al_conf_data_t *data,
                                  struct connect_status *status)
{
   al_conf_if_info_t *if_info = NULL;
   int k;
   static int prev_assoc = 0;
   static int channel = 0;

   if (!prev_assoc && (0 != (channel = mesh_get_ds_iface_channel(data))))
   {
      prev_assoc = 1;
   }
   else if (prev_assoc && !mesh_is_ds_iface_connected(data))
   {
      prev_assoc = 0;
      channel = 0;
   }

   status->prev_assoc = prev_assoc;
   status->channel = channel;

   return;
}

static void *mesh_test_ds_link_status(void *arg)
{
   _al_conf_data_t *data;
   al_conf_if_info_t *if_info = NULL;
   struct connect_status status;
   int al_conf_handle;
   int dot11e_conf_handle;
   int use_virt_if, k;
   char found = 0, buf[32];
   char hostapd_killed = 0;

   al_conf_handle       = libalconf_read_config_data();
   data                 = (_al_conf_data_t *)al_conf_handle;

   dot11e_conf_handle = read_dot11e_category_data();
   use_virt_if = al_conf_get_use_virt_if(AL_CONTEXT al_conf_handle);
   if (!use_virt_if)
   {
       al_conf_close(AL_CONTEXT al_conf_handle);
       dot11e_conf_close(AL_CONTEXT dot11e_conf_handle);
       return;
   }
   if (use_virt_if)
   {
      for (k = 0; k < data->if_count; k++)
      {
          if_info = &data->if_info[k];
          if (if_info && (if_info->phy_type == AL_CONF_IF_PHY_TYPE_802_11_VIRTUAL) && (if_info->use_type == AL_CONF_IF_USE_TYPE_AP))
          {
              found = 1;
              break;
          }
      }
   }
   if (!found)
   {
      al_conf_close(AL_CONTEXT al_conf_handle);
      dot11e_conf_close(AL_CONTEXT dot11e_conf_handle);
      return;
   }
   while (1) {
         mesh_get_ds_interface_status(data, &status);
         if (!status.prev_assoc)
         {
            sprintf(buf, "ifconfig %s down", if_info->name);
            system(buf);
            memset(buf, 0, sizeof(buf));
            sprintf(buf, "killall hostapd");
            system(buf);
            hostapd_killed = 1;
         }
         else if (hostapd_killed && status.channel)
         {
            reinit_configuration(al_conf_handle, dot11e_conf_handle, 0, status.channel);
            hostapd_killed = 0;
         }
         sleep(15);
   }
   al_conf_close(AL_CONTEXT al_conf_handle);
   dot11e_conf_close(AL_CONTEXT dot11e_conf_handle);
   pthread_detach(pthread_self());
   pthread_exit(NULL);
   return;
}
#endif
static void *mesh_test_ethernet_link_status()
{
   unsigned char   *server_ip; //="192.168.0.101";
   unsigned char   buffer[256]     = { '\0' };
   unsigned char   status_buff[32] = { '\0' };
   _al_conf_data_t *data;
   int             link_status     = 0; /*1 signifying servers is pingable; 0 means server not pingable*/
   static int      prev_link_state = 2;
//SPAWAR
   al_conf_if_info_t *if_info;
   int k, ack_timeout = 2;
   int al_conf_handle;

   al_conf_handle       = libalconf_read_config_data();
   data                 = (_al_conf_data_t *)al_conf_handle;

   for (k = 0; k < data->if_count; k++)
   {
      if_info = &data->if_info[k];
      if (!strcmp(if_info->name, "eth1")) {
          ack_timeout = if_info->ack_timeout;
          break;
      }
   }
//SPAWAR_END
   server_ip = data->server_ip_addr;
   printf("printing serverip\n");

   if (NULL == data->server_ip_addr)
   {
      printf("server_ip_addr is NULL ####\n");
   }
   printf("serverip = %s\n", data->server_ip_addr);

   memset(buffer, 0, sizeof(buffer));
//SPAWAR
   sprintf(buffer, "ping -c2 -W%d %s > /dev/null", ack_timeout, server_ip);
//SPAWAR_END

   /* Sleep for 120 sec on boot up to let kernel decide on the mode
    * based on physical connectivity. After boot up, ping will be used
    * determine the behavior.
    */
   sleep(120);

   while (1)
   {
      printf("Thread is running \n");
      printf("PING Command: %s pid <%d>\n", buffer, getpid());
      if (system(buffer))
      {
         link_status = 0;
         printf("Failed to ping server %s\n", server_ip);
      }
      else
      {
         link_status = 1;
         printf("ping successfull\n");
      }

#if 1
      /*Send link status to meshap*/
      if (prev_link_state != link_status)
      {
         memset(status_buff, 0, sizeof(status_buff));
         sprintf(status_buff, "meshd eth_link_status %d", link_status);
         printf("Status buff = %s\n", status_buff);
         system(status_buff);
         prev_link_state = link_status;
      }
#endif
      sleep(2);
   }

   al_conf_close(AL_CONTEXT al_conf_handle);
   pthread_detach(pthread_self());
   pthread_exit(NULL);
}
//SPAWAR
static void mesh_query_board_stats()
{
   char buffer[256];

   memset(buffer, 0, sizeof(buffer));
   sprintf(buffer, "meshd query_board_stats");
   printf("buffer: = %s\n", buffer);
   system(buffer);

   return;
}
//SPAWAR_END

/* This function creates a thread to monitor the ethernet downling (eth1) connection state.
 * It pings to the server_ip_addr parameter of the meshap.conf file and informs meshap about the success/failure of ping*/
static void mesh_ethernet_link_status_create_thread()
{
   pthread_t thread;

   pthread_create(&thread, NULL, mesh_test_ethernet_link_status, NULL);
//	pthread_join(&thread);
}

#ifdef MD1000
static void mesh_ds_link_status_create_thread()
{
   pthread_t thread;

   pthread_create(&thread, NULL, mesh_test_ds_link_status, NULL);
}
#endif

#define WLAN2  (int)BIT(0)
#define VWLAN0 (int)BIT(1)

const uint32_t crc32_tab[] = {
	0x00000000, 0x77073096, 0xee0e612c, 0x990951ba, 0x076dc419, 0x706af48f,
	0xe963a535, 0x9e6495a3,	0x0edb8832, 0x79dcb8a4, 0xe0d5e91e, 0x97d2d988,
	0x09b64c2b, 0x7eb17cbd, 0xe7b82d07, 0x90bf1d91, 0x1db71064, 0x6ab020f2,
	0xf3b97148, 0x84be41de,	0x1adad47d, 0x6ddde4eb, 0xf4d4b551, 0x83d385c7,
	0x136c9856, 0x646ba8c0, 0xfd62f97a, 0x8a65c9ec,	0x14015c4f, 0x63066cd9,
	0xfa0f3d63, 0x8d080df5,	0x3b6e20c8, 0x4c69105e, 0xd56041e4, 0xa2677172,
	0x3c03e4d1, 0x4b04d447, 0xd20d85fd, 0xa50ab56b,	0x35b5a8fa, 0x42b2986c,
	0xdbbbc9d6, 0xacbcf940,	0x32d86ce3, 0x45df5c75, 0xdcd60dcf, 0xabd13d59,
	0x26d930ac, 0x51de003a, 0xc8d75180, 0xbfd06116, 0x21b4f4b5, 0x56b3c423,
	0xcfba9599, 0xb8bda50f, 0x2802b89e, 0x5f058808, 0xc60cd9b2, 0xb10be924,
	0x2f6f7c87, 0x58684c11, 0xc1611dab, 0xb6662d3d,	0x76dc4190, 0x01db7106,
	0x98d220bc, 0xefd5102a, 0x71b18589, 0x06b6b51f, 0x9fbfe4a5, 0xe8b8d433,
	0x7807c9a2, 0x0f00f934, 0x9609a88e, 0xe10e9818, 0x7f6a0dbb, 0x086d3d2d,
	0x91646c97, 0xe6635c01, 0x6b6b51f4, 0x1c6c6162, 0x856530d8, 0xf262004e,
	0x6c0695ed, 0x1b01a57b, 0x8208f4c1, 0xf50fc457, 0x65b0d9c6, 0x12b7e950,
	0x8bbeb8ea, 0xfcb9887c, 0x62dd1ddf, 0x15da2d49, 0x8cd37cf3, 0xfbd44c65,
	0x4db26158, 0x3ab551ce, 0xa3bc0074, 0xd4bb30e2, 0x4adfa541, 0x3dd895d7,
	0xa4d1c46d, 0xd3d6f4fb, 0x4369e96a, 0x346ed9fc, 0xad678846, 0xda60b8d0,
	0x44042d73, 0x33031de5, 0xaa0a4c5f, 0xdd0d7cc9, 0x5005713c, 0x270241aa,
	0xbe0b1010, 0xc90c2086, 0x5768b525, 0x206f85b3, 0xb966d409, 0xce61e49f,
	0x5edef90e, 0x29d9c998, 0xb0d09822, 0xc7d7a8b4, 0x59b33d17, 0x2eb40d81,
	0xb7bd5c3b, 0xc0ba6cad, 0xedb88320, 0x9abfb3b6, 0x03b6e20c, 0x74b1d29a,
	0xead54739, 0x9dd277af, 0x04db2615, 0x73dc1683, 0xe3630b12, 0x94643b84,
	0x0d6d6a3e, 0x7a6a5aa8, 0xe40ecf0b, 0x9309ff9d, 0x0a00ae27, 0x7d079eb1,
	0xf00f9344, 0x8708a3d2, 0x1e01f268, 0x6906c2fe, 0xf762575d, 0x806567cb,
	0x196c3671, 0x6e6b06e7, 0xfed41b76, 0x89d32be0, 0x10da7a5a, 0x67dd4acc,
	0xf9b9df6f, 0x8ebeeff9, 0x17b7be43, 0x60b08ed5, 0xd6d6a3e8, 0xa1d1937e,
	0x38d8c2c4, 0x4fdff252, 0xd1bb67f1, 0xa6bc5767, 0x3fb506dd, 0x48b2364b,
	0xd80d2bda, 0xaf0a1b4c, 0x36034af6, 0x41047a60, 0xdf60efc3, 0xa867df55,
	0x316e8eef, 0x4669be79, 0xcb61b38c, 0xbc66831a, 0x256fd2a0, 0x5268e236,
	0xcc0c7795, 0xbb0b4703, 0x220216b9, 0x5505262f, 0xc5ba3bbe, 0xb2bd0b28,
	0x2bb45a92, 0x5cb36a04, 0xc2d7ffa7, 0xb5d0cf31, 0x2cd99e8b, 0x5bdeae1d,
	0x9b64c2b0, 0xec63f226, 0x756aa39c, 0x026d930a, 0x9c0906a9, 0xeb0e363f,
	0x72076785, 0x05005713, 0x95bf4a82, 0xe2b87a14, 0x7bb12bae, 0x0cb61b38,
	0x92d28e9b, 0xe5d5be0d, 0x7cdcefb7, 0x0bdbdf21, 0x86d3d2d4, 0xf1d4e242,
	0x68ddb3f8, 0x1fda836e, 0x81be16cd, 0xf6b9265b, 0x6fb077e1, 0x18b74777,
	0x88085ae6, 0xff0f6a70, 0x66063bca, 0x11010b5c, 0x8f659eff, 0xf862ae69,
	0x616bffd3, 0x166ccf45, 0xa00ae278, 0xd70dd2ee, 0x4e048354, 0x3903b3c2,
	0xa7672661, 0xd06016f7, 0x4969474d, 0x3e6e77db, 0xaed16a4a, 0xd9d65adc,
	0x40df0b66, 0x37d83bf0, 0xa9bcae53, 0xdebb9ec5, 0x47b2cf7f, 0x30b5ffe9,
	0xbdbdf21c, 0xcabac28a, 0x53b39330, 0x24b4a3a6, 0xbad03605, 0xcdd70693,
	0x54de5729, 0x23d967bf, 0xb3667a2e, 0xc4614ab8, 0x5d681b02, 0x2a6f2b94,
	0xb40bbe37, 0xc30c8ea1, 0x5a05df1b, 0x2d02ef8d
};

uint32_t crc32(const void *buf, size_t size)
{
  const uint8_t *p = buf;
  uint32_t crc;
  crc = ~0U;

  while (size--)
    crc = crc32_tab[(crc ^ *p++) & 0xFF] ^ (crc >> 8);
  return crc ^ ~0U;
}

/*Comparing the AP data with the help of crc16*/
int compare_AP_data(int al_conf_handle)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW: Enter %s: %d\n",__func__,__LINE__);

   int i = 0, al_if_count = 0,hostapd_mask = 0;
   uint32_t old_crc = 0, new_crc = 0;

   al_if_count = al_conf_get_if_count(al_conf_handle);

   memset(new_al_if_info, 0, sizeof(al_conf_if_info_t) * CURRENT_EXPECTED_MAX_IF_COUNT);
   for (i = 0; i < al_if_count; i++)
   {
      al_conf_get_if(al_conf_handle, i, &new_al_if_info[i]);
      /*checking for only AP interfaces*/
      if (!strcmp(old_al_if_info[i].name, new_al_if_info[i].name) && (old_al_if_info[i].use_type == AL_CONF_IF_USE_TYPE_AP))
      {
        old_crc = crc32((unsigned char *)&old_al_if_info[i], sizeof(al_conf_if_info_t));
        new_crc = crc32((unsigned char *)&new_al_if_info[i], sizeof(al_conf_if_info_t));

        printf("CONFIGD : new_checksum = %u old_checksum %u %s:%d\n", new_crc, old_crc,__func__, __LINE__);
        memcpy(&old_al_if_info[i], &new_al_if_info[i], sizeof(al_conf_if_info_t));

        if (old_crc != new_crc) {
           if (!strcmp(old_al_if_info[i].name, "wlan2")) {
                hostapd_mask |= WLAN2;
           }
           else if (!strcmp(old_al_if_info[i].name, "v_wlan0")) {
                hostapd_mask |= VWLAN0;
           }
        }
      }
    }
    al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW: Exit %s: %d\n",__func__,__LINE__);
    return hostapd_mask;
}

int init_configurations(int al_conf_handle, int dot11e_conf_handle, int flag, int start_hostapd)
{
   printf("%s %d\n", __func__, __LINE__);
   int               chan = 0, k, index = 0, channel = 0;
   _al_conf_data_t   *data;
   int               acl_conf_handle;
   acl_conf_t        *acl_data;
   interface_map_t   interface_map;
   al_conf_if_info_t *if_info;
   int hostapd_mask = 0;

   dot11e_conf_category_t *dot11e_data;

   dot11e_data = (dot11e_conf_category_t *)dot11e_conf_handle;

   data = (_al_conf_data_t *)al_conf_handle;

   acl_conf_handle = read_acl_config_data();
   if (acl_conf_handle < 0)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : ACL read failed func: %s: %d\n",__func__,__LINE__);
      return 0;
   }
   acl_data = (acl_conf_t *) acl_conf_handle;
   system("rm /etc/accept_mac_*");
   system("rm /etc/deny_mac_*");

   interface_map.if_map = (_if_map *)malloc(sizeof(_if_map) * data->if_count);

   printf("%s %d\n", __func__, __LINE__);

   if (interface_map.if_map == NULL)
   {
      printf("error allocating memory\n");
      acl_conf_close(AL_CONTEXT acl_conf_handle);
      return 0;
   }

   interface_map.if_count = 0;
   for (k = 0; k < data->if_count; k++)
   {
      if_info = &data->if_info[k];

      if (((if_info->use_type == AL_CONF_IF_USE_TYPE_WM) || (if_info->use_type == AL_CONF_IF_USE_TYPE_AP)) &&
          ((if_info->phy_type == AL_CONF_IF_PHY_TYPE_802_11_VIRTUAL) || (if_info->phy_type == AL_CONF_IF_PHY_TYPE_802_11)))
      {
         iw_meshap_set_txpower_helper(if_info);
      }

      if ((if_info->use_type == AL_CONF_IF_USE_TYPE_AP) && ((if_info->phy_type == AL_CONF_IF_PHY_TYPE_802_11_VIRTUAL) ||
          (if_info->phy_type == AL_CONF_IF_PHY_TYPE_802_11)))
      {
         strncpy(interface_map.if_map[index].if_name, if_info->name, 32);

         interface_map.if_map[index].vector_ptr = &hostapd_ops;

         interface_map.if_map[index].use_type = if_info->use_type;

         sprintf(interface_map.if_map[index].file_name, HOSTAPD_NAME_FORMAT, if_info->name);
         /* In case of FFR eth0 will be DS interface, hence start AP interface on
          * channel specified in meshap.conf
          */
         if((if_info->dca != 0) && (if_info->dca_list_count > 0))
         {
             channel = mesh_get_wm_iface_channel(data, if_info, chan);
             old_al_if_info[k].wm_channel = channel;
             printf("%s %d      ---- %d ----\n", __func__, __LINE__, channel);
         }
         else
         {
             channel = if_info->wm_channel;
             old_al_if_info[k].wm_channel = channel;
         }

#ifdef MD1000
         if (!channel && (if_info->phy_type == AL_CONF_IF_PHY_TYPE_802_11_VIRTUAL))
         {
            sleep(10);
            channel = mesh_get_ds_iface_channel(data);
            old_al_if_info[k].wm_channel = channel;
         }
#endif
         create_hostapd_config_file(data, acl_data, dot11e_data, if_info, interface_map, index, channel);

         index++;
         interface_map.if_count++;
         channel = 0;
      }

      if (((if_info->use_type == AL_CONF_IF_USE_TYPE_DS) || (if_info->use_type == AL_CONF_IF_USE_TYPE_PMON)) &&
          ((if_info->phy_type == AL_CONF_IF_PHY_TYPE_802_11_VIRTUAL) || (if_info->phy_type == AL_CONF_IF_PHY_TYPE_802_11)))
      {
         strncpy(interface_map.if_map[index].if_name, if_info->name, 32);

         interface_map.if_map[index].vector_ptr = &iwconfig_ops;

         interface_map.if_map[index].use_type = if_info->use_type;

         sprintf(interface_map.if_map[index].file_name, IW_NAME_FORMAT, if_info->name);

         create_iw_config_file(data, if_info, interface_map, index);
         index++;
         interface_map.if_count++;
      }
   }

   if (flag)
   {
      EXECUTE_MESHD_CONFIGURE
      hostapd_mask = compare_AP_data(al_conf_handle);
   }
   else
   {
      EXECUTE_MESHD_START
   }

   mesh_start_hostapd(interface_map, flag, &hostapd_mask, start_hostapd);

   acl_conf_close(AL_CONTEXT acl_conf_handle);
   return 0;
}


int un_init_data()
{
   /*
    *	free all datastructures used
    */
   free(imcp_reset_data);
   free(imcp_apconfig_data);
   free(imcp_apconfig_rr_data);
   free(imcp_ipconfig_data);
   free(imcp_ipconfig_rr_data);
   free(imcp_start_ip_if_data);
   free(imcp_sw_update_req_data);
   free(imcp_mesh_key_update_data);
   free(imcp_config_rr_data);
   /*todo free all internal allocated variables in security info and vlan info */
   free(imcp_vlan_config_info_data);

   free(imcp_reg_domain_rr_data);
   free(imcp_reg_domain_config_data);
   free(imcp_ack_timeout_rr_data);
   free(imcp_ack_timeout_config_data);

   free(imcp_hide_ssid_config_data);

   free(imcp_dot11e_category_data);

   free(imcp_dot11e_if_config_data);

   free(imcp_acl_config_data);
   free(imcp_priv_channel_data);
   free(imcp_sip_config_data);

   /**
    * free  list of installation file
    */
}

//SPAWAR
void set_ath_debug_level(enum drv_type type)
{
   int iface, if_count = 0, total_if_count = 0;
   char cmd[256];
   int conf_handle;
   al_conf_if_info_t if_info;

   conf_handle = libalconf_read_config_data();
   if (conf_handle == -1)
   {
      printf("\nCould not read configuration");
      return;
   }

   total_if_count = al_conf_get_if_count(AL_CONTEXT conf_handle);
   for (iface = 0; iface < total_if_count; iface++)
   {
       al_conf_get_if(AL_CONTEXT conf_handle, iface, &if_info);
       if (!strncmp(if_info.name, "wlan", 4))
          if_count++;
   }
   for (iface = 0; iface < if_count; iface++)
   {
       memset(cmd, 0, sizeof(cmd));
       sprintf(cmd, "%s%d%s%d%s", "echo 0 > /sys/kernel/debug/ieee80211/phy",
              iface, "/ath", type, "k/debug");
       printf("cmd: %s\n", cmd);
       system(cmd);
   }
   return;
}
//SPAWAR_END

int send_to_mgmt_gw(void *data, int length)
{
#define MGMT_GW_PORT 57055
    struct sockaddr_in  servaddr;
    int sockfd, len;
    char on = 1;

    if (-1 == (sockfd = socket(AF_INET, SOCK_DGRAM, 0))) {
      printf("\nCONFIGD:\tError opening socket for sending mgmt packet\n");
      return -1;
    }
    if (0 < setsockopt(sockfd, SOL_SOCKET,  SO_REUSEADDR,(char *)&on,
    sizeof(on))) {
        printf("\nCONFIGD:\tError setting SO_REUSEADDR\n");
        perror("setsockopt() failed");
        close(sockfd);
        return -1;
    }

    servaddr.sin_family = AF_INET;
    servaddr.sin_addr.s_addr =  inet_addr(DEVICE_LOCAL_HOST);//_MIP_IP;
    servaddr.sin_port=htons(MGMT_GW_PORT);
    printf("%s: sending %d length data to mgmt gw port %d : \n", __FUNCTION__, length, MGMT_GW_PORT);
    if (-1 == (len = sendto(sockfd, data, length, 0,
        (const struct sockaddr *)&servaddr,sizeof(servaddr)))) {
      printf("\nCONFIGD:\tError while sending mgmt packet.\n");
      perror("sendto");
   }
    close(sockfd);
    return len;
}
/*Duty cycle wm flag*/
int get_wm_channel_flag(al_conf_if_info_t *if_info)
{
    int                                 fd, ret;
    tddi_packet_t                      *packet;
    tddi_get_duty_cycle_flag_request_t *request;
    unsigned char                      *request_packet;
    tddi_packet_t                      *response_packet;
    tddi_get_duty_cycle_flag_t         *response_data;
    unsigned char                       response_buffer[sizeof(tddi_packet_t) + sizeof(tddi_get_duty_cycle_flag_t)] = { 0 };
    packet = request = request_packet = response_packet = response_data = NULL;

    request_packet = (unsigned char *)malloc(sizeof(tddi_packet_t) + sizeof(tddi_get_duty_cycle_flag_request_t));
    if(request_packet == NULL)
    {
        al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : Memory allocation failed func: %s: %d\n",__func__,__LINE__);
        perror("malloc()");
        return -1;
    }
    packet         = (tddi_packet_t *)request_packet;
    request        = (tddi_get_duty_cycle_flag_request_t *)(request_packet + sizeof(tddi_packet_t));

    memset(request_packet, 0, sizeof(tddi_packet_t) + sizeof(tddi_get_duty_cycle_flag_request_t));

    packet->version   = TDDI_CURRENT_VERSION;
    packet->signature = TDDI_PACKET_SIGNATURE;
    packet->type      = TDDI_PACKET_TYPE_REQUEST;
    packet->data_size = sizeof(tddi_get_duty_cycle_flag_request_t);
    packet->sub_type  = TDDI_REQUEST_TYPE_GET_WM_CHANNEL_FLAG;

    strcpy(request->name,if_info->name);

    fd = open(TDDI_FILE_NAME, O_RDWR);

    if (fd == -1)
    {
        fprintf(stderr, "Error opening "TDDI_FILE_NAME "\n");
        al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : (OPEN ERROR) Unable to open TDDI_FILE func: %s: %d\n",__func__,__LINE__);
        free(request_packet);
        return NODE_CONFIG_FAILURE;
    }

    ret = write(fd, request_packet, sizeof(tddi_packet_t) + sizeof(tddi_get_duty_cycle_flag_t));

    if (ret != sizeof(tddi_packet_t) + sizeof(tddi_get_duty_cycle_flag_t))
    {
        fprintf(stderr, "Error writing to "TDDI_FILE_NAME " ret=%d\n", ret);
        free(request_packet);
        return NODE_CONFIG_FAILURE;
    }

    lseek(fd, 0, SEEK_CUR);

    ret = read(fd, response_buffer, sizeof(response_buffer));

    if (ret != sizeof(response_buffer))
    {
        al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : (READ ERROR) Unable to read the buffer func: %s: %d\n",__func__,__LINE__);
        free(request_packet);
        close(fd);
        return NODE_CONFIG_FAILURE;
    }

    free(request_packet);
    close(fd);

    response_packet = (tddi_packet_t *)response_buffer;
    response_data   = (tddi_get_duty_cycle_flag_t *)(response_buffer + sizeof(tddi_packet_t));
    al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"CONFIGD : INFO : duty_cycle_flag = %d, operating channel = %d func : %s : %d\n", response_data->duty_cycle_flag, response_data->operating_channel, __func__, __LINE__);
    if (response_data->duty_cycle_flag == 1)
        return response_data->operating_channel;
    else
        return 0;
}

int main(int argc, char **argv)
{
   int                i;
   struct sockaddr_in client_addr, serv_addr;
   unsigned char      buffer[2400], dec_buffer[2400];
   unsigned char      *buffer_ptr, *dec_buffer_ptr;
   int                bytes_read, clilen, dec_buffer_length, j;
   int                imcp_packet_type, packet_encrypted, ret;
   char               command[256];
   char               addr_buffer[256];
   int                rc, on = 1;
   int                str_len = 0;
   unsigned short     imcp_flags;
   int                ret_val;
   int                sem_ret;
	char               tmp_action;
   int                al_conf_handle;
   struct rlimit      core_limits;

   openlog("configd", LOG_PID|LOG_CONS, LOG_USER);

   core_limits.rlim_cur = RLIM_INFINITY;
   core_limits.rlim_max = RLIM_INFINITY;
   if (setrlimit(RLIMIT_CORE, &core_limits) < 0)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : Unable to set core dump file size limit : %s<%d>\n", __func__, __LINE__);
   }

   al_conf_handle = libalconf_read_config_data();
   if (al_conf_handle == -1)
   {
       printf("\nCould not read configuration");
       return 0;
   }
 
   for (j = 0; j < 6; j++)
   {
      _MAC_ADDRESS[j] = _MAC_ADDRESS[j] - _BEGIN_SIGNATURE[j];
   }
//BOOT_TIME VALIDATION
   if((argc > 1) && !strcmp(argv[1], "validate")) {
	   ret_val = validating_capabilities();

	   if(ret_val != SUCCESS)
	   {
		   printf("configd cannot validate configurations due to an error");
		   return -1;
	   }
	   return 0;
   }
//BOOT_TIME VALIDATION END

//VLAN
   if((argc > 1) && !strcmp(argv[1], "vlan")) {
	  create_vlan_interface();
      init_virtual_configurations(al_conf_handle);
	  al_conf_close(al_conf_handle);
	  return 0;
   }
   al_conf_close(al_conf_handle);
//VLAN_END

   init_data();

   printf("\nCONFIGD:\t/**********CONFIGD STARTED****************/\n");


   /**
    * Wait for AP to start
    */

   _wait_for_ap_to_start();
   _set_ds_netif_ip_address();

//SPAWAR
   mesh_query_board_stats();

   /**
    * Set default log level to all for ATH5K
    */
   set_ath_debug_level(ATH_5K_DRIVER);
//SPAWAR_END

	sem_ret = sem_init(&mutex, 0, 1);/*Semaphore Initialisation*/
	if(sem_ret == -1)
	{
		printf("Getting error while initialising semaphore\n");
	}
	
   /**
    * Spawn post fs change thread
    */
   fs_change_thead_create();

   system("/root/configd_spawn.sh");

   if ((sock = socket(AF_INET, SOCK_DGRAM, 0)) < 0)
   {
      printf("\nCONFIGD:\tError opening socket.Exiting...");
      exit(1);
   }

   printf("\nCONFIGD:\tListening on port : %x waiting for IMCP packets...", IMCP_SERVER_PORT);

   bzero((char *)&serv_addr, sizeof(serv_addr));

   serv_addr.sin_family      = AF_INET;
   serv_addr.sin_addr.s_addr = htonl(INADDR_ANY);
   serv_addr.sin_port        = htons(IMCP_SERVER_PORT);

   rc = setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, (char *)&on, sizeof(on));
   if (rc < 0)
   {
      perror("setsockopt() failed");
      close(sock);
      exit(-1);
   }

   if (bind(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0)
   {
      printf("\nCONFIGD:\tCould not bind to port %x.Exiting...", IMCP_SERVER_PORT);
      exit(2);
   }

   printf("\nCONFIGD:\tBound to port : %x waiting for IMCP packets...", IMCP_SERVER_PORT);
   clilen = sizeof(client_addr);

   for ( ; ; )
   {
      packet_encrypted = 0;

      bytes_read       = recvfrom(sock, buffer, 2400, 0, (struct sockaddr *)&client_addr, &clilen);
      imcp_packet_type = verify_imcp_signature(buffer,&imcp_flags);
      if (imcp_packet_type == -1)
      {
         printf("\nCONFIGD :\tReceived IMCP signature verification failed\n");
         continue;
      }
      else if (imcp_packet_type == -2)
      {
         continue;
      }
       packet_encrypted = imcp_flags & IMCP_USE_ENCRYPTION;

      if (packet_encrypted != 0)
      {
#ifdef _CONFIGD_USE_ENCRYPTION_
         dec_buffer_length = get_imcp_signature_mesh_id_size();

         memcpy(dec_buffer, buffer, dec_buffer_length);

         dec_buffer_ptr = dec_buffer + dec_buffer_length;
         buffer_ptr     = buffer + dec_buffer_length;

         ret = al_decrypt(AL_CONTEXT buffer_ptr, bytes_read - dec_buffer_length, mesh_id_key, mesh_id_key_length, dec_buffer_ptr, AL_ENCRYPTION_DECRYPTION_METHOD_AES_ECB);
         if (ret > 0)
         {
            dec_buffer_length += ret;
         }
         else
         {
            printf("\nCONFIGD:\tDecryption failed dropping packet...");
            continue;
         }
#else
         printf("\nCONFIGD :\tReceived encrypted packet.Dropping packet...");
         continue;
#endif
      }
      else
      {
         memcpy(dec_buffer, buffer, bytes_read);
         dec_buffer_length = bytes_read;
      }

      printf("Configd: packet_type = %d\n", imcp_packet_type);
      packet_process_helper(imcp_packet_type, dec_buffer, dec_buffer_length, &client_addr);
      mesh_query_board_stats(); //SPAWAR
   }
		sem_ret = sem_destroy(&mutex);/*Destroying Semaphore*/

   close(sock);

   un_init_data();

   closelog();

   return 0;
}
