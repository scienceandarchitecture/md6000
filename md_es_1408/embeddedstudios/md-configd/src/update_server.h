/********************************************************************************
* MeshDynamics
* --------------
* File     : update_server.h
* Comments : firmware update functions
* Created  : 3/9/2005
* Author   : Prachiti Gaikwad
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  0  |3/9/2005 | created                                         | Prachiti|
* -----------------------------------------------------------------------------
********************************************************************************/

#ifndef __UPDATE_SERVER_H__
#define __UPDATE_SERVER_H__


#define _DEBUG_ON                     100 // always displayed
#define _DEBUG_OFF                    0
#define _DEBUG_LOW                    1   // only if Low or higher
#define _DEBUG_MEDIUM                 2
#define _DEBUG_HIGH                   3

#define RESPONSE_CMD_SUCCESS          123
#define RESPONSE_CMD_NOT_IMPL         124
#define RESPONSE_CMD_INVALID          125
#define RESPONSE_CMD_EXEC_FAILED      126
#define RESPONSE_INVALID_PARAMETER    127
#define RESPONSE_CHECKSUM_FAILED      128



#define IMCP_UPDATE_SERVER_PORT    0xfefe
#define PERMS                      0644

/**
 *	Command Types
 */

#define FCNT        0x1
#define STOR        0x2
#define RECV        0x3
#define STAT        0x4
#define QUIT        0x5
#define ABRT        0x6

#define _CMD_LEN    0x4

/*
 *	Status  Codes
 */

#define _SUCCESS     0x0
#define _ERR         0x1
#define _STOP        0x2
#define _PUT_FILE    0X3

typedef int (*_command_handler_t)(unsigned char *buffer, void *data);



struct _commands
{
   int                type;
   char               command[4];
   _command_handler_t process_func;
};

typedef struct _commands   commands_t;

/**
 *	Command handlers
 */


int _process_fcnt(unsigned char *buffer, void *data);
int _process_stor(unsigned char *buffer, void *data);
int _process_recv(unsigned char *buffer, void *data);
int _process_stat(unsigned char *buffer, void *data);
int _process_abrt(unsigned char *buffer, void *data);
int _process_quit(unsigned char *buffer, void *data);

/*
 * commands_t update_commands[] = {
 *
 *      {FCNT,"FCNT",_process_fcnt},
 *      {STOR,"STOR",_process_stor},
 *      {RECV,"RECV",_process_recv},
 *      {STAT,"STAT",_process_stat},
 *      {ABRT,"ABRT",_process_abrt},
 *      {QUIT,"QUIT",_process_quit}
 * };
 */


int packet_process_helper(int msup_packet_type, unsigned char *buffer, int new_sock, int bytes_read);
char *__strrev(char *string);
int send_reply(int sock, int response_code);

#endif
