/********************************************************************************
* MeshDynamics
* --------------
* File     : update_server.c
* Comments : Firmware update functions
* Created  : 3/9/2005
* Author   : Prachiti Gaikwad
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                       | Author  |
* -----------------------------------------------------------------------------
* |  7  |06/08/2006|Changes related fwupdate partition              |Prachiti |
*  ----------------------------------------------------------------------------
* |  6  |5/12/2006 | Changes for Firmware Update                    | Bindu   |
* -----------------------------------------------------------------------------
* |  5  |5/04/2005 | Checksum feature disabled                      | Sriram  |
* -----------------------------------------------------------------------------
* |  4  |4/23/2005 | Merged changes by Prachiti                     | Sriram  |
* -----------------------------------------------------------------------------
* |  3  |4/11/2005 | Fixed compilation errors                       | Sriram  |
* -----------------------------------------------------------------------------
* |  2  |3/14/2005 | byte-based packets                             | Prachiti|
* -----------------------------------------------------------------------------
* |  1  |3/14/2005 | CheckSum added                                 | Prachiti|
* -----------------------------------------------------------------------------
* |  0  |3/9/2005  | created                                        | Prachiti|
* -----------------------------------------------------------------------------
*
********************************************************************************/

//RAMESH16MIG included header file for close sys call
#include <unistd.h>

//RAMESH16MIG included header file for inet_ntoa
#include <arpa/inet.h>

#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <errno.h>
#include <netinet/in.h>
#include <fcntl.h>
#include <string.h>
#include <stdarg.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "md5.h"
#include "update_server.h"

#include "al.h"
#include "al_context.h"
#include "al_impl_context.h"



#define POST_FS_PARAM_TYPE_BINARY       1
#define POST_FS_PARAM_TYPE_CONFIG       2
#define POST_FS_PARAM_TYPE_NOTHING      3

#define EXECUTE_MESHD_REBOOT            system("/sbin/meshd reboot");
#define EXECUTE_PRE_FS_CHANGE           system("/root/pre_fs_change.sh");
#define EXECUTE_POST_FS_CHANGE(TYPE)    execute_post_fs_change(TYPE);
#define EXECUTE_MESHD_START             system("/sbin/meshd start");
#define EXECUTE_MESHD_STOP              system("/sbin/meshd stop");
#define EXECUTE_MESHD_CONFIGURE         system("/sbin/meshd configure");
#define EXECUTE_UNLOAD_TORNA            system("/root/unload_torna");
#define EXECUTE_STARTUP_SH              system("/root/startap.sh");
#define EXECUTE_SYSTEM_REBOOT           system("reboot");
#define EXECUTE_MESH_UPDATE_SERVER      system("/sbin/mesh_update_server");
#define EXECUTE_MESH_SET_REBOOT_FLAG    system("/sbin/meshd set_reboot_flag 1");
#define EXECUTE_FWUPDATE_MOUNT          system("/root/fw_update_mount.sh");
#define EXECUTE_FWUPDATE_SAVE           system("/root/fw_update_save.sh");
#define EXECUTE_COMMAND(command)    do { printf("\nExecuting command : \"%s\"", command); system(command); } while (0);

int execute_post_fs_change(int type)
{
   char command[1024];

   switch (type)
   {
   case POST_FS_PARAM_TYPE_BINARY:
      sprintf(command, "/root/post_fs_change.sh  binary");
      break;

   case POST_FS_PARAM_TYPE_CONFIG:
      sprintf(command, "/root/post_fs_change.sh  config");
      break;

   case POST_FS_PARAM_TYPE_NOTHING:
      sprintf(command, "/root/post_fs_change.sh nothing");
      break;

   default:
      printf("\nCONFIGD : EXECUTE_POST_FS_CHANGE invalid type");
      return -1;
   }

   EXECUTE_COMMAND(command);
   return 0;
}


#define RECV_TIMEOUT    -1
#define RECV_ERROR      -2


unsigned int  max_file_count;
int           debug_level   = _DEBUG_ON;
char          update_path[] = "/root/updates/";
unsigned char buffer[2400];

unsigned char MSUP_SIGNATURE[] = { 'M', 'S', 'U', 'P' };

/*
 *	returns MSUP packet type or -1 on error
 */
int verify_msup_signature(unsigned char *buffer)
{
   int ret = -1;
   int packet_type;


   if (memcmp(buffer, MSUP_SIGNATURE, sizeof(MSUP_SIGNATURE)))
   {
      return -1;
   }
   buffer += sizeof(MSUP_SIGNATURE);

   packet_type = *buffer;
   buffer++;

   return packet_type;
}


void DEBUG(int level, const char *format, ...)
{
   va_list args;

   va_start(args, format);

   fflush(stdout);

   if (debug_level >= level)
   {
      vprintf(format, args);
   }
}


char *get_file_name(char *file_path)
{
   char *file_name;

   file_name = file_path + strlen(file_path);

   while (file_name != file_path)
   {
      if (*file_name == '/')
      {
         file_name++;
         return file_name;
      }
      file_name--;
   }

   return NULL;
}


FILE *create_file_in_update_folder(char *file_path)
{
   char update_folder_path[256];
   char *file_name;

   memset(update_folder_path, 0, 256);
   strcpy(update_folder_path, update_path);

   /* check valid file_Path; */
   file_name = get_file_name(file_path);

   if (file_name == NULL)
   {
      return NULL;
   }
   strcat(update_folder_path, file_name);

   return fopen(update_folder_path, "wb");
}


int write_entry_in_inf_file(char *file_name, int type, char *file_path)
{
   char inf_file_name[256];
   FILE *inf_fp    = NULL;
   int  file_count = 0;

   memset(inf_file_name, 0, 256);

   strcpy(inf_file_name, update_path);
   strcat(inf_file_name, "installation.inf");


   inf_fp = fopen(inf_file_name, "a+");
   fprintf(inf_fp, "%s,%d,%s\n", file_name, type, file_path);
   fclose(inf_fp);
   return _SUCCESS;
}


int receive(int socket, unsigned char **buffer, int still_to_receive)
{
   int            rc             = 0;
   int            receive_status = 0;
   struct timeval receive_timeout;
   fd_set         fds;

   unsigned char *temp_buffer;

   temp_buffer = (char *)*buffer;


   receive_timeout.tv_sec  = 100;
   receive_timeout.tv_usec = 0;


   FD_ZERO(&fds);
   FD_SET(socket, &fds);



   rc = select(0, &fds, NULL, NULL, &receive_timeout);


   if (rc == 0)
   {
      return RECV_TIMEOUT;
   }


   if (rc < 0)
   {
      return RECV_ERROR;
   }

   receive_status = recv(socket, temp_buffer, still_to_receive, 0);


   /* If receive error... */
   if (receive_status < 0)
   {
      perror("Error receiving data");
      return(RECV_ERROR);
   }


   *buffer = temp_buffer;
   return 0;
}


int main(int argc, char *argv[])
{
   int                sock, i, new_sock;
   struct sockaddr_in client_addr, serv_addr;
   int                bytes_read, clilen;
   int                packet_type;
   int                status, ret = 0;
   int                msup_packet_type = 0;
   int                desc_ready, _server = 0;
   int                len, rc, on = 1;
   char               *update_file_path;
   char               *configd_file_path;
   char               command[1024];
   struct timeval     timeout;
   fd_set             master_set, working_set;
   int                max_sd, new_sd;
   int                close_conn = 0;

   DEBUG(_DEBUG_LOW, "\nMSUP_SERVER : UPDATE SERVER started.");

   /* NOT USED ANY MORE - UPDATE SERVER WITH PARAM USED TO UPDATE CONFIGD */

   if (argc == 4)
   {
      /**
       *   Call configd Starter
       */


      if (strcmp("-c", argv[1]) != 0)
      {
         DEBUG(_DEBUG_LOW, "\nCONFIGD_RESTARTER : wrong number of arguments\n");
         return -1;
      }

      configd_file_path = argv[2];               // config path
      update_file_path  = argv[3];               // update path

      memset(command, 0, sizeof(command));

      DEBUG(_DEBUG_LOW, "\nCONFIGD_RESTARTER :update_configd cat %s > %s\n", update_file_path, configd_file_path);
      sprintf(command, "cp %s  %s", update_file_path, configd_file_path);
      system(command);

      DEBUG(_DEBUG_LOW, "\nCONFIGD_RESTARTER :update_configd chmod +x %s\n", configd_file_path);
      sprintf(command, "chmod +x %s", configd_file_path);
      system(command);

      memset(command, 0, 1024);
      DEBUG(_DEBUG_LOW, "\nCONFIGD_RESTARTER :update_configd rm -f %s", update_file_path);
      sprintf(command, "rm -f %s", update_file_path);
      system(command);

      EXECUTE_POST_FS_CHANGE(POST_FS_PARAM_TYPE_BINARY)

      memset(command, 0, 1024);
      DEBUG(_DEBUG_LOW, "\nCONFIGD_RESTARTER :update_configd %s&", configd_file_path);
      sprintf(command, "%s&", configd_file_path);
      system(command);

      return 0;
   }


   memset(buffer, 0, 2400);

   if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0)
   {
      DEBUG(_DEBUG_HIGH, "\nError opening socket");
      exit(1);
   }

   rc = setsockopt(sock, SOL_SOCKET, SO_REUSEADDR,
                   (char *)&on, sizeof(on));

   if (rc < 0)
   {
      perror("setsockopt() failed");
      close(sock);
      exit(-1);
   }

   rc = ioctl(sock, FIONBIO, (char *)&on);
   if (rc < 0)
   {
      perror("ioctl() failed");
      close(sock);
      exit(-1);
   }

   bzero((char *)&serv_addr, sizeof(serv_addr));
   serv_addr.sin_family      = AF_INET;
   serv_addr.sin_addr.s_addr = htonl(INADDR_ANY);
   serv_addr.sin_port        = htons(IMCP_UPDATE_SERVER_PORT);

   if (bind(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0)
   {
      DEBUG(_DEBUG_HIGH, "\nCould not bind to port %x", IMCP_UPDATE_SERVER_PORT);
      exit(2);
   }

   DEBUG(_DEBUG_LOW, "\nMSUP_SERVER : Bound to port : %x", IMCP_UPDATE_SERVER_PORT);

   fflush(stdout);

   listen(sock, 5);

   clilen = sizeof(client_addr);

   FD_ZERO(&master_set);
   max_sd = sock;
   FD_SET(sock, &master_set);

   timeout.tv_sec  = 2 * 60;
   timeout.tv_usec = 0;

   EXECUTE_FWUPDATE_MOUNT

   do
   {
      memcpy(&working_set, &master_set, sizeof(master_set));
      rc = select(max_sd + 1, &working_set, NULL, NULL, &timeout);

      if (rc < 0)
      {
         perror("  select() failed");
         break;
      }

      if (rc == 0)
      {
         DEBUG(_DEBUG_LOW, "\nMSUP_SERVER : select() timed out. End program.");
         break;
      }

      for (i = 0; i <= max_sd && desc_ready > 0; ++i)
      {
         if (FD_ISSET(i, &working_set))
         {
            desc_ready -= 1;

            if (i == sock)
            {
               new_sock = accept(sock, (struct sockaddr *)&client_addr, &clilen);
               if (new_sock < 0)
               {
                  if (errno != EWOULDBLOCK)
                  {
                     perror("  accept() failed");
                     _server = 1;
                  }
                  break;
               }
               FD_SET(new_sock, &master_set);
               if (new_sock > max_sd)
               {
                  max_sd = new_sock;
               }
            }
            else
            {
               close_conn = 0;
               EXECUTE_PRE_FS_CHANGE

               do
               {
                  memset(buffer, 0, sizeof(buffer));
                  bytes_read = recv(new_sock, buffer, 2400, 0);

                  if (bytes_read <= 0)
                  {
                     DEBUG(_DEBUG_LOW, "\nMSUP_SERVER :Server Going Down...");
                     close_conn = 1;
                     _server    = 1;
                     break;
                  }
                  DEBUG(_DEBUG_LOW, "\nMSUP_SERVER : Packet received from %s", inet_ntoa(client_addr.sin_addr));

                  msup_packet_type = verify_msup_signature(buffer);
                  if (msup_packet_type == -1)
                  {
                     printf("\nMSUP_SERVER :\t#2");
                     continue;
                  }

                  status = packet_process_helper(msup_packet_type, buffer, new_sock, bytes_read);

                  if (status == _STOP)
                  {
                     DEBUG(_DEBUG_LOW, "\nMSUP_SERVER : STOP COMMAND : Stopping UPDATE SERVER...\n");

                     /** Copy all the files from update folder to fwupdate partition
                      *   and delete them from update folder
                      */

                     EXECUTE_FWUPDATE_SAVE
                     EXECUTE_MESH_SET_REBOOT_FLAG

                        close_conn = 1;
                     _server = 1;
                     break;
                  }

                  if (close_conn)
                  {
                     close(i);
                     FD_CLR(i, &master_set);
                     if (i == max_sd)
                     {
                        while (FD_ISSET(max_sd, &master_set) == 0)
                        {
                           max_sd -= 1;
                        }
                     }
                  }
               } while (1);
            }
         }
      }
   } while (_server == 0);
   for (i = 0; i <= max_sd; ++i)
   {
      if (FD_ISSET(i, &master_set))
      {
         close(i);
      }
   }

   DEBUG(_DEBUG_LOW, "\nMSUP_SERVER : returning from Mesh update servre\n");

   /*
    *	add new type POST_FS_PARAM_TYPE_UPDATE
    */

   return 0;
}


int packet_process_helper(int msup_packet_type, unsigned char *buffer, int new_sock, int bytes_read)
{
   unsigned char command_name[100];
   unsigned char command[1024];

   int  token_cnt = 0;
   int  ret       = 0;
   int  type      = 0;
   long file_size = 0;
   char checksum        [20];
   char file_path       [1024];

   switch (msup_packet_type)
   {
   case FCNT:
      ret = _process_fcnt(buffer, (void *)new_sock);
      break;

   case STOR:
      ret = _process_stor(buffer, (void *)new_sock);
      break;

   case QUIT:
      return _STOP;

      break;
   }


   return ret;
}


int _process_fcnt(unsigned char *buffer, void *data)
{
   /* TODO max_file_cnt not initialised */
   int           new_sock = 0;
   char          command[1024];
   unsigned char *buffer_ptr = buffer + 5;

   new_sock = (int)data;

   max_file_count = *buffer_ptr;

   DEBUG(_DEBUG_LOW, "\nMSUP_SERVER : FCNT COMMAND : File Count = %d", max_file_count);
   send_reply(new_sock, RESPONSE_CMD_SUCCESS);
   DEBUG(_DEBUG_LOW, "\nMSUP_SERVER : FCNT COMMAND SUCCESSFUL\n");
   return _SUCCESS;
}


int _process_stor(unsigned char *buffer, void *data)
{
   char          command[1024];
   char          *file_name;
   int           ret, bytes_written, bytes_read;
   int           new_sock, file_count, i;
   int           token_cnt;
   FILE          *fd;
   int           buffer_size = 1024;
   unsigned char *file_buffer;
   unsigned char recv_buffer[1024];
   int           ptr = 0;
   int           checksum_key_len = 0;
   unsigned char key128[16], checksum[16];
   unsigned char type_code  = 0;
   int           path_len   = 0;
   char          *file_path = NULL;
   long          file_size  = 0;
   unsigned char *buffer_ptr;
   unsigned int  le2cpu;


   fd            = 0;
   bytes_written = 0;
   bytes_read    = 0;
   token_cnt     = 0;
   file_count    = 0;
   new_sock      = 0;
   ret           = 0;


   memset(recv_buffer, 0, buffer_size);
   memset(key128, 0, sizeof(key128));
   memset(checksum, 0, sizeof(checksum));

   buffer_ptr = buffer + 5;
   new_sock   = (int)data;

   /*
    *	Read packet
    */

   type_code = *buffer_ptr;
   buffer_ptr++;

   path_len = *buffer_ptr;
   buffer_ptr++;

   file_path = (char *)malloc(path_len + 1);
   memset(file_path, 0, sizeof(path_len + 1));

   memcpy(file_path, buffer_ptr, path_len);
   file_path[path_len] = 0;
   buffer_ptr          = buffer_ptr + path_len;

   memcpy(&le2cpu, buffer_ptr, sizeof(unsigned int));
   buffer_ptr += sizeof(unsigned int);
   file_size   = al_impl_le32_to_cpu(le2cpu);


   checksum_key_len = *buffer_ptr;
   buffer_ptr++;

   if (checksum_key_len != 16)
   {
      send_reply(new_sock, RESPONSE_CMD_EXEC_FAILED);
      return _ERR;
   }
   else
   {
      memcpy(&checksum, buffer_ptr, checksum_key_len);
      buffer_ptr = buffer_ptr + checksum_key_len;
   }

   /*
    *	start processing
    */

   DEBUG(_DEBUG_LOW, "\nMSUP_SERVER : STOR COMMAND : File - %s, %d bytes", file_path, file_size);

   fd = create_file_in_update_folder(file_path);

   file_name = get_file_name(file_path);

   send_reply(new_sock, RESPONSE_CMD_SUCCESS);

   /* start receiving  the file contents */

   bytes_written = 0;
   bytes_read    = 0;

   file_buffer = (char *)malloc(file_size);
   memset(file_buffer, 0, file_size);



   while (bytes_read < file_size)
   {
      memset(recv_buffer, 0, sizeof(recv_buffer));
      //ret = receive(new_sock, &recv_buffer, buffer_size);
      ret = recv(new_sock, recv_buffer, buffer_size, 0);

      if (ret <= 0)
      {
         DEBUG(_DEBUG_LOW, "\nMSUP_SERVER : Breaking Total bytes read = %u", bytes_read);
         break;
      }
      memcpy(file_buffer + ptr, &recv_buffer, ret);
      ptr         = ptr + ret;
      bytes_read += ret;
   }

   /* add checksum function and check integrity.send response accordingly */

   generate_md(file_buffer, bytes_read, key128);


   if (1)
   {
      /* checksum match */

      if ((bytes_written = fwrite(file_buffer, 1, bytes_read, fd)) == -1)
      {
         perror("\nMSUP_SERVER : Write failed ");
         send_reply(new_sock, RESPONSE_CMD_EXEC_FAILED);

         free(file_buffer);
         fclose(fd);
         return _ERR;
      }

      DEBUG(_DEBUG_LOW, "\nMSUP_SERVER : STOR COMMAND : Checksum Match :)");
      send_reply(new_sock, RESPONSE_CMD_SUCCESS);

      fclose(fd);
   }
   else
   {
      DEBUG(_DEBUG_LOW, "\nMSUP_SERVER : STOR COMMAND : Checksum Mismatch :(");
      send_reply(new_sock, RESPONSE_CHECKSUM_FAILED);
      return _ERR;
   }

   free(file_buffer);
   write_entry_in_inf_file(file_name, type_code, file_path);

   DEBUG(_DEBUG_LOW, "\nMSUP_SERVER : STOR COMMAND SUCCESSFUL \n");
   return _SUCCESS;
}


int _process_recv(unsigned char *buffer, void *data)
{
   return 0;
}


int _process_stat(unsigned char *buffer, void *data)
{
   return 0;
}


int _process_abrt(unsigned char *buffer, void *data)
{
   return _STOP;
}


int _process_quit(unsigned char *buffer, void *data)
{
   return _STOP;
}


int send_reply(int sock, int response_code)
{
   unsigned char buffer[20];
   int           ret, bytes = 0;
   unsigned char *p;

   memset(buffer, 0, 20);
   p = buffer;

   memcpy(p, &MSUP_SIGNATURE, sizeof(MSUP_SIGNATURE));
   p     = p + sizeof(MSUP_SIGNATURE);
   bytes = bytes + sizeof(MSUP_SIGNATURE);

   *p = response_code;
   bytes++;

   ret = send(sock, buffer, bytes, 0);
   if (ret < 0)
   {
      perror("\n s() failed Error\n");
      return _ERR;
   }

   return ret;
}
