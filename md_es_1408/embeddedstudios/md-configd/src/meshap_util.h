/********************************************************************************
 * MeshDynamics
 * --------------
 * File     : meshap_util.h
 * Comments :  This file includes data structures and function pointers for the mesh
 *             parameters
 * Created  : 10/30/2014
 * Author   : Sukshata Nangadalli
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
 *
 *******************************************************************************/

#define _AL_CONF_MAX_TOKEN_SIZE     128
#define AL_802_11_ESSID_MAX_SIZE    32
#define IW_NAME_FORMAT              "/etc/iw_%s.conf"               /* Macro for iwconfig file name*/
#define HOSTAPD_NAME_FORMAT         "/etc/hostapd_%s.conf"          /* Macro for hostapd file name */
#define HOSTAPD_PID_FILE_NAME_FORMAT       "/etc/pidfile_%s.pid"              /*Macro for hostapd pid file */
#define BUFFER_SIZE                 1024
#define NO_OF_BUFFERS               256

#define MESH_FAILOVER_PHY_CONN      1
#define MESH_FAILOVER_PING_CONN     2

#include <dot11e_conf.h>
#include "acl_conf.h"

static const unsigned char *_vendor_info_header = "0012CE01";

char pid_buffer[20];

struct _al_conf_data
{
   char                          name[_AL_CONF_MAX_TOKEN_SIZE + 1];        /* it holds the name of the node */
   char                          description[_AL_CONF_MAX_TOKEN_SIZE + 1]; /* it describe about mesh node */
   char                          mesh_id[_AL_CONF_MAX_TOKEN_SIZE + 1];     /* it holds the name of the mesh_id */
   char                          mesh_imcp_key[16 + 1];
   char                          essid[AL_802_11_ESSID_MAX_SIZE + 1];      /*it holds network name */
   int                           rts_th;                                   /* it holds the size of smallest packet */
   int                           frag_th;                                  /* it holds maximum fragment size */
   int                           beacon_interval;                          /*it holds the time interval between beacon transmissions */
   int                           dynamic_channel_allocation;               /* dynamic channel allocation on/off */
   int                           stay_awake_count;
   int                           bridge_ageing_time;                       /* Ageing time in seconds */
   int                           wds_encrypted;
   char                          model[32];                                /* it holds the operating mode of the device */
   char                          gps_x_coordinate[32];
   char                          gps_y_coordinate[32];
   int                           failover_enabled;
   int                           power_on_default;
   unsigned char                 server_ip_addr[128];
   unsigned char                 mgmt_gw_addr[64];
   int                           mgmt_gw_enable;
   unsigned char                 mgmt_gw_certificates[64];
   int                           disable_backhaul_security;
   int                           fcc_certified_operation;
   int                           etsi_certified_operation;
   int                           regulatory_domain;
   int                           country_code;
   int                           ds_tx_rate;    /* */
   int                           ds_tx_power;   /* it holds the transmit power in dBm */
   unsigned int                  config_sqnr;   /* it holds the configuration sequence number */

   int                           if_count;      /* network interface count */
   al_conf_if_info_t             *if_info;      /* network interface info */

   int                           vlan_count;    /* vlan count */
   al_conf_vlan_info_t           *vlan_info;    /* vlan info */

   al_conf_effistream_criteria_t *criteria_root;

   unsigned char                 preferred_parent[6]; /* Preferred parent address for wireless ds */
   unsigned char                 signal_map[8];       /* curve points */
   int                           heartbeat_interval;
   int                           heartbeat_miss_count;
   int                           hop_cost;     /* Re-used as flags for IGMP support/etc */
   int                           max_allowable_hops;
   int                           las_interval; /* location awareness scan interval */
   int                           change_resistance;
   int                           use_virt_if;

   char                          *parser_buffer;
   char                          *pos;
   char                          current_token[_AL_CONF_MAX_TOKEN_SIZE + 1];
   char                          *curent_token_pos;
   int                           current_token_code;
   int                           line_number;
   int                           column_number;
   int                           parse_mode;
   char                          *mesh_imcp_key_buffer;         /** un-uudecoded, un-aesdecrypted */
   int                           security_heirarchy_level;

   al_conf_dhcp_info_t           dhcp_info;
   al_conf_gps_info_t            gps_info;
   al_conf_logmon_info_t         logmon_info;

   al_conf_option_t              *option_info;
};

typedef struct _al_conf_data   _al_conf_data_t;

/* Function pointers for setting iwconfig parameters */
struct mesh_vectors
{
   int (*set_driver)             (char *buffer);
   int (*set_ieee80211h)         (char *buffer);
   int (*set_vendor_info)        (char *buffer);
   int (*set_essid)              (char *buffer, al_conf_if_info_t *if_info);
   int (*set_rts_th)             (char *buffer, al_conf_if_info_t *if_info);
   int (*set_frag_th)            (char *buffer, al_conf_if_info_t *if_info);
   int (*set_preamble)           (char *buffer, al_conf_if_info_t *if_info);
   int (*set_beacon_interval)    (char *buffer, al_conf_if_info_t *if_info);
   int (*set_country_code)       (char *buffer, int country_code);
   int (*set_if_name)            (char *buffer, al_conf_if_info_t *if_info);
   int (*set_channel)            (char *buffer, al_conf_if_info_t *if_info, int channel);
   int (*set_dca_list)           (char *buffer, al_conf_if_info_t *if_info, char **conf_string);
   int (*set_phy_sub_type)       (char *buffer, al_conf_if_info_t *if_info);
   int (*set_hide_ssid)          (char *buffer, al_conf_if_info_t *if_info);
   int (*set_acl)                (char *buffer, _al_conf_data_t *data, acl_conf_t *acl_data, al_conf_if_info_t *if_info, int vlan_index);
   int (*set_txpower)            (char *buffer, al_conf_if_info_t *if_info);
   int (*set_txrate)             (char *buffer, al_conf_if_info_t *if_info);
   //int (*set_monitor)                        (char *buffer, al_conf_if_info_t *if_info);
   int (*set_security_info)      (char **conf_string, al_conf_if_info_t *if_info, al_security_info_t *security_info);
   int (*set_dot11e_info)        (char **conf_string, dot11e_conf_category_t *dot11e_data);
   int (*set_ieee80211n)         (char *buffer,al_conf_if_info_t *if_info);
   int (*set_wmm_enabled)        (char *buffer,al_conf_if_info_t *if_info);
   int (*set_ht_capab)           (char *buffer,al_conf_if_info_t *if_info,al_ht_capab_t *ht_capab_info);
   int (*set_require_ht)         (char *buffer,al_conf_if_info_t *if_info);
   int (*set_obss_interval)      (char *buffer,al_conf_if_info_t *if_info);
   int (*set_ieee80211ac)         (char *buffer,al_conf_if_info_t *if_info);
   //int (*set_wmm_enabled)         (char *buffer,al_conf_if_info_t *if_info);
   int (*set_vht_capab)           (char *buffer,al_conf_if_info_t *if_info,al_vht_capab_t *vht_capab_info);
   int (*set_require_vht)         (char *buffer,al_conf_if_info_t *if_info);
   int (*set_vht_oper_chwidth)    (char *buffer,al_conf_if_info_t *if_info, al_vht_capab_t *vht_capab_info);
   int (*set_vht_oper_centr_freq_seg0_idx)    (char *buffer,al_conf_if_info_t *if_info, al_vht_capab_t *vht_capab_info);
   int (*set_vht_oper_centr_freq_seg1_idx)    (char *buffer,al_conf_if_info_t *if_info, al_vht_capab_t *vht_capab_info);
//VLAN
   int (*set_bss)                (char* buffer,_al_conf_data_t *data, int vlan_index);
   int (*set_ssid)               (char* buffer,_al_conf_data_t* data, int vlan_index);
   int (*set_bssid)              (char* buffer,_al_conf_data_t* data, int vlan_index, char *mac);
//VLAN_END
};
typedef struct mesh_vectors   mesh_vectors_t;

int init_configurations(int al_conf_handle, int dot11e_conf_handle, int flag, int start_hostapd);

struct if_map
{
   char           if_name[32];           /* it holds the interface name */
   char           file_name[32];         /* it holds the file name */
   unsigned char  use_type;              /* it holds the WM or DS */
   mesh_vectors_t *vector_ptr;
};

typedef struct if_map   _if_map;

/* Interface Map structure */
struct interface_map
{
   int     if_count; /* it holds the value of interface count */
   _if_map *if_map;
};
typedef struct interface_map   interface_map_t;
