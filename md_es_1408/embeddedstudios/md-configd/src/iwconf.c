/********************************************************************************
 * MeshDynamics
 * --------------
 * File     : iwconf.c
 * Comments : This file contains functions for setting iwconfig parameters and iw configuration
 *            file creation
 *
 * Created  : 10/30/2014
 * Author   : Sukshata Ningadalli
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
 **********************************************************************************/

#include <string.h>
#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>

#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include "al.h"
#include "al_context.h"
#include "al_impl_context.h"
#include "al_conf.h"
#include "al_conf_lib.h"
#include "configd.h"

#include "meshap_util.h"


int iw_meshap_set_essid(char *buffer, al_conf_if_info_t *if_info);
int iw_meshap_set_rts_th(char *buffer, al_conf_if_info_t *if_info);
int iw_meshap_set_frag_th(char *buffer, al_conf_if_info_t *if_info);
int iw_meshap_set_channel(char *buffer, al_conf_if_info_t *if_info);
int iw_meshap_set_txpower(char *buffer, al_conf_if_info_t *if_info);
int iw_meshap_set_txrate(char *buffer, al_conf_if_info_t *if_info);
int iw_meshap_set_security_info(char **conf_string, al_conf_if_info_t *if_info, al_security_info_t *security_info);
//int iw_meshap_set_monitor(char *buffer, al_conf_if_info_t *if_info);


int create_iw_config_file(_al_conf_data_t *data, al_conf_if_info_t *if_info, interface_map_t interface_map, int index);

//RAMESH16MIG adding func declaration
int conf_open_file(char *file_name);

mesh_vectors_t iwconfig_ops =
{
   set_essid :                                        iw_meshap_set_essid,
   set_rts_th :                                       iw_meshap_set_rts_th,
   set_frag_th :                                      iw_meshap_set_frag_th,
   set_channel :                                      iw_meshap_set_channel,
   set_txpower :                                      iw_meshap_set_txpower,
   set_txrate :                                       iw_meshap_set_txrate,
   //set_monitor :                                                                          iw_meshap_set_monitor,
   set_security_info :                                iw_meshap_set_security_info,
};

/*set functions for iwconfig*/

int iw_meshap_set_essid(char *buffer, al_conf_if_info_t *if_info)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter: %s: %d\n",__func__,__LINE__);
   int ret;

   ret = sprintf(buffer, "iwconfig %s essid %s\n", if_info->name, if_info->essid);
   system(buffer);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"CONFIGD : INFO : command: %s : %s: %d\n", buffer,__func__,__LINE__);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit : %s: %d\n",__func__,__LINE__);
   return ret;
}


int iw_meshap_set_rts_th(char *buffer, al_conf_if_info_t *if_info)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter: %s: %d\n",__func__,__LINE__);
   int ret;

   ret = sprintf(buffer, "iwconfig %s rts %d\n", if_info->name, if_info->rts_th);
   system(buffer);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"CONFIGD : INFO : command: %s : %s: %d\n", buffer,__func__,__LINE__);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit : %s: %d\n",__func__,__LINE__);
   return ret;
}


int iw_meshap_set_frag_th(char *buffer, al_conf_if_info_t *if_info)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter: %s: %d\n",__func__,__LINE__);
   int ret;

   ret = sprintf(buffer, "iwconfig %s frag %d\n", if_info->name, if_info->frag_th);
   system(buffer);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"CONFIGD : INFO : command: %s : %s: %d\n", buffer,__func__,__LINE__);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit : %s: %d\n",__func__,__LINE__);
   return ret;
}


int iw_meshap_set_channel(char *buffer, al_conf_if_info_t *if_info)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter: %s: %d\n",__func__,__LINE__);
   int ret;

   ret = sprintf(buffer, "iwconfig %s channel %d\n", if_info->name, if_info->wm_channel);
   system(buffer);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"CONFIGD : INFO : command: %s : %s: %d\n", buffer,__func__,__LINE__);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit : %s: %d\n",__func__,__LINE__);
   return ret;
}


int iw_meshap_set_txpower(char *buffer, al_conf_if_info_t *if_info)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter: %s: %d\n",__func__,__LINE__);
   int ret;

   if (if_info->txpower != 0)
   {
      ret = sprintf(buffer, "iwconfig %s txpower %dmW\n", if_info->name, if_info->txpower);
   }
   else
   {
      ret = sprintf(buffer, "iwconfig %s txpower 0\n", if_info->name);
   }

   system(buffer);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"CONFIGD : INFO : command: %s : %s: %d\n", buffer,__func__,__LINE__);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit : %s: %d\n",__func__,__LINE__);
   return ret;
}


int iw_meshap_set_txrate(char *buffer, al_conf_if_info_t *if_info)
{
   al_print_log( AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter: %s: %d\n",__func__,__LINE__);
   int ret;

   ret = sprintf(buffer, "iwconfig %s rate %dM\n", if_info->name, if_info->txrate);
   system(buffer);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"CONFIGD : INFO : command: %s : %s: %d\n", buffer,__func__,__LINE__);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit : %s: %d\n",__func__,__LINE__);
   return ret;
}

#if 0
int iw_meshap_set_monitor(char *buffer, al_conf_if_info_t *if_info)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter : %s: %d\n",__func__,__LINE__);
   int ret;

   ret = sprintf(buffer, "iw phy phy3 interface add mon3 type monitor");
   system(buffer);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"CONFIGD : INFO : command: %s : %s: %d\n", buffer,__func__,__LINE__);

   memset(buffer, 0, BUFFER_SIZE);

   ret = sprintf(buffer, "ifconfig mon3 up");
   system(buffer);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"CONFIGD : INFO : command: %s : %s: %d\n", buffer,__func__,__LINE__);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit : %s: %d\n",__func__,__LINE__);

   return ret;
}
#endif

int iw_meshap_set_security_info(char **conf_string, al_conf_if_info_t *if_info, al_security_info_t *security_info)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter: %s: %d\n",__func__,__LINE__);
   char *config_string = NULL;
   char buffer[BUFFER_SIZE];
   char *ret_buffer;
   int  ret, i, j;
   char *p;
   int  config_string_length;


   ret_buffer = *conf_string;

   p = ret_buffer;

   memset(buffer, 0, BUFFER_SIZE);

   if (security_info->_security_wep.enabled)
   {
      memset(buffer, 0, sizeof(buffer));
      sprintf(buffer, "iwconfig %s key ", if_info->name);
      ret = strlen(buffer);
      memcpy(p, buffer, ret);
      p += ret;
      for (j = 0; j <= security_info->_security_wep.keys[security_info->_security_wep.key_index].len - 1; j++)
      {
         sprintf(buffer, "%02X", security_info->_security_wep.keys[security_info->_security_wep.key_index].bytes[j]);
         ret = strlen(buffer);
         memcpy(p, buffer, ret);
         p += ret;
      }

      al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"CONFIGD : INFO : command: %s : %s: %d\n",ret_buffer,__func__,__LINE__);
      system(ret_buffer);

      *p = '\n';
      p++;
      *conf_string = p;
      return 0;
   }
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit : %s: %d\n",__func__,__LINE__);
   return 0;
}

int iw_meshap_set_txpower_helper(al_conf_if_info_t *if_info)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter: %s: %d\n",__func__,__LINE__);
   char buffer[BUFFER_SIZE];

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit : %s: %d\n",__func__,__LINE__);
   return iw_meshap_set_txpower(buffer, if_info);
}

int mesh_start_iwconfig(interface_map_t interface_map, int flag)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter: %s: %d\n",__func__,__LINE__);
   char buffer[BUFFER_SIZE];
   int  i, ret;
   char tmp1[32];
   char *p, *ret_buffer;

   ret_buffer = (char *)al_heap_alloc(AL_CONTEXT BUFFER_SIZE AL_HEAP_DEBUG_PARAM);

   memset(ret_buffer, 0, BUFFER_SIZE);
   p = ret_buffer;

   for (i = 0; i < interface_map.if_count; i++)
   {
      if ((flag == 0) && (interface_map.if_map[i].use_type == AL_CONF_IF_USE_TYPE_DS))
      {
         strncpy(tmp1, interface_map.if_map[i].file_name, 20);
      	 al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"CONFIGD : INFO : tmp1 : %s : %s : %d\n",tmp1,__func__,__LINE__);
         ret = strlen(tmp1);
         memcpy(p, tmp1, ret);
         p += ret;
         strncpy(tmp1, " ", 5);
         ret = strlen(tmp1);
         memcpy(p, tmp1, ret);
         p += ret;
      }
      else
      {
         /*send sighup to hostapd */
      }
   }
   *p  = 0;
   ret = p - ret_buffer;

   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"CONFIGD : INFO : iwconfig launching %s : %s: %d\n",ret_buffer,__func__,__LINE__);
   sprintf(buffer, "iwconfig -BP pidfile.pid %s", ret_buffer);
   system(buffer);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit : %s: %d\n",__func__,__LINE__);


   return 0;
}

//VLAN
void create_vlan_interface()
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter: %s: %d\n",__func__,__LINE__);
   int                al_conf_handle, i, j, k, ret;
   _al_conf_data_t    *data;
   char mac[6], buffer[128];
	unsigned int tmpCount = 0, tmpIndex = 0;
	unsigned char vCount = 0;
   
   al_conf_handle = libalconf_read_config_data();
   al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"CONFIGD : INFO : al_conf_handle : %s: %d\n",__func__,__LINE__);
   if (al_conf_handle == -1)
   {
	      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : Could not read configuration: %s: %d\n",__func__,__LINE__);
	      return;
	   }

   data = (_al_conf_data_t *)al_conf_handle;

	vCount = _BEGIN_SIGNATURE[18];
   for(i=0;i < data->if_count;i++)
   {
	   /*Create Vlan inteface only on physical AP mode interface*/
	   if ((data->if_info[i].use_type == AL_CONF_IF_USE_TYPE_AP) && !(data->if_info[i].phy_type == AL_CONF_IF_PHY_TYPE_802_11_VIRTUAL))
	   {
		   for(j=1;j < data->vlan_count;j++)
		   {
				if(j <= vCount)
				{
			   	memset(buffer, 0, 128);
			   	ret = sprintf(buffer,"iw dev %s interface add %s type __ap",data->if_info[i].name, data->vlan_info[j].name);
			   	system(buffer);
   				al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"CONFIGD : INFO : command : %s : %s: %dn",buffer,__func__,__LINE__);

			   	memset(buffer, 0, 128);
					for( tmpIndex = 0 ; tmpIndex < 6 ; tmpIndex++ )
					{
						mac[tmpIndex] = _VLANMAC_ADDRESS[tmpIndex + (tmpCount * 6)];      // _VLANMAC_ADDRESS represents VLAN MAC offset
					}
					tmpCount++;

					printf("IF_name:%s\tMAC:%x:%x:%x:%x:%x:%x\n", data->vlan_info[j].name, mac[0], mac[1],mac[2],mac[3],mac[4],mac[5]);
					ret = sprintf(buffer,"ifconfig %s hw ether %02x:%02x:%02x:%02x:%02x:%02x up",
					   data->vlan_info[j].name, mac[0],mac[1],mac[2],mac[3],mac[4],mac[5]);
					system(buffer);
   					al_print_log(AL_CONTEXT AL_LOG_TYPE_INFORMATION,"CONFIGD : INFO : command : %s : %s: %dn",buffer,__func__,__LINE__);
				}
				else
				{
	      				al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : The created vlans[%d] is greater than the patched vlans[%d]: %s: %d\n",data->vlan_count, vCount,__func__,__LINE__);
					break;
				}
		   }
	   }
   }
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit : %s: %d\n",__func__,__LINE__);
}
//VLAN_END

/* Create iwconfig file */
int create_iw_config_file(_al_conf_data_t *data, al_conf_if_info_t *if_info, interface_map_t interface_map, int index)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter: %s: %d\n",__func__,__LINE__);
   char             *config_string = NULL;
   int              config_string_length;
   int              ret, handle;
   char             *p;
   char             buffer[BUFFER_SIZE];
   char             *ret_buffer;
   int              i, j;
   al_conf_option_t *options;

   config_string_length = 0;


   if (data == NULL)
   {
     al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : No data available: %s: %d\n",__func__,__LINE__);
      return -1;
   }

   ret_buffer = (char *)al_heap_alloc(AL_CONTEXT NO_OF_BUFFERS * BUFFER_SIZE AL_HEAP_DEBUG_PARAM);
   memset(ret_buffer, 0, NO_OF_BUFFERS * BUFFER_SIZE);
   p = ret_buffer;

   memset(buffer, 0, BUFFER_SIZE);
   ret = sprintf(buffer, "# Iwconfig file\n\n");
   memcpy(p, buffer, strlen(buffer));
   p += ret;
   *p = '\n';
   p++;

#if 0
   memset(buffer, 0, ret);

   printf("DCA config = %d intf = %s\n", if_info->dca, if_info->name);
   if (!if_info->dca)
   {
      ret = interface_map.if_map[index].vector_ptr->set_channel(buffer, if_info);
      memcpy(p, buffer, strlen(buffer));
      p += ret;
      *p = '\n';
      p++;
      memset(buffer, 0, ret);
   }
#endif
   ret = interface_map.if_map[index].vector_ptr->set_rts_th(buffer, if_info);
   memcpy(p, buffer, strlen(buffer));
   p += ret;
   *p = '\n';
   p++;

   memset(buffer, 0, ret);
   ret = interface_map.if_map[index].vector_ptr->set_frag_th(buffer, if_info);
   memcpy(p, buffer, strlen(buffer));
   p += ret;
   *p = '\n';
   p++;
#if 0
   memset(buffer, 0, ret);
   ret = interface_map.if_map[index].vector_ptr->set_essid(buffer, if_info);
   memcpy(p, buffer, strlen(buffer));
   p += ret;
   *p = '\n';
   p++;
#endif
   memset(buffer, 0, ret);
   ret = interface_map.if_map[index].vector_ptr->set_txpower(buffer, if_info);
   memcpy(p, buffer, strlen(buffer));
   p += ret;
   *p = '\n';
   p++;


   memset(buffer, 0, ret);
   ret = interface_map.if_map[index].vector_ptr->set_txrate(buffer, if_info);
   memcpy(p, buffer, strlen(buffer));
   p += ret;
   *p = '\n';
   p++;

#if 0
   if (!strcmp(if_info->name, "wlan3"))
   {
      memset(buffer, 0, ret);
      ret = interface_map.if_map[index].vector_ptr->set_monitor(buffer, if_info);
      memcpy(p, buffer, strlen(buffer));
      p += ret;
      *p = '\n';
      p++;
   }
#endif

   ret = interface_map.if_map[index].vector_ptr->set_security_info(&p, if_info, &if_info->security_info);


   /* copying string pointer */

   *p = '\n';
   p++;


   *p = 0;
   p++;

   ret           = p - ret_buffer;
   config_string = (char *)al_heap_alloc(AL_CONTEXT ret + 1 AL_HEAP_DEBUG_PARAM);
   strcpy(config_string, ret_buffer);
   config_string_length = ret;

   al_heap_free(AL_CONTEXT ret_buffer);


   handle = conf_open_file(interface_map.if_map[index].file_name);

   al_write_file(AL_CONTEXT handle, config_string, config_string_length);
   al_close_file(AL_CONTEXT handle);

   al_heap_free(AL_CONTEXT config_string);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit : %s: %d\n",__func__,__LINE__);

   return 0;
}
