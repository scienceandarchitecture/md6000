/********************************************************************************
* MeshDynamics
* --------------
* File     : cwtxtest.c
* Comments : Continous Transmission Daemon
* Created  : 9/25/2006
* Author   : Sriram Dayanandan
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  0  |9/25/2006 | Created                                         | Sriram |
* -----------------------------------------------------------------------------
********************************************************************************/

#include <stdio.h>
#include <string.h>
#include <strings.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <fcntl.h>
#include <getopt.h>

static void _usage()
{
   fprintf(stdout,
           "\n"
           "usage: cwtxtest [-n packet_size] [-p udp_port] [-c delay_tx_count] [-t sleep_delay] [-v] ip_address\n"
           "-n         : Specify packet size (default: 1200 bytes)\n"
           "-p			: Specify UDP port (default : 0xABCD)\n"
           "-c			: Transmission count before sleep delay (default : 0)\n"
           "-t			: Sleep delay in seconds (default : 0)\n"
           "-v			: Verbose mode\n"
           "ip_address	: Destination IP address\n\n"
           );
}


int main(int argc, char **argv)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter: %s: %d\n",__func__,__LINE__);
   int            i;
   int            packet_size;
   unsigned short udp_port;
   int            delay_tx_count;
   int            sleep_delay;
   int            sock;
   char           ip_address[64];
   unsigned char  verbose;
   int            ch;

   packet_size    = 1200;
   udp_port       = 0xABCD;
   delay_tx_count = 0;
   sleep_delay    = 0;
   verbose        = 0;

   fprintf(stdout,
           "\nMeshDynamics Continous Transmission Utility version %s\n"
           "Copyright (c) 2006 Meshdynamics, Inc\n"
           "All rights reserved.\n\n",
           "_TORNA_VERSION_STRING_");

   while ((ch = getopt(argc, argv, "n:p:c:t:v")) != EOF)
   {
      switch (ch)
      {
      case 'n':
         packet_size = atoi(optarg);
         break;

      case 'p':
         udp_port = atoi(optarg);
         break;

      case 'c':
         delay_tx_count = atoi(optarg);
         break;

      case 't':
         sleep_delay = atoi(optarg);
         break;

      case 'v':
         verbose = 1;
         break;
      }
   }

   if (optind >= argc)
   {
      fprintf(stderr, "\nNo IP address specified\n");
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : \nNo IP address specified %s:%d\n",__func__,__LINE__);
      
      _usage();
      return -1;
   }

   strcpy(ip_address, argv[optind]);

   fprintf(stdout,
           "Starting transmission to %s with %d packet size on %d UDP port with %d second delay after %d packets\n\n",
           ip_address,
           packet_size,
           udp_port,
           sleep_delay,
           delay_tx_count);

   if ((sock = socket(AF_INET, SOCK_DGRAM, 0)) < 0)
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : opening socket %s:%d\n",__func__,__LINE__);
      perror("socket");
      return -1;
   }

   i = 0;

   while (1)
   {
      if (verbose)
      {
         fprintf(stdout, "Transmitting...\n");
      }

      send_packet_ex(sock, packet_size, udp_port, ip_address);

      if (++i > delay_tx_count)
      {
         i = 0;
         if (sleep_delay != 0)
         {
            sleep(sleep_delay);
         }
      }
   }
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit: %s: %d\n",__func__,__LINE__);

   return 0;
}


int send_packet_ex(int sock, int data_length, int udp_port, const char *ip_address)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter: %s: %d\n",__func__,__LINE__);
   int                clilen, ret, on;
   struct sockaddr_in serv_addr;
   char               output[2400];
   char               *buffer_ptr, *output_data_ptr;

   on = 1;
   setsockopt(sock, SOL_SOCKET, SO_BROADCAST, &on, sizeof(on));

   bzero((char *)&serv_addr, sizeof(serv_addr));

   serv_addr.sin_family      = AF_INET;
   serv_addr.sin_port        = htons(udp_port);
   serv_addr.sin_addr.s_addr = inet_addr(ip_address);

   clilen = sizeof(struct sockaddr_in);

   ret = sendto(sock, output, data_length, 0, (const struct sockaddr *)&serv_addr, clilen);

   if (ret == -1)
   {
       al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : Sendto failed %s: %d\n",__func__,__LINE__);
       perror("sendto");
   }

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit: %s: %d\n",__func__,__LINE__);
   return ret;
}
