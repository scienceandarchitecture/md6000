#
# Copyright (C) 2008-2010 OpenWrt.org
#
# This is free software, licensed under the GNU General Public License v2.
# See /LICENSE for more information.
#

include $(TOPDIR)/rules.mk

PKG_NAME:=libalconf
PKG_RELEASE:=10
PKG_BUILD_DEPENDS:=md-torna

include $(INCLUDE_DIR)/package.mk
include $(INCLUDE_DIR)/kernel.mk

define Package/libalconf
	SECTION:=MeshDynamics
	CATEGORY:=MeshDynamics Applications
  TITLE:=MeshDynamics Lib alconf
	DEPENDS:=+kmod-md-meshap
endef

TARGET_CFLAGS+= \
	-I$(STAGING_DIR)/usr/share/md-meshap \
	-I$(STAGING_DIR)/usr/share/md-meshap/aes \
	-I$(STAGING_DIR)/usr/include/md-meshap \
	-I$(STAGING_DIR)/usr/include/md-meshap/aes \
	-I$(STAGING_DIR)/usr/include/md-torna \
	-D_TORNA_VERSION_STRING_=\"test_shasta\" \
	-D_TORNA_VERSION_MAJOR_=2 \
	-D_TORNA_VERSION_MINOR_=5 \
	-D_TORNA_VERSION_VARIANT_=99 \
	-D_MESHAP_USE_ATOMIC_ \
	-D_TORNA_ENABLE_LOGGER_ \
	-D_TORNA_DEBUG_MASK_VALUE_=0xE \
	-D_TORNA_MESHAP_PACKET_POOL_SIZE_=1024 \
	-DACCESS_POINT_PACKET_POOL_SIZE=2048 \
	-DAL_USE_ENCRYPTION=1 \
	-DAL_CONF_PUT_SEPERATE \
	-D_TORNA_MESHAP_REBOOT_TIME_=10 \
	-DACCESS_POINT_VLAN_BCAST_OPTIMIZE=1 \
	-DFIPS_1402_COMPLIANT=1 \
	-DACCESS_POINT_STA_VERIFICATION_TIME=5000 \
	-DAL_IMPL_INLINE=inline \
	-D_AL_PRINT_LOG_ENABLED \
	-D_AL_PRINT_LOG_FILTER=0x201 \
	-D_MESHAP_AL_LOG_MASK_=0x200 \
	-DACCESS_POINT_STA_AGEING_TIME=5000 \
	-DACCESS_POINT_STAY_AWAKE_COUNT=500 \
	-D_MESHAP_OPTION_DECODE \
	-DAES_BIG_ENDIAN=4321 \
	-DAES_LITTLE_ENDIAN=1234 \
	-DINTERNAL_BYTE_ORDER=AES_LITTLE_ENDIAN \
	-DDOT1X_IMPL_BYTE_ORDER=DOT1X_BE \
	-DRC4_INLINE=inline \
	$(FPIC)

ifeq ($(CONFIG_BIG_ENDIAN),y)
	TARGET_CFLAGS+= \
		-DCODEC_SYS_BIG_ENDIAN \
		-DPLATFORM_BYTE_ORDER=AES_BIG_ENDIAN
else
	TARGET_CFLAGS+= \
		-DCODEC_SYS_LITTLE_ENDIAN \
		-DPLATFORM_BYTE_ORDER=AES_LITTLE_ENDIAN
endif

define Build/Prepare
	mkdir -p $(PKG_BUILD_DIR)
	$(CP) ./src/* $(PKG_BUILD_DIR)/
endef

define Build/Compile
	$(MAKE_VARS) $(MAKE) -C $(PKG_BUILD_DIR) \
		$(TARGET_CONFIGURE_OPTS) \
		CFLAGS="$(TARGET_CFLAGS)"
endef

define Package/libalconf/install
	$(INSTALL_DIR) $(1)/lib
	$(INSTALL_BIN) $(PKG_BUILD_DIR)/libalconf.so.1 $(1)/lib
	$(LN) libalconf.so.1 $(1)/lib/libalconf.so
endef

define Build/InstallDev
	$(INSTALL_DIR) $(1)/usr/lib
	$(INSTALL_DIR) $(1)/usr/include/libalconf
	$(INSTALL_BIN) $(PKG_BUILD_DIR)/libalconf.so.1 $(1)/usr/lib
	$(INSTALL_BIN) $(PKG_BUILD_DIR)/al_conf_lib.h $(1)/usr/include/libalconf
	$(LN) libalconf.so.1 $(1)/usr/lib/libalconf.so
endef

$(eval $(call BuildPackage,libalconf))
