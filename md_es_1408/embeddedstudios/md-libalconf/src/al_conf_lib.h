/********************************************************************************
* MeshDynamics
* --------------
* File     : al_conf_lib.h
* Comments : al_conf get/set library used for configd/snmpd
* Created  : 6/21/2007
* Author   : Prachiti Gaikwad
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  0  |6/21/2007 | Created                                         |Prachiti|
* -----------------------------------------------------------------------------
********************************************************************************/
#ifndef _AL_CONF_LIB_H_
#define _AL_CONF_LIB_H_

int libalconf_read_config_data();

#endif
