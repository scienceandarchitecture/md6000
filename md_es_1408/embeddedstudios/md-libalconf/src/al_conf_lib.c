/********************************************************************************
* MeshDynamics
* --------------
* File     : al_conf_lib.c
* Comments : al_conf get/set library used for configd/snmpd
* Created  : 6/21/2007
* Author   : Prachiti Gaikwad
* Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
*
* File Revision History
* -----------------------------------------------------------------------------
* | No  |Date      |  Comment                                        | Author |
* -----------------------------------------------------------------------------
* |  1  |8/22/2008 | Changes for SIP                                 |Abhijit |
* -----------------------------------------------------------------------------
* |  0  |6/21/2007 | Created                                         |Prachiti|
* -----------------------------------------------------------------------------
********************************************************************************/

//RAMESH16MIG added header files for lseek
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <errno.h>

#include <string.h>
#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include <fcntl.h>
#include <syslog.h>

#include "al.h"
#include "al_context.h"
#include "al_impl_context.h"
#include "al_conf.h"
#include "dot11e_conf.h"
#include "acl_conf.h"
#include "al_codec.h"

#define FILE_MESHAP_CONF    "/etc/meshap.conf"

static int debug_level = AL_LOG_TYPE_INFORMATION | AL_LOG_TYPE_ERROR & 0;

/*
 * Open meshap.conf file,parse and return parser handle ie al_conf_handle
 */
int libalconf_read_config_data()
{
   int           offset, al_conf_handle, fd;
   unsigned char *file_data;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   /**
    * Read in the config file into memory
    */

   fd = open(FILE_MESHAP_CONF, O_RDONLY);

   if (fd == -1)
   {
	  al_print_log(AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : Error while opening FILE_MESHAP_CONF : %s<%d>\n",__func__,__LINE__);
      fprintf(stderr, "CONFIGD:\tError opening "FILE_MESHAP_CONF " for reading...\n");
      return -1;
   }

   offset = lseek(fd, 0, SEEK_END);
   if (offset == -1)
   {
      close(fd);
	  al_print_log(AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : Error while seeking end of FILE_MESHAP_CONF : %s<%d>\n",__func__,__LINE__);
      fprintf(stderr, "CONFIGD:\tError seeking end of "FILE_MESHAP_CONF "...\n");
      return -1;
   }

   file_data = (unsigned char *)malloc(offset + 1);
   memset(file_data, 0, offset + 1);

   lseek(fd, 0, SEEK_SET);

   read(fd, file_data, offset);

   file_data[offset] = '\0';
   close(fd);

   al_conf_handle = al_conf_parse(AL_CONTEXT file_data);

   if (al_conf_handle == 0)
   {
      free(file_data);
	  al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : Error while parsing FILE_MESHAP_CONF : %s<%d>\n",__func__,__LINE__);
      printf("\nError in al_conf");
      return -1;
   }

   free(file_data);

   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit: %s<%d>\n",__func__,__LINE__);

   return al_conf_handle;
}


int al_open_file(AL_CONTEXT_PARAM_DECL int file_id, int mode)
{
   int fd;
   int open_mode;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter: %s<%d>\n",__func__,__LINE__);

   if (mode == AL_OPEN_FILE_MODE_READ)
   {
      open_mode = O_RDONLY;
   }
   else
   {
      if (file_id == AL_FILE_ID_MESHAP_CONFIG)
      {
         unlink("/etc/meshap.conf");
      }
      else if (file_id == AL_FILE_ID_DOT11E_CONFIG)
      {
         unlink("/etc/dot11e.conf");
      }
      else if (file_id == AL_FILE_ID_ACL_CONFIG)
      {
         unlink("/etc/acl.conf");
      }
      else if (file_id == AL_FILE_ID_SIP_CONFIG)
      {
         unlink("/etc/sip.conf");
      }
      open_mode = O_WRONLY | O_CREAT;
   }

   fd = -1;

   if (file_id == AL_FILE_ID_MESHAP_CONFIG)
   {
      fd = open("/etc/meshap.conf", open_mode);
   }
   else if (file_id == AL_FILE_ID_DOT11E_CONFIG)
   {
      fd = open("/etc/dot11e.conf", open_mode);
   }
   else if (file_id == AL_FILE_ID_ACL_CONFIG)
   {
      fd = open("/etc/acl.conf", open_mode);
   }
   else if (file_id == AL_FILE_ID_SIP_CONFIG)
   {
      fd = open("/etc/sip.conf", open_mode);
   }

   if (fd == -1)
   {
      if (file_id == AL_FILE_ID_MESHAP_CONFIG)
      {
		 al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : Error while opening /etc/meshap.conf : %s<%d>\n",__func__,__LINE__);
         perror("Error opening /etc/meshap.conf\n");
         printf("\tError opening /etc/meshap.conf for mode %d...\n", mode);
      }
      else if (file_id == AL_FILE_ID_DOT11E_CONFIG)
      {
		 al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : Error while opening /etc/dot11e.conf : %s<%d>\n",__func__,__LINE__);
         perror("Error opening /etc/dot11e.conf\n");
         printf("\tError opening /etc/dot11e.conf for mode %d...\n", mode);
      }
      else if (file_id == AL_FILE_ID_ACL_CONFIG)
      {
		 al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : Error while opening /etc/acl.conf : %s<%d>\n",__func__,__LINE__);
         perror("Error opening /etc/acl.conf\n");
         printf("\tError opening /etc/acl.conf for mode %d...\n", mode);
      }
      else if (file_id == AL_FILE_ID_SIP_CONFIG)
      {
		 al_print_log(AL_CONTEXT AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : Error while opening /etc/sip.conf : %s<%d>\n",__func__,__LINE__);
         perror("Error opening /etc/sip.conf\n");
         printf("\tError opening /etc/sip.conf for mode %d...\n", mode);
      }

      return -1;
   }
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit: %s<%d>\n",__func__,__LINE__);

   return fd;
}


int al_read_file_line(AL_CONTEXT_PARAM_DECL int file_handle, char *string, int n)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter: %s<%d>\n",__func__,__LINE__);
   if (read(file_handle, string, n) <= 0)
   {
	  al_print_log(AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : read() failed : %s<%d>\n",__func__,__LINE__);
      return -1;
   }
   else
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
      return 0;
   }
}


int al_write_file(AL_CONTEXT_PARAM_DECL int file_handle, unsigned char *buffer, int bytes)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter: %s<%d>\n",__func__,__LINE__);
   if (write(file_handle, buffer, bytes) <= 0)
   {
	  al_print_log(AL_LOG_TYPE_ERROR,"CONFIGD : ERROR : write() failed : %s<%d>\n",__func__,__LINE__);
      return -1;
   }
   else
   {
      al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
      return 0;
   }
}


int al_close_file(AL_CONTEXT_PARAM_DECL int file_handle)
{
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter: %s<%d>\n",__func__,__LINE__);
   close(file_handle);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit: %s<%d>\n",__func__,__LINE__);

   return 0;
}


void *al_heap_alloc(AL_CONTEXT_PARAM_DECL size_t size)
{
	void *ptr = NULL;
	ptr =  malloc(size);
    //al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter: %s<%d>\n",__func__,__LINE__);
	if(ptr) {
		memset(ptr, 0, size);
	}
    //al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
	return ptr;
}


void al_heap_free(AL_CONTEXT_PARAM_DECL void *memblock)
{
   //al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter: %s<%d>\n",__func__,__LINE__);
   free(memblock);
   //al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
}


unsigned char *al_get_gen_2KB_buffer(AL_CONTEXT_PARAM_DECL_SINGLE)
{
   unsigned char *file_data;
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter: %s<%d>\n",__func__,__LINE__);
   file_data = (unsigned char *)malloc(2048);
   al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
   return file_data;
}


void al_release_gen_2KB_buffer(AL_CONTEXT_PARAM_DECL unsigned char *buffer)
{
   //al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Enter: %s<%d>\n",__func__,__LINE__);
   free(buffer);
   //al_print_log(AL_CONTEXT AL_LOG_TYPE_FLOW,"CONFIGD : FLOW : Exit: %s<%d>\n",__func__,__LINE__);
}


int al_print_log(AL_CONTEXT_PARAM_DECL int log_type, const char *format, ...)
{
   va_list args_list;
   if(log_type & debug_level) {
   va_start(args_list, format);
   vsyslog(LOG_INFO,format, args_list);
   }
   return 0;
}
